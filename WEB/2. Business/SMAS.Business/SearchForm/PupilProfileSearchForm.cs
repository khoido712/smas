﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.SearchForm
{
    public class PupilProfileSearchForm
    {

        /// <summary>
        /// Tìm kiếm 'in' với column PupilProfileID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PupilProfileID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PupilProfileID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PupilProfileID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PupilProfileID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PupilProfileID</para>
        /// </summary>
        public object PupilProfileID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CurrentClassID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.CurrentClassID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.CurrentClassID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.CurrentClassID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.CurrentClassID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với CurrentClassID</para>
        /// </summary>
        public object CurrentClassID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CurrentClassID(ClassProfile).EducationLevelID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.EducationLevelID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.EducationLevelID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EducationLevelID</para>
        /// </summary>
        public object EducationLevelID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CurrentClassID(ClassProfile).EducationLevelID(EducationLevel).Grade. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassProfile.EducationLevel.Grade)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassProfile.EducationLevel.Grade = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Grade</para>
        /// </summary>
        public object Grade { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CurrentSchoolID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.CurrentSchoolID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.CurrentSchoolID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.CurrentSchoolID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.CurrentSchoolID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với CurrentSchoolID</para>
        /// </summary>
        public object CurrentSchoolID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CurrentAcademicYearID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.CurrentAcademicYearID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.CurrentAcademicYearID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.CurrentAcademicYearID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.CurrentAcademicYearID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với CurrentAcademicYearID</para>
        /// </summary>
        public object CurrentAcademicYearID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column AreaID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.AreaID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.AreaID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.AreaID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.AreaID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với AreaID</para>
        /// </summary>
        public object AreaID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ProvinceID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ProvinceID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ProvinceID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ProvinceID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ProvinceID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ProvinceID</para>
        /// </summary>
        public object ProvinceID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column DistrictID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.DistrictID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.DistrictID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.DistrictID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.DistrictID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với DistrictID</para>
        /// </summary>
        public object DistrictID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column CommuneID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.CommuneID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.CommuneID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.CommuneID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.CommuneID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với CommuneID</para>
        /// </summary>
        public object CommuneID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column EthnicID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.EthnicID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.EthnicID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.EthnicID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.EthnicID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EthnicID</para>
        /// </summary>
        public object EthnicID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ReligionID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ReligionID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ReligionID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ReligionID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ReligionID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ReligionID</para>
        /// </summary>
        public object ReligionID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PolicyTargetID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PolicyTargetID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PolicyTargetID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PolicyTargetID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PolicyTargetID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PolicyTargetID</para>
        /// </summary>
        public object PolicyTargetID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column FamilyTypeID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.FamilyTypeID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.FamilyTypeID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.FamilyTypeID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.FamilyTypeID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với FamilyTypeID</para>
        /// </summary>
        public object FamilyTypeID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PriorityTypeID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PriorityTypeID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PriorityTypeID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PriorityTypeID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PriorityTypeID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PriorityTypeID</para>
        /// </summary>
        public object PriorityTypeID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PreviousGraduationLevelID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PreviousGraduationLevelID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PreviousGraduationLevelID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PreviousGraduationLevelID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PreviousGraduationLevelID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PreviousGraduationLevelID</para>
        /// </summary>
        public object PreviousGraduationLevelID { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PupilCode
        /// </summary>
        public string PupilCode { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PupilCode
        /// </summary>
        public string E_PupilCode { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column Name
        /// </summary>
        public string E_Name { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FullName
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FullName
        /// </summary>
        public string E_FullName { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column Genre. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.Genre)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.Genre in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.Genre in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.Genre = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với Genre</para>
        /// </summary>
        public object Genre { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column BirthDate
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column BirthDate => Tạo câu lệnh SQL x.BirthDate >= F_BirthDate
        /// </summary>
        public DateTime? F_BirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column BirthDate => Tạo câu lệnh SQL x.BirthDate >= T_BirthDate
        /// </summary>
        public DateTime? T_BirthDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column BirthPlace
        /// </summary>
        public string BirthPlace { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column BirthPlace
        /// </summary>
        public string E_BirthPlace { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column HomeTown
        /// </summary>
        public string HomeTown { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column HomeTown
        /// </summary>
        public string E_HomeTown { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column Telephone
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column Telephone
        /// </summary>
        public string E_Telephone { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column Mobile
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column Mobile
        /// </summary>
        public string E_Mobile { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column Email
        /// </summary>
        public string E_Email { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column TempResidentalAddress
        /// </summary>
        public string TempResidentalAddress { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column TempResidentalAddress
        /// </summary>
        public string E_TempResidentalAddress { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column PermanentResidentalAddress
        /// </summary>
        public string PermanentResidentalAddress { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column PermanentResidentalAddress
        /// </summary>
        public string E_PermanentResidentalAddress { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsResidentIn
        /// </summary>
        public bool? IsResidentIn { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsDisabled
        /// </summary>
        public bool? IsDisabled { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column DisabledTypeID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.DisabledTypeID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.DisabledTypeID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.DisabledTypeID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.DisabledTypeID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với DisabledTypeID</para>
        /// </summary>
        public object DisabledTypeID { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column DisabledSeverity
        /// </summary>
        public string DisabledSeverity { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column DisabledSeverity
        /// </summary>
        public string E_DisabledSeverity { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column BloodType. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.BloodType)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.BloodType in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.BloodType in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.BloodType = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với BloodType</para>
        /// </summary>
        public object BloodType { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column EnrolmentDate
        /// </summary>
        public DateTime? EnrolmentDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column EnrolmentDate => Tạo câu lệnh SQL x.EnrolmentDate >= F_EnrolmentDate
        /// </summary>
        public DateTime? F_EnrolmentDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column EnrolmentDate => Tạo câu lệnh SQL x.EnrolmentDate >= T_EnrolmentDate
        /// </summary>
        public DateTime? T_EnrolmentDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ForeignLanguageTraining. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ForeignLanguageTraining)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ForeignLanguageTraining in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ForeignLanguageTraining in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ForeignLanguageTraining = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ForeignLanguageTraining</para>
        /// </summary>
        public object ForeignLanguageTraining { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsYoungPioneerMember
        /// </summary>
        public bool? IsYoungPioneerMember { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column YoungPioneerJoinedDate
        /// </summary>
        public DateTime? YoungPioneerJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column YoungPioneerJoinedDate => Tạo câu lệnh SQL x.YoungPioneerJoinedDate >= F_YoungPioneerJoinedDate
        /// </summary>
        public DateTime? F_YoungPioneerJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column YoungPioneerJoinedDate => Tạo câu lệnh SQL x.YoungPioneerJoinedDate >= T_YoungPioneerJoinedDate
        /// </summary>
        public DateTime? T_YoungPioneerJoinedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column YoungPioneerJoinedPlace
        /// </summary>
        public string YoungPioneerJoinedPlace { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column YoungPioneerJoinedPlace
        /// </summary>
        public string E_YoungPioneerJoinedPlace { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsYouthLeageMember
        /// </summary>
        public bool? IsYouthLeageMember { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column YouthLeagueJoinedDate
        /// </summary>
        public DateTime? YouthLeagueJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column YouthLeagueJoinedDate => Tạo câu lệnh SQL x.YouthLeagueJoinedDate >= F_YouthLeagueJoinedDate
        /// </summary>
        public DateTime? F_YouthLeagueJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column YouthLeagueJoinedDate => Tạo câu lệnh SQL x.YouthLeagueJoinedDate >= T_YouthLeagueJoinedDate
        /// </summary>
        public DateTime? T_YouthLeagueJoinedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column YouthLeagueJoinedPlace
        /// </summary>
        public string YouthLeagueJoinedPlace { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column YouthLeagueJoinedPlace
        /// </summary>
        public string E_YouthLeagueJoinedPlace { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsCommunistPartyMember
        /// </summary>
        public bool? IsCommunistPartyMember { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column CommunistPartyJoinedDate
        /// </summary>
        public DateTime? CommunistPartyJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column CommunistPartyJoinedDate => Tạo câu lệnh SQL x.CommunistPartyJoinedDate >= F_CommunistPartyJoinedDate
        /// </summary>
        public DateTime? F_CommunistPartyJoinedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column CommunistPartyJoinedDate => Tạo câu lệnh SQL x.CommunistPartyJoinedDate >= T_CommunistPartyJoinedDate
        /// </summary>
        public DateTime? T_CommunistPartyJoinedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column CommunistPartyJoinedPlace
        /// </summary>
        public string CommunistPartyJoinedPlace { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column CommunistPartyJoinedPlace
        /// </summary>
        public string E_CommunistPartyJoinedPlace { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FatherFullName
        /// </summary>
        public string FatherFullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FatherFullName
        /// </summary>
        public string E_FatherFullName { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column FatherBirthDate
        /// </summary>
        public DateTime? FatherBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column FatherBirthDate => Tạo câu lệnh SQL x.FatherBirthDate >= F_FatherBirthDate
        /// </summary>
        public DateTime? F_FatherBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column FatherBirthDate => Tạo câu lệnh SQL x.FatherBirthDate >= T_FatherBirthDate
        /// </summary>
        public DateTime? T_FatherBirthDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FatherJob
        /// </summary>
        public string FatherJob { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FatherJob
        /// </summary>
        public string E_FatherJob { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FatherMobile
        /// </summary>
        public string FatherMobile { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FatherMobile
        /// </summary>
        public string E_FatherMobile { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column MotherFullName
        /// </summary>
        public string MotherFullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column MotherFullName
        /// </summary>
        public string E_MotherFullName { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column MotherBirthDate
        /// </summary>
        public DateTime? MotherBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column MotherBirthDate => Tạo câu lệnh SQL x.MotherBirthDate >= F_MotherBirthDate
        /// </summary>
        public DateTime? F_MotherBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column MotherBirthDate => Tạo câu lệnh SQL x.MotherBirthDate >= T_MotherBirthDate
        /// </summary>
        public DateTime? T_MotherBirthDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column MotherJob
        /// </summary>
        public string MotherJob { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column MotherJob
        /// </summary>
        public string E_MotherJob { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column MotherMobile
        /// </summary>
        public string MotherMobile { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column MotherMobile
        /// </summary>
        public string E_MotherMobile { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column SponsorFullName
        /// </summary>
        public string SponsorFullName { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column SponsorFullName
        /// </summary>
        public string E_SponsorFullName { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column SponsorBirthDate
        /// </summary>
        public DateTime? SponsorBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column SponsorBirthDate => Tạo câu lệnh SQL x.SponsorBirthDate >= F_SponsorBirthDate
        /// </summary>
        public DateTime? F_SponsorBirthDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column SponsorBirthDate => Tạo câu lệnh SQL x.SponsorBirthDate >= T_SponsorBirthDate
        /// </summary>
        public DateTime? T_SponsorBirthDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column SponsorJob
        /// </summary>
        public string SponsorJob { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column SponsorJob
        /// </summary>
        public string E_SponsorJob { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column SponsorMobile
        /// </summary>
        public string SponsorMobile { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column SponsorMobile
        /// </summary>
        public string E_SponsorMobile { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ProfileStatus. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ProfileStatus)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ProfileStatus in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ProfileStatus in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ProfileStatus = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ProfileStatus</para>
        /// </summary>
        public object ProfileStatus { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column CreatedDate
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column CreatedDate => Tạo câu lệnh SQL x.CreatedDate >= F_CreatedDate
        /// </summary>
        public DateTime? F_CreatedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column CreatedDate => Tạo câu lệnh SQL x.CreatedDate >= T_CreatedDate
        /// </summary>
        public DateTime? T_CreatedDate { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsActive
        /// </summary>
        public bool? IsActive { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column ModifiedDate
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '>=' với column ModifiedDate => Tạo câu lệnh SQL x.ModifiedDate >= F_ModifiedDate
        /// </summary>
        public DateTime? F_ModifiedDate { get; set; }

        /// <summary>
        /// Tìm kiếm '<=' với column ModifiedDate => Tạo câu lệnh SQL x.ModifiedDate >= T_ModifiedDate
        /// </summary>
        public DateTime? T_ModifiedDate { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FatherEmail
        /// </summary>
        public string FatherEmail { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FatherEmail
        /// </summary>
        public string E_FatherEmail { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column MotherEmail
        /// </summary>
        public string MotherEmail { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column MotherEmail
        /// </summary>
        public string E_MotherEmail { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column SponsorEmail
        /// </summary>
        public string SponsorEmail { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column SponsorEmail
        /// </summary>
        public string E_SponsorEmail { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ChildOrder. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ChildOrder)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ChildOrder in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ChildOrder in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ChildOrder = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ChildOrder</para>
        /// </summary>
        public object ChildOrder { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column NumberOfChild. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.NumberOfChild)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.NumberOfChild in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.NumberOfChild in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.NumberOfChild = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với NumberOfChild</para>
        /// </summary>
        public object NumberOfChild { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column EatingHabit. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.EatingHabit)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.EatingHabit in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.EatingHabit in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.EatingHabit = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EatingHabit</para>
        /// </summary>
        public object EatingHabit { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ReactionTrainingHabit. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ReactionTrainingHabit)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ReactionTrainingHabit in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ReactionTrainingHabit in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ReactionTrainingHabit = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ReactionTrainingHabit</para>
        /// </summary>
        public object ReactionTrainingHabit { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column AttitudeInStrangeScene. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.AttitudeInStrangeScene)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.AttitudeInStrangeScene in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.AttitudeInStrangeScene in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.AttitudeInStrangeScene = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với AttitudeInStrangeScene</para>
        /// </summary>
        public object AttitudeInStrangeScene { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column OtherHabit
        /// </summary>
        public string OtherHabit { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column OtherHabit
        /// </summary>
        public string E_OtherHabit { get; set; }


        /// <summary>
        /// Tìm kiếm 'like' với column FavoriteToy
        /// </summary>
        public string FavoriteToy { get; set; }

        /// <summary>
        /// Tìm kiếm '=' với column FavoriteToy
        /// </summary>
        public string E_FavoriteToy { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column EvaluationConfigID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.EvaluationConfigID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.EvaluationConfigID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.EvaluationConfigID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.EvaluationConfigID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với EvaluationConfigID</para>
        /// </summary>
        public object EvaluationConfigID { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column PolicyRegimeID. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.PolicyRegimeID)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.PolicyRegimeID in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.PolicyRegimeID in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.PolicyRegimeID = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với PolicyRegimeID</para>
        /// </summary>
        public object PolicyRegimeID { get; set; }


        /// <summary>
        /// Tìm kiếm '=' với column IsSupportForLearning
        /// </summary>
        public bool? IsSupportForLearning { get; set; }


        /// <summary>
        /// Tìm kiếm 'in' với column ClassType. Kiểu dữ liệu:
        /// <para>- IQueryable{int} => Tạo thành câu lệnh SQL exists (select 1 from IQueryable{int} p where p = x.ClassType)</para>
        /// <para>- string => Các giá trị cách nhau bởi dấu phẩy. Ví dụ: "232,, ,432, 4" 
        /// => Tạo thành câu lệnh SQL x.ClassType in (232, 432, 4)</para>
        /// <para>- IEnumerable (các kiểu dữ liệu kế thừa IEnumerable như: List, object[]...) 
        /// => Tạo thành câu lệnh SQL x.ClassType in (danh sách các phần tử trong IEnumerable - 
        /// không tính giá trị null - các phần tử trong IEnumerable phải chuyển sang kiểu int được)</para>
        /// <para>- Kiểu dữ liệu khác => phải chuyển sang kiểu int được, Tạo thành câu lệnh SQL x.ClassType = [giá trị truyền vào]</para>
        /// <para>- Trường hợp string hay kiểu dữ liệu khác khi chuyển sang kiểu int có giá trị là -1 
        /// thì không thực hiện thêm điều kiện tìm kiếm với ClassType</para>
        /// </summary>
        public object ClassType { get; set; }




        public IQueryable<PupilProfile> Search(IQueryable<PupilProfile> Query)
        {

            #region PupilProfileID
            if (PupilProfileID != null)
            {
                if (PupilProfileID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PupilProfileID).Any(y => y == x.PupilProfileID));
                }
                else
                {
                    List<int?> l_PupilProfileID = PupilProfileID.ConvertToListInt();
                    if (l_PupilProfileID.Count == 1)
                    {
                        if (l_PupilProfileID[0] != -1)
                        {
                            int? l2_PupilProfileID = l_PupilProfileID[0];
                            Query = Query.Where(x => x.PupilProfileID == l2_PupilProfileID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PupilProfileID.Contains(x.PupilProfileID));
                    }
                }
            }
            #endregion


            #region CurrentClassID
            if (CurrentClassID != null)
            {
                if (CurrentClassID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)CurrentClassID).Any(y => y == x.CurrentClassID));
                }
                else
                {
                    List<int?> l_CurrentClassID = CurrentClassID.ConvertToListInt();
                    if (l_CurrentClassID.Count == 1)
                    {
                        if (l_CurrentClassID[0] != -1)
                        {
                            int? l2_CurrentClassID = l_CurrentClassID[0];
                            Query = Query.Where(x => x.CurrentClassID == l2_CurrentClassID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_CurrentClassID.Contains(x.CurrentClassID));
                    }
                }
            }
            #endregion


            #region EducationLevelID
            if (EducationLevelID != null)
            {
                if (EducationLevelID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EducationLevelID).Any(y => y == x.ClassProfile.EducationLevelID));
                }
                else
                {
                    List<int?> l_EducationLevelID = EducationLevelID.ConvertToListInt();
                    if (l_EducationLevelID.Count == 1)
                    {
                        if (l_EducationLevelID[0] != -1)
                        {
                            int? l2_EducationLevelID = l_EducationLevelID[0];
                            Query = Query.Where(x => x.ClassProfile.EducationLevelID == l2_EducationLevelID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EducationLevelID.Contains(x.ClassProfile.EducationLevelID));
                    }
                }
            }
            #endregion


            #region Grade
            if (Grade != null)
            {
                if (Grade is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Grade).Any(y => y == x.ClassProfile.EducationLevel.Grade));
                }
                else
                {
                    List<int?> l_Grade = Grade.ConvertToListInt();
                    if (l_Grade.Count == 1)
                    {
                        if (l_Grade[0] != -1)
                        {
                            int? l2_Grade = l_Grade[0];
                            Query = Query.Where(x => x.ClassProfile.EducationLevel.Grade == l2_Grade);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Grade.Contains(x.ClassProfile.EducationLevel.Grade));
                    }
                }
            }
            #endregion


            #region CurrentSchoolID
            if (CurrentSchoolID != null)
            {
                if (CurrentSchoolID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)CurrentSchoolID).Any(y => y == x.CurrentSchoolID));
                }
                else
                {
                    List<int?> l_CurrentSchoolID = CurrentSchoolID.ConvertToListInt();
                    if (l_CurrentSchoolID.Count == 1)
                    {
                        if (l_CurrentSchoolID[0] != -1)
                        {
                            int? l2_CurrentSchoolID = l_CurrentSchoolID[0];
                            Query = Query.Where(x => x.CurrentSchoolID == l2_CurrentSchoolID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_CurrentSchoolID.Contains(x.CurrentSchoolID));
                    }
                }
            }
            #endregion


            #region CurrentAcademicYearID
            if (CurrentAcademicYearID != null)
            {
                if (CurrentAcademicYearID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)CurrentAcademicYearID).Any(y => y == x.CurrentAcademicYearID));
                }
                else
                {
                    List<int?> l_CurrentAcademicYearID = CurrentAcademicYearID.ConvertToListInt();
                    if (l_CurrentAcademicYearID.Count == 1)
                    {
                        if (l_CurrentAcademicYearID[0] != -1)
                        {
                            int? l2_CurrentAcademicYearID = l_CurrentAcademicYearID[0];
                            Query = Query.Where(x => x.CurrentAcademicYearID == l2_CurrentAcademicYearID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_CurrentAcademicYearID.Contains(x.CurrentAcademicYearID));
                    }
                }
            }
            #endregion


            #region AreaID
            if (AreaID != null)
            {
                if (AreaID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)AreaID).Any(y => y == x.AreaID));
                }
                else
                {
                    List<int?> l_AreaID = AreaID.ConvertToListInt();
                    if (l_AreaID.Count == 1)
                    {
                        if (l_AreaID[0] != -1)
                        {
                            int? l2_AreaID = l_AreaID[0];
                            Query = Query.Where(x => x.AreaID == l2_AreaID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_AreaID.Contains(x.AreaID));
                    }
                }
            }
            #endregion


            #region ProvinceID
            if (ProvinceID != null)
            {
                if (ProvinceID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ProvinceID).Any(y => y == x.ProvinceID));
                }
                else
                {
                    List<int?> l_ProvinceID = ProvinceID.ConvertToListInt();
                    if (l_ProvinceID.Count == 1)
                    {
                        if (l_ProvinceID[0] != -1)
                        {
                            int? l2_ProvinceID = l_ProvinceID[0];
                            Query = Query.Where(x => x.ProvinceID == l2_ProvinceID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ProvinceID.Contains(x.ProvinceID));
                    }
                }
            }
            #endregion


            #region DistrictID
            if (DistrictID != null)
            {
                if (DistrictID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)DistrictID).Any(y => y == x.DistrictID));
                }
                else
                {
                    List<int?> l_DistrictID = DistrictID.ConvertToListInt();
                    if (l_DistrictID.Count == 1)
                    {
                        if (l_DistrictID[0] != -1)
                        {
                            int? l2_DistrictID = l_DistrictID[0];
                            Query = Query.Where(x => x.DistrictID == l2_DistrictID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_DistrictID.Contains(x.DistrictID));
                    }
                }
            }
            #endregion


            #region CommuneID
            if (CommuneID != null)
            {
                if (CommuneID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)CommuneID).Any(y => y == x.CommuneID));
                }
                else
                {
                    List<int?> l_CommuneID = CommuneID.ConvertToListInt();
                    if (l_CommuneID.Count == 1)
                    {
                        if (l_CommuneID[0] != -1)
                        {
                            int? l2_CommuneID = l_CommuneID[0];
                            Query = Query.Where(x => x.CommuneID == l2_CommuneID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_CommuneID.Contains(x.CommuneID));
                    }
                }
            }
            #endregion


            #region EthnicID
            if (EthnicID != null)
            {
                if (EthnicID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EthnicID).Any(y => y == x.EthnicID));
                }
                else
                {
                    List<int?> l_EthnicID = EthnicID.ConvertToListInt();
                    if (l_EthnicID.Count == 1)
                    {
                        if (l_EthnicID[0] != -1)
                        {
                            int? l2_EthnicID = l_EthnicID[0];
                            Query = Query.Where(x => x.EthnicID == l2_EthnicID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EthnicID.Contains(x.EthnicID));
                    }
                }
            }
            #endregion


            #region ReligionID
            if (ReligionID != null)
            {
                if (ReligionID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ReligionID).Any(y => y == x.ReligionID));
                }
                else
                {
                    List<int?> l_ReligionID = ReligionID.ConvertToListInt();
                    if (l_ReligionID.Count == 1)
                    {
                        if (l_ReligionID[0] != -1)
                        {
                            int? l2_ReligionID = l_ReligionID[0];
                            Query = Query.Where(x => x.ReligionID == l2_ReligionID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ReligionID.Contains(x.ReligionID));
                    }
                }
            }
            #endregion


            #region PolicyTargetID
            if (PolicyTargetID != null)
            {
                if (PolicyTargetID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PolicyTargetID).Any(y => y == x.PolicyTargetID));
                }
                else
                {
                    List<int?> l_PolicyTargetID = PolicyTargetID.ConvertToListInt();
                    if (l_PolicyTargetID.Count == 1)
                    {
                        if (l_PolicyTargetID[0] != -1)
                        {
                            int? l2_PolicyTargetID = l_PolicyTargetID[0];
                            Query = Query.Where(x => x.PolicyTargetID == l2_PolicyTargetID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PolicyTargetID.Contains(x.PolicyTargetID));
                    }
                }
            }
            #endregion


            #region FamilyTypeID
            if (FamilyTypeID != null)
            {
                if (FamilyTypeID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)FamilyTypeID).Any(y => y == x.FamilyTypeID));
                }
                else
                {
                    List<int?> l_FamilyTypeID = FamilyTypeID.ConvertToListInt();
                    if (l_FamilyTypeID.Count == 1)
                    {
                        if (l_FamilyTypeID[0] != -1)
                        {
                            int? l2_FamilyTypeID = l_FamilyTypeID[0];
                            Query = Query.Where(x => x.FamilyTypeID == l2_FamilyTypeID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_FamilyTypeID.Contains(x.FamilyTypeID));
                    }
                }
            }
            #endregion


            #region PriorityTypeID
            if (PriorityTypeID != null)
            {
                if (PriorityTypeID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PriorityTypeID).Any(y => y == x.PriorityTypeID));
                }
                else
                {
                    List<int?> l_PriorityTypeID = PriorityTypeID.ConvertToListInt();
                    if (l_PriorityTypeID.Count == 1)
                    {
                        if (l_PriorityTypeID[0] != -1)
                        {
                            int? l2_PriorityTypeID = l_PriorityTypeID[0];
                            Query = Query.Where(x => x.PriorityTypeID == l2_PriorityTypeID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PriorityTypeID.Contains(x.PriorityTypeID));
                    }
                }
            }
            #endregion


            #region PreviousGraduationLevelID
            if (PreviousGraduationLevelID != null)
            {
                if (PreviousGraduationLevelID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PreviousGraduationLevelID).Any(y => y == x.PreviousGraduationLevelID));
                }
                else
                {
                    List<int?> l_PreviousGraduationLevelID = PreviousGraduationLevelID.ConvertToListInt();
                    if (l_PreviousGraduationLevelID.Count == 1)
                    {
                        if (l_PreviousGraduationLevelID[0] != -1)
                        {
                            int? l2_PreviousGraduationLevelID = l_PreviousGraduationLevelID[0];
                            Query = Query.Where(x => x.PreviousGraduationLevelID == l2_PreviousGraduationLevelID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PreviousGraduationLevelID.Contains(x.PreviousGraduationLevelID));
                    }
                }
            }
            #endregion


            #region PupilCode
            if (!string.IsNullOrWhiteSpace(PupilCode)) Query = Query.Where(x => x.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            #endregion

            #region E_PupilCode
            if (!string.IsNullOrWhiteSpace(E_PupilCode)) Query = Query.Where(x => x.PupilCode.ToUpper() == E_PupilCode.ToUpper());
            #endregion


            #region Name
            if (!string.IsNullOrWhiteSpace(Name)) Query = Query.Where(x => x.Name.ToUpper().Contains(Name.ToUpper()));
            #endregion

            #region E_Name
            if (!string.IsNullOrWhiteSpace(E_Name)) Query = Query.Where(x => x.Name.ToUpper() == E_Name.ToUpper());
            #endregion


            #region FullName
            if (!string.IsNullOrWhiteSpace(FullName)) Query = Query.Where(x => x.FullName.ToUpper().Contains(FullName.ToUpper()));
            #endregion

            #region E_FullName
            if (!string.IsNullOrWhiteSpace(E_FullName)) Query = Query.Where(x => x.FullName.ToUpper() == E_FullName.ToUpper());
            #endregion


            #region Genre
            if (Genre != null)
            {
                if (Genre is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)Genre).Any(y => y == x.Genre));
                }
                else
                {
                    List<int?> l_Genre = Genre.ConvertToListInt();
                    if (l_Genre.Count == 1)
                    {
                        if (l_Genre[0] != -1)
                        {
                            int? l2_Genre = l_Genre[0];
                            Query = Query.Where(x => x.Genre == l2_Genre);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_Genre.Contains(x.Genre));
                    }
                }
            }
            #endregion


            #region BirthDate
            if (BirthDate != null)
            {
                DateTime l_BirthDate = BirthDate.Value.Date;
                Query = Query.Where(x => x.BirthDate == l_BirthDate);
            }
            #endregion

            #region F_BirthDate
            if (F_BirthDate != null)
            {
                DateTime l_F_BirthDate = F_BirthDate.Value.Date;
                Query = Query.Where(x => x.BirthDate >= l_F_BirthDate);
            }
            #endregion

            #region T_BirthDate
            if (T_BirthDate != null)
            {
                DateTime l_T_BirthDate = T_BirthDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.BirthDate < l_T_BirthDate);
            }
            #endregion


            #region BirthPlace
            if (!string.IsNullOrWhiteSpace(BirthPlace)) Query = Query.Where(x => x.BirthPlace.ToUpper().Contains(BirthPlace.ToUpper()));
            #endregion

            #region E_BirthPlace
            if (!string.IsNullOrWhiteSpace(E_BirthPlace)) Query = Query.Where(x => x.BirthPlace.ToUpper() == E_BirthPlace.ToUpper());
            #endregion


            #region HomeTown
            if (!string.IsNullOrWhiteSpace(HomeTown)) Query = Query.Where(x => x.HomeTown.ToUpper().Contains(HomeTown.ToUpper()));
            #endregion

            #region E_HomeTown
            if (!string.IsNullOrWhiteSpace(E_HomeTown)) Query = Query.Where(x => x.HomeTown.ToUpper() == E_HomeTown.ToUpper());
            #endregion


            #region Telephone
            if (!string.IsNullOrWhiteSpace(Telephone)) Query = Query.Where(x => x.Telephone.ToUpper().Contains(Telephone.ToUpper()));
            #endregion

            #region E_Telephone
            if (!string.IsNullOrWhiteSpace(E_Telephone)) Query = Query.Where(x => x.Telephone.ToUpper() == E_Telephone.ToUpper());
            #endregion


            #region Mobile
            if (!string.IsNullOrWhiteSpace(Mobile)) Query = Query.Where(x => x.Mobile.ToUpper().Contains(Mobile.ToUpper()));
            #endregion

            #region E_Mobile
            if (!string.IsNullOrWhiteSpace(E_Mobile)) Query = Query.Where(x => x.Mobile.ToUpper() == E_Mobile.ToUpper());
            #endregion


            #region Email
            if (!string.IsNullOrWhiteSpace(Email)) Query = Query.Where(x => x.Email.ToUpper().Contains(Email.ToUpper()));
            #endregion

            #region E_Email
            if (!string.IsNullOrWhiteSpace(E_Email)) Query = Query.Where(x => x.Email.ToUpper() == E_Email.ToUpper());
            #endregion


            #region TempResidentalAddress
            if (!string.IsNullOrWhiteSpace(TempResidentalAddress)) Query = Query.Where(x => x.TempResidentalAddress.ToUpper().Contains(TempResidentalAddress.ToUpper()));
            #endregion

            #region E_TempResidentalAddress
            if (!string.IsNullOrWhiteSpace(E_TempResidentalAddress)) Query = Query.Where(x => x.TempResidentalAddress.ToUpper() == E_TempResidentalAddress.ToUpper());
            #endregion


            #region PermanentResidentalAddress
            if (!string.IsNullOrWhiteSpace(PermanentResidentalAddress)) Query = Query.Where(x => x.PermanentResidentalAddress.ToUpper().Contains(PermanentResidentalAddress.ToUpper()));
            #endregion

            #region E_PermanentResidentalAddress
            if (!string.IsNullOrWhiteSpace(E_PermanentResidentalAddress)) Query = Query.Where(x => x.PermanentResidentalAddress.ToUpper() == E_PermanentResidentalAddress.ToUpper());
            #endregion


            #region IsResidentIn
            if (IsResidentIn != null) Query = Query.Where(x => x.IsResidentIn == IsResidentIn);
            #endregion


            #region IsDisabled
            if (IsDisabled != null) Query = Query.Where(x => x.IsDisabled == IsDisabled);
            #endregion


            #region DisabledTypeID
            if (DisabledTypeID != null)
            {
                if (DisabledTypeID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)DisabledTypeID).Any(y => y == x.DisabledTypeID));
                }
                else
                {
                    List<int?> l_DisabledTypeID = DisabledTypeID.ConvertToListInt();
                    if (l_DisabledTypeID.Count == 1)
                    {
                        if (l_DisabledTypeID[0] != -1)
                        {
                            int? l2_DisabledTypeID = l_DisabledTypeID[0];
                            Query = Query.Where(x => x.DisabledTypeID == l2_DisabledTypeID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_DisabledTypeID.Contains(x.DisabledTypeID));
                    }
                }
            }
            #endregion


            #region DisabledSeverity
            if (!string.IsNullOrWhiteSpace(DisabledSeverity)) Query = Query.Where(x => x.DisabledSeverity.ToUpper().Contains(DisabledSeverity.ToUpper()));
            #endregion

            #region E_DisabledSeverity
            if (!string.IsNullOrWhiteSpace(E_DisabledSeverity)) Query = Query.Where(x => x.DisabledSeverity.ToUpper() == E_DisabledSeverity.ToUpper());
            #endregion


            #region BloodType
            if (BloodType != null)
            {
                if (BloodType is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)BloodType).Any(y => y == x.BloodType));
                }
                else
                {
                    List<int?> l_BloodType = BloodType.ConvertToListInt();
                    if (l_BloodType.Count == 1)
                    {
                        if (l_BloodType[0] != -1)
                        {
                            int? l2_BloodType = l_BloodType[0];
                            Query = Query.Where(x => x.BloodType == l2_BloodType);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_BloodType.Contains(x.BloodType));
                    }
                }
            }
            #endregion


            #region EnrolmentDate
            if (EnrolmentDate != null)
            {
                DateTime l_EnrolmentDate = EnrolmentDate.Value.Date;
                Query = Query.Where(x => x.EnrolmentDate == l_EnrolmentDate);
            }
            #endregion

            #region F_EnrolmentDate
            if (F_EnrolmentDate != null)
            {
                DateTime l_F_EnrolmentDate = F_EnrolmentDate.Value.Date;
                Query = Query.Where(x => x.EnrolmentDate >= l_F_EnrolmentDate);
            }
            #endregion

            #region T_EnrolmentDate
            if (T_EnrolmentDate != null)
            {
                DateTime l_T_EnrolmentDate = T_EnrolmentDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.EnrolmentDate < l_T_EnrolmentDate);
            }
            #endregion


            #region ForeignLanguageTraining
            if (ForeignLanguageTraining != null)
            {
                if (ForeignLanguageTraining is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ForeignLanguageTraining).Any(y => y == x.ForeignLanguageTraining));
                }
                else
                {
                    List<int?> l_ForeignLanguageTraining = ForeignLanguageTraining.ConvertToListInt();
                    if (l_ForeignLanguageTraining.Count == 1)
                    {
                        if (l_ForeignLanguageTraining[0] != -1)
                        {
                            int? l2_ForeignLanguageTraining = l_ForeignLanguageTraining[0];
                            Query = Query.Where(x => x.ForeignLanguageTraining == l2_ForeignLanguageTraining);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ForeignLanguageTraining.Contains(x.ForeignLanguageTraining));
                    }
                }
            }
            #endregion


            #region IsYoungPioneerMember
            if (IsYoungPioneerMember != null) Query = Query.Where(x => x.IsYoungPioneerMember == IsYoungPioneerMember);
            #endregion


            #region YoungPioneerJoinedDate
            if (YoungPioneerJoinedDate != null)
            {
                DateTime l_YoungPioneerJoinedDate = YoungPioneerJoinedDate.Value.Date;
                Query = Query.Where(x => x.YoungPioneerJoinedDate == l_YoungPioneerJoinedDate);
            }
            #endregion

            #region F_YoungPioneerJoinedDate
            if (F_YoungPioneerJoinedDate != null)
            {
                DateTime l_F_YoungPioneerJoinedDate = F_YoungPioneerJoinedDate.Value.Date;
                Query = Query.Where(x => x.YoungPioneerJoinedDate >= l_F_YoungPioneerJoinedDate);
            }
            #endregion

            #region T_YoungPioneerJoinedDate
            if (T_YoungPioneerJoinedDate != null)
            {
                DateTime l_T_YoungPioneerJoinedDate = T_YoungPioneerJoinedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.YoungPioneerJoinedDate < l_T_YoungPioneerJoinedDate);
            }
            #endregion


            #region YoungPioneerJoinedPlace
            if (!string.IsNullOrWhiteSpace(YoungPioneerJoinedPlace)) Query = Query.Where(x => x.YoungPioneerJoinedPlace.ToUpper().Contains(YoungPioneerJoinedPlace.ToUpper()));
            #endregion

            #region E_YoungPioneerJoinedPlace
            if (!string.IsNullOrWhiteSpace(E_YoungPioneerJoinedPlace)) Query = Query.Where(x => x.YoungPioneerJoinedPlace.ToUpper() == E_YoungPioneerJoinedPlace.ToUpper());
            #endregion


            #region IsYouthLeageMember
            if (IsYouthLeageMember != null) Query = Query.Where(x => x.IsYouthLeageMember == IsYouthLeageMember);
            #endregion


            #region YouthLeagueJoinedDate
            if (YouthLeagueJoinedDate != null)
            {
                DateTime l_YouthLeagueJoinedDate = YouthLeagueJoinedDate.Value.Date;
                Query = Query.Where(x => x.YouthLeagueJoinedDate == l_YouthLeagueJoinedDate);
            }
            #endregion

            #region F_YouthLeagueJoinedDate
            if (F_YouthLeagueJoinedDate != null)
            {
                DateTime l_F_YouthLeagueJoinedDate = F_YouthLeagueJoinedDate.Value.Date;
                Query = Query.Where(x => x.YouthLeagueJoinedDate >= l_F_YouthLeagueJoinedDate);
            }
            #endregion

            #region T_YouthLeagueJoinedDate
            if (T_YouthLeagueJoinedDate != null)
            {
                DateTime l_T_YouthLeagueJoinedDate = T_YouthLeagueJoinedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.YouthLeagueJoinedDate < l_T_YouthLeagueJoinedDate);
            }
            #endregion


            #region YouthLeagueJoinedPlace
            if (!string.IsNullOrWhiteSpace(YouthLeagueJoinedPlace)) Query = Query.Where(x => x.YouthLeagueJoinedPlace.ToUpper().Contains(YouthLeagueJoinedPlace.ToUpper()));
            #endregion

            #region E_YouthLeagueJoinedPlace
            if (!string.IsNullOrWhiteSpace(E_YouthLeagueJoinedPlace)) Query = Query.Where(x => x.YouthLeagueJoinedPlace.ToUpper() == E_YouthLeagueJoinedPlace.ToUpper());
            #endregion


            #region IsCommunistPartyMember
            if (IsCommunistPartyMember != null) Query = Query.Where(x => x.IsCommunistPartyMember == IsCommunistPartyMember);
            #endregion


            #region CommunistPartyJoinedDate
            if (CommunistPartyJoinedDate != null)
            {
                DateTime l_CommunistPartyJoinedDate = CommunistPartyJoinedDate.Value.Date;
                Query = Query.Where(x => x.CommunistPartyJoinedDate == l_CommunistPartyJoinedDate);
            }
            #endregion

            #region F_CommunistPartyJoinedDate
            if (F_CommunistPartyJoinedDate != null)
            {
                DateTime l_F_CommunistPartyJoinedDate = F_CommunistPartyJoinedDate.Value.Date;
                Query = Query.Where(x => x.CommunistPartyJoinedDate >= l_F_CommunistPartyJoinedDate);
            }
            #endregion

            #region T_CommunistPartyJoinedDate
            if (T_CommunistPartyJoinedDate != null)
            {
                DateTime l_T_CommunistPartyJoinedDate = T_CommunistPartyJoinedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.CommunistPartyJoinedDate < l_T_CommunistPartyJoinedDate);
            }
            #endregion


            #region CommunistPartyJoinedPlace
            if (!string.IsNullOrWhiteSpace(CommunistPartyJoinedPlace)) Query = Query.Where(x => x.CommunistPartyJoinedPlace.ToUpper().Contains(CommunistPartyJoinedPlace.ToUpper()));
            #endregion

            #region E_CommunistPartyJoinedPlace
            if (!string.IsNullOrWhiteSpace(E_CommunistPartyJoinedPlace)) Query = Query.Where(x => x.CommunistPartyJoinedPlace.ToUpper() == E_CommunistPartyJoinedPlace.ToUpper());
            #endregion


            #region FatherFullName
            if (!string.IsNullOrWhiteSpace(FatherFullName)) Query = Query.Where(x => x.FatherFullName.ToUpper().Contains(FatherFullName.ToUpper()));
            #endregion

            #region E_FatherFullName
            if (!string.IsNullOrWhiteSpace(E_FatherFullName)) Query = Query.Where(x => x.FatherFullName.ToUpper() == E_FatherFullName.ToUpper());
            #endregion


            #region FatherBirthDate
            if (FatherBirthDate != null)
            {
                DateTime l_FatherBirthDate = FatherBirthDate.Value.Date;
                Query = Query.Where(x => x.FatherBirthDate == l_FatherBirthDate);
            }
            #endregion

            #region F_FatherBirthDate
            if (F_FatherBirthDate != null)
            {
                DateTime l_F_FatherBirthDate = F_FatherBirthDate.Value.Date;
                Query = Query.Where(x => x.FatherBirthDate >= l_F_FatherBirthDate);
            }
            #endregion

            #region T_FatherBirthDate
            if (T_FatherBirthDate != null)
            {
                DateTime l_T_FatherBirthDate = T_FatherBirthDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.FatherBirthDate < l_T_FatherBirthDate);
            }
            #endregion


            #region FatherJob
            if (!string.IsNullOrWhiteSpace(FatherJob)) Query = Query.Where(x => x.FatherJob.ToUpper().Contains(FatherJob.ToUpper()));
            #endregion

            #region E_FatherJob
            if (!string.IsNullOrWhiteSpace(E_FatherJob)) Query = Query.Where(x => x.FatherJob.ToUpper() == E_FatherJob.ToUpper());
            #endregion


            #region FatherMobile
            if (!string.IsNullOrWhiteSpace(FatherMobile)) Query = Query.Where(x => x.FatherMobile.ToUpper().Contains(FatherMobile.ToUpper()));
            #endregion

            #region E_FatherMobile
            if (!string.IsNullOrWhiteSpace(E_FatherMobile)) Query = Query.Where(x => x.FatherMobile.ToUpper() == E_FatherMobile.ToUpper());
            #endregion


            #region MotherFullName
            if (!string.IsNullOrWhiteSpace(MotherFullName)) Query = Query.Where(x => x.MotherFullName.ToUpper().Contains(MotherFullName.ToUpper()));
            #endregion

            #region E_MotherFullName
            if (!string.IsNullOrWhiteSpace(E_MotherFullName)) Query = Query.Where(x => x.MotherFullName.ToUpper() == E_MotherFullName.ToUpper());
            #endregion


            #region MotherBirthDate
            if (MotherBirthDate != null)
            {
                DateTime l_MotherBirthDate = MotherBirthDate.Value.Date;
                Query = Query.Where(x => x.MotherBirthDate == l_MotherBirthDate);
            }
            #endregion

            #region F_MotherBirthDate
            if (F_MotherBirthDate != null)
            {
                DateTime l_F_MotherBirthDate = F_MotherBirthDate.Value.Date;
                Query = Query.Where(x => x.MotherBirthDate >= l_F_MotherBirthDate);
            }
            #endregion

            #region T_MotherBirthDate
            if (T_MotherBirthDate != null)
            {
                DateTime l_T_MotherBirthDate = T_MotherBirthDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.MotherBirthDate < l_T_MotherBirthDate);
            }
            #endregion


            #region MotherJob
            if (!string.IsNullOrWhiteSpace(MotherJob)) Query = Query.Where(x => x.MotherJob.ToUpper().Contains(MotherJob.ToUpper()));
            #endregion

            #region E_MotherJob
            if (!string.IsNullOrWhiteSpace(E_MotherJob)) Query = Query.Where(x => x.MotherJob.ToUpper() == E_MotherJob.ToUpper());
            #endregion


            #region MotherMobile
            if (!string.IsNullOrWhiteSpace(MotherMobile)) Query = Query.Where(x => x.MotherMobile.ToUpper().Contains(MotherMobile.ToUpper()));
            #endregion

            #region E_MotherMobile
            if (!string.IsNullOrWhiteSpace(E_MotherMobile)) Query = Query.Where(x => x.MotherMobile.ToUpper() == E_MotherMobile.ToUpper());
            #endregion


            #region SponsorFullName
            if (!string.IsNullOrWhiteSpace(SponsorFullName)) Query = Query.Where(x => x.SponsorFullName.ToUpper().Contains(SponsorFullName.ToUpper()));
            #endregion

            #region E_SponsorFullName
            if (!string.IsNullOrWhiteSpace(E_SponsorFullName)) Query = Query.Where(x => x.SponsorFullName.ToUpper() == E_SponsorFullName.ToUpper());
            #endregion


            #region SponsorBirthDate
            if (SponsorBirthDate != null)
            {
                DateTime l_SponsorBirthDate = SponsorBirthDate.Value.Date;
                Query = Query.Where(x => x.SponsorBirthDate == l_SponsorBirthDate);
            }
            #endregion

            #region F_SponsorBirthDate
            if (F_SponsorBirthDate != null)
            {
                DateTime l_F_SponsorBirthDate = F_SponsorBirthDate.Value.Date;
                Query = Query.Where(x => x.SponsorBirthDate >= l_F_SponsorBirthDate);
            }
            #endregion

            #region T_SponsorBirthDate
            if (T_SponsorBirthDate != null)
            {
                DateTime l_T_SponsorBirthDate = T_SponsorBirthDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.SponsorBirthDate < l_T_SponsorBirthDate);
            }
            #endregion


            #region SponsorJob
            if (!string.IsNullOrWhiteSpace(SponsorJob)) Query = Query.Where(x => x.SponsorJob.ToUpper().Contains(SponsorJob.ToUpper()));
            #endregion

            #region E_SponsorJob
            if (!string.IsNullOrWhiteSpace(E_SponsorJob)) Query = Query.Where(x => x.SponsorJob.ToUpper() == E_SponsorJob.ToUpper());
            #endregion


            #region SponsorMobile
            if (!string.IsNullOrWhiteSpace(SponsorMobile)) Query = Query.Where(x => x.SponsorMobile.ToUpper().Contains(SponsorMobile.ToUpper()));
            #endregion

            #region E_SponsorMobile
            if (!string.IsNullOrWhiteSpace(E_SponsorMobile)) Query = Query.Where(x => x.SponsorMobile.ToUpper() == E_SponsorMobile.ToUpper());
            #endregion


            #region ProfileStatus
            if (ProfileStatus != null)
            {
                if (ProfileStatus is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ProfileStatus).Any(y => y == x.ProfileStatus));
                }
                else
                {
                    List<int?> l_ProfileStatus = ProfileStatus.ConvertToListInt();
                    if (l_ProfileStatus.Count == 1)
                    {
                        if (l_ProfileStatus[0] != -1)
                        {
                            int? l2_ProfileStatus = l_ProfileStatus[0];
                            Query = Query.Where(x => x.ProfileStatus == l2_ProfileStatus);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ProfileStatus.Contains(x.ProfileStatus));
                    }
                }
            }
            #endregion


            #region CreatedDate
            if (CreatedDate != null)
            {
                DateTime l_CreatedDate = CreatedDate.Value.Date;
                Query = Query.Where(x => x.CreatedDate == l_CreatedDate);
            }
            #endregion

            #region F_CreatedDate
            if (F_CreatedDate != null)
            {
                DateTime l_F_CreatedDate = F_CreatedDate.Value.Date;
                Query = Query.Where(x => x.CreatedDate >= l_F_CreatedDate);
            }
            #endregion

            #region T_CreatedDate
            if (T_CreatedDate != null)
            {
                DateTime l_T_CreatedDate = T_CreatedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.CreatedDate < l_T_CreatedDate);
            }
            #endregion


            #region IsActive
            if (IsActive != null) Query = Query.Where(x => x.IsActive == IsActive);
            #endregion


            #region ModifiedDate
            if (ModifiedDate != null)
            {
                DateTime l_ModifiedDate = ModifiedDate.Value.Date;
                Query = Query.Where(x => x.ModifiedDate == l_ModifiedDate);
            }
            #endregion

            #region F_ModifiedDate
            if (F_ModifiedDate != null)
            {
                DateTime l_F_ModifiedDate = F_ModifiedDate.Value.Date;
                Query = Query.Where(x => x.ModifiedDate >= l_F_ModifiedDate);
            }
            #endregion

            #region T_ModifiedDate
            if (T_ModifiedDate != null)
            {
                DateTime l_T_ModifiedDate = T_ModifiedDate.Value.Date.AddDays(1);
                Query = Query.Where(x => x.ModifiedDate < l_T_ModifiedDate);
            }
            #endregion


            #region FatherEmail
            if (!string.IsNullOrWhiteSpace(FatherEmail)) Query = Query.Where(x => x.FatherEmail.ToUpper().Contains(FatherEmail.ToUpper()));
            #endregion

            #region E_FatherEmail
            if (!string.IsNullOrWhiteSpace(E_FatherEmail)) Query = Query.Where(x => x.FatherEmail.ToUpper() == E_FatherEmail.ToUpper());
            #endregion


            #region MotherEmail
            if (!string.IsNullOrWhiteSpace(MotherEmail)) Query = Query.Where(x => x.MotherEmail.ToUpper().Contains(MotherEmail.ToUpper()));
            #endregion

            #region E_MotherEmail
            if (!string.IsNullOrWhiteSpace(E_MotherEmail)) Query = Query.Where(x => x.MotherEmail.ToUpper() == E_MotherEmail.ToUpper());
            #endregion


            #region SponsorEmail
            if (!string.IsNullOrWhiteSpace(SponsorEmail)) Query = Query.Where(x => x.SponsorEmail.ToUpper().Contains(SponsorEmail.ToUpper()));
            #endregion

            #region E_SponsorEmail
            if (!string.IsNullOrWhiteSpace(E_SponsorEmail)) Query = Query.Where(x => x.SponsorEmail.ToUpper() == E_SponsorEmail.ToUpper());
            #endregion


            #region ChildOrder
            if (ChildOrder != null)
            {
                if (ChildOrder is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ChildOrder).Any(y => y == x.ChildOrder));
                }
                else
                {
                    List<int?> l_ChildOrder = ChildOrder.ConvertToListInt();
                    if (l_ChildOrder.Count == 1)
                    {
                        if (l_ChildOrder[0] != -1)
                        {
                            int? l2_ChildOrder = l_ChildOrder[0];
                            Query = Query.Where(x => x.ChildOrder == l2_ChildOrder);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ChildOrder.Contains(x.ChildOrder));
                    }
                }
            }
            #endregion


            #region NumberOfChild
            if (NumberOfChild != null)
            {
                if (NumberOfChild is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)NumberOfChild).Any(y => y == x.NumberOfChild));
                }
                else
                {
                    List<int?> l_NumberOfChild = NumberOfChild.ConvertToListInt();
                    if (l_NumberOfChild.Count == 1)
                    {
                        if (l_NumberOfChild[0] != -1)
                        {
                            int? l2_NumberOfChild = l_NumberOfChild[0];
                            Query = Query.Where(x => x.NumberOfChild == l2_NumberOfChild);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_NumberOfChild.Contains(x.NumberOfChild));
                    }
                }
            }
            #endregion


            #region EatingHabit
            if (EatingHabit != null)
            {
                if (EatingHabit is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EatingHabit).Any(y => y == x.EatingHabit));
                }
                else
                {
                    List<int?> l_EatingHabit = EatingHabit.ConvertToListInt();
                    if (l_EatingHabit.Count == 1)
                    {
                        if (l_EatingHabit[0] != -1)
                        {
                            int? l2_EatingHabit = l_EatingHabit[0];
                            Query = Query.Where(x => x.EatingHabit == l2_EatingHabit);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EatingHabit.Contains(x.EatingHabit));
                    }
                }
            }
            #endregion


            #region ReactionTrainingHabit
            if (ReactionTrainingHabit != null)
            {
                if (ReactionTrainingHabit is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ReactionTrainingHabit).Any(y => y == x.ReactionTrainingHabit));
                }
                else
                {
                    List<int?> l_ReactionTrainingHabit = ReactionTrainingHabit.ConvertToListInt();
                    if (l_ReactionTrainingHabit.Count == 1)
                    {
                        if (l_ReactionTrainingHabit[0] != -1)
                        {
                            int? l2_ReactionTrainingHabit = l_ReactionTrainingHabit[0];
                            Query = Query.Where(x => x.ReactionTrainingHabit == l2_ReactionTrainingHabit);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ReactionTrainingHabit.Contains(x.ReactionTrainingHabit));
                    }
                }
            }
            #endregion


            #region AttitudeInStrangeScene
            if (AttitudeInStrangeScene != null)
            {
                if (AttitudeInStrangeScene is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)AttitudeInStrangeScene).Any(y => y == x.AttitudeInStrangeScene));
                }
                else
                {
                    List<int?> l_AttitudeInStrangeScene = AttitudeInStrangeScene.ConvertToListInt();
                    if (l_AttitudeInStrangeScene.Count == 1)
                    {
                        if (l_AttitudeInStrangeScene[0] != -1)
                        {
                            int? l2_AttitudeInStrangeScene = l_AttitudeInStrangeScene[0];
                            Query = Query.Where(x => x.AttitudeInStrangeScene == l2_AttitudeInStrangeScene);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_AttitudeInStrangeScene.Contains(x.AttitudeInStrangeScene));
                    }
                }
            }
            #endregion


            #region OtherHabit
            if (!string.IsNullOrWhiteSpace(OtherHabit)) Query = Query.Where(x => x.OtherHabit.ToUpper().Contains(OtherHabit.ToUpper()));
            #endregion

            #region E_OtherHabit
            if (!string.IsNullOrWhiteSpace(E_OtherHabit)) Query = Query.Where(x => x.OtherHabit.ToUpper() == E_OtherHabit.ToUpper());
            #endregion


            #region FavoriteToy
            if (!string.IsNullOrWhiteSpace(FavoriteToy)) Query = Query.Where(x => x.FavoriteToy.ToUpper().Contains(FavoriteToy.ToUpper()));
            #endregion

            #region E_FavoriteToy
            if (!string.IsNullOrWhiteSpace(E_FavoriteToy)) Query = Query.Where(x => x.FavoriteToy.ToUpper() == E_FavoriteToy.ToUpper());
            #endregion


            #region EvaluationConfigID
            if (EvaluationConfigID != null)
            {
                if (EvaluationConfigID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)EvaluationConfigID).Any(y => y == x.EvaluationConfigID));
                }
                else
                {
                    List<int?> l_EvaluationConfigID = EvaluationConfigID.ConvertToListInt();
                    if (l_EvaluationConfigID.Count == 1)
                    {
                        if (l_EvaluationConfigID[0] != -1)
                        {
                            int? l2_EvaluationConfigID = l_EvaluationConfigID[0];
                            Query = Query.Where(x => x.EvaluationConfigID == l2_EvaluationConfigID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_EvaluationConfigID.Contains(x.EvaluationConfigID));
                    }
                }
            }
            #endregion


            #region PolicyRegimeID
            if (PolicyRegimeID != null)
            {
                if (PolicyRegimeID is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)PolicyRegimeID).Any(y => y == x.PolicyRegimeID));
                }
                else
                {
                    List<int?> l_PolicyRegimeID = PolicyRegimeID.ConvertToListInt();
                    if (l_PolicyRegimeID.Count == 1)
                    {
                        if (l_PolicyRegimeID[0] != -1)
                        {
                            int? l2_PolicyRegimeID = l_PolicyRegimeID[0];
                            Query = Query.Where(x => x.PolicyRegimeID == l2_PolicyRegimeID);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_PolicyRegimeID.Contains(x.PolicyRegimeID));
                    }
                }
            }
            #endregion


            #region IsSupportForLearning
            if (IsSupportForLearning != null) Query = Query.Where(x => x.IsSupportForLearning == IsSupportForLearning);
            #endregion


            #region ClassType
            if (ClassType != null)
            {
                if (ClassType is IQueryable<int>)
                {
                    Query = Query.Where(x => ((IQueryable<int>)ClassType).Any(y => y == x.ClassType));
                }
                else
                {
                    List<int?> l_ClassType = ClassType.ConvertToListInt();
                    if (l_ClassType.Count == 1)
                    {
                        if (l_ClassType[0] != -1)
                        {
                            int? l2_ClassType = l_ClassType[0];
                            Query = Query.Where(x => x.ClassType == l2_ClassType);
                        }
                    }
                    else
                    {
                        Query = Query.Where(x => l_ClassType.Contains(x.ClassType));
                    }
                }
            }
            #endregion




            return Query;
        }
    }
}
