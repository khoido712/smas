﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IAssignSubjectConfigBusiness : IGenericBussiness<AssignSubjectConfig>
    {
        
    }
}
