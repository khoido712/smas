﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IGrowthEvaluationBusiness : IGenericBussiness<GrowthEvaluation>
    {
        IQueryable<GrowthEvaluation> Search(IDictionary<string, object> dic);

        GrowthEvaluation GetObjectGrowEvaluation(int SchoolID, int AcademicYearID, int ClassID, int PupilID);
        void InsertOrUpdateGrowthEvaluation(List<GrowthEvaluationBO> lstGrowthEvaluationBO, Dictionary<string, object> dic);
        void DeleteCommentPupil(IDictionary<string, object> dic);
    }
}
