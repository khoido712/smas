/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPupilProfileBusiness : IGenericBussiness<PupilProfile>
    {

        List<PupilProfile> GetPupilWithPaging(int page, int size, int academicYearId, int schoolId, List<int> educationLevels, int? classId, string code, string fullname, List<int> notInPupilIds, List<int> forceInPupilIds, List<int> searchInPupilIds, out int total);
    }
}