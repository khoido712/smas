/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.Models.Models;
using System.Collections.Generic;

namespace SMAS.Business.IBusiness
{
    public partial interface IEmployeeEvaluationBusiness : IGenericBussiness<EmployeeEvaluation>
    {
        
    }
}