﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ISchoolCodeConfigBusiness : IGenericBussiness<SchoolCodeConfig>
    {
        SchoolCodeConfig SearchBySchool(int CodeConfigID, int SchoolID);
    }
}