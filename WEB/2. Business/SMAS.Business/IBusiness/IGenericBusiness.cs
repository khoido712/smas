﻿



using SMAS.Models.Models;
/**
* The Viettel License
*
* Copyright 2012 Viettel Telecom. All rights reserved.
* VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*//** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace SMAS.Business.IBusiness
{
    public interface IGenericBussiness<T> where T : class
    {

        /// <summary>
        /// Checkduplcate Database
        /// </summary>
        /// <param name="valueToCheck"></param>
        /// <param name="schemaName">Name of the schema.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="currentId">The current id.</param>
        /// <param name="ResKey">The res key.</param>
        /// <author>
        /// hath
        /// </author>
        /// <date>
        /// 9/6/2012
        /// </date>
        void CheckDuplicate(string valueToCheck, string schemaName, string tableName, string columnName, bool? isActive, int? currentId, string ResKey);

        /// <summary>
        /// CheckDuplicateCouple Database
        /// </summary>
        /// <param name="valueToCheck1">The value to check1</param>
        /// <param name="valueToCheck2">The value to check2.</param>
        /// <param name="schemaName">Name of the schema.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName1">The column name1.</param>
        /// <param name="columnName2">The column name2.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="currentId">The current id.</param>
        /// <param name="ResKey">The res key.</param>
        /// <author>
        /// hath
        /// </author>
        /// <date>
        /// 9/6/2012
        /// </date>
        void CheckDuplicateCouple(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive, int? currentId, string ResKey);

        /// <summary>
        /// CheckConstraints Database
        /// </summary>
        /// <param name="schemaName">Name of SchemaName</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="id">The id.</param>
        /// <param name="ResKey">The res key.</param>
        /// <author>
        /// hath
        /// </author>
        /// <date>
        /// 9/6/2012
        /// </date>
        void CheckConstraints(string schemaName, string tableName, int? id, string ResKey);

        void CheckConstraintsWithIsActive(string schemaName, string tableName, int? id, string ResKey);

        /// <summary>
        /// CheckAvailable DataBase
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="ResKey">The res key.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <author>
        /// hath
        /// </author>
        /// <date>
        /// 9/6/2012
        /// </date>
        void CheckAvailable(int? id, string ResKey, bool isActive = true);

        /// <summary>
        /// Gets all objects from database 
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Gets all objects from database with no tracking
        /// </summary>
        IQueryable<T> AllNoTracking { get; }

        /// <summary>
        /// Gets all objects from database		
        /// </summary>
        /// <param name="includeProperties">Specified the conditions.</param>
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// Find object by keys.
        /// </summary>
        /// <param name="id">Specified the search keys.</param>
        T Find(object id);

        /// <summary>
        /// Insert object to database.
        /// </summary>
        /// <param name="entity">Specified the object to save.</param>
        T Insert(T entityToInsert);

        /// <summary>
        /// Update object changes and save to database.
        /// </summary>
        /// <param name="entity">Specified the object to save.</param>
        T Update(T entityToUpdate);

        T BaseUpdate(T entityToUpdate);

        /// <summary>
        /// Cap nhat ko thay doi ngay ModifiedDate
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <returns></returns>
        T UpdateNotModifiedDate(T entityToUpdate);
        /// <summary>
        /// Delete the object from database.
        /// </summary>
        /// <param name="id">Specified a existing object to delete.</param>
        void Delete(object id);

        /// <summary>
        /// Delete list object
        /// </summary>
        /// <param name="entities"></param>
        void DeleteAll(List<T> entities);

        ///<summary>
        ///Save
        ///</summary>

        void Save();

        void SaveDetect();
        ///<summary>
        ///Rollback
        ///</summary>

        void Rollback();


        /// <summary>
        /// Dispose object.
        /// </summary>
        /// <param name="t">Dispose object.</param>
        void Dispose();

        /// <summary>
        /// QuangLM
        /// Insert or Update Batch
        /// </summary>
        /// <param name="listObj"></param>
        /// <param name="batchSize"></param>
        //void BulkInsert(List<T> listObj, int batchSize = 1000);

        /// <summary>
        /// QuangLM
        /// Xoa du lieu diem - Chi danh cho MarkRecord va JudgeRecord
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="strMarkTitle"></param>
        /// <param name="listPupilID"></param>
        void DeleteMarkRecoreByCondition(int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int Semester, List<int> listPupilID,
             bool? IsMarkedDate = null, int? PeriodID = null, string strMarkTitle = null);

        /// <summary>
        /// Lay gia tri tiep theo cua SEQ. SeqName duoc dat theo cau truc: TABLE_SEQ
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        TResult GetNextSeq<TResult>();

        /// <summary>
        /// Lay gia ti tiep theo cua SEQ theo SeqName
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="seqName"></param>
        /// <returns></returns>
        TResult GetNextSeq<TResult>(string seqName);

        /// <summary>
        /// Chiendd1: Thuc hien insert du lieu tu danh sach, su dung ArrayBindCount
        /// </summary>
        /// <param name="lstInsert"></param>
        /// <returns></returns>
        int BulkInsert(List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn);

        /// <summary>
        /// Thuc hien insert va xoa du lieu tu store
        /// </summary>
        /// <param name="lstInsert"></param>
        /// <param name="columnMappings"></param>
        /// <param name="sqlProcedure"></param>
        /// <param name="lstParaDel"></param>
        /// <param name="lstData"></param>
        /// <param name="remvedcolumn"></param>
        /// <returns></returns>
        int BulkInsertAndDelete(List<T> lstInsert, IDictionary<string, object> columnMappings, string sqlProcedure, List<CustomType> lstParaDelete,  params string[] remvedcolumn);


        /// <summary>
        /// Chiendd1: Thuc hien insert du lieu tu danh sach, su dung ArrayBindCount 
        /// </summary>
        /// <param name="stringConnection"></param>
        /// <param name="lstInsert"></param>
        /// <param name="columnMappings"></param>
        /// <param name="remvedcolumn"></param>
        /// <returns></returns>
        int BulkInsert(string stringConnection, List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn);



        /// <summary>
        /// Cap nhat gia tri khi doi tuong da ton tai trong ObjectStateManager
        /// </summary>
        /// <param name="entityToUpdate"></param>
        void UpdateExistsObjectStateManager(T entityToUpdate);


        /// <summary>
        /// set isAutoDetectChangesEnabled=false de tang hieu nang khi thuc hien them moi, xoa,sua  
        /// </summary>
        /// <param name="isAutoDetectChangesEnabled"></param>
        void SetAutoDetectChangesEnabled(bool isAutoDetectChangesEnabled);

        bool ExcuteSql(List<string> lstSql);
    }
}



