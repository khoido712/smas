﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.Collections.Generic;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExamSupervisoryBusiness : IGenericBussiness<DOETExamSupervisory>
    {
        
    }
}
