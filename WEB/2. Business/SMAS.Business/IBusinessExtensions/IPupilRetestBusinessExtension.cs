﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;


namespace SMAS.Business.IBusiness
{
    public partial interface IPupilRetestBusiness
    {
        string GetHashKey(PupilRetestBO entity);
        ProcessedReport InsertPupilRetest(PupilRetestBO entity, Stream data);
        ProcessedReport GetPupilRetest(PupilRetestBO entity);
        Stream CreatePupilRetest(PupilRetestBO entity);
    }
}
