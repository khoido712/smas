/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeeBusiness 
    {
        //IQueryable<Employee> Search(IDictionary<string, object> dic);
        void DeleteTeacher(int EmployeeId, int SchoolID);
        Employee InsertTeacher(Employee insert, bool forImport = false);
        Employee UpdateTeacher(Employee update, bool forImport = false);

        void Validate(Employee insertEmployee, bool forImport = false);
        List<BusinessException> ValidateForImport(Employee insertEmployee);
        Employee InsertEmployee(Employee insert);
        Employee UpdateEmployee(Employee update, bool forUpdate = false);
        void DeleteEmployee(int SupervisingDeptID, int id);
        IQueryable<Employee> Search(IDictionary<string, object> dic);
        IQueryable<Employee> SearchTeacher(int schoolID, IDictionary<string, object> dic);
        IQueryable<Employee> SearchEmployee(int SupervisingDeptID, IDictionary<string, object> dic);
        IQueryable<Employee> SearchByArea(IDictionary<string, object> dic);

        IQueryable<Employee> SearchWorkingTeacherByFaculty(int SchoolID, int FalcultyID = 0);
        IQueryable<Employee> SearchTeacherBySupervisingDept(Dictionary<string, object> dic);
        string GetUserNameForEmployee(int EmployeeID);
        List<bool> IsUsedTeacher(List<int> lstEmployeeId);
        void ImportTeacher(List<Employee> lstInsertEmployee);
        void ImportUpdateTeacher(List<Employee> lstUpdateEmployee);
        IQueryable<EmployeeBO> GetActiveEmployee(int schoolID);
        List<TeacherBO> GetTeachersOfSchool(int schoolID, int academicYearID, bool? IsStatus = null);
        List<TeacherBO> GetListTeacher(List<int> lstEmplyeeID, int StatusID = 0);
        List<TeacherBO> GetListTeacherOfSchoolForContactGroup(int schoolID, int academicYearID, bool? IsStatus = null);
        List<HistoryReceiverBO> GetListEmployeeNameByListEmployeeID(List<int> EmployeeIDList);
        List<TeacherBO> GetHeadTeacherByClassIDs(List<int> LstClassID, int AcademicYearID, int SchoolID);
        int GetHeadTeacherID(int ClassID, int AcademicYearID, int SchoolID);
        List<TeacherBO> GetListTeacherWithTeachingSubject(int SchoolID, int AcademicYearID, int AppliedLevel, int EducationLevel, int ClassID, int SubjectID, int Semester, bool IsRolePrincipal = false, int EmployeeID = 0);
        List<UserBO> GetListFullUsernameByEmployeeID(int SchoolID, List<int> lstEmployeeID);
        List<EmployeeBO> GetListTeacherByContractGroup(IDictionary<string, object> dic);
        List<TeacherBO> GetListTeacherOfSchoolForReport(int schoolId, int academicYearId);
        IQueryable<EmployeeBO> SearchTeacherBySupervisingDeptCustom(Dictionary<string, object> dic);
        DataTable SP_TeacherInfoCustom(Dictionary<string, object> dic);
    }
}
