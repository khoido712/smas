/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassForwardingBusiness 
    {
        IQueryable<ClassForwarding> SearchByAcademicYear(int AcademicYearID,IDictionary<string, object> dic);
        IQueryable<ClassForwarding> Search(IDictionary<string, object> dic);
        void Delete(int ClassForwardingId, int AcademicYearID);
        void TransferData(List<int> lstClass, List<int> lstUpClass, List<int> lstDownClass, int OldAcademicYearID,
            int CurrentAcademicYearID, int EducationLevelID, int SchoolID, int UserID);
        List<ClassForwardingBO> GetTransferData(int EducationLevelID, int SchoolID, int AcademicYearID);
        void CancelTransferData(List<int> lstOldClass, int AcademicYearID, int SchoolID, int EducationLevelID);
        void TransferData(Dictionary<int, int> dicClassFw, bool TransferAllPupilInSelectedClass, int AcademicYearID, int SchoolID, int EducationLevelID);
        List<PupilForwardBO> GetPupilForward(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, bool TransferAllPupilInSelectedClass = false);
        void ForwardPupil(List<PupilForwardBO> lstPupilForwardBO, int AcademicYearID, int SchoolID, int EducationLevelID);
        void CancelForwardPupil(List<PupilForwardBO> lstPupilForwardBO, int AcademicYearID, int SchoolID, int EducationLevelID);
    }
}