/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamPupilAbsenceBusiness 
    {
        IQueryable<ExamPupilAbsence> Search(IDictionary<string, object> search);
        List<ExamPupilAbsenceBO> GetListExamPupil(IDictionary<string, object> search);
        List<ExamPupilAbsenceBO> GetListExamPupilAbsence(IDictionary<string, object> search);
        void InsertListExamPupilAbsence(IDictionary<string, object> insertInfo, long[] ExamPupilID,
        long[] CheckExamPupilID, string[] ContentReasonAbsence, long[] ReasonAbsence);
        void UpdateExamPupilAbsence(long examPupilViolateID, string contentReasonAbsence, bool? reasonAbsence);
        void DeleteExamPupilAbsence(List<long> listExamPupilAbsenceID);
        IQueryable<ExamPupilAbsenceBO> SearchForReport(IDictionary<string, object> dic);

        ProcessedReport GetExamPupilAbsenceReport(IDictionary<string, object> dic);
        Stream CreateExamPupilAbsenceReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamPupilAbsenceReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        /// <summary>
        /// Lay hoc sinh vang thi boi khoa chinh
        /// </summary>
        /// <param name="examPupilAbsenceID"></param>
        /// <returns></returns>
        ExamPupilAbsenceBO GetExamPupilByID(long examPupilAbsenceID, int schoolID, int academicYearID);
    }
}