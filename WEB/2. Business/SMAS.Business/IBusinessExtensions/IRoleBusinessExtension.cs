/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IRoleBusiness 
    {
       
        bool IsSuperRole(int RoleID);
        bool IsProvinceRole(int RoleID);
        bool IsSuperVisingDeptRole(int RoleID);
        bool IsSubSuperVisingDeptRole(int RoleID);
        bool IsSchoolRole(int RoleID);
        bool IsSchoolRole_Primary(int RoleID);
        bool IsSchoolRole_SecondarySchool(int RoleID);
        bool IsSchoolRole_HighSchool(int RoleID);
        bool IsSchoolRole_PreSchool(int RoleID);
        bool IsSchoolRole_ChildCare(int RoleID);
        bool CheckPriviledgeToCreateRole(int RoleID);
        Role GetPreRole(int RoleID);
        IQueryable<Role> GetAllSuperRoles(int RoleID);
        IQueryable<Role> GetNextSubRoles(int RoleID);
        IQueryable<Role> GetAllSubRoles(int RoleID);
        IQueryable<Role> GetAll();

        //?
        bool CheckPrivilegeAssignMenuForRole(int RoleID);
        IQueryable<Role> Search(Dictionary<string, object> dic);
        void Delete(int id);
       // int GetAppliedLevel(int RoleID);
        void SetValuesToCreate(Role role, string roleParentID);
    }
}