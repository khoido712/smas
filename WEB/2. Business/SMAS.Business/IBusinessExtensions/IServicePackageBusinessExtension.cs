/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IServicePackageBusiness 
    {
        IQueryable<SMS_SERVICE_PACKAGE> Search(Dictionary<string, object> dic);
        List<ServicePackageBO> GetAllPackage();
        ServicePackageBO SearchDetailPackage(string ServiceCode, int year);
        List<ServicePackageBO> GetGridServicePackage(int? AcademicYearID, string ServiceCode, int? ApplyType);
        bool InsertServicePackageDetail(ServicePackageBO servicePackageUpdate);
        bool InsertServicePackage(ServicePackageBO servicePacObj);
        bool UpdateServicePackage(ServicePackageBO updatePackage);
        bool UpdateServicePackageDetail(ServicePackageBO servicePackageUpdate);
        ServicePackageBO SearchDetail(int ServicePackageID, int? ServicePackageDeclareDetailID);
        bool DeactiveServicePackage(int ServicePackageID, int? ServicePackageDeclareDetailID);
        int GetServicePackageIDByServiceCode(string serviceCode);
        List<ServicePackageBO> GetListServicesPackage(int year, int semester, AcademicYear academicYearBO, int provinceID, int schoolID, bool? isExtraPackage = null);
        List<ServicePackageBO> GetListServicesPackageFull(int year);
        List<ServicePackageBO> GetListExtraServicesPackage(int year, int semester, AcademicYear academicYearBO, int provinceID, int schoolID);
        MoneyMaxSMSBO GetSMSByServicesPackageID(int servicePackageID, int year, int semester, AcademicYear academicYearBO);
        List<ServicePackageBO> GetListServicesPackageEdit(int year, int semester, AcademicYear academicYearBO, int servicePackageIDEdit, int provinceID, int schoolID = 0);
        MoneyMaxSMSBO GetSMSByServicesPackageIDEdit(int servicePackageID, AcademicYear academicYearBO);
        bool SchooolHasService(int schoolId, int provinceId, string serviceCode);
    }
}