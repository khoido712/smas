﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.IBusiness
{
    public partial interface IJudgeRecordHistoryBusiness
    {
        bool InsertJudgeRecordHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, int SubjectID,int? EmployeeID);
        void InsertJudgeRecordPrimaryHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory);
        void DeleteJudgeRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        IQueryable<JudgeRecordHistory> SearchJudgeRecordHistory(IDictionary<string, object> dic);
        void ImportJudgeRecordHistory(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory);
        void InsertJudgeRecordHistoryByListSubject(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, List<int> listSubjectID,int? EmployeeID);
        void InsertJudgeRecordHistoryByListClass(int UserID, List<JudgeRecordHistory> lstJudgeRecordHistory, List<SummedUpRecordHistory> lstSummedUpRecordHistory, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, List<int> listClassID, int SubjectID,int? EmployeeID);
    }
}
