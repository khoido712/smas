/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IServicePackageSchoolDetailBusiness 
    {
        int CountSchoolApply(int ServicePackageID, int type);
        void Apply(Dictionary<string, object> dic);
        void Apply(int ServicePackageID, List<int> lstSchoolIDAdd, List<int> lstSchoolIDRemove);
        IQueryable<SMS_SERVICE_PACKAGE_UNIT> Search(Dictionary<string, object> dic);
    }
}