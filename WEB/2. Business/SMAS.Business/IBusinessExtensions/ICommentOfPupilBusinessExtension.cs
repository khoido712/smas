﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface ICommentOfPupilBusiness
    {
        IQueryable<CommentOfPupil> Search(IDictionary<string, object> dic);
        void InsertOrUpdateComment(List<CommentOfPupilBO> lstCommentOfPupilBO,IDictionary<string,object> dic);
        void DeleteComment(IDictionary<string, object> dic);
        List<CommentOfPupilBO> GetCommentOfPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int SemesterID, int DayOfMonth, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject);

    }
}
