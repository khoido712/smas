/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeePraiseDisciplineBusiness 
    {
        EmployeePraiseDiscipline InsertPraise(EmployeePraiseDiscipline EmployeePraiseDiscipline);
        EmployeePraiseDiscipline InsertDiscipline(EmployeePraiseDiscipline EmployeePraiseDiscipline);

        EmployeePraiseDiscipline UpdatePraise(EmployeePraiseDiscipline EmployeePraiseDiscipline);
        EmployeePraiseDiscipline UpdateDiscipline(EmployeePraiseDiscipline EmployeePraiseDiscipline);
        void DeletePraise(int Id, int SchoolID, int AcademicYearID);
        void DeleteDiscipline(int Id, int SchoolID, int AcademicYearID);

        IQueryable<EmployeePraiseDiscipline> Search(IDictionary<string, object> dic);
        IQueryable<EmployeePraiseDiscipline> SearchPraise(IDictionary<string, object> dic,int SchoolID);
        IQueryable<EmployeePraiseDiscipline> SearchDiscipline(IDictionary<string, object> dic,int SchoolID);
    }
}