/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.IBusiness
{ 
    public partial interface IUserGroupBusiness 
    {
        bool CheckPrivilegeToAssignGroup(int LogonUserID, List<int> ListGroupID, int RoleID, int UserAccountID);
        void AssignGroupsForUser(List<GroupCheck> lsGroupCheck, int userAccountID);
        bool AssignGroupsForUser(int LogonUserID, List<int> ListGroupID, int RoleID, int UserAccountID);
        IQueryable<UserAccount> GetUsersInGroup(int GroupID);
        IQueryable<GroupCat> GetGroupsOfUser(int UserAccountID);
        IQueryable<UserGroup> GetUserGroupsByUser(int UserAccountID,int RoleID);
    }
}