/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSTypeBusiness 
    {
        List<SMSTypeBO> GetSMSTypeBySchool(byte yTypeCommunication, int? iSchoolID, int? iLevel, bool bIsLock = false, bool bGetAll = false);
        bool ValidateSMSType(int schoolID);
        bool CheckTimeMark(int schoolYearID, int semester, int year);
        bool CheckSMSTypeIsLock(string cTypeCode, int yTypeCommunication, int? iSchoolID);
        List<SMSTypeBO> GetCustomSMSTypeBySchool(int schoolId, int grade, bool? isLocked = null);
    }
}