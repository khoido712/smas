/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISchoolConfigBusiness 
    {
        void InsertSchoolConfig(SMS_SCHOOL_CONFIG schoolconfigObj);
        void UpdateSchoolConfig(SMS_SCHOOL_CONFIG schoolconfigObj);
        bool CheckExitSchoolIDInSchoolConfig(int? schoolID);
        SMS_SCHOOL_CONFIG GetSchoolConfigBySchoolID(int? schoolID);
    }
}