/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Web.Mvc;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IActionAuditBusiness 
    {
        void AuditAction(ControllerContext ControllerContext, DateTime BeginAuditTime, DateTime EndAuditTime, int objectId, string OldObjectJSonValue, string NewObjectJSonValue, string Description);
        List<ActionHistoryObject> ListActionBySchool(int SchoolID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action, int PageNum, int PageSize, ref int Total);
        List<ActionHistoryObject> ListActionBySuperVisingDeptID(int SuperVisingDeptID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action, int PageNum, int PageSize, ref int Total);
        List<ActionHistoryObject> ListActionByAdmin(int AdminID, DateTime FromDate, DateTime ToDate, string Action, int PageNum, int PageSize, ref int Total);
        int GetAdminID(int SchoolID);
        int GetAdminIDBySuperVisingDept(int SupervisingDeptID);
        List<ActionHistoryObject> ListActionBySchoolExport(int SchoolID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action);
        List<ActionHistoryObject> ListActionByAdminExport(int AdminID, DateTime FromDate, DateTime ToDate, string Action);
        List<ActionHistoryObject> ListActionBySuperVisingDeptIDExport(int SuperVisingDeptID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action);
        int EducationGrade(int schoolID);
    }
}