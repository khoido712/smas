/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeeQualificationBusiness 
    {
        EmployeeQualification Insert(EmployeeQualification insertEmployeeQualification);
        EmployeeQualification Update(EmployeeQualification updateEmployeeQualification);
        void Delete(int SchoolOrSupervisingDeptID, int EmployeeQualificationID);
        //IQueryable<EmployeeQualification> Search(IDictionary<string, object> dic);
        IQueryable<EmployeeQualification> SearchBySchool(int SchoolID,IDictionary<string, object> dic=null);
        IQueryable<EmployeeQualification> SearchBySupervisingDept(int SupervisingDeptID,IDictionary<string, object> dic=null);
        void Validate(EmployeeQualification insertEmployeeQualification);
    }
}