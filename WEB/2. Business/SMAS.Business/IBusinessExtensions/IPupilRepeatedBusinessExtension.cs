/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    /// <summary>
    /// </summary>
    /// <author>
    /// dungnt77
    /// </author>
    /// <remarks>
    /// 07/01/2013   10:08 AM
    /// </remarks>
    public partial interface IPupilRepeatedBusiness
    {
        void Insert(PupilRepeated entity);
        void Delete(int SchoolID, int PupilRepeatedID);
        IQueryable<PupilRepeated> Search(IDictionary<string, object> dic);
        IQueryable<PupilRepeated> SearchBySchool(int SchoolID,IDictionary<string, object> dic =null);
        List<PupilRepeatedBO> GetPupilRepeated(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID);
        void RepeatedPupil(List<PupilRepeatedBO> lstPupilRepeatedBO, int AcademicYearID, int SchoolID, int EducationLevelID);
        void CancelRepeatedPupil(List<PupilRepeatedBO> lstPupilRepeatedBO, int AcademicYearID, int SchoolID, int EducationLevelID);
    }
}