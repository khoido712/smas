/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeeWorkingHistoryBusiness 
    {
        EmployeeWorkingHistory Insert(EmployeeWorkingHistory insertEmployeeWorkingHistory);
        EmployeeWorkingHistory Update(EmployeeWorkingHistory updateEmployeeWorkingHistory);
        void Delete(int SchoolOrSupervisingDeptID, int EmployeeWorkingHistoryID);
        //IQueryable<EmployeeWorkingHistory> Search(IDictionary<string, object> dic);
        IQueryable<EmployeeWorkingHistory> SearchBySchool(int SchoolID,  IDictionary<string, object> dic = null);
        IQueryable<EmployeeWorkingHistory> SearchBySupervisingDept(int SupervisingDeptID, IDictionary<string, object> dic = null);
        void Validate(EmployeeWorkingHistory insertEmployeeWorkingHistory);
        IQueryable<EmployeeBO> SearchTeacher(int schoolID, IDictionary<string, object> dic = null);
    }
}