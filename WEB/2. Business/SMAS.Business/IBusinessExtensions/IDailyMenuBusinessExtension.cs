/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDailyMenuBusiness 
    {
        IQueryable<DailyMenu> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);       
        void Insert(DailyMenu entity, List<DailyMenuDetail> lstDailyMenuDetail, int? SchoolID=null, int? AcademicYearID=null);
        void Update(DailyMenu entity, List<DailyMenuDetail> lstDailyMenuDetail, int? SchoolID =null, int? AcademicYearID=null);
        void Delete(int DailyMenuID, int SchoolID);
        bool CheckExistDailyMenuCode(string dailyMenuCode, int SchoolID, int AcademicYearID);
        Stream CreateDailyMenuReport(int SchoolID, int AcademicYearID, int DailyMenuID);
    }
}