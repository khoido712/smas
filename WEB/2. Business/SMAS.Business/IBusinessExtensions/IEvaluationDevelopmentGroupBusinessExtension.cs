/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEvaluationDevelopmentGroupBusiness 
    {
        IQueryable<EvaluationDevelopmentGroup> Search(IDictionary<string, object> dic);
        IQueryable<EvaluationDevelopmentGroup> SearchEval(IDictionary<string, object> dic);
        void InsertEval(List<EvaluationDevelopmentGroup> lstInput);
        void UpdateVal(EvaluationDevelopmentGroup objInpt);
        void UpdateOrderID(List<EvaluationDevelopmentGroup> lstInput, List<EvaluationDevelopmentGroup> lstDB);
        IQueryable<EvaluationDevelopmentGroup> GetListByAppliedLevelAndProvince(int AppliedLevel, int ProvinceID);
        void UpdateLock(int ProvinceID, bool IsLocked);
    }
}