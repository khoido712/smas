﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IConfigConductRankingBusiness 
    {
        IQueryable<ConfigConductRanking> GetAllConfigConductRanking(IDictionary<string, object> dic);
        void Insert(IDictionary<string, object> dic, List<ConfigConductRanking> lstInsert);
        void Delete(IDictionary<string, object> dic, List<ConfigConductRanking> lstDelete);        
    }
}