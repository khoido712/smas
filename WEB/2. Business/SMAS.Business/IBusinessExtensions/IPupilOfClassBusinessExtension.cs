/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.SearchForm;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilOfClassBusiness
    {
        ProcessedReport InsertProcessChildrenContact(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport GetProcessReportChildrenContact(IDictionary<string, object> dicReport);
        ProcessedReport InsertProcessReportMeetingInvitation(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport GetProcessReportMeetingInvitation(IDictionary<string, object> dicReport);

        ProcessedReport InsertProcessReportNumberOfClass(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport InsertProcessReportNumberOfChildren(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport GetProcessReportNumberOfClass(IDictionary<string, object> dicReport);
        ProcessedReport GetProcessReportNumberOfChildren(IDictionary<string, object> dicReport);
        ProcessedReport GetProcessReportOfChilden(IDictionary<string, object> dicReport, string reportCode);
        ProcessedReport InsertProcessReport(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport InsertProcessReportOfChildren(IDictionary<string, object> dicReport, Stream excel, string reportCode);
        ProcessedReport InsertProcessInformationBook(IDictionary<string, object> dicReport, Stream excel, string reportCode);
        
        ProcessedReport GetProcessReport(IDictionary<string, object> dicReport);
        ProcessedReport InsertProcessReportSummaryEvaluation(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport GetProcessReportSummaryEvaluation(IDictionary<string, object> dicReport);

        ProcessedReport InsertProcessReportEvaluation(IDictionary<string, object> dicReport, Stream excel);
        ProcessedReport GetProcessReportEvaluation(IDictionary<string, object> dicReport);
        
        
        void InsertPupilOfClass(int UserID, PupilOfClass lstPupil);
        void UpdatePupilOfClass(int UserID, PupilOfClass lstPupil);
        void Delete(int UserID, int PupilOfClassID, int SchoolID);
        void DeletePupilOfClass(List<PupilOfClass> lsPupilOfClass);
        IQueryable<PupilOfClass> Search(IDictionary<string, object> dic);
        IQueryable<PupilOfClass> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        IQueryable<PupilOfClass> SearchLeftJoin(IDictionary<string, object> dic);
        void ClassForwarding(int UserID, List<PupilOfClass> lsPupilMove, int SchoolID, int newClassID, int maxOrderInNewClass);
        void UpdateOrder(int UserID, int ClassID, List<int> lsPupilID, List<int> lsOrderInClass);

        bool CheckValidatePupilCode(string PupilCode, int ClassID);
        bool CheckValidateName(string Name, int ClassID);
        PupilOfClass CheckValidatePupil(string name, string pupilCode, int ClassID);
        List<PupilOfClassBO> GetPupilOfClassInSemester(int AcademicYearID, int SchoolID, int? EducationLevelID, int Semester, int? ClassID = null, int? AppliedLevel = null);
        IQueryable<ClassInfoBO> GetClassAndTotalPupilNotRegister(IQueryable<ClassProfile> iqClass, IDictionary<string, object> dic);
        IQueryable<ClassInfoBO> GetClassAndTotalPupil(IQueryable<ClassProfile> iqClass, IDictionary<string, object> dic);
        IQueryable<PupilOfClass> SearchBySchool(int SchoolID, PupilOfClassSearchForm SearchForm = null);
        // QuangLM add - 08/08/2013
        IQueryable<PupilOfClassBO> GetPupilInClass(int ClassID);
        List<PupilOfClassBO> GetPupilInSemesterNoCheckAssignDate(int AcademicYearID, int SchoolID, int? EducationLevelID, int semester, int? ClassID = null, int? AppliedLevel = null);
        List<PupilOfClassBO> GetPupilOfClassStatusOrtherStudy(IDictionary<string, object> dic);
        IQueryable<PupilOfClassBO> GetStudyingPupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> dic);

        Stream CreateGraduationExamRegisterReport(IDictionary<string, object> dic);
        ProcessedReport InsertGraduationExamRegisterReport(IDictionary<string, object> dic, Stream data, int appliedLevel);
        ProcessedReport GetGraduationExamRegisterReport(IDictionary<string, object> dic);

        Stream CreatePupilByAgeReport(IDictionary<string, object> dic);
        ProcessedReport InsertPupilByAgeReport(IDictionary<string, object> dic, Stream data, int appliedLevel);
        ProcessedReport GetPupilByAgeReport(IDictionary<string, object> dic);
        IQueryable<PupilOfClassBO> GetPupilInClassTranscripReport(int ClassID);

        ProcessedReport GetChildrenByAgeReport(IDictionary<string, object> dic);
        Stream CreateChildrenByAgeReport(IDictionary<string, object> dic);
        ProcessedReport InsertChildrenByAgeReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetProcessReportSituationStatisticPupil(IDictionary<string, object> dicReport, string reportCode);
        ProcessedReport InsertProcessReportSituationStatisticPupil(IDictionary<string, object> dicReport, Stream excel, string reportCode, int Type);

        Stream ExportSituationStatisticsBySchool(IDictionary<string, object> dic, List<EducationLevel> lstEducation);
        
        Stream ExportSituationStatisticsBySuperVising(IDictionary<string, object> dic);
        List<PupilOfClassBO> GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID);

        void InsertReportAbsencePupilForSuperVising(IDictionary<string, object> dic);
        Stream ReportStatisticsPupilAbsenceBySup(IDictionary<string, object> dic);
        Stream ReportListPupilAbsenceBySup(IDictionary<string, object> dic);
        ProcessedReport GetTicketTakeExamPaperReport(IDictionary<string, object> dic);
        ProcessedReport InsertTicketTakeExamPaperReportZip(IDictionary<string, object> diczip, Stream data);
        ProcessedReport InsertTicketTakeExamPaperReport(IDictionary<string, object> dic, Stream data);

        ProcessedReport GetTemporaryGraduationPaperReport(IDictionary<string, object> dicReport, string reportCode);
        ProcessedReport InsertTemporaryGraduationPaperReport(IDictionary<string, object> dic, Stream data, string reportCode);
        Stream ExportTemporaryGraduationPaper(IDictionary<string, object> dic);
    }
}