/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IQuickHintBusiness 
    {
        List<QuickHint> GetListQuickHintByUserAccount(int userAccountId, string functionCode);
        void InsertOrUpdate(IDictionary<string, object> dic, List<QuickHint> lstQuickHint, bool isImport = false);
        void DeleteQuickHint(IDictionary<string, object> dic);
    }
}