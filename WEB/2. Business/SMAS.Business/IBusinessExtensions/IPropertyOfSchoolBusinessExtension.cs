/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPropertyOfSchoolBusiness 
    {
        IQueryable<PropertyOfSchool> Search(IDictionary<string, object> dic);
        IQueryable<PropertyOfSchool> SearchBySchool(IDictionary<string, object> dic,int SchoolID);
        void Delete(int PropertyOfSchoolID, int SchoolID);
        void DeleteBySchoolID(int SchoolID);
        PropertyOfSchool Insert(PropertyOfSchool propertyOfSchool);
    }
}