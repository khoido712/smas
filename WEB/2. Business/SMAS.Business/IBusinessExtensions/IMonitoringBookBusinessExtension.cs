
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Web.Mvc;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Business.IBusiness
{ 
    public partial interface IMonitoringBookBusiness 
    {
        void Insert(MonitoringBook monitoringBook);
        void InsertHealthTest(MonitoringBook MonitoringBook, PhysicalTest PhysicalTest, EyeTest EyeTest, ENTTest EntTest, SpineTest SpineTest, DentalTest DentalTest, OverallTest OverallTest);
        void Update(MonitoringBook monitoringBook);
        void Delete(int MonitoringBookID, int SchoolID);
        IQueryable<MonitoringBook> Search(IDictionary<string, object> dic);
        IQueryable<MonitoringBook> SearchByPupil(int SchoolID, int AcademicYearID, int PupilID, int ClassID);
        IQueryable<MonitoringBook> SearchBySchool(int SchoolID,IDictionary<string, object> dic);
        IQueryable<MonitoringBookSearch> GetListPupil(IDictionary<string, object> dic);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        IQueryable<MonthlyGrowthBO> GetListMonthlyGrowth(IDictionary<string, object> dic);
        IQueryable<MonthlyGrowthBO> GetListMonthlyGrowthPHHS(IDictionary<string, object> dic);
        IQueryable<MonthlyGrowthBO> GetAllMonitoringBookByPupilID(IDictionary<string, object> dic);
        IQueryable<MonthlyGrowthBO> GetAllMonitoringBookHistoryYourSelfByPupilID(IDictionary<string, object> dic);
        IQueryable<MonthlyGrowthBO> GetAllMonitoringBookByClassID(IDictionary<string, object> dic);
        void GetCreateInfo(IDictionary<string, object> dic, ref bool isActive, ref DateTime? maxDateClass, ref DateTime? maxDateSchool,ref string unitName);

        // Thuyen - 05/09/2017
        void ValidatePhysicalInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);
        void ValidateEyeInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);
        void ValidateEntInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);
        void ValidateSpineInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);
        void ValidateDentalInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);
        void ValidateOverAllInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID);

        void InsertOrUpdateValueHealthTest(
             List<MonitoringBookBO> lstMonitoringBook,
             List<PhysicalTestBO> lstPhysicalTest,
             List<EyeTestBO> lstEyeTest,
             List<ENTTestBO> lstEntTest,
             List<SpineTestBO> lstSpineTest,
             List<DentalTestBO> lstDentalTest,
             List<OverallTestBO> lstOverallTest,
            IDictionary<string, object> SearchInfo);

        void InsertOrUpdateValuePhysical(List<MonitoringBookBO> lstMonitoringBook, List<PhysicalTestBO> lstPhysicalTestBO, IDictionary<string, object> SearchInfo);
        void InsertOrUpdateValueEye(List<MonitoringBookBO> lstMonitoringBook, List<EyeTestBO> lstEyeTestBO, IDictionary<string, object> SearchInfo);
        void InsertOrUpdateValueENT(List<MonitoringBookBO> lstMonitoringBook, List<ENTTestBO> lstENTTestBO, IDictionary<string, object> SearchInfo);
        void InsertOrUpdateValueSpine(List<MonitoringBookBO> lstMonitoringBook, List<SpineTestBO> lstSpineTestBO, IDictionary<string, object> SearchInfo);
        void InsertOrUpdateValueDental(List<MonitoringBookBO> lstMonitoringBook, List<DentalTestBO> lstDentalTestBO, IDictionary<string, object> SearchInfo);
        void InsertOrUpdateValueOverAll(List<MonitoringBookBO> lstMonitoringBook, List<OverallTestBO> lstOverallTestBO, IDictionary<string, object> SearchInfo);

        void DeletePhysical(IDictionary<string, object> dic);
        void DeleteSpin(IDictionary<string, object> dic);
        void DeleteDental(IDictionary<string, object> dic);
        void DeleteOverAll(IDictionary<string, object> dic);
        void DeleteEye(IDictionary<string, object> dic);
        void DeleteEnt(IDictionary<string, object> dic);     
    }
}
