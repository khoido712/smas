/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  Dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISchoolFacultyBusiness
    {
        IQueryable<SchoolFaculty> Search(IDictionary<string, object> dic);
        IQueryable<SchoolFaculty> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        void Delete(int id, int SchoolID);
        
        void InsertSchoolFaculty(List<SchoolFaculty> lstInput);
        void UpdateSchoolFaculty(SchoolFaculty objUpdate, IDictionary<string, object> dic);
        
    }
}