/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISchoolMovementBusiness 
    {
        void InsertSchoolMovement(int UserID, SchoolMovement entity);
        void UpdateSchoolMovement(int UserID, SchoolMovement entity);
        void DeleteSchoolMovement(int UserID, int SchoolMovementID, int SchoolID);
        IQueryable<SchoolMovement> Search(IDictionary<string, object> SearchInfo = null);
        IQueryable<SchoolMovement> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null);
        List<SchoolMovement> GetListSchoolMovement(int AcademicYearID, int SchoolID, int Semester);
        IQueryable<SchoolMovementBO> GetListSchoolMovementByParameter(List<int> lstSchoolID, List<int> lstAcademicYearID, List<int> lstPupilID);
    }
}