﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportEmployeeByGraduationLevelBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKey(ReportEmployeeByGraduationLevelBO reportEmployeeByGraduationLevel);
        ProcessedReport InsertReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity, Stream data);
        ProcessedReport GetReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity);
        Stream CreateReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity);
        ProcessedReport InsertReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity, Stream data);
        ProcessedReport GetReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity);
        Stream CreateReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity);
        ProcessedReport InsertReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity, Stream data);
        ProcessedReport GetReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity);
        Stream CreateReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity);
    }
}
