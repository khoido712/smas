﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface ISynthesisSubjectByPeriodBusiness : IGenericBussiness<ProcessedReport>
    {
        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        string GetHashKey(SynthesisSubjectByPeriod entity);

        /// <summary>
        /// GetSynthesisSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        ProcessedReport GetSynthesisSubjectByPeriod(SynthesisSubjectByPeriod entity);

        /// <summary>
        /// InsertSynthesisSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        ProcessedReport InsertSynthesisSubjectByPeriod(SynthesisSubjectByPeriod entity, Stream data);

        /// <summary>
        /// Tạo file báo cáo
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        Stream CreateSynthesisSubjectByPeriod(SynthesisSubjectByPeriod entity);

    }
}
