﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IRegisterSMSFreeBusiness 
    {
        List<int> GetAllPupilId(int schoolId, int academicYearId);

        List<int> GetAllPupilId(int schoolId, int academicYearId, List<int> status);

        /// <summary>
        /// Lấy toàn bộ học sinh đã có trạng thái (đã đăng ký hoặc bị hủy hoặc đã phê duyệt)
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <returns></returns>
        List<int> GetAllPupilIdRegisterStatus(int schoolId, int academicYearId);

        List<SMS_REGISTER_FREE> GetByPupil(List<int> pupilIds, int academicYearId, int schoolId);

        List<int> GetPupilIdByPupil(List<int> pupilIds, int academicYearId, int schoolId);
        List<int> GetPupilIdByPupil(int academicYearId, int schoolId);
        void UpdateStatus(List<int> pupilIds, int academicYearId, int schoolId, int? status);
        void UpdateStatus(int pupilId, int academicYearId, int schoolId, int? status);
    }
}