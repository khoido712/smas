﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExaminationsBusiness
    {
        IQueryable<DOETExaminations> GetExaminationsOfDept(int supervisingDeptID, int? Year=null);
        IQueryable<DOETExaminations> Search(IDictionary<string, object> search);

        //Dong bo ky thi tu ho CM
        void SyncExamHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel);

        /// <summary>
        /// Dong bo thi sinh sang he thong thi SoHCM
        /// </summary>
        /// <param name="academicYear"></param>
        /// <param name="school"></param>
        /// <param name="appliedLevel"></param>
        /// <param name="examinationId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="doetSubjectId"></param>
        void SyncPupilSMAStoHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId);




        /// <summary>
        /// Xoa thi sinh tren he thong thi SoHCM
        /// </summary>
        /// <param name="academicYear"></param>
        /// <param name="school"></param>
        /// <param name="lstExamPupilId"></param>
        void DelteExamPupilHCM(AcademicYear academicYear, SchoolProfile school, List<DOETExamPupil> lstExamPupil);


        /// <summary>
        /// Dong bo thi sinh tu SoHCM ve SMAS
        /// </summary>
        /// <param name="academicYear"></param>
        /// <param name="school"></param>
        /// <param name="appliedLevel"></param>
        /// <param name="examinationId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="subjectId"></param>
        /// <param name="lstRetThiSinh"></param>
        void SyncPupilHCMtoSMAS(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId, int subjectId, List<BusinessObject.DanhSachThiSinh> lstRetThiSinh);
    }
}
