﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportMarkInputSituationBusiness
    {
        Stream CreateReportMarkInputSituation(int SchoolID, int AcademicYearID, int SemesterID, int EducationLevelID);
        Stream CreateReportPupilNotEnoughMark(int SchoolID, int AcademicYearID, int SemesterID, int EducationLevelID, int ClassID);
        ReportMarkInputSituationBO GetInputMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID);
        ReportMissingMarkSituationBO GetMissingMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID);
        ProcessedReport InsertMarkInputSituationReport(IDictionary<string, object> dic, Stream data);
    }
}
