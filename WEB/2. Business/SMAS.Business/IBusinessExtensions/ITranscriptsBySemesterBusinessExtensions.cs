﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ITranscriptsBySemesterBusiness
    {
        string GetHashKeyForTranscriptsBySemester(TranscriptOfClass entity);
        ProcessedReport GetTranscriptsBySemester(TranscriptOfClass entity);
        ProcessedReport InsertTranscriptsBySemester(TranscriptOfClass entity, Stream data);
        Stream CreateTranscriptsBySemester(TranscriptOfClass entity);
        string GetHashKeyForSummariseTranscriptsBySemester(TranscriptOfClass entity);
        ProcessedReport GetSummariseTranscriptsBySemester(TranscriptOfClass entity);
        ProcessedReport InsertSummariseTranscriptsBySemester(TranscriptOfClass entity, Stream data);
        Stream CreateSummariseTranscriptsBySemester(TranscriptOfClass entity);
        string GetHashKeyForAverageAnnualMark(TranscriptOfClass entity);
        ProcessedReport InsertAverageAnnualMark(TranscriptOfClass entity, Stream data);
        ProcessedReport GetAverageAnnualMark(TranscriptOfClass entity);
        Stream CreateAverageAnnualMark(TranscriptOfClass entity);
    }
}
