/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDentalTestBusiness 
    {
        void Insert(DentalTest entity, MonitoringBook entity1);
        void Update(DentalTest entity, MonitoringBook entity1);
        void Delete(int DentalTestID, int SchoolID);
        IQueryable<DentalTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<DentalTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID);
        List<DentalTest> ListDentalTestByMonitoringBookId(List<int> lstMoniBookID);
    }
}