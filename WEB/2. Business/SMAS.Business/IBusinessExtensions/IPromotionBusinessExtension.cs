/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPromotionBusiness 
    {
        PromotionProgramBO GetPromotionProgramByProvinceId(int provinceId, int unitId);
        List<PromotionProgramBO> GetLstPromotionProgram();
        IQueryable<PromotionProgramDetailBO> GetlistSchool(Dictionary<string, object> dic);
        IQueryable<PromotionProgramDetailBO> GetlistSupervisingDept(Dictionary<string, object> dic);
        string Insert(PromotionProgramBO obj, string lstProvince, ref long PromotionID);
        string Update(PromotionProgramBO obj, string lstProvince);
        string DeletePromotion(int proId);
        PromotionProgramBO GetPromotionProgramById(int prId);
    }
}