/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISupervisingDeptBusiness 
    {
        IQueryable<SupervisingDept> ListChildDept(int? deptId);
        IQueryable<SupervisingDept> Search(IDictionary<string, object> SearchInfo);
        SupervisingDept Insert(SupervisingDept entityToInsert);
        SupervisingDept Update(SupervisingDept entity);
        void Delete(int id, int UserAccountID);
        void InsertSupervisingDept(SupervisingDept supervisingDept, string userName, int UserAccountID, int RoleID, bool isActive,string password = "");
        void UpdateSupervisingDept(SupervisingDept supervisingDept, bool isActive, bool isForgetPassword, int RoleID = 0);
        SupervisingDept GetSupervisingDeptOfUser(int UserAccountID);
        List<SupervisingDept> GetManagedUnitDirectly(int UserAccountID);
        void DeleteSub(int parentID);
        IQueryable<SupervisingDept> GetFollwerSupervising(int SupervisingDeptID);
        List<SubjectCatBO> SearchSubject(IDictionary<string, object> dic);
        List<SupervisingDept> GetListSuperVisingDeptByID(int SupervisingDeptID);
        List<int> GetEmployeeBySuperVisingDeptNoPaging(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID);
        List<EmployeeSMSBO> GetEmployeeBySuperVisingDept(int provinceID, int ParentID, int currentEmployeeID, string searchContent, int supervisingDeptID);
        List<EmployeeBO> GetEmployeeBySuperVisingDeptID(int SupervisingDeptID, bool SupervisingRole, bool SupervisingSubRole);
        List<SupervisingDeptBO> GetListSuperVisingDeptBO(int SupervisingDeptID);
    }
}