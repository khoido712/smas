﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Web.Mvc;

namespace SMAS.Business.IBusiness
{
    public partial interface ILockRatedCommentPupilBusiness
    {
        string SaveLockRatedCommentPupil(int submitType, int schoolId, int educationId, int classId, int academicYearId, Dictionary<int, List<string>> subjectLockTitleDic, List<string> talentLst, List<string> ethicLst, List<int> lstBubjectId);
        IQueryable<LockRatedCommentPupil> Search(IDictionary<string, object> dic);

        void SaveDataLockVNEN(FormCollection frm, IDictionary<string, object> dic, LockRatedCommentPupilBO objLockRatedBO);
        string GetLockTitleVNEN(IDictionary<string, object> dic);
    }
}
