/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.Business.Business;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassProfileBusiness 
    {
        IQueryable<ClassProfile> SearchTeacherTeachingBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<ClassProfile> SearchTeacherTeaching(IDictionary<string, object> dic);

        IQueryable<ClassProfile> Search(IDictionary<string, object> dic);
        IQueryable<ClassProfile> SearchClassBySupervisingDept(Dictionary<string, object> dic);
        void Delete(int ClassProfileID, int AcademicYearID, int AppliedLevel, RESTORE_DATA objRestore, int? UserID = 0);
        void UpdateClassAss(ClassProfile ClassProfile);
        void UpdateCustom(ClassProfile ClassProfile, IDictionary<string, object> dic);
        IQueryable<ClassProfile> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<ClassProfile> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null);
        bool IsHeadTeacher(int TeacherID, int AcademicYearID, int ClassID);
        IQueryable<ClassProfile> GetListClassByHeadTeacher(int TeacherID, int AcademicYearID);

        bool IsSubjectTeacher(int TeacherID, int AcademicYearID, int ClassID);
        IQueryable<ClassProfile> GetListClassBySubjectTeacher(int TeacherID, int AcademicYearID);

        int GetEducationLevelIDByClassID(int ClassID);

        bool IsOverseeingTeacher(int TeacherID, int AcademicYearID, int ClassID);
        IQueryable<ClassProfile> GetListClassByOverSeeing(int TeacherID, int AcademicYearID);

        IQueryable<ClassProfile> GetListClassByTeacher(int TeacherID, int AcademicYearID);
        List<ClassOfTeacherBO> SearchByTeacherPermission(IDictionary<string, object> dic);

        List<ClassOfTeacherBO> SearchByTeacherPermissionMobile(IDictionary<string, object> dic);
        List<ClassOfTeacherBO> GetListTeachingClass(int academicYearID, int appliedLevel, int employeeID);
        bool CheckRoleOfClass(int teacherID, int academicYearID, int classID, RoleOfClass roleCheck);

        void UpdateClassOrder(int SchoolID = 0, int AcademicYearID = 0);

        List<ClassProfileRefBO> GetListClassNotSummedUp(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel);

        void RemoveHeadTeacherInClass(Dictionary<string, object> dic);
        IQueryable<ClassProfile> getClassByAccountRole(int academicYearID, int schoolID, int? educationLevelID, int? classID, int userAccountID, bool isSuperViSingDept, bool isSubSuperVisingDept);
        List<ClassProfileBO> GetListClassBySubject(IDictionary<string, object> dic);

        IQueryable<ClassProfile> getClassByRoleAppliedLevel(int academicYearID, int schoolID, int appliedLevel, int userAccountID);

        IQueryable<ClassProfile> getClassByRoleAppliedLevelMobile(int academicYearID, int schoolID, int appliedLevel, int userAccountID, int role);

        IQueryable<ClassProfile> getClassAdminOrHeadTeacher(int academicYearID, int schoolID, int appliedLevel, int userAccountID);
        List<ClassProfileBO> GetListClassByEducationLevel(IDictionary<string, object> dic);
        bool CheckIsCombinedClass(int schoolId, int academicYear, int AppliedLevel);
        List<ClassProfile> GetListClassBySchool(int SchoolID, int AcademicYearID);
        List<ClassProfile> GetListClass(int AcademicYearID, int EducationLevelID);
        Stream CreateClassProfileReport(IVTWorkbook oBook, List<ClassProfile> listClassProfile, int SchoolID, int AcademicYearID, int AppliedLevel, int AppliedTraningType, List<int?> ListAppliedLevelByGrade);
        void DownloadZipFileClassProfile(List<ClassProfile> listClassProfile, List<int?> ListAppliedLevelByGrade, int SchoolID, int AcademicYearID, int AppliedLevel,int AppliedTraningType, out string folderSaveFile);
        void CreateZipFileClassProfile(IVTWorkbook oBook, List<ClassProfile> listClassProfile, List<int?> ListAppliedLevelByGrade, int SchoolID, int AcademicYearID, int AppliedLevel, int AppliedTraningType, string folder);
        ProcessedReport InsertZipFileClassProfileReport(Transcripts entity, Stream data, string fileNameByLevel, bool isZip = false);
        string GetHashKeyTranscriptsReport(Transcripts entity);
        ProcessedReport GetReportClassProfile(IDictionary<string, object> dic, bool isZip = false);
        List<PropertyOfClassBO> GetDataPropertyOfClass(List<int?> listAppliedLevel, List<int> listClassProfile);
        void SetDataForReportClassProfile(int AppliedLevel, int SchoolID, int AppliedTraningType, List<PropertyOfClassBO> listPropertyOfClassAll,
                                IVTWorksheet oSheet1, List<ClassProfile> listClassProfile, List<ClassSubjectBO> listClassSubjectAll, IQueryable<SubjectCat> listSubjectCatAll,
                                List<ClassSpecialtyCat> listClassSpecialCat, List<int> listAppliedSpecKindergarten = null);
        Dictionary<string, string> DicApprenticeshipClassTypeList();
        Dictionary<string, string> DicApprenticeshipTrainingTypeList();
    }
}