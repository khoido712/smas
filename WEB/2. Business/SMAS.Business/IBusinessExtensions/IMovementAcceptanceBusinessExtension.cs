/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IMovementAcceptanceBusiness 
    {
        /// <summary>
        /// Tiep nhan cac du lieu tu truong cu.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="entity"></param>
        /// <param name="IsInHistory">Chi ra nam hoc dang chon la nam hoc cu hay nam hoc hien tai</param>
        void InsertMovementAcceptance(int UserID, MovementAcceptance entity, bool IsInHistory);
        IQueryable<MovementAcceptance> SearchBySchool(int SchoolID = 0,IDictionary<string, object> dic = null);
        List<MovementAcceptance> GetListMovementAcceptance(int AcademicYearID, int SchoolID, int Semester);
    }
}