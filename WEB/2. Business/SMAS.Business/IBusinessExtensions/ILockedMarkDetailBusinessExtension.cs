/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ILockedMarkDetailBusiness
    {
        void InsertLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail, int SchoolID, int AcademicYearID, int AppliedLevel, int Semester);
        //void InsertLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail);
        //void UpdateLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail);
        void DeleteLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail, int SchoolID);
        //IQueryable<LockedMarkDetail> SearchLockedMarkDetail(IDictionary<string, object> SearchInfo);
        IQueryable<LockedMarkDetail> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        void CheckCurrentLockMark(string clientLockMarkTitle, Dictionary<string, object> dic);
        /// <summary>
        /// 27/03/2014
        /// Quanglm1
        /// Ap dung khoa diem cho toan truong va toan khoi
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        void ApplyLockMark(int SchoolID, int AcademicYearID, int Semester, int Grade, int ClassID, int EducationLevelID = 0);
        IQueryable<LockedMarkDetail> SearchLockedMarkDetail(IDictionary<string, object> SearchInfo);
        void InsertLockedMarkDetail(List<LockedMarkDetail> ListLockedMarkDetail);
    }
}