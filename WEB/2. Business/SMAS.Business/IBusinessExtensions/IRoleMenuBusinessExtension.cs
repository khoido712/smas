/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IRoleMenuBusiness 
    {
        void DeleteRoleOfMenu(int menuId);
        bool AssignMenuForRoles(int MenuID, List<int> ListRoleID, int RoleID);
        IQueryable<Menu> GetMenuOfRole(int RoleID, bool mnuVisible = true);
        IQueryable<Role> GetRoleOfMenu(int menuID);

    }
}