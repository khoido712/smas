/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExaminationsBusiness 
    {
        List<Examinations> GetListExamination(int academicYearID);
        List<ExaminationsBO> GetListExaminations(IDictionary<string, object> search);
        IQueryable<Examinations> Search(IDictionary<string, object> search);
        void InsertExaminations(Examinations exam);
        void UpdateExaminations(Examinations exam, bool DataInheritance, long InheritanceExam, string InheritanceData);
        void DeleteExaminations(int examID, int schoolID);
        ExaminationsBO GetExaminationsByID(long examinationsID);
        void ResetInheritance(long ExaminationsID);
        /// <summary>
        /// Anh xa entity voi column table
        /// </summary>
        IDictionary<string, object> ExamGroupMapping();
        IDictionary<string, object> ExamSubjectMapping();
        IDictionary<string, object> ExamPupilMapping();
        IDictionary<string, object> ExamCandenceBagMapping();
        IDictionary<string, object> ExamDetachableBagMapping();
        IDictionary<string, object> ExamRoomMapping();
        IDictionary<string, object> ExamBagMapping();
        IDictionary<string, object> ExamSupervisoryAssignmentMapping();
        IDictionary<string, object> ExamSupervisoryMapping();
    }
}