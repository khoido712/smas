﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Business.IBusiness
{
    public partial interface ISummedEndingEvaluationBusiness
    {
        List<SummedEndingEvaluation> GetSummedEndingEvaluation(IDictionary<string, object> dic);
        void InsertOrUpdate(IDictionary<string, object> dic, List<SummedEndingEvaluation> lstSummedEnding);
        void DeleteSummedEvaluation(IDictionary<string, object> dic, List<long> lstPupilID);
        /// <summary>
        /// Thuc hien them moi hoac cap nhat du lieu nang luc, pham chat cua lop theo co che bulk.
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <param name="lstSummedEnding"></param>
        void InsertOrUpdatebyClass(int classId,IDictionary<string, object> dic, List<SummedEndingEvaluation> lstSummedEnding);

        IQueryable<SummedEndingEvaluationBO> Search(IDictionary<string, object> dic);

        ProcessedReport InsertGeneralSummedEvaluationReport(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int SemesterID, string ClassName, Stream data, int PrintType, bool IsZip, List<string> lst);

        Stream GetGeneralReport(IDictionary<string, object> dic);
        ProcessedReport GetProcessedReport(int AcademicYearID, int SchoolID, int TypeSemesterID, int EducationLevelID, int ClassID);
        ProcessedReport GetProcessedReportSummedEnding(int AcademicYearID, int SchoolID, int TypeSemesterID, int EducationLevelID, int ClassID, int PrintType, bool IsZip, List<string> lstPrintData);
        ProcessedReport GetProcessedRepeaterReport(IDictionary<string, object> dic);
        Stream GetRepeaterReport(IDictionary<string, object> dic);
        Stream GetGeneralReportZip(IDictionary<string, object> dic, ClassProfile objCP, List<string> lstDataPrint,
                                            List<PupilOfClassBO> lstPOC, IVTWorkbook oBook, AcademicYear objAy, SchoolProfile objSP,
                                            List<ClassSubjectBO> lstSubject, List<RatedCommentPupilBO> lstRatedCommentPupilBO,
                                            List<SummedEndingEvaluationBO> lstSEE, List<EvaluationReward> lstEvaluationReward);
        ProcessedReport InsertRepeaterProcessedReport(IDictionary<string, object> dicInsert, Stream data);
        ProcessedReport InsertGeneralSummedEvaluationReportZip(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int SemesterID, string ClassName, Stream output, int PrintType, bool IsZip, List<string> lst);
        void InsertOrUpdateToMobile(IDictionary<string, object> dic, SummedEndingEvaluation objSummedEnding);
    }
}
