/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISummedUpRecordBusiness 
    {
        void InsertSummedUpRecord(List<SummedUpRecord> lstSummedUpRecord);
        void UpdateSummedUpRecord(List<SummedUpRecord> lstSummedUpRecord);
        void DeleteSummedUpRecord(int UserID, long SummedUpRecordID, int? SchoolID);
       // IQueryable<SummedUpRecord> Search(IDictionary<string, object> dic);
        IQueryable<SummedUpRecord> GetPupilRetestSubject(int PupilID, int AcademicYearID, int SchoolID, int Semester, int Year, IDictionary<string, object> SearchInfo);
        IQueryable<SummedUpRecord> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<SummedUpRecordBO> GetSummedUpRecordOfClass(int AcademicYearID, int SchoolID, int Semester,
            int ClassID, int? PeriodID, int SubjectID);
        IQueryable<SummedUpRecordBO> GetSummedUpRecordCareerOfClass(int AcademicYearID, int SchoolID, int ClassID, int SubjectID);
        //decimal? CaculatorSummedUpRecord(List<MarkRecord> lstMarkRecord, List<MarkType> listMarkType, SemeterDeclaration semeterDeclaration, ClassSubject objClassSubject, List<RegisterSubjectSpecializeBO> lstRegisterBO = null);
        string CaculatorJudgementResult(List<JudgeRecord> lstJudgeRecord, List<MarkType> listMarkType);
        // Quanglm them theo nghiep vu moi
        IQueryable<SummedUpRecordBO> GetSummedUpRecord(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID);
        void InsertOrUpdateSummedUpRecordAfterRetest(int SchoolID, int AcademicYearID, IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest);
        List<SummedUpRecord> DeleteSummedUpRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        IQueryable<SumUpRecordPrimaryBO> GetSummedUpWithRestestOfPrimary(int SchoolID, int AcademicYearID, int Semester, int ClassID, int SubjectID);
        /// <summary>
        /// Lay diem trung binh mon
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="PeriodID"></param>
        /// <param name="isShowRetetResutl"></param>
        /// <returns></returns>
        IQueryable<SummedUpRecordBO> GetSummedUpRecordCurrentYear(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID, bool isShowRetetResutl);
        void DeleteSummedUpRecordPrimary(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);

        /// <summary>
        /// Luu diem tong ket de phuc vu viec phuc vu
        /// </summary>
        /// <param name="lstSummed"></param>
        /// <param name="objRes"></param>
        /// <returns></returns>
        List<RESTORE_DATA_DETAIL> BackUpSummedMark(List<SummedUpRecord> lstSummed, RESTORE_DATA objRes);
        IQueryable<SummedUpRecordBO> GetSummedUpRecordByListClassID(List<int> lstClassID, int AcademicYearID, int SchoolID, int Semester, int? PeriodID);
    }
}
