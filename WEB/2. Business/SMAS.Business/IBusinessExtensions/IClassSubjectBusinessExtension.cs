﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IClassSubjectBusiness
    {
        IQueryable<ClassSubject> Search(IDictionary<string, object> SearchInfo);
        void Insert(List<ClassSubject> ListClassSubject, int SchoolID, int AcademicYearID, int Grade);
        void Update(List<ClassSubject> ListClassSubject, int SchoolID, int AcademicYearID, int Grade);
        //IQueryable<ClassSubject> Search(IDictionary<string, object> SearchInfo);
        void Delete(List<ClassSubject> ListClassSubject, int AcademicYearID);
        IQueryable<ClassSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<ClassSubject> SearchByClass(int ClassID, IDictionary<string, object> SearchInfo = null);
        IQueryable<ClassSubject> SearchByList(IDictionary<string, object> SearchInfo);

        //List<SubjectCat> SearchMarkedSubjectInSchool(int SchoolID, int AcademicYearID, int EducationLevelID);
        //List<ClassSubject> GetListClassBySubject(IDictionary<string, object> SearchInfo);
        //IQueryable<ClassSubject> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null);
        //IQueryable<SubjectCat> SearchSubjectByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null);
        List<ClassSubject> GetListSubjectBySubjectTeacher(int UserAccountID, int AcademicYearID, int SchoolID, int Semester, int ClassID, bool isTeacherCanView);
        /// <summary>
        /// Lay danh sach mon hoc giao vien duoc phan cong phu trach lop:Phuc vu cho viec import so theo doi CLGD
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        List<ClassSubject> GetListSubjectBySubjectTeacher(int UserAccountID, int AcademicYearID, int SchoolID, int Semester, int ClassID,int TyOfTeacher);
        /// <summary>
        /// Chiendd1: 08/07/2015. Khai bao mon hoc cho lop tu mon hoc cho truong
        /// Trong tham bien dic phai co AcademicYear va EducationLevelId
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="lstSchoolSubject"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        bool InsertClassSubjectbySchoolSubject(int schoolId,List<SchoolSubject> lstSchoolSubject, Dictionary<string, object> dic);

        /// <summary>
        /// anhnph1: 29/07/2015. Cap nhat mon hoc tang cuong cho mon o cot Subject_Is_Increase
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="dicSubjectSaveIncrease"></param>
        void UpdateSubjectIncreaseByClass(int AcademicYearID, int SchoolID, int semesterId, IDictionary<string, object> dicSearch, IDictionary<int, int> dicSubjectSaveIncrease);

        /// <summary>
        /// anhnph1: 29/07/2015. Cap nhat mon hoc tang cuong ap dung toan khoi 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="lsClassOfEducationLevelID"></param>
        /// <param name="dicSearchByEducationLevel"></param>
        /// <param name="dicSubjectSaveIncrease"></param>
        void UpdateSubjectIncreaseByEducationLevel(int AcademicYearID, int SchoolID, int semesterId, List<int> lsClassOfEducationLevelID, int ClassCurrent, IDictionary<string, object> dicSearchByEducationLevel, IDictionary<int, int> dicSubjectSaveIncrease);

        /// <summary>
        /// anhnph1: 29/07/2015. Cap nhat mon hoc tang cuong ap dung toan truong 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="lstEducationLevel"></param>
        /// <param name="dicSubjectSaveIncrease"></param>
        void UpdateSubjectIncreaseBySchool(int AcademicYearID, int SchoolID, int semesterId, List<int> lstEducationLevel, int classCurrent, IDictionary<int, int> dicSubjectSaveIncrease, IDictionary<string, object> dicSearchByEducationLevel);

        /// <summary>
        /// Lay danh sach lop hoc theo mon hoc
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="subjectID"></param>
        /// <param name="educationLevelID"></param>
        /// <returns></returns>
        List<ClassProfileTempBO> GetListClassBySubjectId(int academicYearID, int schoolId, int subjectID, int educationLevelId, int teacherId, bool admin, int semester,
            int appliedLevel, bool isAdminSchoolRole, bool isViewAll, bool isEmployeeManager, int userAccountID,int classID,bool? isVNEN = false);

        /// <summary>
        /// danh sach lop hoc theo linh vuc
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="schoolId"></param>
        /// <param name="declareEvationGroupId"></param>
        /// <param name="educationLevelId"></param>
        /// <param name="admin"></param>
        /// <param name="appliedLevel"></param>
        /// <param name="userAccountID"></param>
        /// <param name="classID"></param>
        /// <returns></returns>
        List<ClassProfileTempBO> GetListClassByDeclareEvationGroupId(
            int academicYearID, int schoolId, int declareEvationGroupId, int educationLevelId, bool admin,
            int appliedLevel, int userAccountID, int classID);
       /// <summary>
       /// Chiendd1: Luu bang threadMark khi co su thay doi mon hoc cho lop
       /// </summary>
       /// <param name="lstClassSubject"></param>
       /// <param name="academicYearId"></param>
       /// <param name="schoolId"></param>
       /// <param name="currentSemester"></param>
        void InsertThreadMark(List<ClassSubject> lstClassSubject, int academicYearId, int schoolId, int currentSemester);

        List<ClassSubject> GetListSubjectByClass(int academicYearId, int classId, List<int> listSujectId);
        void SaveMinMarkNumber(List<int> lstClassId, List<ClassSubject> lstModel, int? schoolId, int? academicYearId, int? appliedLevel);

        /// <summary>
        /// Danh sach mon hoc giao vien duoc phan cong giang day hoac phan cong giao vien bo mon(Giao vu)
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        List<SubjectCatBO> GetListSubjectbyTeaching(int UserAccountID, int AcademicYearID,
            int SchoolID, int Semester, int ClassID);

        /// <summary>
        /// Danh sach lop hoc giao vien co quyen giao vien bo mon hoac phan cong giang day
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="schoolId"></param>
        /// <param name="subjectID"></param>
        /// <param name="teacherId"></param>
        /// <param name="schoolAdmin"></param>
        /// <param name="semester"></param>
        /// <param name="isVNEN"></param>
        /// <returns></returns>
        List<ClassProfileTempBO> GetListClassByTeachingAndSubjectId(int academicYearID, int schoolId, int subjectID, int teacherId, bool schoolAdmin, int semester, bool? isVNEN = false);
    }
}