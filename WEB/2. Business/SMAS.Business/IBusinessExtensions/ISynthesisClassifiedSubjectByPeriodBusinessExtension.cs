﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface ISynthesisClassifiedSubjectByPeriodBusiness : IGenericBussiness<ProcessedReport>
    {
        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        string GetHashKey(SynthesisClassifiedSubjectByPeriod entity);

        /// <summary>
        /// GetSynthesisClassifiedSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        ProcessedReport GetSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity);

        /// <summary>
        /// InsertSynthesisClassifiedSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        ProcessedReport InsertSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity, Stream data);

        /// <summary>
        /// Tạo file báo cáo
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        Stream CreateSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity);

    }
}
