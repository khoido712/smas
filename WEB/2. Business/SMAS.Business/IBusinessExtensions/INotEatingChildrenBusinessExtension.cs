/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface INotEatingChildrenBusiness
    {
        int CountNotEatingChildren(int PupilID, int Month, int Year, int SchoolID, int AcademicYearID);
        List<MoneyPerChildren> GetListChildren(int AcademicYearID, int SchoolID, int EducationLevelID,
            int ClassID, string PupilCode, string FullName, int Month, int Year);

        void Insert(int SchoolID, int AcademicYearID, int ClassID, DateTime FromDate,
            DateTime ToDate, List<NotEatingChildren> lstNotEatingChildren);

        IQueryable<NotEatingChildren> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
    }
}