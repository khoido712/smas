﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Business.IBusiness
{
    public partial interface ITranscriptsBusiness
    {
        string GetHashKey(Transcripts transcripts);
        ProcessedReport InsertTranscripts(Transcripts transcripts, Stream data);
        ProcessedReport GetTranscripts(Transcripts transcripts, bool isZip = false);
        Stream CreateTranscripts(Transcripts transcripts);

        // QuangNN2 - Bao cao Hoc ba tieu hoc - 06/04/2015
        ProcessedReport GetReportPrimaryTranscripts(IDictionary<string, object> dic, bool isZip = false);
        Stream CreateReportPrimaryTranscripts(IDictionary<string, object> dic);
        ProcessedReport InsertReportPrimaryTranscripts(IDictionary<string, object> dic, Stream data, bool isZip = false);
        void DownloadZipFileTranscript(Transcripts transcripts, out string folderSaveFile);
        ProcessedReport InsertTranscriptsReport(Transcripts entity, Stream data, bool isZip = false);
        void CreateTranscriptsZipFile(IVTWorkbook oBook, Transcripts transcripts, AcademicYear Aca, List<VJudgeRecord> lstjudgeAll,
            PupilOfClassBO objPoc, SchoolProfile sp, Commune commune, District district, Province province, List<EducationLevel> listLevel,
            List<PupilOfClass> lstPupilOfClassNullAll, List<PupilOfClass> lstPupilOfClassNotNullAll, List<PupilOfClass> lspocAll,
            List<ClassSubject> classSubjectAll, List<TeachingAssignment> iqTeachingAssignmen, List<Employee> IqEmployee,
            List<SummedUpRecordBO> lstSummedUpRecordBOAll, List<PupilRankingBO> lstPupilRankingBOAll, List<PupilPraise> listPupilPraiseAll,
            List<PupilAbsence> pupilAbsenceListAll, List<Employee> lstEmployeeAll, List<SchoolProfile> lstSchoolAll, List<AcademicYear> lstAcaAll,
            bool checkCurrentYear, bool fillPaging, ref List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber);
        
        //void CreateTranscriptsZipFilePrimary(IVTWorkbook oBook, IDictionary<string, object> dic, PupilOfClassBO objPoc, List<AcademicYearBO> queryPOCAll,
        //    List<InfoEvaluationPupil> queryInfoPupilAll,
        //     List<PupilAbsence> queryPupilAbsenceAll, List<ClassSubjectBO> lstClassSubject, List<SummedEvaluation> querySEAll,
        //    List<SummedEvaluationHistory> querySEHAll, List<SummedEndingEvaluation> querySEEAll, List<RewardCommentFinal> queryRCFAll, List<RewardFinal> queryRF,
        //    List<Employee> lstEmployeeAll, List<SchoolProfile> lstSchoolAll, List<AcademicYear> lstAcaAll, List<ClassProfile> lstCpAll);
        void DownloadZipFileTranscriptPrimary(IDictionary<string, object> dic, out string folderSaveFile);
    }
}
