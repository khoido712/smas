/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamSupervisoryViolateBusiness 
    {
        IQueryable<ExamSupervisoryViolateBO> Search(IDictionary<string, object> dic);
        void DeleteList(List<long> listID);
        IQueryable<ExamSupervisoryViolate> SearchViolate(IDictionary<string, object> search);
        ProcessedReport GetExamSupervisoryViolateReport(IDictionary<string, object> dic);
        Stream CreateExamSupervisoryViolateReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamSupervisoryViolateReport(IDictionary<string, object> dic, Stream data);
    }
}