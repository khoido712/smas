﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IGraduationApprovalBusiness : IGenericBussiness<GraduationApproval>
    {
        string CreateOrUpdateGraduationApproval(GraduationApproval GraduationApprovalItem);
        //void UpdateGraduationApproval(List<GraduationApproval> listGraduationApproval);
        void SaveBySubSuperVising(List<GraduationApproval> lstData, Dictionary<string, object> dic);
        Stream ExportApproveGraduationProfile(IVTWorkbook oBook, List<PupilGraduationBO> listApprovePupil, List<PupilGraduationBO> listApprovePupilPriority,
                                                       SchoolProfile objSP, AcademicYear acaYear, Dictionary<string, object> dic);
        IQueryable<GraduationApprovalViewModel> GetDataForExportExcel(Dictionary<string, object> dic);
    }
}
