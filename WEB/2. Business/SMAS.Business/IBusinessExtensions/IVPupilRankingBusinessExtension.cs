/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IVPupilRankingBusiness 
    {
        /// <summary>
        /// QuangLM
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<VPupilRanking> SearchBySchool(int SchoolID, IDictionary<string, object> dic);

        /// <summary>
        /// Thuc hien xep hang sau khi da tong ket
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="dic"></param>
        void UpdateRank(int schoolId, bool isMovedHistory, IDictionary<string, object> dic);

        /// <summary>
        /// Cap nhat xep hang vao DB tu danh sach da duoc xep hang da co
        /// </summary>
        /// <param name="dicPupilNotConductRank"></param>
        /// <param name="lstPupilRanking"></param>
        void UpdateRankPeriod(Dictionary<int, bool> dicPupilNotConductRank, List<PupilRanking> lstPupilRanking, AcademicYear academicYear);
    }
}