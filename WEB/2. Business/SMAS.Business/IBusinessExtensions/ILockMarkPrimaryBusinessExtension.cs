/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ILockMarkPrimaryBusiness
    {
        IQueryable<LockMarkPrimary> SearchLockedMark(IDictionary<string, object> SearchInfo);
        void CheckCurrentLockMark(string clientLockMarkTitle, int classID, int isCommenting);
        void InsertAndDelete(List<LockMarkPrimary> lstLockMarkInsert, List<LockMarkPrimary> lstLockMarkDelete);
    }
}