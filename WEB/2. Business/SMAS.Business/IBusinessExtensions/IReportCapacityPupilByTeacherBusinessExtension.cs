﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportCapacityPupilByTeacherBusiness : IGenericBussiness<ProcessedReport>
    {
        Stream CreateReportCapacityPupilByTeacher(int SchoolID, int AcademicYearID, int AppliedLevel, int semester, int SubjectID, List<int> lstTeacher);
    }
}
