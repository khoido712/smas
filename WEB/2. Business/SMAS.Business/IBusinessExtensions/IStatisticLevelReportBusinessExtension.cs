﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IStatisticLevelReportBusiness
    {
        IQueryable<StatisticLevelReport> SearchBySchool(int SchoolID, Dictionary<string, object> dic = null);
        IQueryable<StatisticLevelReport> Search(IDictionary<string, object> dic);
        /// <summary>
        /// Lấy các mức thống kê của trường
        /// </summary>
        /// <param name="schoolID"></param>
        /// <returns></returns>
        List<StatisticLevelReport> getStatisticLevelReportOfSchool(int schoolID);
    }
}
