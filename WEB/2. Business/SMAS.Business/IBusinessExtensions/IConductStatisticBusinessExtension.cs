/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IConductStatisticBusiness
    {
        void SendAll(List<ConductStatisticsBO> ListData);
        List<ConductStatisticsOfProvinceGroupByLevel23BO> SearchByProvinceGroupByLevel23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvinceGroupByDistrict23BO> SearchByProvinceGroupByDistrict23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvinceGroupByLevel23BO> SearchBySupervisingDeptGroupByLevel23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvinceGroupByLevel23BO> CreateSGDConductStatisticsByLevelSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID);
        List<ConductStatisticsOfProvinceGroupByLevel23BO> CreatePGDConductStatisticsByLevelSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID);
        List<ConductStatisticsOfProvinceGroupByDistrict23BO> CreateSGDConductStatisticsByDistrictSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID);
        List<ConductStatisticsOfProvince23BO> SearchByProvince23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvince23BO> CreateSGDConductStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);
        List<ConductStatisticsOfProvince23BO> SearchBySupervisingDept23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvince23BO> CreatePGDConductStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);
        List<ConductStatisticsOfProvince23BO> CreateSGDConductStatisticsTertiary(IDictionary<string, object> SearchInfo, string InputParameterHashKey, out int FileID);
        List<ConductStatisticsOfProvinceGroupBySubCommittee23> CreateSGDConductStatisticsBySubCommitteeTertiary(IDictionary<string, object> SearchInfo, String InputParameterHashKey, out int ProcessedReportID);
        IQueryable<ConductStatisticsOfProvinceGroupBySubCommittee23> SearchByProvinceGroupBySubCommittee23(IDictionary<string, object> SearchInfo);
        List<ConductStatisticsOfProvinceGroupByDistrict23BO> CreateSGDConductStatisticsByDistrictTertiary(IDictionary<string, object> dic, out int FileID, string inputHash);
        IQueryable<ConductStatistic> Search(IDictionary<string, object> SearchInfo);

        List<ConductStatisticsBO> GetListConductAllSchool(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId);
        List<ConductStatisticsBO> GetListConductAllEdu(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId);
        List<ConductStatisticsBO> GetListConductDistrict(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId);
    }
}