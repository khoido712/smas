/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IApprenticeshipTrainingBusiness
    {
        void InsertApprenticeshipTraining(List<ApprenticeshipTraining> ListApprenticeshipTraining);
        void InsertApprenticeshipTraining(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID,
            List<ApprenticeshipTraining> ListApprenticeshipTraining);
        void UpdateApprenticeshipTraining(List<ApprenticeshipTraining> ListApprenticeshipTraining);
        void DeleteApprenticeshipTraining(int ApprenticeshipTrainingID);
        IQueryable<ApprenticeshipTraining> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        IQueryable<ApprenticeshipTraining> GetListPupilNoStatus(int schoolId, int academicYearId, int apprenticeshipClassId);
        List<ApprenticeshipTrainingBO> GetListPupilApprenticeshipTraining(IDictionary<string, object> dic);
        Stream CreateReportApprenticeshipTraining(int schoolID, int academicYearID, int? apprenticeshipClassID, string pupilCode, string fullName, List<ApprenticeshipTrainingBO> lstPupilID);
        Stream CreateReportApprenticeshipMark(int schoolID, int academicYearID, int apprenticeshipClassID, int semester);
        bool CheckValidatePupilCode(string PupilCode, int ApprenticeshipClassID);
        bool CheckValidateName(string Name, int ApprenticeshipClassID);
    }
}