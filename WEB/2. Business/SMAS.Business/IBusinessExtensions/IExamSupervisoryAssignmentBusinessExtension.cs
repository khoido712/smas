/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using  SMAS.Business.BusinessObject;
using System.IO;
namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamSupervisoryAssignmentBusiness 
    {
        IQueryable<ExamSupervisoryAssignment> Search(IDictionary<string, object> search);
        IQueryable<ExamSupervisoryAssignmentBO> SearchByExamSubject(int academicYearID, int schoolID, long examinationsID, long examGroupID, long subjectID, IDictionary<string, object> options);
        IQueryable<ExamSupervisoryAssignmentBO> AdditionSearch(IDictionary<string, object> search);
        void CheckDuplicateData(ExamSupervisoryAssignment entity,long exceptID);
        void DeleteList(List<ExamSupervisoryAssignment> deleteList);
        void InsertList(List<ExamSupervisoryAssignment> insertList);

        ProcessedReport GetExamSupervisoryAssignReport(IDictionary<string, object> dic);
        Stream CreateExamSupervisoryAssignReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamSupervisoryAssignReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        /// <summary>
        ///  Check du lieu co ton taij khong
        /// </summary>
        /// <param name="subjectIDList"></param>
        /// <param name="examinationsID"></param>
        /// <param name="examGroupID"></param>
        /// <returns></returns>
        bool GetSupervisoryAssign(List<int> subjectIDList, long examinationsID, long examGroupID);
    }
}