/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IAcademicYearOfProvinceBusiness 
    {
        IQueryable<AcademicYearOfProvince> Search(IDictionary<string, object> SearchInfo);
        IQueryable<AcademicYearOfProvince> SearchByProvince(int ProvinceID, IDictionary<string, object> SearchInfo);
        AcademicYearOfProvince Insert(AcademicYearOfProvince entity);
        void Update(AcademicYearOfProvince entity);
        void Delete(int AcademicYearOfProvinceID, int ProvinceID);
        int GetCurrentYear(int ProvinceID, string StandardAcFromDate = null);
        int GetCurrentYearAndSemester(int ProvinceID, out int semester,string StandardAcFromDate = null);
    }
}