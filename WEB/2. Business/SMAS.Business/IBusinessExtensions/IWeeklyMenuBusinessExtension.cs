/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IWeeklyMenuBusiness 
    {
        
        IQueryable<WeeklyMenu> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<WeeklyMenu> SearchNormalMenu(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<WeeklyMenu> SearchAdditionalMenu(int SchoolID, IDictionary<string, object> SearchInfo);
        void InsertNormalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail);
        void InsertAdditionalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail);
        void UpdateNormalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail);
        void UpdateAdditionalMenu(WeeklyMenu entity, List<WeeklyMenuDetail> lstWeeklyMenuDetail);
        void DeleteNormalMenu(int WeeklyMenuID, int SchoolID);
        void DeleteAdditionalMenu(int WeeklyMenuID, int SchoolID);   
    }
}