/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISupplierBusiness 
    {
        void Insert(Supplier supplier);
        void Update(Supplier supplier);
        void Delete(int supplierID, int schoolID);
        IQueryable<Supplier> Search(IDictionary<string, object> dic);
        IQueryable<Supplier> SearchBySchool(int schoolID, IDictionary<string, object> dic);
    }
}