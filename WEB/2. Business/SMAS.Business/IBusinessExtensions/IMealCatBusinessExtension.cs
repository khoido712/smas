/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  Namta
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;


namespace SMAS.Business.IBusiness
{ 
    public partial interface IMealCatBusiness 
    {
        void Insert(MealCat MealCat);
        void Update(MealCat MealCat);
        void Delete(int MealID);
        IQueryable<MealCat> Search(IDictionary<string, object> dic);
        IQueryable<MealCat> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
    }
}