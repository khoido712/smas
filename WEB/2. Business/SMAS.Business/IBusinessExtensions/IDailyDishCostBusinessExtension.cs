/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IDailyDishCostBusiness
    {
        void Insert(DailyDishCost Entity);
        void Update(DailyDishCost Entity, DateTime oldEffectDate);
        void Delete(int DailyDishCostID, int SchoolID);
        IQueryable<DailyDishCost> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        int MaxDailyDishCost(DateTime date, int SchoolID);
    }
}