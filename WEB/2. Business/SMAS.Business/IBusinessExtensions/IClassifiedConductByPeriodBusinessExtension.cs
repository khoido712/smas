﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IClassifiedConductByPeriodBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKey(ClassifiedConductByPeriod entity);

        ProcessedReport GetClassifiedConductByPeriod(ClassifiedConductByPeriod entity);

        ProcessedReport InsertClassifiedConductByPeriod(ClassifiedConductByPeriod entity, Stream data);

        Stream CreateClassifiedConductByPeriod(ClassifiedConductByPeriod entity);

    }
}
