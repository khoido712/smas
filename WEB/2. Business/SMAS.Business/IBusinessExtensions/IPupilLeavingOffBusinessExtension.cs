/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilLeavingOffBusiness
    {
        IQueryable<PupilLeavingOff> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        //IQueryable<PupilLeavingOffBO> Search(IDictionary<string, object> SearchInfo);
        void DeletePupilLeavingOff(int UserID, int PupilLeavingOffID, int SchoolID,int semester);
        void UpdatePupilLeavingOff(int UserID, PupilLeavingOff PupilLeavingOff);
        void InsertPupilLeavingOff(int UserID, PupilLeavingOff PupilLeavingOff);
        List<PupilLeavingOff> GetListPupilLeavingOff(int AcademicYearID, int SchoolID, int Semester);
        List<PupilLeavingOffBO> GetListPupilLeavingOffOfSchool(int AcademicYearID, int SchoolID, int Semester);
        IQueryable<PupilLeavingOffBO> ListPupilLeavingOff(List<int> lstAcademicYearID);
    }
}