﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportSituationOfViolationBusiness
    {
        string GetHashKey(ReportSituationOfViolationBO entity);
        ReportDefinition GetReportDefinitionOfPupilFault(ReportSituationOfViolationBO reportSituationOfViolationBO);
        ProcessedReport ExcelGetPupilFault(ReportSituationOfViolationBO entity);
        ProcessedReport ExcelInsertPupilFault(ReportSituationOfViolationBO entity, Stream data);
        Stream ExcelCreatePupilFault(ReportSituationOfViolationBO entity);
        List<ReportPupilFaultBO> GetListPupilFault(ReportSituationOfViolationBO entity);
        string FaultName(ReportSituationOfViolationBO entity, int pupilID, DateTime? ViolateDate, List<FaultCriteriaBO> lstFaultCriteriaBO, List<FaultCriteria> lstFaultCriteria);
        ReportDefinition GetReportDefinitionOfPupilFaultByClass(ReportSituationOfViolationBO reportSituationOfViolationBO);
        ProcessedReport ExcelGetPupilFaultByClass(ReportSituationOfViolationBO entity);
        ProcessedReport ExcelInsertPupilFaultByClass(ReportSituationOfViolationBO entity, Stream data);
        Stream ExcelCreatePupilFaultByClass(ReportSituationOfViolationBO entity);
    }
}
