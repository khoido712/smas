﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public interface IReportPupilRetestRegistrationBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKey(ReportPupilRetestRegistrationSearchBO entity);

        ProcessedReport GetReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity);

        ProcessedReport InsertReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity, Stream data);

        Stream CreateReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity);

        ProcessedReport GetReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity);

        ProcessedReport InsertReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity, Stream data);

        Stream CreateReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity);
    }
}
