﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportJudgeRecordBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKeyPrimary(ReportJudgeRecordSearchBO entity);

        ProcessedReport GetReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity);

        ProcessedReport InsertReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity, Stream data);

        Stream CreateReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity);
    }
}
