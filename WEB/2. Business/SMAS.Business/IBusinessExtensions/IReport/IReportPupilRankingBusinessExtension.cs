﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportPupilRankingBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKeyPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport GetReportCapacityPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport InsertReportCapacityPrimary(ReportPupilRankingSearchBO entity, Stream data);

        Stream CreateReportCapacityPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport GetReportConductPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport InsertReportConductPrimary(ReportPupilRankingSearchBO entity, Stream data);

        Stream CreateReportConductPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport GetReportFinalResultPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport InsertReportFinalResultPrimary(ReportPupilRankingSearchBO entity, Stream data);

        Stream CreateReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity);

        ProcessedReport GetReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity);

        Stream CreateReportFinalResultPrimary(ReportPupilRankingSearchBO entity);

        ProcessedReport InsertReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity, Stream data);

    }
}
