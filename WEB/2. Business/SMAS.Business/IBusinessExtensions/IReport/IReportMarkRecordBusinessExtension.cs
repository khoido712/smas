﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportMarkRecordBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKeyPrimary(ReportMarkRecordSearchBO entity);

        ProcessedReport GetReportMarkRecordPrimary(ReportMarkRecordSearchBO entity);

        ProcessedReport InsertReportMarkRecordPrimary(ReportMarkRecordSearchBO entity, Stream data);

        Stream CreateReportMarkRecordPrimary(ReportMarkRecordSearchBO entity);

        Stream CreateReportMarkRecordPrimaryTT30(ReportMarkRecordSearchBO entity, bool isMoveHistory);

    }
}
