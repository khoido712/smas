﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportSummedUpRecordBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKeyPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport GetReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport InsertReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity, Stream data);

        Stream CreateReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport GetReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport InsertReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity, Stream data);

        Stream CreateReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport GetReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity);

        ProcessedReport InsertReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity, Stream data);

        Stream CreateReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity);

    }
}
