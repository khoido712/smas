﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportPupilEmulationBusiness : IGenericBussiness<ProcessedReport>
    {
        string GetHashKeyPrimary(ReportPupilEmulationSearchBO entity);

        ProcessedReport GetReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity);

        ProcessedReport InsertReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity, Stream data);

        Stream CreateReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity);

    }
}
