/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IViolatedInvigilatorBusiness 
    {
        List<ViolatedInvigilator> InsertAll(int SchoolID, int AppliedLevel, string ViolationDetail, string DisciplinedForm, List<int> ListInvigilatorAsignmentID);
        ViolatedInvigilator Update(int ViolatedInvigilatorID, int SchoolID, int AppliedLevel, string ViolationDetail, string DisciplinedForm);
        void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListViolatedInvigilatorID);
        IQueryable<ViolatedInvigilator> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        Stream CreateViolatedInvigilatorReport(IDictionary<string, object> dic, out string reportName);
    }
}