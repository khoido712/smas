/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IParticularPupilTreatmentBusiness 
    {
        void InsertParticularPupilTreatment(int UserID, ParticularPupilTreatment entity);
        void UpdateParticularPupilTreatment(int UserID, ParticularPupilTreatment entity);
        void DeleteParticularPupilTreatment(int UserID, int ParticularPupilTreatmentID, int SchoolID);
        IQueryable<ParticularPupilTreatment> SearchParticularPupilTreatment(IDictionary<string, object> dic);
    }
}