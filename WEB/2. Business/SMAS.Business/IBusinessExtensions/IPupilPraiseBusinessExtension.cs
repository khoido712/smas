/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPupilPraiseBusiness 
    {
        void InsertPupilPraise(int UserID, PupilPraise PupilPraise);
        void UpdatePupilPraise(int UserID, PupilPraise PupilPraise);
        void DeletePupilPraise(int UserID, int PupilPraiseID, int SchoolID);
        //IQueryable<PupilPraiseBO> Search(IDictionary<string, object> SearchInfo);
        IQueryable<PupilPraise> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
    }
}