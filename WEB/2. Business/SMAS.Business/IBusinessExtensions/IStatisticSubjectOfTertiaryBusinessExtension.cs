/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IStatisticSubjectOfTertiaryBusiness 
    {
        IQueryable<StatisticSubjectOfTertiary> Search(IDictionary<string, object> dic);
        IQueryable<StatisticSubjectOfTertiary> SearchBySchool(int SchoolID,IDictionary<string, object> dic = null);
        void InsertOrUpdate(int SchoolID, int AcademicYearID, List<StatisticSubjectOfTertiary> lstStatisticSubject);
    }
}