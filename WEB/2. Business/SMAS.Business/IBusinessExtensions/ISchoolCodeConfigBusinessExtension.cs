﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ISchoolCodeConfigBusiness
    {
        IQueryable<SchoolCodeConfig> Search(IDictionary<string, object> dic);
        void Delete(int CodeConfigID);
    }
}
