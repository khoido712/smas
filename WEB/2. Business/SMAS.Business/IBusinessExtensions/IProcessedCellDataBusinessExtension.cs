/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IProcessedCellDataBusiness
    {
        int? GetYear(int AcademicYearID);
        List<ProcessedCellData> GetListReportEmis(int SchoolID, int Year, string ReportCode);
        string GetProvinceName(int ProvinceID);
        int GetEducationGrade(int schoolID);
        ProcessedReport GetProcessedReport(ProcessedCellData entity);
        ProcessedReport InsertProcessCellData(ProcessedCellData entity, Stream Data, string ReportCode, string CodeDefinition);
        void InsertOrUpdateEmisRequest(int SchoolID, int AcademicYearID, int Year, int period, int trainingType, int districtID, int provinceID, string reportCode);
        EmisRequest GetEmisRequest(int SchoolID, int AcademicYearID, string reportCode, int period);
        List<PhysicalFacilitiesBO> GetListDataFacilities(int schoolId, int year, int period, int provinceId, string reportCode);
        void RequestGraduationReport(int SchoolID, int AcademicYearID, string ReportCode);
        int GetStatusGraduationReport(int SchoolID, int AcademicYearID, string reportCode);
        void InsertOrUpdateQD5363Request(int SchoolID, int AppliedLevel, int AcademicYearID, int Year, int period, int trainingType, int districtID, int provinceID, string reportCode);
        int GetStatusRequest(int SchoolID, int AcademicYearID, string reportCode, int period, int appliedLevel = 0);
        IDictionary<string, object> GetListReportEmisAsDic(int SchoolID, int Year, string ReportCode, string SheetName);
        void InsertOrUpdateQD5363ForSupervisingDeptRequest(int? ProvinceID, int? DistrictID, int AppliedLevel, int Year, int period, string reportCode);
        int GetStatusRequestForSupervisingDept(int? ProvinceID, int? DistrictID, int Year, string reportCode, int period, int appliedLevel);
        List<ProcessedCellData> GetListReportEmisForSupervisingDept(int? ProvinceID, int? DistrictID, int Year, string ReportCode);
    }
}