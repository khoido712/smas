/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessExtensions;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IHonourAchivementBusiness 
    {
        //HonourAchivement Insert(HonourAchivement HonourAchivement);
        //HonourAchivement Update(HonourAchivement HonourAchivement);
        //void Delete(int Id);
        //IQueryable<HonourAchivementBO> Search(IDictionary<string, object> dic,int Type);

        //dungnt add
        IQueryable<HonourAchivement> Search(IDictionary<string, object> dic);
         void Validate(HonourAchivement checker, int type);
        HonourAchivement InsertHonourAchivementForEmployee(HonourAchivement HonourAchivement);
        HonourAchivement UpdateHonourAchivementForEmployee(HonourAchivement HonourAchivement);
        void DeleteHonourAchivementForEmployee(int SchoolID,int HonourAchivementForEmployeeID);
        IQueryable<HonourAchivement> SearchHonourAchivementForEmployee(int SchoolID,IDictionary<string, object> dic=null);

        HonourAchivement InsertHonourAchivementForFaculty(HonourAchivement HonourAchivement);
        HonourAchivement UpdateHonourAchivementForFaculty(HonourAchivement HonourAchivement);
        void DeleteHonourAchivementForFaculty(int SchoolID, int HonourAchivementForEmployeeID);
        IQueryable<HonourAchivement> SearchHonourAchivementForFaculty(int SchoolID,IDictionary<string, object> dic=null);

        
    }
}