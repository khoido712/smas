/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamPupilBusiness 
    {
        IQueryable<ExamPupil> Search(IDictionary<string, object> search);
        IQueryable<ExamPupilBO> Search(IDictionary<string, object> dic, int academicYearID, int patitionID);
        IQueryable<ExamPupil> GetExamPupilOfExaminations(long? examinationsID, int schoolID, int academicYearID, int patitionID);
        IQueryable<ExamPupil> GetExamPupilOfExamGroup(long? examinationsID,long? examGroupID, int schoolID, int academicYearID, int partitionID);
        IQueryable<ExamPupil> GetExamPupilOfRoom(long? examinationsID, long? examGroupID, long? examRoomID, int schoolID, int academicYearID, int partitionID);
        List<long> ExamPupilInUsed(List<long> inputExamPupilID, long examinationsID, int partition);
        void DeleteList(List<long> inputExamPupilID);
        void InsertList(List<ExamPupil> insertList);
        void UpdateList(List<ExamPupil> updateList);
        void UpdateExamineeCode(List<ExamPupil> lstExamineeCode);
        IQueryable<ExamPupil> GetGroupedPupilWithSameSubject(List<int> listPupilID, List<int> listSubjectID, long examinationsID, int partition);
        IQueryable<ExamPupilBO> GetListExamineeNumber(IDictionary<string, object> search);
        List<ExamPupilBO> GetListExamPupilOrderBy(IDictionary<string, object> search);
        void UpdateExamineeCodeName(IDictionary<string, object> search, string[] Criteria, int checkAll, string textCriteriaII, string textCriteriaIII);
        void UpdateExamineeCodeNameCustom(IDictionary<string, object> search, List<int> lstCheckCriteria, List<int> lstOrder,
            string textCriteriaIII, string textCriteriaIV, int checkAll, int usingEXCode);
        void UpdateExamineeCodeByRoom(IDictionary<string, object> search, string[] Criteria,
            int checkAll, string textCriteriaII, string textCriteriaIII);
        void DeleteCheckCodeName(IDictionary<string, object> search, List<long> listExamPupil);
        IQueryable<ExamPupilBO> GetExamPupilWithSubject(long? examinationsID, int schoolID, int academicYearID, int patitionID);

        ProcessedReport GetExamPupilByRoomReport(IDictionary<string, object> dic);
        Stream CreateExamPupilByRoomReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamPupilByRoomReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamPupilByClassReport(IDictionary<string, object> dic);
        Stream CreateExamPupilByClassReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamPupilByClassReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamPupilAttendanceReport(IDictionary<string, object> dic);
        Stream CreateExamPupilAttendanceReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamPupilAttendanceReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetTicketTakeExamPaperReport(IDictionary<string, object> dic);
        ProcessedReport InsertTicketTakeExamPaperReportZip(IDictionary<string, object> diczip, Stream data, int appliedLevel, string subjecbName);
        ProcessedReport InsertTicketTakeExamPaperReport(IDictionary<string, object> dic, Stream data, int appliedLevel, string subjecbName);

        ProcessedReport GetPaperContestReport(IDictionary<string, object> dic);
        Stream CreatePaperContestReport(IDictionary<string, object> dic);
        ProcessedReport InsertPaperContestReport(IDictionary<string, object> dic, Stream data, string reportCode, bool isZip = false);
        void DownloadZipFilePagerContest(IDictionary<string, object> dic, out string folderSaveFile);
    }
}