/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPupilRetestRegistrationBusiness 
    {
        void InsertPupilRetestRegistration(int SchoolID, int AcademicYearID, List<PupilRetestRegistration> ListPupilRetestRegistration);
        void UpdatePupilRetestRegistration(int SchoolID, int AcademicYearID, List<PupilRetestRegistration> ListPupilRetestRegistration);
        void DeletePupilRetestRegistration(int PupilRetestRegistrationID, int SchoolID);
        IQueryable<PupilRetestRegistration> SearchPupilRetestRegistration(IDictionary<string, object> dic);
        IQueryable<PupilRetestRegistrationSubjectBO> GetListPupilRetestRegistrationBySubject(IDictionary<string, object> dic);
        IQueryable<PupilRetestRegistrationSubjectBO> GetListPupilRetestExportSumManyMark(IDictionary<string, object> dic);
        IQueryable<PupilRetestToCategoryBO> GetListPupilRetestToCategory(IDictionary<string, object> dic);
        IQueryable<PupilPrimaryRetestToCategoryBO> GetPupilPrimaryRetestToCategory(IDictionary<string, object> dic);
        List<int> SearchSubjectRetest(IDictionary<string, object> dic);
        IQueryable<PupilRetestRegistration> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        int CountNumberRetest(int PupilID, int SchoolID, int AcademicYearID);
        bool CheckNumberOfRetestSecondary(int AcademicYearID, int SchoolID, int ClassID, int PupilID);
        bool CheckNumberOfRetest(int AcademicYearID, int SchoolID, int ClassID, int PupilID);
        
        List<PupilRetestToCategory> CategoryPupilAfterRetest(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID);
        List<PupilRetestHistoryToCategory> CategoryPupilAfterRetestHistory(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID);
        List<PupilRetestToCategory> CategoryPupilAfterRetest_History(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID,bool? isVNEN = false);

        ProcessedReport InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO entity, Stream data);
        string GetHashKey(PupilRetestRegistrationBO entity);
        List<PupilRetestRegistrationBO> GetListPupilRetestRegistration(int AcademicYearID, int SubjectID, int Semester, int SChoolID);
        Stream CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO entity);
        ProcessedReport GetPupilRetest(PupilRetestRegistrationBO entity);

        //IQueryable<PupilRetestRegistrationSubjectBO> GetListPupilRetestRegistrationBySubject_History(IDictionary<string, object> dic);
    }
}
