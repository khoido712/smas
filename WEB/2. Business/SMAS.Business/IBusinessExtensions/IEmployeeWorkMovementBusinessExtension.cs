/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeeWorkMovementBusiness 
    {
        void DeleteMovement(int SchoolID,int EmployeeWorkMovementID);
		IQueryable<EmployeeWorkMovement> Search(IDictionary<string, object> dic);
        IQueryable<EmployeeWorkMovement> SearchBySchool(IDictionary<string, object> dic, int SchoolID);
        IQueryable<EmployeeWorkMovement> SearchRetire(IDictionary<string, object> dic,int SchoolID);
        void DeleteRetire(int EmployeeWorkMovementID, int SchoolID);
        EmployeeWorkMovement UpdateRetire(EmployeeWorkMovement employeeWorkMovement, int SchoolID);
        EmployeeWorkMovement InsertRetire(EmployeeWorkMovement employeeWorkMovement);
        IQueryable<EmployeeWorkMovement> SearchMovement(IDictionary<string, object> SearchInfo, int SchoolID);
        EmployeeWorkMovement InsertMovementToSchoolWithinSystem(EmployeeWorkMovement EmployeeWorkMovement);
        EmployeeWorkMovement InsertMovementToOtherOrganization(EmployeeWorkMovement EmployeeWorkMovement);
        EmployeeWorkMovement UpdateMovementToSchoolWithinSystem(EmployeeWorkMovement EmployeeWorkMovement);
        EmployeeWorkMovement UpdateMovementToOtherOrganization(EmployeeWorkMovement EmployeeWorkMovement);
        EmployeeWorkMovement InsertMovement(EmployeeWorkMovement employeeWorkMovement);
        EmployeeWorkMovement UpdateMovement(EmployeeWorkMovement employeeWorkMovement);

        IQueryable<EmployeeWorkMovement> SearchSeverance(IDictionary<string, object> dic, int SchoolID);
        void DeleteSeverance(int EmployeeWorkMovementID, int SchoolID);
        EmployeeWorkMovement UpdateSeverance(EmployeeWorkMovement employeeWorkMovement, int SchoolID);
        EmployeeWorkMovement InsertSeverance(EmployeeWorkMovement employeeWorkMovement);

    }
}