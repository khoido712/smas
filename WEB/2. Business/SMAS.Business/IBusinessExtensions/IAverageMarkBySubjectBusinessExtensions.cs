﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SMAS.Business.BusinessObject;
using System.Web;
using SMAS.Models.Models;
using System.IO;
namespace SMAS.Business.IBusiness
{
    public partial interface IAverageMarkBySubjectBusiness
    {

        string GetHashKey(AverageMarkBySubjectBO entity);
        ProcessedReport GetAverageMarkBySubject(AverageMarkBySubjectBO entity);
        ProcessedReport InsertAverageMarkBySubject(AverageMarkBySubjectBO entity, Stream data);
        Stream CreateAverageMarkBySubject(AverageMarkBySubjectBO entity);
        ProcessedReport InsertAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity, Stream data);
        Stream CreateAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity);
        ProcessedReport GetAverageMarkBySubjectJudge(AverageMarkBySubjectBO entity);
        ProcessedReport GetAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity);
        Stream CreateAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity);
        ProcessedReport InsertAverageMarkBySubjectAndTeacher(AverageMarkBySubjectBO entity, Stream data);
        ProcessedReport GetAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity);
        Stream CreateAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity);
        ProcessedReport InsertAverageMarkBySubjectAndTeacherJudge(AverageMarkBySubjectBO entity, Stream data);
        Stream CreateAverageMarkBySubjectAndTeacherAll(AverageMarkBySubjectBO entity);
    }
}
