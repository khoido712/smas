/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISkillOfClassBusiness 
    {
        IQueryable<SkillOfClass> Search(IDictionary<string, object> dic);
        void Insert(List<SkillOfClass> lstSkill);
        void Delete(List<int> lsSkillOfClassID);
        
    }
}