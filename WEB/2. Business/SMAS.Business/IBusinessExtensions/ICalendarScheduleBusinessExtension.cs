﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Business;

namespace SMAS.Business.IBusiness
{
    public partial interface ICalendarScheduleBusiness
    {
        IQueryable<CalendarSchedule> Search(IDictionary<string, object> dic);
        CalendarSchedule InsertOrUpdate(CalendarSchedule calendar);
        void GetSection(int seperateID, ref List<CalendarScheduleBusiness.ClassSection> _lst);
    }
}
