﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ITranscriptOfPrimaryClassBusiness
    {
        string GetHashKeyForTranscriptOfPrimaryClass(TranscriptOfClass Entity);
        ProcessedReport InsertTranscriptOfPrimaryClass(TranscriptOfClass Entity, Stream Data);
        ProcessedReport GetTranscriptOfPrimaryClass(TranscriptOfClass Entity);
        Stream CreateTranscriptOfPrimaryClass(TranscriptOfClass Entity);
        string GetHashKeyForSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity);
        ProcessedReport InsertSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity, Stream Data);
        ProcessedReport GetSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity);
        Stream CreateSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity);
    }
}
