﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IManagementHealthInsuranceBusiness
    {
        IQueryable<ManagementHealthInsurance> Search(Dictionary<string, object> dic);
        void Create(ManagementHealthInsurance objMDI);
        void SaveImport(int schoolID, int academicID, int appliLevelID, List<ManagementHealthInsurance> lstMHI);
        void UpdateDB(ManagementHealthInsurance objMDI);
    }
}
