
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IMobileBusiness 
    {
        Conversation GetListConversation(int schoolId, int employeeId, string content, int page, int pageSize);
        List<ContactGroupBO> GetListContactGroup(int schoolID, int role, int employeeId);
        SendSMSSORespone SendSMS(string mobile, string content, int historyTypeID);

        Result SendSMSToGroup(int schoolId, int academicYearId, int contactGroupId, int role, int teacherId, string content, int grade, string schoolName);
        MessageOfConversation GetListConversationDetail(int schoolId, int employeeId, int senderId, int page, int pageSize);

        MessageOfConversation GetMessagesOfGroup(int schoolId, int contactGroupId, int page, int pageSize);
        List<SendSMSToParentBO> GetMessageList(int schoolId, int academicYearId, int year, int classId, int educationLevel, int appliedLevel, int semester, int type, int option, DateTime? fromDate, DateTime? toDate);
        MSendSMSResult SendSMSToParent(MSendSmsRequest data);
        ChatMessageBO InsertChatMessageData(int ChatGroupID, int UserAccountSendID, string ContentMessage);
        List<ChatMessageBO> GetListChatMessage(int ChatGroupID, int UserAccountSendID, int PageIndex, int PageSize);
        bool DeleteChatMessageInGroupByID(int GroupID, List<string> ListmessID);
    }
}