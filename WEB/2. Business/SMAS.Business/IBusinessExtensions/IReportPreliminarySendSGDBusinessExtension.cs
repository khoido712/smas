﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportPreliminarySendSGDBusiness
    {
        string GetHashKey(ReportPreliminaryBO entity);
        Stream ExcelCreateReportPreliminarySendSGD(ReportPreliminaryBO entity);
        ProcessedReport ExcelInsertReportPreliminarySendSGD(ReportPreliminaryBO entity, Stream data);
    }
}
