/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilRankingBusiness
    {
        //void RankingPupil(int UserID, List<PupilRanking> lstPupilRanking);
        void RankingPupilConduct(int UserID, List<PupilRanking> lstPupilRanking, int Semester, bool IsSecondSemesterToSemesterAll);
        /// <summary>
        /// Lay thong tin hoc luc, hanh kiem. isShowRetetResult=true: Hien thi ket qua thi lai
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="isShowReset"></param>
        /// <returns></returns>
        IQueryable<PupilRankingBO> GetPupilToRank(IDictionary<string, object> dic, bool isShowRetetResult);
        List<PupilRankingBO> GetPupilToRankPrimary(IDictionary<string, object> dic);
        //List<PupilRankingBO> GetListPupilToRankConduct(int AcademicYearID, int SchoolID, int ClassID, int Semester);
        Dictionary<int, int?> CategoryOfPupil(int ClassID, List<int> listPupilID);
        Dictionary<int, int?> CategoryOfPupilAfterRetest(int ClassID, List<PupilRankingBO> listPupilID);
        List<PupilRankingBO> GetPupilRetest(IDictionary<string, object> dic);
        List<PupilRankingBO> GetPupilConduct(IDictionary<string, object> dic);
        List<PupilRankingBO> GetPupilConductRetest(IDictionary<string, object> dic);
        List<PupilOfClassRanking> TransferData(IDictionary<string, object> dic);
        void Rank(int RankingCriteria, List<PupilRanking> lstPupilRanking, bool isValidate = true);
        List<PupilRankingBO> GetPupilToRankConductByCapacity(int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);
        List<PupilRankingBO> GetPupilToRankConductByFault(int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);
        IQueryable<PupilOfClassRanking> GetPupilRankingOfClass(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID);
        //int StudyingJudgementOfPupilPrimary(int PupilID, int ClassID, int Semester);
        /// <summary>
        /// Thuc hien tong ket diem cho lop. isShowRetetResult=true: Hien thi ket qua thi lai
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <param name="lstSubject"></param>
        /// <param name="isShowRetetResult"></param>
        /// <param name="AutoMark"></param>
        /// <param name="lstRegisterSubjectBO"></param>
        void RankingPupilOfClass(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject,bool isShowRetetResult, string AutoMark,List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        void RankingPupilOfClassNotRank(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isPassRetest = false,List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        IQueryable<PupilRanking> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        void ClassifyPupilPrimary(List<PupilRankingBO> lstPupilRankingBO);
     /// <summary>
        /// /// Xep loai hoc sinh.isShowRetetResult=true: Lay ket qua thi lai de xet.
        /// \r\n. auto: de danh dau thuc hien bang tool tu dong hay chuc nang. 
     /// </summary>
     /// <param name="lstPupilRankingBO"></param>
     /// <param name="Semester"></param>
     /// <param name="isShowRetetResult"></param>
     /// <param name="auto"></param>
        void ClassifyPupil(List<PupilRankingBO> lstPupilRankingBO, int Semester, bool isShowRetetResult, string auto);
        List<PupilRankingBO> GetPupilToRankPrimaryAfterRetest(IDictionary<string, object> dic);
        void ClassifyPupilPrimaryAfterRetest(List<PupilRankingBO> lstPupilRankingBO, int SchoolID, int AcademicYearID);
        List<PupilRankingBO> GetPupilToRankAfterRetest(IDictionary<string, object> dic);
        void ClassifyPupilAfterRetest(List<PupilRankingBO> lstPupilRankingBO, List<PupilRetestRegistration> lstPupilRetest, int SchoolID, int AppliedLevel, int AcademicYearID);
        void RankingPupilRetest(int UserID, List<PupilRetestToCategory> lstPupilRankingBO,List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        //void RankingPupilRetest(int UserID, List<PupilRetestHistoryToCategory> lstPupilRankingBO, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        decimal? CaculatorAverageSubject(List<SummedUpRecord> ListSummedUpRecord, List<ClassSubject> lstSubject, int Semester,List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        bool IsAppliedTypeSubject(List<ClassSubject> lstClassSubject, int ClassID, int SubjectID);

        //IQueryable<PupilRankingBO> GetListPupilReward(IDictionary<string, object> dic);
        List<PupilRankingBO> GetListPupilRetest(IDictionary<string, object> dic);
        Stream CreatePupilToRankConductByFaultReport(int appliedLevel, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, out string reportName);
        Stream CreatePupilToRankConductByCapacityReport(int appliedLevel, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, out string reportName);

        List<PupilForwardingBO> GetPupilUpClass(int EducationLevelID, int AcademicYearID, List<int> lstClassID);
        /// <summary>
        /// Thuc hien xep hang cho lop.
        /// da co ca luong history
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="Period"></param>
        /// <returns></returns>
        int RankClass(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? Period, bool isShowRetetResult, string auto);

        List<PupilRankingBO> GetPupilToRankPrimaryv2(IDictionary<string, object> dic);
        void ClassifyPupilPrimaryv2(List<PupilRankingBO> lstPupilRankingBO, int Semester);
        IQueryable<PupilRankingBO> GetPupilToSaveSummedUpClass(IDictionary<string, object> dic);

        ProcessedReport GetProcessedReport(PupilRanking entity, string ReportCode);
        ProcessedReport InsertProcessReport(PupilRanking entity, Stream Data, string ReportCode);
        List<PupilRepeaterBO> GetListPupilRepeater(int Grade, int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID,bool? isVNEN = false);
        int EducationGrade(int schoolID);

        /// <summary>
        /// Lay xep loai hoc luc chi theo mon tinh diem va chua xet den nang bac
        /// </summary>
        /// <param name="averageMark"></param>
        /// <param name="lstMandatoryMark"></param>
        /// <param name="lstClassSubject"></param>
        /// <param name="isGDTX"></param>
        /// <param name="lstRegisterSubjectBO"></param>
        /// <returns></returns>
        int GetCapacityTypeByOnlyMark(decimal? averageMark, List<SummedUpRecord> lstMandatoryMark, List<ClassSubject> lstClassSubject, bool isGDTX, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO);
        /// <summary>
        /// Lay xep loai HL cua hoc sinh theo diem
        /// </summary>
        /// <param name="averageMark"></param>
        /// <param name="ListSummedUpRecord"></param>
        /// <param name="lstClassSubject"></param>
        /// <param name="schoolID"></param>
        /// <param name="traningType"></param>
        /// <param name="lstRegisterSubjectBO"></param>
        /// <returns></returns>
        int GetCapacityTypeByMark(decimal? averageMark, List<SummedUpRecord> ListSummedUpRecord, List<ClassSubject> lstClassSubject, int schoolID, TrainingType traningType, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO);

        /// <summary>
        /// Xet nang bac hoc luc cho hoc sinh
        /// </summary>
        /// <param name="capacityID"></param>
        /// <param name="lowestCapacityID"></param>
        /// <param name="isGDTX"></param>
        /// <returns></returns>
        int GetUpCapacity(int capacityID, int lowestCapacityID, bool isGDTX);
        IQueryable<PupilOfClassRanking> GetPupilRankingByClassID(List<int> lstClassID, int AcademicYearID, int SchoolID, int Semester, int? PeriodID);
        List<PupilMarkBO> GetListTraningInfoToday(int SchoolID, int AcademicYearID, List<int> lstClassID, List<int> lstPupilID);
        List<PupilMarkBO> GetListTraningInfoByTime(int SchoolID, int AcademicYearID, List<int> lstClassID, List<int> lstPupilID, DateTime startTime, DateTime endTime);
    }
}
