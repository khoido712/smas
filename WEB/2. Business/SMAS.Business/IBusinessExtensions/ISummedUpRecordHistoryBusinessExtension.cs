/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISummedUpRecordHistoryBusiness 
    {
        void DeleteSummedUpRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        IQueryable<SummedUpRecordHistory> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<SummedUpRecordHistory> SearchSummedUpHistory(IDictionary<string, object> dic);
        void DeleteSummedUpRecordPrimaryHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        IQueryable<SummedUpRecordBO> GetSummedUpRecordHistory(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID, bool isShowRetetResutl);
        void InsertOrUpdateSummedUpRecordAfterRetest(int SchoolID, int AcademicYearID, IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest);
    }
}
