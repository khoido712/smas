/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilDisciplineBusiness
    {
        //IQueryable<PupilDiscipline> SearchPupilDiscipline(IDictionary<string, object> dic);
        IQueryable<PupilDiscipline> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null);
        void DeletePupilDiscipline(int UserID, int PupilDisciplineID, int SchoolID);
        void InsertPupilDiscipline(int UserID, PupilDiscipline pupilDiscipline);
        void UpdatePupilDiscipline(int UserID, PupilDiscipline pupilDiscipline);
    }
}