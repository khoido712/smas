/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IApprenticeshipTrainingAbsenceBusiness 
    {
        ApprenticeshipTrainingAbsence InsertApprenticeshipTrainingAbsence(ApprenticeshipTrainingAbsence Insert);
        ApprenticeshipTrainingAbsence UpdateApprenticeshipTrainingAbsence(ApprenticeshipTrainingAbsence Update);
        void DeleteApprenticeshipTrainingAbsence(int ApprenticeshipTrainingAbsenceID);
        IQueryable<ApprenticeshipTrainingAbsence> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        string GetHashKey(ApprenticeshipTrainingAbsence ApprenticeshipTrainingAbsence);
        int CreatExcelApprenticeshipTrainingAbsence(string InputParameterHashKey, IDictionary<string, object> SearchInfo);
        
    }
}