/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IGroupCatBusiness 
    {
        void DeleteGroupCat(int id);
        IQueryable<GroupCat> GetGroupByRoleAndCreatedUser(int roleID, int createdUserId);
        IQueryable<GroupCat> GetGroupByRoleAndAdminID(int roleID, int AdminID);
        bool CheckPrivilegeToCreateGroup(int UserAccountID);
        bool CreateGroup(int UserAccountID, GroupCat group, int RoleID);
        IQueryable<GroupCat> GetGroupsOfRole(int RoleID);
        int GetRoleOfGroup(int GroupID);
        IQueryable<GroupCat> GetAllActive();
        void InsertGroup(GroupCat model);
        void Delete(int id);
        IQueryable<GroupCat> Search(IDictionary<string, object> dic);
        GroupCat AssignMenu(int GroupCatID, Dictionary<int, int?> ListPermissionMenu);
    }
}
