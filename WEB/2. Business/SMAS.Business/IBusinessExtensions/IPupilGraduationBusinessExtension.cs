/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPupilGraduationBusiness 
    {

        void InsertPupilGraduation(List<PupilGraduation> listPupilGraduation);
        void UpdatePupilGraduation(List<PupilGraduation> listPupilGraduation);
        void DeletePupilGraduation(int UserID, PupilGraduation PupilGraduation);
        void DeletePupilGraduation(List<PupilGraduation> listPupilGraduation);
       // IQueryable<PupilGraduation> Search(IDictionary<string, object> dic);
        IQueryable<PupilGraduation> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        List<PupilGraduationBO> GetApprovePupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic);
        List<PupilGraduationBO> GetGraduationPupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic);
        List<PupilGraduationBO> SearchPupilGraduation(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> pocDic);
    }
}