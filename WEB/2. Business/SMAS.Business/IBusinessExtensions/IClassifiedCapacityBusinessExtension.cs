﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SMAS.Business.BusinessObject;
using System.Web;
using SMAS.Models.Models;
using System.IO;
namespace SMAS.Business.IBusiness
{
    public partial interface IClassifiedCapacityBusiness
    {

        string GetHashKey(ClassifiedCapacityBO entity);
        ProcessedReport GetClassifiedCapacity(ClassifiedCapacityBO entity);
        ProcessedReport InsertClassifiedCapacity(ClassifiedCapacityBO entity, Stream data);
        Stream CreateClassifiedCapacity(ClassifiedCapacityBO entity);
    }
}
