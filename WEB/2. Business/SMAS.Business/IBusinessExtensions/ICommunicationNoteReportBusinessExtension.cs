﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ICommunicationNoteReportBusiness
    {

        ProcessedReport GetCommunicationNoteReport(IDictionary<string, object> dic);
        Stream CreateCommunicationNoteReport(IDictionary<string, object> dic);
        ProcessedReport InsertCommunicationNoteReport(IDictionary<string, object> dic, Stream data);

        ProcessedReport GetVnenCommunicationNoteReport(IDictionary<string, object> dic);
        Stream CreateVnenCommunicationNoteReport(IDictionary<string, object> dic);
        Stream CreateVNENCommunicationNoteMonthReport(IDictionary<string, object> dic);
        ProcessedReport InsertVnenCommunicationNoteReport(IDictionary<string, object> dic, Stream data);
    }
}
