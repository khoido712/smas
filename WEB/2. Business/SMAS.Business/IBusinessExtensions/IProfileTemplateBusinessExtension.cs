/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IProfileTemplateBusiness 
    {
        IQueryable<ProfileTemplate> Search(IDictionary<string, object> dic);
        List<ProfileTemplate> GetTemplateBySchool(int schoolId, int templateId);
        DataTable GetEmployeeInfo(int schoolId, int academicYearId);
        System.IO.Stream ExportEmployeeInfo(int schoolId, int academicYearId, int templateId, int startRow);
        System.IO.Stream ExportPupilInfo(int schoolId, int academicYearId, int templateId, int startRow, List<int> lstReportTypeID, int AppliedLevelID, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID);
        Stream ExportInfoSchool(IDictionary<string, object> dic);
    }
}