/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEvaluationDevelopmentBusiness 
    {
        IQueryable<EvaluationDevelopment> Search(IDictionary<string, object> dic);
        List<EvaluationDevelopment> GetListEvaluationDevelopment(IDictionary<string, object> dic);
        List<EvaluationDevelopment> GetListEvaluationDevelopmentByClass(IDictionary<string, object> dic, int? ClassID);
        void InsertEval(EvaluationDevelopment objInput);
        void UpdateEval(EvaluationDevelopment objInput, List<EvaluationDevelopment> lstDB);
        void DeleteEval(int EvaluationDevelopmentID);
        void Delete(int EvaluationDevelopmentID);      
        void UpdateOrderID(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB);
        IQueryable<EvaluationDevelopment> GetListByAppliedLevelAndProvince(int AppliedLevel, int ProvinceID);
        void InsertOrUpdateMulti(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB,  List<int> lstCheckEvaluationID, List<int> lstUncheckEvaluationID);
        void InsertOrUpdateMultiFromExcel(List<EvaluationDevelopment> lstInput, List<EvaluationDevelopment> lstDB);
        void InsertBySchool(List<EvaluationDevelopment> lstEDI, List<DeclareEvaluationIndex> lstDEI, Dictionary<string, object> dic);
        void DeleteBySchool(List<DeclareEvaluationIndex> lstDEI, Dictionary<string, object> dic);
        void ImportBySchool(List<EvaluationDevelopmentBO> lstData, Dictionary<string, object> dic);
    }
}