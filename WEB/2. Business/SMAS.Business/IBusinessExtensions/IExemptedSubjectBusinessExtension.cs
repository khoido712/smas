/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExemptedSubjectBusiness 
    {
        void InsertExemptedSubject(int UserID, int PupilID, int ClassID, int ExemptedOjectID, List<ExemptedSubject> lstExemptedSubject);
        void UpdateExemptedSubject(int UserID, int PupilID, int ClassID, int ExemptedOjectID, List<ExemptedSubject> lstExemptedSubject, ref ActionAuditDataBO objAC);
        void DeleteExemptedSubject(int UserID, int ExemptedSubjectID, int SchoolID);
        IQueryable<ExemptedSubject> Search(IDictionary<string, object> dic);
        IQueryable<ExemptedSubject> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        bool IsExemptedSubject(int PupilID, int ClassID, int AcademicYearID, int SubjectID, int Semester);
        List<PupilProfile> GetPupilExemptedSubject(int schoolID, int classID);
        IQueryable<ExemptedSubject> GetListExemptedSubject(int ClassID, int Semester = 0);
        IQueryable<ExemptedSubject> GetListExemptedSubject(IEnumerable<int> lstClassID, int Semester);
        IQueryable<ExemptedSubject> GetListExemptedSubjectBySchoolID(int SchoolID, int AcademicYearID, int SubjectID, int Semester = 0);
        /// <summary>
        /// Lay danh sach hoc sinh mien giam trong hoc ky
        /// </summary>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="subjectId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        List<ExemptedSubject> GetExmpSubjectbyAcademicYear(int academicYearId, int semesterId, int subjectId = 0, int classId = 0);
    }
}
