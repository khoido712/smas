/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExaminationBusiness 
    {
         Examination Insert(Examination insertExamination);
         Examination Update(Examination insertExamination);
         void Delete(int SchoolID, int AppliedLevel, int ExaminationID);
         bool HasExaminationSubject(int ExaminationID);
         bool HasCandidate(int ExaminationID);
         bool HasSeparatedCandidateInSubject(int ExaminationID);
         bool HasCandidateFromMultipleLevel(int ExaminationID);
         IQueryable<Examination> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
         Examination SetStage(int SchoolID, int AppliedLevel, int ExaminationID, int CurrentStage);
         List<ExamSubjectBO> getExamScheduleInfo(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int examID);
         List<ExamMarkPupilBO> getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int examID);
         List<ExaminationsBO> GetExamOfSchool(IDictionary<string, object> dic);
    }
}
