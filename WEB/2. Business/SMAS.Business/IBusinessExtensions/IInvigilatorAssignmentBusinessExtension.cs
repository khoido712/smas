/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IInvigilatorAssignmentBusiness 
    {
        IQueryable<InvigilatorAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        void AddInvigilatorForRoom(int SchoolID, int AppliedLevel, int RoomID, int InvigilatorID);
        void AutoAssign(int SchoolID, int AppliedLevel, int ExaminationID, int InvigilatorPerRoom);
        void SwatchInvigilator(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID1, int InvigilatorAssignmentID2);
        void ChangeInvigilator(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID, int InvigilatorID);
        void Delete(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID);
    }
}
