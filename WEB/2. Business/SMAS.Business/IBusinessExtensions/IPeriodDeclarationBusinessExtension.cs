/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPeriodDeclarationBusiness 
    {
        void Insert(PeriodDeclaration insertPeriodDeclaration);
        void Update(PeriodDeclaration insertPeriodDeclaration, bool checkChange);
        
        IQueryable<PeriodDeclaration> Search(IDictionary<string, object> dic);
        List<PeriodDeclaration> SearchBySemester(IDictionary<string, object> dic);
        IQueryable<PeriodDeclaration> SearchBySchool(int SchoolID,IDictionary<string, object> dic);
        void Delete(int id, int AcademicYearID,int SchoolID);
        // <summary>
        /// Quanglm1
        /// Khoa dot hoac mo khoa dot, cap nhat lai thong tin khoa con diem
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        void LockPeriod(int SchoolID, int AcademicYearID, int Semester, int PeriodID);
    }
}