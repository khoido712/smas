/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IPhysicalTestBusiness
    {
        void Delete(int PhysicalTestID, int SchoolID);
        void Update(PhysicalTest PhysicalTest, MonitoringBook MonitoringBook);
        void Insert(PhysicalTest PhysicalTest, MonitoringBook MonitoringBook);
        IQueryable<PhysicalTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        List<PhysicalTest> GetListPhysicalTest(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, string FullName, int HealthPeriodID
            , int PhysicalClassification, int Nutrition, decimal HeightMin, decimal HeightMax, decimal WeightMin, decimal WeightMax, int EducationGrade, int PageSize, int PageNum, ref int toltalRecord, int SemesterId);
        IQueryable<PhysicalTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID);
        ResultBO InsertOrUpdatePhysicalTest(List<MonthlyGrowthBO> lstMonthlyGrowthBO, IDictionary<string, object> dic);
        int CheckNutrition(ClassificationBO objClassificationBO);
        List<PhysicalTest> GetListPhysicalTestExport(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, string FullName, int HealthPeriodID
            , int PhysicalClassification, int Nutrition, decimal HeightMin, decimal HeightMax, decimal WeightMin, decimal WeightMax, int EducationGrade, int SemesterId);
        List<PhysicalTest> ListPhysicalTestByMonitoringBookId(List<int> lstMoniBookID);
    }
}