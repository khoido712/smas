﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;


namespace SMAS.Business.IBusiness
{
    public partial interface IBookmarkedFunctionBusiness
    {
        IQueryable<BookmarkedFunction> Search(IDictionary<string, object> dic);
        List<BookmarkedFunction> GetBookmarkedFunctionOfUser(int schoolId, int academicYearId, int appliedLevel, int userId);
        void Insert(BookmarkedFunction obj);
        void Update(BookmarkedFunction obj);
        void Delete(int id);
    }
}
