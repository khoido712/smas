/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.IBusiness
{
    public partial interface IMarkRecordHistoryBusiness
    {
        void InsertMarkRecordHistoryAll(int UserID, List<MarkRecordHistory> ListMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, List<int> lstPupilExempted, IDictionary<string, object> dicParam);
        bool InsertMarkRecordHistory(int UserID, List<MarkRecordHistory> ListMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, int SubjectID,int? EmployeeID, int? Year = null);
        void DeleteMarkRecordHistory(int UserID, long MarkRecordHistoryID, int? SchoolID);
        void DeleteMarkRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        IQueryable<MarkRecordHistory> SearchMarkRecordHistory(IDictionary<string, object> dic);
        IQueryable<MarkRecordHistory> SearchMarkRecordPrimaryHistory(IDictionary<string, object> dic);
        void InsertMarkRecordTXOfPrimaryHistory(List<MarkRecordHistory> ListMarkRecordHistory);
        void InsertMarkRecordDKOfPrimaryHistory(int User, List<MarkRecordHistory> lstMarkRecordDKOfPrimaryHistory, List<SummedUpRecordHistory> LstSummedUpRecordHistory, int Semester);
        void DeleteMarkRecordPrimaryHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        void ImportMarkRecord(int UserID, List<MarkRecordHistory> lstMarkRecordHistory);

        void InsertMarkRecordHistoryBySubjectID(int UserID, List<MarkRecordHistory> LstMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, List<int> listSubjectID, int? EmployeeID, int? Year = null);

        void InsertMarkRecordHistoryByClassID(int UserID, List<MarkRecordHistory> LstMarkRecord, List<SummedUpRecordHistory> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, List<int> listClassID, int SubjectID, int? EmployeeID, int? Year = null);
        void SummedUpMarkHis(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, int? PeriodID, String strMarkTitle, ClassSubject classSubject);
        void ImportFromOtherSystem(List<ImportFromOtherSystemBO> lstBO, int schoolId, int academicYearId, int appliedId, int classId, int subjectId, int semester, bool isComment, int? employeeId);
    }
}
