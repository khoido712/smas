/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IAcademicYearBusiness 
    {
        IQueryable<AcademicYear> Search(IDictionary<string, object> dic);
        IQueryable<AcademicYear> SearchBySchool( int SchoolID , Dictionary<string, object> dic = null);
        void Delete(int AcademicYearID );
        bool IsCurrentYear(int AcademicYearID);
        bool IsCurrentYear(AcademicYear AcademicYear);
        AcademicYear InsertAcademicYearOfChildren(AcademicYear AcademicYear, bool isInheritance);
        List<int> GetListYearForSupervisingDept(int SupervisingDeptID);
        List<int> GetListYearForSupervisingDept_TH(int SupervisingDeptID);
        List<int> GetListYearForSupervisingDept_THCS(int SupervisingDeptID);
        List<int> GetListYearForSupervisingDept_THPT(int SupervisingDeptID);
        bool IsSecondSemesterTime(int AcademicYearID, DateTime? Time = null);
        bool CheckTimeInYear(int academicYearID, DateTime checkTime);
        List<int> GetListYearForSupervisingDept_Pro(int SupervisingDeptID);
        int GetDefaultSemester(int academicYearID);
    }
}