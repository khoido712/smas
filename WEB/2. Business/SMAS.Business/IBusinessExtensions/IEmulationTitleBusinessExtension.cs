﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IEmulationTitleBusiness
    {
        Stream CreateEmulationTitle(EmulationTitle entity);
        ProcessedReport GetEmulationTitle(EmulationTitle entity);
        ProcessedReport InsertEmulationTitle(EmulationTitle entity, Stream data);
        string GetHashKey(EmulationTitle entity);
    }
}
