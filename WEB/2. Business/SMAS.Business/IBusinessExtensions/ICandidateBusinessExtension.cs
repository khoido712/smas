﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ICandidateBusiness
    {
        IQueryable<Candidate> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        void InsertGroupCandidate(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, int ExaminationSubjectID, IQueryable<PupilOfClass> ListPupilOfClass);
        void NameListCode(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, int ExaminationSubjectID, string Prefix, int CodeLength, bool OrderByClass, bool OrderByName);
        void UpdateRoom(int SchoolID, int AppliedLevel, int CandidateID, int RoomID);
        void Delete(int SchoolID, int ExaminationID, int AppliedLevel, List<int> listCandidateID);
        void UpdateMark(int SchoolID, int AppliedLevel, int CandidateID, string ExamMark, int IsCommenting);
        void DeleteMark(int SchoolID, int AppliedLevel, int CandidateID);
        IEnumerable<CandidateMark> SearchMark(int SchoolID, int AppliedLevel, IDictionary<string, object> SearchInfo);
        
        /// <summary>
        /// Chuyển điểm thi vào sổ điểm cho năm học hien tai.
        /// Nghiệp vụ chỉ đáp ứng cho cấp 2, 3. Cấp 1 chưa có nghiệp vụ này
        /// </summary>
        /// <param name="SchoolID">ID cua truong</param>
        /// <param name="AppliedLevel">Cap hoc: 2 hoac 3</param>
        /// <param name="ExaminationSubjectID">ID mon hoc trong ky thi</param>
        /// <param name="ListCandidateID">Danh sach CandidateID se duoc thuc hien chuyen diem</param>
        /// <param name="Semester">Hoc ky se duoc chuyen diem vao</param>
        /// <param name="MarkType">Loai diem se duoc chuyen diem vao</param>
        /// <param name="MarkIndex">Cot diem se duoc chuyen diem vao</param>
        void TransferMark(int SchoolID, int AppliedLevel, int ExaminationSubjectID, List<int> ListCandidateID, int Semester, string MarkType, int MarkIndex);
        
        /// <summary>
        /// Chuyển điểm thi vào sổ điểm cho các năm học cũ.
        /// Nghiệp vụ chỉ đáp ứng cho cấp 2, 3. Cấp 1 chưa có nghiệp vụ này
        /// </summary>
        /// <param name="SchoolID">ID cua truong</param>
        /// <param name="AppliedLevel">Cap hoc: 2 hoac 3</param>
        /// <param name="ExaminationSubjectID">ID mon hoc trong ky thi</param>
        /// <param name="ListCandidateID">Danh sach CandidateID se duoc thuc hien chuyen diem</param>
        /// <param name="Semester">Hoc ky se duoc chuyen diem vao</param>
        /// <param name="MarkType">Loai diem se duoc chuyen diem vao</param>
        /// <param name="MarkIndex">Cot diem se duoc chuyen diem vao</param>
        void TransferMarkHistory(int SchoolID, int AppliedLevel, int ExaminationSubjectID, List<int> ListCandidateID, int Semester, string MarkType, int MarkIndex);
        
        void InsertList(List<Candidate> ListCandidate);
        void UpdateList(List<Candidate> ListCandidate);

        string GetHashKey(CandidateBO entity);
        ProcessedReport InsertSBD(CandidateBO entity, Stream Data);
        ProcessedReport GetSBD(CandidateBO entity);
        Stream CreateSBD(CandidateBO entity, out string fileName);

        ProcessedReport InsertDSTS(CandidateBO entity, Stream Data);
        ProcessedReport GetDSTS(CandidateBO entity);
        Stream CreateDSTS(CandidateBO entity, out string fileName);

        ProcessedReport InsertSearchCandidateMark(CandidateBO entity, Stream Data);
        ProcessedReport GetSearchCandidateMark(CandidateBO entity);
        Stream CreateSearchCandidateMark(CandidateBO entity, out string fileName);
    }
}
