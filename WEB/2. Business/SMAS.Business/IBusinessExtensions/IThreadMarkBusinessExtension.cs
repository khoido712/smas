﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IThreadMarkBusiness
    {
        int InsertActionList(ThreadMarkBO info); //  Type = 1: Tong ket diem
        /// <summary>
        /// ham update threadMark phuc vu cho viec tinh diem mon nhan xet 
        /// co lam tron hay khong lam tron trong chuc nang cau hinh.Khi cau hinh
        /// phai update lai threadMark de tinh lai TBM
        /// </summary>
        /// <param name="dic"></param>
        void UpdateThreadMark(IDictionary<string, object> dic);

        //void InsertListThreadMark(List<ThreadMark> lstThreadMark);

        int BulkInsertActionList(ThreadMarkBO obj);
        void BulkInsertListThreadMark(List<ThreadMark> lstThreadMark);
        /// <summary>
        /// Them moi doi tuong ThreadMark vao List;
        /// </summary>
        /// <param name="schoolId"></param>        
        /// <param name="academicYearId"></param>
        /// <param name="classId"></param>
        /// <param name="subjectId"></param>
        /// <param name="semester"></param>
        /// <param name="type"></param>
        /// <param name="lstThreadMark"></param>
        void AddThreadMark(int schoolId, int academicYearId, int classId, int subjectId, short semester, int type, int? PeriodID, int pupilId, List<ThreadMark> lstThreadMark);
    }
}
