/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEWalletBusiness 
    {
        EwalletBO GetEWallet(int unitId, bool schoolUser, bool superUser);
        EW_EWALLET ChangeAmountSub(int ewalletID, decimal amount, byte transTypeID, int quantity, byte? serviceType);
        EwalletBO GetEWalletIncludePromotion(int unitId, bool schoolUser, bool superUser);
        void WaitingTopup(int EwalletID);
        bool LockEWallet(int EwalletID, bool isLock);
        EwalletBO ChangesAmountAddDirectly(int ewalletID, decimal amount);
        EWalletObject DeductMoneyAccount(Dictionary<string, object> dic);
    }
}