﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness 
{
    public partial interface IStatisticsForUnitBusiness 
    {

        ProcessedReport GetPeriodStatistic(StatisticReportBO entity,int reportTypeID);

        ProcessedReport GetSemesterMarkPrimary(IDictionary<string, object> dic);

        ProcessedReport InsertPeriodStatistic(StatisticReportBO entity, Stream data, string reportName,int reportTypeID);

        ProcessedReport InsertSemesterMarkPrimary(StatisticsForUnitBO entity, Stream data, string reportName);

        List<MarkStatisticBO> SearchPeriodicMarkForPrimary(StatisticsForUnitBO statisticsForUnit);

        List<MarkStatisticBO> SearchSemesterMarkForPrimary(StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatistic> SearchMarkSubjectCapacityForPrimary (StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatistic> SearchJudgeSubjectCapacityForPrimary(StatisticsForUnitBO statisticsForUnit);

        List<MarkStatisticBO> SearchPeriodicMarkForSecondary(StatisticsForUnitBO statisticsForUnit);

        List<MarkStatisticBO>  SearchSemesterMarkForSecondary(StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatisticsBO> SearchSubjectCapacityForSecondary(StatisticsForUnitBO statisticsForUnit);

        List<ConductStatisticsBO> SearchConductForSecondary(StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatisticsBO> SearchCapacityForSecondary(StatisticsForUnitBO statisticsForUnit);

        List<MarkStatisticBO> SearchPeriodicMarkForTertiary(StatisticsForUnitBO statisticsForUnit);

        List<MarkStatisticBO> SearchSemesterMarkForTertiary(StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatisticsBO>  SearchSubjectCapacityForTertiary (StatisticsForUnitBO statisticsForUnit);

        List<ConductStatisticsBO> SearchConductForTertiary(StatisticsForUnitBO statisticsForUnit);

        List<CapacityStatisticsBO> SearchCapacityForTertiary(StatisticsForUnitBO statisticsForUnit);

        Stream CreatePeriodicMarkForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateSemesterMarkForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateSubjectCapacityForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateConductForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateCapacityForSecondaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreatePeriodicMarkForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateSemesterMarkForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateSubjectCapacityForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateConductForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateCapacityForTertiaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreatePeriodicMarkForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateSemesterMarkForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateMarkSubjectCapacityForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

        Stream CreateJudgeSubjectCapacityForPrimaryReport(StatisticsForUnitBO statisticsForUnit, out string reportName);

    }
}
