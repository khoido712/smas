/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassSupervisorAssignmentBusiness 
    {
        void UpdateForAllEducationLevel(int SchoolID,
            int AppliedLevel, int AcademicYearID, int EducationLevelID,
            int TeacherID, Dictionary<int, List<int>> ClassAndPermission);

        IQueryable<ClassSupervisorAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<ClassSupervisorAssignment> Search(IDictionary<string, object> SearchInfo);

        void DeleteByTeacherID(IDictionary<string, object> SearchInfo);
    }
}