﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportPupilSynthesisBusiness
    {
        Stream ExportPupilSynthesis(int UserAccountID, int SchoolID,int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, IDictionary<String, object> dic,bool isSuperViSingDept,bool isSubSuperVisingDept,out string FileName);
    }
}
