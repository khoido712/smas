﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IRatedCommentPupilHistoryBusiness 
    {
        IQueryable<RatedCommentPupilHistory> Search(IDictionary<string, object> dic);
        void InsertOrUpdate(List<RatedCommentPupilBO> lstRatedCommentPupilBO, IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit);
        void DeleteCommentPupil(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit);
        void InsertOrUpdateToMobile(RatedCommentPupilBO objRatedCommentPupilBO, IDictionary<string, object> dic);
        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);
    }
}