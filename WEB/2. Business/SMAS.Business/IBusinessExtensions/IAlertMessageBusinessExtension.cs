﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;


namespace SMAS.Business.IBusiness
{
    public partial interface IAlertMessageBusiness
    {
        IQueryable<AlertMessage> Search(IDictionary<string, object> dic);
        void Insert(AlertMessage alertMessage);
        void Update(AlertMessage alertMessage);
        void Delete(int AlertMessageID);//, int SchoolID);
        List<string> GetListDeviceToken(int SchoolID);
        string SendNotificationToAPI(List<string> deviceToken, string body, string title);
        void PushAlertMessageScheduler();
    }
}
