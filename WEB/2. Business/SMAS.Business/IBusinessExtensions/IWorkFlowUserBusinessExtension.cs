/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IWorkFlowUserBusiness 
    {
        /// <summary>
        /// Lay danh sach cac cong viec nguoi dung da dinh nghia
        /// </summary>
        /// <returns></returns>
        IQueryable<WorkFlowUserBO> Search(IDictionary<string, object> dic);

        /// <summary>
        /// Lay thong tin cac chuc nang khai bao nam hoc
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<FirstDataFunctionBO> GetFirstDataFunction(IDictionary<string, object> dic, int tabFunction);
    }
}