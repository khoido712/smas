/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ITimerConfigBusiness 
    {
        void DeleteTimerConfig(IDictionary<string, object> dic, Guid timerConfigId);
        void UpdateTimerConfig(IDictionary<string, object> dic, Guid timerConfigId, List<SMS_TIMER_TRANSACTION> lstTimerTransaction);
    }
}