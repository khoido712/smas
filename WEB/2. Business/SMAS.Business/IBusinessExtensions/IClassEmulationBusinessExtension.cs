/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassEmulationBusiness 
    {
        void InsertOrUpdate(int AcademicYearID, int EducationLevelID, DateTime UpdatedDate, Dictionary<int, Dictionary<int, decimal>> Data);
        IQueryable<ClassEmulation> Search(IDictionary<string, object> dic);
        Stream ExportClassEmulationByWeek(int SchoolID, int AcademicYearID, DateTime date, int EducationLevelID, out string FileName);
        Stream ExportClassEmulationByMonth(int SchoolID, int AcademicYearID, int EducationLevelID, DateTime date, out string FileName);
    }
}