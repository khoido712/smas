/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IReviewBookPupilBusiness 
    {
        string GetHashKey(IDictionary<string, object> dic);
        IQueryable<ReviewBookPupil> Search(IDictionary<string, object> dic);
        IQueryable<ReviewBookPupilBO> GetListReviewPupil(IDictionary<string, object> dic);
        Stream CreatePupilEvaluationHDGDReport(IDictionary<string, object> dic);
        ProcessedReport InsertPupilEvaluationHDGDReport(IDictionary<string, object> dic, Stream data);
        Stream CreatePupilEvaluationNLPCReport(IDictionary<string, object> dic);
        ProcessedReport InsertPupilEvaluationNLPCReport(IDictionary<string, object> dic, Stream data);
        List<PupilRepeaterBO> GetListRepeatPupilOfSchool(int academicYearId, int schoolId);
    }
}