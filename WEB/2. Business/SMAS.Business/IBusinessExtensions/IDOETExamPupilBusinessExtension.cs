/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExamPupilBusiness
    {
        IQueryable<DOETExamPupil> Search(IDictionary<string, object> search);
        IQueryable<DOETExamPupilBO> GetListExamPupil(IDictionary<string, object> dic);
        void UpdateExamPupil(List<DOETExamPupilBO> lstExamPupilBO, IDictionary<string, object> dic);
        IQueryable<DOETExamPupil> GetGroupedPupilWithSameSubject(List<int> listPupilID, List<int> listSubjectID, int examinationsID);
        void InsertList(List<DOETExamPupil> insertList);
        IQueryable<DOETExamPupilBO> GetListDOETExamSubject(IDictionary<string, object> dic);
        List<DOETExamMarkBO> GetListMark(List<int> listExamPupilID, List<int> listSubjectId);
        void DeleteList(List<int> listExamPupilID);
        ProcessedReport InsertExamPupilRegisSubReport(IDictionary<string, object> dic, Stream data);
        Stream CreateExamPupilRegisSubReport(IDictionary<string, object> dic);

        /// <summary>
        /// Lay du lieu chua dong bo de dong bo cho HCM
        /// </summary>
        /// <param name="academicYear"></param>
        /// <param name="school"></param>
        /// <param name="appliedLevel"></param>
        /// <param name="examinationId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="subjectId"></param>
        /// <param name="isNotSync"></param>
        /// <returns></returns>
        List<DOETExamPupilBO> GetSyncExamPupilHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId, int subjectId, bool isNotSync);
    }
}