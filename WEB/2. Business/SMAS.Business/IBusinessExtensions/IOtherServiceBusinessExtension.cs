/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IOtherServiceBusiness 
    {
        void Insert(OtherService otherService);
        void Update(OtherService otherService);
        void Delete(int OtherServiceID, int SchoolID);
        IQueryable<OtherService> Search(IDictionary<string, object> dic);
        IQueryable<OtherService> SearchBySchool(int schoolID, IDictionary<string, object> dic);

    }
}