/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEatingGroupBusiness 
    {
        void Insert(EatingGroup Entity, List<int> lstClass);
        void Update(EatingGroup Entity, List<int> lstClass);
        void Delete(int EatingGroupID, int SchoolID);
        IQueryable<EatingGroupBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<EatingGroup> SearchEatingGroup(IDictionary<string, object> dic);
    }
}