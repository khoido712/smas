/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IFoodCatBusiness
    {
        void Insert(FoodCat foodcat, List<FoodMineral> lstFoodMineral);
        void Update(FoodCat foodcat, List<FoodMineral> lstFoodMineral);
        void Delete(int FoodID);
        IQueryable<FoodCat> SearchBySchool(int SchoolID, IDictionary<string, object> food);
        IQueryable<FoodCat> Search(IDictionary<string, object> SearchInfo);
        MineralOfFoodBO MineralOfFood(int FoodID);
        MineralOfDailyMenuBO CaculatorContent(List<int> lstFood, List<decimal> lstWeight, int EatingGroupID, int SchoolID, int AcademicYearID);
        List<RateByMealBO> CaculatorRateByMeal(int DailyMenuID, int SchoolID, int AcademicYearID, int EatingGroupID);
        List<decimal> CaculatorFoodToAdd(List<int> lstFood, List<int> lstNutritionalToAdd, List<decimal> lstContentToAdd, int numnberOfChilden);
        
    }
}