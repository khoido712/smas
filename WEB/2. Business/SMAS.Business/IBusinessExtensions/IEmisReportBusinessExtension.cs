using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.IBusiness
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>12/21/2012</date>
    public partial interface IEmisReportBusiness
    {
        /// <summary>
        /// GetHashKeyForReportSchoolInfo
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        string GetHashKeyForReportSchoolInfo(IDictionary<string, object> Entity);
        
        /// <summary>
        /// GetStatusSentToSupervisor
        /// </summary>
        /// <param name="ReportCode">The report code.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns>
        /// Byte
        /// </returns>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        int GetStatusSentToSupervisor(string ReportCode, string InputParameterHashKey);
        
        /// <summary>
        /// InsertProcessedReportSchoolInfo
        /// </summary>
        /// <param name="ReportCode">The report code.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="ProcName">Name of the proc.</param>
        /// <param name="ProcParams">The proc params.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        ProcessedReport InsertProcessedReportSchoolInfo(string ReportCode, int SchoolID, int AcademicYearID, string ProcName, IDictionary<string, object> ProcParams);
        
        /// <summary>
        /// CreatedEmisReport
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="workBookData">The work book data.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        Stream CreatedEmisReport(ProcessedReport Entity, int SchoolID, int AcademicYearID, WorkBookData workBookData);
    }
}
