/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.SearchForm;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilFaultBusiness
    {
        decimal SumTotalPenalizedMarkByClass(int ClassID, DateTime start, DateTime end);
        IQueryable<PupilFault> SearchBySchool(int SchoolID = 0, IDictionary<string, object> SearchInfo = null);
        //IQueryable<PupilFault> SearchPupilFault(IDictionary<string, object> SearchInfo);
        void DeletePupilFault(int UserID, bool IsAdmin, int PupilDisciplineID, int SchoolID);
        void UpdatePupilFault(int UserID, bool IsAdmin, PupilFault pupilFault);
        void InsertPupilFault(int AcademicYearID, int UserID, int SchoolID, int AppliedLevel, int ClassID, int PupilID, int FaultGroupID, DateTime ViolatedDate, List<int> ListFaultCriteriaID, List<int> ListNumberOfFault);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        //phuongh1 09/08/2013
        void UpdateForPupil(int UserID, bool IsAdmin, int SchoolID, int AcademicYearID, int PupilID, DateTime Date, List<PupilFault> ListData);
        void DeleteAllFaultInDay(int UserID, bool IsAdmin, int PupilFaultID, int SchoolID);
        IQueryable<PupilFault> SearchBySchool(int SchoolID, PupilFaultSearchForm SearchForm);
        void UpdateForListPupil(int UserID, int SchoolID, int AcademicYearID, int ClassID, DateTime Date, List<PupilFault> lstPupilFault,int FaultID);

    }
}