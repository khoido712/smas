﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IDeclareEvaluationIndexBusiness
    {
        IQueryable<DeclareEvaluationIndexBO> Search(IDictionary<string, object> SearchInfo);

        IQueryable<DeclareEvaluationIndexBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);

        IQueryable<EvaluationDevelopmentGroup> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo = null);

        IQueryable<EvaluationDevelopmentGroup> SearchEvaluation(IDictionary<string, object> dic);

        IQueryable<EvaluationDevelopmentGroup> SearchForCb2(int eduID, IDictionary<string, object> dic);

        void Insert(List<DeclareEvaluationIndex> ListDeclareEvaluationGroup, Dictionary<string, object> dic, int a);
        
        void DeleteWhenUpdateGroup(List<DeclareEvaluationIndex> lstDelete);

        void InsertWhenUpdateGroup(List<DeclareEvaluationIndex> lstInsert, int acedmicYear);

        IQueryable<DeclareEvaluationIndex> GetListBySchoolAndAcademicYear(int schoolId, int academicYearId);
        void InsertBySchool(List<DeclareEvaluationIndex> lstDEI);
        IQueryable<DeclareEvaluationIndexBO> SearchDEI(Dictionary<string, object> dic);

        void InsertOrUpdate(List<DeclareEvaluationIndexBO> lstInsert, IDictionary<string, object> dic);
    }
}
