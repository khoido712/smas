﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExamSupervisoryBusiness
    {

        /// <summary>
        /// Lay danh sach giam thi
        /// </summary>
        /// <param name="examinationId"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <returns></returns>
        List<DOETExamSupervisoryBO> GetExamEmpoyee(int examinationId, int schoolId, int academicYearId, int employeeType);
         
        List<DOETExamSupervisoryBO> GetEmployeeNotRegis(int examinationId, int employeeType, int schoolId, int academicYearId, List<int> lstEmployeeRegisId);
    }
}
