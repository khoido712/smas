/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPointReportBusiness 
    {
        string GetHashKeyForPeriod(PointReportBO entity);
        ProcessedReport GetPointReportByPeriod(PointReportBO entity);
        ProcessedReport InsertPointReportByPeriod(PointReportBO entity, Stream data);
        Stream CreatePointReportByPeriod(PointReportBO entity);
        ReportDefinition GetReportDefinitionOfPeriodReport(PointReportBO entity);
        string GetHashKeyForSemester(PointReportBO entity);
        ProcessedReport GetPointReportBySemester(PointReportBO entity);
        ProcessedReport InsertPointReportBySemester(PointReportBO entity, Stream data);
        Stream CreatePointReportBySemester(PointReportBO entity
            , List<MarkRecordBO> lstMarkRecord
            , List<JudgeRecordBO> lstJudgeRecord
            , List<SummedUpRecordBO> lstSummedUpRecord);
        ReportDefinition GetReportDefinitionOfSemesterReport(PointReportBO entity);

        Stream CreatePointReportBySemesterFollowNewPattern(PointReportBO entity, List<SummedUpRecordBO> lstSummedUpRecord);
    }
}