/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.SearchForm;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilProfileBusiness
    {
        void UpdateChildren(int UserID, PupilProfile PupilProfile, List<HabitOfChildren> lstHabitOfChildren, List<FoodHabitOfChildren> lstFoodHabitOfChildren, int OldClassID, int EnrollmentType);
        void InsertChildren(int UserID, PupilProfile InsertPupilProfile, List<HabitOfChildren> lstHabitOfChildren, List<FoodHabitOfChildren> lstFoodHabitOfChildren , int EnrollmentType);
        void InsertPupilProfile(int UserID, PupilProfile InsertPupilProfile, int EnrolmentType, bool isImport = false);
        void ImportPupilProfile(int UserID, List<PupilProfile> lstPupilProfile, List<int> lstEnrollmentType, List<int> lstOrder, List<int> lstClassID);
        void UpdatePupilProfile(int UserID, PupilProfile UpdatePupilProfile, int EnrolmentType, int OldClassID, bool isImport = false);
        void UpdateImportPupilProfile(int UserID, List<PupilProfile> lstUpdatePupilProfile, List<int> lstEnrolmentType, List<int> lstOrderUpdate, List<int> lstOldClassID, List<int> lstClassID);
        void DeletePupilProfile(int UserID, int PupilID, int SchoolID, int AcademicYearId, UserInfoBO userInfo);
        void DeletePupilProfileChildren(int UserID, int PupilID, int SchoolID, int AcademicYearID);
        IQueryable<PupilProfileBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        IQueryable<PupilProfile> GetListPupilPolicyTarget(int AcademicYearID, int SchoolID);
        IQueryable<PupilProfileBO> SearchPupilBySupervisingDept(Dictionary<string, object> dic);
        IQueryable<PupilProfileBO> SearchPupilBySupervisingDept2(Dictionary<string, object> dic);
        IQueryable<PupilProfileBO> SearchForSMSParents(IDictionary<string, object> SearchInfo);
        List<PupilProfile> CountPupilFemaleOfClass(int AcademicYearID, int SchoolID);
        List<PupilProfile> CountPupilMaleOfClass(int AcademicYearID, int SchoolID);
        List<PupilProfile> CountPupilOfClass(int AcademicYearID, int SchoolID);
        List<RESTORE_DATA_DETAIL> DeleteListPupilProfile(int UserID, List<int> ListPupilID, int SchoolID, int AcademicYearID, RESTORE_DATA objRestore);
        void DeleteListPupilProfileChildren(int UserID, List<int> ListPupilID, int SchoolID, int AcademicYearID, RESTORE_DATA objRestore);

        /// <summary>
        /// get list pupil of parent
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>05/06/2013</date>       
        /// <returns></returns>
        IQueryable<SelectPupilBO> GetSelectPupilByParentAccount(IDictionary<string, object> dic);
        IQueryable<PupilProfileBO> Search(IDictionary<string, object> dic);
        IQueryable<PupilProfile> SearchBySchool(int SchoolID, PupilProfileSearchForm SearchForm = null);
        bool IsNotConductRankingPupil(int pupilID, int classID);
        bool IsNotConductRankingPupil(bool isGDTX, int appliedLevel, int? learingType, DateTime? birthDate);
        bool IsNotConductRankingPupilImport(bool isGDTX, int appliedLevel, int? learingType, DateTime? birthDate);
        IQueryable<PupilOfClassBO> getListPupilByListPupilID(List<int> listPupilId, int academicYearId);
        void ImportFromOtherSystem(int UserID, List<PupilProfile> lstPupilProfile, List<OrderInsertBO> lstOrder, List<int> lstClassID);
        List<IDictionary<string, object>> GetStatisticForPupilLeavingOffReport(int schoolId, int yearId);
        List<int> GetListPupilID(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);
        IQueryable<PupilProfileBO> GetListPupilProfile(IDictionary<string, object> dic);
        List<PupilOfClassBO> GetPupilsByIDsPaging(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, ref int totalRecord, int teacherID);
        List<PupilOfClassBO> GetPupilsbySchoolIDPaging(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, ref int totalRecord, int teacherID);
        List<PupilProfileBO> GetListPupilByListPupilID(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID);
        List<PupilProfileBO> GetPupilsformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID);
        List<PupilProfileBO> GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID);
        List<PupilProfileBO> GetPupilsByYear(List<int> PupilIDs, int Year);
        PupilProfileBO GetPupilProfileByYear(int PupilProfileID, int Year);
        bool UpdateRegisterContractPupil(List<int> lstPupilOfClassIDUpdate);
        List<PupilProfileBO> GetListPupilSendSMS(List<int> pupilProfileList, int academicYearID);
        IQueryable<PupilOfClassBO> GetListPupilByListPupilIDForContract(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID);

        List<PupilProfileBO> GetListPupilformatString(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);
        List<PupilMarkBO> GetListMarkRecordSemester(int SchoolID, int ClassID, int AcademicYearID, int Semester);
        List<PupilMarkBO> GetMarkRecordByClass(int SchoolID, int ClassID, int EducationLevel,
             int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester);

        List<PupilMarkBO> GetListMarkOfPeriod(int SchoolID, int ClassID, int AcademicYearID, int PeriodID);
        List<PupilMarkBO> GetListTraningInfoByTime(int SchoolID, int ClassID, int AcademicYearID, DateTime startTime, DateTime endTime);

        List<EvaluationCommentsBO> GetCommentsVNEN(List<long> listPupilID, int schoolID, int academicYearID, int classID, int semesterID, int monthID);
        List<RatedCommentPupilBO> GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, int semesterID);
        List<ExamMarkSemesterBO> GetListExamMarkSemester(List<int> lstPupilID, int SchoolID, int ClassID, int AcademicYearID, int Semester);
        List<ExamSubjectBO> getExamScheduleInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semester, int examID);
        List<ExamMarkPupilBO> getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semester, int examID);
        string GetListCalendarOfPupil(int AcademicYearID, int ClassID, int Semester);
    }
}
