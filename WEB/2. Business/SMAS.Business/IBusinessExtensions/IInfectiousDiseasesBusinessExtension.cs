﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>26/12/2012 4:17:01 PM</date>
    /// </summary>
    public partial interface IInfectiousDiseasBusiness
    {
        InfectiousDiseas Insert(InfectiousDiseas entity);
        InfectiousDiseas Update(InfectiousDiseas entity);
        void Delete(int InfectiousDiseaseID, int SchoolID);
        IQueryable<InfectiousDiseas> Search(IDictionary<string, object> SearchInfo);
        IQueryable<InfectiousDiseas> SearchBySchool(int SchoolID,IDictionary<string, object> SearchInfo =null);

    }
}
