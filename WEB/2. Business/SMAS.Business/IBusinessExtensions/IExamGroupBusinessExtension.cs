﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamGroupBusiness 
    {
        /// <summary>
        /// List nhóm thi theo kỳ thi
        /// </summary>
        /// <param name="examID"></param>
        /// <returns></returns>
        List<ExamGroup> GetListExamGroupByExaminationsID(long examID);
        IQueryable<ExamGroup> Search(IDictionary<string, object> search);

        /// <summary>
        /// Kiểm tra nhóm thi đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="examGroupID">Danh sach ID nhóm thi</param>
        /// <returns>Danh sach cac ID nhom thi da co du lieu rang buoc</returns>
        List<long> ExamGroupInUsed(List<long> inputExamGroupID);

        /// <summary>
        /// Kiểm tra nhóm thi đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="examGroupID">ID nhóm thi</param>
        /// <returns>True neu da co du lieu rang buoc</returns>
        bool isInUsed(long examGroupID);

        
    }
}