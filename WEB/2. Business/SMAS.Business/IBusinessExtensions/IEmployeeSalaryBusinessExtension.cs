/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEmployeeSalaryBusiness 
    {
        EmployeeSalary Insert(EmployeeSalary insertEmployeeSalary);
        EmployeeSalary Update(EmployeeSalary updateEmployeeSalary);
        void Delete(int SchoolOrSupervisingDeptID, int EmployeeSalaryID);
        IQueryable<EmployeeSalary> Search(IDictionary<string, object> dic);
        IQueryable<EmployeeSalary> SearchByEmployee(int EmployeeID,IDictionary<string, object> dic=null);
        IQueryable<EmployeeSalary> SearchBySchool(int SchoolID, IDictionary<string, object> dic=null);
        IQueryable<EmployeeSalary> SearchBySupervisingDept(int SupervisingDeptID, IDictionary<string, object> dic=null);
        void Validate(EmployeeSalary insertEmployeeSalary);
        void InsertOrUpdateSalary(IDictionary<string, object> dic, EmployeeSalary objES);
    }
}