﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportEmployeeProfileBusiness
    {
        string GetHashKey(ReportEmployeeProfileBO reportEmployeeProfile);
        ProcessedReport InsertReportEmployeeList(ReportEmployeeProfileBO entity, Stream data);
        ProcessedReport GetReportEmployeeList(ReportEmployeeProfileBO entity);
        Stream CreateReportEmployeeList(ReportEmployeeProfileBO entity);
        ProcessedReport InsertReportEmployeeContract(ReportEmployeeProfileBO entity, Stream data);
        ProcessedReport GetReportEmployeeContract(ReportEmployeeProfileBO entity);
        Stream CreateReportEmployeeContract(ReportEmployeeProfileBO entity);
        ProcessedReport InsertReportEmployeePayroll(ReportEmployeeProfileBO entity, Stream data);
        ProcessedReport GetReportEmployeePayroll(ReportEmployeeProfileBO entity);
        Stream CreateReportEmployeePayroll(ReportEmployeeProfileBO entity);
        ProcessedReport InsertReportEmployeeSenior(ReportEmployeeProfileBO entity, Stream data);
        ProcessedReport GetReportEmployeeSenior(ReportEmployeeProfileBO entity);
        Stream CreateReportEmployeeSenior(ReportEmployeeProfileBO entity);
        ProcessedReport InsertHeadTeacherProcessReport(IDictionary<string, object> dic, Stream data);
        ProcessedReport GetReportHeadTeacherProcess(IDictionary<string, object> dic);
        Stream CreateHeadTeacherReport(IDictionary<string, object> dic);
    }
}
