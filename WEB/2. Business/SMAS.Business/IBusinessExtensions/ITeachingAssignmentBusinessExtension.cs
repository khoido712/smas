/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ITeachingAssignmentBusiness 
    {
        IQueryable<TeachingAssignmentBO> GetTeachingAssigment(int SchoolID, IDictionary<string, object> dic);
        IQueryable<TeachingAssignmentBO> GetSubjectTeacher(int SchoolID, IDictionary<string, object> dic);
        void Delete(int SchoolID, int TeachingAssignmentID, bool ApplyForAll);
        TeachingAssignment Insert(TeachingAssignment teachingAssignment);
        TeachingAssignment Update(int SchoolID, long TeachingAssignmentID, int FacultyID, int TeacherID, bool ApplyForAll);
        IQueryable<SubjectCat> GetListSubjectBySubjectTeacher(int TeacherID, int AcademicYearID, int SchoolID, int Semester, int ClassID);
        void UpdateForEmployee(int SchoolID, int AcademicYearID, int AppliedLevel, int TeacherID, int Semester, List<TeachingAssignment> lstTeachingAssignment);
        IQueryable<TeachingAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null);
        void UpdateForSubjectAndEmployee(int SchoolID, int AcademicYearID, int AppliedLevel,
           int TeacherID, int SubjectID, List<TeachingAssignment> lstTeachingAssignment);
        decimal GetSectionPerWeek(int SchoolID, int AcademicYearID, int AppliedLevel,
            int TeacherID, int SubjectID, int Semester);
        Dictionary<int, string> GetSectionPerWeekInYear(int SchoolID, int AcademicYearID, int AppliedLevel);
        //decimal GetSectionPerClass(int ClassID, int SubjectID, int Semester);
        Dictionary<int, string> GetSectionPerClassBySubjectInYear(int SchoolID, int AcademicYearID, int SubjectID);
        IQueryable<TeachingAssignmentBO> SearchByTeacher(int schoolID, Dictionary<string, object> dic = null);
        IQueryable<ClassSubjectTemp> GetListSubjectBySubjectTeacher_BM(int TeacherID, int AcademicYearID, int SchoolID, int Semester, int ClassID);
        // Quanglm1 bo sung
        void InsertOrUpdateListTeachingAssignment(int SchoolID, int AcademicYearID, List<TeachingAssignment> listTeachingAssignment);

        void Delete(Dictionary<string, object> dic);

        /// <summary>
        /// Lay danh sach phan cong giang day cua giao vien trong nam hoc
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="aplicationLevelId"></param>
        /// <returns></returns>
        List<TeachingAssignmentBO> GetTeachingAssignmentbyAcademicYear(int schoolId, int academicYearId, int applicationLevel);

        /// <summary>
        /// tra ve danh sach id  lop hoc khong co hoc mon thuoc VNEN
        /// </summary>
        /// <param name="listSubjectId"></param>
        /// <param name="listClassId"></param>
        /// <returns></returns>
        List<int> GetListClassIdNotVNEN(int subjectId, List<int> listClassId);
    }
}