﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IEducationQualityStatisticBusiness
    {
        string GetHashKey(IDictionary<string, object> dic);
        ProcessedReport GetEducationQualityStatisticReport(IDictionary<string, object> dic);
        Stream CreateEducationQualityStatisticReport(IDictionary<string, object> dic);
        ProcessedReport InsertEducationQualityStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel);
    }
}
