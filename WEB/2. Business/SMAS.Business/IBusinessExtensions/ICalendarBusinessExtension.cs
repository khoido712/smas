/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.BusinessExtensions;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ICalendarBusiness 
    {
        IQueryable<Calendar> Search(IDictionary<string, object> dic);
        Calendar InsertOrUpdate(Calendar calendar);
        void Delete(int CalendarID );
        void GetSection(int seperateID, ref List<SMAS.Business.Business.CalendarBusiness.ClassSection> _lst);
        IQueryable<CalendarBO> SearchByPermissionTeacher(IDictionary<string, object> dic);
        Stream ExportExcel(int? semester, int? level, int? classID, int? section, string teacherName, DateTime? dateApplied, int? schoolID, int? academicYearID, int? educationLevel, int? appliedLevel);
        void InsertCalendar(List<Calendar> lstCalendar, IDictionary<string, object> dic);

        int getExcelRow(int section, List<SMAS.Business.Business.CalendarBusiness.ClassSection> lstSection);
        List<CalendarBO> GetListCalendarOfPupil(int AcademicYearID, List<int> lstClassID, int Semester);

    }
}