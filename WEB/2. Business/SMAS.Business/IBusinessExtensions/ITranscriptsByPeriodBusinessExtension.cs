﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ITranscriptsByPeriodBusiness
    {
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        string GetHashKeyForTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);


        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <param name="Data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        ProcessedReport InsertTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass, Stream Data);


        /// <summary>
        /// Lấy bảng điểm học sinh theo đợtđược cập nhật mới nhất
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        ProcessedReport GetTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);


        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        Stream CreateTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);

        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        string GetHashKeyForSummariseTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);

        /// <summary>
        /// Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <param name="Data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        ProcessedReport InsertSummariseTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass, Stream Data);


        /// <summary>
        /// Lấy bảng điểm tổng hợp học sinh cấp 2,3 theo đợtđược cập nhật mới nhất
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        ProcessedReport GetSummariseTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);


        /// <summary>
        /// Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo đợt
        /// </summary>
        /// <param name="entity">The transcript of class.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/21/2012</date>
        Stream CreateSummariseTranscriptsByPeriod(TranscriptOfClass TranscriptOfClass);  
    }
}