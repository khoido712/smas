/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IEvaluationBookReportBusiness 
    {
        ReportDefinition GetReportDefinition(string reportCode);
        string GetHashKeyEvaluationBookReport(EvaluationBookReportBO entity);
        ProcessedReport GetProcessedReport(EvaluationBookReportBO entity, bool isZip = false);
        ProcessedReport InsertEvaluationBookReport(EvaluationBookReportBO entity, Stream data, bool isZip = false);
        Stream GetEvaluationBookReport(EvaluationBookReportBO entity);
        void GetZipFileReport(EvaluationBookReportBO Entity, IDictionary<string, object> dic,out string folder);
    }
}