/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassAssigmentBusiness 
    {
        IQueryable<ClassAssigment> Search(IDictionary<string, object> SearchInfo);
        IQueryable<ClassAssigment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        void UpdateClass(int AcademicYearID, int SchoolID, int ClassID, List<int> ListTeacherID, List<string> ListAssignmentWork, List<int> ListIsTeacherID);
        void Delete(int SchoolID, int ClassAssignmentID);
    }
}