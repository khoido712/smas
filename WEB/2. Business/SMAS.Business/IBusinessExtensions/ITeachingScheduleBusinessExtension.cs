/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ITeachingScheduleBusiness 
    {
        IQueryable<TeachingSchedule> Search(IDictionary<string, object> dic);
        void InsertOrUpdate(TeachingSchedule objTeachingSchedule, IDictionary<string, object> dic);
        void InsertOrUpdateList(List<TeachingSchedule> lstResult, IDictionary<string, object> dic);
        void Delete(TeachingSchedule obj);
        ProcessedReport InsertProcessReport(IDictionary<string, object> dicReport, Stream excel, string reportCode);
    }
}