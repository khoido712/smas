/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ICandidateAbsenceBusiness 
    {
        List<Candidate> InsertAll(int SchoolID, int AppliedLevel, bool HasReason, string ReasonDetail, List<int> ListCandidateID);
        Candidate Update(int CandidateID, int SchoolID, int AppliedLevel, bool HasReason, string ReasonDetail);
        void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListCandidateID);
        IQueryable<Candidate> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        Stream CreateCandidateAbsenceReport(Dictionary<string, object> dic, out string reportName);
    }
}