﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SMAS.Business.BusinessObject;
using System.Web;
using SMAS.Models.Models;
using System.IO;
namespace SMAS.Business.IBusiness
{
    public partial interface IMarkOfSemesterBusiness
    {
        string GetHashKey(MarkOfSemesterBO entity);
        ProcessedReport GetMarkOfSemester(MarkOfSemesterBO entity);
        ProcessedReport InsertMarkOfSemester(MarkOfSemesterBO entity, Stream data);
        Stream CreateMarkOfSemester(MarkOfSemesterBO entity);
    }

}
