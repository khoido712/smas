/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISubjectCatBusiness 
    {
        IQueryable<SubjectCat> Search(IDictionary<string, object> dic);
        void Delete(int SubjectCatID );
        void Insert(List<SubjectCat> ls);
        int GetMaxOrderIsSubjectByGrade(int AppliedLevel);
        void UpdateOrderInSubject(Dictionary<int, int?> dicData);
        List<SubjectCatBO> GetListSubjectBO(List<int> listSubjectID, int AppliedLevel, int AcademicYearID, int ClassID, int EducationLevelID, int Semester);
    }
}
