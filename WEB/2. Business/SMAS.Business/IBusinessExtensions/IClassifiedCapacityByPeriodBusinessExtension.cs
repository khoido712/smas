﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IClassifiedCapacityByPeriodBusiness
    {
        string GetHashKey(TranscriptOfClass Entity);
        ProcessedReport InsertClassifiedCapacityByPeriod(TranscriptOfClass Entity, Stream Data);
        ProcessedReport GetClassifiedCapacityByPeriod(TranscriptOfClass Entity);
        Stream CreateClassifiedCapacityByPeriod(TranscriptOfClass Entity);
    }
}
