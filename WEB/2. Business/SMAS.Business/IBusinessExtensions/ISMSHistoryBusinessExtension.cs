/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSHistoryBusiness 
    {
        IQueryable<SMS_HISTORY> Search(IDictionary<string, object> dic);
        ResultBO SendSMS(Dictionary<string, object> dic);
        ResultBO SendSmstoTeacher(SMSSchoolTeacherBO inputSMS);
        EwalletResultBO ComputeBalanceAccount(int unitID, UserType userType, decimal priceSendSMS, int internalSMS, int externalSMS);
        //string GetBrandNamebyMobile(string mobile, List<PreNumberTelco> lstPreNumberTelco, List<BrandNameUsingBO> lstBrandName);
        SMSEduResultBO SendSmstoSchool(SMSSupervisingDeptSchoolBO inputSMS);
        SMSEduResultBO SendSmstoEmployee(SMSSupervisingDeptEmployeeBO inputSMS);
        SMSEduResultBO SendSMSFromSuperUser(Dictionary<string, object> dic);
        List<CustomSMSBO> GetCustomSMS(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int grade, string template, int periodType, bool forVNEN, List<string> lstContentTypeCode, int? nameDisplay,
            int? semester = null, int? month = null, int? year = null, DateTime? fromDate = null, DateTime? toDate = null);
        IQueryable<TeacherBO> GetListReceiverBySenderGroup(Dictionary<string, object> dic);
        List<SMSCommunicationBO> GetListNotification(Dictionary<string, object> dic, ref int totalRecord);
        SummarySMSBO GetSummarySMS(Dictionary<string, object> dic);
        List<SMSCommunicationBO> GetListNotificationTeacherExecl(Dictionary<string, object> dic);
        List<SMSCommunicationBO> GetListParentContract(Dictionary<string, object> dic);
        List<SMSCommunicationBO> GetListSMSForReport(int schoolId);
        List<ConsumptionReportBO> GetReportConsumption(int schoolId, DateTime startDate, DateTime endDate);
        List<SMSCommunicationBO> SearchUnitHistorySMS(Dictionary<string, object> dic, int superVisingDeptID, ref int totalRecord);
        ResultBO SendSmstoTeacherExternal(SMSSchoolTeacherBO inputSMS, string schoolName);
        ResultBO SendSMSExternal(Dictionary<string, object> dic, int AcademicYearId, int Semester, int Grade, string SchoolName);
    }
}