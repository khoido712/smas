/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IRewardCommentFinalBusiness 
    {
        List<RewardCommentFinal> GetListRewardCommentFinal(IDictionary<string, object> dic);
        List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel> GetCommentByPupil(IDictionary<string, object> dic);
        List<RewardCommentFinalBO> GetRewardFinalOfSchool(IDictionary<string, object> dic);
        List<SummedEvaluationBO> GetCapacityOrQuality(IDictionary<string, object> dic);
        void InsertOrUpdate(IDictionary<string,object> dic, List<RewardCommentFinal> lstRewardCommentFinal);
    }
}