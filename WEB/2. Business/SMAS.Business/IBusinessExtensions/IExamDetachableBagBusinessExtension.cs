/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamDetachableBagBusiness 
    {
        IQueryable<ExamDetachableBag> Search(IDictionary<string, object> search);
        //void InsertList(List<ExamDetachableBag> insertList);
        //void DeleteList(List<ExamDetachableBag> deleteList);

        ProcessedReport GetExamDetachableBagReport(IDictionary<string, object> dic);
        Stream CreateExamDetachableBagReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamDetachableBagReport(IDictionary<string, object> dic, Stream data, int appliedLevel);
        void InsertAllExamDetachableBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamCandenceBag> lstExamCandenceBagInsert, List<ExamDetachableBag> lstExamExamDetachableBagDelete, List<ExamDetachableBag> lstExamDetachableBagInsert);
        void DeleteAllExamDetachableBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamDetachableBag> lstExamExamDetachableBagDelete);
        Stream CreateTemplateDemo(IDictionary<string, object> dic);
        void InsertDeleteExamCandenceBag(List<ExamCandenceBag> lstExamCandenceBagDelete, List<ExamCandenceBag> lstExamCandenceBagInsert);
        void InsertDeleteExamDetachableBag(List<ExamDetachableBag> lstExamExamDetachableBagDelete, List<ExamDetachableBag> lstExamDetachableBagInsert);
    }
}