﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IMarkInputSituationBusiness 
    {
        List<MaxMarkNumberBO> GetMaxInputMarkOfClass(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID,int ClassID = 0);
        List<MarkInputSituationBO> GetMarkInputSituation(int subjectId, int semester, List<ClassProfile> lstClass, List<TeachingAssignment> lstTaAll, List<ClassSubject> lstCsAll, List<PupilOfClass> listPupilOfClass
            , List<MaxMarkNumberBO> listMarkAll, AcademicYear aca);
        Stream CreateMarkInputSituationReport(IDictionary<string, object> dic);
        ProcessedReport InsertMarkInputSituationReport(IDictionary<string, object> dic, Stream data);
        ReportMissingMarkSituationBO GetMissingMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID, int SubjectID);
        Stream CreateReportPupilNotEnoughMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID, int SubjectID, int AppliedLevelID = 0);
        ProcessedReport InsertReportPupilNotEnoughMark(IDictionary<string, object> dic, Stream data);
    }
}
