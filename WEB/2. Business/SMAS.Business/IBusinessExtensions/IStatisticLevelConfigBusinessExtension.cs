﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IStatisticLevelConfigBusiness
    {
        IQueryable<StatisticLevelConfig> Search(IDictionary<string, object> dic);
    }
}
