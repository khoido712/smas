﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SMAS.Business.IBusiness
{
    public interface IStatisticOfTertiaryStartYearBusiness : IGenericBussiness<ProcessedReport>
    {
        ProcessedReport InsertStatisticOfTertiaryStartYear(StatisticOfTertiaryStartYearBO entity, Stream data);
    }
}
