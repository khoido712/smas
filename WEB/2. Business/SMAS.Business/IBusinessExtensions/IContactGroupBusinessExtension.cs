/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IContactGroupBusiness 
    {
        IQueryable<SMS_TEACHER_CONTACT_GROUP> Search(Dictionary<string, object> dic);
        void CheckDefaultGroup(int schoolID, Guid guid);
        SMSEduResultBO InsertOrUpdateGroup(Dictionary<string, object> dic);
        /// <summary>
        /// Check user send sms
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="teacherID"></param>
        /// <param name="contactGroupID"></param>
        /// <returns></returns>
        bool CheckUserSendSMSInContactGroup(int schoolID, int teacherID, int contactGroupID);
        /// <summary>
        /// check xem giao vien co duoc quyen gui tin nhan hay khong
        /// </summary>
        /// <param name="contactGroupID"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        bool CheckTeacherInGroup(int contactGroupID, int teacherID);

        SMSEduResultBO CreateDefaultContactGroups(int schoolID, Guid guid);
        SMSEduResultBO SaveContactGroup(Dictionary<string, object> dic);
        SMSEduResultBO DeleteContactGroup(int contactGroupId, int schoolId);
    }
}