﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportNumberOfPupilByTeacherBusiness
    {
        IQueryable<ReportNumberOfPupilByTeacherBO> GetNumberOfPupilByTecher(int SchoolID, int AcademicYearID, int semester, int FacultyID);
        Stream CreateReportNumberOfPupilByTeacher(int SchoolID, int AcademicYearID, int semester, int FacultyID, List<int> lstTeacher,int AppliedLevelID = 0);
    }
}
