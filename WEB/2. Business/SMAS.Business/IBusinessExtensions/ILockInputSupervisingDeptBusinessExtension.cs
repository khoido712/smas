﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;


namespace SMAS.Business.IBusiness
{
    public partial interface ILockInputSupervisingDeptBusiness
    {
        void Insert(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID);
        int GetHierachyLevelIDByUserAccountID(int UserAccountID);
        void InsertAll(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID);
        void InsertTemp(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID);
        void InsertTempAll(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID, string AppliedString, string chkHK1, string chkHK2);
        void InsertTempAllSubSuper(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID, string AppliedString, string chkHK1, string chkHK2);
    }
}
