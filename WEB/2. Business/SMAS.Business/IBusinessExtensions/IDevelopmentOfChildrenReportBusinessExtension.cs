﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IDevelopmentOfChildrenReportBusiness
    {
         Stream CreateDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, int SchoolID, int AppliedLevel, int ReportType);
         IQueryable<EvaluationDevelopmentGroup> GetListEvaluationDevelopmentGroup(int AppliedLevel);
         IQueryable<DevelopmentByClassBO> GetListDevelopmentByClass(int SchoolID, int AcademicYearID, int ClassID, int? Semester, int AppliedLevel);
         ProcessedReport GetDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, int ReportType);
         ProcessedReport InsertDevelopmentOfChildrenReport(ExcelDevelopmentOfChildren entity, Stream data);
         string GetHashKey(ExcelDevelopmentOfChildren entity);
    }
}
