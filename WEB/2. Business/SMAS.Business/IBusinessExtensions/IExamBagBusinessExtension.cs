/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamBagBusiness 
    {
        IQueryable<ExamBag> Search(IDictionary<string, object> search);
        IQueryable<ExamBagBO> GetExamBagOfExaminations(long examinationsID);
        List<ExamBagBO> GetListExamBag(IDictionary<string, object> search);
        void CheckExamBag(long ExaminationsID, long ExamGroupID, int SubjectID, int[] Criteria, int LenghtCode, int Type);
        void DeleteExamBag(List<long> listExamBagID);
    }
}