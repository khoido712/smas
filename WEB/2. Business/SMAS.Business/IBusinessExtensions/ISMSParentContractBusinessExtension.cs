/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMS_SCHOOL.Utility.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSParentContractBusiness 
    {
        IQueryable<SMS_PARENT_CONTRACT> Search(Dictionary<string, object> dic);
        IQueryable<SMSParentContractBO> getSMSParentContract(Dictionary<string, object> dic);
        List<ContractSimplifyBO> getSMSParentContract(int schoolID, int year, int semester);
        List<PupilProfileBO> getSMSParentContract(int schoolID, int year, int semester, List<PupilProfileBO> lstPupil);
        int countNumberPeopleRegister(Dictionary<string, object> dic, List<PupilProfileBO> lstPupil);
        List<SMS_REGISTER_FREE> GetListRegisterSMSFree(Dictionary<string, object> dic);
        List<PupilProfileBO> SearchSMSParentRegister(Dictionary<string, object> dic, List<PupilProfileBO> pupilProfileBOList);
        List<SMS_PARENT_CONTRACT_DETAIL> getListContractDetailByContractID(int schoolID, int year, int semesterid, int contractID, bool excludeLimitPackage = false);
        List<SMSParentContractBO> GetListSMSParentContractDetailForExtra(int schoolId, int academicYearId, int classId, int year, int semester, int servicePackageId);
        List<SMSParentContractGroupBO> GetListSMSParentContract(int schoolID, int academicYearID, int appliedLevelID, int employeeID, int classID, int educationLevelID, string pupilCode, string pupilName, string mobilePhone,
            int year, int status, int currentPage, int pageSize, ref int totalRecord);
        List<SMSParentContractGroupBO> getListContractUnPaid(Dictionary<string, object> dic);
        List<SMSParentContractGroupBO> GetListExcelRegisted(int schoolID, int academicYearID, int year, int status, int educationLevelID, int semesterid, int classID, string pupilName, string pupilCode, string mobile, int grade, int employeeID);
        List<SMSParentContractGroupBO> GetListExcelRegis(List<PupilProfileBO> listPupil, int schoolID, int year, int status);
        List<SMSParentContractGroupBO> GetListExcelExtra(List<PupilProfileBO> listPupil, int schoolID, int year, int status, string mobile);
        List<SMSParentContractDetailBO> GetListSMSParentContractByShoolID(int schoolID, int year);
        Dictionary<string, object> SaveRegisContract(List<SMSParentContractBO> smsParentContractList, int year);
        bool ExistPaymentTypeContract(int PaymentType, int SchoolID, int Year);
        int getCurrentPaymentTypeOfSchool(int SchoolID, int Year);
        void UpdateDeferredContract(List<SMSParentContractBO> lstParentContract, int year);
        bool checkExistUnpaidSubscriber(List<SMSParentContractBO> lstParentContract);
        IQueryable<PupilProfileBO> GetSMSParentContract(IDictionary<string, object> dic);
        void CreateListSParentAccount(List<SParentAccountBO> lstSParent);
        void UpdateClassIDToSMSParentContract(int NewClassID, int OldClassID, int SemesterID, int AcademicYearID, int SchoolID, int PupilID);
        void GetListSMSParentContractClass(int SchoolID, int AcademicYearID, int SemesterID, List<int> lstClassID);
    }
}