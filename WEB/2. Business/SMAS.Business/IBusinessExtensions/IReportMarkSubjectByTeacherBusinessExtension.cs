﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportMarkSubjectByTeacherBusiness
    {
        string GetHashKey(ReportMarkSubjectBO ReportMarkSubject);
        ProcessedReport InsertReportMarkSubjectByTeacher(ReportMarkSubjectBO entity, Stream data);
        ProcessedReport GetReportMarkSubjectByTeacher(ReportMarkSubjectBO entity);
        Stream CreateReportMarkSubjectByTeacher(ReportMarkSubjectBO entity);
        IQueryable<ClassProfileTempBO> GetListClassByTeacher(IDictionary<string, object> dic = null);
    }
}
