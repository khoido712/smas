/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IVillageBusiness 
    {
        IQueryable<Village> Search(IDictionary<string, object> dic);
        void Delete(int VillageID);

    }
}