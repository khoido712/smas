/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamInputMarkBusiness 
    {
        IQueryable<ExamInputMark> Search(IDictionary<string, object> dic);
        IQueryable<ExamInputMarkBO> SearchBO(IDictionary<string, object> dic);
        IQueryable<ExamInputMarkBO> SearchForResultSearch(IDictionary<string, object> dic);
        IQueryable<ExamInputMark> SearchForTransfer(int? AcademicYearID, int? SchoolID, long ExaminationsID);
        string GetHashKey(IDictionary<string, object> dic);

        ProcessedReport GetExamInputMarkReport(IDictionary<string,object> dic);
        ProcessedReport InsertExamInputMarkReport(IDictionary<string,object> dic, Stream data, int appliedLevel);
        Stream CreateExamInputMarkReport(IDictionary<string,object> dic);

        ProcessedReport GetExamMarkByClassReport(IDictionary<string, object> dic);
        Stream CreateExamMarkByClassReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkByClassReport(IDictionary<string, object> dic, Stream data, int appliedLevel);
        Stream CreateExamMarkByClassReportAll(IDictionary<string, object> dic);

        ProcessedReport GetExamMarkByRoomReport(IDictionary<string, object> dic);
        Stream CreateExamMarkByRoomReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkByRoomReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamMarkByClassStatisticReport(IDictionary<string, object> dic);
        Stream CreateExamMarkByClassStatisticReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkByClassStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamMarkByRoomStatisticReport(IDictionary<string, object> dic);
        Stream CreateExamMarkByRoomStatisticReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkByRoomStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamMarkBySubjectStatisticReport(IDictionary<string, object> dic);
        Stream CreateExamMarkBySubjectStatisticReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkBySubjectStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        // Input Mark
        List<ExamGroup> GetGroupAssignment(IDictionary<string, object> search);
        List<ExamSubjectBO> GetSubjectAssignment(IDictionary<string, object> search);
        List<ExamInputMarkBO> GetListInputMarkCandenceBag(IDictionary<string, object> search);
        List<ExamInputMarkBO> GetListInputMarkExamRoom(IDictionary<string, object> search);
        void InputMark(IDictionary<string, object> info, int[] ListPupilID, long[] CheckPupilID, string[] ExamMark, string[] ActualMark, int[] ClassID);

        // Report Nhap diem theo so phach
        ProcessedReport GetReportDetachableBag(IDictionary<string, object> dic);
        Stream CreateReportDetachableBag(IDictionary<string, object> dic);
        ProcessedReport InsertReportDetachableBag(IDictionary<string, object> dic, Stream data, int appliedLevel);
        // Report Nhap diem theo phong thi
        ProcessedReport GetReportExamRoom(IDictionary<string, object> dic);
        Stream CreateReportExamRoom(IDictionary<string, object> dic);
        ProcessedReport InsertReportExamRoom(IDictionary<string, object> dic, Stream data, int appliedLevel);
        // Report Nhap diem theo ma hoc sinh
        ProcessedReport GetReportPupilCode(IDictionary<string, object> dic);
        Stream CreateReportPupilCode(IDictionary<string, object> dic);
        ProcessedReport InsertReportPupilCode(IDictionary<string, object> dic, Stream data, int appliedLevel);
        // Report Nhap diem theo so bao danh
        ProcessedReport GetReportExamineeCode(IDictionary<string, object> dic);
        Stream CreateReportExamineeCode(IDictionary<string, object> dic);
        ProcessedReport InsertReportExamineeCode(IDictionary<string, object> dic, Stream data, int appliedLevel);

        ProcessedReport GetExamMarkByTeacherStatisticReport(IDictionary<string, object> dic);
        Stream CreateExamMarkByTeacherStatisticReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamMarkByTeacherStatisticReport(IDictionary<string, object> dic, Stream data, int appliedLevel);

        //Import file Excel
        List<ExamPupilInfoBO> GetListPupilContains(IDictionary<string, object> search);
        List<ExamDetachableBagBO> GetListDetachableContains(IDictionary<string, object> search);
        void ImportInputMark(IDictionary<string, object> info, List<ExamInputMarkBO> listInput, int markInputType, ref int successCount);

        /// <summary>
        /// check mon hoc co phai la mon tinh diem khong, neu khong thi thong bao khong ho tro mon nhan xet
        /// </summary>
        /// <param name="examinationsID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="partition"></param>
        /// <param name="subjectID"></param>
        /// <returns></returns>
        bool CheckExamSubjectBySubjectIDReportPrimary(long examinationsID, int schoolID, int academicYearID, int partition, int subjectID);
        ProcessedReport InsertSummanyMarkReport(IDictionary<string, object> dic, Stream data);
        ProcessedReport GetSummanyMarkReport(IDictionary<string, object> dic);
    }
}