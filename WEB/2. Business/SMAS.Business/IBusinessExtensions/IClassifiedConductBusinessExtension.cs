﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IClassifiedConductBusiness
    {
        string GetHashKey(ClassifiedConduct entity);
        ProcessedReport InsertClassifiedConduct(ClassifiedConduct entity, Stream data);
        ProcessedReport GetClassifiedConduct(ClassifiedConduct entity);
        Stream CreateClassifiedConduct(ClassifiedConduct entity);
    }
}
