﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IHealthTestReportBusiness
    {

        string GetHashKeyForSchool(ExcelHealthTestReportBO entity, int ReportType);
        ProcessedReport GetHealthTestReportForSchool(ExcelHealthTestReportBO entity, int ReportType);
        ProcessedReport InsertHealthTestReportForSchool(ExcelHealthTestReportBO entity, Stream data, int ReportType);
        Stream CreateHealthTestReportForSchool(ExcelHealthTestReportBO entity, int ReportType);
        string GetHashKeyPGD(ExcelHealthTestReportPGDBO entity);
        ProcessedReport GetHealthTestReportPGD(ExcelHealthTestReportPGDBO entity);
        ProcessedReport InsertHealthTestReportPGD(ExcelHealthTestReportPGDBO entity, Stream data);
        Stream CreateHealthTestReportPGD(ExcelHealthTestReportPGDBO entity);

    }
}
