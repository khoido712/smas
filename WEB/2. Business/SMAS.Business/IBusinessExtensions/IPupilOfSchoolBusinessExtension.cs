/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPupilOfSchoolBusiness 
    {

        void InsertPupilOfSchool(int UserID, PupilOfSchool PupilOfSchool);
        void UpdatePupilOfSchool(int UserID, PupilOfSchool PupilOfSchool);
        void DeletePupilOfSchool(int PupilOfSchoolID, int SchoolID);
     //   IQueryable<PupilOfSchool> Search(IDictionary<string, object> dic);
        IQueryable<PupilOfSchool> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
   
    }
}