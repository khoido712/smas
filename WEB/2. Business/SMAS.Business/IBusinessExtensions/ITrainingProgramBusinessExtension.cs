/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.Business.Business;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface ITrainingProgramBusiness 
    {
        IQueryable<TrainingProgram> Search(IDictionary<string, object> dic);
    }
}