/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IInvigilatorBusiness 
    {
        IQueryable<Invigilator> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListInvigilatorID);
        void InsertAll(int SchoolID, int AppliedLevel, int ExaminationID, List<int> ListEmployeeID);
    }
}