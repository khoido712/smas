/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISchoolWeekBusiness 
    {
        void Insert(List<SchoolWeek> schoolWeek);
        void Delete(List<int> lsSchoolWeekID);
       // List<SchoolWeek> getListSchoolWeek(IDictionary<string, object> SearchInfo, bool autoGenerateWeek);
        List<SchoolWeek> getListSchoolWeekForEdit(IDictionary<string, object> SearchInfo, bool autoGenerateWeek);
        void AutoGenerateWeek(IDictionary<string, object> SearchInfo);
        IQueryable<SchoolWeek> Search(IDictionary<string, object> dic);
        bool AppliedEducationAndSchool(IDictionary<string, object> dic, List<string> listFromDate, List<string> listToDate);
    }
}