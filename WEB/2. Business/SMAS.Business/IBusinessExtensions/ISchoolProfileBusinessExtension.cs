/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
namespace SMAS.Business.IBusiness
{
    public partial interface ISchoolProfileBusiness
    {
        IQueryable<SchoolProfile> Search(IDictionary<string, object> dic);
        List<int> GetListAppliedLevel(int SchoolID);
        List<RESTORE_DATA_DETAIL> Delete(int SchoolProfileID, int AdminID, RESTORE_DATA objRes);
        void Insert(SchoolProfile schoolProfile, List<int> lstRole, string UserName, bool IsApproved, List<int> lsPropertyOfSchool, bool IsAutoCode, int account, int roleID, string password = "");
        void Update(SchoolProfile schoolProfile, List<int> SchoolPropertyCatID, bool IsForgetPassword, bool IsApproved, bool IsActiveSMAS, bool IsSystemAdmin, int roleID, string password = "");

        List<SchoolProfileAPI> GetSchoolByAPI(IDictionary<string, object> dic);
        //List<IDictionary<string, object>> GetStatisticForClassAndPupilOfPrimaySchool(int schoolId, int yearId);
        List<int> GetListSchoolID(IDictionary<string, object> dic);
        List<SchoolBO> GetListSchool(IDictionary<string, object> dic, ref int total);
        List<SchoolBO> GetListSchoolSP(IDictionary<string, object> dic, IQueryable<SMS_SERVICE_PACKAGE_UNIT> iqSp, bool? applied, ref int total);
        bool IsSMSParentActive(int SchoolID);
        bool IsSMSTeacherActive(int schoolID);
        SchoolBO GetUnitByAdminAccount(string UserName);
        List<SchoolBO> GetSchoolByRegionNotPaging(int EducationGrade, int SupervisingDeptID, string SchoolName);
        List<HistoryReceiverBO> GetListSchoolByListID(int SupervisingDeptID, List<int> SchoolIDList);
        List<SchoolBO> GetSchoolByRegion(int EducationGrade, int SupervisingDeptID, string SchoolName
            , int PageNum, int PageSize, ref int total);
        string GetUserNameBySchoolID(int schoolID);
        IDictionary<int, string> GetListUserNameByListUserID(List<int> lstUserID);
        IDictionary<Guid, string> GetListUserNameCBByListUserID(List<Guid> lstUserID);
        string GetUserNameByUserAccount(int UserAccountID);
    }
}


