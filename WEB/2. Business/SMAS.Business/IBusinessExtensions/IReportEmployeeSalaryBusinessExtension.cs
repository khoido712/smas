﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportEmployeeSalaryBusiness : IGenericBussiness<ProcessedReport>
    {
        IQueryable<ReportEmployeeSalaryBO> GetListEmployeeSalaryBySchool(IDictionary<string, object> SearchInfo);
        List<ReportEmployeeSalaryBO> GetListEmployeeIncreaseSalaryBySchool(DateTime FromDate, DateTime ToDate, int SchoolID, int AppliedLevel);
        Stream CreateReportEmployeeSalary(IDictionary<string, object> SearchInfo);
        Stream CreateReportEmployeeIncreaseSalary(DateTime FromDate, DateTime ToDate, int SchoolID, int AppliedLevel);
    }
}
