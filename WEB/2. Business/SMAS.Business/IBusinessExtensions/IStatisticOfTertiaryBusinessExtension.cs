/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IStatisticOfTertiaryBusiness 
    {
        IQueryable<StatisticOfTertiaryBO> GetListSchoolReported(int ProvinceID, int Year, int DistrictID, int? Status);
        IQueryable<StatisticOfTertiary> SearchBySchool(int SchoolID,IDictionary<string, object> SearchInfo=null);

        Stream CreateSchoolReport(int AcademicYearID, out string reportName);
        Stream CreateProvinceReport(int SupervisingDeptID, int ProvinceID, int Year, out string reportName);
        void InsertOrUpdate(int SchoolID, int AcademicYearID, StatisticOfTertiary sot, List<StatisticSubcommitteeOfTertiary> lstStatisticSubCommittee, List<StatisticSubjectOfTertiary> lstStatisticSubject);
    }
}