/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamSubjectBusiness 
    {
        IQueryable<ExamSubject> Search(IDictionary<string, object> search);
        IQueryable<ExamSubjectBO> GetListExamSubject(IDictionary<string, object> search);
        void InsertExamSubject(List<ExamSubject> listExamSubject, int checkAll);
        void UpdateExamSubject(ExamSubject examSubject, int checkAll);
        void DeleteExamSubject(List<long> examSubjectIDList);
        IQueryable<ExamSubjectBO> FindExamSubject(long ExamSubjectID);

        /// <summary>
        /// Lay danh sach mon thi
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="applieLevel"></param>
        /// <returns></returns>
        List<ExamSubjectBO> GetExamSubject(IDictionary<string, object> dic, int? applieLevel);
    }
}