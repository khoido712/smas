/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.IBusiness
{
    public partial interface IMarkRecordBusiness
    {
        List<MarkRecordObject> GetPupilInMarkRecord(IDictionary<string, object> dic);
        //List<MarkRecord> SearchMarkRecord(IDictionary<string, object> dic);
       // void InsertMarkRecord(int UserID, List<MarkRecord> ListMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, int SubjectID);
        bool InsertMarkRecord(int UserID, List<MarkRecord> ListMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? PeriodID, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, int SubjectID,int? EmployeeID, int? Year = null);
        //void UpdateMarkRecord(int UserID,MarkRecord lstMarkRecord);
        IQueryable<MarkRecordBO> GetMardRecordOfClass(IDictionary<string, object> dic);

        /// <summary>
        /// lay len diem cua tat ca cac mon hoc trong lop
        /// <author>hungnd 20/01/2014</author>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<MarkRecordWSBO> GetMardRecordOfAllSubjectInClass(IDictionary<string, object> dic);

        void InsertMarkRecordOfPrimary(List<MarkRecord> lstMarkRecordOfPrimary);

        void InsertMarkRecordDKOfPrimary(int User, List<MarkRecord> lstMarkRecordDKOfPrimary, List<SummedUpRecord> LstSummedUpRecord, int Semester);
        IQueryable<MarkRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        void ImportMarkRecord(int UserID, List<MarkRecord> lstMarkRecord);
        void DeleteMarkRecord(int UserID, long MarkRecordID, int? SchoolID);
        string GetLockMarkTitle(int schoolid, int academicyearid, int classid, int semester, int subjectid = 0, int periodid = 0);
        string GetLockMarkTitleByEducation(int schoolid, int academicyearid, int educationlevelid, int semester);
        Dictionary<int, string> GetLockMarkTitleBySubject(int schoolid, int academicyearid, int classid, int semester, int subjectid = 0);
        string GetMarkTitle(int PeriodID);
        void InsertMarkRecordTXOfPrimary(List<MarkRecord> ListMarkRecord);
        string GetHashKey(MarkRecord MarkRecord);
        int CreatExcelMarkRecord(string InputParameterHashKey, IDictionary<string, object> SearchInfo);
        Stream ExportReport(int SchoolID, int AcademicYearID, int Semester, int ClassID, int PeriodID, int SubjectID, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null);
        Stream ExportReportForSemester(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null);
        bool CheckMarkTX(string Mark);
        bool CheckMarkDX(string Mark);
        string GetMarkSemesterTitle(int SemesterID);
        string GetMarkSemesterTitle(int SchoolID, int AcademicYearID, int Semester);
        List<List<PupilForImportBO>> GetListPupilForImport(int UserAccountID, int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int Semester, string MarkTitle);
        List<SubjectCat> GetListSubject(int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int Semester, string MarkTitle);
        IQueryable<MarkRecord> GetMarkRecordToDelete(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID);
        string GetMarkTitleForSemester(int SchoolID, int AcademicYearID, int Semester);

        IDictionary<string, object> GetMaxMarkRecord(int AcademicYearID, int SchoolID, int Semester, int? PeriodID = null);
        int GetCountPupilMarkRecordForClass(int SchoolID, int AcademicYearID, int EducationLevelID, int? ClassID, int SubjectID, string TitleMark);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        void DeleteMarkRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);

        IEnumerable<MarkRecordNewBO> GetMardRecordOfClassNew(IDictionary<string, object> dic);
        //ham phuc vu webservice tinh diem tu dong
        string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID);

        string GetLockMarkTitlePrimary(int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int Semester);

        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);
        Stream ExportAnySubject(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null);
        Stream ExportSubjectOfClass(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null);

        Stream ExportReportForSemesterByListSubjectID(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, List<int> listSubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null);
        Stream ExportReportForSemesterByListClassId(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, List<int> listClassID, int SubjectID, int subjectIDIncrease, int isCommenting, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null);
        Stream ExportExcelToMarkOrJuge(IDictionary<string, object> dicParam, out string FileName, Dictionary<int, bool> dicDisplay = null, bool? isAdmin = null);

        void InsertMarkRecordByListSubject(int UserID, List<MarkRecord> LstMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, int ClassID, List<int> listSubjectID,int? EmployeeID, int? PeriodID = null, int? Year = null);
        void InsertMarkRecordByListClass(int UserID, List<MarkRecord> LstMarkRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int Grade, List<int> listClassID, int SubjectID,int? EmployeeID,int? PeriodID = null, int? Year = null);
        Dictionary<int, string> GetLockMarkTitleBySchoolID(int schoolid, int academicyearid, int semester, int classid = 0, int subjectid = 0);
        void InsertMarkRecordAll(int UserID, List<MarkRecord> ListMarkRecord, List<SummedUpRecord> lstSummedUpRecord, List<int> lstPupilExempted, IDictionary<string, object> dicParam);
        void SP_DeleteMarkRecord(int academicYearID, int schoolID, int classID, int semesterID, int subjectID, int? isHistory, int? periodID, List<int> listPupilIdDelete,string strMarkTitle = null);
        void SP_DeleteJudgeRecord(int academicYearID, int schoolID, int classID, int semesterID, int subjectID, int? isHistory, int? periodID, List<int> listPupilIdDelete,string strMarkTitle = null);
        void SP_DeleteSummedUpRecord(int? isHistory, List<int> listPupilId, int peroid, int subjectId, int classId, int semester, int shoolID, int academicYearId);
        /// <summary>
        /// Tinh TBM cho mon hoc theo mon tang cuong, khong tang cuong
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="PeriodID"></param>
        /// <param name="strMarkTitle"></param>
        /// <param name="classSubject"></param>
        //void SummedUpMark(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, int? PeriodID, String strMarkTitle, ClassSubject classSubject);

        /// <summary>
        /// Chiendd1: 24/03/2016. Thuc hien tinh diem TBM va tong ket diem tu dong. Chi ap dung cho mon tinh diem
        /// </summary>
        /// <param name="lstMark"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="lstClassSubject"></param>
        void SummedUpMarkAndSummary(List<MarkRecord> lstMark, int schoolId, int academicYearId, int semesterId);
        /// <summary>
        /// Chiendd: 25/05/2016. Thuc hien xoa diem theo markrecord ID, su dung SP
        /// isHistory=0: Xoa du lieu bang hien tai, 1: Xoa du lieu bang lich su
        /// listMarkRecordId: Danh sach can xoa: Se thuc hien xoa 500 ban ghi 1 lan de dam bao khi truyen danh sach vao Store khong bi loi
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="schoolID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="subjectID"></param>
        /// <param name="isHistory"></param>
        /// <param name="listMarkRecordId"></param>
        //void DeleteMarkRecord(int academicYearID, int schoolID, int classID, int semesterID, int subjectID, int? isHistory, List<long> listMarkRecordId);
        void ImportFromOtherSystem(List<ImportFromOtherSystemBO> lstBO, int schoolId, int academicYearId, int appliedId, int classId, int subjectId, int semester, bool isComment, int? employeeId);

        /// <summary>
        /// tinh TBM hoc ky va tat ca cac dot trong ky
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="semesterId"></param>
        /// <param name="classSubject"></param>
        void SummedMarkAllPeriodAndSemester(List<int> lstPupilID, int schoolId, int academicYearId, int semesterId, ClassSubject classSubject, string strMarkTitle, int? PeriodId);

        #region SNAS GOP EDU
        List<PupilMarkBO> GetMarkRecordByClass(int SchoolID, List<int> lstClassID, int EducationLevel,
             int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester);
        List<PupilMarkBO> GetListMarkRecordSemester(int SchoolID, List<int> lstClassID,List<int> lstPupilID, int AcademicYearID, int Semester, int grade);
        List<PupilMarkBO> GetListMarkRecordTime(int SchoolID, int ClassID, int AcademicYearID, int PeriodID);
        List<PupilMarkBO> GetListMarkRecordTimeByAllClass(List<int> lstClassID, List<int> lstPupilID, int SchoolID, int AcademicYearID, int PeriodID);
        List<PupilMarkBO> GetListExamMarkSemester(List<PupilProfileBO> lstPupil, int SchoolID, List<int> lstClassID, int AcademicYearID, int Semester, int grade);
        #endregion
    }
}
