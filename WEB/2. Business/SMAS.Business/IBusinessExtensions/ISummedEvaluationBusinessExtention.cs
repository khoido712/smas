﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface ISummedEvaluationBusiness
    {
        /// <summary>
        /// Them moi hoac thuc hien cap nhat du lieu. Chi ap dung cho tab CLGD
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdate(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate,List<ActionAuditDataBO> lstActionAuditData,ref ActionAuditDataBO objActionAuditBO);
        /// <summary>
        /// Mapping column tu Entity sang table
        /// </summary>
        /// <returns></returns>
        IDictionary<string, object> ColumnMappings();
        /// <summary>
        /// lay nhan xet danh gia cua hoc sinh theo lop
        /// </summary>
        /// <param name="dic"></param>
        /// <author>Hoantv5</author>
        /// <returns></returns>
        List<SummedEvaluationBO> GetSummedEvaluationByClass(IDictionary<string, object> dic);

        /// <summary>
        /// Them moi hoac thuc hien cap nhat du lieu tat ca cac mon cua 1 lop vao CSDL
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdatebyClass(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate);

        /// <summary>
        /// Chuyen diem thi vao so diem
        /// </summary>
        /// <param name="insertList"></param>
        /// <param name="updateList"></param>
        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);

        IQueryable<SummedEvaluationBO> Search(IDictionary<string, object> dic);

        IQueryable<SummedEvaluation> getListSummedEvaluationByListSubject(IDictionary<string, object> dic);

        List<string> GetListComment(IDictionary<string, object> dic);
    }
}
