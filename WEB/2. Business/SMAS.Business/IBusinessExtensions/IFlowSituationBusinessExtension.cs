﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IFlowSituationBusiness
    {
        ReportDefinition GetReportDefinitionOfClassMovement(FlowSituationBO flowSituationBO);
        string GetHashKeyForPeriod(FlowSituationBO entity);
        ProcessedReport ExcelGetClassMovement(FlowSituationBO entity);
        ProcessedReport ExcelInsertClassMovement(FlowSituationBO entity, Stream data);
        Stream ExcelCreateClassMovement(FlowSituationBO entity);
        ProcessedReport ExcelGetSchoolMovement(FlowSituationBO entity);
        ProcessedReport ExcelInsertSchoolMovement(FlowSituationBO entity, Stream data);
        Stream ExcelCreateSchoolMovement(FlowSituationBO entity);
        ReportDefinition GetReportDefinitionOfSchoolMovement(FlowSituationBO flowSituationBO);
        ReportDefinition GetReportDefinitionOfMovementAcceptance(FlowSituationBO flowSituationBO);
        ProcessedReport ExcelGetMovementAcceptance(FlowSituationBO entity);
        ProcessedReport ExcelInsertMovementAcceptance(FlowSituationBO flowSituationBO, Stream data);
        Stream ExcelCreateMovementAcceptance(FlowSituationBO entity);
        ReportDefinition GetReportDefinitionOfPupilLeavingOff(FlowSituationBO flowSituationBO);
        ProcessedReport ExcelGetPupilLeavingOff(FlowSituationBO flowSituationBO);
        ProcessedReport ExcelInsertPupilLeavingOff(FlowSituationBO flowSituationBO, Stream data);
        Stream ExcelCreatePupilLeavingOff(FlowSituationBO entity);
        //Diên chính sách
        ReportDefinition GetReportDefinitionOfPolicyTarget(FlowSituationBO flowSituationBO);
        string GetHaskKeyForPolicyTarget(FlowSituationBO flowSituationBO);
        ProcessedReport ExcelGetPolicyTarget(FlowSituationBO flowSituationBO);
        ProcessedReport ExcelInsertPolicyTarget(FlowSituationBO flowSituationBO, Stream data);
        Stream ExcelCreatePolicyTarget(FlowSituationBO entity);
    }
}
