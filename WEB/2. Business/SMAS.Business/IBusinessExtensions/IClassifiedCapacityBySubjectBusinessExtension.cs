﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IClassifiedCapacityBySubjectBusiness
    {
        string GetHashKey(ClassifiedCapacityBySubject entity);
        ProcessedReport InsertClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity, Stream data);
        ProcessedReport GetClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity);
        Stream CreateClassifiedCapacityBySubject(ClassifiedCapacityBySubject entity);
    }
}
