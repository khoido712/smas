﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;

namespace SMAS.Business.IBusiness
{
    public partial interface IEvaluationCommentsHistoryBusiness
    {
        /// <summary>
        /// Lay danh gia nhan xet cua hoc sinh trong lop
        /// </summary>
        /// <param name="schoolID">ID truong</param>
        /// <param name="academicYearID">ID nam hoc</param>
        /// <param name="classID">id lop hoc</param>
        /// <param name="semesterID">hoc ky</param>
        /// <param name="subjectID">Tieu chi danh gia: Neu la HDGD thi la subjectId</param>
        /// <param name="TypeOfTeacher">Loai giao vien: 1: GVBM, 2:GVBM</param>
        /// <param name="evaluationID">Mat danh gia</param>
        /// <returns></returns>
        List<EvaluationCommentsBO> GetEvaluationCommentsHistoryByClass(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID);
        /// <summary>
        /// Thuc hien them moi hoac cap nhat nhan xet danh gia vao History
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdateHistory(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, ref List<ActionAuditDataBO> lstActionAuditData);

        /// <summary>
        /// Xoa du lieu trong bang lich su. Thuc chat la cap nhat du lieu ve gia tri null
        /// </summary>
        /// <param name="IDList"></param>
        /// <param name="dic"></param>
        /// <param name="isRetest"></param>
        void BulkDeleteEvaluationCommentsHistory(List<long> IDList, IDictionary<string, object> dic, bool isRetest, ref List<ActionAuditDataBO> lstActionAuditData);
        /// <summary>
        /// Insert hoac update nhan xet cap 1
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        void InsertOrUpdateEvaluationCommentsByPupilID(int MonthID, EvaluationComments insertOrUpdateObj, ref ActionAuditDataBO objAc);

        List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(int MonthID, EvaluationComments insertOrUpdateObj);

        /// <summary>
        /// xoa comments trong history
        /// </summary>
        /// <param name="idList"></param>
        /// <param name="idListSummedEnding"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="semesterID"></param>
        /// <param name="classID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="monthID"></param>

        List<EvaluationCommentActionAuditBO> DeleteEvaluationCommentsHistory(List<long> idList, List<long> idListSummedEnding, int schoolID, int academicYearID, int semesterID, int classID, int evaluationCriteriaID, int typeOfTeacher, int educationLevelID, int evaluationID, int monthID);

        /// <summary>
        /// insert or update history comments
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        /// <param name="listInsertOrUpdateSummed"></param>
        /// <param name="listInsertOrUpdateSummedEnding"></param>
        List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsHistory(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationCommentsHistory> listInsertOrUpdate, List<SummedEvaluationHistory> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding);

        /// <summary>
        /// insert import (GVCN)
        /// </summary>
        /// <param name="monthID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        /// <param name="listInsertOrUpdateSummed"></param>
        /// <param name="listInsertOrUpdateSummedEnding"></param>
        void InsertOrUpdateEvaluationCommentsImportHistory(int monthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationCommentsHistory> listInsertOrUpdate, List<SummedEvaluationHistory> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding);
        /// <summary>
        /// Import theo doi CLGD GVBM vao bang History
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdateEducationQualityImport(IDictionary<string, object> dic, List<EvaluationComments> listInsertOrUpdate);

        /// <summary>
        /// Lay du lieu tu bang lich su de phuc vu xuat excel
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="subjectID"></param>
        /// <param name="TypeOfTeacher"></param>
        /// <param name="evaluationID"></param>
        /// <returns></returns>
        List<EvaluationCommentsBO> getListEvaluationCommentsHistoryBySchoolExport(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID);

        /// <summary>
        /// Thuc hien them moi hoac cap nhat tat cac cac mon hoc trong lop vao bang lich su
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdateHistorybyClass(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate);
        /// <summary>
        /// Lay danh sach nhan xet danh gia cua tat cac cac mon hoc cua lop theo cac tieu chi:hoc ky cho GVBM
        ///
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsHistorybyClass(int classId, IDictionary<string, object> dic, List<int> lstSubject);

        /// <summary>
        /// Lay danh sach nhat xet nhan xet danh gia cac cac thang cua lop cua GVCN
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsMonthbyClassofHeadTeacher(int classId, IDictionary<string, object> dic);

        /// <summary>
        /// Lay danh sach nhan xet danh gia hoc ky cua GVCN
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <param name="lstSubject"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClassAndHeadTeacher(int classId, IDictionary<string, object> dic, List<int> lstSubject);

        IQueryable<EvaluationCommentsHistory> getListEvaluationByListSubject(IDictionary<string, object> dic);

        string CheckLockMark(int classID, int schoolID, int academicYearID);

        void CopyDataEvaluationHitory(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect, ref List<ActionAuditDataBO> lstActionAuditData);

        List<EvaluationCommentActionAuditBO> CopyDataEvaluationHistoryForHeadTeacher(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect);

        List<string> GetListCommentHistory(IDictionary<string, object> dic);
    }
}
