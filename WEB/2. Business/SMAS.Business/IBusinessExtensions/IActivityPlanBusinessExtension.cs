/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IActivityPlanBusiness 
    {
        void InsertOrUpdateActivityPlan(List<int> listClass, ActivityPlan ap, List<Activity> lstActivity);
        int Insert(ActivityPlan ap);
        void Delete(int UserAccountID, int ActivityPlanID, int SchoolID);
        IQueryable<ActivityPlanBO> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
    }
}