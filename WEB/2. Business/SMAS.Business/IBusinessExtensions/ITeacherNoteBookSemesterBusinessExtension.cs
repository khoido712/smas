/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ITeacherNoteBookSemesterBusiness 
    {
        IQueryable<TeacherNoteBookSemester> Search(IDictionary<string, object> dic);
        List<TeacherNoteBookSemester> GetListTeacherNoteBookSemester(IDictionary<string, object> dic);
        void InsertOrUpdate(List<TeacherNoteBookSemester> lstTeacherNoteBookSemster, 
            IDictionary<string, object> dic, 
            List<ActionAuditDataBO> lstActionAuditData, 
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB,
            ref ActionAuditDataBO objActionAuditBO);
        void DeleteTeacherNoteBookSemester(IDictionary<string, object> dic, List<ActionAuditDataBO> lstActionAuditData, ref ActionAuditDataBO objActionAuditBO, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail);
        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);
        void InsertOrUpdateToMobile(TeacherNoteBookSemester objTeacherNoteBookSemster, IDictionary<string, object> dic);
    }
}
