/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ILockTrainingBusiness
    {
        /// <summary>
        /// Lay danh sach khoa diem danh vi pham theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<LockTraining> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null);

        /// <summary>
        /// Tim kiem khoa diem danh, vi pham theo lop hoc
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<LockTraining> SearchByClass(int ClassID, IDictionary<string, object> dic = null);

        /// <summary>
        /// Cap nhat thong tin vao CSDL
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="listLockTraining"></param>
        void UpdateOrInSert(int SchoolID, int AcademicYearID, List<LockTraining> listLockTraining);
    }
}