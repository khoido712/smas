/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using System.Web.Security;
namespace SMAS.Business.IBusiness
{
    public partial interface IUserAccountBusiness
    {
        UserAccount GetUserByProviderUserKey(Guid GUID);
        IQueryable<UserAccount> GetUsersCreatedBy(int creatorID);
        IQueryable<UserAccount> GetUsersAdminCreatedBy(int creatorID);
        IQueryable<UserAccount> GetUsersNormalCreatedBy(int creatorID);
        IQueryable<UserAccount> GetOperationalUsers();
        IQueryable<UserAccount> SearchUser(IDictionary<string, object> dic);
        Employee MapFromUserAccountToEmployee(int userAccountID);
        UserAccount MapFromEmployeeToUserAccount(int employeeID);
        bool DeActiveAccount(int userAccountID);
        bool ActiveAccount(int userAccountID);
        bool IsOperationalUser(int userAccountID);
        bool IsAdmin(int UserAccountID);
        bool IsSchoolAdmin(int UserAccountID);
        bool IsSupervisingDeptAdmin(int UserAccountID);
        //MembershipCreateStatus CreateUser(string userName, int CreatedUserID, bool IsAdmin, int? EmployeeID);
        IQueryable<UserAccount> SearchUserByDept(IDictionary<string, object> dic);
        bool DeleteAccount(int UserAccountID);
        int? CreateDepartmentUser(string userName, int CreatedUserID, bool IsAdmin, int? EmployeeID, int RoleID, bool isActive = true,string password = "");

        void UpdateUser(int UserAccountID, string newUserName);
        void UpdateUser(int UserAccountID, string newUserName, bool isResetPass, int? employeeID, bool isActive,string Password = "");
        int? CreateAdminSupervisingDept(string userName, int CreatedUserID, int RoleID, bool IsActive,string password = "");
        int? CreateAdminSchool(string userName, int CreatedUserID, List<int> lstRole, int roleID,string password = "");
        void ResetPassword(int UserAccountID, string newPassword = "");
        bool IsSystemAdmin(int UserAccountID);
        int? CreateUser(string userName, int CreatedUserID, bool IsAdmin, int? EmployeeID, out  MembershipCreateStatus createStatus, string password = "", bool? isParrent = null);
        bool ChangePassword(int UserAccountID, string NormalPass);
        #region ko co trong ngvu
        List<UserAccount> GetUsersByCreator(int creatorID, bool isAdmin);
        List<CustomUserAcc> MapFromUserAccountToCustomUserAcc(List<UserAccount> lsAcc);
        #endregion

        bool DeActiveByEmployeeID(int EmployeeID);
        void UpdateSynchronize(int UserID, int status);
        void UpdateSynchronize(string userName, int status);
        void ChangePasswordByListUseraccount(IDictionary<int, string> lstUserAccount);
        void ResetPasswordByListUserAccount(List<int> lstUserAccountID);

    }
}