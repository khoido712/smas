/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilRankingHistoryBusiness
    {
        /// <summary>
        /// Thuc hien insert du lieu vao history, 
        /// </summary>
        /// <param name="lstPupilRanking"></param>
        void InsertOrUpdateList(List<PupilRanking> lstPupilRankingInsert, List<PupilRanking> lstPupilRankingUpdate);

        /// <summary>
        /// Lay du lieu trong bang lich su
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<PupilRankingHistory> SearchBySchool(int SchoolID, IDictionary<string, object> dic);

        /// <summary>
        /// Thuc hien trong history
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="Semester"></param>
        /// <param name="IsSecondSemesterToSemesterAll"></param>
        void RankingPupilConductHistory(int UserID, List<PupilRanking> lstPupilRanking, int Semester, bool IsSecondSemesterToSemesterAll);

        /// <summary>
        /// RankingPupilOfClass: thuc hien tong ket diem va xep hang trong history
        /// \r\n. isShowRetetResult=true: Hien thi ket qua thi lai
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <param name="lstSubject"></param>
        /// <param name="lstRegisterSubjectBO"></param>
        void RankingPupilOfClassHistory(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isShowRetetResult, string AutoMark, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);

        void RankingPupilOfClassNotRankHistory(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isPassRetest = false, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);

        void ClassifyPupilHistory(List<PupilRankingBO> lstPupilRankingBO, int Semester, bool isShowRetetResult, string auto);
        void ClassifyPupilAfterRetestHistory(List<PupilRankingBO> lstPupilRankingBO, List<PupilRetestRegistration> lstPupilRetest, int SchoolID, int AppliedLevel, int AcademicYearID);
        void RankingPupilRetestHistory(int UserID, List<PupilRetestHistoryToCategory> lstPupilRankingBO, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);
        

        void Rank(int RankingCriteria, List<PupilRankingHistory> lstPupilRanking, bool isValidate = true);


        /// <summary>
        /// Tinh diem trung binh cac mon
        /// </summary>
        /// <param name="ListSummedUpRecord"></param>
        /// <param name="lstSubject"></param>
        /// <param name="semester"></param>
        /// <param name="lstRegisterSubjectBO"></param>
        /// <returns></returns>
        decimal? CaculatorAverageSubject(List<SummedUpRecordHistory> ListSummedUpRecord, List<ClassSubject> lstSubject, int semester, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null);

        /// <summary>
        /// Convert list PupilRanking sang List<PupilRankingHistory>
        /// </summary>
        /// <param name="lstPupilRanking"></param>
        /// <returns></returns>
        List<PupilRankingHistory> ConvertListToHistory(List<PupilRanking> lstPupilRanking);

        /// <summary>
        /// Thuc hien tong ket du lieu lich su
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="Period"></param>
        /// <param name="isShowRetetResult"></param>
        /// <param name="auto"></param>
        /// <returns></returns>
        int RankClassHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? Period, bool isShowRetetResult, string auto);
        
    }
}
