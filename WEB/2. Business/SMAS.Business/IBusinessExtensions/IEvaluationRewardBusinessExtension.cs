﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IEvaluationRewardBusiness
    {
        IQueryable<EvaluationReward> Search(IDictionary<string, object> dic);
        ProcessedReport GetEvaluationRewardRepord(IDictionary<string, object> dic);
        void InsertEvaluationReward(List<EvaluationReward> lstEvaluationReward, IDictionary<string, object> dic);
        void DeleteEvaluationReward(IDictionary<string, object> dic);
        ProcessedReport InsertEvaluationRewarReport(IDictionary<string, object> dic, Stream data);
        Stream CreateEvaluationRewardReport(IDictionary<string, object> dic);
    }
}
