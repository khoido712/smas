﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IParticularPupilCharacteristicBusiness
    {
        IQueryable<ParticularPupilCharacteristic> Search(IDictionary<string, object> dic);
        ParticularPupilCharacteristic Insert(int UserID, ParticularPupilCharacteristic entityToInsert);
        ParticularPupilCharacteristic Update(int UserID, ParticularPupilCharacteristic entityToUpdate);
        void Delete(int UserID, int ParticularPupilCharacteristicID, int SchoolID);
        IQueryable<ParticularPupilCharacteristic> SearchByParticularPupil(int particularPupilID, IDictionary<string, object> searchInfo);
    }
}