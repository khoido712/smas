/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSParentContractInClassDetailBusiness 
    {
        IQueryable<SMS_PARENT_CONTRACT_CLASS> Search(IDictionary<string, object> dic);
        SMS_PARENT_CONTRACT_CLASS GetMessageBudgetDetailInClass(int schoolID, int academicYearID, int classID, int semester);
        void UpdateMessageBudgetInClass(List<ContractDetailInClassBO> lstContractDetail, int year);
        bool IsExistInfoMessageOfClass(int schoolID, int academicYearID, int classID, int semester);
        List<SMSParentContractClassBO> GetListSMSParentContractClass(int SchoolID, int AcademicYearID, int SemesterID, List<int> lstClassID);
        List<SMSParentContractClassBO> GetListPhoneByListClass(int SchoolID, int AcademicYearID, List<int> lstClassID, int SemesterID);
    }
}