﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
namespace SMAS.Business.IBusiness
{
    public partial interface ILookupInfoReportBusiness
    {
        Stream ExportLookupInfo(Dictionary<String, object> dic, out string FileName);
        Stream ExportLookupInfoPupil(Dictionary<String, object> dic, out string FileName);
        Stream ExportLookupInfoPupilBySuper(Dictionary<String, object> dic, out string FileName);
        Stream ExportLookupInfoClassBySuper(Dictionary<String, object> dic, out string FileName);
        DataTable GetHealthOfPupil(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID, int StatusID, string PupilCode, string FullName, int Genre, int EducationLevelID, int ClassID);
    }
}
