﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IAverageMarkByPeriodBusiness
    {
        string GetHashKey(TranscriptOfClass Entity);
        ProcessedReport InsertAverageMarkByPeriod(TranscriptOfClass Entity, Stream Data);
        ProcessedReport GetAverageMarkByPeriod(TranscriptOfClass Entity);
        Stream CreateAverageMarkByPeriod(TranscriptOfClass Entity);
    }
}
