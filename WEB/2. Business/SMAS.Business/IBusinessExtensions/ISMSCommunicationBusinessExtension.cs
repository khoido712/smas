/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSCommunicationBusiness 
    {
        IQueryable<SMSCommunicationBO> GetListNotificationInbox(IDictionary<string, object> dic);
        
        /// <summary>
        /// get list mailbox groupby sender - paging enable
        /// dung groupby lam performance kem -> duyet tu contactGroup lay SMSCommunication
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>06/05/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<SMSCommunicationBO> GetListMailBoxGroupBySender(IDictionary<string, object> dic);

        /// <summary>
        /// get list detail mail box by sender group
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        List<SMSCommunicationBO> GetListDetailMailBox(IDictionary<string,object> dic);   

        /// <summary>
        /// SendSMS - return SMSResultBO 
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>06/05/2013</date>
        /// <param name="dic">sender, lstReceiverID (to parent), contactGroupID (to teacher), classID, academicYearID, type, schoolID, content</param>
        /// <returns></returns>
        ResultBO SendSMS(IDictionary<string, object> dic);
        bool CheckPermissionToSendSMS(IDictionary<string, object> dic);
        IQueryable<HistorySMS> ListSMSCommunication(int SchoolID, DateTime fromDate, DateTime toDate, int Receiver, int Type,int classID);
    }
}