/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilAbsenceBusiness
    {
        void InsertOrUpdatePupilAbsence(int UserID, bool IsAdmin, int PupilID, int ClassID, int Section, DateTime AbsentFromDate, DateTime AbsentToDate, List<PupilAbsence> lstPupilAbsence);
        // IQueryable<PupilAbsence> SearchPupilAbsence(IDictionary<string, object> dic =null);
        IQueryable<PupilAbsence> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null);
        //void DeletePupilAbsent(int UserID, int PupilID, int ClassID, DateTime AbsentFromDate, DateTime AbsentToDate, int Section);
        int CountTotalAbsenceWithPermission(int PupilID, int AcademicYearID, int Semester);
        int CountTotalAbsenceWithoutPermission(int PupilID, int AcademicYearID, int Semester);
        Stream ExportReport(IDictionary<string, object> dicCP, int SchoolID, int AcademicYearID, int Section, int ClassID, DateTime date, bool IsAdmin, out string FileName, int typeReport, int EmployeeID, int AppliedLevelID = 0);
        void SaveAMonth(int UserID, bool IsAdmin, int SchoolID, int ClassID, int Section, DateTime Month, List<PupilAbsence> ListData, bool CheckAllDay, int AppliedLevel, List<DateTime> lstOutDate = null);
        void SaveAWeek(int UserID, bool IsAdmin, int SchoolID, int ClassID, int Section, DateTime Month, List<PupilAbsence> ListData, DateTime WeekStartDate, DateTime WeekEndDate, bool CheckAllDay, int AppliedLevel);
        void SaveAMonthManyClass(int UserID, bool IsAdmin, int SchoolID, int AcademicYearID, int AppliLevel, int Section, DateTime Month, List<PupilAbsence> ListData, List<DateTime> lstOutDate = null);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        ProcessedReport GetReportPupilAway(PupilAbsenceBO entity);
        ProcessedReport InsertPupilAwayReport(PupilAbsenceBO entity, Stream data);
        List<PupilAbsenceBO> GetListPupilAbsenceBySection(IDictionary<string, object> dic,AcademicYear objAca = null);
        List<PupilAbsence> GetPupilAbsenceByDate(List<PupilAbsence> listAllPupilAbsence);

        ProcessedReport GetReportPupilAbsence(PupilAbsenceBO entity);
        ProcessedReport InsertPupilAbsenceReport(PupilAbsenceBO entity, Stream data);
        List<int> GetTypeSection(List<int> a, int b);

        ProcessedReport GetReportChildrenAway(PupilAbsenceBO entity);
        ProcessedReport InsertChildrenAwayReport(PupilAbsenceBO entity, Stream data);

        ProcessedReport GetReportChildrenAbsence(PupilAbsenceBO entity);
        ProcessedReport InsertChildrenAbsenceReport(PupilAbsenceBO entity, Stream data);

        ProcessedReport GetReportChildrenAbsenceByDay(PupilAbsenceBO entity);
        ProcessedReport InsertChildrenAbsenceReportByDay(PupilAbsenceBO entity, Stream data);
    }
}
