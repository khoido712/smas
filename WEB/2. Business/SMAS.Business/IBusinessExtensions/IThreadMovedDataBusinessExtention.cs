﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IThreadMovedDataBusiness
    {
        IQueryable<ThreadMovedDataBO> Search(IDictionary<string,object> SearchInfo);
        int Insert(List<ThreadMovedDataBO> lstThreadMoveData);


        IQueryable<ThreadMovedDataBO> SearchSummedSchool(IDictionary<string, object> SearchInfo);
        /// <summary>
        /// Tinh lai TBM theo tinh thanh
        /// </summary>
        /// <param name="lstThreadMoveData"></param>
        /// <returns></returns>
        bool ThreadSummedMarkbyProvince(List<ThreadMovedDataBO> lstThreadMoveData, int semesterId);

        /// <summary>
        /// Xoa diem trung
        /// </summary>
        /// <param name="lstThreadMoveData"></param>
        /// <param name="semesterId"></param>
        /// <returns></returns>
        bool ThreadDeleteMarkDuplicate(List<ThreadMovedDataBO> lstThreadMoveData, int semesterId);
    }
}
