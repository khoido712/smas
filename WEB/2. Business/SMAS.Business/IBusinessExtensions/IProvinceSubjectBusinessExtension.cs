/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IProvinceSubjectBusiness 
    {
        IQueryable<ProvinceSubject> Search(IDictionary<string, object> dic);
        IQueryable<ProvinceSubject> SearchByProvince(int ProvinceID, IDictionary<string, object> SearchInfo);
        void Insert(List<ProvinceSubject> list);
        IQueryable<ProvinceSubjectBO> GetListProvinceSubject(IDictionary<string, object> SearchInfo);
    }
}