﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDishInspectionBusiness 
    {
        IQueryable<DishInspection> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        bool CheckDishInspection(int SchoolID, DateTime InspectedDate, int dailyMenu);
        IQueryable<DishInspection> GetInspectedDate(int SchoolID, DateTime FromDate, DateTime ToDate);
        IQueryable<DishInspectionBO> GetListFood(int SchoolID, DateTime InspectedDate);
        IQueryable<DishInspectionBO> GetListOtherService(int SchoolID, DateTime InspectedDate);
        int GetNumberOfChildren(int SchoolID, DateTime InspectedDate);
        Stream CreateReportDishInspectionCost(int SchoolID, int AcademicYearID, DateTime date);
        Stream CreateReportDishInspection(int SchoolID, int AcademicYearID, int DishInspectionID);
        /// <summary>
        /// Xóa đối tượng bằng cách update IsActive = 1
        /// </summary>
        /// <param name="id">ID của record cần xóa.</param>
        /// <param name="LogicallyDelete">Nếu true thì xóa logic bằng cách set IsActive = 1, nếu flase thì xóa thật.</param>
        void Delete(object id,bool LogicallyDelete);
    }
}