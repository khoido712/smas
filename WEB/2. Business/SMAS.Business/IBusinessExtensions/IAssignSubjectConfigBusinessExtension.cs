﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IAssignSubjectConfigBusiness
    {
        List<AssignSubjectConfig> GetList(IDictionary<string, object> SearchInfo);
        IQueryable<AssignSubjectConfig> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<AssignSubjectConfig> Search(IDictionary<string, object> SearchInfo);
        void InsertAssignSubjectConfig(List<AssignSubjectConfig> lstInput);
        void UpdateAssignSubjectConfig(AssignSubjectConfigBO objUpdate, IDictionary<string, object> SearchInfo);
    }
}
