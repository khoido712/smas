﻿using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace SMAS.Business.IBusiness
{
    public partial interface IPhysicalExaminationBusiness
    {
        Dictionary<int, int> GetListMonthAcademicByYear(int UserAccountID, int AcademicYearID, int SchoolID);

        void InsertOrUpdateSinglePE(PhysicalExaminationBO objPhyExBO, IDictionary<string, object> dic, int phyExID);

        void InsertOrUpdatePE(List<PhysicalExaminationBO> lstPhyExBO, IDictionary<string, object> dic);

        IQueryable<PhysicalExamination> Search(IDictionary<string, object> dic);

        void DeletePE(int MonthId, IDictionary<string, object> dic);

        string checkStandardPE(decimal? heightOrweight, string hw, int monthCount, int? gender);

        ProcessedReport GetProcessReportOfMonthlyHeathy(IDictionary<string, object> dicReport, string reportCode);
        ProcessedReport InsertProcessReportOfMonthlyHeathy(IDictionary<string, object> dicReport, Stream excel, string reportCode);
        ProcessedReport GetProcessReportOfMonthlyHeathyDetail(IDictionary<string, object> dicReport, string reportCode);
        ProcessedReport InsertProcessReportOfMonthlyHeathyDetail(IDictionary<string, object> dicReport, Stream excel, string reportCode);

        /// <summary>
        /// Kiểu trả về là Integer Nullable.Trả về giá trị Trạng thái theo giá trị cân - đo
        /// </summary>
        /// <param name="monthId">Tháng Id</param>
        /// <param name="gender">Giới tính. Nữ = 0, Nam = 1. Mặc định là Nữ</param>
        /// <param name="type">Kiểu chiều cao hoặc cân nặng. chiều cao = 0, cân nặng = 1. Mặc định là chiều cao</param>
        /// <param name="value">Giá trị của Chiều cao hoặc Cân nặng. Mặc định là null</param>
        /// <returns></returns>
        int GetPhysicalStatus(int monthId, int gender = 0, int type = 0, decimal? value = null);
        
        ProcessedReport InsertPhysicalExaminationReport(IDictionary<string, object> dic, int thang, string className, Stream data/*, int appliedLevel*/);
        List<KeyValuePair<int, int>> GetListMonthByAcademiId(int UserAccountID, int AcademicYearID, int SchoolID);

    }
}
