/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISchoolSubjectBusiness 
    {
        void Insert(List<SchoolSubject> ListSchoolSubject);
        void Update(List<SchoolSubject> ListSchoolSubject);
        IQueryable<SchoolSubject> Search(IDictionary<string, object> SearchInfo);
        IQueryable<SchoolSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<SchoolSubject> SearchByAcademicYear(int AcademicYearID,IDictionary<string, object> SearchInfo);
        void Delete(List<SchoolSubject> ListSchoolSubject, int SchoolID);
        bool CheckSchoolSubjectContraints(int SchoolSubjectID);
        /// <summary>
        /// loc danh sach mon thi, chi lay mon tinh diem de thong ke bao cao
        /// </summary>
        /// <param name="subjectListID"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <returns></returns>
        List<SubjectCat> GetListSubjectMarkByListSubjectID(List<int> subjectListID);
    }
}