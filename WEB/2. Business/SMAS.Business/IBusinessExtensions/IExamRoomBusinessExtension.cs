/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamRoomBusiness 
    {
        IQueryable<ExamRoom> Search(IDictionary<string, object> search);
        void InsertExamRoom(ExamRoom room);
        void InsertListExamRoom(List<ExamRoom> listExamRoom);
        void UpdateExamRoom(ExamRoom examRoom);
        void DeleteExamRoom(List<long> listExamRoomID);
        /// <summary>
        /// check ma phong thi da ton taij chua
        /// </summary>
        /// <param name="examCode"></param>
        /// <param name="ExaminationsID"></param>
        /// <param name="ExamGroupID"></param>
        /// <returns></returns>
        bool CheckExamRoomByExamCode(string examCode, long ExaminationsID, long ExamGroupID);

        /// <summary>
        /// check ma phong thi da ton tai chua update
        /// </summary>
        /// <param name="examCode"></param>
        /// <param name="ExaminationsID"></param>
        /// <param name="ExamGroupID"></param>
        /// <param name="examRoomID"></param>
        /// <returns></returns>
        bool CheckExamRoomByExamCodeUpdate(string examCode, long ExaminationsID, long ExamGroupID, long examRoomID);

        /// <summary>
        /// Danh sach phong thi theo ky thi va nhom thi
        /// </summary>
        /// <param name="examinationID"></param>
        /// <param name="examGroupID"></param>
        /// <returns></returns>
        List<ExamRoom> GetListExamRoom(long examinationID, long examGroupID);

        List<ExamRoomBO> GetListExamRoomBO(long examinationID, List<long> lstExamGroupID);
    }
}