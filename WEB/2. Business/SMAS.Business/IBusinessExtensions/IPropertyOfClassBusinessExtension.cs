/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPropertyOfClassBusiness 
    {
        IQueryable<PropertyOfClass> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        IQueryable<PropertyOfClass> SearchByClass(int ClassID, IDictionary<string, object> SearchInfo = null);
        void Delete(List<PropertyOfClass> ListPropertyOfClass, int AcademicYearID);
        IQueryable<PropertyOfClass> Search(IDictionary<string, object> SearchInfo);
    }
}