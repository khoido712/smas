﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.IBusiness
{
    public partial interface IMarkOfSemesterBySubjectBusiness
    {

        string GetHashKey(MarkOfSemesterBySubjectBO entity);
        ProcessedReport GetMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity);
        ProcessedReport InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity, Stream data);
        Stream CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity);
        Stream CreateJudgeOfSemesterBySubject(MarkOfSemesterBySubjectBO entity);

    }
}
