﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IPupilRewardReportBusiness
    {
        string GetHashKeyForPriamry(PupilRewardReportBO entity);

        ProcessedReport GetPupilRewardReportForPrimary(PupilRewardReportBO entity);

        ProcessedReport GetPupilRewardReportForPrimaryTT30(PupilRewardReportBO entity);//Thông tư 30

        ProcessedReport InsertPupilRewardPrimaryReport(PupilRewardReportBO entity, Stream data);

        ProcessedReport InsertPupilRewardPrimaryReportTT30(PupilRewardReportBO entity, Stream data);//Thông tư 30

        Stream CreatePupilRewardPrimaryReport(PupilRewardReportBO entity);

        Stream CreatePupilRewardPrimaryReportTT30(PupilRewardReportBO objPupilRewardReport);//Thông tư 30

        string GetHashKey(PupilRewardReportBO entity);

        ProcessedReport GetPupilReward(PupilRewardReportBO entity);

        ProcessedReport InsertPupilReward(PupilRewardReportBO entity, Stream data);

        Stream CreatePupilReward(PupilRewardReportBO entity);
    }
}
