/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDefaultGroupBusiness 
    {
        void Delete(int id);
        IQueryable<DefaultGroup> Search(IDictionary<string, object> dic);

        IQueryable<DefaultGroup> GetAllActive();
        DefaultGroup Insert(DefaultGroup DefaultGroup);
        DefaultGroup Update(DefaultGroup DefaultGroup);
        DefaultGroup AssignMenu(int DefaultGroupID, List<DefaultGroupMenu> lsDefaultGroupMenu);
        IQueryable<DefaultGroupMenu> GetListMenu(int DefaultGroupID);

    }
}