/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ITranscriptsVNENReportBusiness 
    {
        ReportDefinition GetReportDefinition(string reportCode);
        string GetHashKeyTranscriptsVNENReport(TranscriptsVNENReportBO entity);
        ProcessedReport GetProcessedReport(TranscriptsVNENReportBO entity,bool isZip = false);
        ProcessedReport InsertTranscriptsVNENReport(TranscriptsVNENReportBO entity, Stream data, bool isZip = false);
        Stream GetTranscriptsVNENReport(TranscriptsVNENReportBO Entity);
        void DownloadZipFile(TranscriptsVNENReportBO entity,out string FolderSaveFile);
    }
}