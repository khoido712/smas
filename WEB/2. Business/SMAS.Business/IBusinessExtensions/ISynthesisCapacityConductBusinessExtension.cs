﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ISynthesisCapacityConductBusiness
    {
        Stream CreateSynthesisCapacityConduct(SynthesisCapacityConduct entity);
        ProcessedReport GetSynthesisCapacityConduct(SynthesisCapacityConduct entity);
        string GetHashKey(SynthesisCapacityConduct entity);
        ProcessedReport InsertSynthesisCapacityConduct(SynthesisCapacityConduct entity, Stream data);
    }
}
