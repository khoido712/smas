/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IMenuBusiness 
    {
        /// <summary>
        /// Tao moi menu
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="UserAccountID"></param>
        /// <param name="listRoleID"></param>
        void InsertMenu(Menu menu , int[] listRoleID);
        void EditMenu(Menu menu, int[] listRoleID);
        void DeleteMenu(int id);
        bool CheckPrivilegeAssignMenuForGroup(int GroupID, int UserAccountID);
        int GetNumberOfSameParentMenu(int? ParentID);
        Menu GetMenuByControllerAndActionName(string ControllerName, string actionName, string areaName);
        IQueryable<Menu> GetAllSubMenu(int MenuID);
        void UpdateMenuRole(int MenuID, int[] listRoleID);

        /// <summary>
        /// List menu phan quyen
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <param name="MenuID"></param>
        /// <param name="IsAdmin"></param>
        /// <returns></returns>
        List<Menu> GetMenuExamination(int UserAccountID, int MenuID, bool IsAdmin, int? appliedLevel);

        List<Menu> GetMenuByReport(int UserAccountID, int MenuID, bool IsAdmin, int? appliedLevel);

        /// <summary>
        /// Lay menu dang hien
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        Menu GetMenuByArea(string area);

        /// <summary>
        /// Lay menu khong quan tam la co an hay khong
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        Menu GetMenuByAreaNotVisiable(string area);

        Menu GetMenuByAreaNotVisiablePath2(string area);
    }
}