/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IGroupMenuBusiness 
    {
        IQueryable<Menu> GetMenuOfGroup(int GroupID);
        IQueryable<GroupMenu> GetGroupMenus(int GroupID);
        IQueryable<Menu> GetMenuOfGroupByPermission(int GroupID, int Permission);
        bool CheckMenuPermissionOfGroup(int GroupID, int Permission, int MenuID);
        bool CheckMenuPermissionOfGroup(int GroupID, int Permission, string ControllerName, string ActionName, string AreaName);
        IQueryable<Menu> GetMenuOfListGroup(List<int> ListGroupID);
    }
}