﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IInputStatisticsBusiness
    {
        /// <summary>
        /// Lay thong tin de hien thi tren grid
        /// </summary>
        /// <param name="UserAccountID">The user account ID.</param>
        /// <param name="SupervisingDeptID">The supervising dept ID.</param>
        /// <param name="DistrictID">The district ID.</param>
        /// <param name="Year">The year.</param>
        /// <param name="ReportDate">The report date.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <returns>
        /// List{InputStatistics}
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>12/4/2012</date>
        List<InputStatistics> DetailStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime? ReportDate, int AppliedLevel);

        /// <summary>
        /// Xuat bao cao chi tiet
        /// </summary>
        /// <param name="UserAccountID">The user account ID.</param>
        /// <param name="SupervisingDeptID">The supervising dept ID.</param>
        /// <param name="DistrictID">The district ID.</param>
        /// <param name="Year">The year.</param>
        /// <param name="ReportDate">The report date.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>12/4/2012</date>
        ProcessedReport ExportDetailStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime? ReportDate, int AppliedLevel);

        #region Search ReportInputMonitorSituation
        /// <summary>
        /// Tim kiem thong ke tinh hinh nhap lieu
        /// </summary>
        /// <author date="140424">HaiVT</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<ReportInputMonitorBO> SearchInputMonitor(IDictionary<string, object> dic, ref int total);
        #endregion

        #region Fill excel
        /// <summary>
        /// Fill excel
        /// </summary>
        /// <author date="14/05/05">HaiVT</author>
        /// <param name="dic"></param>
        ProcessedReport FillExcelReportInputMonitor(IDictionary<string, object> dic);
        #endregion

        /// <summary>
        /// Tim kiem them vao bang Report Request
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<ReportRequestBO> SearchSchoolReport(IDictionary<string, object> dic);

        /// <summary>
        /// Tong hop du lieu tinh hinh nhap lieu
        /// </summary>
        /// <param name="SearchInfo"></param>
        void CreateDataInputSituation(IDictionary<string, object> SearchInfo);

        /// <summary>
        /// Xuat excel tinh hinh nhap lieu. Ap dung cho admin/phong/so
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        System.IO.Stream ExportInputMonitoringSituation(IDictionary<string, object> SearchInfo);

        void CreateDataKinderGartenReport(IDictionary<string, object> dic);
        System.IO.Stream ExportKinderGartenReport(IDictionary<string, object> SearchInfo);
    }
}
