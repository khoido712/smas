/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IActivityBusiness 
    {
        List<ActivityPlanToSMS> GetActivityToSMS(IDictionary<string, object> dic);
        IQueryable<Activity> SearchByClass(int ClassID, int SchoolID, int AcademicYearID, DateTime ActivityDate);
        IQueryable<Activity> Search(IDictionary<string, object> dic);
        void Insert(List<Activity> lsActivity);
        void InsertCoppy(List<Activity> lsActivity, IDictionary<string, object> dic);
        void Delete(List<int> lsActivityID);
        //IQueryable<Activity> SeachByShool(int SchoolID, int AcademicYearID);
    }
}