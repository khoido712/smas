/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamSupervisoryBusiness 
    {
        IQueryable<ExamSupervisory> Search(IDictionary<string, object> search);
        IQueryable<ExamSupervisoryBO> GetExamSupervisoryOfExaminations(int academicYearID, int schoolID, long examinationsID);
        List<ExamSupervisoryBO> GetListExamSupervisory(IDictionary<string, object> search);
        List<ExamSupervisoryBO> GetListExamSupervisoryDev(IDictionary<string, object> search);
        void DeleteExamSupervisory(List<long> listExamSupervisoryID);
        List<ExamSupervisoryBO> GetListEmployee(IDictionary<string, object> search);
        void InsertListExamSupervisory(List<ExamSupervisory> listExamSupervisory);
        void UpdateListExamSupervisory(List<ExamSupervisory> listExamSupervisory);
        IQueryable<ExamSupervisoryBO> GetExamSupervisoryOfExaminationsCustom(int academicYearID, int schoolID, long examinationsID, long examGroup);
    }
}