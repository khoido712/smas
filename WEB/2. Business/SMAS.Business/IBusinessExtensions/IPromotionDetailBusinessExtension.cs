/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IPromotionDetailBusiness 
    {
        List<SMS_PROMOTION_DETAIL> GetlistPromotionDetail(Dictionary<string, object> dic);
        string InsertPromotionDetail(List<SMS_PROMOTION_DETAIL> lstPromotionDetail, Dictionary<string, object> dic);
        void DeletePromotionDetail(List<long> lstPromotionDetailID);
        IQueryable<SMS_PROMOTION_DETAIL> Search(Dictionary<string, object> dic);
    }
}