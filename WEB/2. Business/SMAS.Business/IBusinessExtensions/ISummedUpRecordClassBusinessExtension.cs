/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISummedUpRecordClassBusiness 
    {
        IQueryable<SummedUpRecordClassBO> GetListClassByEducationLevel(IDictionary<string, object> dic);
        void InsertOrUpdateSummedUpRecordClass(int UserID, List<ClassProfile> ListNoCheck, List<ClassProfile> ListCheck, IDictionary<string, object> dic);
        void UpdateStatusSummedUp(int UserID, List<ClassProfile> lst1, List<ClassProfile> lst2, IDictionary<string, object> dic);
        void InsertSummedUpRecordClass(int UserID, SummedUpRecordClass entity);
        void UpdateSummedUpRecordClass(int UserID, SummedUpRecordClass entity);
        IQueryable<SummedUpRecordClass> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        bool CheckExistClassInSummedUpRecordClass(int SchoolID, int ClassID, int Semester, int? PeriodID,int Type);
        string AutoClassifyingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester);
        string AutoRankingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);
        string AutoMarkSummary(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);
        void InsertOrSummedUpRecordClass(SummedUpRecordClass entity);

        /// <summary>
        /// Kiem tra lop co dang thuc hien tong ket, xep loai
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool CheckExistsSummedExcuting(SummedUpRecordClass entity);

    }
}