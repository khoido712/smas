﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportExamBusiness 
    {
        Stream CreateReportList(IDictionary<string, object> dic);
        Stream CreateRollReport(IDictionary<string, object> dic);
        Stream CreateAssignReport(IDictionary<string, object> dic);
        Stream CreateInputMarkFile(IDictionary<string, object> dic);
        Stream ViolatedInvilgilatorReport(IDictionary<string, object> dic);
        Stream CreateViolatedCandidateReport(IDictionary<string, object> dic);
        Stream CreateInputMarkFileBySBD(IDictionary<string, object> dic);
    }
}
