using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface ICodeConfigBusiness
    {
        IQueryable<CodeConfig> Search(IDictionary<string, object> dic);
        void Delete(int CodeConfigID);

        /// <summary>
        /// Tim kiem ma so tu dong theo tinh thanh
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="provinceID">ID cua tinh thanh pho tim kiem </param>
        /// <param name="codeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Entity cua tinh thanh tim duoc</returns>
        CodeConfig SearchByProvince(int provinceID, int codeType);

        /// <summary>
        /// Tim kiem ma so tu dong theo truong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="SchoolID">ID cua truong hoc can tim</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns></returns>
        CodeConfig SearchBySchool(int SchoolID, int CodeType);

        /// <summary>
        /// Lay so thu tu nam trong code
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="code">Ma so tu dong</param>
        /// <returns>So thu tu cua ma so (nam o vi tri cuoi cung)</returns>
        int GetOrderInCode(string code, int pos);

        /// <summary>
        /// Kiem tra xem So co ma so tu dong da Active khong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="ShoolID">ID cua truong hoc</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Bool : Da Active hay chua</returns>
        bool IsAutoGenCode(int ShoolID, int CodeType);

        /// <summary>
        /// Kiem tra So co cau hinh danh ma tu dong khong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="ShoolID">ID cua truong hoc thuoc so can kiem tra</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Bool: Co duoc thay doi ma so hay khong</returns>
        bool IsSchoolModify(int ShoolID, int CodeType);

        /// <summary>
        /// Lay ma so tu dong cua hoc sinh
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="AcademicYearID">Nam hoc</param>
        /// <param name="educationLevelID">Khoi hoc</param>
        /// <param name="schoolID">Truong hoc</param>
        /// <returns></returns>
        string CreatePupilCode(int AcademicYearID, int educationLevelID, int schoolID,int increase = 0);

        /// <summary>
        /// Lay ma so tu dong cua giao vien
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="schoolID">Truong hoc</param>
        /// <returns>Ma so tu dong cua giao vien</returns>
        string CreateTeacherCode(int schoolID,int increase = 0);

        /// <summary>
        /// Lay ma so tu dong cua hoc sinh cho import
        /// </summary>
        /// <param name="AcademicYearID">AcademicYearID</param>
        /// <param name="educationLevelID">educationLevelID</param>
        /// <param name="schoolID">schoolID</param>
        /// <param name="OriginalPupilCode">OriginalTeacherCode</param>
        /// <param name="maxOrderNumber">maxOrderNumber</param>
        /// <param name="NumberLength">NumberLength</param>
        /// <author>
        /// vtit_dungnt77 - 09/05/2013
        /// </author>
        void GetPupilCodeForImport(int AcademicYearID, int educationLevelID, int schoolID, out string OriginalPupilCode, out int maxOrderNumber, out int NumberLength);

        /// <summary>
        /// Lay ma so tu dong cua gv cho import
        /// </summary>
        /// <param name="schoolID">schoolID</param>
        /// <param name="OriginalTeacherCode">OriginalTeacherCode</param>
        /// <param name="maxOrderNumber">maxOrderNumber</param>
        /// <param name="NumberLength">NumberLength</param>
        /// <author>
        /// vtit_dungnt77 - 09/05/2013
        /// </author>
        void GetTeacherCodeForImport(int schoolID, out string OriginalTeacherCode, out int maxOrderNumber, out int NumberLength);
        /// <summary>
        /// ham danh lai ma hoc sinh cho toan truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="numberlength"></param>
        void ApproveNewPupilCode(int SchoolID, int AcademicYearID);

        /// <summary>
        /// Ham danh lai ma giao vien cho toan truong
        /// </summary>
        /// <param name="SchoolID"></param>
        void ApproveNewTeacherCode(int SchoolID);
    }
}
