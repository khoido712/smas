/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IMasterBookBusiness 
    {
        string GetHashKey(MasterBook entity);
        ProcessedReport InsertMasterBook(MasterBook entity, Stream Data);
        ProcessedReport GetMasterBook(MasterBook entity);
        Stream CreateMasterBook(MasterBook entity, string suffixName = null);

        ProcessedReport GetReportMarkInpurBook(IDictionary<string, object> dic);
        Stream CreateReportMarkInpurBook(IDictionary<string, object> dic, int numOfRows, bool isNumRowPupilNum, bool idg, int fitIn);
        ProcessedReport InsertReportMarkInpurBook(IDictionary<string, object> dic, Stream data);
    }
}