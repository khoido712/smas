/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISMSParentContractDetailBusiness 
    {
        void SaveDetail(List<SMS_PARENT_CONTRACT_DETAIL> listDetailInsert, int year, int schooId);
        bool CheckIDSMSParentContract(int schoolID, int year, int contractID);
        void DeleteContractDetail(int contractID, string phone, int servicePackageID);
        void CancelListContractDetail(List<long> lstContractID, List<string> lstPhone, List<int> lstServicePackageID, List<long> lstContractDetailID);
        List<SMS_PARENT_CONTRACT_DETAIL> GetContractDetailByContactID(int contractID, string phone, int servicePackageID);
        void SaveEditDetail(int contractID, string phoneOld, string phoneNew, int year, List<SMS_PARENT_CONTRACT_DETAIL> editList);
    }
}