﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.IBusiness
{
    public partial interface IJudgeRecordBusiness
    {
        //List<JudgeRecord> SearchJudgeRecord(IDictionary<string, object> dic);
        IQueryable<JudgeRecordBO> GetJudgeRecordOfClass(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID);
       
        /// <summary>
        /// Lấy bảng điểm của lớp cua tat ca cac mon nhan xet
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        IQueryable<JudgeRecordWSBO> GetAllJudgeRecordOfClass(IDictionary<string, object> dic);

        IEnumerable<JudgeRecordNewBO> GetJudgeRecordOfClassNew(IDictionary<string, object> dic);
        bool InsertJudgeRecord(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, int? EmployeeID, List<int> lstPupilExempted = null, int SchoolID = 0, int AcademicYearID = 0, int ClassID = 0, int SubjectID = 0);
        void Delete(int UserID, int JudgeRecordID, int? SchoolID);
        IQueryable<JudgeRecord> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        void ImportJudgeRecord(int UserID, List<JudgeRecord> lstJudgeRecord);
        void InsertJudgeRecordPrimary(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord);

        Stream ExportReport(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null);
        Stream ExportReportForSemester(int SchoolID, int AcademicYearID, int Semester, int AppliedLevel, int EducationLevelID, int ClassID, int PeriodID, int SubjectID, out string FileName, bool? isAdmin = null, Dictionary<int, bool> dicDisplay = null);
        string GetHashKey(JudgeRecordForReportBO entity);
        ProcessedReport InsertJudgeRecordBO(JudgeRecordForReportBO entity, Stream Data);
        ProcessedReport GetJudgeRecordBO(JudgeRecordForReportBO entity);
        Stream CreateJudgeRecordBO(JudgeRecordForReportBO entity);
        void ImportMark(int UserID, List<PupilForImportBO> listPupilForImport, List<PupilForImportBO> listPupilForDelete);

        void ImportMarkHistory(int UserID, List<PupilForImportBO> listPupilForImport, List<PupilForImportBO> listPupilForDelete);
        IQueryable<JudgeRecord> GetJudgeRecordToDelete(int AcademicYearID, int SchoolID, int SubjectID, int Semester, int ClassID, int? PeriodID);
        bool CheckSubjectForSemester(int SchoolID, int AcademicYearID, int ClassID, int Semester, int SubjectID);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        void DeleteJudgeRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID);
        string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID);

        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);
        void InsertJudgeRecordByListSubject(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, int ClassID, List<int> listSubjectID, int? EmployeeID);
        void InsertJudgeRecordByListClass(int UserID, List<JudgeRecord> lstJudgeRecord, List<SummedUpRecord> lstSummedUpRecord, int Semester, int? Period, List<int> lstPupilExempted, int SchoolID, int AcademicYearID, List<int> listClassID, int SubjectID, int? EmployeeID);
        void DeleteJudgeRecordByListID(int academicYearID, int schoolID, int? isHistory, List<long> listJudgeRecordId);

        /// <summary>
        /// Tinh diem TBM dot va hoc ky cho mon nhan xet. 
        /// Nếu nhập điểm theo đợt thì tính điểm HK
        /// Nếu nhập điểm theo kỳ thì tính điểm cho đợt
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="schoolId"></param>
        /// <param name="objAcademicYear"></param>
        /// <param name="semesterId"></param>
        /// <param name="PeriodID"></param>        
        /// <param name="classSubject"></param>
        void SummedJudgeMark(List<int> lstPupilID, AcademicYear objAcademicYear, int semesterId, int? PeriodID, ClassSubject classSubject);
    }
}
