﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExamSubjectBusiness
    {
        IQueryable<DOETExamSubject> Search(IDictionary<string, object> dic);
        List<DOETExamSubjectBO> GetListExamSubject(IDictionary<string, object> dic);        
    }
}
