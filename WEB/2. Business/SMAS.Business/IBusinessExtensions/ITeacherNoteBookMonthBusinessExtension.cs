/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface ITeacherNoteBookMonthBusiness
    {
        IQueryable<TeacherNoteBookMonth> Search(IDictionary<string, object> dic);
        List<TeacherNoteBookMonthBO> GetListTeacherNoteBookMonth(IDictionary<string, object> dic);
        void InsertOrUpdate(
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonth, 
            IDictionary<string, object> dic, 
            List<TeacherNoteBookMonth> lstTeacherNoteBookMonthDB,
            ref List<ActionAuditDataBO> lstActionAuditData);
        void DeleteTeacherNoteBookMonth(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAuditData, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail);
        void InsertOrUpdateForImport(List<TeacherNoteBookMonth> lstTeacherNoteBookMonth, IDictionary<string, object> dic);
        void InsertOrUpdateToMobile(TeacherNoteBookMonth objTeacherNoteBookMonth, IDictionary<string, object> dic);
    }
}
