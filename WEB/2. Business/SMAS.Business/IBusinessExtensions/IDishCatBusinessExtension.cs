/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDishCatBusiness 
    {
        void Insert(DishCat Entity);
        void Update(DishCat Entity);
        void Delete(int DishCatID, int SchoolID);
        IQueryable<DishCat> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
    }
}