﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public interface IReportExamResultBusiness : IGenericBussiness<ProcessedReport>
    {
        Stream CreateReportExamMarkByClass(int SchoolID, IDictionary<string, object> dic);
        Stream CreateReportExamMarkByRoom(int SchoolID, IDictionary<string, object> dic);
        Stream CreateReportQualityOfClass(int SchoolID, IDictionary<string, object> dic);
        Stream CreateReportQualityOfSubject(int SchoolID, IDictionary<string, object> dic);
    }
}
