/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IActivityOfPupilBusiness 
    {
        IQueryable<ActivityOfPupil> SearchBySchool(int? SchoolID, IDictionary<string, object> dic);
        void Insert(List<ActivityOfPupil> lst, IDictionary<string, object> dic);
        List<ActivityOfPupilToSMS> GetActivityOfPupilToSMS(IDictionary<string, object> dic);
        bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds);
        void DeleteActivityOfPupil(List<ActivityOfPupil> lst, IDictionary<string, object> dic);

        List<ActivityOfPupilToSMS> GetActivityOfPupilToSMS(int schoolId, int academicYearId, int classId, DateTime date);
    }
}