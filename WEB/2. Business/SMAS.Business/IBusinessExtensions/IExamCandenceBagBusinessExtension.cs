/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamCandenceBagBusiness 
    {
        //void InsertList(List<ExamCandenceBag> insertList);
        //void DeleteList(List<ExamCandenceBag> deleteList);
        IQueryable<ExamCandenceBag> Search(IDictionary<string, object> search);
        List<ExamCandenceBag> GetExamCandenceBagByRoom(long examinationsId, long examGroupId, List<long> lstExamRoomId);
    }
}