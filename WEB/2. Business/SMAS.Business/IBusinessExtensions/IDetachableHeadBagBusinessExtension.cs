/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDetachableHeadBagBusiness 
    {
        IQueryable<DetachableHeadBag> Search(IDictionary<string, object> dic);
        IQueryable<DetachableHeadBag> SearchBySchool(int schoolID, IDictionary<string, object> dic);
        void AutoGenDetachableHeadNumber(int schoolID, int appliedLevel, int examinationSubjectID, int numberOfBag, string prefix, int startingNumber);
        void ManualGenDetachHeadNumber(int schoolID, int appliedLevel, int examinationSubjectID, string[] NameListCodes, string[] DetachableHeadBags, string[] DetachableHeadNumbers);
    }
}
