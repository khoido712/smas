/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ICapacityStatisticBusiness 
    {
        void SendAll(List<CapacityStatisticsBO> ListData);
        string GetHashKeyForConductCapacityStatistics(CapacityConductReport CapacityConductReport);
        IQueryable<CapacityStatisticsBO> SearchByProvinceGroupBySubCommittee23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsBySubCommitteeTertiary(IDictionary<string, object> SearchInfo, String InputParameterHashKey, out int ProcessedReportID);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int processedReportID);
        List<CapacityStatisticsBO> SearchBySupervisingDept23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> SearchByProvinceGroupByLevel23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> SearchBySupervisingDeptGroupByLevel23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> SearchByProvinceGroupByDistrict23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByLevelSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo ,  out int FileID);
        List<CapacityStatisticsBO> CreatePGDCapacityStatisticsByLevelSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo, out int FileID);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByDistrictSecondary(string InputParameterHashKey, IDictionary<string, object> SearchInfo, out int FileID);
        IQueryable<SubjectCatBO> SearchSubjectHasReport(IDictionary<string, object> dic);
        List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);
        List<CapacityStatisticsBO> CreatePGDCapacitySubjectStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);        
        List<CapacityStatisticsBO> CreatePGDCapacityStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID);
        List<CapacityStatisticsBO> SearchByProvince23(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> CreateSGDCapacityStatisticsByDistrictTertiary(IDictionary<string, object> dic, string InputParameterHashkey, out int FileID);
        List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsTertiary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID);

        /// <Author>dungnt</Author>
        /// <DateTime></DateTime>
        List<CapacityStatisticsBO> CreateSGDCapacitySubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashkey);
        List<CapacityStatisticsBO> CreatePGDCapacitySubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashkey);
        List<CapacityStatisticsBO> SearchByProvince(IDictionary<string, object> SearchInfo);
        List<CapacityStatisticsBO> SearchBySupervisingDept(IDictionary<string, object> SearchInfo);

        List<CapacityStatisticsBO> CreateSGDCapacityJudgeSubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashkey);
        List<CapacityStatisticsBO> CreatePGDCapacityJudgeSubjectStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashkey);

        IQueryable<CapacityStatistic> Search(IDictionary<string, object> SearchInfo);


        /// <summary>
        /// Tong hop du lieu
        /// </summary>
        /// <param name="capacityObj"></param>
        void GeneralData(CapacityStatisticRequestBO capacityObj);

        List<CapacityStatisticsBO> GetListCapacityAllSchool(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelID, bool IsSuperVisingDeptRole, int districtId);

        List<CapacityStatisticsBO> GetListCapacityAllEdu(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId);

        List<CapacityStatisticsBO> GetListCapacityDistrict(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId, bool IsSuperVisingDeptRole, int districtId);

        IQueryable<SchoolProfileBO> GetListSchool(bool IsSuperVisingDeptRole, int provinceId, int districtId, int appliedLevelId);

    }
}