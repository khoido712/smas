﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface ISynthesisSubjectByEducationLevelBusiness
    {
        string GetHashKey(SynthesisSubjectByEducationLevel entity);
        ProcessedReport InsertSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity, Stream data);
        ProcessedReport GetSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity);
        Stream CreateSynthesisSubjectByEducationLevel(SynthesisSubjectByEducationLevel entity);
    }
}
