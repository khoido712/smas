﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{
    public partial interface ISummedEvaluationHistoryBusiness
    {
        /// <summary>
        /// Thuc hien them moi hoac cap nhat ket qua vao bang lich su
        /// </summary>
        /// <param name="schoolID">Id truong</param>
        /// <param name="dic">Cac cot khoa ngoai trong bang</param>
        /// <param name="listInsertOrUpdate">Danh sach diem va nhan xet cuoi ky</param>
        void InsertOrUpdate(int schoolID, IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate, List<ActionAuditDataBO> lstActionAuditData, ref ActionAuditDataBO objActionAuditBO);

        /// <summary>
        /// Them moi hoac thuc hien cap nhat du lieu tat ca cac mon cua 1 lop vao CSDL
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdatebyClass(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate);

        IQueryable<SummedEvaluationBO> Search(IDictionary<string, object> dic);

        IQueryable<SummedEvaluationHistory> getListSummedEvaluationByListSubject(IDictionary<string, object> dic);

        List<string> GetListCommentHistory(IDictionary<string, object> dic);
    }
}
