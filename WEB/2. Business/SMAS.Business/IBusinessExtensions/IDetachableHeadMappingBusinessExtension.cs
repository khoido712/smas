/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IDetachableHeadMappingBusiness 
    {
        DetachableHeadMapping Update(int SchoolID, int AppliedLevel, int DetachableHeadMappingID, int DetachableHeadBagID, string DetachableHeadNumber);
        IQueryable<DetachableHeadMapping> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo);
        Stream CreateDetachableHeadMappingReport(IDictionary<string, object> dic, out string reportName);
    }
}
