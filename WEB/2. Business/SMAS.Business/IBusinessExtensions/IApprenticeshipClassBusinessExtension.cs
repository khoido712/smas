﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{
    public partial interface IApprenticeshipClassBusiness 
    {
        ApprenticeshipClass InsertApprenticeshipClass(ApprenticeshipClass entityToInsert);
        ApprenticeshipClass UpdateApprenticeshipClass(ApprenticeshipClass entityToUpdate);
        void DeleteApprenticeshipClass(int ApprenticeshipClassID, int SchoolID);
        IQueryable<ApprenticeshipClass> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
    }
}