﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IDOETExamGroupBusiness
    {
        IQueryable<DOETExamGroup> Search(IDictionary<string, object> search);
        void InsertOrUpdateGroup(DOETExamGroup groupObj, bool isInsert);
        bool CheckGroupName(int examinationId, string groupName);
        bool CheckGroupCode(int examinationId, string groupCode);
        bool CheckGroupNameUpdate(int examinationId, int examGroupId, string groupName);
        bool CheckGroupCodeUpdate(int examinationId, int examGroupId, string groupCode);
        bool CheckExistPupilInGroup(int examinationId, int examgroupId);
    }
}
