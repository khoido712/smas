/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models.CustomModels;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IIndicatorDataBusiness 
    {
        IQueryable<IndicatorData> SearchIndicatorData(IDictionary<string, object> search);
        void InsertData(List<IndicatorData> listData, List<int> listIndicatorIdDelete, int provinceId, int period, int academicYearId);        
        void InsertDataPreviosPeriod(int academicYearId, int provinceId, int period, int htmlContentCodeId);
        int GetPreviosPeriod(int period);
        List<PhysicalFacilitiesBO> GetListIndicatorData(int provinceId, int period, int schoolId, int year, int? levelApplied = 0);
        List<FacilitiesDataBO> LookupFacilitiesData(IDictionary<string, object> dic);
        List<PhysicalFacilitiesBO> SynthesizeDataforSupervisingDept(IDictionary<string, object> dic, int? levelApplied = 0);
        List<PhysicalFacilitiesBO> SynthesizeDataPrimaryLevelforSupervisingDept(IDictionary<string, object> dic);
        List<PhysicalFacilitiesBO> GetDataByPeriod(int provinceId, int period, List<int> lstSchoolId, int year, List<string> listIndicatorCode = null);        
    }
}