﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IDetailClassifiedSubjectByPeriodBusiness
    {
        string GetHashKey(TranscriptOfClass Entity);
        ProcessedReport InsertDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity, Stream Data);
        ProcessedReport GetDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity);
        Stream CreateDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity);
    }
}
