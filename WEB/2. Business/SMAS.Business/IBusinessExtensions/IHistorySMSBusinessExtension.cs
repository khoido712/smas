/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IHistorySMSBusiness 
    {
        HistorySMS Insert(HistorySMS historySMS);
        HistorySMS Update(HistorySMS historySMS);
        bool Delete(int historySMSID);
        IQueryable<HistorySMS> Search(IDictionary<string, object> SearchInfo);
        void InsertHistorySMS(List<HistorySMS> lsHistorySMS);
    }
}