/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IIndicatorBusiness 
    {
        IQueryable<Indicator> SearchIndicator(IDictionary<string, object> search);
        List<Indicator> GetListIndicatorByHtmlTemplateID(int htmltemplateId);
        int GetCurrentPeriodReport(int academicYearId);
        ProcessedReport GetReportIndicator(PhysicalFacilitiesBO physicalObj);
        ProcessedReport InsertFacilitiesReport(PhysicalFacilitiesBO Entity, Stream Data, string FileName);
    }
}