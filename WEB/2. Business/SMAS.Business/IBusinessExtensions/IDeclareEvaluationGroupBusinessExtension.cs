﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;


namespace SMAS.Business.IBusiness
{
    public partial interface IDeclareEvaluationGroupBusiness
    {
        List<DeclareEvaluationGroup> GetListEvaluationByEvaluationTeacher(
              int AcademicYearID,
             int SchoolID,
             int AppliedLevel,
             int EducationLevel);
        IQueryable<DeclareEvaluationGroup> Search(IDictionary<string, object> SearchInfo);

        IQueryable<DeclareEvaluationGroup> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);

        IQueryable<DeclareEvaluationGroup> SearchByClass(int EducationLevelID, IDictionary<string, object> SearchInfo = null);

        IQueryable<DeclareEvaluationGroup> SearchEva(IDictionary<string, object> SearchInfo);

        IQueryable<DeclareEvaluationGroup> SearchBySchoolEvalation(int SchoolID, IDictionary<string, object> SearchInfo = null);

        void Delete(List<DeclareEvaluationGroup> ListClassSubject);

        void Insert(List<DeclareEvaluationGroup> ListDeclareEvaluationGroup, Dictionary<string, object> dic, int Security);

        void InsertAll(List<DeclareEvaluationGroup> ListDeclareEvaluationGroup, Dictionary<string, object> dic, int educationLevelID);
        List<DeclareEvaluationGroupBO> GetListDeclareEvaluationGroupByLevel(int schoolID, int academicYearID, int educationLevel);

        IQueryable<DeclareEvaluationGroup> GetListBySchoolAndAcademicYear(Dictionary<string, object> dic);

        bool? UpdateDEG(List<DeclareEvaluationGroup> lstUpdate);
    }
}
