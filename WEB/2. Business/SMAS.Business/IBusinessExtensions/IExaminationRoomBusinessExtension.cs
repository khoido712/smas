/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExaminationRoomBusiness 
    {
        IQueryable<ExaminationRoom> SearchBySchool(int SchoolID, IDictionary<string, object> Dictionary = null);
        void AutoAssign(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, int ExaminationSubjectID, int Type, int NumberOf);
        void Insert(int SchoolID, int AppliedLevel, ExaminationRoom ExaminationRoom);
        void Update(int SchoolID, int AppliedLevel, ExaminationRoom ExaminationRoom);
        void Delete(int SchoolID,int AppliedLevel,int ExaminationRoomID);
        List<int> GetListRoomHasSameEducationLevel(int examinationRoomID);
        void AverageAssign(int SchoolID, int AppliedLevel, List<int> lstCandidateID, List<ExaminationRoom> lstExamRoom);
    }
}
