/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IRegisterSubjectSpecializeBusiness 
    {
        List<RegisterSubjectSpecializeBO> GetlistSubjectByClass(IDictionary<string, object> dic);
        void InsertOrUpdate(IDictionary<string, object> dic, List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeON, List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeOFF);
        void DeleteRegisterByListID(List<long> lstID);
        IQueryable<RegisterSubjectSpecialize> Search(IDictionary<string, object> dic);
        void InsertList(List<RegisterSubjectSpecialize> listRegister);
        void DeleteList(List<RegisterSubjectSpecialize> listRegisterCancel);
        void RegisterForClass(List<RegisterSubjectSpecializeBO> listSubject, List<PupilOfClassBO> listPupil, List<ExemptedSubject> listPupilEx, IDictionary<string, object> search);
    }
}