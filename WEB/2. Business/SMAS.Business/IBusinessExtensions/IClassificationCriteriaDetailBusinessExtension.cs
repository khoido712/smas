/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassificationCriteriaDetailBusiness 
    {
        void Insert(List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail);
        void Update(List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail);
        void Delete(int ClassificationCriteriaDetailID);
        IQueryable<ClassificationCriteriaDetail> Search(IDictionary<string, object> SearchInfo);
        IQueryable<ClassificationCriteriaDetail> GetListClassificationDetail(IDictionary<string, object> SearchInfo);
        IQueryable<ClassificationCriteriaDetail> SearchBySchool(IDictionary<string, object> dic);
    }
}