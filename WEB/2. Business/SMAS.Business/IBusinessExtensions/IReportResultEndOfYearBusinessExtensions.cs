﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportResultEndOfYearBusiness
    {
        string GetHashKey(TranscriptOfClass TranscriptOfClass);
        ProcessedReport InsertResultEndOfYear(TranscriptOfClass entity, Stream data);
        ProcessedReport GetReportResultEndOfYear(TranscriptOfClass entity);
        Stream CreateReportResultEndOfYear(TranscriptOfClass entity);
    }
}
