﻿using SMAS.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.IBusiness
{
    public partial interface IPushNotifyRequestBusiness
    {
         /// <summary>
        /// listPush chi cho HS or GV, isPupil: 1_HS, 0_GV, FucntionName = ID chuc nang dinh nghia trong constant
        /// </summary>
        /// <param name="listPush"></param>
        /// <param name="schoolID"></param>
        /// <param name="isPupil"></param>
        /// <param name="FunctionName"></param>
        void AddPushRequest(List<PushNotificationBO> listPush, int schoolID, int FunctionName);
    }
}
