﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportTeachingAssignmentBusiness
    {
        string GetHashKey(ReportTeachingAssignmentBO entity);
        Stream ExcelCreateTeachingAssignment(ReportTeachingAssignmentBO entity);
        ProcessedReport ExcelInsertTeachingAssignment(ReportTeachingAssignmentBO entity, Stream data);
    }
}
