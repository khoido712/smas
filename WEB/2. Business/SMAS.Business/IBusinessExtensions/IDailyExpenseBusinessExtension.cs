/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IDailyExpenseBusiness
    {
        IQueryable<DailyExpense> GetExpenseDate(int SchoolID, DateTime FromDate, DateTime ToDate);
        bool CheckExpenseDate(int SchoolID, DateTime FromDate, DateTime ToDate);
        void Insert(DailyExpense entity);
        void Delete(int SchoolID, DateTime ExpenseDate);
        IQueryable<DailyExpense> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        Stream CreateReportDailyExpense(int SchoolID, int AcademicYearID, DateTime date);
    }
}