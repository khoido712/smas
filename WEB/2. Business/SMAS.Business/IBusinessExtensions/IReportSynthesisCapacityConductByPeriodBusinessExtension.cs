﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IReportSynthesisCapacityConductByPeriodBusiness
    {
        string GetHashKey(TranscriptOfClass TranscriptOfClass);
        ProcessedReport InsertSynthesisCapacityConductByPeriod(TranscriptOfClass entity, Stream data);
        ProcessedReport GetSynthesisCapacityConductByPeriod(TranscriptOfClass entity);
        Stream CreateSynthesisCapacityConductByPeriod(TranscriptOfClass Entity);
    }
}
