/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IClassMovementBusiness 
    {
       // IQueryable<ClassMovement> Search(IDictionary<string, object> dic = null);
        void InsertClassMovement(int UserID, List<ClassMovement> lstMovement, bool? isClassMovement);
        void DeleteClassMovement(int UserID, int ClassMovementID, int SchoolID);
        IQueryable<ClassMovement> SearchBySchool(int SchoolID = 0, IDictionary<string, object> dic = null);
        List<ClassMovement> GetListClassMovement(int AcademicYearID, int SchoolID, int Semester);
        void UpdateClassMovement(int UserID, ClassMovement classMovement);
        IQueryable<ClassMovementBO> ListClassMovement(int schoolID, int academicYearID);
    }
}