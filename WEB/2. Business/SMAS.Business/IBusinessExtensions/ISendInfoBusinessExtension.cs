/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface ISendInfoBusiness 
    {
        SendInfo Insert(SendInfo sendInfo);
        SendInfo Update(SendInfo sendInfo);
        bool Delete(int sendInfoID);
        IQueryable<SendInfo> Search(IDictionary<string, object> dic);
        void InsertSendInfo(List<SendInfo> lsSendInfo);
    }
}