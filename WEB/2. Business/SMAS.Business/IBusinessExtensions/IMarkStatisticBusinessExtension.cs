/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IMarkStatisticBusiness 
    {
        void SendAll(List<MarkStatisticBO> ListData);
        IQueryable<SubjectCatBO> SearchSubjectHasReportTHCS(IDictionary<string, object> dic);
        List<SubjectCat> SearchSubjectHasReport(string ReportCode, int Year
            , int? Semester, bool SentToSupervisor, int EducationLevelID, int? SubCommitteeID
            , int TrainingTypeID, int? SupervisingDeptID, int? ProvinceID);
        List<MarkStatisticsOfProvince23> CreateSGDSemesterMarkStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
        List<MarkStatisticsOfProvince23> CreateSGDPriodicMarkStatisticsTertiary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
        string GetHashKeyForMarkStatistics(MarkReportBO entity);        
        List<MarkStatisticsByDistrictBO> CreateSGDPriodicMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey);
        List<MarkStatisticsByDistrictBO> SearchByProvince(IDictionary<string, object> SearchInfo);
        
        List<MarkStatisticsOfProvince23> CreateSGDPriodicMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
        List<MarkStatisticsOfProvince23> SearchByProvince23(IDictionary<string, object> dic);
        List<MarkStatisticsOfProvince23> CreatePGDPriodicMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
        List<MarkStatisticsOfProvince23> SearchBySupervisingDept23(IDictionary<string, object> dic);
        ProcessedReport GetMarkStatistics(MarkReportBO entity);

        List<MarkStatisticsOfProvince23> CreateSGDSemesterMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
        List<MarkStatisticsOfProvince23> CreatePGDSemesterMarkStatisticsSecondary(IDictionary<string, object> dic, string InputParameterHashKey, out int FileID);
       
        List<MarkStatisticsOfSchoolBO> CreatePGDPriodicMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey);
        List<MarkStatisticsOfSchoolBO> SearchBySupervisingDept(IDictionary<string, object> SearchInfo);

        List<MarkStatisticsByDistrictBO> CreateSGDSemesterMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey);
        List<MarkStatisticsOfSchoolBO> CreatePGDSemesterMarkStatisticsPrimary(IDictionary<string, object> SearchInfo, string InputParameterHashKey);
        IQueryable<MarkStatistic> Search(IDictionary<string, object> SearchInfo);
        void CreateMarkStatistic(IDictionary<string, object> SearchInfo);
        System.IO.Stream ExportMarkStatistic(IDictionary<string, object> SearchInfo);

        void CreatePrimaryStatistic(IDictionary<string, object> SearchInfo);
        System.IO.Stream ExportPrimaryStatistic(IDictionary<string, object> SearchInfo);
        void InsertMarkStatisticBySupervisingDept(IDictionary<string, object> dic, List<SubjectCatBO> lstSubjectCatBO);
        System.IO.Stream ExportSupervisingDeptMarkInputSituation(IDictionary<string, object> dic, List<SubjectCatBO> lstSubjectCatBO);

List<DetailCostOfSalesReport> GetListDetailCostOfSalesReport(IDictionary<string, object> dic);
        List<CurrentAssignCollaboratorReportBO> getCurrentAssignCollaboratorReport(IDictionary<string, object> dic);
        List<DetailDeferredPaymentReportBO> getDetailDeferredPaymentReport(IDictionary<string, object> dic);
        List<DetailSMSEDUReportBO> getDetailSMSEDUReport(IDictionary<string, object> dic);
        List<SubscriberStatisticsPackageReportBO> getSubscriberStatisticsPackageReport(IDictionary<string, object> dic);
        List<DetailContractReportBO> getDetailContractReport(IDictionary<string, object> dic);
        /// <summary>
        /// Tong hop tinh tinh hinh biet dong hoc sinh
        /// </summary>
        /// <param name="dic"></param>
        void InsertReportVariablePupilBySuperVising(IDictionary<string, object> dic);
        /// <summary>
        /// Xuat bao cao tong hop tinh hinh bien dong hoc sinh
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        System.IO.Stream ExportVariablePupilBySuperVising(IDictionary<string, object> dic);

        /// <summary>
        /// Tong hop hoc sinh nghi hoc
        /// </summary>
        /// <param name="dic"></param>
        void InsertReportAbsencePupilForSuperVising(IDictionary<string, object> dic);

        System.IO.Stream ReportStatisticsPupilAbsence(IDictionary<string, object> dic);

    }
}