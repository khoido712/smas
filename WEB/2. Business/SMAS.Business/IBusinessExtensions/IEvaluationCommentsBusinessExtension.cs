﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;

namespace SMAS.Business.IBusiness
{
    public partial interface IEvaluationCommentsBusiness
    {
        /// <summary>
        /// Lay thong tin nhan xet cua hoc sinh
        /// </summary>
        /// <param name="schoolID">ma truong</param>
        /// <param name="academicYearID">id nam hoc</param>
        /// <param name="classID">id lop</param>
        /// <param name="semesterID">id hoc ky</param>
        /// <param name="subjectID">id mon hoc, id cac tieu chi</param>
        /// <param name="TypeOfTeacher">loai gia vien (bm,gvcn)</param>
        /// <param name="evaluationID">cac mat danh gia (1:Mon hoc và hdgd, 2: nang luc, 3: pham chat)</param>
        /// <returns></returns>
        List<EvaluationCommentsBO> getListEvaluationCommentsBySchool(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID);

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>
        List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationComments(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, List<SummedEvaluation> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding);

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        void InsertOrUpdateEvaluationCommentsByPupilID(int MonthID, EvaluationComments insertOrUpdateObj, ref ActionAuditDataBO objAc);

        List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(int MonthID, EvaluationComments insertOrUpdateObj);

        /// <summary>
        /// Xoa nhan xet
        /// </summary>
        /// <param name="IDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="semesterID"></param>
        /// <param name="classID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="evaluationID"></param>
        List<EvaluationCommentActionAuditBO> DeleteEvaluationComments(List<long> idList, List<long> idListSummedEnding, int schoolID, int academicYearID, int semesterID, int classID, int evaluationCriteriaID, int typeOfTeacher, int educationLevelID, int evaluationID, int monthID);

        /// <summary>
        /// Import chat luong giao duc khong tinh thang
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdateEducationQualityImport(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate);


        /// <summary>
        /// Lấy thông tin học sinh có đánh giá (chi dùng riêng cho xuất excel Định kỳ cuối kỳ (có chọn tiêu chí))
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="subjectID"></param>
        /// <param name="TypeOfTeacher"></param>
        /// <param name="evaluationID"></param>
        /// <returns></returns>
        List<EvaluationCommentsBO> getListEvaluationCommentsBySchoolExport(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID);

        /// <summary>
        /// Insert hoac update nhan xet cap 1 theo bulkcopy
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdate(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate,ref List<ActionAuditDataBO> lstActionAuditData);

        /// <summary>
        /// Xoa du lieu theo bulkcopy
        /// </summary>
        /// <param name="IDList"></param>
        /// <param name="dic"></param>
        /// <param name="isRetest">Xoa du lieu thi lai: true: xoa</param>
        void BulkDeleteEvaluationComments(List<long> IDList, IDictionary<string, object> dic, bool isRetest, ref List<ActionAuditDataBO> lstActionAuditData);
        /// <summary>
        /// insert Import (import khong phan biet thang)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID"></param>
        /// <param name="evaluationCriteriaID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        /// <param name="listInsertOrUpdateSummed"></param>
        /// <param name="listInsertOrUpdateSummedEnding"></param>
        void InsertOrUpdateEvaluationCommentsImport(int monthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, List<SummedEvaluation> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding);

        string CheckLockMark(int classID, int schoolID, int academicYearID);


        /// <summary>
        /// bam dieu kien tim kiem
        /// </summary>
        /// <param name="reportEC"></param>
        /// <returns></returns>
        string GetHashKey(ReportEvalutionComments reportEC);

        /// <summary>
        /// xuat bao cao theo doi CLGD_GVCN
        /// </summary>
        /// <param name="reportEC"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        ProcessedReport InsertReport_HeadTeacher(ReportEvalutionComments reportEC, Stream data);
        ProcessedReport GetReport_HeadTeacher(ReportEvalutionComments reportEC);
        Stream CreateReport_HeadTeacher(ReportEvalutionComments reportEC);

        /// <summary>
        /// Xuat bao cao theo doi CLGD_GVBM
        /// </summary>
        /// <param name="reportEC"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        ProcessedReport InsertReport_SubjectTeacher(ReportEvalutionComments reportEC, Stream data);
        ProcessedReport GetReport_SubjectTeacher(ReportEvalutionComments reportEC);
        Stream CreateReport_SubjectTeacher(ReportEvalutionComments reportEC);


        /// <summary>
        /// Lay danh sach nhan xet danh gia cua tat cac cac mon hoc cua lop theo cac tieu chi:hoc ky cho GVBM
        ///
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClass(int classId, IDictionary<string, object> dic, List<int> lstSubject);

        /// <summary>
        /// Thuc hien them moi hoac cap nhat tat cac cac mon hoc trong lop
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="typeOfTeacher"></param>
        /// <param name="listInsertOrUpdate"></param>
        void InsertOrUpdatebyClass(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate);

        /// <summary>
        /// Lay danh sach nhat xet nhan xet danh gia cac cac thang cua lop cua GVCN
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsMonthbyClassofHeadTeacher(int classId, IDictionary<string, object> dic);
        /// <summary>
        /// Lay danh sach nhan xet danh gia hoc ky cua GVCN
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="dic"></param>
        /// <param name="lstSubject"></param>
        /// <returns></returns>
        List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClassAndHeadTeacher(int classId, IDictionary<string, object> dic, List<int> lstSubject);
        /// <summary>
        /// Lay danh sach danh gia cua tat ca cac mon theo thang
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<string> GetListComment(IDictionary<string, object> dic);

        /// <summary>
        /// Lay danh sach nhan xet
        /// </summary>
        /// <param name="dic"> </param>
        /// <returns></returns>
        IQueryable<EvaluationComments> getListEvaluationByListSubject(IDictionary<string, object> dic);


        /// <summary>
        /// Copy data by condition
        /// </summary>
        /// <param name="objDic"></param>
        /// <param name="iCopyObject"></param>
        /// <param name="iCopyLocation"></param>
        /// <param name="month"></param>
        /// <param name="subjectIdSelect"></param>
        void CopyDataEvaluation(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect, ref List<ActionAuditDataBO> lstActionAuditData);

        List<EvaluationCommentActionAuditBO> CopyDataEvaluationForHeadTeacher(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect);
        /// <summary>
        /// Lay danh sach autocomplete khi nhan xet (cap 1)
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<string> getListCommentAutoComplete(IDictionary<string, object> dic);

        /// <summary>
        /// lay danh sach nhan set tat ca cac thang theo giao vien
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        List<EvaluationCommentsBO> GetListEvaluationComments(IDictionary<string, object> dic);
        List<EvaluationCommentsBO> getCommentSchoolVNEN(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID);
        List<EvaluationCommentsBO> getEvaluationCommnetsBySchoolOfPrimary(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID, int userAccountID, bool viewAll = true);
    }
}
