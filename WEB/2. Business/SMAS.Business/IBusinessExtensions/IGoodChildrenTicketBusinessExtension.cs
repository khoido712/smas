/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IGoodChildrenTicketBusiness 
    {
        IQueryable<GoodChildrenTicket> SearchBySchool(int SchoolID, IDictionary<string, object> dic);
        void Insert(IDictionary<string, object> dic, List<GoodChildrenTicket> lst);
        void SearchToDelete(IDictionary<string, object> dic);
        void InsertOrUpdateGoodChildren(List<GoodChildrenTicket> lstGoodChildren, IDictionary<string, object> dic);
        void DeleteGoodChildren(IDictionary<string, object> dic);
       List<PreschoolResultBO> GetGoodTicketOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, DateTime month, int week);
        List<PreschoolResultBO> GetMealMenuOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, DateTime fromDate, DateTime toDate);
        List<PreschoolResultBO> GetPhysicalResultOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int month);
        List<PreschoolResultBO> GetGrowthEvaluationOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID);
        List<PreschoolResultBO> GetDailyActivityOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, DateTime date);
    }
}