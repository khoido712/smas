/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExaminationSubjectBusiness 
    {
        void UpdateAll(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, List<int> ListSubjectID
            , List<DateTime> ListStartTime, List<int> ListDurationInMinute, List<bool> ListHasDetachableHead);
        IQueryable<ExaminationSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null);
        int GetSubjectMarkType(int ExaminationSubjectID, int? AppliedLevel);
        Dictionary<int, int?> GetSubjectMarkType(int AcademicYearID, List<int> ExaminationSubjectID, int? AppliedLevel);
        void CancelExaminationSubject(int SchoolID, int AppliedLevel, int ExaminationID, int EducationLevelID, List<int> ListSubjectID);

    }
}
