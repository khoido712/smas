﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.IBusiness
{
    public partial interface IStatisticTertiaryStartYearBusiness
    {
        void InsertStatisticTertiaryStartYear(StatisticTertiaryStartYear entity);
        IQueryable<StatisticTertiaryStartYear> Search(IDictionary<string, object> dic);
    }
}
