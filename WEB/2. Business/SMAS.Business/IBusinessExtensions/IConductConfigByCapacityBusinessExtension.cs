/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IConductConfigByCapacityBusiness 
    {
        void Insert(List<ConductConfigByCapacity> lstConductConfigByCapacity);
        void Update(List<ConductConfigByCapacity> lstConductConfigByCapacity);
        void Delete(List<ConductConfigByCapacity> lstConductConfigByCapacity, int SchoolID);
        IQueryable<ConductConfigByCapacity> Search(IDictionary<string, object> dic);
        void DeleteConductConfig(List<ConductConfigByCapacity> lstConductConfigByCapacity,int SchoolID);
    }
}