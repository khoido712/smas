﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IRatedCommentPupilBusiness
    {
        IQueryable<RatedCommentPupil> Search(IDictionary<string, object> dic);
        IQueryable<VRatedCommentPupil> VSearch(IDictionary<string, object> dic);
        void InsertOrUpdate(List<RatedCommentPupilBO> lstRatedCommentPupilBO, IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit);
        void DeleteCommentPupil(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail);
        ProcessedReport GetProcessReportRepord(IDictionary<string, object> dic, bool isZip = false);
        ProcessedReport GetProcessReportRepordCapQua(IDictionary<string, object> dic);
        ProcessedReport InsertProcessReport(IDictionary<string, object> dic, Stream data, bool isZip = false);
        Stream CreateSubjectAndEducationGroupReport(IDictionary<string, object> dic);
        Stream CreateCapacityAndQualityReport(IDictionary<string, object> dic);
        ProcessedReport InsertProcessCapQuaReport(IDictionary<string, object> dic, Stream data);
        void InsertOrUpdateToMobile(RatedCommentPupilBO objRatedCommentPupilBO, IDictionary<string, object> dic);
        void ImportFromInputMark(List<Object> insertList, List<Object> updateList);
        void DownloadZipFileSubjectAndEducationGroup(IDictionary<string, object> dic, out string folderSaveFile);
List<RatedCommentPupilBO> GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, int semesterID);
    }
}