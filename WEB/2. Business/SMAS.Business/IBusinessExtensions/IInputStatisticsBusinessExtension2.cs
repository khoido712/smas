﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;

namespace SMAS.Business.IBusiness
{
    public partial interface IInputStatisticsBusiness
    {
        string GroupByDistrictStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate);
        string SchoolStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate);
        ProcessedReport ExportGroupByDistrictStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate);
        ProcessedReport ExportSchoolStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate);
    }
}
