/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Business.IBusiness
{ 
    public partial interface IExamPupilViolateBusiness 
    {
        IQueryable<ExamPupilViolate> Search(IDictionary<string, object> search);
        List<ExamPupilViolateBO> GetListExamPupil(IDictionary<string, object> search);
        IQueryable<ExamViolationType> SearchViolationType(IDictionary<string, object> search);
        List<ExamPupilViolateBO> GetListExamPupilViolate(IDictionary<string, object> search);
        void InsertListExamPupilViolate(IDictionary<string, object> insertInfo, long[] ExamPupilID,
            long[] CheckExamPupilID, string[] ContentViolate, int[] ViolateTypeID);
        void UpdateExamPupilViolate(long examPupilViolateID, string contentViolate, long examViolationType);
        void DeleteExamPupilViolate(List<long> listExamPupilViolateID);

        ProcessedReport GetExamPupilViolateReport(IDictionary<string, object> dic);
        Stream CreateExamPupilViolateReport(IDictionary<string, object> dic);
        ProcessedReport InsertExamPupilViolateReport(IDictionary<string, object> dic, Stream data);
    }
}