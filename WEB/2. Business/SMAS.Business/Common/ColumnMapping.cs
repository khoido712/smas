﻿using System.Collections.Generic;

namespace SMAS.Business.Common
{
    public class ColumnMapping
    {
        private static readonly ColumnMapping _instance = new ColumnMapping();

        public static ColumnMapping Instance
        {
            get
            {
                return _instance;
            }
        }


        public IDictionary<string, object> AcademicYear()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACADEMIC_YEAR()
            //columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("DisplayTitle", "DISPLAY_TITLE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("FirstSemesterStartDate", "FIRST_SEMESTER_START_DATE");
            columnMap.Add("FirstSemesterEndDate", "FIRST_SEMESTER_END_DATE");
            columnMap.Add("SecondSemesterStartDate", "SECOND_SEMESTER_START_DATE");
            columnMap.Add("SecondSemesterEndDate", "SECOND_SEMESTER_END_DATE");
            columnMap.Add("ConductEstimationType", "CONDUCT_ESTIMATION_TYPE");
            columnMap.Add("RankingCriteria", "RANKING_CRITERIA");
            columnMap.Add("IsSecondSemesterToSemesterAll", "IS_SECOND_SEMESTER_TO_SEM_ALL");
            columnMap.Add("IsTeacherCanViewAll", "IS_TEACHER_CAN_VIEW_ALL");
            columnMap.Add("IsHeadTeacherCanSendSMS", "IS_HEAD_TEACHER_CAN_SEND_SMS");
            columnMap.Add("IsSubjectTeacherCanSendSMS", "IS_SUBJECT_TEACHER_CAN_SEN_SMS");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsShowAvatar", "IS_SHOW_AVATAR");
            columnMap.Add("IsShowCalendar", "IS_SHOW_CALENDAR");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("CreatedYear", "CREATED_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsClassification", "IS_CLASSIFICATION");
            columnMap.Add("IsLockNumOrdinal", "IS_LOCK_NUM_ORDINAL");
            columnMap.Add("IsLockPupilProfile", "IS_LOCK_PUPIL_PROFILE");
            columnMap.Add("IsMovedHistory", "IS_MOVED_HISTORY");
            columnMap.Add("IsActive", "IS_ACTIVE");
            return columnMap;

        }
        public IDictionary<string, object> AcademicYearOfProvince()
        { // TypeName="Models.AcademicYearOfProvince()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACADEMIC_YEAR_OF_PROVINCE()
            columnMap.Add("AcademicYearOfProvinceID", "ACADEMIC_YEAR_OF_PROVINCE_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DisplayTitle", "DISPLAY_TITLE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("FirstSemesterStartDate", "FIRST_SEMESTER_START_DATE");
            columnMap.Add("FirstSemesterEndDate", "FIRST_SEMESTER_END_DATE");
            columnMap.Add("SecondSemesterStartDate", "SECOND_SEMESTER_START_DATE");
            columnMap.Add("SecondSemesterEndDate", "SECOND_SEMESTER_END_DATE");
            columnMap.Add("IsInheritData", "IS_INHERIT_DATA");
            columnMap.Add("IsSchoolEdit", "IS_SCHOOL_EDIT");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> ActionAudit()
        { // TypeName="Models.ActionAudit()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTION_AUDIT()
            columnMap.Add("ActionAuditID", "ACTION_AUDIT_ID");
            columnMap.Add("UserID", "USER_ID");
            columnMap.Add("Controller", "CONTROLLER");
            columnMap.Add("Action", "ACTION");
            columnMap.Add("Parameter", "PARAMETER");
            columnMap.Add("BeginAuditTime", "BEGIN_AUDIT_TIME");
            columnMap.Add("EndAuditTime", "END_AUDIT_TIME");
            columnMap.Add("IP", "IP");
            columnMap.Add("Referer", "REFERER");
            columnMap.Add("ObjectId", "OBJECT_ID");
            columnMap.Add("oldObjectValue", "OLD_OBJECT_VALUE");
            columnMap.Add("newObjectValue", "NEW_OBJECT_VALUE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("userFunction", "USER_FUNCTION");
            columnMap.Add("userAction", "USER_ACTION");
            columnMap.Add("userDescription", "USER_DESCRIPTION");
            columnMap.Add("LastDigitNumberSchool", "LAST_DIGIT_NUMBER_SCHOOL");
            columnMap.Add("ExecutedDuration", "EXECUTED_DURATION");
            columnMap.Add("StatusRequest", "STATUS_REQUEST");
            return columnMap;

        }
        public IDictionary<string, object> Activity()
        { // TypeName="Models.Activity()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY()
            columnMap.Add("ActivityID", "ACTIVITY_ID");
            columnMap.Add("ActivityPlanID", "ACTIVITY_PLAN_ID");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("ActivityName", "ACTIVITY_NAME");
            columnMap.Add("FromTime", "FROM_TIME");
            columnMap.Add("ToTime", "TO_TIME");
            columnMap.Add("ActivityTypeID", "ACTIVITY_TYPE_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("ActivityDate", "ACTIVITY_DATE");
            return columnMap;
        }
        public IDictionary<string, object> ActivityOfClass()
        { // TypeName="Models.ActivityOfClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY_OF_CLASS()
            columnMap.Add("ActivityOfClassID", "ACTIVITY_OF_CLASS_ID");
            columnMap.Add("ActivityPlanID", "ACTIVITY_PLAN_ID");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("ActivityOfClassName", "ACTIVITY_OF_CLASS_NAME");
            columnMap.Add("FromTime", "FROM_TIME");
            columnMap.Add("ToTime", "TO_TIME");
            columnMap.Add("ActivityTypeID", "ACTIVITY_TYPE_ID");
            columnMap.Add("ActivityDate", "ACTIVITY_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ActivityOfPupil()
        { // TypeName="Models.ActivityOfPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY_OF_PUPIL()
            columnMap.Add("ActivityOfPupilID", "ACTIVITY_OF_PUPIL_ID");
            columnMap.Add("CommentOfTeacher", "COMMENT_OF_TEACHER");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ActivityTypeID", "ACTIVITY_TYPE_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("ActivityOfClassID", "ACTIVITY_OF_CLASS_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> ActivityPlan()
        { // TypeName="Models.ActivityPlan()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY_PLAN()
            columnMap.Add("ActivityPlanID", "ACTIVITY_PLAN_ID");
            columnMap.Add("PlannedDate", "PLANNED_DATE");
            columnMap.Add("ActivityPlanName", "ACTIVITY_PLAN_NAME");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CommentOfTeacher", "COMMENT_OF_TEACHER");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> ActivityPlanClass()
        { // TypeName="Models.ActivityPlanClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY_PLAN_CLASS()
            columnMap.Add("ActivityPlanClassID", "ACTIVITY_PLAN_CLASS_ID");
            columnMap.Add("ActivityPlanID", "ACTIVITY_PLAN_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> ActivityType()
        { // TypeName="Models.ActivityType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ACTIVITY_TYPE()
            columnMap.Add("ActivityTypeID", "ACTIVITY_TYPE_ID");
            columnMap.Add("ActivityTypeName", "ACTIVITY_TYPE_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> AddMenuForChildren()
        { // TypeName="Models.AddMenuForChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ADD_MENU_FOR_CHILDREN()
            columnMap.Add("AddMenuForChildrenID", "ADD_MENU_FOR_CHILDREN_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("WeeklyMenuID", "WEEKLY_MENU_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> AlertMessage()
        { // TypeName="Models.AlertMessage()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ALERT_MESSAGE()
            columnMap.Add("AlertMessageID", "ALERT_MESSAGE_ID");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("FileUrl", "FILE_URL");
            columnMap.Add("PublishedDate", "PUBLISHED_DATE");
            columnMap.Add("ContentMessage", "CONTENT_MESSAGE");
            columnMap.Add("IsPublish", "IS_PUBLISH");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("TypeID", "TYPE_ID");
            columnMap.Add("CreatedUserID", "CREATED_USER_ID");
            columnMap.Add("UnitID", "UNIT_ID");
            columnMap.Add("IsLocal", "IS_LOCAL");
            return columnMap;

        }
        public IDictionary<string, object> AlertMessageDetail()
        { // TypeName="Models.AlertMessageDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ALERT_MESSAGE_DETAIL()
            columnMap.Add("AlertMessageDetailID", "ALERT_MESSAGE_DETAIL_ID");
            columnMap.Add("AlertMessageID", "ALERT_MESSAGE_ID");
            columnMap.Add("UserID", "USER_ID");
            columnMap.Add("ReceiverTypeID", "RECEIVER_TYPE_ID");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipClass()
        { // TypeName="Models.ApprenticeshipClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_CLASS()
            columnMap.Add("ClassName", "CLASS_NAME");
            columnMap.Add("HeadTeacherID", "HEAD_TEACHER_ID");
            columnMap.Add("ApprenticeshipSubjectID", "APPRENTICESHIP_SUBJECT_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("SubjectType", "SUBJECT_TYPE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("ApprenticeshipClassID", "APPRENTICESHIP_CLASS_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipGroup()
        { // TypeName="Models.ApprenticeshipGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_GROUP()
            columnMap.Add("ApprenticeshipGroupID", "APPRENTICESHIP_GROUP_ID");
            columnMap.Add("GroupName", "GROUP_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipSubject()
        { // TypeName="Models.ApprenticeshipSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_SUBJECT()
            columnMap.Add("ApprenticeshipSubjectID", "APPRENTICESHIP_SUBJECT_ID");
            columnMap.Add("SubjectName", "SUBJECT_NAME");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipTraining()
        { // TypeName="Models.ApprenticeshipTraining()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_TRAINING()
            columnMap.Add("ApprenticeshipTrainingID", "APPRENTICESHIP_TRAINING_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ApprenticeshipSubjectID", "APPRENTICESHIP_SUBJECT_ID");
            columnMap.Add("Result", "RESULT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("ActionMark", "ACTION_MARK");
            columnMap.Add("TheoryMark", "THEORY_MARK");
            columnMap.Add("Ranking", "RANKING");
            columnMap.Add("ApprenticeShipClassID", "APPRENTICE_SHIP_CLASS_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipTrainingAbsence()
        { // TypeName="Models.ApprenticeshipTrainingAbsence()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_TRAINING_ABS()
            columnMap.Add("ApprenticeshipTrainingAbsenceID", "APPRENTICESHIP_TRAINING_ABS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("SectionInDay", "SECTION_IN_DAY");
            columnMap.Add("AbsentDate", "ABSENT_DATE");
            columnMap.Add("IsAccepted", "IS_ACCEPTED");
            columnMap.Add("ApprenticeshipClassID", "APPRENTICESHIP_CLASS_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> ApprenticeshipTrainingSchedule()
        { // TypeName="Models.ApprenticeshipTrainingSchedule()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPRENTICESHIP_TRAINING_SCH()
            columnMap.Add("ApprenticeshipTrainingScheduleID", "APPRENTICESHIP_TRAINING_SCH_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Month", "MONTH");
            columnMap.Add("SectionInDay", "SECTION_IN_DAY");
            columnMap.Add("ScheduleDetail", "SCHEDULE_DETAIL");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModiffedDate", "MODIFFED_DATE");
            columnMap.Add("ApprenticeshipClassID", "APPRENTICESHIP_CLASS_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Area()
        { // TypeName="Models.Area()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="AREA()
            columnMap.Add("AreaID", "AREA_ID");
            columnMap.Add("AreaCode", "AREA_CODE");
            columnMap.Add("AreaName", "AREA_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> AutoSMSMonitor()
        { // TypeName="Models.AutoSMSMonitor()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="AUTO_SMSMONITOR()
            columnMap.Add("MonitorID", "MONITOR_ID");
            columnMap.Add("TypeCode", "TYPE_CODE");
            columnMap.Add("TotalChecked", "TOTAL_CHECKED");
            columnMap.Add("TotalSent", "TOTAL_SENT");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Message", "MESSAGE");
            columnMap.Add("SPName", "SPNAME");
            columnMap.Add("StartTime", "START_TIME");
            columnMap.Add("EndTime", "END_TIME");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> Calendar()
        { // TypeName="Models.Calendar()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CALENDAR()
            columnMap.Add("CalendarID", "CALENDAR_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("SubjectOrder", "SUBJECT_ORDER");
            columnMap.Add("NumberOfPeriod", "NUMBER_OF_PERIOD");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CalendarSchedule()
        { // TypeName="Models.CalendarSchedule()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CALENDAR_SCHEDULE()
            columnMap.Add("CalendarScheduleID", "CALENDAR_SCHEDULE_ID");
            columnMap.Add("CalendarID", "CALENDAR_ID");
            columnMap.Add("SubjectTitle", "SUBJECT_TITLE");
            columnMap.Add("Preparation", "PREPARATION");
            columnMap.Add("HomeWork", "HOME_WORK");
            columnMap.Add("ApplyDate", "APPLY_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CallDetailRecord()
        { // TypeName="Models.CallDetailRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CALL_DETAIL_RECORD()
            columnMap.Add("CallDetailRecordID", "CALL_DETAIL_RECORD_ID");
            columnMap.Add("SMSTeacherContractID", "SMSTEACHER_CONTRACT_ID");
            columnMap.Add("CallingNumber", "CALLING_NUMBER");
            columnMap.Add("CalledNumber", "CALLED_NUMBER");
            columnMap.Add("StaDatetime", "STA_DATETIME");
            columnMap.Add("Seq", "SEQ");
            columnMap.Add("NumberOfSMS", "NUMBER_OF_SMS");
            columnMap.Add("Supplier", "SUPPLIER");
            columnMap.Add("Year", "YEAR");
            return columnMap;

        }
        public IDictionary<string, object> Candidate()
        { // TypeName="Models.Candidate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CANDIDATE()
            columnMap.Add("CandidateID", "CANDIDATE_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("RoomID", "ROOM_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("NamedListCode", "NAMED_LIST_CODE");
            columnMap.Add("NamedListNumber", "NAMED_LIST_NUMBER");
            columnMap.Add("HasReason", "HAS_REASON");
            columnMap.Add("ReasonDetail", "REASON_DETAIL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CandidateAbsence()
        { // TypeName="Models.CandidateAbsence()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CANDIDATE_ABSENCE()
            columnMap.Add("AbsentCandidateID", "ABSENT_CANDIDATE_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("CandidateID", "CANDIDATE_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("RoomID", "ROOM_ID");
            columnMap.Add("HasReason", "HAS_REASON");
            columnMap.Add("ReasonDetail", "REASON_DETAIL");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CapacityLevel()
        { // TypeName="Models.CapacityLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CAPACITY_LEVEL()
            columnMap.Add("CapacityLevelID", "CAPACITY_LEVEL_ID");
            columnMap.Add("CapacityLevel1", "CAPACITY_LEVEL");
            columnMap.Add("AppliedForPrimary", "APPLIED_FOR_PRIMARY");
            columnMap.Add("AppliedForSecondary", "APPLIED_FOR_SECONDARY");
            columnMap.Add("AppliedForTertiary", "APPLIED_FOR_TERTIARY");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CapacityStatistic()
        { // TypeName="Models.CapacityStatistic()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CAPACITY_STATISTIC()
            columnMap.Add("CapacityStatisticID", "CAPACITY_STATISTIC_ID");
            columnMap.Add("ProcessedDate", "PROCESSED_DATE");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SubCommitteeID", "SUB_COMMITTEE_ID");
            columnMap.Add("SentToSupervisor", "SENT_TO_SUPERVISOR");
            columnMap.Add("SentDate", "SENT_DATE");
            columnMap.Add("CapacityLevel01", "CAPACITY_LEVEL_01");
            columnMap.Add("CapacityLevel02", "CAPACITY_LEVEL_02");
            columnMap.Add("CapacityLevel03", "CAPACITY_LEVEL_03");
            columnMap.Add("CapacityLevel04", "CAPACITY_LEVEL_04");
            columnMap.Add("CapacityLevel05", "CAPACITY_LEVEL_05");
            columnMap.Add("CapacityLevel06", "CAPACITY_LEVEL_06");
            columnMap.Add("CapacityLevel07", "CAPACITY_LEVEL_07");
            columnMap.Add("PupilTotal", "PUPIL_TOTAL");
            columnMap.Add("BelowAverage", "BELOW_AVERAGE");
            columnMap.Add("OnAverage", "ON_AVERAGE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsFemale", "IS_FEMALE");
            columnMap.Add("IsEthnic", "IS_ETHNIC");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            return columnMap;

        }
        public IDictionary<string, object> CellDefinition()
        { // TypeName="Models.CellDefinition()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CELL_DEFINITION()
            columnMap.Add("CellDefinitionID", "CELL_DEFINITION_ID");
            columnMap.Add("CellAddress", "CELL_ADDRESS");
            columnMap.Add("SheetName", "SHEET_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("ReportCode", "REPORT_CODE");
            return columnMap;

        }
        public IDictionary<string, object> ClassAssigment()
        { // TypeName="Models.ClassAssigment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_ASSIGMENT()
            columnMap.Add("ClassAssigmentID", "CLASS_ASSIGMENT_ID");
            columnMap.Add("IsHeadTeacher", "IS_HEAD_TEACHER");
            columnMap.Add("AssignmentWork", "ASSIGNMENT_WORK");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassEmulation()
        { // TypeName="Models.ClassEmulation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_EMULATION()
            columnMap.Add("ClassEmulationID", "CLASS_EMULATION_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Month", "MONTH");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("TotalEmulationMark", "TOTAL_EMULATION_MARK");
            columnMap.Add("TotalPenalizedMark", "TOTAL_PENALIZED_MARK");
            columnMap.Add("TotalMark", "TOTAL_MARK");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassEmulationDetail()
        { // TypeName="Models.ClassEmulationDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_EMULATION_DETAIL()
            columnMap.Add("ClassEmulationDetailID", "CLASS_EMULATION_DETAIL_ID");
            columnMap.Add("ClassEmulationID", "CLASS_EMULATION_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EmulationCriteriaId", "EMULATION_CRITERIA_ID");
            columnMap.Add("PenalizedMark", "PENALIZED_MARK");
            columnMap.Add("PenalizedDate", "PENALIZED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassForwarding()
        { // TypeName="Models.ClassForwarding()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_FORWARDING()
            columnMap.Add("ClassForwardingID", "CLASS_FORWARDING_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClosedClassID", "CLOSED_CLASS_ID");
            columnMap.Add("ForwardedClassID", "FORWARDED_CLASS_ID");
            columnMap.Add("RepeatedClassID", "REPEATED_CLASS_ID");
            columnMap.Add("TotalForwardedPupil", "TOTAL_FORWARDED_PUPIL");
            columnMap.Add("TotalRepeatedPupil", "TOTAL_REPEATED_PUPIL");
            columnMap.Add("ForwardedDate", "FORWARDED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassMovement()
        { // TypeName="Models.ClassMovement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_MOVEMENT()
            columnMap.Add("ClassMovementID", "CLASS_MOVEMENT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("FromClassID", "FROM_CLASS_ID");
            columnMap.Add("ToClassID", "TO_CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("MovedDate", "MOVED_DATE");
            columnMap.Add("Reason", "REASON");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("M_DateInto", "M_DATE_INTO");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassProfile()
        { // TypeName="Models.ClassProfile()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_PROFILE()
            //columnMap.Add("ClassProfileID", "CLASS_PROFILE_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SchoolSubsidiaryID", "SCHOOL_SUBSIDIARY_ID");
            columnMap.Add("IsCombinedClass", "IS_COMBINED_CLASS");
            columnMap.Add("CombinedClassCode", "COMBINED_CLASS_CODE");
            columnMap.Add("SubCommitteeID", "SUB_COMMITTEE_ID");
            columnMap.Add("HeadTeacherID", "HEAD_TEACHER_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("DisplayName", "DISPLAY_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("IsSpecializedClass", "IS_SPECIALIZED_CLASS");
            columnMap.Add("FirstForeignLanguageID", "FIRST_FOREIGN_LANGUAGE_ID");
            columnMap.Add("SecondForeignLanguageID", "SECOND_FOREIGN_LANGUAGE_ID");
            columnMap.Add("ApprenticeshipGroupID", "APPRENTICESHIP_GROUP_ID");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("IsITClass", "IS_ITCLASS");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("VemisCode", "VEMIS_CODE");
            columnMap.Add("IsVnenClass", "IS_VNEN_CLASS");
            columnMap.Add("SeperateKey", "SEPERATE_KEY");
            columnMap.Add("IsActive", "IS_ACTIVE");
            return columnMap;

        }
        public IDictionary<string, object> ClassPropertyCat()
        { // TypeName="Models.ClassPropertyCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_PROPERTY_CAT()
            columnMap.Add("ClassPropertyCatID", "CLASS_PROPERTY_CAT_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("PropertyGroup", "PROPERTY_GROUP");
            columnMap.Add("M_PropertyClassID", "M_PROPERTY_CLASS_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("ApplyTrainingType", "APPLY_TRAINING_TYPE");
            return columnMap;

        }
        public IDictionary<string, object> ClassSubject()
        { // TypeName="Models.ClassSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_SUBJECT()
            //columnMap.Add("ClassSubjectID", "CLASS_SUBJECT_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsSpecializedSubject", "IS_SPECIALIZED_SUBJECT");
            columnMap.Add("SectionPerWeekFirstSemester", "SECTION_PER_WEEK_FIRST_SEM");
            columnMap.Add("SectionPerWeekSecondSemester", "SECTION_PER_WEEK_SECOND_SEM");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("IsSecondForeignLanguage", "IS_SECOND_FOREIGN_LANGUAGE");
            columnMap.Add("OrderInSchoolReport", "ORDER_IN_SCHOOL_REPORT");
            columnMap.Add("FirstSemesterCoefficient", "FIRST_SEMESTER_COEFFICIENT");
            columnMap.Add("SecondSemesterCoefficient", "SECOND_SEMESTER_COEFFICIENT");
            columnMap.Add("AppliedType", "APPLIED_TYPE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("SubjectIDIncrease", "SUBJECT_ID_INCREASE");
            columnMap.Add("MMinFirstSemester", "M_MIN_FIRST_SEMESTER");
            columnMap.Add("PMinFirstSemester", "P_MIN_FIRST_SEMESTER");
            columnMap.Add("VMinFirstSemester", "V_MIN_FIRST_SEMESTER");
            columnMap.Add("MMinSecondSemester", "M_MIN_SECOND_SEMESTER");
            columnMap.Add("PMinSecondSemester", "P_MIN_SECOND_SEMESTER");
            columnMap.Add("VMinSecondSemester", "V_MIN_SECOND_SEMESTER");
            columnMap.Add("IsSubjectVNEN", "IS_SUBJECT_VNEN");
            return columnMap;

        }
        public IDictionary<string, object> ClassSupervisorAssignment()
        { // TypeName="Models.ClassSupervisorAssignment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASS_SUPERVISOR_ASSIGNMENT()
            columnMap.Add("ClassSupervisorAssignmentID", "CLASS_SUPERVISOR_ASSIGNMENT_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PermissionLevel", "PERMISSION_LEVEL");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassificationCriteria()
        { // TypeName="Models.ClassificationCriteria()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASSIFICATION_CRITERIA()
            columnMap.Add("ClassificationCriteriaID", "CLASSIFICATION_CRITERIA_ID");
            columnMap.Add("EffectDate", "EFFECT_DATE");
            columnMap.Add("Genre", "GENRE");
            columnMap.Add("TypeConfigID", "TYPE_CONFIG_ID");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassificationCriteriaDetail()
        { // TypeName="Models.ClassificationCriteriaDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASSIFICATION_CRITERIA_DETAIL()
            columnMap.Add("ClassificationCriteriaDetailID", "CLASSIFICATION_CRITERIA_DET_ID");
            columnMap.Add("ClassificationCriteriaID", "CLASSIFICATION_CRITERIA_ID");
            columnMap.Add("Month", "MONTH");
            columnMap.Add("IndexValue", "INDEX_VALUE");
            columnMap.Add("ClassificationLevelHealthID", "CLASSIFICATION_LEVEL_HEALTH_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ClassificationLevelHealth()
        { // TypeName="Models.ClassificationLevelHealth()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CLASSIFICATION_LEVEL_HEALTH()
            columnMap.Add("ClassificationLevelHealthID", "CLASSIFICATION_LEVEL_HEALTH_ID");
            columnMap.Add("Level", "LEVEL");
            columnMap.Add("SymbolConfigID", "SYMBOL_CONFIG_ID");
            columnMap.Add("TypeConfigID", "TYPE_CONFIG_ID");
            columnMap.Add("EvaluationConfigID", "EVALUATION_CONFIG_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CodeConfig()
        { // TypeName="Models.CodeConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CODE_CONFIG()
            columnMap.Add("CodeConfigID", "CODE_CONFIG_ID");
            columnMap.Add("CodeType", "CODE_TYPE");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("IsChoiceSchool", "IS_CHOICE_SCHOOL");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("MiddleCodeType", "MIDDLE_CODE_TYPE");
            columnMap.Add("IsStringIdentify", "IS_STRING_IDENTIFY");
            columnMap.Add("StringIdentify", "STRING_IDENTIFY");
            columnMap.Add("NumberLength", "NUMBER_LENGTH");
            columnMap.Add("IsSchoolModify", "IS_SCHOOL_MODIFY");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_UnitID", "M_UNIT_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }

        public IDictionary<string, object> ConfigConductRanking()
        { // TypeName="Models.ConfigConductRanking()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONFIG_CONDUCT_RANKING()
            columnMap.Add("ConfigConductRankingID", "CONFIG_CONDUCT_RANKING_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LastDigitSchool", "LAST_DIGIT_SCHOOL");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }

        public IDictionary<string, object> Commune()
        { // TypeName="Models.Commune()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="COMMUNE()
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("CommuneCode", "COMMUNE_CODE");
            columnMap.Add("CommuneName", "COMMUNE_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("CommuneExamCode", "COMMUNE_EXAM_CODE");
            columnMap.Add("IsDifficulty", "IS_DIFFICULTY");
            return columnMap;

        }
        public IDictionary<string, object> ConcurrentWorkAssignment()
        { // TypeName="Models.ConcurrentWorkAssignment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONCURRENT_WORK_ASSIGNMENT()
            columnMap.Add("ConcurrentWorkAssignmentID", "CONCURRENT_WORK_ASSIGNMENT_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("ConcurrentWorkTypeID", "CONCURRENT_WORK_TYPE_ID");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConcurrentWorkReplacement()
        { // TypeName="Models.ConcurrentWorkReplacement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONCURRENT_WORK_REPLACEMENT()
            columnMap.Add("ConcurrentWorkReplacementID", "CONCURRENT_WORK_REPLACEMENT_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("ConcurrentWorkAssignmentID", "CONCURRENT_WORK_ASSIGNMENT_ID");
            columnMap.Add("ReplacedTeacherID", "REPLACED_TEACHER_ID");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_OldReplacedTeacherID", "M_OLD_REPLACED_TEACHER_ID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConcurrentWorkType()
        { // TypeName="Models.ConcurrentWorkType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONCURRENT_WORK_TYPE()
            columnMap.Add("ConcurrentWorkTypeID", "CONCURRENT_WORK_TYPE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SectionPerWeek", "SECTION_PER_WEEK");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConductConfigByCapacity()
        { // TypeName="Models.ConductConfigByCapacity()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONDUCT_CONFIG_BY_CAPACITY()
            columnMap.Add("ConductConfigByCapacityID", "CONDUCT_CONFIG_BY_CAPACITY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("CapacityLevelID", "CAPACITY_LEVEL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConductConfigByViolation()
        { // TypeName="Models.ConductConfigByViolation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONDUCT_CONFIG_BY_VIOLATION()
            columnMap.Add("ConductConfigByViolationID", "CONDUCT_CONFIG_BY_VIOLATION_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("MinValue", "MIN_VALUE");
            columnMap.Add("MaxValue", "MAX_VALUE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConductEvaluationDetail()
        { // TypeName="Models.ConductEvaluationDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONDUCT_EVALUATION_DETAIL()
            columnMap.Add("ConductEvaluationDetailID", "CONDUCT_EVALUATION_DETAIL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Task1Evaluation", "TASK_1EVALUATION");
            columnMap.Add("Task2Evaluation", "TASK_2EVALUATION");
            columnMap.Add("Task3Evaluation", "TASK_3EVALUATION");
            columnMap.Add("Task4Evaluation", "TASK_4EVALUATION");
            columnMap.Add("Task5Evaluation", "TASK_5EVALUATION");
            columnMap.Add("EvaluatedDate", "EVALUATED_DATE");
            columnMap.Add("UpdatedDate", "UPDATED_DATE");
            columnMap.Add("M_Year", "M_YEAR");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConductLevel()
        { // TypeName="Models.ConductLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONDUCT_LEVEL()
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ConductStatistic()
        { // TypeName="Models.ConductStatistic()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONDUCT_STATISTIC()
            columnMap.Add("ConductStatisticID", "CONDUCT_STATISTIC_ID");
            columnMap.Add("ProcessedDate", "PROCESSED_DATE");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SubCommitteeID", "SUB_COMMITTEE_ID");
            columnMap.Add("SentToSupervisor", "SENT_TO_SUPERVISOR");
            columnMap.Add("SentDate", "SENT_DATE");
            columnMap.Add("ConductLevel01", "CONDUCT_LEVEL_01");
            columnMap.Add("ConductLevel02", "CONDUCT_LEVEL_02");
            columnMap.Add("ConductLevel03", "CONDUCT_LEVEL_03");
            columnMap.Add("ConductLevel04", "CONDUCT_LEVEL_04");
            columnMap.Add("PupilTotal", "PUPIL_TOTAL");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsFemale", "IS_FEMALE");
            columnMap.Add("IsEthnic", "IS_ETHNIC");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("OnAverage", "ON_AVERAGE");
            return columnMap;

        }
        public IDictionary<string, object> ContactGroup()
        { // TypeName="Models.ContactGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONTACT_GROUP()
            columnMap.Add("ContactGroupID", "CONTACT_GROUP_ID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("SMSCode", "SMSCODE");
            columnMap.Add("IsLock", "IS_LOCK");
            columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("SchoolProfileID", "SCHOOL_PROFILE_ID");
            columnMap.Add("IsDefault", "IS_DEFAULT");
            columnMap.Add("CreateUserID", "CREATE_USER_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Contract()
        { // TypeName="Models.Contract()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONTRACT()
            columnMap.Add("ContractID", "CONTRACT_ID");
            columnMap.Add("ContractTypeID", "CONTRACT_TYPE_ID");
            columnMap.Add("ContractDate", "CONTRACT_DATE");
            columnMap.Add("ExpiredDate", "EXPIRED_DATE");
            columnMap.Add("ContractCode", "CONTRACT_CODE");
            columnMap.Add("Employer", "EMPLOYER");
            columnMap.Add("Employee", "EMPLOYEE");
            columnMap.Add("PlaceOfWork", "PLACE_OF_WORK");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ContractType()
        { // TypeName="Models.ContractType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="CONTRACT_TYPE()
            columnMap.Add("ContractTypeID", "CONTRACT_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DailyDishCost()
        { // TypeName="Models.DailyDishCost()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_DISH_COST()
            columnMap.Add("DailyDishCostID", "DAILY_DISH_COST_ID");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EffectDate", "EFFECT_DATE");
            columnMap.Add("Cost", "COST");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DailyExpense()
        { // TypeName="Models.DailyExpense()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_EXPENSE()
            columnMap.Add("DailyExpenseID", "DAILY_EXPENSE_ID");
            columnMap.Add("ExpenseDate", "EXPENSE_DATE");
            columnMap.Add("Receipt", "RECEIPT");
            columnMap.Add("Expense", "EXPENSE");
            columnMap.Add("BalanceStart", "BALANCE_START");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("BalanceEnd", "BALANCE_END");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DailyFoodInspection()
        { // TypeName="Models.DailyFoodInspection()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_FOOD_INSPECTION()
            columnMap.Add("DailyFoodInspectionID", "DAILY_FOOD_INSPECTION_ID");
            columnMap.Add("Weight", "WEIGHT");
            columnMap.Add("PricePerOnce", "PRICE_PER_ONCE");
            columnMap.Add("DishInspectionID", "DISH_INSPECTION_ID");
            columnMap.Add("FoodGroupID", "FOOD_GROUP_ID");
            columnMap.Add("FoodID", "FOOD_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DailyMenu()
        { // TypeName="Models.DailyMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_MENU()
            columnMap.Add("DailyMenuID", "DAILY_MENU_ID");
            columnMap.Add("DailyMenuCode", "DAILY_MENU_CODE");
            columnMap.Add("NumberOfChildren", "NUMBER_OF_CHILDREN");
            columnMap.Add("MenuType", "MENU_TYPE");
            columnMap.Add("GetFromDishCat", "GET_FROM_DISH_CAT");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("DailyCostOfChildren", "DAILY_COST_OF_CHILDREN");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DailyMenuDetail()
        { // TypeName="Models.DailyMenuDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_MENU_DETAIL()
            columnMap.Add("DailyMenuDetailID", "DAILY_MENU_DETAIL_ID");
            columnMap.Add("DishName", "DISH_NAME");
            columnMap.Add("DishID", "DISH_ID");
            columnMap.Add("DailyMenuID", "DAILY_MENU_ID");
            columnMap.Add("MealID", "MEAL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DailyMenuFood()
        { // TypeName="Models.DailyMenuFood()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_MENU_FOOD()
            columnMap.Add("DailyMenuFoodID", "DAILY_MENU_FOOD_ID");
            columnMap.Add("Weight", "WEIGHT");
            columnMap.Add("PricePerOnce", "PRICE_PER_ONCE");
            columnMap.Add("DailyMenuID", "DAILY_MENU_ID");
            columnMap.Add("FoodID", "FOOD_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DailyOtherServiceInspection()
        { // TypeName="Models.DailyOtherServiceInspection()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DAILY_OTHER_SERVICE_INSPECTION()
            columnMap.Add("DailyOtherServiceInspectionID", "DAILY_OTHER_SERVICE_INS_ID");
            columnMap.Add("Price", "PRICE");
            columnMap.Add("DishInspectionID", "DISH_INSPECTION_ID");
            columnMap.Add("OtherServiceID", "OTHER_SERVICE_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DefaultGroup()
        { // TypeName="Models.DefaultGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DEFAULT_GROUP()
            columnMap.Add("DefaultGroupID", "DEFAULT_GROUP_ID");
            columnMap.Add("RoleID", "ROLE_ID");
            columnMap.Add("DefaultGroupName", "DEFAULT_GROUP_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> DefaultGroupMenu()
        { // TypeName="Models.DefaultGroupMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DEFAULT_GROUP_MENU()
            columnMap.Add("DefaultGroupMenuID", "DEFAULT_GROUP_MENU_ID");
            columnMap.Add("DefaultGroupID", "DEFAULT_GROUP_ID");
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("Permission", "PERMISSION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DentalTest()
        { // TypeName="Models.DentalTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DENTAL_TEST()
            columnMap.Add("DentalTestID", "DENTAL_TEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("NumTeethExtracted", "NUM_TEETH_EXTRACTED");
            columnMap.Add("DescriptionTeethExtracted", "DESCRIPTION_TEETH_EXTRACTED");
            columnMap.Add("NumDentalFilling", "NUM_DENTAL_FILLING");
            columnMap.Add("DescriptionDentalFilling", "DESCRIPTION_DENTAL_FILLING");
            columnMap.Add("NumPreventiveDentalFilling", "NUM_PREVENTIVE_DENTAL_FILLING");
            columnMap.Add("DescriptionPreventiveDentalFilling", "DESCRIPTION_PREVENTIVE_DEN_FIL");
            columnMap.Add("IsScraptTeeth", "IS_SCRAPT_TEETH");
            columnMap.Add("IsTreatment", "IS_TREATMENT");
            columnMap.Add("Other", "OTHER");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("NumDentalFillingOne", "NUM_DENTAL_FILLING_ONE");
            columnMap.Add("NumDentalFillingTwo", "NUM_DENTAL_FILLING_TWO");
            columnMap.Add("NumDentalFillingThree", "NUM_DENTAL_FILLING_THREE");
            columnMap.Add("NumDentalFillingFour", "NUM_DENTAL_FILLING_FOUR");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DetachableHeadBag()
        { // TypeName="Models.DetachableHeadBag()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DETACHABLE_HEAD_BAG()
            columnMap.Add("DetachableHeadBagID", "DETACHABLE_HEAD_BAG_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ExaminationSubjectID", "EXAMINATION_SUBJECT_ID");
            columnMap.Add("BagTitle", "BAG_TITLE");
            columnMap.Add("Prefix", "PREFIX");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DetachableHeadMapping()
        { // TypeName="Models.DetachableHeadMapping()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DETACHABLE_HEAD_MAPPING()
            columnMap.Add("DetachableHeadMappingID", "DETACHABLE_HEAD_MAPPING_ID");
            columnMap.Add("DetachableHeadBagID", "DETACHABLE_HEAD_BAG_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("NamedListCode", "NAMED_LIST_CODE");
            columnMap.Add("DetachableHeadNumber", "DETACHABLE_HEAD_NUMBER");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CandidateID", "CANDIDATE_ID");
            columnMap.Add("NamedListNumber", "NAMED_LIST_NUMBER");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DevelopmentOfChildren()
        { // TypeName="Models.DevelopmentOfChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DEVELOPMENT_OF_CHILDREN()
            columnMap.Add("DevelopmentOfChildrenID", "DEVELOPMENT_OF_CHILDREN_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EvaluationDevelopmentID", "EVALUATION_DEVELOPMENT_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DisabledType()
        { // TypeName="Models.DisabledType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISABLED_TYPE()
            columnMap.Add("DisabledTypeID", "DISABLED_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DisciplineType()
        { // TypeName="Models.DisciplineType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISCIPLINE_TYPE()
            columnMap.Add("DisciplineTypeID", "DISCIPLINE_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("ConductMinusMark", "CONDUCT_MINUS_MARK");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DiseasesType()
        { // TypeName="Models.DiseasesType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISEASES_TYPE()
            columnMap.Add("DiseasesTypeID", "DISEASES_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("IsActive", "IS_ACTIVE");
            return columnMap;

        }
        public IDictionary<string, object> DishCat()
        { // TypeName="Models.DishCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISH_CAT()
            columnMap.Add("DishCatID", "DISH_CAT_ID");
            columnMap.Add("DishName", "DISH_NAME");
            columnMap.Add("NumberOfServing", "NUMBER_OF_SERVING");
            columnMap.Add("Prepare", "PREPARE");
            columnMap.Add("Receipt", "RECEIPT");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("TypeOfDishID", "TYPE_OF_DISH_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DishDetail()
        { // TypeName="Models.DishDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISH_DETAIL()
            columnMap.Add("DishDetailID", "DISH_DETAIL_ID");
            columnMap.Add("WeightPerRation", "WEIGHT_PER_RATION");
            columnMap.Add("FoodID", "FOOD_ID");
            columnMap.Add("DishID", "DISH_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DishInspection()
        { // TypeName="Models.DishInspection()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISH_INSPECTION()
            columnMap.Add("DishInspectionID", "DISH_INSPECTION_ID");
            columnMap.Add("InspectedDate", "INSPECTED_DATE");
            columnMap.Add("MenuType", "MENU_TYPE");
            columnMap.Add("DailyCostOfChildren", "DAILY_COST_OF_CHILDREN");
            columnMap.Add("Expense", "EXPENSE");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("DailyMenuID", "DAILY_MENU_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("NumberOfChildren", "NUMBER_OF_CHILDREN");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> DishOfChildren()
        { // TypeName="Models.DishOfChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISH_OF_CHILDREN()
            columnMap.Add("DishOfChildrenID", "DISH_OF_CHILDREN_ID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> DistributeProgram()
        { // TypeName="Models.DistributeProgram()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISTRIBUTE_PROGRAM()
            columnMap.Add("DistributeProgramID", "DISTRIBUTE_PROGRAM_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolWeekID", "SCHOOL_WEEK_ID");
            columnMap.Add("DistributeProgramOrder", "DISTRIBUTE_PROGRAM_ORDER");
            columnMap.Add("LessonName", "LESSON_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("Last2DigitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> District()
        { // TypeName="Models.District()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISTRICT()
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictCode", "DISTRICT_CODE");
            columnMap.Add("DistrictName", "DISTRICT_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("DistrictExamCode", "DISTRICT_EXAM_CODE");
            columnMap.Add("ExamArea", "EXAM_AREA");
            return columnMap;

        }
        public IDictionary<string, object> DistrictBak()
        { // TypeName="Models.DistrictBak()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DISTRICT_BAK()
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictCode", "DISTRICT_CODE");
            columnMap.Add("DistrictName", "DISTRICT_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EatingGroup()
        { // TypeName="Models.EatingGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EATING_GROUP()
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("EatingGroupName", "EATING_GROUP_NAME");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("MonthFrom", "MONTH_FROM");
            columnMap.Add("MonthTo", "MONTH_TO");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EatingGroupClass()
        { // TypeName="Models.EatingGroupClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EATING_GROUP_CLASS()
            columnMap.Add("EatingGroupClassID", "EATING_GROUP_CLASS_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EducationLevel()
        { // TypeName="Models.EducationLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EDUCATION_LEVEL()
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("IsLastYear", "IS_LAST_YEAR");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Grade", "GRADE");
            return columnMap;

        }
        public IDictionary<string, object> EducationProgram()
        { // TypeName="Models.EducationProgram()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EDUCATION_PROGRAM()
            columnMap.Add("EducationProgramID", "EDUCATION_PROGRAM_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("TrainingTypeID", "TRAINING_TYPE_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SubjectCatID", "SUBJECT_CAT_ID");
            columnMap.Add("NumberOfLesson", "NUMBER_OF_LESSON");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> EducationalManagementGrade()
        { // TypeName="Models.EducationalManagementGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EDUCATIONAL_MANAGEMENT_GRADE()
            columnMap.Add("EducationalManagementGradeID", "EDUCATIONAL_MANAGEMENT_GRA_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> Employee()
        { // TypeName="Models.Employee()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE()
            //columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("EmployeeCode", "EMPLOYEE_CODE");
            columnMap.Add("EmployeeType", "EMPLOYEE_TYPE");
            columnMap.Add("EmploymentStatus", "EMPLOYMENT_STATUS");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("FullName", "FULL_NAME");
            columnMap.Add("BirthDate", "BIRTH_DATE");
            columnMap.Add("BirthPlace", "BIRTH_PLACE");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Mobile", "MOBILE");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("TempResidentalAddress", "TEMP_RESIDENTAL_ADDRESS");
            columnMap.Add("PermanentResidentalAddress", "PERMANENT_RESIDENTAL_ADDRESS");
            columnMap.Add("Genre", "GENRE");
            columnMap.Add("Alias", "ALIAS");
            columnMap.Add("MariageStatus", "MARIAGE_STATUS");
            columnMap.Add("ForeignLanguageQualification", "FOREIGN_LANGUAGE_QUALIFICATION");
            columnMap.Add("HealthStatus", "HEALTH_STATUS");
            columnMap.Add("JoinedDate", "JOINED_DATE");
            columnMap.Add("StartingDate", "STARTING_DATE");
            columnMap.Add("IdentityNumber", "IDENTITY_NUMBER");
            columnMap.Add("IdentityIssuedDate", "IDENTITY_ISSUED_DATE");
            columnMap.Add("IdentityIssuedPlace", "IDENTITY_ISSUED_PLACE");
            columnMap.Add("HomeTown", "HOME_TOWN");
            columnMap.Add("DedicatedForYoungLeague", "DEDICATED_FOR_YOUNG_LEAGUE");
            columnMap.Add("FamilyTypeID", "FAMILY_TYPE_ID");
            columnMap.Add("StaffPositionID", "STAFF_POSITION_ID");
            columnMap.Add("EthnicID", "ETHNIC_ID");
            columnMap.Add("ReligionID", "RELIGION_ID");
            columnMap.Add("GraduationLevelID", "GRADUATION_LEVEL_ID");
            columnMap.Add("ContractID", "CONTRACT_ID");
            columnMap.Add("QualificationTypeID", "QUALIFICATION_TYPE_ID");
            columnMap.Add("SpecialityCatID", "SPECIALITY_CAT_ID");
            columnMap.Add("QualificationLevelID", "QUALIFICATION_LEVEL_ID");
            columnMap.Add("ITQualificationLevelID", "ITQUALIFICATION_LEVEL_ID");
            columnMap.Add("ForeignLanguageGradeID", "FOREIGN_LANGUAGE_GRADE_ID");
            columnMap.Add("PoliticalGradeID", "POLITICAL_GRADE_ID");
            columnMap.Add("StateManagementGradeID", "STATE_MANAGEMENT_GRADE_ID");
            columnMap.Add("EducationalManagementGradeID", "EDUCATIONAL_MANAGEMENT_GRA_ID");
            columnMap.Add("WorkTypeID", "WORK_TYPE_ID");
            columnMap.Add("PrimarilyAssignedSubjectID", "PRIMARILY_ASSIGNED_SUBJECT_ID");
            columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("IsYouthLeageMember", "IS_YOUTH_LEAGE_MEMBER");
            columnMap.Add("YouthLeagueJoinedDate", "YOUTH_LEAGUE_JOINED_DATE");
            columnMap.Add("YouthLeagueJoinedPlace", "YOUTH_LEAGUE_JOINED_PLACE");
            columnMap.Add("IsCommunistPartyMember", "IS_COMMUNIST_PARTY_MEMBER");
            columnMap.Add("CommunistPartyJoinedDate", "COMMUNIST_PARTY_JOINED_DATE");
            columnMap.Add("CommunistPartyJoinedPlace", "COMMUNIST_PARTY_JOINED_PLACE");
            columnMap.Add("FatherFullName", "FATHER_FULL_NAME");
            columnMap.Add("FatherBirthDate", "FATHER_BIRTH_DATE");
            columnMap.Add("FatherJob", "FATHER_JOB");
            columnMap.Add("FatherWorkingPlace", "FATHER_WORKING_PLACE");
            columnMap.Add("MotherFullName", "MOTHER_FULL_NAME");
            columnMap.Add("MotherBirthDate", "MOTHER_BIRTH_DATE");
            columnMap.Add("MotherJob", "MOTHER_JOB");
            columnMap.Add("MotherWorkingPlace", "MOTHER_WORKING_PLACE");
            columnMap.Add("SpouseFullName", "SPOUSE_FULL_NAME");
            columnMap.Add("SpouseBirthDate", "SPOUSE_BIRTH_DATE");
            columnMap.Add("SpouseJob", "SPOUSE_JOB");
            columnMap.Add("SpouseWorkingPlace", "SPOUSE_WORKING_PLACE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("IntoSchoolDate", "INTO_SCHOOL_DATE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("ContractTypeID", "CONTRACT_TYPE_ID");
            columnMap.Add("RegularRefresher", "REGULAR_REFRESHER");
            columnMap.Add("TrainingLevelID", "TRAINING_LEVEL_ID");
            columnMap.Add("M_FamilyTypeName", "M_FAMILY_TYPE_NAME");
            columnMap.Add("M_OldEmployeeID", "M_OLD_EMPLOYEE_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("WorkGroupTypeID", "WORK_GROUP_TYPE_ID");
            columnMap.Add("Image", "IMAGE");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsSyndicate", "IS_SYNDICATE");
            columnMap.Add("SyndicateDate", "SYNDICATE_DATE");
            columnMap.Add("MotelRoomOutsite", "MOTEL_ROOM_OUTSIDE");
            columnMap.Add("TeacherDuties", "TEACHER_DUTIES");
            columnMap.Add("NeedTeacherDuties", "NEED_TEACHER_DUTIES");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeContact()
        { // TypeName="Models.EmployeeContact()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_CONTACT()
            columnMap.Add("EmployeeContactID", "EMPLOYEE_CONTACT_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("ContactGroupID", "CONTACT_GROUP_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeBreather()
        { // TypeName="Models.EmployeeBreather()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_BREATHER()
            columnMap.Add("EmployeeBreatherID", "EMPLOYEE_BREATHER_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMICYEAR_ID");
            columnMap.Add("DateBreather", "DATE_BREATHER");
            columnMap.Add("VacationReasonID", "VACATION_REASON_ID");
            columnMap.Add("DateStartWork", "DATE_START_WORK");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("IsComeBack", "IS_COME_BACK");
            return columnMap;

        }
        public IDictionary<string, object> EmisRequest()
        { // TypeName="Models.EmisRequest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMIS_REQUEST()
            columnMap.Add("EmisRequestID", "EMIS_REQUEST_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYear", "ACADEMIC_YEAR");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Grade", "GRADE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("Period", "PERIOD");
            columnMap.Add("TrainingTypeID", "TRAINING_TYPE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("ReportCode", "REPORT_CODE");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeHistoryStatus()
        { // TypeName="Models.EmployeeHistoryStatus()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_HISTORY_STATUS()
            columnMap.Add("EmployeeHistoryStatusID", "EMPLOYEE_HISTORY_STATUS_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("EmployeeStatus", "EMPLOYEE_STATUS");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeePraiseDiscipline()
        { // TypeName="Models.EmployeePraiseDiscipline()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_PRAISE_DISCIPLINE()
            columnMap.Add("EmployeePraiseDisciplineID", "EMPLOYEE_PRAISE_DISCIPLINE_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("IsDiscipline", "IS_DISCIPLINE");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("DetailContent", "DETAIL_CONTENT");
            columnMap.Add("Form", "FORM");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeQualification()
        { // TypeName="Models.EmployeeQualification()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_QUALIFICATION()
            columnMap.Add("EmployeeQualificationID", "EMPLOYEE_QUALIFICATION_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("SpecialityID", "SPECIALITY_ID");
            columnMap.Add("QualificationTypeID", "QUALIFICATION_TYPE_ID");
            columnMap.Add("QualificationGradeID", "QUALIFICATION_GRADE_ID");
            columnMap.Add("Result", "RESULT");
            columnMap.Add("QualifiedAt", "QUALIFIED_AT");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("QualifiedDate", "QUALIFIED_DATE");
            columnMap.Add("IsPrimaryQualification", "IS_PRIMARY_QUALIFICATION");
            columnMap.Add("IsRequalified", "IS_REQUALIFIED");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("TrainingLevelID", "TRAINING_LEVEL_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_QualificationGradeName", "M_QUALIFICATION_GRADE_NAME");
            columnMap.Add("M_QualificationTypeName", "M_QUALIFICATION_TYPE_NAME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MQualificationType", "M_QUALIFICATION_TYPE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeSalary()
        { // TypeName="Models.EmployeeSalary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_SALARY()
            columnMap.Add("EmployeeSalaryID", "EMPLOYEE_SALARY_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("EmployeeScaleID", "EMPLOYEE_SCALE_ID");
            columnMap.Add("SalaryLevelID", "SALARY_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("AppliedDate", "APPLIED_DATE");
            columnMap.Add("SalaryResolution", "SALARY_RESOLUTION");
            columnMap.Add("SalaryAmount", "SALARY_AMOUNT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeScale()
        { // TypeName="Models.EmployeeScale()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_SCALE()
            columnMap.Add("EmployeeScaleID", "EMPLOYEE_SCALE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("DurationInYear", "DURATION_IN_YEAR");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeWorkMovement()
        { // TypeName="Models.EmployeeWorkMovement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_WORK_MOVEMENT()
            columnMap.Add("EmployeeWorkMovementID", "EMPLOYEE_WORK_MOVEMENT_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("FromSchoolID", "FROM_SCHOOL_ID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("ResolutionDocument", "RESOLUTION_DOCUMENT");
            columnMap.Add("MovementType", "MOVEMENT_TYPE");
            columnMap.Add("ToSchoolID", "TO_SCHOOL_ID");
            columnMap.Add("ToSupervisingDeptID", "TO_SUPERVISING_DEPT_ID");
            columnMap.Add("ToProvinceID", "TO_PROVINCE_ID");
            columnMap.Add("ToDistrictID", "TO_DISTRICT_ID");
            columnMap.Add("MoveTo", "MOVE_TO");
            columnMap.Add("MovedDate", "MOVED_DATE");
            columnMap.Add("IsAccepted", "IS_ACCEPTED");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("OldAccountID", "OLD_ACCOUNT_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("ToCommuneID", "TO_COMMUNE_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeWorkType()
        { // TypeName="Models.EmployeeWorkType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_WORK_TYPE()
            columnMap.Add("EmployeeWorkTypeID", "EMPLOYEE_WORK_TYPE_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("WorkTypeID", "WORK_TYPE_ID");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> EmployeeWorkingHistory()
        { // TypeName="Models.EmployeeWorkingHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMPLOYEE_WORKING_HISTORY()
            columnMap.Add("EmployeeWorkingHistoryID", "EMPLOYEE_WORKING_HISTORY_ID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Organization", "ORGANIZATION");
            columnMap.Add("Department", "DEPARTMENT");
            columnMap.Add("Position", "POSITION");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EmulationCriteria()
        { // TypeName="Models.EmulationCriteria()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EMULATION_CRITERIA()
            columnMap.Add("EmulationCriteriaId", "EMULATION_CRITERIA_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EnergyDistribution()
        { // TypeName="Models.EnergyDistribution()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ENERGY_DISTRIBUTION()
            columnMap.Add("EnergyDistributionID", "ENERGY_DISTRIBUTION_ID");
            columnMap.Add("EnergyDemandFrom", "ENERGY_DEMAND_FROM");
            columnMap.Add("EnergyDemandTo", "ENERGY_DEMAND_TO");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("MealID", "MEAL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ENTTest()
        { // TypeName="Models.ENTTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ENTTEST()
            columnMap.Add("ENTTestID", "ENTTEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("RCapacityHearing", "RCAPACITY_HEARING");
            columnMap.Add("LCapacityHearing", "LCAPACITY_HEARING");
            columnMap.Add("IsDeaf", "IS_DEAF");
            columnMap.Add("IsSick", "IS_SICK");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Ethnic()
        { // TypeName="Models.Ethnic()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ETHNIC()
            columnMap.Add("EthnicID", "ETHNIC_ID");
            columnMap.Add("EthnicCode", "ETHNIC_CODE");
            columnMap.Add("EthnicName", "ETHNIC_NAME");
            columnMap.Add("IsMinority", "IS_MINORITY");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationConfig()
        { // TypeName="Models.EvaluationConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_CONFIG()
            columnMap.Add("EvaluationConfigID", "EVALUATION_CONFIG_ID");
            columnMap.Add("EvaluationConfigName", "EVALUATION_CONFIG_NAME");
            columnMap.Add("PositionOrder", "POSITION_ORDER");
            columnMap.Add("TypeConfigID", "TYPE_CONFIG_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationDevelopment()
        { // TypeName="Models.EvaluationDevelopment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_DEVELOPMENT()
            columnMap.Add("EvaluationDevelopmentID", "EVALUATION_DEVELOPMENT_ID");
            columnMap.Add("EvaluationDevelopmentCode", "EVALUATION_DEVELOPMENT_CODE");
            columnMap.Add("EvaluationDevelopmentName", "EVALUATION_DEVELOPMENT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("IsRequired", "IS_REQUIRED");
            columnMap.Add("InputType", "INPUT_TYPE");
            columnMap.Add("InputMin", "INPUT_MIN");
            columnMap.Add("InputMax", "INPUT_MAX");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("EvaluationDevelopmentGroupID", "EVALUATION_DEVELOPMENT_GRO_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationDevelopmentGroup()
        { // TypeName="Models.EvaluationDevelopmentGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_DEVELOPMENT_GROUP()
            columnMap.Add("EvaluationDevelopmentGroupID", "EVALUATION_DEVELOPMENT_GRO_ID");
            columnMap.Add("EvaluationDevelopmentGroupName", "EVALUATION_DEVELOPMENT_GRO_NAM");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamViolationType()
        { // TypeName="Models.ExamViolationType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_VIOLATION_TYPE()
            columnMap.Add("ExamViolationTypeID", "EXAM_VIOLATION_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AppliedForCandidate", "APPLIED_FOR_CANDIDATE");
            columnMap.Add("AppliedForInvigilator", "APPLIED_FOR_INVIGILATOR");
            columnMap.Add("PenalizedMark", "PENALIZED_MARK");
            columnMap.Add("Penalization", "PENALIZATION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Examination()
        { // TypeName="Models.Examination()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAMINATION()
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Location", "LOCATION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CurrentStage", "CURRENT_STAGE");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("UsingSeparateList", "USING_SEPARATE_LIST");
            columnMap.Add("CandidateFromMultipleLevel", "CANDIDATE_FROM_MULTIPLE_LEVEL");
            columnMap.Add("MarkImportType", "MARK_IMPORT_TYPE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExaminationRoom()
        { // TypeName="Models.ExaminationRoom()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAMINATION_ROOM()
            columnMap.Add("ExaminationRoomID", "EXAMINATION_ROOM_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("ExaminationSubjectID", "EXAMINATION_SUBJECT_ID");
            columnMap.Add("RoomTitle", "ROOM_TITLE");
            columnMap.Add("NumberOfSeat", "NUMBER_OF_SEAT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MAppliedLevel", "M_APPLIED_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExaminationSubject()
        { // TypeName="Models.ExaminationSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAMINATION_SUBJECT()
            columnMap.Add("ExaminationSubjectID", "EXAMINATION_SUBJECT_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("StartTime", "START_TIME");
            columnMap.Add("DurationInMinute", "DURATION_IN_MINUTE");
            columnMap.Add("OrderNumberPrefix", "ORDER_NUMBER_PREFIX");
            columnMap.Add("IsLocked", "IS_LOCKED");
            columnMap.Add("HasDetachableHead", "HAS_DETACHABLE_HEAD");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MAppliedLevel", "M_APPLIED_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExemptedObjectType()
        { // TypeName="Models.ExemptedObjectType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXEMPTED_OBJECT_TYPE()
            columnMap.Add("ExemptedObjectTypeID", "EXEMPTED_OBJECT_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ExemptedSubject()
        { // TypeName="Models.ExemptedSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXEMPTED_SUBJECT()
            columnMap.Add("ExemptedSubjectID", "EXEMPTED_SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("ExemptedObjectID", "EXEMPTED_OBJECT_ID");
            columnMap.Add("FirstSemesterExemptType", "FIRST_SEMESTER_EXEMPT_TYPE");
            columnMap.Add("SecondSemesterExemptType", "SECOND_SEMESTER_EXEMPT_TYPE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExperienceType()
        { // TypeName="Models.ExperienceType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXPERIENCE_TYPE()
            columnMap.Add("ExperienceTypeID", "EXPERIENCE_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EyeTest()
        { // TypeName="Models.EyeTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EYE_TEST()
            columnMap.Add("EyeTestID", "EYE_TEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("REye", "REYE");
            columnMap.Add("LEye", "LEYE");
            columnMap.Add("RMyopic", "RMYOPIC");
            columnMap.Add("LMyopic", "LMYOPIC");
            columnMap.Add("RFarsightedness", "RFARSIGHTEDNESS");
            columnMap.Add("LFarsightedness", "LFARSIGHTEDNESS");
            columnMap.Add("RAstigmatism", "RASTIGMATISM");
            columnMap.Add("LAstigmatism", "LASTIGMATISM");
            columnMap.Add("Other", "OTHER");
            columnMap.Add("IsTreatment", "IS_TREATMENT");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("IsMyopic", "IS_MYOPIC");
            columnMap.Add("IsFarsightedness", "IS_FARSIGHTEDNESS");
            columnMap.Add("IsAstigmatism", "IS_ASTIGMATISM");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> FamilyType()
        { // TypeName="Models.FamilyType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FAMILY_TYPE()
            columnMap.Add("FamilyTypeID", "FAMILY_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> FaultCriteria()
        { // TypeName="Models.FaultCriteria()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FAULT_CRITERIA()
            columnMap.Add("FaultCriteriaID", "FAULT_CRITERIA_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("GroupID", "GROUP_ID");
            columnMap.Add("PenalizedMark", "PENALIZED_MARK");
            columnMap.Add("Absence", "ABSENCE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> FaultGroup()
        { // TypeName="Models.FaultGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FAULT_GROUP()
            columnMap.Add("FaultGroupID", "FAULT_GROUP_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> FoodCat()
        { // TypeName="Models.FoodCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOOD_CAT()
            columnMap.Add("FoodID", "FOOD_ID");
            columnMap.Add("GroupType", "GROUP_TYPE");
            columnMap.Add("DiscardedRate", "DISCARDED_RATE");
            columnMap.Add("Price", "PRICE");
            columnMap.Add("Protein", "PROTEIN");
            columnMap.Add("Fat", "FAT");
            columnMap.Add("Sugar", "SUGAR");
            columnMap.Add("Calo", "CALO");
            columnMap.Add("FoodName", "FOOD_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("TypeOfFoodID", "TYPE_OF_FOOD_ID");
            columnMap.Add("FoodPackingID", "FOOD_PACKING_ID");
            columnMap.Add("FoodGroupID", "FOOD_GROUP_ID");
            columnMap.Add("CalculationUnit", "CALCULATION_UNIT");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> FoodGroup()
        { // TypeName="Models.FoodGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOOD_GROUP()
            columnMap.Add("FoodGroupID", "FOOD_GROUP_ID");
            columnMap.Add("FoodGroupName", "FOOD_GROUP_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> FoodHabitOfChildren()
        { // TypeName="Models.FoodHabitOfChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOOD_HABIT_OF_CHILDREN()
            columnMap.Add("FoodHabitOfChildrenID", "FOOD_HABIT_OF_CHILDREN_ID");
            columnMap.Add("HabitType", "HABIT_TYPE");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("TypeOfDishID", "TYPE_OF_DISH_ID");
            columnMap.Add("M_TypeOfDishID", "M_TYPE_OF_DISH_ID");
            columnMap.Add("DishOfChildrenID", "DISH_OF_CHILDREN_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> FoodMineral()
        { // TypeName="Models.FoodMineral()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOOD_MINERAL()
            columnMap.Add("FoodMineralID", "FOOD_MINERAL_ID");
            columnMap.Add("Weight", "WEIGHT");
            columnMap.Add("FoodID", "FOOD_ID");
            columnMap.Add("MinenalID", "MINENAL_ID");
            return columnMap;

        }
        public IDictionary<string, object> FoodPacking()
        { // TypeName="Models.FoodPacking()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOOD_PACKING()
            columnMap.Add("FoodPackingID", "FOOD_PACKING_ID");
            columnMap.Add("FoodPackingName", "FOOD_PACKING_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> ForeignLanguageGrade()
        { // TypeName="Models.ForeignLanguageGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="FOREIGN_LANGUAGE_GRADE()
            columnMap.Add("ForeignLanguageGradeID", "FOREIGN_LANGUAGE_GRADE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("EncouragePoint", "ENCOURAGE_POINT");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeachingSchedule()
        { // TypeName="Models.TeachingSchedule()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_SCHEDULE()
            columnMap.Add("TeachingScheduleID", "TEACHING_SCHEDULE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolWeekID", "SCHOOL_WEEK_ID");
            columnMap.Add("ScheduleDate", "SCHEDULE_DATE");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("SectionID", "SECTION_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("SubjectOrder", "SUBJECT_ORDER");
            columnMap.Add("TeachingScheduleOrder", "TEACHING_SCHEDULE_ORDER");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("TeachingUtensil", "TEACHING_UTENSIL");
            columnMap.Add("Quantity", "QUANTITY");
            columnMap.Add("InRoom", "IN_ROOM");
            columnMap.Add("IsSelfMade", "IS_SELF_MADE");
            columnMap.Add("Last2DigitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedTime", "CREATED_TIME");
            columnMap.Add("UpdatedTime", "UPDATED_TIME");
            return columnMap;

        }
        public IDictionary<string, object> SchoolWeek()
        { // TypeName="Models.SchoolWeek()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_WEEK()
            columnMap.Add("SchoolWeekID", "SCHOOL_WEEK_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("OrderWeek", "ORDER_WEEK");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedTime", "CREATED_TIME");
            columnMap.Add("UpdatedTime", "UPDATED_TIME");
            return columnMap;

        }
        public IDictionary<string, object> BookmarkedFunction()
        { // TypeName="Models.BookmarkedFunction()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="BOOKMARKED_FUNCTION()
            columnMap.Add("BookmarkedFunctionID", "BOOKMARKED_FUNCTION_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("LevelID", "LEVEL_ID");
            columnMap.Add("FunctionName", "FUNCTION_NAME");
            columnMap.Add("Order", "ORDER");
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");

            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("UserID", "USER_ID");
            return columnMap;

        }
        public IDictionary<string, object> GoodChildrenTicket()
        { // TypeName="Models.GoodChildrenTicket()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="GOOD_CHILDREN_TICKET()
            columnMap.Add("GoodChildrenTicketID", "GOOD_CHILDREN_TICKET_ID");
            columnMap.Add("IsGood", "IS_GOOD");
            columnMap.Add("WeekInMonth", "WEEK_IN_MONTH");
            columnMap.Add("Month", "MONTH");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> GraduationLevel()
        { // TypeName="Models.GraduationLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="GRADUATION_LEVEL()
            columnMap.Add("GraduationLevelID", "GRADUATION_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("FullfilmentStatus", "FULLFILMENT_STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> GroupCat()
        { // TypeName="Models.GroupCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="GROUP_CAT()
            columnMap.Add("GroupCatID", "GROUP_CAT_ID");
            columnMap.Add("RoleID", "ROLE_ID");
            columnMap.Add("CreatedUserID", "CREATED_USER_ID");
            columnMap.Add("GroupName", "GROUP_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("AdminUserID", "ADMIN_USER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_GroupID", "M_GROUP_ID");
            columnMap.Add("M_SchoolID", "M_SCHOOL_ID");
            columnMap.Add("M_SupervisingDeptID", "M_SUPERVISING_DEPT_ID");
            columnMap.Add("MCreatedUserID", "M_CREATED_USER_ID");
            columnMap.Add("MLevel", "M_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> GroupData()
        { // TypeName="Models.GroupData()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="GROUP_DATA()
            columnMap.Add("GroupDataID", "GROUP_DATA_ID");
            columnMap.Add("DataID", "DATA_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> GroupMenu()
        { // TypeName="Models.GroupMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="GROUP_MENU()
            columnMap.Add("GroupMenuID", "GROUP_MENU_ID");
            columnMap.Add("GroupID", "GROUP_ID");
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("Permission", "PERMISSION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MLevel", "M_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> HabitDetail()
        { // TypeName="Models.HabitDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HABIT_DETAIL()
            columnMap.Add("HabitDetailID", "HABIT_DETAIL_ID");
            columnMap.Add("HabitDetailName", "HABIT_DETAIL_NAME");
            columnMap.Add("HabitGroupID", "HABIT_GROUP_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> HabitGroup()
        { // TypeName="Models.HabitGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HABIT_GROUP()
            columnMap.Add("HabitGroupID", "HABIT_GROUP_ID");
            columnMap.Add("HabitGroupName", "HABIT_GROUP_NAME");
            columnMap.Add("PositionOrder", "POSITION_ORDER");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> HabitOfChildren()
        { // TypeName="Models.HabitOfChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HABIT_OF_CHILDREN()
            columnMap.Add("HabitOfChildrenID", "HABIT_OF_CHILDREN_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("HabitDetailID", "HABIT_DETAIL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> HeadTeacherSubstitution()
        { // TypeName="Models.HeadTeacherSubstitution()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HEAD_TEACHER_SUBSTITUTION()
            columnMap.Add("HeadTeacherSubstitutionID", "HEAD_TEACHER_SUBSTITUTION_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("HeadTeacherID", "HEAD_TEACHER_ID");
            columnMap.Add("SubstituedHeadTeacher", "SUBSTITUED_HEAD_TEACHER");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_OldExtraTeacherID", "M_OLD_EXTRA_TEACHER_ID");
            columnMap.Add("M_OldHeadTeacherID", "M_OLD_HEAD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> HealthCheckPeriod()
        { // TypeName="Models.HealthCheckPeriod()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HEALTH_CHECK_PERIOD()
            columnMap.Add("HealthCheckPeriodID", "HEALTH_CHECK_PERIOD_ID");
            columnMap.Add("HealthCheckPeriodName", "HEALTH_CHECK_PERIOD_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> HealthPeriod()
        { // TypeName="Models.HealthPeriod()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HEALTH_PERIOD()
            columnMap.Add("HealthPeriodID", "HEALTH_PERIOD_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> HistorySMS()
        { // TypeName="Models.HistorySMS()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HISTORY_SMS()
            columnMap.Add("HistorySMSID", "HISTORY_SMSID");
            columnMap.Add("Mobile", "MOBILE");
            columnMap.Add("FullName", "FULL_NAME");
            columnMap.Add("TypeID", "TYPE_ID");
            columnMap.Add("ReceiveType", "RECEIVE_TYPE");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Sender", "SENDER");
            columnMap.Add("FlagID", "FLAG_ID");
            columnMap.Add("ContentCount", "CONTENT_COUNT");
            columnMap.Add("CommandCode", "COMMAND_CODE");
            columnMap.Add("CPCode", "CPCODE");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ServiceID", "SERVICE_ID");
            columnMap.Add("ShortContent", "SHORT_CONTENT");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> HonourAchivement()
        { // TypeName="Models.HonourAchivement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HONOUR_ACHIVEMENT()
            columnMap.Add("HonourAchivementID", "HONOUR_ACHIVEMENT_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("HonourAchivementTypeID", "HONOUR_ACHIVEMENT_TYPE_ID");
            columnMap.Add("AchivedDate", "ACHIVED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> HonourAchivementType()
        { // TypeName="Models.HonourAchivementType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HONOUR_ACHIVEMENT_TYPE()
            columnMap.Add("HonourAchivementTypeID", "HONOUR_ACHIVEMENT_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> InfectiousDiseas()
        { // TypeName="Models.InfectiousDiseas()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="INFECTIOUS_DISEASES()
            columnMap.Add("InfectiousDiseasesID", "INFECTIOUS_DISEASES_ID");
            columnMap.Add("DiseasesTypeID", "DISEASES_TYPE_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("InfectionDate", "INFECTION_DATE");
            columnMap.Add("OffDate", "OFF_DATE");
            columnMap.Add("Address", "ADDRESS");
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("DiseasesName", "DISEASES_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("Other", "OTHER");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Invigilator()
        { // TypeName="Models.Invigilator()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="INVIGILATOR()
            columnMap.Add("InvigilatorID", "INVIGILATOR_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MLevel", "M_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> InvigilatorAssignment()
        { // TypeName="Models.InvigilatorAssignment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="INVIGILATOR_ASSIGNMENT()
            columnMap.Add("InvigilatorAssignmentID", "INVIGILATOR_ASSIGNMENT_ID");
            columnMap.Add("InvigilatorID", "INVIGILATOR_ID");
            columnMap.Add("RoomID", "ROOM_ID");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MLevel", "M_LEVEL");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ITQualificationLevel()
        { // TypeName="Models.ITQualificationLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ITQUALIFICATION_LEVEL()
            columnMap.Add("ITQualificationLevelID", "ITQUALIFICATION_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("EncouragePoint", "ENCOURAGE_POINT");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> JudgeRecord()
        { // TypeName="Models.JudgeRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="JUDGE_RECORD()
            columnMap.Add("JudgeRecordID", "JUDGE_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldJudgement", "OLD_JUDGEMENT");
            columnMap.Add("MJudgement", "M_JUDGEMENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> JudgeRecordHistory()
        { // TypeName="Models.JudgeRecordHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="JUDGE_RECORD_HISTORY()
            columnMap.Add("JudgeRecordID", "JUDGE_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldJudgement", "OLD_JUDGEMENT");
            columnMap.Add("MJudgement", "M_JUDGEMENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> LeavingReason()
        { // TypeName="Models.LeavingReason()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LEAVING_REASON()
            columnMap.Add("LeavingReasonID", "LEAVING_REASON_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> LevelHealthConfig()
        { // TypeName="Models.LevelHealthConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LEVEL_HEALTH_CONFIG()
            columnMap.Add("LevelHealthConfigID", "LEVEL_HEALTH_CONFIG_ID");
            columnMap.Add("EffectDate", "EFFECT_DATE");
            columnMap.Add("Genre", "GENRE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("TypeConfigID", "TYPE_CONFIG_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> LevelHealthConfigDetail()
        { // TypeName="Models.LevelHealthConfigDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LEVEL_HEALTH_CONFIG_DETAIL()
            columnMap.Add("LevelHealthConfigDetailID", "LEVEL_HEALTH_CONFIG_DETAIL_ID");
            columnMap.Add("Month", "MONTH");
            columnMap.Add("EvaluationValue", "EVALUATION_VALUE");
            columnMap.Add("EvaluationConfigID", "EVALUATION_CONFIG_ID");
            columnMap.Add("LevelHealthConfigID", "LEVEL_HEALTH_CONFIG_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Location()
        { // TypeName="Models.Location()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOCATION()
            columnMap.Add("LocationID", "LOCATION_ID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> LockMarkPrimary()
        { // TypeName="Models.LockMarkPrimary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOCK_MARK_PRIMARY()
            columnMap.Add("LockMarkPrimaryID", "LOCK_MARK_PRIMARY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("LockPrimary", "LOCK_PRIMARY");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> LockTraining()
        { // TypeName="Models.LockTraining()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOCK_TRAINING()
            columnMap.Add("LockTrainingID", "LOCK_TRAINING_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("IsViolation", "IS_VIOLATION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("TrainingType", "TRAINING_TYPE");
            return columnMap;

        }
        public IDictionary<string, object> LockedMarkDetail()
        { // TypeName="Models.LockedMarkDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOCKED_MARK_DETAIL()
            columnMap.Add("LockedMarkDetailID", "LOCKED_MARK_DETAIL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("MarkIndex", "MARK_INDEX");
            columnMap.Add("LockedDate", "LOCKED_DATE");
            columnMap.Add("UnlockedDate", "UNLOCKED_DATE");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MMarkType", "M_MARK_TYPE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> LogMO()
        { // TypeName="Models.LogMO()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOG_MO()
            columnMap.Add("LogMOID", "LOG_MOID");
            columnMap.Add("ServiceID", "SERVICE_ID");
            columnMap.Add("CPCode", "CPCODE");
            columnMap.Add("RequestID", "REQUEST_ID");
            columnMap.Add("Commandcode", "COMMANDCODE");
            columnMap.Add("SenderNumber", "SENDER_NUMBER");
            columnMap.Add("ReceiverNumber", "RECEIVER_NUMBER");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("ReceiveTime", "RECEIVE_TIME");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> MDataLog()
        { // TypeName="Models.MDataLog()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="M_DATALOG()
            columnMap.Add("DataLogID", "DATALOGID");
            columnMap.Add("TableName", "TABLENAME");
            columnMap.Add("Id", "ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("ModifiedDate", "MODIFIEDDATE");
            columnMap.Add("SyncStatus", "SYNCSTATUS");
            columnMap.Add("OldData", "OLDDATA");
            columnMap.Add("NewData", "NEWDATA");
            columnMap.Add("SyncDate", "SYNCDATE");
            columnMap.Add("PId", "PID");
            columnMap.Add("PartitionKey", "PARTITION_KEY");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            return columnMap;

        }
        public IDictionary<string, object> MIdmapping()
        { // TypeName="Models.MIdmapping()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="M_IDMAPPING()
            columnMap.Add("ID", "ID");
            columnMap.Add("NewID", "NEW_ID");
            columnMap.Add("OldID", "OLD_ID");
            columnMap.Add("OldTableName", "OLD_TABLE_NAME");
            columnMap.Add("NewTableName", "NEW_TABLE_NAME");
            columnMap.Add("Source", "SOURCE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("OldString", "OLD_STRING");
            columnMap.Add("OldGuid", "OLD_GUID");
            columnMap.Add("IsSMAS3", "IS_SMAS3");
            return columnMap;

        }
        public IDictionary<string, object> MMenu()
        { // TypeName="Models.MMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="M_MENU()
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("URL", "URL");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> MMenuMapping()
        { // TypeName="Models.MMenuMapping()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="M_MENU_MAPPING()
            columnMap.Add("ID", "ID");
            columnMap.Add("Url3", "URL3");
            columnMap.Add("Url2", "URL2");
            return columnMap;

        }
        public IDictionary<string, object> MSynchistory()
        { // TypeName="Models.MSynchistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="M_SYNCHISTORY()
            columnMap.Add("ID", "ID");
            columnMap.Add("Syncdate", "SYNCDATE");
            columnMap.Add("Description", "DESCRIPTION");
            return columnMap;

        }
        public IDictionary<string, object> MarkRecord()
        { // TypeName="Models.MarkRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MARK_RECORD()
            columnMap.Add("MarkRecordID", "MARK_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_IsByC", "M_IS_BY_C");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldMark", "OLD_MARK");
            columnMap.Add("M_MarkRecord", "M_MARK_RECORD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> MarkBookPrimaryConfig()
        { // TypeName="Models.MarkBookPrimaryConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MARK_BOOK_PRIMARY_CONFIG()
            columnMap.Add("ConfigID", "CONFIG_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevel1", "EDUCATION_LEVEL1");
            columnMap.Add("EducationLevel2", "EDUCATION_LEVEL2");
            columnMap.Add("EducationLevel3", "EDUCATION_LEVEL3");
            columnMap.Add("EducationLevel4", "EDUCATION_LEVEL4");
            columnMap.Add("EducationLevel5", "EDUCATION_LEVEL5");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> MarkRecordHistory()
        { // TypeName="Models.MarkRecordHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MARK_RECORD_HISTORY()
            columnMap.Add("MarkRecordID", "MARK_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_IsByC", "M_IS_BY_C");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldMark", "OLD_MARK");
            columnMap.Add("M_MarkRecord", "M_MARK_RECORD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> MarkStatistic()
        { // TypeName="Models.MarkStatistic()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MARK_STATISTIC()
            columnMap.Add("MarkStatisticID", "MARK_STATISTIC_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SuperVisingDeptID", "SUPER_VISING_DEPT_ID");
            columnMap.Add("Last2DigitNumberProvince", "LAST_2DIGIT_NUMBER_PROVINCE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("MarkType", "MARK_TYPE");
            columnMap.Add("CriteriaReportID", "CRITERIA_REPORT_ID");
            columnMap.Add("ProcessedDate", "PROCESSED_DATE");


            columnMap.Add("MarkLevel00", "MARK_LEVEL_00");
            columnMap.Add("MarkLevel01", "MARK_LEVEL_01");
            columnMap.Add("MarkLevel02", "MARK_LEVEL_02");
            columnMap.Add("MarkLevel03", "MARK_LEVEL_03");
            columnMap.Add("MarkLevel04", "MARK_LEVEL_04");
            columnMap.Add("MarkLevel05", "MARK_LEVEL_05");
            columnMap.Add("MarkLevel06", "MARK_LEVEL_06");
            columnMap.Add("MarkLevel07", "MARK_LEVEL_07");
            columnMap.Add("MarkLevel08", "MARK_LEVEL_08");
            columnMap.Add("MarkLevel09", "MARK_LEVEL_09");
            columnMap.Add("MarkLevel10", "MARK_LEVEL_10");
            columnMap.Add("MarkLevel11", "MARK_LEVEL_11");
            columnMap.Add("MarkLevel12", "MARK_LEVEL_12");
            columnMap.Add("MarkLevel13", "MARK_LEVEL_13");
            columnMap.Add("MarkLevel14", "MARK_LEVEL_14");
            columnMap.Add("MarkLevel15", "MARK_LEVEL_15");
            columnMap.Add("MarkLevel16", "MARK_LEVEL_16");
            columnMap.Add("MarkLevel17", "MARK_LEVEL_17");
            columnMap.Add("MarkLevel18", "MARK_LEVEL_18");
            columnMap.Add("MarkLevel19", "MARK_LEVEL_19");
            columnMap.Add("MarkLevel20", "MARK_LEVEL_20");
            columnMap.Add("MarkLevel21", "MARK_LEVEL_21");
            columnMap.Add("MarkLevel22", "MARK_LEVEL_22");
            columnMap.Add("MarkLevel23", "MARK_LEVEL_23");
            columnMap.Add("MarkLevel24", "MARK_LEVEL_24");
            columnMap.Add("MarkLevel25", "MARK_LEVEL_25");
            columnMap.Add("MarkLevel26", "MARK_LEVEL_26");
            columnMap.Add("MarkLevel27", "MARK_LEVEL_27");
            columnMap.Add("MarkLevel28", "MARK_LEVEL_28");
            columnMap.Add("MarkLevel29", "MARK_LEVEL_29");
            columnMap.Add("MarkLevel30", "MARK_LEVEL_30");
            columnMap.Add("MarkLevel31", "MARK_LEVEL_31");
            columnMap.Add("MarkLevel32", "MARK_LEVEL_32");
            columnMap.Add("MarkLevel33", "MARK_LEVEL_33");
            columnMap.Add("MarkLevel34", "MARK_LEVEL_34");
            columnMap.Add("MarkLevel35", "MARK_LEVEL_35");
            columnMap.Add("MarkLevel36", "MARK_LEVEL_36");
            columnMap.Add("MarkLevel37", "MARK_LEVEL_37");
            columnMap.Add("MarkLevel38", "MARK_LEVEL_38");
            columnMap.Add("MarkLevel39", "MARK_LEVEL_39");
            columnMap.Add("MarkLevel40", "MARK_LEVEL_40");
            columnMap.Add("MarkTotal", "MARK_TOTAL");
            columnMap.Add("BelowAverage", "BELOW_AVERAGE");
            columnMap.Add("OnAverage", "ON_AVERAGE");
            columnMap.Add("PupilTotal", "PUPIL_TOTAL");
            return columnMap;

        }
        public IDictionary<string, object> MarkType()
        { // TypeName="Models.MarkType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MARK_TYPE()
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Coefficient", "COEFFICIENT");
            columnMap.Add("NumberPerSemester", "NUMBER_PER_SEMESTER");
            columnMap.Add("MustBeInteger", "MUST_BE_INTEGER");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> MealCat()
        { // TypeName="Models.MealCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MEAL_CAT()
            columnMap.Add("MealCatID", "MEAL_CAT_ID");
            columnMap.Add("MealName", "MEAL_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Menu()
        { // TypeName="Models.Menu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MENU()
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("MenuName", "MENU_NAME");
            columnMap.Add("URL", "URL");
            columnMap.Add("ParentID", "PARENT_ID");
            columnMap.Add("CreatedUserID", "CREATED_USER_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MenuPath", "MENU_PATH");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("AdminOnly", "ADMIN_ONLY");
            columnMap.Add("IsVisible", "IS_VISIBLE");
            columnMap.Add("IsCategory", "IS_CATEGORY");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("BlockMenu", "BLOCK_MENU");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_MenuID", "M_MENU_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("ImageURL", "IMAGE_URL");
            columnMap.Add("ProductVersion", "PRODUCT_VERSION");
            return columnMap;

        }
        public IDictionary<string, object> MenuBk()
        { // TypeName="Models.MenuBk()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MENU_BK()
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("MenuName", "MENU_NAME");
            columnMap.Add("URL", "URL");
            columnMap.Add("ParentID", "PARENT_ID");
            columnMap.Add("CreatedUserID", "CREATED_USER_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MenuPath", "MENU_PATH");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("AdminOnly", "ADMIN_ONLY");
            columnMap.Add("IsVisible", "IS_VISIBLE");
            columnMap.Add("IsCategory", "IS_CATEGORY");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("BlockMenu", "BLOCK_MENU");
            columnMap.Add("ImageUrl1", "IMAGE_URL_1");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_MenuID", "M_MENU_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("ImageURL", "IMAGE_URL");
            return columnMap;

        }
        public IDictionary<string, object> MenuPermission()
        { // TypeName="Models.MenuPermission()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MENU_PERMISSION()
            columnMap.Add("MenuPermissionID", "MENU_PERMISSION_ID");
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("Permission", "PERMISSION");
            columnMap.Add("URL", "URL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Message()
        { // TypeName="Models.Message()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MESSAGE()
            columnMap.Add("MessageID", "MESSAGE_ID");
            columnMap.Add("UserNameSend", "USER_NAME_SEND");
            columnMap.Add("UserNameReceive", "USER_NAME_RECEIVE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("SendDate", "SEND_DATE");
            columnMap.Add("EmailReceive", "EMAIL_RECEIVE");
            columnMap.Add("IsRead", "IS_READ");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> MinenalCat()
        { // TypeName="Models.MinenalCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MINENAL_CAT()
            columnMap.Add("MinenalCatID", "MINENAL_CAT_ID");
            columnMap.Add("MinenalName", "MINENAL_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> MOError()
        { // TypeName="Models.MOError()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MOERROR()
            columnMap.Add("MOErrorID", "MOERROR_ID");
            columnMap.Add("LogMOID", "LOG_MOID");
            columnMap.Add("ErrorCode", "ERROR_CODE");
            columnMap.Add("ErrorMessage", "ERROR_MESSAGE");
            columnMap.Add("ReplyContent", "REPLY_CONTENT");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> MonitoringApp()
        { // TypeName="Models.MonitoringApp()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONITORING_APP()
            columnMap.Add("AppID", "APP_ID");
            columnMap.Add("ErrorCodeID", "ERROR_CODE_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("Runner", "RUNNER");
            return columnMap;

        }
        public IDictionary<string, object> MonitoringBook()
        { // TypeName="Models.MonitoringBook()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONITORING_BOOK()
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("MonitoringDate", "MONITORING_DATE");
            columnMap.Add("Symptoms", "SYMPTOMS");
            columnMap.Add("Solution", "SOLUTION");
            columnMap.Add("UnitName", "UNIT_NAME");
            columnMap.Add("MonitoringType", "MONITORING_TYPE");
            columnMap.Add("HealthPeriodID", "HEALTH_PERIOD_ID");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> MonitoringErrorCode()
        { // TypeName="Models.MonitoringErrorCode()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONITORING_ERROR_CODE()
            columnMap.Add("MonitoringErrorCode1", "MONITORING_ERROR_CODE");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> MonthlyBalance()
        { // TypeName="Models.MonthlyBalance()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONTHLY_BALANCE()
            columnMap.Add("MonthlyBalanceID", "MONTHLY_BALANCE_ID");
            columnMap.Add("BalanceMonth", "BALANCE_MONTH");
            columnMap.Add("Balance", "BALANCE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> MonthlyLock()
        { // TypeName="Models.MonthlyLock()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONTHLY_LOCK()
            columnMap.Add("MonthlyLockID", "MONTHLY_LOCK_ID");
            columnMap.Add("LockedMonth", "LOCKED_MONTH");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            return columnMap;

        }
        public IDictionary<string, object> MOQueue()
        { // TypeName="Models.MOQueue()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MOQUEUE()
            columnMap.Add("MOQueueID", "MOQUEUE_ID");
            columnMap.Add("LogMOID", "LOG_MOID");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> MovementAcceptance()
        { // TypeName="Models.MovementAcceptance()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MOVEMENT_ACCEPTANCE()
            columnMap.Add("MovementAcceptanceID", "MOVEMENT_ACCEPTANCE_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("MovedFromClassID", "MOVED_FROM_CLASS_ID");
            columnMap.Add("MovedFromSchoolID", "MOVED_FROM_SCHOOL_ID");
            columnMap.Add("MovedFromClassName", "MOVED_FROM_CLASS_NAME");
            columnMap.Add("MovedFromSchoolName", "MOVED_FROM_SCHOOL_NAME");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("MovedDate", "MOVED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> MT()
        { // TypeName="Models.MT()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MT()
            columnMap.Add("MT_ID", "MT_ID");
            columnMap.Add("HistorySMSID", "HISTORY_SMSID");
            columnMap.Add("ReceiverNumber", "RECEIVER_NUMBER");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("RetryCNT", "RETRY_CNT");
            columnMap.Add("SendTime", "SEND_TIME");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> NotEatingChildren()
        { // TypeName="Models.NotEatingChildren()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="NOT_EATING_CHILDREN()
            columnMap.Add("NotEatingChildrenID", "NOT_EATING_CHILDREN_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("NotEatingDate", "NOT_EATING_DATE");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> NutritionalNorm()
        { // TypeName="Models.NutritionalNorm()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="NUTRITIONAL_NORM()
            columnMap.Add("NutritionalNormID", "NUTRITIONAL_NORM_ID");
            columnMap.Add("EffectDate", "EFFECT_DATE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("DailyDemandFrom", "DAILY_DEMAND_FROM");
            columnMap.Add("DailyDemandTo", "DAILY_DEMAND_TO");
            columnMap.Add("RateFrom", "RATE_FROM");
            columnMap.Add("RateTo", "RATE_TO");
            columnMap.Add("ProteinFrom", "PROTEIN_FROM");
            columnMap.Add("ProteinTo", "PROTEIN_TO");
            columnMap.Add("AnimalProteinFrom", "ANIMAL_PROTEIN_FROM");
            columnMap.Add("AnimalProteinTo", "ANIMAL_PROTEIN_TO");
            columnMap.Add("PlantProteinFrom", "PLANT_PROTEIN_FROM");
            columnMap.Add("PlantProteinTo", "PLANT_PROTEIN_TO");
            columnMap.Add("FatFrom", "FAT_FROM");
            columnMap.Add("FatTo", "FAT_TO");
            columnMap.Add("AnimalFatFrom", "ANIMAL_FAT_FROM");
            columnMap.Add("AnimalFatTo", "ANIMAL_FAT_TO");
            columnMap.Add("PlantFatFrom", "PLANT_FAT_FROM");
            columnMap.Add("PlantFatTo", "PLANT_FAT_TO");
            columnMap.Add("SugarFrom", "SUGAR_FROM");
            columnMap.Add("SugarTo", "SUGAR_TO");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> NutritionalNormMineral()
        { // TypeName="Models.NutritionalNormMineral()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="NUTRITIONAL_NORM_MINERAL()
            columnMap.Add("NutritionalNormMineralID", "NUTRITIONAL_NORM_MINERAL_ID");
            columnMap.Add("ValueTo", "VALUE_TO");
            columnMap.Add("ValueFrom", "VALUE_FROM");
            columnMap.Add("MinenalID", "MINENAL_ID");
            columnMap.Add("NutritionalNormID", "NUTRITIONAL_NORM_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Ofbookmark()
        { // TypeName="Models.Ofbookmark()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFBOOKMARK()
            columnMap.Add("Bookmarkid", "BOOKMARKID");
            columnMap.Add("Bookmarktype", "BOOKMARKTYPE");
            columnMap.Add("Bookmarkname", "BOOKMARKNAME");
            columnMap.Add("Bookmarkvalue", "BOOKMARKVALUE");
            columnMap.Add("Isglobal", "ISGLOBAL");
            return columnMap;

        }
        public IDictionary<string, object> Ofbookmarkperm()
        { // TypeName="Models.Ofbookmarkperm()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFBOOKMARKPERM()
            columnMap.Add("Bookmarkid", "BOOKMARKID");
            columnMap.Add("Bookmarktype", "BOOKMARKTYPE");
            columnMap.Add("Name", "NAME");
            return columnMap;

        }
        public IDictionary<string, object> Ofbookmarkprop()
        { // TypeName="Models.Ofbookmarkprop()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFBOOKMARKPROP()
            columnMap.Add("Bookmarkid", "BOOKMARKID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofconparticipant()
        { // TypeName="Models.Ofconparticipant()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFCONPARTICIPANT()
            columnMap.Add("Conversationid", "CONVERSATIONID");
            columnMap.Add("Joineddate", "JOINEDDATE");
            columnMap.Add("Leftdate", "LEFTDATE");
            columnMap.Add("Barejid", "BAREJID");
            columnMap.Add("Jidresource", "JIDRESOURCE");
            columnMap.Add("Nickname", "NICKNAME");
            return columnMap;

        }
        public IDictionary<string, object> Ofconversation()
        { // TypeName="Models.Ofconversation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFCONVERSATION()
            columnMap.Add("Conversationid", "CONVERSATIONID");
            columnMap.Add("Room", "ROOM");
            columnMap.Add("Isexternal", "ISEXTERNAL");
            columnMap.Add("Startdate", "STARTDATE");
            columnMap.Add("Lastactivity", "LASTACTIVITY");
            columnMap.Add("Messagecount", "MESSAGECOUNT");
            return columnMap;

        }
        public IDictionary<string, object> Ofextcomponentconf()
        { // TypeName="Models.Ofextcomponentconf()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFEXTCOMPONENTCONF()
            columnMap.Add("Subdomain", "SUBDOMAIN");
            columnMap.Add("Wildcard", "WILDCARD");
            columnMap.Add("Secret", "SECRET");
            columnMap.Add("Permission", "PERMISSION");
            return columnMap;

        }
        public IDictionary<string, object> Ofgatewayavatars()
        { // TypeName="Models.Ofgatewayavatars()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGATEWAYAVATARS()
            columnMap.Add("Jid", "JID");
            columnMap.Add("Imagedata", "IMAGEDATA");
            columnMap.Add("Xmpphash", "XMPPHASH");
            columnMap.Add("Legacyidentifier", "LEGACYIDENTIFIER");
            columnMap.Add("CreateDate", "CREATEDATE");
            columnMap.Add("Lastupdate", "LASTUPDATE");
            columnMap.Add("Imagetype", "IMAGETYPE");
            return columnMap;

        }
        public IDictionary<string, object> Ofgatewaypseudoroster()
        { // TypeName="Models.Ofgatewaypseudoroster()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGATEWAYPSEUDOROSTER()
            columnMap.Add("Registrationid", "REGISTRATIONID");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Nickname", "NICKNAME");
            columnMap.Add("Groups", "GROUPS");
            return columnMap;

        }
        public IDictionary<string, object> Ofgatewayregistration()
        { // TypeName="Models.Ofgatewayregistration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGATEWAYREGISTRATION()
            columnMap.Add("Registrationid", "REGISTRATIONID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Transporttype", "TRANSPORTTYPE");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Password", "PASSWORD");
            columnMap.Add("Nickname", "NICKNAME");
            columnMap.Add("Registrationdate", "REGISTRATIONDATE");
            columnMap.Add("Lastlogin", "LASTLOGIN");
            return columnMap;

        }
        public IDictionary<string, object> Ofgatewayrestrictions()
        { // TypeName="Models.Ofgatewayrestrictions()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGATEWAYRESTRICTIONS()
            columnMap.Add("Transporttype", "TRANSPORTTYPE");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Groupname", "GROUPNAME");
            return columnMap;

        }
        public IDictionary<string, object> Ofgatewayvcards()
        { // TypeName="Models.Ofgatewayvcards()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGATEWAYVCARDS()
            columnMap.Add("Jid", "JID");
            columnMap.Add("Value", "VALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofgroup()
        { // TypeName="Models.Ofgroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGROUP()
            columnMap.Add("Groupname", "GROUPNAME");
            columnMap.Add("Description", "DESCRIPTION");
            return columnMap;

        }
        public IDictionary<string, object> Ofgroupprop()
        { // TypeName="Models.Ofgroupprop()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGROUPPROP()
            columnMap.Add("Groupname", "GROUPNAME");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofgroupuser()
        { // TypeName="Models.Ofgroupuser()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFGROUPUSER()
            columnMap.Add("Groupname", "GROUPNAME");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Administrator", "ADMINISTRATOR");
            return columnMap;

        }
        public IDictionary<string, object> Ofid()
        { // TypeName="Models.Ofid()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFID()
            columnMap.Add("Idtype", "IDTYPE");
            columnMap.Add("ID", "ID");
            return columnMap;

        }
        public IDictionary<string, object> Ofmessagearchive()
        { // TypeName="Models.Ofmessagearchive()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMESSAGEARCHIVE()
            columnMap.Add("Conversationid", "CONVERSATIONID");
            columnMap.Add("Fromjid", "FROMJID");
            columnMap.Add("Fromjidresource", "FROMJIDRESOURCE");
            columnMap.Add("Tojid", "TOJID");
            columnMap.Add("Tojidresource", "TOJIDRESOURCE");
            columnMap.Add("Sentdate", "SENTDATE");
            columnMap.Add("Body", "BODY");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucaffiliation()
        { // TypeName="Models.Ofmucaffiliation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCAFFILIATION()
            columnMap.Add("Roomid", "ROOMID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Affiliation", "AFFILIATION");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucconversationlog()
        { // TypeName="Models.Ofmucconversationlog()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCCONVERSATIONLOG()
            columnMap.Add("Roomid", "ROOMID");
            columnMap.Add("Sender", "SENDER");
            columnMap.Add("Nickname", "NICKNAME");
            columnMap.Add("Logtime", "LOGTIME");
            columnMap.Add("Subject", "SUBJECT");
            columnMap.Add("Body", "BODY");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucmember()
        { // TypeName="Models.Ofmucmember()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCMEMBER()
            columnMap.Add("Roomid", "ROOMID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Nickname", "NICKNAME");
            columnMap.Add("Firstname", "FIRSTNAME");
            columnMap.Add("Lastname", "LASTNAME");
            columnMap.Add("URL", "URL");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("Faqentry", "FAQENTRY");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucroom()
        { // TypeName="Models.Ofmucroom()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCROOM()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Roomid", "ROOMID");
            columnMap.Add("Creationdate", "CREATIONDATE");
            columnMap.Add("Modificationdate", "MODIFICATIONDATE");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Naturalname", "NATURALNAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("Lockeddate", "LOCKEDDATE");
            columnMap.Add("Emptydate", "EMPTYDATE");
            columnMap.Add("Canchangesubject", "CANCHANGESUBJECT");
            columnMap.Add("Maxusers", "MAXUSERS");
            columnMap.Add("Publicroom", "PUBLICROOM");
            columnMap.Add("Moderated", "MODERATED");
            columnMap.Add("Membersonly", "MEMBERSONLY");
            columnMap.Add("Caninvite", "CANINVITE");
            columnMap.Add("Roompassword", "ROOMPASSWORD");
            columnMap.Add("Candiscoverjid", "CANDISCOVERJID");
            columnMap.Add("Logenabled", "LOGENABLED");
            columnMap.Add("Subject", "SUBJECT");
            columnMap.Add("Rolestobroadcast", "ROLESTOBROADCAST");
            columnMap.Add("Usereservednick", "USERESERVEDNICK");
            columnMap.Add("Canchangenick", "CANCHANGENICK");
            columnMap.Add("Canregister", "CANREGISTER");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucroomprop()
        { // TypeName="Models.Ofmucroomprop()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCROOMPROP()
            columnMap.Add("Roomid", "ROOMID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucservice()
        { // TypeName="Models.Ofmucservice()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCSERVICE()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Subdomain", "SUBDOMAIN");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("Ishidden", "ISHIDDEN");
            return columnMap;

        }
        public IDictionary<string, object> Ofmucserviceprop()
        { // TypeName="Models.Ofmucserviceprop()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFMUCSERVICEPROP()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofoffline()
        { // TypeName="Models.Ofoffline()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFOFFLINE()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Messageid", "MESSAGEID");
            columnMap.Add("Creationdate", "CREATIONDATE");
            columnMap.Add("Messagesize", "MESSAGESIZE");
            columnMap.Add("Stanza", "STANZA");
            return columnMap;

        }
        public IDictionary<string, object> Ofpresence()
        { // TypeName="Models.Ofpresence()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPRESENCE()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Offlinepresence", "OFFLINEPRESENCE");
            columnMap.Add("Offlinedate", "OFFLINEDATE");
            return columnMap;

        }
        public IDictionary<string, object> Ofprivacylist()
        { // TypeName="Models.Ofprivacylist()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPRIVACYLIST()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Isdefault", "ISDEFAULT");
            columnMap.Add("List", "LIST");
            return columnMap;

        }
        public IDictionary<string, object> Ofprivate()
        { // TypeName="Models.Ofprivate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPRIVATE()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Namespace", "NAMESPACE");
            columnMap.Add("Privatedata", "PRIVATEDATA");
            return columnMap;

        }
        public IDictionary<string, object> Ofproperty()
        { // TypeName="Models.Ofproperty()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPROPERTY()
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubaffiliation()
        { // TypeName="Models.Ofpubsubaffiliation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBAFFILIATION()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Affiliation", "AFFILIATION");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubdefaultconf()
        { // TypeName="Models.Ofpubsubdefaultconf()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBDEFAULTCONF()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Leaf", "LEAF");
            columnMap.Add("Deliverpayloads", "DELIVERPAYLOADS");
            columnMap.Add("Maxpayloadsize", "MAXPAYLOADSIZE");
            columnMap.Add("Persistitems", "PERSISTITEMS");
            columnMap.Add("Maxitems", "MAXITEMS");
            columnMap.Add("Notifyconfigchanges", "NOTIFYCONFIGCHANGES");
            columnMap.Add("Notifydelete", "NOTIFYDELETE");
            columnMap.Add("Notifyretract", "NOTIFYRETRACT");
            columnMap.Add("Presencebased", "PRESENCEBASED");
            columnMap.Add("Senditemsubscribe", "SENDITEMSUBSCRIBE");
            columnMap.Add("Publishermodel", "PUBLISHERMODEL");
            columnMap.Add("Subscriptionenabled", "SUBSCRIPTIONENABLED");
            columnMap.Add("Accessmodel", "ACCESSMODEL");
            columnMap.Add("Language", "LANGUAGE");
            columnMap.Add("Replypolicy", "REPLYPOLICY");
            columnMap.Add("Associationpolicy", "ASSOCIATIONPOLICY");
            columnMap.Add("Maxleafnodes", "MAXLEAFNODES");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubitem()
        { // TypeName="Models.Ofpubsubitem()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBITEM()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("ID", "ID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Creationdate", "CREATIONDATE");
            columnMap.Add("Payload", "PAYLOAD");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubnode()
        { // TypeName="Models.Ofpubsubnode()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBNODE()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("Leaf", "LEAF");
            columnMap.Add("Creationdate", "CREATIONDATE");
            columnMap.Add("Modificationdate", "MODIFICATIONDATE");
            columnMap.Add("Parent", "PARENT");
            columnMap.Add("Deliverpayloads", "DELIVERPAYLOADS");
            columnMap.Add("Maxpayloadsize", "MAXPAYLOADSIZE");
            columnMap.Add("Persistitems", "PERSISTITEMS");
            columnMap.Add("Maxitems", "MAXITEMS");
            columnMap.Add("Notifyconfigchanges", "NOTIFYCONFIGCHANGES");
            columnMap.Add("Notifydelete", "NOTIFYDELETE");
            columnMap.Add("Notifyretract", "NOTIFYRETRACT");
            columnMap.Add("Presencebased", "PRESENCEBASED");
            columnMap.Add("Senditemsubscribe", "SENDITEMSUBSCRIBE");
            columnMap.Add("Publishermodel", "PUBLISHERMODEL");
            columnMap.Add("Subscriptionenabled", "SUBSCRIPTIONENABLED");
            columnMap.Add("Configsubscription", "CONFIGSUBSCRIPTION");
            columnMap.Add("Accessmodel", "ACCESSMODEL");
            columnMap.Add("Payloadtype", "PAYLOADTYPE");
            columnMap.Add("Bodyxslt", "BODYXSLT");
            columnMap.Add("Dataformxslt", "DATAFORMXSLT");
            columnMap.Add("Creator", "CREATOR");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("Language", "LANGUAGE");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Replypolicy", "REPLYPOLICY");
            columnMap.Add("Associationpolicy", "ASSOCIATIONPOLICY");
            columnMap.Add("Maxleafnodes", "MAXLEAFNODES");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubnodegroups()
        { // TypeName="Models.Ofpubsubnodegroups()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBNODEGROUPS()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("Rostergroup", "ROSTERGROUP");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubnodejids()
        { // TypeName="Models.Ofpubsubnodejids()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBNODEJIDS()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Associationtype", "ASSOCIATIONTYPE");
            return columnMap;

        }
        public IDictionary<string, object> Ofpubsubsubscription()
        { // TypeName="Models.Ofpubsubsubscription()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFPUBSUBSUBSCRIPTION()
            columnMap.Add("Serviceid", "SERVICEID");
            columnMap.Add("Nodeid", "NODEID");
            columnMap.Add("ID", "ID");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Owner", "OWNER");
            columnMap.Add("State", "STATE");
            columnMap.Add("Deliver", "DELIVER");
            columnMap.Add("Digest", "DIGEST");
            columnMap.Add("DigestFrequency", "DIGEST_FREQUENCY");
            columnMap.Add("Expire", "EXPIRE");
            columnMap.Add("Includebody", "INCLUDEBODY");
            columnMap.Add("Showvalues", "SHOWVALUES");
            columnMap.Add("Subscriptiontype", "SUBSCRIPTIONTYPE");
            columnMap.Add("Subscriptiondepth", "SUBSCRIPTIONDEPTH");
            columnMap.Add("Keyword", "KEYWORD");
            return columnMap;

        }
        public IDictionary<string, object> Ofremoteserverconf()
        { // TypeName="Models.Ofremoteserverconf()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFREMOTESERVERCONF()
            columnMap.Add("Xmppdomain", "XMPPDOMAIN");
            columnMap.Add("Remoteport", "REMOTEPORT");
            columnMap.Add("Permission", "PERMISSION");
            return columnMap;

        }
        public IDictionary<string, object> Ofroster()
        { // TypeName="Models.Ofroster()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFROSTER()
            columnMap.Add("Rosterid", "ROSTERID");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Jid", "JID");
            columnMap.Add("Sub", "SUB");
            columnMap.Add("Ask", "ASK");
            columnMap.Add("Recv", "RECV");
            columnMap.Add("Nick", "NICK");
            return columnMap;

        }
        public IDictionary<string, object> Ofrostergroups()
        { // TypeName="Models.Ofrostergroups()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFROSTERGROUPS()
            columnMap.Add("Rosterid", "ROSTERID");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("Groupname", "GROUPNAME");
            return columnMap;

        }
        public IDictionary<string, object> Ofrrds()
        { // TypeName="Models.Ofrrds()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFRRDS()
            columnMap.Add("ID", "ID");
            columnMap.Add("Updateddate", "UPDATEDDATE");
            columnMap.Add("Bytes", "BYTES");
            return columnMap;

        }
        public IDictionary<string, object> Ofsaslauthorized()
        { // TypeName="Models.Ofsaslauthorized()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFSASLAUTHORIZED()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Principal", "PRINCIPAL");
            return columnMap;

        }
        public IDictionary<string, object> Ofsecurityauditlog()
        { // TypeName="Models.Ofsecurityauditlog()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFSECURITYAUDITLOG()
            columnMap.Add("Msgid", "MSGID");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Entrystamp", "ENTRYSTAMP");
            columnMap.Add("Summary", "SUMMARY");
            columnMap.Add("Node", "NODE");
            columnMap.Add("Details", "DETAILS");
            return columnMap;

        }
        public IDictionary<string, object> Ofsipuser()
        { // TypeName="Models.Ofsipuser()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFSIPUSER()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Sipusername", "SIPUSERNAME");
            columnMap.Add("Sipauthuser", "SIPAUTHUSER");
            columnMap.Add("Sipdisplayname", "SIPDISPLAYNAME");
            columnMap.Add("Sippassword", "SIPPASSWORD");
            columnMap.Add("Sipserver", "SIPSERVER");
            columnMap.Add("Stunserver", "STUNSERVER");
            columnMap.Add("Stunport", "STUNPORT");
            columnMap.Add("Usestun", "USESTUN");
            columnMap.Add("Voicemail", "VOICEMAIL");
            columnMap.Add("Enabled", "ENABLED");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Outboundproxy", "OUTBOUNDPROXY");
            columnMap.Add("Promptcredentials", "PROMPTCREDENTIALS");
            return columnMap;

        }
        public IDictionary<string, object> Ofuser()
        { // TypeName="Models.Ofuser()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFUSER()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Plainpassword", "PLAINPASSWORD");
            columnMap.Add("Encryptedpassword", "ENCRYPTEDPASSWORD");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("Creationdate", "CREATIONDATE");
            columnMap.Add("Modificationdate", "MODIFICATIONDATE");
            return columnMap;

        }
        public IDictionary<string, object> Ofuserflag()
        { // TypeName="Models.Ofuserflag()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFUSERFLAG()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Starttime", "STARTTIME");
            columnMap.Add("Endtime", "ENDTIME");
            return columnMap;

        }
        public IDictionary<string, object> Ofuserprop()
        { // TypeName="Models.Ofuserprop()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFUSERPROP()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Propvalue", "PROPVALUE");
            return columnMap;

        }
        public IDictionary<string, object> Ofvcard()
        { // TypeName="Models.Ofvcard()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFVCARD()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Vcard", "VCARD");
            return columnMap;

        }
        public IDictionary<string, object> Ofversion()
        { // TypeName="Models.Ofversion()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OFVERSION()
            columnMap.Add("Name", "NAME");
            columnMap.Add("Version", "VERSION");
            return columnMap;

        }
        public IDictionary<string, object> aspnet_Applications()
        { // TypeName="Models.aspnet_Applications()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_APPLICATIONS()
            columnMap.Add("ApplicationName", "APPLICATIONNAME");
            columnMap.Add("LoweredApplicationName", "LOWEREDAPPLICATIONNAME");
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("RowNum__", "ROWNUM__");
            return columnMap;

        }
        public IDictionary<string, object> aspnet_Membership()
        { // TypeName="Models.aspnet_Membership()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_MEMBERSHIP()
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("UserId", "USERID");
            columnMap.Add("Password", "PASSWORD");
            columnMap.Add("PasswordFormat", "PASSWORDFORMAT");
            columnMap.Add("PasswordSalt", "PASSWORDSALT");
            columnMap.Add("MobilePIN", "MOBILEPIN");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("LoweredEmail", "LOWEREDEMAIL");
            columnMap.Add("PasswordQuestion", "PASSWORDQUESTION");
            columnMap.Add("PasswordAnswer", "PASSWORDANSWER");
            columnMap.Add("IsApproved", "ISAPPROVED");
            columnMap.Add("IsLockedOut", "ISLOCKEDOUT");
            columnMap.Add("CreateDate", "CREATEDATE");
            columnMap.Add("LastLoginDate", "LASTLOGINDATE");
            columnMap.Add("LastPasswordChangedDate", "LASTPASSWORDCHANGEDDATE");
            columnMap.Add("LastLockoutDate", "LASTLOCKOUTDATE");
            columnMap.Add("FailedPasswordAttemptCount", "FAILEDPWDATTEMPTCOUNT");
            columnMap.Add("FailedPasswordAttemptWindowStart", "FAILEDPWDATTEMPTWINSTART");
            columnMap.Add("FailedPasswordAnswerAttemptCount", "FAILEDPWDANSWERATTEMPTCOUNT");
            columnMap.Add("FailedPasswordAnswerAttemptWindowStart", "FAILEDPWDANSWERATTEMPTWINSTART");
            columnMap.Add("Comment", "COMMENTS");
            columnMap.Add("M_ProvinceID", "M_PROVINCEID");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("RowNum__", "ROWNUM__");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetMembershipBk244()
        { // TypeName="Models.OraAspnetMembershipBk244()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_MEMBERSHIP_BK244()
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("UserId", "USERID");
            columnMap.Add("Password", "PASSWORD");
            columnMap.Add("PasswordFormat", "PASSWORDFORMAT");
            columnMap.Add("PasswordSalt", "PASSWORDSALT");
            columnMap.Add("MobilePIN", "MOBILEPIN");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("LoweredEmail", "LOWEREDEMAIL");
            columnMap.Add("PasswordQuestion", "PASSWORDQUESTION");
            columnMap.Add("PasswordAnswer", "PASSWORDANSWER");
            columnMap.Add("IsApproved", "ISAPPROVED");
            columnMap.Add("IsLockedOut", "ISLOCKEDOUT");
            columnMap.Add("CreateDate", "CREATEDATE");
            columnMap.Add("LastLoginDate", "LASTLOGINDATE");
            columnMap.Add("LastPasswordChangedDate", "LASTPASSWORDCHANGEDDATE");
            columnMap.Add("LastLockoutDate", "LASTLOCKOUTDATE");
            columnMap.Add("FailedPasswordAttemptCount", "FAILEDPWDATTEMPTCOUNT");
            columnMap.Add("FailedPasswordAttemptWindowStart", "FAILEDPWDATTEMPTWINSTART");
            columnMap.Add("FailedPasswordAnswerAttemptCount", "FAILEDPWDANSWERATTEMPTCOUNT");
            columnMap.Add("FailedPasswordAnswerAttemptWindowStart", "FAILEDPWDANSWERATTEMPTWINSTART");
            columnMap.Add("Comment", "COMMENTS");
            columnMap.Add("M_ProvinceID", "M_PROVINCEID");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("RowNum__", "ROWNUM__");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetPaths()
        { // TypeName="Models.OraAspnetPaths()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_PATHS()
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("Pathid", "PATHID");
            columnMap.Add("Path", "PATH");
            columnMap.Add("Loweredpath", "LOWEREDPATH");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetPersonaliznallusers()
        { // TypeName="Models.OraAspnetPersonaliznallusers()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_PERSONALIZNALLUSERS()
            columnMap.Add("Pathid", "PATHID");
            columnMap.Add("Pagesettings", "PAGESETTINGS");
            columnMap.Add("Lastupdateddate", "LASTUPDATEDDATE");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetPersonaliznperuser()
        { // TypeName="Models.OraAspnetPersonaliznperuser()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_PERSONALIZNPERUSER()
            columnMap.Add("ID", "ID");
            columnMap.Add("Pathid", "PATHID");
            columnMap.Add("UserId", "USERID");
            columnMap.Add("Pagesettings", "PAGESETTINGS");
            columnMap.Add("Lastupdateddate", "LASTUPDATEDDATE");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetProfile()
        { // TypeName="Models.OraAspnetProfile()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_PROFILE()
            columnMap.Add("UserId", "USERID");
            columnMap.Add("Propertynames", "PROPERTYNAMES");
            columnMap.Add("Propertyvaluesstring", "PROPERTYVALUESSTRING");
            columnMap.Add("Propertyvaluesbinary", "PROPERTYVALUESBINARY");
            columnMap.Add("Lastupdateddate", "LASTUPDATEDDATE");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetRoles()
        { // TypeName="Models.OraAspnetRoles()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_ROLES()
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("Roleid", "ROLEID");
            columnMap.Add("Rolename", "ROLENAME");
            columnMap.Add("Loweredrolename", "LOWEREDROLENAME");
            columnMap.Add("Description", "DESCRIPTION");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetSessionapplications()
        { // TypeName="Models.OraAspnetSessionapplications()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_SESSIONAPPLICATIONS()
            columnMap.Add("Appid", "APPID");
            columnMap.Add("Appname", "APPNAME");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetSessions()
        { // TypeName="Models.OraAspnetSessions()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_SESSIONS()
            columnMap.Add("Sessionid", "SESSIONID");
            columnMap.Add("Created", "CREATED");
            columnMap.Add("Expires", "EXPIRES");
            columnMap.Add("Lockdate", "LOCKDATE");
            columnMap.Add("Lockdatelocal", "LOCKDATELOCAL");
            columnMap.Add("Lockcookie", "LOCKCOOKIE");
            columnMap.Add("Timeout", "TIMEOUT");
            columnMap.Add("Locked", "LOCKED");
            columnMap.Add("Sessionitemshort", "SESSIONITEMSHORT");
            columnMap.Add("Sessionitemlong", "SESSIONITEMLONG");
            columnMap.Add("Flags", "FLAGS");
            return columnMap;

        }
        public IDictionary<string, object> OraAspnetSitemap()
        { // TypeName="Models.OraAspnetSitemap()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_SITEMAP()
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("ID", "ID");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("URL", "URL");
            columnMap.Add("Roles", "ROLES");
            columnMap.Add("Parent", "PARENT");
            return columnMap;

        }
        public IDictionary<string, object> aspnet_Users()
        { // TypeName="Models.aspnet_Users()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ORA_ASPNET_USERS()
            columnMap.Add("ALLOW_SITE", "ALLOW_SITE");
            columnMap.Add("ApplicationId", "APPLICATIONID");
            columnMap.Add("UserId", "USERID");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("LoweredUserName", "LOWEREDUSERNAME");
            columnMap.Add("MobileAlias", "MOBILEALIAS");
            columnMap.Add("IsAnonymous", "ISANONYMOUS");
            columnMap.Add("LastActivityDate", "LASTACTIVITYDATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCEID");
            columnMap.Add("M_OldID", "M_OLDID");
            columnMap.Add("RowNum__", "ROWNUM__");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> OtherService()
        { // TypeName="Models.OtherService()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OTHER_SERVICE()
            columnMap.Add("OtherServiceID", "OTHER_SERVICE_ID");
            columnMap.Add("OtherServiceName", "OTHER_SERVICE_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("Price", "PRICE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EffectDate", "EFFECT_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> OverallTest()
        { // TypeName="Models.OverallTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="OVERALL_TEST()
            columnMap.Add("OverallTestID", "OVERALL_TEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("HistoryYourSelf", "HISTORY_YOUR_SELF");
            columnMap.Add("OverallInternal", "OVERALL_INTERNAL");
            columnMap.Add("DescriptionOverallInternall", "DESCRIPTION_OVERALL_INTERNALL");
            columnMap.Add("OverallForeign", "OVERALL_FOREIGN");
            columnMap.Add("DescriptionForeign", "DESCRIPTION_FOREIGN");
            columnMap.Add("SkinDiseases", "SKIN_DISEASES");
            columnMap.Add("DescriptionSkinDiseases", "DESCRIPTION_SKIN_DISEASES");
            columnMap.Add("IsHeartDiseases", "IS_HEART_DISEASES");
            columnMap.Add("IsBirthDefect", "IS_BIRTH_DEFECT");
            columnMap.Add("Other", "OTHER");
            columnMap.Add("EvaluationHealth", "EVALUATION_HEALTH");
            columnMap.Add("RequireOfDoctor", "REQUIRE_OF_DOCTOR");
            columnMap.Add("Doctor", "DOCTOR");
            columnMap.Add("IsDredging", "IS_DREDGING");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Package()
        { // TypeName="Models.Package()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PACKAGE()
            columnMap.Add("PackageID", "PACKAGE_ID");
            columnMap.Add("PackageCode", "PACKAGE_CODE");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("ServiceID", "SERVICE_ID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("MaxSMS", "MAX_SMS");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateUser", "CREATE_USER");
            columnMap.Add("UpdateUser", "UPDATE_USER");
            return columnMap;

        }
        public IDictionary<string, object> ParticularPupil()
        { // TypeName="Models.ParticularPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PARTICULAR_PUPIL()
            columnMap.Add("ParticularPupilID", "PARTICULAR_PUPIL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("RecordedDate", "RECORDED_DATE");
            columnMap.Add("UpdatedDate", "UPDATED_DATE");
            columnMap.Add("RealUpdatedDate", "REAL_UPDATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ParticularPupilCharacteristic()
        { // TypeName="Models.ParticularPupilCharacteristic()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PARTICULAR_PUPIL_CHA()
            columnMap.Add("ParticularPupilCharacteristicID", "PARTICULAR_PUPIL_CHA_ID");
            columnMap.Add("ParticularPupilID", "PARTICULAR_PUPIL_ID");
            columnMap.Add("FamilyCharacteristic", "FAMILY_CHARACTERISTIC");
            columnMap.Add("PsychologyCharacteristic", "PSYCHOLOGY_CHARACTERISTIC");
            columnMap.Add("RealUpdatedDate", "REAL_UPDATED_DATE");
            columnMap.Add("RecordedDate", "RECORDED_DATE");
            columnMap.Add("UpdatedDate", "UPDATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ParticularPupilTreatment()
        { // TypeName="Models.ParticularPupilTreatment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PARTICULAR_PUPIL_TREATMENT()
            columnMap.Add("ParticularPupilTreatmentID", "PARTICULAR_PUPIL_TREATMENT_ID");
            columnMap.Add("ParticularPupilID", "PARTICULAR_PUPIL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("TreatmentResolution", "TREATMENT_RESOLUTION");
            columnMap.Add("Result", "RESULT");
            columnMap.Add("AppliedDate", "APPLIED_DATE");
            columnMap.Add("UpdatedDate", "UPDATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PeriodDeclaration()
        { // TypeName="Models.PeriodDeclaration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PERIOD_DECLARATION()
            columnMap.Add("PeriodDeclarationID", "PERIOD_DECLARATION_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("InterviewMark", "INTERVIEW_MARK");
            columnMap.Add("StrInterviewMark", "STR_INTERVIEW_MARK");
            columnMap.Add("StartIndexOfInterviewMark", "START_INDEX_OF_INTERVIEW_MARK");
            columnMap.Add("WritingMark", "WRITING_MARK");
            columnMap.Add("StrWritingMark", "STR_WRITING_MARK");
            columnMap.Add("StartIndexOfWritingMark", "START_INDEX_OF_WRITING_MARK");
            columnMap.Add("TwiceCoeffiecientMark", "TWICE_COEFFIECIENT_MARK");
            columnMap.Add("StrTwiceCoeffiecientMark", "STR_TWICE_MARK");
            columnMap.Add("StartIndexOfTwiceCoeffiecientMark", "START_INDEX_OF_TWICE_COE_MAR");
            columnMap.Add("IsLock", "IS_LOCK");
            columnMap.Add("ContaintSemesterMark", "CONTAINT_SEMESTER_MARK");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PhysicalTest()
        { // TypeName="Models.PhysicalTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PHYSICAL_TEST()
            columnMap.Add("PhysicalTestID", "PHYSICAL_TEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("Date", "DATE");
            columnMap.Add("Height", "HEIGHT");
            columnMap.Add("Weight", "WEIGHT");
            columnMap.Add("Breast", "BREAST");
            columnMap.Add("PhysicalClassification", "PHYSICAL_CLASSIFICATION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Nutrition", "NUTRITION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> PolicyRegime()
        { // TypeName="Models.PolicyRegime()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="POLICY_REGIME()
            columnMap.Add("PolicyRegimeID", "POLICY_REGIME_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> PolicyTarget()
        { // TypeName="Models.PolicyTarget()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="POLICY_TARGET()
            columnMap.Add("PolicyTargetID", "POLICY_TARGET_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Code", "CODE");
            return columnMap;

        }
        public IDictionary<string, object> PoliticalGrade()
        { // TypeName="Models.PoliticalGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="POLITICAL_GRADE()
            columnMap.Add("PoliticalGradeID", "POLITICAL_GRADE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PraiseType()
        { // TypeName="Models.PraiseType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PRAISE_TYPE()
            columnMap.Add("PraiseTypeID", "PRAISE_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("ConductMark", "CONDUCT_MARK");
            columnMap.Add("GraduationMark", "GRADUATION_MARK");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PriorityType()
        { // TypeName="Models.PriorityType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PRIORITY_TYPE()
            columnMap.Add("PriorityTypeID", "PRIORITY_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ProcessedCellData()
        { // TypeName="Models.ProcessedCellData()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROCESSED_CELL_DATA()
            columnMap.Add("ProcessedCellDataID", "PROCESSED_CELL_DATA_ID");
            columnMap.Add("ProcessedDate", "PROCESSED_DATE");
            columnMap.Add("ProcessedReportID", "PROCESSED_REPORT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CellDefinitionID", "CELL_DEFINITION_ID");
            columnMap.Add("CellValue", "CELL_VALUE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("CellAddress", "CELL_ADDRESS");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("SheetName", "SHEET_NAME");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            return columnMap;

        }
        public IDictionary<string, object> ProcessedReport()
        { // TypeName="Models.ProcessedReport()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROCESSED_REPORT()
            columnMap.Add("ProcessedReportID", "PROCESSED_REPORT_ID");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("ProcessedDate", "PROCESSED_DATE");
            columnMap.Add("ReportData", "REPORT_DATA");
            columnMap.Add("DeserializeClass", "DESERIALIZE_CLASS");
            columnMap.Add("HandlerProcess", "HANDLER_PROCESS");
            columnMap.Add("ExpiredDate", "EXPIRED_DATE");
            columnMap.Add("SentToSupervisor", "SENT_TO_SUPERVISOR");
            columnMap.Add("ReportName", "REPORT_NAME");
            columnMap.Add("InputParameterHashKey", "INPUT_PARAMETER_HASH_KEY");
            columnMap.Add("ViewBooklet", "VIEW_BOOKLET");
            return columnMap;

        }
        public IDictionary<string, object> ProcessedReportParameter()
        { // TypeName="Models.ProcessedReportParameter()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROCESSED_REPORT_PARAMETER()
            columnMap.Add("ProcessedReportParameterID", "PROCESSED_REPORT_PARAMETER_ID");
            columnMap.Add("ProcessedReportID", "PROCESSED_REPORT_ID");
            columnMap.Add("ParameterName", "PARAMETER_NAME");
            columnMap.Add("ParameterValue", "PARAMETER_VALUE");
            return columnMap;

        }
        public IDictionary<string, object> PropertyOfClass()
        { // TypeName="Models.PropertyOfClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROPERTY_OF_CLASS()
            columnMap.Add("PropertyOfClassID", "PROPERTY_OF_CLASS_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("ClassPropertyCatID", "CLASS_PROPERTY_CAT_ID");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PropertyOfSchool()
        { // TypeName="Models.PropertyOfSchool()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROPERTY_OF_SCHOOL()
            columnMap.Add("PropertyOfSchoolID", "PROPERTY_OF_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SchoolPropertyCatID", "SCHOOL_PROPERTY_CAT_ID");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Province()
        { // TypeName="Models.Province()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROVINCE()
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("ProvinceName", "PROVINCE_NAME");
            columnMap.Add("ProvinceCode", "PROVINCE_CODE");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("ProvinceExamCode", "PROVINCE_EXAM_CODE");
            return columnMap;

        }
        public IDictionary<string, object> ProvinceSubject()
        { // TypeName="Models.ProvinceSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PROVINCE_SUBJECT()
            columnMap.Add("ProvinceSubjectID", "PROVINCE_SUBJECT_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("AcademicYearOfProvinceID", "ACADEMIC_YEAR_OF_PROVINCE_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("IsRegularEducation", "IS_REGULAR_EDUCATION");
            columnMap.Add("Coefficient", "COEFFICIENT");
            columnMap.Add("AppliedType", "APPLIED_TYPE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("SectionPerWeekFirstSemester", "SECTION_PER_WEEK_FIRST_SEM");
            columnMap.Add("SectionPerWeekSecondSemester", "SECTION_PER_WEEK_SECOND_SEM");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilAbsence()
        { // TypeName="Models.PupilAbsence()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_ABSENCE()
            columnMap.Add("PupilAbsenceID", "PUPIL_ABSENCE_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("AbsentDate", "ABSENT_DATE");
            columnMap.Add("IsAccepted", "IS_ACCEPTED");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilDiscipline()
        { // TypeName="Models.PupilDiscipline()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_DISCIPLINE()
            columnMap.Add("PupilDisciplineID", "PUPIL_DISCIPLINE_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("DisciplineTypeID", "DISCIPLINE_TYPE_ID");
            columnMap.Add("IsRecordedInSchoolReport", "IS_RECORDED_IN_SCHOOL_REPORT");
            columnMap.Add("DisciplineLevel", "DISCIPLINE_LEVEL");
            columnMap.Add("DisciplinedDate", "DISCIPLINED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("LocationID", "LOCATION_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilEmulation()
        { // TypeName="Models.PupilEmulation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_EMULATION()
            columnMap.Add("PupilEmulationID", "PUPIL_EMULATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("HonourAchivementTypeID", "HONOUR_ACHIVEMENT_TYPE_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("M_Category", "M_CATEGORY");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilFault()
        { // TypeName="Models.PupilFault()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_FAULT()
            columnMap.Add("PupilFaultID", "PUPIL_FAULT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("FaultID", "FAULT_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ViolatedDate", "VIOLATED_DATE");
            columnMap.Add("NumberOfFault", "NUMBER_OF_FAULT");
            columnMap.Add("TotalPenalizedMark", "TOTAL_PENALIZED_MARK");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Note", "NOTE");
            return columnMap;

        }
        public IDictionary<string, object> PupilGraduation()
        { // TypeName="Models.PupilGraduation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_GRADUATION()
            columnMap.Add("PupilGraduationID", "PUPIL_GRADUATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("GraduationLevel", "GRADUATION_LEVEL");
            columnMap.Add("GraduationGrade", "GRADUATION_GRADE");
            columnMap.Add("IssuedNumber", "ISSUED_NUMBER");
            columnMap.Add("RecordNumber", "RECORD_NUMBER");
            columnMap.Add("IssuedDate", "ISSUED_DATE");
            columnMap.Add("ReceivedDate", "RECEIVED_DATE");
            columnMap.Add("IsReceived", "IS_RECEIVED");
            columnMap.Add("PriorityReason", "PRIORITY_REASON");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Exemption", "EXEMPTION");
            columnMap.Add("Receiver", "RECEIVER");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("MeritPoint", "MERIT_POINT");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilLeavingOff()
        { // TypeName="Models.PupilLeavingOff()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_LEAVING_OFF()
            columnMap.Add("PupilLeavingOffID", "PUPIL_LEAVING_OFF_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LeavingReasonID", "LEAVING_REASON_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("LeavingDate", "LEAVING_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsReserve", "IS_RESERVE");
            columnMap.Add("AcceptClassID", "ACCEPT_CLASS_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilOfClass()
        { // TypeName="Models.PupilOfClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_OF_CLASS()
            columnMap.Add("PupilOfClassID", "PUPIL_OF_CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("OrderInClass", "ORDER_IN_CLASS");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("NoConductEstimation", "NO_CONDUCT_ESTIMATION");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsRegisterContract", "IS_REGISTER_CONTRACT");
            return columnMap;

        }
        public IDictionary<string, object> PupilOfSchool()
        { // TypeName="Models.PupilOfSchool()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_OF_SCHOOL()
            //columnMap.Add("PupilOfSchoolID", "PUPIL_OF_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EnrolmentDate", "ENROLMENT_DATE");
            columnMap.Add("EnrolmentType", "ENROLMENT_TYPE");
            columnMap.Add("M_Year", "M_YEAR");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilPraise()
        { // TypeName="Models.PupilPraise()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_PRAISE()
            columnMap.Add("PupilPraiseID", "PUPIL_PRAISE_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PraiseTypeID", "PRAISE_TYPE_ID");
            columnMap.Add("PraiseDate", "PRAISE_DATE");
            columnMap.Add("PraiseLevel", "PRAISE_LEVEL");
            columnMap.Add("IsRecordedInSchoolReport", "IS_RECORDED_IN_SCHOOL_REPORT");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("LocationID", "LOCATION_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilProfile()
        { // TypeName="Models.PupilProfile()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_PROFILE()
            //columnMap.Add("PupilProfileID", "PUPIL_PROFILE_ID");
            columnMap.Add("CurrentClassID", "CURRENT_CLASS_ID");
            columnMap.Add("CurrentSchoolID", "CURRENT_SCHOOL_ID");
            columnMap.Add("CurrentAcademicYearID", "CURRENT_ACADEMIC_YEAR_ID");
            columnMap.Add("AreaID", "AREA_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("EthnicID", "ETHNIC_ID");
            columnMap.Add("ReligionID", "RELIGION_ID");
            columnMap.Add("PolicyTargetID", "POLICY_TARGET_ID");
            columnMap.Add("FamilyTypeID", "FAMILY_TYPE_ID");
            columnMap.Add("PriorityTypeID", "PRIORITY_TYPE_ID");
            columnMap.Add("PreviousGraduationLevelID", "PREVIOUS_GRADUATION_LEVEL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("Name", "NAME");
            columnMap.Add("FullName", "FULL_NAME");
            columnMap.Add("Genre", "GENRE");
            columnMap.Add("BirthDate", "BIRTH_DATE");
            columnMap.Add("BirthPlace", "BIRTH_PLACE");
            columnMap.Add("HomeTown", "HOME_TOWN");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Mobile", "MOBILE");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("TempResidentalAddress", "TEMP_RESIDENTAL_ADDRESS");
            columnMap.Add("PermanentResidentalAddress", "PERMANENT_RESIDENTAL_ADDRESS");
            columnMap.Add("IsResidentIn", "IS_RESIDENT_IN");
            columnMap.Add("IsDisabled", "IS_DISABLED");
            columnMap.Add("DisabledTypeID", "DISABLED_TYPE_ID");
            columnMap.Add("DisabledSeverity", "DISABLED_SEVERITY");
            columnMap.Add("BloodType", "BLOOD_TYPE");
            columnMap.Add("EnrolmentMark", "ENROLMENT_MARK");
            columnMap.Add("EnrolmentDate", "ENROLMENT_DATE");
            columnMap.Add("ForeignLanguageTraining", "FOREIGN_LANGUAGE_TRAINING");
            columnMap.Add("IsYoungPioneerMember", "IS_YOUNG_PIONEER_MEMBER");
            columnMap.Add("YoungPioneerJoinedDate", "YOUNG_PIONEER_JOINED_DATE");
            columnMap.Add("YoungPioneerJoinedPlace", "YOUNG_PIONEER_JOINED_PLACE");
            columnMap.Add("IsYouthLeageMember", "IS_YOUTH_LEAGE_MEMBER");
            columnMap.Add("YouthLeagueJoinedDate", "YOUTH_LEAGUE_JOINED_DATE");
            columnMap.Add("YouthLeagueJoinedPlace", "YOUTH_LEAGUE_JOINED_PLACE");
            columnMap.Add("IsCommunistPartyMember", "IS_COMMUNIST_PARTY_MEMBER");
            columnMap.Add("CommunistPartyJoinedDate", "COMMUNIST_PARTY_JOINED_DATE");
            columnMap.Add("CommunistPartyJoinedPlace", "COMMUNIST_PARTY_JOINED_PLACE");
            columnMap.Add("FatherFullName", "FATHER_FULL_NAME");
            columnMap.Add("FatherBirthDate", "FATHER_BIRTH_DATE");
            columnMap.Add("FatherJob", "FATHER_JOB");
            columnMap.Add("FatherMobile", "FATHER_MOBILE");
            columnMap.Add("MotherFullName", "MOTHER_FULL_NAME");
            columnMap.Add("MotherBirthDate", "MOTHER_BIRTH_DATE");
            columnMap.Add("MotherJob", "MOTHER_JOB");
            columnMap.Add("MotherMobile", "MOTHER_MOBILE");
            columnMap.Add("SponsorFullName", "SPONSOR_FULL_NAME");
            columnMap.Add("SponsorBirthDate", "SPONSOR_BIRTH_DATE");
            columnMap.Add("SponsorJob", "SPONSOR_JOB");
            columnMap.Add("SponsorMobile", "SPONSOR_MOBILE");
            columnMap.Add("ProfileStatus", "PROFILE_STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("HeightAtBirth", "HEIGHT_AT_BIRTH");
            columnMap.Add("FatherEmail", "FATHER_EMAIL");
            columnMap.Add("MotherEmail", "MOTHER_EMAIL");
            columnMap.Add("SponsorEmail", "SPONSOR_EMAIL");
            columnMap.Add("ChildOrder", "CHILD_ORDER");
            columnMap.Add("NumberOfChild", "NUMBER_OF_CHILD");
            columnMap.Add("EatingHabit", "EATING_HABIT");
            columnMap.Add("ReactionTrainingHabit", "REACTION_TRAINING_HABIT");
            columnMap.Add("AttitudeInStrangeScene", "ATTITUDE_IN_STRANGE_SCENE");
            columnMap.Add("OtherHabit", "OTHER_HABIT");
            columnMap.Add("FavoriteToy", "FAVORITE_TOY");
            columnMap.Add("EvaluationConfigID", "EVALUATION_CONFIG_ID");
            columnMap.Add("WeightAtBirth", "WEIGHT_AT_BIRTH");
            columnMap.Add("PolicyRegimeID", "POLICY_REGIME_ID");
            columnMap.Add("SupportingPolicy", "SUPPORTING_POLICY");
            columnMap.Add("IsReceiveRiceSubsidy", "IS_RECEIVE_RICE_SUBSIDY");
            columnMap.Add("IsResettlementTarget", "IS_RESETTLEMENT_TARGET");
            columnMap.Add("IsSupportForLearning", "IS_SUPPORT_FOR_LEARNING");
            columnMap.Add("ClassType", "CLASS_TYPE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsFatherSMS", "IS_FATHER_SMS");
            columnMap.Add("IsMotherSMS", "IS_MOTHER_SMS");
            columnMap.Add("IsSponsorSMS", "IS_SPONSOR_SMS");
            columnMap.Add("Image", "IMAGE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("PupilLearningType", "PUPIL_LEARNING_TYPE");
            columnMap.Add("LanguageCertificateID", "LANGUAGE_CERTIFICATE_ID");
            columnMap.Add("ItCertificateID", "IT_CERTIFICATE_ID");
            columnMap.Add("ItCertificate", "IT_CERTIFICATE");
            columnMap.Add("LanguageCertificate", "LANGUAGE_CERTIFICATE");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("VillageID", "VILLAGE_ID");
            columnMap.Add("FavouriteTypeOfDish", "FAVOURITE_TYPE_OF_DISH");
            columnMap.Add("HateTypeOfDish", "HATE_TYPE_OF_DISH");
            columnMap.Add("IdentifyNumber", "IDENTIFY_NUMBER");
            columnMap.Add("HomePlace", "HOME_PLACE");
            columnMap.Add("StorageNumber", "STORAGE_NUMBER");
            columnMap.Add("IsSwimming", "IS_SWIMMING");
            //columnMap.Add("sync", "SYNC_CODE");
            return columnMap;

        }
        public IDictionary<string, object> PupilRanking()
        { // TypeName="Models.PupilRanking()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_RANKING()
            columnMap.Add("PupilRankingID", "PUPIL_RANKING_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("AverageMark", "AVERAGE_MARK");
            columnMap.Add("TotalAbsentDaysWithPermission", "TOTAL_ABSENT_DAYS_WITH_PER");
            columnMap.Add("TotalAbsentDaysWithoutPermission", "TOTAL_ABSENT_DAYS_WITHOUT_PER");
            columnMap.Add("CapacityLevelID", "CAPACITY_LEVEL_ID");
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("RankingDate", "RANKING_DATE");
            columnMap.Add("StudyingJudgementID", "STUDYING_JUDGEMENT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsCategory", "IS_CATEGORY");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilRankingComment", "PUPIL_RANKING_COMMENT");
            return columnMap;

        }
        public IDictionary<string, object> PupilRankingHistory()
        { // TypeName="Models.PupilRankingHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_RANKING_HISTORY()
            columnMap.Add("PupilRankingID", "PUPIL_RANKING_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("AverageMark", "AVERAGE_MARK");
            columnMap.Add("TotalAbsentDaysWithPermission", "TOTAL_ABSENT_DAYS_WITH_PER");
            columnMap.Add("TotalAbsentDaysWithoutPermission", "TOTAL_ABSENT_DAYS_WITHOUT_PER");
            columnMap.Add("CapacityLevelID", "CAPACITY_LEVEL_ID");
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("RankingDate", "RANKING_DATE");
            columnMap.Add("StudyingJudgementID", "STUDYING_JUDGEMENT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsCategory", "IS_CATEGORY");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilRankingComment", "PUPIL_RANKING_COMMENT");
            return columnMap;

        }
        public IDictionary<string, object> PupilRepeated()
        { // TypeName="Models.PupilRepeated()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_REPEATED()
            columnMap.Add("PupilRepeatedID", "PUPIL_REPEATED_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("OldClassID", "OLD_CLASS_ID");
            columnMap.Add("RepeatedClassID", "REPEATED_CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("RepeatedDate", "REPEATED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilRetestRegistration()
        { // TypeName="Models.PupilRetestRegistration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_RETEST_REGISTRATION()
            columnMap.Add("PupilRetestRegistrationID", "PUPIL_RETEST_REGISTRATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("RegisteredDate", "REGISTERED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> PupilReviewEvaluate()
        { // TypeName="Models.PupilReviewEvaluate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUPIL_REVIEW_EVALUATE()
            columnMap.Add("PupilReviewEvaluateID", "PUPIL_REVIEW_EVALUATE_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("EducationLevel", "EDUCATION_LEVEL");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("MonthReview", "MONTH_REVIEW");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("DateTimeComment", "DATETIME_COMMENT");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("MarkRate", "MARK_RATE");
            return columnMap;

        }
        public IDictionary<string, object> QualificationGrade()
        { // TypeName="Models.QualificationGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="QUALIFICATION_GRADE()
            columnMap.Add("QualificationGradeID", "QUALIFICATION_GRADE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> QualificationLevel()
        { // TypeName="Models.QualificationLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="QUALIFICATION_LEVEL()
            columnMap.Add("QualificationLevelID", "QUALIFICATION_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> QualificationType()
        { // TypeName="Models.QualificationType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="QUALIFICATION_TYPE()
            columnMap.Add("QualificationTypeID", "QUALIFICATION_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> Religion()
        { // TypeName="Models.Religion()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="RELIGION()
            columnMap.Add("ReligionID", "RELIGION_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Code", "CODE");
            return columnMap;

        }
        public IDictionary<string, object> ReportCategory()
        { // TypeName="Models.ReportCategory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REPORT_CATEGORY()
            columnMap.Add("ReportCategoryID", "REPORT_CATEGORY_ID");
            columnMap.Add("ReportCategoryName", "REPORT_CATEGORY_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ReportDefinition()
        { // TypeName="Models.ReportDefinition()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REPORT_DEFINITION()
            columnMap.Add("ReportDefinitionID", "REPORT_DEFINITION_ID");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("ReportCategoryID", "REPORT_CATEGORY_ID");
            columnMap.Add("TemplateName", "TEMPLATE_NAME");
            columnMap.Add("TemplateDirectory", "TEMPLATE_DIRECTORY");
            columnMap.Add("OutputNamePattern", "OUTPUT_NAME_PATTERN");
            columnMap.Add("OutputFormat", "OUTPUT_FORMAT");
            columnMap.Add("OutputMimeType", "OUTPUT_MIME_TYPE");
            columnMap.Add("RenderingHandler", "RENDERING_HANDLER");
            columnMap.Add("IsPreprocessed", "IS_PREPROCESSED");
            return columnMap;

        }
        public IDictionary<string, object> ReportParameter()
        { // TypeName="Models.ReportParameter()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REPORT_PARAMETER()
            columnMap.Add("ReportParameterID", "REPORT_PARAMETER_ID");
            columnMap.Add("ReportCode", "REPORT_CODE");
            columnMap.Add("ParameterName", "PARAMETER_NAME");
            columnMap.Add("ParameterOrder", "PARAMETER_ORDER");
            columnMap.Add("IsOptional", "IS_OPTIONAL");
            columnMap.Add("DefaultValue", "DEFAULT_VALUE");
            columnMap.Add("ParameterType", "PARAMETER_TYPE");
            return columnMap;

        }
        public IDictionary<string, object> Role()
        { // TypeName="Models.Role()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ROLE()
            columnMap.Add("RoleID", "ROLE_ID");
            columnMap.Add("ParentID", "PARENT_ID");
            columnMap.Add("RoleName", "ROLE_NAME");
            columnMap.Add("RolePath", "ROLE_PATH");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> RoleMenu()
        { // TypeName="Models.RoleMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="ROLE_MENU()
            columnMap.Add("RoleMenuID", "ROLE_MENU_ID");
            columnMap.Add("RoleID", "ROLE_ID");
            columnMap.Add("MenuID", "MENU_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Rule()
        { // TypeName="Models.Rule()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="RULE()
            columnMap.Add("RuleID", "RULE_ID");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("RowSelected", "ROW_SELECTED");
            columnMap.Add("RowInserted", "ROW_INSERTED");
            columnMap.Add("SrcTable", "SRC_TABLE");
            columnMap.Add("DestTable", "DEST_TABLE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("RuleContent", "RULE_CONTENT");
            columnMap.Add("UpdateContent", "UPDATE_CONTENT");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("RowChildSelected", "ROW_CHILD_SELECTED");
            columnMap.Add("RowChildInserted", "ROW_CHILD_INSERTED");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("DestChildTable", "DEST_CHILD_TABLE");
            return columnMap;

        }
        public IDictionary<string, object> SalaryLevel()
        { // TypeName="Models.SalaryLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SALARY_LEVEL()
            columnMap.Add("SalaryLevelID", "SALARY_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("ScaleID", "SCALE_ID");
            columnMap.Add("SubLevel", "SUB_LEVEL");
            columnMap.Add("Coefficient", "COEFFICIENT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolCodeConfig()
        { // TypeName="Models.SchoolCodeConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_CODE_CONFIG()
            columnMap.Add("SchoolCodeConfigID", "SCHOOL_CODE_CONFIG_ID");
            columnMap.Add("CodeConfigID", "CODE_CONFIG_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("StartNumber", "START_NUMBER");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("M_Type", "M_TYPE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MConfigSchool", "M_CONFIG_SCHOOL");
            columnMap.Add("MConfigStudent", "M_CONFIG_STUDENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolFaculty()
        { // TypeName="Models.SchoolFaculty()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_FACULTY()
            //columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("FacultyName", "FACULTY_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolMovement()
        { // TypeName="Models.SchoolMovement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_MOVEMENT()
            columnMap.Add("SchoolMovementID", "SCHOOL_MOVEMENT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("MovedToProvinceID", "MOVED_TO_PROVINCE_ID");
            columnMap.Add("MovedToDistrictID", "MOVED_TO_DISTRICT_ID");
            columnMap.Add("MovedToClassID", "MOVED_TO_CLASS_ID");
            columnMap.Add("MovedToSchoolID", "MOVED_TO_SCHOOL_ID");
            columnMap.Add("MovedToClassName", "MOVED_TO_CLASS_NAME");
            columnMap.Add("MovedToSchoolName", "MOVED_TO_SCHOOL_NAME");
            columnMap.Add("MovedToSchoolAddress", "MOVED_TO_SCHOOL_ADDRESS");
            columnMap.Add("MovedDate", "MOVED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolProfile()
        { // TypeName="Models.SchoolProfile()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_PROFILE()
            columnMap.Add("SchoolProfileID", "SCHOOL_PROFILE_ID");
            columnMap.Add("SchoolCode", "SCHOOL_CODE");
            columnMap.Add("SchoolName", "SCHOOL_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("AreaID", "AREA_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("TrainingTypeID", "TRAINING_TYPE_ID");
            columnMap.Add("SchoolTypeID", "SCHOOL_TYPE_ID");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Fax", "FAX");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("Address", "ADDRESS");
            columnMap.Add("EstablishedDate", "ESTABLISHED_DATE");
            columnMap.Add("Website", "WEBSITE");
            columnMap.Add("EducationGrade", "EDUCATION_GRADE");
            columnMap.Add("ReportTile", "REPORT_TILE");
            columnMap.Add("HeadMasterName", "HEAD_MASTER_NAME");
            columnMap.Add("HeadMasterPhone", "HEAD_MASTER_PHONE");
            columnMap.Add("HasSubsidiary", "HAS_SUBSIDIARY");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("IsNewSchoolModel", "IS_NEW_SCHOOL_MODEL");
            columnMap.Add("InSpecialDifficultZone", "IN_SPECIAL_DIFFICULT_ZONE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AdminID", "ADMIN_ID");
            columnMap.Add("SchoolYearTitle", "SCHOOL_YEAR_TITLE");
            columnMap.Add("SMSTeacherActiveType", "SMSTEACHER_ACTIVE_TYPE");
            columnMap.Add("SMSParentActiveType", "SMSPARENT_ACTIVE_TYPE");
            columnMap.Add("CancelActiveReason", "CANCEL_ACTIVE_REASON");
            columnMap.Add("M_OldAdminID", "M_OLD_ADMIN_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsActiveSMAS", "IS_ACTIVE_SMAS");
            columnMap.Add("Image", "IMAGE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("SchoolExamCode", "SCHOOL_EXAM_CODE");
            columnMap.Add("ProductVersion", "PRODUCT_VERSION");
            return columnMap;

        }
        public IDictionary<string, object> SchoolPropertyCat()
        { // TypeName="Models.SchoolPropertyCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_PROPERTY_CAT()
            columnMap.Add("SchoolPropertyCatID", "SCHOOL_PROPERTY_CAT_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("AppliedLevelDetail", "APPLIED_LEVEL_DETAIL");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolSubject()
        { // TypeName="Models.SchoolSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_SUBJECT()
            //columnMap.Add("SchoolSubjectID", "SCHOOL_SUBJECT_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("FirstSemesterCoefficient", "FIRST_SEMESTER_COEFFICIENT");
            columnMap.Add("SecondSemesterCoefficient", "SECOND_SEMESTER_COEFFICIENT");
            columnMap.Add("AppliedType", "APPLIED_TYPE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("SectionPerWeekFirstSemester", "SECTION_PER_WEEK_FIRST_SEM");
            columnMap.Add("SectionPerWeekSecondSemester", "SECTION_PER_WEEK_SECOND_SEM");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolSubsidiary()
        { // TypeName="Models.SchoolSubsidiary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_SUBSIDIARY()
            columnMap.Add("SchoolSubsidiaryID", "SCHOOL_SUBSIDIARY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SubsidiaryCode", "SUBSIDIARY_CODE");
            columnMap.Add("SubsidiaryName", "SUBSIDIARY_NAME");
            columnMap.Add("Address", "ADDRESS");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Fax", "FAX");
            columnMap.Add("DistanceFromHQ", "DISTANCE_FROM_HQ");
            columnMap.Add("SquareArea", "SQUARE_AREA");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SchoolType()
        { // TypeName="Models.SchoolType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_TYPE()
            columnMap.Add("SchoolTypeID", "SCHOOL_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SCSService()
        { // TypeName="Models.SCSService()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCSSERVICE()
            columnMap.Add("SCSServiceID", "SCSSERVICE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ServiceCode", "SERVICE_CODE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("ManagementUrl", "MANAGEMENT_URL");
            columnMap.Add("PortalUrl", "PORTAL_URL");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("UpdateDate", "UPDATE_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SemeterDeclaration()
        { // TypeName="Models.SemeterDeclaration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SEMETER_DECLARATION()
            columnMap.Add("SemeterDeclarationID", "SEMETER_DECLARATION_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("InterviewMark", "INTERVIEW_MARK");
            columnMap.Add("WritingMark", "WRITING_MARK");
            columnMap.Add("TwiceCoeffiecientMark", "TWICE_COEFFIECIENT_MARK");
            columnMap.Add("IsLock", "IS_LOCK");
            columnMap.Add("CoefficientInterview", "COEFFICIENT_INTERVIEW");
            columnMap.Add("CoefficientWriting", "COEFFICIENT_WRITING");
            columnMap.Add("CoefficientTwice", "COEFFICIENT_TWICE");
            columnMap.Add("SemesterMark", "SEMESTER_MARK");
            columnMap.Add("CoefficientSemester", "COEFFICIENT_SEMESTER");
            columnMap.Add("AppliedDate", "APPLIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SendInfo()
        { // TypeName="Models.SendInfo()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SEND_INFO()
            columnMap.Add("SendInfoID", "SEND_INFO_ID");
            columnMap.Add("PackageID", "PACKAGE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("EmployeeCode", "EMPLOYEE_CODE");
            columnMap.Add("ReceiverName", "RECEIVER_NAME");
            columnMap.Add("TypeOfReceiver", "TYPE_OF_RECEIVER");
            columnMap.Add("SchedularType", "SCHEDULAR_TYPE");
            columnMap.Add("InfoType", "INFO_TYPE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("ReceiverEmail", "RECEIVER_EMAIL");
            columnMap.Add("ReceiverMobile", "RECEIVER_MOBILE");
            columnMap.Add("SendType", "SEND_TYPE");
            columnMap.Add("RetryNum", "RETRY_NUM");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("CreateUser", "CREATE_USER");
            columnMap.Add("SupervisingDeptCode", "SUPERVISING_DEPT_CODE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SendResult()
        { // TypeName="Models.SendResult()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SEND_RESULT()
            columnMap.Add("SendResultID", "SEND_RESULT_ID");
            columnMap.Add("PackageID", "PACKAGE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("EmployeeCode", "EMPLOYEE_CODE");
            columnMap.Add("TypeOfReceiver", "TYPE_OF_RECEIVER");
            columnMap.Add("SchedularType", "SCHEDULAR_TYPE");
            columnMap.Add("InfoType", "INFO_TYPE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("ReceiverEmail", "RECEIVER_EMAIL");
            columnMap.Add("ReceiverMobile", "RECEIVER_MOBILE");
            columnMap.Add("SendType", "SEND_TYPE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("TimeSend", "TIME_SEND");
            columnMap.Add("ServiceNumber", "SERVICE_NUMBER");
            columnMap.Add("RetryNum", "RETRY_NUM");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("CreateUser", "CREATE_USER");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> Service()
        { // TypeName="Models.Service()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SERVICE()
            columnMap.Add("ServiceID", "SERVICE_ID");
            columnMap.Add("ServiceCode", "SERVICE_CODE");
            columnMap.Add("ServiceName", "SERVICE_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("IsStatus", "IS_STATUS");
            columnMap.Add("ServiceType", "SERVICE_TYPE");
            columnMap.Add("UnitPrice", "UNIT_PRICE");
            columnMap.Add("SchedularType", "SCHEDULAR_TYPE");
            columnMap.Add("MaxSMS", "MAX_SMS");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateUser", "CREATE_USER");
            columnMap.Add("UpdateUser", "UPDATE_USER");
            return columnMap;

        }
        public IDictionary<string, object> Skill()
        { // TypeName="Models.Skill()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SKILL()
            columnMap.Add("SkillID", "SKILL_ID");
            columnMap.Add("SkillTitleID", "SKILL_TITLE_ID");
            columnMap.Add("WeekNumber", "WEEK_NUMBER");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Period", "PERIOD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SkillOfClass()
        { // TypeName="Models.SkillOfClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SKILL_OF_CLASS()
            columnMap.Add("SkillOfClassID", "SKILL_OF_CLASS_ID");
            columnMap.Add("SkillTitleID", "SKILL_TITLE_ID");
            columnMap.Add("WeekNumber", "WEEK_NUMBER");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Period", "PERIOD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SkillOfPupil()
        { // TypeName="Models.SkillOfPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SKILL_OF_PUPIL()
            columnMap.Add("SkillOfPupilID", "SKILL_OF_PUPIL_ID");
            columnMap.Add("SkillOfClassID", "SKILL_OF_CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CommentOfTeacher", "COMMENT_OF_TEACHER");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("IsPass", "IS_PASS");
            columnMap.Add("Prize", "PRIZE");
            columnMap.Add("CommentOfParent", "COMMENT_OF_PARENT");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SkillTitle()
        { // TypeName="Models.SkillTitle()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SKILL_TITLE()
            columnMap.Add("SkillTitleID", "SKILL_TITLE_ID");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SkillTitleClass()
        { // TypeName="Models.SkillTitleClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SKILL_TITLE_CLASS()
            columnMap.Add("SkillTitleClassID", "SKILL_TITLE_CLASS_ID");
            columnMap.Add("SkillTitleID", "SKILL_TITLE_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SMSCommunication()
        { // TypeName="Models.SMSCommunication()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSCOMMUNICATION()
            columnMap.Add("SMSCommunicationID", "SMSCOMMUNICATION_ID");
            columnMap.Add("HistorySMSID", "HISTORY_SMSID");
            columnMap.Add("ContactGroupID", "CONTACT_GROUP_ID");
            columnMap.Add("SenderGroup", "SENDER_GROUP");
            columnMap.Add("Receiver", "RECEIVER");
            columnMap.Add("Sender", "SENDER");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsAdmin", "IS_ADMIN");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("IsRead", "IS_READ");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("LogMOID", "LOG_MOID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SMSCommunicationGroup()
        { // TypeName="Models.SMSCommunicationGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSCOMMUNICATION_GROUP()
            columnMap.Add("SMSCommunicationGroupID", "SMSCOMMUNICATION_GROUP_ID");
            columnMap.Add("SMSCommunicationID", "SMSCOMMUNICATION_ID");
            columnMap.Add("ContactGroupID", "CONTACT_GROUP_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("PupilProfileID", "PUPIL_PROFILE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsAdmin", "IS_ADMIN");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("UpdateDate", "UPDATE_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SMSConfirmation()
        { // TypeName="Models.SMSConfirmation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSCONFIRMATION()
            columnMap.Add("ConfirmationID", "CONFIRMATION_ID");
            columnMap.Add("LogMOID", "LOG_MOID");
            columnMap.Add("PhoneNumber", "PHONE_NUMBER");
            columnMap.Add("ConfirmType", "CONFIRM_TYPE");
            columnMap.Add("ConfirmValue", "CONFIRM_VALUE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("ConfirmCount", "CONFIRM_COUNT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("UpdateDate", "UPDATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> SMSConfirmationDetail()
        { // TypeName="Models.SMSConfirmationDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSCONFIRMATION_DETAIL()
            columnMap.Add("ConfirmationDetailID", "CONFIRMATION_DETAIL_ID");
            columnMap.Add("ConfirmationID", "CONFIRMATION_ID");
            columnMap.Add("ConfirmValue", "CONFIRM_VALUE");
            columnMap.Add("ConfirmObjectID", "CONFIRM_OBJECT_ID");
            columnMap.Add("ConfirmDescription", "CONFIRM_DESCRIPTION");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> SMSParentContract()
        { // TypeName="Models.SMSParentContract()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSPARENT_CONTRACT()
            columnMap.Add("SMSParentContractID", "SMSPARENT_CONTRACT_ID");
            columnMap.Add("ContractCode", "CONTRACT_CODE");
            columnMap.Add("SchoolCode", "SCHOOL_CODE");
            columnMap.Add("Phone", "PHONE");
            columnMap.Add("ContractorName", "CONTRACTOR_NAME");
            columnMap.Add("ContractorRelationship", "CONTRACTOR_RELATIONSHIP");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedTime", "CREATED_TIME");
            columnMap.Add("UpdatedTime", "UPDATED_TIME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SMSParentNumSMSSent()
        { // TypeName="Models.SMSParentNumSMSSent()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSPARENT_NUM_SMSSENT()
            columnMap.Add("SMSParentNumSMSSentID", "SMSPARENT_NUM_SMSSENT_ID");
            columnMap.Add("SMSParentSubscriberID", "SMSPARENT_SUBSCRIBER_ID");
            columnMap.Add("MarkDay", "MARK_DAY");
            columnMap.Add("MarkMonth", "MARK_MONTH");
            columnMap.Add("MarkSemester", "MARK_SEMESTER");
            columnMap.Add("MarkYear", "MARK_YEAR");
            columnMap.Add("DiligenceDay", "DILIGENCE_DAY");
            columnMap.Add("DiligenceMonth", "DILIGENCE_MONTH");
            columnMap.Add("DiligenceSemester", "DILIGENCE_SEMESTER");
            columnMap.Add("DiligenceYear", "DILIGENCE_YEAR");
            columnMap.Add("Notification", "NOTIFICATION");
            columnMap.Add("CreatedTime", "CREATED_TIME");
            columnMap.Add("UpdatedTime", "UPDATED_TIME");
            columnMap.Add("MarkWeek", "MARK_WEEK");
            columnMap.Add("DiligenceWeek", "DILIGENCE_WEEK");
            columnMap.Add("OtherMessage", "OTHER_MESSAGE");
            columnMap.Add("IsExpired", "IS_EXPIRED");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SMSParentSubscriber()
        { // TypeName="Models.SMSParentSubscriber()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSPARENT_SUBSCRIBER()
            columnMap.Add("SMSParentSubscriberID", "SMSPARENT_SUBSCRIBER_ID");
            columnMap.Add("SMSParentContractID", "SMSPARENT_CONTRACT_ID");
            columnMap.Add("Subscriber", "SUBSCRIBER");
            columnMap.Add("SubscriberType", "SUBSCRIBER_TYPE");
            columnMap.Add("PackageID", "PACKAGE_ID");
            columnMap.Add("SubscriptionTime", "SUBSCRIPTION_TIME");
            columnMap.Add("TypeOfSubcriptionTime", "TYPE_OF_SUBCRIPTION_TIME");
            columnMap.Add("SubscriptionStatus", "SUBSCRIPTION_STATUS");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("Money", "MONEY");
            columnMap.Add("RegistrationType", "REGISTRATION_TYPE");
            columnMap.Add("CreatedTime", "CREATED_TIME");
            columnMap.Add("UpdatedTime", "UPDATED_TIME");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("SubscriberName", "SUBSCRIBER_NAME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SMSTeacherContract()
        { // TypeName="Models.SMSTeacherContract()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSTEACHER_CONTRACT()
            columnMap.Add("SMSTeacherContractID", "SMSTEACHER_CONTRACT_ID");
            columnMap.Add("ContractCode", "CONTRACT_CODE");
            columnMap.Add("PackageID", "PACKAGE_ID");
            columnMap.Add("SchoolCode", "SCHOOL_CODE");
            columnMap.Add("SupervisingDeptCode", "SUPERVISING_DEPT_CODE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("MaxSMS", "MAX_SMS");
            columnMap.Add("CreateUser", "CREATE_USER");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateUser", "UPDATE_USER");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            return columnMap;

        }
        public IDictionary<string, object> SMSType()
        { // TypeName="Models.SMSType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSTYPE()
            columnMap.Add("TypeID", "TYPE_ID");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("DefaultTemplate", "DEFAULT_TEMPLATE");
            columnMap.Add("TypeCode", "TYPE_CODE");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Periodic", "PERIODIC");
            columnMap.Add("DefaultSendTime", "DEFAULT_SEND_TIME");
            columnMap.Add("DefaultIsAutosend", "DEFAULT_IS_AUTOSEND");
            columnMap.Add("DefaultIsLock", "DEFAULT_IS_LOCK");
            columnMap.Add("DefaultSendBefore", "DEFAULT_SEND_BEFORE");
            columnMap.Add("IsAllowAutoSend", "IS_ALLOW_AUTO_SEND");
            columnMap.Add("IsAutoSchoolQuota", "IS_AUTO_SCHOOL_QUOTA");
            return columnMap;

        }
        public IDictionary<string, object> SMSTypeDetail()
        { // TypeName="Models.SMSTypeDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SMSTYPE_DETAIL()
            columnMap.Add("SMSTypeDetailID", "SMSTYPE_DETAIL_ID");
            columnMap.Add("TypeID", "TYPE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsLock", "IS_LOCK");
            columnMap.Add("IsAutoSend", "IS_AUTO_SEND");
            columnMap.Add("SendTime", "SEND_TIME");
            columnMap.Add("Template", "TEMPLATE");
            columnMap.Add("SendBefore", "SEND_BEFORE");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SpecialityCat()
        { // TypeName="Models.SpecialityCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SPECIALITY_CAT()
            columnMap.Add("SpecialityCatID", "SPECIALITY_CAT_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SpineTest()
        { // TypeName="Models.SpineTest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SPINE_TEST()
            columnMap.Add("SpineTestID", "SPINE_TEST_ID");
            columnMap.Add("MonitoringBookID", "MONITORING_BOOK_ID");
            columnMap.Add("IsDeformityOfTheSpine", "IS_DEFORMITY_OF_THE_SPINE");
            columnMap.Add("ExaminationStraight", "EXAMINATION_STRAIGHT");
            columnMap.Add("ExaminationItalic", "EXAMINATION_ITALIC");
            columnMap.Add("Other", "OTHER");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> StaffPosition()
        { // TypeName="Models.StaffPosition()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STAFF_POSITION()
            columnMap.Add("StaffPositionID", "STAFF_POSITION_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> StateManagementGrade()
        { // TypeName="Models.StateManagementGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATE_MANAGEMENT_GRADE()
            columnMap.Add("StateManagementGradeID", "STATE_MANAGEMENT_GRADE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> StatisticLevelConfig()
        { // TypeName="Models.StatisticLevelConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_LEVEL_CONFIG()
            columnMap.Add("StatisticLevelConfigID", "STATISTIC_LEVEL_CONFIG_ID");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("MinValue", "MIN_VALUE");
            columnMap.Add("MaxValue", "MAX_VALUE");
            columnMap.Add("SignMax", "SIGN_MAX");
            columnMap.Add("SignMin", "SIGN_MIN");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("StatisticLevelReportID", "STATISTIC_LEVEL_REPORT_ID");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("CreateDate", "CREATE_DATE");
            return columnMap;

        }
        public IDictionary<string, object> StatisticLevelReport()
        { // TypeName="Models.StatisticLevelReport()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_LEVEL_REPORT()
            columnMap.Add("StatisticLevelReportID", "STATISTIC_LEVEL_REPORT_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            return columnMap;

        }
        public IDictionary<string, object> StatisticOfTertiary()
        { // TypeName="Models.StatisticOfTertiary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_OF_TERTIARY()
            columnMap.Add("StatisticOfTertiaryID", "STATISTIC_OF_TERTIARY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilFromSecondary", "PUPIL_FROM_SECONDARY");
            columnMap.Add("PupilOfOther", "PUPIL_OF_OTHER");
            columnMap.Add("ComputerRoom", "COMPUTER_ROOM");
            columnMap.Add("NumberOfComputer", "NUMBER_OF_COMPUTER");
            columnMap.Add("NumberOfComputerInternetConnected", "NUMBER_OF_COMPUTER_INT_CON");
            columnMap.Add("HasWebsite", "HAS_WEBSITE");
            columnMap.Add("HasDatabase", "HAS_DATABASE");
            columnMap.Add("LearningRoom", "LEARNING_ROOM");
            columnMap.Add("NumOfStandard", "NUM_OF_STANDARD");
            columnMap.Add("NumOfOverStandard", "NUM_OF_OVER_STANDARD");
            columnMap.Add("NumOfUnderStandard", "NUM_OF_UNDER_STANDARD");
            columnMap.Add("NumOfStaff", "NUM_OF_STAFF");
            columnMap.Add("SentDate", "SENT_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> StatisticSubcommitteeOfTertiary()
        { // TypeName="Models.StatisticSubcommitteeOfTertiary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_SUBCOMMITTEE_OF_TER()
            columnMap.Add("StatisticSubcommitteeOfTertiaryID", "STATISTIC_SUB_OF_TER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubCommitteeID", "SUB_COMMITTEE_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("TotalClass", "TOTAL_CLASS");
            columnMap.Add("TotalPupil", "TOTAL_PUPIL");
            columnMap.Add("SpecicalType", "SPECICAL_TYPE");
            columnMap.Add("SentDate", "SENT_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> StatisticSubjectOfTertiary()
        { // TypeName="Models.StatisticSubjectOfTertiary()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_SUBJECT_OF_TERTIARY()
            columnMap.Add("StatisticSubjectOfTertiaryID", "STATISTIC_SUBJECT_OF_TER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("TotalTeacher", "TOTAL_TEACHER");
            columnMap.Add("SpecicalType", "SPECICAL_TYPE");
            columnMap.Add("SentDate", "SENT_DATE");
            columnMap.Add("M_SubjectName", "M_SUBJECT_NAME");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> StatisticTertiaryStartYear()
        { // TypeName="Models.StatisticTertiaryStartYear()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTIC_TERTIARY_START_YEAR()
            columnMap.Add("StatisticTerStartID", "STATISTIC_TER_START_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ReportStatus", "REPORT_STATUS");
            columnMap.Add("ReportDate", "REPORT_DATE");
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> StatisticsConfig()
        { // TypeName="Models.StatisticsConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STATISTICS_CONFIG()
            columnMap.Add("StatisticsConfigID", "STATISTICS_CONFIG_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("MarkType", "MARK_TYPE");
            columnMap.Add("MappedColumn", "MAPPED_COLUMN");
            columnMap.Add("DisplayName", "DISPLAY_NAME");
            columnMap.Add("MinValue", "MIN_VALUE");
            columnMap.Add("MaxValue", "MAX_VALUE");
            columnMap.Add("EqualValue", "EQUAL_VALUE");
            columnMap.Add("DisplayOrder", "DISPLAY_ORDER");
            columnMap.Add("ReportType", "REPORT_TYPE");
            columnMap.Add("SubjectType", "SUBJECT_TYPE");
            return columnMap;

        }
        public IDictionary<string, object> StudyingJudgement()
        { // TypeName="Models.StudyingJudgement()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="STUDYING_JUDGEMENT()
            columnMap.Add("StudyingJudgementID", "STUDYING_JUDGEMENT_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> SubCommittee()
        { // TypeName="Models.SubCommittee()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUB_COMMITTEE()
            columnMap.Add("SubCommitteeID", "SUB_COMMITTEE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Abbreviation", "ABBREVIATION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SubjectCat()
        { // TypeName="Models.SubjectCat()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUBJECT_CAT()
            columnMap.Add("SubjectCatID", "SUBJECT_CAT_ID");
            columnMap.Add("SubjectName", "SUBJECT_NAME");
            columnMap.Add("Abbreviation", "ABBREVIATION");
            columnMap.Add("DisplayName", "DISPLAY_NAME");
            columnMap.Add("MiddleSemesterTest", "MIDDLE_SEMESTER_TEST");
            columnMap.Add("IsExemptible", "IS_EXEMPTIBLE");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("HasPractice", "HAS_PRACTICE");
            columnMap.Add("IsForeignLanguage", "IS_FOREIGN_LANGUAGE");
            columnMap.Add("IsCoreSubject", "IS_CORE_SUBJECT");
            columnMap.Add("IsApprenticeshipSubject", "IS_APPRENTICESHIP_SUBJECT");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("ApprenticeshipGroupID", "APPRENTICESHIP_GROUP_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("OrderInSubject", "ORDER_IN_SUBJECT");
            columnMap.Add("IsEditIsCommentting", "IS_EDIT_IS_COMMENTTING");
            columnMap.Add("ReadAndWriteMiddleTest", "READ_AND_WRITE_MIDDLE_TEST");
            columnMap.Add("Color", "COLOR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Code", "CODE");
            return columnMap;

        }
        public IDictionary<string, object> SummedUpRecord()
        { // TypeName="Models.SummedUpRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_UP_RECORD()
            columnMap.Add("SummedUpRecordID", "SUMMED_UP_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("SummedUpMark", "SUMMED_UP_MARK");
            columnMap.Add("JudgementResult", "JUDGEMENT_RESULT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("SummedUpDate", "SUMMED_UP_DATE");
            columnMap.Add("ReTestMark", "RE_TEST_MARK");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            return columnMap;

        }
        public IDictionary<string, object> SummedUpRecordClass()
        { // TypeName="Models.SummedUpRecordClass()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_UP_RECORD_CLASS()
            columnMap.Add("SummedUpRecordClassID", "SUMMED_UP_RECORD_CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("NumberOfPupil", "NUMBER_OF_PUPIL");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> SummedUpRecordHistory()
        { // TypeName="Models.SummedUpRecordHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_UP_RECORD_HISTORY()
            columnMap.Add("SummedUpRecordID", "SUMMED_UP_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("SummedUpMark", "SUMMED_UP_MARK");
            columnMap.Add("JudgementResult", "JUDGEMENT_RESULT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("SummedUpDate", "SUMMED_UP_DATE");
            columnMap.Add("ReTestMark", "RE_TEST_MARK");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            return columnMap;

        }
        public IDictionary<string, object> SupervisingDept()
        { // TypeName="Models.SupervisingDept()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUPERVISING_DEPT()
            columnMap.Add("SupervisingDeptID", "SUPERVISING_DEPT_ID");
            columnMap.Add("SupervisingDeptCode", "SUPERVISING_DEPT_CODE");
            columnMap.Add("SupervisingDeptName", "SUPERVISING_DEPT_NAME");
            columnMap.Add("ParentID", "PARENT_ID");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("HierachyLevel", "HIERACHY_LEVEL");
            columnMap.Add("AreaID", "AREA_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("Address", "ADDRESS");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Fax", "FAX");
            columnMap.Add("Website", "WEBSITE");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("TraversalPath", "TRAVERSAL_PATH");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("AdminID", "ADMIN_ID");
            columnMap.Add("IsActiveSMAS", "IS_ACTIVE_SMAS");
            columnMap.Add("SMSActiveType", "SMSACTIVE_TYPE");
            columnMap.Add("M_OldAdminID", "M_OLD_ADMIN_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_GUID", "M_GUID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("SupervisingDeptExamCode", "SUPERVISING_DEPT_EXAM_CODE");
            return columnMap;

        }
        public IDictionary<string, object> Supplier()
        { // TypeName="Models.Supplier()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUPPLIER()
            columnMap.Add("SupplierID", "SUPPLIER_ID");
            columnMap.Add("SupplierName", "SUPPLIER_NAME");
            columnMap.Add("Address", "ADDRESS");
            columnMap.Add("Phone", "PHONE");
            columnMap.Add("Shipper", "SHIPPER");
            columnMap.Add("ShipperPhone", "SHIPPER_PHONE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> SymbolConfig()
        { // TypeName="Models.SymbolConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SYMBOL_CONFIG()
            columnMap.Add("SymbolConfigID", "SYMBOL_CONFIG_ID");
            columnMap.Add("SymbolConfigName", "SYMBOL_CONFIG_NAME");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Synchronize()
        { // TypeName="Models.Synchronize()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SYNCHRONIZE()
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("NumberOfRule", "NUMBER_OF_RULE");
            columnMap.Add("OracleSource", "ORACLE_SOURCE");
            columnMap.Add("SqlSource", "SQL_SOURCE");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Type", "TYPE");
            return columnMap;

        }
        public IDictionary<string, object> SystemParameter()
        { // TypeName="Models.SystemParameter()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SYSTEM_PARAMETER()
            columnMap.Add("SystemParameterID", "SYSTEM_PARAMETER_ID");
            columnMap.Add("ParameterName", "PARAMETER_NAME");
            columnMap.Add("KeyName", "KEY_NAME");
            columnMap.Add("KeyValue", "KEY_VALUE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> TeacherGrade()
        { // TypeName="Models.TeacherGrade()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHER_GRADE()
            columnMap.Add("TeacherGradeID", "TEACHER_GRADE_ID");
            columnMap.Add("GradeResolution", "GRADE_RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeacherGrading()
        { // TypeName="Models.TeacherGrading()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHER_GRADING()
            columnMap.Add("TeacherGradingID", "TEACHER_GRADING_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SchoolFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("TeacherGradeID", "TEACHER_GRADE_ID");
            columnMap.Add("GradingDate", "GRADING_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeacherNoteBookMonth()
        { // TypeName="Models.TeacherNoteBookMonth()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHER_NOTEBOOK_MONTH()
            columnMap.Add("TeacherNoteBookMonthID", "TEACHER_NOTEBOOK_MONTH_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MonthID", "MONTH_ID");
            columnMap.Add("CommentSubject", "COMMENT_SUBJECT");
            columnMap.Add("CommentCQ", "COMMENT_CQ");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> TeacherNoteBookSemester()
        { // TypeName="Models.TeacherNoteBookSemester()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHER_NOTEBOOK_SEMESTER()
            columnMap.Add("TeacherNoteBookSemesterID", "TEACHER_NOTEBOOK_SEMESTER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("PERIODIC_SCORE_MIDDLE", "PERIODIC_SCORE_MIDDLE");
            columnMap.Add("PERIODIC_SCORE_END", "PERIODIC_SCORE_END");
            columnMap.Add("PERIODIC_SCORE_MIDDLE_JUDGE", "PERIODIC_SCORE_MIDDLE_JUDGE");
            columnMap.Add("PERIODIC_SCORE_END_JUDGLE", "PERIODIC_SCORE_END_JUDGLE");
            columnMap.Add("AVERAGE_MARK", "AVERAGE_MARK");
            columnMap.Add("AVERAGE_MARK_JUDGE", "AVERAGE_MARK_JUDGE");
            columnMap.Add("Rate", "RATE");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> TeacherOfFaculty()
        { // TypeName="Models.TeacherOfFaculty()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHER_OF_FACULTY()
            columnMap.Add("TeacherOfFacultyID", "TEACHER_OF_FACULTY_ID");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("IsLeader", "IS_LEADER");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeachingAssignment()
        { // TypeName="Models.TeachingAssignment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_ASSIGNMENT()
            columnMap.Add("TeachingAssignmentID", "TEACHING_ASSIGNMENT_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeachingExperience()
        { // TypeName="Models.TeachingExperience()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_EXPERIENCE()
            columnMap.Add("TeachingExperienceID", "TEACHING_EXPERIENCE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("FacultyID", "FACULTY_ID");
            columnMap.Add("ExperienceTypeID", "EXPERIENCE_TYPE_ID");
            columnMap.Add("RegisteredLevel", "REGISTERED_LEVEL");
            columnMap.Add("Grade", "GRADE");
            columnMap.Add("RegisteredDate", "REGISTERED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("M_OldTeacherID", "M_OLD_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TeachingPeriod()
        { // TypeName="Models.TeachingPeriod()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_PERIOD()
            columnMap.Add("TeachingPeriodID", "TEACHING_PERIOD_ID");
            columnMap.Add("SchoolTypeID", "SCHOOL_TYPE_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("Lesson", "LESSON");
            columnMap.Add("DecreaseLessonHead", "DECREASE_LESSON_HEAD");
            columnMap.Add("DecreaseLessonConcurrent", "DECREASE_LESSON_CONCURRENT");
            return columnMap;

        }
        public IDictionary<string, object> TeachingRegistration()
        { // TypeName="Models.TeachingRegistration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_REGISTRATION()
            columnMap.Add("TeachingRegistrationID", "TEACHING_REGISTRATION_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("WeekOfYear", "WEEK_OF_YEAR");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("Section", "SECTION");
            columnMap.Add("StartPeriod", "START_PERIOD");
            columnMap.Add("EndPeriod", "END_PERIOD");
            columnMap.Add("LessionName", "LESSION_NAME");
            columnMap.Add("TrainingTool", "TRAINING_TOOL");
            columnMap.Add("IsSubstitued", "IS_SUBSTITUED");
            columnMap.Add("RegisteredDate", "REGISTERED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> TeachingTargetRegistration()
        { // TypeName="Models.TeachingTargetRegistration()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEACHING_TARGET_REGISTRATION()
            columnMap.Add("TeachingTargetRegistrationID", "TEACHING_TARGET_REG_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("ExcellentGrade", "EXCELLENT_GRADE");
            columnMap.Add("GoodGrade", "GOOD_GRADE");
            columnMap.Add("FairGrade", "FAIR_GRADE");
            columnMap.Add("WeakGrade", "WEAK_GRADE");
            columnMap.Add("RegisteredDate", "REGISTERED_DATE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> TestGroupuserxmpp()
        { // TypeName="Models.TestGroupuserxmpp()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEST_GROUPUSERXMPP()
            columnMap.Add("Groupuserid", "GROUPUSERID");
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Groupname", "GROUPNAME");
            return columnMap;

        }
        public IDictionary<string, object> TestGroupxmpp()
        { // TypeName="Models.TestGroupxmpp()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEST_GROUPXMPP()
            columnMap.Add("Groupid", "GROUPID");
            columnMap.Add("Groupname", "GROUPNAME");
            columnMap.Add("Groupdescription", "GROUPDESCRIPTION");
            return columnMap;

        }
        public IDictionary<string, object> TestUserxmpp()
        { // TypeName="Models.TestUserxmpp()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEST_USERXMPP()
            columnMap.Add("UserName", "USERNAME");
            columnMap.Add("Password", "PASSWORD");
            columnMap.Add("Name", "NAME");
            columnMap.Add("Email", "EMAIL");
            return columnMap;

        }
        public IDictionary<string, object> ThreadMark()
        { // TypeName="Models.ThreadMark()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="THREAD_MARK()
            columnMap.Add("ThreadMarkID", "THREAD_MARK_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("StatusResult", "STATUS_RESULT");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("NumberScan", "NUMBER_SCAN");
            return columnMap;

        }
        public IDictionary<string, object> TrainingLevel()
        { // TypeName="Models.TrainingLevel()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TRAINING_LEVEL()
            columnMap.Add("TrainingLevelID", "TRAINING_LEVEL_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TrainingType()
        { // TypeName="Models.TrainingType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TRAINING_TYPE()
            columnMap.Add("TrainingTypeID", "TRAINING_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TypeConfig()
        { // TypeName="Models.TypeConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TYPE_CONFIG()
            columnMap.Add("TypeConfigID", "TYPE_CONFIG_ID");
            columnMap.Add("TypeConfigName", "TYPE_CONFIG_NAME");
            columnMap.Add("PositionOrder", "POSITION_ORDER");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> TypeOfDish()
        { // TypeName="Models.TypeOfDish()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TYPE_OF_DISH()
            columnMap.Add("TypeOfDishID", "TYPE_OF_DISH_ID");
            columnMap.Add("TypeOfDishName", "TYPE_OF_DISH_NAME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> TypeOfFood()
        { // TypeName="Models.TypeOfFood()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TYPE_OF_FOOD()
            columnMap.Add("TypeOfFoodID", "TYPE_OF_FOOD_ID");
            columnMap.Add("TypeOfFoodName", "TYPE_OF_FOOD_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            return columnMap;

        }
        public IDictionary<string, object> UserAccount()
        { // TypeName="Models.UserAccount()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="USER_ACCOUNT()
            //columnMap.Add("UserAccountID", "USER_ACCOUNT_ID");
            columnMap.Add("GUID", "GUID");
            columnMap.Add("EmployeeID", "EMPLOYEE_ID");
            columnMap.Add("CreatedUserID", "CREATED_USER_ID");
            columnMap.Add("IsAdmin", "IS_ADMIN");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsOperationalUser", "IS_OPERATIONAL_USER");
            columnMap.Add("FirstLoginDate", "FIRST_LOGIN_DATE");
            columnMap.Add("LoginFalseCount", "LOGIN_FALSE_COUNT");
            columnMap.Add("M_SuperUserID", "M_SUPER_USER_ID");
            columnMap.Add("M_TeacherID", "M_TEACHER_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("SelectLevel", "SELECT_LEVEL");
            columnMap.Add("IsParent", "IS_PARENT");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> UserGroup()
        { // TypeName="Models.UserGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="USER_GROUP()
            columnMap.Add("UserGroupID", "USER_GROUP_ID");
            columnMap.Add("UserID", "USER_ID");
            columnMap.Add("GroupID", "GROUP_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_GUID", "M_GUID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("MOldGroupID", "M_OLD_GROUP_ID");
            columnMap.Add("MRoleID", "M_ROLE_ID");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> UserProvince()
        { // TypeName="Models.UserProvince()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="USER_PROVINCE()
            columnMap.Add("UserProvinceID", "USER_PROVINCE_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("UserAccountID", "USER_ACCOUNT_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> UserRole()
        { // TypeName="Models.UserRole()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="USER_ROLE()
            columnMap.Add("UserRoleID", "USER_ROLE_ID");
            columnMap.Add("UserID", "USER_ID");
            columnMap.Add("RoleID", "ROLE_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_GUID", "M_GUID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Village()
        { // TypeName="Models.Village()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="VILLAGE()
            columnMap.Add("VillageID", "VILLAGE_ID");
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("VillageCode", "VILLAGE_CODE");
            columnMap.Add("VillageName", "VILLAGE_NAME");
            columnMap.Add("ShortName", "SHORT_NAME");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            return columnMap;

        }
        public IDictionary<string, object> ViolatedCandidate()
        { // TypeName="Models.ViolatedCandidate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="VIOLATED_CANDIDATE()
            columnMap.Add("ViolatedCandidateID", "VIOLATED_CANDIDATE_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("CandidateID", "CANDIDATE_ID");
            columnMap.Add("ExamViolationTypeID", "EXAM_VIOLATION_TYPE_ID");
            columnMap.Add("ViolationDetail", "VIOLATION_DETAIL");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("RoomID", "ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("NameListCode", "NAME_LIST_CODE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ViolatedInvigilator()
        { // TypeName="Models.ViolatedInvigilator()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="VIOLATED_INVIGILATOR()
            columnMap.Add("ViolatedInvigilatorID", "VIOLATED_INVIGILATOR_ID");
            columnMap.Add("ExaminationID", "EXAMINATION_ID");
            columnMap.Add("InvigilatorID", "INVIGILATOR_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("RoomID", "ROOM_ID");
            columnMap.Add("DisciplinedForm", "DISCIPLINED_FORM");
            columnMap.Add("ViolationDetail", "VIOLATION_DETAIL");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MInvigilatorAssignmentID", "M_INVIGILATOR_ASSIGNMENT_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> WeeklyMenu()
        { // TypeName="Models.WeeklyMenu()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WEEKLY_MENU()
            columnMap.Add("WeeklyMenuID", "WEEKLY_MENU_ID");
            columnMap.Add("WeeklyMenuCode", "WEEKLY_MENU_CODE");
            columnMap.Add("MenuType", "MENU_TYPE");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("StartDate", "START_DATE");
            columnMap.Add("EatingGroupID", "EATING_GROUP_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> WeeklyMenuDetail()
        { // TypeName="Models.WeeklyMenuDetail()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WEEKLY_MENU_DETAIL()
            columnMap.Add("WeeklyMenuDetailID", "WEEKLY_MENU_DETAIL_ID");
            columnMap.Add("DayOfWeek", "DAY_OF_WEEK");
            columnMap.Add("DailyMenuID", "DAILY_MENU_ID");
            columnMap.Add("WeeklyMenuID", "WEEKLY_MENU_ID");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            return columnMap;

        }
        public IDictionary<string, object> WorkFlow()
        { // TypeName="Models.WorkFlow()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WORK_FLOW()
            columnMap.Add("WorkFlowID", "WORK_FLOW_ID");
            columnMap.Add("WorkFlowName", "WORK_FLOW_NAME");
            columnMap.Add("ParentID", "PARENT_ID");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("Url", "URL");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("ProductVersion", "PRODUCT_VERSION");
            columnMap.Add("ViewName", "VIEW_NAME");
            columnMap.Add("WorkFlowContent", "WORK_FLOW_CONTENT");
            columnMap.Add("OrderNo", "ORDER_NO");
            columnMap.Add("ConfigAction", "CONFIG_ACTION");
            columnMap.Add("AvailableAdmin", "AVAILABLE_ADMIN");
            columnMap.Add("AvailableTeacher", "AVAILABLE_TEACHER");
            columnMap.Add("AvailableSup", "AVAILABLE_SUP");
            return columnMap;

        }
        public IDictionary<string, object> WorkFlowUser()
        { // TypeName="Models.WorkFlowUser()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WORK_FLOW_USER()
            columnMap.Add("WorkFlowUserID", "WORK_FLOW_USER_ID");
            columnMap.Add("WorkFlowID", "WORK_FLOW_ID");
            columnMap.Add("UserID", "USER_ID");
            columnMap.Add("OrderID", "ORDER_ID");
            columnMap.Add("IsPopUp", "IS_POPUP");
            columnMap.Add("IsAutoAlert", "IS_AUTO_ALERT");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("DefaultTab", "DEFAULT_TAB");
            return columnMap;

        }
        public IDictionary<string, object> WorkGroupType()
        { // TypeName="Models.WorkGroupType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WORK_GROUP_TYPE()
            columnMap.Add("WorkGroupTypeID", "WORK_GROUP_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> WorkType()
        { // TypeName="Models.WorkType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="WORK_TYPE()
            columnMap.Add("WorkTypeID", "WORK_TYPE_ID");
            columnMap.Add("Resolution", "RESOLUTION");
            columnMap.Add("WorkGroupTypeID", "WORK_GROUP_TYPE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ThreadMovedData()
        { // TypeName="Models.ThreadMovedData()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="THREAD_MOVED_DATA()
            columnMap.Add("ThreadMovedDataID", "THREAD_MOVED_DATA_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("MoveAcademicYearID", "MOVE_ACADEMIC_YEAR_ID");
            columnMap.Add("MoveYear", "MOVE_YEAR");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("NodeId", "NODE_ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("NumberScan", "NUMBER_SCAN");
            return columnMap;

        }

        public IDictionary<string, object> VJudgeRecord()
        { // TypeName="Models.VJudgeRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="V_JUDGE_RECORD()
            columnMap.Add("JudgeRecordID", "JUDGE_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Judgement", "JUDGEMENT");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldJudgement", "OLD_JUDGEMENT");
            columnMap.Add("MJudgement", "M_JUDGEMENT");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> VMarkRecord()
        { // TypeName="Models.VMarkRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="V_MARK_RECORD()
            columnMap.Add("MarkRecordID", "MARK_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("MarkTypeID", "MARK_TYPE_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Title", "TITLE");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("MarkedDate", "MARKED_DATE");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("M_IsByC", "M_IS_BY_C");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("IsSMS", "IS_SMS");
            columnMap.Add("OldMark", "OLD_MARK");
            columnMap.Add("M_MarkRecord", "M_MARK_RECORD");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("LogChange", "LOG_CHANGE");
            return columnMap;

        }
        public IDictionary<string, object> VPupilRanking()
        { // TypeName="Models.VPupilRanking()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="V_PUPIL_RANKING()
            columnMap.Add("PupilRankingID", "PUPIL_RANKING_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("AverageMark", "AVERAGE_MARK");
            columnMap.Add("TotalAbsentDaysWithPermission", "TOTAL_ABSENT_DAYS_WITH_PER");
            columnMap.Add("TotalAbsentDaysWithoutPermission", "TOTAL_ABSENT_DAYS_WITHOUT_PER");
            columnMap.Add("CapacityLevelID", "CAPACITY_LEVEL_ID");
            columnMap.Add("ConductLevelID", "CONDUCT_LEVEL_ID");
            columnMap.Add("Rank", "RANK");
            columnMap.Add("RankingDate", "RANKING_DATE");
            columnMap.Add("StudyingJudgementID", "STUDYING_JUDGEMENT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("IsCategory", "IS_CATEGORY");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilRankingComment", "PUPIL_RANKING_COMMENT");
            return columnMap;

        }
        public IDictionary<string, object> VSummedUpRecord()
        { // TypeName="Models.VSummedUpRecord()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="V_SUMMED_UP_RECORD()
            columnMap.Add("SummedUpRecordID", "SUMMED_UP_RECORD_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("IsCommenting", "IS_COMMENTING");
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("PeriodID", "PERIOD_ID");
            columnMap.Add("SummedUpMark", "SUMMED_UP_MARK");
            columnMap.Add("JudgementResult", "JUDGEMENT_RESULT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("SummedUpDate", "SUMMED_UP_DATE");
            columnMap.Add("ReTestMark", "RE_TEST_MARK");
            columnMap.Add("ReTestJudgement", "RE_TEST_JUDGEMENT");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsOldData", "IS_OLD_DATA");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("Year", "YEAR");
            return columnMap;

        }

        public IDictionary<string, object> Evaluations1()
        { // TypeName="Models.Evaluation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION()
            columnMap.Add("EvaluationName", "EVALUATION_NAME");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationComments()
        { // TypeName="Models.EvaluationComments()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_COMMENTS()
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("UpdateTime", "UPDATETIME");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("NoteUpdate", "NOTE_UPDATE");
            columnMap.Add("NoteInsert", "NOTE_INSERT");
            columnMap.Add("CommentsM5M10", "COMMENTS_M5_M10");
            columnMap.Add("CommentsM4M9", "COMMENTS_M4_M9");
            columnMap.Add("CommentsM3M8", "COMMENTS_M3_M8");
            columnMap.Add("CommentsM2M7", "COMMENTS_M2_M7");
            columnMap.Add("CommentsM1M6", "COMMENTS_M1_M6");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("TypeOfTeacher", "TYPE_OF_TEACHER");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EvaluationCommentsID", "EVALUATION_COMMENTS_ID");
            return columnMap;

        }
        public IDictionary<string, object> RatedCommentPupil()
        { // TypeName="Models.RatedCommentPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="RATED_COMMENT_PUPIL()
            columnMap.Add("RatedCommentPupilID", "RATED_COMMENT_PUPIL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("PeriodicMiddleMark", "PERIODIC_MIDDLE_MARK");
            columnMap.Add("MiddleEvaluation", "MIDDLE_EVALUATION");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("PeriodicMiddleJudgement", "PERIODIC_MIDDLE_JUDGEMENT");
            columnMap.Add("PeriodicEndingJudgement", "PERIODIC_ENDING_JUDGEMENT");
            columnMap.Add("CapacityMiddleEvaluation", "CAPACITY_MIDDLE_EVALUATION");
            columnMap.Add("CapacityEndingEvaluation", "CAPACITY_ENDING_EVALUATION");
            columnMap.Add("QualityMiddleEvaluation", "QUALITY_MIDDLE_EVALUATION");
            columnMap.Add("QualityEndingEvaluation", "QUALITY_ENDING_EVALUATION");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EvaluationRetraning", "EVALUATION_RETRAINING");
            columnMap.Add("FullNameComment", "FULL_NAME_COMMENT");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("LogChangeID", "LOG_CHANGE_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }
        public IDictionary<string, object> RatedCommentPupilHistory()
        { // TypeName="Models.RatedCommentPupilHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="RATED_COMMENT_PUPIL_HISTORY()
            columnMap.Add("RatedCommentPupilHistoryID", "RATED_COMMENT_PUPIL_HISTORY_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("PeriodicMiddleMark", "PERIODIC_MIDDLE_MARK");
            columnMap.Add("MiddleEvaluation", "MIDDLE_EVALUATION");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("PeriodicMiddleJudgement", "PERIODIC_MIDDLE_JUDGEMENT");
            columnMap.Add("PeriodicEndingJudgement", "PERIODIC_ENDING_JUDGEMENT");
            columnMap.Add("CapacityMiddleEvaluation", "CAPACITY_MIDDLE_EVALUATION");
            columnMap.Add("CapacityEndingEvaluation", "CAPACITY_ENDING_EVALUATION");
            columnMap.Add("QualityMiddleEvaluation", "QUALITY_MIDDLE_EVALUATION");
            columnMap.Add("QualityEndingEvaluation", "QUALITY_ENDING_EVALUATION");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EvaluationRetraning", "EVALUATION_RETRAINING");
            columnMap.Add("FullNameComment", "FULL_NAME_COMMENT");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("LogChangeID", "LOG_CHANGE_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }
        public IDictionary<string, object> LockRatedCommentPupil()
        { // TypeName="Models.LockRatedCommentPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOCK_RATED_COMMENT_PUPIL()
            columnMap.Add("LockRatedCommentPupilID", "LOCK_RATED_COMMENT_PUPIL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("LockTitle", "LOCK_TITLE");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }
        public IDictionary<string, object> CollectionComments()
        { // TypeName="Models.CollectionComments()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="COLLECTION_COMMENTS()
            columnMap.Add("CollectionCommentsID", "COLLECTION_COMMENTS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("AccountID", "ACCOUNT_ID");
            columnMap.Add("CommentCode", "COMMENT_CODE");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }
        public IDictionary<string, object> CommentOfPupil()
        { // TypeName="Models.CommentOfPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="COMMENT_OF_PUPIL()
            columnMap.Add("CommentOfPupilID", "COMMENT_OF_PUPIL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("MonthID", "MONTH_ID");
            columnMap.Add("DateTimeComment", "DATETIME_COMMENT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            columnMap.Add("LogChangeID", "LOG_CHANGE_ID");
            return columnMap;

        }
        public IDictionary<string, object> CommentOfPupilHistory()
        { // TypeName="Models.CommentOfPupilHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="COMMENT_OF_PUPIL_HISTORY()
            columnMap.Add("CommentOfPupilHistoryID", "COMMENT_OF_PUPIL_HISTORY_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("MonthID", "MONTH_ID");
            columnMap.Add("DateTimeComment", "DATETIME_COMMENT");
            columnMap.Add("Comment", "COMMENT");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            columnMap.Add("LogChangeID", "LOG_CHANGE_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationReward()
        { // TypeName="Models.EvaluationReward()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_REWARD()
            columnMap.Add("EvaluationRewardID", "EVALUATION_REWARD_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("RewardID", "REWARD_ID");
            columnMap.Add("OrderID", "ORDER_ID");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }

        public IDictionary<string, object> RewardFinal()
        { // TypeName="Models.RewardFinal()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REWARD_FINAL()
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("RewardMode", "REWARD_MODE");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("RewardFinalID", "REWARD_FINAL_ID");
            return columnMap;

        }
        public IDictionary<string, object> UpdateReward()
        { // TypeName="Models.UpdateReward()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="UPDATE_REWARD()
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("Rewards", "REWARDS");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("UpdateRewardID", "UPDATE_REWARD_ID");
            return columnMap;

        }
        public IDictionary<string, object> RewardCommentFinal()
        { // TypeName="Models.RewardCommentFinal()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REWARD_COMMENT_FINAL()
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("OutstandingAchievement", "OUTSTANDING_ACHIEVEMENT");
            columnMap.Add("RewardID", "REWARD_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("RewardCommentFinalID", "REWARD_COMMENT_FINAL_ID");
            return columnMap;

        }
        public IDictionary<string, object> ReviewBookPupil()
        { // TypeName="Models.ReviewBookPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REVIEW_BOOK_PUPIL()
            columnMap.Add("ReviewBookPupilID", "REVIEW_BOOK_PUPIL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("CapacityRate", "CAPACITY_RATE");
            columnMap.Add("CQComment", "CQ_COMMENT");
            columnMap.Add("QualityRate", "QUALITY_RATE");
            columnMap.Add("CommentAdd", "COMMENT_ADD");
            columnMap.Add("RateAndYear", "RATE_ENDYEAR");
            columnMap.Add("RateAdd", "RATE_ADD");
            columnMap.Add("CapacitySummer", "CAPACITY_SUMMER");
            columnMap.Add("QualitySummer", "QUALITY_SUMMER");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATETIME");
            return columnMap;

        }
        public IDictionary<string, object> RegisterSubjectSpecialize()
        { // TypeName="Models.RegisterSubjectSpecialize()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REGISTER_SUBJECT_SPECIALIZE()
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SubjectTypeID", "SUBJECT_TYPE_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("LastDigitYearID", "LAST_DIGIT_YEAR_ID");
            columnMap.Add("RegisterSubjectSpecializeID", "REGISTER_SUBJECT_SPECIALIZE_ID");
            return columnMap;

        }

        public IDictionary<string, object> EvaluationCommentsHistory()
        { // TypeName="Models.EvaluationCommentsHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_COMMENTS_HISTORY()
            columnMap.Add("UpdateTime", "UPDATETIME");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("NoteUpdate", "NOTE_UPDATE");
            columnMap.Add("NoteInsert", "NOTE_INSERT");
            columnMap.Add("CommentsM5M10", "COMMENTS_M5_M10");
            columnMap.Add("CommentsM4M9", "COMMENTS_M4_M9");
            columnMap.Add("CommentsM3M8", "COMMENTS_M3_M8");
            columnMap.Add("CommentsM2M7", "COMMENTS_M2_M7");
            columnMap.Add("CommentsM1M6", "COMMENTS_M1_M6");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("TypeOfTeacher", "TYPE_OF_TEACHER");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EvaluationCommentsID", "EVALUATION_COMMENTS_ID");
            return columnMap;

        }


        public IDictionary<string, object> SummedEndingEvaluation()
        { // TypeName="Models.SummedEndingEvaluation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_ENDING_EVALUATION()
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("RateAdd", "RATE_ADD");
            columnMap.Add("Reward", "REWARD");
            columnMap.Add("SummedEndingEvaluationID", "SUMMED_ENDING_EVALUATION_ID");
            return columnMap;

        }

        public IDictionary<string, object> Template()
        { // TypeName="Models.Template()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="TEMPLATE()
            columnMap.Add("TemplateID", "TEMPLATE_ID");
            columnMap.Add("ReportId", "REPORT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("IsSchool", "IS_SCHOOL");
            columnMap.Add("TemplateCode", "TEMPLATE_CODE");
            columnMap.Add("TemplateName", "TEMPLATE_NAME");
            columnMap.Add("Path", "PATH");
            columnMap.Add("EducationGrade", "EDUCATION_GRADE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> HtmlTemplate()
        { // TypeName="Models.HtmlTemplate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HTML_TEMPLATE()
            columnMap.Add("HtmlTemplateID", "HTML_TEMPLATE_ID");
            columnMap.Add("SheetName", "SHEET_NAME");
            columnMap.Add("TemplateID", "TEMPLATE_ID");
            columnMap.Add("HtmlContent", "HTML_CONTENT");
            columnMap.Add("HtmlContentCodeID", "HTML_CONTENT_CODE_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> HtmlContentCode()
        { // TypeName="Models.HtmlContentCode()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="HTML_CONTENT_CODE()
            columnMap.Add("HtmlContentCodeID", "HTML_CONTENT_CODE_ID");
            columnMap.Add("ContentCode", "CONTENT_CODE");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("ContentName", "CONTENT_NAME");
            columnMap.Add("OrderNumber", "ORDER_NUMBER");
            columnMap.Add("TemplateID", "TEMPLATE_ID");
            return columnMap;

        }

        public IDictionary<string, object> DOETExamGroup()
        { // TypeName="Models.DOETExamGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_EXAM_GROUP()
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamGroupCode", "EXAM_GROUP_CODE");
            columnMap.Add("ExamGroupName", "EXAM_GROUP_NAME");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SyncTime", "SYNC_TIME");
            columnMap.Add("SyncSourceId", "SYNC_SOURCE_ID");
            return columnMap;

        }


        public IDictionary<string, object> VacationReason()
        { // TypeName="Models.VacationReason()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="VACATION_REASON()
            columnMap.Add("VacationReasonID", "VACATION_REASON_ID");
            columnMap.Add("VacationName", "VACATION_NAME");
            columnMap.Add("IsEmployee", "IS_EMPLOYEE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            return columnMap;

        }

        public IDictionary<string, object> DOETExamSubject()
        { // TypeName="Models.DOETExamSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_EXAM_SUBJECT()
            columnMap.Add("ExamSubjectID", "EXAM_SUBJECT_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("DOETSubjectID", "DOET_SUBJECT_ID");
            columnMap.Add("DOETSubjectCode", "DOET_SUBJECT_CODE");
            columnMap.Add("SchedulesExam", "SCHEDULES_EXAM");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SyncTime", "SYNC_TIME");
            columnMap.Add("SyncSourceId", "SYNC_SOURCE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DOETSubject()
        { // TypeName="Models.DOETSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_SUBJECT()
            columnMap.Add("DOETSubjectID", "DOET_SUBJECT_ID");
            columnMap.Add("DOETSubjectName", "DOET_SUBJECT_NAME");
            columnMap.Add("DOETSubjectCode", "DOET_SUBJECT_CODE");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SyncTime", "SYNC_TIME");
            columnMap.Add("SyncSourceId", "SYNC_SOURCE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DOETExaminations()
        { // TypeName="Models.DOETExaminations()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_EXAMINATIONS()
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExaminationsName", "EXAMINATIONS_NAME");
            columnMap.Add("UnitID", "UNIT_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("FromDate", "FROM_DATE");
            columnMap.Add("ToDate", "TO_DATE");
            columnMap.Add("DeadLine", "DEADLINE");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("ExamCouncil", "EXAM_COUNCIL");
            columnMap.Add("AppliedType", "APPLIED_TYPE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("SyncTime", "SYNC_TIME");
            columnMap.Add("SyncSourceId", "SYNC_SOURCE_ID");
            return columnMap;

        }
        public IDictionary<string, object> DOETExamPupil()
        { // TypeName="Models.DOETExamPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_EXAM_PUPIL()
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("UnitID", "UNIT_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ExamineeNumber", "EXAMINEE_NUMBER");
            columnMap.Add("ExamRoom", "EXAM_ROOM");
            columnMap.Add("Location", "LOCATION");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> DOETExamMark()
        { // TypeName="Models.DOETExamMark()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="DOET_EXAM_MARK()
            columnMap.Add("ExamMarkID", "EXAM_MARK_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("DoetSubjectID", "DOET_SUBJECT_ID");
            columnMap.Add("Mark", "MARK");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;

        }
        public IDictionary<string, object> PushNotifyRequest()
        { // TypeName="Models.PushNotifyRequest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="PUSH_NOTIFY_REQUEST()
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("FunctionName", "FUNCTION_NAME");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("IsPupil", "IS_PUPIL");
            columnMap.Add("ShortContent", "SHORT_CONTENT");
            columnMap.Add("Content", "CONTENT");
            columnMap.Add("ContactID", "CONTACT_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PushNotifyRequestID", "PUSH_NOTIFY_REQUEST_ID");
            return columnMap;

        }

        public IDictionary<string, object> ReportType()
        { // TypeName="Models.ReportType()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REPORT_TYPE()
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ReportTypeID", "REPORT_TYPE_ID");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("Code", "CODE");
            return columnMap;

        }

        public IDictionary<string, object> LoginMobile()
        { // TypeName="Models.LoginMobile()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="LOGIN_MOBILE()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SessionID", "SESSION_ID");
            columnMap.Add("DeviceID", "DEVICE_ID");
            columnMap.Add("Username", "USERNAME");
            columnMap.Add("LoginMobileID", "LOGIN_MOBILE_ID");
            return columnMap;

        }

        public IDictionary<string, object> IndicatorData()
        { // TypeName="Models.IndicatorData()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="INDICATOR_DATA()
            columnMap.Add("IndicatorDataID", "INDICATOR_DATA_ID");
            columnMap.Add("IndicatorID", "INDICATOR_ID");
            columnMap.Add("SchoolId", "SCHOOL_ID");
            columnMap.Add("CellValue", "CELL_VALUE");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("LastDigitProvinceID", "LAST_DIGIT_PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationGrade", "EDUCATION_GRADE");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("CreateYear", "CREATE_YEAR");
            return columnMap;

        }



        public IDictionary<string, object> Indicator()
        { // TypeName="Models.Indicator()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="INDICATOR()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("IndicatorID", "INDICATOR_ID");
            columnMap.Add("Formula", "FORMULA");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("HtmlTemplateID", "HTML_TEMPLATE_ID");
            columnMap.Add("CellAddress", "CELL_ADDRESS");
            columnMap.Add("IndicatorCode", "INDICATOR_CODE");
            columnMap.Add("IsSum", "IS_SUM");
            columnMap.Add("MaxLength", "MAX_LENGTH");
            columnMap.Add("MaxValue", "MAX_VALUE");
            columnMap.Add("FormulaValidate", "FORMULA_VALIDATE");
            columnMap.Add("CellCalPrePeriod", "CELL_CAL_PRE_PERIOD");
            columnMap.Add("CellReportLCU", "CELL_REPORT_LCU");
            return columnMap;

        }

        public IDictionary<string, object> ReportRequest()
        { // TypeName="Models.ReportRequest()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="REPORT_REQUEST()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ReportRequestID", "REPORT_REQUEST_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationGrade", "EDUCATION_GRADE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("Year", "YEAR");
            return columnMap;

        }

        public IDictionary<string, object> SchoolPortal()
        { // TypeName="Models.SchoolPortal()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SCHOOL_PORTAL()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("IsSchool", "IS_SCHOOL");
            columnMap.Add("ID", "ID");
            columnMap.Add("SchoolPortalID", "SCHOOL_PORTAL_ID");
            return columnMap;

        }

        public IDictionary<string, object> ApplicationConfig()
        { // TypeName="Models.ApplicationConfig()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="APPLICATION_CONFIG()
            columnMap.Add("ApplicationConfigID", "APPLICATION_CONFIG_ID");
            columnMap.Add("Username", "USERNAME");
            columnMap.Add("SectionID", "SECTION_ID");
            columnMap.Add("RegisID", "REGIS_ID");
            columnMap.Add("ClientID", "CLIENT_ID");
            columnMap.Add("NotifyNewMessage", "NOTIFY_NEW_MESSEGE");
            columnMap.Add("NotifyScheduleChanged", "NOTIFY_SCHEDULE_CHANGED");
            columnMap.Add("NottifySchoolBoyRecordUpdated", "NOTIFY_SCHOOLBOY_RECORDUPDATED");
            columnMap.Add("SendNotifyViaApp", "SEND_NOTIFY_VIA_APP");
            columnMap.Add("SendNotifyViaSMS", "SEND_NOTIFY_VIA_SMS");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("CurrentRole", "CURRENT_ROLE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("CreateDate", "CREATE_DATE");
            columnMap.Add("UpdateDate", "UPDATE_DATE");
            return columnMap;

        }

        public IDictionary<string, object> SummedEvaluation()
        { // TypeName="Models.SummedEvaluation()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_EVALUATION()
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EndingComments", "ENDING_COMMENTS");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SummedEvaluationID", "SUMMED_EVALUATION_ID");
            return columnMap;

        }
        public IDictionary<string, object> SummedEvaluationHistory()
        { // TypeName="Models.SummedEvaluationHistory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="SUMMED_EVALUATION_HISTORY()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EndingComments", "ENDING_COMMENTS");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SummedEvaluationID", "SUMMED_EVALUATION_ID");
            return columnMap;

        }
        public IDictionary<string, object> EvaluationCriterias()
        { // TypeName="Models.EvaluationCriteria()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EVALUATION_CRITERIA()
            columnMap.Add("CriteriaName", "CRITERIA_NAME");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("TypeID", "TYPE_ID");
            columnMap.Add("Note", "NOTE");
            return columnMap;

        }
        public IDictionary<string, object> MonthComments()
        { // TypeName="Models.MonthComments()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="MONTH_COMMENTS()
            columnMap.Add("Semester", "SEMESTER");
            columnMap.Add("Name", "NAME");
            columnMap.Add("MonthID", "MONTH_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamBag()
        { // TypeName="Models.ExamBag()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_BAG()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamBagCode", "EXAM_BAG_CODE");
            columnMap.Add("ExamBagID", "EXAM_BAG_ID");
            return columnMap;

        }

        public IDictionary<string, object> ExamCandenceBag()
        { // TypeName="Models.ExamCandenceBag()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_CANDENCE_BAG()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamCandenceBagCode", "EXAM_CANDENCE_BAG_CODE");
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            return columnMap;

        }

        public IDictionary<string, object> ExamDetachableBag()
        { // TypeName="Models.ExamDetachableBag()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_DETACHABLE_BAG()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamDetachableBagCode", "EXAM_DETACHABLE_BAG_CODE");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamDetachableBagID", "EXAM_DETACHABLE_BAG_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamGroup()
        { // TypeName="Models.ExamGroup()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_GROUP()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupName", "EXAM_GROUP_NAME");
            columnMap.Add("ExamGroupCode", "EXAM_GROUP_CODE");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamInputMark()
        { // TypeName="Models.ExamInputMark()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_INPUT_MARK()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ActualMark", "ACTUAL_MARK");
            columnMap.Add("ExamMark", "EXAM_MARK");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamInputMarkID", "EXAM_INPUT_MARK_ID");
            columnMap.Add("ExamJudgeMark", "EXAM_JUDGE_MARK");

            return columnMap;

        }
        public IDictionary<string, object> ExamInputMarkAssigned()
        { // TypeName="Models.ExamInputMarkAssigned()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_INPUT_MARK_ASSIGNED()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SchooFacultyID", "SCHOOL_FACULTY_ID");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamInputMarkAssignedID", "EXAM_INPUT_MARK_ASSIGNED_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamPupil()
        { // TypeName="Models.ExamPupil()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_PUPIL()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExamineeNumber", "EXAMINEE_NUMBER");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamPupilAbsence()
        { // TypeName="Models.ExamPupilAbsence()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_PUPIL_ABSENCE()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ReasonAbsence", "REASON_ABSENCE");
            columnMap.Add("ContentReasonAbsence", "CONTENT_REASON_ABSENCE");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamPupilAbsenceID", "EXAM_PUPIL_ABSENCE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamPupilViolate()
        { // TypeName="Models.ExamPupilViolate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_PUPIL_VIOLATE()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamViolationTypeID", "EXAM_VIOLATETION_TYPE_ID");
            columnMap.Add("ContentViolate", "CONTENT_VIOLATED");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("EamPupilViolateID", "EXAM_PUPIL_VIOLATE_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamRoom()
        { // TypeName="Models.ExamRoom()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_ROOM()
            columnMap.Add("UpdatTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("SeatNumber", "SEAT_NUMBER");
            columnMap.Add("ExamRoomCode", "EXAM_ROOM_CODE");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamSubject()
        { // TypeName="Models.ExamSubject()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_SUBJECT()
            columnMap.Add("UpdateTme", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SchedulesExam", "SCHEDULES_EXAM");
            columnMap.Add("SubjectCode", "SUBJECT_CODE");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamSubjectID", "EXAM_SUBJECT_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamSupervisory()
        { // TypeName="Models.ExamSupervisory()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_SUPERVISORY()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamSupervisoryAssignment()
        { // TypeName="Models.ExamSupervisoryAssignment()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_SUPERVISORY_ASSIGNMENT()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            columnMap.Add("ExamSupervisoryAssignmentID", "EXAM_SUPERVISORY_ASSIGNMENT_ID");
            return columnMap;

        }
        public IDictionary<string, object> ExamSupervisoryViolate()
        { // TypeName="Models.ExamSupervisoryViolate()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAM_SUPERVISORY_VIOLATE()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamViolationTypeID", "EXAM_VIOLATETION_TYPE_ID");
            columnMap.Add("ContentViolated", "CONTENT_VIOLATED");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamSupervisoryViolateID", "EXAM_SUPERVISORY_VIOLATE_ID");
            return columnMap;

        }
        public IDictionary<string, object> Examinations()
        { // TypeName="Models.Examinations()
            Dictionary<string, object> columnMap = new Dictionary<string, object>();// StoreEntitySet="EXAMINATIONS()
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("InheritanceData", "INHERITANCE_DATA");
            columnMap.Add("InheritanceExam", "INHERITANCE_EXAM");
            columnMap.Add("DataInheritance", "DATA_INHERITANCE");
            columnMap.Add("MarkClosing", "MARK_CLOSING");
            columnMap.Add("MarkInput", "MARK_INPUT");
            columnMap.Add("MarkInputType", "MARK_INPUT_TYPE");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExaminationsName", "EXAMINATIONS_NAME");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            return columnMap;

        }


        public IDictionary<string, object> RestoreDataDetail()
        { 
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("RESTORE_DATA_DETAIL_ID", "RESTORE_DATA_DETAIL_ID");
            columnMap.Add("RESTORE_DATA_ID", "RESTORE_DATA_ID");
            columnMap.Add("SCHOOL_ID", "SCHOOL_ID");
            columnMap.Add("ACADEMIC_YEAR_ID", "ACADEMIC_YEAR_ID");
            columnMap.Add("RESTORE_DATA_TYPE_ID", "RESTORE_DATA_TYPE_ID");
            columnMap.Add("CREATED_DATE", "CREATED_DATE");
            columnMap.Add("END_DATE", "END_DATE");
            columnMap.Add("LAST_2DIGIT_NUMBER_SCHOOL", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("SQL_UNDO", "SQL_UNDO");
            columnMap.Add("SQL_DELETE", "SQL_DELETE");
            columnMap.Add("TABLE_NAME", "TABLE_NAME");
            columnMap.Add("ORDER_ID", "ORDER_ID");
            columnMap.Add("IS_VALIDATE", "IS_VALIDATE");
            return columnMap;

        }
    }
}
