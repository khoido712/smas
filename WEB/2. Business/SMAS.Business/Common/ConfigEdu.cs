﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SMAS.Business.Common
{
    public class ConfigEdu
    {
        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        public static string GetString(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        public static int GetInt(string key)
        {
            return Convert.ToInt32(WebConfigurationManager.AppSettings[key]);
        }

        /// <summary>
        /// duong dan file template excel
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        public static string TeamplateExcel
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.TEAMPLATE_EXCEL];
            }
        }

        /// <summary>
        /// default language
        /// </summary>
        /// <author>HaiVT</author>
        public static string DefaultLanguage
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_DEFAULTLANGUAGE];
            }
        }

        /// <summary>
        /// get log response time, true if write log
        /// </summary>
        /// <author>HaiVT</author>
        public static bool LogResponseTime
        {
            get
            {
                return Convert.ToBoolean(WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_LOG_RESPONSE_TIME]);
            }
        }

        /// <summary>
        /// IDP url
        /// </summary>
        /// <author date="2014/01/21">HaiVT</author>
        public static string IDPUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_IDP_URL];
            }
        }

        /// <summary>
        /// check login status
        /// </summary>
        /// <author date="2014/01/21">HaiVT</author>
        public static string IDPCheckLoginStatus
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_IDP_CHECK_LOGIN_STATUS];
            }
        }

        /// <summary>
        /// IDP service provider indentity
        /// </summary>
        /// <author date="2014/01/22">HaiVT</author>
        public static string IDPServiceProviderIndentity
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_IDP_SERVICE_PROVIDER_INDENTITY];
            }
        }

        /// <summary>
        /// IDP service provider Name
        /// </summary>
        /// <author date="2014/01/22">HaiVT</author>
        public static string IDPServiceProviderName
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_IDP_SERVICE_PROVIDER_NAME];
            }
        }

        /// <summary>
        /// IDP certificate
        /// </summary>
        /// <author date"2014/01/22">HaiVT</author>
        public static string IDPCertificate
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_IDP_CERTIFICATE];
            }
        }

        /// <summary>
        /// SMS school url
        /// </summary>
        /// <author date="2014/01/22">HaiVT</author>
        public static string SmsSchoolURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_SMS_SCHOOL_URL];
            }
        }

        /// <summary>
        /// SMS SCHOOL check login status
        /// </summary>
        /// <author date="2014/01/22">HaiVT</author>
        public static string SmsSchoolCheckLoginStatus
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_SMS_SCHOOL_CHECK_LOGIN_STATUS];
            }
        }        

        /// <summary>
        /// sms school clear session
        /// </summary>
        /// <author date="2014/01/22">HaiVT</author>
        public static string SmsSchoolClearSession
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_SMS_SCHOOL_CLEAR_SESSION];
            }
        }

        /// <summary>
        /// danh sach dau so duoc phep gui tin nhan
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        public static string PreNumber
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_PRE_NUMBER];
            }
        }

        /// <summary>
        /// dia chi sparent
        /// </summary>
        /// <author date="2014/02/28">HaiVT</author>
        public static string SParentUrl
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_SPARENT_URL];
            }
        }

        /// <summary>
        ///  danh sach tu cam
        /// </summary>
        /// <author date="2014/03/13">HaiVT</author>
        public static string SensitiveKeyword
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_SENSITIVE_KEYWORD];
            }
        }

        /// <summary>
        /// Các gói dùng thử ko tính tiền
        /// </summary>
        /// <author date="2014/09/26">AnhVD</author>
        public static string TrialPackages
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstantsEdu.WEB_APP_SETTING_TRIAL_PACKAGES];
            }
        }
    }
}
