﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace SMAS.Business.Common
{
    public class ResEdu
    {
        private static ResourceManager resourceManager = new ResourceManager(GlobalConstantsEdu.UTILITY_RES_RESOURCE, Assembly.Load(GlobalConstantsEdu.UTILITY_RES_GLOBAL_RESOURCE));

        /// <summary>
        /// get resource content from resourceName
        /// </summary>
        /// <author date="2013/01/15">HaiVT</author>
        /// <param name="resourceName">resource name</param>
        /// <returns>resourceName if not found</returns>
        public static string Get(string resourceName)
        {
            string value = resourceManager.GetString(resourceName);
            return string.IsNullOrEmpty(value) ? resourceName : value;
        }
    }
}