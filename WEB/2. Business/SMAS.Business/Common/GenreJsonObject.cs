﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Common
{
    public class GenreJsonObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
