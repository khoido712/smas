﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using Oracle.DataAccess.Client;

namespace SMAS.Business.Common
{
    public class TestHieuNangAdapter
    {
        public const string Query = "Insert into TEST_HIEU_NANG (THREAD_ID,MODULE,SUB_MODULE,START_TIME,END_TIME, TOTAL_TIME) values (:1,:2,:3,:4,:5, :6)";
        private static List<List<object>> ListData;
        public static List<OracleDbType> ListParam = new List<OracleDbType>()
        {
            OracleDbType.Long,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.TimeStamp,
            OracleDbType.TimeStamp,
            OracleDbType.Double
        };
        private static int GetMaxBulk()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["MaxBulkLog"]);
        }

        public static void Insert(TestHieuNangBO entity)
        {
            if (ListData == null)
            {
                ListData = new List<List<object>>();
                for (int i = 0; i < ListParam.Count; i++)
                {
                    ListData.Add(new List<object>());
                }
            }
            List<object> data = new List<object>()
            {
                entity.ThreadID,
                entity.Module,
                entity.SubModule,
                entity.StartTime,
                entity.EndTime,
                (entity.EndTime -  entity.StartTime).TotalMilliseconds
            };
            for (int i = 0; i < ListParam.Count; i++)
            {
                ListData[i].Add(ConvertDataSqlToOracle(ListParam[i], data[i]));
            }
            if (ListData[0].Count >= GetMaxBulk())
            {
                PushToDatabase();
            }
        }

        private static void PushToDatabase()
        {
            lock (ListData)
            {
                LoggingThread log = new LoggingThread(ListData);
                Thread oThread = new Thread(new ThreadStart(log.Log));
                oThread.Start();
                ListData = new List<List<object>>();
                for (int i = 0; i < ListParam.Count; i++)
                {
                    ListData.Add(new List<object>());
                }
            }
        }

        static object ConvertDataSqlToOracle(OracleDbType OracleDbType, object value)
        {
            try
            {
                object res = value;
                if (value != null)
                {
                    switch (OracleDbType)
                    {
                        case OracleDbType.Raw:
                            if (value is Guid)
                            {
                                res = ((Guid)value).ToByteArray();
                            }
                            break;
                        case OracleDbType.Byte:
                        case OracleDbType.Int16:
                        case OracleDbType.Int32:
                        case OracleDbType.Int64:
                        case OracleDbType.Long:
                            if (value is bool)
                            {
                                res = (bool)value ? 1 : 0;
                            }
                            else
                            {
                                res = long.Parse(value.ToString().Split('.')[0]);
                            }
                            break;
                        case OracleDbType.Decimal:
                        case OracleDbType.Double:
                            if (value is bool)
                            {
                                res = (bool)value ? 1 : 0;
                            }
                            else
                            {
                                res = Double.Parse(value.ToString());
                            }
                            break;
                        case OracleDbType.Date:
                        case OracleDbType.TimeStamp:
                            if (value is TimeSpan)
                            {
                                res = new DateTime(1900, 1, 1, ((TimeSpan)value).Hours,
                                    ((TimeSpan)value).Minutes, ((TimeSpan)value).Milliseconds);
                            }
                            else 
                            {
                                res = value;
                            }
                            break;
                        case OracleDbType.Clob:
                        case OracleDbType.NChar:
                        case OracleDbType.NClob:
                        case OracleDbType.NVarchar2:
                        case OracleDbType.Varchar2:
                        case OracleDbType.Char:
                            res = value.ToString() == "" ? " " : value.ToString();
                            break;
                    }
                }
                else
                {
                    if (OracleDbType == OracleDbType.Clob
                        || OracleDbType == OracleDbType.NChar
                        || OracleDbType == OracleDbType.NClob
                        || OracleDbType == OracleDbType.NVarchar2
                        || OracleDbType == OracleDbType.Varchar2
                        || OracleDbType == OracleDbType.Char)
                    {
                        res = " ";
                    }
                }
                return res;
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Không thể convert dữ liệu ({0}) sang kiểu {1}", value, OracleDbType.ToString()));
            }
        }
    }

    public class LoggingThread
    {

        List<List<object>> ListData;

        public LoggingThread(List<List<object>> List)
        {
            ListData = List.ToList();
        }

        public void Log()
        {
            OracleConnection conn = new OracleConnection(ConfigurationManager.AppSettings["LogConnectionString"]);
            conn.Open();
            try
            {
                OracleCommand cmd = new OracleCommand(TestHieuNangAdapter.Query, conn);
                cmd.CommandTimeout = 0;
                for (int i = 0; i < TestHieuNangAdapter.ListParam.Count; i++)
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleDbType = TestHieuNangAdapter.ListParam[i];
                    param.Value = ListData[i].ToArray();
                    cmd.Parameters.Add(param);
                }
                cmd.ArrayBindCount = ListData[0].Count;
                cmd.ExecuteNonQuery();
            }
            catch
            {
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
