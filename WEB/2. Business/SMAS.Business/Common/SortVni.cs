﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Common
{
    public static class SortVni
    {
        /// <summary>
        /// Ma hoa toan bo xau truyen vao
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public static string VniEnCode(string inputs)
        {
            string[] arrInput = inputs.Replace("  ", " ").Replace("_", "").Replace("-", "").Split(' ');
            List<string> listInput = new List<string>() { arrInput[arrInput.Length - 1] };
            if (arrInput.Length > 1)
            {
                for (int i = 0; i < arrInput.Length - 1; i++)
                {
                    listInput.Add(arrInput[i]);
                }
            }
            string output = string.Empty;
            foreach (string input in listInput)
            {
                output += EnCode(input);
            }
            return output;
        }

        /// <summary>
        /// Ma hoa chuoi TV truyen vao
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string EnCode(string input)
        {
            input = input.ToLower();
            string outputChar = string.Empty;
            string outputSigned = string.Empty;
            string strNotSigned = "AaĂăÂâBbCcDdĐđEeÊêFfGgHhIiJjKkLlMmNnOoÔôƠơPpQqRrSsTtUuƯưVvWwXxYyZz";
            string strSigned = "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬEÈẺẼÉẸÊỀỂỄẾỆIÌỈĨÍỊOÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢUÙỦŨÚỤƯỪỬỮỨỰYỲỶỸÝỴaàảãáạăằẳẵắặâầẩẫấậeèẻẽéẹêềểễếệiìỉĩíịoòỏõóọôồổỗốộơờởỡớợuùủũúụưừửữứựyỳỷỹýỵ";
            string cma1 = "aaacaeaoaqaybkbmbocaccckabadafaparazblbnbpcbcdcl1b1c1d1e1f1a";
            string cma2 = "aaabacadaeafagahaiajakalamanaoapaqarasatauavawaxayazbabbbcbdbebfbgbhbibjbkblbmbnbobpbqbrbsbtbubvbwbxbybzcacbcccdcecfcgchcicjckclcmcn";
            int len = input.Length;
            for (int i = 0; i < len; i++)
            {
                char s = input[i];
                // Neu la ky tu co dau
                if (strSigned.Contains(s))
                {
                    int index = strSigned.IndexOf(s);
                    outputChar += cma1.Substring((index / 6) * 2, 2);
                    outputSigned += cma1.Substring(48 + ((5 + index) % 6) * 2, 2);
                }
                else if (strNotSigned.Contains(s))
                {
                    int index = strNotSigned.IndexOf(s);
                    outputChar += cma2.Substring(2 * index, 2);
                }
                else
                {
                    outputChar += s;
                }
            }
            return outputChar + outputSigned;
        }
    }
}
