﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
namespace SMAS.Business.Common
{

    /// <summary>
    /// Simplex method of linear programming
    /// </summary>
    /// <author>Quanglm</author>
    /// <date>11/16/2012</date>
    class Simplex
    {
        private decimal round = (decimal)0.01;
        private string question;
        private bool maximization = true;		// this is a max problem
        private bool okToRoll = true;		// Danh dau co nghiem hay khong
        private decimal epsilon = (decimal)0.00000000000001;
        private bool phase1 = false;			// are we in phase 1?
        private int numVariables = 1;

        private bool singular = false;
        private int TableauNumber = 1;				// the number of tableaus
        private int maxSteps = 50;					// maximum number of tableaux 
        private int numConstraints = 0;
        private int[] starred;
        private int maxSigDig = 13; // max number of sig digits
        private decimal[] activeVars;
        private decimal[,] theTableau;
        private string[] variables;


        // Chua danh sach ket qua
        private decimal[] _resVal = null;
        public decimal[] ResVal { get { return _resVal; } }
        // variable
        private string[] _variables = null;
        public string[] Variables { get { return _variables; } }
        // Gia tri
        private decimal _optionalVal;
        public decimal OptionalVal { get { return _optionalVal; } }
        // Tu dien
        public Dictionary<string, decimal> DicVariableVal
        {
            get
            {
                if (_variables == null )
                {
                    return null;
                }
                Dictionary<string, decimal> dic = new Dictionary<string, decimal>();
                for (int i = 0; i < _variables.Length; i++)
                {
                    dic[_variables[i]] = _resVal[i];
                }
                return dic;
            }
        }

        // constructor
        public Simplex(string question)
        {
            this.question = question;
        }

        // Method
        /// <summary>
        /// Chuyen thanh cac bieu thuc
        /// </summary>
        /// <param name="InString">The in string.</param>
        /// <returns>
        /// String[][]
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private string[] parseLinearExpr(string InString)
        {

            // **********
            // Returns an array: with 0th entry = an array of variable names 
            // (eg. ["x", "x'", "y", "z"])
            // and subsequent entries the coefficients.
            // to get the number of coefficients, just take the length of the array in position 0.
            // first remove a leading cr if there

            InString = InString.Replace("(", "").Replace(")", "");   // get rid of parens (not needed once x is gone...
            var stringlen = InString.Length;
            // THE ABOVE LINE REMOVES A STRANGE BUG IN NETSCAPE, WHICH SEEMS
            // TO INSERT A SPURIOUS LINE BREAK  (0A) THERE RATHER THAN A CR (0D)
            // first insert a leading 1

            // ***HERE THE FOLLOWING LINE WAS ADDED AS A FIX FOR WINTEL

            if (!looksLikeANumber(InString[0].ToString())) InString = "1" + InString;
            InString = InString.Replace("+", "_+");
            InString = InString.Replace("-", "_-");
            var ch = '_';
            string[] Ar = InString.Split(ch);
            int ArLen = Ar.Length;

            string[] parsd = new string[ArLen + 1];
            // now for the variable names

            string[] vars = new string[ArLen];
            for (var i = 1; i <= ArLen; i++)
            {
                string val = Ar[i - 1];
                parsd[i] = val.Replace("_", "");

                // parser gives 1st entry as what is before first sign -- ignore it
                vars[i - 1] = GetMatch(parsd[i]);
                parsd[i] = parsd[i].Replace(vars[i - 1], "");
                if (parsd[i] == "+") parsd[i] = "1";  // fix up the coefficients

                else if (parsd[i] == "-") parsd[i] = "-1";

                parsd[i] = parsd[i].Replace("+", "");
            }
            parsd[0] = String.Join(",", vars);
            return (parsd);
        }

        /// <summary>
        /// GetMatch
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private string GetMatch(string input)
        {
            string reglex = "([a-zA-Z].*)";
            // Here we call Regex.Match.
            Match match = Regex.Match(input, reglex,
                RegexOptions.IgnoreCase);

            // Here we check the Match instance.
            if (match.Success)
            {
                // Finally, we get the Group value and display it.
                string key = match.Groups[1].Value;
                return key;
            }
            return null;
        }

        /// <summary>
        /// looksLikeANumber
        /// </summary>
        /// <param name="theString">The string.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private bool looksLikeANumber(string theString)
        {

            // returns true if theString looks like it can be evaluated

            var result = true;

            var Length = theString.Length;

            if (Length == 0) return (false);

            var y = "1234567890-+*. /";

            var yLength = y.Length;

            for (var i = 0; i < Length; i++)
            {

                string x = theString[i].ToString();

                result = false;

                for (var j = 0; j < yLength; j++)
                {

                    if (x == y[j].ToString()) { result = true; break; }

                } // j

                if (result == false) return (false);

            } // i

            return (result);

        }

        /// <summary>
        /// checkString
        /// </summary>
        /// <param name="inString">The in string.</param>
        /// <param name="cutString">The cut string.</param>
        /// <param name="backtrack">if set to <c>true</c> [backtrack].</param>
        /// <returns>
        /// Int32
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private int checkString(string inString, string cutString, bool backtrack)
        {
            int found = -1;
            string theString = inString;
            int Length = theString.Length;
            int symbLength = cutString.Length;
            for (int i = Length - symbLength - 1; i > -1; i--)
            {
                string TempChar = theString.Substring(i, symbLength);
                if (TempChar == cutString)
                {
                    found = i;
                    if (backtrack)
                    {
                        i = -1;
                    }
                }

            }
            return (found);
        }

        /// <summary>
        /// Tinh toan ham muc tieu
        /// </summary>
        /// <param name="InMatrix">The in matrix.</param>
        /// <returns>
        /// Decimal[][]
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private decimal[,] simplexMethod(decimal[,] InMatrix)
        {
            int rows = InMatrix.GetLength(0);
            int cols = InMatrix.GetLength(1);

            var negIndicator = false;

            decimal[] testRatio = new decimal[rows];

            int theRow = -1;
            singular = false;


            // PHASE I

            while ((phase1) && (TableauNumber <= maxSteps))
            {

                // big chunk of code removed here including an "if"

                // first unstar all rows with zeros on the right-hand side 
                // by reversing the inequalities
                // this is absolutely necessary in case of things like
                // -x - y >= 0
                // reversing the signs and removing the star
                // will not effect the active variable name
                // as it value is still zero in this case
                // and its pivot will now be positive


                var checkingForZeros = true;
                var foundAZero = false;
                while (checkingForZeros)
                {
                    int theRowx = 0;
                    checkingForZeros = false;
                    for (int i = 0; i < rows - 1; i++)
                    {
                        if (starred[i] == 1)
                        {
                            theRowx = i;
                            break;
                        }
                    } // i                    
                    // check the first column to see if it has a zero on the
                    // right-hand side and is hence equivalent to <= constraint

                    if ((InMatrix[theRowx, cols - 1] == 0) && (starred[theRowx] == 1))
                    {
                        checkingForZeros = true;
                        foundAZero = true;
                        for (var j = 0; j < cols - 1; j++)
                        {
                            InMatrix[theRowx, j] *= -1;
                        } // j
                        starred[theRowx] = 0;
                        // add additional tableaus
                        TableauNumber += 1;
                    }// found a zero on the right-hand side
                } // while checking for zeros

                // at this  point, check if there are any starred rows left
                phase1 = false;
                for (var i = 0; i < numConstraints; i++)
                {
                    if (starred[i] == 1) { phase1 = true; break; }
                } // i

                if (phase1)
                {
                    // there are starred rows left
                    // scan the first starred row starred row for the largest pos. element & pivot on that column

                    // this is actually step 2
                    if (!foundAZero)
                    {
                        // find the largest positive entry in the first starred row
                        // and pivot

                        decimal rowmax = 0;
                        int theRowx = -1;
                        int theColx = -1;
                        for (int i = 0; i < rows - 1; i++)
                        {
                            if (starred[i] == 1) { theRowx = i; break; }
                        }

                        for (int j = 0; j < cols - 2; j++)
                        {

                            decimal numx = Math.Round(InMatrix[theRowx, j], 10);

                            if (numx > rowmax)
                            {
                                rowmax = numx;
                                theColx = j;
                            }

                        } // j

                        if (rowmax == 0) { singular = true; return (InMatrix); }


                        else
                        {

                            // get the lowest ratio and pivot on theRowx, theColx;

                            for (var i = 0; i < rows - 1; i++)
                            {

                                testRatio[i] = -1;

                                if (Math.Round(InMatrix[i, theColx], maxSigDig) > 0) // dont want to pivot on a number too close to zero
                                {
                                    if (Math.Abs(InMatrix[i, cols - 1]) < epsilon) InMatrix[i, cols - 1] = 0;
                                    // fixing numbers really close to zero
                                    testRatio[i] = InMatrix[i, cols - 1] / InMatrix[i, theColx];
                                }

                            } // i

                            decimal minRatio = 10000000000000;

                            theRow = -1;			// this will have smallest ratio

                            for (var i = 0; i < rows - 1; i++)
                            {

                                if ((testRatio[i] >= 0) && (testRatio[i] < minRatio))
                                {

                                    minRatio = testRatio[i];

                                    theRow = i;

                                } // end if



                                else if ((testRatio[i] >= 0) && (testRatio[i] == minRatio))
                                {

                                    if (starred[i] == 1) theRow = i;

                                    // select starred ones in preference to others
                                    else if (new Random().NextDouble() > 0.5) theRow = i;
                                    // random tie-breaking
                                }

                            } // i

                            // escape clause follows

                            if (theRow == -1) { singular = true; return (InMatrix); }
                            InMatrix = pivot(InMatrix, rows, cols, theRow, theColx);

                            // end of this step
                        } // if did not find a zero

                        TableauNumber += 1;
                    }

                } // end of phase 1 treatment
                // phase1 = false  // TEMPORARY MEASURE TO PREVENT INF LOOPS

            }
            // END OF PHASE I
            // NOW PHASE II
            decimal testnum = 0;
            for (int i = 0; i < cols - 1; i++)
            {

                testnum = Math.Round(InMatrix[rows - 1, i], 10);

                if (testnum < 0)
                {

                    negIndicator = true;

                }

            }
            var theCol = 0;
            if (negIndicator)
            {

                // look for most negative of them;

                decimal minval = 0;
                for (int i = 0; i <= cols - 1; i++)
                {
                    testnum = Math.Round(InMatrix[rows - 1, i], 10);
                    if (testnum < minval)
                    {
                        minval = testnum;
                        theCol = i;
                    }
                }
            }
            while ((negIndicator) && (TableauNumber <= maxSteps)) // phase 2
            {

                for (var i = 0; i < rows - 1; i++)
                {

                    testRatio[i] = -1;

                    if (Math.Round(InMatrix[i, theCol], maxSigDig) > 0) // dont want to pivot on a number too close to zero
                    {
                        if (Math.Abs(InMatrix[i, cols - 1]) < epsilon) InMatrix[i, cols] = 0;
                        // fixing numbers really close to zero
                        testRatio[i] = InMatrix[i, cols - 1] / InMatrix[i, theCol];
                    }

                } // i

                decimal minRatio = 10000000000000;

                theRow = -1;			// this will have smallest ratio

                for (int i = 0; i <= rows - 1; i++)
                {
                    if ((testRatio[i] >= 0) && (testRatio[i] < minRatio))
                    {
                        minRatio = testRatio[i];
                        theRow = i;
                    }

                    else if ((testRatio[i] >= 0) && (testRatio[i] == minRatio))
                    {
                        if (new Random().NextDouble() > 0.5) theRow = i;
                        // random tie-breaking
                    }

                } // i

                // escape clause: 
                if (theRow == -1) { singular = true; return (InMatrix); }
                InMatrix = pivot(InMatrix, rows, cols, theRow, theCol);
                // end of this step
                TableauNumber += 1;
                negIndicator = false;
                for (var i = 0; i < cols - 1; i++)
                {
                    if (Math.Round(InMatrix[rows - 1, i], 10) < 0)
                    {
                        negIndicator = true;
                    }
                }

                // ERROR CORRECTION BELOW:
                if (negIndicator)  // need to select the most negative EVERY time
                {
                    // look for most negative of them;
                    decimal minval = 0;

                    for (int i = 0; i < cols - 1; i++)
                    {

                        testnum = Math.Round(InMatrix[rows, i], 10);

                        if (testnum < minval)
                        {

                            minval = testnum;

                            theCol = i;

                        }

                    } // i

                }  // end if negIndicator is still true
            }


            return (InMatrix);

        }

        /// <summary>
        /// Dao cot
        /// </summary>
        /// <param name="InMatrix">The in matrix.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="cols">The cols.</param>
        /// <param name="theRow">The row.</param>
        /// <param name="theCol">The col.</param>
        /// <returns>
        /// Decimal[][]
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private decimal[,] pivot(decimal[,] InMatrix, int rows, int cols, int theRow, int theCol)
        {

            // alert("theRow = " + theRow + "theCol" + theCol);

            decimal thePivot = InMatrix[theRow, theCol];

            activeVars[theRow] = theCol; // reset the active variable
            starred[theRow] = 0; // unstar the row

            for (var i = 0; i < cols; i++)
            {

                InMatrix[theRow, i] = InMatrix[theRow, i] / thePivot;

            } // i

            // now pivot

            for (int i = 0; i < rows; i++)
            {

                if ((i != theRow) && (InMatrix[i, theCol] != 0))
                {

                    decimal factr = InMatrix[i, theCol];

                    for (int j = 0; j < cols; j++)
                    {

                        InMatrix[i, j] = InMatrix[i, j] - factr * InMatrix[theRow, j];

                    }

                }

            }

            return (InMatrix);

        }

        /// <summary>
        /// Khoi tao ma tran he so
        /// </summary>
        /// <param name="theString">The string.</param>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private void SetupTableau(string theString)
        {

            // reads problem and sets up the first tableau
            theString = theString.ToLower().Trim().Replace(" ", "");

            // first, adjust some globals...

            maximization = true;

            singular = false;		// start with a clean slate
            // look for "maximize" and chop the string there

            var check = checkString(theString, "max", false);

            if (check == -1)

            { check = checkString(theString, "min", false); maximization = false; phase1 = true; }

            if (check == -1)
            {
                okToRoll = false;
                return;
            }

            int len = theString.Length;

            theString = cutString(theString, check, len);

            // now the string starts with "max or "min"
            // now extract the objective and constraints
            string[] tempAr = theString.Split(';');
            var numConstTemp = tempAr.Length - 1;
            numConstraints = tempAr.Length - 1;
            List<string> listExpMore = new List<string>();
            for (var i = 1; i <= numConstTemp; i++)
            {
                if (tempAr[i] != null && tempAr[i].Contains("=") && !tempAr[i].Contains("<=") && !tempAr[i].Contains(">="))
                {
                    tempAr[i] = tempAr[i].Replace("=", "<=");
                    listExpMore.Add(tempAr[i].Replace("<=", ">="));
                }
            }
            // Add more constrains
            int moreCount = listExpMore.Count;
            if (moreCount > 0)
            {
                string[] arrMore = new string[numConstTemp + 1 + moreCount];
                for (int i = 0; i <= numConstTemp + moreCount; i++)
                {
                    if (i <= numConstTemp)
                    {
                        arrMore[i] = tempAr[i];
                    }
                    else
                    {
                        arrMore[i] = listExpMore[i - (numConstTemp + 1)];
                    }
                }
                tempAr = arrMore;
            }
            // the first line should contain the objective

            string objectFunc = tempAr[0];

            // get rid of "subject to, if there"

            check = checkString(objectFunc, "subj", true);

            if (check > 0) objectFunc = cutString(objectFunc, 0, check);



            // now look for objective

            check = checkString(objectFunc, "=", false);

            if (check <= 0)
            {
                okToRoll = false;
                return;
            }

            string objectiveName = objectFunc[check - 1].ToString();

            len = objectFunc.Length;

            var expression = cutString(objectFunc, check + 1, len);

            // alert(expression);

            string[] OBJ = parseLinearExpr(expression);

            variables = OBJ[0].Split(',');
            numConstraints = tempAr.Length - 1;
            // make the tableau .. note that all the variables are assumed to appear in the objective!!!

            numVariables = variables.Length;

            int numRows = numConstraints + 1;

            int numCols = numRows + numVariables + 1;
            // create the tableau

            theTableau = new decimal[numRows, numCols];

            if (phase1) starred = new int[numRows];		// for starred rows


            // do the last row
            for (var j = 0; j < numCols; j++) theTableau[numRows - 1, j] = 0; // init

            for (var i = 0; i < numVariables; i++)
            {
                decimal val = decimal.Parse(OBJ[i + 1]);
                if (maximization) theTableau[numRows - 1, i] = -val;
                else theTableau[numRows - 1, i] = val;
            }

            theTableau[numRows - 1, numCols - 2] = 1;
            theTableau[numRows - 1, numCols - 1] = 0;

            // now extract the constraints
            // first remove the "subject to..."

            theString = tempAr[1];

            var x = checkString(theString, "to", false);

            len = theString.Length;

            if (x != -1) theString = cutString(theString, x + 2, len);

            tempAr[1] = theString;

            var GTE = false; // greater-than-eq flag

            activeVars = new decimal[numConstraints];
            starred = new int[numConstraints];
            for (var i = 0; i < numConstraints; i++)
            {

                activeVars[i] = i + numVariables;
                starred[i] = 0;

                GTE = false; // clean slate
                // alert(tempAr[1+i]);
                // first get the inequalities out of the way	
                string[] twoPart = tempAr[1 + i].Split(new string[] { "<=" }, StringSplitOptions.None);

                if (twoPart.Length < 2)
                {
                    // alert(tempAr[1+i]);
                    twoPart = tempAr[1 + i].Split(new string[] { ">=" }, StringSplitOptions.None);
                    phase1 = true;
                    GTE = true;
                }

                if (twoPart.Length < 2)
                {
                    i += 1;
                    okToRoll = false;
                }

                string[] leftHandSide = parseLinearExpr(twoPart[0]);
                for (int j = 0; j < numCols; j++) theTableau[i, j] = 0;	// init

                theTableau[i, numCols - 1] = decimal.Parse(twoPart[1]); 		// the right-hand side

                if (GTE)
                {
                    theTableau[i, numVariables + i] = -1;
                    starred[i] = 1;
                    phase1 = true;
                }

                else
                {
                    theTableau[i, numVariables + i] = 1;
                }

                int theIndex = 0;
                for (int j = 0; j < numVariables; j++)
                {

                    string theVar = variables[j];
                    theIndex = -1;
                    string[] leftHandSideArrZero = leftHandSide[0].Split(',');

                    for (int k = 0; k < leftHandSideArrZero.Length; k++)
                    {
                        if (leftHandSideArrZero[k] == theVar)
                        {
                            theIndex = k;
                            break;
                        }
                    }

                    if (theIndex == -1)
                    {
                        theTableau[i, j] = 0;
                    }
                    else
                    {
                        theTableau[i, j] = decimal.Parse(leftHandSide[theIndex + 1]);
                    }
                }

            }

        }

        /// <summary>
        /// Lay ket qua
        /// </summary>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        private void DisplayFinalStatus()
        {

            // gives the solution or error messages 
            if (TableauNumber > maxSteps) return;// Khong co ket qua

            else if (singular) return;// Khong co ket qua

            else
            {
                decimal numx = 0;
                int count = 0;
                string theChar;

                int numRows = this.theTableau.GetLength(0);
                int numCols = this.theTableau.GetLength(1);
                decimal objectiveVal = theTableau[numRows - 1, numCols - 1];
                _optionalVal = objectiveVal;
                if (!maximization) _optionalVal = -objectiveVal;

                int[] thePivotPosn = new int[numVariables + 1];
                _variables = new string[numVariables];
                _resVal = new decimal[numVariables];
                bool useThis = true;
                for (int j = 0; j < numVariables; j++)
                {
                    useThis = true;
                    count = 0;

                    int theRowx = 0;

                    theChar = variables[j];		// name of this variable
                    _variables[j] = theChar;
                    thePivotPosn[j] = 0;
                    useThis = true;

                    for (var i = 0; i < numRows; i++)
                    {
                        numx = Math.Round(theTableau[i, j], 10);
                        if (numx != 0)
                        {
                            count++;
                            if (numx != 0) theRowx = i;
                        }
                    } // i

                    if ((count == 1) && (Math.Round(theTableau[theRowx, j], 10) > 0))
                    {
                        // correction May 20 2010 he second condition above did not check that the pivot was positive!!!
                        thePivotPosn[j] = theRowx; // row of that pivot
                        // check if we have not already used a pivot in that row
                        // in the case of more than one pivot per row
                        for (var u = 1; u <= j - 1; u++) if (thePivotPosn[j] == thePivotPosn[u]) useThis = false;

                        // present solution 

                        if (useThis)
                        {
                            // Bug fix April 3 2009
                            // if there were more than two pivots ion a row
                            // and it used an earlier one for which the pivot was not 1
                            // then it reported the incorrect solution
                            // fix: divide by the pivot in all cases just in case...
                            // implemented in the next two lines
                            decimal oVal = (theTableau[theRowx, numCols - 1] / theTableau[theRowx, j]);
                            decimal val = Math.Round(oVal, 2);
                            if (val < oVal)
                            {
                                val += round;
                            }
                            _resVal[j] = val;
                        }


                    } // if a pivot there                    
                }
            } // end of presentation
        }

        /// <summary>
        /// DoIt
        /// </summary>
        /// <author>Quanglm</author>
        /// <date>11/19/2012</date>
        public void DoIt()
        {
            if (okToRoll)
            {
                TableauNumber = 1;
                this.SetupTableau(question);
            }

            if (okToRoll)
            {
                theTableau = simplexMethod(theTableau);
                DisplayFinalStatus();
            }

        }

        /// <summary>
        /// Quanglm
        /// Thay the cho ham Substring
        /// </summary>
        /// <param name="input"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        private string cutString(string input, int startIndex, int endIndex)
        {
            if (input == null || endIndex <= startIndex)
            {
                return null;
            }
            if (input.Length < (endIndex))
            {
                return null;
            }
            return input.Substring(startIndex, endIndex - startIndex);
        }
    }

}