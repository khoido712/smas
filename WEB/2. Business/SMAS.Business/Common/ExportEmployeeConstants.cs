﻿namespace SMAS.Business.Common
{
    public class ExportEmployeeConstants
    {
        public const int STT_1 = 1;
        public const int TO_BO_MON_2 = 2;
        public const int MA_CAN_BO_3 = 3;
        public const int HO_VA_TEN_4 = 4;
        public const int GIOI_TINH_5 = 5;
        public const int NGAY_SINH_6 = 6;
        public const int LOAI_CONG_VIEC_7 = 7;
        public const int HINH_THUC_HD_8 = 8;
        public const int DAN_TOC_9 = 9;
        public const int TON_GIAO_10 = 10;
        public const int CAP_DAY_CHINH_11 = 11;
        public const int NGAY_VAO_TRUONG_12 = 12;
        public const int SO_CMND_13 = 13;
        public const int NGAY_CAP_CMND_14 = 14;
        public const int NOI_CAP_CMND_15 = 15;
        public const int SO_DIEN_THOAI_16 = 16;
        public const int SO_DIEN_THOAI_DI_DONG_17 = 17;
        public const int EMAIL_18 = 18;
        public const int SO_BHXH_19 = 19;
        public const int QUE_QUAN_20 = 20;
        public const int TINH_THANH_21 = 21;
        public const int QUAN_HUYEN_22 = 22;
        public const int XA_PHUONG_23 = 23;
        public const int DIA_CHI_THUONG_TRU_24 = 24;
        public const int SUC_KHOE_25 = 25;
        public const int DOAN_VIEN_26 = 26;
        public const int DANG_VIEN_27 = 27;
        public const int CONG_DOAN_VIEN_28 = 28;
        public const int THANH_PHAN_GD_29 = 29;
        public const int HO_TEN_BO_30 = 30;
        public const int NAM_SINH_BO_31 = 31;
        public const int NGHE_NGHIEP_BO_32 = 32;
        public const int NOI_LAM_VIEC_BO_33 = 33;
        public const int HO_TEN_ME_34 = 34;
        public const int NAM_SINH_ME_35 = 35;
        public const int NGHE_NGHIEP_ME_36 = 36;
        public const int NOI_LAM_VIEC_ME_37 = 37;
        public const int HO_VA_TEN_VC_38 = 38;
        public const int NAM_SINH_VC_39 = 39;
        public const int NGHE_NGHIEP_VC_40 = 40;
        public const int NOI_LAM_VIEC_VC_41 = 41;
        public const int TRINH_DO_CMNV_CAONHAT_42 = 42;
        public const int CHUYEN_NGANH_DAO_TAO_43 = 43;
        public const int TRINH_DO_VAN_HOA_44 = 44;
        public const int TRINH_DO_NGOAI_NGU_45 = 45;
        public const int TRINH_DO_TIN_HOC_46 = 46;
        public const int TRINH_DO_LY_LUAN_CT_47 = 47;
        public const int TRINH_DO_QLNN_48 = 48;
        public const int TRINH_DO_QLGD_49 = 49;
        public const int CHUYEN_TRACH_DD_50 = 50;
        public const int BOI_DUONG_TX_51 = 51;
        public const int NGAY_VAO_NGHE_52 = 52;

        public const int MO_TA_LOI_53 = 53;

    }
}
