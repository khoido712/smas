﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Common
{
    public class RestoreDataConstant
    {
        /// <summary>
        /// khoi phuc du lieu truong
        /// </summary>
        public const int RESTORE_DATA_TYPE_SCHOOL = 1;
        /// <summary>
        /// Khoi phuc nam hoc
        /// </summary>
        public const int RESTORE_DATA_TYPE_YEAR = 2;

        /// <summary>
        /// Khoi phuc lop hoc
        /// </summary>
        public const int RESTORE_DATA_TYPE_CLASS = 3;

        /// <summary>
        /// Khoi phuc hoc sinh
        /// </summary>
        public const int RESTORE_DATA_TYPE_PUPIL = 4;

        /// <summary>
        /// Khoi phuc diem
        /// </summary>
        public const int RESTORE_DATA_TYPE_MARK = 5;

        public const string SQL_UNDO_SCHOOL = "UPDATE SCHOOL_PROFILE SET IS_ACTIVE=1,SCHOOL_CODE='{0}' WHERE SCHOOL_PROFILE_ID={1}";


        public const string SQL_UNDO_USER_ACCOUNT = "UPDATE USER_ACCOUNT SET IS_ACTIVE=1 WHERE USER_ACCOUNT_ID={0}";

        public const string SQL_UNDO_USER_NAME = "UPDATE ORA_ASPNET_USERS SET USERNAME='{0}', loweredusername='{1}' WHERE USERID='{2}' AND USERNAME!='{3}'";

        public const string SQL_UNDO_PUPIL_PROFILE = "UPDATE PUPIL_PROFILE SET IS_ACTIVE=1, PUPIL_CODE='{0}' WHERE PUPIL_PROFILE_ID={1}";

        public const string SQL_UNDO_CLASS = "UPDATE CLASS_PROFILE SET IS_ACTIVE=1,DISPLAY_NAME='{0}' WHERE CLASS_PROFILE_ID={1}";

        public const string SQL_UNDO_ACADEMIC_YEAR = "UPDATE ACADEMIC_YEAR SET IS_ACTIVE=1 WHERE ACADEMIC_YEAR_ID={0}";
        public const string SQL_UNDO_ACADEMIC_YEAR_CHANGETIME = "UPDATE ACADEMIC_YEAR SET IS_ACTIVE=1, FIRST_SEMESTER_END_DATE=to_date('{0}','dd/MM/yyyy'),SECOND_SEMESTER_END_DATE=to_date('{1}','dd/MM/yyyy') WHERE ACADEMIC_YEAR_ID={2}";

        public const string SQL_DELETE_EXISTS_MARK = "DELETE MARK_RECORD WHERE ACADEMIC_YEAR_ID={0} and SCHOOL_ID={1} AND LAST_2DIGIT_NUMBER_SCHOOL={2} AND PUPIL_ID={3} AND CLASS_ID={4} AND SUBJECT_ID={5} AND MARK_TYPE_ID={6} AND SEMESTER={7}";

        public const string SQL_DELETE_EXISTS_JUDGE_MARK = "DELETE JUDGE_RECORD WHERE ACADEMIC_YEAR_ID={0} and SCHOOL_ID={1} AND LAST_2DIGIT_NUMBER_SCHOOL={2} AND PUPIL_ID={3} AND CLASS_ID={4} AND SUBJECT_ID={5} AND MARK_TYPE_ID={6} AND SEMESTER={7}";

        public const string SQL_DELETE_EXISTS_SUMMED_MARK = "DELETE SUMMED_UP_RECORD WHERE PUPIL_ID={0} AND CLASS_ID={1} AND SCHOOL_ID={2} AND ACADEMIC_YEAR_ID={3} AND SUBJECT_ID={4} AND SEMESTER={5} AND NVL(PERIOD_ID,0)={6} AND LAST_2DIGIT_NUMBER_SCHOOL={7}";

        public const string SQL_DELETE_TEACHER_NOTEBOOK_MONTH = "DELETE TEACHER_NOTEBOOK_MONTH WHERE ACADEMIC_YEAR_ID={0} and SCHOOL_ID={1} AND PARTITION_ID={2} AND PUPIL_ID={3} AND CLASS_ID={4} AND SUBJECT_ID={5} AND MONTH_ID={6}";

        public const string SQL_DELETE_TEACHER_NOTEBOOK_SEMESTER = "DELETE TEACHER_NOTEBOOK_SEMESTER WHERE ACADEMIC_YEAR_ID={0} and SCHOOL_ID={1} AND PARTITION_ID={2} AND PUPIL_ID={3} AND CLASS_ID={4} AND SUBJECT_ID={5} AND SEMESTER_ID={6}";

        public const string SQL_DELETE_RATED_COMMENT_PUPIL = "DELETE RATED_COMMENT_PUPIL WHERE  ACADEMIC_YEAR_ID={0} AND SCHOOL_ID={1} AND CLASS_ID={2} AND SUBJECT_ID={3} AND PUPIL_ID={4} AND SEMESTER_ID={5} AND EVALUATION_ID={6} AND LAST_DIGIT_SCHOOL_ID={7}";

        public const string TABLE_SCHOOL_PROFILE = "SCHOOL_PROFILE";
        public const string TABLE_USERS = "ORA_ASPNET_USERS";
        public const string TABLE_USER_ACCOUNT = "USER_ACCOUNT";
        public const string TABLE_ACADEMIC_YEAR = "ACADEMIC_YEAR";
        public const string TABLE_CLASS_PROFILE = "CLASS_PROFILE";
        public const string TABLE_PUPIL_PROFILE = "PUPIL_PROFILE";
        public const string TABLE_MARK_RECORD = "MARK_RECORD";
        public const string TABLE_JUDGE_RECORD = "JUDGE_RECORD";
        public const string TABLE_SUMMED_UP_RECORD = "SUMMED_UP_RECORD";
        public const string TABLE_TEACHER_NOTEBOOK_MONTH = "TEACHER_NOTEBOOK_MONTH";
        public const string TABLE_TEACHER_NOTEBOOK_SEMESTER = "TEACHER_NOTEBOOK_SEMESTER";

        public const string TABLE_RATED_COMMENT_PUPIL = "RATED_COMMENT_PUPIL";

    }
}
