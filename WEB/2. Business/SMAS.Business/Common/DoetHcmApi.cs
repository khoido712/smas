﻿using log4net;
using Newtonsoft.Json.Linq;
using SMAS.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SMAS.Business.Common
{
    public class DoetHcmApi
    {
        
        public static string API_URL
        {
            get
            {
                return WebConfigurationManager.AppSettings["SMAS_API_PARTNER_URL"];
            }
        }
        private static string UserName
        {
            get
            {
                return WebConfigurationManager.AppSettings["SMAS_API_PARTNER_USER"];
            }
        }
        private static string PassWord
        {
            get
            {
                return WebConfigurationManager.AppSettings["SMAS_API_PARTNER_PASSWORD"];
            }
        }

        private static readonly ILog logger = LogManager.GetLogger("SMAS");
        private static HttpClient CreateHttpClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("UserName", UserName);
            client.DefaultRequestHeaders.Add("Password", PassWord);
            return client;
        }

        protected static async Task<string> GetAction(string url)
        {
            try
            {
                var response = await CreateHttpClient().GetAsync(url);
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(responseString))
                {
                    return "";
                }
                JToken token = JToken.Parse(responseString);
                responseString = token.ToString();
                return responseString;
            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return "";

            }

        }

        protected static async Task<string> PostAction(string url, string strJson)
        {
            var content = new StringContent(strJson, Encoding.UTF8, "application/json");
            try
            {
                var response = await CreateHttpClient().PostAsync(url, content);
                var responseString = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(responseString))
                {
                    return "";
                }
                JToken token = JToken.Parse(responseString);
                responseString = token.ToString();
                return responseString;

            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return "";
            }

        }

        protected static async Task<string> DeleteAction(string url)
        {
            try
            {
                var response = await CreateHttpClient().DeleteAsync(url);
                var responseString = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(responseString))
                {
                    return "";
                }
                JToken token = JToken.Parse(responseString);
                responseString = token.ToString();
                return responseString;
            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return hre.Message;
            }

        }



        /// <summary>
        /// Lay danh sach ky thi theo truong
        /// </summary>
        /// <param name="nienHocId">gia tri dau cua nam hoc</param>
        /// <param name="matruong">ma truong</param>
        /// <param name="caphoc"></param>
        /// <returns></returns>
        public static List<KyThi> GetKiThiTheoTruong(int nienHocId, string matruong, int caphoc)
        {
            try
            {
                string url = string.Format("{0}KiThi/GetKiThi?SchoolID={1}&NienHocID={2}&truonghoccap={3}", API_URL, matruong, nienHocId, caphoc);
                var result = Task.Run(async () =>
                {
                    return await GetAction(url);
                }).Result;


                if (string.IsNullOrEmpty(result))
                {
                    return new List<KyThi>();
                }
                var jsonList = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObjectExam>(result);
                if (jsonList == null)
                {
                    return new List<KyThi>();
                }
                var lstVal = jsonList.DanhSachKyThi;

                return lstVal.OrderByDescending(s => s.HanChotDangKy).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<KyThi>();
            }
        }

        public static List<MonThi> GetMonThi(int kithiId)
        {
            try
            {
                string url = string.Format("{0}KiThi/GetMonThi?KythiId={1}", API_URL, kithiId);
                var result = Task.Run(async () =>
                {
                    return await GetAction(url);
                }).Result;
                if (string.IsNullOrEmpty(result))
                {
                    return new List<MonThi>();
                }
                JToken token = JToken.Parse(result);
                result = token.ToString();
                var jsonList = Newtonsoft.Json.JsonConvert.DeserializeObject<RootKiThiMon>(result);
                if (jsonList == null)
                {
                    return new List<MonThi>();
                }
                var lstVal = jsonList.KiThiMon.ToList();

                return lstVal;
            }
            catch (Exception ex)
            {
                return new List<MonThi>();
            }
        }

        public static List<DanhSachThiSinh> GetThiSinh(int kithiId, string matruong, int monthiId)
        {
            try
            {

                string url = string.Format("{0}ThiSinh/GetThiSinh?kithiId={1}&matruong={2}&monthiId={3}", API_URL, kithiId, matruong, monthiId);
                var result = Task.Run(async () =>
                {
                    return await GetAction(url);
                }).Result;


                var jsonList = Newtonsoft.Json.JsonConvert.DeserializeObject<RootThiSinh>(result);
                if (jsonList == null)
                {
                    return new List<DanhSachThiSinh>();
                }
                var lstVal = jsonList.Table.ToList();

                return lstVal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetMaThiSinh(int kythiId, string matruong)
        {
            string url = string.Format("{0}ThiSinh/GetMaThiSinh?kythiId={1}&matruong={2}", API_URL, kythiId, matruong);
            var result = Task.Run(async () =>
            {
                return await GetAction(url);
            }).Result;
            return result;
        }


        public static string NopThiSinh(int kythiId, string matruong, int monthiId, string jsThiSinh)
        {
            try
            {
                string url = string.Format("{0}ThiSinh/NopThiSinh?kythiId={1}&matruong={2}&monthiId={3}&jsThiSinh={4}", API_URL, kythiId, matruong, monthiId, jsThiSinh);
                var result = Task.Run(async () =>
                {
                    return await PostAction(url, jsThiSinh);
                }).Result;
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return "";
            }



        }

        public static string NopThiSinh2(int kythiId, string matruong, int monthiId, string jsThiSinh)
        {
            try
            {
                ThiSinhDTO thiSinhDTO = new ThiSinhDTO
                {
                    kiThiID = kythiId,
                    monThiID = monthiId,
                    SchoolID = matruong,
                    jsThiSinh = jsThiSinh

                };

                string jsonExamPupil = Newtonsoft.Json.JsonConvert.SerializeObject(thiSinhDTO);
                string url = string.Format("{0}ThiSinh/NopThiSinh2", API_URL);
                var result = Task.Run(async () =>
                {
                    return await PostAction(url, jsonExamPupil);
                }).Result;
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return "";
            }



        }

        public static string Submit_Lop(string DotBaoCaoId,string jsClassAPIBO )
        {
            try
            {
                string url = string.Format("{0}lop?DotBaoCao=" + DotBaoCaoId, API_URL);
                var result = Task.Run(async () =>
                {
                    return await PostAction(url, jsClassAPIBO);
                }).Result;
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return "";
            }
        }

        public static string XoaThiSinh(List<string> mathisinh)
        {

            string url = string.Format("{0}ThiSinh/XoaThiSinh2", API_URL);
            string jsMaThiSinh = Newtonsoft.Json.JsonConvert.SerializeObject(mathisinh);
            var result = Task.Run(async () =>
            {
                return await PostAction(url, jsMaThiSinh);
            }).Result;
            return result;
        }

        public static string NopCanBo(int kythiId, string matruong, int loaicanbo, string jsCanBo)
        {
            string url = string.Format("{0}CanBo/NopCanBo?kythiId={1}&matruong={2}&loaicanbo={3}&jsCanBo={4}", API_URL, kythiId, matruong, loaicanbo, jsCanBo);
            var result = Task.Run(async () =>
            {
                return await PostAction(url, jsCanBo);
            }).Result;
            return result;
        }

        public static string NopCanBo2(int kythiId, string matruong, int loaicanbo, string jsCanBo)
        {
            NopCanBoDTO nopCanBoDTO = new NopCanBoDTO
            {
                KythiId=kythiId,
                LoaiCanBo=loaicanbo,
                MaTruong=matruong,
                JsCanBo=jsCanBo
            };

            string jsonNopCanBo = Newtonsoft.Json.JsonConvert.SerializeObject(nopCanBoDTO);
            string url = string.Format("{0}CanBo/NopCanBo2", API_URL);
            var result = Task.Run(async () =>
            {
                return await PostAction(url, jsonNopCanBo);
            }).Result;
            return result;
        }

        public static List<CanBo> GetCanBo(int kythiId, string matruong, int loaicanbo)
        {

            string url = string.Format("{0}CanBo/GetCanBo?kythiId={1}&matruong={2}&loaicanbo={3}", API_URL, kythiId, matruong, loaicanbo);
            var result = Task.Run(async () =>
            {
                return await GetAction(url);
            }).Result;

            var jsonList = Newtonsoft.Json.JsonConvert.DeserializeObject<RootCanBo>(result);
            if (jsonList == null)
            {
                return new List<CanBo>();
            }
            var lstVal = jsonList.Table.ToList();

            return lstVal;

        }

        public static string XoaCanbo(string macanbo)
        {
            string url = string.Format("{0}CanBo/XoaCanbo?macanbo={1}", API_URL, macanbo);
            var result = Task.Run(async () =>
            {
                return await PostAction(url, "");
            }).Result;
            return result;
        }

        public static string XoaCanbo2(List<string> lstMaCanBo)
        {
            if (lstMaCanBo == null || lstMaCanBo.Count == 0)
            {
                return "";
            }
            string jsMaCanBo = Newtonsoft.Json.JsonConvert.SerializeObject(lstMaCanBo);
            string url = string.Format("{0}CanBo/XoaCanbo2", API_URL);
            var result = Task.Run(async () =>
            {
                return await PostAction(url, jsMaCanBo);
            }).Result;
            return result;
        }

        private static string Encode(string pass, string schoolCode)
        {
            byte[] encodedBytes;
            using (var md5 = new System.Security.Cryptography.MD5CryptoServiceProvider())
            {
                var originalBytes = Encoding.Default.GetBytes("[" + pass + "$" + schoolCode
                    + "==>" + DateTime.Now.ToString("dd-MM-yyyy") + "]");
                encodedBytes = md5.ComputeHash(originalBytes);
            }
            return Convert.ToBase64String(encodedBytes).Replace("=", "");
        }

        public static string GenerateURL(string schoolCode)
        {
            string key = Encode("--As)+SA*$#_2", schoolCode);
            string url = "http://quanly.hcm.edu.vn/intheduthi/?id=" + schoolCode.Trim() + "&key=" + key;
            return url;
        }

        public static string GetDanToc()
        {
            string url = string.Format("{0}DanhMuc/GetDanToc", API_URL);
            var result = Task.Run(async () =>
            {
                return await GetAction(url);
            }).Result;
            return result;
        }

        public static string GetHuyen()
        {
            string url = string.Format("http://api.hocsinh.hcm.edu.vn/api/danhmuc?type=Huyen");
            var result = Task.Run(async () =>
            {
                return await GetAction(url);
            }).Result;
            return result;
        }

        public static string GetTrinhDoDaoTao()
        {
            string url = string.Format("{0}DanhMuc/GetTrinhDoDaoTao", API_URL);
            var result = Task.Run(async () =>
            {
                return await GetAction(url);
            }).Result;
            return result;
        }
    }
}
