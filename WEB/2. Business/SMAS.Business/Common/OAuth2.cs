﻿using log4net;
using Newtonsoft.Json;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace SMAS.Business.Common.Util
{
    public class OAuth2
    {
        private static readonly ILog logger = LogManager.GetLogger("SSO");


        protected static async Task<string> GetAction(HttpClient client, string url, HttpContent content)
        {
            try
            {
                var response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return "";

            }

        }

        protected static async Task<string> PostAction(HttpClient client, string url, HttpContent content)
        {
            try
            {
                var response = await client.PostAsync(url, content);
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return "";
            }

        }

        protected static async Task<string> PostActionJson(HttpClient client, string url, string jsContent)
        {
            try
            {
                var content = new StringContent(jsContent, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, content);
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
            catch (HttpRequestException hre)
            {
                logger.Error(hre.Message, hre);
                return "";
            }

        }


        public static TokenBO Login(string userName, string password)
        {
            var client = new HttpClient();

            try
            {
                var values = new Dictionary<string, string>{
                        { "grant_type", OAuthConstant.GRANT_TYPE_PASSWORD },
                        { "client_id", OAuthConstant.CLIENT_ID},
                        { "client_secret", OAuthConstant.CLIENT_SECRET },
                        { "scope", OAuthConstant.SCOPE },
                        { "username", userName },
                        { "password", password }
                    };

                client.BaseAddress = new Uri(OAuthConstant.VIETTEL_STUDY_API);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                HttpContent content = new FormUrlEncodedContent(values);
                var response = Task.Run(async () =>
                {
                    return await PostAction(client, OAuthConstant.PARA_ACCESS_TOKEN, content);
                }).Result;

                TokenBO token = JsonConvert.DeserializeObject<TokenBO>(response);
                return token;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new TokenBO();
            }
            finally
            {
                client.Dispose();
            }


        }

        /// <summary>
        /// Doi mat khau tren SSO
        /// </summary>
        /// <param name="accesstokken"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public static void ChangePass(string userName, string oldPassword, string newPassword)
        {
            var client = new HttpClient();
            try
            {
                var values = new Dictionary<string, string>{
                        { "clientId", OAuthConstant.CLIENT_ID },
                        { "clientSecret", OAuthConstant.CLIENT_SECRET },
                        { "username", userName},
                        { "oldPassword", oldPassword},
                        { "newPassword", newPassword}
                    };

                client.BaseAddress = new Uri(OAuthConstant.VIETTEL_STUDY_API);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                HttpContent content = new FormUrlEncodedContent(values);
                var response = Task.Run(async () =>
                {
                    return await PostAction(client, OAuthConstant.PARA_CHANGE_PASS, content);
                }).Result;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                client.Dispose();
            }

        }

        public static TokenBO GetAccessToken(string authorizationCode)
        {
            
            var client = new HttpClient();

            try
            {
                //string url=$"{OAuthConstant.URL_VNSUTYDY}api/Core/OAuth/AuthCode/accessToken?client_id={}&client_secret=CLIENT_SECRET&grant_type=authorization_code&code=AUTHORIZATION_CODE&redirect_uri=CALLBACK_URL"
                var values = new Dictionary<string, string>{

                        { "client_id", OAuthConstant.CLIENT_ID},
                        { "client_secret", OAuthConstant.CLIENT_SECRET },
                        { "grant_type", "authorization_code" },
                        { "code", authorizationCode},
                        { "redirect_uri", OAuthConstant.URL_CALL_BACK }
                    };

                client.BaseAddress = new Uri(OAuthConstant.VIETTEL_STUDY_API);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                HttpContent content = new FormUrlEncodedContent(values);

                var response = Task.Run(async () =>
                {
                    return await PostAction(client, OAuthConstant.PARA_ACCESS_TOKEN, content);
                }).Result;
                TokenBO token = JsonConvert.DeserializeObject<TokenBO>(response);
                return token;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new TokenBO();
            }
            finally
            {
                client.Dispose();
            }
        }


        /// <summary>
        /// Lay thong tin username sau khi da co access_token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static TokenUser GetUserName(TokenBO token)
        {
            if (token == null)
            {
                return null;
            }
            var client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(OAuthConstant.VIETTEL_STUDY_API);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                var values = new Dictionary<string, string>{

                        { "accessToken",token.access_token},
                    };
                HttpContent content = new FormUrlEncodedContent(values);

                //string para = string.Format("api/Core/OAuth/API/me?accessToken=", token.access_token);

                var response = Task.Run(async () =>
                {
                    return await PostAction(client, OAuthConstant.PARA_GET_USERNAME, content);
                }).Result;
                TokenUser tokenUser = JsonConvert.DeserializeObject<TokenUser>(response);
                return tokenUser;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message,ex);
                return new TokenUser();
            }
            finally
            {
                client.Dispose();
            }
        }


        public static async void AddUser(OAuthUserBO userBO)
        {
            
            using (var client = new HttpClient())
            {
                try
                {
                    var values = new Dictionary<string, string>{
                        { "clientid", OAuthConstant.CLIENT_ID },
                        { "clientsecret", OAuthConstant.CLIENT_SECRET },
                        { "code", userBO.UserName },
                        { "password", userBO.Password },
                        { "email", userBO.Email },
                        { "fullName", userBO.FullName },
                        { "phone", userBO.Phone },
                        { "address", userBO.Address },
                        { "avatar", userBO.Avatar },
                        { "birthDate", userBO.BirthDate },
                        { "gender", userBO.Gender },
                        { "province", userBO.Province },
                        { "district", userBO.District },
                        { "school", userBO.UnitName },
                        { "jobPosition", userBO.JobPosition }
                    };

                    client.BaseAddress = new Uri(OAuthConstant.VIETTEL_STUDY_API);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    HttpContent content = new FormUrlEncodedContent(values);
                    var response = await client.PostAsync(OAuthConstant.PARA_ADD_USER, content);
                    if (response.IsSuccessStatusCode)
                    {
                        var responseString = await response.Content.ReadAsStringAsync();
                        LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, "userName=" + userBO.UserName, "Tao user", "");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message,ex);
                }
                finally
                {
                    client.Dispose();
                }
            }
        }

        public static void AddUser(List<OAuthUserBO> lstUser)
        {
            if (lstUser == null)
            {
                return;
            }
            foreach (var item in lstUser)
            {
                AddUser(item);
            }
        }

    }
}