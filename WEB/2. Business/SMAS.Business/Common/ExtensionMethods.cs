﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Microsoft.Security.Application;
using System.Configuration;
using System.Web.Configuration;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SMAS.Business.Common.Extension
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get Guild value
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>08/05/2013</date>
        /// <param name="val"></param>
        /// <returns></returns>
        public static long GetLong(object val, long def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is long)
            {
                return (long)val;
            }
            else
            {
                try
                {
                    return long.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception(" GetInt error, object is not a Decimal value");
                }
            }
        }

        /// <summary>
        /// Get Guild value
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>08/05/2013</date>
        /// <param name="val"></param>
        /// <returns></returns>
        public static long GetLong(this Dictionary<string, object> dic, string key, long def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetLong(dic[key], def);
        }
        /// <summary>
        /// fast convert string to int
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>09/04/2013</date>
        public static int IntParseFast(this string value)
        {
            int result = 0;
            for (int i = 0; i < value.Length; i++)
            {
                result = 10 * result + (value[i] - 48);
            }
            return result;
        }

        /// <summary>
        /// get 8 character random
        /// </summary>
        /// <author>HaiVT 09/08/2013</author>
        /// <returns></returns>
        public static string GetRandomCharacter(this string value)
        {
            Random random = new Random();
            char[] arrChar = new char[8];
            for (int i = 0; i < arrChar.Length; i++)
            {
                arrChar[i] = value[random.Next(value.Length)];
            }
            return new string(arrChar);
        }

        private static string _nChar = "\n";
        private static string _rChar = "\r";
        private static string _rnChar = "\r\n";
        private static string _brChar = "@|br|";
        private static string _brTag = "<br/>";
        /// <summary>
        /// get safe html with new line
        /// </summary>
        /// <author>HaiVT - 14/08/2013</author>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetSafeHtmlNewLine(this string value)
        {
            value = value.Replace(_rnChar, _brChar).Replace(_nChar, _brChar).Replace(_rChar, _brChar);
            value = Sanitizer.GetSafeHtmlFragment(value);
            value = value.Replace(_brChar, _brTag);
            return value;
        }
        //public static T GetObject<T>(this Dictionary<string, object> dic, string key) where T : new()
        //{
        //    if (!dic.ContainsKey(key))
        //    {
        //        return new T();
        //    }
        //    return (T)dic[key];
        //}
        public static Guid? GetGuid(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            return GetGuid(dic[key]);
        }
        public static Guid? GetGuid(object val)
        {
            if (val == null)
            {
                return null;
            }
            else if (val is Guid)
            {
                return (Guid)val;
            }
            else
            {
                try
                {
                    return Guid.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetGuid error, object is not a Guid value");
                }
            }
        }
        public static string GetString(this Dictionary<string, object> dic, string key, string def = "")
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetString(dic[key], def);
        }

        public static string GetString(object val, string def = "")
        {
            if (val == null)
            {
                return def;
            }

            if (val is string)
            {
                return (string)val;
            }
            else
            {
                throw new Exception("GetString error, object is not a string value");
            }
        }

        /// <summary>
        /// get string list 
        /// </summary>
        /// <author date="2013/11/29">HaiVT</author>
        public static List<string> GetStringList(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return GetStringList(null);
            }

            return GetStringList(dic[key]);
        }

        /// <summary>
        /// get string list
        /// </summary>
        /// <author date="2013/11/29">HaiVT</author>
        public static List<string> GetStringList(object val)
        {
            List<string> def = new List<string>();
            if (val == null)
            {
                return def;
            }

            if (val is List<string>)
            {
                return (List<string>)val;
            }
            else
            {
                throw new Exception("SMAS.Business.Common.Utils: GetStringList error, object is not a List<string> value");
            }
        }

        public static int GetInt(this Dictionary<string, object> dic, string key, int def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetInt(dic[key], def);
        }

        public static List<int> GetIntList(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return GetIntList(null);
            }

            return GetIntList(dic[key]);
        }

        public static List<int> GetIntList(object val)
        {
            List<int> def = new List<int>();
            if (val == null)
            {
                return def;
            }

            if (val is List<int>)
            {
                return (List<int>)val;
            }
            else
            {
                throw new Exception("SMAS.Business.Common.Utils: GetIntList error, object is not a List<Int> value");
            }
        }

        public static bool ? GetNullableBool(this Dictionary<string, object> dic, string key, bool ? def = false)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetNullableBool(dic[key], def);
        }

        public static bool? GetNullableBool(object val, bool? def = true)
        {
            if (val == null)
            {
                return def;
            }

            if (val is bool)
            {
                return (bool)val;
            }
            else
            {
                throw new Exception("GetBool error, object is not a bool value");
            }
        }

        public static bool GetBool(this Dictionary<string, object> dic, string key, bool def = false)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetBool(dic[key], def);
        }

        public static bool GetBool(object val, bool def = false)
        {
            if (val == null)
            {
                return def;
            }

            if (val is bool)
            {
                return (bool)val;
            }
            else
            {
                throw new Exception("GetBool error, object is not a bool value");
            }
        }

        public static int GetInt(object val, int def = 0)
        {
            if ((val == null) || String.IsNullOrEmpty(val.ToString()))
            {
                return def;
            }

            if (val is int)
            {
                return (int)val;
            }
            else
            {
                try
                {
                    return Int32.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetInt error, object is not a Int value");
                }
            }
        }

        public static byte GetByte(object val, byte def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is byte)
            {
                return (byte)val;
            }
            else
            {
                try
                {
                    return byte.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("Getbyte error, object is not a byte value");
                }
            }
        }

        public static byte GetByte(this Dictionary<string, object> dic, string key, byte def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetByte(dic[key], def);
        }

        public static DateTime? GetDateTime(this Dictionary<string, object> dic, string key, DateTime? def = null)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetDateTime(dic[key], def);
        }

        public static DateTime? GetDateTime(object val, DateTime? def = null)
        {
            if (val == null)
            {
                return null;
            }

            if (val is DateTime)
            {
                return (DateTime)val;
            }
            else
            {
                throw new Exception("GetDateTime error, object is not a DateTime value");
            }
        }


        public static short? GetNullableShort(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetShort(dic[key]);
        }

        public static short GetShort(object val, short def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is short)
            {
                return (short)val;
            }
            else
            {
                try
                {
                    return short.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetSchort error, object is not a int value");
                }
            }
        }

        public static short GetShort(this Dictionary<string, object> dic, string key, short def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetShort(dic[key], def);
        }

        public static int? GetNullableInt(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetInt(dic[key]);
        }

        public static decimal GetDecimal(object val, decimal def = 0)
        {
            if ((val == null) || String.IsNullOrEmpty(val.ToString()))
            {
                return def;
            }

            if (val is decimal)
            {
                return (decimal)val;
            }
            else
            {
                try
                {
                    return decimal.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetDecimal error, object is not a decimal value");
                }
            }
        }
        public static decimal GetDecimal(this Dictionary<string, object> dic, string key, decimal def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetDecimal(dic[key], def);
        }

        /// <summary>
        /// chuyển thành tiếng việt không dấu
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        //public static string StripVNSign(this string text)
        //{
        //    if (string.IsNullOrWhiteSpace(text))
        //    {
        //        return string.Empty;
        //    }

        //    //'Dấu Ngang
        //    text = text.Replace("A", "A")
        //    .Replace("a", "a")
        //    .Replace("Ă", "A")
        //    .Replace("ă", "a")
        //    .Replace("Â", "A")
        //    .Replace("â", "a")
        //    .Replace("E", "E")
        //    .Replace("e", "e")
        //    .Replace("Ê", "E")
        //    .Replace("ê", "e")
        //    .Replace("I", "I")
        //    .Replace("i", "i")
        //    .Replace("O", "O")
        //    .Replace("o", "o")
        //    .Replace("Ô", "O")
        //    .Replace("ô", "o")
        //    .Replace("Ơ", "O")
        //    .Replace("ơ", "o")
        //    .Replace("U", "U")
        //    .Replace("u", "u")
        //    .Replace("Ư", "U")
        //    .Replace("ư", "u")
        //    .Replace("Y", "Y")
        //    .Replace("y", "y")

        //    //    'Dấu Huyền
        //    .Replace("À", "A")
        //    .Replace("à", "a")
        //    .Replace("Ằ", "A")
        //    .Replace("ằ", "a")
        //    .Replace("Ầ", "A")
        //    .Replace("ầ", "a")
        //    .Replace("È", "E")
        //    .Replace("è", "e")
        //    .Replace("Ề", "E")
        //    .Replace("ề", "e")
        //    .Replace("Ì", "I")
        //    .Replace("ì", "i")
        //    .Replace("Ò", "O")
        //    .Replace("ò", "o")
        //    .Replace("Ồ", "O")
        //    .Replace("ồ", "o")
        //    .Replace("Ờ", "O")
        //    .Replace("ờ", "o")
        //    .Replace("Ù", "U")
        //    .Replace("ù", "u")
        //    .Replace("Ừ", "U")
        //    .Replace("ừ", "u")
        //    .Replace("Ỳ", "Y")
        //    .Replace("ỳ", "y")

        //    //'Dấu Sắc
        //    .Replace("Á", "A")
        //    .Replace("á", "a")
        //    .Replace("Ắ", "A")
        //    .Replace("ắ", "a")
        //    .Replace("Ấ", "A")
        //    .Replace("ấ", "a")
        //    .Replace("É", "E")
        //    .Replace("é", "e")
        //    .Replace("Ế", "E")
        //    .Replace("ế", "e")
        //    .Replace("Í", "I")
        //    .Replace("í", "i")
        //    .Replace("Ó", "O")
        //    .Replace("ó", "o")
        //    .Replace("Ố", "O")
        //    .Replace("ố", "o")
        //    .Replace("Ớ", "O")
        //    .Replace("ớ", "o")
        //    .Replace("Ú", "U")
        //    .Replace("ú", "u")
        //    .Replace("Ứ", "U")
        //    .Replace("ứ", "u")
        //    .Replace("Ý", "Y")
        //    .Replace("ý", "y")

        //    //'Dấu Hỏi
        //    .Replace("Ả", "A")
        //    .Replace("ả", "a")
        //    .Replace("Ẳ", "A")
        //    .Replace("ẳ", "a")
        //    .Replace("Ẩ", "A")
        //    .Replace("ẩ", "a")
        //    .Replace("Ẻ", "E")
        //    .Replace("ẻ", "e")
        //    .Replace("Ể", "E")
        //    .Replace("ể", "e")
        //    .Replace("Ỉ", "I")
        //    .Replace("ỉ", "i")
        //    .Replace("Ỏ", "O")
        //    .Replace("ỏ", "o")
        //    .Replace("Ổ", "O")
        //    .Replace("ổ", "o")
        //    .Replace("Ở", "O")
        //    .Replace("ở", "o")
        //    .Replace("Ủ", "U")
        //    .Replace("ủ", "u")
        //    .Replace("Ử", "U")
        //    .Replace("ử", "u")
        //    .Replace("Ỷ", "Y")
        //    .Replace("ỷ", "y")

        //    //'Dấu Ngã   
        //    .Replace("Ã", "A")
        //    .Replace("ã", "a")
        //    .Replace("Ẵ", "A")
        //    .Replace("ẵ", "a")
        //    .Replace("Ẫ", "A")
        //    .Replace("ẫ", "a")
        //    .Replace("Ẽ", "E")
        //    .Replace("ẽ", "e")
        //    .Replace("Ễ", "E")
        //    .Replace("ễ", "e")
        //    .Replace("Ĩ", "I")
        //    .Replace("ĩ", "i")
        //    .Replace("Õ", "O")
        //    .Replace("õ", "o")
        //    .Replace("Ỗ", "O")
        //    .Replace("ỗ", "o")
        //    .Replace("Ỡ", "O")
        //    .Replace("ỡ", "o")
        //    .Replace("Ũ", "U")
        //    .Replace("ũ", "u")
        //    .Replace("Ữ", "U")
        //    .Replace("ữ", "u")
        //    .Replace("Ỹ", "Y")
        //    .Replace("ỹ", "y")

        //    //'Dẫu Nặng
        //    .Replace("Ạ", "A")
        //    .Replace("ạ", "a")
        //    .Replace("Ặ", "A")
        //    .Replace("ặ", "a")
        //    .Replace("Ậ", "A")
        //    .Replace("ậ", "a")
        //    .Replace("Ẹ", "E")
        //    .Replace("ẹ", "e")
        //    .Replace("Ệ", "E")
        //    .Replace("ệ", "e")
        //    .Replace("Ị", "I")
        //    .Replace("ị", "i")
        //    .Replace("Ọ", "O")
        //    .Replace("ọ", "o")
        //    .Replace("Ộ", "O")
        //    .Replace("ộ", "o")
        //    .Replace("Ợ", "O")
        //    .Replace("ợ", "o")
        //    .Replace("Ụ", "U")
        //    .Replace("ụ", "u")
        //    .Replace("Ự", "U")
        //    .Replace("ự", "u")
        //    .Replace("Ỵ", "Y")
        //    .Replace("ỵ", "y")
        //    .Replace("Đ", "D")
        //    .Replace("Ð", "D")
        //    .Replace("đ", "d");
        //    return text;
        //}

        /// <summary>
        /// viethd31: Hàm cắt dấu mới thay thế hàm cũ
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSign(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            List<string> ListAllVNChars = new List<string>{
                "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ",
			"ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê",
			"ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ",
			"ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở",
			"ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ",
			"ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ",
			"Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ",
			"Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò",
			"Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ",
			"Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử",
			"Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ", "ê", "ù", "à",

			"á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
			"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê",
			"ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò",
			"ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
			"ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư",
			"ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ",
			"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ",
			"Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ",
			"Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ",
			"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ",
			"Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ",
			"Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
			"Đ"
            };

            List<string> ListAllUnsignChars = new List<string>{
                "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
			"a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e",
			"e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o",
			"o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
			"o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y",
			"y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A",
			"A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E",
			"E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O",
			"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
			"O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
			"U", "Y", "Y", "Y", "Y", "Y", "D", "e", "u", "a",

			"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
			"a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
			"e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o",
			"o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u",
			"u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y",
			"d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
			"A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E",
			"E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O",
			"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U",
			"U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y",
			"Y", "Y", "D"
            };

            for (int i = 0; i < ListAllVNChars.Count; i++)
            {
                text = text.Replace(ListAllVNChars[i], ListAllUnsignChars[i]);
            }
            return text;
        }
        public static string RemoveUnprintableChar(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            for (int i = text.Length - 1; i >= 0; i--)
            {
                string s = text[i].ToString().StripVNSign();
                if (!string.IsNullOrWhiteSpace(s) && (s[0] < 32 || s[0] > 126))
                {
                    text = text.Remove(i, 1);
                }
            }

            return text;
        }

        /// <summary>
        /// Loại bỏ dấu bảng mã Unicode tổ hợp
        /// </summary>
        /// <author>Anhvd9</author>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSignComposed(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            //'Dấu Ngang
            text = text.Replace("A", "A")
            .Replace("a", "a")
            .Replace("Ă", "A")
            .Replace("ă", "a")
            .Replace("Â", "A")
            .Replace("â", "a")
            .Replace("E", "E")
            .Replace("e", "e")
            .Replace("Ê", "E")
            .Replace("ê", "e")
            .Replace("I", "I")
            .Replace("i", "i")
            .Replace("O", "O")
            .Replace("o", "o")
            .Replace("Ô", "O")
            .Replace("ô", "o")
            .Replace("Ơ", "O")
            .Replace("ơ", "o")
            .Replace("U", "U")
            .Replace("u", "u")
            .Replace("Ư", "U")
            .Replace("ư", "u")
            .Replace("Y", "Y")
            .Replace("y", "y")

            //    'Dấu Huyền
            .Replace("À", "A")
            .Replace("à", "a")
            .Replace("Ằ", "A")
            .Replace("ằ", "a")
            .Replace("Ầ", "A")
            .Replace("ầ", "a")
            .Replace("È", "E")
            .Replace("è", "e")
            .Replace("Ề", "E")
            .Replace("ề", "e")
            .Replace("Ì", "I")
            .Replace("ì", "i")
            .Replace("Ò", "O")
            .Replace("ò", "o")
            .Replace("Ồ", "O")
            .Replace("ồ", "o")
            .Replace("Ờ", "O")
            .Replace("ờ", "o")
            .Replace("Ù", "U")
            .Replace("ù", "u")
            .Replace("Ừ", "U")
            .Replace("ừ", "u")
            .Replace("Ỳ", "Y")
            .Replace("ỳ", "y")

            //'Dấu Sắc
            .Replace("Á", "A")
            .Replace("á", "a")
            .Replace("Ắ", "A")
            .Replace("ắ", "a")
            .Replace("Ấ", "A")
            .Replace("ấ", "a")
            .Replace("É", "E")
            .Replace("é", "e")
            .Replace("Ế", "E")
            .Replace("ế", "e")
            .Replace("Í", "I")
            .Replace("í", "i")
            .Replace("Ó", "O")
            .Replace("ó", "o")
            .Replace("Ố", "O")
            .Replace("ố", "o")
            .Replace("Ớ", "O")
            .Replace("ớ", "o")
            .Replace("Ú", "U")
            .Replace("ú", "u")
            .Replace("Ứ", "U")
            .Replace("ứ", "u")
            .Replace("Ý", "Y")
            .Replace("ý", "y")

            //'Dấu Hỏi
            .Replace("Ả", "A")
            .Replace("ả", "a")
            .Replace("Ẳ", "A")
            .Replace("ẳ", "a")
            .Replace("Ẩ", "A")
            .Replace("ẩ", "a")
            .Replace("Ẻ", "E")
            .Replace("ẻ", "e")
            .Replace("Ể", "E")
            .Replace("ể", "e")
            .Replace("Ỉ", "I")
            .Replace("ỉ", "i")
            .Replace("Ỏ", "O")
            .Replace("ỏ", "o")
            .Replace("Ổ", "O")
            .Replace("ổ", "o")
            .Replace("Ở", "O")
            .Replace("ở", "o")
            .Replace("Ủ", "U")
            .Replace("ủ", "u")
            .Replace("Ử", "U")
            .Replace("ử", "u")
            .Replace("Ỷ", "Y")
            .Replace("ỷ", "y")

            //'Dấu Ngã   
            .Replace("Ã", "A")
            .Replace("ã", "a")
            .Replace("Ẵ", "A")
            .Replace("ẵ", "a")
            .Replace("Ẫ", "A")
            .Replace("ẫ", "a")
            .Replace("Ẽ", "E")
            .Replace("ẽ", "e")
            .Replace("Ễ", "E")
            .Replace("ễ", "e")
            .Replace("Ĩ", "I")
            .Replace("ĩ", "i")
            .Replace("Õ", "O")
            .Replace("õ", "o")
            .Replace("Ỗ", "O")
            .Replace("ỗ", "o")
            .Replace("Ỡ", "O")
            .Replace("ỡ", "o")
            .Replace("Ũ", "U")
            .Replace("ũ", "u")
            .Replace("Ữ", "U")
            .Replace("ữ", "u")
            .Replace("Ỹ", "Y")
            .Replace("ỹ", "y")

            //'Dẫu Nặng
            .Replace("Ạ", "A")
            .Replace("ạ", "a")
            .Replace("Ặ", "A")
            .Replace("ặ", "a")
            .Replace("Ậ", "A")
            .Replace("ậ", "a")
            .Replace("Ẹ", "E")
            .Replace("ẹ", "e")
            .Replace("Ệ", "E")
            .Replace("ệ", "e")
            .Replace("Ị", "I")
            .Replace("ị", "i")
            .Replace("Ọ", "O")
            .Replace("ọ", "o")
            .Replace("Ộ", "O")
            .Replace("ộ", "o")
            .Replace("Ợ", "O")
            .Replace("ợ", "o")
            .Replace("Ụ", "U")
            .Replace("ụ", "u")
            .Replace("Ự", "U")
            .Replace("ự", "u")
            .Replace("Ỵ", "Y")
            .Replace("ỵ", "y")
            .Replace("D", "D")
            .Replace("Đ", "D")
            .Replace("đ", "d");
            return text;
        }

        /// <summary>
        /// chuyển thành tiếng việt không dấu va bo khoang trang
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSignAndSpace(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            //'Dấu Ngang
            text = text.Replace("A", "A")
            .Replace("a", "a")
            .Replace("Ă", "A")
            .Replace("ă", "a")
            .Replace("Â", "A")
            .Replace("â", "a")
            .Replace("E", "E")
            .Replace("e", "e")
            .Replace("Ê", "E")
            .Replace("ê", "e")
            .Replace("I", "I")
            .Replace("i", "i")
            .Replace("O", "O")
            .Replace("o", "o")
            .Replace("Ô", "O")
            .Replace("ô", "o")
            .Replace("Ơ", "O")
            .Replace("ơ", "o")
            .Replace("U", "U")
            .Replace("u", "u")
            .Replace("Ư", "U")
            .Replace("ư", "u")
            .Replace("Y", "Y")
            .Replace("y", "y")

            //    'Dấu Huyền
            .Replace("À", "A")
            .Replace("à", "a")
            .Replace("Ằ", "A")
            .Replace("ằ", "a")
            .Replace("Ầ", "A")
            .Replace("ầ", "a")
            .Replace("È", "E")
            .Replace("è", "e")
            .Replace("Ề", "E")
            .Replace("ề", "e")
            .Replace("Ì", "I")
            .Replace("ì", "i")
            .Replace("Ò", "O")
            .Replace("ò", "o")
            .Replace("Ồ", "O")
            .Replace("ồ", "o")
            .Replace("Ờ", "O")
            .Replace("ờ", "o")
            .Replace("Ù", "U")
            .Replace("ù", "u")
            .Replace("Ừ", "U")
            .Replace("ừ", "u")
            .Replace("Ỳ", "Y")
            .Replace("ỳ", "y")

            //'Dấu Sắc
            .Replace("Á", "A")
            .Replace("á", "a")
            .Replace("Ắ", "A")
            .Replace("ắ", "a")
            .Replace("Ấ", "A")
            .Replace("ấ", "a")
            .Replace("É", "E")
            .Replace("é", "e")
            .Replace("Ế", "E")
            .Replace("ế", "e")
            .Replace("Í", "I")
            .Replace("í", "i")
            .Replace("Ó", "O")
            .Replace("ó", "o")
            .Replace("Ố", "O")
            .Replace("ố", "o")
            .Replace("Ớ", "O")
            .Replace("ớ", "o")
            .Replace("Ú", "U")
            .Replace("ú", "u")
            .Replace("Ứ", "U")
            .Replace("ứ", "u")
            .Replace("Ý", "Y")
            .Replace("ý", "y")

            //'Dấu Hỏi
            .Replace("Ả", "A")
            .Replace("ả", "a")
            .Replace("Ẳ", "A")
            .Replace("ẳ", "a")
            .Replace("Ẩ", "A")
            .Replace("ẩ", "a")
            .Replace("Ẻ", "E")
            .Replace("ẻ", "e")
            .Replace("Ể", "E")
            .Replace("ể", "e")
            .Replace("Ỉ", "I")
            .Replace("ỉ", "i")
            .Replace("Ỏ", "O")
            .Replace("ỏ", "o")
            .Replace("Ổ", "O")
            .Replace("ổ", "o")
            .Replace("Ở", "O")
            .Replace("ở", "o")
            .Replace("Ủ", "U")
            .Replace("ủ", "u")
            .Replace("Ử", "U")
            .Replace("ử", "u")
            .Replace("Ỷ", "Y")
            .Replace("ỷ", "y")

            //'Dấu Ngã   
            .Replace("Ã", "A")
            .Replace("ã", "a")
            .Replace("Ẵ", "A")
            .Replace("ẵ", "a")
            .Replace("Ẫ", "A")
            .Replace("ẫ", "a")
            .Replace("Ẽ", "E")
            .Replace("ẽ", "e")
            .Replace("Ễ", "E")
            .Replace("ễ", "e")
            .Replace("Ĩ", "I")
            .Replace("ĩ", "i")
            .Replace("Õ", "O")
            .Replace("õ", "o")
            .Replace("Ỗ", "O")
            .Replace("ỗ", "o")
            .Replace("Ỡ", "O")
            .Replace("ỡ", "o")
            .Replace("Ũ", "U")
            .Replace("ũ", "u")
            .Replace("Ữ", "U")
            .Replace("ữ", "u")
            .Replace("Ỹ", "Y")
            .Replace("ỹ", "y")

            //'Dẫu Nặng
            .Replace("Ạ", "A")
            .Replace("ạ", "a")
            .Replace("Ặ", "A")
            .Replace("ặ", "a")
            .Replace("Ậ", "A")
            .Replace("ậ", "a")
            .Replace("Ẹ", "E")
            .Replace("ẹ", "e")
            .Replace("Ệ", "E")
            .Replace("ệ", "e")
            .Replace("Ị", "I")
            .Replace("ị", "i")
            .Replace("Ọ", "O")
            .Replace("ọ", "o")
            .Replace("Ộ", "O")
            .Replace("ộ", "o")
            .Replace("Ợ", "O")
            .Replace("ợ", "o")
            .Replace("Ụ", "U")
            .Replace("ụ", "u")
            .Replace("Ự", "U")
            .Replace("ự", "u")
            .Replace("Ỵ", "Y")
            .Replace("ỵ", "y")
            .Replace("Đ", "D")
            .Replace("Ð", "D")
            .Replace("đ", "d")
            .Replace(" ", "");
            return text;
        }

        public static string ConvertLineToSpace(string value)
        {
            return value.Replace(Environment.NewLine, GlobalConstantsEdu.SPACE).Replace(GlobalConstantsEdu.R_CHAR, GlobalConstantsEdu.SPACE).Replace(GlobalConstantsEdu.N_CHAR, GlobalConstantsEdu.SPACE);
        }

        private static List<string> SplitStringToList(string inputText, char cSlpit)
        {
            if (string.IsNullOrEmpty(inputText))
            {
                return null;
            }
            return new List<string>(inputText.Split(new char[] { cSlpit }));
        }

        /// <summary>
        /// kiem tra so dien thoai, true neu hop le, false neu khong hop le
        /// </summary>
        /// <author date="2014/01/10">TuTV4</author>
        /// <param name="number">so dien thoai</param>
        /// <returns>true neu hop le false neu khong hop le</returns>
        public static bool CheckMobileNumber(this string number)
        {
            List<string> headerNumbers = SplitStringToList(WebConfigurationManager.AppSettings[GlobalConstantsEdu.PRE_NUMBER].ToString(), GlobalConstantsEdu.COMMON_COMMA);
            List<string> headerLen = null;
            

            if (String.IsNullOrEmpty(number))
            {
                return false;
            }

            if ((number.Length < 9) || (number.Length > 12))
            {
                return false;
            }

            if ((headerNumbers == null) || (headerNumbers.Count == 0))
            {
                return false;
            }

            if (number.Length == 9)
            {
                string strPreNumberLen9 = WebConfigurationManager.AppSettings[GlobalConstantsEdu.PRE_NUMBER_LEN_9];
                if (!string.IsNullOrEmpty(strPreNumberLen9))
                {
                    headerLen = new List<string>(strPreNumberLen9.Split(new char[] {GlobalConstantsEdu.COMMON_COMMA}));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 10)
            {
                string strPreNumberLen10 = WebConfigurationManager.AppSettings[GlobalConstantsEdu.PRE_NUMBER_LEN_10];
                if (!string.IsNullOrEmpty(strPreNumberLen10))
                {
                    headerLen = new List<string>(strPreNumberLen10.Split(new char[] {GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 11)
            {
                string strPreNumberLen11 = WebConfigurationManager.AppSettings[GlobalConstantsEdu.PRE_NUMBER_LEN_11];
                if (!string.IsNullOrEmpty(strPreNumberLen11))
                {
                    headerLen = new List<string>(strPreNumberLen11.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                string strPreNumberLen12 = WebConfigurationManager.AppSettings[GlobalConstantsEdu.PRE_NUMBER_LEN_12];
                if (!string.IsNullOrEmpty(strPreNumberLen12))
                {
                    headerLen = new List<string>(strPreNumberLen12.Split(new char[] {GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// kiem tra so dien thoai, true neu hop le, false neu khong hop le
        /// </summary>
        /// <author date="2014/01/10">TuTV4</author>
        /// <param name="number">so dien thoai</param>
        /// <returns>true neu hop le false neu khong hop le</returns>
        public static bool CheckMobileNumberVT(this string number)
        {
            List<string> headerLen = null;

            if (String.IsNullOrEmpty(number))
            {
                return false;
            }

            if ((number.Length < 9) || (number.Length > 12))
            {
                return false;
            }

            if (number.Length == 9)
            {
                string strPreNumberLen9 = WebConfigurationManager.AppSettings["PrefixViettel9"];
                if (!string.IsNullOrEmpty(strPreNumberLen9))
                {
                    headerLen = new List<string>(strPreNumberLen9.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 10)
            {
                string strPreNumberLen10 = WebConfigurationManager.AppSettings["PrefixViettel10"];
                if (!string.IsNullOrEmpty(strPreNumberLen10))
                {
                    headerLen = new List<string>(strPreNumberLen10.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 11)
            {
                string strPreNumberLen11 = WebConfigurationManager.AppSettings["PrefixViettel11"];
                if (!string.IsNullOrEmpty(strPreNumberLen11))
                {
                    headerLen = new List<string>(strPreNumberLen11.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                string strPreNumberLen12 = WebConfigurationManager.AppSettings["PrefixViettel12"];
                if (!string.IsNullOrEmpty(strPreNumberLen12))
                {
                    headerLen = new List<string>(strPreNumberLen12.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
                }
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen != null)
                {
                    return headerLen.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// chi su dung so dien thoai co dinh dang so 0 o dau
        /// </summary>
        /// <author date="2014-01-10" >TuTV4</author>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static bool CheckViettelNumber(this string phoneNumber)
        {
            string viettel10 = (WebConfigurationManager.AppSettings[GlobalConstantsEdu.PREFIX_VIETTEL_10] != null) ? WebConfigurationManager.AppSettings[GlobalConstantsEdu.PREFIX_VIETTEL_10].ToString() : string.Empty;
            string viettel11 = (WebConfigurationManager.AppSettings[GlobalConstantsEdu.PREFIX_VIETTEL_11] != null) ? WebConfigurationManager.AppSettings[GlobalConstantsEdu.PREFIX_VIETTEL_11].ToString() : string.Empty;

            if (phoneNumber.Length == 10)
            {
                string[] preArr = viettel10.Split(new char[] {GlobalConstantsEdu.COMMON_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in preArr)
                {
                    if (phoneNumber.StartsWith(s)) return true;
                }
            }
            else if (phoneNumber.Length == 11)
            {
                string[] preArr = viettel11.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in preArr)
                {
                    if (phoneNumber.StartsWith(s)) return true;
                }
            }

            return false;
        }

        /// <summary>
        /// lay dai so sau cua so dien thoai
        /// </summary>
        /// <param name="Phone"></param>
        /// <returns></returns>
        public static string SubstringPhone(this string Phone)
        {
            string result = Phone;
            if (Phone != null && Phone.Length == 9)
            {
                result = Phone;
            }
            else if (Phone != null && Phone.Length == 10)
            {
                if (Convert.ToInt32(Phone.Substring(0, 1)) == 0)
                {
                    result = Phone.Substring(1);
                }
                else
                {
                    result = Phone;
                }
            }
            else if (Phone != null && Phone.Length == 11)
            {
                if (Convert.ToInt32(Phone.Substring(0, 2)) == 84)
                {
                    result = Phone.Substring(2);
                }
                if (Convert.ToInt32(Phone.Substring(0, 1)) == 0)
                {
                    result = result.Substring(1);
                }
            }
            else if (Phone != null && Phone.Length == 12)
            {
                if (Convert.ToInt32(Phone.Substring(0, 2)) == 84)
                {
                    result = Phone.Substring(2);
                }
            }
            return result;
        }
        
        /// <summary>
        /// lay thong tin dang boolean dang NULL
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool? GetNullableBool(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetBool(dic[key]);
        }

        /// <summary>
        /// dau ":"
        /// </summary>
        private const string _COLON = ":";

        /// <summary>
        /// dau " : "
        /// </summary>
        private const string _COLON_SPACE = " : ";
        /// <summary>
        /// check isCentive content SMS, true if invalid, false if valid
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>01/04/2013</date>
        /// <param name="content"></param>
        /// <param name="lstSensitive"></param>
        /// <returns></returns>
        public static bool CheckContentSMS(this string content)
        {
            content = content.Replace(Environment.NewLine, GlobalConstantsEdu.SPACE).Replace(_nChar, GlobalConstantsEdu.SPACE).ToUpper().Replace(_COLON, _COLON_SPACE);
            content = GlobalConstantsEdu.SPACE + content + GlobalConstantsEdu.SPACE;
            string sensitiveWords = ConfigEdu.SensitiveKeyword;
            sensitiveWords = string.IsNullOrEmpty(sensitiveWords) ? string.Empty : sensitiveWords;
            List<string> lstSensitive = SplitStringToList(sensitiveWords, GlobalConstantsEdu.COMMON_COMMA).Select(p => GlobalConstantsEdu.SPACE + p.ToUpper() + GlobalConstantsEdu.SPACE).ToList();
            if (string.IsNullOrEmpty(content) || (lstSensitive == null))
            {
                return false;
            }            

            //viethd31: fix loi sau tu xau la ky tu dac biet
            string tmpContent = Regex.Replace(content, @"[^0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+", GlobalConstantsEdu.SPACE);

            return lstSensitive.Where(p => tmpContent.Contains(p)).Count() > 0;

        }

        public static bool CheckContentSMS(this string content, ref List<string> lstInputedSensitiveWord)
        {
            content = content.Replace(Environment.NewLine, GlobalConstantsEdu.SPACE).Replace(_nChar, GlobalConstantsEdu.SPACE).ToUpper().Replace(_COLON, _COLON_SPACE);
            content = GlobalConstantsEdu.SPACE + content + GlobalConstantsEdu.SPACE;
            string sensitiveWords = ConfigEdu.SensitiveKeyword;
            sensitiveWords = string.IsNullOrEmpty(sensitiveWords) ? string.Empty : sensitiveWords;
            List<string> lstSensitive = SplitStringToList(sensitiveWords, GlobalConstantsEdu.COMMON_COMMA).Select(p => GlobalConstantsEdu.SPACE + p.ToUpper() + GlobalConstantsEdu.SPACE).ToList();
            if (string.IsNullOrEmpty(content) || (lstSensitive == null))
            {
                return false;
            }            

            //viethd31: fix loi sau tu xau la ky tu dac biet
            string tmpContent = Regex.Replace(content, @"[^0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+", GlobalConstantsEdu.SPACE);

            lstInputedSensitiveWord = lstSensitive.Where(p => tmpContent.Contains(p.Trim())).Select(o=>o.Trim()).ToList();
            return lstSensitive.Where(p => tmpContent.Contains(p.Trim())).Count() > 0;

        }

        /// <summary>
        /// return short sms content
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>31/03/2013</date>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string ToShortSMSContent(this string content, int numToShort)
        {
            if (string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            if (content.Length <= numToShort)
            {
                return content;
            }

            string[] tmp = content.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string shortContent = string.Empty;

            if (tmp.Length == 1)
            {
                if (tmp[0].Length > numToShort)
                {
                    shortContent = tmp[0].Substring(0, numToShort) + "...";
                }
                else
                {
                    shortContent = tmp[0];
                }
            }
            else
            {
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0, size = tmp.Length; i < size; i++)
                {
                    if (strBuilder.Length > numToShort)
                    {
                        shortContent = strBuilder.ToString().Substring(0, numToShort) + "...";
                        break;
                    }
                    if (i == size - 1)
                    {
                        shortContent = strBuilder.ToString();
                        break;
                    }

                    strBuilder.Append(tmp[i]).Append(" ");
                }
            }
            return shortContent;
        }

        /// <summary>
        /// Doi chuoi sang ngay thang dang dd/MM/yyyy
        /// </summary>
        /// <param name="strDate"></param>
        /// <param name="formatDate"></param>
        /// <returns></returns>
        public static DateTime? ConvertStringToDate(this string strDate, string formatDate)
        {
            try
            {
                strDate = strDate.Trim();
                if (String.IsNullOrEmpty(strDate))
                {
                    return null;
                }
                DateTime retVal;
                if (formatDate.Equals(GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_MMDDYY))
                {
                    if (!DateTime.TryParseExact(strDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_MMDDYY, null, DateTimeStyles.AssumeLocal, out retVal))
                    {
                        return null;
                    }
                    return retVal;
                }
                else if (formatDate.Equals(GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY))
                {
                    if (!DateTime.TryParseExact(strDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY, null, DateTimeStyles.AssumeLocal, out retVal))
                    {
                        return null;
                    }
                    return retVal;
                }
                return null;

            }
            catch 
            {
                return null;
            }
        }

        /// <summary>
        /// Them tien to 0 cho so dien thoai khong co 0 hoac 84 truoc
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string ExtensionMobile(this string phone)
        {
            if (string.IsNullOrEmpty(phone)) return "";
            if (phone.StartsWith("84")) phone = phone.Substring(2);

            string strmobile = (WebConfigurationManager.AppSettings[GlobalConstantsEdu.EXTENSION_MOBILE] != null) ? WebConfigurationManager.AppSettings[GlobalConstantsEdu.EXTENSION_MOBILE].ToString() : string.Empty;
            List<string> strExtension = new List<string>(strmobile.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));

            if (strExtension != null && strExtension.Count > 0)
            {
                foreach (string s in strExtension)
                {
                    if (phone.StartsWith(s)) return "0" + phone;
                }              
            }
            return phone;
        }

        /// <summary>
        /// Xoa bo 0 hoac 84 (ma vung) so thue bao khi tru tien
        /// </summary>
        /// <author date="12/10/2015">AnhVD9</author>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string RemoveCountryCode(this string phone)
        {
            if (string.IsNullOrEmpty(phone)) return "";
            if (phone.StartsWith("0")){
                phone = phone.Substring(1);
            }
            else if (phone.StartsWith("84")){
                phone = phone.Substring(2);
            }
            return phone;
        }

        /// <summary>
        /// lay cap hoc cua khoi
        /// </summary>
        /// <param name="educationLevelID">cap hoc</param>
        /// <returns></returns>
        public static int GetEducationGradeByLevelID(this int educationLevelID)
        { 
            if(educationLevelID <= 5 && educationLevelID >= 1)
            {
                //cap 1
                return 1;
            }
            else if (educationLevelID <= 9 && educationLevelID >= 6)
            {
                //cap 2
                return 2;
            }
            else if (educationLevelID <= 12 && educationLevelID >= 10)
            {
                //cap 3
                return 3;
            }
            else if (educationLevelID <= 15 && educationLevelID >= 13)
            {
                //cap nha tre
                return 4;
            }
            else if (educationLevelID <= 18 && educationLevelID >= 16)
            {
                //cap mau giao
                return 5;
            }
            return -1;
        }

        /// <summary>
        /// Kiem tra so dien thoai duoc ho tro gui tin nhan
        /// </summary>
        /// <author date="13/11/2015">AnhVD9</author>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool isSupportPhone(this string number)
        {
            if (String.IsNullOrEmpty(number))
            {
                return false;
            }
            if ((number.Length < 9) || (number.Length > 12))
            {
                return false;
            }

            string headNumbers = ConfigEdu.GetString("SupportPreNumber");
            List<string> lstHeadNumber = new List<string>(headNumbers.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
            string headNumber = string.Empty;
            if (number.StartsWith("0"))
            {
                headNumber = number.Substring(1, number.Length - 8);
            }
            else if (number.StartsWith("84"))
            {
                headNumber = number.Substring(2, number.Length - 9);
            }
            else 
            {
                headNumber = number.Substring(0, number.Length - 7);
            }
  
            if (lstHeadNumber != null)
            {
                return lstHeadNumber.Exists(a => a.Contains(headNumber));
            }

            return false;
        }

        /// <summary>
        /// Kiem tra so dien thoai co ho tro nhan tin co dau
        /// </summary>
        /// <author date="25/06/2016">Anhvd9</author>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool isAllowSendSignSMSPhone(this string number)
        {
            if (String.IsNullOrEmpty(number))
            {
                return false;
            }
            if ((number.Length < 9) || (number.Length > 12))
            {
                return false;
            }

            string headNumbers = ConfigEdu.GetString("AllowUnicodePreNumber");
            List<string> lstHeadNumber = new List<string>(headNumbers.Split(new char[] { GlobalConstantsEdu.COMMON_COMMA }));
            string headNumber = string.Empty;
            if (number.StartsWith("0"))
            {
                headNumber = number.Substring(1, number.Length - 8);
            }
            else if (number.StartsWith("84"))
            {
                headNumber = number.Substring(2, number.Length - 9);
            }
            else if (number.StartsWith("+84"))
            {
                headNumber = number.Substring(3, number.Length - 10);
            }
            else
            {
                headNumber = number.Substring(0, number.Length - 7);
            }

            if (lstHeadNumber != null)
            {
                return lstHeadNumber.Exists(a => a.Contains(headNumber));
            }

            return false;
        }

        public static string ToNameOnly(this string value)
        {
            string[] arr = Regex.Split(value, @"\s{2,}");
            string name = arr[arr.Length - 1];
            return name;
        }

        public static string FormatName(this string value, int? type = null)
        {
            string[] arr = value.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            string name = arr[arr.Length - 1];

           
            if (type == GlobalConstantsEdu.NAME_DISPLAY_NAME)
            {
                return name;
            }
            else if (type == GlobalConstantsEdu.NAME_DISPLAY_ACRONYM)
            {
                string acronym = string.Empty;
                if (arr.Length > 2)
                {
                    for (int i = 0; i < arr.Length - 1; i++)
                    {
                        acronym += arr[i][0].ToString().ToUpper();
                    }
                }

                return acronym + name;
            }
            else
            {
                return value;
            }

            
        }
        public static int countNumSMS(this string content, bool isSign = false)
        {
            if (string.IsNullOrEmpty(content)) return 0;
            // Với tin nhắn có dấu
            if (isSign)
            {
                if (content.Length <= GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT) return 1;
                else if (content.Length % GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT == 0) return content.Length / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT;
                else return content.Length / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT + 1;
            }
            else // tin nhắn không dấu
            {
                if (content.Length % GlobalConstantsEdu.COMMON_SMS_COUNT == 0) return content.Length / GlobalConstantsEdu.COMMON_SMS_COUNT;
                else return content.Length / GlobalConstantsEdu.COMMON_SMS_COUNT + 1;
            }
        }

    }
}