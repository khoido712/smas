﻿/**
* Validate object with metadata
* Author: NhungNT
**/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using SMAS.Business.Common;
using System.ComponentModel;
using SMAS.Models.CustomAttribute;

namespace SMAS.Business.Common
{
    public class ValidationMetadata
    {
        public static void ValidateObject(object obj)
        {
            //ValidationContext
            ValidationContext vc = new ValidationContext(obj, null, null);
            //get type of object
            Type type = obj.GetType();

            //get properties of class
            PropertyInfo[] lsPropertyInfo = type.GetProperties();

            //get MetadataTypeAttribute of class
            MetadataTypeAttribute[] metadataTypes = type.GetCustomAttributes(typeof(MetadataTypeAttribute), true).OfType<MetadataTypeAttribute>().ToArray();
            MetadataTypeAttribute metadata = metadataTypes.FirstOrDefault();

            //value of property
            object val = null;

            //Check dataAnnotation in metadata class
            if (metadata != null)
            {
                //get properties in class meatadata file
                //if it is virtual property then pass it
                PropertyInfo[] lsProperties = metadata.MetadataClassType.GetProperties();
                foreach (PropertyInfo property in lsProperties)
                {
                    if (property.GetGetMethod().IsVirtual)
                    {
                        continue;
                    }
                    //get value of property
                    foreach (PropertyInfo p in lsPropertyInfo)
                    {
                        if (p.GetGetMethod().IsVirtual)
                        {
                            continue;
                        }
                        if (p.Name == property.Name)
                        {
                            val = p.GetValue(obj, null);
                            break;
                        }
                    }
                    //validate property
                    ValidateProperty(property, val, vc);
                }
            }
            //Check data annotation in data model class
            foreach (PropertyInfo property in lsPropertyInfo)
            {
                if (property.GetGetMethod().IsVirtual)
                {
                    continue;
                }
                val = property.GetValue(obj, null);
                ValidateProperty(property, val, vc);
            }

        }

        private static void ValidateProperty(PropertyInfo property, object val, ValidationContext vc)
        {
            //get attributes of property
            Object[] lsAttribute = property.GetCustomAttributes(true);
            //validate 
            string DisplayName = "";
            foreach (object att in lsAttribute)
            {
                if (att is DisplayNameAttribute)
                {
                    DisplayName = ((DisplayNameAttribute)att).DisplayName;
                    break;
                }

                if (att is DisplayAttribute)
                {

                    DisplayName = ((DisplayAttribute)att).GetName();
                    break;
                }

            }

            foreach (object att in lsAttribute)
            {
                if (att is ValidationAttribute)
                {
                    bool isvalid = true;
                    List<object> Params = new List<object>();
                    Params.Add(DisplayName);

                    if (att is DuplicateAttribute)
                    {
                        DuplicateAttribute da = (DuplicateAttribute)att;
                        isvalid = da.Valid(val, vc);
                    }
                    else if (att is DataConstraintAttribute)
                    {
                        DataConstraintAttribute da = (DataConstraintAttribute)att;
                        isvalid = da.Valid(val, vc);
                        if (!isvalid)
                        {
                            Params.Add(da.RelateProperty);
                        }
                    }
                    else if (att is RangeAttribute)
                    {
                        RangeAttribute ra = (RangeAttribute)att;
                        isvalid = ra.IsValid(val);
                        if (!isvalid)
                        {
                            Params.Add(ra.Minimum);
                            Params.Add(ra.Maximum);
                        }
                    } else if (att is StringLengthAttribute)
                    {
                        StringLengthAttribute strLen = (StringLengthAttribute)att;
                        isvalid = strLen.IsValid(val);
                        if (!isvalid)
                            Params.Add(strLen.MaximumLength);
                    }
                    else
                    {
                        ValidationAttribute va = (ValidationAttribute)att;
                        isvalid = va.IsValid(val);
                    }
                    if (!isvalid)
                    {
                        throw new BusinessException(((ValidationAttribute)att).ErrorMessageResourceName, Params);
                    }
                }
            }
        }
    }
}