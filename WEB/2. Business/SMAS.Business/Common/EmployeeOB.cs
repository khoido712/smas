using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.Models;

namespace SMAS.Business.Common
{
    /// <summary>
    /// 
    /// <author>dungnt77</author>
    /// <date>09/10/2012 9:05:13 AM</date>
    /// </summary>
   public class EmployeeOB
    {
        //Select e.EmployeeID, e.FullName, e.EmployeeCode, e.BirthDate, 
        //e.Genre,e.SchoolFacultyID e.WorkTypeID, e.ContractID, e.Mobile, ehs.EmployeeStatus
        #region Member Variables

       public Employee Employee { get; set; }
        [Display(Name = "Employee_Label_EmployeeID")]  
        public int EmployeeID { get; set; }

        [Display(Name = "Employee_Label_FullName")]
        public string FullName { get; set; }

        [Display(Name = "Employee_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }

        [Display(Name = "Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [Display(Name = "Employee_Label_Genre")]
        public bool Genre { get; set; }

        [Display(Name = "Employee_Label_SchoolFacultyID")]
        public Nullable<int> SchoolFacultyID { get; set; }

        [Display(Name = "Employee_Label_WorkTypeID")]
        public Nullable<int> WorkTypeID { get; set; }

        [Display(Name = "Employee_Label_ContractID")]
        public Nullable<int> ContractID { get; set; }

        [Display(Name = "Employee_Label_Mobile")]
        public string Mobile { get; set; }

        public int EmployeeStatus { get; set; }


        [Display(Name = "SchoolProfile_Label_ProvinceID")]
        public Nullable<int> ProvinceID { get; set; }

        [Display(Name = "SchoolProfile_Label_DistrictID")]
        public Nullable<int> DistrictID { get; set; }

        [Display(Name = "SchoolProfile_Label_SupervisingDeptID")]
        public Nullable<int> SupervisingDeptID { get; set; }

        public Nullable<int> SchoolID { get; set; }

        [Display(Name = "Employee_Label_IsActive")]
        public bool IsActive { get; set; }

        [Display(Name = "Employee_Label_EmployeeType")]
        public int EmployeeType { get; set; }

        public Nullable<int> AppliedLevel { get; set; }
        public Nullable<int> TrainingTypeID { get; set; }
        public Nullable<int> ContractTypeID { get; set; }
        public string TraversalPath { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// <author>dungnt77</author>
        /// <date>09/10/2012 9:05:13 AM</date>
        /// </summary>
        public EmployeeOB()
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Functions
        #endregion


    }
}
