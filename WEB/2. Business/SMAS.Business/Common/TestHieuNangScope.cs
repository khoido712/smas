﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Common
{
    public class TestHieuNangScope : IDisposable
    {

        public TestHieuNangBO TestHieuNangBO { get; set; }
        
        public TestHieuNangScope(long ThreadID, string Module, string SubModule)
        {
            TestHieuNangBO = new TestHieuNangBO();
            TestHieuNangBO.ThreadID = ThreadID;
            TestHieuNangBO.Module = Module;
            TestHieuNangBO.SubModule = SubModule;
            TestHieuNangBO.StartTime = DateTime.Now;
        }

        void IDisposable.Dispose()
        {
            TestHieuNangBO.EndTime = DateTime.Now;
            TestHieuNangAdapter.Insert(TestHieuNangBO);
        }

    }
}
