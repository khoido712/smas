﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Common
{
    public class SMASConvert
    {
        public static string ConvertSemester(int semester)
        {
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                return "Học kỳ I";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                return "Học kỳ II";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                return "Cả năm";
            }
            else
            {
                return "";
            }
        }

        public static string ConvertSemesterSpecial(int semester)
        {
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                return "Học kỳ I";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                return "Học kỳ II (cả năm)";
            }
            else
            {
                return "";
            }
        }

        public static string ConvertSemesterSpecial2(int semester)
        {
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                return "Học kỳ I";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                return "Cả năm";
            }
            else
            {
                return "";
            }
        }

        public static string ConvertGenre(int genre)
        {
            if (genre == SystemParamsInFile.GENRE_MALE)
            {
                return "Nam";
            }
            else if (genre == SystemParamsInFile.GENRE_FEMALE)
            {
                return "Nữ";
            }
            else
            {
                return "";
            }
        }
        public static string ConvertPrimaryCapacityLevel(decimal? mark)
        {
            if (mark == null)
            {
                return "";
            }
            if (9 <= mark && mark <= 10)
            {
                return "Giỏi";
            }
            else if (7 <= mark && mark < 9)
            {
                return "Khá";
            }
            else if (5 <= mark && mark < 7)
            {
                return "TB";
            }
            else if (mark < 5)
            {
                return "Yếu";
            }
            else
            {
                return "";
            }
        }

        public static string ConvertSection(int section)
        {
            if (section == SystemParamsInFile.SECTION_MORNING)
            {
                return "buổi sáng";
            }
            else if (section == SystemParamsInFile.SECTION_AFTERNOON)
            {
                return "buổi chiều";
            }
            else if (section == SystemParamsInFile.SECTION_EVENING)
            {
                return "buổi tối";
            }
            else
            {
                return "";
            }
        }
    }
}
