﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.Common
{
    public class SummanyMarkBO
    {
        public string PupilCode { get; set; }
        public string PupilName { get; set; }
        public string[] lsSubject { get; set; }
        public string CapacityLevel { get; set; }
        public string ConductLevel { get; set; }
    }
}
