using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;

namespace SMAS.Business.Common
{
    public class PupilOfClassRanking
    {
        public long? PupilRankingID { get; set; }
        public int? ClassID { get; set; }
        public int PupilID { get; set; }
        public int AcademicYearID { get; set; }
      
        public int? EducationLevelID { get; set; }
        public string Period { get; set; }
        public int? PeriodID { get; set; }
        public int? Semester { get; set; }
        public int ProfileStatus { get; set; }
        public int? StudyingJudgementID { get; set; }
        
        public int SchoolID { get; set; }
        public int? Year { get; set; }

        public DateTime? AssignDate { get; set; }
        public int Status { get; set; }
        //minhh update
        public int? Rank { get; set; }
        public string CapacityLevel { get; set; }
        public int? CapacityLevelID { get; set; }
        public decimal? AverageMark { get; set; }
        public string ConductLevel { get; set; }
        public int? ConductLevelID { get; set; }
        public DateTime? RankingDate { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        // Quanglm bo sung them thong tin ve so buoi nghi hoc
        public int? TotalAbsentDaysWithoutPermission { get; set; }
        public int? TotalAbsentDaysWithPermission { get; set; }
        public string PupilRankingComment { get; set; }
    }
}
