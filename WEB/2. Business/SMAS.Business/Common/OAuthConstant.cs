﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SMAS.Business.Common
{
    public class OAuthConstant
    {

        /// <summary>
        /// Dia chi public viettelstudy
        /// </summary>
        public static string VIETTEL_STUDY_URL
        {
            get
            {
                return WebConfigurationManager.AppSettings["VIETTEL_STUDY_URL"];
            }
        }

        /// <summary>
        /// API Viettel study
        /// </summary>
        public static string VIETTEL_STUDY_API
        {
            get
            {
                return WebConfigurationManager.AppSettings["VIETTEL_STUDY_API"];
            }
        }
        /// <summary>
        /// UserName ket noi ViettelStudy
        /// </summary>
        public static string CLIENT_ID
        {
            get
            {
                return WebConfigurationManager.AppSettings["VIETTEL_STUDY_USERNAME"];
            }
        }
        /// <summary>
        /// Password ket noi ViettelStudy
        /// </summary>
        public static string CLIENT_SECRET
        {
            get
            {
                return WebConfigurationManager.AppSettings["VIETTEL_STUDY_PASSWORD"];
            }
        }

        /// <summary>
        /// User callback dang ky tren ViettelStudy
        /// </summary>
        public static string URL_CALL_BACK
        {
            get
            {
                return WebConfigurationManager.AppSettings["VIETTEL_STUDY_URL_CALL_BACK"];
            }
        }

        public const string SCOPE = "info";
        public const string PARA_ACCESS_TOKEN = "api/Core/OAuth/AuthCode/accessToken";//"api/Core/OAuth/Password/accessToken";
        public const string PARA_CHANGE_PASS = "api/Core/OAuth/API/changePassword";
        public const string PARA_GET_USERNAME = "api/Core/OAuth/API/me";//"api/Core/OAuth/API/me?";
        public const string PARA_ADD_USER = "api/Core/OAuth/API/addUser";
        public const string GRANT_TYPE_PASSWORD = "password";
        public const string LOGIN_VN_STUDY = "LOGIN_VN_STUDY";
        public const string PASSWROD_DEFAULT = "123456a@";
        public const string ALGORITHM_MD5 = "MD5";
        public const string ALGORITHM_SHA1 = "SHA1";
        public const string ALGORITHM_SHA256 = "SHA256";
        public const string ALGORITHM_HASHPASS = "HashPassword";


    }
}