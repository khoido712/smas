﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Common
{

    public class GlobalConstants
    {
        public const int PARTITION = 100;
        public const int ROLE_ADMIN_CAPCAO = 1;
        public const int ROLE_ADMIN_CAPTINH = 2;
        public const int ROLE_ADMIN_CAPSO = 3;
        public const int ROLE_ADMIN_CAPPHONG = 4;
        public const int ROLE_ADMIN_TRUONGC1 = 7;
        public const int ROLE_ADMIN_TRUONGC2 = 6;
        public const int ROLE_ADMIN_TRUONGC3 = 5;
        public const int ROLE_ADMIN_NHATRE = 8;
        public const int ROLE_ADMIN_MAUGIAO = 9;
        //Product Version
        public const int PRODUCT_VERSION_ID_BASIC = 1;//phien ban rut gon
        public const int PRODUCT_VERSION_ID_ADVANCE = 2;//phien ban chuan
        public const int PRODUCT_VERSION_ID_FULL = 3;//phien ban day du
        //ID Sở Hồ Chí Minh
        public const int ProvinceID_HCM = 40;
        public const int ProvinceID_NA = 104;
        public const string Province_HCM = "HỒ CHÍ MINH";
        public const string Province_CODE_HCM = "HCM";
        public const string USERACCID = "Id of current log on UserAccount";
        public const string USERACCOUNT = "current log on UserAccount";
        public const string ROLE_PARENTS = "role parents of this role";
        public const string ROLE_PARENT_ID = "ParentID";
        public const string GROUPS = "groups of current Log on UserAccount";
        public const string ROLES = "roles of current Log on UserAccount";
        public const string MENUS = "menus of current Log on UserAccount";
        public const string SELECTED_ROLE = "selected role of current Log on UserAccount";
        public const string SELECTED_GROUP = "selected group of current Log on UserAccount";
        public const int SUPER_ROLE_ID = 6;

        public const string GROUPID = "SELECTED_GROUP_ID";

        public const string ROLEID = "SELECTED_ROLE_ID";
        //
        public const int PERMISSION_LEVEL_VIEW = 1;
        public const int PERMISSION_LEVEL_CREATE = 2;
        public const int PERMISSION_LEVEL_EDIT = 3;
        public const int PERMISSION_LEVEL_DELETE = 4;
        public const int PERMISSION_LEVEL_UNIDENTIFIED = 0;

        //
        public const string ACCOUNTS = "List Account Admin Created By Admin";
        public const string NORMAL_ACCOUNTS = "List Normal Account Created By Admin";
        public const string EMPLOYEES = "ListEmployees";

        public const string MENUITEMS = "ListMenuItems";
        public const string MSG = "message";
        public const string LS_ROLES = "ListRoleCheck";
        public const string MENU_PARENTS = "menu parents";

        public const string MENUPERMISSION = "";

        // Constant for SMAS schema
        public const string LIST_SCHEMA = "List";
        public const string SCHOOL_SCHEMA = "School";
        public const string PUPIL_SCHEMA = "Pupil";
        public const string REPORT_SCHEMA = "Report";
        public const string EXAM_SCHEMA = "Exam";
        public const string ADM_SCHEMA = "Adm";
        public const string DBO_SCHEMA = "dbo";
        public const string NUTRITION_SCHEMA = "Nutrition";
        public const string TRS_SCHEMA = "Trs";
        public const string HEALTH_SCHEMA = "Health";
        // Constans
        public const int ACTIVE = 1;
        public const int INACTIVE = 0;

        // Constans Rank

        // Subject Type

        public const int SUBJECTCAT_ISCOMMENTING_MARK = 0;
        public const int SUBJECTCAT_ISCOMMENTING_COMMENT = 1;
        public const int SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT = 2;


        public const int CONDUCT_TYPE_GOOD_SECONDARY = 3;
        public const int CONDUCT_TYPE_GOOD_TERTIARY = 7;
        public const int CONDUCT_TYPE_FAIR_SECONDARY = 4;
        public const int CONDUCT_TYPE_NORMAL_SECONDARY = 5;
        public const int CONDUCT_TYPE_WEAK_SECONDARY = 6;
        public const int CONDUCT_TYPE_FAIR_TERTIARY = 8;
        public const int CONDUCT_TYPE_NORMAL_TERTIARY = 9;
        public const int CONDUCT_TYPE_WEAK_TERTIARY = 10;

        public const int STUDYING_JUDGEMENT_GRADUATION = 5;
        public const int STUDYING_JUDGEMENT_UPCLASS = 1;
        public const int STUDYING_JUDGEMENT_STAYCLASS = 4;
        public const int STUDYING_JUDGEMENT_RETEST = 2;
        public const int STUDYING_JUDGEMENT_BACKTRAINING = 3;

        // Constants for school type
        public const int PRIMARY_EDUCATION_SCHOOL = 1;

        public const int EMPLOYMENT_STATUS_WORKING = 1;
        public const int EMPLOYMENT_STATUS_MOVED = 2;
        public const int EMPLOYMENT_STATUS_RETIRED = 3;
        public const int EMPLOYMENT_STATUS_SEVERANCE = 5;
        public const int EMPLOYMENT_STATUS_BREATHER = 6;


        public const int PUPIL_STATUS_GRADUATED = 2;
        public const int PUPIL_STATUS_STUDYING = 1;
        public const int PUPIL_STATUS_LEAVED_OFF = 4;
        public const int EMPLOYEE_TYPE_TEACHER = 1;   //loai giao vien
        public const int EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF = 3; //nhan vien phong so

        /// <summary>
        /// Đã chuyển trường
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL = 3;

        /// </summary>
        /// <summary>
        /// Đã chuyển lớp
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_CLASS = 5;

        // Move Type - EmployeeWorkMovement
        public const int MOVE_TYPE_SYSSCHOOL = 1;
        public const int MOVE_TYPE_OTHERDEPT = 2;

        //class profile permission
        public const int SUPERVISING_PERMISSION_HEAD_TEACHER = 1;
        public const int SUPERVISING_PERMISSION_SUBJECT_TEACHER = 2;

        public const int SUPERVISING_PERMISSION_OVERSEEING = 3;

        public const int CONTRACT_TYPE_STAFF = 0;//Bien che
        public const int CONTRACT_TYPE_NOSTAFF = 1;//Khong bien che
        public const int CONTRACT_TYPE_BUDGET = 2;//Trong ngan sach\

        public const int COUNT_CHILDREN = 16;
        public const int COUNT_CHILDCARE = 8;
        public const int COUNT_TERTIARY = 4;
        public const int COUNT_SECONDARY = 2;
        public const int COUNT_PRIMARY = 1;

        public const int CODECONFIG_CODETYPE_PUPIL = 1; //Cau hinh ma hoc sinh
        public const int CODECONFIG_CODETYPE_TEACHER = 2; //Cau hinh ma giao vien

        public const int CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR = 1;
        public const int CODECONFIG_MIDDLECODETYPE_START_LEVEL_1 = 2;
        public const int CODECONFIG_MIDDLECODETYPE_COURSE = 3;
        public const int CODECONFIG_MIDDLECODETYPE_NO_USED = 4;

        public const int WORK_MOVEMENT_WITHIN_SYSTEM = 1;
        public const int WORK_MOVEMENT_OTHER_ORGANIZATION = 2;
        public const int WORK_MOVEMENT_RETIREMENT = 3;
        public const int WORK_MOVEMENT_OTHER = 4;
        public const int WORK_MOVEMENT_SEVERANCE = 5;


        public const int EXEMPT_TYPE_ALL = 2;
        public const string JUDGEMENTRESULT_B = "B";
        public const int CONDUCT_TYPE_D = 1;
        public const int CONDUCT_TYPE_CD = 2;
        public const int CAPACITY_TYPE_EXCELLENT = 1;
        public const int CAPACITY_TYPE_GOOD = 2;
        public const int CAPACITY_TYPE_NORMAL = 3;
        public const int CAPACITY_TYPE_WEAK = 4;
        public const int CAPACITY_TYPE_POOR = 5;

        public const int SUMMEDUPMARK_TYPE_EXCELLENT = 9;
        public const int SUMMEDUPMARK_TYPE_GOOD = 7;
        public const int SUMMEDUPMARK_TYPE_NORMAL = 5;


        //dungnt them vao trong nv employee
        public const int EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE = 3;
        public const int EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE = 5;
        public const int EDUCATION_HIERACHY_SUBLEVEL_PROVINCE_OFFICE = 4;
        public const int EDUCATION_HIERACHY_SUBLEVEL_DISTRICT_OFFICE = 6;
        public const int GENRE_UNIDENTIFIED = 2;
        public const int EDUCATION_HIERACHY_SCHOOL = 7;
        /// <summary>
        /// hoc ki 1
        /// </summary>
        public const int SEMESTER_OF_YEAR_FIRST = 1;
        /// <summary>
        /// hoc ki 2
        /// </summary>
        public const int SEMESTER_OF_YEAR_SECOND = 2;
        /// <summary>
        /// ca nam
        /// </summary>
        public const int SEMESTER_OF_YEAR_ALL = 3;
        /// <summary>
        /// hoc lại
        /// </summary>
        public const int SEMESTER_OF_YEAR_BACKTRAINING = 4;
        /// <summary>
        /// thi lại
        /// </summary>
        public const int SEMESTER_OF_YEAR_RETEST = 5;
        /// <summary>
        /// kết quả đánh giá cuối kỳ
        /// </summary>
        public const int SEMESTER_OF_EVALUATION_RESULT = 4;


        /// <summary>
        /// const hoc ki cuoi ki cap 1
        /// </summary>
        public const int SEMESTER_OF_YEAR_PRIMARY = 4;


        public const bool SEMETERDECLARATION_ISLOCK = true;

        //Quanglm them cac bien luu thong tin diem cho viec xep loai
        public const decimal EXCELLENT_MARK = 8.0m;
        public const decimal GOOD_MARK = 6.5m;
        public const decimal GOOD_MARK_PRIMARY = 6m;//cap 1 kha = 6
        public const decimal NORMAL_MARK = 5.0m;
        public const decimal WEAK_MARK = 3.5m;
        public const decimal POOR_MARK = 2.0m;
        public const decimal MAX_MARK = 10.0m;
        // Diem nghe
        public const decimal NPT_POOR_MARK = 3.0m;
        public const decimal NPT_GOOD_MARK = 7.0m;
        public const decimal NPT_FROM_EXCELLENT_MARK = 9.0m;
        public const decimal NPT_TO_EXCELLENT_MARK = 10.0m;
        public const string PASS = "Đ";
        public const string NOPASS = "CĐ";

        public const decimal ADD_EXCELLENT_MARK = 0.3m;
        public const decimal ADD_GOOD_MARK = 0.2m;
        public const decimal ADD_NORMAL_MARK = 0.1m;

        public const int ISCOMMENTING_TYPE_MARK = 0;

        public const int ISCOMMENTING_TYPE_JUDGE = 1;

        public const int ISCOMMENTING_TYPE_MARK_JUDGE = 2;

        public const int SUBJECT_FIRSTSEMESTERCOEFFICIENT_ZERO = 0;
        public const int SUBJECT_SECONDSEMESTERCOEFFICIENT_ZERO = 0;


        public const int HONOUR_ACHIVEMENT_TYPE_PUPIL = 2;
        public const string HONOURACHIVEMENTTYPE_RESOLUTION_EXCELLENT = "Học sinh giỏi";
        public const string HONOURACHIVEMENTTYPE_RESOLUTION_GOOD = "Học sinh tiên tiến";

        public const int TOTALABSENTDAY = 45;

        public const int ABSENT_TYPE_NOTALLOWED = 1;

        public const int ABSENT_TYPE_ALLOWED = 2;



        //foodcat

        public const int FOOD_GROUP_TYPE_ANIMAL = 1;
        public const int FOOD_GROUP_TYPE_PLANT = 2;

        //EXAMINATION_STAGE_CREATED	

        public const int EXAMINATION_STAGE_CREATED = 0;
        public const int EXAMINATION_STAGE_CANDIDATE_LISTED = 1;
        public const int EXAMINATION_STAGE_ROOM_ASSIGNED = 2;

        public const int EXAMINATION_STAGE_INVILIGATOR_ASSIGNED = 3;
        public const int EXAMINATION_STAGE_HEAD_ATTACHED = 4;
        public const int EXAMINATION_STAGE_MARK_COMPLETED = 5;
        public const int EXAMINATION_STAGE_FINISHED = 6;

        //MENU_TYPE: Loại thực đơn
        public const int MENU_TYPE_NORMAL = 1;
        public const int MENU_TYPE_ADDITIONAL = 2;
        //DAY_OF_WEEK
        public const int DAY_OF_WEEK_MONDAY = 2;
        public const int DAY_OF_WEEK_TUESDAY = 3;
        public const int DAY_OF_WEEK_WEDNESDAY = 4;
        public const int DAY_OF_WEEK_THURSDAY = 5;
        public const int DAY_OF_WEEK_FRIDAY = 6;
        public const int DAY_OF_WEEK_SATURDAY = 7;
        // Dinh nghia loai diem
        public const string MARK_TYPE_M = "M";
        public const string MARK_TYPE_P = "P";
        public const string MARK_TYPE_V = "V";
        public const string MARK_TYPE_HK = "HK";

        //Contact Group ID
        public const int SCS_CONTACT_GROUP_ALL = 0; //Toan truong
        public const string SCS_CONTACT_GROUP_ALL_SMSCode = "toantruong"; //Toan truong
        public const string SCS_CONTACT_GROUP_ALL_NAME = "Toàn trường";
        //Type SMSCommunication
        public const byte SMS_Communicatoin_Parent_To_Teacher = 1;
        public const byte SMS_Communicatoin_Teacher_To_Parent = 2;
        public const byte SMS_Communicatoin_Teacher_To_Teacher = 3;

        //Type HistorySMS        
        public const int HISTORYSMS_TO_TEACHER_ID = 1;
        public const int HISTORYSMS_TO_COMMUNICATION_ID = 20;
        public const int HISTORYSMS_TO_DAYMARK_ID = 11;
        public const int HISTORYSMS_TO_WEEKMARK_ID = 12;
        public const int HISTORYSMS_TO_HEALTH_ID = 13;
        public const int HISTORYSMS_TO_ACTIVE_ID = 14;
        public const int HISTORYSMS_TO_TRAINING_ID = 15;
        public const int HISTORYSMS_TO_MONTHSMS_ID = 16;
        public const int HISTORYSMS_TO_SEMESTERSMS_ID = 17;
        public const int HISTORYSMS_TO_ACTIVITY_ID = 18;
        public const int HISTORYSMS_TO_GROWTH_ID = 19;

        public const string HISTORYSMS_TO_TEACHER_TYPECODE = "TNGV";
        public const string HISTORYSMS_TO_COMMUNICATION_TYPECODE = "TNTD";
        public const string HISTORYSMS_TO_DAYMARK_TYPECODE = "TNDN";
        public const string HISTORYSMS_TO_WEEKMARK_TYPECODE = "TNDT";
        public const string HISTORYSMS_TO_HEALTH_TYPECODE = "TNSK";
        public const string HISTORYSMS_TO_ACTIVE_TYPECODE = "TNHD";
        public const string HISTORYSMS_TO_TRAINING_TYPECODE = "TNRL";
        public const string HISTORYSMS_TO_MONTHSMS_TYPECODE = "TNDTH";
        public const string HISTORYSMS_TO_SEMESTERSMS_TYPECODE = "TNHKI";
        public const string HISTORYSMS_TO_ACTIVITY_TYPECODE = "TNHD";
        public const string HISTORYSMS_TO_GROWTH_TYPECODE = "TNTT";

        public const int HISTORY_RECEIVER_PUPIL_ID = 0;
        public const int HISTORY_RECEIVER_PARENT_ID = 1;
        public const int HISTORY_RECEIVER_TEACHER_ID = 2;

        //wild dung de ghi log
        public const string WILD_LOG = "*#*";
        public const string ACTION_VIEW = "1";
        public const string ACTION_ADD = "2";
        public const string ACTION_UPDATE = "3";
        public const string ACTION_DELETE = "4";
        public const string ACTION_IMPORT = "5";
        public const string ACTION_EXPORT = "6";

        /// <summary>
        /// Loại sổ theo dõi sức khoẻ
        /// </summary>
        public const int MONITORINGBOOK_TYPE_DATE = 1; // theo ngày
        public const int MONITORINGBOOK_TYPE_PERIOD = 2; // theo đợt
        public const int MONITORINGBOOK_TYPE_MONTH = 3; // theo tháng

        //physical test
        public const int PHYSICALTEST_MAL_NUTRITION = 1; //suy dinh duong
        public const int PHYSICALTEST_NORMAL_WEIGHT = 2; //binh thuong
        public const int PHYSICALTEST_OVER_WEIGHT = 3; //beo phi
        public const int PHYSICALTEST_UNDENTIFY_NUTRITION = 0; //khong xac dinh

        //TypeConfig Table
        public const int TypeConfig_Weight = 1;
        public const int TypeConfig_Height = 2;
        public const int TypeConfig_BMI = 3;

        public const bool ClassificationCriterialGenre_MALE = true;
        public const bool ClassificationCriterialGenre_FEMALE = false;
        public const int SMS_COUNT = 160;
        public const short MT_STATUS_NOT_SEND = 0;

        public const string SPACE = " ";
        public const string SEMICOLON_DELEMITER = ";";

        //Buoi hoc
        public const string SECTION_MONING = "Sáng";
        public const string SECTION_AFTERNOON = "Chiều";
        public const string SECTION_EVENING = "Tối";

        /// <summary>
        /// Giới tính nam
        /// </summary>
        public const int GENRE_MALE = 1;
        public const string GENRE_MALE_TITLE = "Nam";
        /// <summary>
        /// Giới tính nữ
        /// </summary>
        public const int GENRE_FEMALE = 0;
        public const string GENRE_FEMALE_TITLE = "Nữ";

        /// <summary>
        /// Hằng số phục vụ tiến trình tổng kết điểm
        /// </summary>
        public const int SUMMED_UP_MARK_AUTO_TYPE = 1;

        public const string AUTO_MARK = "Auto";
        public const string NOT_AUTO_MARK = "No";
        /// <summary>
        /// Hằng số phục vụ tiến trình tính lại xếp hạng và xếp loại nếu có thay đổi về hạnh kiểm
        /// </summary>
        public const int CONDUCT_AUTO_TYPE = 2;

        /// <summary>
        /// Nhóm công việc là hiệu trưởng 
        /// </summary>
        public const int WORKTYPE_PRINCIPAL = 4;
        public const int WORKTYPE_ASSISTANT_PRINCIPAL = 5;

        #region Khoa diem danh vi pham
        public const short LOCK_TRAINING_VIOLATION = 1;
        public const short LOCK_TRAINING_ABSENCE = 2;
        public const short LOCK_TRAINING_ABSENCE_VIOLATION = 3;
        #endregion

        /// <summary>
        /// Môn chuyen
        /// </summary>
        public const int APPLIED_SUBJECT_SPECIALIZE = 0;//môn chuyên
        /// <summary>
        /// Môn tự chọn
        /// </summary>
        public const int APPLIED_SUBJECT_ELECTIVE = 1;//môn tự chọn
        /// <summary>
        /// tự chọn cộng ưu tiên
        /// </summary>
        public const int APPLIED_SUBJECT_ELECTIVE_PRIORITIE = 2;

        /// <summary>
        /// tự chọn tính điểm
        /// </summary>
        public const int APPLIED_SUBJECT_ELECTIVE_SCORE = 3;

        /// <summary>
        /// Mon nghe
        /// </summary>
        public const int APPLIED_SUBJECT_APPRENTICESHIP = 4;
        public const int APPLIED_LEVEL_PRIMARY = 1;
        public const int APPLIED_LEVEL_SECONDARY = 2;
        public const int APPLIED_LEVEL_TERTIARY = 3;
        public const int APPLIED_LEVEL_CRECHE = 4;
        public const int APPLIED_LEVEL_KINDER_GARTEN = 5;

        public const int EDUCATION_LEVEL_ID = 0;
        public const int EDUCATION_LEVEL_ID1 = 1;
        public const int EDUCATION_LEVEL_ID2 = 2;
        public const int EDUCATION_LEVEL_ID3 = 3;
        public const int EDUCATION_LEVEL_ID4 = 4;
        public const int EDUCATION_LEVEL_ID5 = 5;
        public const int EDUCATION_LEVEL_ID6 = 6;
        public const int EDUCATION_LEVEL_ID7 = 7;
        public const int EDUCATION_LEVEL_ID8 = 8;
        public const int EDUCATION_LEVEL_ID9 = 9;
        public const int EDUCATION_LEVEL_ID10 = 10;
        public const int EDUCATION_LEVEL_ID11= 11;
        public const int EDUCATION_LEVEL_ID12 = 12;
        public const int EDUCATION_LEVEL_ID13 = 13;
        public const int EDUCATION_LEVEL_ID14 = 14;
        public const int EDUCATION_LEVEL_ID15 = 15;
        public const int EDUCATION_LEVEL_ID16 = 16;
        public const int EDUCATION_LEVEL_ID17 = 17;
        public const int EDUCATION_LEVEL_ID18 = 18;
        public const int EDUCATION_LEVEL_ID19 = 19;

        public const string MANAGE_SCHOOL = "Quản trị trường";
        public const string MANAGE_SO = "Quản trị sở";
        public const string MANAGE_PHONG = "Quản trị phòng";

        public const string TOAN = "Toán";
        public const string TV = "Tiếng Việt";

        public const int MonthID_1 = 1;
        public const int MonthID_2 = 2;
        public const int MonthID_3 = 3;
        public const int MonthID_4 = 4;
        public const int MonthID_5 = 5;
        public const int MonthID_6 = 6;
        public const int MonthID_7 = 7;
        public const int MonthID_8 = 8;
        public const int MonthID_9 = 9;
        public const int MonthID_10 = 10;
        public const int MonthID_11 = 11;
        public const int MonthID_12 = 12;
        public const int MonthID_EndSemester1 = 15;
        public const int MonthID_EndSemester2 = 16;

        public const int TAB_EDUCATION_BOOKMARK_PRIMARY = 1;
        public const int TAB_CAPACTIES_BOOKMARK_PRIMARY = 2;
        public const int TAB_QUALITIES_BOOKMARK_PRIMARY = 3;
        public const int EVALUATION_RESULT = 4;

        public const int EVALUTION_HEAD_TEACHER = 2;
        public const int EVALUTION_TEACHER_SUBJECT = 1;

        public const string REGISTER_SUBJECT_SPECIALIZE_SEQ = "REGISTER_SS_SEQ";

        public const int EXAM_ORGANIZE = 1;
        public const int EXAM_PROCESSING_TEST = 2;
        public const int EXAM_STATICTIS_REPORT = 3;

        public const string TAB_MENU_INDEX = "Tc";
        public const string SESSION_TAB_TC = "SessionTabTc";

        public const string TAB_MENU_INDEX_REPORT = "Bc";
        public const string SESSION_TAB_REPORT = "SessionTabReport";

        public const string TAB_MENU_INDEX_DOET_EXAM = "Qlt";
        public const string SESSION_TAB_DOET_EXAM = "SessionTabDoetExam";

        public const int TOTAL_SETION_ABSENCE = 3;

        // Tiêu chí Năng lực
        public const int EVALUATION_CRITERIA_ID1 = 1;
        public const int EVALUATION_CRITERIA_ID2 = 2;
        public const int EVALUATION_CRITERIA_ID3 = 3;

        // Tiêu chí Phẩm chất
        public const int EVALUATION_CRITERIA_ID4 = 4;
        public const int EVALUATION_CRITERIA_ID5 = 5;
        public const int EVALUATION_CRITERIA_ID6 = 6;
        public const int EVALUATION_CRITERIA_ID7 = 7;

        public const int TAB_EDUCATION = 1;
        /// <summary>
        /// Nang luc
        /// </summary>
        public const int TAB_CAPACTIES = 2;
        /// <summary>
        /// Pham chat
        /// </summary>
        public const int TAB_QUALITIES = 3;

        public const string SHORTFINISH = "HT";
        public const int REVIEW_TYPE_FINISH = 1;
        public const string SHORTNOTFINISH = "CHT";
        public const string SHORTNOTEVALUATION = "CĐG";
        public const int REVIEW_TYPE_NOT_FINISH = 2;

        public const string SHORT_OK = "Đ";
        public const string SHORT_NOT_OK = "CĐ";

        //Thuoc dien
        public const string PUPLRANKING_GRADUATION1 = "Hoàn thành chương trình tiểu học";
        public const string PUPLRANKING_GRADUATION2 = "Được xét tốt nghiệp";
        public const string PUPLRANKING_GRADUATION3 = "Được dự thi tốt nghiệp";

        public const int THREAD_MARK_STATUS_COMPLETE = 0;

        public const int THREAD_MARK_STATUS_NOT_COMPLETE = 1;

        #region Account status

        /// <summary>
        /// Tai khoan da dong bo
        /// </summary>
        public const int STATUS_SYNC = 1;

        /// <summary>
        /// Tai khoan them moi
        /// </summary>
        public const int STATUS_CREATE_USER = -1;

        /// <summary>
        /// Tai khoan cap nhat thong tin
        /// </summary>
        public const int STATUS_UPDATE_INFO = -2;

        /// <summary>
        /// Tai khoan bi xoa
        /// </summary>
        public const int STATUS_DELETE_USER = -3;

        /// <summary>
        /// Tai khoan doi mat khau
        /// </summary>
        public const int STATUS_CHANGE_PASSWORD = -4;

        #endregion

        /// <summary>
        /// Constant Indicator Code
        /// </summary>
        public const string SO_HOC_SINH = "E7";
        public const string SO_GIAO_VIEN = "F7";
        public const string SO_LOP = "G7";
        public const string SO_LOP_KHAI_BAO_MON_HOC = "H7";
        public const string SO_LOP_DA_PHAN_CONG_GD_HKI = "I7";
        public const string SO_LOP_DA_NHAP_DIEM_HKI = "J7";
        public const string SO_LOP_DA_NHAP_DIEM_HANH_KIEM_HKI = "K7";
        public const string SO_LOP_DA_TONG_KET_HKI = "L7";
        public const string SO_LOP_DA_PHAN_CONG_GD_HKII = "M7";
        public const string SO_LOP_DA_NHAP_DIEM_HKII = "N7";
        public const string SO_LOP_DA_NHAP_DIEM_HANH_KIEM_HKII = "O7";
        public const string SO_LOP_DA_TONG_KET_HKII = "P7";
        public const string SO_USER_GIAO_VIEN = "Q7";
        public const string SO_USER_DA_LOGIN = "R7";
        public const string SO_HS_DA_NHAP_SDT_CN = "S7";
        public const string SO_HS_DA_NHAP_SDT_CHA = "T7";
        public const string SO_HS_DA_NHAP_SDT_ME = "U7";

        /// <summary>
        /// Partition ThreadMark theo may cai tool
        /// </summary>
        public const int MOD_SCHOOL_THREAD_MARK = 3;

        public const int EXPORT_MARK_RECORD_ONE_SUBJECT = 1;
        public const int EXPORT_MARK_RECORD_LIST_SUBJECT_ID = 2;
        public const int EXPORT_MARK_RECORD_LIST_CLASS_ID = 3;
        public const string STR_SPECIAL_COMMENT_X = "©X©X©";
        public const string STR_SPECIAL_COMMENT_U = "©U©U©";
        public const string PUPIL_RANKIG_AUTO = "Auto";

        #region Chế độ hỗ trợ
        /// <summary>
        /// Chế độ theo QĐ số 12/2013/QĐ-TTg
        /// </summary>
        public const string POLICY_122003_GOVERNMENT_DECISION = "Chế độ theo QĐ số 12/2013/QĐ-TTg";

        /// <summary>
        /// Chế độ theo QĐ số 2123/QĐ-TTg
        /// </summary>
        public const string POLICY_2123_GOVERNMENT_DECISION = "Chế độ theo QĐ số 2123/QĐ-TTg";

        /// <summary>
        /// Chế độ theo QĐ số 1672/QĐ-TTg
        /// </summary>
        public const string POLICY_1672_GOVERNMENT_DECISION = "Chế độ theo QĐ số 1672/QĐ-TTg";

        /// <summary>
        /// Chế độ theo QĐ QĐ 01-UBND Tỉnh Lai Châu
        /// </summary>
        public const string POLICY_01_UBND_LCU_GOVERNMENT_DECISION = "Chế độ theo QĐ QĐ 01-UBND Tỉnh Lai Châu";
        #endregion

        //Dien hoc sinh
        public const string PUPILPROFILE_LABEL_CLASSTYPENOITRU = "Nội trú";
        public const string PUPILPROFILE_LABEL_CLASSTYPEBANTRU = "Bán trú";
        public const string PUPILPROFILE_LABEL_CLASSTYPEBANTRUDANNUOI = "Bán trú dân nuôi";
        public const string PUPILPROFILE_LABEL_CLASSTYPENOITRUDANNUOI = "Nội trú dân nuôi";

        //Mat tieu chi
        public const int TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP = 1;//MON HOC & HDGD
        public const int TYPE_EVALUATION_CAPACITYANDQUALITY = 2;//NANG LUC PHAM CHAT

        public const int EVALUATION_SUBJECT_AND_EDUCTIONGROUP = 1;
        public const int EVALUATION_CAPACITY = 2;
        public const int EVALUATION_QUALITY = 3;
        public const string EVALUATION_GOOD_COMPLETE = "T";
        public const string EVALUATION_COMPLETE = "H";
        public const string EVALUATION_CAPQUA_COMPLETE = "Đ";
        public const string EVALUATION_NOT_COMPLETE = "C";

        public const int EVALUATION_REWARD_FORTUITY = 1;//KHEN THUONG DOT XUẤT
        public const int EVALUATION_REWARD_OUTSTANDING_ACHIEVEMENT = 2;//HS CO THANH TICH VUOT TROI

        // Quanglm1 add constants for Evaluation folow TT22
        public const int EVALUATION_GOOD_COMPLETE_VAL = 1;
        public const int EVALUATION_CAPQUA_COMPLETE_VAL = 2;
        public const int EVALUATION_NOT_COMPLETE_VAL = 3;

        public const string SP_DELETE_MARK_RECORD = "SP_DELETE_MARK_RECORD_NEW";

        public const int PERIOD_TYPE_DAILY = 1;
        public const int PERIOD_TYPE_WEEKLY = 2;
        public const int PERIOD_TYPE_MONTHLY = 3;
        public const int PERIOD_TYPE_SEMESTER = 4;
        public const int PERIOD_TYPE_CUSTOM = 5;

        public const string URL_SSO = "identityProvider";
        public const string URL_SMAS = "smasUrl";
        public const string URL_SMS_SCHOOL = "smsEduUrl";

        //can do suc khoe
        public const int BOY_HEIGHT = 1; //CHIEU CAO BE TRAI
        public const int BOY_WEIGHT = 2; //CAN NANG BE TRAI
        public const int GIRL_HEIGHT = 3; //CHIEU CAO BE GAI
        public const int GIRL_WEIGHT = 4; //CAN NANG BE GAI

        //login VN Study
        public const string URL_VNSUTYDY = "http://beta.viettelstudy.vn/";
        public const string CLIENT_ID = "59599c12d559305a8436d603";
        public const string CLIENT_SECRET = "smas2017#a@";
        public const string SCOPE = "info";
        public const string PARA_ACCESS_TOKEN = "api/Core/OAuth/Password/accessToken";
        public const string PARA_CHANGE_PASS = "api/Core/OAuth/API/changePassword";
        public const string PARA_ADD_USER = "api/Core/OAuth/API/addUser";
        public const string GRANT_TYPE_PASSWORD = "password";
        public const string LOGIN_VN_STUDY = "LOGIN_VN_STUDY";

        //Dat, Chua dat
        public const int T = 1;
        public const int D = 2;
        public const int C = 3;

        //Ket qua hoc tap
        public const int STUDY_RESULT_HTT = 1;
        public const int STUDY_RESULT_HT = 2;
        public const int STUDY_RESULT_CHT = 3;

        //HT,CHT
        public const int CDG = 0;
        public const int HT = 1;
        public const int CHT = 2;

        public const int TOKEN_EXPIRED_TIME = 900;

        public const int EMPLOYEE_EVALUATION_FIELD1 = 1;//chuan nghe nghiep
        public const int EMPLOYEE_EVALUATION_FIELD2 = 2;//danh gia vien chuc
        public const int EMPLOYEE_EVALUATION_FIELD3 = 3;//boi duong thuong xuyen
        public const int EMPLOYEE_EVALUATION_FIELD4 = 4;//giao vien day gioi

        public const string COLUMN_TYPE_JSON = "Json";
        public const string COLUMN_TYPE_DATE = "Date";
        public const string COLUMN_TYPE_NUMBER = "Number";
        public const string COLUMN_TYPE_X = "X";
        public const short CONTRACT_TYPE_WITH_SCHOOL = 1;
        public const short CONTRACT_TYPE_WITH_SUB_SUPERVISINGDEPT = 2;
        public const short CONTRACT_TYPE_WITH_SUPERVISINGDEPT = 3;

        #region Trang thai Hop dong
        public const short CONTRACT_STATUS_ACTIVE = 1;
        public const short CONTRACT_STATUS_CANCEL = 2;
        public const short CONTRACT_STATUS_CANCEL_BCCS = 3;
        #endregion

        public const int COLLABORATOR_STATUS_ASSIGNED = 1; // Đã gán
        public const int COLLABORATOR_STATUS_NOT_ASSIGN = 2; // Chưa gán
        public const int COLLABORATOR_STATUS_CANCEL_ASSIGN = 0; // Gán rùi hủy

        public const string LEVEL_EXPERT = "(Giỏi)"; // Hoc luc gioi
        public const string LEVEL_GOOD = "(Khá)"; // Hoc luc kha
        public const string LEVEL_NORMAL = "(TB)"; // Hoc luc trung binh
        public const string LEVEL_WEAK = "(Yếu)"; // Hoc luc yeu
        public const string LEVEL_FOOL = "(Kém)"; // Hoc luc kem

        public const int REPORT_TYPE_OF_EMPLOYEE_PROFILE = 1;
        public const int REPORT_TYPE_OF_SCHOOL_PROFILE = 2;
		public const int REPORT_TYPE_OF_EMPLOYEE = 4;
        public const int REPORT_TEMPLATE_PST = 5;//Ho so cap 1,2,3
        public const int REPORT_TEMPLATE_CHILDREN = 6;//Ho so cap 1,2,3


        public const int REPORT_TYPE_OF_GENERAL_5 = 5;//Ho so cap 1,2,3
        public const int REPORT_TYPE_OF_GENERAL_6 = 6;//Ho so cap 1,2,3
        public const int REPORT_TYPE_OF_GENERAL_7 = 7;//Ho so cap 1,2,3
        public const int REPORT_TYPE_OF_GENERAL_8 = 8;////Ho so cap MAM NON
        public const int REPORT_TYPE_OF_GENERAL_9 = 9;////Ho so cap MAM NON
        public const int REPORT_TYPE_OF_GENERAL_10 = 10;////Ho so cap MAM NON
        public const int REPORT_TYPE_OF_GENERAL_17 = 17;////Ho so cap MAM NON
        public const int REPORT_TYPE_OF_GENERAL_11 = 11;////MON HOC CAP 1
        public const int REPORT_TYPE_OF_GENERAL_12 = 12;////MON HOC CAP 2
        public const int REPORT_TYPE_OF_GENERAL_13 = 13;////MON HOC CAP 3
        public const int REPORT_TYPE_OF_GENERAL_14 = 14;////KET QUA HOC TAP CAP 2,3
        public const int REPORT_TYPE_OF_GENERAL_15 = 15;////NANG LUC PHAM CHAT
        public const int REPORT_TYPE_OF_GENERAL_16 = 16;////KHAM SUC KHOE
        public const int REPORT_TYPE_OF_GENERAL_19 = 19;////THong tin lop hoc
        public const int REPORT_TYPE_OF_GENERAL_20 = 20;////THong tin lop hoc

        public const int REPORT_TYPE_OF_GENERAL_25 = 25;////MH truong khai bao cap 1
        public const int REPORT_TYPE_OF_GENERAL_26 = 26;////MH truong khai bao cap 2
        public const int REPORT_TYPE_OF_GENERAL_27 = 27;////MH truong khai bao cap 3
        
		

#region Push notification
        public const string FIREBASE_LEGACY_KEY = "AIzaSyBeZ_9Jmwz8GiT9m62L7bk0aEtXGly9iCo";
        public const string WEB_ADDRESS = "https://fcm.googleapis.com/fcm/send";
        #endregion

    }
}
