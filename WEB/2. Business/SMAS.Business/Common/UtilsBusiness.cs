﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.Business.BusinessObject;
using System.IO;
using System.Data.Objects;
using System.Web;
using System.Web.Configuration;
using Oracle.DataAccess.Client;
using System.Configuration;
using SMAS.VTUtils.Log;
using log4net;

namespace SMAS.Business.Common
{
    /// <summary>
    /// 
    /// <author>DungNt</author>
    /// <date>02/10/2012 2:29:52 PM</date>
    /// </summary>
    public static class UtilsBusiness
    {
        private static ILog logger = log4net.LogManager.GetLogger("SMAS");
        #region Member Variables
        #endregion

        #region Properties
        #endregion

        #region Functions
        // DungVA - 28/03/2013
        //Kiểm tra giáo viên có quyền GVCN không.
        public static bool HasHeadTeacherPermission(int UserID, int ClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {

                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassProfile cp theo
            //cp.HeadTeacherID = TeacherID
            //cp.ClassProfileID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassProfileRepository cpr = new ClassProfileRepository();
            IQueryable<ClassProfile> lstCpr = cpr.All.Where(o => o.HeadTeacherID == teacherID && (o.ClassProfileID == ClassID || ClassID == 0));
            if (lstCpr.Count() > 0)
            {
                return true;
            }

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_HEAD_TEACHER(1)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 5
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            (o.ClassID == ClassID || ClassID == 0) &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER);
            if (lstCsa.Count() > 0)
            {
                return true;
            }

            // bổ sung kiểm tra là GV giảng dạy của cấp mầm non
            ClassAssigmentRepository classAss = new ClassAssigmentRepository();
            IQueryable<ClassAssigment> iQueryClassAss = classAss.All.Where(x => (x.ClassID == ClassID || ClassID == 0) && x.IsTeacher == true
                                                                            && x.TeacherID == teacherID);

            if (iQueryClassAss != null && iQueryClassAss.Count() > 0)
            {
                return true;
            }


            //Tìm kiếm trong bảng HeadTeacherSubstitution hts theo
            //hts.HeadTeacherID = TeacherID
            //hts.ClassID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            HeadTeacherSubstitutionRepository hts = new HeadTeacherSubstitutionRepository();
            DateTime dateTimeNow = DateTime.Now;
            IQueryable<HeadTeacherSubstitution> lstHts = hts.All
                .Where(o => o.SubstituedHeadTeacher == teacherID
                        && (o.ClassID == ClassID || ClassID == 0)
                        && EntityFunctions.TruncateTime(o.AssignedDate) <= EntityFunctions.TruncateTime(dateTimeNow)
                        && EntityFunctions.TruncateTime(o.EndDate) >= EntityFunctions.TruncateTime(dateTimeNow));
            if (lstHts.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }   

        public static bool HasHeadTeacherPermission2(int UserID, string CombineClassCode)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {

                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassProfile cp theo
            //cp.HeadTeacherID = TeacherID
            //cp.ClassProfileID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassProfileRepository cpr = new ClassProfileRepository();
            IQueryable<ClassProfile> lstCpr = cpr.All.Where(o => o.HeadTeacherID == teacherID && (string.IsNullOrEmpty(o.CombinedClassCode) || o.CombinedClassCode == CombineClassCode));
            if (lstCpr.Count() > 0)
            {
                return true;
            }

            // Bổ sung tìm kiếm GV Giảng dạy của cấp mầm non
            lstCpr = cpr.All.Where(o => o.IsCombinedClass == true && (string.IsNullOrEmpty(o.CombinedClassCode) || o.CombinedClassCode == CombineClassCode));
            if (lstCpr.Count() > 0)
            {
                List<int> lstClassID = lstCpr.Select(x => x.ClassProfileID).ToList();
                ClassAssigmentRepository classAssRep = new ClassAssigmentRepository();
                IQueryable<ClassAssigment> lstClassAss = classAssRep.All.Where(x => x.TeacherID == teacherID && lstClassID.Contains(x.ClassID) && x.IsTeacher == true);
                if (lstClassAss.Count() > 0)
                {
                    return true;
                }
            }

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_HEAD_TEACHER(1)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 5
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            (o.ClassProfile.CombinedClassCode == CombineClassCode || string.IsNullOrEmpty(o.ClassProfile.CombinedClassCode)) &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER);
            if (lstCsa.Count() > 0)
            {
                return true;
            }


            //Tìm kiếm trong bảng HeadTeacherSubstitution hts theo
            //hts.HeadTeacherID = TeacherID
            //hts.ClassID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            HeadTeacherSubstitutionRepository hts = new HeadTeacherSubstitutionRepository();
            DateTime dateTimeNow = DateTime.Now;
            IQueryable<HeadTeacherSubstitution> lstHts = hts.All
                .Where(o => o.SubstituedHeadTeacher == teacherID
                         && (o.ClassProfile.CombinedClassCode == CombineClassCode || string.IsNullOrEmpty(o.ClassProfile.CombinedClassCode))
                        && EntityFunctions.TruncateTime(o.AssignedDate) <= EntityFunctions.TruncateTime(dateTimeNow)
                        && EntityFunctions.TruncateTime(o.EndDate) >= EntityFunctions.TruncateTime(dateTimeNow));
            if (lstHts.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        public static Dictionary<int, bool> HasHeadTeacherPermission(int UserID, List<int> lstClassID)
        {
            Dictionary<int, bool> dicReturn = new Dictionary<int, bool>();

            //Khoi tao
            foreach (int classID in lstClassID)
                dicReturn[classID] = false;

            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                foreach (int classID in lstClassID)
                    dicReturn[classID] = true;
                return dicReturn;
            }

            UserAccount userAccount = ua.Find(UserID);
            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (userAccount == null || userAccount.EmployeeID == null)
                return dicReturn;

            teacherID = userAccount.EmployeeID;

            //Tìm kiếm trong bảng ClassProfile cp theo
            //cp.HeadTeacherID = TeacherID
            //cp.ClassProfileID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassProfileRepository cpr = new ClassProfileRepository();
            List<int> lstCpr = cpr.All.Where(o => o.HeadTeacherID == teacherID && lstClassID.Contains(o.ClassProfileID)).Select(u => u.ClassProfileID).ToList();
            if (lstCpr.Count > 0)
                foreach (int classID in lstClassID)
                    dicReturn[classID] = lstCpr.Any(u => u == classID);

            // Bổ sung tìm kiếm là GV giảng dạy của cấp mầm non
            ClassAssigmentRepository classAss = new ClassAssigmentRepository();
            List<int> lstClassAss = classAss.All.Where(x => lstClassID.Contains(x.ClassID) && x.IsTeacher == true
                                                                             && x.TeacherID == teacherID).Select(x => x.ClassID).ToList();

            if (lstClassAss.Count() > 0)
            {
                foreach (int classID in lstClassAss)
                    dicReturn[classID] = lstClassAss.Any(u => u == classID);
            }

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_HEAD_TEACHER(1)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 5
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            List<int> lstCsa = csa.All.Where(o => o.TeacherID == teacherID
                                                    && lstClassID.Contains(o.ClassID)
                                                    && o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER).Select(u => u.ClassID).ToList();
            if (lstCsa.Count > 0)
                foreach (int classID in lstClassID)
                    dicReturn[classID] = dicReturn[classID] || lstCsa.Any(u => u == classID);

            //Tìm kiếm trong bảng HeadTeacherSubstitution hts theo
            //hts.HeadTeacherID = TeacherID
            //hts.ClassID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            HeadTeacherSubstitutionRepository hts = new HeadTeacherSubstitutionRepository();
            DateTime datetimeNow = DateTime.Now;
            List<int> lstHts = hts.All.Where(o => o.SubstituedHeadTeacher == teacherID
                                                && lstClassID.Contains(o.ClassID)
                                                && EntityFunctions.TruncateTime(o.AssignedDate) <= EntityFunctions.TruncateTime(datetimeNow)
                                                && EntityFunctions.TruncateTime(o.EndDate) >= EntityFunctions.TruncateTime(datetimeNow))
                                        .Select(u => u.ClassID)
                                        .ToList();
            if (lstHts.Count > 0)
                foreach (int classID in lstClassID)
                    dicReturn[classID] = dicReturn[classID] || lstHts.Any(u => u == classID);

            return dicReturn;
        }   

        public static bool HasHeadAdminSchoolPermission(int UserID)
        {
            UserAccountBusiness ua = new UserAccountBusiness(null);
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }
            else return false;
        }

        public static bool HasHeadTeacherApprenticeShipPermission(int UserID, int ApprenticeShipClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {

                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassProfile cp theo
            //cp.HeadTeacherID = TeacherID
            //cp.ClassProfileID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ApprenticeshipClassRepository objApprenticeshipClass = new ApprenticeshipClassRepository();

            IQueryable<ApprenticeshipClass> lstApprenticeshipClass = objApprenticeshipClass.All.Where(o => o.ApprenticeshipClassID == ApprenticeShipClassID && o.HeadTeacherID == teacherID);
            if (lstApprenticeshipClass.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        //Kiểm tra có phải GVBM không.
        public static bool HasSubjectTeacherPermission(int UserID, int ClassID, int SubjectID, int Semester = 0)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER(2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                                o.ClassID == ClassID &&
                                                                o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);

            if (lstCsa.Count() > 0)
            {
                return true;
            }

            //Tìm kiếm trong bảng TeachingAssignment ta theo
            //ta.TeacherID = TeacherID
            //ta.ClassID = ClassID 
            //ta.SubjectID = SubjectID
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            TeachingAssignmentRepository ta = new TeachingAssignmentRepository();
            IQueryable<TeachingAssignment> lstTa = ta.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.SubjectID == SubjectID &&
                                                            o.IsActive);
            if (Semester != 0)
            {
                lstTa = lstTa.Where(o => o.Semester == Semester);
            }
            if (lstTa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        public static bool HasClassAssignmentPermission(int UserID, int ClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER(2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassAssigmentRepository csa = new ClassAssigmentRepository();
            IQueryable<ClassAssigment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                                o.ClassID == ClassID
                                                                );

            if (lstCsa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        //Kiểm tra có phải GVBM không.
        public static bool HasSubjectTeacherPermission_BM(int UserID, int ClassID, int Semester = 0)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER(2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                                o.ClassID == ClassID &&
                                                                o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);

            if (lstCsa.Count() > 0)
            {
                return true;
            }

            //Tìm kiếm trong bảng TeachingAssignment ta theo
            //ta.TeacherID = TeacherID
            //ta.ClassID = ClassID 
            //ta.SubjectID = SubjectID
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            TeachingAssignmentRepository ta = new TeachingAssignmentRepository();
            IQueryable<TeachingAssignment> lstTa = ta.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            o.ClassID == ClassID &&
                                                            o.IsActive);

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstTa = lstTa.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            }
            else
            {
                lstTa = lstTa.Where(o => o.Semester == Semester);
            }
            if (lstTa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        //Tamhm1: Kiểm tra có quyền giáo viên bộ môn hay không, phục vụ cho việc hiển thị tài liệu hướng
        //dẫn sử dụng cho người dùng, không bắt buộc truyền vào kỳ, chỉ cần có quyền giáo viên bộ môn là hiển thị.
        //Kiểm tra có phải GVBM không.
        public static bool HasSubjectTeacherPermission_BM_HELP(int UserID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER(2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                                o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);

            if (lstCsa.Count() > 0)
            {
                return true;
            }

            //Tìm kiếm trong bảng TeachingAssignment ta theo
            //ta.TeacherID = TeacherID
            //ta.ClassID = ClassID 
            //ta.SubjectID = SubjectID
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            TeachingAssignmentRepository ta = new TeachingAssignmentRepository();
            IQueryable<TeachingAssignment> lstTa = ta.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            o.IsActive);

            if (lstTa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        //Kiểm tra có phải GVBM không.
        public static Dictionary<int, bool> HasSubjectTeacherPermission(int UserID, List<int> lstClassID, int SubjectID, int Semester = 0)
        {
            //Khoi tao
            Dictionary<int, bool> dicPermission = new Dictionary<int, bool>();
            foreach (int classID in lstClassID)
                dicPermission[classID] = false;

            // DungVA - 28/03/2013
            UserAccountBusiness userAccountBusiness = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = userAccountBusiness.IsSchoolAdmin(UserID);

            //Neu la admin truong 
            if (isSchoolAdmin)
            {
                foreach (int classID in lstClassID)
                    dicPermission[classID] = true;
                return dicPermission;
            }

            //Neu khong tim thay EmployeeID thi tra ve luon, khi do cac gia tri trong dictionary deu la false do khoi tao
            UserAccount userAccount = userAccountBusiness.Find(UserID);
            if (userAccount.EmployeeID == null)
                return dicPermission;

            teacherID = userAccount.EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER(2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không sang bước 4
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            List<ClassSupervisorAssignment> lstCsa = csa.All.Where(o => o.TeacherID == teacherID &&
                                                                        o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).ToList();
            foreach (int classID in lstClassID)
                dicPermission[classID] = lstCsa.Any(u => u.ClassID == classID);

            TeachingAssignmentRepository ta = new TeachingAssignmentRepository();
            List<TeachingAssignment> lstTa = ta.All.Where(o => o.TeacherID == teacherID &&
                                                                (o.SubjectID == SubjectID || SubjectID == 0) &&
                                                                (o.Semester == Semester || Semester == 0) &&
                                                                o.IsActive).ToList();
            foreach (int classID in lstClassID)
                dicPermission[classID] = dicPermission[classID] || lstTa.Any(u => u.ClassID == classID);

            return dicPermission;
        }

        //Kiểm tra có phải Ban Giám Hiệu không
        public static bool IsBGH(int UserID)
        {
            UserAccountBusiness uaBu = new UserAccountBusiness(null);
            UserAccount ua = uaBu.Find(UserID);

            if (ua == null || !ua.IsActive || ua.Employee == null || ua.Employee.WorkGroupTypeID != SystemParamsInFile.WORK_GROUP_TYPE_BANGIAMHIEU) return false;

            return true;
        }

        //Kiểm tra có quyền Giáo viên phụ trách ko (gồm cả GVCN và Admin)
        public static bool HasChargeTeacherPemission(int UserID, int ClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Nếu HasHeadTeacherPermission(UserID, ClassID) = true => return true
            bool hhtp = HasHeadTeacherPermission(UserID, ClassID);
            if (hhtp)
            {
                return true;
            }

            //Tìm kiếm trong bảng ClassAssignment ca theo
            //ca.TeacherID = TeacherID
            //ca.ClassID = ClassID or ClassID = 0
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            ClassAssigmentRepository ca = new ClassAssigmentRepository();
            IQueryable<ClassAssigment> lstCa = ca.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            (o.ClassID == ClassID || ClassID == 0));
            if (lstCa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            //End of DungVA

            //return true;
        }

        //Kiểm tra có phải Giám thị không.
        public static bool HasOverseeingTeacherPermission(int UserID, int ClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_OVERSEEING (3)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            (o.ClassID == ClassID || ClassID == 0) &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING);
            if (lstCsa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        //Kiểm tra Employee thuộc cán bộ quản lý không
        public static bool HasEmployeeAdminPermission(int employeeID)
        {
            if (employeeID <= 0)
            {
                return false;
            }
            EmployeeRepository _employeeRepository = new EmployeeRepository();
            return _employeeRepository.All.Any(x => x.EmployeeID == employeeID && x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN);
        }

        //Kiem tra quyen giam thi cho danh sach lop
        public static Dictionary<int, bool> HasOverseeingTeacherPermission(int UserID, List<int> ListClassID)
        {
            Dictionary<int, bool> dicPermission = new Dictionary<int, bool>();
            ListClassID.ForEach(u => dicPermission[u] = false);

            UserAccountBusiness uaBu = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = uaBu.IsSchoolAdmin(UserID);

            if (isSchoolAdmin)
            {
                ListClassID.ForEach(u => dicPermission[u] = true);
                return dicPermission;
            }

            UserAccount ua = uaBu.Find(UserID);

            if (ua.EmployeeID == null)
                return dicPermission;

            teacherID = ua.EmployeeID;

            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            List<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            ListClassID.Contains(o.ClassID) &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING).ToList();

            ListClassID.ForEach(u => dicPermission[u] = lstCsa.Any(v => v.ClassID == u));

            return dicPermission;
        }

        //Hỏi lại HưngND??? - .
        public static bool HasSubjectTeacherPermissionSupervising(int UserID, int ClassID)
        {
            // DungVA - 28/03/2013
            UserAccountBusiness ua = new UserAccountBusiness(null);
            int? teacherID;
            bool isSchoolAdmin = ua.IsSchoolAdmin(UserID);

            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            if (isSchoolAdmin)
            {
                return true;
            }

            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            if (ua.Find(UserID).EmployeeID == null)
            {
                return false;
            }
            teacherID = ua.Find(UserID).EmployeeID;

            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER (2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false

            ClassSupervisorAssignmentRepository csa = new ClassSupervisorAssignmentRepository();
            IQueryable<ClassSupervisorAssignment> lstCsa = csa.All.Where(
                                                            o => o.TeacherID == teacherID &&
                                                            (o.ClassID == ClassID || ClassID == 0) &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER);
            if (lstCsa.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            // End of DungVA

            //return true;
        }

        // End Of DungVA


        public static bool HasOverSeePermission(int UserID, int ClassID)
        {
            return true;
        }

        public static bool HasHeadTeacherPermissionSupervising(int UserID, int ClassID)
        {
            //UserAccountBusiness.IsSchoolAdmin(UserID) = true return true
            //Nếu UserAccount(UserID).EmployeeID = null trả về false
            //TeacherID = UserAccount(UserID).EmployeeID
            //Tìm kiếm trong bảng ClassSupervisorAssignment csa theo
            //csa.TeacherID = TeacherID
            //csa.ClassID = ClassID or ClassID = 0
            //csa.PermissionLevel = SUPERVISING_PERMISSION_SUBJECT_TEACHER (2)
            //Nếu tìm thấy bản ghi nào thỏa mãn. Return true. Nếu không return false
            return true;
        }


        public static void CheckValidateMark(int AppliedLevel, int IsCommenting, string Mark)
        {
            //- Nếu AppliedLevel = 1:
            //  + Nếu IsCommenting = 0 -> Mark phải sô nguyên từ 0 -> 10
            //  + Nếu IsCommenting = 1 -> dữ liệu phải là các giá trị A+, A, B
            //- Nếu AppliedLevel = 2,3:
            //  + Nếu IsCommenting = 0 -> Mark phải sô thực từ 0 -> 10, chỉ có 1 chữ số sau dấu thập phân
            //  + Nếu IsCommenting = 1 -> Mark phải là các giá trị Đ, CĐ

            if (Mark != null && Mark != "")
            {
                if (AppliedLevel == 1)
                {
                    if (IsCommenting == 0)
                    {
                        double mark = Convert.ToDouble(Mark);
                        if (mark < 0.0 || mark > 10.0)
                        {
                            throw new BusinessException("Common_Validate_MarkNotInRange", (new object[] { 0.0, 10.0 }).ToList());
                        }
                    }
                    if (IsCommenting == 1)
                    {
                        string[] strmarks = new string[] { "A+", "A", "B" };
                        if (!strmarks.Contains(Mark.ToUpper().Trim()))
                        {
                            throw new BusinessException("Common_Validate_MarkNotInRange", (new object[] { "A+", "B" }).ToList());
                        }
                    }
                }
                else if (AppliedLevel == 2 || AppliedLevel == 3)
                {
                    if (IsCommenting == 0)
                    {
                        double mark = -1;
                        try
                        {
                            mark = Convert.ToDouble(Mark);
                        }
                        catch
                        {
                            throw new BusinessException("Common_Validate_InvalidMark");
                        }

                        if (mark < 0.0 || mark > 10.0)
                        {
                            throw new BusinessException("Common_Validate_MarkNotInRange", (new object[] { 0.0, 10.0 }).ToList());
                        }
                    }
                    if (IsCommenting == 1)
                    {
                        string[] strmarks = new string[] { "Đ", "CĐ" };
                        if (!strmarks.Contains(Mark.ToUpper().Trim()))
                        {
                            throw new BusinessException("Common_Validate_MarkNotInRange", (new object[] { "CĐ", "Đ" }).ToList());
                        }
                    }
                }
            }
        }

        //PhuongH - 22/05/2013 - Thêm tiêu chí tìm kiếm theo kỳ: xem lớp học A có học môn B trong kỳ X
        public static IQueryable<ClassSubject> AddCriteriaSemester(this IQueryable<ClassSubject> Query, int Semester)
        {
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(o => o.SectionPerWeekFirstSemester > 0 && o.SectionPerWeekSecondSemester > 0);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(o => o.SectionPerWeekFirstSemester > 0);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                Query = Query.Where(o => o.SectionPerWeekSecondSemester > 0);
            }
            return Query;
        }

        #region Sap xep thu tu lop hoc cho dung
        public static List<ClassProfile> ToList(this IQueryable<ClassProfile> Query)
        {
            return Query.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList<ClassProfile>();
        }

        public static List<ClassProfileTempBO> ToList(this IQueryable<ClassProfileTempBO> Query)
        {
            return Query.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList<ClassProfileTempBO>();
        }
        #endregion

        //PhuongH - 22/05/2013 - Thêm tiêu chí tìm kiếm theo kỳ: xem học sinh A có được miễn giảm toàn phần (không có điểm trung bình môn) trong kỳ X không
        public static IQueryable<ExemptedSubject> AddCriteriaSemester(this IQueryable<ExemptedSubject> Query, int Semester)
        {
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(o => o.FirstSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL
                    && o.SecondSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(o => o.FirstSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                Query = Query.Where(o => o.SecondSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL);
            }
            return Query;
        }

        //PhuongH - 22/05/2013 - Thêm tiêu chí tìm kiếm các học sinh có học trong kỳ
        public static IQueryable<PupilOfClass> AddCriteriaSemester(this IQueryable<PupilOfClass> Query, AcademicYear AcademicYear, int Semester)
        {
            List<int> listValidStatus = new List<int> {
                SystemParamsInFile.PUPIL_STATUS_STUDYING,
                SystemParamsInFile.PUPIL_STATUS_GRADUATED
            };
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.FirstSemesterEndDate
                && (listValidStatus.Contains(x.Status)
                || x.EndDate > AcademicYear.FirstSemesterEndDate));
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(x => listValidStatus.Contains(x.Status)
                        && x.AssignedDate <= AcademicYear.SecondSemesterEndDate
                        );
            }
            return Query;
        }

        //Viethd - 03/03/2015 - Thêm tiêu chí tìm kiếm các học sinh có học trong kỳ với đầy đủ status
        public static IQueryable<PupilOfClass> AddCriteriaSemesterFullStatus(this IQueryable<PupilOfClass> Query, AcademicYear AcademicYear, int Semester)
        {
            List<int> listValidStatus = new List<int> {
                SystemParamsInFile.PUPIL_STATUS_STUDYING,
                SystemParamsInFile.PUPIL_STATUS_GRADUATED,
                SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF,
                SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
            };
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.FirstSemesterEndDate
                && (listValidStatus.Contains(x.Status)
                || x.EndDate > AcademicYear.FirstSemesterEndDate));
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(x => listValidStatus.Contains(x.Status)
                        && x.AssignedDate <= AcademicYear.SecondSemesterEndDate
                        );
            }
            return Query;
        }

        //TUTV - 14/11/2014 - Thêm tiêu chí tìm kiếm các học sinh có học trong kỳ không kiểm tra status
        public static IQueryable<PupilOfClass> AddCriteriaSemesterNoCheckStatus(this IQueryable<PupilOfClass> Query, AcademicYear AcademicYear, int Semester)
        {
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.FirstSemesterEndDate);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.SecondSemesterEndDate);
            }
            return Query;
        }
        //HoanTV5 loc theo cau hinh 
        public static IQueryable<PupilOfClass> AddPupilStatus(this IQueryable<PupilOfClass> Query, bool isNotShow)
        {
            if (isNotShow)
            {
                Query = Query.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            return Query;
        }

        //hath8 - convert capacitylevel
        public static string ConvertCapacity(string CapacityLevel)
        {
            string result = "";
            if (CapacityLevel != null)
            {
                result = CapacityLevel;
                if (CapacityLevel.Trim().ToUpper() == "GIỎI")
                {
                    result = "G";
                }
                if (CapacityLevel.Trim().ToUpper() == "KHÁ")
                {
                    result = "K";
                }
                if (CapacityLevel.Trim().ToUpper() == "TRUNG BÌNH" || CapacityLevel.Trim().ToUpper() == "TB")
                {
                    result = "Tb";
                }
                if (CapacityLevel.Trim().ToUpper() == "YẾU")
                {
                    result = "Y";
                }
                if (CapacityLevel.Trim().ToUpper() == "TỐT")
                {
                    result = "T";
                }
                if (CapacityLevel.Trim().ToUpper() == "HỌC SINH GIỎI")
                {
                    result = "HSG";
                }
                if (CapacityLevel.Trim().ToUpper() == "HỌC SINH TIÊN TIẾN")
                {
                    result = "HSTT";
                }
                if (CapacityLevel.Trim().ToUpper() == "RÈN LUYỆN LẠI")
                {
                    result = "RLKTH";
                }
            }
            return result;
        }

        //Lay si so cua lop trong truong hop chuyen truong chuyen lop
        public static IQueryable<PupilOfClass> GetPupil(this IQueryable<PupilOfClass> Query, AcademicYear AcademicYear, int? Semester)
        {
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.FirstSemesterEndDate);
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(x => x.AssignedDate <= AcademicYear.SecondSemesterEndDate);
            }
            return Query;
        }
        #endregion

        public static Stream ToStream(string pathFileTemp)
        {
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFileTemp))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            return memStream;
        }

        //Tamhm1 - su dung xuat excel
        public static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        /// <summary>
        /// Tao partion theo School mod 100;
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public static int GetPartionId(int schoolId, int defaultNumer = 100)
        {
            return schoolId % defaultNumer;
        }

        /// <summary>
        /// Partition theo tinh thanh
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="defaultNumer"></param>
        /// <returns></returns>
        public static int GetPartionProvince(int provinceId)
        {
            return provinceId % 64;
        }



        /// <summary>
        /// Kiem tra nam hoc dang thuc hien da chuyen du lieu vao lich su chua. true: da chuyen vao lich su, false: du lieu nam hoc hien tai
        /// </summary>
        /// <param name="currentYear"></param>
        /// <returns></returns>
        public static bool IsMoveHistory(AcademicYear objAcademicYear)
        {
            if (objAcademicYear != null && objAcademicYear.IsMovedHistory.HasValue && objAcademicYear.IsMovedHistory == 1)
                return true;
            return false;
        }

        /// <summary>
        /// Kiem tra cot diem cua tieu hoc da khoa chua: true: da khoa
        /// </summary>
        /// <param name="semesterID"></param>
        /// <param name="strLockMark">cac thang bi khoa</param>
        /// <param name="title"></param>              
        /// <param name="strFinal">Cot cuoi ky</param>
        /// <returns></returns>
        public static bool CheckLockMarkBySemester(int semesterID, string strLockMark, string title, string strFinal)
        {
            string str_disabled = string.Empty;
            string strSemester = string.Empty;

            // kiem tra hoc ki
            if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                strSemester = "HK1";
                if (!string.IsNullOrEmpty(strFinal))
                {
                    strFinal = "CK1";
                }
            }
            else if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "HK2";
                if (title == "T1") title = "T6";
                else if (title == "T2") title = "T7";
                else if (title == "T3") title = "T8";
                else if (title == "T4") title = "T9";
                else if (title == "T5") title = "T10";
                if (!string.IsNullOrEmpty(strFinal))
                {
                    strFinal = "CK2";
                }
            }

            strLockMark = strLockMark.Replace("T10", "TH10");
            title = title.Replace("T10", "TH10");

            if (strLockMark.Contains(title) || strLockMark.Contains(strSemester) || (!string.IsNullOrEmpty(strFinal) && strLockMark.Contains(strFinal)))
            {
                return true;
            }
            return false;

        }

        /// <summary>
        /// Chiendd: 05/05/2015, Lay thuoc dien hoc sinh.
        /// Do trong bang StudyingJudgement khong co thuoc dien cho hoc sinh cap 2, 3
        /// </summary>
        /// <param name="pr"></param>
        /// <param name="applied_level"></param>
        /// <returns></returns>
        public static string GetStudyingJudgementResolution(PupilRankingBO pr, int applied_level)
        {
            if (pr == null || pr.StudyingJudgementID == null)
            {
                return string.Empty;
            }
            if (pr.StudyingJudgementID.HasValue && pr.StudyingJudgementID != 5)
            {
                return pr.StudyingJudgementResolution;
            }

            if (applied_level == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                return GlobalConstants.PUPLRANKING_GRADUATION1;
            }
            else if (applied_level == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                return GlobalConstants.PUPLRANKING_GRADUATION2;
            }
            else if (applied_level == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                return GlobalConstants.PUPLRANKING_GRADUATION3;
            }
            return string.Empty;

        }

        public static String JoinPupilId(List<int> lstPupilId)
        {
            if (lstPupilId == null)
            {
                return "";
            }
            String pupilIds = String.Join(",", lstPupilId.ToArray());
            return pupilIds;
        }

        public static string RemoveSpecialCharacter(string text)
        {
            text = text.Replace("'", "");
            text = text.Replace("@", "");
            text = text.Replace("/*", "");
            text = text.Replace("\"", "");
            text = text.Replace("//", "");
            return text;
        }

        public static List<YearBO> GetListYearDOET(List<int> listYear)
        {
            YearBO yearobj = null;
            List<YearBO> listResult = new List<YearBO>();
            if (listYear != null && listYear.Count > 0)
            {
                string displayYear = string.Empty;
                for (int i = 0; i < listYear.Count; i++)
                {
                    yearobj = new YearBO();
                    displayYear = listYear[i] + " - " + (listYear[i] + 1);
                    yearobj.YearID = listYear[i];
                    yearobj.DisplayYear = displayYear;
                    listResult.Add(yearobj);
                }
            }
            return listResult;
        }

        public static int GetNumTask
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["NumTaskReport"] != null)
                {
                    return Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["NumTaskReport"]);
                }

                return 50;
            }
        }
        /// <summary>
        /// Lay danh sach cac cap truong theo cap hoc
        /// </summary>
        /// <param name="appliedLevel"></param>
        /// <returns></returns>
        public static List<int> GetEducationGrade(int appliedLevel)
        {

            List<int> lstVal;
            if (appliedLevel == 2)
            {
                lstVal = new List<int>(new int[5] { 2, 3, 6, 7, 31 });
                return lstVal;
            }
            else if (appliedLevel == 3)
            {
                lstVal = new List<int>(new int[4] { 4, 6, 7, 31 });
                return lstVal;
            }
            else if (appliedLevel == 1)
            {
                lstVal = new List<int>(new int[4] { 1, 3, 7, 31 });
                return lstVal;
            }
            else if (appliedLevel == 4 || appliedLevel == 5)
            {
                lstVal = new List<int>(new int[4] { 8, 16, 24, 31 });
                return lstVal;
            }
            return new List<int>();
        }

        /// <summary>
        /// Lay danh sach cap hoc tu cap truong. 
        /// </summary>
        /// <param name="educationGradeId"></param>
        /// <returns></returns>
        public static List<int> GetAppliedLevelbyEducationGrade(int educationGradeId)
        {
            List<int> lstVal = new List<int>();
            switch (educationGradeId)
            {
                case 1: //tieu hoc
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_PRIMARY);
                        break;
                    }
                case 2: //thcs
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                        break;
                    }
                case 3: //ptcs
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_PRIMARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                        break;
                    }
                case 4: //thpt
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_TERTIARY);
                        break;
                    }
                case 6: //ptth
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_TERTIARY);
                        break;
                    }
                case 7: //pt
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_PRIMARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_TERTIARY);
                        break;
                    }
                case 8: //nt
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_CRECHE);

                        break;
                    }
                case 16: //mg
                    {

                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN);

                        break;
                    }
                case 24: //MN
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_CRECHE);

                        break;
                    }
                case 31: //MN+PT
                    {
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_CRECHE);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_PRIMARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                        lstVal.Add(GlobalConstants.APPLIED_LEVEL_TERTIARY);
                        break;
                    }
                default:
                    break;
            }
            return lstVal;
        }

        /// <summary>
        /// Lay danh sach buoi hoc tu danh sach lop
        /// </summary>
        /// <param name="lstCP"></param>
        /// <returns></returns>
        public static List<int> GetListSectionID(List<ClassProfile> lstCP)
        {
            List<int?> lstSetionId = lstCP.Select(s => s.Section).Distinct().ToList();


            List<int> lstVal = new List<int>();
            List<int> lstSession = null;

            for (int i = 0; i < lstSetionId.Count; i++)
            {
                lstSession = GetListSectionID(lstSetionId[i] ?? 0);
                if (lstSession != null)
                {
                    lstVal.AddRange(lstSession);
                }
            }
            lstVal = lstVal.Distinct().OrderBy(o => o).ToList();
            return lstVal;

        }
        public static List<int> GetListSectionID(int SectionID)
        {
            List<int> lstResult = new List<int>();
            switch (SectionID)
            {
                case SystemParamsInFile.SECTION_MORNING:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_MORNING);
                        break;
                    }
                case SystemParamsInFile.SECTION_AFTERNOON:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_AFTERNOON);
                        break;
                    }
                case SystemParamsInFile.SECTION_EVENING:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_EVENING);
                        break;
                    }
                case SystemParamsInFile.SECTION_MORNING_AFTERNOON:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_MORNING);
                        lstResult.Add(SystemParamsInFile.SECTION_AFTERNOON);
                        break;
                    }
                case SystemParamsInFile.SECTION_MORNING_EVENING:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_MORNING);
                        lstResult.Add(SystemParamsInFile.SECTION_EVENING);
                        break;
                    }
                case SystemParamsInFile.SECTION_AFTERNOON_EVENING:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_AFTERNOON);
                        lstResult.Add(SystemParamsInFile.SECTION_EVENING);
                        break;
                    }
                case SystemParamsInFile.SECTION_ALL:
                    {
                        lstResult.Add(SystemParamsInFile.SECTION_MORNING);
                        lstResult.Add(SystemParamsInFile.SECTION_AFTERNOON);
                        lstResult.Add(SystemParamsInFile.SECTION_EVENING);
                        break;
                    }
            }
            return lstResult;
        }

        /// <summary>
        /// chiendd1: 25.05.206. Tach 1 list ra nhieu list nho theo kich thuoc cho truoc
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="locations"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        public static List<List<T>> SplitListToMultipleList<T>(List<T> locations, int nSize = 500)
        {
            var list = new List<List<T>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }

        /// <summary>
        /// Lay hoc ky theo ngay truyen vao
        /// </summary>
        /// <param name="objAcademicYear"></param>
        /// <param name="dtValue"></param>
        /// <returns></returns>
        public static int GetSemesterbyDate(AcademicYear objAcademicYear, DateTime dtValue)
        {
            if (objAcademicYear == null)
            {
                return GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            if (objAcademicYear.FirstSemesterStartDate <= dtValue && dtValue <= objAcademicYear.FirstSemesterEndDate)
            {
                return GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }

            return GlobalConstants.SEMESTER_OF_YEAR_SECOND;
        }

        /// <summary>
        /// Kiem tra thoi gian nam hoc co phai phai la nam hoc hien tai hay khong
        /// </summary>
        /// <param name="objAcademicYear"></param>
        /// <returns></returns>
        public static bool IsCurrentAcademicYear(AcademicYear objAcademicYear)
        {
            if (objAcademicYear == null)
            {
                return false;
            }
            if (objAcademicYear.FirstSemesterStartDate <= DateTime.Now &&
                DateTime.Now <= objAcademicYear.SecondSemesterEndDate)
            {
                return true;
            }
            return false;
        }

        //Get Url SSO
        public static string UrlSSO
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstants.URL_SSO];
            }
        }

        //Get Url SMAS
        public static string UrlSmas
        {
            get
            {
                try
                {
                    string url = WebConfigurationManager.AppSettings[GlobalConstants.URL_SMAS];
                    if (string.IsNullOrEmpty(url))
                    {
                        url = HttpContext.Current.Request.Url.Authority;
                    }
                    return url;
                }
                catch
                {
                    return HttpContext.Current.Request.Url.Authority;
                }

            }
        }

        public static string GetSupervisingDeptName(int SchoolID, int AppliedLevelID)
        {
            List<SupervisingDept> lstSup = new List<SupervisingDept>();
            SupervisingDept objSupervisingDept = new SupervisingDept();
            SupervisingDeptRepository Suprepository = new SupervisingDeptRepository();
            SchoolProfileRepository SchoolProfileRepository = new SchoolProfileRepository();
            SchoolProfile objSP = SchoolProfileRepository.Find(SchoolID);
            int EducationGrade = objSP != null ? objSP.EducationGrade : 0;
            int DistrictID = objSP != null && objSP.DistrictID.HasValue ? objSP.DistrictID.Value : 0;
            string SupervisingDeptName = string.Empty;
            if (EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY
                || EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY
                || EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL
                || EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS)
            {
                if (AppliedLevelID == SystemParamsInFile.APPLIED_LEVEL_PRIMARY || AppliedLevelID == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    lstSup = Suprepository.All.Where(p => p.DistrictID == DistrictID).ToList();
                    objSupervisingDept = lstSup.Count > 0 ? lstSup.FirstOrDefault() : null;
                    SupervisingDeptName = objSupervisingDept != null ? objSupervisingDept.SupervisingDeptName : "";
                }
                else
                {
                    objSupervisingDept = Suprepository.Find(objSP.SupervisingDeptID);
                    SupervisingDeptName = objSupervisingDept != null ? objSupervisingDept.SupervisingDeptName : "";
                }
            }
            else
            {
                objSupervisingDept = Suprepository.Find(objSP.SupervisingDeptID);
                SupervisingDeptName = objSupervisingDept != null ? objSupervisingDept.SupervisingDeptName : "";
            }
            return SupervisingDeptName;
        }

        //Get Url SMS_SCHOOL
        public static string UrlSmsSchool
        {
            get
            {
                return WebConfigurationManager.AppSettings[GlobalConstants.URL_SMS_SCHOOL];
            }
        }

        /// <summary>
        /// Lay dia chi day du cua url va action
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string GetFullUrl(string returnUrl, string action)
        {

            if (string.IsNullOrEmpty(action))
            {
                return returnUrl;
            }
            return string.Format("{0}/{1}", returnUrl, action);
        }

        public static ProcessedReport ValidateProcessReport(int ProcessReportID, int SchoolID, int AcademicYearID)
        {
            ProcessedReport objProcessReport = new ProcessedReport();
            ProcessedReportBusiness ProcessReportB = new ProcessedReportBusiness(null);
            objProcessReport = ProcessReportB.Find(ProcessReportID);
            if (SchoolID == 0)
            {
                return objProcessReport;
            }
            else if (objProcessReport.InputParameterHashKey.Contains(SchoolID.ToString()) || objProcessReport.InputParameterHashKey.Contains(AcademicYearID.ToString()))
            {
                return objProcessReport;
            }
            else
            {
                throw new BusinessException("Không tìm thấy báo cáo này");
            }
        }


        public static string CreateSourceDB(string sourcedb, string input)
        {
            if (string.IsNullOrEmpty(sourcedb) && !string.IsNullOrEmpty(input))
            {
                return input;
            }
            if (!string.IsNullOrEmpty(sourcedb) && sourcedb.Contains(input))
            {
                return sourcedb;
            }
            return string.Format("{0},{1}", sourcedb, input);
        }

        public static void KPILog(KPILog kpi)
        {
            if (StaticData.lstKPIFunction == null)
            {
                return;
            }
            KPIFunction objFunction = StaticData.lstKPIFunction.FirstOrDefault(s => s.ActionName.Contains(kpi.ServiceCode) && s.Controller.Contains(kpi.ActionName));
            if (objFunction == null)
            {
                return;
            }

            string ip = LogExtensions.GetIPAddress();
            string yearMonth = kpi.StartTime.ToString("yyyy") + kpi.StartTime.ToString("MM");
            int partitionMonth = Convert.ToInt32(yearMonth);
            OracleConnection conn = new OracleConnection(WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString());

            try
            {
                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }
                string sqlQuery = "INSERT INTO KPI_LOG(APPLICATION_CODE,SERVICE_CODE,SESSION_ID,IP_PORT_PARENT_NODE,IP_PORT_CURRENT_NODE,REQUEST_CONTENT,RESPONSE_CONTENT,START_TIME,END_TIME,DURATION,ERROR_CODE,ERROR_DESCRIPTION,TRANSACTION_STATUS,ACTION_NAME,USERNAME,ACCOUNT,PARTITION_MONTH) VALUES(:v1,:v2,:v3,:v4,:v5,:v6,:v7,:v8,:v9,:v10,:v11,:v12,:v13,:v14,:v15,:v16,:v17)";
                OracleCommand cmd = new OracleCommand(sqlQuery, conn);
                cmd.Parameters.Add("v1", OracleDbType.NVarchar2, objFunction.ApplicationCode, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v2", OracleDbType.NVarchar2, objFunction.KPIFunctionCode, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v3", OracleDbType.NVarchar2, kpi.SessionID, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v4", OracleDbType.NVarchar2, ip, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v5", OracleDbType.NVarchar2, kpi.IPPortCurrentNode, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v6", OracleDbType.NVarchar2, kpi.RequestContent, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v7", OracleDbType.NVarchar2, kpi.ResponseContent, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v8", OracleDbType.Date, kpi.StartTime, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v9", OracleDbType.Date, kpi.EndTime, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v10", OracleDbType.NVarchar2, kpi.Duration, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v11", OracleDbType.Int32, kpi.ErrorCode, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v12", OracleDbType.NVarchar2, kpi.ErrorDescription, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v13", OracleDbType.Int32, kpi.TransactionStatus, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v14", OracleDbType.NVarchar2, kpi.ActionName, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v15", OracleDbType.NVarchar2, kpi.UserName, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v16", OracleDbType.NVarchar2, kpi.Account, System.Data.ParameterDirection.Input);
                cmd.Parameters.Add("v17", OracleDbType.Int32, partitionMonth, System.Data.ParameterDirection.Input);
                cmd.CommandTimeout = 0;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }


        }

        public static byte[] FileToByteArray(string _FileName)
        {
            byte[] _Buffer = null;
            System.IO.FileStream _FileStream = null;
            System.IO.BinaryReader _BinaryReader = null;
            try
            {
                // Open file for reading
                _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // attach filestream to binary reader
                _BinaryReader = new System.IO.BinaryReader(_FileStream);

                // get total int length of the file
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);

                return _Buffer;
            }
            catch (Exception _Exception)
            {
                // Error                
                LogExtensions.ErrorExt(logger, DateTime.Now, "FileToByteArray", "null", _Exception);
                return null;
            }
            finally
            {
                if (_FileStream != null)
                {
                    _FileStream.Close();
                    _FileStream.Dispose();
                }

                // close file reader
                if (_BinaryReader != null)
                {
                    _BinaryReader.Close();
                    _BinaryReader.Dispose();
                }
            }
        }


       

        /// <summary>
        /// Lay cap hoc phuc vu cho ky thi o HCM
        /// </summary>
        /// <param name="educationGradeId"></param>
        /// <returns></returns>
        public static int GetAppliedHCM(int educationGradeId)
        {
            int apppliedLevelId = 123;
            switch (educationGradeId)
            {
                case 1: //tieu hoc
                    {
                        apppliedLevelId=1;
                        break;
                    }
                case 2: //thcs
                    {
                        apppliedLevelId= 2;
                        break;
                    }
                case 3: //ptcs
                    {
                        apppliedLevelId = 12;
                        break;
                    }
                case 4: //thpt
                    {
                        apppliedLevelId = 3;
                        break;
                    }
                case 6: //ptth
                    {
                        apppliedLevelId = 23;
                        break;
                    }
                case 7: //pt
                    {
                        apppliedLevelId = 123;
                        break;
                    }
                
                case 31: //MN+PT
                    {
                        apppliedLevelId = 123;
                        break;
                    }
                default:
                    break;
            }
            return apppliedLevelId;
        }

        public static Dictionary<string, object> GetListColumnDescription()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            using (SMASEntities context = new SMASEntities())
            {
                List<ColumnDescription> lstCd = context.Set<ColumnDescription>().ToList();
                foreach (var item in lstCd)
                {
                    try
                    {
                        if (item.ColumnOrder == 0)
                        {
                            dic.Add(item.FieldName, item.Resolution);
                        }
                        else
                        {
                            dic.Add(item.FieldName, string.Format("{0}. {1}", item.ColumnOrder, item.Resolution));
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            return dic;
        }


        public static IQueryable<EmployeeHistoryStatus> AddCriteriaEmployee(this IQueryable<EmployeeHistoryStatus> Query, AcademicYear AcademicYear, int Semester)
        {

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Query = Query.Where(x => x.FromDate <= AcademicYear.FirstSemesterEndDate && (x.ToDate == null || x.ToDate > AcademicYear.FirstSemesterStartDate));
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Query = Query.Where(x => (x.FromDate <= AcademicYear.SecondSemesterEndDate) && (x.ToDate == null || x.ToDate > AcademicYear.SecondSemesterEndDate));
            }
            return Query;
        }

    }
}
