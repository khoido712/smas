﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using SMAS.Models.Models;

namespace SMAS.Business.Common
{
    public class ReportUtils
    {
        public static string ConvertMarkForM(decimal mark)
        {
            string markOutput = "";
            if (mark == 0)
            {
                markOutput = "0";
            }
            else
            {
                markOutput = mark.ToString("#");
            }
            return markOutput;
        }

        public static string ConvertMarkForP(decimal mark)
        {
            string markOutput = "";
            if (mark == 10)
            {
                markOutput = "10";
            }
            else if (mark == 0)
            {
                markOutput = "0";
            }
            else if (mark.ToString().StartsWith("0"))
            {
                markOutput = mark.ToString("0.#").Replace(",", ".");
            }
            else
            {
                markOutput = mark.ToString("#.0").Replace(",", ".");
            }
            return markOutput;
            //return (mark == 10) ? "10" : mark.ToString("#.0").Replace(',', '.');
        }

        public static string ConvertMarkForV(decimal mark)
        {
            string markOutput = "";
            if (mark == 10)
            {
                markOutput = "10";
            }
            else if (mark == 0)
            {
                markOutput = "0";
            }
            else if (mark.ToString().Trim().StartsWith("0"))
            {
                markOutput = mark.ToString("0.#").Replace(",", ".");
            }
            else
            {
                markOutput = mark.ToString("#.0").Replace(',', '.');
            }
            return markOutput;
        }
        public static string ConvertMarkForV_SGTGD(decimal mark, bool isSoHCM)
        {

            string markOutput = "";
            if (isSoHCM)
            {
                if (mark == 10)
                {
                    markOutput = "10";
                }
                else if (mark == 0)
                {
                    markOutput = "0";
                }
                else if (mark < 2 && !mark.ToString().Trim().StartsWith("0"))
                {
                    string temp = mark.ToString("#.0").Replace(',', '.');
                    markOutput = "0" + temp;
                }
                else if (mark.ToString().Trim().StartsWith("0"))
                {
                    markOutput = mark.ToString("00.#").Replace(",", ".");
                }
                else
                {
                    markOutput = mark.ToString("#.0").Replace(',', '.');
                }
            }
            else
            {
                if (mark == 10)
                {
                    markOutput = "10";
                }
                else if (mark == 0)
                {
                    markOutput = "0";
                }
                else if (mark.ToString().Trim().StartsWith("0"))
                {
                    markOutput = mark.ToString("0.#").Replace(",", ".");
                }
                else
                {
                    markOutput = mark.ToString("#.0").Replace(',', '.');
                }
            }

            return markOutput;
        }
        public static string ConvertMarkForM_SGTGD(decimal mark, bool isSoHCM)
        {
            string markOutput = "";
            if (isSoHCM)
            {
                if (mark == 0)
                {
                    markOutput = "0";
                }
                else if (mark == 1)
                {
                    markOutput = "01";
                }
                else
                {
                    markOutput = mark.ToString("#");
                }
            }
            else
            {
                if (mark == 0)
                {
                    markOutput = "0";
                }
                else
                {
                    markOutput = mark.ToString("#");
                }
            }
            return markOutput;
        }
        public static List<string> ConvertListMarkForV(List<decimal?> lstmark, bool isSoHCM)
        {
            List<string> lstOutput = new List<string>();
            for (int i = 0; i < lstmark.Count(); i++)
            {
                string mark = ConvertMarkForV_SGTGD(lstmark[i].Value, isSoHCM);
                lstOutput.Add(mark);
            }
            return lstOutput;
        }
        public static List<string> ConvertListMarkForM(List<decimal?> lstmark, bool isSoHCM)
        {
            List<string> lstOutput = new List<string>();
            for (int i = 0; i < lstmark.Count(); i++)
            {
                string mark = ConvertMarkForM_SGTGD(lstmark[i].Value, isSoHCM);
                lstOutput.Add(mark);
            }
            return lstOutput;
        }
        public static string ConvertMarkForSecondaryLevel(decimal mark)
        {
            string result = "";
            if (mark == 0)
            {
                result = "0";
            }
            else if (mark.ToString().StartsWith("0"))
            {
                result = mark.ToString("0.#").Replace(",", ".");
            }
            else if (Math.Round(mark) == mark)
            {
                result = mark.ToString("#");
            }
            else
            {
                result = mark.ToString("#.0").Replace(',', '.');
            }
            return result;
        }

        public static byte[] Compress(Stream input)
        {
            using (var compressStream = new MemoryStream())
            using (var compressor = new DeflateStream(compressStream, CompressionMode.Compress))
            {
                input.CopyTo(compressor);
                compressor.Close();
                return compressStream.ToArray();
            }
        }

        public static Stream Decompress(byte[] input)
        {
            var output = new MemoryStream();

            using (var compressStream = new MemoryStream(input))
            using (var decompressor = new DeflateStream(compressStream, CompressionMode.Decompress))
                decompressor.CopyTo(output);

            output.Position = 0;
            return output;
        }

        public static string GetHashKey(IDictionary<string, object> dic)
        {
            string result = "";
            List<string> keys = dic.Keys.OrderBy(x => x.ToString()).ToList(); ;
            foreach (string key in keys)
            {
                result += "{" + key + " : " + dic[key] + "}";
            }
            return result;
        }

        public static string ConvertSemesterForReportName(int semester)
        {
            string result = "";
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                result = "HKI";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                result = "HKII";
            }
            else
            {
                result = "CN";
            }
            return result;
        }

        public static string ConvertSemesterForDisplayTitle(int semester)
        {
            string result = "";
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                result = "Học kỳ I";
            }
            else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                result = "Học kỳ II";
            }
            else
            {
                result = "Cả năm";
            }
            return result;
        }

        public static string ConvertAppliedLevelForReportName(int appliedLevel)
        {
            string result = "";
            if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                result = "TH";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                result = "THCS";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                result = "THPT";
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                result = "NT";
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                result = "MG";
            }
            return result;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            str = StripVNSign(str);
            StringBuilder sb = new StringBuilder();
            string strReplace = "";
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') ||
                    (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                strReplace = sb.ToString().Replace("__", "_");
            }
            return strReplace;
        }

        /// <summary>
        /// chuyển thành tiếng việt không dấu
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSign(string text)
        {
            //Ky tu dac biet

            /*for (int i = 32; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "-");
            }*/
            //text = text.Replace(".", "-");
            //text = text.Replace(" ", "-");
            //text = text.Replace(",", "-");
            //text = text.Replace(";", "-");
            //text = text.Replace(":", "-");
            //'Dấu Ngang
            text = text.Replace("A", "A");
            text = text.Replace("a", "a");
            text = text.Replace("Ă", "A");
            text = text.Replace("ă", "a");
            text = text.Replace("Â", "A");
            text = text.Replace("â", "a");
            text = text.Replace("E", "E");
            text = text.Replace("e", "e");
            text = text.Replace("Ê", "E");
            text = text.Replace("ê", "e");
            text = text.Replace("I", "I");
            text = text.Replace("i", "i");
            text = text.Replace("O", "O");
            text = text.Replace("o", "o");
            text = text.Replace("Ô", "O");
            text = text.Replace("ô", "o");
            text = text.Replace("Ơ", "O");
            text = text.Replace("ơ", "o");
            text = text.Replace("U", "U");
            text = text.Replace("u", "u");
            text = text.Replace("Ư", "U");
            text = text.Replace("ư", "u");
            text = text.Replace("Y", "Y");
            text = text.Replace("y", "y");

            //    'Dấu Huyền
            text = text.Replace("À", "A");
            text = text.Replace("à", "a");
            text = text.Replace("Ằ", "A");
            text = text.Replace("ằ", "a");
            text = text.Replace("Ầ", "A");
            text = text.Replace("ầ", "a");
            text = text.Replace("È", "E");
            text = text.Replace("è", "e");
            text = text.Replace("Ề", "E");
            text = text.Replace("ề", "e");
            text = text.Replace("Ì", "I");
            text = text.Replace("ì", "i");
            text = text.Replace("Ò", "O");
            text = text.Replace("ò", "o");
            text = text.Replace("Ồ", "O");
            text = text.Replace("ồ", "o");
            text = text.Replace("Ờ", "O");
            text = text.Replace("ờ", "o");
            text = text.Replace("Ù", "U");
            text = text.Replace("ù", "u");
            text = text.Replace("Ừ", "U");
            text = text.Replace("ừ", "u");
            text = text.Replace("Ỳ", "Y");
            text = text.Replace("ỳ", "y");

            //'Dấu Sắc
            text = text.Replace("Á", "A");
            text = text.Replace("á", "a");
            text = text.Replace("Ắ", "A");
            text = text.Replace("ắ", "a");
            text = text.Replace("Ấ", "A");
            text = text.Replace("ấ", "a");
            text = text.Replace("É", "E");
            text = text.Replace("é", "e");
            text = text.Replace("Ế", "E");
            text = text.Replace("ế", "e");
            text = text.Replace("Í", "I");
            text = text.Replace("í", "i");
            text = text.Replace("Ó", "O");
            text = text.Replace("ó", "o");
            text = text.Replace("Ố", "O");
            text = text.Replace("ố", "o");
            text = text.Replace("Ớ", "O");
            text = text.Replace("ớ", "o");
            text = text.Replace("Ú", "U");
            text = text.Replace("ú", "u");
            text = text.Replace("Ứ", "U");
            text = text.Replace("ứ", "u");
            text = text.Replace("Ý", "Y");
            text = text.Replace("ý", "y");

            //'Dấu Hỏi
            text = text.Replace("Ả", "A");
            text = text.Replace("ả", "a");
            text = text.Replace("Ẳ", "A");
            text = text.Replace("ẳ", "a");
            text = text.Replace("Ẩ", "A");
            text = text.Replace("ẩ", "a");
            text = text.Replace("Ẻ", "E");
            text = text.Replace("ẻ", "e");
            text = text.Replace("Ể", "E");
            text = text.Replace("ể", "e");
            text = text.Replace("Ỉ", "I");
            text = text.Replace("ỉ", "i");
            text = text.Replace("Ỏ", "O");
            text = text.Replace("ỏ", "o");
            text = text.Replace("Ổ", "O");
            text = text.Replace("ổ", "o");
            text = text.Replace("Ở", "O");
            text = text.Replace("ở", "o");
            text = text.Replace("Ủ", "U");
            text = text.Replace("ủ", "u");
            text = text.Replace("Ử", "U");
            text = text.Replace("ử", "u");
            text = text.Replace("Ỷ", "Y");
            text = text.Replace("ỷ", "y");

            //'Dấu Ngã   
            text = text.Replace("Ã", "A");
            text = text.Replace("ã", "a");
            text = text.Replace("Ẵ", "A");
            text = text.Replace("ẵ", "a");
            text = text.Replace("Ẫ", "A");
            text = text.Replace("ẫ", "a");
            text = text.Replace("Ẽ", "E");
            text = text.Replace("ẽ", "e");
            text = text.Replace("Ễ", "E");
            text = text.Replace("ễ", "e");
            text = text.Replace("Ĩ", "I");
            text = text.Replace("ĩ", "i");
            text = text.Replace("Õ", "O");
            text = text.Replace("õ", "o");
            text = text.Replace("Ỗ", "O");
            text = text.Replace("ỗ", "o");
            text = text.Replace("Ỡ", "O");
            text = text.Replace("ỡ", "o");
            text = text.Replace("Ũ", "U");
            text = text.Replace("ũ", "u");
            text = text.Replace("Ữ", "U");
            text = text.Replace("ữ", "u");
            text = text.Replace("Ỹ", "Y");
            text = text.Replace("ỹ", "y");

            //'Dẫu Nặng
            text = text.Replace("Ạ", "A");
            text = text.Replace("ạ", "a");
            text = text.Replace("Ặ", "A");
            text = text.Replace("ặ", "a");
            text = text.Replace("Ậ", "A");
            text = text.Replace("ậ", "a");
            text = text.Replace("Ẹ", "E");
            text = text.Replace("ẹ", "e");
            text = text.Replace("Ệ", "E");
            text = text.Replace("ệ", "e");
            text = text.Replace("Ị", "I");
            text = text.Replace("ị", "i");
            text = text.Replace("Ọ", "O");
            text = text.Replace("ọ", "o");
            text = text.Replace("Ộ", "O");
            text = text.Replace("ộ", "o");
            text = text.Replace("Ợ", "O");
            text = text.Replace("ợ", "o");
            text = text.Replace("Ụ", "U");
            text = text.Replace("ụ", "u");
            text = text.Replace("Ự", "U");
            text = text.Replace("ự", "u");
            text = text.Replace("Ỵ", "Y");
            text = text.Replace("ỵ", "y");
            text = text.Replace("Đ", "D");
            text = text.Replace("đ", "d");
            text = text.Replace("/", "_");
            text = text.Replace("\\", "_");
            text = text.Replace(" ", "");
            return text;
        }

        public static string ConvertFullDate(DateTime date)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ngày ").Append(date.Day).Append(" tháng ").Append(date.Month).Append(" năm ").Append(date.Year);
            return sb.ToString();
        }

        public static string GetTemplatePath(ReportDefinition ReportDef)
        {
            //Code lởm comment lại
            return SystemParamsInFile.TEMPLATE_FOLDER + ReportDef.TemplateDirectory + "/" + ReportDef.TemplateName;

            //char separator = System.IO.Path.DirectorySeparatorChar; // để sử dụng sau khi update lại db của server với directory 
            //return System.AppDomain.CurrentDomain.BaseDirectory + ReportDef.TemplateName;
        }

        public static string ConvertAppliedLevelToFullReportName(int appliedLevel)
        {
            string result = "";
            if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                result = "TIỂU HỌC";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                result = "TRUNG HỌC CƠ SỞ";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                result = "TRUNG HỌC PHỔ THÔNG";
            }
            return result;
        }

        /// <summary>
        /// QuangLM
        /// Lay ty le phan tram cua tung phan tu trong danh sach dau vao
        /// Phan tu cuoi cung neu tong bang tong truyen vao se lay 100 tru cho tong nhung cai truong do
        /// Neu du lieu bi sai tuc la tong cac phan tu trong danh sach lon hon tong truyen vao
        /// thi se lay dung ty le cua no de nguoi dung tu xu ly
        /// </summary>
        /// <param name="listNo"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<double?> GetPerCentInList(List<int?> listNo, int? total)
        {
            if (listNo == null)
            {
                return null;
            }
            if (!total.HasValue || total.Value == 0)
            {
                return listNo.Select(o => new Nullable<double>(0.0)).ToList();
            }
            List<double?> listPercent = new List<double?>();
            int totalNo = 0;
            double totalPercent = 0.0;
            int pos = -1;
            bool isFull = false;
            for (int i = 0; i < listNo.Count; i++)
            {
                int? no = listNo[i];
                if (no.HasValue)
                {
                    totalNo += no.Value;
                }
                double? percent = no.HasValue && total.HasValue ? Math.Round(100.0 * no.Value / total.Value, 1, MidpointRounding.AwayFromZero) : new Nullable<double>();

                if (!isFull && total.Value == totalNo)
                {
                    listPercent.Add(Math.Round(100.0 - totalPercent, 1, MidpointRounding.AwayFromZero));
                    isFull = true;
                    pos = i;
                }
                else
                {
                    listPercent.Add(percent);
                    if (pos >= 0 && percent.HasValue && percent.Value > 0)
                    {
                        listPercent[pos] = 100 - listPercent[pos];
                        pos = -1;
                    }
                }
                if (percent.HasValue)
                {
                    totalPercent += percent.Value;
                }
            }
            return listPercent;
        }
    }
}
