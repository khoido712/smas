﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Common
{
    public class TestHieuNangBO
    {
        public long ThreadID { get; set; }
        public string Module { get; set; }
        public string SubModule { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
