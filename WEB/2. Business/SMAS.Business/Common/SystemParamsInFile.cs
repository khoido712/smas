﻿// -----------------------------------------------------------------------
// <copyright file="SystemParamsInFile.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SMAS.Business.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Định nghĩa các tham số hệ thống
    /// </summary>
    public class SystemParamsInFile
    {
        /// <summary>
        ///Toàn quốc COUNTRY
        /// </summary>
        public const int COUNTRY = -1;
        public const string COUNTRY_NAME = "Toàn Quốc";

        /// <summary>
        /// Trạng thái Active
        /// </summary>
        public const int COMMON_STATUS_ISACTIVE = 1;

        /// <summary>
        /// Trạng thái không Active
        /// </summary>
        public const int COMMON_STATUS_NOTISACTIVE = 0;

        /// <summary>
        /// Đã tổng kết
        /// </summary>
        /// <author>trangdd</author>
        /// <date>11/01/2012</date>
        public const int STATUS_SUR_CLASS_COMPLETE = 3;
        /// <summary>
        /// Đang tổng kết
        /// </summary>
        /// <author>trangdd</author>
        /// <date>11/01/2012</date>
        public const int STATUS_SUR_CLASS_COMPLETING = 2;
        /// <summary>
        /// Chưa tổng kết
        /// </summary>
        /// <author>trangdd</author>
        /// <date>11/01/2012</date>
        public const int STATUS_SUR_CLASS_NOTCOMPLETE = 1;
        /// <summary>
        /// Tổng kết
        /// </summary>
        /// <author>trangdd</author>
        /// <date>11/01/2012</date>
        public const int SUMMED_UP_RECORD_CLASS_TYPE_1 = 1;
        /// <summary>
        /// Xếp loại
        /// </summary>
        /// <author>trangdd</author>
        /// <date>11/01/2012</date>
        public const int SUMMED_UP_RECORD_CLASS_TYPE_2 = 2;
        public const int DISABLED_REFRACTIVE_MYOPIC = 1;
        public const int DISABLED_REFRACTIVE_FARSIGHTEDNESS = 2;
        public const int DISABLED_REFRACTIVE_ASTIGMATISM = 3;
        public const int DISABLED_REFRACTIVE_OTHER = 4;
        public const int DISABLED_REFRACTIVE_NORMAL = 5;

        public const int NUTRITION_TYPE_OVERWEIGHT = 2;
        public const int NUTRITION_TYPE_NORMALWEIGHT = 3;
        public const int NUTRITION_TYPE_MALNUTRITION = 1;

        public const string EYE_ISTREATMENT_1 = "Có đeo kính";
        public const string EYE_ISTREATMENT_0 = "Không đeo kính";

        public const int PHYSICAL_CLASSIFICATION_EXCELLENT = 1;
        public const int PHYSICAL_CLASSIFICATION_GOOD = 2;
        public const int PHYSICAL_CLASSIFICATION_NORMAL = 3;
        public const int PHYSICAL_CLASSIFICATION_WEAK = 4;
        public const int PHYSICAL_CLASSIFICATION_POOR = 5;

        public const string PERIOD_EXAMINATION_1 = "Đợt 1";
        public const string PERIOD_EXAMINATION_2 = "Đợt 2";

        public const string EvaluationHealth_Type_A = "Loại A";
        public const string EvaluationHealth_Type_B = "Loại B";
        public const string EvaluationHealth_Type_C = "Loại C";

        public const int ITEM_EXAMINATION_PHYSICAL = 1;
        public const int ITEM_EXAMINATION_EYE = 2;
        public const int ITEM_EXAMINATION_TEETH = 3;
        public const int ITEM_EXAMINATION_ENT = 4;
        public const int ITEM_EXAMINATION_SPINE = 5;
        public const int ITEM_EXAMINATION_OVERRALL = 6;

        public const int EVALUATION_HEALTH_0 = 0;
        public const int EVALUATION_HEALTH_A = 1;
        public const int EVALUATION_HEALTH_B = 2;
        public const int EVALUATION_HEALTH_C = 3;

        public const int HEALTH_STATE_DISEASE = 1;
        public const int HEALTH_STATE_NORMAL = 2;

        /// <summary>
        /// Đợt 1
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/12/2012</date>
        public const int HEALTH_PERIOD_ONE = 1;
        /// <summary>
        /// Đợt 2
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/12/2012</date>
        public const int HEALTH_PERIOD_TWO = 2;
        /// <summary>
        /// Tất cả
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/12/2012</date>
        public const int HEALTH_STATUS_ALL = 0;
        /// <summary>
        /// Đã khám
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/12/2012</date>
        public const int HEALTH_STATUS_DK = 1;
        /// <summary>
        /// Chưa khám
        /// </summary>
        /// <author>trangdd</author>
        /// <date>12/12/2012</date>
        public const int HEALTH_STATUS_CK = 2;
        public const string SEMESTER_I = "Học kỳ I";
        public const string SEMESTER_II = "Học kỳ II";

        public const string HS_THCS_DSPHANCONGGIAMTHI_KHOI6_MONCONGNGHE = "HS_THCS_DSPhanCongGiamThi_Khoi6_MonCongNghe";
        public const string TEMPLATE_THONGKE_THELUC = "Template_Thongke_Theluc";
        public const string TEMPLATE_THONGKE_MAT = "Template_Thongke_Mat";
        public const string TEMPLATE_THONGKE_RANGHAMMAT = "Template_Thongke_Ranghammat";
        public const string TEMPLATE_THONGKE_COTSONG = "Template_Thongke_Cotsong";
        public const string TEMPLATE_THONGKE_TONGQUAT = "TemplateThongKeTongQuat";
        public const string HS_THPT_TKTINHHINHNHAPDIEM_KHOI10_HKI = "HS_THPT_TKTinhHinhNhapDiem_Khoi10_HKI";
        public const string HS_THCS_DSHOCSINHTHIEUDIEM_KHOI6_HKI = "HS_THCS_DSHocSinhThieuDiem_Khoi6_HKI";
        public const string TEMPLATE_BANGXEPLOAIHANHKIEM_THEOKY = "BangXepLoaiHanhKiem_TheoKy";
        public const string TEMPLATE_BANGXEPLOAIHANHKIEM_THEOKY_VP = "BangXepLoaiHanhKiem_TheoKy_ViPham";
        public const string TEMPLATE_DANHSACHLOPHOC_MN = "DanhSachHocSinh_MN";
        public const string TEMPLATE_TK_DIEMKTDK_TH = "SoGD_TH_ThongKeDiemKTDinhKy";
        public const string TEMPLATE_TK_DIEMKTDK_THCS = "SoGD_THCS_ThongKeDiemKTDinhKy";
        public const string TEMPLATE_TK_DIEMKTHK_TH = "SoGD_TH_TKDonViBCDiemKTHocKy";
        public const string TEMPLATE_TK_DIEMKTHK_THCS = "SoGD_THCS_ TKDonViBCDiemKTHocKy";
        public const string TEMPLATE_TK_DIEMTBM_THCS = "SoGD_THCS_TKDonViBCDiemTBM";
        public const string TEMPLATE_TK_DIEMTHEODOT = "TongKetDiemTheoDot";
        public const string MN_THONGKESISO = "TRE_MN_TKSiSo";
        public const string TKHSNghiHoc_PhongSo = "TKHSNghiHoc_PhongSo";
         public const string ChiSoDanhGiaTre = "ChiSoDanhGiaTre";

        public const string MN_DANHSACHTRE = "TRE_MN_DSTre";
        public const string MN_TRECHINHSACH = "DS_TKTreChinhSach";
        public const string TRE_MN_TKTreChinhSach = "TRE_MN_TKTreChinhSach";
        public const string BC_Yte_TKCandosuckhoe = "BC_Yte_TKCandosuckhoe";
        public const string BC_Yte_KetQuaCanDoSK = "BC_Yte_KetQuaCanDoSK";
        public const string DANHGIASUPHATTRIENTRE_MAM1_THECHAT = "Danhgiasuphattrien_Mam1_TheChat";
        public const string SoTheoDoiSKNam_TheLuc_12_Dot1 = "SoTheoDoiSKNam_TheLuc_12_Dot1";
        public const string SoTheoDoiSKNam_Mat_12_Dot1 = "SoTheoDoiSKNam_Mat_12_Dot1";
        public const string SoTheoDoiSKNam_TaiMuiHong_12_Dot1 = "SoTheoDoiSKNam_TaiMuiHong_12_Dot1";
        public const string SoTheoDoiSKNam_CotSong_12_Dot1 = "SoTheoDoiSKNam_CotSong_12_Dot1";
        public const string SoTheoDoiSKNam_RangHamMat_12_Dot1 = "SoTheoDoiSKNam_RangHamMat_12_Dot1";
        public const string SoTheoDoiSKNam_TongQuat_12_Dot1 = "SoTheoDoiSKNam_TongQuat_12_Dot1";
        public const string SoTheoDoiSKNam_12_Dot1 = "SoTheoDoiSKNam_12_Dot1";
        public const string THI_THPT_DSThisinh_LHS = "THI_THPT_DSThisinh_LHS";
        public const string GiayTotNghiepTamThoi = "GiayTotNghiepTamThoi";

        public const string HS_THPT_SoDangBo = "HS_THPT_SoDangBo_Khoi11";     
        public const string PDF_HS_THPT_SoDangBo = "PDF_HS_THPT_SoDangBo_Khoi11"; 

        public const string SummaryResultEvaluation = "TongHopKQDanhGiaSuPTTre";
        public const string ChildrenContact = "TRE_MN_Phieulienlac_Mam1";
        public const string ReportEvaluation = "BC_MN_PhieuDanhGiaPT";
        public const string MeetingInvitationReport = "GiayMoiHopPHHS_Khoi10";
        //MÃ DÂN TỘC KINH
        public const string ETHNIC_CODE_KINH = "01";
        public const int ETHNIC_ID_KINH = 1;
        //MÃ DÂN TỘC NGƯỜI NƯỚC NGOÀI
        public const string ETHNIC_CODE_NN = "55";
        public const int ETHNIC_ID_NN = 79;
        /// <summary>
        /// The SG d_ THP t_ THONGKEDIEMKIEMTRADINHKY
        /// </summary>
        /// <author>hath</author>
        /// <date>12/12/2012</date>
        public const string SGD_THPT_THONGKEDIEMKIEMTRADINHKY = "SGD_THPT_ThongKeDiemKiemTraDinhKy";
        /// <summary>
        /// The SG d_ THP t_ THONGKEDIEMKIEMTRAHOCKY
        /// </summary>
        /// <author>hath</author>
        /// <date>12/12/2012</date>
        public const string SGD_THPT_THONGKEDIEMKIEMTRAHOCKY = "SGD_THPT_ThongKeDiemKiemTraHocKy";

        /// <summary>
        /// The QL t_ PHONGTH i_ THICHATLUONGHOCK y2
        /// </summary>
        /// <author>hath</author>
        /// <date>12/12/2012</date>
        public const string QLT_PHONGTHI_THICHATLUONGHOCKY2 = "QLT_Phongthi_Thichatluonghocky2";

        /// <summary>
        /// The QL t_ PHONGTH i_ CHUAXE p_ THICHATLUONGHOCK y2
        /// </summary>
        /// <author>hath</author>
        /// <date>12/21/2012</date>
        public const string QLT_PHONGTHI_CHUAXEP_THICHATLUONGHOCKY2 = "QLT_Phongthi_Chuaxep_Thichatluonghocky2";

        /// <summary>
        /// The H s_ THP t_ DKDTTOTNGHIEP
        /// </summary>
        /// <author>hath</author>
        /// <date>12/21/2012</date>
        public const string HS_THPT_DKDTTOTNGHIEP = "HS_THPT_DKDTTotNghiep";

        /// <summary>
        /// The H s_ THP t_ HS_THPT_CapBangTN
        /// </summary>
        /// <author>hieund</author>
        /// <date>12/21/2012</date>
        public const string HS_THPT_CAPBANGTN = "HS_THPT_CapBangTN";

        /// <summary>
        /// The G v_ THC s_ DSGIAMTHI
        /// </summary>
        /// <author>hath</author>
        /// <date>12/13/2012</date>
        public const string GV_THCS_DSGIAMTHI = "GV_THCS_DSGiamThi";

        /// <summary>
        ///Bình thường
        /// </summary> 
        public const int MENU_TYPE_NORMAL = 1;
        /// <summary>
        ///Bình thường
        /// </summary> 
        public const int ATTITUDE_IN_STRANGE_SCENE_NORMAL = 1;
        /// <summary>
        ///Vui vẻ
        /// </summary> 
        public const int ATTITUDE_IN_STRANGE_SCENE_HAPPY = 2;
        /// <summary>
        ///Sợ hãi
        /// </summary> 
        public const int ATTITUDE_IN_STRANGE_SCENE_SCALE = 3;
        /// <summary>
        ///Bình thường
        /// </summary> 
        public const int REACTION_TRAINING_HABIT_NORMAL = 1;
        /// <summary>
        ///Khó chịu
        /// </summary> 
        public const int REACTION_TRAINING_HABIT_DISCOMFORT = 2;
        /// <summary>
        ///Tích cực
        /// </summary> 
        public const int REACTION_TRAINING_HABIT_POSITIVE = 3;
        /// <summary>
        ///Món ăn chính
        /// </summary> 
        public const int FOOD_HABIT_OF_CHILDREN_MAIN = 1;
        /// <summary>
        ///Món ăn ưa thích
        /// </summary> 
        public const int FOOD_HABIT_OF_CHILDREN_LIKE = 2;
        /// <summary>
        ///Món ăn ghét nhất
        /// </summary> 
        public const int FOOD_HABIT_OF_CHILDREN_HATE = 3;
        /// <summary>
        ///Kiểu chuỗi 
        /// </summary> 
        public const int EVALUATION_DEVELOPMENT_TYPE_STRING = 1;
        /// <summary>
        ///Kiểu số 
        /// </summary> 
        public const int EVALUATION_DEVELOPMENT_TYPE_NUMBER = 2;
        /// <summary>
        ///Kiểu ngày tháng
        /// </summary> 
        public const int EVALUATION_DEVELOPMENT_TYPE_DATE = 3;
        /// <summary>
        ///Nhóm máu A 
        /// </summary> 
        public const int BLOOD_TYPE_A = 1;

        /// <summary>
        ///Nhóm máu B 
        /// </summary> 
        public const int BLOOD_TYPE_B = 2;

        /// <summary>
        ///Nhóm máu AB 
        /// </summary> 
        public const int BLOOD_TYPE_AB = 3;

        /// <summary>
        ///Nhóm máu O 
        /// </summary> 
        public const int BLOOD_TYPE_O = 4;

        /// <summary>
        ///Nhóm máu không xác định 
        /// </summary> 
        public const int BLOOD_TYPE_OTHER = 0;

        /// <summary>
        ///Đ: Thực hiện đầy đủ - Cấp 1 
        /// </summary> 
        public const int CONDUCT_TYPE_D = 1;

        /// <summary>
        ///CĐ: Thực hiện chưa đủ - Cấp 1 
        /// </summary> 
        public const int CONDUCT_TYPE_CD = 2;

        /// <summary>
        ///Tốt – Cấp 2 
        /// </summary> 
        public const int CONDUCT_TYPE_GOOD_SECONDARY = 3;

        /// <summary>
        ///Khá – Cấp 2 
        /// </summary> 
        public const int CONDUCT_TYPE_FAIR_SECONDARY = 4;

        /// <summary>
        ///Trung bình – Cấp 2 
        /// </summary> 
        public const int CONDUCT_TYPE_NORMAL_SECONDARY = 5;

        /// <summary>
        ///Yếu - Cấp 2 
        /// </summary> 
        public const int CONDUCT_TYPE_WEAK_SECONDARY = 6;

        /// <summary>
        ///Tốt – Cấp 3 
        /// </summary> 
        public const int CONDUCT_TYPE_GOOD_TERTIARY = 7;

        /// <summary>
        ///Khá – Cấp 3 
        /// </summary> 
        public const int CONDUCT_TYPE_FAIR_TERTIARY = 8;

        /// <summary>
        ///Trung bình – Cấp 3 
        /// </summary> 
        public const int CONDUCT_TYPE_NORMAL_TERTIARY = 9;

        /// <summary>
        ///Yếu - Cấp 3 
        /// </summary> 
        public const int CONDUCT_TYPE_WEAK_TERTIARY = 10;

        /// <summary>
        ///Giỏi 
        /// </summary> 
        public const int CAPACITY_TYPE_EXCELLENT = 1;

        /// <summary>
        ///Khá 
        /// </summary> 
        public const int CAPACITY_TYPE_GOOD = 2;

        /// <summary>
        ///Trung bình 
        /// </summary> 
        public const int CAPACITY_TYPE_NORMAL = 3;

        /// <summary>
        ///Yếu 
        /// </summary> 
        public const int CAPACITY_TYPE_WEAK = 4;

        /// <summary>
        ///Kém 
        /// </summary> 
        public const int CAPACITY_TYPE_POOR = 5;

        /// <summary>
        ///A+ : Môn nhận xét cấp 1 
        /// </summary> 
        public const int CAPACITY_TYPE_APLUS = 6;

        /// <summary>
        ///A: Môn nhận xét cấp 1 
        /// </summary> 
        public const int CAPACITY_TYPE_A = 7;

        /// <summary>
        ///B: Môn nhận xét cấp 1 
        /// </summary> 
        public const int CAPACITY_TYPE_B = 8;

        /// <summary>
        ///Đ: Môn nhận xét cấp 2, 3 
        /// </summary> 
        public const int CAPACITY_TYPE_D = 9;

        /// <summary>
        ///CĐ: Môn nhận xét cấp 2, 3 
        /// </summary> 
        public const int CAPACITY_TYPE_CD = 10;

        /// <summary>
        ///Lên lớp 
        /// </summary> 
        public const int STUDYING_JUDGEMENT_UPCLASS = 1;

        /// <summary>
        ///Thi lại 
        /// </summary> 
        public const int STUDYING_JUDGEMENT_RETEST = 2;

        /// <summary>
        ///Rèn luyện lại 
        /// </summary> 
        public const int STUDYING_JUDGEMENT_BACKTRAINING = 3;

        /// <summary>
        ///Ở lại lớp 
        /// </summary> 
        public const int STUDYING_JUDGEMENT_STAYCLASS = 4;

        /// <summary>
        ///Cấp 1: Hoàn thành chương trình tiểu học 
        ///Cấp 2: Được xét tốt nghiệp
        ///Cấp 3: Được dự thi tốt nghiệp
        /// </summary> 
        public const int STUDYING_JUDGEMENT_GRADUATION = 5;

        /// <summary>
        ///Miễn giảm học kỳ 1 
        /// </summary> 
        public const int EXEMPT_SCOPE_FISRT_SEMESTER = 1;

        /// <summary>
        ///Miễn giảm học kỳ 2 
        /// </summary> 
        public const int EXEMPT_SCOPE_SECOND_SEMESTER = 2;

        /// <summary>
        ///Miễn giảm cả năm 
        /// </summary> 
        public const int EXEMPT_SCOPE_FULL_YEAR = 3;

        /// <summary>
        ///Miễn giảm kiểu khác / Không xác định 
        /// </summary> 
        public const int EXEMPT_SCOPE_OTHER = 0;

        /// <summary>
        ///Miễn phần thực hành 
        /// </summary> 
        public const int EXEMPT_TYPE_PRACTICE = 1;

        /// <summary>
        ///Miễn toàn phần 
        /// </summary> 
        public const int EXEMPT_TYPE_ALL = 2;

        /// <summary>
        ///Hình thức khác 
        /// </summary> 
        public const int EXEMPT_TYPE_OTHER = 0;

        /// <summary>
        ///Giới tính nam 
        /// </summary> 
        public const int GENRE_MALE = 1;

        /// <summary>
        ///Giới tính nữ 
        /// </summary> 
        public const int GENRE_FEMALE = 0;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int GENRE_UNIDENTIFIED = 2;

        /// <summary>
        ///Giới tính tat ca va lua chon 
        /// </summary> 
        public const int GENRE_ALL = -1;

        /// <summary>
        ///Giáo viên 
        /// </summary> 
        public const int EMPLOYEE_TYPE_SCHOOL_TEACHER = 1;

        /// <summary>
        ///Cán bộ quản lý cấp bộ / sở / phòng ban 
        /// </summary> 
        public const int EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF = 3;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int EMPLOYEE_TYPE_OTHER = 0;

        /// <summary>
        ///Độc thân 
        /// </summary> 
        public const int MARIAGE_STATUS_SINGLE = 1;

        /// <summary>
        ///Đã kết hôn 
        /// </summary> 
        public const int MARIAGE_STATUS_MARRIED = 2;

        /// <summary>
        ///Đã li hôn 
        /// </summary> 
        public const int MARIAGE_STATUS_DIVORCED = 3;

        /// <summary>
        ///Đang góa (chồng / vợ) 
        /// </summary> 
        public const int MARIAGE_STATUS_WIDOWDED = 4;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int MARIAGE_STATUS_UNIDENTIFIED = 0;

        /// <summary>
        ///Trường chỉ có cấp 1 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_PRIMARY = 1;

        /// <summary>
        ///Trường chỉ có cấp 2 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_SECONDARY = 2;

        /// <summary>
        ///Trường chỉ có cấp 3 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_TERTIARY = 4;

        /// <summary>
        ///Trường có cấp 1,2 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY = 3;

        /// <summary>
        ///Trường có cấp 2,3 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY = 6;

        /// <summary>
        ///Trường có cả 3 cấp 1,2,3 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_ALL = 7;

        /// <summary>
        ///Cấp nhà trẻ 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_CRECHE = 8;

        /// <summary>
        ///Cấp mẫu giáo 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_KINDERGARTEN = 16;

        /// <summary>
        ///Trường mầm non (có cả cấp nhà trẻ và mẫu giáo) 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_PRESCHOOL = 24;

        /// <summary>
        /// "Mầm non + Phổ thông", gồm những cấp học: mẫu giáo, nhà trẻ, 1, 2, 3.
        /// </summary>
        public const int SCHOOL_EDUCATION_GRADE_BCIS = 31;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int SCHOOL_EDUCATION_GRADE_UNIDENTIFIED = 0;

        public const int EDUCATION_LEVEL_FIRST = 1;
        public const int EDUCATION_LEVEL_SECOND = 2;
        public const int EDUCATION_LEVEL_THIRD = 3;
        public const int EDUCATION_LEVEL_FOURTH = 4;
        public const int EDUCATION_LEVEL_FIFTH = 5;
        public const int EDUCATION_LEVEL_SIXTH = 6;
        public const int EDUCATION_LEVEL_SEVENTH = 7;
        public const int EDUCATION_LEVEL_EIGHTH = 8;
        public const int EDUCATION_LEVEL_NINTH = 9;
        public const int EDUCATION_LEVEL_TENTH = 10;
        public const int EDUCATION_LEVEL_ELEVENTH = 11;
        public const int EDUCATION_LEVEL_TWELFTH = 12;

        /// <summary>
        /// Trạng thái gửi xét duyệt tốt nghiệp
        /// 1: Chờ xét duyệt
        /// 2: Đã xét duyệt
        /// 3: Hủy xét duyệt
        /// </summary>
        public const int GRADUCATION_STATUS_WAITING = 1;
        public const int GRADUCATION_STATUS_APPROVED = 2;
        public const int GRADUCATION_STATUS_CANCEL = 3;

        /// <summary>
        ///Hệ số nhỏ nhất 
        /// </summary> 
        public const int SUBJECT_COEFFICIENT_MIN = 0;

        /// <summary>
        ///Hệ số lớn nhất 
        /// </summary> 
        public const int SUBJECT_COEFFICIENT_MAX = 2;

        /// <summary>
        ///Hệ số cho môn chuyên 
        /// </summary> 
        public const int SUBJECT_FOR_SPECIAL = 3;

        /// <summary>
        ///Tính điểm thông thường 
        /// </summary> 
        public const int SUBJECT_TYPE_MANDATORY = 0;

        /// <summary>
        ///Tự chọn 
        /// </summary> 
        public const int SUBJECT_TYPE_VOLUNTARY = 1;

        /// <summary>
        ///Tự chọn tinh điểm 
        /// </summary> 
        public const int SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK = 3;

        /// <summary>
        /// Tự chọn cộng ưu tiên
        /// </summary>
        public const int SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE = 2;
      
        /// <summary>
        ///Môn nghề 
        /// </summary> 
        public const int SUBJECT_TYPE_APPRENTICESHIP = 4;

        /// <summary>
        ///Môn Ngoại ngữ 2 
        /// </summary> 
        public const int SUBJECT_TYPE_SECOND_FOREIGN_LANGUAGE = 5;

        //kHAI BÁO LOẠI MÔN TRONG CHỨC NĂNG KHAI BÁO MÔN HỌC CHO LỚP.
        public const int APPLIED_TYPE_MANDATORY = 0;
        public const int APPLIED_TYPE_VOLUNTARY = 1;
        public const int APPLIED_TYPE_VOLUNTARY_PRIORITY = 2;
        public const int APPLIED_TYPE_VOLUNTARY_PLUS_MARK = 3;
        public const int APPLIED_TYPE_APPRENTICESHIP = 4;
        /// <summary>
        ///Thứ 2 
        /// </summary> 
        public const int DAY_OF_WEEK_MONDAY = 2;

        /// <summary>
        ///Thứ 3 
        /// </summary> 
        public const int DAY_OF_WEEK_TUESDAY = 3;

        /// <summary>
        ///Thứ 4 
        /// </summary> 
        public const int DAY_OF_WEEK_WEDNESDAY = 4;

        /// <summary>
        ///Thứ 5 
        /// </summary> 
        public const int DAY_OF_WEEK_THURSDAY = 5;

        /// <summary>
        ///Thứ 6 
        /// </summary> 
        public const int DAY_OF_WEEK_FRIDAY = 6;

        /// <summary>
        ///Thứ 7 
        /// </summary> 
        public const int DAY_OF_WEEK_SATURDAY = 7;

        /// <summary>
        ///Chủ nhật 
        /// </summary> 
        public const int DAY_OF_WEEK_SUNDAY = 8;

        /// <summary>
        ///ALL
        /// </summary> 
        public const int PUPIL_STATUS_ALL = 0;

        /// <summary>
        ///Đang học 
        /// </summary> 
        public const int PUPIL_STATUS_STUDYING = 1;

        /// <summary>
        ///Đã tốt nghiệp 
        /// </summary> 
        public const int PUPIL_STATUS_GRADUATED = 2;

        /// <summary>
        ///Đã chuyển trường 
        /// </summary> 
        public const int PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL = 3;

        /// <summary>
        ///Đã thôi học 
        /// </summary> 
        public const int PUPIL_STATUS_LEAVED_OFF = 4;

        /// <summary>
        ///Đã chuyển lớp 
        /// </summary> 
        public const int PUPIL_STATUS_MOVED_TO_OTHER_CLASS = 5;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int PUPIL_STATUS_UNIDENTIFIED = 0;

        /// <summary>
        ///Hệ học 3 năm 
        /// </summary> 
        public const int FOREIGN_LANGUAGE_TRAINING_3_YEARS = 1;

        /// <summary>
        ///Hệ học 7 năm 
        /// </summary> 
        public const int FOREIGN_LANGUAGE_TRAINING_7_YEARS = 2;

        /// <summary>
        ///Hệ học 10 năm 
        /// </summary> 
        public const int FOREIGN_LANGUAGE_TRAINING_10_YEARS = 3;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED = 0;

        /// <summary>
        ///Trúng tuyển 
        /// </summary> 
        public const int ENROLMENT_TYPE_PASSED_EXAMINATION = 0;

        /// <summary>
        ///Chuyển đến từ trường khác 
        /// </summary> 
        public const int ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL = 1;

        /// <summary>
        ///Xét tuyển 
        /// </summary> 
        public const int ENROLMENT_TYPE_SELECTED = 2;

        /// <summary>
        ///Hình thức khác 
        /// </summary> 
        public const int ENROLMENT_TYPE_OTHER = 3;

        /// <summary>
        ///Cấp bộ 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_MINISTRY = 1;

        /// <summary>
        ///Các phòng ban quản lý cấp bộ 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_MINISTRY_DEPARTMENT = 2;

        /// <summary>
        ///Cấp Sở GD 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE = 3;

        /// <summary>
        ///Các phòng ban quản lý cấp Sở GD 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT = 4;

        /// <summary>
        ///Cấp Phòng GD 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE = 5;

        /// <summary>
        ///Cấp phòng, ban thuộc Phòng Giáo dục 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT = 6;

        /// <summary>
        ///Cấp trường 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_SCHOOL = 7;

        /// <summary>
        ///Cấp lớp 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_CLASS = 8;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int EDUCATION_HIERACHY_LEVEL_UNIDENTIFIED = 0;

        /// <summary>
        ///Học kỳ 1 
        /// </summary> 
        public const int SEMESTER_OF_YEAR_FIRST = 1;

        /// <summary>
        ///Học kỳ 2 
        /// </summary> 
        public const int SEMESTER_OF_YEAR_SECOND = 2;

        /// <summary>
        /// Trong hè
        /// </summary>
        public const int SEMESTER_IN_SUMMER = 6;

        /// <summary>
        /// Cả năm
        /// </summary> 
        public const int SEMESTER_OF_YEAR_ALL = 3;

        /// <summary>
        /// Rèn luyện lại
        /// </summary>
        public const int SEMESTER_OF_YEAR_BACKTRAINING = 4;

        /// <summary>
        /// Thi lại
        /// </summary>
        public const int SEMESTER_OF_YEAR_RETEST = 5;

        /// <summary>
        ///Chua xet duyet
        /// </summary> 
        public const int GRADUATION_GRADE_NOT_APPROVED = 0;

        /// <summary>
        ///Giỏi / Xuất sắc 
        /// </summary> 
        public const int GRADUATION_GRADE_EXCELLENT = 1;

        /// <summary>
        ///Khá 
        /// </summary> 
        public const int GRADUATION_GRADE_GOOD = 2;

        /// <summary>
        /// Trung bình khá 
        /// </summary> 
        public const int GRADUATION_GRADE_ABOVE_FAIR = 3;

        /// <summary>
        ///Trung bình 
        /// </summary> 
        public const int GRADUATION_GRADE_FAIR = 4;

        /// <summary>
        /// Da nhan bang tot nghiep
        /// </summary>
        public const int GRADUAION_RECEIVED = 1;

        /// <summary>
        /// Chua nhan bang tot nghiep
        /// </summary>
        public const int GRADUAION_NOT_RECEIVED = 0;

        /// <summary>
        ///Tiểu học 
        /// </summary> 
        public const int EDUCATION_GRADE_PRIMARY = 1;

        /// <summary>
        ///Trung học cơ sở 
        /// </summary> 
        public const int EDUCATION_GRADE_SECONDARY = 2;

        /// <summary>
        ///Phổ thông cơ sở
        /// </summary> 
        public const int EDUCATION_GRADE_TERTIARY = 3;

        /// <summary>
        ///Tháng 1 
        /// </summary> 
        public const int MONTH_OF_YEAR_JANUARY = 1;

        /// <summary>
        ///Tháng 2 
        /// </summary> 
        public const int MONTH_OF_YEAR_FEBRUARY = 2;

        /// <summary>
        ///Tháng 3 
        /// </summary> 
        public const int MONTH_OF_YEAR_MARCH = 3;

        /// <summary>
        ///Tháng 4 
        /// </summary> 
        public const int MONTH_OF_YEAR_APRIL = 4;

        /// <summary>
        ///Tháng 5 
        /// </summary> 
        public const int MONTH_OF_YEAR_MAY = 5;

        /// <summary>
        ///Tháng 6 
        /// </summary> 
        public const int MONTH_OF_YEAR_JUNE = 6;

        /// <summary>
        ///Tháng 7 
        /// </summary> 
        public const int MONTH_OF_YEAR_JULY = 7;

        /// <summary>
        ///Tháng 8 
        /// </summary> 
        public const int MONTH_OF_YEAR_AUGUST = 8;

        /// <summary>
        ///Tháng 9 
        /// </summary> 
        public const int MONTH_OF_YEAR_SEPTEMBER = 9;

        /// <summary>
        ///Tháng 10 
        /// </summary> 
        public const int MONTH_OF_YEAR_OCTOBER = 10;

        /// <summary>
        ///Tháng 11 
        /// </summary> 
        public const int MONTH_OF_YEAR_NOVEMBER = 11;

        /// <summary>
        ///Tháng 12 
        /// </summary> 
        public const int MONTH_OF_YEAR_DECEMBER = 12;

        /// <summary>
        ///Loại giỏi 
        /// </summary> 
        public const int APPRENTICESHIP_TRAINING_RESULT_EXCELLENT = 1;

        /// <summary>
        ///Loại khá 
        /// </summary> 
        public const int APPRENTICESHIP_TRAINING_RESULT_GOOD = 2;

        /// <summary>
        ///Loại trung bình 
        /// </summary> 
        public const int APPRENTICESHIP_TRAINING_RESULT_FAIR = 3;

        /// <summary>
        ///Không đạt 
        /// </summary> 
        public const int APPRENTICESHIP_TRAINING_RESULT_NOT_PASSED = 0;

        /// <summary>
        ///Quyền giáo viên chủ nhiệm 
        /// </summary> 
        public const int SUPERVISING_PERMISSION_HEAD_TEACHER = 1;

        /// <summary>
        ///Quyền giáo viên bộ môn 
        /// </summary> 
        public const int SUPERVISING_PERMISSION_SUBJECT_TEACHER = 2;

        /// <summary>
        ///Quyền giám thị 
        /// </summary> 
        public const int SUPERVISING_PERMISSION_OVERSEEING = 3;

        /// <summary>
        ///Giáo viên / nhân viên 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_EMPLOYEE = 1;

        /// <summary>
        ///Học sinh 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_PUPIL = 2;

        /// <summary>
        ///Khoa, tổ bộ môn 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_FACULTY = 3;

        /// <summary>
        ///Lớp 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_CLASS = 4;

        /// <summary>
        ///Trường 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_SCHOOL = 5;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int HONOUR_ACHIVEMENT_TYPE_UNIDENTIFIED = 0;

        /// <summary>
        ///Xếp loại A 
        /// </summary> 
        public const string TEACHING_EXPERIENCE_GRADE_A = "A";

        /// <summary>
        ///Xếp loại B 
        /// </summary> 
        public const string TEACHING_EXPERIENCE_GRADE_B = "B";

        /// <summary>
        ///Xếp loại C 
        /// </summary> 
        public const string TEACHING_EXPERIENCE_GRADE_C = "C";

        /// <summary>
        ///Chuyển tới trường / đơn vị khác trong hệ thống  
        /// </summary> 
        public const int WORK_MOVEMENT_WITHIN_SYSTEM = 1;

        /// <summary>
        ///Chuyển tới trường khác ngoài hệ thống / tổ chức khác 
        /// </summary> 
        public const int WORK_MOVEMENT_OTHER_ORGANIZATION = 2;

        /// <summary>
        ///Nghỉ hưu 
        /// </summary> 
        public const int WORK_MOVEMENT_RETIREMENT = 3;

        /// <summary>
        /// Nghỉ việc
        /// </summary>
        public const int WORK_MOVEMENT_SEVERANCEMENT = 5;

        /// <summary>
        /// Tạm nghỉ
        /// </summary>
        public const int WORK_MOVEMENT_BREATHER = 6;

        /// <summary>
        ///Thay đổi khác 
        /// </summary> 
        public const int WORK_MOVEMENT_OTHER = 4;

        /// <summary>
        ///Số tiết tối đa mỗi buổi học 
        /// </summary> 
        public const int PERIOD_PER_SECTION_DEFAULT = 5;

        /// <summary>
        ///Mới khởi tạo 
        /// </summary> 
        public const int EXAMINATION_STAGE_CREATED = 0;

        /// <summary>
        ///Đã lập DS thí sinh và đánh SBD 
        /// </summary> 
        public const int EXAMINATION_STAGE_CANDIDATE_LISTED = 1;

        /// <summary>
        ///Đã xếp phòng thi 
        /// </summary> 
        public const int EXAMINATION_STAGE_ROOM_ASSIGNED = 2;

        /// <summary>
        ///Đã phân công giám thị 
        /// </summary> 
        public const int EXAMINATION_STAGE_INVILIGATOR_ASSIGNED = 3;

        /// <summary>
        ///Đã đánh phách 
        /// </summary> 
        public const int EXAMINATION_STAGE_HEAD_ATTACHED = 4;

        /// <summary>
        ///Đã vào điểm thi 
        /// </summary> 
        public const int EXAMINATION_STAGE_MARK_COMPLETED = 5;

        /// <summary>
        ///Kết thúc kỳ thi 
        /// </summary> 
        public const int EXAMINATION_STAGE_FINISHED = 6;

        /// <summary>
        ///Quyền được xem 
        /// </summary> 
        public const int PERMISSION_LEVEL_VIEW = 1;

        /// <summary>
        ///Quyền được thêm mới 
        /// </summary> 
        public const int PERMISSION_LEVEL_CREATE = 2;

        /// <summary>
        ///Quyền được sửa 
        /// </summary> 
        public const int PERMISSION_LEVEL_EDIT = 3;

        /// <summary>
        ///Quyền được xóa 
        /// </summary> 
        public const int PERMISSION_LEVEL_DELETE = 4;

        /// <summary>
        ///Quyền chưa xác định 
        /// </summary> 
        public const int PERMISSION_LEVEL_UNIDENTIFIED = 0;

        /// <summary>
        ///Học sinh 
        /// </summary> 
        public const int SERVICE_TARGET_CODE_PUPIL = 1;

        /// <summary>
        ///Phụ huynh 
        /// </summary> 
        public const int SERVICE_TARGET_CODE_PARENT = 2;

        /// <summary>
        ///Giáo viên, nhân viên nhà trường 
        /// </summary> 
        public const int SERVICE_TARGET_CODE_EMPLOYEE = 4;

        /// <summary>
        ///Không xác định, áp dụng cho tất cả các đối tượng 
        /// </summary> 
        public const int SERVICE_TARGET_CODE_UNIDENTIFIED = 0;

        /// <summary>
        ///Chạy hàng ngày 
        /// </summary> 
        public const int SERVICE_RUNNING_FREQUENCY_DAYLY = 1;

        /// <summary>
        ///Chạy hàng tuần 
        /// </summary> 
        public const int SERVICE_RUNNING_FREQUENCY_WEEKLY = 2;

        /// <summary>
        ///Chạy hàng tháng 
        /// </summary> 
        public const int SERVICE_RUNNING_FREQUENCY_MONTHLY = 3;

        /// <summary>
        ///Chạy hàng năm 
        /// </summary> 
        public const int SERVICE_RUNNING_FREQUENCY_YEARLY = 4;

        /// <summary>
        ///Chạy định kỳ sau một khoảng thời gian nhất định 
        /// </summary> 
        public const int SERVICE_RUNNING_FREQUENCY_INTERVAL = 5;

        /// <summary>
        ///Đang làm việc  
        /// </summary> 
        public const int EMPLOYMENT_STATUS_WORKING = 1;

        /// <summary>
        ///Chuyển công tác 
        /// </summary> 
        public const int EMPLOYMENT_STATUS_MOVED = 2;

        /// <summary>
        ///Nghỉ hưu 
        /// </summary> 
        public const int EMPLOYMENT_STATUS_RETIRED = 3;

        /// <summary>
        /// Nghỉ việc
        /// </summary>
        public const int EMPLOYMENT_STATUS_SEVERANCE = 5;
        /// <summary>
        ///Tạm nghỉ
        /// </summary>
        public const int EMPLOYMENT_STATUS_BREATHER = 6;

        /// <summary>
        ///Thôi việc 
        /// </summary> 
        public const int EMPLOYMENT_STATUS_TEMPORARYLY_LEAVED_OFF = 4;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int EMPLOYMENT_STATUS_UNIDENTIFIED = 0;

        /// <summary>
        /// Đánh giá bổ sung:
        /// 0:Chưa đánh gia;1:Hoàn thành;2:Chưa hoàn thành
        /// </summary>
        public const int SUMMEDENDING_RATE_ADD_CDG = 0;
        public const int SUMMEDENDING_RATE_ADD_HT = 1;
        public const int SUMMEDENDING_RATE_ADD_CHT = 2;
        public const int TT22_SUMMEDENDING_RATE_ADD_CHT = 0;

        /// <summary>
        ///SMS 
        /// </summary> 
        public const int SERVICE_TYPE_SMS = 1;

        /// <summary>
        ///Email 
        /// </summary> 
        public const int SERVICE_TYPE_EMAIL = 2;

        /// <summary>
        ///Other 
        /// </summary> 
        public const int SERVICE_TYPE_OTHER = 0;

        /// <summary>
        ///Gửi thành công 
        /// </summary> 
        public const int SMS_SERVICE_ERROR_CODE_SUCCESS = 0;

        /// <summary>
        ///Số ĐT người nhận không hợp lệ 
        /// </summary> 
        public const int SMS_SERVICE_ERROR_CODE_INVALID_RECEIVER = 1;

        /// <summary>
        ///Không thiết lập được connection với SMS Gateway 
        /// </summary> 
        public const int SMS_SERVICE_ERROR_CODE_GATEWAY_FAILED = 2;

        /// <summary>
        ///Nội dung sai Codepage hoặc có ký tự không hợp lệ 
        /// </summary> 
        public const int SMS_SERVICE_ERROR_CODE_INVALID_CONTENT = 3;

        /// <summary>
        ///Các lỗi khác 
        /// </summary> 
        public const int SMS_SERVICE_ERROR_CODE_OTHER = 99;

        /// <summary>
        ///Gửi thành công 
        /// </summary> 
        public const int EMAIL_SERVICE_ERROR_CODE_SUCCESS = 0;

        /// <summary>
        ///Địa chỉ Email người nhận không hợp lệ 
        /// </summary> 
        public const int EMAIL_SERVICE_ERROR_CODE_INVALID_RECEIVER = 1;

        /// <summary>
        ///Thông thiết lập được kết nối tới SMTP Server 
        /// </summary> 
        public const int EMAIL_SERVICE_ERROR_CODE_SERVER_FAILED = 2;

        /// <summary>
        ///Hộp thư người nhận vượt quá dung lượng cho phép 
        /// </summary> 
        public const int EMAIL_SERVICE_ERROR_CODE_RECEIVER_OVERQUOTA = 3;

        /// <summary>
        ///Các lỗi khác 
        /// </summary> 
        public const int EMAIL_SERVICE_ERROR_CODE_OTHER = 99;

        /// <summary>
        ///Xếp loại theo vi phạm 
        /// </summary> 
        public const int CONDUCT_ESTIMATION_TYPE_VIOLATION = 1;

        /// <summary>
        ///Xếp loại theo học lực 
        /// </summary> 
        public const int CONDUCT_ESTIMATION_TYPE_CAPACITY = 2;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int CONDUCT_ESTIMATION_TYPE_UNIDENTIFIED = 0;

        /// <summary>
        ///Theo điểm trung bình các môn 
        /// </summary> 
        public const int RANKING_CRITERIA_AVERAGE_MARK = 1;


        /// <summary>
        /// Theo hoc luc
        /// </summary>
        public const int RANKING_CRITERIA_CAPACITY = 2;

        /// <summary>
        /// TBM và HL
        /// </summary>
        public const int RANKING_CRITERIA_AVERAGE_CAPACITY = 3;

        /// <summary>
        /// TBM và HK
        /// </summary>
        public const int RANKING_CRITERIA_AVERAGE_CONDUCT = 5;

        /// <summary>
        /// TBM, HL và HK
        /// </summary>
        public const int RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT = 7;

        /// <summary>
        ///Theo hạnh kiểm 
        /// </summary> 
        public const int RANKING_CRITERIA_CONDUCT = 4;

        /// <summary>
        ///Hệ số môn chuyên kỳ 1 
        /// </summary> 
        public const int SPECIALIZED_SUBJECT_COEFFICIENT_FISRT_SEMESTER = 3;

        /// <summary>
        ///Hệ số môn chuyên kỳ 2 
        /// </summary> 
        public const int SPECIALIZED_SUBJECT_COEFFICIENT_SECOND_SEMESTER = 3;

        /// <summary>
        ///Nhập điểm thi theo số báo danh 
        /// </summary> 
        public const int MARK_IMPORT_TYPE_ORDER_NUMBER = 1;

        /// <summary>
        ///Nhập điểm thi theo mã học sinh / họ tên 
        /// </summary> 
        public const int MARK_IMPORT_TYPE_PUPILCODE = 2;

        /// <summary>
        ///Nhập điểm thi theo số phách 
        /// </summary> 
        public const int MARK_IMPORT_TYPE_DETACHABLE_HEAD = 3;

        /// <summary>
        /// Không xác định
        /// </summary>
        public const int ORDER_TYPE_UNIDENTIFIED = 0;

        /// <summary>
        /// Xếp loại thi đua theo tuần
        /// </summary>
        public const int ORDER_TYPE_WEEK = 1;

        /// <summary>
        /// Xếp loại thi đua theo tháng
        /// </summary>
        public const int ORDER_TYPE_MONTH = 2;

        /// <summary>
        /// Giáo viên chủ nhiệm
        /// </summary>
        public const int TEACHER_ROLE_HEADTEACHER = 1;

        /// <summary>
        /// Giáo viên chủ nhiệm - giảng dạy
        /// </summary>
        public const int TEACHER_ROLE_TEACHING = 10;

        /// <summary>
        ///  Giáo viên phụ trách - chủ nhiệm - giảng dạy
        /// </summary>
        public const int TEACHER_ROLE_REPOSIBILITY = 11;

        /// <summary>
        /// Giáo viên bộ môn
        /// </summary>
        public const int TEACHER_ROLE_SUBJECTTEACHER = 2;

        /// <summary>
        /// Giáo viên chủ nhiệm và GVBM
        /// </summary>
        public const int TEACHER_ROLE_HEAD_SUBJECTTEACHER = 4;

        /// <summary>
        /// Giám thị
        /// </summary>
        public const int TEACHER_ROLE_OVERSEEING = 3;

        /// <summary>
        /// Giáo viên chủ nhiệm và giám thị
        /// </summary>
        public const int TEACHER_ROLE_HEAD_OVERSEEING = 5;

        /// <summary>
        /// GVBM và giám thị
        /// </summary>
        public const int TEACHER_ROLE_SUBJECT_OVERSEEING = 6;

        /// <summary>
        /// GVCN, GVBM và giám thị
        /// </summary>
        public const int TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING = 7;

        /// <summary>
        /// Cấp 1
        /// </summary>
        public const int APPLIED_LEVEL_PRIMARY = 1;

        /// <summary>
        /// Cấp 2
        /// </summary>
        public const int APPLIED_LEVEL_SECONDARY = 2;

        /// <summary>
        /// Cấp 3
        /// </summary>
        public const int APPLIED_LEVEL_TERTIARY = 3;

        /// <summary>
        /// Nhà trẻ
        /// </summary>
        public const int APPLIED_LEVEL_CRECHE = 4;

        /// <summary>
        /// Mẫu giáo
        /// </summary>
        public const int APPLIED_LEVEL_KINDER_GARTEN = 5;

        /// <summary>
        /// Lớp Ghép
        /// </summary>
        public const int Combine_Class_ID = 100;
        public const string Combine_Class_Name = "Lớp ghép";

        /// <summary>
        /// Môn tính điểm
        /// </summary>
        public const int ISCOMMENTING_TYPE_MARK = 0;

        /// <summary>
        /// Môn nhận xét
        /// </summary>
        public const int ISCOMMENTING_TYPE_JUDGE = 1;

        /// <summary>
        /// Môn tính điểm kết hợp với nhận xét
        /// </summary>
        public const int ISCOMMENTING_TYPE_MARK_JUDGE = 2;

        /// <summary>
        /// 100% số nhận xét đều Đạt
        /// </summary>
        public const int MARK_A_PLUS = 1;

        /// <summary>
        /// >= 50% nhận xét đều Đạt
        /// </summary>
        public const int MARK_A = 2;

        /// <summary>
        /// các trường hợp còn lại
        /// </summary>
        public const int MARK_B = 3;

        /// <summary>
        /// khong co diem
        /// </summary>
        public const int NO_MARK = 0;
        /// <summary>
        /// Hoc nghe loai gioi
        /// </summary>
        public const int APPRENTICESHIP_TRAINING_REANKING_EXPERT = 1;

        /// <summary>
        /// Hoc nghe loai kha
        /// </summary>
        public const int APPRENTICESHIP_TRAINING_REANKING_GOOD = 2;

        /// <summary>
        /// Hoc nghe loai trung binh
        /// </summary>
        public const int APPRENTICESHIP_TRAINING_REANKING_NORMAL = 3;

        public const int DEFAULT_RELIGION = 17;

        public const int DEFAULT_ETHNIC = 1;

        /// <summary>
        /// Giáo dục phổ thông
        /// </summary>
        public const int TRAINING_TYPE_NORMAL = 1;

        /// <summary>
        /// Giáo dục thường xuyên
        /// </summary>
        public const int TRAINING_TYPE_VOCATIONAL = 2;

        /// <summary>
        /// đủ điều kiện TN
        /// </summary>
        public const int GRADUATION_STATUS_APPROVE_SUFFICIENT = 1;

        /// <summary>
        /// chưa đủ điều kiện xét duyệt TN
        /// </summary>
        public const int GRADUATION_STATUS_APPROVE_INSUFFICIENT = 2;

        /// <summary>
        /// đủ điều kiện dự thi TN
        /// </summary>
        public const int GRADUATION_STATUS_EXAMINATION_SUFFICIENT = 3;

        /// <summary>
        /// chưa đủ điều kiện dự thi TN
        /// </summary>
        public const int GRADUATION_STATUS_EXAMINATION_INSUFFICIENT = 4;

        /// <summary>
        /// Đã tốt nghiệp
        /// </summary>
        public const int GRADUATION_STATUS_GRADUATED = 5;

        public const int INDEX_PAGE = 5;

        public const int GENERAL_SUMMED_EVALUATION_GKI = 1;
        public const int GENERAL_SUMMED_EVALUATION_GKII = 2;
        public const int GENERAL_SUMMED_EVALUATION_CKI = 3;
        public const int GENERAL_SUMMED_EVALUATION_CKII = 4;

        /// <summary>
        ///Kích thước giấy A4
        /// </summary> 
        public const int PAPER_SIZE_A4 = 1;

        /// <summary>
        ///Kích thước giấy A5
        /// </summary> 
        public const int PAPER_SIZE_A5 = 2;

        /// <summary>
        ///Kiểu điểm miệng
        /// </summary> 
        public const string MARK_TYPE_M = "M";
        public const int MARKTYPE_M = 1;
        /// <summary>
        ///Kiểu điểm viết
        /// </summary> 
        public const string MARK_TYPE_V = "V";

        /// <summary>
        ///Kiểu điểm 15 phút
        /// </summary> 
        public const string MARK_TYPE_P = "P";

        /// <summary>
        ///Kiểu điểm kiểm tra học kỳ
        /// </summary> 
        public const string MARK_TYPE_HK = "HK";

        /// <summary>
        ///khoá học kỳ
        /// </summary> 
        public const string MARK_TYPE_LHK = "LHK";

        /// <summary>
        ///Kiểu điểm kiểm tra giữa học kỳ        
        /// </summary> 
        public const string MARK_TYPE_GK1 = "GK1";

        /// <summary>
        ///Kiểu điểm kiểm tra cuối học kỳ
        /// </summary> 
        public const string MARK_TYPE_GK2 = "GK2";

        /// <summary>
        ///Kiểu điểm kiểm tra cuối học kỳ 1 - dành cho cấp 1, tương đương kiểm tra học kỳ
        /// </summary> 
        public const string MARK_TYPE_CK1 = "CK1";

        /// <summary>
        ///Kiểu điểm kiểm tra cuối học kỳ 2 - dành cho cấp 1, tương đương kiểm tra học kỳ
        /// </summary> 
        public const string MARK_TYPE_CK2 = "CN";

        public const string JUDGE_MARK_APLUS = "A+";

        public const string JUDGE_MARK_A = "A";

        public const string JUDGE_MARK_B = "B";

        public const string JUDGE_MARK_D = "Đ";

        public const string JUDGE_MARK_CD = "CĐ";

        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo đợt size A4
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4 = "InPhieuBaoDiemTheoDot_SizeA4";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo đợt size A5
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5 = "InPhieuBaoDiemTheoDot_SizeA5";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 1 size A4
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4 = "InPhieuBaoDiemTheoKy1_SizeA4";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 1 size A5
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A5 = "InPhieuBaoDiemTheoKy1_SizeA5";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 2 size A4
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4 = "InPhieuBaoDiemTheoKy2_SizeA4";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 2 size A5
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A5 = "InPhieuBaoDiemTheoKy2_SizeA5";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 1 - Mẫu 2
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_I_MAU_2 = "InPhieuBaoDiemTheoKy1_Mau2";
        /// <summary>
        ///Mã báo cáo in phiếu báo điểm theo kỳ 2 - Mẫu 2
        /// </summary> 
        public const string REPORT_PHIEU_BAO_DIEM_THEO_KY_II_MAU_2 = "InPhieuBaoDiemTheoKy2_Mau2";

        /// <summary>
        ///Mã báo cáo danh sách khen thưởng học sinh cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_KHEN_THUONG_CAP1 = "HS_TH_DanhSachKhenThuong";
        /// <summary>
        ///Mã báo cáo danh sách khen thưởng theo TT30 học sinh cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_KHEN_THUONG_TT30_CAP1 = "HS_TH_DanhSachKhenThuongTT30";
        /// <summary>
        ///Mã báo cáo danh sách khen thưởng theo TT22 học sinh cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_KHEN_THUONG_TT22 = "HS_TH_QuanlykhenthuongTT22";

        public const string REPORT_SUBJECT_AND_EDUCATION_GROUP = "HS_TH_SubjectAndEducationGroup";

        public const string REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP = "HS_TH_SubjectAndEducationGroup_FileZip";

        public const string REPORT_CAPACITY_AND_QUALITY = "HS_TH_CapacityAndQuality";

        public const string REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA = "HS_TH_Bangtonghop";

        public const string REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3 = "HS_TH_Bangtonghop_A3";

        public const string REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP = "HS_TH_Bangtonghop_Zip";

        public const string REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP = "HS_TH_Bangtonghop_A3_Zip";
        /// <summary>
        ///Mã báo cáo danh sách học sinh đăng ký thi lại cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_DANG_KY_THI_LAI_CAP1 = "HS_TH_DSHocSinhDangKyThiLai";
        /// <summary>
        ///Mã báo cáo danh sách học sinh thi lại cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_THI_LAI_CAP1 = "HS_TH_DSHocSinhThiLai";
        /// <summary>
        ///Mã báo cáo điểm thi học kỳ môn tính điểm cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1 = "HS_TH_ThongKeDiemThiMonTinhDiem";
        /// <summary>
        ///Mã báo cáo điểm thi học kỳ môn nhận xét cấp 1
        /// </summary> 
        public const string REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1 = "HS_TH_ThongKeDiemThiMonNhanXet";
        /// <summary>
        ///Mã báo cáo xếp loại chi tiết theo môn cấp 1
        /// </summary> 
        public const string REPORT_DS_XEP_LOAI_CHI_TIET_CAP1 = "HS_TH_XepLoaiChiTietTheoMon";
        /// <summary>
        ///Mã báo cáo thống kê xếp loại giáo dục cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1 = "HS_TH_ThongKeXLGD";
        /// <summary>
        ///Mã báo cáo thống kê xếp loại hạnh kiểm cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1 = "HS_TH_ThongKeXLHK";
        /// <summary>
        ///Mã báo cáo thống kê xếp loại học lực môn cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1 = "HS_TH_XepLoaiHocLucMon";
        /// <summary>
        ///Mã báo cáo thống kê tổng hợp xếp loại học lực môn theo khối cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1 = "HS_TH_ThongKeTongHopCacMonTheoKhoi";
        /// <summary>
        ///Mã báo cáo thống kê danh hiệu thi đua cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1 = "HS_TH_ThongKeDanhHieuThiDua";
        ///Mã báo cáo thống kê kết quả cuối năm cấp 1 
        /// </summary> 
        public const string REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1 = "HS_TH_ThongKeKetQuaCuoiNam";
        ///Mã báo cáo thống kê kết quả cuối năm cấp 1 TT30
        /// </summary> 
        public const string REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30 = "HS_TH_ThongKeKetQuaCuoiNam_TT30";
        /// <summary>
        ///Mã báo cáo danh sách khen thưởng học sinh cấp 2,3
        /// </summary> 
        public const string REPORT_DS_HS_KHEN_THUONG = "DanhSachKhenThuong";
        /// <summary>
        ///Thực đơn tuần    
        /// </summary> 
        public const string REPORT_THUC_DON_TUAN = "ThucDonTuan";
        /// <summary>
        ///Thực đơn tuần bổ sung  
        /// </summary> 
        public const string REPORT_THUC_DON_TUAN_BO_SUNG = "ThucDonTuanBoSung";
        /// <summary>
        ///Mã báo cáo danh sách học sinh thi lại
        /// </summary> 
        public const string REPORT_DS_HS_THI_LAI = "DanhSachHocSinhThiLai";

        /// <summary>
        ///Mã báo cáo thống kê điểm thi học kỳ
        /// </summary> 
        public const string REPORT_TK_DIEMTHI_HK = "ThongKeDiemThiHocKy";

        /// <summary>
        ///Mã báo cáo thống kê điểm thi học kỳ theo môn tính điểm
        /// </summary> 
        public const string REPORT_Tk_DIEMTHI_HK_THEOMON_TINHDIEM = "ThongKeDiemThiHocKyTheoMonTinhDiem";

        /// <summary>
        ///Mã báo cáo thống kê điểm trung bình môn
        /// </summary> 
        public const string REPORT_TK_DIEM_TRUNG_BINH_MON = "ThongKeDiemTBM";

        public const string REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV = "ThongKeDiemTBMTheoGV";

        public const string HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV = "HS_THPT_ThongKeDiemKTDK_HKI_GDCD_GV";

        public const string REPORT_TK_DIEM_TRUNG_BINH_MON_THEO_GV_NHANXET = "ThongKeDiemTBMTheoGVNhanXet";

        /// <summary>
        ///Mã báo cáo thống kê xếp loại chi tiết môn
        /// </summary> 
        public const string REPORT_TK_XEP_LOAI_CHITIET_MON = "ThongKeXepLoaiChiTietTheoMon";

        /// <summary>
        ///Mã báo cáo thống kê xếp loại học lực
        /// </summary> 
        public const string REPORT_TK_XEP_LOAI_HOC_LUC = "ThongKeXepLoaiHocLuc";

        /// <summary>
        ///Mã báo cáo thống kê điểm thi học kỳ theo môn nhận xét
        /// </summary> 
        public const string REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET = "ThongKeDiemThiHocKyMonNhanXet";

        /// <summary>
        ///Mã báo cáo thống kê điểm trung bình môn nhận xét
        /// </summary> 
        public const string REPORT_TK_DIEM_TB_THEOMON_NHANXET = "ThongKeDiemTBMonNhanXet";
        /// <summary>
        ///Mã báo cáo thống kê điểm thi học kỳ
        /// </summary> 
        public const string REPORT_TKDiemThiHocKy = "ThongKeDiemThiHocKy";
        /// <summary>
        ///Mã báo cáo thống kê điểm thi học kỳ theo môn tính điểm
        /// </summary> 
        public const string REPORT_TKDiemThiHocKyTheoMonTinhDiem = "ThongKeDiemThiHocKyTheoMonTinhDiem";
        /// <summary>
        ///Mã báo cáo thống kê điểm trung bình môn
        /// </summary> 
        public const string REPORT_ThongKeDiemTBM = "ThongKeDiemTBM";
        /// <summary>
        ///Mã báo cáo danh sách thi lai
        /// </summary> 
        public const string REPORT_DS_HS_DANG_KY_THI_LAI = "HS_TH_DSHocSinhDangKyThiLai";
        /// <summary>
        ///Nơi lưu trữ template file
        /// </summary> 
        //public const string TEMPLATE_FOLDER = @"D:\Template";
        public static string TEMPLATE_FOLDER
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory + "Template";
                //return @"C:\Template";
            }
        }// = @"C:\Template";
        /// <summary>
        /// Mã báo cáo học sinh chuyển lớp
        /// </summary>
        public const string REPORT_HOC_SINH_CHUYEN_LOP_A4 = "TKHSChuyenLop";
        /// <summary>
        /// Mã báo cáo học sinh chuyển trường
        /// </summary>
        /// 
        public const string REPORT_HOC_SINH_CHUYEN_TRUONG_A4 = "TKHSChuyenTruong";
        /// <summary>
        /// Thong Ke Diem Kiem Tra Dinh Ky Cap 2 SGD
        /// </summary>
        public const string REPORT_SGD_ThongKeDiemKiemTraDinhKyCap2 = "SGD_THCS_TongHopDiemKiemTraDinhKy";
        /// <summary>
        /// Thong Ke Diem Kiem Tra Dinh Ky Cap 2 PGD
        /// </summary>
        public const string REPORT_PGD_ThongKeDiemKiemTraDinhKyCap2 = "PGD_THCS_TongHopDiemKiemTraDinhKy";
        /// <summary>
        /// SGD THCS Thong Ke HLM
        /// </summary>
        public const string REPORT_SGD_THCS_ThongKeHLM = "SGD_THCS_ThongKeHLM";
        /// <summary>
        /// PGD THCS Thong Ke HLM
        /// </summary>
        public const string REPORT_PGD_THCS_ThongKeHLM = "PGD_THCS_ThongKeHLM";
        /// <summary>
        /// SGD THCS Thong Ke Diem Kiem Tra Hoc Ky
        /// </summary>
        public const string REPORT_SGD_ThongKeDiemKiemTraHocKy = "SGD_THCS_ThongKeDiemKiemTraHocKy";

        /// <summary>
        /// PGD THCS Thong Ke Diem Kiem Tra Hoc Ky
        /// </summary>
        public const string REPORT_PGD_ThongKeDiemKiemTraHocKy = "PGD_THCS_ThongKeDiemKiemTraHocKy";

        /// Thong Ke tieu hoc SGD
        /// </summary>
        public const string REPORT_SGD_ThongKeTieuHoc = "REPORT_SGD_ThongKeTieuHoc";

        /// Thong Ke tieu hoc PGD
        /// </summary>
        public const string REPORT_PGD_ThongKeTieuHoc = "REPORT_PGD_ThongKeTieuHoc";

        /// <summary>
        /// THONG KE TIEU HOC TT22
        /// MON HOC VA HDGD
        /// </summary>
        public const string REPORT_SGD_SUBJECT_AND_EDUCATION_GROUP = "REPORT_SUBJECT_AND_EDUCATION_GROUP";
        /// <summary>
        /// THONG KE NANG LUC PHAM CHAT
        /// </summary>
        public const string REPORT_SGD_CAPACITY_QUALITY = "REPORT_CAPACITY_QUALITY";
        /// <summary>
        /// THONG KE KET QUA CUOI NAM
        /// </summary>
        public const string REPORT_SGD_SUMMED_ENDING_EVALUATION = "REPORT_SUMMED_ENDING_EVALUATION";


        //public const string REPORT_DS_HS_KHEN_THUONG_CAP1 = "DSHSKhenThuongCap1";
        public const string REPORT_DANH_SACH_KHEN_THUONG_A4 = "DanhSachKhenThuong_SizeA4";
        /// <summary>
        /// Mã báo cáo học sinh chuyển đến
        /// </summary>
        public const string REPORT_HOC_SINH_CHUYEN_DEN_A4 = "TKHSChuyenDen";
        /// <summary>
        /// Mã báo cáo danh sách học sinh thôi học
        /// </summary>
        public const string REPORT_HOC_SINH_THOI_HOC_A4 = "TKHSThoiHoc";


        /// <summary>
        /// Mã báo cáo danh sách học sinh thuộc diện chính sách
        /// </summary>
        public const string REPORT_HOC_SINH_DIEN_CHINH_SACH_A4 = "TKHSChinhSach";
        /// <summary>
        /// Mã báo cáo  học sinh vi phạm
        /// </summary>
        public const string REPORT_BAO_CAO_HS_VI_PHAM = "BaoCaoHSViPham";
        /// <summary>
        /// Mã báo cáo thống kê vi phạm
        /// </summary>
        public const string REPORT_THONG_KE_VI_PHAM = "TKViPham";
        /// <summary>
        /// Mã báo cáo thống kê xếp loại hạnh kiểm theo đợt
        /// </summary>
        public const string REPORT_THONG_KE_XLHK_THEO_DOT = "ThongKeXepLoaiHanhKiemTheoDot";

        /// <summary>
        /// Bảng điểm tổng hợp lớp cấp 2, 3 theo đợt
        /// </summary>
        public const string REPORT_BANGDIEMTONGHOPLOPCAP23THEODOT = "BangDiemTongHopLopCap23TheoDot";
        /// <summary>
        /// Sổ đánh giá học sinh
        /// </summary>
        public const string REPORT_EVALUATION_BOOK = "HS_THCS_SoDanhGiaHS_Lop6A_CN";
        public const string REPORT_EVALUATION_BOOK_ZIPFILE = "HS_THCS_SoDanhGiaHS_Lop6A_CN_ZipFile";
        public const string REPORT_TRANSCRIPTS_VNEN = "HS_THCS_Hocba_NguenVanA";
        public const string REPORT_TRANSCRIPTS_VNEN_ZIPFILE = "HS_THCS_Hocba_Zipfile";
        public const string REPORT_REPEATER_TT22 = "ReportRepeaterTT22";
        public const string REPORT_RATEADD_TT22 = "ReportRateAddTT22";
        public const string FOLDER_ZIP_FILE = "FileZip";
        public const string UPLOAD_FILE = "UpLoadFile";
        public const string FILE_DOWNLOAD_DOCUMENT = "DANHSACHCHUCNANG.pdf";
        public const int TRAINING_TYPE_GDTX = 3;
        /// <summary>
        /// Đơn giá
        /// </summary>
        public const int FOOD_GROUP_TYPE_ANIMAL = 1;
        public const int FOOD_GROUP_TYPE_PLANT = 2;

        /// <summary>
        /// Activity
        /// </summary>
        public const int SECTION_OF_DAY_MORNING = 1;
        public const int SECTION_OF_DAY_EVENING = 2;

        /// <summary>
        /// MenuType - Thực đơn thường
        /// </summary>
        public const int MENU_TYPE_GENERAL = 1;

        /// <summary>
        /// MenuType - Thực đơn đơn bổ sung
        /// </summary>
        public const int MENU_TYPE_ADDITIONAL = 2;

        #region Vùng đặc biệt khó khăn
        /// <summary>
        /// Thuộc xã đặc biệt khó khăn
        /// </summary>
        public const int IN_PARTICULARLY_DIFFICULT_COMMUNE = 1;

        /// <summary>
        /// Thuộc bản đặc biệt khó khăn, xã bình thường
        /// </summary>
        public const int IN_NORMAL_COMMUNE = 2;

        /// <summary>
        /// Khác
        /// </summary>
        public const int IN_OTHER_COMMUNE = 3;
        #endregion

        #region Chế độ hỗ trợ
        /// <summary>
        /// Chế độ theo QĐ số 12/2013/QĐ-TTg
        /// </summary>
        public const int POLICY_122003_GOVERNMENT_DECISION = 1;

        /// <summary>
        /// Chế độ theo QĐ số 2123/QĐ-TTg
        /// </summary>
        public const int POLICY_2123_GOVERNMENT_DECISION = 2;

        /// <summary>
        /// Chế độ theo QĐ số 1672/QĐ-TTg
        /// </summary>
        public const int POLICY_1672_GOVERNMENT_DECISION = 3;

        /// <summary>
        /// Chế độ theo QĐ QĐ 01-UBND Tỉnh Lai Châu
        /// </summary>
        public const int POLICY_01_UBND_LCU_GOVERNMENT_DECISION = 4;

        /// <summary>
        /// Tỉnh Lai Châu
        /// </summary>
        public const int PROVINCEID_LAICHAU = 45;
        #endregion

        /// <summary>
        /// Đạm động vật
        /// </summary>
        public const int NUTRITION_COMPONENT_ANIMAL_PROTEIN = 1;

        /// <summary>
        /// Đạm thực vật
        /// </summary>
        public const int NUTRITION_COMPONENT_PLANT_PROTEIN = 2;

        /// <summary>
        /// Béo động vật
        /// </summary>
        public const int NUTRITION_COMPONENT_ANIMAL_FAT = 3;

        /// <summary>
        /// Béo thực vật
        /// </summary>
        public const int NUTRITION_COMPONENT_PLANT_FAT = 4;

        /// <summary>
        /// Đường
        /// </summary>
        public const int NUTRITION_COMPONENT_SUGAR = 5;

        /// <summary>
        /// SERVICE_CODE_ACTIVITY
        /// </summary>
        public const string SERVICE_CODE_ACTIVITY = "ABC";

        /// <summary>
        /// SMS Parents: Type Of Receiver
        /// </summary>
        public const int RECEIVE_TYPE_PUPIL = 0;
        public const int RECEIVE_TYPE_FATHER = 1;
        public const int RECEIVE_TYPE_MOTHER = 2;
        public const int RECEIVE_TYPE_PARENT = 3;
        public const int RECEIVE_TYPE_TEACHER = 4;
        public const int RECEIVE_TYPE_EMPLOYEE = 5;
        public const int RECEIVE_TYPE_SUB_EMPLOYEE = 6;
        public const int RECEIVE_TYPE_SCHOOL = 7;

        public const int HISTORY_RECEIVE_TYPE_PUPIL = 0;
        public const int HISTORY_RECEIVE_TYPE_PARENTS = 1;
        public const int HISTORY_RECEIVE_TYPE_EMPLOYEE = 2;

        /// <summary>
        /// SMS Parents: SchedularType
        /// </summary>
        public const int SCHEDULE_DAY = 0;
        public const int SCHEDULE_WEEK = 1;
        public const int SCHEDULE_MONTH = 2;
        public const int SCHEDULE_DISTANCE = 3;
        public const int SCHEDULE_YEAR = 4;
        public const int SCHEDULE_UNEXPECTED = 5;

        /// <summary>
        /// SMS Parents: InfoType
        /// </summary>
        public const int INFO_TYPE_PARENTS = 0;
        public const int INFO_TYPE_SUMUP = 1;
        public const int INFO_TYPE_RANKING = 2;
        public const int INFO_TYPE_CONDUCT_CAPACITY = 3;
        public const int INFO_TYPE_CONDUCT_CAPACITY_SEMESTER = 4;
        public const int INFO_TYPE_FAULTS_ABSENT = 5;
        public const int INFO_TYPE_PARENT_UNEXPECTED = 6;
        public const int INFO_TYPE_TEACHER_UNEXPECTED = 7;
        public const int INFO_TYPE_SCHOOL_UNEXPECTED = 8;
        public const int INFO_TYPE_EMPLOYEE_UNEXPECTED = 9;

        public const int SMS_TYPE_FOR_PARENT = 0;
        public const int SMS_TYPE_FOR_TEACHER = 1;
        public const int SMS_TYPE_CAPACITY = 2;
        public const int SMS_TYPE_CONDUCT = 3;

        /// <summary>
        /// SMS Parents: Contract Status
        /// </summary>
        public const int CONTRACT_STATUS_WAITING_APPROVE = 1;
        public const int CONTRACT_STATUS_NORMAL = 2;
        public const int CONTRACT_STATUS_REFUSED = 3;
        public const int CONTRACT_STATUS_CANCELED = 4;

        /// <summary>
        /// SMS Parents: Subscription Status
        /// </summary>
        public const int SUBSCRIPTION_STATUS_NOT_PAID = 0;
        public const int SUBSCRIPTION_STATUS_NORMAL = 1;
        public const int SUBSCRIPTION_STATUS_EXPIRED = 2;
        public const int SUBSCRIPTION_STATUS_CLOSED = 3;

        /// <summary>
        /// SMS Parents: Type Of Subscription Time
        /// </summary>
        public const int SUBSCRIPTION_TIME_PERIOD = 0;
        public const int SUBSCRIPTION_TIME_SEMESTER_1 = 1;
        public const int SUBSCRIPTION_TIME_SEMESTER_2 = 2;
        public const int SUBSCRIPTION_TIME_ALL_OF_YEAR = 3;

        /// <summary>
        /// SMS Parents: SendType
        /// </summary>
        public const int SEND_TYPE_MOBILE = 0;
        public const int SEND_TYPE_EMAIL = 1;

        /// <summary>
        /// SMS Parents: Send SMS Status
        /// </summary>
        public const int STATUS_FAIL = 0;
        public const int STATUS_SUCCESS = 1;

        /// <summary>
        /// SMS Teacher: Contract Status
        /// </summary>
        public const int SMS_TEACHER_ACTIVE_STATUS = 1;
        public const int SMS_TEACHER_BLOCK_STATUS = 2;

        public enum NetworkProviders
        {
            VIETTEL,
            VINAPHONE,
            MOBIPHONE,
            VIETNAMOBILE,
            GMOBILE,
            SFONE,
            OTHERS
        };

        /// <summary>
        ///Gui thong bao vao tai khoan he thong
        /// </summary> 
        public const int SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT = 1;

        /// <summary>
        ///Gui thong bao vao tai khoan email
        /// </summary> 
        public const int SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT = 2;


        // Mã báo cáo
        public const string REPORT_BANGDIEMMONHOCCAP1KYI = "BangDiemMonHocCap1KyI";
        public const string REPORT_BANGDIEMTONGHOPLOPCAP1 = "BangDiemTongHopLopCap1";
        public const string REPORT_BANGDIEMMONHOCCAP1KYII = "BangDiemMonHocCap1KyII";
        public const string REPORT_BANGDIEMMONHOCCAP1CN = "BangDiemMonHocCap1CN";
        public const string REPORT_INSOGOITENVAGHIDIEM = "SoGoiTenVaGhiDiem";
        public const string REPORT_THCS_INSOGOITENVAGHIDIEM = "THCS_SoGoiTenVaGhiDiem";
        public const string REPORT_BANGDIEMLOPCAP23THEODOT = "BangDiemLopCap23TheoDot";
        public const string REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY = "BangDiemTongHopLopCap23TheoKy";
        public const string REPORT_SODIEMBOMONTHEOGIAOVIEN = "SoDiemBoMonTheoGiaoVien";
        public const string REPORT_PUPILREPRATERS = "HS_DSHocSinhOLaiLop";
        public const string REPORT_MARKINPUTBOOK = "SoGhiDiemGiaoVien";

        public class StatisticsConfig
        {
            //Loại điểm dưới trung bình
            public static int MARK_TYPE_BELOW_AVERAGE = 1;
            //Loại điểm trên trung bình
            public static int MARK_TYPE_ABOVE_AVERAGE = 2;
            //Loại điểm: tìm kiếm all
            public static int MARK_TYPE_ALL = 0;
            //Loại môn tính điểm
            public static int SUBJECT_TYPE_MARK = 1;
            //Loại môn nhận xét
            public static int SUBJECT_TYPE_JUDGE = 2;
            //Loại môn: tìm kiếm tất cả
            public static int SUBJECT_TYPE_ALL = 0;
            //Loại báo cáo Thống kê điểm thi học kỳ
            public static string REPORT_TYPE_DIEM_THI_HOC_KY = "DiemThiHocKy";

            //Loại báo cáo Thống kê học lực môn
            public static string REPORT_TYPE_HOC_LUC_MON = "HocLucMon";
            //Loại báo cáo Thống kê học lực 
            public static string REPORT_TYPE_HOC_LUC = "HocLuc";
            //Loại báo cáo Thống kê hạnh kiểm
            public static string REPORT_TYPE_HANH_KIEM = "HanhKiem";
            //Loại báo cáo Thống kê kết quả cuối năm
            public static string REPORT_TYPE_KET_QUA_CUOI_NAM = "KetQuaCuoiNam";

            //Loại báo cáo Thống kê học lực môn
            public static string REPORT_TYPE_TRUNG_BINH_MON = "TrungBinhMon";
            //Loại báo cáo Thống kê học lực môn
            public static string REPORT_TYPE_HOC_LUC_MON_CHI_TIET = "HocLucMonChiTiet";
            //Loại báo cáo Thống kê điểm kiểm tra định kỳ
            public static string REPORT_TYPE_DIEM_KIEM_TRA_DINH_KY = "DiemKiemTraDinhKy";
            //Loại báo cáo Thống kê điểm kiểm tra Học kỳ
            public static string REPORT_TYPE_DIEM_KIEM_TRA_HOC_KY = "DiemKiemTraHocKy";
            public static string REPORT_TYPE_CHAT_LUONG_CAC_LOP = "ChatLuongCacLop";
            public static string REPORT_TYPE_CHAT_LUONG_MON_THI = "ChatLuongMonThi";

        }
        public const string REPORT_DANHSACHLOP_123MN = "REPORT_DANHSACHLOP_123MN";
        public const string REPORT_DANHSACHLOP_123MN_Zip = "REPORT_DANHSACHLOP_123MN_Zip";
        public const string REPORT_INHOCBATHEOMAUCAP1 = "HocBaHocSinhCap1";
        public const string REPORT_INHOCBATHEOMAUCAP23 = "HocBaHocSinhCap23";
        public const string REPORT_INHOCBATHEOMAUCAP23TT58 = "HocBaHocSinhCap23TT58";
        public const string REPORT_INHOCBATHEOMAUCAP23_ZIPFILE = "HocBaHocSinhCap23_ZipFile";
        public const string REPORT_INHOCBATHEOMAUCAP23_GDTX = "HocBaHocSinhCap23_GDTX";
        public const string REPORT_INHOCBATHEOMAUCAP23_GDTX_ZIPFILE = "HocBaHocSinhCap23_GDTX_ZipFile";
        public const int REPORT_EDUCATION_GRADE_PRIMARY = 1;
        public const int REPORT_EDUCATION_GRADE_SECONDARY = 2;
        public const int REPORT_EDUCATION_GRADE_TERTIARY = 3;
        public const string REPORT_CAPACITYBYMARK_EXCELLENT = "Giỏi";
        public const string REPORT_CAPACITYBYMARK_GOOD = "Khá";
        public const string REPORT_CAPACITYBYMARK_NORMAL = "TB";
        public const string REPORT_CAPACITYBYMARK_BAD = "Yếu";
        public const string REPORT_BANG_DIEM_LOP_CAP23_HOCKYI = "BangDiemMonHocCap23KyI";
        public const string REPORT_BANG_DIEM_LOP_CAP23_HOCKYII = "BangDiemMonHocCap23KyII";
        public const string REPORT_BANG_DIEM_LOP_CAP23_CANAM = "BangDiemMonHocCap23CN";
        public const string REPORT_BANG_DIEM_BINH_QUAN_CA_NAM = "BangDiemBinhQuanCN";
        public const string REPORT_THONG_KE_XEP_LOAI_HANH_KIEM = "ThongKeXepLoaiHanhKiem";
        public const string REPORT_THONG_KE_XEP_LOAI_HOC_LUC_MON = "ThongKeXepLoaiHocLucMon";
        public const string REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM = "ThongKeTongHopHocLucHanhKiem";
        public const string REPORT_THONG_KE_TONG_HOP_CAC_MON_THEO_KHOI = "ThongKeTongHopCacMonTheoKhoi";
        public const string REPORT_THONG_KE_DANH_HIEU_THI_DUA = "ThongKeDanhHieuThiDua";
        public const string REPORT_THONG_KE_HANH_KIEM_SO = "SGD_THCS_ThongKeHanhKiem";
        public const string REPORT_THONG_KE_HANH_KIEM_PHONG = "PGD_THCS_ThongKeHanhKiem";
        public const string REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2 = "SGD_THCS_ThongKeHanhKiemTheoQuanHuyen";
        public const string REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2 = "SGD_THCS_ThongKeHanhKiemTheoKhoi";
        //ThongKeDiemTBMTheoDot
        public const string REPORT_THONG_KE_DIEM_TBM_THEO_DOT = "ThongKeDiemTBMTheoDot";

        //ThongKeKetQuaCuoiNam
        public const string REPORT_THONG_KE_KET_QUA_CUOI_NAM = "ThongKeKetQuaCuoiNam";
        //Thong Ke Tong Hop Hoc Luc Hanh Kiem
        public const string REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT = "ThongKeTongHopHocLucHanhKiemTheoDot";
        //ThongKeDiemKiemTraDinhKyCap3
        public const string REPORT_THONG_KE_DIEM_KIEM_TRA_DINH_KY_CAP_3 = "ThongKeDiemKiemTraDinhKyCap3";



        public const string REPORT_SGD_THPT_THONGKEDIEMKIEMTRADINHKY = "SGD_THPT_ThongKeDiemKiemTraDinhKy";
        public const string REPORT_SGD_THPT_THONGKEDIEMKIEMTRAHOCKY = "SGD_THPT_ThongKeDiemKiemTraHocKy";
        public const string REPORT_SGD_THPT_THONGKEHLM = "SGD_THPT_ThongKeHLM";
        public const string REPORT_SGD_THPT_THONGKEHOCLUC = "SGD_THPT_ThongKeHocLuc";
        public const string REPORT_SGD_THPT_THONGKEHANHKIEM = "SGD_THPT_ThongKeHanhKiem";
        public const string REPORT_SGD_THPT_THONGKEHOCLUCTHEOBAN = "SGD_THPT_ThongKeHocLucTheoBan";
        public const string REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN = "SGD_THPT_ThongKeHanhKiemTheoBan";
        public const string REPORT_SGD_THPT_THONGKEHOCLUCTHEOQUANHUYEN = "SGD_THPT_ThongKeHocLucTheoQuanHuyen";
        public const string REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN = "SGD_THPT_ThongKeHanhKiemTheoQuanHuyen";

        public const string REPORT_SGD_THCS_THONGKEHOCLUCTHEOKHOI = "SGD_THCS_ThongKeHocLucTheoKhoi";
        public const string REPORT_PGD_THCS_THONGKEHOCLUCTHEOKHOI = "PGD_THCS_ThongKeHocLucTheoKhoi";
        public const string REPORT_SGD_THCS_THONGKEHOCLUCTHEOQUANHUYEN = "SGD_THCS_ThongKeHocLucTheoQuanHuyen";


        public const string REPORT_BANGDIEMTHCAP1 = "BangDiemTHCap1";
        public const string REPORT_BANGDIEMTHCAP23 = "BangDiemTHCap23";

        public const string SGD_THPT_THONGKEHANHKIEM = "SGD_THPT_ThongKeHanhKiem";
        public const string THONGKEHANHKIEMTHEOBANCAP2 = "ThongKeHanhKiemTheoBanCap2";

        public const string THONGKEDIEMKIEMTRADINHKYCAP1 = "ThongKeDiemKiemTraDinhKyCap1";
        public const string THONGKEDIEMKIEMTRAHOCKYCAP1 = "ThongKeDiemKiemTraHocKyCap1";
        public const string THONGKEHOCLUCMONCAP1 = "ThongKeHocLucMonCap1";
        public const string THONGKEHOCLUCMONTINHDIEMCAP1 = "ThongKeHocLucMonTinhDiemCap1";
        public const string THONGKEHOCLUCMONNHANXETCAP1 = "ThongKeHocLucMonNhanXetCap1";
        public const string THONGKEHANHKIEMCAP1 = "ThongKeHanhKiemCap1";
        public const string THONGKEHOCLUCCAP1 = "ThongKeHocLucCap1";


        public const string THONGKEDIEMKIEMTRADINHKYCAP2 = "ThongKeDiemKiemTraDinhKyCap2";
        public const string THONGKEDIEMKIEMTRAHOCKYCAP2 = "ThongKeDiemKiemTraHocKyCap2";
        public const string THONGKEHOCLUCMONCAP2 = "ThongKeHocLucMonCap2";
        public const string THONGKEHANHKIEMCAP2 = "ThongKeHanhKiemCap2";
        public const string THONGKEHOCLUCCAP2 = "ThongKeHocLucCap2";

        public const string THONGKEDIEMKIEMTRADINHKYCAP3 = "ThongKeDiemKiemTraDinhKyCap3";
        public const string THONGKEDIEMKIEMTRAHOCKYCAP3 = "ThongKeDiemKiemTraHocKyCap3";
        public const string THONGKEHOCLUCMONCAP3 = "ThongKeHocLucMonCap3";
        public const string THONGKEHANHKIEMTHEOBANCAP3 = "ThongKeHanhKiemTheoBanCap3";
        public const string THONGKEHOCLUCTHEOBANCAP3 = "ThongKeHocLucTheoBanCap3";
        public const string REPORT_THONGKE_TONGHOP_MON_THEODOT = "ThongKeTongHopCacMonTheoDot";
        public const string REPORT_THONGKE_TONGHOP_XEPLOAI_MON_THEODOT = "ThongKeTongHopXepLoaiCacMonTheoDot";
        public const string REPORT_THONGKE_CHITIET_TINHHINH_NHAPLIEU = "ThongKeChiTietTinhHinhNhapLieu";
        public const string REPORT_THONGKE_TINHHINH_NHAPLIEU_PHONGGD = "TinhHinhNhapLieuTaiCacPhongGD";
        public const string REPORT_THONGKE_TINHHINH_NHAPLIEU_QUAN_TRUONG = "TinhHinhNhapLieuTheoQuanTaiTruong";
        public const string REPORT_THONG_KE_HOC_LUC_SO = "SGD_THCS_ThongKeHocLuc";
        public const string REPORT_THONG_KE_HOC_LUC_PHONG = "PGD_THCS_ThongKeHocLuc";
        public const string REPORT_SGD_THPT_ThongKeHLM = "SGD_THPT_ThongKeHLM";

        //BC giáo viên
        public const string REPORT_PHAN_CONG_GIANG_DAY = "BC_PhanCongGiangDay";
        public const string REPORT_SO_LUONG_HOC_SINH_THEO_GIAO_VIEN = "BaoCaoSoLuongHSTheoGV";
        public const string REPORT_HOC_LUC_HOC_SINH_THEO_GIAO_VIEN_CAP1 = "BCHocLucHSTheoGiaoVienCap1";
        public const string REPORT_HOC_LUC_HOC_SINH_THEO_GIAO_VIEN_CAP23 = "BCHocLucHSTheoGiaoVienCap23";
        public const string REPORT_DANH_SACH_CAN_BO = "DanhSachCanBo";
        public const string REPORT_DANH_SACH_CAN_BO_BCGV = "DanhSachCanBo_BCGV";
        public const string REPORT_DANH_SACH_CAN_BO_HOP_DONG = "DSCanBoHopDong";
        public const string REPORT_DANH_SACH_CAN_BO_BIEN_CHE = "DSCanBoBienChe";
        public const string REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN = "DSCanBoTheoThamNien";
        public const string REPORT_DANH_SACH_BANG_LUONG_CAN_BO = "BangLuongCanBo";
        public const string REPORT_DANH_SACH_CAN_BO_DEN_KY_TANG_LUONG = "DanhSachCanBoDenKyTangLuong";
        public const string REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN = "DSCanBoDatVaVuotChuan";
        public const string REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN = "DSCanBoDuoiChuan";
        public const string REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH = "DSCanBoTheoTDCM";
        public const string REPORT_DANH_SACH_GVCN = "DanhSachGVCN";

        //Báo cáo sơ kết gửi sở gd
        public const string REPORT_SO_KET_GUI_SOGD = "BCSoKetGuiSoGD";
        public const string REPORT_SO_KET_GUI_SOGD_THPT = "BCSoKetGuiSoGD_THPT";

        //Loại hợp đồng
        public const int CONTRACT_TYPE_STAFF = 0; //Biên chế
        public const int CONTRACT_TYPE_NOSTAFF = 1; //Hợp đồng
        public const int CONTRACT_TYPE_BUDGET = 2; //Trong ngân sách

        //Confirm anh Chien, fix cung loai bao cao Bien che, hop dong
        public const string CONTRACT_TYPE_STAFF_NAME = "biên chế"; //Biên chế
        public const string CONTRACT_TYPE_NOSTAFF_NAME = "hợp đồng"; //Hợp đồng
        public const string CONTRACT_TYPE_NOSTAFF_68 = "hợp đồng nghị định 68";

        public const int CHILDREN_REPORT_TYPE_EVALUATIONDEVELOPMENT = 1;
        public const int CHILDREN_REPORT_TYPE_RESULTDEVELOPMENT = 2;
        public const int CHILDREN_REPORT_TYPE_GENERALEVALUATIONDEVELOPMENT = 3;
        public const string REPORT_BANG_DANH_GIA_SU_PHAT_TRIEN = "BangDanhGiaSuPhatTrien";
        public const string REPORT_BANG_KET_QUA_DANH_GIA_TRE = "BangKetQuaDanhGia";
        public const string REPORT_TONG_HOP_KET_QUA_DANH_GIA = "TongHopKetQuaDanhGia";
        public const string REPORT_TIEN_AN_HANG_THANG = "TienAnHangThang";

        //Trình độ cán bộ so với chuẩn
        public const int FULLFILLMENT_STATUS_STANDARD = 2; //Đạt chuẩn
        public const int FULLFILLMENT_STATUS_UNDER_STANDARD = 1; //Dưới chuẩn
        public const int FULLFILLMENT_STATUS_ABOVE_STANDARD = 3; //Vượt chuẩn

        //BC Học Nghề
        public const string REPORT_DIEM_DANH_LOP_NGHE = "BangDiemDanhHocNghe";
        public const string REPORT_SO_DIEM_MON_NGHE = "BangSoDiemMonNghe";
        //Loại
        public const string ApprenticeshipTraining_Type_Excellent = "Giỏi";
        public const string ApprenticeshipTraining_Type_Good = "Khá";
        public const string ApprenticeshipTraining_Type_Normal = "Trung Bình";
        public const string ApprenticeshipTraining_Type_Weak = "Yếu";
        public const string ApprenticeshipTraining_Type_Poor = "Kém";
        public const string REPORT_PHIEU_HOC_NGHE = "HS_THPT_PhieuHocNghe";
        public const string REPORT_SO_GOI_TEN_VA_GHI_DIEM_NGHE = "HS_THPT_SoGoiTenVaGhiDiemNghe";
        public const string THI_THISINHVPQC = "ThisinhVPQC";
        public const string THI_GIAMTHIVPQC = "GiamThiVPQC";
        public const string THI_THISINHVANGTHI = "ThisinhVangThi";


        public const string QLT_DSTS_THICHATLUONGHOCKY = "QLT_DSTS_Thichatluonghocky";
        public const string QLT_SBD_THICHATLUONGHOCKY = "QLT_SBD_Thichatluonghocky";
        public const string QLT_DANHPHACH = "QLT_DanhPhach";

        public const string HS_DIEMMONNHANXETCAP1 = "HS_DiemMonNhanXetCap1";

        public const string BANGDIEMTHEODOT = "BangDiemTheoDot";
        public const string BANG_DIEM_THEO_DOT_MOM_NHAN_XET = "BangDiemTheoDotMonNhanXet";

        public const string REPORT_TIEN_AN_TAM_CHI_THEO_THUC_DON = "TamTinhTienAn";

        public const string REPORT_THUC_DON_NGAY = "ThucDonNgay";
        public const string REPORT_BANG_DIEU_TRA = "BDT";
        public const string REPORT_TINH_TIEN_CHO = "TinhTienCho";

        public const string BANGDIEMTHEOHOCKYCAP23 = "BangDiemTheoHocKyCap23";
        public const string BANG_DIEM_THEO_HOC_KY_CAP23_MON_NHAN_XET = "BangDiemTheoHocKyCap23MonNhanXet";
        public const string THI_KETQUATHIMON = "THI_KetQuaThiMon";
        public const string REPORT_DANH_SACH_THI_SINH = "Thi_DSThiSinh";
        public const string HS_BANGDIEMDANH = "HS_BangDiemDanh";
        public const string HS_BANGDIEMDANH_TUAN = "BangDiemDanhTuanLop_10A1";
        public const string DS_CAPTHE_BHYT = "DS_CAPTHE_BHYT";
        public const string REPORT_DANH_SACH_THI_SINH_DIEM_DANH = "Thi_DiemDanhThiSinhTheoPhong";
        public const string REPORT_DANH_SACH_THI_SINH_DIEM_DANH_SBD = "Thi_DiemDanhThiSinhTheoPhong_SBD";
        public const string REPORT_DS_PHAN_CONG_GIAM_THI = "Thi_DSPhanCongGiamThi";
        public const string REPORT_DANHSACHCANBO = "SGD_DanhSachCanBo";
        public const string REPORT_DANHSACHHS = "SGD_DanhSachHS";
        public const string REPORT_DANHSACHLOP = "SGD_DanhSachLop";
        public const string REPORT_DANH_SACH_THI_SINH_MA_HS = "Thi_NhapDiemTheoMHS";
        public const string REPORT_DANH_SACH_THI_SINH_SBD = "Thi_NhapDiemTheoSBD";
        public const string REPORT_DANH_SACH_THI_SINH_THEO_SO_PHACH = "Thi_NhapDiemTheoSoPhach";
        public const string MAU_IMPORT_LOP_HOC = "Mau_Import_LopHoc";
        public const string REPORT_DECLARE_EVALUATION_INDEX = "REPORT_DECLARE_EVALUATION_INDEX";

        public const string IMPORT_THONGTINHOCSINH = "HS_Import_ThongTinHocSinh";
        public const string IMPORT_TH_THONGTINHOCSINH = "HS_TH_Import_ThongTinHocSinh";
        public const string IMPORT_THCS_THONGTINHOCSINH = "HS_THCS_Import_ThongTinHocSinh";
        public const string IMPORT_THPT_THONGTINHOCSINH = "HS_THPT_Import_ThongTinHocSinh";
        //Mau import moi 2016 Aug
        public const string IMPORT_THONGTINHOCSINH_201608 = "HS_Import_ThongTinHocSinh_2016";
        public const string IMPORT_THONGTINHOCSINH_2018 = "HS_Import_ThongTinHocSinh_2018";

        public const string REPORT_GIAM_THI_VPQC = "Thi_GiamThiVPQC";
        public const string REPORT_THI_SINH_VPQC = "Thi_ThiSinhVPQC";

        public const string REPORT_KETQUA_KHAM_SUCKHOE_TRUONG = "KetQuaKhamSucKhoeTruong";
        public const string REPORT_KETQUA_KHAM_MAT = "KetQuaKhamMat";
        public const string REPORT_KETQUA_KHAM_SUCKHOE_PGD = "KetQuaKhamSucKhoePGD";
        public const string REPORT_LOOKUPINFO_FEMALE = "Nữ";
        public const string REPORT_LOOKUPINFO_MALE = "Nam";
        public const string REPORT_LOOKUPINFO_STUDYING = "Đang học";
        public const string REPORT_LOOKUPINFO_GRADUATED = "Đã tốt nghiệp";
        public const string REPORT_LOOKUPINFO_MOVED_TO_OTHER_SCHOOL = "Đã chuyển trường";
        public const string REPORT_LOOKUPINFO_LEAVED_OFF = "Đã thôi học";
        public const string REPORT_LOOKUPINFO_MOVED_TO_OTHER_CLASS = "chuyển lớp";
        public const string REPORT_LOOKUPINFO_ISYOUTHLEAGEMEMBER = "Là Đoàn viên";
        public const string REPORT_LOOKUPINFO_ISCOMMUNISTPARTYMEMBER = "Là Đảng viên";
        public const string REPORT_LOOKUPINFO_NO = "Không";
        public const string REPORT_DANH_SACH_DIEM_THI_THEO_LOP = "Thi_KetQuaThiTheoLop";
        public const string REPORT_DANH_SACH_DIEM_THI_THEO_PHONG_THI = "Thi_KetQuaThiTheoPhongThi";
        public const string REPORT_THONG_KE_CHAT_LUONG_CAC_LOP = "Thi_ThongKeChatLuongCacLop";
        public const string REPORT_THONG_KE_CHAT_LUONG_MON_THI = "Thi_ThongKeChatLuongMonThi";
        public const string MAU_IMPORT_HO_SO_TRE = "5_BM_DanhSachHocSinh";
        public const string MAU_IMPORT_HO_SO_TRE_MOI = "5_BM_DanhSachHocSinh_Moi";

        public const int SECTION_MORNING = 1;
        public const int SECTION_AFTERNOON = 2;
        public const int SECTION_MORNING_AFTERNOON = 4;
        public const int SECTION_EVENING = 3;
        public const int SECTION_MORNING_EVENING = 5;
        public const int SECTION_AFTERNOON_EVENING = 6;
        public const int SECTION_ALL = 7;

        public const int TYPE_PERIOD = 2;
        public const int TYPE_MONTH = 3;
        public const string TEMPLATE_THONGKE_TAIMUIHONG = "Template_Thongke_Taimuihong";

        public const string TH_THONGKE_DIEMKIEMTRA_HOCKY = "TH_ThongKeDiemKiemTraHocKy";
        public const string TH_THONGKE_DIEMKT_DINHKY = "TH_ThongKeDiemKTDinhKy";
        public const string TH_THONGKE_HOCLUCMON_NX = "TH_ThongKeHocLucMonNX";
        public const string TH_THONGKE_HOCLUCMON_TINHDIEM = "TH_ThongKeHocLucMonTinhDiem";
        public const string TH_THONGKE_DIEM_TBM = "TH_ThongKeDiemTBM";
        public const string TH_THONGKEHOCLUCMONNX = "TH_ThongKeHocLucMonNX";
        public const string TH_THONGKEHOCLUCMONTINHDIEM = "TH_ThongKeHocLucMonTinhDiem";
        public const string THCS_THONGKE_DIEMKIEMTRA_HOCKY = "THCS_ThongKeDiemKiemTraHocKy";
        public const string THCS_THONGKE_DIEMKT_DINHKY = "THCS_ThongKeDiemKTDinhKy";
        public const string THCS_THONGKE_DIEM_TBM = "THCS_ThongKeDiemTBM";
        public const string THCS_THONGKE_XLHANH_KIEM = "THCS_ThongKeXLHanhKiem";
        public const string THCS_THONGKE_XLHOCLUC = "THCS_ThongKeXLHocLuc";
        public const string THPT_THONGKE_DIEMKIEMTRA_HOCKY = "THPT_ThongKeDiemKiemTraHocKy";
        public const string THPT_THONGKE_DIEMKT_DINHKY = "THPT_ThongKeDiemKTDinhKy";
        public const string THPT_THONGKE_DIEMTBM = "THPT_ThongKeDiemTBM";
        public const string THPT_THONGKE_XLHANHKIEM_THEOBAN = "THPT_ThongKeXLHanhKiemTheoBan";
        public const string THPT_THONGKE_XLHOCLUC_THEOBAN = "THPT_ThongKeXLHocLucTheoBan";
        public const string HS_BaoCaoHSNghiHoc = "HS_BaoCaoHSNghiHoc";
        public const string HS_THCS_TKHSNghiHoc = "HS_THCS_TKHSNghiHoc";
        public const string HS_DANHSACHHOCSINH = "HS_DSHocSinh";
        public const string HS_THONGKESISO = "HS_TKSiSo";
        public const string HS_THONG_KE_DO_TUOI = "HS_THONG_KE_DO_TUOI";
        public const string THONG_KE_SO_LIEU_TRE = "THONG_KE_SO_LIEU_TRE";
        public const string MAU_2_THPT_SO = "Mau_2_THPT_SO";
        public const string SOGD_THPT_DS = "SOGD_THPT_DS";
        public const string MAU_2_THPT = "Mau_2_THPT";
        public const string HS_THPT_BAOCAOSOLIEU_DN = "HS_THPT_BAOCAOSOLIEU_DN";
        public const string REPORT_THPT_DAU_NAM = "Mau_2_THPT_Mau truong";
        public const string IMPORT_DIEMTHI_HOCKY = "Import_DiemThi_HocKy";
        public const string REPORT_GRADUATION_EXAM_REGISTER = "REPORT_GRADUATION_EXAM_REGISTER";
        public const string CAN_D0_SUC_KHOE = "CAN_D0_SUC_KHOE";
        public const string Tre_BaoCaoTreNghiHoc = "Tre_BaoCaoTreNghiHoc";
        public const string Tre_TKTreNghiHoc = "Tre_TKTreNghiHoc";
        public const string Tre_TKTreNghiHocTheoNgay = "Tre_TKTreNghiHocTheoNgay";

        public const string BANGXEPLOAI_HANHKIEM_THEODOT_HL = "BangXepLoaiHanhKiem_TheoDot_HocLuc";
        public const string BANGXEPLOAI_HANHKIEM_THEODOT_VP = "BangXepLoaiHanhKiem_TheoDot_ViPham";

        public const string PREVIOUSGRADUATIONLEVEL_LEVEL1 = "Giỏi";
        public const string PREVIOUSGRADUATIONLEVEL_LEVEL2 = "Khá";
        public const string PREVIOUSGRADUATIONLEVEL_LEVEL3 = "Trung Bình";

        public const int PREVIOUSGRADUATIONLEVEL_iLEVEL1 = 1;
        public const int PREVIOUSGRADUATIONLEVEL_iLEVEL2 = 2;
        public const int PREVIOUSGRADUATIONLEVEL_iLEVEL3 = 3;
        public const string TONG_KET_DIEM_HOC_KY = "BDTongKetDot1HocKy2Lop10A";

        public const string REPORT_SMSPARENTS_CONTRACT = "SMSParentContract";

        public const string HS_THCS_DSXETDUYETTOTNGHIEP = "HS_THCS_DSXetDuyetTotNghiep";
        public const string REPORT_GRADUATION_APPROVAL = "REPORT_GRADUATION_APPROVAL";

        public const int RECEIVE_TYPE_PARENTS = 0;

        public const int CLASSTYPE_NOITRU = 1;
        public const int CLASSTYPE_BANTRU = 2;
        public const int CLASSTYPE_BANTRUDANNUOI = 3;
        public const int CLASSTYPE_NOITRUDANNUOI = 4;

        public const int HOME_PLACE_IN_SCHOOL = 1;
        public const int HOME_PLACE_LODGE = 2;

        public const string SMS_THONGKETINNHANTHEOGIAOVIEN = "SMS_ThongKeTinNhanTheoGiaoVien";
        public const string SMS_THONGKETINNHANTHEOHOCSINH = "SMS_ThongKeTinNhanTheoHocSinh";
        public const string SMS_DSTHUEBAOCHANCATKHONGTHANHCONG = "SMS_DSThueBaoChanCatKhongThanhCong";
        public const string SMS_DSTHUEBAOCHANCATTINNHAN = "SMS_DSThueBaoChanCatTinNhan";
        public const int TEACHER_RECEIVER = 4;
        public const int PARENT_RECEIVER = 3;

        public const string AuthenString = "";

        public const int DEACTIVE_SMSTEACHER_CONTRACT = 1;
        //chieu dai pass mac dinh
        public const int MAX_PASS_LENTGTH = 8;

        public const int SCHOOL_TYPE_TRUONG_CHUYEN = 6;

        public const int CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR = 1;
        public const int CODECONFIG_MIDDLECODETYPE_START_LEVEL_1 = 2;
        public const int CODECONFIG_MIDDLECODETYPE_COURSE = 3;
        public const int CODECONFIG_MIDDLECODETYPE_NO_USED = 4;

        //Môn toán
        public const string MON_TOAN = "Toán";
        // Môn tiếng việt
        public const string MON_TIENGVIET = "Tiếng Việt";
        // Xếp loại học lực
        //Giỏi
        public const string XL_GIOI = "Giỏi";
        //Khá
        public const string XL_KHA = "Khá";
        //Trung bình
        public const string XL_TRUNGBINH = "Trung Bình";
        //Yếu
        public const string XL_YEU = "Yếu";


        public const string DGK1 = "DGK1";
        public const string VGK1 = "VGK1";
        public const string GK1 = "GK1";
        public const string DCK1 = "DCK1";
        public const string VCK1 = "VCK1";
        public const string CK1 = "CK1";
        public const string DGK2 = "DGK2";
        public const string VGK2 = "VGK2";
        public const string GK2 = "GK2";
        public const string DCN = "DCN";
        public const string VCN = "VCN";
        public const string CN = "CN";
        public const string C1 = "C1";
        public const string TX = "DTX"; //DTX moi chuan, neu DB dang de la TX thi doi lai

        public const string PUPIL_ABSENCE_P = "P";
        public const string PUPIL_ABSENCE_K = "K";

        //Thang 1 2 3 4 5 6 7 8 9 mon tinh diem
        public const string T1 = "T1";
        public const string T2 = "T2";
        public const string T3 = "T3";
        public const string T4 = "T4";
        public const string T5 = "T5";
        public const string T6 = "T6";
        public const string T7 = "T7";
        public const string T8 = "T8";
        public const string T9 = "T9";
        public const string T10 = "T10";
        public const string GKI = "GK1";
        public const string GKII = "GK2";
        public const string CKI = "CK1";
        public const string CKII = "CN";
        public const string KTDKCKI = "CK1";
        public const string HK_I = "HK1";
        public const string KTDKCKII = "CK2";
        public const string HK_II = "HK2";

        //Mon nhan xet
        public const string NX1 = "NX1";
        public const string NX2 = "NX2";
        public const string NX3 = "NX3";
        public const string NX4 = "NX4";
        public const string NX5 = "NX5";
        public const string NX6 = "NX6";
        public const string NX7 = "NX7";
        public const string NX8 = "NX8";
        public const string NX9 = "NX9";
        public const string NX10 = "NX_10";
        public const string HKI = "HK1";
        public const string HKII = "HK2";

        /// <summary>
        /// nhân viên
        /// </summary>
        public const int WORK_GROUP_TYPE_EMPLOYEE = 1;
        /// <summary>
        /// giáo viên
        /// </summary>
        public const int WORK_GROUP_TYPE_TEACHER = 2;

        /// <summary>
        /// cán bộ quản lý
        /// </summary>
        public const int WORK_GROUP_TYPE_EMPLOYEE_ADMIN = 3;
        /// <summary>
        /// GDTX
        /// </summary>
        public const int SCHOOL_TYPE_TRUONG_GDTX = 9;

        public const int GET_NUMPUPIL_IN_CLASS_TYPE0 = 0;
        public const int GET_NUMPUPIL_IN_CLASS_TYPE1 = 1;
        public const int GET_NUMPUPIL_IN_CLASS_TYPE2 = 2;
        public const int GET_NUMPUPIL_IN_CLASS_TYPE3 = 3;
        public const string EXAM_ROOM_NAME = "Phòng thi số ";

        //Kiem tra Quyen xem
        public const int PER_VIEW = 1;
        //Quyen Them
        public const int PER_CREATE = 2;
        //Quyen sua
        public const int PER_UPDATE = 3;
        //Quyen xoa
        public const int PER_DELETE = 4;

        public const string PERMISSION = "checkPERMISSION";
        public const string PERMISSION_VIEW = "PERMISSION_VIEW";
        public const string PERMISSION_CREATE = "PERMISSION_CREATE";
        public const string PERMISSION_UPDATE = "PERMISSION_UPDATE";
        public const string PERMISSION_DELETE = "PERMISSION_DELETE";

        public const int BINH_THUONG = 1;
        public const int DAN_TOC_NT = 2;
        public const int DAN_TOC_BT = 7;

        public const int PUPIL_LEARNING_TYPE_CENTRALIZING = 1;
        public const int PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING = 2;
        public const int PUPIL_LEARNING_TYPE_SELF_LEARNING = 3;

        // const - Quanglm1 add 31/07/2013 - Cap khen thuong, ky luat
        /// <summary>
        ///Cấp bộ 
        /// </summary> 
        public const int DISCIPLINE_LEVEL_MINISTRY = 1;

        /// <summary>
        ///Cấp Sở GD 
        /// </summary> 
        public const int DISCIPLINE_LEVEL_PROVINCE_OFFICE = 2;

        /// <summary>
        ///Cấp Phòng GD 
        /// </summary> 
        public const int DISCIPLINE_LEVEL_DISTRICT_OFFICE = 3;

        /// <summary>
        ///Cấp trường 
        /// </summary> 
        public const int DISCIPLINE_LEVEL_SCHOOL = 4;

        /// <summary>
        ///Cấp lớp 
        /// </summary> 
        public const int DISCIPLINE_LEVEL_CLASS = 5;
        // End - Quanglm1

        public const int WORK_GROUP_TYPE_BANGIAMHIEU = 3;

        //bBang dau khai bao muc thong ke
        public class Sign_Compare
        {
            public const int GREATER_OR_EQUAL_SIGN = 1;
            public const int GREATEER_THAN_SIGN = 2;
            public const int EQUAL_SIGN = 3;
            public const int SMALLER_SIGN = 4;
            public const int SMALLER_OR_EQUAL_SIGN = 5;
        }

        //Hath Resolution Applied
        public const string STR_PRIMARY = "Cấp 1";
        public const string STR_SECONDARY = "Cấp 2";
        public const string STR_TERTIARY = "Cấp 3";
        public const string STR_CRECHE = "Nhà trẻ";
        public const string STR_KINDER_GARTEN = "Mẫu giáo";

        public const decimal DEFAULT_MERIT_POINTS_FOR_CERTIFICATE = 1;
        public const int TYPE_CERTIFICATE = 1;
        public const int SINH_HOAT_SUBJECT = -2;
        //const cho phan cong giang day NAMTA
        public class TeachingAssignment
        {
            public TeachingAssignment(int AppliedLevel, int SchoolTypeID)
            {
                #region ham lay nghiem

                if (AppliedLevel == APPLIED_LEVEL_PRIMARY)
                {
                    if (SchoolTypeID == DAN_TOC_BT)
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap1.DanTocBT.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap1.DanTocBT.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap1.DanTocBT.KIEM_NHIEM;
                    }
                    else
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap1.BinhThuong.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap1.BinhThuong.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap1.BinhThuong.KIEM_NHIEM;
                    }
                }
                else if (AppliedLevel == APPLIED_LEVEL_SECONDARY)
                {

                    if (SchoolTypeID == DAN_TOC_NT)
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap2.DanTocNT.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap2.DanTocNT.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap2.DanTocNT.KIEM_NHIEM;
                    }
                    else if (SchoolTypeID == DAN_TOC_BT)
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap2.DanTocBT.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap2.DanTocBT.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap2.DanTocBT.KIEM_NHIEM;
                    }
                    else
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap2.BinhThuong.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap2.BinhThuong.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap2.BinhThuong.KIEM_NHIEM;
                    }
                }
                else if (AppliedLevel == APPLIED_LEVEL_TERTIARY)
                {
                    if (SchoolTypeID == DAN_TOC_NT)
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap3.DanTocNT.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap3.DanTocNT.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap3.DanTocNT.KIEM_NHIEM;
                    }
                    else
                    {
                        this.DINH_MUC_TIET_DAY = TeachingAssignment.Cap3.BinhThuong.DINH_MUC_TIET_DAY;
                        this.GVCN = TeachingAssignment.Cap3.BinhThuong.GVCN;
                        this.KIEM_NHIEM = TeachingAssignment.Cap3.BinhThuong.KIEM_NHIEM;
                    }

                }
                #endregion

            }

            public int DINH_MUC_TIET_DAY = 0;
            public int GVCN = 0;
            public int KIEM_NHIEM = 0;

            public class Cap1
            {
                public class BinhThuong
                {
                    public const int DINH_MUC_TIET_DAY = 23;
                    public const int GVCN = 3;
                    public const int KIEM_NHIEM = 3;
                }
                public class DanTocBT
                {
                    public const int DINH_MUC_TIET_DAY = 21;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }
            }
            public class Cap2
            {
                public class BinhThuong
                {
                    public const int DINH_MUC_TIET_DAY = 19;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }
                public class DanTocNT
                {
                    public const int DINH_MUC_TIET_DAY = 17;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }
                public class DanTocBT
                {
                    public const int DINH_MUC_TIET_DAY = 15;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }
            }
            public class Cap3
            {
                public class BinhThuong
                {
                    public const int DINH_MUC_TIET_DAY = 17;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }
                public class DanTocNT
                {
                    public const int DINH_MUC_TIET_DAY = 15;
                    public const int GVCN = 4;
                    public const int KIEM_NHIEM = 3;
                }

            }
        }

        //const cho bao cao theo doi CLGD
        public const string REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM = "SoTheoDoiCLGDLopK1_GVBM";
        public const string REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN = "SoTheoDoiCLGDLopK1_GVCN";

        //const luu gia tri khi dinh dang template
        public const int ROW1_SHEET2 = 6;
        public const int ROW2_SHEET2 = 24;
        public const int ROWPAGE1_SHEET2 = 15;
        public const int ROWPAGE2_SHEET2 = 20;

        public const int ROW1_SHEET3 = 9;
        public const int ROW2_SHEET3 = 16;

        public const int ROW1_SHEET4_SUBJECTTEACHER = 9;
        public const int ROW2_SHEET4_SUBJECTTEACHER = 33;

        public const int ROW1_SHEET4_HEADTEACHER = 10;
        public const int ROW2_SHEET4_HEADTEACHER = 38;
        public const int COL_INITIALIZATION = 3;

        public const string REMOVE_CLASS = "LOP";
        public const int INDEX_FOREIGN_LANGUAGE = 5;

        #region BC phan thi
        public const string REPORT_EXAM_INPUT_MARK = "Report_Exam_Input_Mark";
        public const string REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI = "Report_DanhSachThiSinhTheoPhongThi";
        public const string REPORT_DANH_SACH_THI_SINH_THEO_LOP = "Report_DanhSachThiSinhTheoLop";
        public const string REPORT_DANH_SACH_DIEM_DANH = "Report_DanhSachDiemDanh";
        public const string REPORT_PHIEU_THU_BAI_THI = "THI_THPT_PhieuThuBaiThi_[NhomThi]_[MonThi]";
        public const string REPORT_PHIEU_THU_BAI_THI_ZIP = "THI_THPT_PhieuThuBaiThi_[NhomThi]";
        public const string DSHSNghiHoc_PhongSo = "DSHSNghiHoc_PhongSo";
        public const string DSHSNghiHoc_PhongSo_1 = "DSHSNghiHoc_PhongSo_1";
        public const string REPORT_DANH_SACH_PHAN_CONG_GIAM_THI = "Report_DanhSachPhanCongGiamThi";
        public const string REPORT_DANH_SACH_THI_SINH_VANG_THI = "Report_DanhSachThiSinhVangThi";
        public const string REPORT_DANH_SACH_DIEMTHI_THEO_LOP = "Report_DanhSachDiemThiTheoLop";
        public const string REPORT_DANH_SACH_DIEMTHI_THEO_PHONG = "Report_DanhSachDiemThiTheoPhong";
        public const string REPORT_THONG_KE_DIEMTHI_THEO_LOP = "Report_ThongKeDiemThiTheoLop";
        public const string REPORT_THONG_KE_DIEMTHI_THEO_MON = "Report_ThongKeDiemThiTheoMon";
        public const string REPORT_DANH_SACH_SO_PHACH = "Report_DanhSachSoPhach";
        public const string REPORT_THI_SINH_VI_PHAM = "Report_ThiSinhViPham";
        public const string REPORT_GIAM_THI_VI_PHAM = "Report_GiamThiViPham";

        public const string REPORT_NHAP_DIEM_THEO_SO_PHACH = "Report_NhapDiemThiTheoSoPhach";
        public const string REPORT_NHAP_DIEM_THEO_PHONG_THI = "Report_NhapDiemThiTheoPhongThi";
        public const string REPORT_NHAP_DIEM_THEO_MA_HOC_SINH = "Report_NhapDiemThiTheoMaHocSinh";
        public const string REPORT_NHAP_DIEM_THEO_SO_BAO_DANH = "Report_NhapDiemThiTheoSoBaoDanh";
        public const string REPORT_THONG_KE_DIEMTHI_THEO_GIAO_VIEN = "Report_ThongKeDiemThiTheoGiaoVien";
        public const string REPORT_THONG_KE_DIEMTHI_THEO_PHONG = "Report_ThongKeDiemThiTheoPhong";
        public const string REPORT_DANH_SACH_THI_SINH_PHONG_DK = "Report_DanhSachThiSinhPhongDk";
        public const string REPORT_PHIEU_BAO_THI = "HS_PhieuBaoThi";
        public const string REPORT_PHIEU_BAO_THI_ZIP = "HS_PhieuBaoThi.zip";
        #endregion

        public const string REPORT_SUMMANY_MARK = "Report_Summany_Mark";

        public const string REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC = "Report_ThongKeChatLuongGiaoDuc";
        public const string REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC_NEW = "Report_ThongKeChatLuongGiaoDuc_Moi";
        #region Khai bao const MarkInputType cho Examinations
        public const int MARK_INPUT_TYPE_DETACHABLE_BAG = 1;
        public const int MARK_INPUT_TYPE_EXAM_ROOM = 2;
        public const int MARK_INPUT_TYPE_PUPIL_CODE = 3;
        public const int MARK_INPUT_TYPE_EXAMINEE_CODE = 4;

        public const int START_ROW_REPORT_DETACHABLE_BAG = 10;
        public const int START_ROW_REPORT_EXAM_ROOM = 10;
        public const int START_ROW_REPORT_PUPIL_CODE = 10;
        public const int START_ROW_REPORT_EXAMINEE_CODE = 10;
        #endregion

        // QuangNN2 - Báo cáo Học bạ tiểu học - 06/04/2015
        public const string REPORT_PRIMARY_TRANSCRIPTS = "ReportPrimaryTranscripts";
        public const string REPORT_PRIMARY_TRANSCRIPTS_TT22 = "ReportPrimaryTranscriptsTT22";
        public const string REPORT_PRIMARY_TRANSCRIPTS_ZIPFILE = "ReportPrimaryTranscriptsZipFfile";

        public const string EMIS_REQUEST_REPORT_CODE_EQMS = "EQMS";
        public const string EMIS_REQUEST_REPORT_CODE_EMIS = "EMIS";
        public const string REPORT_QD5363_CODE = "QD5363";
        public const string REPORT_THONGKE_BOHOC_CODE = "TKBOHOC";
        public const string EMIS_REQUEST_REPORT_CODE_EMIS_PS = "TKTIEUHOC";
        public const string REPORT_FACILITIES = "CSVC_Truong";
        public const string DIFINITION_REPORT_CODE_EQMS = "EQMS_KyDauNam";
        public const string REPORT_FACILITIES_GN = "CSVC_Truong_GN";
        public const string REPORT_FACILITIES_CN = "CSVC_Truong_CN";

        public const string REPORT_EQMS_FIRST_SHEET_NAME_SCHOOL = "Truong";
        public const string REPORT_EQMS_FIRST_SHEET_NAME_TEACHER = "Nhan su";
        public const string REPORT_EQMS_FIRST_SHEET_NAME_CLASS = "Lop-HS";

        //Bao cao so danh gia hoc sinh
        public const string REPORT_PUPIL_EVALUATION_HDGD = "ReportPupilEvaluationHDGD";
        public const string REPORT_PUPIL_EVALUATION_NLPC = "ReportPupilEvaluationNLPC";
        
        //Nhap du lieu dong trong excel
        public const string REPORT_PHYSICAL_FACILITIES = "CSVCT";
        public const string REPORT_PHYSICAL_FACILITIES_PRIAMRY_LEVEL = "CSVCPRIMARY";  
        public const int PARTITION_INDICATOR_DATA = 63;
        public const int FIRST_PERIOD = 1;
        public const int MID_PERIOD = 2;
        public const int END_PERIOD = 3;

        //Bao cao Phieu lien lac
        public const string REPORT_COMMUNICATION_NOTE = "ReportCommunicationNote";

        public const string REPORT_COMMUNICATION_NOTE_K1_GHKI = "PhieulienlacKhoi1GHK1";
        public const string REPORT_COMMUNICATION_NOTE_K5_GHKI = "PhieulienlacKhoi5GHK1";
        public const string REPORT_COMMUNICATION_NOTE_K1_HKII = "PhieulienlacKhoi1HK2";
        public const string REPORT_COMMUNICATION_NOTE_K5_HKII = "PhieulienlacKhoi5HK2";

        //Bao cao Phieu lien lac VNEN
        public const string REPORT_VNEN_COMMUNICATION_NOTE = "ReportVNENCommunicationNote";
        public const string REPORT_VNEN_COMMUNICATION_NOTE_MONTH = "ReportVNENCommunicationNoteMonth";

        //Bao cao cap nhat khen thuong
        public const string REPORT_UPDATE_REWARD = "ReportUpdateReward";
        public const string FOLDER_TRUONG = "Truong";
        public const string FOLDER_HS = "HS";
        public const string TEMPLATE_NAME_PHYCICAL_FACILITIES = "Truong_BCCSVC.xls";
        public const string TEMPLATE_NAME_DSXETDUYETTNTHCS = "HS_XetTotNghiepTHCS.xls";
        public const string TEMPLATE_NAME_PHYCICAL_FACILITIES_PRIMARY = "Truong_BCCSVC_MN.xls";
        public const string TEMPLATE_NAME_PHYCICAL_FACILITIES_THPT_GN = "BieuSLTonghop_THPT_GN.xls";
        public const string TEMPLATE_NAME_PHYCICAL_FACILITIES_THPT_CN = "BieuSLTonghop_THPT_CN.xls";
        public const int REPORT_TEMPLATE_CSVS_ID = 1;
        public const int REPORT_TEMPLATE_CSVS_MN_ID = 2;

        //Bao cao giam sat nhap diem
        public const string REPORT_MARK_INPUT_SITUATION = "BaoCaoTinhHinhNhapDiem";
        //Bao cao danh sach thieu diem
        public const string REPORT_MISSING_MARK = "BaoCaoDanhSachThieuDiem";

        //Bao cao tinh hinh nhap lieu  GVBM
        public const string REPORT_DATA_INPUT_MONITOR_GVBM = "BaoCaoTinhHinhNhapLieuGVBM";
        public const string REPORT_DATA_INPUT_MONITOR_GVCN = "BaoCaoTinhHinhNhapLieuGVCN";
        public const string REPORT_DATA_INPUT_MONITOR_SAEG = "BaoCaoTinhHinhNhapLieuMonHoc";
        public const string REPORT_DATA_INPUT_MONITOR_CQ = "BaoCaoTinhHinhNhapLieuNLPC";


        //Giay chung nhan ket qua hoc tap
        public const string REPORT_LEARNING_RESULT_CERTIFICATE = "GiayChungNhanKetQuaHocTap";

        //thong ke hoc luc phong so
        public const string CAPACITY_STATISTIC_CODE = "ThongKeHocLucPSTheoTruongCap23";
        public const string CAPACITY_STATISTIC_EDUCATION_LEVEL_CODE = "ThongKeHocLucPhongSoTheoKhoiCap23";
        public const string CAPACITY_STATISTIC_DISTRICT_CODE = "ThongKeHocLucPhongSoTheoQuanHuyenCap23";
        public const int CAPACITY_STATISTIC_ID = 1;
        public const int CAPACITY_STATISTIC_EDUCATION_LEVEL_ID = 2;
        public const int CAPACITY_STATISTIC_DISTRICT_ID = 3;

        //thong ke hanh kiem phong so
        public const string CONDUCT_STATISTIC_CODE = "ThongKeHanhKiemPSTheoTruongCap23";
        public const string CONDUCT_STATISTIC_EDUCATION_LEVEL_CODE = "ThongKeHanhKiemPhongSoTheoKhoiCap23";
        public const string CONDUCT_STATISTIC_DISTRICT_CODE = "ThongKeHanhKiemPhongSoTheoQuanHuyenCap23";
        public const int CONDUCT_STATISTIC_ID = 4;
        public const int CONDUCT_STATISTIC_EDUCATION_LEVEL_ID = 5;
        public const int CONDUCT_STATISTIC_DISTRICT_ID = 6;

        public class AdditionReport
        {
            public const int NORMAL = 0; //Bao cao binh thuong
            public const int FEMALE = 1; //Hoc sinh nu
            public const int ETHNIC = 2;// hoc sinh dan toc
            public const int FEMALE_AND_ETHNIC = 3;//Hoc sinh nu dan toc
            public const int DIEM_KIEM_TRA_DINH_KY = 1;
            public const int DIEM_KIEM_TRA_HOC_KY = 2;
            public const int HLM = 3;
            
            /// <summary>
            /// Mau bao cao cho cap MG, NT
            /// </summary>
            public const string REPORT_DATA_INPUT_MONITOR_KINDER_GARTEN = "SGD_NT_ThongKeChiTietTinhHinhNhapLieu";

            /// <summary>
            /// Mau bao cao cho cap TH
            /// </summary>
            public const string REPORT_DATA_INPUT_MONITOR_PRIMARY = "SGD_TH_ThongKeChiTietTinhHinhNhapLieu";

            /// <summary>
            /// Mau bao cao cho cap THCS, THPT
            /// </summary>
            public const string REPORT_DATA_INPUT_MONITOR_TERTIARY = "SGD_THPT_ThongKeChiTietTinhHinhNhapLieu";

            public const string SGD_TKBienDongHS = "SGD_TKBienDongHS";
        };

        // Thống kê tình hình biến động học sinh cho trường
        public const string HS_THCS_TKBiendongHS_112017 = "HS_THCS_TKBiendongHS_112017";
        // Thống kê tình hình biến động học sinh cho phòng - sở
        public const string SGD_TKBienDongHS_112017 = "SGD_TKBienDongHS_112017";

        public const byte PRODUCT_BASIC_VERSION = 1;
        public const short PRODUCT_STANDARD_VERSION = 2;
        public const short PRODUCT_FULL_VERSION = 3;
    }
}
