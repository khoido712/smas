﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Common
{
    public class WorkFlowConstants
    {
        /// <summary>
        /// Nhom cong viec tao du lieu dau nam
        /// </summary>
        public const int GROUP_CTEATED_FIRST_DATA = 1;

        /// <summary>
        /// Nhom cong viec quan ly lop hoc
        /// </summary>
        public const int GROUP_CLASS_MANAGER = 2;

        /// <summary>
        /// Nhom cong viec tinh hinh nhap lieu
        /// </summary>
        public const int GROUP_INPUT_DATA = 3;
        /// <summary>
        /// Nhom cong viec so lieu tong hop
        /// </summary>
        public const int GROUP_GENERAL_DATA = 4;
        /// <summary>
        /// Nhom cong viec bang tin/Thong bao
        /// </summary>
        public const int GROUP_NEWS_MESSAGE = 5;
        /// <summary>
        /// Nhom cong viec thoi khoa bieu
        /// </summary>
        public const int GROUP_SCHEDULE = 6;
        /// <summary>
        /// Nhom cong viec luong nghiep vu chinh
        /// </summary>
        public const int GROUP_WORK_FLOW = 7;
        /// <summary>
        /// Khai bao thong tin 
        /// </summary>
        public const int TAB_DECLARE_INFO = 8;

        /// <summary>
        /// Cap nhat ho so
        /// </summary>
        public const int TAB_UPDATE_PROFILE = 9;

        /// <summary>
        /// Phan cong chuyen mon
        /// </summary>
        public const int TAB_TEACHING_ASSINGMENT = 10;

        /// <summary>
        /// Cau hinh he thong
        /// </summary>
        public const int TAB_SYSTEM_CONFIG = 11;

        /// <summary>
        /// Tab bang tin 
        /// </summary>
        public const int TAB_MAIL_BOX = 12;

        /// <summary>
        /// Tab thong bao
        /// </summary>
        public const int TAB_MESSAGE_BOX = 27;

        /// <summary>
        /// Thoi khoa bieu tuan
        /// </summary>
        public const int TAB_WEEK_CALENDAR = 28;

        /// <summary>
        /// TAB thoi khoa bieu
        /// </summary>
        public const int TAB_SCHEDULE = 13;

        #region Hang so phuc vu cho khai bao du lieu dau nam

        /// <summary>
        /// Thoi gian nam hoc
        /// </summary>
        public const int FUNC_ACADEMIC_YEAR = 14;
        
        /// <summary>
        /// To bo mon
        /// </summary>
        public const int FUNC_SCHOOL_FACULTY = 15;
        
        /// <summary>
        /// Khai bao lop hoc
        /// </summary>
        public const int FUNC_SCHOOL_CLASS = 16;

        /// <summary>
        /// Khai bao mon hoc cho lop
        /// </summary>
        public const int FUNC_CLASS_SUBJECT = 17;
        

        /// <summary>
        /// Khai bao mon hoc cho lop VNEN
        /// </summary>
        public const int FUNC_CLASS_SUBJECT_VNEN = 18;
        

        /// <summary>
        /// Ket chuyen du lieu
        /// </summary>
        public const int FUNC_TRANSFER_DATA = 19;
        

        /// <summary>
        /// Ho so can bo
        /// </summary>
        public const int FUNC_EMPLOYEE_PROFILE = 20;
        
        /// <summary>
        /// Ho so hoc sinh
        /// </summary>
        public const int FUNC_PUPIL_PROFILE = 21;
        

        /// <summary>
        /// Phan cong giang day
        /// </summary>
        public const int FUNC_TEACHING_ASSIGNMENT = 22;
        

        /// <summary>
        /// Phan cong chu nhiem
        /// </summary>
        public const int FUNC_HEAD_TEACHER = 23;
        

        /// <summary>
        /// Cau hinh so con diem
        /// </summary>
        public const int FUNC_MARK_CONFIG = 24;
        

        /// <summary>
        /// Cau hinh so con diem theo dot
        /// </summary>
        public const int FUNC_MARK_PERIOD_CONFIG = 25;
        
        /// <summary>
        /// Cau hinh chung
        /// </summary>
        public const int FUNC_COMMON_CONFIG = 26;
        

        #endregion


        #region Trang thai
        /// <summary>
        /// Đã khai báo
        /// </summary>
        public const string STATUS_DECLARED = "Đã khai báo";

        /// <summary>
        /// Chua khai bao
        /// </summary>
        public const string STATUS_NOT_DECLARE = "<span style='color:red'>Chưa khai báo</span>";

        /// <summary>
        /// Chua khai bao day du
        /// </summary>
        public const string STATUS_NOT_FULL_DECLARE = "<span style='color:orange'>Chưa khai báo đầy đủ</span>";


        /// <summary>
        /// Đã thực hiện
        /// </summary>
        public const string STATUS_EXECUTED = "Đã thực hiện";

        /// <summary>
        /// Chua thuc hien
        /// </summary>
        public const string STATUS_NOT_EXECUTE = "<span style='color:red'>Chưa thực hiện</span>";

        /// <summary>
        /// Chua thuc hien day du
        /// </summary>
        public const string STATUS_NOT_FULL_EXECUTE = "<span style='color:orange'>Chưa thực hiện đầy đủ</span>";

        #endregion

        public const string HTML_SPAN_COLOR_ORANGE = "<span style='color:orange'>";

        public const string HTML_SPAN_COLOR_RED = "<span style='color:red'>";

        public const string HTML_END_SPAN = "</span>";



    }
}
