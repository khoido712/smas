﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IGraduationApprovalRepository : IGenericRepository<GraduationApproval>
    {
    }
}
