/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IVRatedCommentPupilRepository : IGenericRepository<VRatedCommentPupil>
    {
        
    }
}