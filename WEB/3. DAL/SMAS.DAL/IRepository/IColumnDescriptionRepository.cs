﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IColumnDescriptionRepository: IGenericRepository<ColumnDescription>
    {
		
    }
}
