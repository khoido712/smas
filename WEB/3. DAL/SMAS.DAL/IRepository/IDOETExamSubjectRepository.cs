﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.IRepository
{
    public interface IDOETExamSubjectRepository: IGenericRepository<DOETExamSubject>
    {
        
    }
}
