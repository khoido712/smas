/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

using SMAS.Models.Models;


namespace SMAS.DAL.IRepository
{ 
    public interface IGenericRepository <T>: IDisposable  where T : class
    {
		/// <summary>
        /// Gets all objects from database 
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Gets all objects from database with no tracking
        /// </summary>
        IQueryable<T> AllNoTracking { get; }
		
		/// <summary>
        /// Gets all objects from database		
        /// </summary>
		/// <param name="includeProperties">Specified the conditions.</param>
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
		
		/// <summary>
        /// Find object by keys.
        /// </summary>
        /// <param name="id">Specified the search keys.</param>
        T Find(object id);		

		/// <summary>
        /// Insert object to database.
        /// </summary>
        /// <param name="entity">Specified the object to save.</param>
        void Insert(T entityToInsert);
		
		/// <summary>
        /// Update object changes and save to database.
        /// </summary>
        /// <param name="entity">Specified the object to save.</param>
        void Update(T entityToUpdate);


        
		/// <summary>
        /// Delete the object from database.
        /// </summary>
        /// <param name="id">Specified a existing object to delete.</param>
        void Delete(object id);

        /// <summary>
        /// Delete the object from database.
        /// </summary>
        /// <param name="id">Specified a existing object to delete.</param>
        void Delete(T entityToDelete);

        /// <summary>
        /// Delete list object from database.
        /// </summary>
        /// <param name="id">Specified a existing object to delete.</param>
        void DeleteAll(List<T> entities);
		
		/// <summary>
        /// Save change to database.
        /// </summary>
        /// <param name="t">Specified the object to save.</param>
        void Save();
        void SaveDetect();

        

        /// <summary>
        /// Check trung
        /// </summary>
        /// <param name="valueToCheck"></param>
        /// <param name="schemaName"></param>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <param name="isActive">true: chi check nhung ban ghi co isActive = 1; false: check tat ca</param>
        /// <param name="currentId"></param>
        /// <returns></returns>
        bool CheckDuplicate(string valueToCheck, string schemaName, string tableName, string columnName, bool? isActive, int? currentId);
        bool CheckDuplicateCouple(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive, int? currentId);
        bool CheckConstraints(string schemaName, string tableName, int? id);
        bool CheckConstraintsWithIsActive(string schemaName, string tableName, int? id);
        bool ExistsRow(string SchemaName, string TableName, IDictionary<string, object> SearchInfo, IDictionary<string, object> ExceptInfo = null);
        bool CheckConstraintsWithExceptTable(string schemaName, string tableName, string ExceptedTableName, int? id);

        /// <summary>
        /// Lay gia tri tiep theo cua SEQ. SeqName duoc dat theo cau truc: TABLE_SEQ
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        TResult GetNextSeq<TResult>();

        /// <summary>
        /// Lay gia ti tiep theo cua SEQ theo SeqName
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="seqName"></param>
        /// <returns></returns>
        TResult GetNextSeq<TResult>(string seqName);

        /// <summary>
        /// Cap nhat gia tri khi doi tuong da ton tai trong ObjectStateManager
        /// </summary>
        /// <param name="entityToUpdate"></param>
        void UpdateExistsObjectStateManager(T entityToUpdate);
        /// <summary>
        /// Chiendd1: Thuc hien insert du lieu tu danh sach, su dung ArrayBindCount,
        ///  1: Insert thanh cong, 0: khong thanh cong
        /// </summary>
        /// <param name="lstInsert"></param>
        /// <returns></returns>
        int BulkInsert(List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn);

        int BulkInsert(string stringConnection, List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn);

        /// <summary>
        /// Thuc hien insert va xoa du lieu tu store
        /// </summary>
        /// <param name="lstInsert"></param>
        /// <param name="columnMappings"></param>
        /// <param name="sqlProcedure"></param>
        /// <param name="lstParaDel"></param>
        /// <param name="lstData"></param>
        /// <param name="remvedcolumn"></param>
        /// <returns></returns>
        int BulkInsertAndDelete(List<T> lstInsert, IDictionary<string, object> columnMappings, string sqlProcedure, List<CustomType> lstParaDelete, params string[] remvedcolumn);

        /// <summary>
        /// set isAutoDetectChangesEnabled=false de tang hieu nang khi thuc hien them moi, xoa,sua  
        /// </summary>
        /// <param name="isAutoDetectChangesEnabled"></param>
        void SetAutoDetectChangesEnabled(bool isAutoDetectChangesEnabled);

        bool ExcuteSql(List<string> lstSql);

    }
}



