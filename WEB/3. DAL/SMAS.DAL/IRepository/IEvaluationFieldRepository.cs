﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IEvaluationFieldRepository: IGenericRepository<EvaluationField>
    {
		
    }
}
