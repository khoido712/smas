﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IDOETExamSupervisoryRepository: IGenericRepository<DOETExamSupervisory>
    {
        
    }
}
