﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IEvaluationLevelRepository: IGenericRepository<EvaluationLevel>
    {
		
    }
}
