﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IForeignLanguageCatRepository: IGenericRepository<ForeignLanguageCat>
    {
    }
}
