﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IEmployeeEvaluationRepository: IGenericRepository<EmployeeEvaluation>
    {
		
    }
}
