﻿using SMAS.Models.Models;

namespace SMAS.DAL.IRepository
{
    public interface IProfileTemplateRepository: IGenericRepository<ProfileTemplate>
    {
		
    }
}
