/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{ 
    public class aspnet_UsersRepository : GenericRepository<aspnet_Users>, Iaspnet_UsersRepository
    {        
		public aspnet_UsersRepository() : base()
        {
        }
        public aspnet_UsersRepository(SMASEntities context) : base(context)
        {
        }
    }
}