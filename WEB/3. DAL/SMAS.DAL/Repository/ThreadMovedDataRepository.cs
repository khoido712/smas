﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class ThreadMovedDataRepository : GenericRepository<ThreadMovedData>, IThreadMovedDataRepository
    {
        public ThreadMovedDataRepository() : base()
        {
        }
        public ThreadMovedDataRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
