/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{ 
    public class StaffPositionRepository : GenericRepository<StaffPosition>, IStaffPositionRepository
    {        
		public StaffPositionRepository() : base()
        {
        }
        public StaffPositionRepository(SMASEntities context) : base(context)
        {
        }
    }
}