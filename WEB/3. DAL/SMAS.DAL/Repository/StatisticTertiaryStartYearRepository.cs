﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class StatisticTertiaryStartYearRepository : GenericRepository<StatisticTertiaryStartYear>, IStatisticTertiaryStartYearRepository
    {        
		public StatisticTertiaryStartYearRepository() : base()
        {
        }
        public StatisticTertiaryStartYearRepository(SMASEntities context) : base(context)
        {
        }
    }
}