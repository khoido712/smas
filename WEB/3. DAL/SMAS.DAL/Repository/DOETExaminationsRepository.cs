﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;

namespace SMAS.DAL.Repository
{
    public class DOETExaminationsRepository : GenericRepository<DOETExaminations>, IDOETExaminationsRepository
    {
        public DOETExaminationsRepository() : base()
        {
        }
        public DOETExaminationsRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
