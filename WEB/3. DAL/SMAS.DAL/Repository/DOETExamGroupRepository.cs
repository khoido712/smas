﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class DOETExamGroupRepository : GenericRepository<DOETExamGroup>, IDOETExamGroupRepository
    {
        public DOETExamGroupRepository()
            : base()
        {
        }
        public DOETExamGroupRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
