/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{ 
    public class ServicePackageDeclareRepository : GenericRepository<SMS_SERVICE_PACKAGE_DECLARE>, IServicePackageDeclareRepository
    {        
		public ServicePackageDeclareRepository() : base()
        {
        }
        public ServicePackageDeclareRepository(SMASEntities context) : base(context)
        {
        }
    }
}