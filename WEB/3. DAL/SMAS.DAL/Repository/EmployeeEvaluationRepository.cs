/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class EmployeeEvaluationRepository : GenericRepository<EmployeeEvaluation>, IEmployeeEvaluationRepository
    {        
		public EmployeeEvaluationRepository() : base()
        {
        }
        public EmployeeEvaluationRepository(SMASEntities context) : base(context)
        {
        }
    }
}