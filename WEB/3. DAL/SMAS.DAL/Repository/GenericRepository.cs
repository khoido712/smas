﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;

using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System.Reflection;
using System.Data.Entity.Validation;
using Oracle.DataAccess.Client;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using SMAS.VTUtils.Log;

namespace SMAS.DAL.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        internal SMASEntities Context;
        internal DbSet<T> DbSet;
        public string connectionString;
        public GenericRepository()
        {
            Context = new SMASEntities();
            DbSet = Context.Set<T>();
            if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
        }

        public GenericRepository(SMASEntities context)
        {
            Context = context;
            DbSet = Context.Set<T>();
            if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
        }

        public IQueryable<T> All
        {
            get { return DbSet; }
        }

        public IQueryable<T> AllNoTracking
        {
            get { return DbSet.AsNoTracking(); }
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = DbSet;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public T Find(object id)
        {
            return DbSet.Find(id);
        }

        public void Insert(T entityToInsert)
        {
            DbSet.Add(entityToInsert);
        }

        public void Update(T entityToUpdate)
        {
            DbSet.Attach(entityToUpdate);
            Context.Entry(entityToUpdate).State = EntityState.Modified;

        }
        public void UpdateExistsObjectStateManager(T entityToUpdate)
        {
            var entry = this.Context.Entry(entityToUpdate);
            var objectContext = ((IObjectContextAdapter)Context).ObjectContext;
            var key = this.GetPrimaryKey(objectContext, entry);
            var currentEntry = this.DbSet.Find(key);
            if (entry.State == EntityState.Detached)
            {
                if (currentEntry != null)
                {
                    var attachedEntry = this.Context.Entry(currentEntry);
                    attachedEntry.CurrentValues.SetValues(entityToUpdate);
                }
                else
                {
                    this.DbSet.Attach(entityToUpdate);
                    entry.State = EntityState.Modified;
                }
            }


        }
        private int GetPrimaryKey(ObjectContext objectContext, DbEntityEntry entry)
        {
            var objectStateEntry = objectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            object keyValue = objectStateEntry.EntityKey.EntityKeyValues[0].Value;
            return (keyValue == null) ? 0 : (int)keyValue;
        }

        public void Delete(T entityToDelete)
        {
            if (Context.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }


        public void Delete(object id)
        {
            T entityToDelete = DbSet.Find(id);
            this.Delete(entityToDelete);
        }


        public void DeleteAll(List<T> entities)
        {
            foreach (T entity in entities)
            {
                this.Delete(entity);
            }
        }

        public void Save()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                string error = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("{0}:{1}; ", ve.PropertyName, ve.ErrorMessage);
                    }

                }

                throw new Exception(e.Message);
            }
        }

        public void SaveDetect()
        {
            try
            {
                var detachContext = ((IObjectContextAdapter)Context).ObjectContext;
                detachContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);
            }
            catch (DbEntityValidationException e)
            {
                string error = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("{0}:{1}; ", ve.PropertyName, ve.ErrorMessage);
                    }

                }

                throw new Exception(e.StackTrace);
            }
        }

        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
            }
        }

        public bool CheckDuplicate(string valueToCheck, string schemaName, string tableName, string columnName, bool? isActive, int? currentId)
        {
            int? num = Context.CheckDuplicate(valueToCheck, schemaName, tableName, columnName, isActive, currentId).First();
            return num != null && num > 0;
        }

        public bool CheckDuplicateCouple(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive, int? currentId)
        {
            int? num = Context.CheckDuplicateCouple(valueToCheck1, valueToCheck2, schemaName, tableName, columnName1, columnName2, isActive, currentId).First();
            return num != null && num > 0;
        }

        public bool CheckConstraints(string schemaName, string tableName, int? id)
        {
            int? num = Context.CheckConstraints(schemaName, tableName, id).First();
            return num != null && num > 0;
        }
        public bool CheckConstraintsWithIsActive(string schemaName, string tableName, int? id)
        {
            int? num = SMASUtils.CheckConstraintsWithIsActive(Context, schemaName, tableName, id).First();
            return num != null && num > 0;
        }
        public bool CheckConstraintsWithExceptTable(string schemaName, string tableName, string ExceptedTableName, int? id)
        {
            if (ExceptedTableName != null || ExceptedTableName != "")
            {
                ExceptedTableName = ExceptedTableName.Trim();
            }
            ExceptedTableName = "," + ExceptedTableName + ",";
            int? num = Context.CheckConstraintsWithExceptTable(schemaName, tableName, ExceptedTableName, id).First();
            return num != null && num > 0;
        }
        public bool CheckAvailable(int? id, bool hasIsActive = true)
        {
            if (id == null)
                return true;
            bool isAvailable = true;
            T o = this.Find(id);
            if (o == null)
            {
                isAvailable = false;
            }
            else if (hasIsActive)
            {
                Type t = o.GetType();
                PropertyInfo p = t.GetProperty("IsActive");
                if (p != null)
                {
                    bool isA = (bool)p.GetValue(o, null);
                    isAvailable = isA;
                }
            }

            return isAvailable;
        }

        /// <summary>
        /// Kiểm tra có tồn tại một bản ghi nào thỏa mãn điều kiện tìm kiếm  hay không.
        /// Phục vụ cho việc check trùng (Check duplicate), kiểm tra tính hợp lệ của dữ liệu (check compatiable)
        /// </summary>
        /// <param name="SchemaName"></param>
        /// <param name="TableName"></param>
        /// <param name="SearchInfo">Các tiêu chí sẽ tìm kiếm với điều kiện 'bằng'</param>
        /// <param name="ExceptInfo">Các tiêu chí sẽ tìm kiếm với điệu kiện 'khác'</param>
        /// <returns>true: nếu tồn tại bản ghi,. false: Ngược lại</returns>
        public bool ExistsRow(string SchemaName, string TableName, IDictionary<string, object> SearchInfo, IDictionary<string, object> ExceptInfo = null)
        {
            string SQL = "SELECT count(*) FROM " + SMASUtils.ConvertToOracle(TableName)
                       + " WHERE ";

            List<OracleParameter> parametes = new List<OracleParameter>();
            int i = 0;
            foreach (string s in SearchInfo.Keys)
            {
                if (SearchInfo[s] == null)
                {
                    SQL += i == 0 ?
                      SMASUtils.ConvertToOracle(s) + " is null " :
                      " AND " + SMASUtils.ConvertToOracle(s) + " is null";
                    i++;
                }
                else
                {
                    object val;
                    if (SearchInfo[s].ToString().ToLower() == "true")
                    {
                        val = 1;
                    }
                    else if (SearchInfo[s].ToString().ToLower() == "false")
                    {
                        val = 0;
                    }
                    else
                    {
                        val = SearchInfo[s];
                    }
                    parametes.Add(new OracleParameter("p" + i, val));
                    SQL += i == 0 ?
                           SMASUtils.ConvertToOracle(s) + " = :p" + i :
                           " AND " + SMASUtils.ConvertToOracle(s) + " = :p" + i;
                    i++;
                }
            }

            if (ExceptInfo != null)
            {
                foreach (string s in ExceptInfo.Keys)
                {
                    if (ExceptInfo[s] == null)
                    {
                        SQL += i == 0 ?
                          SMASUtils.ConvertToOracle(s) + " is not null " :
                          " AND " + SMASUtils.ConvertToOracle(s) + " is not null";
                        i++;
                    }
                    else
                    {
                        object val;
                        if (ExceptInfo[s].ToString().ToLower() == "true")
                        {
                            val = 1;
                        }
                        else if (ExceptInfo[s].ToString().ToLower() == "false")
                        {
                            val = 0;
                        }
                        else
                        {
                            val = ExceptInfo[s];
                        }
                        parametes.Add(new OracleParameter("p" + i, val));
                        SQL += i == 0 ?
                               SMASUtils.ConvertToOracle(s) + " != :p" + i :
                               " AND " + SMASUtils.ConvertToOracle(s) + " != :p" + i;
                        i++;
                    }
                }
            }

            ObjectResult<int> res = ((IObjectContextAdapter)Context).ObjectContext.ExecuteStoreQuery<int>(SQL, parametes.ToArray());
            int count = res.First();

            return count > 0;
        }

        public TResult GetNextSeq<TResult>()
        {
            string table = SMASUtils.ConvertToOracle(typeof(T).Name);
            string seq = table.ToUpper() + "_SEQ";
            string nextSeq = String.Format("SELECT {0}.NEXTVAL from DUAL", seq);
            TResult value = Context.Database.SqlQuery<TResult>(nextSeq).FirstOrDefault();
            return value;
        }

        public TResult GetNextSeq<TResult>(string seqName)
        {
            if (String.IsNullOrEmpty(seqName))
            {
                throw new Exception("Khong tim thay sequence");
            }
            string nextSeq = String.Format("SELECT {0}.NEXTVAL from DUAL", seqName.ToUpper());
            TResult value = Context.Database.SqlQuery<TResult>(nextSeq).FirstOrDefault();
            return value;
        }

        public int BulkInsert(List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn)
        {
            if (lstInsert == null || lstInsert.Count <= 0)
            {
                return 0;
            }
            List<OracleDbType> ListType = new List<OracleDbType>();
            List<List<object>> ListParam = new List<List<object>>();
            //PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            var props = TypeDescriptor.GetProperties(typeof(T))
                                               .Cast<PropertyDescriptor>()
                                               .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                               .ToArray();

            StringBuilder query = new StringBuilder();
            query.Append("INSERT INTO " + GetTableName() + "(");
            List<object> param;
            List<string> lstColumn = new List<string>();
            for (int i = 0; i < props.Count(); i++)
            {

                PropertyDescriptor prop = props[i];
                if (remvedcolumn != null && remvedcolumn.Count() > 0 && remvedcolumn.Contains(prop.Name))
                {
                    continue;
                }
                if (prop.PropertyType.FullName.Contains("SMAS.Models.Models"))
                {
                    continue;
                }

                //query.Append(columnMappings[prop.Name].ToString() + " ,");
                lstColumn.Add(columnMappings[prop.Name].ToString());
                param = new List<object>();
                if (prop.PropertyType.FullName.Contains("System.Int16"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int32"))
                {
                    ListType.Add(OracleDbType.Int32);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int64"))
                {
                    ListType.Add(OracleDbType.Int64);
                }
                else if (prop.PropertyType.FullName.Contains("System.String"))
                {
                    ListType.Add(OracleDbType.NVarchar2);
                }
                else if (prop.PropertyType.FullName.Contains("System.DateTime"))
                {
                    ListType.Add(OracleDbType.Date);
                }
                else if (prop.PropertyType.FullName.Contains("System.Guid"))
                {
                    ListType.Add(OracleDbType.Raw);
                }
                else if (prop.PropertyType.FullName.Contains("System.Double"))
                {
                    ListType.Add(OracleDbType.Double);
                }
                else if (prop.PropertyType.FullName.Contains("System.Byte"))
                {
                    ListType.Add(OracleDbType.Byte);
                }
                else if (prop.PropertyType.FullName.Contains("System.Decimal"))
                {
                    ListType.Add(OracleDbType.Decimal);
                }
                else if (prop.PropertyType.FullName.Contains("System.Boolean"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                foreach (var a in lstInsert)
                {
                    param.Add(props[i].GetValue(a));
                }
                ListParam.Add(param);


            }

            //query.Remove(query.Length - 1, 1);
            //query.Append(") values (");
            String columns = String.Join(",", lstColumn.ToArray());
            query.Append(columns);
            query.Append(") values (");
            if (ListType == null || ListType.Count == 0)
            {
                return 0;
            }
            List<String> lstParaInsert = new List<string>();
            for (int j = 1; j < ListType.Count + 1; j++)
            {
                //query.Append(":" + j + " ,");
                lstParaInsert.Add(":" + j);
            }

            query.Append(String.Join(",", lstParaInsert.ToArray()));
            //query.Remove(query.Length - 1, 1);
            query.Append(")");

            if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
            int retVal = 1;
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                string sql = SafeReplace(query.ToString(), "COMMENT", "\"COMMENT\"", true);
                                sql = SafeReplace(sql.ToString(), "YEAR", "\"YEAR\"", true);
                                retVal = DirectPushToDatabase(sql, ListType, ListParam, conn);
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                retVal = 0;
                                tran.Rollback();
                                throw new Exception(ex.StackTrace);
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    retVal = 0;
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return retVal;

        }

        public int BulkInsertAndDelete(List<T> lstInsert, IDictionary<string, object> columnMappings, string sqlProcedure, List<CustomType> lstParaDelete, params string[] remvedcolumn)
        {
            StringBuilder query = new StringBuilder();
            List<OracleDbType> ListType = new List<OracleDbType>();
            List<List<object>> ListParam = new List<List<object>>();
            CreateQueryBulk(lstInsert, columnMappings, out query, out ListType, out ListParam, remvedcolumn);
            int retVal = 1;
            if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                Task task1 = Task.Factory.StartNew(() =>
                                {
                                    if (lstInsert.Count > 0 && ListType.Count > 0 && ListParam.Count > 0)
                                    {
                                        string sql = SafeReplace(query.ToString(), "COMMENT", "\"COMMENT\"", true);
                                        sql = SafeReplace(sql.ToString(), "YEAR", "\"YEAR\"", true);
                                        retVal = DirectPushToDatabase(sql, ListType, ListParam, conn);
                                    }
                                });
                                Task task2 = Task.Factory.StartNew(() =>
                                {
                                    if (lstParaDelete != null)
                                    {
                                        retVal += ExcuteStoreProcedure(conn, sqlProcedure, lstParaDelete);
                                    }

                                });

                                Task.WaitAll(task1, task2);

                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                retVal = 0;
                                tran.Rollback();
                                throw new Exception(ex.StackTrace);
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    retVal = 0;
                    throw new Exception(ex.StackTrace);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return retVal;
        }


        public int BulkInsert(string stringConnection, List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn)
        {
            if (lstInsert == null || lstInsert.Count <= 0)
            {
                return 0;
            }
            List<OracleDbType> ListType = new List<OracleDbType>();
            List<List<object>> ListParam = new List<List<object>>();
            //PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            var props = TypeDescriptor.GetProperties(typeof(T))
                                               .Cast<PropertyDescriptor>()
                                               .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                               .ToArray();

            StringBuilder query = new StringBuilder();
            string tableName = GetTableName();
            query.Append("INSERT INTO " + tableName + "(");
            List<object> param;
            List<string> lstColumn = new List<string>();
            for (int i = 0; i < props.Count(); i++)
            {
                PropertyDescriptor prop = props[i];
                if (remvedcolumn != null && remvedcolumn.Count() > 0 && remvedcolumn.Contains(prop.Name))
                {
                    continue;
                }
                if (prop.PropertyType.FullName.Contains("SMAS.Models.Models"))
                {
                    continue;
                }

                //query.Append(columnMappings[prop.Name].ToString() + " ,");
                lstColumn.Add(columnMappings[prop.Name].ToString());
                param = new List<object>();
                if (prop.PropertyType.FullName.Contains("System.Int16"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int32"))
                {
                    ListType.Add(OracleDbType.Int32);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int64"))
                {
                    ListType.Add(OracleDbType.Int64);
                }
                else if (prop.PropertyType.FullName.Contains("System.String"))
                {
                    ListType.Add(OracleDbType.NVarchar2);
                }
                else if (prop.PropertyType.FullName.Contains("System.DateTime"))
                {
                    ListType.Add(OracleDbType.Date);
                }
                else if (prop.PropertyType.FullName.Contains("System.Guid"))
                {
                    ListType.Add(OracleDbType.Raw);
                }
                else if (prop.PropertyType.FullName.Contains("System.Double"))
                {
                    ListType.Add(OracleDbType.Double);
                }
                else if (prop.PropertyType.FullName.Contains("System.Byte"))
                {
                    ListType.Add(OracleDbType.Byte);
                }
                else if (prop.PropertyType.FullName.Contains("System.Decimal"))
                {
                    ListType.Add(OracleDbType.Decimal);
                }
                else if (prop.PropertyType.FullName.Contains("System.Boolean"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                foreach (var a in lstInsert)
                {
                    param.Add(props[i].GetValue(a));
                }
                ListParam.Add(param);
            }

            //query.Remove(query.Length - 1, 1);
            //query.Append(") values (");
            String columns = String.Join(",", lstColumn.ToArray());
            query.Append(columns);
            query.Append(") values (");
            if (ListType == null || ListType.Count == 0)
            {
                return 0;
            }
            List<String> lstParaInsert = new List<string>();
            for (int j = 1; j < ListType.Count + 1; j++)
            {
                //query.Append(":" + j + " ,");
                lstParaInsert.Add(":" + j);
            }

            query.Append(String.Join(",", lstParaInsert.ToArray()));
            //query.Remove(query.Length - 1, 1);
            query.Append(")");

            int retVal = 1;
            using (OracleConnection conn = new OracleConnection(stringConnection))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                string sql = SafeReplace(query.ToString(), "COMMENT", "\"COMMENT\"", true);
                                sql = SafeReplace(sql.ToString(), "YEAR", "\"YEAR\"", true);
                                retVal = DirectPushToDatabase(sql, ListType, ListParam, conn);
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                retVal = 0;
                                tran.Rollback();
                                throw new Exception(ex.Message);
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    retVal = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return retVal;
        }


        private void CreateQueryBulk(List<T> lstInsert, IDictionary<string, object> columnMappings, out StringBuilder query, out List<OracleDbType> ListType, out List<List<object>> ListParam, params string[] remvedcolumn)
        {
            if (lstInsert == null || lstInsert.Count <= 0)
            {
                query = new StringBuilder();
                ListType = new List<OracleDbType>();
                ListParam = new List<List<object>>();
                return;
            }

            ListType = new List<OracleDbType>();
            ListParam = new List<List<object>>();
            var props = TypeDescriptor.GetProperties(typeof(T))
                                               .Cast<PropertyDescriptor>()
                                               .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                               .ToArray();

            query = new StringBuilder();
            query.Append("INSERT INTO " + GetTableName() + "(");
            List<object> param;
            List<string> lstColumn = new List<string>();
            for (int i = 0; i < props.Count(); i++)
            {
                PropertyDescriptor prop = props[i];
                if (remvedcolumn != null && remvedcolumn.Count() > 0 && remvedcolumn.Contains(prop.Name))
                {
                    continue;
                }
                if (prop.PropertyType.FullName.Contains("SMAS.Models.Models"))
                {
                    continue;
                }

                //query.Append(columnMappings[prop.Name].ToString() + " ,");
                lstColumn.Add(columnMappings[prop.Name].ToString());
                param = new List<object>();
                if (prop.PropertyType.FullName.Contains("System.Int16"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int32"))
                {
                    ListType.Add(OracleDbType.Int32);
                }
                else if (prop.PropertyType.FullName.Contains("System.Int64"))
                {
                    ListType.Add(OracleDbType.Int64);
                }
                else if (prop.PropertyType.FullName.Contains("System.String"))
                {
                    ListType.Add(OracleDbType.NVarchar2);
                }
                else if (prop.PropertyType.FullName.Contains("System.DateTime"))
                {
                    ListType.Add(OracleDbType.Date);
                }
                else if (prop.PropertyType.FullName.Contains("System.Guid"))
                {
                    ListType.Add(OracleDbType.Raw);
                }
                else if (prop.PropertyType.FullName.Contains("System.Double"))
                {
                    ListType.Add(OracleDbType.Double);
                }
                else if (prop.PropertyType.FullName.Contains("System.Byte"))
                {
                    ListType.Add(OracleDbType.Byte);
                }
                else if (prop.PropertyType.FullName.Contains("System.Decimal"))
                {
                    ListType.Add(OracleDbType.Decimal);
                }
                else if (prop.PropertyType.FullName.Contains("System.Boolean"))
                {
                    ListType.Add(OracleDbType.Int16);
                }
                foreach (var a in lstInsert)
                {
                    param.Add(props[i].GetValue(a));
                }
                ListParam.Add(param);
            }

            string columns = string.Join(",", lstColumn.ToArray());
            query.Append(columns);
            query.Append(") values (");
            if (ListType == null || ListType.Count == 0)
            {
                return;
            }
            List<String> lstParaInsert = new List<string>();
            for (int j = 1; j < ListType.Count + 1; j++)
            {
                lstParaInsert.Add(":" + j);
            }

            query.Append(String.Join(",", lstParaInsert.ToArray()));
            query.Append(")");
        }
        private int DirectPushToDatabase(string Query, List<OracleDbType> ListType, List<List<object>> ListData, OracleConnection Conn)
        {
            int Count = ListData[0].Count();
            if (Count == 0)
            {
                return 0;
            }
            DateTime t = DateTime.Now;
            OracleCommand cmd = new OracleCommand(Query, Conn);
            cmd.CommandTimeout = 0;
            for (int i = 0; i < ListType.Count; i++)
            {
                OracleParameter param = new OracleParameter();
                param.OracleDbType = ListType[i];
                param.Value = ListData[i].ToArray();
                cmd.Parameters.Add(param);
            }
            cmd.ArrayBindCount = Count;
            int retval = cmd.ExecuteNonQuery();
            return retval;
        }

        private int ExcuteStoreProcedure(OracleConnection Conn, string sqlProcedure, List<CustomType> lstPara)
        {
            int retVal = 0;
            CustomType cType = lstPara.FirstOrDefault(c => c.IsListParaValue);
            if (cType != null)
            {
                #region thuc hien excute store co du lieu vao la  list
                List<string> lstDataDelete = (cType.ListValue == null) ? new List<string>() : cType.ListValue;
                List<List<string>> lstVal = SplitListToMultipleList(lstDataDelete, 200);
                for (int i = 0; i < lstVal.Count; i++)
                {
                    OracleCommand command = null;
                    try
                    {
                        command = new OracleCommand
                        {
                            CommandType = CommandType.StoredProcedure,
                            CommandText = sqlProcedure,
                            Connection = Conn,
                            CommandTimeout = 0,
                        };
                        for (int k = 0; k < lstPara.Count; k++)
                        {
                            if (!lstPara[k].IsListParaValue)
                            {
                                command.Parameters.Add(lstPara[k].ParaName, lstPara[k].ParaValue);
                            }
                            else
                            {
                                string strId = string.Join(",", lstVal[i]);
                                command.Parameters.Add(lstPara[k].ParaName, strId);
                            }

                        }
                        retVal += command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (command != null)
                        {
                            command.Dispose();
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Thuc hien excute gia tri trong store khong truyen list
                OracleCommand command = null;
                try
                {
                    command = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = sqlProcedure,
                        Connection = Conn,
                        CommandTimeout = 0
                    };
                    for (int i = 0; i < lstPara.Count; i++)
                    {
                        command.Parameters.Add(lstPara[i].ParaName, lstPara[i].ParaValue);
                    }
                    retVal += command.ExecuteNonQuery();
                }
                finally
                {
                    if (command != null)
                    {
                        command.Dispose();
                    }
                }
                #endregion
            }

            return retVal;


        }


        private string GetTableName()
        {
            ObjectContext objectContext = ((IObjectContextAdapter)Context).ObjectContext;
            string sql = objectContext.CreateObjectSet<T>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) ");
            Match match = regex.Match(sql);
            string tableName = match.Groups["table"].Value;
            return tableName;
        }






        private string SafeReplace(string input, string find, string replace, bool matchWholeWord)
        {
            string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
            return Regex.Replace(input, textToFind, replace);
        }

        public void SetAutoDetectChangesEnabled(bool isAutoDetectChangesEnabled)
        {
            Context.Configuration.ValidateOnSaveEnabled = isAutoDetectChangesEnabled;
            Context.Configuration.AutoDetectChangesEnabled = isAutoDetectChangesEnabled;
        }

        private List<List<T>> SplitListToMultipleList<T>(List<T> locations, int nSize = 500)
        {
            var list = new List<List<T>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }

        public bool ExcuteSql(List<string> lstSql)
        {
            if (lstSql.Count == 0)
            {
                return false;
            }
            if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
            try
            {
                using (OracleConnection conn = new OracleConnection(connectionString))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand cmd = null;
                                foreach (string sql in lstSql)
                                {
                                    cmd = new OracleCommand(sql, conn);
                                    //LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, "SQL=" + sql, "Cau lenh thuc hien", "Null");
                                    cmd.CommandTimeout = 0;
                                    cmd.ExecuteNonQuery();
                                }

                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                tran.Rollback();

                                string para = "null";
                                //LogExtensions.ErrorExt(logger, DateTime.Now, para, ex);
                                return false;
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        string para = "null";
                        //LogExtensions.ErrorExt(logger, DateTime.Now, para, ex);
                        return false;
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                string para = "null";
                //LogExtensions.ErrorExt(logger, DateTime.Now, para, ex);
                return false;
            }
            finally
            {
                //SetAutoDetectChangesEnabled(context, true);
            }
        }


    }
}