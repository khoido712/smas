﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class RestoreDataRepository : GenericRepository<RESTORE_DATA>, IRestoreDataRepository
    {        
		public RestoreDataRepository() : base()
        {
        }
        public RestoreDataRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
