﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class StatisticLevelConfigRepository : GenericRepository<StatisticLevelConfig>, IStatisticLevelConfigRepository
    {
        public StatisticLevelConfigRepository() : base()
        {
        }
        public StatisticLevelConfigRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
