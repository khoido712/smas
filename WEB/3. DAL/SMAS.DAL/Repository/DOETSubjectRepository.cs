﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class DOETSubjectRepository : GenericRepository<DOETSubject>, IDOETSubjectRepository
    {
        public DOETSubjectRepository()
            : base()
        {
        }
        public DOETSubjectRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
