﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class LoginMobileRepository : GenericRepository<LoginMobile>, ILoginMobileRepository
    {
        public LoginMobileRepository() : base()
        {
        }
        public LoginMobileRepository(SMASEntities context) : base(context)
        {
        }
    }
}
