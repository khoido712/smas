﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class LockRatedCommentPupilRepository : GenericRepository<LockRatedCommentPupil>, ILockRatedCommentPupilRepository
    {        
		public LockRatedCommentPupilRepository() : base()
        {
        }
        public LockRatedCommentPupilRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}