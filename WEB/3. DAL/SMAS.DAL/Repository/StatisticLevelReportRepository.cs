﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class StatisticLevelReportRepository : GenericRepository<StatisticLevelReport>, IStatisticLevelReportRepository
    {
        public StatisticLevelReportRepository() : base()
        {
        }
        public StatisticLevelReportRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
