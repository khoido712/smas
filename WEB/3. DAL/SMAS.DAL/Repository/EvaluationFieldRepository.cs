/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class EvaluationFieldRepository : GenericRepository<EvaluationField>, IEvaluationFieldRepository
    {        
		public EvaluationFieldRepository() : base()
        {
        }
        public EvaluationFieldRepository(SMASEntities context) : base(context)
        {
        }
    }
}