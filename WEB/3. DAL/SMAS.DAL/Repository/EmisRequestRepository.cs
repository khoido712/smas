﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;

namespace SMAS.DAL.Repository
{
   public class EmisRequestRepository : GenericRepository<EmisRequest>, IEmisRequestRepository
    {
       public EmisRequestRepository() : base()
        {
        }
       public EmisRequestRepository(SMASEntities context)
           : base(context)
        {
        }
    }
}
