﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class ExamDetachableBagRepository : GenericRepository<ExamDetachableBag>, IExamDetachableBagRepository
    {
        public ExamDetachableBagRepository() : base()
        {
        }
        public ExamDetachableBagRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
