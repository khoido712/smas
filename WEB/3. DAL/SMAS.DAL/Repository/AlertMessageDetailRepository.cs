﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class AlertMessageDetailRepository : GenericRepository<AlertMessageDetail>, IAlertMessageDetailRepository
    {
        public AlertMessageDetailRepository()
            : base()
        {
        }
        public AlertMessageDetailRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
