﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class ThreadMarkRepository : GenericRepository<ThreadMark>, IThreadMarkRepository
    {        
		public ThreadMarkRepository() : base()
        {
        }
        public ThreadMarkRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
