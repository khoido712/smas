/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{ 
    public class EWalletTransactionRepository : GenericRepository<EW_EWALLET_TRANSACTION>, IEWalletTransactionRepository
    {        
		public EWalletTransactionRepository() : base()
        {
        }
        public EWalletTransactionRepository(SMASEntities context) : base(context)
        {
        }
    }
}