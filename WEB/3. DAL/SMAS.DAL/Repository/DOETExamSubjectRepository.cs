﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.DAL.Repository
{
    public class DOETExamSubjectRepository : GenericRepository<DOETExamSubject>, IDOETExamSubjectRepository
    {
        public DOETExamSubjectRepository()
            : base()
        {
        }
        public DOETExamSubjectRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
