/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{ 
    public class ProcessedReportParameterRepository : GenericRepository<ProcessedReportParameter>, IProcessedReportParameterRepository
    {        
		public ProcessedReportParameterRepository() : base()
        {
        }
        public ProcessedReportParameterRepository(SMASEntities context) : base(context)
        {
        }

        public void Insert(IDictionary<string, object> dic, ProcessedReport processedReport)
        {
            foreach (string key in dic.Keys)
            {
                // Neu du lieu truyen vao khong co thi bo qua
                if (dic[key] == null)
                {
                    continue;
                }
                ProcessedReportParameter prp = new ProcessedReportParameter();
                prp.ParameterName = key;
                prp.ParameterValue = dic[key].ToString();
                prp.ProcessedReport = processedReport;
                processedReport.ProcessedReportParameters.Add(prp);
            }    
        }
    }
}