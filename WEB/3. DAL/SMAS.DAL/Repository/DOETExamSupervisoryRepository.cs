﻿using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class DOETExamSupervisoryRepository : GenericRepository<DOETExamSupervisory>, IDOETExamSupervisoryRepository
    {
        public DOETExamSupervisoryRepository()
            : base()
        {
        }
        public DOETExamSupervisoryRepository(SMASEntities context)
            : base(context)
        {
        }
    }
}
