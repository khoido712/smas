/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.DAL.IRepository;
using SMAS.Models.Models;

namespace SMAS.DAL.Repository
{
    public class EvaluationLevelRepository : GenericRepository<EvaluationLevel>, IEvaluationLevelRepository
    {        
		public EvaluationLevelRepository() : base()
        {
        }
        public EvaluationLevelRepository(SMASEntities context) : base(context)
        {
        }
    }
}