﻿using Aspose.Cells;
using Aspose.Cells.Rendering;
using Aspose.Pdf.Facades;
using System;
using System.Collections.Generic;
using System.IO;

namespace SMAS.VTUtils.Pdf
{
    public class PdfExport : IPdfExport
    {
        /// <summary>
        /// In dong gay
        /// </summary>
        public const int VIEW_BOOKLET = 1;
        public const string CONTENT_TYPE_PDF = "application/pdf";
        public const string CONTENT_TYPE_ZIP = "application/zip";
        public const string CONTENT_TYPE_XLS = "application/octet-stream";
        public const string EXT_PDF = "pdf";
        public const string EXT_XLS = "xls";
        public const int EXT_XLS_ID = 5;
        public const int EXT_PDF_ID = 13;
        public List<OutputFile> lstFormat;
        public static bool setLicense = false;

        public PdfExport()
        {
            if (!setLicense)
            {
                Utils.RemoveLicense.ActivateMemoryPatching();
                setLicense = true;
            }

        }
        public string CreateFolder()
        {
            string inFolder = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\" + Guid.NewGuid();
            DirectoryInfo objdir = new DirectoryInfo(inFolder);
            if (!objdir.Exists)
            {
                objdir.Create();
            }
            return inFolder;
        }

        public void DeleteFolder(string pathFolder)
        {
            DirectoryInfo objdir = new DirectoryInfo(pathFolder);
            objdir.Delete(true);
        }


        public OutputFile ConvertPdf(Stream excel, string reportName, int booklet, Aspose.Cells.PaperSizeType defaultPaper = Aspose.Cells.PaperSizeType.PaperA4)
        {
            Aspose.Cells.Workbook workBook = new Aspose.Cells.Workbook(excel);
            workBook.Settings.PaperSize = defaultPaper;
            
            reportName = reportName.Replace(".xls", "");
            reportName = reportName.Replace(".xlsx", "");
            string pathFile = CreateGuidFile(EXT_PDF, false);
            //Define PdfSaveOptions
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions(Aspose.Cells.SaveFormat.Pdf);

            //Set the compliance type
            //pdfSaveOptions.Compliance = PdfCompliance.PdfA1b;
            pdfSaveOptions.CalculateFormula = true;
            
            workBook.Save(pathFile,pdfSaveOptions);
            string outputFile = "";
            if (booklet == VIEW_BOOKLET)
            {
                outputFile = CreateGuidFile(EXT_PDF);
                BookletPdf(pathFile, outputFile);
                DeleteFile(pathFile);
            }
            else
            {
                outputFile = pathFile;
            }
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(outputFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }

            DeleteFile(outputFile);
            OutputFile objVal = new OutputFile
            {
                ContentFile = memStream,
                SaveFileId = EXT_PDF_ID,
                SaveFileName = reportName + "." + EXT_PDF
            };
            return objVal;
        }

        /// <summary>
        /// Giai nen file zip tu stream
        /// </summary>
        /// <param name="zipStream"></param>
        /// <param name="outFolder">Duong dan file duoc giai nen</param>
        public void UnzipFromStream(Stream zipStream, string outFolder)
        {

            ICSharpCode.SharpZipLib.Zip.ZipInputStream zipInputStream = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(zipStream);
            ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = zipInputStream.GetNextEntry();
            while (zipEntry != null)
            {
                String entryFileName = zipEntry.Name;
                // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                // Optionally match entrynames against a selection list here to skip as desired.
                // The unpacked length is available in the zipEntry.Size property.

                byte[] buffer = new byte[4096];     // 4K is optimum

                // Manipulate the output filename here as desired.
                String fullZipToPath = Path.Combine(outFolder, entryFileName);
                string directoryName = Path.GetDirectoryName(fullZipToPath);
                if (directoryName.Length > 0)
                    Directory.CreateDirectory(directoryName);

                // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                // of the file, but does not waste memory.
                // The "using" will close the stream even if an exception occurs.
                using (FileStream streamWriter = File.Create(fullZipToPath))
                {
                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(zipInputStream, streamWriter, buffer);
                }
                zipEntry = zipInputStream.GetNextEntry();
            }
        }

        public void ZipFolder(string outFilename, string inFolderName)
        {
            FileStream fsOut = File.Create(outFilename);
            ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(fsOut);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

            //zipStream.Password = password;  // optional. Null is the same as not setting. Required if using AES.

            // This setting will strip the leading part of the folder path in the entries, to
            // make the entries relative to the starting folder.
            // To include the full path for each entry up to the drive root, assign folderOffset = 0.
            int folderOffset = inFolderName.Length + (inFolderName.EndsWith("\\") ? 0 : 1);

            ZipFolder(inFolderName, zipStream, folderOffset);

            zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zipStream.Close();
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="inFileName"></param>
        /// <returns></returns>
        public MemoryStream ConvertZipFileToStream(string inFileName)
        {
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(inFileName))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }

            return memStream;


        }

        /// <summary>
        /// Chuyen file XLS trong thu muc thanh file pdf
        /// </summary>
        /// <param name="inFolder"></param>
        public void ConvertFileInFolderToPDF(string inFolder, int viewBooklet)
        {
            string[] filePaths = Directory.GetFiles(inFolder, "*.xls");
            for (int i = 0; i < filePaths.Length; i++)
            {
                string fileName = filePaths[i];
                Aspose.Cells.Workbook objCell = new Aspose.Cells.Workbook(fileName);
                string filepdf = fileName.Replace(".xls", "") + "." + EXT_PDF;

                objCell.Save(filepdf, Aspose.Cells.SaveFormat.Pdf);
                if (viewBooklet == VIEW_BOOKLET)
                {
                    //Tao file booklet.
                    string filebooklet =fileName.Replace(".xls", "")+"_"+(i+1) + "." + EXT_PDF;
                    BookletPdf(filepdf, filebooklet);
                    DeleteFile(filepdf);
                }
                File.Delete(fileName);
            }
        }

        #region Private

        private void ZipFolder(string path, ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream, int folderOffset)
        {

            string[] files = Directory.GetFiles(path);

            foreach (string filename in files)
            {
                FileInfo fi = new FileInfo(filename);

                string entryName = filename.Substring(folderOffset); // Makes the name in zip based on the folder
                entryName = ICSharpCode.SharpZipLib.Zip.ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                ICSharpCode.SharpZipLib.Zip.ZipEntry newEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity

                // Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                // A password on the ZipOutputStream is required if using AES.
                //   newEntry.AESKeySize = 256;

                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length;

                zipStream.PutNextEntry(newEntry);

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using (FileStream streamReader = File.OpenRead(filename))
                {
                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }
            string[] folders = Directory.GetDirectories(path);
            foreach (string folder in folders)
            {
                ZipFolder(folder, zipStream, folderOffset);
            }

        }

        private List<OutputFile> GetListFormatFile()
        {
            if (lstFormat != null && lstFormat.Count > 0)
            {
                return lstFormat;
            }
            lstFormat.Add(new OutputFile { SaveFileId = EXT_XLS_ID, SaveFileName = EXT_XLS });
            lstFormat.Add(new OutputFile { SaveFileId = EXT_PDF_ID, SaveFileName = EXT_PDF });
            return lstFormat;
        }

        private Aspose.Cells.SaveFormat GetFormartFile(int saveFileId)
        {
            switch (saveFileId)
            {
                case EXT_XLS_ID:
                    return Aspose.Cells.SaveFormat.Excel97To2003;
                case EXT_PDF_ID:
                    return Aspose.Cells.SaveFormat.Pdf;
                default:
                    break;
            }
            return Aspose.Cells.SaveFormat.Excel97To2003;
        }

        private void BookletPdf(string InputPathFile, string outputFile)
        {
            PdfFileEditor pdfEditor = new PdfFileEditor();            
            pdfEditor.MakeBooklet(InputPathFile, outputFile);
        }

        /// <summary>
        /// Tao duong dan cho file. Neu saveFile=true thuc hien tao file
        /// </summary>
        /// <param name="extentionFile"></param>
        /// <param name="saveFile"></param>
        /// <returns></returns>
        private string CreateGuidFile(string extentionFile, bool saveFile = false)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\" + Guid.NewGuid() + "." + extentionFile;
            if (saveFile)
            {
                FileInfo fi = new FileInfo(pathFile);
                fi.Create();
            }
            return pathFile;
        }

        private void DeleteFile(string pathFile)
        {
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
        }
        #endregion
    }

}
