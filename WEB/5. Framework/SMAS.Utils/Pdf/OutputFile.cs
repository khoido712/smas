﻿using System.IO;

namespace SMAS.VTUtils.Pdf
{
    public class OutputFile
    {
        public int SaveFileId { get; set; }
        public string SaveFileName { get; set; }
        public Stream ContentFile { get; set; }
    }
}

