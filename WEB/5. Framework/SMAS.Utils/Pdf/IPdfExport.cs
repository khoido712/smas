﻿using System.IO;

namespace SMAS.VTUtils.Pdf
{
    public interface IPdfExport
    {
        /// <summary>
        /// Tao thu muc chua file excel, pdf
        /// </summary>
        /// <returns></returns>
        string CreateFolder();

        /// <summary>
        /// Xoa thu muc
        /// </summary>
        /// <param name="pathFolder"></param>
        void DeleteFolder(string pathFolder);

        /// <summary>
        /// Chuyen doi file excel tu dinh dang stream sang pdf
        /// </summary>
        /// <param name="excel"></param>
        /// <param name="reportName"></param>
        /// <param name="defaultPaper"></param>
        /// <returns></returns>
        OutputFile ConvertPdf(Stream excel, string reportName,int booklet, Aspose.Cells.PaperSizeType defaultPaper = Aspose.Cells.PaperSizeType.PaperA4);


        /// <summary>
        /// Giai nen file zip tu stream
        /// </summary>
        /// <param name="zipStream"></param>
        /// <param name="outFolder">Duong dan file duoc giai nen</param>
        void UnzipFromStream(Stream zipStream, string outFolder);

        /// <summary>
        /// Zip file trong thu muc
        /// </summary>
        /// <param name="outFilename">Ten file</param>
        /// <param name="inFolderName"></param>
        void ZipFolder(string outFilename, string inFolderName);
        /// <summary>
        /// Convert file zip to stream
        /// </summary>
        /// <param name="inFileName"></param>
        /// <returns></returns>
        MemoryStream ConvertZipFileToStream(string inFileName);

        /// <summary>
        /// Convert files excel in folder to pdf
        /// </summary>
        /// <param name="inFolder"></param>
        /// <param name="viewBooklet">1: View booklet</param>
        void ConvertFileInFolderToPDF(string inFolder, int viewBooklet);
    }
}
