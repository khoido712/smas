﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization.Charting.ChartTypes;
using System.Web;
using System.Drawing;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
//using dotnetCHARTING;
namespace SMAS.VTUtils.DrawingChart
{
    public static class VTChartHelpers
    {
        public static FileContentResult Chart1()
        {
            var bytes = new System.Web.Helpers.Chart(width: 600, height: 400, theme: ChartTheme.Vanilla3D)

                .AddLegend("abc", "def")
                .AddTitle("Test chart MVC3")
                .AddSeries(
                chartType: "pie",
                xValue: new[] { "KKK", "BBB", "CCC", "DDDD" },
                yValues: new[] { "60", "75", "80", "85" })
                .GetBytes("png");
            return new FileContentResult(bytes, "image/png");
        }
        public static System.Web.UI.DataVisualization.Charting.Chart drawingChart()
        {
            string st1 = "Suy dinh dưỡng nặng";
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            Random random = new Random();
            chart1.Series.Add(st1);
            for (int pointIndex = 0; pointIndex < 10; pointIndex++)
            {

                chart1.Series[st1].Points.AddY(random.Next(20, 100));
            }
            chart1.Series[st1].ChartType = SeriesChartType.Pie;
            chart1.Series[st1]["PointWidth"] = "0.5";// Show data points labels
            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style
            chart1.Series[st1]["BarLabelStyle"] = "Center";// Show chart as 3D
            chart1.ChartAreas.Add("ChartArea1");
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;// Draw chart as 3D 
            chart1.Series[st1]["DrawingStyle"] = "Cylinder";
            chart1.Visible = true;
            return chart1;
        }
        public static MemoryStream drawBarChart(List<ViettelDataChart> listData)
        {
            Bitmap image = new Bitmap(500, 500);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 600;
            chart1.Height = 600;
            chart1.Titles.Add("Hien thi noi dung Title 1");
            
            chart1.ToolTip="Bam vao chart xem dc ngay";
            //chart1.Series.Add(st1);
            
            for (int i = 0; i < listData.Count; i++)
            {
                System.Web.UI.DataVisualization.Charting.Series series = new System.Web.UI.DataVisualization.Charting.Series();
                series.Name = listData[i].Label;
                chart1.Series.Add(listData[i].Label);

                //chart1.Series[listData[i].Label].Points.AddXY(listData[i].Label, listData[i].Value);
                chart1.Series[listData[i].Label].Points.AddY(listData[i].Value);
                chart1.Series[listData[i].Label].AxisLabel = "Hien thi data";
                
                
                //chart1.Legends.Add(listData[i].Label);
                //chart1.Series["s"].LegendText += listData[i].Label;
                //chart1.Series[st1].LegendToolTip = listData[i].Label;
                //chart1.Series[st1].Legend = listData[i].Label;
                //chart1.BackColor = Color.Transparent;
                chart1.BackColor = Color.FromArgb(255, 255, 255);
                Legend legend = new Legend();
                chart1.Legends.Add(legend);
                //Hien thi ten & gia tri cua tung loai du lieu
                chart1.Series[listData[i].Label].Label = "#VALY";
                
                //cach hien thi ten & gia tri tren Chart: Center, left, right
                chart1.Series[listData[i].Label]["BarLabelStyle"] = "Left";// Show chart as 3D
                chart1.Series[listData[i].Label].Color = System.Drawing.Color.FromArgb(((int)(((byte)(200)))),((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
                //set kieu chart: Bar, Pie, Column...
                chart1.Series[listData[i].Label].ChartType = SeriesChartType.Bar;
                // set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
                chart1.Series[listData[i].Label]["DrawingStyle"] = "Cylinder";
            }

            System.Web.UI.DataVisualization.Charting.ChartArea ca = new System.Web.UI.DataVisualization.Charting.ChartArea();
            chart1.ChartAreas.Add(ca);
            ca.BackColor = System.Drawing.Color.White;
            ca.AxisX.IsLabelAutoFit = false;
            ca.AxisX.LabelStyle.Angle = -90;
            ca.Area3DStyle.Enable3D = false;
            
            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            //Response.ContentType = "image/png";
            //imageStream.WriteTo(Response.OutputStream);
            //g.Dispose();
            //image.Dispose();
            //return st11;
            return imageStream;
        }
        public static MemoryStream drawPieChart(List<ViettelDataChart> listData)
        {


            string st1 = "Suy dinh dưỡng nặng";
            Bitmap image = new Bitmap(600, 600);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 600;
            chart1.Height = 600;
            chart1.Titles.Add("Hien thi noi dung Title 2");
            chart1.Series.Add(st1);

            for (int i = 0; i < listData.Count; i++)
            {

                chart1.Series[st1].Points.AddXY(listData[i].Label, listData[i].Value);
                //chart1.Legends.Add(listData[i].Label);
                chart1.BackColor = Color.Transparent;

            }
            Legend legend = new Legend();
            chart1.Legends.Add(legend);
            chart1.Series[st1].Label = "#AXISLABEL (#VALY)";
            chart1.Series[st1]["PieLabelStyle"] = "Outside";// Show chart as 3D
            chart1.Series[st1].ChartType = SeriesChartType.Pie;
            chart1.Series[st1]["PointWidth"] = "0.5";// Show data points labels
            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style
            
            chart1.ChartAreas.Add("ChartArea1");
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;// Draw chart as 3D 
            chart1.Series[st1]["DrawingStyle"] = "Cylinder";
            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            //Response.ContentType = "image/png";
            //imageStream.WriteTo(Response.OutputStream);
            //g.Dispose();
            //image.Dispose();
            //return st11;
    
            
            return imageStream;
        }
        public static MemoryStream drawLineChart(List<ViettelDataChart> listData)
        {


            string st1 = "Suy dinh dưỡng nặng";
            Bitmap image = new Bitmap(600, 600);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 600;
            chart1.Height = 600;
            chart1.Titles.Add("Hien thi noi dung Title 2");
            chart1.Series.Add(st1);

            for (int i = 0; i < listData.Count; i++)
            {

                chart1.Series[st1].Points.AddXY(listData[i].Label, listData[i].Value);
                //chart1.Legends.Add(listData[i].Label);
                chart1.BackColor = Color.Transparent;

            }
            //Legend legend = new Legend();
            //chart1.Legends.Add(legend);
            chart1.Series[st1].Label = "#AXISLABEL (#VALY)";
            chart1.Series[st1].Color = System.Drawing.Color.Red;
            chart1.Series[st1]["PieLabelStyle"] = "Outside";// Show chart as 3D
            chart1.Series[st1].ChartType = SeriesChartType.Line;
            chart1.Series[st1]["PointWidth"] = "0.5";// Show data points labels
            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style

            chart1.ChartAreas.Add("ChartArea1");
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;// Draw chart as 3D 
            chart1.Series[st1]["DrawingStyle"] = "Cylinder";
            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            //Response.ContentType = "image/png";
            //imageStream.WriteTo(Response.OutputStream);
            //g.Dispose();
            //image.Dispose();
            //return st11;


            return imageStream;
        }
        public static MemoryStream drawColumnChart(List<ViettelDataChart> listData)
        {
            string st1 = "Suy dinh dưỡng nặng";
            Bitmap image = new Bitmap(600, 600);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 600;
            chart1.Height = 600;
            chart1.Titles.Add("Hien thi noi dung Title 2");
            chart1.Series.Add(st1);
            //Màu nền của biểu đồ
            chart1.BackColor = Color.Transparent;
            for (int i = 0; i < listData.Count; i++)
            {

                chart1.Series[st1].Points.AddXY(listData[i].Label, listData[i].Value);
                //chart1.Legends.Add(listData[i].Label);
                chart1.Series[st1].BackSecondaryColor = Color.Red;
                //if (i == 0)
                //{
                //    chart1.Series[st1].Color = System.Drawing.Color.Violet;
                //}
                //else
                //{
                //    chart1.Series[st1].Color = System.Drawing.Color.FromArgb(215, 47, 6);
                //}
            }
            //Legend legend = new Legend();
            //chart1.Legends.Add(legend);
            chart1.Series[st1].Label = "#AXISLABEL (#VALY)";
            // set màu của cột biểu đồ
            //chart1.Series[st1].Color = System.Drawing.Color.Violet;
            
            // set màu nền Label của từng cột
            chart1.Series[st1].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st1].LabelForeColor = System.Drawing.Color.Red;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st1].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st1].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st1].BorderColor = System.Drawing.Color.Red;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st1].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st1].BorderWidth = 1;
            
            // Set kiểu chart
            chart1.Series[st1].ChartType = SeriesChartType.Column;
            
            //độ dầy của cột
            chart1.Series[st1]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style
            chart1.Series[st1].IsVisibleInLegend = false;// Set data points label style
            
            /************************** cấu hình ChartAreas *********************************/
            chart1.ChartAreas.Add("ChartArea1");
            // set kiểu biểu đồ là 2D hay 3D (true or false)
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            // set độ nghiêng của biểu đồ 3D
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;
            // set độ nghiêng của chân biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 0;
            // set độ dầy của không gian tường của biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 30;
            // set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st1]["DrawingStyle"] = "Cylinder";

            

            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            //Response.ContentType = "image/png";
            //imageStream.WriteTo(Response.OutputStream);
            //g.Dispose();
            //image.Dispose();
            //return st11;


            return imageStream;
        }
        public static MemoryStream drawStockChart(List<ViettelDataChart> listData1, List<ViettelDataChart> listData2, List<ViettelDataChart> listData3, List<ViettelDataChart> listData4, List<ViettelDataChart> listData5)
        {
            string st1 = "Suy dinh dưỡng nặng";
            string st2 = "Suy dinh dưỡng vừa";
            string st3 = "Cân nặng bình thường";
            string st4 = "Cân nặng cao hơn so với tuổi";
            string st5 = "Chỉ số của trẻ";
            Bitmap image = new Bitmap(1000, 600);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 1000;
            chart1.Height = 600;
            chart1.Titles.Add("Hien thi noi dung Title 2");
            #region Layer 1
            chart1.Series.Add(st1);
            //Màu nền của biểu đồ
            chart1.BackColor = Color.Transparent;
            chart1.BackImage = "";
            for (int i = 0; i < listData1.Count; i++)
            {
                //chart1.Series[st1].Points.AddXY("T"+i, listData1[i].Value);
                chart1.Series[st1].Points.AddY(listData1[i].Value);
                //chart1.Legends.Add(listData[i].Label);
                chart1.Series[st1].BackSecondaryColor = Color.Red;

            }

            //chart1.Series[st1].Label = "#AXISLABEL (#VALY)";
            // set màu của cột biểu đồ
            chart1.Series[st1].Color = Color.FromArgb(255,124,129);

            // set màu nền Label của từng cột
            //chart1.Series[st1].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st1].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st1].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st1].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st1].BorderColor = System.Drawing.Color.Red;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            //chart1.Series[st1].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st1].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st1].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st1]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style
            chart1.Series[st1].IsVisibleInLegend = false;// Set data points label style

            /************************** cấu hình ChartAreas *********************************/
            chart1.ChartAreas.Add("ChartArea1");
            // set kiểu biểu đồ là 2D hay 3D (true or false)
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // set độ nghiêng của biểu đồ 3D
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;
            // set độ nghiêng của chân biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 0;
            // set độ dầy của không gian tường của biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 30;
            // set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default


            chart1.ChartAreas["ChartArea1"].AlignmentStyle = AreaAlignmentStyles.AxesView;
            

            // Create a new legend called "Legend2".
            chart1.Legends.Add(new Legend("Legend2"));

            // Set Docking of the Legend chart to the Default Chart Area.
            chart1.Legends["Legend2"].DockedToChartArea = "ChartArea1";
            chart1.Legends["Legend2"].LegendStyle = LegendStyle.Table;
            chart1.Legends["Legend2"].IsDockedInsideChartArea = true;
            // Assign the legend to Series1.
            chart1.Series[st1].Legend = "Legend2";
            
            chart1.Series[st1].IsVisibleInLegend = true;
            #endregion
            
            #region Layer 2
            // Layer 2 of Chart

            chart1.Series.Add(st2);
            for (int i = 0; i < listData2.Count; i++)
            {

                //chart1.Series[st2].Points.AddXY(listData2[i].Label, listData2[i].Value);
                chart1.Series[st2].Points.AddY(listData2[i].Value);
                
                //chart1.Legends.Add(listData[i].Label);
                //chart1.Series[st2].BackSecondaryColor = Color.Red;
                //if (i == 0)
                //{
                //    chart1.Series[st2].Points[i].Color = Color.Red;
                //}
                //else
                //{
                //    chart1.Series[st2].Points[i].Color = getColor("Yellow");
                //}

            }

            //chart1.Series[st2].Label = "#AXISLABEL (#VALX)";
            //chart1.Series[st2].Label = "Y = #VALY\nX = #VALX";
            
            
             //set màu của cột biểu đồ
            chart1.Series[st2].Color = Color.FromArgb(251,154,151);

            // set màu nền Label của từng cột
            chart1.Series[st2].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st2].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st2].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st2].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st2].BorderColor = System.Drawing.Color.Pink;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st2].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st2].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st2].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st2]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st2].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st2].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st2].Legend = "Legend2";
            chart1.Series[st2].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st2]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 3
            // Layer 3 of Chart

            chart1.Series.Add(st3);
            for (int i = 0; i < listData3.Count; i++)
            {
                chart1.Series[st3].Points.AddY(listData3[i].Value);
            }

            //chart1.Series[st3].Label = "#AXISLABEL (#VALX)";
            //chart1.Series[st3].Label = "Y = #VALY\nX = #VALX";


            //set màu của cột biểu đồ
            chart1.Series[st3].Color = Color.FromArgb(204,255,255);

            // set màu nền Label của từng cột
            chart1.Series[st3].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st3].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st3].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st3].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st3].BorderColor = System.Drawing.Color.LightBlue;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st3].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st3].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st3].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st3]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st3].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st3].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st3].Legend = "Legend2";
            chart1.Series[st3].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st3]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 4
            // Layer 4 of Chart

            chart1.Series.Add(st4);
            for (int i = 0; i < listData4.Count; i++)
            {
                chart1.Series[st4].Points.AddY(listData4[i].Value);
            }

            //chart1.Series[st4].Label = "#AXISLABEL (#VALX)";
            //chart1.Series[st4].Label = "Y = #VALY\nX = #VALX";


            //set màu của cột biểu đồ
            chart1.Series[st4].Color = Color.FromArgb(254,255,153);

            // set màu nền Label của từng cột
            chart1.Series[st4].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st4].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st4].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st4].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st4].BorderColor = System.Drawing.Color.Yellow;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st4].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st4].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st4].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st4]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st4].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st4].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st4].Legend = "Legend2";
            chart1.Series[st4].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st4]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 5
            // Layer 5 of Chart

            chart1.Series.Add(st5);
            for (int i = 0; i < listData5.Count; i++)
            {
                chart1.Series[st5].Points.AddY(listData5[i].Value);
            }

            //chart1.Series[st5].Label = "#AXISLABEL (#VALX)";
            //chart1.Series[st5].Label = "Y = #VALY\nX = #VALX";


            //set màu của cột biểu đồ
            chart1.Series[st5].Color = Color.Black;

            // set màu nền Label của từng cột
            //chart1.Series[st5].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            //chart1.Series[st5].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st5].MarkerSize = 1;
            // set style trên đỉnh cột
            chart1.Series[st5].MarkerStyle = MarkerStyle.Star4;
            // set đường viền của cột
            //chart1.Series[st5].BorderColor = System.Drawing.Color.Red;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            //chart1.Series[st5].BorderDashStyle = ChartDashStyle.NotSet;
            // set độ dầy của đường viền
            chart1.Series[st5].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st5].ChartType = SeriesChartType.FastLine;

            //độ dầy của cột
            chart1.Series[st5]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st5].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st5].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st5].Legend = "Legend2";
            chart1.Series[st5].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st5]["DrawingStyle"] = "Cylinder";
            #endregion
            #region Layer 6
            // Layer 6 of Chart

            chart1.Series.Add("Series6");
            for (int i = 0; i < listData5.Count; i++)
            {
                //chart1.Series["Series6"].Points.AddY(listData5[i].Value);
                chart1.Series["Series6"].Points.AddXY("T"+i,listData5[i].Value);
            }

            //chart1.Series[st5].Label = "#AXISLABEL (#VALX)";
            //chart1.Series[st5].Label = "Y = #VALY\nX = #VALX";

            //set màu của cột biểu đồ
            chart1.Series["Series6"].Color = Color.DarkBlue;

            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series["Series6"].MarkerSize = 1;
            // set style trên đỉnh cột
            chart1.Series["Series6"].MarkerStyle = MarkerStyle.Star4;
            // set độ dầy của đường viền
            chart1.Series["Series6"].BorderWidth = 1;
            // Set kiểu chart
            chart1.Series["Series6"].ChartType = SeriesChartType.Stock;
            //độ dầy của cột
            chart1.Series["Series6"]["PointWidth"] = "10";// Show data points labels
            //chart1.Series["Series6"].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st5].IsVisibleInLegend = false;// Set data points label style
            //chart1.Series["Series6"].Legend = "Legend2";
            //chart1.Series["Series6"].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series["Series6"]["DrawingStyle"] = "Cylinder";
            
            chart1.ChartAreas["ChartArea1"].AxisY.Enabled = AxisEnabled.True;
            chart1.ChartAreas["ChartArea1"].AxisY.IsMarksNextToAxis = true;
            chart1.ChartAreas["ChartArea1"].AxisY.Title = "Cân nặng của trẻ";
            chart1.ChartAreas["ChartArea1"].AxisY.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisY.MinorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorTickMark.LineColor = Color.Green;
            chart1.ChartAreas["ChartArea1"].AxisY.MinorTickMark.LineColor = Color.Green;
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.ForeColor = Color.Black;
            //chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.ForeColor = Color.FromArgb;
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "{0:0.0}";
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.IsEndLabelVisible = true;
            //chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Calibri", 4, FontStyle.Regular);
            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisY.MaximumAutoSize = 100;
            chart1.ChartAreas["ChartArea1"].AxisY.Interval = 10;
            // Hiển thị kiểu 2 trục biểu đồ
            chart1.ChartAreas["ChartArea1"].AxisX.ArrowStyle = AxisArrowStyle.None;
            // Cho phép hiển thị từng trục biểu đồ
            chart1.ChartAreas["ChartArea1"].AxisX.Crossing = 1;
            chart1.ChartAreas["ChartArea1"].AxisX.Title = "Tháng tuổi";
            chart1.ChartAreas["ChartArea1"].AxisX.IsStartedFromZero = true;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            chart1.ChartAreas["ChartArea1"].AxisX.Maximum = 60;
            chart1.ChartAreas["ChartArea1"].AxisX.IsMarksNextToAxis = true;
            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.IsEndLabelVisible = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MaximumAutoSize = 60;
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 2;
            chart1.ChartAreas["ChartArea1"].AxisX.LineColor = Color.HotPink;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MinorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorTickMark.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisX.MinorTickMark.LineColor = Color.DarkBlue;
            #endregion

            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            //Response.ContentType = "image/png";
            //imageStream.WriteTo(Response.OutputStream);
            //g.Dispose();
            //image.Dispose();
            //return st11;


            return imageStream;
        }
        //public static void print()
        //{
        //    dotnetCHARTING.Chart a = new dotnetCHARTING.Chart();
        //    a.Type = ChartType.Pie;
        //    // Set he chart size.
        //    a.Width = 600;
        //    a.Height = 350;

        //    // Set the title.
        //    a.Title = "My Chart";
        //    a.Debug = true;
        //    a.DefaultElement.ToolTip = "%SeriesName <br> %YValue (%PercentOfTotal)";
        //    // Set the directory where the images will be stored.
        //    a.TempDirectory = "temp";
        //    // Enable JSCharting
        //    a.JS.Enabled = true;
        //    // Add the random data.
        //    a.SeriesCollection.Add(getRandomData());
            
        //    //System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
        //    //pd.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(drawPieChart);
        //    //window.op
        //}
        //static dotnetCHARTING.SeriesCollection getRandomData()
        //{
        //    dotnetCHARTING.SeriesCollection SC = new dotnetCHARTING.SeriesCollection();
        //    Random myR = new Random();
        //    for (int a = 1; a < 5; a++)
        //    {
        //        dotnetCHARTING.Series s = new dotnetCHARTING.Series();
        //        s.Name = "Series " + a;
        //        for (int b = 1; b < 5; b++)
        //        {
        //            Element e = new Element();
        //            e.Name = "E " + b;
        //            e.YValue = myR.Next(50);
        //            s.Elements.Add(e);
        //        }
        //        SC.Add(s);
        //    }


        //    return SC;
        //}
        public static Color getColor(string nameColor){
            return Color.FromName(nameColor);
        }

        public static MemoryStream drawStockChart(List<ViettelDataChart> listData1, List<ViettelDataChart> listData2, List<ViettelDataChart> listData3, List<ViettelDataChart> listData4, List<ViettelDataChart> listData5,Dictionary<string,object> dic)
        {
            string st1 = "Suy dinh dưỡng nặng";
            string st2 = "Suy dinh dưỡng vừa";
            string st3 = "Cân nặng bình thường";
            string st4 = "Cân nặng cao hơn so với tuổi";
            string st5 = "Chỉ số của trẻ";
            string title = (string)dic["Title"] ;
            Bitmap image = new Bitmap(1000, 600);
            Graphics g = Graphics.FromImage(image);
            System.Web.UI.DataVisualization.Charting.Chart chart1 = new System.Web.UI.DataVisualization.Charting.Chart();
            chart1.Width = 1000;
            chart1.Height = 600;
            chart1.Titles.Add(title);  
            #region Layer 1
            chart1.Series.Add(st1);
            //Màu nền của biểu đồ
            chart1.BackColor = Color.Transparent;
            chart1.BackImage = "";
            for (int i = 0; i < listData1.Count; i++)
            {
                //chart1.Series[st1].Points.AddXY("T"+i, listData1[i].Value);
                chart1.Series[st1].Points.AddY(listData1[i].Value);
                //chart1.Legends.Add(listData[i].Label);
                chart1.Series[st1].BackSecondaryColor = Color.Red;

            }

            //chart1.Series[st1].Label = "#AXISLABEL (#VALY)";
            // set màu của cột biểu đồ
            chart1.Series[st1].Color = Color.FromArgb(255, 124, 129);

            // set màu nền Label của từng cột
            //chart1.Series[st1].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st1].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st1].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st1].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st1].BorderColor = System.Drawing.Color.Red;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            //chart1.Series[st1].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st1].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st1].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st1]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st1].IsValueShownAsLabel = true;// Set data points label style
            chart1.Series[st1].IsVisibleInLegend = false;// Set data points label style

            /************************** cấu hình ChartAreas *********************************/
            chart1.ChartAreas.Add("ChartArea1");
            // set kiểu biểu đồ là 2D hay 3D (true or false)
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // set độ nghiêng của biểu đồ 3D
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Rotation = 10;
            // set độ nghiêng của chân biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Perspective = 0;
            // set độ dầy của không gian tường của biểu đồ
            chart1.ChartAreas["ChartArea1"].Area3DStyle.WallWidth = 30;
            // set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default


            chart1.ChartAreas["ChartArea1"].AlignmentStyle = AreaAlignmentStyles.AxesView;


            // Create a new legend called "Legend2".
            chart1.Legends.Add(new Legend("Legend2"));

            // Set Docking of the Legend chart to the Default Chart Area.
            chart1.Legends["Legend2"].DockedToChartArea = "ChartArea1";
            chart1.Legends["Legend2"].LegendStyle = LegendStyle.Table;
            chart1.Legends["Legend2"].IsDockedInsideChartArea = true;
            // Assign the legend to Series1.
            chart1.Series[st1].Legend = "Legend2";

            chart1.Series[st1].IsVisibleInLegend = true;
            #endregion

            #region Layer 2
            // Layer 2 of Chart

            chart1.Series.Add(st2);
            for (int i = 0; i < listData2.Count; i++)
            {
                chart1.Series[st2].Points.AddY(listData2[i].Value);
            }
            //set màu của cột biểu đồ
            chart1.Series[st2].Color = Color.FromArgb(251, 154, 151);

            // set màu nền Label của từng cột
            chart1.Series[st2].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st2].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st2].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st2].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st2].BorderColor = System.Drawing.Color.Pink;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st2].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st2].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st2].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st2]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st2].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st2].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st2].Legend = "Legend2";
            chart1.Series[st2].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st2]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 3
            // Layer 3 of Chart

            chart1.Series.Add(st3);
            for (int i = 0; i < listData3.Count; i++)
            {
                chart1.Series[st3].Points.AddY(listData3[i].Value);
            }
            //set màu của cột biểu đồ
            chart1.Series[st3].Color = Color.FromArgb(204, 255, 255);

            // set màu nền Label của từng cột
            chart1.Series[st3].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st3].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st3].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st3].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st3].BorderColor = System.Drawing.Color.LightBlue;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st3].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st3].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st3].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st3]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st3].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st3].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st3].Legend = "Legend2";
            chart1.Series[st3].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st3]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 4
            // Layer 4 of Chart

            chart1.Series.Add(st4);
            for (int i = 0; i < listData4.Count; i++)
            {
                chart1.Series[st4].Points.AddY(listData4[i].Value);
            }
            //set màu của cột biểu đồ
            chart1.Series[st4].Color = Color.FromArgb(254, 255, 153);

            // set màu nền Label của từng cột
            chart1.Series[st4].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            chart1.Series[st4].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st4].MarkerSize = 10;
            // set style trên đỉnh cột
            chart1.Series[st4].MarkerStyle = MarkerStyle.Star10;
            // set đường viền của cột
            chart1.Series[st4].BorderColor = System.Drawing.Color.Yellow;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            chart1.Series[st4].BorderDashStyle = ChartDashStyle.DashDotDot;
            // set độ dầy của đường viền
            chart1.Series[st4].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st4].ChartType = SeriesChartType.StackedArea100;

            //độ dầy của cột
            chart1.Series[st4]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st4].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st4].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st4].Legend = "Legend2";
            chart1.Series[st4].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st4]["DrawingStyle"] = "Cylinder";
            #endregion

            #region Layer 5
            // Layer 5 of Chart

            chart1.Series.Add(st5);
            for (int i = 0; i < listData5.Count; i++)
            {
                chart1.Series[st5].Points.AddY(listData5[i].Value);
            }
            //set màu của cột biểu đồ
            chart1.Series[st5].Color = Color.Black;

            // set màu nền Label của từng cột
            //chart1.Series[st5].LabelBackColor = System.Drawing.Color.Transparent;
            // set màu chữ Label của từng cột
            //chart1.Series[st5].LabelForeColor = System.Drawing.Color.Transparent;
            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series[st5].MarkerSize = 1;
            // set style trên đỉnh cột
            chart1.Series[st5].MarkerStyle = MarkerStyle.Star4;
            // set đường viền của cột
            //chart1.Series[st5].BorderColor = System.Drawing.Color.Red;
            // set kiểu đường viền ( chấm chấm, gạch gạch)
            //chart1.Series[st5].BorderDashStyle = ChartDashStyle.NotSet;
            // set độ dầy của đường viền
            chart1.Series[st5].BorderWidth = 1;

            // Set kiểu chart
            chart1.Series[st5].ChartType = SeriesChartType.FastLine;

            //độ dầy của cột
            chart1.Series[st5]["PointWidth"] = "1";// Show data points labels

            chart1.Series[st5].IsValueShownAsLabel = true;// Set data points label style
            //chart1.Series[st5].IsVisibleInLegend = false;// Set data points label style
            chart1.Series[st5].Legend = "Legend2";
            chart1.Series[st5].IsVisibleInLegend = true;

            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series[st5]["DrawingStyle"] = "Cylinder";
            #endregion
            #region Layer 6
            // Layer 6 of Chart

            chart1.Series.Add("Series6");
            for (int i = 0; i < listData5.Count; i++)
            {
                //chart1.Series["Series6"].Points.AddY(listData5[i].Value);
                chart1.Series["Series6"].Points.AddXY("T" + i, listData5[i].Value);
            }
            //set màu của cột biểu đồ
            chart1.Series["Series6"].Color = Color.DarkBlue;

            // khoảng cách chữ label của từng cột đến đầu cột
            chart1.Series["Series6"].MarkerSize = 1;
            // set style trên đỉnh cột
            chart1.Series["Series6"].MarkerStyle = MarkerStyle.Star4;
            // set độ dầy của đường viền
            chart1.Series["Series6"].BorderWidth = 1;
            // Set kiểu chart
            chart1.Series["Series6"].ChartType = SeriesChartType.Stock;
            //độ dầy của cột
            chart1.Series["Series6"]["PointWidth"] = "10";// Show data points labels
            /************************** cấu hình ChartAreas *********************************/
            // Không cấu hình ChartAreas nữa mà chỉ cấu hình 1 lần dùng chung cho các lớp
            //// set kieu Drawing Style:  Cylinder, Emboss, LightToDark, Wedge, Default
            chart1.Series["Series6"]["DrawingStyle"] = "Cylinder";

            chart1.ChartAreas["ChartArea1"].AxisY.Enabled = AxisEnabled.True;
            chart1.ChartAreas["ChartArea1"].AxisY.IsMarksNextToAxis = true;
            chart1.ChartAreas["ChartArea1"].AxisY.Title = "Cân nặng của trẻ";
            chart1.ChartAreas["ChartArea1"].AxisY.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisY.MinorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisY.MajorTickMark.LineColor = Color.Green;
            chart1.ChartAreas["ChartArea1"].AxisY.MinorTickMark.LineColor = Color.Green;
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.ForeColor = Color.Black;
            //chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.ForeColor = Color.FromArgb;
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "{0:0.0}";
            chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.IsEndLabelVisible = true;
            //chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Calibri", 4, FontStyle.Regular);
            chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisY.MaximumAutoSize = 100;
            chart1.ChartAreas["ChartArea1"].AxisY.Interval = 10;
            // Hiển thị kiểu 2 trục biểu đồ
            chart1.ChartAreas["ChartArea1"].AxisX.ArrowStyle = AxisArrowStyle.None;
            // Cho phép hiển thị từng trục biểu đồ
            chart1.ChartAreas["ChartArea1"].AxisX.Crossing = 1;
            chart1.ChartAreas["ChartArea1"].AxisX.Title = "Tháng tuổi";
            chart1.ChartAreas["ChartArea1"].AxisX.IsStartedFromZero = true;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            chart1.ChartAreas["ChartArea1"].AxisX.Maximum = 60;
            chart1.ChartAreas["ChartArea1"].AxisX.IsMarksNextToAxis = true;
            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.IsEndLabelVisible = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MaximumAutoSize = 60;
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 2;
            chart1.ChartAreas["ChartArea1"].AxisX.LineColor = Color.HotPink;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MinorTickMark.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorTickMark.LineColor = Color.Black;
            chart1.ChartAreas["ChartArea1"].AxisX.MinorTickMark.LineColor = Color.DarkBlue;
            #endregion

            MemoryStream imageStream = new MemoryStream();
            chart1.SaveImage(imageStream, ChartImageFormat.Png);
            chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.SystemDefault;
            string st11 = chart1.RenderingCompatibility.ToString();
            return imageStream;
        }
             
    }
}
