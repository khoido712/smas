﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.VTUtils.Log
{
    public class ErrorLogBO
    {
       // public  const string LOG_ERROR = "ERROR";
        /// <summary>
        /// Loại loi: Error
        /// </summary>
        //public string LogType { get; set; }
        /// <summary>
        /// Thoi gian loi
        /// </summary>
        public DateTime EventDate { get; set; }
        /// <summary>
        /// Ten class bi loi
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Mo ta loi
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Cac tham so thuc hien giao dich
        /// </summary>
        public string ParamList { get; set; }
        /// <summary>
        /// Ma loi
        /// </summary>
        public string ErrorCode { get; set; }

        public string ErrorMessage
        {
            get
            {
                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}", "ERROR", EventDate.ToString("yyyy/MM/dd HH:mm:ss:ff"), ClassName, Description, ParamList, ErrorCode);
            }
        }
    }
}
