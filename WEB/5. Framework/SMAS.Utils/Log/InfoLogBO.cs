﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.VTUtils.Log
{
    public class InfoLogBO
    {

        /// <summary>
        /// Loại loi: START_ACTION,  END_ACTION, START_CONNECT, END_CONNECT
        /// </summary>
        public string LogType { get; set; }
        /// <summary>
        /// Ma ung dung
        /// </summary>
        public string AppCode { get; set; }

        public DateTime EventDate { get; set; }

        /// <summary>
        /// Tai khoan tac dong
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Dia chi Ip tac dong
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// Ten duong dan
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Ten ham tac dong hoc Url tac dong
        /// </summary>
        public string Function { get; set; }
        /// <summary>
        /// Cac tham so thuc hien giao dich
        /// </summary>
        public string ParamList { get; set; }

        /// <summary>
        /// Ten lop tac dong
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// Khoang thoi gian thuc hien, ms
        /// </summary>
        public string Duration
        {
            get
            {
                TimeSpan span = DateTime.Now - EventDate;
                return span.Milliseconds.ToString();
            }
        }

        /// <summary>
        /// Mo ta
        /// </summary>
        public string Description { get; set; }

        public string Message
        {
            get
            {

                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}",
                                        LogType,
                                        AppCode,
                                        EventDate.ToString("yyyy/MM/dd HH:mm:ss:ff"),
                                        UserName,
                                        IpAddress,
                                        Path,
                                        Function,
                                        ParamList,
                                        ClassName,
                                        Duration,
                                        Description
                                      );
            }
        }
    }
}
