﻿using log4net;
using System;
using System.Net;

namespace SMAS.VTUtils.Log
{
    public class LogExtensions
    {
        public const string LOG_TYPE_START_ACTION = "START_ACTION";
        public const string LOG_TYPE_END_ACTION = "END_ACTION";
        public const string LOG_TYPE_START_CONNECT = "START_CONNECT";
        public const string LOG_TYPE_END_CONNECT = "END_CONNECT";
        public const string LOG_TYPE_INFO = "INFO";
        public const string APP_CODE = "SMAS";
        public const string LOG_TYPE_LOGIN = "LOGIN";
        public const string LOG_TYPE_LOGOUT = "LOGOUT";
        public const string LOG_ERROR = "ERROR";
        public const string LOGIN_SUCCESS = "LOGIN_SUCCESS";
        public const string LOGIN_FAIL = "LOGIN_FAIL";
        public const string LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
        public const string LOGOUT_FAIL = "LOGOUT_FAIL";

        /// <summary>
        /// Ghi log nghiep vu theo quy dinh 168 cua tap doan. Phai dien du cac tieu chi
        /// 1. logType: START_ACTION OR END_ACTION OR START_CONNECT OR END_CONNECT OR INFO
        /// 2. classAndMethodName: Ten class va method dat log
        /// 3. paraList: danh sach tham so
        /// 4. description: Mo ta nghiep vu
        /// 5. eventDate: Ngay thuc hien
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logType"></param>
        /// <param name="classAndMethodName"></param>
        /// <param name="paraList"></param>
        /// <param name="description"></param>
        /// <param name="eventDate"></param>
        public static void InfoExt(ILog logger, string logType, DateTime eventDate, string paraList, string description, string userName)
        {
            string className = "";
            //Log nghiep vu
            InfoLogBO infoLog = new InfoLogBO();
            infoLog.LogType = logType;
            infoLog.ClassName = className;
            infoLog.AppCode = APP_CODE;
            infoLog.EventDate = eventDate;
            infoLog.Function = className;
            infoLog.IpAddress = GetIPAddress();
            infoLog.ParamList = paraList;
            infoLog.Path = "";
            infoLog.UserName = userName;
            logger.Info(infoLog.Message);
        }


        /// <summary>
        /// Ghi log loi theo quy dinh 168 cua tap doan.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="eventDate"></param>
        /// <param name="classAndMethodName"></param>
        /// <param name="paraList"></param>
        /// <param name="description"></param>
        /// <param name="errorCode"></param>
        public static void ErrorExt(ILog logger, DateTime eventDate, string className, string paraList, Exception ex)
        {
            //Log nghiep vu
            ErrorLogBO errLog = new ErrorLogBO()
            {
                ClassName = className,
                Description = ex.Message,
                ErrorCode = "",
                EventDate = eventDate,
                ParamList = " Para=" + paraList
            };
            if (ex != null)
            {
                logger.Error(errLog.ErrorMessage, ex);
            }
            else
            {
                logger.Error(errLog.ErrorMessage);
            }

        }

        public static string GetIPAddress()
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                string hostIp = context.Request.UserHostAddress;
                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return string.Format("{0}_UserHostAddress:{1}", addresses[0], hostIp);
                    }
                }
                return string.Format("{0}_hostIp:{1}", context.Request.ServerVariables["REMOTE_ADDR"], hostIp);
            }
            catch
            {
                return "";
            }

        }
        /*private static string GetClassAndMethodName
        {
            get
            {
                if (GetCurrentClass == null)
                {
                    return "";
                }
                string classAndMethodName = GetCurrentClass.Name;
                return classAndMethodName;
            }
        }*/

        public static string GetClientIp()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            string hostIp = context.Request.UserHostAddress;
            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return string.Format("{0}", addresses[0]);
                }
            }
            return string.Format("{0}", context.Request.ServerVariables["REMOTE_ADDR"]);
        }

        public static string GetHostIp()
        {

            string hostIp = "";
            System.Net.IPAddress[] a = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            if (a != null && a.Length > 0)
            {

                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {

                        hostIp = a[i].ToString();
                        break;
                    }
                }
            }

            return hostIp;

        }
    }
}
