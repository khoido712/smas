﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.VTUtils.Utils
{
    public class Reflector
    {
        /// <summary>
        /// Lấy tên của property của đối tượng.
        /// </summary>
        /// <param name="expression"></param>
        /// <returns>Tên của property</returns>
        public static string GetPropertyName<P, T>(Expression<Func<P, T>> expression)
        {
            MemberExpression memberExpression = (MemberExpression)expression.Body;
            return memberExpression.Member.Name;
        }
    }

    /// <summary>
    /// Help to get property or method name of class T.
    /// </summary>
    public static class StaticReflection
    {
        /// <summary>
        /// Get property name of class T.
        /// </summary>
        /// <typeparam name="T">Class you need to get property name.</typeparam>
        /// <param name="instance"></param>
        /// <param name="expression">Select property you need to get name.</param>
        /// <returns></returns>
        public static string GetMemberName<T>(
            this T instance,
            Expression<Func<T, object>> expression)
        {
            return GetMemberName(expression);
        }

        /// <summary>
        /// Get property name of class T.
        /// </summary>
        /// <typeparam name="T">Class you need to get property name.</typeparam>
        /// <param name="expression">Select property you need to get name.</param>
        /// <returns></returns>
        public static string GetMemberName<T>(Expression<Func<T, object>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }
            return GetMemberName(expression.Body);
        }

        /// <summary>
        /// Get property name of class T.
        /// </summary>
        /// <typeparam name="T">Class you need to get property name.</typeparam>
        /// <param name="instance"></param>
        /// <param name="expression">Select property you need to get name.</param>
        /// <returns></returns>
        public static string GetMemberName<T>(
            this T instance,
            Expression<Action<T>> expression)
        {
            return GetMemberName(expression);
        }

        /// <summary>
        /// Get property name of class T.
        /// </summary>
        /// <typeparam name="T">Class you need to get property name.</typeparam>
        /// <param name="expression">Select property you need to get name.</param>
        /// <returns></returns>
        public static string GetMemberName<T>(
            Expression<Action<T>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException(
                    "The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        private static string GetMemberName(Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("The expression cannot be null.");
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member.Name;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression = (MethodCallExpression)expression;
                return methodCallExpression.Method.Name;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMemberName(unaryExpression);
            }

            if (expression is ConstantExpression)
            {
                var constantExpression = (ConstantExpression)expression;
                var methodInfo = (MemberInfo)constantExpression.Value;
                return methodInfo.Name;
            }
            throw new ArgumentException("Invalid expression");
        }

        private static string GetMemberName(UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression)
            {
                var methodExpression = (MethodCallExpression)unaryExpression.Operand;
                if (methodExpression.Method.ReturnParameter.ParameterType == typeof(Delegate))
                {
                    return GetMemberName(methodExpression.Object);
                }
                else
                {
                    return methodExpression.Method.Name;
                }
            }
            else if (unaryExpression.Operand is MemberExpression)
            {
                var memberExpression = (MemberExpression)unaryExpression.Operand;
                return memberExpression.Member.Name;
            }
            throw new ArgumentException("Invalid expression");
        }
    }
}
