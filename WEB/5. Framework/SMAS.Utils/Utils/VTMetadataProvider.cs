﻿/*
 * Define MetadataProvider 
 * Author: NhungNT
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SMAS.VTUtils.Utils
{
    public class VTMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(System.Collections.Generic.IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var _default = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
            _default.IsRequired = attributes.Where(x => x is RequiredAttribute).Count() > 0;
            return _default;
        }
    }
}
