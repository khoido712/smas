﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using System.Web;



namespace SMAS.VTUtils.Utils
{
    

    /// <summary>
    /// Class using for Classic Menu
    /// </summary>
    public class MenuBuilder
    {
       
        List<Menu> mainMenu { get; set; }
        List<Menu> MainParentMenu = new List<Menu>();
        List<ItemMenuModel> allMenu;
        List<ItemMenuModel> ParentMenuModel;
        List<Menu> extractParentLevel = new List<Menu>(); // mainMenu da bo di Level1

        public MenuBuilder(List<Menu> menuByRole)
        {
            mainMenu = menuByRole.Where(a=>a.IsActive == true).OrderBy(a=>a.OrderNumber).ToList();
            SetParentMenu();
        }

        public List<ItemMenuModel> GetAllMenu()
        {
            allMenu = MappingData(mainMenu);
            return allMenu;
        }

        /// <summary>
        /// Thiết lập danh sách Menu cấp 1 trong list all Menu
        /// </summary>
        private void SetParentMenu()
        {
            //Lấy Menu level 1, có parentID = null
            if (mainMenu != null)
            {
                MainParentMenu = mainMenu.Where(m => m.ParentID == null).OrderBy(a => a.OrderNumber).ToList();
                ParentMenuModel = MappingData(MainParentMenu);
            }
            else
            {
                ParentMenuModel = null;
            }
        }
        /// <summary>
        /// Lấy ra danh sách Menu cấp 1 
        /// </summary>
        /// <returns>Danh sách Menu cấp 1</returns>
        public List<ItemMenuModel> GetParentMenu()
        {
            return ParentMenuModel;
        }
        /// <summary>
        /// Map từ danh sách Menu sang danh sách ItemMenuModel để bind với Telerik Menu
        /// </summary>
        /// <param name="srcItems">danh sách Menu</param>
        /// <returns>danh sách ItemMenuModel</returns>
        public List<ItemMenuModel> MappingData(List<Menu> srcItems)
        {
            // mapping model Menu sang model ItemMenuModel
            List<ItemMenuModel> targetItems = new List<ItemMenuModel>();
            foreach (Menu objMenu in srcItems)
            {
                ItemMenuModel objModelMenu = SetModelValue(objMenu);
                List<Menu> lstSubMenuItems = mainMenu.Where(a => a.ParentID == objMenu.MenuID).ToList();
                SetMappingSubMenu(objModelMenu, lstSubMenuItems);
                targetItems.Add(objModelMenu);
            }
            //săp xep theo Order
            return targetItems.OrderBy(m => m.Order).ToList();
        }

        private ItemMenuModel SetMappingSubMenu(ItemMenuModel obujMenu, List<Menu> lstSubItems)
        {
            if (lstSubItems != null && lstSubItems.Count > 0)
            {
                foreach (Menu objMenu in lstSubItems)
                {
                    ItemMenuModel objModelMenu = SetModelValue(objMenu);
                    obujMenu.MenuChildren.Add(objModelMenu);
                    List<Menu> lstSubMenuItems = mainMenu.Where(a => a.ParentID == objMenu.MenuID).ToList();
                    SetMappingSubMenu(objModelMenu, lstSubMenuItems);
                }
            }
            return obujMenu;
        }
        /// <summary>
        /// Gan gia tri cua menu vao ViewModel
        /// </summary>
        /// <param name="objMenu"></param>
        /// <returns></returns>
        string DEFAULT_IMAGE = "/Content/images/info.png";
        private ItemMenuModel SetModelValue(Menu objMenu)
        {
            ItemMenuModel objModelMenu = new ItemMenuModel();
            //lay resoucre tuong ung voi MenuName
            string nameRK = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
            if (!string.IsNullOrEmpty(nameRK))
            {
                objModelMenu.Text = nameRK;
            }
            else
            {
                objModelMenu.Text = objMenu.MenuName;
            }
            objModelMenu.ItemMenuID = objMenu.MenuID;
            objModelMenu.ItemParentID = objMenu.ParentID;
            objModelMenu.MenuUrl = objMenu.URL;
            objModelMenu.Visible = objMenu.IsVisible;
            objModelMenu.MenuPath = objMenu.MenuPath.Replace("/", "-");
            objModelMenu.IsCategory = objMenu.IsCategory;
            objModelMenu.ImageUrl = objMenu.ImageURL == null ? DEFAULT_IMAGE.ToString() : objMenu.ImageURL;
            objModelMenu.BlockMenu = objMenu.BlockMenu;
            return objModelMenu;
        }
    }

    /// <summary>
    /// Class using for Ribbon Menu
    /// Mapping database to Model
    /// Hungnd - 21/02/2013
    /// </summary>
    public class MenuRibbonBuilder
    {
        #region Variable

        List<Menu> mainMenu { get; set; }
        List<Menu> MainParentMenu = new List<Menu>();
        List<ItemMenuModel> parentMenuModel;

        #endregion
        public MenuRibbonBuilder(List<Menu> menuByRole)
        {
            mainMenu = menuByRole;
            SetParentMenu();
            SetParentMenuModel();
        }

        #region Properies

        public List<ItemMenuModel> ParentMenuModel
        {
            get { return parentMenuModel; }
            set { parentMenuModel = value; }
        }

        /// <summary>
        /// Set Parent Model
        /// </summary>
        private void SetParentMenuModel()
        {
            this.ParentMenuModel = SetItemsData(MainParentMenu, true);
        }

        /// <summary>
        /// Set main parent Menu
        /// Hungnd - 21/02/2013
        /// </summary>
        private void SetParentMenu()
        {
            if (mainMenu != null)
            {
                MainParentMenu = mainMenu.Where(m => m.ParentID == null 
                    && m.IsActive == true 
                    && m.IsVisible == true).ToList();
            }
            else
            {
                MainParentMenu = new List<Menu>();
            }
        }

        #endregion

        #region Extent Method

        /// <summary>
        /// Set value in database to items in Model (with no sub menu)
        /// Hungnd - 21/02/2013
        /// </summary>
        /// <param name="srcItems"></param>
        /// <param name="IsParent"></param>
        /// <returns></returns>
        /// 
        string DEFAULT_IMAGE = "/Content/images/info.png";
        public List<ItemMenuModel> SetItemsData(List<Menu> srcItems, bool IsParent)
        {
            // mapping model Menu sang model ItemMenuModel
            List<ItemMenuModel> targetItems = new List<ItemMenuModel>();
            srcItems = srcItems.OrderBy(o => o.OrderNumber).ToList();
            foreach (Menu objMenu in srcItems)
            {
                ItemMenuModel objModelMenu = MappingModelValue(objMenu);
                if (!IsParent)
                {
                    List<Menu> lstSubMenuItems = mainMenu.Where(a => a.ParentID == objMenu.MenuID
                        && a.IsVisible == true
                        && a.IsActive == true).ToList();
                    SetMappingSubMenu(objModelMenu, lstSubMenuItems);
                    objModelMenu.ImageUrl = objMenu.ImageURL == null ? DEFAULT_IMAGE.ToString() : objMenu.ImageURL;
                    objModelMenu.IsCategory = true;
                }
                targetItems.Add(objModelMenu);
            }
            //săp xep theo Order
            return targetItems.OrderBy(m => m.Order).ToList();
        }

        /// <summary>
        /// Mapping Sub menu
        /// Hungnd 21/02/2013
        /// </summary>
        /// <param name="obujMenu"></param>
        /// <param name="lstSubItems"></param>
        /// <returns></returns>
        private ItemMenuModel SetMappingSubMenu(ItemMenuModel obujMenu, List<Menu> lstSubItems)
        {
            if (lstSubItems != null && lstSubItems.Count > 0)
            {
                foreach (Menu objMenu in lstSubItems)
                {
                    ItemMenuModel objModelMenu = MappingModelValue(objMenu);
                    obujMenu.MenuChildren.Add(objModelMenu);
                    List<Menu> lstSubMenuItems = mainMenu.Where(a => a.ParentID == objMenu.MenuID
                        && a.IsActive == true
                        && a.IsVisible == true).ToList();
                    SetMappingSubMenu(objModelMenu, lstSubMenuItems);
                }
            }
            return obujMenu;
        }

        /// <summary>
        /// Mapping one to one - database to Model
        /// Hungnd - 21/02/2013
        /// </summary>
        /// <param name="objMenu"></param>
        /// <returns></returns>
        private ItemMenuModel MappingModelValue(Menu objMenu)
        {
            ItemMenuModel objModelMenu = new ItemMenuModel();
            //lay resoucre tuong ung voi MenuName
            string nameRK = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
            if (!string.IsNullOrEmpty(nameRK))
            {
                objModelMenu.Text = nameRK;
            }
            else
            {
                objModelMenu.Text = objMenu.MenuName;
            }
            objModelMenu.ItemMenuID = objMenu.MenuID;
            objModelMenu.ItemParentID = objMenu.ParentID;
            objModelMenu.MenuUrl = objMenu.URL;
            objModelMenu.Visible = objMenu.IsVisible;
            objModelMenu.MenuPath = objMenu.MenuPath.Replace("/", "-");
            objModelMenu.IsCategory = objMenu.IsCategory;
            return objModelMenu;
        }

        #endregion
    }
}

