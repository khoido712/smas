﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.VTUtils.Utils
{
    public class VTDataImageValidation
    {
        /// <summary>
        ///Vị trí sheet
        /// </summary>
        public int SheetIndex { get; set; }
        /// <summary>
        /// Hàng trên cùng bên trái
        /// </summary>
        public int UpperLeftRow { get; set; }
        /// <summary>
        /// Cột bên trái
        /// </summary>
        public int UpperLeftColumn { get; set; }
        /// <summary>
        /// Hàng dưới cùng bên phải
        /// </summary>
        public int LowerRightRow { get; set; }
        /// <summary>
        /// Cột dưới bên phải
        /// </summary>
        public int LowerRightColumn { get; set; }
        /// <summary>
        /// Scale chiều cao
        /// </summary>
        public int HeightScale { get; set; }
        /// <summary>
        /// Scale chiều rộng
        /// </summary>
        public int WidthScale { get; set; }
        /// <summary>
        /// Kiểu format canh chỉnh ảnh
        /// Value FormatFill = {1, 2, 3}
        /// </summary>
        public int TypeFormat { get; set; } 
        /// <summary>
        /// Mảng băm
        /// </summary>
        public byte[] ByteImage { get; set; }
    }

    public enum FormatFill
    {
        Scale = 1,
        UnScale = 2,
        AlignmentLeftRight = 3,
    }
}
