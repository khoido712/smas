﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.VTUtils.Utils
{
    public class VTDataPageNumber
    {
        /// <summary>
        //Sheet cần fill
        /// </summary>
        public int SheetIndex { get; set; }
        /// <summary>
        /// Fill số trang ở header hoặc footer.
        /// Header là false, Footer là true.
        /// </summary>
        public bool IsHeaderOrFooter { get; set; }
        /// <summary>
        /// Căn chỉnh số trang.
        /// 1: Căn trái.
        /// 2: Căn giữa.
        /// 3: Căn phải.
        /// </summary>
        public int AlignPageNumber { get; set; }
        /// <summary>
        /// Số trang bắt đầu của từng sheet
        /// </summary>
        public int StartPageNumberOfSheet { get; set; }
    }
}
