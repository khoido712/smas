﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.VTUtils.Utils
{
    public class SystemParams
    {
        public static Dictionary<string, object> listParams;

        /// <summary>
        /// Lay tham so he thong tuong ung voi key trong bang Adm.SystemParameters
        /// </summary>
        /// <param name="key">key name cua tham so he thong</param>
        /// <returns></returns>
        public static int GetInt(string key)
        {
            string value = (string)listParams[key];
            int result = Convert.ToInt32(value);
            return result;
        }

       
        
    }
}