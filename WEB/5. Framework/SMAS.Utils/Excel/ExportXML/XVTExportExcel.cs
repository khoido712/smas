﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public class XVTExportExcel
    {
        /// <summary>
        /// Create workbook ny file path
        /// </summary>
        /// <param name="templateFilePath"></param>
        /// <returns></returns>
        public static IXVTWorkbook OpenWorkbook(string templateFilePath)
        {
            return new XVTWorkbook(new ClosedXML.Excel.XLWorkbook(templateFilePath));
        }
    }
}
