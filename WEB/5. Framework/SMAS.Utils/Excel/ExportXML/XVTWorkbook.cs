﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClosedXML.Excel;
using System.IO;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public class XVTWorkbook : IXVTWorkbook
    {
        #region Declare Variable
        public ClosedXML.Excel.XLWorkbook Workbook { get; set; }

        /// <summary>
        /// Declare Workbook
        /// </summary>
        /// <param name="workbook"></param>
        public XVTWorkbook(ClosedXML.Excel.XLWorkbook workbook)
        {
            Workbook = workbook;
        }
        #endregion

        #region Method of Object

        /// <summary>
        /// Get Worksheet by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IXVTWorksheet GetSheet(int index)
        {
            return new XVTWorksheet(Workbook.Worksheet(index));
        }

        /// <summary>
        /// Get Worksheet by sheetName
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IXVTWorksheet GetSheet(string sheetName)
        {
            return new XVTWorksheet(Workbook.Worksheet(sheetName));
        }

        /// <summary>
        /// Copy Sheet to Next Position
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="SheetName"></param>
        /// <returns></returns>
        public IXVTWorksheet CopySheet(IXVTWorksheet worksheet, string SheetName = "New Scheet")
        {
            // Check neu sheet name da ton tai them so vao phia sau
            if (Workbook.Worksheets.Where(a => a.Name == SheetName).Count() > 0)
            {
                SheetName = SheetName + "01";
            }
            worksheet.CopyTo(SheetName);
            return new XVTWorksheet(Workbook.Worksheets.Last());
        }

        /// <summary>
        /// Convert Workbook to stream
        /// </summary>
        /// <returns></returns>
        public Stream ToStream()
        {
            MemoryStream OutPut = new MemoryStream();
            Workbook.SaveAs(OutPut);
            if (OutPut != null)
            {
                // return the filestream
                // Rewind the memory stream to the beginning
                OutPut.Seek(0, SeekOrigin.Begin);
            }
            return OutPut;
        }
        #endregion

        #region Properties Of Object
        #endregion
    }
}
