﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Office2010.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NativeExcel;
using System.Drawing;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public class XVTRange : IXVTRange
    {
        public ClosedXML.Excel.IXLRange Range { get; set; }

        /// <summary>
        /// Declare Range
        /// </summary>
        /// <param name="range"></param>
        public XVTRange(ClosedXML.Excel.IXLRange range)
        {
            Range = range;
        }

        public void SetFontStyle(bool bold, bool italic, double? size, bool strikeThrough, bool underline)
        {
            Range.Style.Font.Bold = bold;
            Range.Style.Font.FontColor = XLColor.Black;
            Range.Style.Font.Italic = italic;
            if (size.HasValue) Range.Style.Font.FontSize = size.Value;
            Range.Style.Font.Strikethrough = strikeThrough;
            Range.Style.Font.Underline = underline ? XLFontUnderlineValues.Single : XLFontUnderlineValues.None;
        }

        public void FillColor(System.Drawing.Color color)
        {
            if (color == System.Drawing.Color.Gray)
            {
                Range.Style.Fill.BackgroundColor = XLColor.Gray;
            }
            else if(color == System.Drawing.Color.Green)
            { 
                Range.Style.Fill.BackgroundColor = XLColor.Green;
            }
            else if (color == System.Drawing.Color.Blue)
            {
                Range.Style.Fill.BackgroundColor = XLColor.Blue;
            }
            else if (color == System.Drawing.Color.Yellow)
            {
                Range.Style.Fill.BackgroundColor = XLColor.Yellow;
            }
           
        }

        public void SetHAlign(int position)
        {
            if (position == 0)
            {
                Range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            } 
            else if (position == 6)
            {
                Range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            } 
            else if (position == 7)
            {
                Range.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
            }         
        }


        #region Method of Object
        /// <summary>
        /// Set boder for rande
        /// </summary>
        /// <param name="borderStyle"></param>
        /// <param name="borderIndex"></param>
        public void SetBorder(XLVTBorderStyleValues borderStyle, XVTBorderIndex borderIndex = XVTBorderIndex.OutsideBorder)
        {
            switch (borderIndex)
            {
                case XVTBorderIndex.DiagonalBorder: Range.Style.Border.DiagonalBorder = GetBorderStyle(borderStyle);
                    break;
                case XVTBorderIndex.BottomBorder: Range.Style.Border.BottomBorder = GetBorderStyle(borderStyle);
                    break;
                case XVTBorderIndex.TopBorder: Range.Style.Border.TopBorder = GetBorderStyle(borderStyle);
                    break;
                case XVTBorderIndex.RightBorder: Range.Style.Border.RightBorder = GetBorderStyle(borderStyle);
                    break;
                case XVTBorderIndex.LeftBorder: Range.Style.Border.LeftBorder = GetBorderStyle(borderStyle);
                    break;
                case XVTBorderIndex.AllBorder: 
                        Range.Style.Border.InsideBorder = GetBorderStyle(borderStyle);
                        Range.Style.Border.OutsideBorder = GetBorderStyle(borderStyle);
                    break;
                default: Range.Style.Border.OutsideBorder = GetBorderStyle(borderStyle);
                    break;
            }
        }

        public void SetHAlignCus(ClosedXML.Excel.XLAlignmentHorizontalValues _align)
        {
            ClosedXML.Excel.XLAlignmentHorizontalValues align = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            switch (align)
            {
                case ClosedXML.Excel.XLAlignmentHorizontalValues.CenterContinuous:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.CenterContinuous;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.Distributed:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.Distributed;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.Fill:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.Fill;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.General:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.General;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.Justify:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.Justify;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.Left:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                    break;
                case ClosedXML.Excel.XLAlignmentHorizontalValues.Right:
                    _align = ClosedXML.Excel.XLAlignmentHorizontalValues.Right;
                    break;
            }
            //Range.HorizontalAlignment = _align;
            Range.Style.Alignment.Horizontal = _align;
        }

        public void FillColorCus(ClosedXML.Excel.XLColor color)
        {
            Range.Style.Fill.BackgroundColor = color;
        }

        public void SetFontStyleCus(bool bold, bool italic, bool strikeThrough, bool underline)
        {
            Range.Style.Font.Bold = bold;
            Range.Style.Font.Italic = italic;
            Range.Style.Font.Strikethrough = strikeThrough;
            if (underline) 
                Range.Style.Font.Underline = ClosedXML.Excel.XLFontUnderlineValues.SingleAccounting;
        }

        #endregion

        #region Properties Of Object

        /// <summary>
        /// Set value of range
        /// WriteOnly
        /// </summary>
        public object Value
        {
            set
            {
                Range.Value = value;
            }
        }

        /// <summary>
        /// Set formula
        /// WriteOnly
        /// </summary>
        public string Formula
        {
            set
            {
                Range.FormulaA1 = value;
            }
        }

        /// <summary>
        /// Get Count of rows in range
        /// ReadOnly
        /// </summary>
        public int RowsCount
        {
            get
            {
                return Range.RowCount();
            }
        }

        /// <summary>
        /// Get Count of columns in range
        /// ReadOnly
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return Range.ColumnCount();
            }
        }

        #endregion

        #region Private Function
        private ClosedXML.Excel.XLBorderStyleValues GetBorderStyle(XLVTBorderStyleValues borderStyle)
        {
            ClosedXML.Excel.XLBorderStyleValues lineStyle;
            switch (borderStyle)
            {
                case XLVTBorderStyleValues.Dashed: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Dashed;
                    break;
                case XLVTBorderStyleValues.Dotted: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Dotted;
                    break;
                case XLVTBorderStyleValues.Double: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Double;
                    break;
                case XLVTBorderStyleValues.Medium: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Medium;
                    break;
                case XLVTBorderStyleValues.Thick: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Thick;
                    break;
                default: lineStyle = ClosedXML.Excel.XLBorderStyleValues.Thin;
                    break;
            }
            return lineStyle;
        }
        #endregion
    }
}
