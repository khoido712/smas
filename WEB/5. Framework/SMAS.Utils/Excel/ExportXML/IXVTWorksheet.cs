﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.VTUtils.Excel.Export;
using DocumentFormat.OpenXml.Office2010.Excel;
using ClosedXML.Excel;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public interface IXVTWorksheet
    {
        ClosedXML.Excel.IXLWorksheet Worksheet { get; set; }
        /// <summary>
        /// Get Range by first address and last Address
        /// </summary>
        /// <param name="firstCellAddress"></param>
        /// <param name="lastCellAddress"></param>
        /// <returns></returns>
        IXVTRange GetRange(string firstCellAddress, string lastCellAddress);

        /// <summary>
        /// Get range by ColNum and RowNum
        /// </summary>
        /// <param name="firstRow"></param>
        /// <param name="firstCoumn"></param>
        /// <param name="lastRow"></param>
        /// <param name="lastColumn"></param>
        /// <returns></returns>
        IXVTRange GetRange(int firstRow, int firstCoumn, int lastRow, int lastColumn);

        /// <summary>
        /// Get Range by Address
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <returns></returns>
        IXVTRange GetRange(string CellAddress);

        /// <summary>
        /// Set Cell Value by Address
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <param name="value"></param>
        void SetCellValue(string CellAddress, string value);

        /// <summary>
        /// Set column width
        /// </summary>
        /// <param name="column"></param>
        /// <param name="width"></param>
        void SetColumnWidth(int column, double width);

        /// <summary>
        /// Set column width
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <param name="width"></param>
        void SetColumnWidth(string CellAddress, double width);     

        /// <summary>
        /// set cell value
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="value"></param>
        void SetCellValue(int row, int column, string value);

        /// <summary>
        /// Delete defined Sheet
        /// </summary>
        void Delete();

        /// <summary>
        /// Get or Set Sheet Name
        /// </summary>
        string SheetName { get; set; }

        /// <summary>
        /// copy Sheet
        /// </summary>
        /// <param name="SheetName"></param>
        void CopyTo(string SheetName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SheetName"></param>
        /// <param name="Position"></param>
        void CopyTo(string SheetName, int Position);
         /// <summary>
        /// Copy Anything of row to target row
        /// </summary>
        /// <param name="PoisionTempRow"></param>
        /// <param name="firstRow"></param>
        /// <param name="lastRow"></param>
        void CopyRow(int PoisionTempRow, int firstRow, int lastRow);

        /// <summary>
        /// An dong
        /// </summary>
        /// <param name="Row"></param>
        void HideRow(int Row);

        /// <summary>
        /// Xoa Dong
        /// </summary>
        /// <param name="Row"></param>
        void DeleteRow(int Row);

        /// <summary>
        /// Xet ten font, kick co font
        /// </summary>
        /// <param name="Row"></param>
        void SetFontName(string fontName, decimal fontSize);
    }
}
