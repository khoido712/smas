﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public interface IXVTRange
    {
        ClosedXML.Excel.IXLRange Range { get; set; }
        /// <summary>
        /// Set value of range
        /// WriteOnly
        /// </summary>
        object Value { set; }

        /// <summary>
        /// Set formula
        /// WriteOnly
        /// </summary>
        string Formula { set; }

        /// <summary>
        /// Get Count of rows in range
        /// ReadOnly
        /// </summary>
        int RowsCount { get; }

        /// <summary>
        /// Get Count of columns in range
        /// ReadOnly
        /// </summary>
        int ColumnsCount { get; }

         /// <summary>
        /// Set boder for rande
        /// </summary>
        /// <param name="borderStyle"></param>
        /// <param name="borderIndex"></param>
        void SetBorder(XLVTBorderStyleValues borderStyle, XVTBorderIndex borderIndex = XVTBorderIndex.OutsideBorder);

        void SetHAlignCus(ClosedXML.Excel.XLAlignmentHorizontalValues _align);

        void SetHAlign(int align);

        void FillColorCus(ClosedXML.Excel.XLColor color);

        void FillColor(System.Drawing.Color color);

        void SetFontStyleCus(bool bold, bool italic, bool strikeThrough, bool underline);

        void SetFontStyle(bool bold, bool italic, double? size, bool strikeThrough, bool underline);
    }
}
