﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public interface IXVTWorkbook
    {
        /// <summary>
        /// Get Worksheet by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        IXVTWorksheet GetSheet(int index);

        /// <summary>
        /// Get Worksheet by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        IXVTWorksheet GetSheet(string sheetName);

        /// <summary>
        /// Copy Sheet to Next Position
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="SheetName"></param>
        /// <returns></returns>
        IXVTWorksheet CopySheet(IXVTWorksheet worksheet, string SheetName = "New Scheet");

        /// <summary>
        /// Convert Workbook to stream
        /// </summary>
        /// <returns></returns>
        Stream ToStream();
    }
}
