﻿using SMAS.VTUtils.Excel.ExportXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.Excel.ExportXML
{
    public class XVTWorksheet : IXVTWorksheet
    {
        #region Declare Variable
        public ClosedXML.Excel.IXLWorksheet Worksheet { get; set; }
        /// <summary>
        /// Declate Worksheet
        /// </summary>
        /// <param name="worksheet"></param>
        public XVTWorksheet(ClosedXML.Excel.IXLWorksheet worksheet)
        {
            Worksheet = worksheet;
        }

        /// <summary>
        /// Declate Worksheet
        /// </summary>
        public XVTWorksheet()
        {
        }

        #endregion

        #region Method of Object

        public void SetColumnWidth(int column, double width)
        {
            Worksheet.Column(column).Width = width;
        }

        /// <summary>
        /// Get Range by first address and last Address
        /// </summary>
        /// <param name="firstCellAddress"></param>
        /// <param name="lastCellAddress"></param>
        /// <returns></returns>
        public IXVTRange GetRange(string firstCellAddress, string lastCellAddress)
        {
            return new XVTRange(Worksheet.Range(firstCellAddress, lastCellAddress));
        }

        /// <summary>
        /// Get range by ColNum and RowNum
        /// </summary>
        /// <param name="firstRow"></param>
        /// <param name="firstCoumn"></param>
        /// <param name="lastRow"></param>
        /// <param name="lastColumn"></param>
        /// <returns></returns>
        public IXVTRange GetRange(int firstRow, int firstCoumn, int lastRow, int lastColumn)
        {
            return new XVTRange(Worksheet.Range(firstRow, firstCoumn, lastRow, lastColumn));
        }

        /// <summary>
        /// Get Range by Address
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <returns></returns>
        public IXVTRange GetRange(string CellAddress)
        {
            return new XVTRange(Worksheet.Range(CellAddress));
        }

        /// <summary>
        /// Set Cell Value by Address
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <param name="value"></param>
        public void SetCellValue(string CellAddress, string value)
        {
            IXVTRange XCell = new XVTRange(Worksheet.Range(CellAddress));
            XCell.Value = value;
        }

        /// <summary>
        /// Set Cell Value
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="value"></param>
        public void SetCellValue(int row, int column, string value)
        {
            IXVTRange XCell = new XVTRange(Worksheet.Range(row, column, row, column));
            XCell.Value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CellAddress"></param>
        /// <param name="width"></param>
        public void SetColumnWidth(string CellAddress, double width)
        {
            var col = Worksheet.Columns(CellAddress);
            col.Width = width;
        }  

        /// <summary>
        /// Delete defined Sheet
        /// </summary>
        public void Delete()
        {
            Worksheet.Delete();
        }

        /// <summary>
        /// Copy một khoảng dữ liệu (tập các Cell) sang một khoảng khác trong sheet (Độ cao của các Row sẽ không được Copy)
        /// </summary>
        /// <param name="range">Khoảng dữ liệu (tập các Cell)</param>
        /// <param name="firstRow">Vị trí hàng đầu tiên của khoảng sẽ paste dữ liệu</param>
        /// <param name="firstColumn">Vị trí cột đầu tiên của khoảng sẽ paste dữ liệu</param>
        public void CopyPaste(IXVTRange xRange, int firstRow, int firstColumn, bool copyStyleOnly = false)
        {
            ClosedXML.Excel.IXLRange TempRange = Worksheet.Range(firstRow, firstColumn, firstRow + xRange.RowsCount - 1,
                firstColumn + xRange.ColumnsCount - 1);
            //xRange.Copy(TempRange, type);
        }

        /// <summary>
        /// Copy Anything of row to target row
        /// </summary>
        /// <param name="PoisionTempRow"></param>
        /// <param name="firstRow"></param>
        /// <param name="lastRow"></param>
        public void CopyRow(int PoisionTempRow, int firstRow, int lastRow)
        {
            ClosedXML.Excel.IXLRow TmpRow = Worksheet.Row(PoisionTempRow);
            ClosedXML.Excel.IXLRow TargetRow;
            for (int i = firstRow; i <= lastRow; i++)
            {
                TargetRow = Worksheet.Row(i);
                TmpRow.CopyTo(TargetRow);
            }
        }

        /// <summary>
        /// Copy Sheet
        /// </summary>
        /// <param name="SheetName"></param>
        public void CopyTo(string SheetName)
        {
                Worksheet.CopyTo(SheetName);
        }
        /// <summary>
        /// Copy Sheet
        /// </summary>
        /// <param name="SheetName"></param>
        public void CopyTo(string SheetName, int Position)
        {

            Worksheet.CopyTo(SheetName, Position);
        }

        /// <summary>
        /// Hide Row
        /// </summary>
        /// <param name="Row"></param>
        public void HideRow(int Row)
        {
            Worksheet.Row(Row).Hide();
        }

        /// <summary>
        /// Delete Row
        /// </summary>
        /// <param name="Row"></param>
        public void DeleteRow(int Row)
        {
            Worksheet.Row(Row).Delete();
        }

        /// <summary>
        /// Set Font Name
        /// </summary>
        /// <param name="Row"></param>
        public void SetFontName(string fontName, decimal fontSize) 
        {
            if (!String.IsNullOrEmpty(fontName))
            {
                Worksheet.Style.Font.FontName = fontName;
            }
            if (fontSize > 0)
            {
                Worksheet.Style.Font.FontSize = (double)fontSize;
            }
        }


        ///// <summary>
        ///// Delete Row
        ///// </summary>
        ///// <param name="Row"></param>
        //public void SetFontStyle(int Row)
        //{
        //    Worksheet.Range(
        //}

        #endregion

        #region Properties


        

        /// <summary>
        /// Get or Set Sheet Name
        /// </summary>
        public string SheetName
        {
            get
            {
                return Worksheet.Name;
            }
            set
            {
                Worksheet.Name = value;
            }
        }

        #endregion
    }
}
