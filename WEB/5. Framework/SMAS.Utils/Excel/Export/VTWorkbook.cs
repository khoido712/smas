﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using log4net;
using SMAS.VTUtils.Utils;
using SMAS.VTUtils.Log;

namespace SMAS.VTUtils.Excel.Export
{
    public class VTWorkbook : IVTWorkbook
    {
        //private string FOLDER_SAVE_FILE = "BaoCaoSoDanhGiaHS";
        private static ILog logger = LogManager.GetLogger(typeof(VTWorkbook).Name);
        public NativeExcel.IWorkbook Workbook { get; set; }
        public NPOI.HSSF.UserModel.HSSFWorkbook HWorkbook { get; set; }

        public VTWorkbook(NativeExcel.IWorkbook workbook)
        {
            Workbook = workbook;
        }
        public IVTWorksheet GetSheet(int index)
        {
            return new VTWorksheet(Workbook.Worksheets[index]);
        }

        public IVTWorksheet GetSheet(string sheetName)
        {
            return new VTWorksheet(Workbook.Worksheets[sheetName]);
        }


        public List<IVTWorksheet> GetSheets()
        {
            List<IVTWorksheet> list = new List<IVTWorksheet>();
            foreach (NativeExcel.IWorksheet sheet in Workbook.Worksheets)
                list.Add(new VTWorksheet(sheet));
            return list;
        }

        public IVTWorksheet CopySheetToLast(IVTWorksheet worksheet, string lastCell = null)
        {
            NativeExcel.IWorksheet templateSheet = worksheet.Worksheet;
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddAfter(Workbook.Worksheets.Count);

            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            if (lastCell == null)
            {
                vtSheet.CopySheet(worksheet);
            }
            else
            {
                vtSheet.CopySheet(worksheet, lastCell);
            }
            return vtSheet;
        }

        public IVTWorksheet CopySheetToBeforeLast(IVTWorksheet worksheet, string lastCell = null)
        {
            NativeExcel.IWorksheet templateSheet = worksheet.Worksheet;
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddBefore(Workbook.Worksheets.Count);
            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            if (lastCell == null)
            {
                vtSheet.CopySheet(worksheet);
            }
            else
            {
                vtSheet.CopySheet(worksheet, lastCell);
            }
            return vtSheet;
        }

        public IVTWorksheet CopySheetToBefore(IVTWorksheet worksheet, IVTWorksheet beforeSheet)
        {
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddBefore(beforeSheet.Worksheet);
            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            vtSheet.CopySheet(worksheet);
            return vtSheet;
        }

        public IVTWorksheet CopySheetToBefore(IVTWorksheet worksheet, int index)
        {
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddBefore(index);
            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            vtSheet.CopySheet(worksheet);
            return vtSheet;
        }

        public IVTWorksheet CopySheetToBefore(IVTRange worksheetRange, IVTWorksheet beforeSheet, string sheetname = "")
        {
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddBefore(beforeSheet.Worksheet);
            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            vtSheet.CopyPasteSameSize(worksheetRange, 1, 1);
            vtSheet.Name = sheetname;
            return vtSheet;
        }

        public IVTWorksheet CopySheetToBefore(IVTRange worksheetRange, int index, string sheetname = "")
        {
            NativeExcel.IWorksheet sheet = Workbook.Worksheets.AddBefore(index);
            IVTWorksheet vtSheet = new VTWorksheet(sheet);
            vtSheet.CopyPasteSameSize(worksheetRange, 1, 1);
            vtSheet.Name = sheetname;
            return vtSheet;
        }

        public void SaveToFile(string pathFile)
        {
            Workbook.SaveAs(pathFile);
        }

        public Stream ToStream()
        {
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            Workbook.SaveAs(pathFile);
            RemoveNativeDemoText_NPOI(pathFile);
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
            return memStream;
        }

        public Stream ToStream(int removeRow)
        {
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\" + Guid.NewGuid().ToString() + ".xls";
            String pathFile2 = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\" + Guid.NewGuid().ToString() + ".xls";
            Aspose.Cells.Workbook workbook2 = null;
            try
            {
                Workbook.SaveAs(pathFile);
                VTUtils.Pdf.PdfExport export = new VTUtils.Pdf.PdfExport();
                workbook2 = new Aspose.Cells.Workbook(pathFile);
                int sheet = workbook2.Worksheets.Count;
                for (int i = 0; i < sheet; i++)
                {
                    workbook2.Worksheets[i].Cells.DeleteRow(removeRow);
                }

                workbook2.Save(pathFile2);
                MemoryStream memStream;
                using (FileStream fileStream = File.OpenRead(pathFile2))
                {
                    memStream = new MemoryStream();
                    memStream.SetLength(fileStream.Length);
                    fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                }
                return memStream;
            }
            finally
            {
                FileInfo fi = new FileInfo(pathFile);
                fi.Delete();
                FileInfo fi2 = new FileInfo(pathFile2);
                fi2.Delete();
                if (workbook2 != null)
                {
                    workbook2.Dispose();
                }
            }
        }



        public void CreateZipFile(string fileName, string folder)
        {
            String FileSaveFolder = folder + "\\" + fileName;
            Workbook.SaveAs(FileSaveFolder);
            RemoveNativeDemoText_NPOI(FileSaveFolder);
        }    

        public Stream ToStream(bool DeleteRowFirst, bool copyHeightWhileShift = false)
        {
            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            Workbook.SaveAs(pathFile);
            RemoveNativeDemoText_NPOI(pathFile, DeleteRowFirst, copyHeightWhileShift);
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
            return memStream;
        }

        public Stream ToStreamNumberValidationData(int sheetIndex, decimal fromNumber, decimal toNumber, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            Workbook.SaveAs(pathFile);
            CreateDataValidation(pathFile, sheetIndex, fromNumber, toNumber, fromRow, fromColumn, toRow, toColumn);
            RemoveNativeDemoText_NPOI(pathFile);
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
            return memStream;
        }

        public void CreateDataValidation(string filePath, int sheetIndex, decimal fromNumber, decimal toNumber, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);
            if (hssfworkbook != null)
            {
                CellRangeAddressList rangeList = new CellRangeAddressList(fromRow - 1, toRow - 1, fromColumn - 1, toColumn - 1); // Vì NPOI sử dụng row và column bắt đầu từ 0
                DVConstraint dvconstraint = DVConstraint.CreateNumericConstraint(2, NPOI.SS.UserModel.OperatorType.BETWEEN, fromNumber.ToString(), toNumber.ToString());
                HSSFDataValidation dataValidation = new HSSFDataValidation(rangeList, dvconstraint);
                HSSFSheet sheet = (HSSFSheet)hssfworkbook.GetSheetAt(sheetIndex - 1);
                if (sheet != null)
                {
                    sheet.AddValidationData(dataValidation);
                    file = new FileStream(filePath, FileMode.Create);
                    hssfworkbook.Write(file);
                    if (file != null)
                    {
                        file.Close();
                        file.Dispose();
                    }
                }
            }
            if (file != null)
            {
                file.Close();
                file.Dispose();
            }
        }

        public Stream ToStreamNumberValidationData(int sheetIndex, string[] lstContrains, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            Workbook.SaveAs(pathFile);
            CreateDataValidation(pathFile, sheetIndex, lstContrains, fromRow, fromColumn, toRow, toColumn);
            RemoveNativeDemoText_NPOI(pathFile);
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
            return memStream;
        }

        public Stream ToStreamValidationData(List<VTDataValidation> lstSheet)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            Workbook.SaveAs(pathFile);
            FileStream file = new FileStream(pathFile, FileMode.Open, FileAccess.ReadWrite);
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);
            if (hssfworkbook != null)
            {
                VTDataValidation objValidation;
                CellRangeAddressList rangeList;
                DVConstraint dvconstraint = null;
                HSSFDataValidation dataValidation;
                HSSFSheet sheet;
                for (int i = 0; i < lstSheet.Count; i++)
                {
                    objValidation = lstSheet[i];
                    rangeList = new CellRangeAddressList(objValidation.FromRow - 1, objValidation.ToRow - 1, objValidation.FromColumn - 1, objValidation.ToColumn - 1);
                    if (objValidation.Type == VTValidationType.LIST)
                    {
                        dvconstraint = DVConstraint.CreateExplicitListConstraint(objValidation.Contrains);
                    }
                    else if (objValidation.Type == NPOI.SS.UserModel.ValidationType.INTEGER)
                    {
                        dvconstraint = DVConstraint.CreateNumericConstraint(VTValidationType.INTEGER, NPOI.SS.UserModel.OperatorType.BETWEEN, objValidation.FromValue, objValidation.ToValue);
                    }
                    else if (objValidation.Type == NPOI.SS.UserModel.ValidationType.DECIMAL)
                    {
                        dvconstraint = DVConstraint.CreateNumericConstraint(VTValidationType.DECIMAL, NPOI.SS.UserModel.OperatorType.BETWEEN, objValidation.FromValue, objValidation.ToValue);
                    }
                    else if (objValidation.Type == NPOI.SS.UserModel.ValidationType.TEXT_LENGTH)
                    {
                        dvconstraint = DVConstraint.CreateNumericConstraint(VTValidationType.TEXT_LENGTH, NPOI.SS.UserModel.OperatorType.LESS_OR_EQUAL, objValidation.MinLength, objValidation.MaxLength);
                    }

                    if (dvconstraint != null)
                    {
                        dataValidation = new HSSFDataValidation(rangeList, dvconstraint);
                        sheet = (HSSFSheet)hssfworkbook.GetSheetAt(objValidation.SheetIndex - 1);
                        if (sheet != null)
                        {
                            sheet.AddValidationData(dataValidation);
                        }
                    }
                }
                file = new FileStream(pathFile, FileMode.Create);
                hssfworkbook.Write(file);
                file.Close();
                file.Dispose();
            }

            RemoveNativeDemoText_NPOI(pathFile);
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(pathFile))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            FileInfo fi = new FileInfo(pathFile);
            fi.Delete();
            return memStream;
        }
        public void CreateDataValidation(string filePath, int sheetIndex, string[] lstContrains, int fromRow, int fromColumn, int toRow, int toColumn)
        {
            FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);
            if (hssfworkbook != null)
            {
                CellRangeAddressList rangeList = new CellRangeAddressList(fromRow - 1, toRow - 1, fromColumn - 1, toColumn - 1); // Vì NPOI sử dụng row và column bắt đầu từ 0
                DVConstraint dvconstraint = DVConstraint.CreateExplicitListConstraint(lstContrains);
                HSSFDataValidation dataValidation = new HSSFDataValidation(rangeList, dvconstraint);
                HSSFSheet sheet = (HSSFSheet)hssfworkbook.GetSheetAt(sheetIndex - 1);
                if (sheet != null)
                {
                    sheet.AddValidationData(dataValidation);
                    file = new FileStream(filePath, FileMode.Create);
                    hssfworkbook.Write(file);
                    if (file != null)
                    {
                        file.Close();
                        file.Dispose();
                    }
                }
            }
            if (file != null)
            {
                file.Close();
                file.Dispose();
            }

        }
        /// <summary>
        /// Remove demoText using NPOI API
        /// Chuẩn hóa Formula để hổ trợ các function mà Native không hổ trợ.
        /// Hungnd 22/02/2012 khi exception return
        /// </summary>
        /// <param name="fileName">đường dẫn đến file Excel cần thực hiện</param>
        private static void RemoveNativeDemoText_NPOI(string filePath, bool DeleteRowFirst = false, bool copyHeightWhileShift = false)
        {
            FileStream file = null;
            try
            {
                file = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
                HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);
                for (int i = 0; i < hssfworkbook.NumberOfSheets; i++)
                {
                    HSSFSheet sheet = (HSSFSheet)hssfworkbook.GetSheetAt(i);
                    IRow blankRow = sheet.CreateRow(0);
                    ICell blankCell = blankRow.CreateCell(0, CellType.STRING);
                    blankCell.SetCellValue(string.Empty);
                    int lastRowNum = sheet.LastRowNum;
                    if (DeleteRowFirst)
                    {
                        //xoa dong dau tien (23/12/2013 - Hungnd)
                        ISheet isheet = hssfworkbook.GetSheetAt(i);
                        int rowIndex = isheet.GetRow(0).RowNum;
                        if (rowIndex >= 0 && rowIndex < lastRowNum)
                        {
                            if (copyHeightWhileShift)
                            {
                                sheet.ShiftRows(rowIndex + 1, lastRowNum, -1, true, false);
                            }
                            else
                            {
                                sheet.ShiftRows(rowIndex + 1, lastRowNum, -1);
                            }
                        }
                    }
                    for (int j = 0; j <= lastRowNum; j++)
                    {
                        IRow row = sheet.GetRow(j);
                        if (row == null) continue;
                        for (int k = 0; k <= row.LastCellNum; k++)
                        {
                            ICell cell = row.GetCell(k);
                            if (cell != null)
                            {
                                if (cell.CellType == CellType.STRING)
                                {
                                    if (cell.StringCellValue.StartsWith("="))
                                    {
                                        cell.CellFormula = cell.StringCellValue.Substring(1);
                                    }
                                }
                            }
                        }
                    }
                }
                file = new FileStream(filePath, FileMode.Create);
                hssfworkbook.Write(file);
            }
            catch (Exception ex)
            {                
                string paramList = string.Format("Path: {0}, DeleteRowFirst={1}, copyHeightWhileShift={2}", filePath, DeleteRowFirst, copyHeightWhileShift);
                LogExtensions.ErrorExt(logger, DateTime.Now, "RemoveNativeDemoText_NPOI", paramList, ex);

                return;
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                    file.Dispose();
                }
            }
        }

        #region insert image && page number
        public Stream InsertImage(List<VTDataImageValidation> listDataImageValidation, List<VTDataPageNumber> listDataPageNumber = null)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\Excels\\" + Guid.NewGuid().ToString() + ".xls";
            string pathfile2 = "";
            Workbook.SaveAs(pathFile);
            RemoveNativeDemoText_NPOI(pathFile);
            if (!Pdf.PdfExport.setLicense)
            {
                Utils.RemoveLicense.ActivateMemoryPatching();
            }
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(pathFile);
            Aspose.Cells.Worksheet sheet = null;
            MemoryStream memoryStream = new MemoryStream();
            
            try
            {
                VTDataImageValidation objValidation;
                for (int i = 0; i < listDataImageValidation.Count; i++)
                {
                    objValidation = listDataImageValidation[i];
                    if (objValidation.ByteImage != null && objValidation.ByteImage.Length > 0)
                    {
                        memoryStream = new MemoryStream();
                        memoryStream.Write(objValidation.ByteImage, 0, objValidation.ByteImage.Length);
                        sheet = workbook.Worksheets[(objValidation.SheetIndex - 1)];

                        if (objValidation.TypeFormat == (int)FormatFill.Scale)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), memoryStream, objValidation.WidthScale, objValidation.HeightScale);
                        }
                        else if (objValidation.TypeFormat == (int)FormatFill.UnScale)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), memoryStream);
                        }
                        else if (objValidation.TypeFormat == (int)FormatFill.AlignmentLeftRight)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), objValidation.LowerRightRow, objValidation.LowerRightColumn, memoryStream);
                        }                        
                    }
                }

                if (listDataPageNumber != null && listDataPageNumber.Count() > 0)
                {
                    VTDataPageNumber objPageNumber = null;
                    for (int p = 0; p < listDataPageNumber.Count(); p++)
                    {
                        objPageNumber = listDataPageNumber[p];
                        if (objPageNumber.SheetIndex >= 1)
                        {                           
                            sheet = workbook.Worksheets[(objPageNumber.SheetIndex - 1)];
                            sheet.PageSetup.IsAutoFirstPageNumber = false;
                            sheet.PageSetup.IsHFScaleWithDoc = true;
                            sheet.PageSetup.IsHFAlignMargins = true;
                            //sheet.PageSetup.FitToPagesWide = 1;
                            sheet.PageSetup.FirstPageNumber = objPageNumber.StartPageNumberOfSheet; 
                            sheet.PageSetup.SetFooter((objPageNumber.AlignPageNumber -1), "&P");
                        }
                    }                
                }

                pathfile2 = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\Excels\\" + Guid.NewGuid().ToString() + ".xls";
                workbook.Save(pathfile2);
                                
                using (FileStream fileStream = File.OpenRead(pathfile2))
                {
                    var memoryStream2 = new MemoryStream();
                    memoryStream2.SetLength(fileStream.Length);
                    fileStream.Read(memoryStream2.GetBuffer(), 0, (int)fileStream.Length);
                    return memoryStream2;
                }

            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();

                }
                if (!string.IsNullOrEmpty(pathFile))
                {
                    new FileInfo(pathFile).Delete();
                }
                
                if (!string.IsNullOrEmpty(pathfile2))
                {
                    new FileInfo(pathfile2).Delete();
                }
            }
        }

        public void CreateZipFile(string fileName, string folder, List<VTDataImageValidation> listDataImageValidation, List<VTDataPageNumber> listDataPageNumber = null)
        {
            String FileSaveFolder = folder + "\\" + fileName;
            InsertImage(listDataImageValidation, FileSaveFolder, listDataPageNumber);
        }

        private void InsertImage(List<VTDataImageValidation> listDataImageValidation, string FileSaveFolder, List<VTDataPageNumber> listDataPageNumber = null)
        {
            string pathFile = FileSaveFolder;
            string pathfile2 = "";
            Workbook.SaveAs(pathFile);
            RemoveNativeDemoText_NPOI(pathFile);
            if (!Pdf.PdfExport.setLicense)
            {
                Utils.RemoveLicense.ActivateMemoryPatching();
            }
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(pathFile);
            Aspose.Cells.Worksheet sheet = null;
            MemoryStream memoryStream = new MemoryStream();

            try
            {
                VTDataImageValidation objValidation;
                for (int i = 0; i < listDataImageValidation.Count; i++)
                {
                    objValidation = listDataImageValidation[i];
                    if (objValidation.ByteImage != null && objValidation.ByteImage.Length > 0)
                    {
                        memoryStream = new MemoryStream();
                        memoryStream.Write(objValidation.ByteImage, 0, objValidation.ByteImage.Length);
                        sheet = workbook.Worksheets[(objValidation.SheetIndex - 1)];

                        if (objValidation.TypeFormat == (int)FormatFill.Scale)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), memoryStream, objValidation.WidthScale, objValidation.HeightScale);
                        }
                        else if (objValidation.TypeFormat == (int)FormatFill.UnScale)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), memoryStream);
                        }
                        else if (objValidation.TypeFormat == (int)FormatFill.AlignmentLeftRight)
                        {
                            sheet.Pictures.Add((objValidation.UpperLeftRow - 1), (objValidation.UpperLeftColumn - 1), objValidation.LowerRightRow, objValidation.LowerRightColumn, memoryStream);
                        }
                    }
                }

                if (listDataPageNumber != null && listDataPageNumber.Count() > 0)
                {
                    VTDataPageNumber objPageNumber = null;
                    for (int p = 0; p < listDataPageNumber.Count(); p++)
                    {
                        objPageNumber = listDataPageNumber[p];
                        if (objPageNumber.SheetIndex >= 1)
                        {
                            sheet = workbook.Worksheets[(objPageNumber.SheetIndex - 1)];
                            sheet.PageSetup.IsAutoFirstPageNumber = false;
                            sheet.PageSetup.IsHFScaleWithDoc = true;
                            sheet.PageSetup.IsHFAlignMargins = true;
                            //sheet.PageSetup.FitToPagesWide = 1;
                            sheet.PageSetup.FirstPageNumber = objPageNumber.StartPageNumberOfSheet;
                            sheet.PageSetup.SetFooter((objPageNumber.AlignPageNumber - 1), "&P");
                        }
                    }
                }


                pathfile2 = FileSaveFolder;
                workbook.Save(pathfile2);

                using (FileStream fileStream = File.OpenRead(pathfile2))
                {
                    var memoryStream2 = new MemoryStream();
                    memoryStream2.SetLength(fileStream.Length);
                    fileStream.Read(memoryStream2.GetBuffer(), 0, (int)fileStream.Length);
                    workbook.Save(memoryStream2, Aspose.Cells.SaveFormat.Excel97To2003);                    
                }
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();

                }
            }
        }

        public Stream FillPageNumber(List<VTDataPageNumber> listDataPageNumber)
        {
            string pathFile = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\Excels\\" + Guid.NewGuid().ToString() + ".xls";
            string pathfile2 = "";
            Workbook.SaveAs(pathFile);
            RemoveNativeDemoText_NPOI(pathFile);
            if (!Pdf.PdfExport.setLicense)
            {
                Utils.RemoveLicense.ActivateMemoryPatching();
            }
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(pathFile);
            Aspose.Cells.Worksheet sheet = null;
            MemoryStream memoryStream = new MemoryStream();

            try
            {
                if (listDataPageNumber != null && listDataPageNumber.Count() > 0)
                {
                    VTDataPageNumber objPageNumber = null;
                    for (int p = 0; p < listDataPageNumber.Count(); p++)
                    {
                        objPageNumber = listDataPageNumber[p];
                        if (objPageNumber.SheetIndex >= 1)
                        {
                            sheet = workbook.Worksheets[(objPageNumber.SheetIndex - 1)];
                            sheet.PageSetup.IsAutoFirstPageNumber = false;
                            sheet.PageSetup.IsHFScaleWithDoc = true;
                            sheet.PageSetup.IsHFAlignMargins = true;
                            //sheet.PageSetup.FitToPagesWide = 1;
                            sheet.PageSetup.FirstPageNumber = objPageNumber.StartPageNumberOfSheet;
                            sheet.PageSetup.SetFooter((objPageNumber.AlignPageNumber - 1), "&P");
                        }
                    }
                }

                pathfile2 = AppDomain.CurrentDomain.BaseDirectory + "Uploads\\Excels\\" + Guid.NewGuid().ToString() + ".xls";
                workbook.Save(pathfile2);

                using (FileStream fileStream = File.OpenRead(pathfile2))
                {
                    var memoryStream2 = new MemoryStream();
                    memoryStream2.SetLength(fileStream.Length);
                    fileStream.Read(memoryStream2.GetBuffer(), 0, (int)fileStream.Length);
                    return memoryStream2;
                }

            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();

                }
                if (!string.IsNullOrEmpty(pathFile))
                {
                    new FileInfo(pathFile).Delete();
                }

                if (!string.IsNullOrEmpty(pathfile2))
                {
                    new FileInfo(pathfile2).Delete();
                }
            }
        }
        #endregion
    }
}
