﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SMAS.VTUtils.Excel.Export
{
    public interface IVTWorkbook
    {
        /// <summary>
        /// Lay danh sach cac sheet trong book
        /// </summary>
        /// <returns></returns>
        List<IVTWorksheet> GetSheets();

        /// <summary>
        /// Lấy Sheet trong Workbook
        /// </summary>
        /// <param name="index">Số thứ tự của sheet, tính từ 1</param>
        /// <returns></returns>
        IVTWorksheet GetSheet(int index);

        IVTWorksheet GetSheet(string sheetName);

        /// <summary>
        /// Tạo mới một sheet đặt ở cuối Excel và copy dữ liệu từ một sheet có sẵn (copy toàn bộ hoặc 1 phần)
        /// </summary>
        /// <param name="worksheet">Sheet cần copy</param>
        /// <param name="lastCell">Cell cuối cùng cần copy (nếu copy một phần), null nếu copy toàn bộ</param>
        /// <returns></returns>
        IVTWorksheet CopySheetToLast(IVTWorksheet worksheet, string lastCell = null);

        /// <summary>
        /// Copy 1 sheet truoc 1 sheet khac
        /// </summary>
        /// <param name="worksheet">worksheet can copy</param>
        /// <param name="beforeSheet">copy den truoc beforeSheet</param>
        /// <returns></returns>
        IVTWorksheet CopySheetToBefore(IVTWorksheet worksheet, IVTWorksheet beforeSheet);

        /// <summary>
        /// Copy 1 sheet den mot vi tri cho truoc
        /// </summary>
        /// <param name="worksheet">worksheet can copy</param>
        /// <param name="index">vi tri can copy toi</param>
        /// <returns></returns>
        IVTWorksheet CopySheetToBefore(IVTWorksheet worksheet, int index);

        /// <summary>
        /// Lưu vào một file
        /// </summary>
        /// <param name="pathFile">Đường dẫn file + tên file</param>
        void SaveToFile(string pathFile);

        /// <summary>
        /// Tạo mới một sheet đặt ở ngay trước sheet cuối Excel và copy dữ liệu từ một sheet có sẵn
        /// </summary>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        IVTWorksheet CopySheetToBeforeLast(IVTWorksheet worksheet, string lastCell = null);

        /// <summary>
        /// Chuyển sang định dạng Stream
        /// </summary>
        Stream ToStream();

        /// <summary>
        /// Chuyen file excel sang dinh dang Stream, xoa dong dau tien
        /// </summary>
        /// <param name="removeRow"></param>
        /// <returns></returns>
        Stream ToStream(int removeRow);
        //Stream ToStream(Aspose.Cells.SaveFormat saveFormatOutPut);

        /// <summary>
        /// ham tao nhieu file thanh file zip
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassName"></param>
        /// <param name="semesterName"></param>
        void CreateZipFile(string fileName,string newFolder);

        /// <summary>
        /// Xuat va xoa dong dau tien
        /// </summary>
        /// <param name="DeleteRowFirst"></param>
        /// <returns></returns>
        Stream ToStream(bool DeleteRowFirst, bool copyHeightWhileShift = false);

        /// <summary>
        /// <para>Tao validation chi cho phep theo dinh dang da chon trong cell</para>
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="sheetIndex"></param>
        /// <param name="fromNumber"></param>
        /// <param name="toNumber"></param>
        /// <param name="fromRow"></param>
        /// <param name="fromColumn"></param>
        /// <param name="toRow"></param>
        /// <param name="toColumn"></param>
        Stream ToStreamNumberValidationData(int sheetIndex, decimal fromNumber, decimal toNumber, int fromRow, int fromColumn, int toRow, int toColumn);

        Stream ToStreamNumberValidationData(int sheetIndex, string[] lstContrains, int fromRow, int fromColumn, int toRow, int toColumn);

        /// <summary>
        /// CopySheetToBefore
        /// </summary>
        /// <param name="worksheetRange">The worksheet range.</param>
        /// <param name="beforeSheet">The before sheet.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 14/01/2013   9:07 AM
        /// </remarks>
        IVTWorksheet CopySheetToBefore(IVTRange worksheetRange, IVTWorksheet beforeSheet, string sheetname = "");

        /// <summary>
        /// CopySheetToBefore
        /// </summary>
        /// <param name="worksheetRange">The worksheet range.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 14/01/2013   9:07 AM
        /// </remarks>
        IVTWorksheet CopySheetToBefore(IVTRange worksheetRange, int index, string sheetname = "");
        /// <summary>
        /// Chiendd: 31/10/2014
        /// Tao validation cho Sheet. Neu file excel co nhieu sheet thi dien thong tin tung sheet
        /// </summary>
        /// <param name="lstSheet"></param>
        /// <returns></returns>
        Stream ToStreamValidationData(List<VTDataValidation> lstSheet);

        /// <summary>
        /// Chèn ảnh
        /// Đánh số trang nếu có
        /// </summary>
        /// <param name="listDataImageValidation">Danh sách các tham số dữ liệu</param>
        /// <returns></returns>
        Stream InsertImage(List<SMAS.VTUtils.Utils.VTDataImageValidation> listDataImageValidation, List<SMAS.VTUtils.Utils.VTDataPageNumber> listDataPageNumber = null);

        /// <summary>
        /// Insert ảnh vào excel và tạo thành file zip.
        /// Đánh số trang (nếu có)
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="folder"></param>
        /// <param name="listDataImageValidation"></param>
        void CreateZipFile(string fileName, string folder, List<SMAS.VTUtils.Utils.VTDataImageValidation> listDataImageValidation, List<SMAS.VTUtils.Utils.VTDataPageNumber> listDataPageNumber = null);

        /// <summary>
        /// Đánh số trang
        /// </summary>
        /// <param name="listDataPageNumber"></param>
        /// <returns></returns>
        Stream FillPageNumber(List<SMAS.VTUtils.Utils.VTDataPageNumber> listDataPageNumber);
    }
}
