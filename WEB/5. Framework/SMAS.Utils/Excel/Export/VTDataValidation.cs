﻿using System;
namespace SMAS.VTUtils.Excel.Export
{
    public class VTDataValidation
    {
        public int SheetIndex { get; set; }
        public string[] Contrains { get; set; }
        public int FromRow { get; set; }
        public int ToRow { get; set; }
        public int FromColumn { get; set; }
        public int ToColumn { get; set; }
        /// <summary>
        /// FromValue, ToValue Chi ap dung khi validate so
        /// </summary>
        public string FromValue { get; set; }
        /// <summary>
        /// FromValue, ToValue Chi ap dung khi validate so
        /// </summary>
        public string ToValue{get;set;}
        /// <summary>
        /// Type=3: Tao combobob, Type=2: Validate number decimal, type=1: Validate number interger
        /// </summary>
        public int Type { get; set; }

        public string MinLength { get; set; }
        public string MaxLength { get; set; }
       
    }

    
}
