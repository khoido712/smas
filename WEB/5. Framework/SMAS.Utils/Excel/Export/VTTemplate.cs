﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ClosedXML.Excel;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Excel.ExportXML;

namespace SMAS.VTUtils.Excel.Export
{

    /// <summary>
    /// Sử dụng để đưa dữ liệu excel vào list
    /// </summary>
    public static class VTTemplate
    {
        public static IXLWorksheet Binding(this IXLWorksheet sheet, IDictionary<string, object> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            string cell;
            foreach (KeyValuePair<string, object> dic in data)
            {
                if (!string.IsNullOrWhiteSpace(dic.Key) && dic.Value != null)
                {
                    cell = dic.Key.ToUpper();
                    sheet.Cell(cell).Value = (dic.Value ?? "").ToString();
                }
            }
            return sheet;
        }

        public static IXVTWorksheet Binding(this IXVTWorksheet sheet, IDictionary<string, object> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            string cell;
            foreach (KeyValuePair<string, object> dic in data)
            {
                if (!string.IsNullOrWhiteSpace(dic.Key) && dic.Value != null)
                {
                    cell = dic.Key.ToUpper();
                    sheet.SetCellValue(cell, (dic.Value ?? "").ToString());
                }
            }
            return sheet;
        }

        /// <summary>
        ///  Đưa dữ liệu vào sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IXVTWorksheet Binding(this IXVTWorksheet sheet, List<IDictionary<string, object>> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            string cell;
            object value;
            foreach (IDictionary<string, object> dic in data)
            {
                foreach (var key in dic.Keys)
                {
                    if (!string.IsNullOrWhiteSpace(key) && dic.TryGetValue(key, out value))
                    {
                        cell = key.ToUpper();
                        sheet.SetCellValue(cell, (value ?? "").ToString());
                    }
                }
            }
            return sheet;
        }
        /// <summary>
        /// Đưa dữu liệu vào sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="startRow"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IXVTWorksheet Binding(this IXVTWorksheet sheet, int startRow, List<IDictionary<string, object>> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            if (startRow <= 0)
            {
                startRow = 0;
            }
            object value;
            var i = startRow;
            foreach (IDictionary<string, object> dic in data)
            {
                foreach (string key in dic.Keys)
                {
                    if (!string.IsNullOrWhiteSpace(key) && dic.TryGetValue(key, out value))
                    {
                        sheet.SetCellValue((key + i).ToUpper(), (value ?? "").ToString());
                    }
                }
                i++;
            }
            return sheet;
        }

        public static IVTWorksheet Binding(this IVTWorksheet sheet, Dictionary<string, object> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            string cell;
            foreach (KeyValuePair<string, object> dic in data)
            {
                if (!string.IsNullOrWhiteSpace(dic.Key) && dic.Value != null)
                {
                    cell = dic.Key.ToUpper();
                    sheet.SetCellValue(cell, (dic.Value ?? "").ToString());
                }
            }
            return sheet;
        }

        /// <summary>
        ///  Đưa dữ liệu vào sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IVTWorksheet Binding(this IVTWorksheet sheet, List<Dictionary<string, object>> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            string cell;
            object value;
            foreach (IDictionary<string, object> dic in data)
            {
                foreach (var key in dic.Keys)
                {
                    if (!string.IsNullOrWhiteSpace(key) && dic.TryGetValue(key, out value))
                    {
                        cell = key.ToUpper();
                        sheet.SetCellValue(cell, (value ?? "").ToString());
                    }
                }
            }
            return sheet;
        }
        /// <summary>
        /// Đưa dữu liệu vào sheet
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="startRow"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IVTWorksheet Binding(this IVTWorksheet sheet, int startRow, List<Dictionary<string, object>> data)
        {
            if (sheet == null)
            {
                return sheet;
            }
            if (data == null || !data.Any())
            {
                return sheet;
            }
            if (startRow <= 0)
            {
                startRow = 0;
            }
            object value;
            var i = startRow;
            foreach (IDictionary<string, object> dic in data)
            {
                foreach (string key in dic.Keys)
                {
                    if (!string.IsNullOrWhiteSpace(key) && dic.TryGetValue(key, out value))
                    {
                        sheet.SetCellValue((key + i).ToUpper(), (value ?? "").ToString());
                    }
                }
                i++;
            }
            return sheet;
        }
    }
}
