﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace SMAS.VTUtils.HtmlExport
{
    public class VTHtmlExport
    {
        public const string TEMPLATE_PART_START = "<!-- {0} -->";
        public const string TEMPLATE_PART_END = "<!-- End {0} -->";
        public string TemplateText { get; set; }
        public Dictionary<string, string> PartData { get; set; }
        public Dictionary<string, string> PartTemplate { get; set; }

        public VTHtmlExport(string templatePath)
        {
            StreamReader streamReader = new StreamReader(templatePath);
            this.TemplateText = streamReader.ReadToEnd();
            streamReader.Close();
            streamReader.Dispose();
            PartData = new Dictionary<string, string>();
            PartTemplate = new Dictionary<string, string>();

            MatchCollection matches = Regex.Matches(TemplateText, @"<!-- ([^\s:-]*) -->");
            foreach (Match match in matches)
            {
                string PartName = match.Groups[1].Value;
                PartTemplate[PartName] = _GetPartTemplate(PartName);
                PartData[PartName] = "";
                this.TemplateText = this.TemplateText.Replace(PartTemplate[PartName], "${" + PartName + "}");
            }
        }

        public string GetPartTemplate(string PartName)
        {
            return PartTemplate[PartName];
        }

        public void AppendToPart(string PartName, string value)
        {
            if (PartData.ContainsKey(PartName))
            {
                PartData[PartName] += value;
            }
        }

        public string FillVariableDataForPart(string PartName, Dictionary<string, object> Data, bool AppendToPart = true)
        {
            string TemplatePart = GetPartTemplate(PartName);
            string output = this._FillVariableData(TemplatePart, Data);
            if (AppendToPart)
            {
                this.AppendToPart(PartName, output);
            }

            return output;
        }

        public void FillVariableData(Dictionary<string, object> Data)
        {
            foreach (string PartName in PartTemplate.Keys)
            {
                this.FillVariableDataForPart(PartName, Data);
            }
        }

        public string CreateHtmlOutput(Dictionary<string, object> Data)
        {
            foreach (string PartName in PartTemplate.Keys)
            {
                Data[PartName] = PartData[PartName];
            }

            string output = this._FillVariableData(this.TemplateText, Data);
            return output;
        }

        // ================================== Private Method ===============================================

        private object GetPropertyValue(object obj, string property)
        {
            if (property.ToUpper().StartsWith("GET("))
            {
                string key = property.Substring(4, property.Length - 5);
                Dictionary<string, object> dic = (Dictionary<string, object>)obj;
                if (dic.ContainsKey(key))
                {
                    return dic[key];
                }
                else
                {
                    return "";
                }
            }
            object val = obj.GetType().GetProperty(property).GetValue(obj, null);
            if (val == null)
            {
                val = "";
            }
            return val;
        }

        private string GetPartLoopVariable(string TemplatePart)
        {
            Match match = Regex.Match(TemplatePart, @"<!-- loop:([^-]*) -->");
            if (match.Success)
            {
                return match.Groups[1].Value;
            }
            
            return null;
            
        }

        private string _GetPartTemplate(string PartName)
        {
            string StartPart = string.Format(TEMPLATE_PART_START, PartName);
            string EndPart = string.Format(TEMPLATE_PART_END, PartName);

            int start = this.TemplateText.IndexOf(StartPart);
            int end = this.TemplateText.IndexOf(EndPart);

            if (start < 0 || end < 0)
            {
                return "";
            }

            string PartText = this.TemplateText.Substring(start, end - start + EndPart.Length);

            return PartText;
        }

        private string _FillVariableData(string text, Dictionary<string, object> Data)
        {
            string TemplatePart = text;
            string output = "";

            bool IsLoopPart = TemplatePart.IndexOf("<!-- loop:") >= 0;
            string LoopVariable = GetPartLoopVariable(TemplatePart);

            if (IsLoopPart)
            {
                if (Data.ContainsKey(LoopVariable))
                {
                    object LoopObj = Data[LoopVariable];
                    List<object> listObj = (List<object>)LoopObj;
                    foreach (object Obj in listObj)
                    {
                        string value = TemplatePart;
                        MatchCollection matches = Regex.Matches(TemplatePart, @"\$\{([^\}]+)\}");
                        foreach (Match match in matches)
                        {
                            string key = match.Groups[1].Value;
                            string[] arrKey = key.Split('.');

                            object val = Obj;
                            foreach (string s in arrKey)
                            {
                                val = GetPropertyValue(val, s);
                            }

                            value = value.Replace("${" + key + "}", val.ToString());
                        }
                        output += value;
                    }
                }
            }
            else
            {
                string value = TemplatePart;
                MatchCollection matches = Regex.Matches(TemplatePart, @"\$\{([^\}]+)\}");
                foreach (Match match in matches)
                {
                    string key = match.Groups[1].Value;
                    string[] arrKey = key.Split('.');

                    object val = null;
                    if (Data.ContainsKey(arrKey[0]))
                    {
                        object val0 = Data[arrKey[0]];

                        foreach (string s in arrKey)
                        {
                            if (val == null)
                            {
                                val = val0;
                            }
                            else
                            {
                                val = GetPropertyValue(val, s);
                            }
                        }
                    }
                    else
                    {
                        val = "";
                    }

                    value = value.Replace("${" + key + "}", val.ToString());
                }
                output += value;
            }

            return output;
        }
    }
}
