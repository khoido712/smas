﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers
{
    public class ViettelCheckboxList
    {
        public string Label { get; set; }
        public object Value { get; set; }
        public bool cchecked { get; set; }
        public bool disabled { get; set; }

        public ViettelCheckboxList()
        {
        }

        public ViettelCheckboxList(string Label, object Value, bool cchecked, bool disabled)
        {
            this.Label = Label;
            this.Value = Value;
            this.cchecked = cchecked;
            this.disabled = disabled;
        }
    }
}
