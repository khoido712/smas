﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

using System.Web.Mvc.Html;
using System.Linq.Expressions;
using System.Web.Routing;
using System.Web.UI;
using Telerik.Web.Mvc.UI;
using Telerik.Web.Mvc.UI.Fluent;
using System.Web.UI.WebControls;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using SMAS.VTUtils.HtmlHelpers;
using Microsoft.Ajax.Utilities;
//using System.Web.UI.DataVisualization.Charting;

namespace Com.Viettel.Framework.HtmlHelpers
{
    public static class HtmlHelperExtensions
    {
        public static string ActivePage(this HtmlHelper helper, string controller, string action)
        {
            string classValue = "";

            string currentController = helper.ViewContext.Controller
                                                         .ValueProvider
                                                         .GetValue("controller")
                                                         .RawValue.ToString();
            string currentAction = helper.ViewContext.Controller
                                                     .ValueProvider.GetValue("action")
                                                     .RawValue
                                                     .ToString();

            if (currentController == controller && currentAction == action)
            {
                classValue = "selected";
            }

            return classValue;
        }

        public static MvcHtmlString JsMinify(
            this HtmlHelper helper, Func<object, object> markup)
        {
            string notMinifiedJs =
             (markup.DynamicInvoke(helper.ViewContext) ?? "").ToString();

            var minifier = new Minifier();
            var minifiedJs = minifier.MinifyJavaScript(notMinifiedJs, new CodeSettings
            {
                EvalTreatment = EvalTreatment.MakeImmediateSafe,
                PreserveImportantComments = false
            });
            return new MvcHtmlString(minifiedJs);
        }

        public static MvcHtmlString MessageFor(this HtmlHelper helper, string viewDataKey, string cssClass)
        {

            if (helper.ViewData[viewDataKey] != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class=\"");
                sb.Append(cssClass);
                sb.Append("\" >");
                sb.Append(helper.ViewData[viewDataKey]);
                sb.Append("</div>");
                return MvcHtmlString.Create(sb.ToString());
            }
            else
            {
                return MvcHtmlString.Empty;
            }

        }

        public static string ActivePage(this HtmlHelper helper, string controller)
        {
            string classValue = "";

            string currentController = helper.ViewContext.Controller
                                                         .ValueProvider
                                                         .GetValue("controller")
                                                         .RawValue
                                                         .ToString();

            if (currentController == controller)
            {
                classValue = "selected";
            }

            return classValue;
        }

        #region ViettelControls

        //public static MvcHtmlString ViettelLabel(this HtmlHelper helper,string txt)
        //{
        //    StringBuilder sb = new StringBuilder();
        //      sb.Append( helper.Label("test my control").ToString());
        //    //or use TagBuilder
        //    return new MvcHtmlString(sb.ToString());
        //}

        #region Action

        // Summary:
        //     Invokes the specified child action method and returns the result as an HTML
        //     string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName)
        {
            return htmlHelper.Action(actionName);
        }
        //
        // Summary:
        //     Invokes the specified child action method with the specified parameters and
        //     returns the result as an HTML string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. You can use routeValues
        //     to provide the parameters that are bound to the action method parameters.
        //     The routeValues parameter is merged with the original route values and overrides
        //     them.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName, object routeValues)
        {
            return htmlHelper.Action(actionName, routeValues);
        }
        //
        // Summary:
        //     Invokes the specified child action method using the specified parameters
        //     and returns the result as an HTML string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        //   routeValues:
        //     A dictionary that contains the parameters for a route. You can use routeValues
        //     to provide the parameters that are bound to the action method parameters.
        //     The routeValues parameter is merged with the original route values and overrides
        //     them.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName, RouteValueDictionary routeValues)
        {
            return htmlHelper.Action(actionName, routeValues);
        }
        //
        // Summary:
        //     Invokes the specified child action method using the specified controller
        //     name and returns the result as an HTML string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        //   controllerName:
        //     The name of the controller that contains the action method.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName, string controllerName)
        {
            return htmlHelper.Action(actionName, controllerName);
        }
        //
        // Summary:
        //     Invokes the specified child action method using the specified parameters
        //     and controller name and returns the result as an HTML string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        //   controllerName:
        //     The name of the controller that contains the action method.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. You can use routeValues
        //     to provide the parameters that are bound to the action method parameters.
        //     The routeValues parameter is merged with the original route values and overrides
        //     them.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName, string controllerName, object routeValues)
        {
            return htmlHelper.Action(actionName, controllerName, routeValues);
        }

        //
        // Summary:
        //     Invokes the specified child action method using the specified parameters
        //     and controller name and returns the result as an HTML string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   actionName:
        //     The name of the action method to invoke.
        //
        //   controllerName:
        //     The name of the controller that contains the action method.
        //
        //   routeValues:
        //     A dictionary that contains the parameters for a route. You can use routeValues
        //     to provide the parameters that are bound to the action method parameters.
        //     The routeValues parameter is merged with the original route values and overrides
        //     them.
        //
        // Returns:
        //     The child action result as an HTML string.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The htmlHelper parameter is null.
        //
        //   System.ArgumentException:
        //     The actionName parameter is null or empty.
        //
        //   System.InvalidOperationException:
        //     The required virtual path data cannot be found.
        public static MvcHtmlString ViettelAction(this HtmlHelper htmlHelper, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return htmlHelper.Action(actionName, controllerName, routeValues);
        }

        #endregion

        #region ActionLink
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName)
        {
            return htmlHelper.ActionLink(linkText, actionName);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues)
        {
            return htmlHelper.ActionLink(linkText, actionName, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues)
        {
            return htmlHelper.ActionLink(linkText, actionName, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   controllerName:
        //     The name of the controller.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element. The attributes
        //     are retrieved through reflection by examining the properties of the object.
        //     The object is typically created by using object initializer syntax.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, object htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   controllerName:
        //     The name of the controller.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   controllerName:
        //     The name of the controller.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   controllerName:
        //     The name of the controller.
        //
        //   protocol:
        //     The protocol for the URL, such as "http" or "https".
        //
        //   hostName:
        //     The host name for the URL.
        //
        //   fragment:
        //     The URL fragment name (the anchor name).
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   actionName:
        //     The name of the action.
        //
        //   controllerName:
        //     The name of the controller.
        //
        //   protocol:
        //     The protocol for the URL, such as "http" or "https".
        //
        //   hostName:
        //     The host name for the URL.
        //
        //   fragment:
        //     The URL fragment name (the anchor name).
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelActionLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes);
        }

        #endregion

        #region AntiForgeryToken

        //
        // Summary:
        //     Generates a hidden form field (anti-forgery token) that is validated when
        //     the form is submitted.
        //
        // Returns:
        //     The generated form field (anti-forgery token).
        public static MvcHtmlString ViettelAntiForgeryToken(this HtmlHelper htmlHelper)
        {
            return htmlHelper.AntiForgeryToken();
        }
        //
        // Summary:
        //     Generates a hidden form field (anti-forgery token) that is validated when
        //     the form is submitted. The field value is generated using the specified salt
        //     value.
        //
        // Parameters:
        //   salt:
        //     The salt value, which can be any non-empty string.
        //
        // Returns:
        //     The generated form field (anti-forgery token).
        public static MvcHtmlString ViettelAntiForgeryToken(this HtmlHelper htmlHelper, string salt)
        {
            return htmlHelper.AntiForgeryToken(salt);
        }
        //
        // Summary:
        //     Generates a hidden form field (anti-forgery token) that is validated when
        //     the form is submitted. The field value is generated using the specified salt
        //     value, domain, and path.
        //
        // Parameters:
        //   salt:
        //     The salt value, which can be any non-empty string.
        //
        //   domain:
        //     The application domain.
        //
        //   path:
        //     The virtual path.
        //
        // Returns:
        //     The generated form field (anti-forgery token).
        public static MvcHtmlString ViettelAntiForgeryToken(this HtmlHelper htmlHelper, string salt, string domain, string path)
        {
            return htmlHelper.AntiForgeryToken(salt, domain, path);
        }

        /// <summary>
        /// Anti Forgery Token For Ajax request
        /// </summary>
        /// <author>HaiVT - 09/08/2013</author>
        /// <returns></returns>
        public static MvcHtmlString ViettelAntiForgeryTokenForAjaxPost(this HtmlHelper helper)
        {
            var antiForgeryInputTag = helper.AntiForgeryToken().ToString();
            // Above gets the following: <input name="__RequestVerificationToken" type="hidden" value="PnQE7R0MIBBAzC7SqtVvwrJpGbRvPgzWHo5dSyoSaZoabRjf9pCyzjujYBU_qKDJmwIOiPRDwBV1TNVdXFVgzAvN9_l2yt9-nf4Owif0qIDz7WRAmydVPIm6_pmJAI--wvvFQO7g0VvoFArFtAR2v6Ch1wmXCZ89v0-lNOGZLZc1" />
            var removedStart = antiForgeryInputTag.Replace(@"<input name=""__RequestVerificationToken"" type=""hidden"" value=""", "");
            var tokenValue = removedStart.Replace(@""" />", "");
            if (antiForgeryInputTag == removedStart || removedStart == tokenValue)
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return new MvcHtmlString(string.Format(@"{0}:""{1}""", "__RequestVerificationToken", tokenValue));
            }
        }

        #endregion

        #region Checkbox

        // Summary:
        //     Returns a check box input element by using the specified HTML helper and
        //     the name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.CheckBox(name);
        }
        //
        // Summary:
        //     Returns a check box input element by using the specified HTML helper, the
        //     name of the form field, and a value to indicate whether the check box is
        //     selected.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        //   isChecked:
        //     true to select the check box; otherwise, false.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, bool isChecked)
        {
            return htmlHelper.CheckBox(name, isChecked);
        }
        //
        // Summary:
        //     Returns a check box input element by using the specified HTML helper, the
        //     name of the form field, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.CheckBox(name, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a check box input element by using the specified HTML helper, the
        //     name of the form field, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return htmlHelper.CheckBox(name, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a check box input element by using the specified HTML helper, the
        //     name of the form field, a value to indicate whether the check box is selected,
        //     and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        //   isChecked:
        //     true to select the check box; otherwise, false.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, bool isChecked, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.CheckBox(name, isChecked, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a check box input element by using the specified HTML helper, the
        //     name of the form field, a value that indicates whether the check box is selected,
        //     and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field.
        //
        //   isChecked:
        //     true to select the check box; otherwise, false.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "checkbox".
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, bool isChecked, object htmlAttributes)
        {
            return htmlHelper.CheckBox(name, isChecked, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a check box input element for each property in the object that is
        //     represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "checkbox" for each
        //     property in the object that is represented by the specified expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression)
        {
            return htmlHelper.CheckBoxFor(expression);
        }
        //
        // Summary:
        //     Returns a check box input element for each property in the object that is
        //     represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "checkbox" for each
        //     property in the object that is represented by the specified expression, using
        //     the specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.CheckBoxFor(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a check box input element for each property in the object that is
        //     represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "checkbox" for each
        //     property in the object that is represented by the specified expression, using
        //     the specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlAttributes)
        {
            return htmlHelper.CheckBoxFor(expression, htmlAttributes);
        }

        #endregion

        #region Display

        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     a string expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression)
        {
            return html.Display(expression);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     a string expression, using additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression, object additionalViewData)
        {
            return html.Display(expression, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the expression, using the specified template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression, string templateName)
        {
            return html.Display(expression, templateName);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the expression, using the specified template and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression, string templateName, object additionalViewData)
        {
            return html.Display(expression, templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the expression, using the specified template and an HTML field ID.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression, string templateName, string htmlFieldName)
        {
            return html.Display(expression, templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the expression, using the specified template, HTML field ID, and additional
        //     view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplay(this HtmlHelper html, string expression, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.Display(expression, templateName, htmlFieldName, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the System.Linq.Expressions.Expression expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return html.DisplayFor(expression);
        }
        //
        // Summary:
        //     Returns a string that contains each property value in the object that is
        //     represented by the specified expression, using additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object additionalViewData)
        {
            return html.DisplayFor(expression, additionalViewData);
        }
        //
        // Summary:
        //     Returns a string that contains each property value in the object that is
        //     represented by the System.Linq.Expressions.Expression, using the specified
        //     template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName)
        {
            return html.DisplayFor(expression, templateName);
        }
        //
        // Summary:
        //     Returns a string that contains each property value in the object that is
        //     represented by the specified expression, using the specified template and
        //     additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, object additionalViewData)
        {
            return html.DisplayFor(expression, templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the System.Linq.Expressions.Expression, using the specified template and
        //     an HTML field ID.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, string htmlFieldName)
        {
            return html.DisplayFor(expression, templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the specified expression, using the template, an HTML field ID, and additional
        //     view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.DisplayFor(expression, templateName, htmlFieldName, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html)
        {
            return html.DisplayForModel();
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model, using additional view
        //     data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html, object additionalViewData)
        {
            return html.DisplayForModel(additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model using the specified template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html, string templateName)
        {
            return html.DisplayForModel(templateName);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model, using the specified template
        //     and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html, string templateName, object additionalViewData)
        {
            return html.DisplayForModel(templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model using the specified template
        //     and HTML field ID.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html, string templateName, string htmlFieldName)
        {
            return html.DisplayForModel(templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the model, using the specified template,
        //     an HTML field ID, and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template that is used to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     The HTML markup for each property in the model.
        public static MvcHtmlString ViettelDisplayForModel(this HtmlHelper html, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.DisplayForModel(templateName, htmlFieldName, additionalViewData);
        }

        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the specified expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        // Returns:
        //     The HTML markup for each property in the object that is represented by the
        //     expression.
        public static MvcHtmlString ViettelDisplayText(this HtmlHelper html, string name)
        {
            return html.DisplayText(name);
        }
        //
        // Summary:
        //     Returns HTML markup for each property in the object that is represented by
        //     the specified expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TResult:
        //     The type of the result.
        //
        // Returns:
        //     The HTML markup for each property.zz 12/29/2010 1:25:49 PM
        public static MvcHtmlString ViettelDisplayTextFor<TModel, TResult>(this HtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression)
        {
            return html.DisplayTextFor(expression);
        }

        #endregion

        #region DropDownList

        // Summary:
        //     Returns a single-selection select element using the specified HTML helper
        //     and the name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        // Returns:
        //     An HTML select element.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.DropDownList(name);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, and the specified list items.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList)
        {
            return htmlHelper.DropDownList(name, selectList);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, and an option label.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   optionLabel:
        //     The text for a default empty item. This parameter can be null.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, string optionLabel)
        {
            return htmlHelper.DropDownList(name, optionLabel);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, the specified list items, and the specified HTML
        //     attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.DropDownList(name, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, the specified list items, and the specified HTML
        //     attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            return htmlHelper.DropDownList(name, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, the specified list items, and an option label.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   optionLabel:
        //     The text for a default empty item. This parameter can be null.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, string optionLabel)
        {
            return htmlHelper.DropDownList(name, selectList, optionLabel);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, the specified list items, an option label, and
        //     the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   optionLabel:
        //     The text for a default empty item. This parameter can be null.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.DropDownList(name, selectList, optionLabel, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a single-selection select element using the specified HTML helper,
        //     the name of the form field, the specified list items, an option label, and
        //     the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   optionLabel:
        //     The text for a default empty item. This parameter can be null.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelDropDownList(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, string optionLabel, object htmlAttributes)
        {
            return htmlHelper.DropDownList(name, selectList, optionLabel, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList)
        {
            return htmlHelper.DropDownListFor(expression, selectList);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            return htmlHelper.DropDownListFor(expression, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items and option label.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel)
        {
            return htmlHelper.DropDownListFor(expression, selectList, optionLabel);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items, option label,
        //     and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items, option label,
        //     and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, object htmlAttributes)
        {
            return htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes);
        }

        #endregion

        #region Editor

        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression)
        {
            return html.Editor(expression);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression, object additionalViewData)
        {
            return html.Editor(expression, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression, string templateName)
        {
            return html.Editor(expression, templateName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression, string templateName, object additionalViewData)
        {
            return html.Editor(expression, templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template and HTML field name.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression, string templateName, string htmlFieldName)
        {
            return html.Editor(expression, templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template, HTML field name, and additional
        //     view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditor(this HtmlHelper html, string expression, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.Editor(expression, templateName, htmlFieldName, additionalViewData);
        }
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes = null, bool readOnly = false, bool disabled = false)
        {
            if (htmlAttributes == null)
            {
                htmlAttributes = new Dictionary<string, object>();
            }
            if (readOnly)
            {
                htmlAttributes["readonly"] = "readonly";
            }
            if (disabled)
            {
                htmlAttributes["disabled"] = "disabled";
            }
            //aunh1
            ModelMetadata fieldMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);

            string label = html.LabelFor(expression).ToString();

            string control = html.EditorFor(expression).ToString();
            string htmlAtt = " ";
            foreach (string s in htmlAttributes.Keys)
            {
                htmlAtt += s + "=\"" + htmlAttributes[s] + "\"";
                htmlAtt += " ";
            }

            control = control.Replace("<input ", "<input " + htmlAtt);
            control = control.Replace("<select ", "<select " + htmlAtt);
            //PhuongH 13/05/2013
            control = control.Replace("<textarea ", "<textarea " + htmlAtt);

            var valExp = html.ValidationMessageFor(expression);
            string message = valExp != null ? html.ValidationMessageFor(expression).ToString() : "";

            var propertyInfo = (expression.Body as MemberExpression).Member as PropertyInfo;
            string PropertyName = "";
            if (propertyInfo != null)
            {
                PropertyName = propertyInfo.Name;
            }

            if (fieldMetadata.IsRequired && propertyInfo.PropertyType != typeof(bool))
            {
                label += " <span style=\"color: red;\">*</span>";
            }

            string result = "<div class=\"vt-control " + PropertyName + "\">"
                          + "<div class=\"editor-label\">"
                          + label
                          + "</div>"
                          + "<div class=\"editor-field\">"
                          + control
                          + message
                          + "</div>"
                          + "</div>";




            return new MvcHtmlString(result);
        }

        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string label, IDictionary<string, object> htmlAttributes = null, bool readOnly = false, bool disabled = false)
        {
            if (htmlAttributes == null)
            {
                htmlAttributes = new Dictionary<string, object>();
            }
            if (readOnly)
            {
                htmlAttributes["readonly"] = "readonly";
            }
            if (disabled)
            {
                htmlAttributes["disabled"] = "disabled";
            }
            //aunh1
            ModelMetadata fieldMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);

            string control = html.EditorFor(expression).ToString();
            string htmlAtt = " ";
            foreach (string s in htmlAttributes.Keys)
            {
                htmlAtt += s + "=\"" + htmlAttributes[s] + "\"";
                htmlAtt += " ";
            }

            control = control.Replace("<input ", "<input " + htmlAtt);
            control = control.Replace("<select ", "<select " + htmlAtt);
            //PhuongH 13/05/2013
            control = control.Replace("<textarea ", "<textarea " + htmlAtt);

            var valExp = html.ValidationMessageFor(expression);
            string message = valExp != null ? html.ValidationMessageFor(expression).ToString() : "";

            var propertyInfo = (expression.Body as MemberExpression).Member as PropertyInfo;
            string PropertyName = "";
            if (propertyInfo != null)
            {
                PropertyName = propertyInfo.Name;
            }

            if (fieldMetadata.IsRequired && propertyInfo.PropertyType != typeof(bool))
            {
                label += " <span style=\"color: red;\">*</span>";
            }

            string result = "<div class=\"vt-control " + PropertyName + "\">"
                          + "<div class=\"editor-label\">"
                          + label
                          + "</div>"
                          + "<div class=\"editor-field\">"
                          + control
                          + message
                          + "</div>"
                          + "</div>";




            return new MvcHtmlString(result);
        }

        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object additionalViewData)
        {
            return html.EditorFor(expression, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the System.Linq.Expressions.Expression expression, using the specified
        //     template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName)
        {
            return html.EditorFor(expression, templateName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, object additionalViewData)
        {
            return html.EditorFor(expression, templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the System.Linq.Expressions.Expression expression, using the specified
        //     template and HTML field name.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, string htmlFieldName)
        {
            return html.EditorFor(expression, templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the object that is represented
        //     by the expression, using the specified template, HTML field name, and additional
        //     view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element for each property in the object that is represented
        //     by the expression.
        public static MvcHtmlString ViettelEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.EditorFor(expression, templateName, htmlFieldName, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        // Returns:
        //     An HTML input element for each property in the model.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html)
        {
            return html.EditorForModel();
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model, using additional
        //     view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the model.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html, object additionalViewData)
        {
            return html.EditorForModel(additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model, using the specified
        //     template.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        // Returns:
        //     An HTML input element for each property in the model and in the specified
        //     template.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html, string templateName)
        {
            return html.EditorForModel(templateName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model, using the specified
        //     template and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the model.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html, string templateName, object additionalViewData)
        {
            return html.EditorForModel(templateName, additionalViewData);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model, using the specified
        //     template name and HTML field name.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        // Returns:
        //     An HTML input element for each property in the model and in the named template.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html, string templateName, string htmlFieldName)
        {
            return html.EditorForModel(templateName, htmlFieldName);
        }
        //
        // Summary:
        //     Returns an HTML input element for each property in the model, using the template
        //     name, HTML field name, and additional view data.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   templateName:
        //     The name of the template to use to render the object.
        //
        //   htmlFieldName:
        //     A string that is used to disambiguate the names of HTML input elements that
        //     are rendered for properties that have the same name.
        //
        //   additionalViewData:
        //     An anonymous object that can contain additional view data that will be merged
        //     into the System.Web.Mvc.ViewDataDictionary<TModel> instance that is created
        //     for the template.
        //
        // Returns:
        //     An HTML input element for each property in the model.
        public static MvcHtmlString ViettelEditorForModel(this HtmlHelper html, string templateName, string htmlFieldName, object additionalViewData)
        {
            return html.EditorForModel(templateName, htmlFieldName, additionalViewData);
        }

        #endregion

        #region Hidden

        //
        // Summary:
        //     Returns a hidden input element by using the specified HTML helper and the
        //     name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden".
        public static MvcHtmlString ViettelHidden(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.Hidden(name);
        }
        //
        // Summary:
        //     Returns a hidden input element by using the specified HTML helper, the name
        //     of the form field, and the value.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the hidden input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden".
        public static MvcHtmlString ViettelHidden(this HtmlHelper htmlHelper, string name, object value)
        {
            return htmlHelper.Hidden(name, value);
        }
        //
        // Summary:
        //     Returns a hidden input element by using the specified HTML helper, the name
        //     of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the hidden input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden".
        public static MvcHtmlString ViettelHidden(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.Hidden(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a hidden input element by using the specified HTML helper, the name
        //     of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the hidden input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden".
        public static MvcHtmlString ViettelHidden(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            return htmlHelper.Hidden(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML hidden input element for each property in the object that
        //     is represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden" for each property
        //     in the object that is represented by the expression.
        public static MvcHtmlString ViettelHiddenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.HiddenFor<TModel, TProperty>(expression);
        }
        //
        // Summary:
        //     Returns an HTML hidden input element for each property in the object that
        //     is represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden" for each property
        //     in the object that is represented by the expression.
        public static MvcHtmlString ViettelHiddenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.HiddenFor(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML hidden input element for each property in the object that
        //     is represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An input element whose type attribute is set to "hidden" for each property
        //     in the object that is represented by the expression.
        public static MvcHtmlString ViettelHiddenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.HiddenFor<TModel, TProperty>(expression, htmlAttributes);
        }

        #endregion

        #region Label


        public static MvcHtmlString ViettelLabelStar(this HtmlHelper html, string expression)
        {

            expression = expression + " <span style=\"color: red;\">*</span>";
            return html.Label(expression);
        }
        // Summary:
        //     Returns an HTML label element and the property name of the property that
        //     is represented by the specified expression.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the property to display.
        //
        // Returns:
        //     An HTML label element and the property name of the property that is represented
        //     by the expression.
        public static MvcHtmlString ViettelLabel(this HtmlHelper html, string expression)
        {

            return html.Label(expression);
        }
        //
        //
        // Returns:
        //     Returns System.Web.Mvc.MvcHtmlString.
        public static MvcHtmlString ViettelLabel(this HtmlHelper html, string expression, string labelText)
        {
            return html.Label(expression, labelText);
        }
        //
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the property to display.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML label element and the property name of the property that is represented
        //     by the expression.
        public static MvcHtmlString ViettelLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return html.LabelFor<TModel, TValue>(expression);
        }

        public static MvcHtmlString ViettelLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            string label = html.LabelFor(expression).ToString();
            string attrs = string.Empty;

            if (htmlAttributes != null)
                foreach (var item in htmlAttributes)
                    attrs += item.Key + "=\"" + item.Value + "\"";

            label = label.Replace("<lable>", "<label " + attrs + " >");

            return new MvcHtmlString(label);
        }
        //
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the property to display.
        //
        //   labelText:
        //     The label text.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TValue:
        //     The type of the value.
        //
        // Returns:
        //     An HTML label element and the property name of the property that is represented
        //     by the expression.
        public static MvcHtmlString ViettelLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string labelText)
        {
            return html.LabelFor<TModel, TValue>(expression, labelText);
        }
        //
        // Summary:
        //     Returns an HTML label element and the property name of the property that
        //     is represented by the model.
        //
        // Parameters:
        //   html:
        //     The HTML helper instance that this method extends.
        //
        // Returns:
        //     An HTML label element and the property name of the property that is represented
        //     by the model.
        public static MvcHtmlString ViettelLabelForModel(this HtmlHelper html)
        {
            return html.LabelForModel();
        }
        //
        //
        // Returns:
        //     Returns System.Web.Mvc.MvcHtmlString.
        public static MvcHtmlString ViettelLabelForModel(this HtmlHelper html, string labelText)
        {
            return html.LabelForModel(labelText);
        }

        #endregion

        #region ListBox

        //
        // Summary:
        //     Returns a multi-select select element using the specified HTML helper and
        //     the name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        // Returns:
        //     An HTML select element.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelListBox(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.ListBox(name);
        }
        //
        // Summary:
        //     Returns a multi-select select element using the specified HTML helper, the
        //     name of the form field, and the specified list items.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelListBox(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList)
        {
            return htmlHelper.ListBox(name, selectList);
        }
        //
        // Summary:
        //     Returns a multi-select select element using the specified HTML helper, the
        //     name of the form field, the specified list items, and the specified HMTL
        //     attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list..
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelListBox(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ListBox(name, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a multi-select select element using the specified HTML helper, the
        //     name of the form field, and the specified list items.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An HTML select element with an option subelement for each item in the list..
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        public static MvcHtmlString ViettelListBox(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            return htmlHelper.ListBox(name, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression and using the specified list items.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList)
        {
            return htmlHelper.ListBoxFor<TModel, TProperty>(expression, selectList);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ListBoxFor<TModel, TProperty>(expression, selectList, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML select element for each property in the object that is represented
        //     by the specified expression using the specified list items and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     display.
        //
        //   selectList:
        //     A collection of System.Web.Mvc.SelectListItem objects that are used to populate
        //     the drop-down list.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML select element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            return htmlHelper.ListBoxFor<TModel, TProperty>(expression, selectList, htmlAttributes);
        }

        #endregion

        #region Partial

        // Summary:
        //     Renders the specified partial view as an HTML-encoded string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   partialViewName:
        //     The name of the partial view to render.
        //
        // Returns:
        //     The partial view that is rendered as an HTML-encoded string.
        public static MvcHtmlString ViettelPartial(this HtmlHelper htmlHelper, string partialViewName)
        {
            return htmlHelper.Partial(partialViewName);
        }
        //
        // Summary:
        //     Renders the specified partial view as an HTML-encoded string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   partialViewName:
        //     The name of the partial view to render.
        //
        //   model:
        //     The model for the partial view.
        //
        // Returns:
        //     The partial view that is rendered as an HTML-encoded string.
        public static MvcHtmlString ViettelPartial(this HtmlHelper htmlHelper, string partialViewName, object model)
        {
            return htmlHelper.Partial(partialViewName, model);
        }
        //
        // Summary:
        //     Renders the specified partial view as an HTML-encoded string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   partialViewName:
        //     The name of the partial view to render.
        //
        //   viewData:
        //     The view data dictionary for the partial view.
        //
        // Returns:
        //     The partial view that is rendered as an HTML-encoded string.
        public static MvcHtmlString ViettelPartial(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            return htmlHelper.Partial(partialViewName, viewData);
        }
        //
        // Summary:
        //     Renders the specified partial view as an HTML-encoded string.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   partialViewName:
        //     The name of the partial view.
        //
        //   model:
        //     The model for the partial view.
        //
        //   viewData:
        //     The view data dictionary for the partial view.
        //
        // Returns:
        //     The partial view that is rendered as an HTML-encoded string.
        public static MvcHtmlString ViettelPartial(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData)
        {
            return htmlHelper.Partial(partialViewName, model, viewData);
        }

        #endregion

        #region Password

        //
        // Summary:
        //     Returns a password input element by using the specified HTML helper and the
        //     name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        // Returns:
        //     An input element whose type attribute is set to "password".
        public static MvcHtmlString ViettelPassword(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.Password(name);
        }
        //
        // Summary:
        //     Returns a password input element by using the specified HTML helper, the
        //     name of the form field, and the value.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the password input element. If this value is null, the value
        //     of the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        // Returns:
        //     An input element whose type attribute is set to "password".
        public static MvcHtmlString ViettelPassword(this HtmlHelper htmlHelper, string name, object value)
        {
            return htmlHelper.Password(name, value);
        }
        //
        // Summary:
        //     Returns a password input element by using the specified HTML helper, the
        //     name of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the password input element. If this value is null, the value
        //     of the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "password".
        public static MvcHtmlString ViettelPassword(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.Password(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a password input element by using the specified HTML helper, the
        //     name of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the password input element. If this value is null, the value
        //     of the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "password".
        public static MvcHtmlString ViettelPassword(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            return htmlHelper.Password(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a password input element for each property in the object that is
        //     represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "password" for each
        //     property in the object that is represented by the specified expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelPasswordFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.PasswordFor<TModel, TProperty>(expression);
        }
        //
        // Summary:
        //     Returns a password input element for each property in the object that is
        //     represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "password" for each
        //     property in the object that is represented by the specified expression, using
        //     the specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelPasswordFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.PasswordFor<TModel, TProperty>(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a password input element for each property in the object that is
        //     represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "password" for each
        //     property in the object that is represented by the specified expression, using
        //     the specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelPasswordFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.PasswordFor<TModel, TProperty>(expression, htmlAttributes);
        }

        #endregion

        #region RadioButton
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value)
        {
            return htmlHelper.RadioButton(name, value);
        }
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   isChecked:
        //     true to select the radio button; otherwise, false.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value, bool isChecked)
        {
            return htmlHelper.RadioButton(name, value, isChecked);
        }
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RadioButton(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            return htmlHelper.RadioButton(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   isChecked:
        //     true to select the radio button; otherwise, false.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value, bool isChecked, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RadioButton(name, value, isChecked, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a radio button input element that is used to present mutually exclusive
        //     options.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   isChecked:
        //     true to select the radio button; otherwise, false.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "radio".
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The name parameter is null or empty.
        //
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButton(this HtmlHelper htmlHelper, string name, object value, bool isChecked, object htmlAttributes)
        {
            return htmlHelper.RadioButton(name, value, isChecked, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a radio button input element for each property in the object that
        //     is represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "radio" for each property
        //     in the object that is represented by the specified expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value)
        {
            return htmlHelper.RadioButtonFor<TModel, TProperty>(expression, value);
        }
        //
        // Summary:
        //     Returns a radio button input element for each property in the object that
        //     is represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "radio" for each property
        //     in the object that is represented by the specified expression, using the
        //     specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RadioButtonFor<TModel, TProperty>(expression, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a radio button input element for each property in the object that
        //     is represented by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   value:
        //     If this radio button is selected, the value of the radio button that is submitted
        //     when the form is posted. If the value of the selected radio button in the
        //     System.Web.Mvc.ViewDataDictionary or the System.Web.Mvc.ModelStateDictionary
        //     object matches this value, this radio button is selected.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "radio" for each property
        //     in the object that is represented by the specified expression, using the
        //     specified HTML attributes.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The value parameter is null.
        public static MvcHtmlString ViettelRadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object value, object htmlAttributes)
        {
            return htmlHelper.RadioButtonFor<TModel, TProperty>(expression, value, htmlAttributes);
        }

        #endregion

        #region RouteLink

        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, object routeValues)
        {
            return htmlHelper.RouteLink(linkText, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, RouteValueDictionary routeValues)
        {
            return htmlHelper.RouteLink(linkText, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName)
        {
            return htmlHelper.RouteLink(linkText, routeName);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, object routeValues, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, object routeValues)
        {
            return htmlHelper.RouteLink(linkText, routeName, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, RouteValueDictionary routeValues)
        {
            return htmlHelper.RouteLink(linkText, routeName, routeValues);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, object routeValues, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   protocol:
        //     The protocol for the URL, such as "http" or "https".
        //
        //   hostName:
        //     The host name for the URL.
        //
        //   fragment:
        //     The URL fragment name (the anchor name).
        //
        //   routeValues:
        //     An object that contains the parameters for a route. The parameters are retrieved
        //     through reflection by examining the properties of the object. The object
        //     is typically created by using object initializer syntax.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, protocol, fragment, hostName, routeValues, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an anchor element (a element) that contains the virtual path of the
        //     specified action.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   linkText:
        //     The inner text of the anchor element.
        //
        //   routeName:
        //     The name of the route that is used to return a virtual path.
        //
        //   protocol:
        //     The protocol for the URL, such as "http" or "https".
        //
        //   hostName:
        //     The host name for the URL.
        //
        //   fragment:
        //     The URL fragment name (the anchor name).
        //
        //   routeValues:
        //     An object that contains the parameters for a route.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An anchor element (a element).
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The linkText parameter is null or empty.
        public static MvcHtmlString ViettelRouteLink(this HtmlHelper htmlHelper, string linkText, string routeName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.RouteLink(linkText, routeName, protocol, fragment, hostName, routeValues, htmlAttributes);
        }
        #endregion


        #region TextArea



        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper
        //     and the name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.TextArea(name);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, and the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextArea(name, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper
        //     and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return htmlHelper.TextArea(name, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, and the text content.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   value:
        //     The text content.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, string value)
        {
            return htmlHelper.TextArea(name, value);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, the text content, and the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   value:
        //     The text content.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, string value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextArea(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, the text content, and the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   value:
        //     The text content.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, string value, object htmlAttributes)
        {
            return htmlHelper.TextArea(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, the text content, the number of rows and columns,
        //     and the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   value:
        //     The text content.
        //
        //   rows:
        //     The number of rows.
        //
        //   columns:
        //     The number of columns.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, string value, int rows, int columns, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextArea(name, value, rows, columns, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the specified textarea element by using the specified HTML helper,
        //     the name of the form field, the text content, the number of rows and columns,
        //     and the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        //   value:
        //     The text content.
        //
        //   rows:
        //     The number of rows.
        //
        //   columns:
        //     The number of columns.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     The textarea element.
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string name, string value, int rows, int columns, object htmlAttributes)
        {
            return htmlHelper.TextArea(name, value, rows, columns, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML textarea element for each property in the object that is
        //     represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML textarea element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.TextAreaFor<TModel, TProperty>(expression);
        }

        //
        // Summary:
        //     Returns an HTML textarea element for each property in the object that is
        //     represented by the specified expression using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML textarea element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextAreaFor<TModel, TProperty>(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML textarea element for each property in the object that is
        //     represented by the specified expression using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML textarea element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.TextAreaFor<TModel, TProperty>(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML textarea element for each property in the object that is
        //     represented by the specified expression using the specified HTML attributes
        //     and the number of rows and columns.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   rows:
        //     The number of rows.
        //
        //   columns:
        //     The number of columns.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML textarea element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int rows, int columns, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextAreaFor<TModel, TProperty>(expression, rows, columns, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an HTML textarea element for each property in the object that is
        //     represented by the specified expression using the specified HTML attributes
        //     and the number of rows and columns.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   rows:
        //     The number of rows.
        //
        //   columns:
        //     The number of columns.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     An HTML textarea element for each property in the object that is represented
        //     by the expression.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The expression parameter is null.
        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int rows, int columns, object htmlAttributes)
        {
            return htmlHelper.TextAreaFor<TModel, TProperty>(expression, rows, columns, htmlAttributes);
        }

        #endregion

        #region TextBox

        //
        // Summary:
        //     Returns a text input element by using the specified HTML helper and the name
        //     of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        // Returns:
        //     An input element whose type attribute is set to "text".
        public static MvcHtmlString ViettelTextBox(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.TextBox(name);
        }
        //
        // Summary:
        //     Returns a text input element by using the specified HTML helper, the name
        //     of the form field, and the value.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the text input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        // Returns:
        //     An input element whose type attribute is set to "text".
        public static MvcHtmlString ViettelTextBox(this HtmlHelper htmlHelper, string name, object value)
        {
            return htmlHelper.TextBox(name, value);
        }
        //
        // Summary:
        //     Returns a text input element by using the specified HTML helper, the name
        //     of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the text input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "text".
        public static MvcHtmlString ViettelTextBox(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextBox(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a text input element by using the specified HTML helper, the name
        //     of the form field, the value, and the HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        //   value:
        //     The value of the text input element. If this value is null, the value of
        //     the element is retrieved from the System.Web.Mvc.ViewDataDictionary object.
        //     If no value exists there, the value is retrieved from the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Returns:
        //     An input element whose type attribute is set to "text".
        public static MvcHtmlString ViettelTextBox(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            return htmlHelper.TextBox(name, value, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a text input element for each property in the object that is represented
        //     by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "text" for each property
        //     in the object that is represented by the expression.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The expression parameter is null or empty.
        public static MvcHtmlString ViettelTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.TextBoxFor<TModel, TProperty>(expression);
        }
        //
        // Summary:
        //     Returns a text input element for each property in the object that is represented
        //     by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element type attribute is set to "text" for each property in
        //     the object that is represented by the expression.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The expression parameter is null or empty.
        public static MvcHtmlString ViettelTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes);
        }
        //
        // Summary:
        //     Returns a text input element for each property in the object that is represented
        //     by the specified expression, using the specified HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes to set for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the value.
        //
        // Returns:
        //     An HTML input element whose type attribute is set to "text" for each property
        //     in the object that is represented by the expression.
        //
        // Exceptions:
        //   System.ArgumentException:
        //     The expression parameter is null or empty.
        public static MvcHtmlString ViettelTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes);
        }

        #endregion

        // Made by DungVA
        #region Textbox - more
        public static MvcHtmlString ViettelTestTextbox1<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string label, string textboxname, bool ronly, IDictionary<string, object> htmlAttributes)
        {
            string strTextbox = "<td>" + html.Label(label) + " <span style=\"color: red;\">*</span>" + "</td>" + "<td>" + html.TextBoxFor<TModel, TValue>(expression, htmlAttributes) + "</td>";
            return new MvcHtmlString(strTextbox);
        }
        public static MvcHtmlString ViettelTextbox(this HtmlHelper html, string label, string textboxname, object value, IDictionary<string, object> htmlAttributes, bool required, bool ronly = false)
        {
            string strTextboxfull;
            string strTextbox;
            string strTextboxtemp = html.TextBox(textboxname, value, htmlAttributes).ToString();

            if (ronly)
            {
                strTextbox = strTextboxtemp.Insert(strTextboxtemp.Length - 2, "ReadOnly =\"" + ronly + "\"");
            }
            else
            {
                strTextbox = strTextboxtemp;
            }
            string strlabel = "";
            if (label != null)
            {
                if (required)
                {
                    strlabel = "<div class=\"editor-label\">"
                                + html.Label(label)
                                + " <span style=\"color: red;\">*</span>"
                                + "</div>";
                }
                else
                {
                    strlabel = "<div class=\"editor-label\">"
                                + html.Label(label)
                                + "</div>";
                }
            }


            strTextboxfull = "<div class=\"vt-control\">"
                        + strlabel
                        + "<div class=\"editor-field\">"
                        + strTextbox + html.ValidationMessage(textboxname)
                        + "</div>"
                        + "</div>";

            return new MvcHtmlString(strTextboxfull);
        }
        public static MvcHtmlString ViettelTextbox(this HtmlHelper html, string label, string textboxname, object value, IDictionary<string, object> htmlAttributes, bool required, bool readOnly = false, bool disabled = false)
        {
            if (readOnly)
            {
                htmlAttributes["readonly"] = "readonly";
            }
            if (disabled)
            {
                htmlAttributes["disabled"] = "disabled";
            }
            string strTextbox;
            if (required)
            {
                strTextbox = "<div class=\"vt-control\">"
                            + "<div class=\"editor-label\">"
                            + html.Label(label)
                            + " <span style=\"color: red;\">*</span>"
                            + "</div>"
                            + "<div class=\"editor-field\">"
                            + html.TextBox(textboxname, value, htmlAttributes)
                            + "</div>"
                            + "</div>";
            }
            else
            {
                strTextbox = "<div class=\"vt-control\">"
                            + "<div class=\"editor-label\">"
                            + html.Label(label)
                            + "</div>"
                            + "<div class=\"editor-field\">"
                            + html.TextBox(textboxname, value, htmlAttributes)
                            + "</div>"
                            + "</div>";
            }
            return new MvcHtmlString(strTextbox);
        }
        public static MvcHtmlString ViettelTextboxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string label, IDictionary<string, object> htmlAttributes, bool required, bool readOnly = false, bool disabled = false, bool ValidateMessage = true)
        {
            if (readOnly)
            {
                htmlAttributes["readonly"] = "readonly";
            }
            if (disabled)
            {
                htmlAttributes["disabled"] = "disabled";
            }
            string strTextbox;
            if (required)
            {
                if (string.IsNullOrEmpty(label))
                {
                    strTextbox = "<div class=\"vt-control\">" + "<div class=\"editor-field\">" + htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes) + (ValidateMessage == true ? htmlHelper.ValidationMessageFor(expression).ToString() : "") + "</div>" + "</div>";
                }
                else
                {
                    strTextbox = "<div class=\"vt-control\">" + "<div class=\"editor-label\">" + htmlHelper.Label("", label) + " <span style=\"color: red;\">*</span>" + "</div>" + "<div class=\"editor-field\">" + htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes) + (ValidateMessage == true ? (htmlHelper.ValidationMessageFor(expression) == null ? "" : htmlHelper.ValidationMessageFor(expression).ToString()) : "") + "</div>" + "</div>";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(label))
                {
                    strTextbox = "<div class=\"vt-control\">" + "<div class=\"editor-field\">" + htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes) + (ValidateMessage == true ? htmlHelper.ValidationMessageFor(expression).ToString() : "") + "</div>" + "</div>";
                }
                else
                {
                    strTextbox = "<div class=\"vt-control\">" + "<div class=\"editor-label\">" + htmlHelper.Label("", label) + "</div>" + "<div class=\"editor-field\">" + htmlHelper.TextBoxFor<TModel, TProperty>(expression, htmlAttributes) + (ValidateMessage == true ? (htmlHelper.ValidationMessageFor(expression) == null ? "" : htmlHelper.ValidationMessageFor(expression).ToString()) : "") + "</div>" + "</div>";
                }
            }
            return new MvcHtmlString(strTextbox);
        }
        #endregion

        #region TextArea - more

        // Summary:
        //     Returns the specified textarea element and Label element by using the specified HTML helper
        //     and the name of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field to return.
        //
        // Returns:
        //     The textarea element and label element
        public static MvcHtmlString ViettelTextArea(this HtmlHelper htmlHelper, string label, string textAreaName, string value, bool required, IDictionary<string, object> htmlAttributes)
        {
            string strTextarea;
            if (required)
            {
                strTextarea = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(label)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + htmlHelper.TextArea(textAreaName, value, htmlAttributes)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strTextarea = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(label)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + htmlHelper.TextArea(textAreaName, value, htmlAttributes)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strTextarea);
        }

        public static MvcHtmlString ViettelTextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string label, bool required, IDictionary<string, object> htmlAttributes)
        {
            string strTextareaFor;
            if (required)
            {
                strTextareaFor = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(label)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + htmlHelper.TextAreaFor<TModel, TProperty>(expression, htmlAttributes)
                    + htmlHelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strTextareaFor = "<div class=\"vt-control\">"
                              + "<div class=\"editor-label\">"
                              + htmlHelper.Label(label)
                              + "</div>" + "<div class=\"editor-field\">"
                              + htmlHelper.TextAreaFor<TModel, TProperty>(expression, htmlAttributes)
                              + htmlHelper.ValidationMessageFor(expression)
                              + "</div>"
                              + "</div>";
            }
            return new MvcHtmlString(strTextareaFor);
        }
        #endregion

        #region Button
        // Summary:
        //     Returns a text input element by using the specified HTML helper and the name
        //     of the form field.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   name:
        //     The name of the form field and the System.Web.Mvc.ViewDataDictionary key
        //     that is used to look up the value.
        //
        // Returns:
        //     An input element whose type attribute is set to "button or submit" by user.

        //public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, string type = "button", string onclick = "", string cclass = "")
        //{
        //    string strButton = "<button type=\"" + type + "\" class=\"" + (string.IsNullOrEmpty(cclass) ? "ButtonStyle Sprite1" : cclass) + "\" onclick=\"" + onclick + "\"><span class=\"Sprite1\">" + label + "</span></button>";
        //    return new MvcHtmlString(strButton);
        //}

        //public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, IDictionary<string, object> htmlAttributes, string type = "button", string onclick = "", string cclass = "")
        //{
        //    string keyvalue = " ";
        //    foreach (KeyValuePair<string, object> pair in htmlAttributes)
        //    {
        //        string key = pair.Key;
        //        object value = pair.Value;
        //        keyvalue += key + "=\"" + value + "\" ";
        //    }
        //    string strButton = "<button type=\"" + type + "\" class=\"" + (string.IsNullOrEmpty(cclass) ? "ButtonStyle Sprite1" : cclass) + "\" onclick=\"" + onclick + "\" " + keyvalue + " ><span class=\"Sprite1\">" + label + "</span></button>";
        //    return new MvcHtmlString(strButton);
        //}

        //public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, string type = "button", string onclick = "", string cclass = "", string id = "", string addtionalData = "")
        //{
        //    //string strButton = "<input type=\"" + type + "\" value = \"" + label + "\" onclick = \"" + onclick + "\" class =\" " + cclass + "\" id =\"" + id + "\"/>";
        //    string strButton = "<button type=\"" + type + "\" class=\"" + (string.IsNullOrEmpty(cclass) ? "ButtonStyle Sprite1" : cclass) + "\" onclick=\"" + onclick + "\" id=\"" + id + "\" " + addtionalData + " ><span class=\"Sprite1\">" + label + "</span></button>";
        //    return new MvcHtmlString(strButton);
        //}

        public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, string type = "button", string onclick = "", string cclass = "")
        {
            string strButton = "<input type=\"" + type + "\" value = \"" + label + "\" onclick = \"" + onclick + "\" class =\" " + cclass + "\"/>";
            return new MvcHtmlString(strButton);
        }

        public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label,
            string type = "button", string onclick = "", bool isDisable = false)
        {
            string strButton = "<input type=\"" + type
                + "\" value = \"" + label
                + "\" onclick = \"" + onclick + "\" "
                + (isDisable ? " disabled=\"disabled\" " : "")
                + " class =\" " + (isDisable ? "t-state-disabled" : "") + "\"/>";
            return new MvcHtmlString(strButton);
        }

        public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, IDictionary<string, object> htmlAttributes, string type = "button", string onclick = "", string cclass = "")
        {
            string keyvalue = " ";
            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object value = pair.Value;
                keyvalue += key + "=\"" + value + "\" ";
            }
            string strButton = "<input type=\"" + type + "\" value = \"" + label + "\" onclick = \"" + onclick + "\" class =\"" + cclass + "\"" + keyvalue + "/>";
            return new MvcHtmlString(strButton);
        }
        public static MvcHtmlString ViettelButton(this HtmlHelper htmlHelper, string label, string type = "button", string onclick = "", string cclass = "", string id = "", string addtionalData = "")
        {

            string strButton = "<input type=\"" + type + "\" value = \"" + label + "\" onclick = \"" + onclick + "\" class =\"" + cclass + "\" id =\"" + id + "\"/>";

            //string strButton = "<input type=\"" + type + "\" value = \"" + label + "\" onclick = \"" + onclick + "\" class =\" " + cclass + "\" id =\"" + id + "\"/>";
            //string strButton = "<button type=\"" + type + "\" class=\"" + (string.IsNullOrEmpty(cclass) ? "ButtonStyle Sprite1" : "ButtonStyle Sprite1 "+cclass) + "\" onclick=\"" + onclick + "\" id=\"" + id + "\" " + addtionalData + " ><span class=\"Sprite1\">" + label + "</span></button>";

            return new MvcHtmlString(strButton);
        }
        #endregion

        #region Combobox - more
        public static MvcHtmlString ViettelCombobox(this HtmlHelper htmlHelper, string labelName, string name, IEnumerable<SelectListItem> selectList, int placeholderType, IDictionary<string, object> htmlAttributes, bool disable, bool required)
        {
            string optionLabel = "";
            string strCombobox;
            if (placeholderType == 0)
            {
                optionLabel = "[Lựa chọn]";
            }
            if (placeholderType == 1)
            {
                optionLabel = "[Tất cả]";
            }
            if (placeholderType == 2)
            {
                optionLabel = null;
            }
            if (placeholderType == 3)
            {
                optionLabel = "[Chưa xếp phòng thi]";
            }

            string htmlLabel = string.Empty;
            if (labelName != null && !labelName.Trim().Equals(string.Empty))
            {
                if (required)
                {
                    htmlLabel = "<div class=\"editor-label\">"
                    + htmlHelper.Label(labelName)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>";
                }
                else
                {
                    htmlLabel = "<div class=\"editor-label\">"
                        + htmlHelper.Label(labelName)
                        + "</div>";
                }
            }
            if (required)
            {
                strCombobox = "<div class=\"vt-control\">"
                    + htmlLabel
                    + "<div class=\"editor-field\">"
                    + htmlHelper.DropDownList(name, selectList, optionLabel, htmlAttributes)
                    + htmlHelper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strCombobox = "<div class=\"vt-control\">"
                    + htmlLabel
                    + "<div class=\"editor-field\">"
                    + htmlHelper.DropDownList(name, selectList, optionLabel, htmlAttributes)
                    + htmlHelper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }

            return new MvcHtmlString(strCombobox);
        }

        public static MvcHtmlString ViettelComboboxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string labelName, IEnumerable<SelectListItem> selectList, int placeholderType, IDictionary<string, object> htmlAttributes, bool required)
        {
            string optionLabel = "";
            string strCombobox;
            if (placeholderType == 0)
            {
                optionLabel = "[Lựa chọn]";
            }
            if (placeholderType == 1)
            {
                optionLabel = "[Tất cả]";
            }
            if (placeholderType == 2)
            {
                optionLabel = null;
            }
            if (placeholderType == 3)
            {
                optionLabel = "[Chưa xếp phòng thi]";
            }
            if (placeholderType == 4)
            {
                optionLabel = "[Trình độ]";
            }
            if (placeholderType == 5)
            {
                optionLabel = "[Hình thức]";
            }
            if (required)
            {
                 strCombobox = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label("", labelName)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes)
                    + htmlHelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strCombobox = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label("", labelName)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes)
                    + htmlHelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strCombobox);
        }
        #endregion

        #region Datepicker
        public static MvcHtmlString ViettelDatePicker(this HtmlHelper helper, string name, string labelname, DateTime? value, bool required, IDictionary<string, object> htmlAttributes, string DateTimeFormat = "dd/MM/yyyy")
        {
            string callTelerik = helper.Telerik().DatePicker().Name(name).Format(DateTimeFormat).HtmlAttributes(htmlAttributes).Value(value).ToString();
            string strDatePicker;

            if (required)
            {
                strDatePicker = "<div class=\"vt-control\">";
                if (string.IsNullOrEmpty(labelname))
                {
                    strDatePicker += callTelerik
                   + helper.ValidationMessage(name)
                   + "</div>";
                }
                else
                {
                    strDatePicker += "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + helper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
                }


            }
            else
            {
                strDatePicker = "<div class=\"vt-control\">";
                if (string.IsNullOrEmpty(labelname))
                {
                    strDatePicker += callTelerik
                   + helper.ValidationMessage(name)
                  + "</div>";
                }
                else
                {
                    strDatePicker += "<div class=\"editor-label\">"
                   + helper.Label(labelname)
                   + "</div>"
                   + "<div class=\"editor-field\">"
                   + callTelerik
                    + helper.ValidationMessage(name)
                   + "</div>"
                   + "</div>";
                }

            }
            return new MvcHtmlString(strDatePicker);
        }

        public static MvcHtmlString ViettelDateTimePicker(this HtmlHelper helper, string name, string labelname, DateTime? value, bool required, IDictionary<string, object> htmlAttributes, string DateTimeFormat = "dd/MM/yyyy HH:mm:ss")
        {
            string callTelerik = helper.Telerik().DateTimePicker().Name(name).Format(DateTimeFormat).HtmlAttributes(htmlAttributes).Value(value).ToString();
            string strDatePicker;
            if (required)
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + helper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                     + helper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strDatePicker);
        }
        public static MvcHtmlString ViettelDatePickerFor<TModel>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, DateTime?>> expression, bool required, IDictionary<string, object> htmlAttributes, string DateTimeFormat = "dd/MM/yyyy")
        {
            string strDatePickerFor;
            string callTelerik = htmlhelper.Telerik().DatePickerFor(expression).Format(DateTimeFormat).HtmlAttributes(htmlAttributes).ToString();
            if (required)
            {
                strDatePickerFor = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlhelper.LabelFor(expression)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + htmlhelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strDatePickerFor = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlhelper.LabelFor(expression)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + htmlhelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strDatePickerFor);
        }

        public static MvcHtmlString ViettelDatePickerFor<TModel>(this HtmlHelper<TModel> htmlhelper, Expression<Func<TModel, DateTime?>> expression, string label, bool required, IDictionary<string, object> htmlAttributes, string DateTimeFormat = "dd/MM/yyyy")
        {
            string strDatePickerFor;
            string callTelerik = htmlhelper.Telerik().DatePickerFor(expression).Format(DateTimeFormat).HtmlAttributes(htmlAttributes).ToString();
            if (required)
            {
                strDatePickerFor = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + label
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + htmlhelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strDatePickerFor = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + label
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + htmlhelper.ValidationMessageFor(expression)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strDatePickerFor);
        }

        public static MvcHtmlString ViettelChart(this HtmlHelper helper, string name, string labelname, DateTime? value, bool required, IDictionary<string, object> htmlAttributes)
        {
            //string callTelerik = helper.Telerik().DatePicker().Name(name).HtmlAttributes(htmlAttributes).Value(value).ToString();
            string callTelerik = helper.Telerik().Chart()
                .Name(name)
                .Title("Test Chart for Telerik")
                .Legend(legend => legend.Position(ChartLegendPosition.Bottom))
                //.DataBinding(expression)
                .ToString();
            string strDatePicker;
            if (required)
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strDatePicker);
        }

        public static MvcHtmlString ViettelTimePicker(this HtmlHelper helper, string name, string labelname, DateTime? value, bool required, IDictionary<string, object> htmlAttributes, string DateTimeFormat = "HH:mm:ss")
        {
            string callTelerik = helper.Telerik().TimePicker().Name(name).Format(DateTimeFormat).HtmlAttributes(htmlAttributes).Value(value).ToString();
            string strDatePicker;
            if (required)
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                    + helper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strDatePicker = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + helper.Label(labelname)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + callTelerik
                     + helper.ValidationMessage(name)
                    + "</div>"
                    + "</div>";
            }
            return new MvcHtmlString(strDatePicker);
        }
        #endregion

        #region Checkbox - more
        //public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes, string labelName, bool cchecked, bool required, bool labelOnRight = false, bool cdisable = false)
        //{
        //    string strCheckbox;
        //    string check = cchecked ? "checked" : "";
        //    string disable = cdisable ? "disabled" : "";
        //    string rtl = "";
        //    string tmp = "";
        //    if (labelOnRight)
        //    {
        //        rtl = "style='direction:rtl;text-align: left;'";
        //        tmp = "-right";
        //    }
        //    if (required)
        //    {
        //        strCheckbox = "<div class=\"vt-control\" " + rtl + ">"
        //            + "<div class=\"editor-label" + tmp + "\">"
        //            + htmlHelper.Label(labelName)
        //            + " <span style=\"color: red;\">*</span>"
        //            + "</div>"
        //            + "<div class=\"editor-checkbox-field\">"
        //            //+ htmlHelper.CheckBox(name, cchecked, htmlAttributes)
        //            + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\" ";
        //        if (htmlAttributes != null)
        //        {
        //            foreach (string key in htmlAttributes.Keys)
        //            {
        //                strCheckbox += key + "=\"" + htmlAttributes[key] + "\" ";
        //            }
        //        }
        //        strCheckbox += "/>"
        //            + "</div>"
        //            + "</div>";
        //    }
        //    else
        //    {
        //        strCheckbox = "<div class=\"vt-control\"" + rtl + ">"
        //            + "<div class=\"editor-label" + tmp + "\">"
        //            + htmlHelper.Label(labelName)
        //            + "</div>"
        //            + "<div class=\"editor-checkbox-field\">"
        //            //+ htmlHelper.CheckBox(name, cchecked, htmlAttributes)
        //            + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\" ";
        //        if (htmlAttributes != null)
        //        {
        //            foreach (string key in htmlAttributes.Keys)
        //            {
        //                strCheckbox += key + "=\"" + htmlAttributes[key] + "\" ";
        //            }
        //        }
        //        strCheckbox += "/>"
        //            + "</div>"
        //            + "</div>";
        //    }

        //    return new MvcHtmlString(strCheckbox);
        //}
        //hath
        public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, object value, IDictionary<string, object> htmlAttributes, string labelName, bool cchecked, bool required, bool labelOnRight = false, bool cdisable = false, bool labelOff=false)
        {
            string strCheckbox = "";
            string check = cchecked ? "checked" : "";
            string disable = cdisable ? "disabled" : "";
            string rtl = "";
            string tmp = "";
            if (labelOnRight)
            {
                rtl = "style='direction:rtl;text-align: left;'";
                tmp = "-right";
            }
            if (labelOff)
            {
                strCheckbox = "<label class='custom-checkbox'>"
                    //+ htmlHelper.CheckBox(name, cchecked, htmlAttributes)
                       + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\" ";
                if (htmlAttributes != null)
                {
                    foreach (string key in htmlAttributes.Keys)
                    {
                        strCheckbox += key + "=\"" + htmlAttributes[key] + "\" ";
                    }
                }
                strCheckbox += "/>"
                    + "<span class='checkmark'></span>"
                    + "</label>";
            }
            else
            {
                if (required)
                {
                    strCheckbox = "<label class='custom-checkbox'>"
                        + labelName
                        + " <span style=\"color: red;\">*</span>"
                        + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\" ";
                    if (htmlAttributes != null)
                    {
                        foreach (string key in htmlAttributes.Keys)
                        {
                            strCheckbox += key + "=\"" + htmlAttributes[key] + "\" ";
                        }
                    }
                    strCheckbox += "/>"
                        + "<span class='checkmark'></span>"
                        + "</label>";
                }
                else
                {
                    strCheckbox = "<label class='custom-checkbox'>"
                        + labelName
                        + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\" ";
                    if (htmlAttributes != null)
                    {
                        foreach (string key in htmlAttributes.Keys)
                        {
                            strCheckbox += key + "=\"" + htmlAttributes[key] + "\" ";
                        }
                    }
                    strCheckbox += "/>"
                        + "<span class='checkmark'></span>"
                        + "</label>";
                }
            }
            return new MvcHtmlString(strCheckbox);
        }

        //public static MvcHtmlString ViettelCheckBox(this HtmlHelper htmlHelper, string name, object value,object id, string labelName,bool ddisabled, bool cchecked, bool required)
        //{
        //    string strCheckbox;
        //    string check = cchecked ? "checked" : "";
        //    string disable = ddisabled ? "disabled" : "";
        //    if (required)
        //    {
        //        strCheckbox = "<div class=\"vt-control\">"
        //            + "<div class=\"editor-label\">"
        //            + htmlHelper.Label(labelName)
        //            + " <span style=\"color: red;\">*</span>"
        //            + "</div>"
        //            + "<div class=\"editor-field\">"
        //            //+ htmlHelper.CheckBox(name, cchecked, htmlAttributes)
        //            + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\"" + " id=\"" + id + "\" />"
        //            + "</div>"
        //            + "</div>";
        //    }
        //    else
        //    {
        //        strCheckbox = "<div class=\"vt-control\">"
        //            + "<div class=\"editor-label\">"
        //            + htmlHelper.Label(labelName)
        //            + "</div>"
        //            + "<div class=\"editor-field\">"
        //            //+ htmlHelper.CheckBox(name, cchecked, htmlAttributes)
        //             + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " " + disable + " value=\"" + value + "\"" + " id=\"" + id + "\" />"
        //            + "</div>"
        //            + "</div>";
        //    }

        //    return new MvcHtmlString(strCheckbox);
        //}

        public static MvcHtmlString ViettelCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, IDictionary<string, object> htmlAttributes, string labelName, bool required, bool labelOnRight = false)
        {
            string strCheckboxFor;
            string rtl = "";
            string tmp = "";
            if (labelOnRight)
            {
                rtl = "style='direction:rtl;text-align: left;'";
                tmp = "-right";
            }
            if (required)
            {
                strCheckboxFor = "<label class='custom-checkbox'>"
                    + labelName
                    + " <span style=\"color: red;\">*</span>"
                    + htmlHelper.CheckBoxFor(expression, htmlAttributes)
                    + "<span class='checkmark'></span>"
                    + "</label>";
            }
            else
            {
                strCheckboxFor = "<label class='custom-checkbox'>"
                    + labelName
                    + htmlHelper.CheckBoxFor(expression, htmlAttributes)
                    + "<span class='checkmark'></span>"
                    + "</label>";
            }
            return new MvcHtmlString(strCheckboxFor);
        }
        #endregion

        #region ListCheckbox

        public static MvcHtmlString ViettelListCheckBox(this HtmlHelper htmlHelper, string name, string labelName, bool required, List<ViettelCheckboxList> selectList)
        {
            string strListcheckbox = "";
            string strListcheckboxfull;
            if (selectList != null)
            {
                for (int i = 0; i < selectList.Count(); i++)
                {
                    //strListcheckbox += " <input type=\"checkbox\" value=\"\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\"/>" + htmlHelper.Label(selectList[i].Text);
                    bool disabled = selectList[i].disabled;
                    string check = selectList[i].cchecked ? "checked" : "";
                    if (disabled)
                    {
                        strListcheckbox += 
                            //"<div class=\"vt-checkbox-item\">"
                            //+ "<div class=\"editor-field\">"
                            //+ htmlHelper.CheckBox(name, selectList[i].cchecked, new { @disabled = "true", @value = selectList[i].Value })
                            "<label class=\"custom-checkbox\">"
                            + selectList[i].Label
                            +"<input type=\"checkbox\" name=\"" + name + "\" disabled " + check + " value=\"" + selectList[i].Value + "\" />"
                            + "<span class=\"checkmark\"></span></label></br>"
                            //+ "</div>"
                            //+ "<div class=\"editor-label\">"
                            //+ htmlHelper.Label(selectList[i].Label)
                            //+ "</div>"
                            //+ "</div>"
                            ;
                    }
                    else
                    {
                        strListcheckbox +=
                            //"<div class=\"vt-checkbox-item\">"
                            //+ "<div class=\"editor-field\">"
                            //+ htmlHelper.CheckBox(name, selectList[i].cchecked, new { @value = selectList[i].Value })
                            "<label class=\"custom-checkbox\">"
                            + selectList[i].Label
                            + "<input type=\"checkbox\" name=\"" + name + "\" " + check + " value=\"" + selectList[i].Value + "\" />"
                            + "<span class=\"checkmark\"></span></label></br>"
                            //+ "</div>"
                            //+ "<div class=\"editor-label\">"
                            //+ htmlHelper.Label(selectList[i].Label)
                            //+ "</div>"
                            //+ "</div>"
                            ;
                    }

                }
            }
            if (required)
            {
                if (string.IsNullOrEmpty(labelName))
                {
                    strListcheckboxfull = 
                        "<div class=\"vt-control\">"
                        + "<div class=\"editor-label\">"
                        + " <span style=\"color: red;\">*</span>"
                        + "</div>"
                       + "<div class=\"editor-field vt-list-checkbox\">"
                       + strListcheckbox
                       + "</div>"
                       + "</div>"
                       ;
                }
                else
                {
                    strListcheckboxfull = "<div class=\"vt-control\">"
                        + "<div class=\"editor-label\">"
                        + htmlHelper.Label(labelName)
                        + " <span style=\"color: red;\">*</span>"
                        + "</div>"
                        + "<div class=\"editor-field vt-list-checkbox\">"
                        + strListcheckbox
                        + "</div>"
                        + "</div>";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(labelName))
                {
                    strListcheckboxfull = "<div class=\"vt-control\">"
                       + "<div class=\"editor-field vt-list-checkbox\">"
                       + strListcheckbox
                       + "</div>"
                       + "</div>";
                }
                else
                {
                    strListcheckboxfull = "<div class=\"vt-control\">"
                        + "<div class=\"editor-label\">"
                        + htmlHelper.Label(labelName)
                        + "</div>"
                        + "<div class=\"editor-field vt-list-checkbox\">"
                        + strListcheckbox
                        + "</div>"
                        + "</div>";
                }
            }
            return new MvcHtmlString(strListcheckboxfull);
        }
        #endregion

        public static MvcHtmlString ViettelComboboxMultipleSelection(this HtmlHelper htmlHelper, string name, string labelName, List<SelectListItem> selectList, bool required, string placeHolder, int width = 100)
        {
            string result = "<script type=\"text/javascript\">"
                          + "     $(document).ready(function () {"
                          + "          $(\"#" + name + "\").dropdownchecklist({ emptyText: \"" + placeHolder + "\", width: " + width + "});"
                          + "      });"
                          + "  </script>";
            result += "<div class=\"vt-control\">";
            result += (labelName == null || labelName == "") ? "" : "<div class=\"editor-label\">" + labelName + (required ? " <span style=\"color: red;\">*</span>" : "") + "</div>";

            result += "<div class=\"editor-field vt-combobox-multiple-selection\">"
                   + "<select multiple=\"multiple\" name=\"" + name + "\" id=\"" + name + "\">";

            foreach (SelectListItem item in selectList)
            {
                if (item.Selected)
                {
                    result += "<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>";
                }
                else
                {
                    result += "<option value=\"" + item.Value + "\">" + item.Text + "</option>";
                }
            }

            result += "</select>"
                   + "</div>"
                   + "</div>";
            return new MvcHtmlString(result);
        }


        public static MvcHtmlString ViettelListButtonStyle(this HtmlHelper htmlHelper, string id, string value, IDictionary<string, object> htmlAttributes, string JavaScriptMethod, List<ViettelCheckboxList> selectList, bool newLine = false)
        {

            string keyvalue = " ";
            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object val = pair.Value;
                keyvalue += key + "=\"" + val + "\" ";
            }

            string strButtonfull = "<ul class=\"ResetList TabOptionList\" id=\"" + id + "\" " + keyvalue + ">";

            if (selectList != null)
            {
                for (int i = 0; i < selectList.Count(); i++)
                {
                    bool ischeck = selectList[i].cchecked;
                    bool disable = selectList[i].disabled;
                    if (newLine)
                    {
                        strButtonfull += "<li class=\"Item1\" value=\"" + selectList[i].Value + "\"><a href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" class=\"ButtonStyle Sprite1\"><span class=\"Sprite1\"  >" + selectList[i].Label + "</span></a></li>";
                    }
                    else
                    {
                        strButtonfull += "<li value=\"" + selectList[i].Value + "\"><a href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" class=\"ButtonStyle Sprite1\"><span class=\"Sprite1\">" + selectList[i].Label + "</span></a></li>";
                    }
                }
            }
            strButtonfull += "</ul>";
            return new MvcHtmlString(strButtonfull);
        }

        public static MvcHtmlString ViettelListButtonStyle2(this HtmlHelper htmlHelper, string id, string value, IDictionary<string, object> htmlAttributes, string JavaScriptMethod, List<ViettelCheckboxList> selectList, bool newLine = false)
        {

            string keyvalue = " ";
            string active = "";
            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object val = pair.Value;
                keyvalue += key + "=\"" + val + "\" ";
            }

            string strButtonfull = "<div class=\"btn-group\" id=\"" + id + "\" " + keyvalue + ">";

            if (selectList != null)
            {
                for (int i = 0; i < selectList.Count(); i++)
                {
                    active = "";
                    bool ischeck = selectList[i].cchecked;
                    bool disable = selectList[i].disabled;

                    if (selectList[i].Value.ToString() == value)
                    {
                        active = "Active";
                    }
                    if (ischeck)
                    {
                        strButtonfull += "<a class='btn btn-bold btn-primary " + active + "' href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" value=\"" + selectList[i].Value + "\"  \">" + selectList[i].Label + "<span class='icon icon-check-circle'></span></a>";
                    }
                    else
                    {
                        strButtonfull += "<a class='btn btn-bold btn-primary " + active + "' href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" value=\"" + selectList[i].Value + "\" \">" + selectList[i].Label + "</a>";
                    }
                }
            }
            strButtonfull += "</div>";
            return new MvcHtmlString(strButtonfull);
        }
        public static MvcHtmlString ViettelListButtonStyleTK(this HtmlHelper htmlHelper, string id, string value, IDictionary<string, object> htmlAttributes, string JavaScriptMethod, List<ViettelCheckboxList> selectList, bool newLine = false)
        {

            string keyvalue = " ";
            string active = "";
            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object val = pair.Value;
                keyvalue += key + "=\"" + val + "\" ";
            }

            string strButtonfull = "<ul class=\"ResetList\" id=\"" + id + "\" " + keyvalue + ">";

            if (selectList != null)
            {
                for (int i = 0; i < selectList.Count(); i++)
                {
                    active = "";
                    bool ischeck = selectList[i].cchecked;
                    bool disable = selectList[i].disabled;
                    if (selectList[i].Value.ToString() == value)
                    {
                        active = "Active";
                    }
                    if (newLine)
                    {
                        strButtonfull += "<li value=\"" + selectList[i].Value + "\"><a href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" class=\"ButtonStyleTK " + active + "\">" + selectList[i].Label + "</a></li>";
                    }
                    else
                    {
                        strButtonfull += "<li value=\"" + selectList[i].Value + "\" style=\"float:left;\"><a href=\"javascript:void(0);\" onclick=\"" + JavaScriptMethod + "\" class=\"ButtonStyleTK " + active + "\">" + selectList[i].Label + "</a></li>";
                    }
                }
            }
            strButtonfull += "</ul>";
            return new MvcHtmlString(strButtonfull);
        }
        #region Radiobox

        public static MvcHtmlString ViettelRadioBox(this HtmlHelper htmlHelper, string name, string labelName, IDictionary<string, object> htmlAttributes, List<ViettelCheckboxList> selectList, bool required, bool newLine = false, int colum = 1)
        {
            string strRadiobox = "";
            string strRadiobocfull;
            string keyvalue = " ";

            if (labelName == null || labelName == "")
            {
                labelName = " ";
            }

            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object value = pair.Value;
                keyvalue += key + "=\"" + value + "\" ";
            }
            if (selectList != null)
            {
                if (colum == 1)
                {
                    for (int i = 0; i < selectList.Count(); i++)
                    {
                        bool ischeck = selectList[i].cchecked;
                        bool disable = selectList[i].disabled;
                        if (newLine)
                        {
                            if (ischeck && disable)
                            {
                                strRadiobox += " <" + "br>" +"<label class=\"custom-radio-button\">"+selectList[i].Label+ " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                        else
                        {
                            if (ischeck && disable)
                            {
                                strRadiobox += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                strRadiobox += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                strRadiobox += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                strRadiobox += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                    }
                }
                else
                {
                    //dungnt test
                    List<string> lsRadio = new List<string>(colum);
                    for (int j = 0; j < colum; j++)
                    {
                        lsRadio.Add("");
                    }
                    for (int i = 0; i < selectList.Count(); i++)
                    {
                        bool ischeck = selectList[i].cchecked;
                        bool disable = selectList[i].disabled;

                        if (newLine)
                        {
                            if (ischeck && disable)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                        else
                        {
                            if (ischeck && disable)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + selectList[i].Value + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                lsRadio[i % colum] += "<label class=\"custom-radio-button\">" + selectList[i].Label + " <input type=\"radio\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                      
                    }
                    int width = 100 / colum;
                    string beginDiv = "<div style=\"float: left;width:"+width+"%\">";
                    string endDiv = "</div>";
                    foreach (var item in lsRadio)
                    {
                        strRadiobox += beginDiv + item + endDiv;
                    }
                }
            }

            strRadiobox = strRadiobox.Trim().StartsWith("<br>") ? strRadiobox.Trim().Substring(4) : strRadiobox; // Hieund9 16/05/2013 - Loai bo xuong dong dau tien

            if (required)
            {
                strRadiobocfull = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(labelName)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + strRadiobox
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strRadiobocfull = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(labelName)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + strRadiobox
                    + "</div>"
                    + "</div>";
            }
            if (labelName == " ")
            {
                return new MvcHtmlString(strRadiobox);
            }
            return new MvcHtmlString(strRadiobocfull);
        }

        public static MvcHtmlString ViettelListCheckBox(this HtmlHelper htmlHelper, string name, string labelName, IDictionary<string, object> htmlAttributes, List<ViettelCheckboxList> selectList, bool required, bool newLine = false, int colum = 1)
        {
            string strRadiobox = "";
            string strRadiobocfull;
            string keyvalue = " ";

            if (labelName == null || labelName == "")
            {
                labelName = " ";
            }

            foreach (KeyValuePair<string, object> pair in htmlAttributes)
            {
                string key = pair.Key;
                object value = pair.Value;
                keyvalue += key + "=\"" + value + "\" ";
            }
            if (selectList != null)
            {
                if (colum == 1)
                {
                    for (int i = 0; i < selectList.Count(); i++)
                    {
                        bool ischeck = selectList[i].cchecked;
                        bool disable = selectList[i].disabled;
                        if (newLine)
                        {
                            if (ischeck && disable)
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-checkbox\">"+selectList[i].Label+"<input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                strRadiobox += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                        else
                        {
                            if (ischeck && disable)
                            {
                                strRadiobox += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                strRadiobox += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                strRadiobox += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                strRadiobox += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                    }
                }
                else
                {
                    //dungnt test
                    List<string> lsRadio = new List<string>(colum);
                    for (int j = 0; j < colum; j++)
                    {
                        lsRadio.Add("");
                    }
                    for (int i = 0; i < selectList.Count(); i++)
                    {
                        bool ischeck = selectList[i].cchecked;
                        bool disable = selectList[i].disabled;

                        if (newLine)
                        {
                            if (ischeck && disable)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                lsRadio[i % colum] += " <" + "br>" + "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }
                        else
                        {
                            if (ischeck && disable)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked disabled " + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == false && disable == true)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" disabled" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else if (ischeck == true && disable == false)
                            {
                                lsRadio[i % colum] += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\" checked" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                            else
                            {
                                lsRadio[i % colum] += "<label class=\"custom-checkbox\">" + selectList[i].Label + " <input type=\"checkbox\" value=\"" + selectList[i].Value + "\" name=\"" + name + "\" id = \"" + name + i + "\"" + keyvalue + "/><span class=\"checkmark\"></span></label>";
                            }
                        }

                    }
                    int width = 100 / colum;
                    string beginDiv = "<div style=\"float: left;width:" + width + "%\">";
                    string endDiv = "</div>";
                    foreach (var item in lsRadio)
                    {
                        strRadiobox += beginDiv + item + endDiv;
                    }
                }
            }

            strRadiobox = strRadiobox.Trim().StartsWith("<br>") ? strRadiobox.Trim().Substring(4) : strRadiobox; // Hieund9 16/05/2013 - Loai bo xuong dong dau tien

            if (required)
            {
                strRadiobocfull = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(labelName)
                    + " <span style=\"color: red;\">*</span>"
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + strRadiobox
                    + "</div>"
                    + "</div>";
            }
            else
            {
                strRadiobocfull = "<div class=\"vt-control\">"
                    + "<div class=\"editor-label\">"
                    + htmlHelper.Label(labelName)
                    + "</div>"
                    + "<div class=\"editor-field\">"
                    + strRadiobox
                    + "</div>"
                    + "</div>";
            }
            if (labelName == " ")
            {
                return new MvcHtmlString(strRadiobox);
            }
            return new MvcHtmlString(strRadiobocfull);
        }

        //public static MvcHtmlString ViettelChartTest(this HtmlHelper htmlHelper)
        //{
        //    Chart chart1 = new Chart();
        //    Random random = new Random();
        //    chart1.Series.Add("Series1");
        //    for (int pointIndex = 0; pointIndex < 10; pointIndex++)
        //    {

        //        chart1.Series["Series1"].Points.AddY(random.Next(20, 100));
        //    }
        //    chart1.Series["Series1"].ChartType = SeriesChartType.Pie;
        //    chart1.Series["Series1"]["PointWidth"] = "0.5";// Show data points labels
        //    chart1.Series["Series1"].IsValueShownAsLabel = true;// Set data points label style
        //    chart1.Series["Series1"]["BarLabelStyle"] = "Center";// Show chart as 3D
        //    chart1.ChartAreas.Add("ChartArea1");
        //    chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;// Draw chart as 3D 
        //    chart1.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //    string a = "<Chart BackColor=\"#D3DFF0\" BackGradientStyle=\"TopBottom\" BackSecondaryColor=\"White\" BorderColor=\"26, 59, 105\" BorderlineDashStyle=\"Solid\" BorderWidth=\"2\" Palette=\"BrightPastel\">\r\n <ChartAreas>\r\n <ChartArea Name=\"Default\" _Template_=\"All\" BackColor=\"64, 165, 191, 228\" BackGradientStyle=\"TopBottom\" BackSecondaryColor=\"White\" BorderColor=\"64, 64, 64, 64\" BorderDashStyle=\"Solid\" ShadowColor=\"Transparent\" /> \r\n </ChartAreas>\r\n <Legends>\r\n <Legend _Template_=\"All\" BackColor=\"Transparent\" Font=\"Trebuchet MS, 8.25pt, style=Bold\" IsTextAutoFit=\"False\" /> \r\n </Legends>\r\n <BorderSkin SkinStyle=\"Emboss\" /> \r\n </Chart>";
        //    return new MvcHtmlString(a);
        //    //return new MvcHtmlString(chart1.ToString());
        //}
        #endregion

        #region ValidationMessage
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName)
        {
            return htmlHelper.ValidationMessage(modelName);
        }
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ValidationMessage(modelName, htmlAttributes);
        }
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName, object htmlAttributes)
        {
            return htmlHelper.ValidationMessage(modelName, htmlAttributes);
        }
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName, string validationMessage)
        {
            return htmlHelper.ValidationMessage(modelName, validationMessage);
        }
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName, string validationMessage, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ValidationMessage(modelName, validationMessage, htmlAttributes);
        }
        //
        // Summary:
        //     Displays a validation message if an error exists for the specified field
        //     in the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   modelName:
        //     The name of the property or model object that is being validated.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessage(this HtmlHelper htmlHelper, string modelName, string validationMessage, object htmlAttributes)
        {
            return htmlHelper.ValidationMessage(modelName, validationMessage, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the HTML markup for a validation-error message for each data field
        //     that is represented by the specified expression.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.ValidationMessageFor<TModel, TProperty>(expression);
        }
        //
        // Summary:
        //     Returns the HTML markup for a validation-error message for each data field
        //     that is represented by the specified expression, using the specified message.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage)
        {
            return htmlHelper.ValidationMessageFor<TModel, TProperty>(expression, validationMessage);
        }
        //
        // Summary:
        //     Returns the HTML markup for a validation-error message for each data field
        //     that is represented by the specified expression, using the specified message
        //     and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ValidationMessageFor<TModel, TProperty>(expression, validationMessage, htmlAttributes);
        }
        //
        // Summary:
        //     Returns the HTML markup for a validation-error message for each data field
        //     that is represented by the specified expression, using the specified message
        //     and HTML attributes.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   expression:
        //     An expression that identifies the object that contains the properties to
        //     render.
        //
        //   validationMessage:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Type parameters:
        //   TModel:
        //     The type of the model.
        //
        //   TProperty:
        //     The type of the property.
        //
        // Returns:
        //     If the property or object is valid, an empty string; otherwise, a span element
        //     that contains an error message.
        public static MvcHtmlString ViettelValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, object htmlAttributes)
        {
            return htmlHelper.ValidationMessageFor<TModel, TProperty>(expression, validationMessage, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper)
        {
            return htmlHelper.ValidationSummary();
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object and optionally displays only
        //     model-level errors.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   excludePropertyErrors:
        //     true to have the summary display model-level errors only, or false to have
        //     the summary display all errors.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors)
        {
            return htmlHelper.ValidationSummary(excludePropertyErrors);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HMTL helper instance that this method extends.
        //
        //   message:
        //     The message to display if the specified field contains an error.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, string message)
        {
            return htmlHelper.ValidationSummary(message);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object and optionally displays only
        //     model-level errors.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   excludePropertyErrors:
        //     true to have the summary display model-level errors only, or false to have
        //     the summary display all errors.
        //
        //   message:
        //     The message to display with the validation summary.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message)
        {
            return htmlHelper.ValidationSummary(excludePropertyErrors, message);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   message:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes for the element.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, string message, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ValidationSummary(message, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages in the System.Web.Mvc.ModelStateDictionary
        //     object.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   message:
        //     The message to display if the specified field contains an error.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, string message, object htmlAttributes)
        {
            return htmlHelper.ValidationSummary(message, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object and optionally displays only
        //     model-level errors.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   excludePropertyErrors:
        //     true to have the summary display model-level errors only, or false to have
        //     the summary display all errors.
        //
        //   message:
        //     The message to display with the validation summary.
        //
        //   htmlAttributes:
        //     A dictionary that contains the HTML attributes for the element.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message, IDictionary<string, object> htmlAttributes)
        {
            return htmlHelper.ValidationSummary(excludePropertyErrors, message, htmlAttributes);
        }
        //
        // Summary:
        //     Returns an unordered list (ul element) of validation messages that are in
        //     the System.Web.Mvc.ModelStateDictionary object and optionally displays only
        //     model-level errors.
        //
        // Parameters:
        //   htmlHelper:
        //     The HTML helper instance that this method extends.
        //
        //   excludePropertyErrors:
        //     true to have the summary display model-level errors only, or false to have
        //     the summary display all errors.
        //
        //   message:
        //     The message to display with the validation summary.
        //
        //   htmlAttributes:
        //     An object that contains the HTML attributes for the element.
        //
        // Returns:
        //     A string that contains an unordered list (ul element) of validation messages.
        public static MvcHtmlString ViettelValidationSummary(this HtmlHelper htmlHelper, bool excludePropertyErrors, string message, object htmlAttributes)
        {
            return htmlHelper.ValidationSummary(excludePropertyErrors, message, htmlAttributes);
        }

        #endregion

        #region viettel detailt -dungnt
        public static MvcHtmlString ViettelDetailFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes = null)
        {
            if (htmlAttributes == null)
            {
                htmlAttributes = new Dictionary<string, object>()
                    {
                    {"disabled","disabled"}    
                    };
            }
            //aunh1
            ModelMetadata fieldMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);

            string label = html.LabelFor(expression).ToString() + ":";

            string control = html.EditorFor(expression).ToString();
            string htmlAtt = " ";
            foreach (string s in htmlAttributes.Keys)
            {
                htmlAtt += s + "=\"" + htmlAttributes[s] + "\"";
                htmlAtt += " ";
            }

            control = control.Replace("<input ", "<input " + htmlAtt);
            control = control.Replace("<select ", "<select " + htmlAtt);
            control = control.Replace("<textarea ", "<textarea " + htmlAtt);

            var propertyInfo = (expression.Body as MemberExpression).Member as PropertyInfo;
            string PropertyName = "";
            if (propertyInfo != null)
            {
                PropertyName = propertyInfo.Name;
            }

            string result = "<div class=\"vt-control " + PropertyName + "\">"
                          + "<div class=\"editor-label\">"
                          + label
                          + "</div>"
                          + "<div class=\"editor-field\">"
                          + control
                          + "</div>"
                          + "</div>";




            return new MvcHtmlString(result);
        }
        public static MvcHtmlString ViettelDetailFor_NoColon<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes = null)
        {
            if (htmlAttributes == null)
            {
                htmlAttributes = new Dictionary<string, object>()
                    {
                    {"disabled","disabled"}    
                    };
            }
            //aunh1
            ModelMetadata fieldMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);

            string label = html.LabelFor(expression).ToString() + " ";

            string control = html.EditorFor(expression).ToString();
            string htmlAtt = " ";
            foreach (string s in htmlAttributes.Keys)
            {
                htmlAtt += s + "=\"" + htmlAttributes[s] + "\"";
                htmlAtt += " ";
            }

            control = control.Replace("<input ", "<input " + htmlAtt);
            control = control.Replace("<select ", "<select " + htmlAtt);
            control = control.Replace("<textarea ", "<textarea " + htmlAtt);

            var propertyInfo = (expression.Body as MemberExpression).Member as PropertyInfo;
            string PropertyName = "";
            if (propertyInfo != null)
            {
                PropertyName = propertyInfo.Name;
            }

            string result = "<div class=\"vt-control " + PropertyName + "\">"
                          + "<div class=\"editor-label\">"
                          + label
                          + "</div>"
                          + "<div class=\"editor-field\">"
                          + control
                          + "</div>"
                          + "</div>";




            return new MvcHtmlString(result);
        }

        public static MvcHtmlString ViettelDetailFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> labelAttributes, IDictionary<string, object> controlAttributes)
        {
            ModelMetadata fieldMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);

            string label = html.LabelFor(expression).ToString();
            string labelAttrs = string.Empty;

            if (labelAttributes != null)
                foreach (var item in labelAttributes)
                    labelAttrs += item.Key + "=\"" + item.Value + "\"";
            label = label.Replace("<label ", "<label " + labelAttrs + " ");

            string control = html.EditorFor(expression).ToString();
            string controlAttrs = string.Empty;

            if (controlAttributes != null)
                foreach (var item in controlAttributes)
                    controlAttrs += item.Key + "=\"" + item.Value + "\"";
            control = control.Replace("<input ", "<input disabled=\"disabled\" " + controlAttrs + " ");
            control = control.Replace("<select ", "<select disabled=\"disabled\" " + controlAttrs + " ");
            control = control.Replace("<textarea ", "<textarea disabled=\"disabled\" " + controlAttrs + " ");
            string result = label + control;
            return new MvcHtmlString(result);
        }
        #endregion

        #endregion


        #region helper extension of telerik
        private static IDictionary<string, int> controllerToProductIdMap = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase)
            {
                { "grid", 718 },
                { "menu", 719 },
                { "panelbar", 720 },
                { "tabstrip", 721 }
            };

        public static ExampleConfigurator Configurator(this HtmlHelper html, string title)
        {
            return new ExampleConfigurator(html)
                        .Title(title);
        }
        //        #region not use
        //        public static string ExampleTitle(this HtmlHelper html)
        //        {
        //            XmlSiteMap sitemap = (XmlSiteMap)html.ViewData["telerik.mvc.examples"];
        //            string controller = (string)html.ViewContext.RouteData.Values["controller"];
        //            string action = (string)html.ViewContext.RouteData.Values["action"];

        //            SiteMapNode exampleSiteMapNode = sitemap.RootNode.ChildNodes
        //                .SelectRecursive(node => node.ChildNodes)
        //                .FirstOrDefault(node => controller.Equals(node.ControllerName, StringComparison.OrdinalIgnoreCase) &&
        //                    action.Equals(node.ActionName, StringComparison.OrdinalIgnoreCase));

        //            if (exampleSiteMapNode != null)
        //            {
        //                return exampleSiteMapNode.Title;
        //            }

        //            return string.Empty;
        //        }

        //        public static string ProductVersion(this HtmlHelper html)
        //        {
        //            Assembly controlAssembly = typeof(Menu).Assembly;
        //            Version version = controlAssembly.GetName().Version;

        //            int quarter = version.Minor;
        //            int versionYear = version.Major;
        //            int year = versionYear;
        //            int date = version.Build;
        //            int month = date / 100;

        //            if (month > 12)
        //            {
        //                year++;
        //                month %= 12;
        //            }

        //            int day = date % 100;

        //            return string.Format("Version Q{0} {1}, released {2:d2}/{3:d2}/{4}",
        //                    quarter, versionYear, month, day, year);
        //        }

        //        public static IHtmlString ProductMetaTag(this HtmlHelper html)
        //        {
        //            string controller = (string)html.ViewContext.RouteData.Values["controller"];

        //            if (!controllerToProductIdMap.ContainsKey(controller))
        //            {
        //                return string.Empty.Raw();
        //            }

        //            return String.Format("<meta name=\"ProductId\" content=\"{0}\" />", controllerToProductIdMap[controller]).Raw();
        //        }


        //        public static IHtmlString CheckBox(this HtmlHelper html, string id, bool isChecked, string labelText)
        //        {
        //            return (html.CheckBox(id, isChecked) + new HtmlElement("label").Attribute("for", id).Html(labelText).ToString()).Raw();
        //        }

        //        public static string GetCurrentTheme(this HtmlHelper html)
        //        {
        //            return html.ViewContext.HttpContext.Request.QueryString["theme"] ?? "vista";
        //        }

        //        public static IHtmlString WaveValidatorLink(this HtmlHelper html)
        //        {
        //            var link = new HtmlElement("a")
        //                .Attributes(new
        //                {
        //                    href = String.Format("http://wave.webaim.org/report?url={0}", System.Web.HttpUtility.UrlEncode(html.ViewContext.HttpContext.Request.Url.AbsoluteUri)),
        //                    id = "wave-validate",
        //                    @class = ""
        //                });

        //            new HtmlElement("span").AddClass("t-icon wave-logo").AppendTo(link);

        //            new LiteralNode("Validate with WAVE").AppendTo(link);

        //            return link.ToString().Raw();
        //        }

        //        public static string SwitchToRazorLink(this HtmlHelper html)
        //        {
        //#if MVC3
        //            var link = html.ActionLink("Switch to Razor view engine",
        //                (string)html.ViewContext.RouteData.Values["action"],
        //                new { area = "razor", controller = (string)html.ViewContext.RouteData.Values["controller"] },
        //                new { @class = "" });

        //            return link.ToString();
        //#else
        //            return "";
        //#endif
        //        }
        //    }
        //        #endregion

        public class ExampleConfigurator : IDisposable
        {
            public const string CssClass = "configurator";

            private HtmlTextWriter writer;
            private HtmlHelper htmlHelper;
            private string title;
            private MvcForm form;

            public ExampleConfigurator(HtmlHelper htmlHelper)
            {
                this.htmlHelper = htmlHelper;
                this.writer = new HtmlTextWriter(htmlHelper.ViewContext.Writer);
            }

            public ExampleConfigurator Title(string title)
            {
                this.title = title;

                return this;
            }

            public ExampleConfigurator Begin()
            {
                if (this.form == null)
                {
                    this.writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);
                }
                this.writer.RenderBeginTag(HtmlTextWriterTag.Div);

                this.writer.AddAttribute(HtmlTextWriterAttribute.Class, "legend");
                this.writer.RenderBeginTag(HtmlTextWriterTag.H3);
                this.writer.Write(this.title);
                this.writer.RenderEndTag();

                return this;
            }

            public ExampleConfigurator End()
            {
                this.writer.RenderEndTag();

                if (this.form != null)
                {
                    this.form.EndForm();
                }

                return this;
            }

            public void Dispose()
            {
                this.End();
            }

            public ExampleConfigurator PostTo(string action, string controller)
            {
                string theme = this.htmlHelper.ViewContext.HttpContext.Request.Params["theme"] ?? "vista";

                this.form = this.htmlHelper.BeginForm(action, controller, new { theme = theme }, FormMethod.Post, new { @class = CssClass });

                return this;
            }
        #endregion
        }

        //AuNH
        public static MvcHtmlString ViettelDialog(
            this HtmlHelper helper,
            string buttonTitle,
            string dialogName,
            string dialogTitle,
            string actionName,
            string controllerName,
            string onOpen = "smas.onOpenDialog",
            string onClose = "smas.onCloseDialog",
            int width = 0,
            int height = 0)
        {

            string res = "<input type='button' value='" + buttonTitle + "' onclick='smas.openDialog(\"" + dialogName + "\")'>";
            var html = helper.Telerik().Window()
                           .Name(dialogName)
                           .Title(dialogTitle)
                           .LoadContentFrom(actionName, controllerName)
                           .Buttons(buttons => buttons.Maximize().Close())
                           .Resizable()
                           .Draggable(true)
                           .ClientEvents(events => events.OnOpen(onOpen).OnClose(onClose))
                           .Visible(false)
                           .Modal(true);
            if (width > 0)
                html = html.Width(width);
            if (height > 0)
                html = html.Height(height);

            res += html.ToHtmlString();
            return new MvcHtmlString(res);
        }
    }
}
