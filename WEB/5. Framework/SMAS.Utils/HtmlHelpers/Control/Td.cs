﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Td : BaseControl
    {
        public Td()
            : base("td")
        {
        }

        public static Td Parse<T>(T obj, IDictionary<string, string> Attributes = null)
        {
            Td td = new Td();
            td.InnerHTML = new StringBuilder(ParseValueToString<T>(obj));
            td.Attributes = Attributes; 
            return td;
        }

        public static Td Parse<T>(T obj, ICollection<string> Attributes)
        {
            Td td = new Td();
            td.InnerHTML = new StringBuilder(ParseValueToString<T>(obj));
            Dictionary<string, string> dic = new Dictionary<string, string>();
            for (int i = 0; i < Attributes.Count; i += 2)
            {
                dic[Attributes.ElementAt(i)] = Attributes.ElementAt(i + 1);
            }
                td.Attributes = dic;
            return td;
        }
    }
}
