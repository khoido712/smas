﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Table : BaseControl
    {
        public Thead Thead { get; set; }
        public Tbody Tbody { get; set; }

        public Table()
            : base("table")
        {
            Thead = new Thead();
            Tbody = new Tbody();
        }

        public override StringBuilder GetInnerHTML()
        {
            StringBuilder result = new StringBuilder();
            return result.Append(Thead.ToHTML()).AppendLine().Append(Tbody.ToHTML()).AppendLine();
        }
    }
}
