﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Thead : BaseControl
    {
        public List<Tr> ListTR { get; set; }

        public Thead()
            : base("thead")
        {
            ListTR = new List<Tr>();
        }

        public override StringBuilder GetInnerHTML()
        {
            StringBuilder result = new StringBuilder();
            foreach (Tr tr in ListTR)
            {
                result.Append(tr.ToHTML()).AppendLine();
            }
            return result;
        }

        public Thead AddTR(Tr tr)
        {
            if (this.ListTR == null)
            {
                this.ListTR = new List<Tr>();
            }
            this.ListTR.Add(tr);
            return this;
        }
    }
}
