﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Tbody : BaseControl
    {
        public List<Tr> ListTR { get; set; }

        public Tbody()
            : base("tbody")
        {
            ListTR = new List<Tr>();
        }

        public override StringBuilder GetInnerHTML()
        {
            StringBuilder result = new StringBuilder();
            foreach (Tr tr in ListTR)
            {
                result.Append(tr.ToHTML()).AppendLine();
            }
            return result;
        }

        public Tbody AddTR(Tr tr)
        {
            if (this.ListTR == null)
            {
                this.ListTR = new List<Tr>();
            }
            this.ListTR.Add(tr);
            return this;
        }
    }
}
