﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Th : BaseControl
    {
        public Th()
            : base("th")
        {
        }

        public static Th Parse<T>(T obj, IDictionary<string, string> Attributes = null)
        {
            Th td = new Th();
            td.InnerHTML = new StringBuilder(ParseValueToString<T>(obj));
            td.Attributes = Attributes;
            return td;
        }

        public static Th Parse<T>(T obj, ICollection<string> Attributes)
        {
            Th th = new Th();
            th.InnerHTML = new StringBuilder(ParseValueToString<T>(obj));
            Dictionary<string, string> dic = new Dictionary<string, string>();
            for (int i = 0; i < Attributes.Count; i += 2)
            {
                dic[Attributes.ElementAt(i)] = Attributes.ElementAt(i + 1);
            }
            th.Attributes = dic;
            return th;
        }
    }
}
