﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class Tr : BaseControl
    {
        public List<Td> ListTD { get; set; }
        public List<Th> ListTH { get; set; }

        public Tr()
            : base("tr")
        {
            ListTD = new List<Td>();
            ListTH = new List<Th>();
        }

        public static Tr Parse(IDictionary<string, string> Attributes = null)
        {
            Tr tr = new Tr();
            tr.Attributes = Attributes;
            return tr;
        }

        public override StringBuilder GetInnerHTML()
        {
            StringBuilder result = new StringBuilder();
            foreach (Td td in ListTD)
            {
                result.Append(td.ToHTML()).AppendLine();
            }
            foreach (Th th in ListTH)
            {
                result.Append(th.ToHTML()).AppendLine();
            }
            return result;
        }

        public static Tr ParseTD<T>(ICollection<T> collection, IDictionary<string, string> Attributes = null) 
        {
            Tr tr = new Tr();
            foreach (T ele in collection)
            {
                tr.AddTD(ele, Attributes);
            }
            return tr;
        }

        public static Tr ParseTD<T>(ICollection<T> collection, ICollection<string> Attributes)
        {
            Tr tr = new Tr();
            foreach (T ele in collection)
            {
                tr.AddTD(ele, Attributes);
            }
            return tr;
        }

        public Tr AddTD(Td td)
        {
            if (this.ListTD == null)
            {
                this.ListTD = new List<Td>();
            }
            this.ListTD.Add(td);
            return this;
        }

        public Tr AddTD<T>(T obj, IDictionary<string, string> Attributes = null)
        {
            if (this.ListTD == null)
            {
                this.ListTD = new List<Td>();
            }
            this.ListTD.Add(Td.Parse(obj, Attributes));
            return this;
        }

        public Tr AddTD<T>(T obj, ICollection<string> Attributes)
        {
            if (this.ListTD == null)
            {
                this.ListTD = new List<Td>();
            }
            this.ListTD.Add(Td.Parse(obj, Attributes));
            return this;
        }

        public Tr AddRangeTD<T>(ICollection<T> collection, IDictionary<string, string> Attributes = null)
        {
            foreach (T ele in collection)
            {
                this.AddTD(ele, Attributes);
            }
            return this;
        }

        public Tr AddRangeTD<T>(ICollection<T> collection, ICollection<string> Attributes)
        {
            foreach (T ele in collection)
            {
                this.AddTD(ele, Attributes);
            }
            return this;
        }

        public static Tr ParseTH<T>(ICollection<T> collection, IDictionary<string, string> Attributes = null)
        {
            Tr tr = new Tr();
            foreach (T ele in collection)
            {
                tr.AddTH(ele, Attributes);
            }
            return tr;
        }

        public static Tr ParseTH<T>(ICollection<T> collection, ICollection<string> Attributes)
        {
            Tr tr = new Tr();
            foreach (T ele in collection)
            {
                tr.AddTH(ele, Attributes);
            }
            return tr;
        }

        public Tr AddTH(Th th)
        {
            if (this.ListTH == null)
            {
                this.ListTH = new List<Th>();
            }
            this.ListTH.Add(th);
            return this;
        }

        public Tr AddTH<T>(T obj, IDictionary<string, string> Attributes = null)
        {
            if (this.ListTH == null)
            {
                this.ListTH = new List<Th>();
            }
            this.ListTH.Add(Th.Parse(obj, Attributes));
            return this;
        }

        public Tr AddTH<T>(T obj, ICollection<string> Attributes)
        {
            if (this.ListTH == null)
            {
                this.ListTH = new List<Th>();
            }
            this.ListTH.Add(Th.Parse(obj, Attributes));
            return this;
        }

        public Tr AddRangeTH<T>(ICollection<T> collection, IDictionary<string, string> Attributes = null)
        {
            foreach (T ele in collection)
            {
                this.AddTH(ele, Attributes);
            }
            return this;
        }

        public Tr AddRangeTH<T>(ICollection<T> collection, ICollection<string> Attributes)
        {
            foreach (T ele in collection)
            {
                this.AddTH(ele, Attributes);
            }
            return this;
        }
    }
}
