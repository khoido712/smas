﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers.Control
{
    public class BaseControl
    {
        public string TagName { get; set; }
        public StringBuilder InnerHTML { get; set; }
        public IDictionary<string, string> Attributes { get; set; }

        public BaseControl(string tagName)
        {
            TagName = tagName;
            Attributes = new Dictionary<string, string>();
        }

        public virtual StringBuilder GetInnerHTML()
        {
            return InnerHTML;
        }

        public virtual StringBuilder ToHTML()
        {
            StringBuilder res = new StringBuilder();
            List<string> listAttr = new List<string>();
            if (Attributes != null)
            {
                foreach (string key in Attributes.Keys)
                {
                    listAttr.Add(Attributes[key] == null ?
                        key : string.Format("{0}=\"{1}\"", key, Attributes[key]));
                }
            }
            return res.AppendFormat("<{0} {1}>\n{2}\n</{0}>", TagName, string.Join(" ", listAttr), GetInnerHTML());
        }

        public override string ToString()
        {
            return this.ToHTML().ToString();
        }

        public void SetAttributes(ICollection<string> Attributes)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            for (int i = 0; i < Attributes.Count; i += 2)
            {
                dic[Attributes.ElementAt(i)] = Attributes.ElementAt(i + 1);
            }
            this.Attributes = dic;
        }

        public static string ParseValueToString<T>(T obj)
        {
            if (object.Equals(null, default(T)))
            {
                return "";
            }
            return obj.ToString();
        }
    }
}
