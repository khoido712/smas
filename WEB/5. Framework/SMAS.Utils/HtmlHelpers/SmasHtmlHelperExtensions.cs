﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Telerik.Web.Mvc.UI;
using Telerik.Web.Mvc.UI.Fluent;

namespace SMAS.VTUtils.HtmlHelpers
{
    public static class SmasHtmlHelperExtensions
    {
        //AuNH
        public static MvcHtmlString ViettelDialog(
            this HtmlHelper helper,
            string buttonTitle,
            string dialogName,
            string dialogTitle,
            string actionName,
            string controllerName,
            string onOpen = "smas.onOpenDialog",
            string onClose = "smas.onCloseDialog",
            int width = 0,
            int height = 0)
        {

            string res = "<input type='button' value='" + buttonTitle + "' onclick='smas.openDialog(\"" + dialogName + "\")'>";
            var html = helper.Telerik().Window()
                           .Name(dialogName)
                           .Title(dialogTitle)
                           .LoadContentFrom(actionName, controllerName)
                           .Buttons(buttons => buttons.Maximize().Close())
                           .Resizable()
                           .Draggable(true)
                           .ClientEvents(events => events.OnOpen(onOpen).OnClose(onClose))
                           .Visible(false)
                           .Modal(true);
            if (width > 0)
                html = html.Width(width);
            if (height > 0)
                html = html.Height(height);

            res += html.ToHtmlString();
            return new MvcHtmlString(res);
        }
    }
}
