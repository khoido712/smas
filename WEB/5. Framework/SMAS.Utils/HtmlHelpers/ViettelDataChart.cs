﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.VTUtils.HtmlHelpers
{
    public class ViettelDataChart
    {
        public string Label { get; set; }
        public int Value { get; set; }
        public string color { get; set; }
        public bool disabled { get; set; }

        public ViettelDataChart()
        {
        }

        public ViettelDataChart(string Label, int Value, string color)
        {
            this.Label = Label;
            this.Value = Value;
            this.color = color;
            
        }
    }
}
