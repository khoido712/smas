﻿using System;

namespace SMAS.VTUtils.LockAction
{
    public class LockItem
    {
        //public Type ObjectType { get; set; }
        public string KeyName { get; set; }
        public string SessionId { get; set; }
        public DateTime LockedAt { get; set; }
    }
}