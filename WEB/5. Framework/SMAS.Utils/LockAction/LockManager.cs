﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SMAS.VTUtils.LockAction
{
    public static class LockManager
    {
        private static readonly List<LockItem> LockedItems;

        static LockManager()
        {
            LockedItems = new List<LockItem>();
        }

        /// <summary>
        /// tao key cho doi tuong lock. 
        /// </summary>
        /// <param name="keyNames"></param>
        /// <returns></returns>
        public static string CreatedKey(List<string> keyNames)
        {
            if (keyNames == null || keyNames.Count == 0)
            {
                return "";
            }
            return keyNames.Aggregate("", (current, t) => current + t);
        }
        public static bool IsLocked(string keyName)
        {
            var locksQuery = from l in LockedItems
                             where l.KeyName == keyName
                             select l;

            return (locksQuery.Any());
        }

        public static bool Lock(string keyName, string sessionId)
        {
            if (IsLocked(keyName))
                return false;

            lock (LockedItems)
            {
                var lockItem = new LockItem
                {
                    KeyName = keyName,
                    LockedAt = DateTime.Now,
                    SessionId = sessionId
                };

                LockedItems.Add(lockItem);
                return true;
            }
        }

        public static void ReleaseLock(String key, string sessionId)
        {
            lock (LockedItems)
            {
                var locksQuery = from l in LockedItems
                                 where l.KeyName == key
                                       && sessionId == l.SessionId
                                 select l;

                ReleaseLocks(LockedItems);
            }
        }

        public static void ReleaseSessionLocks(string sessionId)
        {
            lock (LockedItems)
            {
                var locksQuery = from l in LockedItems
                                 where sessionId == l.SessionId
                                 select l;

                ReleaseLocks(locksQuery);
            }
        }

        public static void ReleaseExpiredLocks(int timeRelease)
        {
            lock (LockedItems)
            {
                var locksQuery = from l in LockedItems
                                 where (DateTime.Now - l.LockedAt).TotalMinutes > timeRelease
                                 select l;

                ReleaseLocks(locksQuery);
            }
        }

        public static void ReleaseAllLocks()
        {
            lock (LockedItems)
            {
                LockedItems.Clear();
            }
        }

        private static void ReleaseLocks(IEnumerable<LockItem> lockItems)
        {
            var enumerable = lockItems as IList<LockItem> ?? lockItems.ToList();
            if (!enumerable.Any())
                return;

            foreach (var lockItem in enumerable.ToList())
                LockedItems.Remove(lockItem);
        }

        public static String JoinPupilId(List<int> lstPupilId)
        {
            if (lstPupilId == null)
            {
                return "";
            }
            String pupilIds = String.Join(",", lstPupilId.ToArray());
            return pupilIds;
        }
    }
}