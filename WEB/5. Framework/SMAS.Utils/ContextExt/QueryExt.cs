﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;

namespace SMAS.VTUtils.Excel.ContextExt
{
    /// <summary>
    /// Mở rộng truy vấn cho DbContext
    /// </summary>
    public static class QueryExt
    {
        /// <summary>
        /// Thực hiện truy vấn khi đã có cậu lệnh và tham số
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns>Trả về kiểu danh sách của 1 từ điển, mỗi từ điển là 1 raw trong csdl</returns>
        public static IEnumerable<IDictionary<string, object>> CollectionFromSql(this DbContext dbContext, string sql, Dictionary<string, object> parameters)
        {
            using (var cmd = dbContext.Database.Connection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (cmd.Connection.State != ConnectionState.Open) { 
                    cmd.Connection.Open();
                }

                if (parameters != null && parameters.Any())
                {
                    foreach (KeyValuePair<string, object> param in parameters)
                    {
                        DbParameter dbParameter = cmd.CreateParameter();
                        dbParameter.ParameterName = param.Key;
                        dbParameter.Value = param.Value;
                        cmd.Parameters.Add(dbParameter);
                    }
                }

                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var dataRow = GetDataRow(dataReader);
                        yield return dataRow;
                    }
                }
            }
        }

        /// <summary>
        /// Thực thi truy vấn sql để trả về danh sách kiểu T truyền vào
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static List<T> GenericFromSql<T>(this DbContext dbContext, string sql, Dictionary<string, object> parameters) where T : new()
        {
            using (var cmd = dbContext.Database.Connection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }

                if (parameters != null && parameters.Any())
                {
                    foreach (KeyValuePair<string, object> param in parameters)
                    {
                        DbParameter dbParameter = cmd.CreateParameter();
                        dbParameter.ParameterName = param.Key;
                        dbParameter.Value = param.Value;
                        cmd.Parameters.Add(dbParameter);
                    }
                }

                using (var dataReader = cmd.ExecuteReader())
                {
                    return dataReader.MapToList<T>();
                }
            }
        } 

        public static int CountFromSql(this DbContext dbContext, string sql, Dictionary<string, object> parameters)
        {
            using (var cmd = dbContext.Database.Connection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }

                if (parameters != null && parameters.Any())
                {
                    foreach (KeyValuePair<string, object> param in parameters)
                    {
                        DbParameter dbParameter = cmd.CreateParameter();
                        dbParameter.ParameterName = param.Key;
                        dbParameter.Value = param.Value;
                        cmd.Parameters.Add(dbParameter);
                    }
                }
                object total = cmd.ExecuteScalar();
                return total == null ? 0 : Convert.ToInt32(total);
            }
        } 
        
        /// <summary>
        /// Convert dữ liệu từ dữ liệu thô sang từ điển (C#)
        /// </summary>
        /// <param name="dataReader"></param>
        /// <returns>Kiểu từ điển với tên trường và gia trị</returns>
        private static IDictionary<string, object> GetDataRow(DbDataReader dataReader)
        {
            var dataRow = new ExpandoObject() as IDictionary<string, object>;
            for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++) { 
                dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);
            }
            return dataRow;
        }

        /// <summary>
        /// Chuyển dữ liệu từ dạng
        /// A  B
        /// A1 B1
        /// A2 B2
        /// sang dạng
        /// A1 A2
        /// B1 B2
        /// </summary>
        /// <param name="data"></param>
        /// <param name="keyName"></param>
        /// <param name="valueName"></param>
        /// <returns></returns>
        public static List<IDictionary<string, object>> TransformDictionary(List<IDictionary<string, object>> data, string keyName, string valueName)
        {
            if (data == null || !data.Any())
            {
                return data;
            }
            if (string.IsNullOrWhiteSpace(keyName) || string.IsNullOrWhiteSpace(valueName))
            {
                return null;
            }
            List<IDictionary<string, object>> result = new List<IDictionary<string, object>>();
            Dictionary<string, object> temp = new Dictionary<string, object>();
            object key;
            object value;
            foreach (IDictionary<string, object> dic in data)
            {
                if (dic.TryGetValue(keyName, out key) && dic.TryGetValue(valueName, out value))
                {
                    if (key != null && !string.IsNullOrWhiteSpace(key.ToString()) && !temp.ContainsKey(key.ToString())) {
                        temp.Add(key.ToString(), value);
                        result.Add(temp);
                        temp = new Dictionary<string, object>();
                    }
                }
            }
            return result;
        }
    }
}