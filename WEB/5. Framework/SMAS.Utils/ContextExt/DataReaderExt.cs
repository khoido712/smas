﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SMAS.VTUtils.Excel.ContextExt
{
    public static class DataReaderExt
    {
        public static List<T> MapToList<T>(this DbDataReader dr) where T : new()
        {
            if (dr != null)
            {
                var entity = typeof(T);
                var entities = new List<T>();
                var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                var propDict = props.ToDictionary(p => p.Name.ToUpper(), p => p);

                while (dr.Read())
                {
                    T newObject = new T();
                    for (int index = 0; index < dr.FieldCount; index++)
                    {
                        string fieldName = dr.GetName(index).ToUpper().Replace("_", "");
                        if (propDict.ContainsKey(fieldName))
                        {
                            var info = propDict[fieldName];
                            if ((info != null) && info.CanWrite)
                            {
                                var val = (dr.GetValue(index) == DBNull.Value) ? null : dr.GetValue(index);
                                if (val == null)
                                {
                                    continue;
                                }
                                if (val is bool)
                                {
                                    info.SetValue(newObject, Convert.ToBoolean(val), null);
                                }
                                else if (val is int || val is long)
                                {
                                    info.SetValue(newObject, Convert.ToInt32(val), null);
                                }
                                else if (val is short)
                                {
                                    info.SetValue(newObject, Convert.ToBoolean(val), null);
                                }
                                else
                                {
                                    info.SetValue(newObject, val, null);
                                }
                            }
                        }
                    }
                    entities.Add(newObject);
                }
                return entities;
            }
            return null;
        }
    }
}
