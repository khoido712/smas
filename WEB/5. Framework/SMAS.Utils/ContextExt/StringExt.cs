﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SMAS.VTUtils.Excel.ContextExt
{
    public static class StringExt
    {
        private const int NAME_DISPLAY_FULLNAME = 1;
        private const int NAME_DISPLAY_ACRONYM = 2;
        private const int NAME_DISPLAY_NAME = 3;
        /// <summary>
        /// da dang ky sms
        /// </summary>
        private const short STATUS_REGISTED = 1;
        /// <summary>
        /// chua dang ky
        /// </summary>
        private const short STATUS_NOT_REGISTED = 2;
        /// <summary>
        /// Tam ngung su dung
        /// </summary>
        private const short STATUS_STOP_USING = 3;
        /// <summary>
        /// Da huy
        /// </summary>
        private const short STATUS_DELETE_CONTRACT_DETAIL = 4;

        /// <summary>
        /// Nợ cước
        /// </summary>
        private const short STATUS_UNPAID = 5;
        /// <summary>
        /// Nợ cước quá hạn
        /// </summary>
        private const short STATUS_OVERDUE_UNPAID = 6;

        public static string FormatName(this string value, int? type = null)
        {
            string[] arr = value.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            string name = arr[arr.Length - 1];


            if (type == NAME_DISPLAY_NAME)
            {
                return name;
            }
            else if (type == NAME_DISPLAY_ACRONYM)
            {
                string acronym = string.Empty;
                if (arr.Length > 2)
                {
                    for (int i = 0; i < arr.Length - 1; i++)
                    {
                        acronym += arr[i][0].ToString().ToUpper();
                    }
                }

                return acronym + name;
            }
            else
            {
                return value;
            }
        }
    }
}
