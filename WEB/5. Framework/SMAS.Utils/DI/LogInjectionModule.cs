﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac.Core;
using log4net;

namespace SMAS.VTUtils.DI
{    
        public class LogInjectionModule : Autofac.Module
        {
            protected override void AttachToComponentRegistration(IComponentRegistry registry, IComponentRegistration registration)
            {
                registration.Preparing += OnComponentPreparing;
            }

            static void OnComponentPreparing(object sender, PreparingEventArgs e)
            {
                var t = e.Component.Activator.LimitType;
                e.Parameters = e.Parameters.Union(new[]
        {
            new ResolvedParameter((p, i) => p.ParameterType == typeof(ILog), (p, i) => LogManager.GetLogger(t))
        });
            }
        }
    
}
