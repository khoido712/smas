﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Globalization;

namespace SMAS.Models.CustomAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DateTimeAttribute : ValidationAttribute, IClientValidatable
    {
        public const string DefaultErrorMessage = "'{0}' is not a valid date";

        public DateTimeAttribute()
            : base(DefaultErrorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            return true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string message = String.Format(CultureInfo.CurrentUICulture, ErrorMessageString, metadata.DisplayName);

            var rule = new ModelClientValidationRule
            {
                ErrorMessage = message,
                ValidationType = "datetime"
            };
            
            yield return rule;
        }
    }
}
