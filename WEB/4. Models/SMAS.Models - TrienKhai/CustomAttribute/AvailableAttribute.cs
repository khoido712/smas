using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.ComponentModel;
using System.Data.Entity;
using System.Reflection;

//Nhungnt
namespace SMAS.Models.CustomAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class AvailableAttribute : ValidationAttribute
    {
        public const string DefaultErrorMessage = "'{0}' is not available";
        public string PropertyName { get; set; }
        public Type ContextType { get; set; }
        public Type ModelType { get; set; }

        public AvailableAttribute(Type dbContext, Type model, string propertyName)
            : base(DefaultErrorMessage)
        {
            ContextType = dbContext;
            ModelType = model;
            PropertyName = propertyName;
        }

        public override string FormatErrorMessage(string name)
        {
            return name;
        }

        public override bool IsValid(object value)
        {
            //
            if (value == null || (value is string && string.IsNullOrWhiteSpace((string)value)) || (value is int && (int)value == 0)) return true;
            //
            bool isvalid = true;

            object dbContext = Activator.CreateInstance(ContextType);
            DbSet sett = ((DbContext)dbContext).Set(ModelType);
            object obj = sett.Find(value);

            if (obj == null)
                isvalid = false;
            else
            {
                Type t = obj.GetType();
                PropertyInfo p = t.GetProperty("IsActive");
                if (p != null)
                {
                    bool valActive = (bool)p.GetValue(obj, null);
                    isvalid = valActive;
                }
                else
                    isvalid = true;
            }

            return isvalid;
        }        
    }
}
