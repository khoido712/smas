﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel;

namespace SMAS.Models.CustomAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DataConstraintAttribute : ValidationAttribute, IClientValidatable
    {
        private const string DefaultErrorMessage = "You must be enter {0} {1} {2}";
        public const string EQUALS = "=";
        public const string NOT_EQUALS = "#";
        public const string GREATER = ">";
        public const string GREATER_EQUALS = ">=";
        public const string LESS = "<";
        public const string LESS_EQUALS = "<=";

        public string RelateProperty { get; set; }
        public string Operation { get; set; }
        public DateTime? RelateValue { get; set; }
        public string Message { get; set; }
        private bool isvalid { get; set; }

        public DataConstraintAttribute(string Operation, string RelateProperty)
            : base(DefaultErrorMessage)
        {
            this.Operation = Operation;
            this.RelateProperty = RelateProperty;
            if (RelateProperty == "DateTime.Now")
            {
                this.RelateProperty = null;
                this.RelateValue = DateTime.Now;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return name;
        }

        public bool Valid(object value, ValidationContext vc)
        {
            ValidationResult vr = this.IsValid(value, vc);
            return isvalid;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            isvalid = true;
            PropertyInfo RelatePropertyInfo = null;
            if (RelateProperty != null && RelateProperty != "")
            {
                RelatePropertyInfo = validationContext.ObjectType.GetProperty(this.RelateProperty);
                if (RelatePropertyInfo == null)
                {
                    return new ValidationResult(string.Format("unknown property {0}", this.RelateProperty));
                }

                var RelatePropertyValue = RelatePropertyInfo.GetValue(validationContext.ObjectInstance, null);

                //if (value == null || !(value is DateTime))
                //{
                //    return ValidationResult.Success;
                //}

                if (RelatePropertyValue == null || !(RelatePropertyValue is DateTime))
                {
                    return ValidationResult.Success;
                }

                this.RelateValue = (DateTime)RelatePropertyValue;
            }

            if (value == null || !(value is DateTime))
            {
                return ValidationResult.Success;
            }

            // Compare values
            if (Operation == EQUALS && (DateTime)value == (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            if (Operation == NOT_EQUALS && (DateTime)value != (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            if (Operation == GREATER && (DateTime)value > (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            if (Operation == GREATER_EQUALS && (DateTime)value >= (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            if (Operation == LESS && (DateTime)value < (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            if (Operation == LESS_EQUALS && (DateTime)value <= (DateTime)RelateValue)
            {
                return ValidationResult.Success;
            }
            Message = "Validate_DataConstraint";
            isvalid = false;
            return new ValidationResult(Message);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            PropertyInfo RelatePropertyInfo = null;
            if (RelateProperty != null)
            {
                RelatePropertyInfo = metadata.ContainerType.GetProperty(this.RelateProperty);
            }
            string msg = String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                metadata.DisplayName, Operation,
                (RelatePropertyInfo != null ? GetDisplayName(RelatePropertyInfo) : RelateValue.Value.ToShortDateString()));

            var rule = new ModelClientValidationRule
            {
                ErrorMessage = msg,
                ValidationType = "dateconstraint"
            };
            rule.ValidationParameters["relateproperty"] = this.RelateProperty;
            rule.ValidationParameters["operation"] = this.Operation;
            rule.ValidationParameters["relatevalue"] = string.Format("{0:dd/MM/yyyy}", this.RelateValue);
            yield return rule;
        }

        private string GetDisplayName(PropertyInfo property)
        {
            //get attributes of property
            Object[] lsAttribute = property.GetCustomAttributes(true);
            //validate 
            string DisplayName = "";
            foreach (object att in lsAttribute)
            {
                if (att is DisplayNameAttribute)
                {
                    DisplayName = ((DisplayNameAttribute)att).DisplayName;
                    break;
                }

                if (att is DisplayAttribute)
                {

                    DisplayName = ((DisplayAttribute)att).GetName();
                    break;
                }

            }
            return DisplayName;
        }
    }
}
