using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Data.Entity;
using System.Reflection;
using System.Data.Objects;
using System.Web.Mvc;
//nhungnt
namespace SMAS.Models.CustomAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DuplicateAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "'{0}' is duplicate";
        private string PropertyName { get; set; }
        private Type ContextType { get; set; }
        private Type ModelType { get; set; }
        private string Schema { get; set; }
        private bool isvalid { get; set; }

        public DuplicateAttribute(Type dbContext,string schema, Type model, string propertyName)
            : base(DefaultErrorMessage)
        {
            ContextType = dbContext;
            ModelType = model;
            PropertyName = propertyName;
            Schema = schema;
            isvalid = true;
            
        }

        public override string FormatErrorMessage(string name)
        {
            return name;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (value == null || (value is string && string.IsNullOrWhiteSpace((string)value)))
            {
                return ValidationResult.Success;
            }          

            object objInstance = context.ObjectInstance;            
            int? id = GetId(objInstance);
            bool? isActive = getIsActive(objInstance);
            object dbContext = Activator.CreateInstance(ContextType);

            MethodInfo md = ContextType.GetMethod("CheckDuplicate", new Type[] { typeof(string), typeof(string), typeof(string), typeof(string), typeof(bool?), typeof(int?) });
            ObjectResult<int?> resultQuery = (ObjectResult<int?>)md.Invoke(dbContext, new object[] { value, Schema, ModelType.Name, PropertyName, isActive, id });
            int? result = ((int?)resultQuery.First());
            if (result == null || result > 0)
            {
                isvalid = false;
                return new ValidationResult(ErrorMessage);
            }
           
            return ValidationResult.Success;
        }

        public bool Valid(object value, ValidationContext vc)
        {
            ValidationResult vr = this.IsValid(value, vc);
            return isvalid;
        }

        private PropertyInfo GetProperty()
        {
            PropertyInfo property = null;
            foreach (PropertyInfo p in ModelType.GetProperties())
            {
                if (p.Name.Equals(PropertyName))
                {
                    property = p;
                    break;
                }
            }
            return property;
        }

        private int? GetId(object instance)
        {
            int? id = null;            
            string idName = ModelType.Name + "ID";
            foreach (PropertyInfo p in instance.GetType().GetProperties())
            {
                if (p.Name.Equals(idName))
                {
                    id = (int?) p.GetValue(instance,null);
                    break;
                }
            }
            return id;
        }
        private bool? getIsActive(object instance)
        {
            bool? isactive = false;
            foreach (PropertyInfo p in ModelType.GetProperties())
            {
                if (p.Name.Equals("IsActive"))
                {
                    isactive = true;
                    break;
                }
            }
            return isactive;

        }
    }
}
