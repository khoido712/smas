//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMS_PARENT_CONTRACT_CLASS_HIS
    {
        public int SMS_PARENT_CONTRACT_CLASS_ID { get; set; }
        public int CLASS_ID { get; set; }
        public int ACADEMIC_YEAR_ID { get; set; }
        public int SCHOOL_ID { get; set; }
        public int YEAR { get; set; }
        public int SEMESTER { get; set; }
        public int TOTAL_SMS { get; set; }
        public int SENT_TOTAL { get; set; }
        public System.DateTime CREATED_DATE { get; set; }
        public Nullable<System.DateTime> UPDATED_DATE { get; set; }
        public string UPDATED_NOTE { get; set; }
    }
}
