//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMS_CONTENT_TYPE
    {
        public int CONTENT_TYPE_ID { get; set; }
        public string CONTENT_TYPE_CODE { get; set; }
        public string CONTENT_TYPE_NAME { get; set; }
        public bool FORG_RADE1 { get; set; }
        public bool FORG_RADE2 { get; set; }
        public bool FORG_RADE3 { get; set; }
        public bool FOR_VNEN { get; set; }
        public bool FOR_NOTVNEN { get; set; }
        public bool PERIOD_DAILY { get; set; }
        public bool PERIOD_WEEKLY { get; set; }
        public bool PERIOD_MONTHLY { get; set; }
        public bool PERIOD_SEMESTER { get; set; }
        public bool PERIOD_CUSTOM { get; set; }
        public string TEMPLATE_NAME { get; set; }
        public int ORDER_NUMBER { get; set; }
    }
}
