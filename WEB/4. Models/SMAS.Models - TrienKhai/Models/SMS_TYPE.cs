//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMS_TYPE
    {
        public int SMS_TYPE_ID { get; set; }
        public string SMS_TYPE_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string DEFAULT_TEMPLATE { get; set; }
        public string TYPE_CODE { get; set; }
        public int TYPE_ID { get; set; }
        public string PERIODIC { get; set; }
        public Nullable<System.DateTime> DEFAULT_SEND_TIME { get; set; }
        public bool DEFAULT_IS_AUTO_SEND { get; set; }
        public bool DEFAULT_IS_LOCK { get; set; }
        public int DEFAULT_SEND_BEFORE { get; set; }
        public bool IS_ALLOW_AUTO_SEND { get; set; }
        public bool IS_AUTO_SCHOOL_QUOTA { get; set; }
        public string APPLIED_LEVEL { get; set; }
        public int ORDER_ID { get; set; }
    }
}
