//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityPlanClass
    {
        public int ActivityPlanClassID { get; set; }
        public int ActivityPlanID { get; set; }
        public int ClassID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public string MSourcedb { get; set; }
    
        public virtual ActivityPlan ActivityPlan { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
    }
}
