﻿using System.Collections.Generic;

namespace SMAS.Models.Models
{
    public class CustomType
    {
        public string ParaName { get; set; }
        public string ParaType { get; set; }
        public string ParaValue { get; set; }
        public List<string> ListValue { get; set; }
        public bool IsListParaValue { get; set; }
    }
}
