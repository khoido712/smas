//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PupilOfClass
    {
        public int PupilOfClassID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<System.DateTime> AssignedDate { get; set; }
        public Nullable<int> OrderInClass { get; set; }
        public string Description { get; set; }
        public Nullable<bool> NoConductEstimation { get; set; }
        public int Status { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<int> Last2digitNumberSchool { get; set; }
        public Nullable<int> CreatedAcademicYear { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
        public Nullable<bool> IsRegisterContract { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual PupilProfile PupilProfile { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
