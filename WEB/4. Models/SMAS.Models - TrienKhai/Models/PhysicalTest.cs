//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PhysicalTest
    {
        public int PhysicalTestID { get; set; }
        public int MonitoringBookID { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<decimal> Height { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<decimal> Breast { get; set; }
        public Nullable<int> PhysicalClassification { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> Nutrition { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<int> LogID { get; set; }
        public string MSourcedb { get; set; }
    
        public virtual MonitoringBook MonitoringBook { get; set; }
    }
}
