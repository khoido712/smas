//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMS_SERVICE_PACKAGE_DECLARE
    {
        public int SERVICE_PACKAGE_DECLARE_ID { get; set; }
        public int SERVICE_PACKAGE_ID { get; set; }
        public Nullable<int> YEAR { get; set; }
        public Nullable<int> SEMESTER { get; set; }
        public Nullable<System.DateTime> FROM_DATE { get; set; }
        public Nullable<System.DateTime> TO_DATE { get; set; }
        public Nullable<int> EFFECTIVE_DAYS { get; set; }
        public string DESCRIPTION { get; set; }
        public bool IS_INTERNAL { get; set; }
        public bool IS_EXTERNAL { get; set; }
        public bool IS_WHOLE_YEAR_PACKAGE { get; set; }
        public bool IS_EXTRA_PACKAGE { get; set; }
        public bool IS_LIMIT { get; set; }
        public Nullable<int> APPLY_TYPE { get; set; }
        public bool STATUS { get; set; }
        public System.DateTime CREATED_TIME { get; set; }
        public Nullable<System.DateTime> UPDATED_TIME { get; set; }
    }
}
