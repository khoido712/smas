//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FoodCat
    {
        public FoodCat()
        {
            this.DailyFoodInspections = new HashSet<DailyFoodInspection>();
            this.DailyMenuFoods = new HashSet<DailyMenuFood>();
            this.DishDetails = new HashSet<DishDetail>();
            this.FoodMinerals = new HashSet<FoodMineral>();
        }
    
        public int FoodID { get; set; }
        public int GroupType { get; set; }
        public Nullable<int> DiscardedRate { get; set; }
        public int Price { get; set; }
        public Nullable<decimal> Protein { get; set; }
        public Nullable<decimal> Fat { get; set; }
        public Nullable<decimal> Sugar { get; set; }
        public Nullable<decimal> Calo { get; set; }
        public string FoodName { get; set; }
        public string ShortName { get; set; }
        public int TypeOfFoodID { get; set; }
        public Nullable<int> FoodPackingID { get; set; }
        public Nullable<int> FoodGroupID { get; set; }
        public int CalculationUnit { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual ICollection<DailyFoodInspection> DailyFoodInspections { get; set; }
        public virtual ICollection<DailyMenuFood> DailyMenuFoods { get; set; }
        public virtual ICollection<DishDetail> DishDetails { get; set; }
        public virtual FoodGroup FoodGroup { get; set; }
        public virtual FoodPacking FoodPacking { get; set; }
        public virtual TypeOfFood TypeOfFood { get; set; }
        public virtual ICollection<FoodMineral> FoodMinerals { get; set; }
    }
}
