//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassEmulation
    {
        public ClassEmulation()
        {
            this.ClassEmulationDetails = new HashSet<ClassEmulationDetail>();
        }
    
        public int ClassEmulationID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public int ClassID { get; set; }
        public Nullable<int> AcademicYearID { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> Month { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Nullable<decimal> TotalEmulationMark { get; set; }
        public Nullable<decimal> TotalPenalizedMark { get; set; }
        public Nullable<decimal> TotalMark { get; set; }
        public Nullable<int> Rank { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual ICollection<ClassEmulationDetail> ClassEmulationDetails { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
