//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ofpubsubdefaultconf
    {
        public string Serviceid { get; set; }
        public decimal Leaf { get; set; }
        public decimal Deliverpayloads { get; set; }
        public decimal Maxpayloadsize { get; set; }
        public decimal Persistitems { get; set; }
        public decimal Maxitems { get; set; }
        public decimal Notifyconfigchanges { get; set; }
        public decimal Notifydelete { get; set; }
        public decimal Notifyretract { get; set; }
        public decimal Presencebased { get; set; }
        public decimal Senditemsubscribe { get; set; }
        public string Publishermodel { get; set; }
        public decimal Subscriptionenabled { get; set; }
        public string Accessmodel { get; set; }
        public string Language { get; set; }
        public string Replypolicy { get; set; }
        public string Associationpolicy { get; set; }
        public decimal Maxleafnodes { get; set; }
    }
}
