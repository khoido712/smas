//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassificationLevelHealth
    {
        public ClassificationLevelHealth()
        {
            this.ClassificationCriteriaDetails = new HashSet<ClassificationCriteriaDetail>();
        }
    
        public int ClassificationLevelHealthID { get; set; }
        public Nullable<int> Level { get; set; }
        public int SymbolConfigID { get; set; }
        public int TypeConfigID { get; set; }
        public int EvaluationConfigID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual ICollection<ClassificationCriteriaDetail> ClassificationCriteriaDetails { get; set; }
        public virtual EvaluationConfig EvaluationConfig { get; set; }
        public virtual SymbolConfig SymbolConfig { get; set; }
        public virtual TypeConfig TypeConfig { get; set; }
    }
}
