//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TypeConfig
    {
        public TypeConfig()
        {
            this.ClassificationCriterias = new HashSet<ClassificationCriteria>();
            this.ClassificationLevelHealths = new HashSet<ClassificationLevelHealth>();
            this.EvaluationConfigs = new HashSet<EvaluationConfig>();
            this.LevelHealthConfigs = new HashSet<LevelHealthConfig>();
        }
    
        public int TypeConfigID { get; set; }
        public string TypeConfigName { get; set; }
        public int PositionOrder { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual ICollection<ClassificationCriteria> ClassificationCriterias { get; set; }
        public virtual ICollection<ClassificationLevelHealth> ClassificationLevelHealths { get; set; }
        public virtual ICollection<EvaluationConfig> EvaluationConfigs { get; set; }
        public virtual ICollection<LevelHealthConfig> LevelHealthConfigs { get; set; }
    }
}
