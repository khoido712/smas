//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MIdmapping
    {
        public long ID { get; set; }
        public Nullable<int> NewID { get; set; }
        public Nullable<int> OldID { get; set; }
        public string OldTableName { get; set; }
        public string NewTableName { get; set; }
        public string Source { get; set; }
        public Nullable<long> SynchronizeID { get; set; }
        public string OldString { get; set; }
        public Nullable<System.Guid> OldGuid { get; set; }
        public Nullable<bool> IsSMAS3 { get; set; }
    }
}
