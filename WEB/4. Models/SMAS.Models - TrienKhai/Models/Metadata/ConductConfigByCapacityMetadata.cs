

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConductConfigByCapacityMetadata))]
public partial class ConductConfigByCapacity { }

public partial class ConductConfigByCapacityMetadata
    {
  
    [Display(Name = "ConductConfigByCapacity_Label_ConductConfigByCapacityID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductConfigByCapacityID { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_ConductLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductLevelID { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_CapacityLevelID")]	
    public Nullable<int> CapacityLevelID { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConductConfigByCapacity_Label_CapacityLevel")]	
    public virtual CapacityLevel CapacityLevel { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_ConductLevel")]	
    public virtual ConductLevel ConductLevel { get; set; }	
  
    [Display(Name = "ConductConfigByCapacity_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

