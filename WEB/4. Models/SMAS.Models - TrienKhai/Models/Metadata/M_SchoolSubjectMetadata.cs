

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_SchoolSubjectMetadata))]
public partial class M_SchoolSubject { }

public partial class M_SchoolSubjectMetadata
    {
  
    [Display(Name = "M_SchoolSubject_Label_SchoolSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolSubjectID { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_FirstSemesterCoefficient")]	
    public Nullable<int> FirstSemesterCoefficient { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_SecondSemesterCoefficient")]	
    public Nullable<int> SecondSemesterCoefficient { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_AppliedType")]	
    public Nullable<int> AppliedType { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_IsCommenting")]	
    public Nullable<int> IsCommenting { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_SchoolSubject_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

