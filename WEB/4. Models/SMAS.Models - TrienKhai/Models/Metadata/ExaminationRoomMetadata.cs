

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ExaminationRoomMetadata))]
public partial class ExaminationRoom { }

public partial class ExaminationRoomMetadata
    {
  
    [Display(Name = "ExaminationRoom_Label_ExaminationRoomID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationRoomID { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_ExaminationID")]	
    public Nullable<int> ExaminationID { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_ExaminationSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationSubjectID { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_RoomTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RoomTitle { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_NumberOfSeat")]	
    public Nullable<int> NumberOfSeat { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_Description")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ExaminationRoom_Label_Candidates")]	
    public virtual ICollection<Candidate> Candidates { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_CandidateAbsences")]	
    public virtual ICollection<CandidateAbsence> CandidateAbsences { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_InvigilatorAssignments")]	
    public virtual ICollection<InvigilatorAssignment> InvigilatorAssignments { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
  
    [Display(Name = "ExaminationRoom_Label_ViolatedInvigilators")]	
    public virtual ICollection<ViolatedInvigilator> ViolatedInvigilators { get; set; }	
    }
}

