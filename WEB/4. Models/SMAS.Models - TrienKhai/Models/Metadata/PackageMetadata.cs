

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PackageMetadata))]
public partial class Package { }

public partial class PackageMetadata
    {
  
    [Display(Name = "Package_Label_PackageID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PackageID { get; set; }	
  
    [Display(Name = "Package_Label_PackageCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PackageCode { get; set; }	
  
    [Display(Name = "Package_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "Package_Label_ServiceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ServiceID { get; set; }	
  
    [Display(Name = "Package_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Package_Label_MaxSMS")]	
    public Nullable<int> MaxSMS { get; set; }	
  
    [Display(Name = "Package_Label_CreateTime")]	
    public Nullable<System.DateTime> CreateTime { get; set; }	
  
    [Display(Name = "Package_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	
  
    [Display(Name = "Package_Label_CreateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CreateUser { get; set; }	
  
    [Display(Name = "Package_Label_UpdateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string UpdateUser { get; set; }	

  
    [Display(Name = "Package_Label_Service")]	
    public virtual Service Service { get; set; }	
  
    [Display(Name = "Package_Label_SendInfoes")]	
    public virtual ICollection<SendInfo> SendInfoes { get; set; }	
  
    [Display(Name = "Package_Label_SendResults")]	
    public virtual ICollection<SendResult> SendResults { get; set; }	
  
    [Display(Name = "Package_Label_SMSTeacherContracts")]	
    public virtual ICollection<SMSTeacherContract> SMSTeacherContracts { get; set; }	
    }
}

