

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TypeOfDishMetadata))]
public partial class TypeOfDish { }

public partial class TypeOfDishMetadata
    {
  
    [Display(Name = "TypeOfDish_Label_TypeOfDishID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeOfDishID { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_TypeOfDishName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TypeOfDishName { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_Note")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "TypeOfDish_Label_DishCats")]	
    public virtual ICollection<DishCat> DishCats { get; set; }	
  
    [Display(Name = "TypeOfDish_Label_FoodHabitOfChildrens")]	
    public virtual ICollection<FoodHabitOfChildren> FoodHabitOfChildrens { get; set; }	
    }
}

