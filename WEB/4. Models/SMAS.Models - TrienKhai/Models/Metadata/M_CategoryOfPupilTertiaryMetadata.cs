

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_CategoryOfPupilTertiaryMetadata))]
public partial class M_CategoryOfPupilTertiary { }

public partial class M_CategoryOfPupilTertiaryMetadata
    {
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_CategoryOfPupilTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CategoryOfPupilTertiaryID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_CapacityID")]	
    public Nullable<int> CapacityID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_ConductID")]	
    public Nullable<int> ConductID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_AbsentTotalDays")]	
    public Nullable<int> AbsentTotalDays { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_Category")]	
    public Nullable<byte> Category { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_IsReviewed")]	
    public Nullable<bool> IsReviewed { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_CapacityNotRetest")]	
    public Nullable<byte> CapacityNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_ConductNotRetest")]	
    public Nullable<byte> ConductNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_CategoryNotRetest")]	
    public Nullable<byte> CategoryNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

