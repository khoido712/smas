

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(FoodGroupMetadata))]
public partial class FoodGroup { }

public partial class FoodGroupMetadata
    {
  
    [Display(Name = "FoodGroup_Label_FoodGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodGroupID { get; set; }	
  
    [Display(Name = "FoodGroup_Label_FoodGroupName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FoodGroupName { get; set; }	
  
    [Display(Name = "FoodGroup_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "FoodGroup_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "FoodGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "FoodGroup_Label_FoodCats")]	
    public virtual ICollection<FoodCat> FoodCats { get; set; }	
  
    [Display(Name = "FoodGroup_Label_DailyFoodInspections")]	
    public virtual ICollection<DailyFoodInspection> DailyFoodInspections { get; set; }	
    }
}

