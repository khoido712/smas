

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CallDetailRecordMetadata))]
public partial class CallDetailRecord { }

public partial class CallDetailRecordMetadata
    {
  
    [Display(Name = "CallDetailRecord_Label_CallDetailRecordID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CallDetailRecordID { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_SMSTeacherContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSTeacherContractID { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_CallingNumber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CallingNumber { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_CalledNumber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CalledNumber { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_StaDatetime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime StaDatetime { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_Seq")]	
    public Nullable<int> Seq { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_NumberOfSMS")]	
    public Nullable<int> NumberOfSMS { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_Supplier")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Supplier { get; set; }	
  
    [Display(Name = "CallDetailRecord_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	

  
    [Display(Name = "CallDetailRecord_Label_SMSTeacherContract")]	
    public virtual SMSTeacherContract SMSTeacherContract { get; set; }	
    }
}

