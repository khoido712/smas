

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilGraduationMetadata))]
public partial class PupilGraduation { }

public partial class PupilGraduationMetadata
    {
  
    [Display(Name = "PupilGraduation_Label_PupilGraduationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilGraduationID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_GraduationLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GraduationLevel { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_GraduationGrade")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GraduationGrade { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_IssuedNumber")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string IssuedNumber { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_RecordNumber")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RecordNumber { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_IssuedDate")]	
    public Nullable<System.DateTime> IssuedDate { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_ReceivedDate")]	
    public Nullable<System.DateTime> ReceivedDate { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_IsReceived")]	
    public Nullable<int> IsReceived { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_PriorityReason")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PriorityReason { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_Status")]	
    public Nullable<int> Status { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_Exemption")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Exemption { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_MeritPoint")]	
    public Nullable<decimal> MeritPoint { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_Receiver")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Receiver { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PupilGraduation_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "PupilGraduation_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

