

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeachingRegistrationMetadata))]
public partial class TeachingRegistration { }

public partial class TeachingRegistrationMetadata
    {
  
    [Display(Name = "TeachingRegistration_Label_TeachingRegistrationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeachingRegistrationID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_WeekOfYear")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WeekOfYear { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_DayOfWeek")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DayOfWeek { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_Section")]	
    public Nullable<bool> Section { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_StartPeriod")]	
    public Nullable<int> StartPeriod { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_EndPeriod")]	
    public Nullable<int> EndPeriod { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_LessionName")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string LessionName { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_TrainingTool")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TrainingTool { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_IsSubstitued")]	
    public Nullable<bool> IsSubstitued { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_RegisteredDate")]	
    public Nullable<System.DateTime> RegisteredDate { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeachingRegistration_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "TeachingRegistration_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
    }
}

