

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ContractTypeMetadata))]
public partial class ContractType { }

public partial class ContractTypeMetadata
    {
  
    [Display(Name = "ContractType_Label_ContractTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContractTypeID { get; set; }	
  
    [Display(Name = "ContractType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ContractType_Label_Type")]	
    public Nullable<int> Type { get; set; }	
  
    [Display(Name = "ContractType_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ContractType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ContractType_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "ContractType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ContractType_Label_Contracts")]	
    public virtual ICollection<Contract> Contracts { get; set; }	
  
    [Display(Name = "ContractType_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
    }
}

