

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(aspnet_SchemaVersionsMetadata))]
public partial class aspnet_SchemaVersions { }

public partial class aspnet_SchemaVersionsMetadata
    {
  
    [Display(Name = "aspnet_SchemaVersions_Label_Feature")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Feature { get; set; }	
  
    [Display(Name = "aspnet_SchemaVersions_Label_CompatibleSchemaVersion")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CompatibleSchemaVersion { get; set; }	
  
    [Display(Name = "aspnet_SchemaVersions_Label_IsCurrentVersion")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsCurrentVersion { get; set; }	
    }
}

