

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DailyFoodInspectionMetadata))]
public partial class DailyFoodInspection { }

public partial class DailyFoodInspectionMetadata
    {
  
    [Display(Name = "DailyFoodInspection_Label_DailyFoodInspectionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyFoodInspectionID { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_Weight")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal Weight { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_PricePerOnce")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PricePerOnce { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_DishInspectionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishInspectionID { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_FoodGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodGroupID { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_FoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodID { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DailyFoodInspection_Label_FoodCat")]	
    public virtual FoodCat FoodCat { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_FoodGroup")]	
    public virtual FoodGroup FoodGroup { get; set; }	
  
    [Display(Name = "DailyFoodInspection_Label_DishInspection")]	
    public virtual DishInspection DishInspection { get; set; }	
    }
}

