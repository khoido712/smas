

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DishInspectionMetadata))]
public partial class DishInspection { }

public partial class DishInspectionMetadata
    {
  
    [Display(Name = "DishInspection_Label_DishInspectionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishInspectionID { get; set; }	
  
    [Display(Name = "DishInspection_Label_InspectedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime InspectedDate { get; set; }	
  
    [Display(Name = "DishInspection_Label_MenuType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuType { get; set; }	
  
    [Display(Name = "DishInspection_Label_DailyCostOfChildren")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyCostOfChildren { get; set; }	
  
    [Display(Name = "DishInspection_Label_Expense")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Expense { get; set; }	
  
    [Display(Name = "DishInspection_Label_EatingGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupID { get; set; }	
  
    [Display(Name = "DishInspection_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "DishInspection_Label_DailyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuID { get; set; }	
  
    [Display(Name = "DishInspection_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "DishInspection_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "DishInspection_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "DishInspection_Label_NumberOfChildren")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NumberOfChildren { get; set; }	
  
    [Display(Name = "DishInspection_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DishInspection_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DishInspection_Label_EatingGroup")]	
    public virtual EatingGroup EatingGroup { get; set; }	
  
    [Display(Name = "DishInspection_Label_DailyFoodInspections")]	
    public virtual ICollection<DailyFoodInspection> DailyFoodInspections { get; set; }	
  
    [Display(Name = "DishInspection_Label_DailyMenu")]	
    public virtual DailyMenu DailyMenu { get; set; }	
  
    [Display(Name = "DishInspection_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

