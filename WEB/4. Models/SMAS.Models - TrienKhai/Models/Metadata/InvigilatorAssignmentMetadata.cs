

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(InvigilatorAssignmentMetadata))]
public partial class InvigilatorAssignment { }

public partial class InvigilatorAssignmentMetadata
    {
  
    [Display(Name = "InvigilatorAssignment_Label_InvigilatorAssignmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InvigilatorAssignmentID { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_InvigilatorID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InvigilatorID { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_RoomID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoomID { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_AssignedDate")]	
    public Nullable<System.DateTime> AssignedDate { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "InvigilatorAssignment_Label_ExaminationRoom")]	
    public virtual ExaminationRoom ExaminationRoom { get; set; }	
  
    [Display(Name = "InvigilatorAssignment_Label_Invigilator")]	
    public virtual Invigilator Invigilator { get; set; }	
    }
}

