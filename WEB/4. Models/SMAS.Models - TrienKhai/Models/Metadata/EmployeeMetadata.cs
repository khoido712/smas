

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employee { }

    public partial class EmployeeMetadata
    {

        [Display(Name = "Employee_Label_EmployeeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeID { get; set; }

        [Display(Name = "Employee_Label_EmployeeCode")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string EmployeeCode { get; set; }

        [Display(Name = "Employee_Label_EmployeeType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EmployeeType { get; set; }

        [Display(Name = "Employee_Label_EmploymentStatus")]
        public Nullable<int> EmploymentStatus { get; set; }

        [Display(Name = "Employee_Label_SupervisingDeptID")]
        public Nullable<int> SupervisingDeptID { get; set; }

        [Display(Name = "Employee_Label_SchoolID")]
        public Nullable<int> SchoolID { get; set; }

        [Display(Name = "Employee_Label_Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Name { get; set; }

        [Display(Name = "Employee_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FullName { get; set; }

        [Display(Name = "Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [Display(Name = "Employee_Label_BirthPlace")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string BirthPlace { get; set; }

        [Display(Name = "Employee_Label_Telephone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Telephone { get; set; }

        [Display(Name = "Employee_Label_Mobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Mobile { get; set; }

        [Display(Name = "Employee_Label_Email")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Email { get; set; }

        [Display(Name = "Employee_Label_TempResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string TempResidentalAddress { get; set; }

        [Display(Name = "Employee_Label_PermanentResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PermanentResidentalAddress { get; set; }

        [Display(Name = "Employee_Label_Genre")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool Genre { get; set; }

        [Display(Name = "Employee_Label_Alias")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Alias { get; set; }

        [Display(Name = "Employee_Label_MariageStatus")]
        public Nullable<int> MariageStatus { get; set; }

        [Display(Name = "Employee_Label_ForeignLanguageQualification")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ForeignLanguageQualification { get; set; }

        [Display(Name = "Employee_Label_HealthStatus")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string HealthStatus { get; set; }

        [Display(Name = "Employee_Label_JoinedDate")]
        public Nullable<System.DateTime> JoinedDate { get; set; }

        [Display(Name = "Employee_Label_StartingDate")]
        public Nullable<System.DateTime> StartingDate { get; set; }

        [Display(Name = "Employee_Label_IdentityNumber")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string IdentityNumber { get; set; }

        [Display(Name = "Employee_Label_IdentityIssuedDate")]
        public Nullable<System.DateTime> IdentityIssuedDate { get; set; }

        [Display(Name = "Employee_Label_IdentityIssuedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string IdentityIssuedPlace { get; set; }

        [Display(Name = "Employee_Label_HomeTown")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string HomeTown { get; set; }

        [Display(Name = "Employee_Label_Image")]
        public byte[] Image { get; set; }

        [Display(Name = "Employee_Label_DedicatedForYoungLeague")]
        public Nullable<bool> DedicatedForYoungLeague { get; set; }

        [Display(Name = "Employee_Label_FamilyTypeID")]
        public Nullable<int> FamilyTypeID { get; set; }

        [Display(Name = "Employee_Label_StaffPositionID")]
        public Nullable<int> StaffPositionID { get; set; }

        [Display(Name = "Employee_Label_EthnicID")]
        public Nullable<int> EthnicID { get; set; }

        [Display(Name = "Employee_Label_ReligionID")]
        public Nullable<int> ReligionID { get; set; }

        [Display(Name = "Employee_Label_GraduationLevelID")]
        public Nullable<int> GraduationLevelID { get; set; }

        [Display(Name = "Employee_Label_ContractID")]
        public Nullable<int> ContractID { get; set; }

        [Display(Name = "Employee_Label_QualificationTypeID")]
        public Nullable<int> QualificationTypeID { get; set; }

        [Display(Name = "Employee_Label_SpecialityCatID")]
        public Nullable<int> SpecialityCatID { get; set; }

        [Display(Name = "Employee_Label_QualificationLevelID")]
        public Nullable<int> QualificationLevelID { get; set; }

        [Display(Name = "Employee_Label_ITQualificationLevelID")]
        public Nullable<int> ITQualificationLevelID { get; set; }

        [Display(Name = "Employee_Label_ForeignLanguageGradeID")]
        public Nullable<int> ForeignLanguageGradeID { get; set; }

        [Display(Name = "Employee_Label_PoliticalGradeID")]
        public Nullable<int> PoliticalGradeID { get; set; }

        [Display(Name = "Employee_Label_StateManagementGradeID")]
        public Nullable<int> StateManagementGradeID { get; set; }

        [Display(Name = "Employee_Label_EducationalManagementGradeID")]
        public Nullable<int> EducationalManagementGradeID { get; set; }

        [Display(Name = "Employee_Label_WorkTypeID")]
        public Nullable<int> WorkTypeID { get; set; }

        [Display(Name = "Employee_Label_PrimarilyAssignedSubjectID")]
        public Nullable<int> PrimarilyAssignedSubjectID { get; set; }

        [Display(Name = "Employee_Label_SchoolFacultyID")]
        public Nullable<int> SchoolFacultyID { get; set; }

        [Display(Name = "Employee_Label_IsYouthLeageMember")]
        public Nullable<bool> IsYouthLeageMember { get; set; }

        [Display(Name = "Employee_Label_YouthLeagueJoinedDate")]
        public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }

        [Display(Name = "Employee_Label_YouthLeagueJoinedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string YouthLeagueJoinedPlace { get; set; }

        [Display(Name = "Employee_Label_IsCommunistPartyMember")]
        public Nullable<bool> IsCommunistPartyMember { get; set; }

        [Display(Name = "Employee_Label_CommunistPartyJoinedDate")]
        public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }

        [Display(Name = "Employee_Label_CommunistPartyJoinedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string CommunistPartyJoinedPlace { get; set; }

        [Display(Name = "Employee_Label_FatherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FatherFullName { get; set; }

        [Display(Name = "Employee_Label_FatherBirthDate")]
        public Nullable<System.DateTime> FatherBirthDate { get; set; }

        [Display(Name = "Employee_Label_FatherJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FatherJob { get; set; }

        [Display(Name = "Employee_Label_FatherWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FatherWorkingPlace { get; set; }

        [Display(Name = "Employee_Label_MotherFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string MotherFullName { get; set; }

        [Display(Name = "Employee_Label_MotherBirthDate")]
        public Nullable<System.DateTime> MotherBirthDate { get; set; }

        [Display(Name = "Employee_Label_MotherJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string MotherJob { get; set; }

        [Display(Name = "Employee_Label_MotherWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string MotherWorkingPlace { get; set; }

        [Display(Name = "Employee_Label_SpouseFullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string SpouseFullName { get; set; }

        [Display(Name = "Employee_Label_SpouseBirthDate")]
        public Nullable<System.DateTime> SpouseBirthDate { get; set; }

        [Display(Name = "Employee_Label_SpouseJob")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string SpouseJob { get; set; }

        [Display(Name = "Employee_Label_SpouseWorkingPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string SpouseWorkingPlace { get; set; }

        [Display(Name = "Employee_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }

        [Display(Name = "Employee_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "Employee_Label_IsActive")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }

        [Display(Name = "Employee_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "Employee_Label_IntoSchoolDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.DateTime IntoSchoolDate { get; set; }

        [Display(Name = "Employee_Label_AppliedLevel")]
        public Nullable<int> AppliedLevel { get; set; }

        [Display(Name = "Employee_Label_ContractTypeID")]
        public Nullable<int> ContractTypeID { get; set; }

        [Display(Name = "Employee_Label_RegularRefresher")]
        public Nullable<bool> RegularRefresher { get; set; }

        [Display(Name = "Employee_Label_TrainingLevelID")]
        public Nullable<int> TrainingLevelID { get; set; }

        [Display(Name = "Employee_Label_M_FamilyTypeName")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string M_FamilyTypeName { get; set; }

        [Display(Name = "Employee_Label_M_OldEmployeeID")]
        public Nullable<int> M_OldEmployeeID { get; set; }

        [Display(Name = "Employee_Label_M_OldTeacherID")]
        public Nullable<int> M_OldTeacherID { get; set; }

        [Display(Name = "Employee_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "Employee_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }

        [Display(Name = "Employee_Label_WorkGroupTypeID")]
        public Nullable<int> WorkGroupTypeID { get; set; }

        [Display(Name = "Employee_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Note { get; set; }

    }
}

