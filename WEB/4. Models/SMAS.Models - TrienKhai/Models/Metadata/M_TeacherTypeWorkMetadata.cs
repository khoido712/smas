

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_TeacherTypeWorkMetadata))]
public partial class M_TeacherTypeWork { }

public partial class M_TeacherTypeWorkMetadata
    {
  
    [Display(Name = "M_TeacherTypeWork_Label_TeacherTypeWorkID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherTypeWorkID { get; set; }	
  
    [Display(Name = "M_TeacherTypeWork_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "M_TeacherTypeWork_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "M_TeacherTypeWork_Label_TypeWorkID")]	
    public Nullable<int> TypeWorkID { get; set; }	
  
    [Display(Name = "M_TeacherTypeWork_Label_StartDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime StartDate { get; set; }	
    }
}

