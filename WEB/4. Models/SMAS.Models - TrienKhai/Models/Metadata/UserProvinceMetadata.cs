

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(UserProvinceMetadata))]
public partial class UserProvince { }

public partial class UserProvinceMetadata
    {
  
    [Display(Name = "UserProvince_Label_UserProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserProvinceID { get; set; }	
  
    [Display(Name = "UserProvince_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "UserProvince_Label_UserAccountID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserAccountID { get; set; }	
  
    [Display(Name = "UserProvince_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "UserProvince_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "UserProvince_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "UserProvince_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

