

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SubCommitteeMetadata))]
public partial class SubCommittee { }

public partial class SubCommitteeMetadata
    {
  
    [Display(Name = "SubCommittee_Label_SubCommitteeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubCommitteeID { get; set; }	
  
    [Display(Name = "SubCommittee_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "SubCommittee_Label_Abbreviation")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Abbreviation { get; set; }	
  
    [Display(Name = "SubCommittee_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SubCommittee_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SubCommittee_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SubCommittee_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "SubCommittee_Label_CapacityStatistics")]	
    public virtual ICollection<CapacityStatistic> CapacityStatistics { get; set; }	
  
    [Display(Name = "SubCommittee_Label_ClassProfiles")]	
    public virtual ICollection<ClassProfile> ClassProfiles { get; set; }	
  
    [Display(Name = "SubCommittee_Label_ConductStatistics")]	
    public virtual ICollection<ConductStatistic> ConductStatistics { get; set; }	
  
    /*[Display(Name = "SubCommittee_Label_MarkStatistics")]	
    public virtual ICollection<MarkStatistic> MarkStatistics { get; set; }	*/
  
    [Display(Name = "SubCommittee_Label_StatisticSubcommitteeOfTertiaries")]	
    public virtual ICollection<StatisticSubcommitteeOfTertiary> StatisticSubcommitteeOfTertiaries { get; set; }	
    }
}

