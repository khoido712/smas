

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(OverallTestMetadata))]
public partial class OverallTest { }

public partial class OverallTestMetadata
    {
  
    [Display(Name = "OverallTest_Label_OverallTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OverallTestID { get; set; }	
  
    [Display(Name = "OverallTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "OverallTest_Label_HistoryYourSelf")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HistoryYourSelf { get; set; }	
  
    [Display(Name = "OverallTest_Label_OverallInternal")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OverallInternal { get; set; }	
  
    [Display(Name = "OverallTest_Label_DescriptionOverallInternall")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionOverallInternall { get; set; }	
  
    [Display(Name = "OverallTest_Label_OverallForeign")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OverallForeign { get; set; }	
  
    [Display(Name = "OverallTest_Label_DescriptionForeign")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionForeign { get; set; }	
  
    [Display(Name = "OverallTest_Label_SkinDiseases")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkinDiseases { get; set; }	
  
    [Display(Name = "OverallTest_Label_DescriptionSkinDiseases")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionSkinDiseases { get; set; }	
  
    [Display(Name = "OverallTest_Label_IsHeartDiseases")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsHeartDiseases { get; set; }	
  
    [Display(Name = "OverallTest_Label_IsBirthDefect")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsBirthDefect { get; set; }	
  
    [Display(Name = "OverallTest_Label_Other")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Other { get; set; }	
  
    [Display(Name = "OverallTest_Label_EvaluationHealth")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationHealth { get; set; }	
  
    [Display(Name = "OverallTest_Label_RequireOfDoctor")]
    [StringLength(1000,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RequireOfDoctor { get; set; }	
  
    [Display(Name = "OverallTest_Label_Doctor")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Doctor { get; set; }	
  
    [Display(Name = "OverallTest_Label_IsDredging")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsDredging { get; set; }	
  
    [Display(Name = "OverallTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "OverallTest_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "OverallTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "OverallTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "OverallTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "OverallTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

