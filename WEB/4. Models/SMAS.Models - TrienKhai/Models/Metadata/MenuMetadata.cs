

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MenuMetadata))]
public partial class Menu { }

public partial class MenuMetadata
    {
  
    [Display(Name = "Menu_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "Menu_Label_MenuName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MenuName { get; set; }	
  
    [Display(Name = "Menu_Label_URL")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string URL { get; set; }	
  
    [Display(Name = "Menu_Label_ParentID")]	
    public Nullable<int> ParentID { get; set; }	
  
    [Display(Name = "Menu_Label_CreatedUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CreatedUserID { get; set; }	
  
    [Display(Name = "Menu_Label_OrderNumber")]	
    public Nullable<int> OrderNumber { get; set; }	
  
    [Display(Name = "Menu_Label_MenuPath")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MenuPath { get; set; }	
  
    [Display(Name = "Menu_Label_Description")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Menu_Label_AdminOnly")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool AdminOnly { get; set; }	
  
    [Display(Name = "Menu_Label_IsVisible")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsVisible { get; set; }	
  
    [Display(Name = "Menu_Label_IsCategory")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsCategory { get; set; }	
  
    [Display(Name = "Menu_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Menu_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Menu_Label_BlockMenu")]	
    public Nullable<int> BlockMenu { get; set; }	
  
    [Display(Name = "Menu_Label_ImageURL")]	
    public string ImageURL { get; set; }	
  
    [Display(Name = "Menu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Menu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "Menu_Label_M_MenuID")]	
    public Nullable<int> M_MenuID { get; set; }	

  
    [Display(Name = "Menu_Label_DefaultGroupMenus")]	
    public virtual ICollection<DefaultGroupMenu> DefaultGroupMenus { get; set; }	
  
    [Display(Name = "Menu_Label_GroupMenus")]	
    public virtual ICollection<GroupMenu> GroupMenus { get; set; }	
  
    //[Display(Name = "Menu_Label_Menu1")]	
    //public virtual ICollection<Menu> Menu1 { get; set; }	
  
    [Display(Name = "Menu_Label_Menu2")]	
    public virtual Menu Menu2 { get; set; }	
  
    [Display(Name = "Menu_Label_MenuPermissions")]	
    public virtual ICollection<MenuPermission> MenuPermissions { get; set; }	
  
    [Display(Name = "Menu_Label_RoleMenus")]	
    public virtual ICollection<RoleMenu> RoleMenus { get; set; }	
  
    [Display(Name = "Menu_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

