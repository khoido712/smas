

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SubjectCatMetadata))]
public partial class SubjectCat { }

public partial class SubjectCatMetadata
    {
  
    [Display(Name = "SubjectCat_Label_SubjectCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectCatID { get; set; }	
  
    [Display(Name = "SubjectCat_Label_SubjectName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubjectName { get; set; }	
  
    [Display(Name = "SubjectCat_Label_Abbreviation")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Abbreviation { get; set; }	
  
    [Display(Name = "SubjectCat_Label_DisplayName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisplayName { get; set; }	
  
    [Display(Name = "SubjectCat_Label_MiddleSemesterTest")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool MiddleSemesterTest { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsExemptible")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsExemptible { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsCommenting")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int IsCommenting { get; set; }	
  
    [Display(Name = "SubjectCat_Label_HasPractice")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool HasPractice { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsForeignLanguage")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsForeignLanguage { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsCoreSubject")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsCoreSubject { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsApprenticeshipSubject")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsApprenticeshipSubject { get; set; }	
  
    [Display(Name = "SubjectCat_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SubjectCat_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SubjectCat_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SubjectCat_Label_ApprenticeshipGroupID")]	
    public Nullable<int> ApprenticeshipGroupID { get; set; }	
  
    [Display(Name = "SubjectCat_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "SubjectCat_Label_OrderInSubject")]	
    public Nullable<int> OrderInSubject { get; set; }	
  
    [Display(Name = "SubjectCat_Label_IsEditIsCommentting")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsEditIsCommentting { get; set; }	
  
    [Display(Name = "SubjectCat_Label_ReadAndWriteMiddleTest")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool ReadAndWriteMiddleTest { get; set; }	
  
    [Display(Name = "SubjectCat_Label_Color")]
    [StringLength(7,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Color { get; set; }	

  
    }
}

