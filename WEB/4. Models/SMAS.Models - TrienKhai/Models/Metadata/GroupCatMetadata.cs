

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(GroupCatMetadata))]
public partial class GroupCat { }

public partial class GroupCatMetadata
    {
  
    [Display(Name = "GroupCat_Label_GroupCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupCatID { get; set; }	
  
    [Display(Name = "GroupCat_Label_RoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleID { get; set; }	
  
    [Display(Name = "GroupCat_Label_CreatedUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CreatedUserID { get; set; }	
  
    [Display(Name = "GroupCat_Label_GroupName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string GroupName { get; set; }	
  
    [Display(Name = "GroupCat_Label_Description")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "GroupCat_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "GroupCat_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "GroupCat_Label_AdminUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AdminUserID { get; set; }	
  
    [Display(Name = "GroupCat_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "GroupCat_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "GroupCat_Label_M_GroupID")]	
    public Nullable<int> M_GroupID { get; set; }	
  
    [Display(Name = "GroupCat_Label_M_SchoolID")]	
    public Nullable<int> M_SchoolID { get; set; }	
  
    [Display(Name = "GroupCat_Label_M_SupervisingDeptID")]	
    public Nullable<int> M_SupervisingDeptID { get; set; }	

  
    //[Display(Name = "GroupCat_Label_ActionAudits")]	
    //public virtual ICollection<ActionAudit> ActionAudits { get; set; }	
  
    [Display(Name = "GroupCat_Label_Role")]	
    public virtual Role Role { get; set; }	
  
    [Display(Name = "GroupCat_Label_GroupMenus")]	
    public virtual ICollection<GroupMenu> GroupMenus { get; set; }	
  
    [Display(Name = "GroupCat_Label_UserGroups")]	
    public virtual ICollection<UserGroup> UserGroups { get; set; }	
  
    [Display(Name = "GroupCat_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

