

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CandidateAbsenceMetadata))]
public partial class CandidateAbsence { }

public partial class CandidateAbsenceMetadata
    {
  
    [Display(Name = "CandidateAbsence_Label_AbsentCandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AbsentCandidateID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_ExaminationID")]	
    public Nullable<int> ExaminationID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_CandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CandidateID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_RoomID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoomID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_HasReason")]	
    public Nullable<bool> HasReason { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_ReasonDetail")]
    [StringLength(160,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReasonDetail { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "CandidateAbsence_Label_Candidate")]	
    public virtual Candidate Candidate { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_ExaminationRoom")]	
    public virtual ExaminationRoom ExaminationRoom { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "CandidateAbsence_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
    }
}

