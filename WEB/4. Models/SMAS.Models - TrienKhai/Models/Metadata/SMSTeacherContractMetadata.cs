

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSTeacherContractMetadata))]
public partial class SMSTeacherContract { }

public partial class SMSTeacherContractMetadata
    {
  
    [Display(Name = "SMSTeacherContract_Label_SMSTeacherContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSTeacherContractID { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_ContractCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(64,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractCode { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_PackageID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PackageID { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_SchoolCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SchoolCode { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_SupervisingDeptCode")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SupervisingDeptCode { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_Status")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Status { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_MaxSMS")]	
    public Nullable<int> MaxSMS { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_CreateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CreateUser { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_CreateTime")]	
    public Nullable<System.DateTime> CreateTime { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_UpdateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string UpdateUser { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SMSTeacherContract_Label_CallDetailRecords")]	
    public virtual ICollection<CallDetailRecord> CallDetailRecords { get; set; }	
  
    [Display(Name = "SMSTeacherContract_Label_Package")]	
    public virtual Package Package { get; set; }	
    }
}

