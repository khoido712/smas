

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeWorkingHistoryMetadata))]
public partial class EmployeeWorkingHistory { }

public partial class EmployeeWorkingHistoryMetadata
    {
  
    [Display(Name = "EmployeeWorkingHistory_Label_EmployeeWorkingHistoryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeWorkingHistoryID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_EmployeeID")]	
    public Nullable<int> EmployeeID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_SupervisingDeptID")]	
    public Nullable<int> SupervisingDeptID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_Organization")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Organization { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_Department")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Department { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_Position")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Position { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_Resolution")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeeWorkingHistory_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EmployeeWorkingHistory_Label_SupervisingDept")]	
    public virtual SupervisingDept SupervisingDept { get; set; }	
    }
}

