

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolTypeMetadata))]
public partial class SchoolType { }

public partial class SchoolTypeMetadata
    {
  
    [Display(Name = "SchoolType_Label_SchoolTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolTypeID { get; set; }	
  
    [Display(Name = "SchoolType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "SchoolType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SchoolType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SchoolType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "SchoolType_Label_SchoolProfiles")]	
    public virtual ICollection<SchoolProfile> SchoolProfiles { get; set; }	
    }
}

