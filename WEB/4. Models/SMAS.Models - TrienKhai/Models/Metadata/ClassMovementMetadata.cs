

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassMovementMetadata))]
public partial class ClassMovement { }

public partial class ClassMovementMetadata
    {
  
    [Display(Name = "ClassMovement_Label_ClassMovementID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassMovementID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_FromClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FromClassID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_ToClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ToClassID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_MovedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime MovedDate { get; set; }	
  
    [Display(Name = "ClassMovement_Label_Reason")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Reason { get; set; }	
  
    [Display(Name = "ClassMovement_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "ClassMovement_Label_M_DateInto")]	
    public Nullable<System.DateTime> M_DateInto { get; set; }	
  
    [Display(Name = "ClassMovement_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassMovement_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassMovement_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ClassMovement_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ClassMovement_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ClassMovement_Label_ClassProfile1")]	
    public virtual ClassProfile ClassProfile1 { get; set; }	
  
    [Display(Name = "ClassMovement_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "ClassMovement_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

