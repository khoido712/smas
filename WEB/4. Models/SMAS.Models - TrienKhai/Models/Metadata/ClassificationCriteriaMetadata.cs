

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassificationCriteriaMetadata))]
public partial class ClassificationCriteria { }

public partial class ClassificationCriteriaMetadata
    {
  
    [Display(Name = "ClassificationCriteria_Label_ClassificationCriteriaID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassificationCriteriaID { get; set; }	
  
    [Display(Name = "ClassificationCriteria_Label_EffectDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime EffectDate { get; set; }	
  
    [Display(Name = "ClassificationCriteria_Label_Genre")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool Genre { get; set; }	
  
    [Display(Name = "ClassificationCriteria_Label_TypeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeConfigID { get; set; }	
  
    [Display(Name = "ClassificationCriteria_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	

  
    [Display(Name = "ClassificationCriteria_Label_TypeConfig")]	
    public virtual TypeConfig TypeConfig { get; set; }	
  
    [Display(Name = "ClassificationCriteria_Label_ClassificationCriteriaDetails")]	
    public virtual ICollection<ClassificationCriteriaDetail> ClassificationCriteriaDetails { get; set; }	
    }
}

