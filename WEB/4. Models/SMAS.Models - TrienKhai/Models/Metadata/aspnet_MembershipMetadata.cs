

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(aspnet_MembershipMetadata))]
public partial class aspnet_Membership { }

public partial class aspnet_MembershipMetadata
    {
  
    [Display(Name = "aspnet_Membership_Label_ApplicationId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid ApplicationId { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_UserId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid UserId { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_Password")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Password { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_PasswordFormat")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PasswordFormat { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_PasswordSalt")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PasswordSalt { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_MobilePIN")]
    [StringLength(16,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MobilePIN { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_Email")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Email { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_LoweredEmail")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string LoweredEmail { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_PasswordQuestion")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PasswordQuestion { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_PasswordAnswer")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PasswordAnswer { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_IsApproved")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsApproved { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_IsLockedOut")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsLockedOut { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_LastLoginDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime LastLoginDate { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_LastPasswordChangedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime LastPasswordChangedDate { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_LastLockoutDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime LastLockoutDate { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_FailedPasswordAttemptCount")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FailedPasswordAttemptCount { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_FailedPasswordAttemptWindowStart")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FailedPasswordAttemptWindowStart { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_FailedPasswordAnswerAttemptCount")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FailedPasswordAnswerAttemptCount { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_FailedPasswordAnswerAttemptWindowStart")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_Comment")]	
    public string Comment { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "aspnet_Membership_Label_aspnet_Applications")]	
    public virtual aspnet_Applications aspnet_Applications { get; set; }	
  
    [Display(Name = "aspnet_Membership_Label_aspnet_Users")]	
    public virtual aspnet_Users aspnet_Users { get; set; }	
    }
}

