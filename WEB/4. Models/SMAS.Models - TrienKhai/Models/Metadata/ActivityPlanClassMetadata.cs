

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityPlanClassMetadata))]
public partial class ActivityPlanClass { }

public partial class ActivityPlanClassMetadata
    {
  
    [Display(Name = "ActivityPlanClass_Label_ActivityPlanClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityPlanClassID { get; set; }	
  
    [Display(Name = "ActivityPlanClass_Label_ActivityPlanID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityPlanID { get; set; }	
  
    [Display(Name = "ActivityPlanClass_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ActivityPlanClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ActivityPlanClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ActivityPlanClass_Label_ActivityPlan")]	
    public virtual ActivityPlan ActivityPlan { get; set; }	
  
    [Display(Name = "ActivityPlanClass_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

