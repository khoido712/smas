

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeWorkMovementMetadata))]
public partial class EmployeeWorkMovement { }

public partial class EmployeeWorkMovementMetadata
    {
  
    [Display(Name = "EmployeeWorkMovement_Label_EmployeeWorkMovementID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeWorkMovementID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_FromSchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FromSchoolID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_ResolutionDocument")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ResolutionDocument { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_MovementType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MovementType { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_ToSchoolID")]	
    public Nullable<int> ToSchoolID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_ToSupervisingDeptID")]	
    public Nullable<int> ToSupervisingDeptID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_ToProvinceID")]	
    public Nullable<int> ToProvinceID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_ToDistrictID")]	
    public Nullable<int> ToDistrictID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_MoveTo")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MoveTo { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_MovedDate")]	
    public Nullable<System.DateTime> MovedDate { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_IsAccepted")]	
    public Nullable<bool> IsAccepted { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_FacultyID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FacultyID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_OldAccountID")]	
    public Nullable<int> OldAccountID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeeWorkMovement_Label_District")]	
    public virtual District District { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_SchoolProfile1")]	
    public virtual SchoolProfile SchoolProfile1 { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_SupervisingDept")]	
    public virtual SupervisingDept SupervisingDept { get; set; }	
  
    [Display(Name = "EmployeeWorkMovement_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

