

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolSubjectMetadata))]
public partial class SchoolSubject { }

public partial class SchoolSubjectMetadata
    {
  
    [Display(Name = "SchoolSubject_Label_SchoolSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolSubjectID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_FirstSemesterCoefficient")]	
    public Nullable<int> FirstSemesterCoefficient { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SecondSemesterCoefficient")]	
    public Nullable<int> SecondSemesterCoefficient { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_AppliedType")]	
    public Nullable<int> AppliedType { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_IsCommenting")]	
    public Nullable<int> IsCommenting { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SectionPerWeekFirstSemester")]	
    public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SectionPerWeekSecondSemester")]	
    public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SchoolSubject_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SchoolSubject_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

