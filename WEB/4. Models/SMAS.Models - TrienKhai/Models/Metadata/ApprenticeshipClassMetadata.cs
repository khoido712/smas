

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ApprenticeshipClassMetadata))]
public partial class ApprenticeshipClass { }

public partial class ApprenticeshipClassMetadata
    {
  
    [Display(Name = "ApprenticeshipClass_Label_ClassName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ClassName { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_HeadTeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HeadTeacherID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipSubjectID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_SubjectType")]	
    public Nullable<int> SubjectType { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_IsCommenting")]	
    public Nullable<int> IsCommenting { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipClassID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipSubject")]	
    public virtual ApprenticeshipSubject ApprenticeshipSubject { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipTrainings")]	
    public virtual ICollection<ApprenticeshipTraining> ApprenticeshipTrainings { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipTrainingAbsences")]	
    public virtual ICollection<ApprenticeshipTrainingAbsence> ApprenticeshipTrainingAbsences { get; set; }	
  
    [Display(Name = "ApprenticeshipClass_Label_ApprenticeshipTrainingSchedules")]	
    public virtual ICollection<ApprenticeshipTrainingSchedule> ApprenticeshipTrainingSchedules { get; set; }	
    }
}

