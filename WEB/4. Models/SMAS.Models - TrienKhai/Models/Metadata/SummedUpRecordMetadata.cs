

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(SummedUpRecordMetadata))]
    public partial class SummedUpRecord { }

    public partial class SummedUpRecordMetadata
    {

        [Display(Name = "SummedUpRecord_Label_SummedUpRecordID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public long SummedUpRecordID { get; set; }

        [Display(Name = "SummedUpRecord_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [Display(Name = "SummedUpRecord_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [Display(Name = "SummedUpRecord_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolID { get; set; }

        [Display(Name = "SummedUpRecord_Label_AcademicYearID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int AcademicYearID { get; set; }

        [Display(Name = "SummedUpRecord_Label_SubjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID { get; set; }

        [Display(Name = "SummedUpRecord_Label_IsCommenting")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int IsCommenting { get; set; }

        [Display(Name = "SummedUpRecord_Label_Semester")]
        public Nullable<int> Semester { get; set; }

        [Display(Name = "SummedUpRecord_Label_PeriodID")]
        public Nullable<int> PeriodID { get; set; }

        [Display(Name = "SummedUpRecord_Label_SummedUpMark")]
        public Nullable<decimal> SummedUpMark { get; set; }

        [Display(Name = "SummedUpRecord_Label_JudgementResult")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string JudgementResult { get; set; }

        [Display(Name = "SummedUpRecord_Label_Comment")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Comment { get; set; }

        [Display(Name = "SummedUpRecord_Label_SummedUpDate")]
        public Nullable<System.DateTime> SummedUpDate { get; set; }

        [Display(Name = "SummedUpRecord_Label_ReTestMark")]
        public Nullable<decimal> ReTestMark { get; set; }

        [Display(Name = "SummedUpRecord_Label_ReTestJudgement")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ReTestJudgement { get; set; }

        [Display(Name = "SummedUpRecord_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "SummedUpRecord_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }

        [Display(Name = "SummedUpRecord_Label_IsOldData")]
        public Nullable<bool> IsOldData { get; set; }

        //public Nullable<bool> IsLegalBot { get; set; }

        [Display(Name = "SummedUpRecord_Label_SubjectCat")]
        public virtual SubjectCat SubjectCat { get; set; }

        [Display(Name = "SummedUpRecord_Label_PupilProfile")]
        public virtual PupilProfile PupilProfile { get; set; }

        [Display(Name = "SummedUpRecord_Label_AcademicYear")]
        public virtual AcademicYear AcademicYear { get; set; }

        [Display(Name = "SummedUpRecord_Label_ClassProfile")]
        public virtual ClassProfile ClassProfile { get; set; }

        [Display(Name = "SummedUpRecord_Label_PeriodDeclaration")]
        public virtual PeriodDeclaration PeriodDeclaration { get; set; }

        [Display(Name = "SummedUpRecord_Label_SchoolProfile")]
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}

