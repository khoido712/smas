

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DailyMenuMetadata))]
public partial class DailyMenu { }

public partial class DailyMenuMetadata
    {
  
    [Display(Name = "DailyMenu_Label_DailyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuID { get; set; }	
  
    [Display(Name = "DailyMenu_Label_DailyMenuCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DailyMenuCode { get; set; }	
  
    [Display(Name = "DailyMenu_Label_NumberOfChildren")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NumberOfChildren { get; set; }	
  
    [Display(Name = "DailyMenu_Label_MenuType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuType { get; set; }	
  
    [Display(Name = "DailyMenu_Label_GetFromDishCat")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool GetFromDishCat { get; set; }	
  
    [Display(Name = "DailyMenu_Label_EatingGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupID { get; set; }	
  
    [Display(Name = "DailyMenu_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "DailyMenu_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "DailyMenu_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "DailyMenu_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "DailyMenu_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "DailyMenu_Label_DailyCostOfChildren")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyCostOfChildren { get; set; }	
  
    [Display(Name = "DailyMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DailyMenu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DailyMenu_Label_EatingGroup")]	
    public virtual EatingGroup EatingGroup { get; set; }	
  
    [Display(Name = "DailyMenu_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "DailyMenu_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "DailyMenu_Label_DailyMenuDetails")]	
    public virtual ICollection<DailyMenuDetail> DailyMenuDetails { get; set; }	
  
    [Display(Name = "DailyMenu_Label_DailyMenuFoods")]	
    public virtual ICollection<DailyMenuFood> DailyMenuFoods { get; set; }	
  
    [Display(Name = "DailyMenu_Label_DishInspections")]	
    public virtual ICollection<DishInspection> DishInspections { get; set; }	
  
    [Display(Name = "DailyMenu_Label_WeeklyMenuDetails")]	
    public virtual ICollection<WeeklyMenuDetail> WeeklyMenuDetails { get; set; }	
    }
}

