

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActionAuditMetadata))]
public partial class ActionAudit { }

public partial class ActionAuditMetadata
    {
  
    [Display(Name = "ActionAudit_Label_ActionAuditID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActionAuditID { get; set; }	
  
    [Display(Name = "ActionAudit_Label_UserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserID { get; set; }	
  
    [Display(Name = "ActionAudit_Label_Controller")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Controller { get; set; }	
  
    [Display(Name = "ActionAudit_Label_Action")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Action { get; set; }	
  
    [Display(Name = "ActionAudit_Label_Parameter")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Parameter { get; set; }	
  
    [Display(Name = "ActionAudit_Label_BeginAuditTime")]	
    public Nullable<System.DateTime> BeginAuditTime { get; set; }	
  
    [Display(Name = "ActionAudit_Label_EndAuditTime")]	
    public Nullable<System.DateTime> EndAuditTime { get; set; }	
  
    [Display(Name = "ActionAudit_Label_IP")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string IP { get; set; }	
  
    [Display(Name = "ActionAudit_Label_Referer")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Referer { get; set; }	
  
    [Display(Name = "ActionAudit_Label_ObjectId")]	
    public Nullable<int> ObjectId { get; set; }	
  
    [Display(Name = "ActionAudit_Label_oldObjectValue")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string oldObjectValue { get; set; }	
  
    [Display(Name = "ActionAudit_Label_newObjectValue")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string newObjectValue { get; set; }	
  
    [Display(Name = "ActionAudit_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ActionAudit_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

