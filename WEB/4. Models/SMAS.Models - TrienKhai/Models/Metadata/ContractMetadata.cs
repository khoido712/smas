

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ContractMetadata))]
public partial class Contract { }

public partial class ContractMetadata
    {
  
    [Display(Name = "Contract_Label_ContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContractID { get; set; }	
  
    [Display(Name = "Contract_Label_ContractTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContractTypeID { get; set; }	
  
    [Display(Name = "Contract_Label_ContractDate")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractDate { get; set; }	
  
    [Display(Name = "Contract_Label_ExpiredDate")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ExpiredDate { get; set; }	
  
    [Display(Name = "Contract_Label_ContractCode")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractCode { get; set; }	
  
    [Display(Name = "Contract_Label_Employer")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Employer { get; set; }	
  
    [Display(Name = "Contract_Label_Employee")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Employee { get; set; }	
  
    [Display(Name = "Contract_Label_PlaceOfWork")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PlaceOfWork { get; set; }	
  
    [Display(Name = "Contract_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Contract_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Contract_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "Contract_Label_ContractType")]	
    public virtual ContractType ContractType { get; set; }	
  
    //[Display(Name = "Contract_Label_Employees")]	
    //public virtual ICollection<Employee> Employees { get; set; }	
    }
}

