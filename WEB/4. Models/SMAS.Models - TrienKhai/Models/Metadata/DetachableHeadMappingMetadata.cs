

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DetachableHeadMappingMetadata))]
public partial class DetachableHeadMapping { }

public partial class DetachableHeadMappingMetadata
    {
  
    [Display(Name = "DetachableHeadMapping_Label_DetachableHeadMappingID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DetachableHeadMappingID { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_DetachableHeadBagID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DetachableHeadBagID { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_OrderNumber")]	
    public Nullable<int> OrderNumber { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_NamedListCode")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NamedListCode { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_DetachableHeadNumber")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DetachableHeadNumber { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_Description")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_CandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CandidateID { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_NamedListNumber")]	
    public Nullable<int> NamedListNumber { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DetachableHeadMapping_Label_Candidate")]	
    public virtual Candidate Candidate { get; set; }	
  
    [Display(Name = "DetachableHeadMapping_Label_DetachableHeadBag")]	
    public virtual DetachableHeadBag DetachableHeadBag { get; set; }	
    }
}

