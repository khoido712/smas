

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SkillTitleClassMetadata))]
public partial class SkillTitleClass { }

public partial class SkillTitleClassMetadata
    {
  
    [Display(Name = "SkillTitleClass_Label_SkillTitleClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillTitleClassID { get; set; }	
  
    [Display(Name = "SkillTitleClass_Label_SkillTitleID")]	
    public Nullable<int> SkillTitleID { get; set; }	
  
    [Display(Name = "SkillTitleClass_Label_ClassID")]	
    public Nullable<int> ClassID { get; set; }	

  
    [Display(Name = "SkillTitleClass_Label_SkillTitle")]	
    public virtual SkillTitle SkillTitle { get; set; }	
  
    [Display(Name = "SkillTitleClass_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

