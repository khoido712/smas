

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DishCatMetadata))]
public partial class DishCat { }

public partial class DishCatMetadata
    {
  
    [Display(Name = "DishCat_Label_DishCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishCatID { get; set; }	
  
    [Display(Name = "DishCat_Label_DishName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DishName { get; set; }	
  
    [Display(Name = "DishCat_Label_NumberOfServing")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NumberOfServing { get; set; }	
  
    [Display(Name = "DishCat_Label_Prepare")]	
    public string Prepare { get; set; }	
  
    [Display(Name = "DishCat_Label_Receipt")]	
    public string Receipt { get; set; }	
  
    [Display(Name = "DishCat_Label_Note")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note { get; set; }	
  
    [Display(Name = "DishCat_Label_TypeOfDishID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeOfDishID { get; set; }	
  
    [Display(Name = "DishCat_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "DishCat_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "DishCat_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "DishCat_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "DishCat_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DishCat_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DishCat_Label_TypeOfDish")]	
    public virtual TypeOfDish TypeOfDish { get; set; }	
  
    [Display(Name = "DishCat_Label_DailyMenuDetails")]	
    public virtual ICollection<DailyMenuDetail> DailyMenuDetails { get; set; }	
  
    [Display(Name = "DishCat_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "DishCat_Label_DishDetails")]	
    public virtual ICollection<DishDetail> DishDetails { get; set; }	
    }
}

