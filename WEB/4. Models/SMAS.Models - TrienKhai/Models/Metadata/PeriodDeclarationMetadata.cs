

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(PeriodDeclarationMetadata))]
    public partial class PeriodDeclaration { }

    public partial class PeriodDeclarationMetadata
    {

        [Display(Name = "PeriodDeclaration_Label_PeriodDeclarationID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PeriodDeclarationID { get; set; }

        [Display(Name = "PeriodDeclaration_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [Display(Name = "PeriodDeclaration_Label_AcademicYearID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int AcademicYearID { get; set; }

        [Display(Name = "PeriodDeclaration_Label_SchoolID")]
        public Nullable<int> SchoolID { get; set; }

        [Display(Name = "PeriodDeclaration_Label_Year")]
        public Nullable<int> Year { get; set; }

        [Display(Name = "PeriodDeclaration_Label_Semester")]
        public Nullable<int> Semester { get; set; }

        [Display(Name = "PeriodDeclaration_Label_FromDate")]
        public Nullable<System.DateTime> FromDate { get; set; }

        [Display(Name = "PeriodDeclaration_Label_EndDate")]
        public Nullable<System.DateTime> EndDate { get; set; }

        [Display(Name = "PeriodDeclaration_Label_InterviewMark")]
        public Nullable<int> InterviewMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_StartIndexOfInterviewMark")]
        public Nullable<int> StartIndexOfInterviewMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_WritingMark")]
        public Nullable<int> WritingMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_StartIndexOfWritingMark")]
        public Nullable<int> StartIndexOfWritingMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_TwiceCoeffiecientMark")]
        public Nullable<int> TwiceCoeffiecientMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_StartIndexOfTwiceCoeffiecientMark")]
        public Nullable<int> StartIndexOfTwiceCoeffiecientMark { get; set; }

        [Display(Name = "PeriodDeclaration_Label_IsLock")]
        public Nullable<bool> IsLock { get; set; }

        [Display(Name = "PeriodDeclaration_Label_ContaintSemesterMark")]
        public Nullable<bool> ContaintSemesterMark { get; set; }

    }
}

