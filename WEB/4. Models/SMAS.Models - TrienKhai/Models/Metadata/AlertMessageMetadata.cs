

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(AlertMessageMetadata))]
public partial class AlertMessage { }

public partial class AlertMessageMetadata
    {
  
    [Display(Name = "AlertMessage_Label_AlertMessageID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AlertMessageID { get; set; }	
  
    [Display(Name = "AlertMessage_Label_Title")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "AlertMessage_Label_FileUrl")]	
    public string FileUrl { get; set; }	
  
    [Display(Name = "AlertMessage_Label_PublishedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime PublishedDate { get; set; }	
  
    [Display(Name = "AlertMessage_Label_ContentMessage")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public string ContentMessage { get; set; }	
  
    [Display(Name = "AlertMessage_Label_IsPublish")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsPublish { get; set; }	
  
    [Display(Name = "AlertMessage_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "AlertMessage_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "AlertMessage_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
    }
}

