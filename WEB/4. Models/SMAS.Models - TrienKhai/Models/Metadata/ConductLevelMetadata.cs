

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConductLevelMetadata))]
public partial class ConductLevel { }

public partial class ConductLevelMetadata
    {
  
    [Display(Name = "ConductLevel_Label_ConductLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductLevelID { get; set; }	
  
    [Display(Name = "ConductLevel_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ConductLevel_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "ConductLevel_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ConductLevel_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "ConductLevel_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ConductLevel_Label_ConductConfigByCapacities")]	
    public virtual ICollection<ConductConfigByCapacity> ConductConfigByCapacities { get; set; }	
  
    [Display(Name = "ConductLevel_Label_ConductConfigByViolations")]	
    public virtual ICollection<ConductConfigByViolation> ConductConfigByViolations { get; set; }	
  
    [Display(Name = "ConductLevel_Label_PupilRankings")]	
    public virtual ICollection<PupilRanking> PupilRankings { get; set; }	
    }
}

