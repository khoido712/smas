

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CapacityStatisticMetadata))]
public partial class CapacityStatistic { }

public partial class CapacityStatisticMetadata
    {
  
    [Display(Name = "CapacityStatistic_Label_CapacityStatisticID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CapacityStatisticID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_ProcessedDate")]	
    public Nullable<System.DateTime> ProcessedDate { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportCode { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SubCommitteeID")]	
    public Nullable<int> SubCommitteeID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SentToSupervisor")]	
    public Nullable<bool> SentToSupervisor { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel01")]	
    public Nullable<int> CapacityLevel01 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel02")]	
    public Nullable<int> CapacityLevel02 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel03")]	
    public Nullable<int> CapacityLevel03 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel04")]	
    public Nullable<int> CapacityLevel04 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel05")]	
    public Nullable<int> CapacityLevel05 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel06")]	
    public Nullable<int> CapacityLevel06 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_CapacityLevel07")]	
    public Nullable<int> CapacityLevel07 { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_PupilTotal")]	
    public Nullable<int> PupilTotal { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_BelowAverage")]	
    public Nullable<int> BelowAverage { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_OnAverage")]	
    public Nullable<int> OnAverage { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_IsCommenting")]	
    public Nullable<int> IsCommenting { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "CapacityStatistic_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SubCommittee")]	
    public virtual SubCommittee SubCommittee { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "CapacityStatistic_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

