

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeScaleMetadata))]
public partial class EmployeeScale { }

public partial class EmployeeScaleMetadata
    {
  
    [Display(Name = "EmployeeScale_Label_EmployeeScaleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeScaleID { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_Resolution")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_DurationInYear")]	
    public Nullable<int> DurationInYear { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "EmployeeScale_Label_EmployeeSalaries")]	
    public virtual ICollection<EmployeeSalary> EmployeeSalaries { get; set; }	
  
    [Display(Name = "EmployeeScale_Label_SalaryLevels")]	
    public virtual ICollection<SalaryLevel> SalaryLevels { get; set; }	
    }
}

