

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(PupilRankingMetadata))]
    public partial class PupilRanking { }

    public partial class PupilRankingMetadata
    {

        [Display(Name = "PupilRanking_Label_PupilRankingID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public long PupilRankingID { get; set; }

        [Display(Name = "PupilRanking_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [Display(Name = "PupilRanking_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [Display(Name = "PupilRanking_Label_AcademicYearID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int AcademicYearID { get; set; }

        [Display(Name = "PupilRanking_Label_EducationLevelID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevelID { get; set; }

        [Display(Name = "PupilRanking_Label_Semester")]
        public Nullable<int> Semester { get; set; }

        [Display(Name = "PupilRanking_Label_PeriodID")]
        public Nullable<int> PeriodID { get; set; }

        [Display(Name = "PupilRanking_Label_AverageMark")]
        public Nullable<decimal> AverageMark { get; set; }

        [Display(Name = "PupilRanking_Label_TotalAbsentDaysWithPermission")]
        public Nullable<int> TotalAbsentDaysWithPermission { get; set; }

        [Display(Name = "PupilRanking_Label_TotalAbsentDaysWithoutPermission")]
        public Nullable<int> TotalAbsentDaysWithoutPermission { get; set; }

        [Display(Name = "PupilRanking_Label_CapacityLevelID")]
        public Nullable<int> CapacityLevelID { get; set; }

        [Display(Name = "PupilRanking_Label_ConductLevelID")]
        public Nullable<int> ConductLevelID { get; set; }

        [Display(Name = "PupilRanking_Label_Rank")]
        public Nullable<int> Rank { get; set; }

        [Display(Name = "PupilRanking_Label_RankingDate")]
        public Nullable<System.DateTime> RankingDate { get; set; }

        [Display(Name = "PupilRanking_Label_StudyingJudgementID")]
        public Nullable<int> StudyingJudgementID { get; set; }

        [Display(Name = "PupilRanking_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolID { get; set; }

        [Display(Name = "PupilRanking_Label_IsCategory")]
        public Nullable<bool> IsCategory { get; set; }

        [Display(Name = "PupilRanking_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "PupilRanking_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }

        [Display(Name = "PupilRanking_Label_IsOldData")]
        public Nullable<bool> IsOldData { get; set; }



    }
}

