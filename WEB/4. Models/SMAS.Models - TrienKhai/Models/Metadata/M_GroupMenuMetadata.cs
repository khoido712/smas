

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_GroupMenuMetadata))]
public partial class M_GroupMenu { }

public partial class M_GroupMenuMetadata
    {
  
    [Display(Name = "M_GroupMenu_Label_GroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupID { get; set; }	
  
    [Display(Name = "M_GroupMenu_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "M_GroupMenu_Label_Permission")]	
    public Nullable<int> Permission { get; set; }	
  
    [Display(Name = "M_GroupMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_GroupMenu_Label_ID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ID { get; set; }	
    }
}

