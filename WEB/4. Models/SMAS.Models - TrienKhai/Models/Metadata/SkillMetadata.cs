

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SkillMetadata))]
public partial class Skill { }

public partial class SkillMetadata
    {
  
    [Display(Name = "Skill_Label_SkillID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillID { get; set; }	
  
    [Display(Name = "Skill_Label_SkillTitleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillTitleID { get; set; }	
  
    [Display(Name = "Skill_Label_Description")]	
    public string Description { get; set; }	
  
    [Display(Name = "Skill_Label_FromDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FromDate { get; set; }	
  
    [Display(Name = "Skill_Label_ToDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ToDate { get; set; }	
  
    [Display(Name = "Skill_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Skill_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "Skill_Label_Period")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Period { get; set; }	

  
    [Display(Name = "Skill_Label_SkillTitle")]	
    public virtual SkillTitle SkillTitle { get; set; }	
    }
}

