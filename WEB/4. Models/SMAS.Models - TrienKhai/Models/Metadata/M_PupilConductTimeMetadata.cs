

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_PupilConductTimeMetadata))]
public partial class M_PupilConductTime { }

public partial class M_PupilConductTimeMetadata
    {
  
    [Display(Name = "M_PupilConductTime_Label_PupilConductTimeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilConductTimeID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_MarkConfigByTimeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkConfigByTimeID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_ConductID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_LevelID")]	
    public Nullable<byte> LevelID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_PupilConductTime_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

