

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ENTTestMetadata))]
public partial class ENTTest { }

public partial class ENTTestMetadata
    {
  
    [Display(Name = "ENTTest_Label_ENTTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ENTTestID { get; set; }	
  
    [Display(Name = "ENTTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "ENTTest_Label_RCapacityHearing")]	
    public Nullable<int> RCapacityHearing { get; set; }	
  
    [Display(Name = "ENTTest_Label_LCapacityHearing")]	
    public Nullable<int> LCapacityHearing { get; set; }	
  
    [Display(Name = "ENTTest_Label_IsDeaf")]
    public bool IsDeaf { get; set; }	
  
    [Display(Name = "ENTTest_Label_IsSick")]
    public bool IsSick { get; set; }	
  
    [Display(Name = "ENTTest_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ENTTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ENTTest_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ENTTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ENTTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ENTTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ENTTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

