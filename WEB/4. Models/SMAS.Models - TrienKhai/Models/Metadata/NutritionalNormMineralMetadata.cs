

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(NutritionalNormMineralMetadata))]
public partial class NutritionalNormMineral { }

public partial class NutritionalNormMineralMetadata
    {
  
    [Display(Name = "NutritionalNormMineral_Label_NutritionalNormMineralID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NutritionalNormMineralID { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_ValueTo")]	
    public Nullable<decimal> ValueTo { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_ValueFrom")]	
    public Nullable<decimal> ValueFrom { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_MinenalID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MinenalID { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_NutritionalNormID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NutritionalNormID { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "NutritionalNormMineral_Label_MinenalCat")]	
    public virtual MinenalCat MinenalCat { get; set; }	
  
    [Display(Name = "NutritionalNormMineral_Label_NutritionalNorm")]	
    public virtual NutritionalNorm NutritionalNorm { get; set; }	
    }
}

