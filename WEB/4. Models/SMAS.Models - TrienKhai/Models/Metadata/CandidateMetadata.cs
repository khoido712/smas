

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CandidateMetadata))]
public partial class Candidate { }

public partial class CandidateMetadata
    {
  
    [Display(Name = "Candidate_Label_CandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CandidateID { get; set; }	
  
    [Display(Name = "Candidate_Label_ExaminationID")]	
    public Nullable<int> ExaminationID { get; set; }	
  
    [Display(Name = "Candidate_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "Candidate_Label_RoomID")]	
    public Nullable<int> RoomID { get; set; }	
  
    [Display(Name = "Candidate_Label_ClassID")]	
    public Nullable<int> ClassID { get; set; }	
  
    [Display(Name = "Candidate_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "Candidate_Label_OrderNumber")]	
    public Nullable<int> OrderNumber { get; set; }	
  
    [Display(Name = "Candidate_Label_Mark")]	
    public Nullable<decimal> Mark { get; set; }	
  
    [Display(Name = "Candidate_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "Candidate_Label_PupilCode")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "Candidate_Label_Judgement")]
    [StringLength(2,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Judgement { get; set; }	
  
    [Display(Name = "Candidate_Label_NamedListCode")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NamedListCode { get; set; }	
  
    [Display(Name = "Candidate_Label_NamedListNumber")]	
    public Nullable<int> NamedListNumber { get; set; }	
  
    [Display(Name = "Candidate_Label_HasReason")]	
    public Nullable<bool> HasReason { get; set; }	
  
    [Display(Name = "Candidate_Label_ReasonDetail")]
    [StringLength(160,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReasonDetail { get; set; }	
  
    [Display(Name = "Candidate_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Candidate_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Candidate_Label_CandidateAbsences")]	
    public virtual ICollection<CandidateAbsence> CandidateAbsences { get; set; }	
  
    [Display(Name = "Candidate_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "Candidate_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "Candidate_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "Candidate_Label_ExaminationRoom")]	
    public virtual ExaminationRoom ExaminationRoom { get; set; }	
  
    [Display(Name = "Candidate_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "Candidate_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "Candidate_Label_DetachableHeadMappings")]	
    public virtual ICollection<DetachableHeadMapping> DetachableHeadMappings { get; set; }	
  
    [Display(Name = "Candidate_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
    }
}

