

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(StatisticSubjectOfTertiaryMetadata))]
public partial class StatisticSubjectOfTertiary { }

public partial class StatisticSubjectOfTertiaryMetadata
    {
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_StatisticSubjectOfTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int StatisticSubjectOfTertiaryID { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_TotalTeacher")]	
    public Nullable<int> TotalTeacher { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SpecicalType")]	
    public Nullable<int> SpecicalType { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_M_SubjectName")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M_SubjectName { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "StatisticSubjectOfTertiary_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

