

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilRepeatedMetadata))]
public partial class PupilRepeated { }

public partial class PupilRepeatedMetadata
    {
  
    [Display(Name = "PupilRepeated_Label_PupilRepeatedID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilRepeatedID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_OldClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OldClassID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_RepeatedClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RepeatedClassID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_RepeatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime RepeatedDate { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PupilRepeated_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_ClassProfile1")]	
    public virtual ClassProfile ClassProfile1 { get; set; }	
  
    [Display(Name = "PupilRepeated_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

