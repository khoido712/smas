

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CapacityLevelMetadata))]
public partial class CapacityLevel { }

public partial class CapacityLevelMetadata
    {
  
    [Display(Name = "CapacityLevel_Label_CapacityLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CapacityLevelID { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_CapacityLevel1")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CapacityLevel1 { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_AppliedForPrimary")]	
    public Nullable<bool> AppliedForPrimary { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_AppliedForSecondary")]	
    public Nullable<bool> AppliedForSecondary { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_AppliedForTertiary")]	
    public Nullable<bool> AppliedForTertiary { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "CapacityLevel_Label_ConductConfigByCapacities")]	
    public virtual ICollection<ConductConfigByCapacity> ConductConfigByCapacities { get; set; }	
  
    [Display(Name = "CapacityLevel_Label_PupilRankings")]	
    public virtual ICollection<PupilRanking> PupilRankings { get; set; }	
    }
}

