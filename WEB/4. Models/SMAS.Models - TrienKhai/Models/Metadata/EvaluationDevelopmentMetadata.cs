

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EvaluationDevelopmentMetadata))]
public partial class EvaluationDevelopment { }

public partial class EvaluationDevelopmentMetadata
    {
  
    [Display(Name = "EvaluationDevelopment_Label_EvaluationDevelopmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationDevelopmentID { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_EvaluationDevelopmentCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EvaluationDevelopmentCode { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_EvaluationDevelopmentName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EvaluationDevelopmentName { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_IsRequired")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsRequired { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_InputType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InputType { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_InputMin")]	
    public Nullable<decimal> InputMin { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_InputMax")]	
    public Nullable<decimal> InputMax { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_EvaluationDevelopmentGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationDevelopmentGroupID { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "EvaluationDevelopment_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_DevelopmentOfChildrens")]	
    public virtual ICollection<DevelopmentOfChildren> DevelopmentOfChildrens { get; set; }	
  
    [Display(Name = "EvaluationDevelopment_Label_EvaluationDevelopmentGroup")]	
    public virtual EvaluationDevelopmentGroup EvaluationDevelopmentGroup { get; set; }	
    }
}

