

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MenuPermissionMetadata))]
public partial class MenuPermission { }

public partial class MenuPermissionMetadata
    {
  
    [Display(Name = "MenuPermission_Label_MenuPermissionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuPermissionID { get; set; }	
  
    [Display(Name = "MenuPermission_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "MenuPermission_Label_Permission")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Permission { get; set; }	
  
    [Display(Name = "MenuPermission_Label_URL")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string URL { get; set; }	
  
    [Display(Name = "MenuPermission_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "MenuPermission_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "MenuPermission_Label_Menu")]	
    public virtual Menu Menu { get; set; }	
    }
}

