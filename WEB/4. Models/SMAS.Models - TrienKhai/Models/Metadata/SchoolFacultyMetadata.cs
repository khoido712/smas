

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolFacultyMetadata))]
public partial class SchoolFaculty { }

public partial class SchoolFacultyMetadata
    {
  
    [Display(Name = "SchoolFaculty_Label_SchoolFacultyID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolFacultyID { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_FacultyName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FacultyName { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SchoolFaculty_Label_ConcurrentWorkAssignments")]	
    public virtual ICollection<ConcurrentWorkAssignment> ConcurrentWorkAssignments { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_ConcurrentWorkReplacements")]	
    public virtual ICollection<ConcurrentWorkReplacement> ConcurrentWorkReplacements { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_EmployeePraiseDisciplines")]	
    public virtual ICollection<EmployeePraiseDiscipline> EmployeePraiseDisciplines { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_EmployeeWorkMovements")]	
    public virtual ICollection<EmployeeWorkMovement> EmployeeWorkMovements { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_HonourAchivements")]	
    public virtual ICollection<HonourAchivement> HonourAchivements { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_TeacherGradings")]	
    public virtual ICollection<TeacherGrading> TeacherGradings { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_TeacherOfFaculties")]	
    public virtual ICollection<TeacherOfFaculty> TeacherOfFaculties { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_TeachingAssignments")]	
    public virtual ICollection<TeachingAssignment> TeachingAssignments { get; set; }	
  
    [Display(Name = "SchoolFaculty_Label_TeachingExperiences")]	
    public virtual ICollection<TeachingExperience> TeachingExperiences { get; set; }	
    }
}

