

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityOfClassMetadata))]
public partial class ActivityOfClass { }

public partial class ActivityOfClassMetadata
    {
  
    [Display(Name = "ActivityOfClass_Label_ActivityOfClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityOfClassID { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityPlanID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityPlanID { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_DayOfWeek")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte DayOfWeek { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_Section")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte Section { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityOfClassName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ActivityOfClassName { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_FromTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.TimeSpan FromTime { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ToTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.TimeSpan ToTime { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityTypeID { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ActivityDate { get; set; }	

  
    [Display(Name = "ActivityOfClass_Label_ActivityPlan")]	
    public virtual ActivityPlan ActivityPlan { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityType")]	
    public virtual ActivityType ActivityType { get; set; }	
  
    [Display(Name = "ActivityOfClass_Label_ActivityOfPupils")]	
    public virtual ICollection<ActivityOfPupil> ActivityOfPupils { get; set; }	
    }
}

