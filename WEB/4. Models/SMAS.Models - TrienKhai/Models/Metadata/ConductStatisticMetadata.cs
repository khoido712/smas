

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConductStatisticMetadata))]
public partial class ConductStatistic { }

public partial class ConductStatisticMetadata
    {
  
    [Display(Name = "ConductStatistic_Label_ConductStatisticID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductStatisticID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ProcessedDate")]	
    public Nullable<System.DateTime> ProcessedDate { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportCode { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SubCommitteeID")]	
    public Nullable<int> SubCommitteeID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SentToSupervisor")]	
    public Nullable<bool> SentToSupervisor { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ConductLevel01")]	
    public Nullable<int> ConductLevel01 { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ConductLevel02")]	
    public Nullable<int> ConductLevel02 { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ConductLevel03")]	
    public Nullable<int> ConductLevel03 { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_ConductLevel04")]	
    public Nullable<int> ConductLevel04 { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_PupilTotal")]	
    public Nullable<int> PupilTotal { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConductStatistic_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SubCommittee")]	
    public virtual SubCommittee SubCommittee { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ConductStatistic_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

