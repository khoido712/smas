

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EyeTestMetadata))]
public partial class EyeTest { }

public partial class EyeTestMetadata
    {
  
    [Display(Name = "EyeTest_Label_EyeTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EyeTestID { get; set; }	
  
    [Display(Name = "EyeTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "EyeTest_Label_REye")]
    public int REye { get; set; }	
  
    [Display(Name = "EyeTest_Label_LEye")]
    public int LEye { get; set; }	
  
    [Display(Name = "EyeTest_Label_RMyopic")]	
    public Nullable<decimal> RMyopic { get; set; }	
  
    [Display(Name = "EyeTest_Label_LMyopic")]	
    public Nullable<decimal> LMyopic { get; set; }	
  
    [Display(Name = "EyeTest_Label_RFarsightedness")]	
    public Nullable<decimal> RFarsightedness { get; set; }	
  
    [Display(Name = "EyeTest_Label_LFarsightedness")]	
    public Nullable<decimal> LFarsightedness { get; set; }	
  
    [Display(Name = "EyeTest_Label_RAstigmatism")]	
    public Nullable<decimal> RAstigmatism { get; set; }	
  
    [Display(Name = "EyeTest_Label_LAstigmatism")]	
    public Nullable<decimal> LAstigmatism { get; set; }	
  
    [Display(Name = "EyeTest_Label_Other")]
    [StringLength(1000,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Other { get; set; }	
  
    [Display(Name = "EyeTest_Label_IsTreatment")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsTreatment { get; set; }	
  
    [Display(Name = "EyeTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "EyeTest_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "EyeTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "EyeTest_Label_IsMyopic")]	
    public Nullable<bool> IsMyopic { get; set; }	
  
    [Display(Name = "EyeTest_Label_IsFarsightedness")]	
    public Nullable<bool> IsFarsightedness { get; set; }	
  
    [Display(Name = "EyeTest_Label_IsAstigmatism")]	
    public Nullable<bool> IsAstigmatism { get; set; }	
  
    [Display(Name = "EyeTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EyeTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EyeTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

