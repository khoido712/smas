

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeacherGradingMetadata))]
public partial class TeacherGrading { }

public partial class TeacherGradingMetadata
    {
  
    [Display(Name = "TeacherGrading_Label_TeacherGradingID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherGradingID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_SchoolFacultyID")]	
    public Nullable<int> SchoolFacultyID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_TeacherGradeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherGradeID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_GradingDate")]	
    public Nullable<System.DateTime> GradingDate { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeacherGrading_Label_TeacherGrade")]	
    public virtual TeacherGrade TeacherGrade { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "TeacherGrading_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

