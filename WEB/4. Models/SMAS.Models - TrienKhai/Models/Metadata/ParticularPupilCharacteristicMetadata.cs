

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ParticularPupilCharacteristicMetadata))]
public partial class ParticularPupilCharacteristic { }

public partial class ParticularPupilCharacteristicMetadata
    {
  
    [Display(Name = "ParticularPupilCharacteristic_Label_ParticularPupilCharacteristicID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParticularPupilCharacteristicID { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_ParticularPupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParticularPupilID { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_FamilyCharacteristic")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FamilyCharacteristic { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_PsychologyCharacteristic")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PsychologyCharacteristic { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_RealUpdatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime RealUpdatedDate { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_RecordedDate")]	
    public Nullable<System.DateTime> RecordedDate { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_UpdatedDate")]	
    public Nullable<System.DateTime> UpdatedDate { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ParticularPupilCharacteristic_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ParticularPupilCharacteristic_Label_ParticularPupil")]	
    public virtual ParticularPupil ParticularPupil { get; set; }	
    }
}

