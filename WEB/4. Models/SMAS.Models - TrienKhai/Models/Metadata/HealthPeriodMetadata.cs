

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HealthPeriodMetadata))]
public partial class HealthPeriod { }

public partial class HealthPeriodMetadata
    {
  
    [Display(Name = "HealthPeriod_Label_HealthPeriodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HealthPeriodID { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_Resolution")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_FromDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FromDate { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_ToDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ToDate { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "HealthPeriod_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "HealthPeriod_Label_MonitoringBooks")]	
    public virtual ICollection<MonitoringBook> MonitoringBooks { get; set; }	
    }
}

