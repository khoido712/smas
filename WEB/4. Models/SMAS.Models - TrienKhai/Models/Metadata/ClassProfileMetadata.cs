

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassProfileMetadata))]
public partial class ClassProfile { }

public partial class ClassProfileMetadata
    {
  
    [Display(Name = "ClassProfile_Label_ClassProfileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassProfileID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolSubsidiaryID")]	
    public Nullable<int> SchoolSubsidiaryID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_IsCombinedClass")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsCombinedClass { get; set; }	
  
    [Display(Name = "ClassProfile_Label_CombinedClassCode")]
    [StringLength(40,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CombinedClassCode { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SubCommitteeID")]	
    public Nullable<int> SubCommitteeID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_HeadTeacherID")]	
    public Nullable<int> HeadTeacherID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_DisplayName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisplayName { get; set; }	
  
    [Display(Name = "ClassProfile_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ClassProfile_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "ClassProfile_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "ClassProfile_Label_IsSpecializedClass")]	
    public Nullable<bool> IsSpecializedClass { get; set; }	
  
    [Display(Name = "ClassProfile_Label_FirstForeignLanguageID")]	
    public Nullable<int> FirstForeignLanguageID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SecondForeignLanguageID")]	
    public Nullable<int> SecondForeignLanguageID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ApprenticeshipGroupID")]	
    public Nullable<int> ApprenticeshipGroupID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_Section")]	
    public Nullable<int> Section { get; set; }	
  
    [Display(Name = "ClassProfile_Label_IsITClass")]	
    public Nullable<bool> IsITClass { get; set; }	
  
    [Display(Name = "ClassProfile_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassProfile_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassProfile_Label_Candidates")]	
    public virtual ICollection<Candidate> Candidates { get; set; }	
  
    [Display(Name = "ClassProfile_Label_MonitoringBooks")]	
    public virtual ICollection<MonitoringBook> MonitoringBooks { get; set; }	
  
    [Display(Name = "ClassProfile_Label_EatingGroupClasses")]	
    public virtual ICollection<EatingGroupClass> EatingGroupClasses { get; set; }	
  
    [Display(Name = "ClassProfile_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SubCommittee")]	
    public virtual SubCommittee SubCommittee { get; set; }	
  
    [Display(Name = "ClassProfile_Label_AddMenuForChildrens")]	
    public virtual ICollection<AddMenuForChildren> AddMenuForChildrens { get; set; }	
  
    [Display(Name = "ClassProfile_Label_NotEatingChildrens")]	
    public virtual ICollection<NotEatingChildren> NotEatingChildrens { get; set; }	  
  
    [Display(Name = "ClassProfile_Label_ActivityPlanClasses")]	
    public virtual ICollection<ActivityPlanClass> ActivityPlanClasses { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ApprenticeshipTrainings")]	
    public virtual ICollection<ApprenticeshipTraining> ApprenticeshipTrainings { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ApprenticeshipTrainingAbsences")]	
    public virtual ICollection<ApprenticeshipTrainingAbsence> ApprenticeshipTrainingAbsences { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassMovements")]	
    public virtual ICollection<ClassMovement> ClassMovements { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassMovements1")]	
    public virtual ICollection<ClassMovement> ClassMovements1 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ConductEvaluationDetails")]	
    public virtual ICollection<ConductEvaluationDetail> ConductEvaluationDetails { get; set; }	
  
    [Display(Name = "ClassProfile_Label_DevelopmentOfChildrens")]	
    public virtual ICollection<DevelopmentOfChildren> DevelopmentOfChildrens { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ExemptedSubjects")]	
    public virtual ICollection<ExemptedSubject> ExemptedSubjects { get; set; }	
  
    [Display(Name = "ClassProfile_Label_GoodChildrenTickets")]	
    public virtual ICollection<GoodChildrenTicket> GoodChildrenTickets { get; set; }	
  
    [Display(Name = "ClassProfile_Label_JudgeRecords")]	
    public virtual ICollection<JudgeRecord> JudgeRecords { get; set; }

    //[Display(Name = "ClassProfile_Label_M_MarkRecord")]
    //public virtual ICollection<M_MarkRecord> M_MarkRecord { get; set; }	
  
    [Display(Name = "ClassProfile_Label_MarkRecords")]	
    public virtual ICollection<MarkRecord> MarkRecords { get; set; }	
  
    [Display(Name = "ClassProfile_Label_MovementAcceptances")]	
    public virtual ICollection<MovementAcceptance> MovementAcceptances { get; set; }	
  
    [Display(Name = "ClassProfile_Label_MovementAcceptances1")]	
    public virtual ICollection<MovementAcceptance> MovementAcceptances1 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ParticularPupils")]	
    public virtual ICollection<ParticularPupil> ParticularPupils { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilAbsences")]	
    public virtual ICollection<PupilAbsence> PupilAbsences { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilDisciplines")]	
    public virtual ICollection<PupilDiscipline> PupilDisciplines { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilEmulations")]	
    public virtual ICollection<PupilEmulation> PupilEmulations { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilFaults")]	
    public virtual ICollection<PupilFault> PupilFaults { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilGraduations")]	
    public virtual ICollection<PupilGraduation> PupilGraduations { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilLeavingOffs")]	
    public virtual ICollection<PupilLeavingOff> PupilLeavingOffs { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilOfClasses")]	
    public virtual ICollection<PupilOfClass> PupilOfClasses { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilPraises")]	
    public virtual ICollection<PupilPraise> PupilPraises { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilProfiles")]	
    public virtual ICollection<PupilProfile> PupilProfiles { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilRankings")]	
    public virtual ICollection<PupilRanking> PupilRankings { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilRetestRegistrations")]	
    public virtual ICollection<PupilRetestRegistration> PupilRetestRegistrations { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolMovements")]	
    public virtual ICollection<SchoolMovement> SchoolMovements { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolMovements1")]	
    public virtual ICollection<SchoolMovement> SchoolMovements1 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SummedUpRecords")]	
    public virtual ICollection<SummedUpRecord> SummedUpRecords { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SummedUpRecordClasses")]	
    public virtual ICollection<SummedUpRecordClass> SummedUpRecordClasses { get; set; }	
  
    [Display(Name = "ClassProfile_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ClassProfile_Label_Calendars")]	
    public virtual ICollection<Calendar> Calendars { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassAssigments")]	
    public virtual ICollection<ClassAssigment> ClassAssigments { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassEmulations")]	
    public virtual ICollection<ClassEmulation> ClassEmulations { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassEmulationDetails")]	
    public virtual ICollection<ClassEmulationDetail> ClassEmulationDetails { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassForwardings")]	
    public virtual ICollection<ClassForwarding> ClassForwardings { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassForwardings1")]	
    public virtual ICollection<ClassForwarding> ClassForwardings1 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassForwardings2")]	
    public virtual ICollection<ClassForwarding> ClassForwardings2 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ClassProfile_Label_SchoolSubsidiary")]	
    public virtual SchoolSubsidiary SchoolSubsidiary { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassSubjects")]	
    public virtual ICollection<ClassSubject> ClassSubjects { get; set; }	
  
    [Display(Name = "ClassProfile_Label_ClassSupervisorAssignments")]	
    public virtual ICollection<ClassSupervisorAssignment> ClassSupervisorAssignments { get; set; }	
  
    [Display(Name = "ClassProfile_Label_HeadTeacherSubstitutions")]	
    public virtual ICollection<HeadTeacherSubstitution> HeadTeacherSubstitutions { get; set; }	
  
    [Display(Name = "ClassProfile_Label_LockedMarkDetails")]	
    public virtual ICollection<LockedMarkDetail> LockedMarkDetails { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PropertyOfClasses")]	
    public virtual ICollection<PropertyOfClass> PropertyOfClasses { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilRepeateds")]	
    public virtual ICollection<PupilRepeated> PupilRepeateds { get; set; }	
  
    [Display(Name = "ClassProfile_Label_PupilRepeateds1")]	
    public virtual ICollection<PupilRepeated> PupilRepeateds1 { get; set; }	
  
    [Display(Name = "ClassProfile_Label_TeachingAssignments")]	
    public virtual ICollection<TeachingAssignment> TeachingAssignments { get; set; }	
  
    [Display(Name = "ClassProfile_Label_TeachingRegistrations")]	
    public virtual ICollection<TeachingRegistration> TeachingRegistrations { get; set; }	
  
    [Display(Name = "ClassProfile_Label_TeachingTargetRegistrations")]	
    public virtual ICollection<TeachingTargetRegistration> TeachingTargetRegistrations { get; set; }	
    }
}

