

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityPlanMetadata))]
public partial class ActivityPlan { }

public partial class ActivityPlanMetadata
    {
  
    [Display(Name = "ActivityPlan_Label_ActivityPlanID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityPlanID { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_PlannedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime PlannedDate { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_ActivityPlanName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ActivityPlanName { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_FromDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FromDate { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_ToDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ToDate { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_CommentOfTeacher")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CommentOfTeacher { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ActivityPlan_Label_Activities")]	
    public virtual ICollection<Activity> Activities { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_ActivityPlanClasses")]	
    public virtual ICollection<ActivityPlanClass> ActivityPlanClasses { get; set; }	
  
    [Display(Name = "ActivityPlan_Label_ActivityOfClasses")]	
    public virtual ICollection<ActivityOfClass> ActivityOfClasses { get; set; }	
    }
}

