

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolSubsidiaryMetadata))]
public partial class SchoolSubsidiary { }

public partial class SchoolSubsidiaryMetadata
    {
  
    [Display(Name = "SchoolSubsidiary_Label_SchoolSubsidiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolSubsidiaryID { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_SubsidiaryCode")]
    [StringLength(120,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubsidiaryCode { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_SubsidiaryName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubsidiaryName { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_Address")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Address { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_Telephone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Telephone { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_Fax")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Fax { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_DistanceFromHQ")]	
    public Nullable<decimal> DistanceFromHQ { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_SquareArea")]	
    public Nullable<decimal> SquareArea { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SchoolSubsidiary_Label_ClassProfiles")]	
    public virtual ICollection<ClassProfile> ClassProfiles { get; set; }	
  
    [Display(Name = "SchoolSubsidiary_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

