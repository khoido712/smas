

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(FaultCriteriaMetadata))]
    public partial class FaultCriteria { }

    public partial class FaultCriteriaMetadata
    {

        [Display(Name = "FaultCriteria_Label_FaultCriteriaID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int FaultCriteriaID { get; set; }

        [Display(Name = "FaultCriteria_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [Display(Name = "FaultCriteria_Label_GroupID")]
        public Nullable<int> GroupID { get; set; }

        [Display(Name = "FaultCriteria_Label_PenalizedMark")]
        public Nullable<decimal> PenalizedMark { get; set; }

        [Display(Name = "FaultCriteria_Label_Absence")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Absence { get; set; }

        [Display(Name = "FaultCriteria_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }

        [Display(Name = "FaultCriteria_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "FaultCriteria_Label_IsActive")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }

        [Display(Name = "FaultCriteria_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "FaultCriteria_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "FaultCriteria_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }


        [Display(Name = "FaultCriteria_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolID { get; set; }

        [Display(Name = "FaultCriteria_Label_SchoolProfile")]
        public virtual SchoolProfile SchoolProfile { get; set; }

        [Display(Name = "FaultCriteria_Label_FaultGroup")]
        public virtual FaultGroup FaultGroup { get; set; }

        [Display(Name = "FaultCriteria_Label_PupilFaults")]
        public virtual ICollection<PupilFault> PupilFaults { get; set; }
    }
}

