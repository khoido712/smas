

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SummedUpRecordClassMetadata))]
public partial class SummedUpRecordClass { }

public partial class SummedUpRecordClassMetadata
    {
  
    [Display(Name = "SummedUpRecordClass_Label_SummedUpRecordClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SummedUpRecordClassID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_Status")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Status { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_NumberOfPupil")]	
    public Nullable<int> NumberOfPupil { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_Semester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Semester { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_PeriodID")]	
    public Nullable<int> PeriodID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Type { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SummedUpRecordClass_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_PeriodDeclaration")]	
    public virtual PeriodDeclaration PeriodDeclaration { get; set; }	
  
    [Display(Name = "SummedUpRecordClass_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

