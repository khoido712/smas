

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(GroupMenuMetadata))]
public partial class GroupMenu { }

public partial class GroupMenuMetadata
    {
  
    [Display(Name = "GroupMenu_Label_GroupMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupMenuID { get; set; }	
  
    [Display(Name = "GroupMenu_Label_GroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupID { get; set; }	
  
    [Display(Name = "GroupMenu_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "GroupMenu_Label_Permission")]	
    public Nullable<int> Permission { get; set; }	
  
    [Display(Name = "GroupMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "GroupMenu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "GroupMenu_Label_GroupCat")]	
    public virtual GroupCat GroupCat { get; set; }	
  
    [Display(Name = "GroupMenu_Label_Menu")]	
    public virtual Menu Menu { get; set; }	
    }
}

