

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ServiceMetadata))]
public partial class Service { }

public partial class ServiceMetadata
    {
  
    [Display(Name = "Service_Label_ServiceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ServiceID { get; set; }	
  
    [Display(Name = "Service_Label_ServiceCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(25,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceCode { get; set; }	
  
    [Display(Name = "Service_Label_ServiceName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceName { get; set; }	
  
    [Display(Name = "Service_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Service_Label_IsStatus")]	
    public Nullable<int> IsStatus { get; set; }	
  
    [Display(Name = "Service_Label_ServiceType")]	
    public Nullable<int> ServiceType { get; set; }	
  
    [Display(Name = "Service_Label_UnitPrice")]	
    public Nullable<decimal> UnitPrice { get; set; }	
  
    [Display(Name = "Service_Label_SchedularType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchedularType { get; set; }	
  
    [Display(Name = "Service_Label_MaxSms")]	
    public Nullable<int> MaxSMS { get; set; }	
  
    [Display(Name = "Service_Label_CreateTime")]	
    public Nullable<System.DateTime> CreateTime { get; set; }	
  
    [Display(Name = "Service_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	
  
    [Display(Name = "Service_Label_CreateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CreateUser { get; set; }	
  
    [Display(Name = "Service_Label_UpdateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string UpdateUser { get; set; }	

  
    [Display(Name = "Service_Label_Packages")]	
    public virtual ICollection<Package> Packages { get; set; }	
    }
}

