

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkOfTimeMetadata))]
public partial class M_MarkOfTime { }

public partial class M_MarkOfTimeMetadata
    {
  
    [Display(Name = "M_MarkOfTime_Label_MarkOfTimeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkOfTimeID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_CapacityID")]	
    public Nullable<byte> CapacityID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_AverageTime")]	
    public Nullable<decimal> AverageTime { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_MarkConfigByTimeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkConfigByTimeID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_MarkAllSubject")]
    [StringLength(640,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MarkAllSubject { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_RankID")]	
    public Nullable<int> RankID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_LevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LevelID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkOfTime_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

