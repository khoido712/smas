

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(FoodMineralMetadata))]
public partial class FoodMineral { }

public partial class FoodMineralMetadata
    {
  
    [Display(Name = "FoodMineral_Label_FoodMineralID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodMineralID { get; set; }	
  
    [Display(Name = "FoodMineral_Label_Weight")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal Weight { get; set; }	
  
    [Display(Name = "FoodMineral_Label_FoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodID { get; set; }	
  
    [Display(Name = "FoodMineral_Label_MinenalID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MinenalID { get; set; }	

  
    [Display(Name = "FoodMineral_Label_FoodCat")]	
    public virtual FoodCat FoodCat { get; set; }	
  
    [Display(Name = "FoodMineral_Label_MinenalCat")]	
    public virtual MinenalCat MinenalCat { get; set; }	
    }
}

