

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(AcademicYearMetadata))]
public partial class AcademicYear { }

public partial class AcademicYearMetadata
    {
  
    [Display(Name = "AcademicYear_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "AcademicYear_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "AcademicYear_Label_DisplayTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisplayTitle { get; set; }	
  
    [Display(Name = "AcademicYear_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "AcademicYear_Label_FirstSemesterStartDate")]	
    public Nullable<System.DateTime> FirstSemesterStartDate { get; set; }	
  
    [Display(Name = "AcademicYear_Label_FirstSemesterEndDate")]	
    public Nullable<System.DateTime> FirstSemesterEndDate { get; set; }	
  
    [Display(Name = "AcademicYear_Label_SecondSemesterStartDate")]	
    public Nullable<System.DateTime> SecondSemesterStartDate { get; set; }	
  
    [Display(Name = "AcademicYear_Label_SecondSemesterEndDate")]	
    public Nullable<System.DateTime> SecondSemesterEndDate { get; set; }	
  
    [Display(Name = "AcademicYear_Label_ConductEstimationType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductEstimationType { get; set; }	
  
    [Display(Name = "AcademicYear_Label_RankingCriteria")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RankingCriteria { get; set; }	
  
    [Display(Name = "AcademicYear_Label_IsSecondSemesterToSemesterAll")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsSecondSemesterToSemesterAll { get; set; }	
  
    [Display(Name = "AcademicYear_Label_IsTeacherCanViewAll")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsTeacherCanViewAll { get; set; }	
  
    [Display(Name = "AcademicYear_Label_IsHeadTeacherCanSendSMS")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsHeadTeacherCanSendSMS { get; set; }	
  
    [Display(Name = "AcademicYear_Label_IsSubjectTeacherCanSendSMS")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsSubjectTeacherCanSendSMS { get; set; }	
  
    [Display(Name = "AcademicYear_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "AcademicYear_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    
    }
}

