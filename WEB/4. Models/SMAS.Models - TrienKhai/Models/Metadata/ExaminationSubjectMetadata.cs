

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ExaminationSubjectMetadata))]
public partial class ExaminationSubject { }

public partial class ExaminationSubjectMetadata
    {
  
    [Display(Name = "ExaminationSubject_Label_ExaminationSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationSubjectID { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_ExaminationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationID { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_StartTime")]	
    public Nullable<System.DateTime> StartTime { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_DurationInMinute")]	
    public Nullable<int> DurationInMinute { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_OrderNumberPrefix")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string OrderNumberPrefix { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_IsLocked")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsLocked { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_HasDetachableHead")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool HasDetachableHead { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ExaminationSubject_Label_Candidates")]	
    public virtual ICollection<Candidate> Candidates { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_CandidateAbsences")]	
    public virtual ICollection<CandidateAbsence> CandidateAbsences { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_DetachableHeadBags")]	
    public virtual ICollection<DetachableHeadBag> DetachableHeadBags { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_ExaminationRooms")]	
    public virtual ICollection<ExaminationRoom> ExaminationRooms { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
  
    [Display(Name = "ExaminationSubject_Label_ViolatedInvigilators")]	
    public virtual ICollection<ViolatedInvigilator> ViolatedInvigilators { get; set; }	
    }
}

