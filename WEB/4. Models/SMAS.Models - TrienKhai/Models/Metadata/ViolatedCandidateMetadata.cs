

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ViolatedCandidateMetadata))]
public partial class ViolatedCandidate { }

public partial class ViolatedCandidateMetadata
    {
  
    [Display(Name = "ViolatedCandidate_Label_ViolatedCandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ViolatedCandidateID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ExaminationID")]	
    public Nullable<int> ExaminationID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_CandidateID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CandidateID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ExamViolationTypeID")]	
    public Nullable<int> ExamViolationTypeID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ViolationDetail")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ViolationDetail { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_PupilCode")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_RoomID")]	
    public Nullable<int> RoomID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_PupilID")]	
    public Nullable<int> PupilID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_NameListCode")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NameListCode { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ViolatedCandidate_Label_Candidate")]	
    public virtual Candidate Candidate { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ExaminationRoom")]	
    public virtual ExaminationRoom ExaminationRoom { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_ExamViolationType")]	
    public virtual ExamViolationType ExamViolationType { get; set; }	
  
    [Display(Name = "ViolatedCandidate_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
    }
}

