

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DentalTestMetadata))]
public partial class DentalTest { }

public partial class DentalTestMetadata
    {
  
    [Display(Name = "DentalTest_Label_DentalTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DentalTestID { get; set; }	
  
    [Display(Name = "DentalTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumTeethExtracted")]	
    public Nullable<int> NumTeethExtracted { get; set; }	
  
    [Display(Name = "DentalTest_Label_DescriptionTeethExtracted")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionTeethExtracted { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumDentalFilling")]
    public int NumDentalFilling { get; set; }	
  
    [Display(Name = "DentalTest_Label_DescriptionDentalFilling")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionDentalFilling { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumPreventiveDentalFilling")]	
    public Nullable<int> NumPreventiveDentalFilling { get; set; }	
  
    [Display(Name = "DentalTest_Label_DescriptionPreventiveDentalFilling")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DescriptionPreventiveDentalFilling { get; set; }	
  
    [Display(Name = "DentalTest_Label_IsScraptTeeth")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsScraptTeeth { get; set; }	
  
    [Display(Name = "DentalTest_Label_IsTreatment")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsTreatment { get; set; }	
  
    [Display(Name = "DentalTest_Label_Other")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Other { get; set; }	
  
    [Display(Name = "DentalTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "DentalTest_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "DentalTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumDentalFillingOne")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NumDentalFillingOne { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumDentalFillingTwo")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NumDentalFillingTwo { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumDentalFillingThree")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NumDentalFillingThree { get; set; }	
  
    [Display(Name = "DentalTest_Label_NumDentalFillingFour")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NumDentalFillingFour { get; set; }	
  
    [Display(Name = "DentalTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DentalTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DentalTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

