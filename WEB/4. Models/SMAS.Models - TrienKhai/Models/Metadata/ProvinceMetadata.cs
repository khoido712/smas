

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ProvinceMetadata))]
public partial class Province { }

public partial class ProvinceMetadata
    {
  
    [Display(Name = "Province_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "Province_Label_ProvinceName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ProvinceName { get; set; }	
  
    [Display(Name = "Province_Label_ProvinceCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ProvinceCode { get; set; }	
  
    [Display(Name = "Province_Label_ShortName")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ShortName { get; set; }	
  
    [Display(Name = "Province_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Province_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Province_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "Province_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "Province_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Province_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Province_Label_UserProvinces")]	
    public virtual ICollection<UserProvince> UserProvinces { get; set; }	
  
    [Display(Name = "Province_Label_Districts")]	
    public virtual ICollection<District> Districts { get; set; }	
  
    [Display(Name = "Province_Label_AcademicYearOfProvinces")]	
    public virtual ICollection<AcademicYearOfProvince> AcademicYearOfProvinces { get; set; }	
  
    [Display(Name = "Province_Label_CodeConfigs")]	
    public virtual ICollection<CodeConfig> CodeConfigs { get; set; }	
  
    [Display(Name = "Province_Label_EmployeeWorkMovements")]	
    public virtual ICollection<EmployeeWorkMovement> EmployeeWorkMovements { get; set; }	
  
    [Display(Name = "Province_Label_ProvinceSubjects")]	
    public virtual ICollection<ProvinceSubject> ProvinceSubjects { get; set; }	
  
    [Display(Name = "Province_Label_PupilProfiles")]	
    public virtual ICollection<PupilProfile> PupilProfiles { get; set; }	
  
    [Display(Name = "Province_Label_SchoolMovements")]	
    public virtual ICollection<SchoolMovement> SchoolMovements { get; set; }	
  
    [Display(Name = "Province_Label_SchoolProfiles")]	
    public virtual ICollection<SchoolProfile> SchoolProfiles { get; set; }	
  
    [Display(Name = "Province_Label_SupervisingDepts")]	
    public virtual ICollection<SupervisingDept> SupervisingDepts { get; set; }	
    }
}

