

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CalendarScheduleMetadata))]
public partial class CalendarSchedule { }

public partial class CalendarScheduleMetadata
    {
  
    [Display(Name = "CalendarSchedule_Label_CalendarScheduleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CalendarScheduleID { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_CalendarID")]	
    public Nullable<int> CalendarID { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_SubjectTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubjectTitle { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_Preparation")]
    [StringLength(1024,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Preparation { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_HomeWork")]
    [StringLength(1024,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HomeWork { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_ApplyDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ApplyDate { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "CalendarSchedule_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "CalendarSchedule_Label_Calendar")]	
    public virtual Calendar Calendar { get; set; }	
    }
}

