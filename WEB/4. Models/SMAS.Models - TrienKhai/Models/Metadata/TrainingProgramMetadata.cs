

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TrainingProgramMetadata))]
public partial class TrainingProgram { }

public partial class TrainingProgramMetadata
    {
  
    [Display(Name = "TrainingType_Label_TrainingTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    public int TrainingProgramID { get; set; }	
  
    [Display(Name = "TrainingType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "TrainingType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "TrainingType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
    }
}

