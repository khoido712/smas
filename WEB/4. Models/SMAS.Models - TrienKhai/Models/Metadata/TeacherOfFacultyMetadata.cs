

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeacherOfFacultyMetadata))]
public partial class TeacherOfFaculty { }

public partial class TeacherOfFacultyMetadata
    {
  
    [Display(Name = "TeacherOfFaculty_Label_TeacherOfFacultyID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherOfFacultyID { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_FacultyID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FacultyID { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_IsLeader")]	
    public Nullable<bool> IsLeader { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeacherOfFaculty_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "TeacherOfFaculty_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
    }
}

