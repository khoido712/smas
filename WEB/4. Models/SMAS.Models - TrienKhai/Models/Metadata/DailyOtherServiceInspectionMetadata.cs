

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DailyOtherServiceInspectionMetadata))]
public partial class DailyOtherServiceInspection { }

public partial class DailyOtherServiceInspectionMetadata
    {
  
    [Display(Name = "DailyOtherServiceInspection_Label_DailyOtherServiceInspectionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyOtherServiceInspectionID { get; set; }	
  
    [Display(Name = "DailyOtherServiceInspection_Label_Price")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Price { get; set; }	
  
    [Display(Name = "DailyOtherServiceInspection_Label_DishInspectionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishInspectionID { get; set; }	
  
    [Display(Name = "DailyOtherServiceInspection_Label_OtherServiceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OtherServiceID { get; set; }	
  
    [Display(Name = "DailyOtherServiceInspection_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DailyOtherServiceInspection_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DailyOtherServiceInspection_Label_OtherService")]	
    public virtual OtherService OtherService { get; set; }	
    }
}

