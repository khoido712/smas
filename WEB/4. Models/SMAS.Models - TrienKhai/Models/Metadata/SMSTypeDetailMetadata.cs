

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSTypeDetailMetadata))]
public partial class SMSTypeDetail { }

public partial class SMSTypeDetailMetadata
    {
  
    [Display(Name = "SMSTypeDetail_Label_SMSTypeDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSTypeDetailID { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_TypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeID { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_IsLock")]	
    public Nullable<bool> IsLock { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_IsAutoSend")]	
    public Nullable<bool> IsAutoSend { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_SendTime")]	
    public Nullable<System.DateTime> SendTime { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_Template")]
    [StringLength(1000,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Template { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_SendBefore")]	
    public Nullable<byte> SendBefore { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	

  
    [Display(Name = "SMSTypeDetail_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SMSTypeDetail_Label_SMSType")]	
    public virtual SMSType SMSType { get; set; }	
    }
}

