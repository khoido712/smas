

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkOfSemesterPrimaryMetadata))]
public partial class M_MarkOfSemesterPrimary { }

public partial class M_MarkOfSemesterPrimaryMetadata
    {
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_MarkOfSemesterPrimaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkOfSemesterPrimaryID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_Title")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_Mark")]	
    public Nullable<decimal> Mark { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_Semester")]	
    public Nullable<short> Semester { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_MarkType")]	
    public Nullable<byte> MarkType { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_PatternSMS")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

