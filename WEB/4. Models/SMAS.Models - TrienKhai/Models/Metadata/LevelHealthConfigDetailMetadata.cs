

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(LevelHealthConfigDetailMetadata))]
public partial class LevelHealthConfigDetail { }

public partial class LevelHealthConfigDetailMetadata
    {
  
    [Display(Name = "LevelHealthConfigDetail_Label_LevelHealthConfigDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LevelHealthConfigDetailID { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_Month")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Month { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_EvaluationValue")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal EvaluationValue { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_EvaluationConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationConfigID { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_LevelHealthConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LevelHealthConfigID { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "LevelHealthConfigDetail_Label_EvaluationConfig")]	
    public virtual EvaluationConfig EvaluationConfig { get; set; }	
  
    [Display(Name = "LevelHealthConfigDetail_Label_LevelHealthConfig")]	
    public virtual LevelHealthConfig LevelHealthConfig { get; set; }	
    }
}

