

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(AcademicYearOfProvinceMetadata))]
public partial class AcademicYearOfProvince { }

public partial class AcademicYearOfProvinceMetadata
    {
  
    [Display(Name = "AcademicYearOfProvince_Label_AcademicYearOfProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearOfProvinceID { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_DisplayTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisplayTitle { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_FirstSemesterStartDate")]	
    public Nullable<System.DateTime> FirstSemesterStartDate { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_FirstSemesterEndDate")]	
    public Nullable<System.DateTime> FirstSemesterEndDate { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_SecondSemesterStartDate")]	
    public Nullable<System.DateTime> SecondSemesterStartDate { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_SecondSemesterEndDate")]	
    public Nullable<System.DateTime> SecondSemesterEndDate { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_IsInheritData")]	
    public Nullable<bool> IsInheritData { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_IsSchoolEdit")]	
    public Nullable<bool> IsSchoolEdit { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "AcademicYearOfProvince_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "AcademicYearOfProvince_Label_ProvinceSubjects")]	
    public virtual ICollection<ProvinceSubject> ProvinceSubjects { get; set; }	
    }
}

