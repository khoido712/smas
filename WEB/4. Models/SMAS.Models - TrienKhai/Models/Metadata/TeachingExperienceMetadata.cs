

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeachingExperienceMetadata))]
public partial class TeachingExperience { }

public partial class TeachingExperienceMetadata
    {
  
    [Display(Name = "TeachingExperience_Label_TeachingExperienceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeachingExperienceID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_FacultyID")]	
    public Nullable<int> FacultyID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_ExperienceTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExperienceTypeID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_RegisteredLevel")]	
    public Nullable<int> RegisteredLevel { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_Grade")]
    [StringLength(2,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Grade { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_RegisteredDate")]	
    public Nullable<System.DateTime> RegisteredDate { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeachingExperience_Label_ExperienceType")]	
    public virtual ExperienceType ExperienceType { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "TeachingExperience_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

