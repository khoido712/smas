

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(WorkTypeMetadata))]
public partial class WorkType { }

public partial class WorkTypeMetadata
    {
  
    [Display(Name = "WorkType_Label_WorkTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WorkTypeID { get; set; }	
  
    [Display(Name = "WorkType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "WorkType_Label_WorkGroupTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WorkGroupTypeID { get; set; }	
  
    [Display(Name = "WorkType_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "WorkType_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "WorkType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "WorkType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "WorkType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "WorkType_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "WorkType_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "WorkType_Label_WorkGroupType")]	
    public virtual WorkGroupType WorkGroupType { get; set; }	
  
    [Display(Name = "WorkType_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
  
    [Display(Name = "WorkType_Label_EmployeeWorkTypes")]	
    public virtual ICollection<EmployeeWorkType> EmployeeWorkTypes { get; set; }	
  
    [Display(Name = "WorkType_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

