

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ReportDefinitionMetadata))]
public partial class ReportDefinition { }

public partial class ReportDefinitionMetadata
    {
  
    [Display(Name = "ReportDefinition_Label_ReportDefinitionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ReportDefinitionID { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportCode { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_ReportCategoryID")]	
    public Nullable<int> ReportCategoryID { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_TemplateName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TemplateName { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_TemplateDirectory")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TemplateDirectory { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_OutputNamePattern")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string OutputNamePattern { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_OutputFormat")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string OutputFormat { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_OutputMimeType")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string OutputMimeType { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_RenderingHandler")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RenderingHandler { get; set; }	
  
    [Display(Name = "ReportDefinition_Label_IsPreprocessed")]	
    public Nullable<bool> IsPreprocessed { get; set; }	

  
    [Display(Name = "ReportDefinition_Label_ReportCategory")]	
    public virtual ReportCategory ReportCategory { get; set; }	
    }
}

