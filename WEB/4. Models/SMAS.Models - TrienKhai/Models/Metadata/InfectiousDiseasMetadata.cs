

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(InfectiousDiseasMetadata))]
public partial class InfectiousDiseas { }

public partial class InfectiousDiseasMetadata
    {
  
    [Display(Name = "InfectiousDiseas_Label_InfectiousDiseasesID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InfectiousDiseasesID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_DiseasesTypeID")]	
    public Nullable<int> DiseasesTypeID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_InfectionDate")]	
    public Nullable<System.DateTime> InfectionDate { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_OffDate")]	
    public Nullable<System.DateTime> OffDate { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_Address")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Address { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_CommuneID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CommuneID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_DiseasesName")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DiseasesName { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_Note")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "InfectiousDiseas_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

    }
}

