

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ApprenticeshipTrainingAbsenceMetadata))]
public partial class ApprenticeshipTrainingAbsence { }

public partial class ApprenticeshipTrainingAbsenceMetadata
    {
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_ApprenticeshipTrainingAbsenceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipTrainingAbsenceID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_PupilCode")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_SectionInDay")]	
    public Nullable<int> SectionInDay { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_AbsentDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime AbsentDate { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_IsAccepted")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsAccepted { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_ApprenticeshipClassID")]	
    public Nullable<int> ApprenticeshipClassID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_ApprenticeshipClass")]	
    public virtual ApprenticeshipClass ApprenticeshipClass { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingAbsence_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

