

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(StatisticsConfigMetadata))]
public partial class StatisticsConfig { }

public partial class StatisticsConfigMetadata
    {
  
    [Display(Name = "StatisticsConfig_Label_StatisticsConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int StatisticsConfigID { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_MarkType")]	
    public Nullable<int> MarkType { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_MappedColumn")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MappedColumn { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_DisplayName")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisplayName { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_MinValue")]	
    public Nullable<decimal> MinValue { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_MaxValue")]	
    public Nullable<decimal> MaxValue { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_EqualValue")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EqualValue { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_DisplayOrder")]	
    public Nullable<int> DisplayOrder { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_ReportType")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportType { get; set; }	
  
    [Display(Name = "StatisticsConfig_Label_SubjectType")]	
    public Nullable<int> SubjectType { get; set; }	
    }
}

