

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkRecordTertiaryMetadata))]
public partial class M_MarkRecordTertiary { }

public partial class M_MarkRecordTertiaryMetadata
    {
  
    [Display(Name = "M_MarkRecordTertiary_Label_MarkRecordID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkRecordID { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M11")]	
    public Nullable<decimal> M11 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M12")]	
    public Nullable<decimal> M12 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M13")]	
    public Nullable<decimal> M13 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M14")]	
    public Nullable<decimal> M14 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M15")]	
    public Nullable<decimal> M15 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P11")]	
    public Nullable<decimal> P11 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P12")]	
    public Nullable<decimal> P12 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P13")]	
    public Nullable<decimal> P13 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P14")]	
    public Nullable<decimal> P14 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P15")]	
    public Nullable<decimal> P15 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V11")]	
    public Nullable<decimal> V11 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V12")]	
    public Nullable<decimal> V12 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V13")]	
    public Nullable<decimal> V13 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V14")]	
    public Nullable<decimal> V14 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V15")]	
    public Nullable<decimal> V15 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_SemesterTest1")]	
    public Nullable<decimal> SemesterTest1 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_AvgSemester1")]	
    public Nullable<decimal> AvgSemester1 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M21")]	
    public Nullable<decimal> M21 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M22")]	
    public Nullable<decimal> M22 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M23")]	
    public Nullable<decimal> M23 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M24")]	
    public Nullable<decimal> M24 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M25")]	
    public Nullable<decimal> M25 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P21")]	
    public Nullable<decimal> P21 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P22")]	
    public Nullable<decimal> P22 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P23")]	
    public Nullable<decimal> P23 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P24")]	
    public Nullable<decimal> P24 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_P25")]	
    public Nullable<decimal> P25 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V21")]	
    public Nullable<decimal> V21 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V22")]	
    public Nullable<decimal> V22 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V23")]	
    public Nullable<decimal> V23 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V24")]	
    public Nullable<decimal> V24 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V25")]	
    public Nullable<decimal> V25 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_SemesterTest2")]	
    public Nullable<decimal> SemesterTest2 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_AvgSemester2")]	
    public Nullable<decimal> AvgSemester2 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_AvgOfYear")]	
    public Nullable<decimal> AvgOfYear { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_TestAgain")]	
    public Nullable<decimal> TestAgain { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_CapacityOfYear")]	
    public Nullable<int> CapacityOfYear { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_CapacityOfSemester1")]	
    public Nullable<int> CapacityOfSemester1 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_CapacityOfSemester2")]	
    public Nullable<int> CapacityOfSemester2 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V110")]	
    public Nullable<decimal> V110 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V16")]	
    public Nullable<decimal> V16 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V17")]	
    public Nullable<decimal> V17 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V18")]	
    public Nullable<decimal> V18 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V19")]	
    public Nullable<decimal> V19 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V210")]	
    public Nullable<decimal> V210 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V26")]	
    public Nullable<decimal> V26 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V27")]	
    public Nullable<decimal> V27 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V28")]	
    public Nullable<decimal> V28 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_V29")]	
    public Nullable<decimal> V29 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_Note1")]
    [StringLength(512,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note1 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_Note2")]
    [StringLength(512,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note2 { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_PatternSMS")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_PatternDay")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternDay { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_PatternMonth")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternMonth { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_PatternWeek")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternWeek { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_Partition_SubjectID")]	
    public Nullable<int> Partition_SubjectID { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkRecordTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

