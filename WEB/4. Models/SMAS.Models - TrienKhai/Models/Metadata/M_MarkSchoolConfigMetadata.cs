

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkSchoolConfigMetadata))]
public partial class M_MarkSchoolConfig { }

public partial class M_MarkSchoolConfigMetadata
    {
  
    [Display(Name = "M_MarkSchoolConfig_Label_MarkSchoolConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkSchoolConfigID { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_MarkTypeID")]	
    public Nullable<int> MarkTypeID { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_Coefficient")]	
    public Nullable<int> Coefficient { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_MarkNumber")]	
    public Nullable<int> MarkNumber { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_DateUse")]	
    public Nullable<System.DateTime> DateUse { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_CreateDate")]	
    public Nullable<System.DateTime> CreateDate { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkSchoolConfig_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

