

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConcurrentWorkTypeMetadata))]
public partial class ConcurrentWorkType { }

public partial class ConcurrentWorkTypeMetadata
    {
  
    [Display(Name = "ConcurrentWorkType_Label_ConcurrentWorkTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConcurrentWorkTypeID { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_SectionPerWeek")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SectionPerWeek { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_Resolution")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConcurrentWorkType_Label_ConcurrentWorkAssignments")]	
    public virtual ICollection<ConcurrentWorkAssignment> ConcurrentWorkAssignments { get; set; }	
  
    [Display(Name = "ConcurrentWorkType_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

