

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_SuperUserMetadata))]
public partial class M_SuperUser { }

public partial class M_SuperUserMetadata
    {
  
    [Display(Name = "M_SuperUser_Label_SuperUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SuperUserID { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_FullName")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FullName { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_BirthDay")]	
    public Nullable<System.DateTime> BirthDay { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_Sex")]	
    public Nullable<bool> Sex { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_NativeVillage")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string NativeVillage { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_Address")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Address { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_Phone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Phone { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_Email")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Email { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_UnitID")]	
    public Nullable<int> UnitID { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_EmployeeID")]	
    public Nullable<int> EmployeeID { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_SuperUser_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

