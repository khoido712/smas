

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeSalaryMetadata))]
public partial class EmployeeSalary { }

public partial class EmployeeSalaryMetadata
    {
  
    [Display(Name = "EmployeeSalary_Label_EmployeeSalaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeSalaryID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_EmployeeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_EmployeeScaleID")]	
    public Nullable<int> EmployeeScaleID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SalaryLevelID")]	
    public Nullable<int> SalaryLevelID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SupervisingDeptID")]	
    public Nullable<int> SupervisingDeptID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_AppliedDate")]	
    public Nullable<System.DateTime> AppliedDate { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SalaryResolution")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SalaryResolution { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SalaryAmount")]	
    public Nullable<int> SalaryAmount { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeeSalary_Label_EmployeeScale")]	
    public virtual EmployeeScale EmployeeScale { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SalaryLevel")]	
    public virtual SalaryLevel SalaryLevel { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EmployeeSalary_Label_SupervisingDept")]	
    public virtual SupervisingDept SupervisingDept { get; set; }	
    }
}

