

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ApprenticeshipGroupMetadata))]
public partial class ApprenticeshipGroup { }

public partial class ApprenticeshipGroupMetadata
    {
  
    [Display(Name = "ApprenticeshipGroup_Label_ApprenticeshipGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipGroupID { get; set; }	
  
    [Display(Name = "ApprenticeshipGroup_Label_GroupName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string GroupName { get; set; }	
  
    [Display(Name = "ApprenticeshipGroup_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ApprenticeshipGroup_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "ApprenticeshipGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ApprenticeshipGroup_Label_SubjectCats")]	
    public virtual ICollection<SubjectCat> SubjectCats { get; set; }	
    }
}

