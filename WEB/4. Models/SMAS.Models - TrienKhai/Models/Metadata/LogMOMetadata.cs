

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(LogMOMetadata))]
public partial class LogMO { }

public partial class LogMOMetadata
    {
  
    [Display(Name = "LogMO_Label_LogMOID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LogMOID { get; set; }	
  
    [Display(Name = "LogMO_Label_ServiceID")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceID { get; set; }	
  
    [Display(Name = "LogMO_Label_CPCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CPCode { get; set; }	
  
    [Display(Name = "LogMO_Label_RequestID")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RequestID { get; set; }	
  
    [Display(Name = "LogMO_Label_Commandcode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Commandcode { get; set; }	
  
    [Display(Name = "LogMO_Label_SenderNumber")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SenderNumber { get; set; }	
  
    [Display(Name = "LogMO_Label_ReceiverNumber")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReceiverNumber { get; set; }	
  
    [Display(Name = "LogMO_Label_Content")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Content { get; set; }	
  
    [Display(Name = "LogMO_Label_Status")]	
    public Nullable<bool> Status { get; set; }	
  
    [Display(Name = "LogMO_Label_ReceiveTime")]	
    public Nullable<System.DateTime> ReceiveTime { get; set; }	
  
    [Display(Name = "LogMO_Label_CreateDate")]	
    public Nullable<System.DateTime> CreateDate { get; set; }	

  
    [Display(Name = "LogMO_Label_MOErrors")]	
    public virtual ICollection<MOError> MOErrors { get; set; }	
  
    [Display(Name = "LogMO_Label_SMSCommunications")]	
    public virtual ICollection<SMSCommunication> SMSCommunications { get; set; }	
  
    [Display(Name = "LogMO_Label_SMSConfirmations")]	
    public virtual ICollection<SMSConfirmation> SMSConfirmations { get; set; }	
    }
}

