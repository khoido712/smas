

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MonitoringBookMetadata))]
public partial class MonitoringBook { }

public partial class MonitoringBookMetadata
    {
  
    [Display(Name = "MonitoringBook_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_MonitoringDate")]	
    public Nullable<System.DateTime> MonitoringDate { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_Symptoms")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Symptoms { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_Solution")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Solution { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_UnitName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string UnitName { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_MonitoringType")]	
    public Nullable<int> MonitoringType { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_HealthPeriodID")]	
    public Nullable<int> HealthPeriodID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "MonitoringBook_Label_IsSMS")]	
    public Nullable<bool> IsSMS { get; set; }	

  
    
    }
}

