

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(LockedMarkDetailMetadata))]
public partial class LockedMarkDetail { }

public partial class LockedMarkDetailMetadata
    {
  
    [Display(Name = "LockedMarkDetail_Label_LockedMarkDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LockedMarkDetailID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_MarkIndex")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkIndex { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_LockedDate")]	
    public Nullable<System.DateTime> LockedDate { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_UnlockedDate")]	
    public Nullable<System.DateTime> UnlockedDate { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_MarkTypeID")]	
    public Nullable<int> MarkTypeID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "LockedMarkDetail_Label_MarkType")]	
    public virtual MarkType MarkType { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "LockedMarkDetail_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

