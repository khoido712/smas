

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ViolatedInvigilatorMetadata))]
public partial class ViolatedInvigilator { }

public partial class ViolatedInvigilatorMetadata
    {
  
    [Display(Name = "ViolatedInvigilator_Label_ViolatedInvigilatorID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ViolatedInvigilatorID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_ExaminationID")]	
    public Nullable<int> ExaminationID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_InvigilatorID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InvigilatorID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_TeacherID")]	
    public Nullable<int> TeacherID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_RoomID")]	
    public Nullable<int> RoomID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_DisciplinedForm")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisciplinedForm { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_ViolationDetail")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ViolationDetail { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ViolatedInvigilator_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_ExaminationRoom")]	
    public virtual ExaminationRoom ExaminationRoom { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_Invigilator")]	
    public virtual Invigilator Invigilator { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ViolatedInvigilator_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
    }
}

