

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConductConfigByViolationMetadata))]
public partial class ConductConfigByViolation { }

public partial class ConductConfigByViolationMetadata
    {
  
    [Display(Name = "ConductConfigByViolation_Label_ConductConfigByViolationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductConfigByViolationID { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_ConductLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductLevelID { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_MinValue")]	
    public Nullable<decimal> MinValue { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_MaxValue")]	
    public Nullable<decimal> MaxValue { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConductConfigByViolation_Label_ConductLevel")]	
    public virtual ConductLevel ConductLevel { get; set; }	
  
    [Display(Name = "ConductConfigByViolation_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

