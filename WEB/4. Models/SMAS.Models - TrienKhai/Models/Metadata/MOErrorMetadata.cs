

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MOErrorMetadata))]
public partial class MOError { }

public partial class MOErrorMetadata
    {
  
    [Display(Name = "MOError_Label_MOErrorID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MOErrorID { get; set; }	
  
    [Display(Name = "MOError_Label_LogMOID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LogMOID { get; set; }	
  
    [Display(Name = "MOError_Label_ErrorCode")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ErrorCode { get; set; }	
  
    [Display(Name = "MOError_Label_ErrorMessage")]
    [StringLength(1024,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ErrorMessage { get; set; }	
  
    [Display(Name = "MOError_Label_ReplyContent")]
    [StringLength(1024,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReplyContent { get; set; }	
  
    [Display(Name = "MOError_Label_CreateDate")]	
    public Nullable<System.DateTime> CreateDate { get; set; }	

  
    [Display(Name = "MOError_Label_LogMO")]	
    public virtual LogMO LogMO { get; set; }	
    }
}

