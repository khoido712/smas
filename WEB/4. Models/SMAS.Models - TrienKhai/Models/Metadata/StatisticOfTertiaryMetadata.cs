

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(StatisticOfTertiaryMetadata))]
public partial class StatisticOfTertiary { }

public partial class StatisticOfTertiaryMetadata
    {
  
    [Display(Name = "StatisticOfTertiary_Label_StatisticOfTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int StatisticOfTertiaryID { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_PupilFromSecondary")]	
    public Nullable<int> PupilFromSecondary { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_PupilOfOther")]	
    public Nullable<int> PupilOfOther { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_ComputerRoom")]	
    public Nullable<int> ComputerRoom { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumberOfComputer")]	
    public Nullable<int> NumberOfComputer { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumberOfComputerInternetConnected")]	
    public Nullable<int> NumberOfComputerInternetConnected { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_HasWebsite")]	
    public Nullable<bool> HasWebsite { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_HasDatabase")]	
    public Nullable<bool> HasDatabase { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_LearningRoom")]	
    public Nullable<int> LearningRoom { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumOfStandard")]	
    public Nullable<int> NumOfStandard { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumOfOverStandard")]	
    public Nullable<int> NumOfOverStandard { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumOfUnderStandard")]	
    public Nullable<int> NumOfUnderStandard { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_NumOfStaff")]	
    public Nullable<int> NumOfStaff { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "StatisticOfTertiary_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "StatisticOfTertiary_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

