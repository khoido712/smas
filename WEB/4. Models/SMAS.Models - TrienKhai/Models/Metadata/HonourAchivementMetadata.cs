

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HonourAchivementMetadata))]
public partial class HonourAchivement { }

public partial class HonourAchivementMetadata
    {
  
    [Display(Name = "HonourAchivement_Label_HonourAchivementID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HonourAchivementID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_HonourAchivementTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HonourAchivementTypeID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_AchivedDate")]	
    public Nullable<System.DateTime> AchivedDate { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Type { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_EmployeeID")]	
    public Nullable<int> EmployeeID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_FacultyID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FacultyID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "HonourAchivement_Label_HonourAchivementType")]	
    public virtual HonourAchivementType HonourAchivementType { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "HonourAchivement_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

