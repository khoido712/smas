

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_CategoryOfPupilSecondaryMetadata))]
public partial class M_CategoryOfPupilSecondary { }

public partial class M_CategoryOfPupilSecondaryMetadata
    {
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_CategoryOfPupilSecondaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CategoryOfPupilSecondaryID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_CapacityID")]	
    public Nullable<int> CapacityID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_ConductID")]	
    public Nullable<int> ConductID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_AbsentTotalDays")]	
    public Nullable<int> AbsentTotalDays { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_Category")]	
    public Nullable<byte> Category { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_IsReviewed")]	
    public Nullable<bool> IsReviewed { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_CapacityNotRetest")]	
    public Nullable<byte> CapacityNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_ConductNotRetest")]	
    public Nullable<byte> ConductNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_CategoryNotRetest")]	
    public Nullable<byte> CategoryNotRetest { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilSecondary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

