

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_PupilConductPrimaryMetadata))]
public partial class M_PupilConductPrimary { }

public partial class M_PupilConductPrimaryMetadata
    {
  
    [Display(Name = "M_PupilConductPrimary_Label_PupilConductTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilConductTertiaryID { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M11")]	
    public Nullable<int> M11 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M21")]	
    public Nullable<int> M21 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M12")]	
    public Nullable<int> M12 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M22")]	
    public Nullable<int> M22 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M13")]	
    public Nullable<int> M13 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M23")]	
    public Nullable<int> M23 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M14")]	
    public Nullable<int> M14 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M24")]	
    public Nullable<int> M24 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M15")]	
    public Nullable<int> M15 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M25")]	
    public Nullable<int> M25 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_Semester1")]	
    public Nullable<int> Semester1 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_Semester2")]	
    public Nullable<int> Semester2 { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_MustRetest")]	
    public Nullable<bool> MustRetest { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_ReTest")]	
    public Nullable<int> ReTest { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_PupilConductPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

