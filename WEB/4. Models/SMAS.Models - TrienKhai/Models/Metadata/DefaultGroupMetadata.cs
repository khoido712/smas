

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DefaultGroupMetadata))]
public partial class DefaultGroup { }

public partial class DefaultGroupMetadata
    {
  
    [Display(Name = "DefaultGroup_Label_DefaultGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DefaultGroupID { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_RoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleID { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_DefaultGroupName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DefaultGroupName { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "DefaultGroup_Label_Role")]	
    public virtual Role Role { get; set; }	
  
    [Display(Name = "DefaultGroup_Label_DefaultGroupMenus")]	
    public virtual ICollection<DefaultGroupMenu> DefaultGroupMenus { get; set; }	
    }
}

