

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(aspnet_ApplicationsMetadata))]
public partial class aspnet_Applications { }

public partial class aspnet_ApplicationsMetadata
    {
  
    [Display(Name = "aspnet_Applications_Label_ApplicationName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ApplicationName { get; set; }	
  
    [Display(Name = "aspnet_Applications_Label_LoweredApplicationName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string LoweredApplicationName { get; set; }	
  
    [Display(Name = "aspnet_Applications_Label_ApplicationId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid ApplicationId { get; set; }	
  
    [Display(Name = "aspnet_Applications_Label_Description")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	

  
    [Display(Name = "aspnet_Applications_Label_aspnet_Membership")]	
    public virtual ICollection<aspnet_Membership> aspnet_Membership { get; set; }	
  
    [Display(Name = "aspnet_Applications_Label_aspnet_Users")]	
    public virtual ICollection<aspnet_Users> aspnet_Users { get; set; }	
    }
}

