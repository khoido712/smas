

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HabitDetailMetadata))]
public partial class HabitDetail { }

public partial class HabitDetailMetadata
    {
  
    [Display(Name = "HabitDetail_Label_HabitDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HabitDetailID { get; set; }	
  
    [Display(Name = "HabitDetail_Label_HabitDetailName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HabitDetailName { get; set; }	
  
    [Display(Name = "HabitDetail_Label_HabitGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HabitGroupID { get; set; }	
  
    [Display(Name = "HabitDetail_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "HabitDetail_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "HabitDetail_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "HabitDetail_Label_HabitGroup")]	
    public virtual HabitGroup HabitGroup { get; set; }	
  
    [Display(Name = "HabitDetail_Label_HabitOfChildrens")]	
    public virtual ICollection<HabitOfChildren> HabitOfChildrens { get; set; }	
    }
}

