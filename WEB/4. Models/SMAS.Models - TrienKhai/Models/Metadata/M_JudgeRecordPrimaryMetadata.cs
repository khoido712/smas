

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_JudgeRecordPrimaryMetadata))]
public partial class M_JudgeRecordPrimary { }

public partial class M_JudgeRecordPrimaryMetadata
    {
  
    [Display(Name = "M_JudgeRecordPrimary_Label_JudgeRecordPrimaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int JudgeRecordPrimaryID { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J11")]	
    public Nullable<int> J11 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J12")]	
    public Nullable<int> J12 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J13")]	
    public Nullable<int> J13 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J14")]	
    public Nullable<int> J14 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_Semester1")]	
    public Nullable<int> Semester1 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J21")]	
    public Nullable<int> J21 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J22")]	
    public Nullable<int> J22 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J23")]	
    public Nullable<int> J23 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J24")]	
    public Nullable<int> J24 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_Semester2")]	
    public Nullable<int> Semester2 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_MustRetest")]	
    public Nullable<bool> MustRetest { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_AfterRetest")]	
    public Nullable<byte> AfterRetest { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_SubjectID")]	
    public Nullable<int> SubjectID { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_Emulation")]	
    public Nullable<byte> Emulation { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J15")]	
    public Nullable<int> J15 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_J25")]	
    public Nullable<int> J25 { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_Partition_SubjectID")]	
    public Nullable<int> Partition_SubjectID { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_JudgeRecordPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

