

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MonthlyBalanceMetadata))]
public partial class MonthlyBalance { }

public partial class MonthlyBalanceMetadata
    {
  
    [Display(Name = "MonthlyBalance_Label_MonthlyBalanceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonthlyBalanceID { get; set; }	
  
    [Display(Name = "MonthlyBalance_Label_BalanceMonth")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime BalanceMonth { get; set; }	
  
    [Display(Name = "MonthlyBalance_Label_Balance")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Balance { get; set; }	
  
    [Display(Name = "MonthlyBalance_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "MonthlyBalance_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "MonthlyBalance_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "MonthlyBalance_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

