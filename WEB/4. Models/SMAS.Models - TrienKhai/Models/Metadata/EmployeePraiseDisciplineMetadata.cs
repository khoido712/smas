

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeePraiseDisciplineMetadata))]
public partial class EmployeePraiseDiscipline { }

public partial class EmployeePraiseDisciplineMetadata
    {
  
    [Display(Name = "EmployeePraiseDiscipline_Label_EmployeePraiseDisciplineID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeePraiseDisciplineID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_EmployeeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_SchoolFacultyID")]	
    public Nullable<int> SchoolFacultyID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_IsDiscipline")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsDiscipline { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_Resolution")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_DetailContent")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DetailContent { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_Form")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Form { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_Description")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeePraiseDiscipline_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeePraiseDiscipline_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
    }
}

