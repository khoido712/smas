

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSTypeMetadata))]
public partial class SMSType { }

public partial class SMSTypeMetadata
    {
  
    [Display(Name = "SMSType_Label_TypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeID { get; set; }	
  
    [Display(Name = "SMSType_Label_Name")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Name { get; set; }	
  
    [Display(Name = "SMSType_Label_Description")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SMSType_Label_DefaultTemplate")]
    [StringLength(1000,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DefaultTemplate { get; set; }	
  
    [Display(Name = "SMSType_Label_TypeCode")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TypeCode { get; set; }	
  
    [Display(Name = "SMSType_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte Type { get; set; }	
  
    [Display(Name = "SMSType_Label_Periodic")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Periodic { get; set; }	

  
    [Display(Name = "SMSType_Label_SMSTypeDetails")]	
    public virtual ICollection<SMSTypeDetail> SMSTypeDetails { get; set; }	
    }
}

