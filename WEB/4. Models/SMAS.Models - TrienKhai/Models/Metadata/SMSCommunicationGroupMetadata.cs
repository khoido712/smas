

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSCommunicationGroupMetadata))]
public partial class SMSCommunicationGroup { }

public partial class SMSCommunicationGroupMetadata
    {
  
    [Display(Name = "SMSCommunicationGroup_Label_SMSCommunicationGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSCommunicationGroupID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_SMSCommunicationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSCommunicationID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_ContactGroupID")]	
    public Nullable<int> ContactGroupID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_TeacherID")]	
    public Nullable<int> TeacherID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_PupilProfileID")]	
    public Nullable<int> PupilProfileID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_IsAdmin")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsAdmin { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte Type { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_UpdateDate")]	
    public Nullable<System.DateTime> UpdateDate { get; set; }	

  
    [Display(Name = "SMSCommunicationGroup_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_ContactGroup")]	
    public virtual ContactGroup ContactGroup { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SMSCommunicationGroup_Label_SMSCommunication")]	
    public virtual SMSCommunication SMSCommunication { get; set; }	
    }
}

