

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MarkStatisticMetadata))]
public partial class MarkStatistic { }

public partial class MarkStatisticMetadata
    {
  
    [Display(Name = "MarkStatistic_Label_MarkStatisticID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkStatisticID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_ProcessedDate")]	
    public Nullable<System.DateTime> ProcessedDate { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportCode { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_AcademicYearID")]	
    public Nullable<int> AcademicYearID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SubCommitteeID")]	
    public Nullable<int> SubCommitteeID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SentToSupervisor")]	
    public Nullable<bool> SentToSupervisor { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel00")]	
    public Nullable<int> MarkLevel00 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel01")]	
    public Nullable<int> MarkLevel01 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel02")]	
    public Nullable<int> MarkLevel02 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel03")]	
    public Nullable<int> MarkLevel03 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel04")]	
    public Nullable<int> MarkLevel04 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel05")]	
    public Nullable<int> MarkLevel05 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel06")]	
    public Nullable<int> MarkLevel06 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel07")]	
    public Nullable<int> MarkLevel07 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel08")]	
    public Nullable<int> MarkLevel08 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel09")]	
    public Nullable<int> MarkLevel09 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel10")]	
    public Nullable<int> MarkLevel10 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel11")]	
    public Nullable<int> MarkLevel11 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel12")]	
    public Nullable<int> MarkLevel12 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel13")]	
    public Nullable<int> MarkLevel13 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel14")]	
    public Nullable<int> MarkLevel14 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel15")]	
    public Nullable<int> MarkLevel15 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel16")]	
    public Nullable<int> MarkLevel16 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel17")]	
    public Nullable<int> MarkLevel17 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel18")]	
    public Nullable<int> MarkLevel18 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel19")]	
    public Nullable<int> MarkLevel19 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel20")]	
    public Nullable<int> MarkLevel20 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel21")]	
    public Nullable<int> MarkLevel21 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel22")]	
    public Nullable<int> MarkLevel22 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel23")]	
    public Nullable<int> MarkLevel23 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel24")]	
    public Nullable<int> MarkLevel24 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel25")]	
    public Nullable<int> MarkLevel25 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel26")]	
    public Nullable<int> MarkLevel26 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel27")]	
    public Nullable<int> MarkLevel27 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel28")]	
    public Nullable<int> MarkLevel28 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel29")]	
    public Nullable<int> MarkLevel29 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel30")]	
    public Nullable<int> MarkLevel30 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel31")]	
    public Nullable<int> MarkLevel31 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel32")]	
    public Nullable<int> MarkLevel32 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel33")]	
    public Nullable<int> MarkLevel33 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel34")]	
    public Nullable<int> MarkLevel34 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel35")]	
    public Nullable<int> MarkLevel35 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel36")]	
    public Nullable<int> MarkLevel36 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel37")]	
    public Nullable<int> MarkLevel37 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel38")]	
    public Nullable<int> MarkLevel38 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel39")]	
    public Nullable<int> MarkLevel39 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkLevel40")]	
    public Nullable<int> MarkLevel40 { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_MarkTotal")]	
    public Nullable<int> MarkTotal { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_BelowAverage")]	
    public Nullable<int> BelowAverage { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_OnAverage")]	
    public Nullable<int> OnAverage { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "MarkStatistic_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SubCommittee")]	
    public virtual SubCommittee SubCommittee { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "MarkStatistic_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

