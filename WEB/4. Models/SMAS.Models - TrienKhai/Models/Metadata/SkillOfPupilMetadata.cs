

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SkillOfPupilMetadata))]
public partial class SkillOfPupil { }

public partial class SkillOfPupilMetadata
    {
  
    [Display(Name = "SkillOfPupil_Label_SkillOfPupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillOfPupilID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_SkillOfClassID")]	
    public Nullable<int> SkillOfClassID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_PupilID")]	
    public Nullable<int> PupilID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_ClassID")]	
    public Nullable<int> ClassID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_AcademicYearID")]	
    public Nullable<int> AcademicYearID { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_CommentOfTeacher")]	
    public string CommentOfTeacher { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_IsSMS")]	
    public Nullable<bool> IsSMS { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_CommentOfParent")]	
    public string CommentOfParent { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_IsPass")]	
    public Nullable<bool> IsPass { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_Rank")]	
    public Nullable<int> Rank { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_Prize")]	
    public string Prize { get; set; }	

  
    [Display(Name = "SkillOfPupil_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SkillOfPupil_Label_SkillOfClass")]	
    public virtual SkillOfClass SkillOfClass { get; set; }	
    }
}

