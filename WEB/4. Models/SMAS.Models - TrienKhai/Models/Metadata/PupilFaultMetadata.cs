

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilFaultMetadata))]
public partial class PupilFault { }

public partial class PupilFaultMetadata
    {
  
    [Display(Name = "PupilFault_Label_PupilFaultID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFaultID { get; set; }	
  
    [Display(Name = "PupilFault_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "PupilFault_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "PupilFault_Label_FaultID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FaultID { get; set; }	
  
    [Display(Name = "PupilFault_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "PupilFault_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "PupilFault_Label_ViolatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ViolatedDate { get; set; }	
  
    [Display(Name = "PupilFault_Label_NumberOfFault")]	
    public Nullable<int> NumberOfFault { get; set; }	
  
    [Display(Name = "PupilFault_Label_TotalPenalizedMark")]	
    public Nullable<decimal> TotalPenalizedMark { get; set; }	
  
    [Display(Name = "PupilFault_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "PupilFault_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "PupilFault_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilFault_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PupilFault_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "PupilFault_Label_FaultCriteria")]	
    public virtual FaultCriteria FaultCriteria { get; set; }	
  
    [Display(Name = "PupilFault_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "PupilFault_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "PupilFault_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "PupilFault_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

