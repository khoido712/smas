

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeachingTargetRegistrationMetadata))]
public partial class TeachingTargetRegistration { }

public partial class TeachingTargetRegistrationMetadata
    {
  
    [Display(Name = "TeachingTargetRegistration_Label_TeachingTargetRegistrationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeachingTargetRegistrationID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_ExcellentGrade")]	
    public Nullable<int> ExcellentGrade { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_GoodGrade")]	
    public Nullable<int> GoodGrade { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_FairGrade")]	
    public Nullable<int> FairGrade { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_WeakGrade")]	
    public Nullable<int> WeakGrade { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_RegisteredDate")]	
    public Nullable<System.DateTime> RegisteredDate { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeachingTargetRegistration_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "TeachingTargetRegistration_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
    }
}

