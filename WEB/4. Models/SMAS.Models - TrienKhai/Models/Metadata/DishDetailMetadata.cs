

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DishDetailMetadata))]
public partial class DishDetail { }

public partial class DishDetailMetadata
    {
  
    [Display(Name = "DishDetail_Label_DishDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishDetailID { get; set; }	
  
    [Display(Name = "DishDetail_Label_WeightPerRation")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal WeightPerRation { get; set; }	
  
    [Display(Name = "DishDetail_Label_FoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodID { get; set; }	
  
    [Display(Name = "DishDetail_Label_DishID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishID { get; set; }	
  
    [Display(Name = "DishDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DishDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DishDetail_Label_FoodCat")]	
    public virtual FoodCat FoodCat { get; set; }	
  
    [Display(Name = "DishDetail_Label_DishCat")]	
    public virtual DishCat DishCat { get; set; }	
    }
}

