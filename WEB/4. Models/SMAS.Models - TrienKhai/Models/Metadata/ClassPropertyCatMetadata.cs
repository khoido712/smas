

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassPropertyCatMetadata))]
public partial class ClassPropertyCat { }

public partial class ClassPropertyCatMetadata
    {
  
    [Display(Name = "ClassPropertyCat_Label_ClassPropertyCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassPropertyCatID { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_Resolution")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_PropertyGroup")]	
    public Nullable<int> PropertyGroup { get; set; }	
  
    [Display(Name = "ClassPropertyCat_Label_M_PropertyClassID")]	
    public Nullable<int> M_PropertyClassID { get; set; }	

  
    [Display(Name = "ClassPropertyCat_Label_PropertyOfClasses")]	
    public virtual ICollection<PropertyOfClass> PropertyOfClasses { get; set; }	
    }
}

