

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PropertyOfSchoolMetadata))]
public partial class PropertyOfSchool { }

public partial class PropertyOfSchoolMetadata
    {
  
    [Display(Name = "PropertyOfSchool_Label_PropertyOfSchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PropertyOfSchoolID { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_SchoolPropertyCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolPropertyCatID { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PropertyOfSchool_Label_SchoolPropertyCat")]	
    public virtual SchoolPropertyCat SchoolPropertyCat { get; set; }	
  
    [Display(Name = "PropertyOfSchool_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

