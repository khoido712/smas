

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ApprenticeshipTrainingScheduleMetadata))]
public partial class ApprenticeshipTrainingSchedule { }

public partial class ApprenticeshipTrainingScheduleMetadata
    {
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_ApprenticeshipTrainingScheduleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipTrainingScheduleID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_Month")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Month { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_SectionInDay")]	
    public Nullable<int> SectionInDay { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_ScheduleDetail")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(31,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ScheduleDetail { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_ModiffedDate")]	
    public Nullable<System.DateTime> ModiffedDate { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_ApprenticeshipClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipClassID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_ApprenticeshipClass")]	
    public virtual ApprenticeshipClass ApprenticeshipClass { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ApprenticeshipTrainingSchedule_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

