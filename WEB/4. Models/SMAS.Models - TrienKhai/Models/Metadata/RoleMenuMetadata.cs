

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(RoleMenuMetadata))]
public partial class RoleMenu { }

public partial class RoleMenuMetadata
    {
  
    [Display(Name = "RoleMenu_Label_RoleMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleMenuID { get; set; }	
  
    [Display(Name = "RoleMenu_Label_RoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleID { get; set; }	
  
    [Display(Name = "RoleMenu_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "RoleMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "RoleMenu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "RoleMenu_Label_Menu")]	
    public virtual Menu Menu { get; set; }	
  
    [Display(Name = "RoleMenu_Label_Role")]	
    public virtual Role Role { get; set; }	
    }
}

