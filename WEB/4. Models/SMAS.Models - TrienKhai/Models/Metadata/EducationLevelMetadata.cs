

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EducationLevelMetadata))]
public partial class EducationLevel { }

public partial class EducationLevelMetadata
    {
  
    [Display(Name = "EducationLevel_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "EducationLevel_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "EducationLevel_Label_IsLastYear")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsLastYear { get; set; }	
  
    [Display(Name = "EducationLevel_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "EducationLevel_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "EducationLevel_Label_Grade")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Grade { get; set; }	

  
    [Display(Name = "EducationLevel_Label_Candidates")]	
    public virtual ICollection<Candidate> Candidates { get; set; }	
  
    [Display(Name = "EducationLevel_Label_CandidateAbsences")]	
    public virtual ICollection<CandidateAbsence> CandidateAbsences { get; set; }	
  
    [Display(Name = "EducationLevel_Label_DetachableHeadBags")]	
    public virtual ICollection<DetachableHeadBag> DetachableHeadBags { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ExaminationRooms")]	
    public virtual ICollection<ExaminationRoom> ExaminationRooms { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ExaminationSubjects")]	
    public virtual ICollection<ExaminationSubject> ExaminationSubjects { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ViolatedInvigilators")]	
    public virtual ICollection<ViolatedInvigilator> ViolatedInvigilators { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ApprenticeshipTrainings")]	
    public virtual ICollection<ApprenticeshipTraining> ApprenticeshipTrainings { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ApprenticeshipTrainingAbsences")]	
    public virtual ICollection<ApprenticeshipTrainingAbsence> ApprenticeshipTrainingAbsences { get; set; }	
  
    [Display(Name = "EducationLevel_Label_CapacityStatistics")]	
    public virtual ICollection<CapacityStatistic> CapacityStatistics { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ClassMovements")]	
    public virtual ICollection<ClassMovement> ClassMovements { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ClassProfiles")]	
    public virtual ICollection<ClassProfile> ClassProfiles { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ConductStatistics")]	
    public virtual ICollection<ConductStatistic> ConductStatistics { get; set; }	
  
    [Display(Name = "EducationLevel_Label_EducationPrograms")]	
    public virtual ICollection<EducationProgram> EducationPrograms { get; set; }	
  
    [Display(Name = "EducationLevel_Label_EvaluationDevelopments")]	
    public virtual ICollection<EvaluationDevelopment> EvaluationDevelopments { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ExemptedSubjects")]	
    public virtual ICollection<ExemptedSubject> ExemptedSubjects { get; set; }	
  
    [Display(Name = "EducationLevel_Label_MarkStatistics")]	
    public virtual ICollection<MarkStatistic> MarkStatistics { get; set; }	
  
    [Display(Name = "EducationLevel_Label_MovementAcceptances")]	
    public virtual ICollection<MovementAcceptance> MovementAcceptances { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ParticularPupils")]	
    public virtual ICollection<ParticularPupil> ParticularPupils { get; set; }	
  
    [Display(Name = "EducationLevel_Label_ProvinceSubjects")]	
    public virtual ICollection<ProvinceSubject> ProvinceSubjects { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilAbsences")]	
    public virtual ICollection<PupilAbsence> PupilAbsences { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilDisciplines")]	
    public virtual ICollection<PupilDiscipline> PupilDisciplines { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilFaults")]	
    public virtual ICollection<PupilFault> PupilFaults { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilLeavingOffs")]	
    public virtual ICollection<PupilLeavingOff> PupilLeavingOffs { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilPraises")]	
    public virtual ICollection<PupilPraise> PupilPraises { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilRankings")]	
    public virtual ICollection<PupilRanking> PupilRankings { get; set; }	
  
    [Display(Name = "EducationLevel_Label_PupilRepeateds")]	
    public virtual ICollection<PupilRepeated> PupilRepeateds { get; set; }	
  
    [Display(Name = "EducationLevel_Label_SchoolMovements")]	
    public virtual ICollection<SchoolMovement> SchoolMovements { get; set; }	
  
    [Display(Name = "EducationLevel_Label_SchoolSubjects")]	
    public virtual ICollection<SchoolSubject> SchoolSubjects { get; set; }	
  
    [Display(Name = "EducationLevel_Label_StatisticSubcommitteeOfTertiaries")]	
    public virtual ICollection<StatisticSubcommitteeOfTertiary> StatisticSubcommitteeOfTertiaries { get; set; }	
    }
}

