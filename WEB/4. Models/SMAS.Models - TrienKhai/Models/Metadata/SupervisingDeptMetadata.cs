

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SupervisingDeptMetadata))]
public partial class SupervisingDept { }

public partial class SupervisingDeptMetadata
    {
  
    [Display(Name = "SupervisingDept_Label_SupervisingDeptID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SupervisingDeptID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_SupervisingDeptCode")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SupervisingDeptCode { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_SupervisingDeptName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SupervisingDeptName { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_ParentID")]	
    public Nullable<int> ParentID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_OrderNumber")]	
    public Nullable<int> OrderNumber { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_HierachyLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HierachyLevel { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_AreaID")]	
    public Nullable<int> AreaID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_ProvinceID")]	
    public Nullable<int> ProvinceID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_DistrictID")]	
    public Nullable<int> DistrictID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Address")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Address { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Telephone")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Telephone { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Fax")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Fax { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Website")]
    [StringLength(512,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Website { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Email")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Email { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_TraversalPath")]    
    public string TraversalPath { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_AdminID")]	
    public Nullable<int> AdminID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_IsActiveSMAS")]	
    public Nullable<bool> IsActiveSMAS { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_SMSActiveType")]	
    public Nullable<int> SMSActiveType { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_M_OldAdminID")]	
    public Nullable<int> M_OldAdminID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SupervisingDept_Label_Area")]	
    public virtual Area Area { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_District")]	
    public virtual District District { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeHistoryStatus")]	
    public virtual ICollection<EmployeeHistoryStatus> EmployeeHistoryStatus { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeQualifications")]	
    public virtual ICollection<EmployeeQualification> EmployeeQualifications { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeSalaries")]	
    public virtual ICollection<EmployeeSalary> EmployeeSalaries { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeWorkingHistories")]	
    public virtual ICollection<EmployeeWorkingHistory> EmployeeWorkingHistories { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeWorkMovements")]	
    public virtual ICollection<EmployeeWorkMovement> EmployeeWorkMovements { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_EmployeeWorkTypes")]	
    public virtual ICollection<EmployeeWorkType> EmployeeWorkTypes { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_SchoolProfiles")]	
    public virtual ICollection<SchoolProfile> SchoolProfiles { get; set; }	
  
    //[Display(Name = "SupervisingDept_Label_SupervisingDept1")]	
    //public virtual ICollection<SupervisingDept> SupervisingDept1 { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_SupervisingDept2")]	
    public virtual SupervisingDept SupervisingDept2 { get; set; }	
  
    [Display(Name = "SupervisingDept_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

