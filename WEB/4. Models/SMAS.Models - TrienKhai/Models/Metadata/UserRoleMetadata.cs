

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(UserRoleMetadata))]
public partial class UserRole { }

public partial class UserRoleMetadata
    {
  
    [Display(Name = "UserRole_Label_UserRoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserRoleID { get; set; }	
  
    [Display(Name = "UserRole_Label_UserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserID { get; set; }	
  
    [Display(Name = "UserRole_Label_RoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleID { get; set; }	
  
    [Display(Name = "UserRole_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "UserRole_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "UserRole_Label_Role")]	
    public virtual Role Role { get; set; }	
  
    [Display(Name = "UserRole_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

