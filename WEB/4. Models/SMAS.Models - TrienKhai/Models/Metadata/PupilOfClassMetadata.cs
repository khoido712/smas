

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilOfClassMetadata))]
public partial class PupilOfClass { }

public partial class PupilOfClassMetadata
    {
  
    [Display(Name = "PupilOfClass_Label_PupilOfClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilOfClassID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_AssignedDate")]	
    public Nullable<System.DateTime> AssignedDate { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_OrderInClass")]	
    public Nullable<int> OrderInClass { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_NoConductEstimation")]	
    public Nullable<bool> NoConductEstimation { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_Status")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Status { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilOfClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	


    }
}

