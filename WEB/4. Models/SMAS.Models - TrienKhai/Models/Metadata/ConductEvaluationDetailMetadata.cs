

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConductEvaluationDetailMetadata))]
public partial class ConductEvaluationDetail { }

public partial class ConductEvaluationDetailMetadata
    {
  
    [Display(Name = "ConductEvaluationDetail_Label_ConductEvaluationDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConductEvaluationDetailID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Task1Evaluation")]	
    public Nullable<int> Task1Evaluation { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Task2Evaluation")]	
    public Nullable<int> Task2Evaluation { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Task3Evaluation")]	
    public Nullable<int> Task3Evaluation { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Task4Evaluation")]	
    public Nullable<int> Task4Evaluation { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_Task5Evaluation")]	
    public Nullable<int> Task5Evaluation { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_EvaluatedDate")]	
    public Nullable<System.DateTime> EvaluatedDate { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_UpdatedDate")]	
    public Nullable<System.DateTime> UpdatedDate { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_M_Year")]	
    public Nullable<int> M_Year { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConductEvaluationDetail_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "ConductEvaluationDetail_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

