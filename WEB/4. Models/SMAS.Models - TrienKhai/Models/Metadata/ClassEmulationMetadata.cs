

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassEmulationMetadata))]
public partial class ClassEmulation { }

public partial class ClassEmulationMetadata
    {
  
    [Display(Name = "ClassEmulation_Label_ClassEmulationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassEmulationID { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_AcademicYearID")]	
    public Nullable<int> AcademicYearID { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_Month")]	
    public Nullable<int> Month { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_TotalEmulationMark")]	
    public Nullable<decimal> TotalEmulationMark { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_TotalPenalizedMark")]	
    public Nullable<decimal> TotalPenalizedMark { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_TotalMark")]	
    public Nullable<decimal> TotalMark { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_Rank")]	
    public Nullable<int> Rank { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassEmulation_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ClassEmulation_Label_ClassEmulationDetails")]	
    public virtual ICollection<ClassEmulationDetail> ClassEmulationDetails { get; set; }	
    }
}

