

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SendResultMetadata))]
public partial class SendResult { }

public partial class SendResultMetadata
    {
  
    [Display(Name = "SendResult_Label_SendResultID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SendResultID { get; set; }	
  
    [Display(Name = "SendResult_Label_PackageID")]	
    public Nullable<int> PackageID { get; set; }	
  
    [Display(Name = "SendResult_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "SendResult_Label_PupilCode")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "SendResult_Label_EmployeeCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EmployeeCode { get; set; }	
  
    [Display(Name = "SendResult_Label_TypeOfReceiver")]	
    public Nullable<int> TypeOfReceiver { get; set; }	
  
    [Display(Name = "SendResult_Label_SchedularType")]	
    public Nullable<int> SchedularType { get; set; }	
  
    [Display(Name = "SendResult_Label_InfoType")]	
    public Nullable<int> InfoType { get; set; }	
  
    [Display(Name = "SendResult_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "SendResult_Label_Title")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "SendResult_Label_Content")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Content { get; set; }	
  
    [Display(Name = "SendResult_Label_ReceiverEmail")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReceiverEmail { get; set; }	
  
    [Display(Name = "SendResult_Label_ReceiverMobile")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReceiverMobile { get; set; }	
  
    [Display(Name = "SendResult_Label_SendType")]	
    public Nullable<int> SendType { get; set; }	
  
    [Display(Name = "SendResult_Label_Status")]	
    public Nullable<int> Status { get; set; }	
  
    [Display(Name = "SendResult_Label_TimeSend")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TimeSend { get; set; }	
  
    [Display(Name = "SendResult_Label_ServiceNumber")]
    [StringLength(18,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceNumber { get; set; }	
  
    [Display(Name = "SendResult_Label_RetryNum")]	
    public Nullable<int> RetryNum { get; set; }	
  
    [Display(Name = "SendResult_Label_CreateTime")]	
    public Nullable<System.DateTime> CreateTime { get; set; }	
  
    [Display(Name = "SendResult_Label_CreateUser")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CreateUser { get; set; }	
  
    [Display(Name = "SendResult_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SendResult_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SendResult_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SendResult_Label_Package")]	
    public virtual Package Package { get; set; }	
    }
}

