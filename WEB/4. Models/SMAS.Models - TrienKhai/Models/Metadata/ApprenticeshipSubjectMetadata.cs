

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ApprenticeshipSubjectMetadata))]
public partial class ApprenticeshipSubject { }

public partial class ApprenticeshipSubjectMetadata
    {
  
    [Display(Name = "ApprenticeshipSubject_Label_ApprenticeshipSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ApprenticeshipSubjectID { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_SubjectName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubjectName { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ApprenticeshipSubject_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_ApprenticeshipClasses")]	
    public virtual ICollection<ApprenticeshipClass> ApprenticeshipClasses { get; set; }	
  
    [Display(Name = "ApprenticeshipSubject_Label_ApprenticeshipTrainings")]	
    public virtual ICollection<ApprenticeshipTraining> ApprenticeshipTrainings { get; set; }	
    }
}

