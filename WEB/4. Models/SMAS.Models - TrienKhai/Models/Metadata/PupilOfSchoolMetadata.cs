

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilOfSchoolMetadata))]
public partial class PupilOfSchool { }

public partial class PupilOfSchoolMetadata
    {
  
    [Display(Name = "PupilOfSchool_Label_PupilOfSchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilOfSchoolID { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_EnrolmentDate")]	
    public Nullable<System.DateTime> EnrolmentDate { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_EnrolmentType")]	
    public Nullable<int> EnrolmentType { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_M_Year")]	
    public Nullable<int> M_Year { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilOfSchool_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
   
    }
}

