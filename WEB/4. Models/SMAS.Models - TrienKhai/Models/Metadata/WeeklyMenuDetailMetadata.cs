

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(WeeklyMenuDetailMetadata))]
public partial class WeeklyMenuDetail { }

public partial class WeeklyMenuDetailMetadata
    {
  
    [Display(Name = "WeeklyMenuDetail_Label_WeeklyMenuDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WeeklyMenuDetailID { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_DayOfWeek")]	
    public Nullable<int> DayOfWeek { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_DailyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuID { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_WeeklyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WeeklyMenuID { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "WeeklyMenuDetail_Label_DailyMenu")]	
    public virtual DailyMenu DailyMenu { get; set; }	
  
    [Display(Name = "WeeklyMenuDetail_Label_WeeklyMenu")]	
    public virtual WeeklyMenu WeeklyMenu { get; set; }	
    }
}

