

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolProfileMetadata))]
public partial class SchoolProfile { }

public partial class SchoolProfileMetadata
    {
  
    [Display(Name = "SchoolProfile_Label_SchoolProfileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolProfileID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SchoolCode")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SchoolCode { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SchoolName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SchoolName { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_ShortName")]
    [StringLength(60,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ShortName { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_AreaID")]	
    public Nullable<int> AreaID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_ProvinceID")]	
    public Nullable<int> ProvinceID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_DistrictID")]	
    public Nullable<int> DistrictID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_CommuneID")]	
    public Nullable<int> CommuneID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SupervisingDeptID")]	
    public Nullable<int> SupervisingDeptID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_TrainingTypeID")]	
    public Nullable<int> TrainingTypeID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SchoolTypeID")]	
    public Nullable<int> SchoolTypeID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Telephone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Telephone { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Fax")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Fax { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Email")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Email { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Address")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Address { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_EstablishedDate")]	
    public Nullable<System.DateTime> EstablishedDate { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Website")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Website { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_Image")]	
    public byte[] Image { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_EducationGrade")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationGrade { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_ReportTile")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportTile { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_HeadMasterName")]
    [StringLength(60,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HeadMasterName { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_HeadMasterPhone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HeadMasterPhone { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_HasSubsidiary")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool HasSubsidiary { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_AdminID")]	
    public Nullable<int> AdminID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SchoolYearTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SchoolYearTitle { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SMSTeacherActiveType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSTeacherActiveType { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_SMSParentActiveType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentActiveType { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_CancelActiveReason")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CancelActiveReason { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_M_OldAdminID")]	
    public Nullable<System.Guid> M_OldAdminID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolProfile_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    
    }
}

