

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EthnicMetadata))]
public partial class Ethnic { }

public partial class EthnicMetadata
    {
  
    [Display(Name = "Ethnic_Label_EthnicID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EthnicID { get; set; }	
  
    [Display(Name = "Ethnic_Label_EthnicCode")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EthnicCode { get; set; }	
  
    [Display(Name = "Ethnic_Label_EthnicName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EthnicName { get; set; }	
  
    [Display(Name = "Ethnic_Label_IsMinority")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsMinority { get; set; }	
  
    [Display(Name = "Ethnic_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Ethnic_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Ethnic_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Ethnic_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "Ethnic_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
  
    [Display(Name = "Ethnic_Label_PupilProfiles")]	
    public virtual ICollection<PupilProfile> PupilProfiles { get; set; }	
    }
}

