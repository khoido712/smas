

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_UserGroupMetadata))]
public partial class M_UserGroup { }

public partial class M_UserGroupMetadata
    {
  
    [Display(Name = "M_UserGroup_Label_GroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupID { get; set; }	
  
    [Display(Name = "M_UserGroup_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_UserGroup_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "M_UserGroup_Label_ID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ID { get; set; }	
  
    [Display(Name = "M_UserGroup_Label_UserID")]	
    public Nullable<System.Guid> UserID { get; set; }	
    }
}

