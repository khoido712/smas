﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(StatisticOfTertiaryStartYearMetadata))]
    public partial class StatisticTertiaryStartYear { }

    public partial class StatisticOfTertiaryStartYearMetadata
    {

        [Display(Name = "StatisticOfTertiaryStartYear_Label_StatisticTerStartID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int StatisticTerStartID { get; set; }

        [Display(Name = "StatisticOfTertiaryStartYear_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolID { get; set; }

        [Display(Name = "StatisticOfTertiaryStartYear_Label_ReportDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.DateTime ReportDate { get; set; }

        [Display(Name = "StatisticOfTertiaryStartYear_Label_ReportStatus")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool? ReportStatus { get; set; }

        [Display(Name = "StatisticOfTertiaryStartYear_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }


        [Display(Name = "StatisticOfTertiaryStartYear_Label_SupervisingDeptID")]
        public int SupervisingDeptID { get; set; }
    }
}
