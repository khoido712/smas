

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SymbolConfigMetadata))]
public partial class SymbolConfig { }

public partial class SymbolConfigMetadata
    {
  
    [Display(Name = "SymbolConfig_Label_SymbolConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SymbolConfigID { get; set; }	
  
    [Display(Name = "SymbolConfig_Label_SymbolConfigName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(5,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SymbolConfigName { get; set; }	
  
    [Display(Name = "SymbolConfig_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "SymbolConfig_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SymbolConfig_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "SymbolConfig_Label_ClassificationLevelHealths")]	
    public virtual ICollection<ClassificationLevelHealth> ClassificationLevelHealths { get; set; }	
    }
}

