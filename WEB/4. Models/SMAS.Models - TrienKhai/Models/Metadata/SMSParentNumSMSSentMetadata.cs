

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSParentNumSMSSentMetadata))]
public partial class SMSParentNumSMSSent { }

public partial class SMSParentNumSMSSentMetadata
    {
  
    [Display(Name = "SMSParentNumSMSSent_Label_SMSParentNumSMSSentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentNumSMSSentID { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_SMSParentSubscriberID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentSubscriberID { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_MarkDay")]	
    public Nullable<int> MarkDay { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_MarkMonth")]	
    public Nullable<int> MarkMonth { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_MarkSemester")]	
    public Nullable<int> MarkSemester { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_MarkYear")]	
    public Nullable<int> MarkYear { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_DiligenceDay")]	
    public Nullable<int> DiligenceDay { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_DiligenceMonth")]	
    public Nullable<int> DiligenceMonth { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_DiligenceSemester")]	
    public Nullable<int> DiligenceSemester { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_DiligenceYear")]	
    public Nullable<int> DiligenceYear { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_Notification")]	
    public Nullable<int> Notification { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_CreatedTime")]	
    public Nullable<System.DateTime> CreatedTime { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_UpdatedTime")]	
    public Nullable<System.DateTime> UpdatedTime { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_MarkWeek")]	
    public Nullable<int> MarkWeek { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_DiligenceWeek")]	
    public Nullable<int> DiligenceWeek { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_OtherMessage")]	
    public Nullable<int> OtherMessage { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_IsExpired")]	
    public Nullable<int> IsExpired { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SMSParentNumSMSSent_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SMSParentNumSMSSent_Label_SMSParentSubscriber")]	
    public virtual SMSParentSubscriber SMSParentSubscriber { get; set; }	
    }
}

