

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeachingAssignmentMetadata))]
public partial class TeachingAssignment { }

public partial class TeachingAssignmentMetadata
    {
  
    [Display(Name = "TeachingAssignment_Label_TeachingAssignmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public long TeachingAssignmentID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_FacultyID")]	
    public Nullable<int> FacultyID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_ClassID")]	
    public Nullable<int> ClassID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_AssignedDate")]	
    public Nullable<System.DateTime> AssignedDate { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "TeachingAssignment_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "TeachingAssignment_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

