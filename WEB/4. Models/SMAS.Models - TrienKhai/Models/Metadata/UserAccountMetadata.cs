

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(UserAccountMetadata))]
public partial class UserAccount { }

public partial class UserAccountMetadata
    {
  
    [Display(Name = "UserAccount_Label_UserAccountID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserAccountID { get; set; }	
  
    [Display(Name = "UserAccount_Label_GUID")]	
    public Nullable<System.Guid> GUID { get; set; }	
  
    [Display(Name = "UserAccount_Label_EmployeeID")]	
    public Nullable<int> EmployeeID { get; set; }	
  
    [Display(Name = "UserAccount_Label_CreatedUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CreatedUserID { get; set; }	
  
    [Display(Name = "UserAccount_Label_IsAdmin")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsAdmin { get; set; }	
  
    [Display(Name = "UserAccount_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "UserAccount_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "UserAccount_Label_IsOperationalUser")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsOperationalUser { get; set; }	
  
    [Display(Name = "UserAccount_Label_FirstLoginDate")]	
    public Nullable<System.DateTime> FirstLoginDate { get; set; }	
  
    [Display(Name = "UserAccount_Label_LoginFalseCount")]	
    public Nullable<int> LoginFalseCount { get; set; }	
  
    [Display(Name = "UserAccount_Label_M_SuperUserID")]	
    public Nullable<int> M_SuperUserID { get; set; }	
  
    [Display(Name = "UserAccount_Label_M_TeacherID")]	
    public Nullable<int> M_TeacherID { get; set; }	
  
    [Display(Name = "UserAccount_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "UserAccount_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "UserAccount_Label_PupilID")]	
    public Nullable<int> PupilID { get; set; }	
  
    [Display(Name = "UserAccount_Label_SelectLevel")]	
    public Nullable<short> SelectLevel { get; set; }	

  
    [Display(Name = "UserAccount_Label_ActionAudits")]	
    public virtual ICollection<ActionAudit> ActionAudits { get; set; }	
  
    [Display(Name = "UserAccount_Label_GroupCats")]	
    public virtual ICollection<GroupCat> GroupCats { get; set; }	
  
    [Display(Name = "UserAccount_Label_Menus")]	
    public virtual ICollection<Menu> Menus { get; set; }	
  
    [Display(Name = "UserAccount_Label_EmployeeWorkMovements")]	
    public virtual ICollection<EmployeeWorkMovement> EmployeeWorkMovements { get; set; }	
  
    [Display(Name = "UserAccount_Label_SchoolProfiles")]	
    public virtual ICollection<SchoolProfile> SchoolProfiles { get; set; }	
  
    [Display(Name = "UserAccount_Label_SupervisingDepts")]	
    public virtual ICollection<SupervisingDept> SupervisingDepts { get; set; }	
  
    [Display(Name = "UserAccount_Label_aspnet_Users")]	
    public virtual aspnet_Users aspnet_Users { get; set; }	
  
    //[Display(Name = "UserAccount_Label_UserAccount1")]	
    //public virtual ICollection<UserAccount> UserAccount1 { get; set; }	
  
    [Display(Name = "UserAccount_Label_UserAccount2")]	
    public virtual UserAccount UserAccount2 { get; set; }	
  
    [Display(Name = "UserAccount_Label_UserGroups")]	
    public virtual ICollection<UserGroup> UserGroups { get; set; }	
  
    [Display(Name = "UserAccount_Label_UserProvinces")]	
    public virtual ICollection<UserProvince> UserProvinces { get; set; }	
  
    [Display(Name = "UserAccount_Label_UserRoles")]	
    public virtual ICollection<UserRole> UserRoles { get; set; }	
  
    [Display(Name = "UserAccount_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "UserAccount_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
    }
}

