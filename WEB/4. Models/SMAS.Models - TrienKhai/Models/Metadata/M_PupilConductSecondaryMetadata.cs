

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_PupilConductSecondaryMetadata))]
public partial class M_PupilConductSecondary { }

public partial class M_PupilConductSecondaryMetadata
    {
  
    [Display(Name = "M_PupilConductSecondary_Label_PupilConductSecondaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilConductSecondaryID { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_Semester1")]	
    public Nullable<int> Semester1 { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_Semester2")]	
    public Nullable<int> Semester2 { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_Both")]	
    public Nullable<int> Both { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_MustRetest")]	
    public Nullable<bool> MustRetest { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_Retest")]	
    public Nullable<int> Retest { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_PupilConductSecondary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

