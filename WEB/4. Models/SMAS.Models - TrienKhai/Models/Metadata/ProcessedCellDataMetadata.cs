

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ProcessedCellDataMetadata))]
public partial class ProcessedCellData { }

public partial class ProcessedCellDataMetadata
    {
  
    [Display(Name = "ProcessedCellData_Label_ProcessedCellDataID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProcessedCellDataID { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_ProcessedDate")]	
    public Nullable<System.DateTime> ProcessedDate { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_ProcessedReportID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProcessedReportID { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_CellDefinitionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CellDefinitionID { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_CellValue")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CellValue { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ProcessedCellData_Label_ProcessedReport")]	
    public virtual ProcessedReport ProcessedReport { get; set; }	
  
    [Display(Name = "ProcessedCellData_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

