

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityTypeMetadata))]
public partial class ActivityType { }

public partial class ActivityTypeMetadata
    {
  
    [Display(Name = "ActivityType_Label_ActivityTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityTypeID { get; set; }	
  
    [Display(Name = "ActivityType_Label_ActivityTypeName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ActivityTypeName { get; set; }	
  
    [Display(Name = "ActivityType_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "ActivityType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ActivityType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ActivityType_Label_Activities")]	
    public virtual ICollection<Activity> Activities { get; set; }	  	
  
    [Display(Name = "ActivityType_Label_ActivityOfClasses")]	
    public virtual ICollection<ActivityOfClass> ActivityOfClasses { get; set; }	
  
    [Display(Name = "ActivityType_Label_ActivityOfPupils")]	
    public virtual ICollection<ActivityOfPupil> ActivityOfPupils { get; set; }	
    }
}

