

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SkillTitleMetadata))]
public partial class SkillTitle { }

public partial class SkillTitleMetadata
    {
  
    [Display(Name = "SkillTitle_Label_SkillTitleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillTitleID { get; set; }

    [Display(Name = "SkillTitle_Label_Title")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
    public string Title { get; set; }	
  
    [Display(Name = "SkillTitle_Label_Description")]
    [StringLength(1024,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SkillTitle_Label_FromDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FromDate { get; set; }	
  
    [Display(Name = "SkillTitle_Label_ToDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ToDate { get; set; }	
  
    [Display(Name = "SkillTitle_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "SkillTitle_Label_AcademicYearID")]	
    public Nullable<int> AcademicYearID { get; set; }	
  
    [Display(Name = "SkillTitle_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SkillTitle_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SkillTitle_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "SkillTitle_Label_Skills")]	
    public virtual ICollection<Skill> Skills { get; set; }	
  
    [Display(Name = "SkillTitle_Label_SkillOfClasses")]	
    public virtual ICollection<SkillOfClass> SkillOfClasses { get; set; }	
  
    [Display(Name = "SkillTitle_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SkillTitle_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SkillTitle_Label_SkillTitleClasses")]	
    public virtual ICollection<SkillTitleClass> SkillTitleClasses { get; set; }	
    }
}

