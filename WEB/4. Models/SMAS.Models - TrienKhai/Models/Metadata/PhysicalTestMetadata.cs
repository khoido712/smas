

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PhysicalTestMetadata))]
public partial class PhysicalTest { }

public partial class PhysicalTestMetadata
    {
  
    [Display(Name = "PhysicalTest_Label_PhysicalTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PhysicalTestID { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_Date")]
    public System.DateTime Date { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_Height")]
    public decimal? Height { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_Weight")]
    public decimal? Weight { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_Breast")]
    //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal? Breast { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_PhysicalClassification")]
    public int? PhysicalClassification { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_Nutrition")]	
    public Nullable<int> Nutrition { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PhysicalTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PhysicalTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

