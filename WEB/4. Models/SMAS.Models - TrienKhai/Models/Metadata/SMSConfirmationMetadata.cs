

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSConfirmationMetadata))]
public partial class SMSConfirmation { }

public partial class SMSConfirmationMetadata
    {
  
    [Display(Name = "SMSConfirmation_Label_ConfirmationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConfirmationID { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_LogMOID")]	
    public Nullable<int> LogMOID { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_PhoneNumber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PhoneNumber { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_ConfirmType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConfirmType { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_ConfirmValue")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ConfirmValue { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_Status")]	
    public Nullable<int> Status { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_ConfirmCount")]	
    public Nullable<int> ConfirmCount { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_Description")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "SMSConfirmation_Label_UpdateDate")]	
    public Nullable<System.DateTime> UpdateDate { get; set; }	

  
    [Display(Name = "SMSConfirmation_Label_LogMO")]	
    public virtual LogMO LogMO { get; set; }	
  
    //[Display(Name = "SMSConfirmation_Label_SMSConfirmationDetails")]	
    //public virtual ICollection<SMSConfirmationDetail> SMSConfirmationDetails { get; set; }	
    }
}

