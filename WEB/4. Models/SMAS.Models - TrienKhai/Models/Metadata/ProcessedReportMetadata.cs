

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ProcessedReportMetadata))]
public partial class ProcessedReport { }

public partial class ProcessedReportMetadata
    {
  
    [Display(Name = "ProcessedReport_Label_ProcessedReportID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProcessedReportID { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportCode { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ProcessedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime ProcessedDate { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_InputParameterHashKey")]	
    public string InputParameterHashKey { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ReportData")]	
    public byte[] ReportData { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_DeserializeClass")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DeserializeClass { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_HandlerProcess")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HandlerProcess { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ExpiredDate")]	
    public Nullable<System.DateTime> ExpiredDate { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_SentToSupervisor")]	
    public Nullable<bool> SentToSupervisor { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ReportName")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReportName { get; set; }	

  
    [Display(Name = "ProcessedReport_Label_ProcessedCellDatas")]	
    public virtual ICollection<ProcessedCellData> ProcessedCellDatas { get; set; }	
  
    [Display(Name = "ProcessedReport_Label_ProcessedReportParameters")]	
    public virtual ICollection<ProcessedReportParameter> ProcessedReportParameters { get; set; }	
    }
}

