

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SCSServiceMetadata))]
public partial class SCSService { }

public partial class SCSServiceMetadata
    {
  
    [Display(Name = "SCSService_Label_SCSServiceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SCSServiceID { get; set; }	
  
    [Display(Name = "SCSService_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SCSService_Label_ServiceCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceCode { get; set; }	
  
    [Display(Name = "SCSService_Label_Status")]	
    public Nullable<bool> Status { get; set; }	
  
    [Display(Name = "SCSService_Label_ManagementUrl")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ManagementUrl { get; set; }	
  
    [Display(Name = "SCSService_Label_PortalUrl")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PortalUrl { get; set; }	
  
    [Display(Name = "SCSService_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "SCSService_Label_UpdateDate")]	
    public Nullable<System.DateTime> UpdateDate { get; set; }	

  
    [Display(Name = "SCSService_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

