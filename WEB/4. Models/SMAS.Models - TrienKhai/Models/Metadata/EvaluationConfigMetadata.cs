

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EvaluationConfigMetadata))]
public partial class EvaluationConfig { }

public partial class EvaluationConfigMetadata
    {
  
    [Display(Name = "EvaluationConfig_Label_EvaluationConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationConfigID { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_EvaluationConfigName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EvaluationConfigName { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_PositionOrder")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PositionOrder { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_TypeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeConfigID { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "EvaluationConfig_Label_ClassificationLevelHealths")]	
    public virtual ICollection<ClassificationLevelHealth> ClassificationLevelHealths { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_TypeConfig")]	
    public virtual TypeConfig TypeConfig { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_LevelHealthConfigDetails")]	
    public virtual ICollection<LevelHealthConfigDetail> LevelHealthConfigDetails { get; set; }	
  
    [Display(Name = "EvaluationConfig_Label_PupilProfiles")]	
    public virtual ICollection<PupilProfile> PupilProfiles { get; set; }	
    }
}

