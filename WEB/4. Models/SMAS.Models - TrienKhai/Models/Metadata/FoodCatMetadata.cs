

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(FoodCatMetadata))]
public partial class FoodCat { }

public partial class FoodCatMetadata
    {
  
    [Display(Name = "FoodCat_Label_FoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodID { get; set; }	
  
    [Display(Name = "FoodCat_Label_GroupType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupType { get; set; }	
  
    [Display(Name = "FoodCat_Label_DiscardedRate")]	
    public Nullable<int> DiscardedRate { get; set; }	
  
    [Display(Name = "FoodCat_Label_Price")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Price { get; set; }	
  
    [Display(Name = "FoodCat_Label_Protein")]	
    public Nullable<decimal> Protein { get; set; }	
  
    [Display(Name = "FoodCat_Label_Fat")]	
    public Nullable<decimal> Fat { get; set; }	
  
    [Display(Name = "FoodCat_Label_Sugar")]	
    public Nullable<decimal> Sugar { get; set; }	
  
    [Display(Name = "FoodCat_Label_Calo")]	
    public Nullable<decimal> Calo { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FoodName { get; set; }	
  
    [Display(Name = "FoodCat_Label_ShortName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ShortName { get; set; }	
  
    [Display(Name = "FoodCat_Label_TypeOfFoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeOfFoodID { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodPackingID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int? FoodPackingID { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodGroupID")]	
    public Nullable<int> FoodGroupID { get; set; }	
  
    [Display(Name = "FoodCat_Label_CalculationUnit")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CalculationUnit { get; set; }	
  
    [Display(Name = "FoodCat_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "FoodCat_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "FoodCat_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "FoodCat_Label_DailyFoodInspections")]	
    public virtual ICollection<DailyFoodInspection> DailyFoodInspections { get; set; }	
  
    [Display(Name = "FoodCat_Label_DailyMenuFoods")]	
    public virtual ICollection<DailyMenuFood> DailyMenuFoods { get; set; }	
  
    [Display(Name = "FoodCat_Label_DishDetails")]	
    public virtual ICollection<DishDetail> DishDetails { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodGroup")]	
    public virtual FoodGroup FoodGroup { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodPacking")]	
    public virtual FoodPacking FoodPacking { get; set; }	
  
    [Display(Name = "FoodCat_Label_TypeOfFood")]	
    public virtual TypeOfFood TypeOfFood { get; set; }	
  
    [Display(Name = "FoodCat_Label_FoodMinerals")]	
    public virtual ICollection<FoodMineral> FoodMinerals { get; set; }	
    }
}

