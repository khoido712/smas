

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EvaluationDevelopmentGroupMetadata))]
public partial class EvaluationDevelopmentGroup { }

public partial class EvaluationDevelopmentGroupMetadata
    {
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_EvaluationDevelopmentGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationDevelopmentGroupID { get; set; }	
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_EvaluationDevelopmentGroupName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EvaluationDevelopmentGroupName { get; set; }	
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EvaluationDevelopmentGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "EvaluationDevelopmentGroup_Label_EvaluationDevelopments")]	
    public virtual ICollection<EvaluationDevelopment> EvaluationDevelopments { get; set; }	
    }
}

