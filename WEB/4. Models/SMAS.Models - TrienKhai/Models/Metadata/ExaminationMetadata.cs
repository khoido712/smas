

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ExaminationMetadata))]
public partial class Examination { }

public partial class ExaminationMetadata
    {
  
    [Display(Name = "Examination_Label_ExaminationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationID { get; set; }	
  
    [Display(Name = "Examination_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "Examination_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "Examination_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "Examination_Label_Title")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "Examination_Label_Location")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Location { get; set; }	
  
    [Display(Name = "Examination_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Examination_Label_CurrentStage")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CurrentStage { get; set; }	
  
    [Display(Name = "Examination_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "Examination_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "Examination_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "Examination_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Examination_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "Examination_Label_UsingSeparateList")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UsingSeparateList { get; set; }	
  
    [Display(Name = "Examination_Label_CandidateFromMultipleLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CandidateFromMultipleLevel { get; set; }	
  
    [Display(Name = "Examination_Label_MarkImportType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkImportType { get; set; }	
  
    [Display(Name = "Examination_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "Examination_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Examination_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Examination_Label_Candidates")]	
    public virtual ICollection<Candidate> Candidates { get; set; }	
  
    [Display(Name = "Examination_Label_CandidateAbsences")]	
    public virtual ICollection<CandidateAbsence> CandidateAbsences { get; set; }	
  
    [Display(Name = "Examination_Label_DetachableHeadBags")]	
    public virtual ICollection<DetachableHeadBag> DetachableHeadBags { get; set; }	
  
    [Display(Name = "Examination_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "Examination_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "Examination_Label_ExaminationRooms")]	
    public virtual ICollection<ExaminationRoom> ExaminationRooms { get; set; }	
  
    [Display(Name = "Examination_Label_ExaminationSubjects")]	
    public virtual ICollection<ExaminationSubject> ExaminationSubjects { get; set; }	
  
    [Display(Name = "Examination_Label_Invigilators")]	
    public virtual ICollection<Invigilator> Invigilators { get; set; }	
  
    [Display(Name = "Examination_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
  
    [Display(Name = "Examination_Label_ViolatedInvigilators")]	
    public virtual ICollection<ViolatedInvigilator> ViolatedInvigilators { get; set; }	
    }
}

