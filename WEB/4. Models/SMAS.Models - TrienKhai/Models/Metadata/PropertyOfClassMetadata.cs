

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PropertyOfClassMetadata))]
public partial class PropertyOfClass { }

public partial class PropertyOfClassMetadata
    {
  
    [Display(Name = "PropertyOfClass_Label_PropertyOfClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PropertyOfClassID { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_ClassPropertyCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassPropertyCatID { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "PropertyOfClass_Label_ClassPropertyCat")]	
    public virtual ClassPropertyCat ClassPropertyCat { get; set; }	
  
    [Display(Name = "PropertyOfClass_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

