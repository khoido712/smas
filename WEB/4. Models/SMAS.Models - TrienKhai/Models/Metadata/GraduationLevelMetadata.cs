

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(GraduationLevelMetadata))]
public partial class GraduationLevel { }

public partial class GraduationLevelMetadata
    {
  
    [Display(Name = "GraduationLevel_Label_GraduationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GraduationLevelID { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_FullfilmentStatus")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FullfilmentStatus { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "GraduationLevel_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "GraduationLevel_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
    }
}

