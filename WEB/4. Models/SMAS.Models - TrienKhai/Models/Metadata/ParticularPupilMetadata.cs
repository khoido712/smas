

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ParticularPupilMetadata))]
public partial class ParticularPupil { }

public partial class ParticularPupilMetadata
    {
  
    [Display(Name = "ParticularPupil_Label_ParticularPupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParticularPupilID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_RecordedDate")]	
    public Nullable<System.DateTime> RecordedDate { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_UpdatedDate")]	
    public Nullable<System.DateTime> UpdatedDate { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_RealUpdatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime RealUpdatedDate { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ParticularPupil_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_ParticularPupilCharacteristics")]	
    public virtual ICollection<ParticularPupilCharacteristic> ParticularPupilCharacteristics { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ParticularPupil_Label_ParticularPupilTreatments")]	
    public virtual ICollection<ParticularPupilTreatment> ParticularPupilTreatments { get; set; }	
    }
}

