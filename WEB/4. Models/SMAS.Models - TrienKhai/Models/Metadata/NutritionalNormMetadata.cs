

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(NutritionalNormMetadata))]
public partial class NutritionalNorm { }

public partial class NutritionalNormMetadata
    {
  
    [Display(Name = "NutritionalNorm_Label_NutritionalNormID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NutritionalNormID { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_EffectDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime EffectDate { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_DailyDemandFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyDemandFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_DailyDemandTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyDemandTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_RateFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RateFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_RateTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RateTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_ProteinFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProteinFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_ProteinTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProteinTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_AnimalProteinFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AnimalProteinFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_AnimalProteinTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AnimalProteinTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_PlantProteinFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PlantProteinFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_PlantProteinTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PlantProteinTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_FatFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FatFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_FatTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FatTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_AnimalFatFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AnimalFatFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_AnimalFatTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AnimalFatTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_PlantFatFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PlantFatFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_PlantFatTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PlantFatTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_SugarFrom")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SugarFrom { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_SugarTo")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SugarTo { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_EatingGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupID { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "NutritionalNorm_Label_EatingGroup")]	
    public virtual EatingGroup EatingGroup { get; set; }	
  
    [Display(Name = "NutritionalNorm_Label_NutritionalNormMinerals")]	
    public virtual ICollection<NutritionalNormMineral> NutritionalNormMinerals { get; set; }	
    }
}

