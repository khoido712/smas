

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_PupilConductTertiaryMetadata))]
public partial class M_PupilConductTertiary { }

public partial class M_PupilConductTertiaryMetadata
    {
  
    [Display(Name = "M_PupilConductTertiary_Label_PupilConductTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilConductTertiaryID { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_Semester1")]	
    public Nullable<int> Semester1 { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_Semester2")]	
    public Nullable<int> Semester2 { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_Both")]	
    public Nullable<int> Both { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_MustRetest")]	
    public Nullable<bool> MustRetest { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_ReTest")]	
    public Nullable<int> ReTest { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_PupilConductTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

