

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(DiseasesTypeMetadata))]
    public partial class DiseasesType { }

    public partial class DiseasesTypeMetadata
    {

        [Display(Name = "DiseasesType_Label_DiseasesTypeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int DiseasesTypeID { get; set; }

        [Display(Name = "DiseasesType_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [Display(Name = "DiseasesType_Label_Description")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }

        [Display(Name = "DiseasesType_Label_IsActive")]
        public Nullable<bool> IsActive { get; set; }
    }
}

