

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CalendarMetadata))]
public partial class Calendar { }

public partial class CalendarMetadata
    {
  
    [Display(Name = "Calendar_Label_CalendarID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CalendarID { get; set; }	
  
    [Display(Name = "Calendar_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "Calendar_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "Calendar_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "Calendar_Label_DayOfWeek")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DayOfWeek { get; set; }	
  
    [Display(Name = "Calendar_Label_Section")]	
    public byte Section { get; set; }	
  
    [Display(Name = "Calendar_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "Calendar_Label_SubjectOrder")]	
    public Nullable<int> SubjectOrder { get; set; }	
  
    [Display(Name = "Calendar_Label_NumberOfPeriod")]	
    public Nullable<int> NumberOfPeriod { get; set; }	
  
    [Display(Name = "Calendar_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "Calendar_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Calendar_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Calendar_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Calendar_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "Calendar_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "Calendar_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

