

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSParentSubscriberMetadata))]
public partial class SMSParentSubscriber { }

public partial class SMSParentSubscriberMetadata
    {
  
    [Display(Name = "SMSParentSubscriber_Label_SMSParentSubscriberID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentSubscriberID { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SMSParentContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentContractID { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_Subscriber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Subscriber { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SubscriberType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubscriberType { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_PackageID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PackageID { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SubscriptionTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime SubscriptionTime { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_TypeOfSubcriptionTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeOfSubcriptionTime { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SubscriptionStatus")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubscriptionStatus { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_Money")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal Money { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_RegistrationType")]	
    public Nullable<int> RegistrationType { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_CreatedTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedTime { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_UpdatedTime")]	
    public Nullable<System.DateTime> UpdatedTime { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_Status")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Status { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SubscriberName")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SubscriberName { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SMSParentSubscriber_Label_SMSParentContract")]	
    public virtual SMSParentContract SMSParentContract { get; set; }	
  
    [Display(Name = "SMSParentSubscriber_Label_SMSParentNumSMSSents")]	
    public virtual ICollection<SMSParentNumSMSSent> SMSParentNumSMSSents { get; set; }	
    }
}

