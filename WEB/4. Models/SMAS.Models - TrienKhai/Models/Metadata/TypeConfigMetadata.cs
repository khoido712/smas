

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TypeConfigMetadata))]
public partial class TypeConfig { }

public partial class TypeConfigMetadata
    {
  
    [Display(Name = "TypeConfig_Label_TypeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeConfigID { get; set; }	
  
    [Display(Name = "TypeConfig_Label_TypeConfigName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TypeConfigName { get; set; }	
  
    [Display(Name = "TypeConfig_Label_PositionOrder")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PositionOrder { get; set; }	
  
    [Display(Name = "TypeConfig_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "TypeConfig_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "TypeConfig_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "TypeConfig_Label_ClassificationCriterias")]	
    public virtual ICollection<ClassificationCriteria> ClassificationCriterias { get; set; }	
  
    [Display(Name = "TypeConfig_Label_ClassificationLevelHealths")]	
    public virtual ICollection<ClassificationLevelHealth> ClassificationLevelHealths { get; set; }	
  
    [Display(Name = "TypeConfig_Label_EvaluationConfigs")]	
    public virtual ICollection<EvaluationConfig> EvaluationConfigs { get; set; }	
  
    [Display(Name = "TypeConfig_Label_LevelHealthConfigs")]	
    public virtual ICollection<LevelHealthConfig> LevelHealthConfigs { get; set; }	
    }
}

