

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DailyMenuDetailMetadata))]
public partial class DailyMenuDetail { }

public partial class DailyMenuDetailMetadata
    {
  
    [Display(Name = "DailyMenuDetail_Label_DailyMenuDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuDetailID { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_DishName")]
    [StringLength(150,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DishName { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_DishID")]	
    public Nullable<int> DishID { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_DailyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuID { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_MealID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MealID { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DailyMenuDetail_Label_MealCat")]	
    public virtual MealCat MealCat { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_DailyMenu")]	
    public virtual DailyMenu DailyMenu { get; set; }	
  
    [Display(Name = "DailyMenuDetail_Label_DishCat")]	
    public virtual DishCat DishCat { get; set; }	
    }
}

