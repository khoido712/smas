

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ContactGroupMetadata))]
public partial class ContactGroup { }

public partial class ContactGroupMetadata
    {
  
    [Display(Name = "ContactGroup_Label_ContactGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContactGroupID { get; set; }	
  
    [Display(Name = "ContactGroup_Label_Name")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Name { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SMSCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SMSCode { get; set; }	
  
    [Display(Name = "ContactGroup_Label_IsLock")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsLock { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SchoolFacultyID")]	
    public Nullable<int> SchoolFacultyID { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SchoolProfileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolProfileID { get; set; }	
  
    [Display(Name = "ContactGroup_Label_IsDefault")]	
    public Nullable<bool> IsDefault { get; set; }	
  
    [Display(Name = "ContactGroup_Label_CreateUserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid CreateUserID { get; set; }	
  
    [Display(Name = "ContactGroup_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	

  
    [Display(Name = "ContactGroup_Label_aspnet_Membership")]	
    public virtual aspnet_Membership aspnet_Membership { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SMSCommunications")]	
    public virtual ICollection<SMSCommunication> SMSCommunications { get; set; }	
  
    [Display(Name = "ContactGroup_Label_SMSCommunicationGroups")]	
    public virtual ICollection<SMSCommunicationGroup> SMSCommunicationGroups { get; set; }	
  
    [Display(Name = "ContactGroup_Label_EmployeeContacts")]	
    public virtual ICollection<EmployeeContact> EmployeeContacts { get; set; }	
    }
}

