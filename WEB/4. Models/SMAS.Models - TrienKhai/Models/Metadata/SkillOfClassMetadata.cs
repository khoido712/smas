

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SkillOfClassMetadata))]
public partial class SkillOfClass { }

public partial class SkillOfClassMetadata
    {
  
    [Display(Name = "SkillOfClass_Label_SkillOfClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SkillOfClassID { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_SkillTitleID")]	
    public Nullable<int> SkillTitleID { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_Description")]	
    public string Description { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_Period")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Period { get; set; }	

  
    [Display(Name = "SkillOfClass_Label_SkillTitle")]	
    public virtual SkillTitle SkillTitle { get; set; }	
  
    [Display(Name = "SkillOfClass_Label_SkillOfPupils")]	
    public virtual ICollection<SkillOfPupil> SkillOfPupils { get; set; }	
    }
}

