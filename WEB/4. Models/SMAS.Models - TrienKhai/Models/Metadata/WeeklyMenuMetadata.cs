

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(WeeklyMenuMetadata))]
public partial class WeeklyMenu { }

public partial class WeeklyMenuMetadata
    {
  
    [Display(Name = "WeeklyMenu_Label_WeeklyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WeeklyMenuID { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_WeeklyMenuCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string WeeklyMenuCode { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_MenuType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuType { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_Note")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Note { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_StartDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime StartDate { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_EatingGroupID")]	
    public Nullable<int> EatingGroupID { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "WeeklyMenu_Label_EatingGroup")]	
    public virtual EatingGroup EatingGroup { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "WeeklyMenu_Label_WeeklyMenuDetails")]	
    public virtual ICollection<WeeklyMenuDetail> WeeklyMenuDetails { get; set; }	
    }
}

