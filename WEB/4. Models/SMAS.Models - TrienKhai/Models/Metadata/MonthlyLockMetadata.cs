

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MonthlyLockMetadata))]
public partial class MonthlyLock { }

public partial class MonthlyLockMetadata
    {
  
    [Display(Name = "MonthlyLock_Label_MonthlyLockID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonthlyLockID { get; set; }	
  
    [Display(Name = "MonthlyLock_Label_LockedMonth")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime LockedMonth { get; set; }	
  
    [Display(Name = "MonthlyLock_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
    }
}

