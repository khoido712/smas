

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ParticularPupilTreatmentMetadata))]
public partial class ParticularPupilTreatment { }

public partial class ParticularPupilTreatmentMetadata
    {
  
    [Display(Name = "ParticularPupilTreatment_Label_ParticularPupilTreatmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParticularPupilTreatmentID { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_ParticularPupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParticularPupilID { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_TreatmentResolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TreatmentResolution { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_Result")]
    [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Result { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_AppliedDate")]	
    public Nullable<System.DateTime> AppliedDate { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_UpdatedDate")]	
    public Nullable<System.DateTime> UpdatedDate { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ParticularPupilTreatment_Label_ParticularPupil")]	
    public virtual ParticularPupil ParticularPupil { get; set; }	
  
    [Display(Name = "ParticularPupilTreatment_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
    }
}

