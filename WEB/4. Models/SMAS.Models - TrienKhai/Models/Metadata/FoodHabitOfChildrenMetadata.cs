

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(FoodHabitOfChildrenMetadata))]
public partial class FoodHabitOfChildren { }

public partial class FoodHabitOfChildrenMetadata
    {
  
    [Display(Name = "FoodHabitOfChildren_Label_FoodHabitOfChildrenID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodHabitOfChildrenID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_HabitType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HabitType { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_TypeOfDishID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeOfDishID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_M_TypeOfDishID")]	
    public Nullable<int> M_TypeOfDishID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_DishOfChildrenID")]	
    public Nullable<int> DishOfChildrenID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "FoodHabitOfChildren_Label_TypeOfDish")]	
    public virtual TypeOfDish TypeOfDish { get; set; }	
  
    [Display(Name = "FoodHabitOfChildren_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
    }
}

