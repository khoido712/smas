

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(TeacherGradeMetadata))]
public partial class TeacherGrade { }

public partial class TeacherGradeMetadata
    {
  
    [Display(Name = "TeacherGrade_Label_TeacherGradeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherGradeID { get; set; }	
  
    [Display(Name = "TeacherGrade_Label_GradeResolution")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string GradeResolution { get; set; }	
  
    [Display(Name = "TeacherGrade_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "TeacherGrade_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "TeacherGrade_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "TeacherGrade_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "TeacherGrade_Label_TeacherGradings")]	
    public virtual ICollection<TeacherGrading> TeacherGradings { get; set; }	
    }
}

