

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PupilProfileMetadata))]
public partial class PupilProfile { }

public partial class PupilProfileMetadata
    {
  
    [Display(Name = "PupilProfile_Label_PupilProfileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilProfileID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CurrentClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CurrentClassID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CurrentSchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CurrentSchoolID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CurrentAcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CurrentAcademicYearID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_AreaID")]	
    public Nullable<int> AreaID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_DistrictID")]	
    public Nullable<int> DistrictID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CommuneID")]	
    public Nullable<int> CommuneID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_EthnicID")]	
    public Nullable<int> EthnicID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ReligionID")]	
    public Nullable<int> ReligionID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PolicyTargetID")]	
    public Nullable<int> PolicyTargetID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FamilyTypeID")]	
    public Nullable<int> FamilyTypeID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PriorityTypeID")]	
    public Nullable<int> PriorityTypeID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PreviousGraduationLevelID")]	
    public Nullable<int> PreviousGraduationLevelID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PupilCode")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Name")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Name { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FullName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FullName { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Genre")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Genre { get; set; }	
  
    [Display(Name = "PupilProfile_Label_BirthDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime BirthDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_BirthPlace")]
    [StringLength(800,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string BirthPlace { get; set; }	
  
    [Display(Name = "PupilProfile_Label_HomeTown")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string HomeTown { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Image")]	
    public byte[] Image { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Telephone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Telephone { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Mobile")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Mobile { get; set; }	
  
    [Display(Name = "PupilProfile_Label_Email")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Email { get; set; }	
  
    [Display(Name = "PupilProfile_Label_TempResidentalAddress")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string TempResidentalAddress { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PermanentResidentalAddress")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PermanentResidentalAddress { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsResidentIn")]	
    public Nullable<bool> IsResidentIn { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsDisabled")]	
    public Nullable<bool> IsDisabled { get; set; }	
  
    [Display(Name = "PupilProfile_Label_DisabledTypeID")]	
    public Nullable<int> DisabledTypeID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_DisabledSeverity")]
    [StringLength(360,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DisabledSeverity { get; set; }	
  
    [Display(Name = "PupilProfile_Label_BloodType")]	
    public Nullable<int> BloodType { get; set; }	
  
    [Display(Name = "PupilProfile_Label_EnrolmentMark")]	
    public Nullable<decimal> EnrolmentMark { get; set; }	
  
    [Display(Name = "PupilProfile_Label_EnrolmentDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime EnrolmentDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ForeignLanguageTraining")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ForeignLanguageTraining { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsYoungPioneerMember")]	
    public Nullable<bool> IsYoungPioneerMember { get; set; }	
  
    [Display(Name = "PupilProfile_Label_YoungPioneerJoinedDate")]	
    public Nullable<System.DateTime> YoungPioneerJoinedDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_YoungPioneerJoinedPlace")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string YoungPioneerJoinedPlace { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsYouthLeageMember")]	
    public Nullable<bool> IsYouthLeageMember { get; set; }	
  
    [Display(Name = "PupilProfile_Label_YouthLeagueJoinedDate")]	
    public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_YouthLeagueJoinedPlace")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string YouthLeagueJoinedPlace { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsCommunistPartyMember")]	
    public Nullable<bool> IsCommunistPartyMember { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CommunistPartyJoinedDate")]	
    public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CommunistPartyJoinedPlace")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CommunistPartyJoinedPlace { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FatherFullName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FatherFullName { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FatherBirthDate")]	
    public Nullable<System.DateTime> FatherBirthDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FatherJob")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FatherJob { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FatherMobile")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FatherMobile { get; set; }	
  
    [Display(Name = "PupilProfile_Label_MotherFullName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MotherFullName { get; set; }	
  
    [Display(Name = "PupilProfile_Label_MotherBirthDate")]	
    public Nullable<System.DateTime> MotherBirthDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_MotherJob")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MotherJob { get; set; }	
  
    [Display(Name = "PupilProfile_Label_MotherMobile")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MotherMobile { get; set; }	
  
    [Display(Name = "PupilProfile_Label_SponsorFullName")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SponsorFullName { get; set; }	
  
    [Display(Name = "PupilProfile_Label_SponsorBirthDate")]	
    public Nullable<System.DateTime> SponsorBirthDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_SponsorJob")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SponsorJob { get; set; }	
  
    [Display(Name = "PupilProfile_Label_SponsorMobile")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SponsorMobile { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ProfileStatus")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProfileStatus { get; set; }	
  
    [Display(Name = "PupilProfile_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "PupilProfile_Label_HeightAtBirth")]	
    public Nullable<decimal> HeightAtBirth { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FatherEmail")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FatherEmail { get; set; }	
  
    [Display(Name = "PupilProfile_Label_MotherEmail")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MotherEmail { get; set; }	
  
    [Display(Name = "PupilProfile_Label_SponsorEmail")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SponsorEmail { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ChildOrder")]	
    public Nullable<int> ChildOrder { get; set; }	
  
    [Display(Name = "PupilProfile_Label_NumberOfChild")]	
    public Nullable<int> NumberOfChild { get; set; }	
  
    [Display(Name = "PupilProfile_Label_EatingHabit")]	
    public Nullable<int> EatingHabit { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ReactionTrainingHabit")]	
    public Nullable<int> ReactionTrainingHabit { get; set; }	
  
    [Display(Name = "PupilProfile_Label_AttitudeInStrangeScene")]	
    public Nullable<int> AttitudeInStrangeScene { get; set; }	
  
    [Display(Name = "PupilProfile_Label_OtherHabit")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string OtherHabit { get; set; }	
  
    [Display(Name = "PupilProfile_Label_FavoriteToy")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FavoriteToy { get; set; }	
  
    [Display(Name = "PupilProfile_Label_EvaluationConfigID")]	
    public Nullable<int> EvaluationConfigID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_WeightAtBirth")]	
    public Nullable<decimal> WeightAtBirth { get; set; }	
  
    [Display(Name = "PupilProfile_Label_PolicyRegimeID")]	
    public Nullable<int> PolicyRegimeID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_IsSupportForLearning")]	
    public Nullable<bool> IsSupportForLearning { get; set; }	
  
    [Display(Name = "PupilProfile_Label_ClassType")]	
    public Nullable<int> ClassType { get; set; }	
  
    [Display(Name = "PupilProfile_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PupilProfile_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
  
    }
}

