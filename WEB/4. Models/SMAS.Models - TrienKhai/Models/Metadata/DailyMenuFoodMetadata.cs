

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DailyMenuFoodMetadata))]
public partial class DailyMenuFood { }

public partial class DailyMenuFoodMetadata
    {
  
    [Display(Name = "DailyMenuFood_Label_DailyMenuFoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuFoodID { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_Weight")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public decimal Weight { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_PricePerOnce")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PricePerOnce { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_DailyMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DailyMenuID { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_FoodID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FoodID { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DailyMenuFood_Label_FoodCat")]	
    public virtual FoodCat FoodCat { get; set; }	
  
    [Display(Name = "DailyMenuFood_Label_DailyMenu")]	
    public virtual DailyMenu DailyMenu { get; set; }	
    }
}

