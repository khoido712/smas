

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ProvinceSubjectMetadata))]
public partial class ProvinceSubject { }

public partial class ProvinceSubjectMetadata
    {
  
    [Display(Name = "ProvinceSubject_Label_ProvinceSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceSubjectID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_AcademicYearOfProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearOfProvinceID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_IsRegularEducation")]	
    public Nullable<bool> IsRegularEducation { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_Coefficient")]	
    public Nullable<int> Coefficient { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_AppliedType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedType { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_IsCommenting")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int IsCommenting { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_SectionPerWeekFirstSemester")]	
    public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_SectionPerWeekSecondSemester")]	
    public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ProvinceSubject_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "ProvinceSubject_Label_AcademicYearOfProvince")]	
    public virtual AcademicYearOfProvince AcademicYearOfProvince { get; set; }	
    }
}

