

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkRecordMetadata))]
public partial class M_MarkRecord { }

public partial class M_MarkRecordMetadata
    {
  
    [Display(Name = "M_MarkRecord_Label_MarkRecordID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkRecordID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public short SubjectID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_MarkTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public short MarkTypeID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public short Year { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_Semester")]	
    public Nullable<byte> Semester { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_PeriodID")]	
    public Nullable<int> PeriodID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_Title")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_Mark")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Mark { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_OrderNumber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte OrderNumber { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_MarkedDate")]	
    public Nullable<System.DateTime> MarkedDate { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_RowNum__")]	
    public Nullable<int> RowNum__ { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "M_MarkRecord_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "M_MarkRecord_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

