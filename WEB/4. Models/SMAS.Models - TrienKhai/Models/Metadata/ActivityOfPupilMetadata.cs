

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityOfPupilMetadata))]
public partial class ActivityOfPupil { }

public partial class ActivityOfPupilMetadata
    {
  
    [Display(Name = "ActivityOfPupil_Label_ActivityOfPupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityOfPupilID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_CommentOfTeacher")]
    [StringLength(2000,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CommentOfTeacher { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ActivityTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityTypeID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_IsSMS")]	
    public Nullable<bool> IsSMS { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_CreateDate")]	
    public Nullable<System.DateTime> CreateDate { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ActivityOfClassID")]	
    public Nullable<int> ActivityOfClassID { get; set; }	

  
    [Display(Name = "ActivityOfPupil_Label_ActivityOfClass")]	
    public virtual ActivityOfClass ActivityOfClass { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ActivityType")]	
    public virtual ActivityType ActivityType { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "ActivityOfPupil_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

