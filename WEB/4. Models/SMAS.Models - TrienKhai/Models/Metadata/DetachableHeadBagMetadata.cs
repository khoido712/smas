

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DetachableHeadBagMetadata))]
public partial class DetachableHeadBag { }

public partial class DetachableHeadBagMetadata
    {
  
    [Display(Name = "DetachableHeadBag_Label_DetachableHeadBagID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DetachableHeadBagID { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_ExaminationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationID { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_EducationLevelID")]	
    public Nullable<int> EducationLevelID { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_ExaminationSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationSubjectID { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_BagTitle")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string BagTitle { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_Prefix")]
    [StringLength(5,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Prefix { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DetachableHeadBag_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_ExaminationSubject")]	
    public virtual ExaminationSubject ExaminationSubject { get; set; }	
  
    [Display(Name = "DetachableHeadBag_Label_DetachableHeadMappings")]	
    public virtual ICollection<DetachableHeadMapping> DetachableHeadMappings { get; set; }	
    }
}

