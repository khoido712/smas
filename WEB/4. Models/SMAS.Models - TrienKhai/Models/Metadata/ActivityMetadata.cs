

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ActivityMetadata))]
public partial class Activity { }

public partial class ActivityMetadata
    {
  
    [Display(Name = "Activity_Label_ActivityID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityID { get; set; }	
  
    [Display(Name = "Activity_Label_ActivityPlanID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityPlanID { get; set; }	
  
    [Display(Name = "Activity_Label_DayOfWeek")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte DayOfWeek { get; set; }	
  
    [Display(Name = "Activity_Label_Section")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Section { get; set; }	
  
    [Display(Name = "Activity_Label_ActivityName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ActivityName { get; set; }	
  
    [Display(Name = "Activity_Label_FromTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.TimeSpan FromTime { get; set; }	
  
    [Display(Name = "Activity_Label_ToTime")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.TimeSpan ToTime { get; set; }	
  
    [Display(Name = "Activity_Label_ActivityTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ActivityTypeID { get; set; }	
  
    [Display(Name = "Activity_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Activity_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Activity_Label_ActivityPlan")]	
    public virtual ActivityPlan ActivityPlan { get; set; }	
  
    [Display(Name = "Activity_Label_ActivityType")]	
    public virtual ActivityType ActivityType { get; set; }	
    }
}

