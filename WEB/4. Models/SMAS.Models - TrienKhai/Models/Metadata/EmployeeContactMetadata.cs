

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeContactMetadata))]
public partial class EmployeeContact { }

public partial class EmployeeContactMetadata
    {
  
    [Display(Name = "EmployeeContact_Label_EmployeeContactID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeContactID { get; set; }	
  
    [Display(Name = "EmployeeContact_Label_EmployeeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeID { get; set; }	
  
    [Display(Name = "EmployeeContact_Label_ContactGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContactGroupID { get; set; }	
  
    [Display(Name = "EmployeeContact_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "EmployeeContact_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	

  
    [Display(Name = "EmployeeContact_Label_ContactGroup")]	
    public virtual ContactGroup ContactGroup { get; set; }	
  
    [Display(Name = "EmployeeContact_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
    }
}

