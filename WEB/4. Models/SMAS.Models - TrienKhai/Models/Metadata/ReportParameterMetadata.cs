

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ReportParameterMetadata))]
public partial class ReportParameter { }

public partial class ReportParameterMetadata
    {
  
    [Display(Name = "ReportParameter_Label_ReportParameterID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ReportParameterID { get; set; }	
  
    [Display(Name = "ReportParameter_Label_ReportCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public byte[] ReportCode { get; set; }	
  
    [Display(Name = "ReportParameter_Label_ParameterName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ParameterName { get; set; }	
  
    [Display(Name = "ReportParameter_Label_ParameterOrder")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ParameterOrder { get; set; }	
  
    [Display(Name = "ReportParameter_Label_IsOptional")]	
    public Nullable<bool> IsOptional { get; set; }	
  
    [Display(Name = "ReportParameter_Label_DefaultValue")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DefaultValue { get; set; }	
  
    [Display(Name = "ReportParameter_Label_ParameterType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ParameterType { get; set; }	
    }
}

