

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(InvigilatorMetadata))]
public partial class Invigilator { }

public partial class InvigilatorMetadata
    {
  
    [Display(Name = "Invigilator_Label_InvigilatorID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InvigilatorID { get; set; }	
  
    [Display(Name = "Invigilator_Label_ExaminationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExaminationID { get; set; }	
  
    [Display(Name = "Invigilator_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "Invigilator_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "Invigilator_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "Invigilator_Label_Examination")]	
    public virtual Examination Examination { get; set; }	
  
    [Display(Name = "Invigilator_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "Invigilator_Label_InvigilatorAssignments")]	
    public virtual ICollection<InvigilatorAssignment> InvigilatorAssignments { get; set; }	
  
    [Display(Name = "Invigilator_Label_ViolatedInvigilators")]	
    public virtual ICollection<ViolatedInvigilator> ViolatedInvigilators { get; set; }	
    }
}

