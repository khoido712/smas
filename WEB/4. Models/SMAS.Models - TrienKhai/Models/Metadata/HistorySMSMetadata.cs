

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HistorySMSMetadata))]
public partial class HistorySMS { }

public partial class HistorySMSMetadata
    {
  
    [Display(Name = "HistorySMS_Label_HistorySMSID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HistorySMSID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_Mobile")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Mobile { get; set; }	
  
    [Display(Name = "HistorySMS_Label_FullName")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string FullName { get; set; }	
  
    [Display(Name = "HistorySMS_Label_TypeID")]	
    public Nullable<int> TypeID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_ReceiveType")]	
    public Nullable<int> ReceiveType { get; set; }	
  
    [Display(Name = "HistorySMS_Label_CreateDate")]	
    public Nullable<System.DateTime> CreateDate { get; set; }	
  
    [Display(Name = "HistorySMS_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "HistorySMS_Label_Content")]
    [StringLength(2048,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Content { get; set; }	
  
    [Display(Name = "HistorySMS_Label_Status")]	
    public Nullable<bool> Status { get; set; }	
  
    [Display(Name = "HistorySMS_Label_Sender")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Sender { get; set; }	
  
    [Display(Name = "HistorySMS_Label_FlagID")]	
    public Nullable<int> FlagID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_ContentCount")]	
    public Nullable<int> ContentCount { get; set; }	
  
    [Display(Name = "HistorySMS_Label_CommandCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CommandCode { get; set; }	
  
    [Display(Name = "HistorySMS_Label_CPCode")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string CPCode { get; set; }	
  
    [Display(Name = "HistorySMS_Label_EmployeeID")]	
    public Nullable<int> EmployeeID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_PupilID")]	
    public Nullable<int> PupilID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_ServiceID")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ServiceID { get; set; }	
  
    [Display(Name = "HistorySMS_Label_ShortContent")]
    [StringLength(1280,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ShortContent { get; set; }	
  
    [Display(Name = "HistorySMS_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	

  
    [Display(Name = "HistorySMS_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "HistorySMS_Label_SMSCommunications")]	
    public virtual ICollection<SMSCommunication> SMSCommunications { get; set; }	
  
    [Display(Name = "HistorySMS_Label_MTs")]	
    public virtual ICollection<MT> MTs { get; set; }	
    }
}

