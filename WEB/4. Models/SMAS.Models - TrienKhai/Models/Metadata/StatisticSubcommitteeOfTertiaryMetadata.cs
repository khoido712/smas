

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(StatisticSubcommitteeOfTertiaryMetadata))]
public partial class StatisticSubcommitteeOfTertiary { }

public partial class StatisticSubcommitteeOfTertiaryMetadata
    {
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_StatisticSubcommitteeOfTertiaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int StatisticSubcommitteeOfTertiaryID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SubCommitteeID")]	
    public Nullable<int> SubCommitteeID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_TotalClass")]	
    public Nullable<int> TotalClass { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_TotalPupil")]	
    public Nullable<int> TotalPupil { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SpecicalType")]	
    public Nullable<int> SpecicalType { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SentDate")]	
    public Nullable<System.DateTime> SentDate { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SubCommittee")]	
    public virtual SubCommittee SubCommittee { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "StatisticSubcommitteeOfTertiary_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

