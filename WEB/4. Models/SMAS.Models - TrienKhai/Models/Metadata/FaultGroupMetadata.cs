

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(FaultGroupMetadata))]
public partial class FaultGroup { }

public partial class FaultGroupMetadata
    {
  
    [Display(Name = "FaultGroup_Label_FaultGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int FaultGroupID { get; set; }	
  
    [Display(Name = "FaultGroup_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "FaultGroup_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "FaultGroup_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "FaultGroup_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "FaultGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "FaultGroup_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "FaultGroup_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }

    [Display(Name = "FaultGroup_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    public int SchoolID { get; set; }

    [Display(Name = "FaultGroup_Label_SchoolProfile")]
    public virtual SchoolProfile SchoolProfile { get; set; }	

    [Display(Name = "FaultGroup_Label_FaultCriterias")]	
    public virtual ICollection<FaultCriteria> FaultCriterias { get; set; }	
    }
}

