

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HeadTeacherSubstitutionMetadata))]
public partial class HeadTeacherSubstitution { }

public partial class HeadTeacherSubstitutionMetadata
    {
  
    [Display(Name = "HeadTeacherSubstitution_Label_HeadTeacherSubstitutionID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HeadTeacherSubstitutionID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_HeadTeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HeadTeacherID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_SubstituedHeadTeacher")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubstituedHeadTeacher { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_AssignedDate")]	
    public Nullable<System.DateTime> AssignedDate { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_M_OldExtraTeacherID")]	
    public Nullable<int> M_OldExtraTeacherID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_M_OldHeadTeacherID")]	
    public Nullable<int> M_OldHeadTeacherID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "HeadTeacherSubstitution_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_Employee1")]	
    public virtual Employee Employee1 { get; set; }	
  
    [Display(Name = "HeadTeacherSubstitution_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

