

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(JudgeRecordMetadata))]
    public partial class JudgeRecord { }

    public partial class JudgeRecordMetadata
    {

        [Display(Name = "JudgeRecord_Label_JudgeRecordID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public long JudgeRecordID { get; set; }

        [Display(Name = "JudgeRecord_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [Display(Name = "JudgeRecord_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [Display(Name = "JudgeRecord_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolID { get; set; }

        [Display(Name = "JudgeRecord_Label_AcademicYearID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int AcademicYearID { get; set; }

        [Display(Name = "JudgeRecord_Label_SubjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID { get; set; }

        [Display(Name = "JudgeRecord_Label_MarkTypeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int MarkTypeID { get; set; }

        [Display(Name = "JudgeRecord_Label_Semester")]
        public Nullable<int> Semester { get; set; }

        [Display(Name = "JudgeRecord_Label_Judgement")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Judgement { get; set; }

        [Display(Name = "JudgeRecord_Label_ReTestJudgement")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ReTestJudgement { get; set; }

        [Display(Name = "JudgeRecord_Label_OrderNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int OrderNumber { get; set; }

        [Display(Name = "JudgeRecord_Label_MarkedDate")]
        public Nullable<System.DateTime> MarkedDate { get; set; }

        [Display(Name = "JudgeRecord_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "JudgeRecord_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "JudgeRecord_Label_Title")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Title { get; set; }



        [Display(Name = "JudgeRecord_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "JudgeRecord_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }

        [Display(Name = "JudgeRecord_Label_IsOldData")]
        public Nullable<bool> IsOldData { get; set; }

    }
}

