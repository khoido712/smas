

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkOfSemesterSecondaryMetadata))]
public partial class M_MarkOfSemesterSecondary { }

public partial class M_MarkOfSemesterSecondaryMetadata
    {
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_MarkOfSemesterID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkOfSemesterID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Semester1")]	
    public Nullable<decimal> Semester1 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Capacity1")]	
    public Nullable<byte> Capacity1 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Semester2")]	
    public Nullable<decimal> Semester2 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Capacity2")]	
    public Nullable<byte> Capacity2 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Both")]	
    public Nullable<decimal> Both { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_CapacityID")]	
    public Nullable<byte> CapacityID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_AfterTestAgain")]	
    public Nullable<decimal> AfterTestAgain { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_CapacityAfterTestAgain")]	
    public Nullable<byte> CapacityAfterTestAgain { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_MarkAllSubject")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MarkAllSubject { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_MarkAllSubject_1")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MarkAllSubject_1 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_MarkAllSubject_2")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MarkAllSubject_2 { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_PatternSMS_M_C_R")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS_M_C_R { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_PatternSMS")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_BothNotRetest")]	
    public Nullable<decimal> BothNotRetest { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_CapacityNotRetest")]	
    public Nullable<byte> CapacityNotRetest { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkOfSemesterSecondary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

