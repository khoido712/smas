

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassificationCriteriaDetailMetadata))]
public partial class ClassificationCriteriaDetail { }

public partial class ClassificationCriteriaDetailMetadata
    {
  
    [Display(Name = "ClassificationCriteriaDetail_Label_ClassificationCriteriaDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassificationCriteriaDetailID { get; set; }	
  
    [Display(Name = "ClassificationCriteriaDetail_Label_ClassificationCriteriaID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassificationCriteriaID { get; set; }	
  
    [Display(Name = "ClassificationCriteriaDetail_Label_Month")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Month { get; set; }	
  
    [Display(Name = "ClassificationCriteriaDetail_Label_IndexValue")]	
    public Nullable<decimal> IndexValue { get; set; }	
  
    [Display(Name = "ClassificationCriteriaDetail_Label_ClassificationLevelHealthID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassificationLevelHealthID { get; set; }	

  
    [Display(Name = "ClassificationCriteriaDetail_Label_ClassificationCriteria")]	
    public virtual ClassificationCriteria ClassificationCriteria { get; set; }	
  
    [Display(Name = "ClassificationCriteriaDetail_Label_ClassificationLevelHealth")]	
    public virtual ClassificationLevelHealth ClassificationLevelHealth { get; set; }	
    }
}

