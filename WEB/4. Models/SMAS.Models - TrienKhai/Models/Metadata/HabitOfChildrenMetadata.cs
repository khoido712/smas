

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(HabitOfChildrenMetadata))]
public partial class HabitOfChildren { }

public partial class HabitOfChildrenMetadata
    {
  
    [Display(Name = "HabitOfChildren_Label_HabitOfChildrenID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HabitOfChildrenID { get; set; }	
  
    [Display(Name = "HabitOfChildren_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "HabitOfChildren_Label_HabitDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HabitDetailID { get; set; }	
  
    [Display(Name = "HabitOfChildren_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "HabitOfChildren_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "HabitOfChildren_Label_HabitDetail")]	
    public virtual HabitDetail HabitDetail { get; set; }	
  
    [Display(Name = "HabitOfChildren_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
    }
}

