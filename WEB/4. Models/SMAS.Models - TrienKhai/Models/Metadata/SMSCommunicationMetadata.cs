

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSCommunicationMetadata))]
public partial class SMSCommunication { }

public partial class SMSCommunicationMetadata
    {
  
    [Display(Name = "SMSCommunication_Label_SMSCommunicationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSCommunicationID { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_HistorySMSID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int HistorySMSID { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_ContactGroupID")]	
    public Nullable<int> ContactGroupID { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_SenderGroup")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public long SenderGroup { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_Receiver")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Receiver { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_Sender")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Sender { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_IsAdmin")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsAdmin { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_CreateDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreateDate { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte Type { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_IsRead")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsRead { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_UpdateTime")]	
    public Nullable<System.DateTime> UpdateTime { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_LogMOID")]	
    public Nullable<int> LogMOID { get; set; }	

  
    [Display(Name = "SMSCommunication_Label_ContactGroup")]	
    public virtual ContactGroup ContactGroup { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_HistorySM")]	
    public virtual HistorySMS HistorySMS { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_LogMO")]	
    public virtual LogMO LogMO { get; set; }	
  
    [Display(Name = "SMSCommunication_Label_SMSCommunicationGroups")]	
    public virtual ICollection<SMSCommunicationGroup> SMSCommunicationGroups { get; set; }	
    }
}

