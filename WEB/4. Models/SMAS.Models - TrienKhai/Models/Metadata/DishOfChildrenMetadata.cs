

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DishOfChildrenMetadata))]
public partial class DishOfChildren { }

public partial class DishOfChildrenMetadata
    {
  
    [Display(Name = "DishOfChildren_Label_DishOfChildrenID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DishOfChildrenID { get; set; }	
  
    [Display(Name = "DishOfChildren_Label_Name")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Name { get; set; }	
  
    [Display(Name = "DishOfChildren_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "DishOfChildren_Label_OrderNumber")]	
    public Nullable<int> OrderNumber { get; set; }	
  
    [Display(Name = "DishOfChildren_Label_Type")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public byte Type { get; set; }	
  
    [Display(Name = "DishOfChildren_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

