

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SalaryLevelMetadata))]
public partial class SalaryLevel { }

public partial class SalaryLevelMetadata
    {
  
    [Display(Name = "SalaryLevel_Label_SalaryLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SalaryLevelID { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_Resolution")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_ScaleID")]	
    public Nullable<int> ScaleID { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_SubLevel")]	
    public Nullable<int> SubLevel { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_Coefficient")]	
    public Nullable<decimal> Coefficient { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "SalaryLevel_Label_EmployeeScale")]	
    public virtual EmployeeScale EmployeeScale { get; set; }	
  
    [Display(Name = "SalaryLevel_Label_EmployeeSalaries")]	
    public virtual ICollection<EmployeeSalary> EmployeeSalaries { get; set; }	
    }
}

