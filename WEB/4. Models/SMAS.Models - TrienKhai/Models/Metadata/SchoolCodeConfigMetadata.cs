

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolCodeConfigMetadata))]
public partial class SchoolCodeConfig { }

public partial class SchoolCodeConfigMetadata
    {
  
    [Display(Name = "SchoolCodeConfig_Label_SchoolCodeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolCodeConfigID { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_CodeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CodeConfigID { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_StartNumber")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int StartNumber { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_M_Type")]	
    public Nullable<int> M_Type { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SchoolCodeConfig_Label_CodeConfig")]	
    public virtual CodeConfig CodeConfig { get; set; }	
  
    [Display(Name = "SchoolCodeConfig_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

