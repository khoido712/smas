

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(CommuneMetadata))]
    public partial class Commune { }

    public partial class CommuneMetadata
    {

        [Display(Name = "Commune_Label_CommuneID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int CommuneID { get; set; }

        [Display(Name = "Commune_Label_DistrictID")]
        public Nullable<int> DistrictID { get; set; }

        [Display(Name = "Commune_Label_CommuneCode")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string CommuneCode { get; set; }

        [Display(Name = "Commune_Label_CommuneName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string CommuneName { get; set; }

        [Display(Name = "Commune_Label_ShortName")]
        [StringLength(20, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ShortName { get; set; }

        [Display(Name = "Commune_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }

        [Display(Name = "Commune_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "Commune_Label_IsActive")]
        public Nullable<bool> IsActive { get; set; }

        [Display(Name = "Commune_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}

