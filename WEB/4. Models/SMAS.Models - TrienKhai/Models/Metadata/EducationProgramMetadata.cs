

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EducationProgramMetadata))]
public partial class EducationProgram { }

public partial class EducationProgramMetadata
    {
  
    [Display(Name = "EducationProgram_Label_EducationProgramID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationProgramID { get; set; }	
  
    [Display(Name = "EducationProgram_Label_AppliedLevel")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AppliedLevel { get; set; }	
  
    [Display(Name = "EducationProgram_Label_TrainingTypeID")]	
    public Nullable<int> TrainingTypeID { get; set; }	
  
    [Display(Name = "EducationProgram_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "EducationProgram_Label_SubjectCatID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectCatID { get; set; }	
  
    [Display(Name = "EducationProgram_Label_NumberOfLesson")]	
    public Nullable<decimal> NumberOfLesson { get; set; }	
  
    [Display(Name = "EducationProgram_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "EducationProgram_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EducationProgram_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "EducationProgram_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "EducationProgram_Label_SubjectCat")]	
    public virtual SubjectCat SubjectCat { get; set; }	
  
    [Display(Name = "EducationProgram_Label_TrainingType")]	
    public virtual TrainingType TrainingType { get; set; }	
    }
}

