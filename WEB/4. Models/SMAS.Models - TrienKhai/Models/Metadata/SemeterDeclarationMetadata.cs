

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SemeterDeclarationMetadata))]
public partial class SemeterDeclaration { }

public partial class SemeterDeclarationMetadata
    {
  
    [Display(Name = "SemeterDeclaration_Label_SemeterDeclarationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SemeterDeclarationID { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_Resolution")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_Semester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Semester { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_InterviewMark")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int InterviewMark { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_WritingMark")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int WritingMark { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_TwiceCoeffiecientMark")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TwiceCoeffiecientMark { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_IsLock")]	
    public Nullable<bool> IsLock { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_CoefficientInterview")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CoefficientInterview { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_CoefficientWriting")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CoefficientWriting { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_CoefficientTwice")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CoefficientTwice { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_SemesterMark")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SemesterMark { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_CoefficientSemester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CoefficientSemester { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_AppliedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime AppliedDate { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SemeterDeclaration_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SemeterDeclaration_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

