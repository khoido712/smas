

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ExamViolationTypeMetadata))]
public partial class ExamViolationType { }

public partial class ExamViolationTypeMetadata
    {
  
    [Display(Name = "ExamViolationType_Label_ExamViolationTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ExamViolationTypeID { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_AppliedForCandidate")]	
    public Nullable<bool> AppliedForCandidate { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_AppliedForInvigilator")]	
    public Nullable<bool> AppliedForInvigilator { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_PenalizedMark")]	
    public Nullable<int> PenalizedMark { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_Penalization")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Penalization { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ExamViolationType_Label_ViolatedCandidates")]	
    public virtual ICollection<ViolatedCandidate> ViolatedCandidates { get; set; }	
  
    [Display(Name = "ExamViolationType_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

