

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(CodeConfigMetadata))]
public partial class CodeConfig { }

public partial class CodeConfigMetadata
    {
  
    [Display(Name = "CodeConfig_Label_CodeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CodeConfigID { get; set; }	
  
    [Display(Name = "CodeConfig_Label_CodeType")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CodeType { get; set; }	
  
    [Display(Name = "CodeConfig_Label_ProvinceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProvinceID { get; set; }	
  
    [Display(Name = "CodeConfig_Label_IsChoiceSchool")]	
    public Nullable<bool> IsChoiceSchool { get; set; }	
  
    [Display(Name = "CodeConfig_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "CodeConfig_Label_MiddleCodeType")]	
    public Nullable<int> MiddleCodeType { get; set; }	
  
    [Display(Name = "CodeConfig_Label_IsStringIdentify")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsStringIdentify { get; set; }	
  
    [Display(Name = "CodeConfig_Label_StringIdentify")]
    [StringLength(5,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string StringIdentify { get; set; }	
  
    [Display(Name = "CodeConfig_Label_NumberLength")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NumberLength { get; set; }	
  
    [Display(Name = "CodeConfig_Label_IsSchoolModify")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsSchoolModify { get; set; }	
  
    [Display(Name = "CodeConfig_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "CodeConfig_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "CodeConfig_Label_M_UnitID")]	
    public Nullable<int> M_UnitID { get; set; }	
  
    [Display(Name = "CodeConfig_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "CodeConfig_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "CodeConfig_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "CodeConfig_Label_SchoolCodeConfigs")]	
    public virtual ICollection<SchoolCodeConfig> SchoolCodeConfigs { get; set; }	
    }
}

