

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SpineTestMetadata))]
public partial class SpineTest { }

public partial class SpineTestMetadata
    {
  
    [Display(Name = "SpineTest_Label_SpineTestID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SpineTestID { get; set; }	
  
    [Display(Name = "SpineTest_Label_MonitoringBookID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MonitoringBookID { get; set; }	
  
    [Display(Name = "SpineTest_Label_IsDeformityOfTheSpine")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsDeformityOfTheSpine { get; set; }	
  
    [Display(Name = "SpineTest_Label_ExaminationStraight")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ExaminationStraight { get; set; }	
  
    [Display(Name = "SpineTest_Label_ExaminationItalic")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ExaminationItalic { get; set; }	
  
    [Display(Name = "SpineTest_Label_Other")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Other { get; set; }	
  
    [Display(Name = "SpineTest_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "SpineTest_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "SpineTest_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "SpineTest_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SpineTest_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SpineTest_Label_MonitoringBook")]	
    public virtual MonitoringBook MonitoringBook { get; set; }	
    }
}

