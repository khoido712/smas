

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
    [MetadataType(typeof(ApprenticeshipTrainingMetadata))]
    public partial class ApprenticeshipTraining { }

    public partial class ApprenticeshipTrainingMetadata
    {

        [Display(Name = "ApprenticeshipTraining_Label_ApprenticeshipTrainingID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ApprenticeshipTrainingID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ClassID")]
        public Nullable<int> ClassID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_SchoolID")]
        public Nullable<int> SchoolID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_AcademicYearID")]
        public Nullable<int> AcademicYearID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_EducationLevelID")]
        public Nullable<int> EducationLevelID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ApprenticeshipSubjectID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ApprenticeshipSubjectID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_Result")]
        [Range(0, 10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public Nullable<decimal> Result { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_Description")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PupilCode { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ActionMark")]
        [Range(0, 10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public Nullable<decimal> ActionMark { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_TheoryMark")]
        [Range(0, 10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public Nullable<decimal> TheoryMark { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_Ranking")]
        public Nullable<int> Ranking { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ApprenticeShipClassID")]
        public Nullable<int> ApprenticeShipClassID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_M_ProvinceID")]
        public Nullable<int> M_ProvinceID { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_M_OldID")]
        public Nullable<int> M_OldID { get; set; }


        [Display(Name = "ApprenticeshipTraining_Label_ApprenticeshipSubject")]
        public virtual ApprenticeshipSubject ApprenticeshipSubject { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_EducationLevel")]
        public virtual EducationLevel EducationLevel { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ApprenticeshipClass")]
        public virtual ApprenticeshipClass ApprenticeshipClass { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_AcademicYear")]
        public virtual AcademicYear AcademicYear { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_ClassProfile")]
        public virtual ClassProfile ClassProfile { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_PupilProfile")]
        public virtual PupilProfile PupilProfile { get; set; }

        [Display(Name = "ApprenticeshipTraining_Label_SchoolProfile")]
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
