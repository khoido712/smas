

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassEmulationDetailMetadata))]
public partial class ClassEmulationDetail { }

public partial class ClassEmulationDetailMetadata
    {
  
    [Display(Name = "ClassEmulationDetail_Label_ClassEmulationDetailID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassEmulationDetailID { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_ClassEmulationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassEmulationID { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_ClassID")]	
    public Nullable<int> ClassID { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_EmulationCriteriaId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmulationCriteriaId { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_PenalizedMark")]	
    public Nullable<decimal> PenalizedMark { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_PenalizedDate")]	
    public Nullable<System.DateTime> PenalizedDate { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassEmulationDetail_Label_EmulationCriteria")]	
    public virtual EmulationCriteria EmulationCriteria { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_ClassEmulation")]	
    public virtual ClassEmulation ClassEmulation { get; set; }	
  
    [Display(Name = "ClassEmulationDetail_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

