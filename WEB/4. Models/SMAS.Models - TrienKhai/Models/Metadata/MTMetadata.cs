

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MTMetadata))]
public partial class MT { }

public partial class MTMetadata
    {
  
    [Display(Name = "MT_Label_MT_ID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MT_ID { get; set; }	
  
    [Display(Name = "MT_Label_HistorySMSID")]	
    public Nullable<int> HistorySMSID { get; set; }	
  
    [Display(Name = "MT_Label_ReceiverNumber")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ReceiverNumber { get; set; }	
  
    [Display(Name = "MT_Label_Content")]
    [StringLength(1280,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Content { get; set; }	
  
    [Display(Name = "MT_Label_Status")]	
    public Nullable<short> Status { get; set; }	
  
    [Display(Name = "MT_Label_CreateTime")]	
    public Nullable<System.DateTime> CreateTime { get; set; }	
  
    [Display(Name = "MT_Label_RetryCNT")]	
    public Nullable<int> RetryCNT { get; set; }	
  
    [Display(Name = "MT_Label_SendTime")]	
    public Nullable<System.DateTime> SendTime { get; set; }	

  
    [Display(Name = "MT_Label_HistorySMS")]	
    public virtual HistorySMS HistorySMS { get; set; }	
    }
}

