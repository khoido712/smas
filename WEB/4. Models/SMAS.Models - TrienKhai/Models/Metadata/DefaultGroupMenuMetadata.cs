

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DefaultGroupMenuMetadata))]
public partial class DefaultGroupMenu { }

public partial class DefaultGroupMenuMetadata
    {
  
    [Display(Name = "DefaultGroupMenu_Label_DefaultGroupMenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DefaultGroupMenuID { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_DefaultGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DefaultGroupID { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_MenuID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MenuID { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_Permission")]	
    public Nullable<int> Permission { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "DefaultGroupMenu_Label_DefaultGroup")]	
    public virtual DefaultGroup DefaultGroup { get; set; }	
  
    [Display(Name = "DefaultGroupMenu_Label_Menu")]	
    public virtual Menu Menu { get; set; }	
    }
}

