

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassSupervisorAssignmentMetadata))]
public partial class ClassSupervisorAssignment { }

public partial class ClassSupervisorAssignmentMetadata
    {
  
    [Display(Name = "ClassSupervisorAssignment_Label_ClassSupervisorAssignmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassSupervisorAssignmentID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_PermissionLevel")]	
    public Nullable<int> PermissionLevel { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_AssignedDate")]	
    public Nullable<System.DateTime> AssignedDate { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassSupervisorAssignment_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "ClassSupervisorAssignment_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

