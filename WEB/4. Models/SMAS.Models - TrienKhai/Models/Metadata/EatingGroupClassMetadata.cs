

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EatingGroupClassMetadata))]
public partial class EatingGroupClass { }

public partial class EatingGroupClassMetadata
    {
  
    [Display(Name = "EatingGroupClass_Label_EatingGroupClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupClassID { get; set; }	
  
    [Display(Name = "EatingGroupClass_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "EatingGroupClass_Label_EatingGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupID { get; set; }	
  
    [Display(Name = "EatingGroupClass_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EatingGroupClass_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EatingGroupClass_Label_EatingGroup")]	
    public virtual EatingGroup EatingGroup { get; set; }	
  
    [Display(Name = "EatingGroupClass_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
    }
}

