

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ProcessedReportParameterMetadata))]
public partial class ProcessedReportParameter { }

public partial class ProcessedReportParameterMetadata
    {
  
    [Display(Name = "ProcessedReportParameter_Label_ProcessedReportParameterID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProcessedReportParameterID { get; set; }	
  
    [Display(Name = "ProcessedReportParameter_Label_ProcessedReportID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ProcessedReportID { get; set; }	
  
    [Display(Name = "ProcessedReportParameter_Label_ParameterName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ParameterName { get; set; }	
  
    [Display(Name = "ProcessedReportParameter_Label_ParameterValue")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ParameterValue { get; set; }	

  
    [Display(Name = "ProcessedReportParameter_Label_ProcessedReport")]	
    public virtual ProcessedReport ProcessedReport { get; set; }	
    }
}

