

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SchoolMovementMetadata))]
public partial class SchoolMovement { }

public partial class SchoolMovementMetadata
    {
  
    [Display(Name = "SchoolMovement_Label_SchoolMovementID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolMovementID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_Semester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Semester { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToProvinceID")]	
    public Nullable<int> MovedToProvinceID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToDistrictID")]	
    public Nullable<int> MovedToDistrictID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToClassID")]	
    public Nullable<int> MovedToClassID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToSchoolID")]	
    public Nullable<int> MovedToSchoolID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToClassName")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MovedToClassName { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToSchoolName")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MovedToSchoolName { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedToSchoolAddress")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MovedToSchoolAddress { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_MovedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime MovedDate { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_Description")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SchoolMovement_Label_District")]	
    public virtual District District { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_ClassProfile1")]	
    public virtual ClassProfile ClassProfile1 { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "SchoolMovement_Label_SchoolProfile1")]	
    public virtual SchoolProfile SchoolProfile1 { get; set; }	
    }
}

