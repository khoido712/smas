

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConcurrentWorkAssignmentMetadata))]
public partial class ConcurrentWorkAssignment { }

public partial class ConcurrentWorkAssignmentMetadata
    {
  
    [Display(Name = "ConcurrentWorkAssignment_Label_ConcurrentWorkAssignmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConcurrentWorkAssignmentID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_FacultyID")]	
    public Nullable<int> FacultyID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_ConcurrentWorkTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConcurrentWorkTypeID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConcurrentWorkAssignment_Label_ConcurrentWorkType")]	
    public virtual ConcurrentWorkType ConcurrentWorkType { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "ConcurrentWorkAssignment_Label_ConcurrentWorkReplacements")]	
    public virtual ICollection<ConcurrentWorkReplacement> ConcurrentWorkReplacements { get; set; }	
    }
}

