

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(SMSParentContractMetadata))]
public partial class SMSParentContract { }

public partial class SMSParentContractMetadata
    {
  
    [Display(Name = "SMSParentContract_Label_SMSParentContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SMSParentContractID { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_ContractCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(64,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractCode { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_SchoolCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string SchoolCode { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_Phone")]
    [StringLength(15,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Phone { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_ContractorName")]
    [StringLength(120,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractorName { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_ContractorRelationship")]
    [StringLength(64,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ContractorRelationship { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_PupilCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(30,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PupilCode { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_Status")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Status { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_CreatedTime")]	
    public Nullable<System.DateTime> CreatedTime { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_UpdatedTime")]	
    public Nullable<System.DateTime> UpdatedTime { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "SMSParentContract_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "SMSParentContract_Label_SMSParentSubscribers")]	
    public virtual ICollection<SMSParentSubscriber> SMSParentSubscribers { get; set; }	
    }
}

