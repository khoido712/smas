

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeHistoryStatusMetadata))]
public partial class EmployeeHistoryStatus { }

public partial class EmployeeHistoryStatusMetadata
    {
  
    [Display(Name = "EmployeeHistoryStatus_Label_EmployeeHistoryStatusID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeHistoryStatusID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_EmployeeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_SupervisingDeptID")]	
    public Nullable<int> SupervisingDeptID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_EmployeeStatus")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeStatus { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_FromDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime FromDate { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeeHistoryStatus_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EmployeeHistoryStatus_Label_SupervisingDept")]	
    public virtual SupervisingDept SupervisingDept { get; set; }	
    }
}

