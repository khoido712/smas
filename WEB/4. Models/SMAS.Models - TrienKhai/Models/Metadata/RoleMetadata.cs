

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(RoleMetadata))]
public partial class Role { }

public partial class RoleMetadata
    {
  
    [Display(Name = "Role_Label_RoleID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RoleID { get; set; }	
  
    [Display(Name = "Role_Label_ParentID")]	
    public Nullable<int> ParentID { get; set; }	
  
    [Display(Name = "Role_Label_RoleName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RoleName { get; set; }	
  
    [Display(Name = "Role_Label_RolePath")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string RolePath { get; set; }	
  
    [Display(Name = "Role_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "Role_Label_Description")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "Role_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	

  
    /*[Display(Name = "Role_Label_ActionAudits")]	
    public virtual ICollection<ActionAudit> ActionAudits { get; set; }	*/
  
    [Display(Name = "Role_Label_DefaultGroups")]	
    public virtual ICollection<DefaultGroup> DefaultGroups { get; set; }	
  
    [Display(Name = "Role_Label_GroupCats")]	
    public virtual ICollection<GroupCat> GroupCats { get; set; }	
  
    //[Display(Name = "Role_Label_Role1")]	
    //public virtual ICollection<Role> Role1 { get; set; }	
  
    [Display(Name = "Role_Label_Role2")]	
    public virtual Role Role2 { get; set; }	
  
    [Display(Name = "Role_Label_RoleMenus")]	
    public virtual ICollection<RoleMenu> RoleMenus { get; set; }	
  
    [Display(Name = "Role_Label_UserRoles")]	
    public virtual ICollection<UserRole> UserRoles { get; set; }	
    }
}

