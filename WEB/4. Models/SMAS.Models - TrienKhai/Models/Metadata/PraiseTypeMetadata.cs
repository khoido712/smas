

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(PraiseTypeMetadata))]
public partial class PraiseType { }

public partial class PraiseTypeMetadata
    {
  
    [Display(Name = "PraiseType_Label_PraiseTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PraiseTypeID { get; set; }	
  
    [Display(Name = "PraiseType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "PraiseType_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "PraiseType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "PraiseType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "PraiseType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "PraiseType_Label_ConductMark")]	
    public Nullable<decimal> ConductMark { get; set; }	
  
    [Display(Name = "PraiseType_Label_GraduationMark")]	
    public Nullable<decimal> GraduationMark { get; set; }	
  
    [Display(Name = "PraiseType_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "PraiseType_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
  
    }
}

