

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(UserGroupMetadata))]
public partial class UserGroup { }

public partial class UserGroupMetadata
    {
  
    [Display(Name = "UserGroup_Label_UserGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserGroupID { get; set; }	
  
    [Display(Name = "UserGroup_Label_UserID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int UserID { get; set; }	
  
    [Display(Name = "UserGroup_Label_GroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int GroupID { get; set; }	
  
    [Display(Name = "UserGroup_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "UserGroup_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
  
    [Display(Name = "UserGroup_Label_M_GUID")]	
    public Nullable<System.Guid> M_GUID { get; set; }	

  
    [Display(Name = "UserGroup_Label_GroupCat")]	
    public virtual GroupCat GroupCat { get; set; }	
  
    [Display(Name = "UserGroup_Label_UserAccount")]	
    public virtual UserAccount UserAccount { get; set; }	
    }
}

