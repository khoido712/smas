

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EatingGroupMetadata))]
public partial class EatingGroup { }

public partial class EatingGroupMetadata
    {
  
    [Display(Name = "EatingGroup_Label_EatingGroupID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EatingGroupID { get; set; }	
  
    [Display(Name = "EatingGroup_Label_EatingGroupName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string EatingGroupName { get; set; }	
  
    [Display(Name = "EatingGroup_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "EatingGroup_Label_MonthFrom")]	
    public Nullable<int> MonthFrom { get; set; }	
  
    [Display(Name = "EatingGroup_Label_MonthTo")]	
    public Nullable<int> MonthTo { get; set; }	
  
    [Display(Name = "EatingGroup_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "EatingGroup_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "EatingGroup_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "EatingGroup_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "EatingGroup_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EatingGroup_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EatingGroup_Label_DailyMenus")]	
    public virtual ICollection<DailyMenu> DailyMenus { get; set; }	
  
    [Display(Name = "EatingGroup_Label_DishInspections")]	
    public virtual ICollection<DishInspection> DishInspections { get; set; }	
  
    [Display(Name = "EatingGroup_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "EatingGroup_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EatingGroup_Label_EatingGroupClasses")]	
    public virtual ICollection<EatingGroupClass> EatingGroupClasses { get; set; }	
  
    [Display(Name = "EatingGroup_Label_EnergyDistributions")]	
    public virtual ICollection<EnergyDistribution> EnergyDistributions { get; set; }	
  
    [Display(Name = "EatingGroup_Label_NutritionalNorms")]	
    public virtual ICollection<NutritionalNorm> NutritionalNorms { get; set; }	
  
    [Display(Name = "EatingGroup_Label_WeeklyMenus")]	
    public virtual ICollection<WeeklyMenu> WeeklyMenus { get; set; }	
    }
}

