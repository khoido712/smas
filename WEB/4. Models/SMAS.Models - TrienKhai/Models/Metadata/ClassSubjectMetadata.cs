

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassSubjectMetadata))]
public partial class ClassSubject { }

public partial class ClassSubjectMetadata
    {
  
    [Display(Name = "ClassSubject_Label_ClassSubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public long ClassSubjectID { get; set; }	
  
    [Display(Name = "ClassSubject_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "ClassSubject_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "ClassSubject_Label_IsSpecializedSubject")]	
    public Nullable<bool> IsSpecializedSubject { get; set; }	
  
    [Display(Name = "ClassSubject_Label_SectionPerWeekFirstSemester")]	
    public Nullable<int> SectionPerWeekFirstSemester { get; set; }	
  
    [Display(Name = "ClassSubject_Label_SectionPerWeekSecondSemester")]	
    public Nullable<int> SectionPerWeekSecondSemester { get; set; }	
  
    [Display(Name = "ClassSubject_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "ClassSubject_Label_EndDate")]	
    public Nullable<System.DateTime> EndDate { get; set; }	
  
    [Display(Name = "ClassSubject_Label_IsSecondForeignLanguage")]	
    public Nullable<bool> IsSecondForeignLanguage { get; set; }	
  
    [Display(Name = "ClassSubject_Label_OrderInSchoolReport")]	
    public Nullable<int> OrderInSchoolReport { get; set; }	
  
    [Display(Name = "ClassSubject_Label_FirstSemesterCoefficient")]
    public Nullable<decimal> FirstSemesterCoefficient { get; set; }	
  
    [Display(Name = "ClassSubject_Label_SecondSemesterCoefficient")]
    public Nullable<decimal> SecondSemesterCoefficient { get; set; }	
  
    [Display(Name = "ClassSubject_Label_AppliedType")]	
    public Nullable<int> AppliedType { get; set; }	
  
    [Display(Name = "ClassSubject_Label_IsCommenting")]	
    public Nullable<int> IsCommenting { get; set; }	
  
    [Display(Name = "ClassSubject_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassSubject_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
   
    }
}

