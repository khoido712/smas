

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MovementAcceptanceMetadata))]
public partial class MovementAcceptance { }

public partial class MovementAcceptanceMetadata
    {
  
    [Display(Name = "MovementAcceptance_Label_MovementAcceptanceID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MovementAcceptanceID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_PupilID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_EducationLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EducationLevelID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_MovedFromClassID")]	
    public Nullable<int> MovedFromClassID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_MovedFromSchoolID")]	
    public Nullable<int> MovedFromSchoolID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_MovedFromClassName")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MovedFromClassName { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_MovedFromSchoolName")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MovedFromSchoolName { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_MovedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime MovedDate { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_Description")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "MovementAcceptance_Label_EducationLevel")]	
    public virtual EducationLevel EducationLevel { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_ClassProfile1")]	
    public virtual ClassProfile ClassProfile1 { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_PupilProfile")]	
    public virtual PupilProfile PupilProfile { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "MovementAcceptance_Label_SchoolProfile1")]	
    public virtual SchoolProfile SchoolProfile1 { get; set; }	
    }
}

