

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ConcurrentWorkReplacementMetadata))]
public partial class ConcurrentWorkReplacement { }

public partial class ConcurrentWorkReplacementMetadata
    {
  
    [Display(Name = "ConcurrentWorkReplacement_Label_ConcurrentWorkReplacementID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConcurrentWorkReplacementID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_AcademicYearID")]	
    public Nullable<int> AcademicYearID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_SchoolFacultyID")]	
    public Nullable<int> SchoolFacultyID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_ConcurrentWorkAssignmentID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ConcurrentWorkAssignmentID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_ReplacedTeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ReplacedTeacherID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_ToDate")]	
    public Nullable<System.DateTime> ToDate { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_M_OldReplacedTeacherID")]	
    public Nullable<int> M_OldReplacedTeacherID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ConcurrentWorkReplacement_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_ConcurrentWorkAssignment")]	
    public virtual ConcurrentWorkAssignment ConcurrentWorkAssignment { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_Employee1")]	
    public virtual Employee Employee1 { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_SchoolFaculty")]	
    public virtual SchoolFaculty SchoolFaculty { get; set; }	
  
    [Display(Name = "ConcurrentWorkReplacement_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

