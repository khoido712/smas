

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_CategoryOfPupilPrimaryMetadata))]
public partial class M_CategoryOfPupilPrimary { }

public partial class M_CategoryOfPupilPrimaryMetadata
    {
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_CategoryOfPupilPrimaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int CategoryOfPupilPrimaryID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_ConductID")]	
    public Nullable<int> ConductID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_AbsentTotalDays")]	
    public Nullable<int> AbsentTotalDays { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_Category")]	
    public Nullable<byte> Category { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_IsReviewed")]	
    public Nullable<bool> IsReviewed { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_CategoryOfPupilPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

