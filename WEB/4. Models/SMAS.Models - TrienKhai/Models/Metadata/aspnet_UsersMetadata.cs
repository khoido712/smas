

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(aspnet_UsersMetadata))]
public partial class aspnet_Users { }

public partial class aspnet_UsersMetadata
    {
  
    [Display(Name = "aspnet_Users_Label_ApplicationId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid ApplicationId { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_UserId")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.Guid UserId { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_UserName")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string UserName { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_LoweredUserName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(256,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string LoweredUserName { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_MobileAlias")]
    [StringLength(16,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string MobileAlias { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_IsAnonymous")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsAnonymous { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_LastActivityDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime LastActivityDate { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "aspnet_Users_Label_aspnet_Applications")]	
    public virtual aspnet_Applications aspnet_Applications { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_aspnet_Membership")]	
    public virtual aspnet_Membership aspnet_Membership { get; set; }	
  
    [Display(Name = "aspnet_Users_Label_UserAccounts")]	
    public virtual ICollection<UserAccount> UserAccounts { get; set; }	
    }
}

