

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_MarkRecordPrimaryMetadata))]
public partial class M_MarkRecordPrimary { }

public partial class M_MarkRecordPrimaryMetadata
    {
  
    [Display(Name = "M_MarkRecordPrimary_Label_MarkRecordID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkRecordID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Year")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Year { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M1")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M1 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M2")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M2 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M3")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M3 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M4")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M4 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M5")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M5 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M6")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M6 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M7")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M7 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M8")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M8 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M9")]
    [StringLength(128,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M9 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_SubjectID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SubjectID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Semester1")]	
    public Nullable<int> Semester1 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Semester2")]	
    public Nullable<int> Semester2 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_MustRetes")]	
    public Nullable<bool> MustRetes { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_AfterRetest")]	
    public Nullable<byte> AfterRetest { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_CapacityID")]	
    public Nullable<int> CapacityID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_MiddleSemester1")]	
    public Nullable<int> MiddleSemester1 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_MiddleSemester2")]	
    public Nullable<int> MiddleSemester2 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Emulation")]	
    public Nullable<byte> Emulation { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Capacity1")]	
    public Nullable<int> Capacity1 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Capacity2")]	
    public Nullable<int> Capacity2 { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_PatternSMS")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternSMS { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_PatternMonth")]
    [StringLength(250,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternMonth { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_PatternWeek")]
    [StringLength(300,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string PatternWeek { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_Partition_SubjectID")]	
    public Nullable<int> Partition_SubjectID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_MarkRecordPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

