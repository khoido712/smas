

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(EmployeeQualificationMetadata))]
public partial class EmployeeQualification { }

public partial class EmployeeQualificationMetadata
    {
  
    [Display(Name = "EmployeeQualification_Label_EmployeeQualificationID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeQualificationID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_EmployeeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EmployeeID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SupervisingDeptID")]	
    public Nullable<int> SupervisingDeptID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SpecialityID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SpecialityID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_QualificationTypeID")]	
    public Nullable<int> QualificationTypeID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_QualificationGradeID")]	
    public Nullable<int> QualificationGradeID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_Result")]
    [StringLength(200,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Result { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_QualifiedAt")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string QualifiedAt { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_FromDate")]	
    public Nullable<System.DateTime> FromDate { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_QualifiedDate")]	
    public Nullable<System.DateTime> QualifiedDate { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_IsPrimaryQualification")]	
    public Nullable<bool> IsPrimaryQualification { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_IsRequalified")]	
    public Nullable<bool> IsRequalified { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_TrainingLevelID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TrainingLevelID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_M_OldTeacherID")]	
    public Nullable<int> M_OldTeacherID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_M_QualificationGradeName")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M_QualificationGradeName { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_M_QualificationTypeName")]
    [StringLength(500,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string M_QualificationTypeName { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "EmployeeQualification_Label_QualificationGrade")]	
    public virtual QualificationGrade QualificationGrade { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_QualificationType")]	
    public virtual QualificationType QualificationType { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SpecialityCat")]	
    public virtual SpecialityCat SpecialityCat { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_TrainingLevel")]	
    public virtual TrainingLevel TrainingLevel { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_Employee")]	
    public virtual Employee Employee { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "EmployeeQualification_Label_SupervisingDept")]	
    public virtual SupervisingDept SupervisingDept { get; set; }	
    }
}

