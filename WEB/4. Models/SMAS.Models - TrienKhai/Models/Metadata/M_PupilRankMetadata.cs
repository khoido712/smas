

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_PupilRankMetadata))]
public partial class M_PupilRank { }

public partial class M_PupilRankMetadata
    {
  
    [Display(Name = "M_PupilRank_Label_PupilRankID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilRankID { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_ClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassID { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_Ranks")]	
    public Nullable<int> Ranks { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_Semester")]	
    public Nullable<int> Semester { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_PupilRank_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

