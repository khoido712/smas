

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ForeignLanguageGradeMetadata))]
public partial class ForeignLanguageGrade { }

public partial class ForeignLanguageGradeMetadata
    {
  
    [Display(Name = "ForeignLanguageGrade_Label_ForeignLanguageGradeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ForeignLanguageGradeID { get; set; }	
  
    [Display(Name = "ForeignLanguageGrade_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "ForeignLanguageGrade_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "ForeignLanguageGrade_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "ForeignLanguageGrade_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ForeignLanguageGrade_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ForeignLanguageGrade_Label_Employees")]	
    public virtual ICollection<Employee> Employees { get; set; }	
    }
}

