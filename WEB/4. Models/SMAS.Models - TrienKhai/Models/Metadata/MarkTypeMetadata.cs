

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(MarkTypeMetadata))]
public partial class MarkType { }

public partial class MarkTypeMetadata
    {
  
    [Display(Name = "MarkType_Label_MarkTypeID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int MarkTypeID { get; set; }	
  
    [Display(Name = "MarkType_Label_Title")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Title { get; set; }	
  
    [Display(Name = "MarkType_Label_Resolution")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(100,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Resolution { get; set; }	
  
    [Display(Name = "MarkType_Label_Coefficient")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Coefficient { get; set; }	
  
    [Display(Name = "MarkType_Label_NumberPerSemester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int NumberPerSemester { get; set; }	
  
    [Display(Name = "MarkType_Label_MustBeInteger")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool MustBeInteger { get; set; }	
  
    [Display(Name = "MarkType_Label_AppliedLevel")]	
    public Nullable<int> AppliedLevel { get; set; }	
  
    [Display(Name = "MarkType_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "MarkType_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "MarkType_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "MarkType_Label_JudgeRecords")]	
    public virtual ICollection<JudgeRecord> JudgeRecords { get; set; }	
  
    [Display(Name = "MarkType_Label_LockedMarkDetails")]	
    public virtual ICollection<LockedMarkDetail> LockedMarkDetails { get; set; }	
  
    [Display(Name = "MarkType_Label_MarkRecords")]	
    public virtual ICollection<MarkRecord> MarkRecords { get; set; }	
    }
}

