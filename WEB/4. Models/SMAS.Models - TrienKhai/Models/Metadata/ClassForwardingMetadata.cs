

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassForwardingMetadata))]
public partial class ClassForwarding { }

public partial class ClassForwardingMetadata
    {
  
    [Display(Name = "ClassForwarding_Label_ClassForwardingID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassForwardingID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_AcademicYearID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int AcademicYearID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_SchoolID")]	
    public Nullable<int> SchoolID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ClosedClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClosedClassID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ForwardedClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ForwardedClassID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_RepeatedClassID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int RepeatedClassID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_TotalForwardedPupil")]	
    public Nullable<int> TotalForwardedPupil { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_TotalRepeatedPupil")]	
    public Nullable<int> TotalRepeatedPupil { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ForwardedDate")]	
    public Nullable<System.DateTime> ForwardedDate { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "ClassForwarding_Label_AcademicYear")]	
    public virtual AcademicYear AcademicYear { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ClassProfile")]	
    public virtual ClassProfile ClassProfile { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ClassProfile1")]	
    public virtual ClassProfile ClassProfile1 { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_ClassProfile2")]	
    public virtual ClassProfile ClassProfile2 { get; set; }	
  
    [Display(Name = "ClassForwarding_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
    }
}

