

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(LevelHealthConfigMetadata))]
public partial class LevelHealthConfig { }

public partial class LevelHealthConfigMetadata
    {
  
    [Display(Name = "LevelHealthConfig_Label_LevelHealthConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int LevelHealthConfigID { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_EffectDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime EffectDate { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_Genre")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Genre { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_TypeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeConfigID { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "LevelHealthConfig_Label_TypeConfig")]	
    public virtual TypeConfig TypeConfig { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_SchoolProfile")]	
    public virtual SchoolProfile SchoolProfile { get; set; }	
  
    [Display(Name = "LevelHealthConfig_Label_LevelHealthConfigDetails")]	
    public virtual ICollection<LevelHealthConfigDetail> LevelHealthConfigDetails { get; set; }	
    }
}

