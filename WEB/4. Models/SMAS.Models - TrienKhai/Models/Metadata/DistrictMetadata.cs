

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(DistrictMetadata))]
public partial class District { }

public partial class DistrictMetadata
    {
  
    [Display(Name = "District_Label_DistrictID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int DistrictID { get; set; }	
  
    [Display(Name = "District_Label_ProvinceID")]	
    public Nullable<int> ProvinceID { get; set; }	
  
    [Display(Name = "District_Label_DistrictCode")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(10,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DistrictCode { get; set; }	
  
    [Display(Name = "District_Label_DistrictName")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
    [StringLength(50,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string DistrictName { get; set; }	
  
    [Display(Name = "District_Label_ShortName")]
    [StringLength(20,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string ShortName { get; set; }	
  
    [Display(Name = "District_Label_Description")]
    [StringLength(400,ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
    public string Description { get; set; }	
  
    [Display(Name = "District_Label_CreatedDate")]	
    public Nullable<System.DateTime> CreatedDate { get; set; }	
  
    [Display(Name = "District_Label_IsActive")]	
    public Nullable<bool> IsActive { get; set; }	
  
    [Display(Name = "District_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	
  
    [Display(Name = "District_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "District_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	

  
    [Display(Name = "District_Label_Communes")]	
    public virtual ICollection<Commune> Communes { get; set; }	
  
    [Display(Name = "District_Label_Province")]	
    public virtual Province Province { get; set; }	
  
    [Display(Name = "District_Label_EmployeeWorkMovements")]	
    public virtual ICollection<EmployeeWorkMovement> EmployeeWorkMovements { get; set; }	
  
    [Display(Name = "District_Label_PupilProfiles")]	
    public virtual ICollection<PupilProfile> PupilProfiles { get; set; }	
  
    [Display(Name = "District_Label_SchoolMovements")]	
    public virtual ICollection<SchoolMovement> SchoolMovements { get; set; }	
  
    [Display(Name = "District_Label_SchoolProfiles")]	
    public virtual ICollection<SchoolProfile> SchoolProfiles { get; set; }	
  
    [Display(Name = "District_Label_SupervisingDepts")]	
    public virtual ICollection<SupervisingDept> SupervisingDepts { get; set; }	
    }
}

