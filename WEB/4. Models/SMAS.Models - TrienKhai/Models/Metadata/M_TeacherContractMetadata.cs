

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_TeacherContractMetadata))]
public partial class M_TeacherContract { }

public partial class M_TeacherContractMetadata
    {
  
    [Display(Name = "M_TeacherContract_Label_TeacherContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherContractID { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_SchoolID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SchoolID { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_TeacherID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TeacherID { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_ContractID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ContractID { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_StartDate")]	
    public Nullable<System.DateTime> StartDate { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_RowNum__")]	
    public Nullable<int> RowNum__ { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_TeacherContract_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

