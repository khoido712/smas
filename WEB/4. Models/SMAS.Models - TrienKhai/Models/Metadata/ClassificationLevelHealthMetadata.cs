

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(ClassificationLevelHealthMetadata))]
public partial class ClassificationLevelHealth { }

public partial class ClassificationLevelHealthMetadata
    {
  
    [Display(Name = "ClassificationLevelHealth_Label_ClassificationLevelHealthID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int ClassificationLevelHealthID { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_Level")]	
    public Nullable<int> Level { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_SymbolConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int SymbolConfigID { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_TypeConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int TypeConfigID { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_EvaluationConfigID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int EvaluationConfigID { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_CreatedDate")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public System.DateTime CreatedDate { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_IsActive")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public bool IsActive { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_ModifiedDate")]	
    public Nullable<System.DateTime> ModifiedDate { get; set; }	

  
    [Display(Name = "ClassificationLevelHealth_Label_ClassificationCriteriaDetails")]	
    public virtual ICollection<ClassificationCriteriaDetail> ClassificationCriteriaDetails { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_EvaluationConfig")]	
    public virtual EvaluationConfig EvaluationConfig { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_SymbolConfig")]	
    public virtual SymbolConfig SymbolConfig { get; set; }	
  
    [Display(Name = "ClassificationLevelHealth_Label_TypeConfig")]	
    public virtual TypeConfig TypeConfig { get; set; }	
    }
}

