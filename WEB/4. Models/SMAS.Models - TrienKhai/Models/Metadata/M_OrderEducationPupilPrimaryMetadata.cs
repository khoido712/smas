

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.App_GlobalResources;

namespace SMAS.Models.Models
{
[MetadataType(typeof(M_OrderEducationPupilPrimaryMetadata))]
public partial class M_OrderEducationPupilPrimary { }

public partial class M_OrderEducationPupilPrimaryMetadata
    {
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_OrderEducationPupilPrimaryID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int OrderEducationPupilPrimaryID { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_PupilFileID")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int PupilFileID { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_Year")]	
    public Nullable<int> Year { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_Capacity")]	
    public Nullable<byte> Capacity { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_Semester")]
    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]	
    public int Semester { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_M_ProvinceID")]	
    public Nullable<int> M_ProvinceID { get; set; }	
  
    [Display(Name = "M_OrderEducationPupilPrimary_Label_M_OldID")]	
    public Nullable<int> M_OldID { get; set; }	
    }
}

