//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RESTORE_DATA
    {
        public System.Guid RESTORE_DATA_ID { get; set; }
        public int SCHOOL_ID { get; set; }
        public int ACADEMIC_YEAR_ID { get; set; }
        public int RESTORE_DATA_TYPE_ID { get; set; }
        public System.DateTime DELETED_DATE { get; set; }
        public string SHORT_DESCRIPTION { get; set; }
        public string DELETED_USER { get; set; }
        public string DELETED_FULLNAME { get; set; }
        public System.DateTime RESTORED_DATE { get; set; }
        public int RESTORED_STATUS { get; set; }
    }
}
