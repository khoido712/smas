//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassMovement
    {
        public int ClassMovementID { get; set; }
        public int PupilID { get; set; }
        public int FromClassID { get; set; }
        public int ToClassID { get; set; }
        public int EducationLevelID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public System.DateTime MovedDate { get; set; }
        public string Reason { get; set; }
        public Nullable<int> Semester { get; set; }
        public Nullable<System.DateTime> M_DateInto { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual PupilProfile PupilProfile { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }
        public virtual ClassProfile ClassProfile1 { get; set; }
    }
}
