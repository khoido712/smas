//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CallDetailRecord
    {
        public int CallDetailRecordID { get; set; }
        public int SMSTeacherContractID { get; set; }
        public string CallingNumber { get; set; }
        public string CalledNumber { get; set; }
        public System.DateTime StaDatetime { get; set; }
        public Nullable<int> Seq { get; set; }
        public Nullable<int> NumberOfSMS { get; set; }
        public int Supplier { get; set; }
        public int Year { get; set; }
    
        public virtual SMSTeacherContract SMSTeacherContract { get; set; }
    }
}
