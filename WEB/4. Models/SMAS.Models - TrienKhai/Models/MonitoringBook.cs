//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringBook
    {
        public MonitoringBook()
        {
            this.DentalTests = new HashSet<DentalTest>();
            this.ENTTests = new HashSet<ENTTest>();
            this.EyeTests = new HashSet<EyeTest>();
            this.OverallTests = new HashSet<OverallTest>();
            this.PhysicalTests = new HashSet<PhysicalTest>();
            this.SpineTests = new HashSet<SpineTest>();
        }
    
        public int MonitoringBookID { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public Nullable<System.DateTime> MonitoringDate { get; set; }
        public string Symptoms { get; set; }
        public string Solution { get; set; }
        public string UnitName { get; set; }
        public Nullable<int> MonitoringType { get; set; }
        public Nullable<int> HealthPeriodID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int EducationLevelID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public string MSourcedb { get; set; }
    
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual ICollection<DentalTest> DentalTests { get; set; }
        public virtual ICollection<ENTTest> ENTTests { get; set; }
        public virtual ICollection<EyeTest> EyeTests { get; set; }
        public virtual HealthPeriod HealthPeriod { get; set; }
        public virtual PupilProfile PupilProfile { get; set; }
        public virtual ICollection<OverallTest> OverallTests { get; set; }
        public virtual ICollection<PhysicalTest> PhysicalTests { get; set; }
        public virtual ICollection<SpineTest> SpineTests { get; set; }
    }
}
