//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Location
    {
        public Location()
        {
            this.PupilDisciplines = new HashSet<PupilDiscipline>();
            this.PupilPraises = new HashSet<PupilPraise>();
        }
    
        public int LocationID { get; set; }
        public string Name { get; set; }
        public System.DateTime CreatedDate { get; set; }
    
        public virtual ICollection<PupilDiscipline> PupilDisciplines { get; set; }
        public virtual ICollection<PupilPraise> PupilPraises { get; set; }
    }
}
