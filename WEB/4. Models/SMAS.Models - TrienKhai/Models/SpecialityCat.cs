//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpecialityCat
    {
        public SpecialityCat()
        {
            this.Employees = new HashSet<Employee>();
            this.EmployeeQualifications = new HashSet<EmployeeQualification>();
        }
    
        public int SpecialityCatID { get; set; }
        public string Resolution { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<EmployeeQualification> EmployeeQualifications { get; set; }
    }
}
