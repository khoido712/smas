//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PupilFault
    {
        public int PupilFaultID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int FaultID { get; set; }
        public int AcademicYearID { get; set; }
        public Nullable<int> EducationLevelID { get; set; }
        public System.DateTime ViolatedDate { get; set; }
        public Nullable<int> NumberOfFault { get; set; }
        public Nullable<decimal> TotalPenalizedMark { get; set; }
        public string Description { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public string MSourcedb { get; set; }
        public Nullable<int> Last2digitNumberSchool { get; set; }
        public Nullable<int> CreatedAcademicYear { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
        public string Note { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual FaultCriteria FaultCriteria { get; set; }
        public virtual PupilProfile PupilProfile { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
