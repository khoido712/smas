using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Oracle.DataAccess.Client;
using System.Data;

namespace SMAS.Models.Models
{
    public static class SMASUtils
    {
        public static String[] ORACLE_KEYWORD_SPECIAL = {"level"};

        public static List<int?> CheckConstraints(this DbContext entity, string schemaName, string tableName, int? id)
        {
            ObjectParameter tableNameParam = new ObjectParameter("P_TABLE_NAME", ConvertToOracle(tableName));
            ObjectParameter idParam = new ObjectParameter("P_ID", id);

            ObjectParameter resParam = new ObjectParameter("RES", typeof(int));

            ((IObjectContextAdapter)entity).ObjectContext.ExecuteFunction("CHECK_CONSTRAINTS", tableNameParam, idParam, resParam);
            return new List<int?>() { Int32.Parse(resParam.Value.ToString()) };
        }

        public static List<int?> CheckConstraintsWithIsActive(this DbContext entity, string schemaName, string tableName, int? id)
        {
            ObjectParameter tableNameParam = new ObjectParameter("P_TABLE_NAME", ConvertToOracle(tableName));
            ObjectParameter idParam = new ObjectParameter("P_ID", id);

            ObjectParameter resParam = new ObjectParameter("RES", typeof(int));

            ((IObjectContextAdapter)entity).ObjectContext.ExecuteFunction("CHECK_CONTRAINTS_WITH_ISACTIVE", tableNameParam, idParam, resParam);
            return new List<int?>() { Int32.Parse(resParam.Value.ToString()) };
        }

        public static List<int?> CheckConstraintsWithExceptTable(this DbContext entity, string schemaName, string tableName, string exceptTableName, Nullable<int> id)
        {
            List<string> arr = exceptTableName.Split(',').Where(x => !String.IsNullOrWhiteSpace(x))
                .Select(x => ConvertToOracle(x)).ToList();
            exceptTableName = "," + string.Join(",", arr) + ",";
            ObjectParameter tableNameParam = new ObjectParameter("P_TABLE_NAME", ConvertToOracle(tableName));
            ObjectParameter idParam = new ObjectParameter("P_ID", id);
            ObjectParameter exceptTableNameParam = new ObjectParameter("P_EXCEPT_TABLE_NAME", exceptTableName);
            ObjectParameter resParam = new ObjectParameter("RES", typeof(int));

            ((IObjectContextAdapter)entity).ObjectContext.ExecuteFunction("CHECK_CONSTRAINTS_EXCEPT_TABLE", tableNameParam, exceptTableNameParam, idParam, resParam);
            return new List<int?>() { Int32.Parse(resParam.Value.ToString()) };
        }

        public static List<Nullable<int>> CheckDuplicate(this DbContext entity, string valueToCheck, string schemaName,
            string tableName, string columnName, Nullable<bool> isActive, Nullable<int> id)
        {
            ObjectParameter tableNameParam = new ObjectParameter("P_TABLE_NAME", ConvertToOracle(tableName));
            ObjectParameter columnNameParam = new ObjectParameter("P_COLUMN_NAME", ConvertToOracle(columnName));
            ObjectParameter valueParam = new ObjectParameter("P_VALUE", valueToCheck);
            ObjectParameter isActiveParam = new ObjectParameter("P_IS_ACTIVE", isActive == true ? 1 : 0);
            ObjectParameter idParam = new ObjectParameter("P_ID", id);
            ObjectParameter resParam = new ObjectParameter("RES", typeof(int));

            ((IObjectContextAdapter)entity).ObjectContext.ExecuteFunction("CHECK_DUPLICATE", tableNameParam, columnNameParam,
                valueParam, isActiveParam, idParam, resParam);
            return new List<int?>() { Int32.Parse(resParam.Value.ToString()) };
        }

        public static List<Nullable<int>> CheckDuplicateCouple(this DbContext entity, string valueToCheck1, string valueToCheck2, string schemaName,
            string tableName, string columnName1, string columnName2, Nullable<bool> isActive, Nullable<int> id)
        {
            ObjectParameter tableNameParam = new ObjectParameter("P_TABLE_NAME", ConvertToOracle(tableName));
            ObjectParameter columnName1Param = new ObjectParameter("P_COLUMN_NAME1", ConvertToOracle(columnName1));
            ObjectParameter columnName2Param = new ObjectParameter("P_COLUMN_NAME2", ConvertToOracle(columnName2));
            ObjectParameter value1Param = new ObjectParameter("P_VALUE1", valueToCheck1);
            ObjectParameter value2Param = new ObjectParameter("P_VALUE2", valueToCheck2);
            ObjectParameter isActiveParam = new ObjectParameter("P_IS_ACTIVE", isActive == true ? 1 : 0);
            ObjectParameter idParam = new ObjectParameter("P_ID", id);
            ObjectParameter resParam = new ObjectParameter("RES", typeof(int));

            ((IObjectContextAdapter)entity).ObjectContext.ExecuteFunction("CHECK_DUPLICATE_COUPLE", tableNameParam, columnName1Param, columnName2Param,
                value1Param, value2Param, isActiveParam, idParam, resParam);
            return new List<int?>() { Int32.Parse(resParam.Value.ToString()) };
        }

        //Không dùng cho các bảng Membership
        public static string ConvertToOracle(string s, int max = 30)
        {

            //Neu Ten bang hoac column trung voi tu khoa thi them dau ""
            if (ORACLE_KEYWORD_SPECIAL.Contains(s.ToLower()))
            {
                StringBuilder strOracle = new StringBuilder();
                strOracle.Append("\"");
                strOracle.Append(s.ToUpper());
                strOracle.Append("\"");
                s = strOracle.ToString();
            }

            s = s.Replace("-", "_");
            String ora = "";
            int len = s.Length;
            bool isUpper = true;
            for (int i = 0; i < len; i++)
            {
                String t = s.Substring(i, 1);
                if (t.Trim().Equals(""))
                {
                    continue;
                }

                // Neu chuyen tu ky tu thuong sang ky tu hoa thi them dau gach duoi
                if (!isUpper && t.Equals(t.ToUpper()) && !t.Equals("_"))
                {
                    ora += "_" + t.ToUpper();
                }
                else
                {
                    ora += t.ToUpper();
                }
                if (t.Equals(t.ToUpper()))
                {
                    isUpper = true;
                }
                else
                {
                    isUpper = false;
                }
            }
            return cutLongName(ora, max);
        }

        public static String cutLongName(String s, int max = 30)
        {
            int maxOraLen = max;
            int minCut = 3;

            int len = s.Length;
            if (len <= maxOraLen)
            {
                return s;
            }
            String[] arrTemp = s.Split('_');
            String name = "";
            len = arrTemp.Length;
            int countChar = s.Length;
            // Thuc hien cat doi voi nhung cuoi lon hon 3 ky tu
            for (int i = len - 1; i >= 0; i--)
            {
                String t = arrTemp[i];
                if (t.Length > minCut)
                {
                    arrTemp[i] = t.Substring(0, minCut);
                    countChar -= t.Length - minCut;
                }
                if (countChar <= maxOraLen)
                {
                    break;
                }
            }
            // Neu da cat nhung van qua dai thi thuc hien bo luon nhung phan nay
            if (countChar > maxOraLen)
            {
                for (int i = len - 1; i >= 0; i--)
                {
                    countChar -= arrTemp[i].Length;
                    if (i != 0)
                    {
                        countChar--;
                        len--;
                    }
                    arrTemp[i] = "";
                    if (countChar <= maxOraLen)
                    {
                        break;
                    }
                }
            }
            for (int i = 0; i < len; i++)
            {
                name += "_" + arrTemp[i];
            }
            name = name.Substring(1).ToUpper();
            return name;
        }

        public static void AddArray<T>(this OracleParameterCollection parameters, string name, OracleDbType dbType, T[] array, ParameterDirection dir, T emptyArrayValue)
        {
            parameters.Add(new OracleParameter
            {
                ParameterName = name,
                OracleDbType = dbType,
                CollectionType = OracleCollectionType.PLSQLAssociativeArray
            });

            // oracle does not support passing null or empty arrays.
            // so pass an array with exactly one element
            // with a predefined value and use it to check
            // for empty array condition inside the proc code
            if (array == null || array.Length == 0)
            {
                parameters[name].Value = new T[1] { emptyArrayValue };
                parameters[name].Size = 1;
            }
            else
            {
                parameters[name].Value = array;
                parameters[name].Size = array.Length;
            }
        }
    }
}
