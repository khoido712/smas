﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Models.Models.CustomModels
{
    public class ClassProfileRefBO
    {
        public int CLASS_PROFILE_ID { get; set; }
        public int EDUCATION_LEVEL_ID { get; set; }
        public int? ORDER_NUMBER { get; set; }
        public string DISPLAY_NAME { get; set; }
    }
}
