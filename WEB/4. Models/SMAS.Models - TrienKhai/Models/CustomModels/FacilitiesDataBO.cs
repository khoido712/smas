﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Models.Models.CustomModels
{
    public class FacilitiesDataBO
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public int DistrictID { get; set; }
        public string DistrictName { get; set; }

        public int NumOfDeptClassroom { get; set; }
        public int NumOfPublicHouse { get; set; }
        public int NumOfSchoolBoardHouse { get; set; }
        public int NumOfBoardingHouse { get; set; }
        public int LibraryStatus { get; set; }
        public int OnewayKitchenStatus { get; set; }
        public int SchoolGateStatus { get; set; }
        public int SchoolFence { get; set; }
        public int WaterSource { get; set; }
        public int NumOfStandardWC { get; set; }
        public int NumOfNotStandardWC { get; set; }
        public int NumOfShortlivedRoom { get; set; }
        public int NumOfThreeShiftsRoom { get; set; }
    }
}
