using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;


namespace SMAS.Models.Models.CustomModels
{
    public class CustomUserAcc
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters int.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
               
        [Display(Name ="Employee")]
        public Nullable<int> EmployeeID { get; set; } // map voi bang Employee

        [Display(Name ="Is Admin")]
        [Required]
        public bool IsAdmin { get; set; }

        [Display(Name = "UserAccountID")]
        public int UserAccountID { get; set; }
    }
}
