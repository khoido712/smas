﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Models.Models.CustomModels
{
    public class GroupCheck
    {
        public GroupCat group {get; set;}
        public bool isChecked {get; set;}
    }
}