﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Models.Models.CustomModels
{
    public class RoleCheck
    {
        public Role Role {get;set;}

        public bool IsChecked {get;set;}

    }

}