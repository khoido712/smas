﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Models.Models.CustomModels
{


    // Object luu diem mon tinh diem va nhan xet de thuc hien insert vao CSDL
    public class MarkJudgeBO : INullable, IOracleCustomType
    {
        private bool objectIsNull;

        [OracleObjectMappingAttribute("PUPIL_ID")]
        public int PupilID { get; set; }

        [OracleObjectMappingAttribute("SUBJECT_ID")]
        public int SubjectID { get; set; }

        [OracleObjectMappingAttribute("CLASS_ID")]
        public int ClassID { get; set; }

        [OracleObjectMappingAttribute("MARK_TYPE_ID")]
        public int MarkTypeID { get; set; }

        [OracleObjectMappingAttribute("JUDGEMENT")]
        public string Judgement { get; set; }

        [OracleObjectMappingAttribute("MARK")]
        public decimal? Mark { get; set; }

        [OracleObjectMappingAttribute("TITLE")]
        public string Title { get; set; }

        [OracleObjectMappingAttribute("ORDER_NUMBER")]
        public int OrderNumber { get; set; }

        public static MarkJudgeBO Null
        {
            get
            {
                MarkJudgeBO markjudgeBO = new MarkJudgeBO();
                markjudgeBO.objectIsNull = true;
                return markjudgeBO;
            }
        }

        #region INullable Members

        public bool IsNull
        {
            get { return objectIsNull; }
        }

        #endregion

        #region IOracleCustomType Members

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            // Convert from the Custom Type to Oracle Object            
            OracleUdt.SetValue(con, pUdt, "PUPIL_ID", PupilID);
            OracleUdt.SetValue(con, pUdt, "SUBJECT_ID", SubjectID);
            OracleUdt.SetValue(con, pUdt, "CLASS_ID", ClassID);
            OracleUdt.SetValue(con, pUdt, "MARK_TYPE_ID", MarkTypeID);
            if (!string.IsNullOrEmpty(Judgement))
            {
                OracleUdt.SetValue(con, pUdt, "JUDGEMENT", Judgement);
            }
            if (Mark.HasValue)
            {
                OracleUdt.SetValue(con, pUdt, "MARK", Mark);
            }
            OracleUdt.SetValue(con, pUdt, "TITLE", Title);
            OracleUdt.SetValue(con, pUdt, "ORDER_NUMBER", OrderNumber);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            PupilID = (int)OracleUdt.GetValue(con, pUdt, "PUPIL_ID");
            SubjectID = (int)OracleUdt.GetValue(con, pUdt, "SUBJECT_ID");
            ClassID = (int)OracleUdt.GetValue(con, pUdt, "CLASS_ID");
            MarkTypeID = (int)OracleUdt.GetValue(con, pUdt, "MARK_TYPE_ID");
            Judgement = (string)OracleUdt.GetValue(con, pUdt, "JUDGEMENT");
            Mark = (decimal?)OracleUdt.GetValue(con, pUdt, "MARK");
            Title = (string)OracleUdt.GetValue(con, pUdt, "TITLE");
            OrderNumber = (int)OracleUdt.GetValue(con, pUdt, "ORDER_NUMBER");
        }

        #endregion

    }

    [OracleCustomTypeMappingAttribute("SMAS3.MARK_JUDGE_OBJECT")]
    public class MarkJudgeBOFactory : IOracleCustomTypeFactory
    {
        #region IOracleCustomTypeFactory Members
        public IOracleCustomType CreateObject()
        {
            return new MarkJudgeBO();
        }
        #endregion
    }

    /* CompanyInfoList Class
   **   An instance of a CompanyInfoList class represents an CompanyInfoList object
   **   A custom type must implement INullable and IOracleCustomType interfaces
   */
    public class MarkJudgeBOList : INullable, IOracleCustomType
    {
        [OracleArrayMapping()]
        public MarkJudgeBO[] MarkJudgeBOArray;

        private bool objectIsNull;

        #region INullable Members

        public bool IsNull
        {
            get { return objectIsNull; }
        }

        public static MarkJudgeBOList Null
        {
            get
            {
                MarkJudgeBOList obj = new MarkJudgeBOList();
                obj.objectIsNull = true;
                return obj;
            }
        }

        #endregion

        #region IOracleCustomType Members

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, 0, MarkJudgeBOArray);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            MarkJudgeBOArray = (MarkJudgeBO[])OracleUdt.GetValue(con, pUdt, 0);
        }

        #endregion
    }

    [OracleCustomTypeMapping("SMAS3.MARK_JUDGE_TYPE")]
    public class MarkJudgeBOListFactory : IOracleCustomTypeFactory, IOracleArrayTypeFactory
    {
        #region IOracleCustomTypeFactory Members
        public IOracleCustomType CreateObject()
        {
            return new MarkJudgeBOList();
        }

        #endregion

        #region IOracleArrayTypeFactory Members
        public Array CreateArray(int numElems)
        {
            return new MarkJudgeBO[numElems];
        }

        public Array CreateStatusArray(int numElems)
        {
            return null;
        }

        #endregion
    }
}
