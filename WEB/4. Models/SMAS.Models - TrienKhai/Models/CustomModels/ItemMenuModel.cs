using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Models.Models.CustomModels
{
    public class ItemMenuModel
    {

        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ParameterName { get; set; }

        public string Text { get; set; } // ten cua Resource Key
        public string ImageUrl { get; set; }
        public bool Visible { get; set; }
        public bool IsCategory { get; set; }

        //public string RouteName { get; set; }
        //public RouteValueDictionary RouteValues { get; set; }
        public string MenuUrl { get; set; }
        public string MenuPath { get; set; }
        public int Order { get; set; }      //thu tu hien thi
        public int ItemMenuID { get; set; }     //ID cua ItemMenuModel
        public int? ItemParentID { get; set; }   //ID cua Item cha
        public List<ItemMenuModel> MenuChildren { get; set; } //List cac item con
        //
        public bool isAccess { get; set; }
        //
        public bool viewPermission { get; set; }
        public bool addPermission { get; set; }
        public bool editPermission { get; set; }
        public bool deletePermission { get; set; }
        public Nullable<int> BlockMenu { get; set; }
        public ItemMenuModel()
        {
            this.MenuChildren = new List<ItemMenuModel>();
        }

    }
}
