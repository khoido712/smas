﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Models.Models.CustomModels
{
    public class MarkRecordNewBO
    {
        public int PUPIL_ID { get; set; }
        public string FULL_NAME { get; set; }
        public string S_NAME { get; set; }
        public int STATUS { get; set; }
        public Nullable<int> ORDER_IN_CLASS { get; set; }
        public DateTime? END_DATE { get; set; }
        public string S_COMMENT { get; set; }
        public Nullable<decimal> M1 { get; set; }
        public Nullable<decimal> M2 { get; set; }
        public Nullable<decimal> M3 { get; set; }
        public Nullable<decimal> M4 { get; set; }
        public Nullable<decimal> M5 { get; set; }
        public Nullable<decimal> P1 { get; set; }
        public Nullable<decimal> P2 { get; set; }
        public Nullable<decimal> P3 { get; set; }
        public Nullable<decimal> P4 { get; set; }
        public Nullable<decimal> P5 { get; set; }
        public Nullable<decimal> V1 { get; set; }
        public Nullable<decimal> V2 { get; set; }
        public Nullable<decimal> V3 { get; set; }
        public Nullable<decimal> V4 { get; set; }
        public Nullable<decimal> V5 { get; set; }
        public Nullable<decimal> V6 { get; set; }
        public Nullable<decimal> V7 { get; set; }
        public Nullable<decimal> V8 { get; set; }
        public Nullable<decimal> HK{ get; set; }
        public Nullable<decimal> HK1 { get; set; }
        public Nullable<decimal> HK2 { get; set; }
        public Nullable<decimal> CN {get; set; }
        public string LOG_CHANGE { get; set; }
       
    }
    //------JUDGE
    public class JudgeRecordNewBO
    {
        public int PUPIL_ID { get; set; }
        public string FULL_NAME { get; set; }
        public string S_NAME { get; set; }
        public int STATUS { get; set; }
        public Nullable<int> ORDER_IN_CLASS { get; set; }
        public DateTime? END_DATE { get; set; }
        public string S_COMMENT { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
        public string P3 { get; set; }
        public string P4 { get; set; }
        public string P5 { get; set; }
        public string V1 { get; set; }
        public string V2 { get; set; }
        public string V3 { get; set; }
        public string V4 { get; set; }
        public string V5 { get; set; }
        public string V6 { get; set; }
        public string V7 { get; set; }
        public string V8 { get; set; }
        public string HK { get; set; }
        public string HK1 { get; set; }
        public string HK2 { get; set; }
        public string CN { get; set; }
        public string LOG_CHANGE { get; set; }
    }

    // AnhVD9 - 20151109 - Bao cao SMAS
    public class ReportSMASBO
    {
        public int SchoolID { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string SchoolName { get; set; }
        public string UserName { get; set; }

        public int SoLopKhaiBao { get; set; }
        public int SoGVKhaiBao { get; set; }
        public int SoHSKhaiBao { get; set; }
        public int SoMonHocKhaiBao { get; set; }

        public int SoGVPhanCong { get; set; }

        public int SoHSXLHK1 { get; set; }
        public int SoHSXLHK2 { get; set; }
        public int SoHSXLCaNam { get; set; }
    }
   
}
