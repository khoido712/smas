﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Models.Models.CustomModels
{


    // Diem phuc vu cho mon co nhan xet (GDCD)
    public class MarkCommentBO : INullable, IOracleCustomType
    {
        private bool objectIsNull;

        [OracleObjectMappingAttribute("PUPIL_ID")]
        public int PupilID { get; set; }

        [OracleObjectMappingAttribute("MARK_COMMENT")]
        public string MarkComment { get; set; }

        public static MarkCommentBO Null
        {
            get
            {
                MarkCommentBO markCommentBO = new MarkCommentBO();
                markCommentBO.objectIsNull = true;
                return markCommentBO;
            }
        }

        #region INullable Members

        public bool IsNull
        {
            get { return objectIsNull; }
        }

        #endregion

        #region IOracleCustomType Members

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            // Convert from the Custom Type to Oracle Object            
            OracleUdt.SetValue(con, pUdt, "PUPIL_ID", PupilID);

            if (!string.IsNullOrEmpty(MarkComment))
            {
                OracleUdt.SetValue(con, pUdt, "MARK_COMMENT", MarkComment);
            }
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            PupilID = (int)OracleUdt.GetValue(con, pUdt, "PUPIL_ID");
            MarkComment = (string)OracleUdt.GetValue(con, pUdt, "MARK_COMMENT");
        }

        #endregion

    }

    [OracleCustomTypeMappingAttribute("SMAS3.MARK_COMMENT_OBJECT")]
    public class MarkCommentBOFactory : IOracleCustomTypeFactory
    {
        #region IOracleCustomTypeFactory Members
        public IOracleCustomType CreateObject()
        {
            return new MarkCommentBO();
        }
        #endregion
    }

    /* CompanyInfoList Class
   **   An instance of a CompanyInfoList class represents an CompanyInfoList object
   **   A custom type must implement INullable and IOracleCustomType interfaces
   */
    public class MarkCommentBOList : INullable, IOracleCustomType
    {
        [OracleArrayMapping()]
        public MarkCommentBO[] MarkCommentBOArray;

        private bool objectIsNull;

        #region INullable Members

        public bool IsNull
        {
            get { return objectIsNull; }
        }

        public static MarkCommentBOList Null
        {
            get
            {
                MarkCommentBOList obj = new MarkCommentBOList();
                obj.objectIsNull = true;
                return obj;
            }
        }

        #endregion

        #region IOracleCustomType Members

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, 0, MarkCommentBOArray);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            MarkCommentBOArray = (MarkCommentBO[])OracleUdt.GetValue(con, pUdt, 0);
        }

        #endregion
    }

    [OracleCustomTypeMapping("SMAS3.MARK_COMMENT_TYPE")]
    public class MarkCommentBOListFactory : IOracleCustomTypeFactory, IOracleArrayTypeFactory
    {
        #region IOracleCustomTypeFactory Members
        public IOracleCustomType CreateObject()
        {
            return new MarkCommentBOList();
        }

        #endregion

        #region IOracleArrayTypeFactory Members
        public Array CreateArray(int numElems)
        {
            return new MarkCommentBO[numElems];
        }

        public Array CreateStatusArray(int numElems)
        {
            return null;
        }

        #endregion
    }
}
