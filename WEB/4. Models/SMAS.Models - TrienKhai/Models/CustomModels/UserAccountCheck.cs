﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Models.Models.CustomModels
{
    public class UserAccountCheck
    {
        public UserAccount UserAccount { get; set; }
        public bool IsChecked { get; set; }
    }
}