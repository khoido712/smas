//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class APP_USER_CLAIMS
    {
        public long USER_CLAIMS_ID { get; set; }
        public System.Guid USER_ID { get; set; }
        public string CLAIM_TYPE { get; set; }
        public string CLAIM_VALUE { get; set; }
    }
}
