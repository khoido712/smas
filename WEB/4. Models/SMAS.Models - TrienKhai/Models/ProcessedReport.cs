//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcessedReport
    {
        public ProcessedReport()
        {
            this.ProcessedCellDatas = new HashSet<ProcessedCellData>();
            this.ProcessedReportParameters = new HashSet<ProcessedReportParameter>();
        }
    
        public int ProcessedReportID { get; set; }
        public string ReportCode { get; set; }
        public System.DateTime ProcessedDate { get; set; }
        public byte[] ReportData { get; set; }
        public string DeserializeClass { get; set; }
        public string HandlerProcess { get; set; }
        public Nullable<System.DateTime> ExpiredDate { get; set; }
        public Nullable<bool> SentToSupervisor { get; set; }
        public string ReportName { get; set; }
        public string InputParameterHashKey { get; set; }
        public Nullable<int> ViewBooklet { get; set; }
    
        public virtual ICollection<ProcessedCellData> ProcessedCellDatas { get; set; }
        public virtual ICollection<ProcessedReportParameter> ProcessedReportParameters { get; set; }
    }
}
