//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LockedMarkDetail
    {
        public int LockedMarkDetailID { get; set; }
        public int AcademicYearID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public Nullable<int> Semester { get; set; }
        public int MarkIndex { get; set; }
        public Nullable<System.DateTime> LockedDate { get; set; }
        public Nullable<System.DateTime> UnlockedDate { get; set; }
        public Nullable<int> MarkTypeID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        public Nullable<int> M_OldID { get; set; }
        public string MMarkType { get; set; }
        public string MSourcedb { get; set; }
        public int Last2digitNumberSchool { get; set; }
        public Nullable<int> CreatedAcademicYear { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual ClassProfile ClassProfile { get; set; }
        public virtual MarkType MarkType { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }
        public virtual SubjectCat SubjectCat { get; set; }
    }
}
