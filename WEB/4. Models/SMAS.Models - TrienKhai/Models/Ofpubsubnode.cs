//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ofpubsubnode
    {
        public string Serviceid { get; set; }
        public string Nodeid { get; set; }
        public decimal Leaf { get; set; }
        public string Creationdate { get; set; }
        public string Modificationdate { get; set; }
        public string Parent { get; set; }
        public decimal Deliverpayloads { get; set; }
        public Nullable<decimal> Maxpayloadsize { get; set; }
        public Nullable<decimal> Persistitems { get; set; }
        public Nullable<decimal> Maxitems { get; set; }
        public decimal Notifyconfigchanges { get; set; }
        public decimal Notifydelete { get; set; }
        public decimal Notifyretract { get; set; }
        public decimal Presencebased { get; set; }
        public decimal Senditemsubscribe { get; set; }
        public string Publishermodel { get; set; }
        public decimal Subscriptionenabled { get; set; }
        public decimal Configsubscription { get; set; }
        public string Accessmodel { get; set; }
        public string Payloadtype { get; set; }
        public string Bodyxslt { get; set; }
        public string Dataformxslt { get; set; }
        public string Creator { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string Replypolicy { get; set; }
        public string Associationpolicy { get; set; }
        public Nullable<decimal> Maxleafnodes { get; set; }
    }
}
