//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class APP_USER_SETTING
    {
        public System.Guid USER_SETTING_ID { get; set; }
        public System.Guid USER_ID { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATE { get; set; }
        public string SKIN_NAME { get; set; }
        public string MENU_TYPE { get; set; }
        public string LANGUAGE_NAME { get; set; }
        public string SYNC_CODE { get; set; }
        public Nullable<System.DateTime> SYNC_TIME { get; set; }
        public int PARTITION_ID { get; set; }
    }
}
