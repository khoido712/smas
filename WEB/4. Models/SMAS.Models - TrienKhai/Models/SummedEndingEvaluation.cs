//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMAS.Models.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SummedEndingEvaluation
    {
        public long SummedEndingEvaluationID { get; set; }
        public int PupilID { get; set; }
        public int SemesterID { get; set; }
        public int EvaluationID { get; set; }
        public int SchoolID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public int ClassID { get; set; }
        public int AcademicYearID { get; set; }
        public string EndingEvaluation { get; set; }
        public string EvaluationReTraining { get; set; }
        public System.DateTime CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public Nullable<int> RateAdd { get; set; }
        public Nullable<int> Reward { get; set; }
        public Nullable<int> SynchronizeID { get; set; }
    }
}
