﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.VTUtils.Utils;
using SMAS.Business.Common;
namespace SMAS.Web.Utils
{
    public class ListAppliedLevel
    {
        List<AppliedLevel> ListAppliedLevels;

        public ListAppliedLevel()
        {
            ListAppliedLevels = new List<AppliedLevel>();
            AppliedLevel level1 = new AppliedLevel { AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_CRECHE, AppliedLevelName = "Cấp Nhà trẻ" };
            AppliedLevel level2 = new AppliedLevel { AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN, AppliedLevelName = "Cấp Mẫu giáo" };
            AppliedLevel level3 = new AppliedLevel { AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_PRIMARY, AppliedLevelName = "Cấp 1" };
            AppliedLevel level4 = new AppliedLevel { AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_SECONDARY, AppliedLevelName = "Cấp 2" };
            AppliedLevel level5 = new AppliedLevel { AppliedLevelID = SystemParamsInFile.APPLIED_LEVEL_TERTIARY, AppliedLevelName = "Cấp 3" };
            ListAppliedLevels.Add(level1);
            ListAppliedLevels.Add(level2);
            ListAppliedLevels.Add(level3);
            ListAppliedLevels.Add(level4);
            ListAppliedLevels.Add(level5);
        }
               

        public List<AppliedLevel> GetListAppliedLevel(List<int> appliedLevels)
        {
            List<AppliedLevel> result = new List<AppliedLevel>();
            List<AppliedLevel> resultSortApplied = new List<AppliedLevel>();
            int countStop = 0;

            foreach (var i in appliedLevels)
            {
                result.Add(this.ListAppliedLevels.Where(a => a.AppliedLevelID == i).FirstOrDefault());
            }

            if (result.Count() == 5)
            {
                for (int i = SystemParamsInFile.APPLIED_LEVEL_CRECHE - 1; i < appliedLevels.Count(); i++)
                {
                    resultSortApplied.Add(this.ListAppliedLevels.Where(a => a.AppliedLevelID == i + 1).FirstOrDefault());
                    countStop++;
                    if ((i + 1) == appliedLevels.Count())
                    {
                        i = -1;
                    }
                    if (countStop == appliedLevels.Count())
                    {
                        break;
                    }
                }
                return resultSortApplied;
            }           
            
            return result;
        }

        public string GetAppliedName(int appliedlevelID)
        {
            if (ListAppliedLevels.Where(ap => ap.AppliedLevelID == appliedlevelID).Count() > 0)
                return ListAppliedLevels.Where(ap => ap.AppliedLevelID == appliedlevelID).First().AppliedLevelName;
            else return "";
        }
    }
    public class AppliedLevel
    {
        public int AppliedLevelID { get; set; }
        public string AppliedLevelName { get; set; }

    }
}