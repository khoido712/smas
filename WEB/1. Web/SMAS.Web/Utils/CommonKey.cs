﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Utils
{
    public class CommonKey
    {
        public class AuditActionKey
        {
            public const string OldJsonObject = "oldJsonObject";
            public const string NewJsonObject = "newJsonObject";
            public const string ObjectID = "objectID";
            public const string Description = "description";
            public const string Parameter = "parameter";
            public const string userAction = "userAction";
            public const string userFunction = "userFunction";
            public const string userDescription = "userDescription";
        }
    }
}