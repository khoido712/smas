using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using SMAS.Web.Constants;
using SMAS.Models.Models;

namespace SMAS.Web.Utils
{
    public class SessionObject
    {
        static HttpSessionState Session
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ApplicationException("No Http Context, No Session to Get!");

                return HttpContext.Current.Session;
            }
        }

        public static T Get<T>(string key)
        {
            if (Session[key] == null)
                return default(T);
            else
                return (T)Session[key];
        }

        public static void Set<T>(string key, T value)
        {
            Session[key] = value;
        }

        public static string GetString(string key)
        {
            string s = Get<string>(key);
            return s == null ? string.Empty : s;
        }

        public static void SetString(string key, string value)
        {
            Set<string>(key, value);
        }

        public static int GetInt(string key)
        {
            int s = Get<int>(key);
            return s;
        }

        public static void SetInt(string key, int value)
        {
            Set<int>(key, value);
        }

        public static int? GetNullableInt(string key)
        {
            int? s = Get<int?>(key);
            return s;
        }

        public static void SetNullableInt(string key, int? value)
        {
            Set<int?>(key, value);
        }

        public static int? GetShort(string key)
        {
            int? s = Get<int?>(key);
            return s;
        }

        public static void SetShort(string key, int? value)
        {
            Set<int?>(key, value);
        }

        //

        public static bool GetBool(string key)
        {
            bool s = Get<bool>(key);
            return s;
        }

        public static void SetBool(string key, bool value)
        {
            Set<bool>(key, value);
        }

        public static List<EducationLevel> GetEducationLevels(string key)
        {
            List<EducationLevel> s = Session[key] as List<EducationLevel>;
            return s;
        }

        public static void SetEducationLevels(string key, List<EducationLevel> value)
        {
            Session[key] = value;
        }

        public static List<int> GetAppliedLevels(string key)
        {
            List<int> s = Session[key] as List<int>;
            return s;
        }

        public static void SetAppliedLevels(string key, List<int> value)
        {
            Session[key] = value;
        }

        //HaiVT 26/04/2013 - get useraccount
        public static UserAccount GetUserAccount(string key)
        {
            UserAccount s = Session[key] as UserAccount;
            return s;
        }

        //HaiVT 26/04/2013 - set useraccount
        public static void SetUserAccount(string key, UserAccount value)
        {
            Session[key] = value;
        }

        //HaiVT 26/04/2013 - get useraccount
        public static Survey GetSurvey(string key)
        {
            Survey s = Session[key] as Survey;
            return s;
        }

        //HaiVT 26/04/2013 - set useraccount
        public static void SetSurvey(string key, Survey value)
        {
            Session[key] = value;
        }
    }

}
