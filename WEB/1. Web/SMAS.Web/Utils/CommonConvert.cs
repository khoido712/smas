using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.Common;

namespace SMAS.Web.Utils 
{
    public class CommonConvert
    {
        public static string Genre(int Genre)
        {
            string res = "";
            if (SystemParamsInFile.GENRE_MALE == Genre)
            {
                res = "Common_Label_Male";
            }
            else if (SystemParamsInFile.GENRE_FEMALE == Genre)
            {
                res = "Common_Label_Female";
            }
            else
            {
                res = "Common_Label_GerneUndifined";
            }
            return Res.Get(res);
        }
        public static string ConvertSemester(int Semester)
        {
            string res = "";
            if (SystemParamsInFile.SEMESTER_OF_YEAR_FIRST == Semester)
            {
                res = SystemParamsInFile.SEMESTER_I;
            }
            else if (SystemParamsInFile.SEMESTER_OF_YEAR_SECOND == Semester)
            {
                res = SystemParamsInFile.SEMESTER_II;
            }
            else
            {
                res = "Cả năm";
            }
            return Res.Get(res);
        }
        public static string Genrebool(bool Genre)
        {
            string res = "";
            if (true == Genre)
            {
                res = "Common_Label_Male";
            }
            else if (false == Genre)
            {
                res = "Common_Label_Female";
            }
            else
            {
                res = "Common_Label_GerneUndifined";
            }
            return Res.Get(res);
        }
        public static int ConvertToApplied(string EducationGrade)
        { 
            if(EducationGrade == "Cấp 1")
            {
                return 1;
            }
            if (EducationGrade == "Cấp 2")
            {
                return 2;
            }
            if (EducationGrade == "Cấp 3")
            {
                return 3;
            }
            if (EducationGrade == "Nhà trẻ")
            {
                return 4;
            }
            if (EducationGrade == "Mẫu giáo")
            {
                return 5;
            }
            return 0;
        }
        public static string ConvertToResolutionApplied(int Applied)
        {
            if (Applied == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return SystemParamsInFile.STR_PRIMARY;
            }
            if (Applied == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                return SystemParamsInFile.STR_SECONDARY;
            }
            if (Applied == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                return SystemParamsInFile.STR_TERTIARY;
            }
            if (Applied == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
            {
                return SystemParamsInFile.STR_CRECHE;
            }
            if (Applied == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                return SystemParamsInFile.STR_KINDER_GARTEN;
            }
            return "Error";
        }
        public static string SummedUpRecordClass_Status(int? status,int type)
        {
            string res = "";
            if (!status.HasValue)
            {
                res = (type == 1 ? "Common_Label_NotComplete" : "Common_Label_RankingNotComplete");
            }
            else
            {
                if (status.Value == 3)
                {
                    res = (type == 1 ? "Common_Label_Complete" : "Common_Label_RankingComplete");
                }
                if (status.Value == 2)
                {
                    res = (type == 1 ? "Common_Label_Completing" : "Common_Label_Completing");
                }
                if (status.Value == 1)
                {
                    res = (type == 1 ? "Common_Label_NotComplete" : "Common_Label_RankingNotComplete");
                }
            }
            return Res.Get(res);
        }
        public static string Tox(bool? check)
        {
            string Tox = "";
            if (check == null)
            {
                return Tox;
            }
            if (check.Value == true)
            {
                Tox = "x";
            }
            return Tox;
        }
        public static string ToYesNo(bool check)
        {
            string Tox = "";

            if (check == true)
            {
                Tox = "Có";
            }
            else
            {
                Tox = "Không";
            }
            return Tox;
        }
        public static string PhysicalClassification(int PhysicalClassification)
        {
            string res = "";
            if (PhysicalClassification == SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT)
            {
                res = "PHYSICAL_CLASSIFICATION_EXCELLENT";
            }
            if (PhysicalClassification == SystemParamsInFile.PHYSICAL_CLASSIFICATION_GOOD)
            {
                res = "PHYSICAL_CLASSIFICATION_GOOD";
            }
            if (PhysicalClassification == SystemParamsInFile.PHYSICAL_CLASSIFICATION_NORMAL)
            {
                res = "PHYSICAL_CLASSIFICATION_NORMAL";
            }
            if (PhysicalClassification == SystemParamsInFile.PHYSICAL_CLASSIFICATION_WEAK)
            {
                res = "PHYSICAL_CLASSIFICATION_WEAK";
            }
            if (PhysicalClassification == SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR)
            {
                res = "PHYSICAL_CLASSIFICATION_POOR";
            }
            return Res.Get(res);
        }
        public static string Nutrition(int? Nutrition)
        {
            string res = "";
            if (Nutrition == null)
            {
                return res;
            }
            if (Nutrition == SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT)
            {
                res = "NUTRITION_TYPE_OVERWEIGHT";
            }
            if (Nutrition == SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT)
            {
                res = "NUTRITION_TYPE_NORMALWEIGHT";
            }
            if (Nutrition == SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION)
            {
                res = "NUTRITION_TYPE_MALNUTRITION";
            }
           
            return Res.Get(res);
        }

        /// <summary>
        /// convert ConductLevelResolution to DisplayConductLevel (Đ, CĐ)
        /// </summary>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static string DisplayConductLevel(string resolution)
        {
            if (resolution.ToLower().Equals(SMAS.Web.Constants.GlobalConstants.CONDUCTLEVELRESOLUTION_D))
            {
                return SMAS.Web.Constants.GlobalConstants.CONDUCTLEVELDISPLAY_D;
            }
            else if (resolution.ToLower().Equals(SMAS.Web.Constants.GlobalConstants.CONDUCTLEVELRESOLUTION_CD))
            {
                return SMAS.Web.Constants.GlobalConstants.CONDUCTLEVELDISPLAY_CD;
            }
            else return string.Empty;
        }

        /// <summary>
        /// Lấy thông tin Kỳ số liệu
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        public static string GetNameByPeriodId(int periodId)
        {
            string periodName = string.Empty;
            if (periodId == SystemParamsInFile.FIRST_PERIOD)
            {
                periodName = Res.Get("Lbl_First_Semester");
            }
            else if (periodId == SystemParamsInFile.MID_PERIOD)
            {
                periodName = Res.Get("Lbl_Mid_Semester");
            }
            else if (periodId == SystemParamsInFile.END_PERIOD)
            {
                periodName = Res.Get("Lbl_End_Semester");
            }
            return periodName;
        }
    }
}
