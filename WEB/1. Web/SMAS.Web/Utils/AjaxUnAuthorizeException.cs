﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SMAS.Web.Utils
{
    public class AjaxUnAuthorizeException: Exception
    {
        public string MessageKey;
        public List<object> Params;
        public AjaxUnAuthorizeException()
            : base()
        {
            // Add implementation (if required)
        }

        public AjaxUnAuthorizeException(string message)
            : base(message)
        {
            // Add implementation (if required)
            this.MessageKey = message;
        }

        public AjaxUnAuthorizeException(string message, System.Exception inner)
            : base(message, inner)
        {
            // Add implementation (if required)
        }

        protected AjaxUnAuthorizeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            // Add implementation (if required)
        }

        public AjaxUnAuthorizeException(string MessageKey, List<object> Params)
        {
            this.MessageKey = MessageKey;
            this.Params = Params;
        }
    }
}