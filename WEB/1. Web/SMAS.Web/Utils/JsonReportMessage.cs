﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;

namespace SMAS.Web.Utils
{
    public class JsonReportMessage
    {
        public const string OLD = "old";
        public const string NEW = "new";
        public const string ERROR = "error";
        public string Type { get; set; }
        public string Message { get; set; }
        public string ReportID { get; set; }
        public string ProcessedDate { get; set; }

        public JsonReportMessage()
        {
        }

        public JsonReportMessage(ProcessedReport processedReport, string type = OLD)
        {
            Type = type;
            ReportID = processedReport.ProcessedReportID.ToString();
            ProcessedDate = processedReport.ProcessedDate.ToString("dd/MM/yyyy");
        }
    }
}