﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

// AuNH1
// For custom pagination

namespace SMAS.Web.Utils
{
    public class Paginate<T>
    {
        // Page num
        public int page { get; set; }
        
        // page size
        public int size { get; set; }


        // Total row of full list
        public int total { get; set; }

        // Paginated List
        public IEnumerable<T> List { get; set; }

        public IQueryable<T> Query { get; set; }

        public Paginate(IQueryable<T> Query)
        {
            this.Query = Query;
        }

        public void paginate() {
            if (page == 0)
                page = 1;

            if (size == 0)
                size = Config.PageSize;

            if (this.Query == null)
            {
                this.total = 0;
                this.List = new List<T>();
            }
            else
            {
                this.total = this.Query.Count();
                this.List = this.Query.Skip((page - 1) * size).Take(size).ToList();
            }
        }
    }
}