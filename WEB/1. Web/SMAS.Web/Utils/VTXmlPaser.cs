﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Web.Mvc;

namespace SMAS.Web.Utils
{
    public class VTXmlPaser
    {
        private string path { get; set; }
        public XmlDataDocument xmldoc { get; set; }

        public VTXmlPaser(string path)
        {
            xmldoc = new XmlDataDocument();
            this.path = path;
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            xmldoc.Load(fs);
            fs.Close();
            fs.Dispose();
            xmldoc = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public List<XmlNode> GetNodes(string NodePath)
        {
            string[] arr = NodePath.Trim().Split('.');


            XmlNodeList nodes = xmldoc.GetElementsByTagName(arr[arr.Length - 1]);

            List<XmlNode> res = new List<XmlNode>();
            foreach (XmlNode node in nodes)
            {
                XmlNode tmp = node;
                bool valid = true;
                for (int i = arr.Length - 2; i >= 0; i--)
                {
                    if (tmp == null || tmp.ParentNode == null || tmp.ParentNode.Name != arr[i])
                    {
                        valid = false;
                        break;
                    }

                    tmp = tmp.ParentNode;
                }

                if (valid)
                {
                    res.Add(node);
                }
            }

            return res;
        }

        public void SaveToFile(string filename)
        {
            this.xmldoc.Save(filename);            
        }

        public static string GetProperty(string fileName,string Param)
        {
            XmlTextReader reader =null;
            string property = string.Empty;

            try
            {
                var physicalPath = Path.Combine(fileName);
                reader = new XmlTextReader(physicalPath);
                reader.Settings.DtdProcessing =DtdProcessing.Prohibit;
                XmlDocument doc = new XmlDocument();
                XmlNode node = doc.ReadNode(reader);
                doc.PreserveWhitespace = false;
                var Listnode = node.SelectNodes(Param);

                if (Listnode != null && Listnode.Count > 0)
                {
                    property = Listnode[0] != null ? Listnode[0].InnerText : "";
                }
                reader.Close();
            }
            catch
            {
                property="";
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return property;
        }

    }
}
