﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using SMAS.Web.Constants;
using SMAS.VTUtils.Utils;
using SMAS.Models.Models;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using SMAS.Business.Common;
using System.Web.Configuration;
using System.Collections;
using System.Web.Helpers;
using System.Web.Mvc;
using System.IO;
using log4net;
using System.Security.Cryptography;


namespace SMAS.Web.Utils
{
    public static class Utils
    {
        private static ILog logger = LogManager.GetLogger(typeof(Utils).Name);
        public static string htmlEndCode(string val)
        {
            return "";
        }
        public static decimal ParseDecimal(object val)
        {
            CultureInfo CultureInfo = CultureInfo.CurrentCulture;
            return Decimal.Parse(val.ToString());
        }
        /// <summary>
        /// Chen them cac ky tu xuong dong (<br />) trong html vao xau 
        /// Duoc cat boi dau cach va dua va doi dai nhap vao se quyet dinh chen the <br/>
        /// </summary>
        /// <param name="title"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string ConvertTitle2Display(string title, int length = 1)
        {
            if (title == null)
            {
                return string.Empty;
            }
            string[] arrTitle = title.Trim().Split(' ');
            int count = arrTitle.Length;
            string res = string.Empty;
            for (int i = 0; i < count; i++)
            {
                res += " " + arrTitle[i];
                if ((i + 1) % length == 0 || i == count - 1)
                {
                    res += "<br />";
                }
            }
            if (res.Equals(string.Empty))
            {
                res = res.Substring(1);
            }
            return res;
        }

        public static object GetValueByProperty(object source, string properties)
        {
            Type f = source.GetType();
            PropertyInfo fproperties = f.GetProperty(properties);
            if (fproperties == null)
            {
                return null;
            }
            else
            {
                object value = fproperties.GetValue(source, null);
                return value;
            }
        }

        /// <summary>
        /// Copy dữ liệu từ đối tượng này sang đối tượng khác
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="ExceptVirtual"></param>
        public static void BindTo(object source, object dest, bool ExceptVirtual = false, bool NullValue = true)
        {
            Type f = source.GetType();
            PropertyInfo[] fproperties = f.GetProperties();

            Type m = dest.GetType();
            PropertyInfo[] mproperties = m.GetProperties();
            List<string> tmp = new List<string>();
            foreach (PropertyInfo p in mproperties)
            {
                tmp.Add(p.Name);
            }

            foreach (PropertyInfo p in fproperties)
            {
                if (!NullValue)
                {
                    if (p.GetValue(source, null) == null)
                    {
                        continue;
                    }
                }
                if (ExceptVirtual)
                {
                    if (p.GetGetMethod().IsVirtual)
                    {
                        continue;
                    }
                }
                if (tmp.Contains(p.Name))
                {
                    object value = p.GetValue(source, null);
                    m.GetProperty(p.Name).SetValue(dest, value, null);
                }
            }
        }

        /// <summary>
        /// Thực hiện cắt bỏ các dấu cách thừa ở 2 đầu đối với các tường kiểu string
        /// </summary>
        /// <param name="obj"></param>
        public static void TrimObject(object obj)
        {
            Type t = obj.GetType();
            PropertyInfo[] ps = t.GetProperties();

            foreach (PropertyInfo p in ps)
            {
                if (p.PropertyType == typeof(string) || p.PropertyType == typeof(String))
                {
                    string str = (string)p.GetValue(obj, null);
                    str = str == null ? null : str.Trim();
                    p.SetValue(obj, str, null);
                }
            }
        }
        /// <summary>
        /// Map tu RoleID sang cap hoc tuong ung
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public static int MapRoleToAppliedLevel(int RoleID)
        {
            int appliedLevel = 0;
            if (RoleID == SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_NHATRE) appliedLevel = SystemParams.GetInt("EDUCATION_GRADE_CHILDCARE");
            else if (RoleID == SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_MAUGIAO) appliedLevel = SystemParams.GetInt("EDUCATION_GRADE_PRESCHOOL");
            else if (RoleID == SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC1) appliedLevel = SystemParams.GetInt("EDUCATION_GRADE_PRIMARY");
            else if (RoleID == SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC2) appliedLevel = SystemParams.GetInt("EDUCATION_GRADE_SECONDARY");
            else if (RoleID == SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC3) appliedLevel = SystemParams.GetInt("EDUCATION_GRADE_TERTIARY");
            return appliedLevel;
        }

        public static int MapAppliedLevelToRole(int appliedLevel)
        {
            int RoleID = 0;
            if (appliedLevel == SystemParams.GetInt("EDUCATION_GRADE_CHILDCARE")) RoleID = SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_NHATRE;
            else if (appliedLevel == SystemParams.GetInt("EDUCATION_GRADE_PRESCHOOL")) RoleID = SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_MAUGIAO;
            else if (appliedLevel == SystemParams.GetInt("EDUCATION_GRADE_PRIMARY")) RoleID = SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC1;
            else if (appliedLevel == SystemParams.GetInt("EDUCATION_GRADE_SECONDARY")) RoleID = SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC2;
            else if (appliedLevel == SystemParams.GetInt("EDUCATION_GRADE_TERTIARY")) RoleID = SMAS.Business.Common.GlobalConstants.ROLE_ADMIN_TRUONGC3;
            return RoleID;
        }

        public static string GetFunctionPath(string URL, List<SMAS.Models.Models.Menu> menus)
        {
            StringBuilder functionPath = new StringBuilder();
            foreach (SMAS.Models.Models.Menu m in menus)
            {
                if (m.URL == URL && m.IsActive == true)
                {
                    IEnumerable<SMAS.Models.Models.Menu> result = menus.Where(o => o.MenuID == m.MenuID);
                    if (result.Count() > 0)
                    {
                        //lay resouceName cua chuc nang cha
                        if (result.First().Menu2 != null)
                        {
                            string MenuParentResourceName = result.First().Menu2.MenuName;
                            string MenuParentName = Resources.MenuResources.ResourceManager.GetString(MenuParentResourceName);
                            if (string.IsNullOrEmpty(MenuParentName))
                            {
                                MenuParentName = MenuParentResourceName;
                            }
                            functionPath.Append(MenuParentName).Append(" >> ");
                        }
                        //
                        string MenuResourceName = result.First().MenuName;
                        string MenuName = Resources.MenuResources.ResourceManager.GetString(MenuResourceName);
                        if (string.IsNullOrEmpty(MenuName))
                        {
                            MenuName = MenuResourceName;
                        }
                        functionPath.Append(MenuName).Append(" >> ");
                    }
                }
            }
            return functionPath.ToString();
            /*
                string[] listMenuID = m.MenuPath.Split('/');
                if (listMenuID.Length > 0)
                {
                    foreach (string sMenuID in listMenuID)
                    {
                        if (!string.IsNullOrEmpty(sMenuID))
                        {
                            int iMenuID = Convert.ToInt32(sMenuID);
                            IEnumerable<Menu> result = menus.Where(o => o.MenuID == iMenuID);
                            if (result.Count() > 0)
                            {
                                string MenuResourceName = result.First().MenuName;
                                string MenuName = Resources.MenuResources.ResourceManager.GetString(MenuResourceName);
                                if (string.IsNullOrEmpty(MenuName))
                                {
                                    MenuName = MenuResourceName;
                                }
                                functionPath.Append(MenuName).Append(" >> ");
                            }
                        }                            
                    }
                    
                }
             */
        }


        /// <summary>
        /// Char[]
        /// </summary>
        /// <author>
        /// vtit_dungnt77--------------04/03/2013
        /// </author>
        public static char[] startingChars = new char[] { '<', '&' };

        /// <summary>
        /// IsDangerousString
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public static bool IsDangerousString(string s)
        {
            if (s == null) s = "";
            int startIndex = 0;
            while (true)
            {
                int num2 = s.IndexOfAny(startingChars, startIndex);
                if (num2 < 0)
                {
                    return false;
                }
                if (num2 == (s.Length - 1))
                {
                    return false;
                }
                char ch = s[num2];
                if (ch != '&')
                {
                    if ((ch == '<') && ((IsAtoZ(s[num2 + 1]) || (s[num2 + 1] == '!')) || ((s[num2 + 1] == '/') || (s[num2 + 1] == '?'))))
                    {
                        return true;
                    }
                }
                else if (s[num2 + 1] == '#')
                {
                    return true;
                }
                startIndex = num2 + 1;
            }
        }
        /// <summary>
        /// Check string is?  contains unexpected character. Character store in array String.
        /// </summary>
        /// <param name="stringCheck"></param>
        /// <param name="arrayExpectedChar"></param>
        /// <returns></returns>
        /// <author>
        /// ducpt1
        /// </author>
        public static bool IsUnexpectedChar(string stringCheck, string[] arrayExpectedChar)
        {
            if (arrayExpectedChar.Any(stringCheck.Contains))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// IsAtoZ
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public static bool IsAtoZ(char c)
        {
            return (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
        }

        /// <summary>
        /// IsPhoneNumber
        /// </summary>
        /// <param name="val1"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public static bool IsPhoneNumber(object val1)
        {
            if (val1 == null)
            {
                return true;
            }
            //dac trung so dien thoai cua viet nam
            //8,9.10 ->max la 15 so
            Regex regexObj =
                new Regex(@"^\+?([0-9]{8})?([0-9]{1})?([0-9]{1})?([0-9]{5})?$");
            List<string> mobileHeader = new List<string>() { "09", "01", "849", "841" };
            if (regexObj.IsMatch((string)val1))
            {
                string valStr = (string)val1;
                string temp = valStr.Substring(0, 2);
                if (mobileHeader.Contains(temp))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ham them dau cach tu dong NAMTA
        /// </summary>
        /// <param name="strGrid"></param>
        /// <param name="pos"></param>
        /// <param name="type">=1 doi voi cac doi tuong ko chua dau cach =2 doi voi doi tuong co dau cach</param>
        /// <returns></returns>
        public static string AutoInsertSpace(string strGrid, int pos, int type = 1)
        {
            if (type == 1 && strGrid != null)
            {
                while (pos < strGrid.Length)
                {
                    strGrid = strGrid.Insert(pos, " ");
                    pos += pos;
                }
            }
            else if (type == 2)
            {
                var arrStr = strGrid.Split(' ');
                strGrid = "";
                var lenPos = pos;
                string strTmp = "";
                if (arrStr != null && arrStr.Count() > 0)
                {

                    for (int i = 0; i < arrStr.Count(); i++)
                    {
                        if (arrStr[i].Count() > lenPos)
                        {
                            if (i != 0)
                                strGrid = " " + strGrid;
                            strGrid = AutoInsertSpace(arrStr[i], lenPos);
                        }
                        else
                        {
                            strTmp += " " + arrStr[i];
                            if (strTmp.Count() > pos)
                            {
                                string sAdd = new string(' ', (pos - strGrid.Count()));
                                strGrid.Insert(strGrid.Count(), sAdd);
                                pos += pos;
                            }
                            else
                            {
                                strGrid = strTmp;
                            }
                        }
                    }


                }
            }
            return strGrid;
        }


        /// <summary>
        /// Kiểm tra có phải là số hay ko
        /// </summary>
        /// <param name="pValue">Value</param>
        /// <param name="Reskey">The reskey.</param>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public static bool ValidateIsNumber(string pValue)
        {
            if (pValue == null) return true;
            foreach (Char c in pValue)
            {

                if (!Char.IsDigit(c))
                {
                    return false;
                }

            }
            return true;

        }
        /// <summary>
        /// validate email
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string emailString)
        {
            try
            {
                string regexPattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
                Match matches = Regex.Match(emailString, regexPattern);
                return matches.Success;

            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// chuyển thành tiếng việt không dấu
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSign(string text)
        {
            //Ky tu dac biet

            /*for (int i = 32; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "-");
            }*/
            //text = text.Replace(".", "-");
            //text = text.Replace(" ", "-");
            //text = text.Replace(",", "-");
            //text = text.Replace(";", "-");
            //text = text.Replace(":", "-");
            //'Dấu Ngang
            text = text.Replace("A", "A");
            text = text.Replace("a", "a");
            text = text.Replace("Ă", "A");
            text = text.Replace("ă", "a");
            text = text.Replace("Â", "A");
            text = text.Replace("â", "a");
            text = text.Replace("E", "E");
            text = text.Replace("e", "e");
            text = text.Replace("Ê", "E");
            text = text.Replace("ê", "e");
            text = text.Replace("I", "I");
            text = text.Replace("i", "i");
            text = text.Replace("O", "O");
            text = text.Replace("o", "o");
            text = text.Replace("Ô", "O");
            text = text.Replace("ô", "o");
            text = text.Replace("Ơ", "O");
            text = text.Replace("ơ", "o");
            text = text.Replace("U", "U");
            text = text.Replace("u", "u");
            text = text.Replace("Ư", "U");
            text = text.Replace("ư", "u");
            text = text.Replace("Y", "Y");
            text = text.Replace("y", "y");

            //    'Dấu Huyền
            text = text.Replace("À", "A");
            text = text.Replace("à", "a");
            text = text.Replace("Ằ", "A");
            text = text.Replace("ằ", "a");
            text = text.Replace("Ầ", "A");
            text = text.Replace("ầ", "a");
            text = text.Replace("È", "E");
            text = text.Replace("è", "e");
            text = text.Replace("Ề", "E");
            text = text.Replace("ề", "e");
            text = text.Replace("Ì", "I");
            text = text.Replace("ì", "i");
            text = text.Replace("Ò", "O");
            text = text.Replace("ò", "o");
            text = text.Replace("Ồ", "O");
            text = text.Replace("ồ", "o");
            text = text.Replace("Ờ", "O");
            text = text.Replace("ờ", "o");
            text = text.Replace("Ù", "U");
            text = text.Replace("ù", "u");
            text = text.Replace("Ừ", "U");
            text = text.Replace("ừ", "u");
            text = text.Replace("Ỳ", "Y");
            text = text.Replace("ỳ", "y");

            //'Dấu Sắc
            text = text.Replace("Á", "A");
            text = text.Replace("á", "a");
            text = text.Replace("Ắ", "A");
            text = text.Replace("ắ", "a");
            text = text.Replace("Ấ", "A");
            text = text.Replace("ấ", "a");
            text = text.Replace("É", "E");
            text = text.Replace("é", "e");
            text = text.Replace("Ế", "E");
            text = text.Replace("ế", "e");
            text = text.Replace("Í", "I");
            text = text.Replace("í", "i");
            text = text.Replace("Ó", "O");
            text = text.Replace("ó", "o");
            text = text.Replace("Ố", "O");
            text = text.Replace("ố", "o");
            text = text.Replace("Ớ", "O");
            text = text.Replace("ớ", "o");
            text = text.Replace("Ú", "U");
            text = text.Replace("ú", "u");
            text = text.Replace("Ứ", "U");
            text = text.Replace("ứ", "u");
            text = text.Replace("Ý", "Y");
            text = text.Replace("ý", "y");

            //'Dấu Hỏi
            text = text.Replace("Ả", "A");
            text = text.Replace("ả", "a");
            text = text.Replace("Ẳ", "A");
            text = text.Replace("ẳ", "a");
            text = text.Replace("Ẩ", "A");
            text = text.Replace("ẩ", "a");
            text = text.Replace("Ẻ", "E");
            text = text.Replace("ẻ", "e");
            text = text.Replace("Ể", "E");
            text = text.Replace("ể", "e");
            text = text.Replace("Ỉ", "I");
            text = text.Replace("ỉ", "i");
            text = text.Replace("Ỏ", "O");
            text = text.Replace("ỏ", "o");
            text = text.Replace("Ổ", "O");
            text = text.Replace("ổ", "o");
            text = text.Replace("Ở", "O");
            text = text.Replace("ở", "o");
            text = text.Replace("Ủ", "U");
            text = text.Replace("ủ", "u");
            text = text.Replace("Ử", "U");
            text = text.Replace("ử", "u");
            text = text.Replace("Ỷ", "Y");
            text = text.Replace("ỷ", "y");

            //'Dấu Ngã   
            text = text.Replace("Ã", "A");
            text = text.Replace("ã", "a");
            text = text.Replace("Ẵ", "A");
            text = text.Replace("ẵ", "a");
            text = text.Replace("Ẫ", "A");
            text = text.Replace("ẫ", "a");
            text = text.Replace("Ẽ", "E");
            text = text.Replace("ẽ", "e");
            text = text.Replace("Ễ", "E");
            text = text.Replace("ễ", "e");
            text = text.Replace("Ĩ", "I");
            text = text.Replace("ĩ", "i");
            text = text.Replace("Õ", "O");
            text = text.Replace("õ", "o");
            text = text.Replace("Ỗ", "O");
            text = text.Replace("ỗ", "o");
            text = text.Replace("Ỡ", "O");
            text = text.Replace("ỡ", "o");
            text = text.Replace("Ũ", "U");
            text = text.Replace("ũ", "u");
            text = text.Replace("Ữ", "U");
            text = text.Replace("ữ", "u");
            text = text.Replace("Ỹ", "Y");
            text = text.Replace("ỹ", "y");

            //'Dẫu Nặng
            text = text.Replace("Ạ", "A");
            text = text.Replace("ạ", "a");
            text = text.Replace("Ặ", "A");
            text = text.Replace("ặ", "a");
            text = text.Replace("Ậ", "A");
            text = text.Replace("ậ", "a");
            text = text.Replace("Ẹ", "E");
            text = text.Replace("ẹ", "e");
            text = text.Replace("Ệ", "E");
            text = text.Replace("ệ", "e");
            text = text.Replace("Ị", "I");
            text = text.Replace("ị", "i");
            text = text.Replace("Ọ", "O");
            text = text.Replace("ọ", "o");
            text = text.Replace("Ộ", "O");
            text = text.Replace("ộ", "o");
            text = text.Replace("Ợ", "O");
            text = text.Replace("ợ", "o");
            text = text.Replace("Ụ", "U");
            text = text.Replace("ụ", "u");
            text = text.Replace("Ự", "U");
            text = text.Replace("ự", "u");
            text = text.Replace("Ỵ", "Y");
            text = text.Replace("ỵ", "y");
            text = text.Replace("Đ", "D");
            text = text.Replace("đ", "d");
            return text;
        }


        public static string StripVNSignAndSpace(string text, bool removesymbol=false)
        {
            //Ky tu dac biet

            /*for (int i = 32; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "-");
            }*/
            //text = text.Replace(".", "-");
            //text = text.Replace(" ", "-");
            //text = text.Replace(",", "-");
            //text = text.Replace(";", "-");
            //text = text.Replace(":", "-");
            //'Dấu Ngang
            text = text.Replace("A", "A");
            text = text.Replace("a", "a");
            text = text.Replace("Ă", "A");
            text = text.Replace("ă", "a");
            text = text.Replace("Â", "A");
            text = text.Replace("â", "a");
            text = text.Replace("E", "E");
            text = text.Replace("e", "e");
            text = text.Replace("Ê", "E");
            text = text.Replace("ê", "e");
            text = text.Replace("I", "I");
            text = text.Replace("i", "i");
            text = text.Replace("O", "O");
            text = text.Replace("o", "o");
            text = text.Replace("Ô", "O");
            text = text.Replace("ô", "o");
            text = text.Replace("Ơ", "O");
            text = text.Replace("ơ", "o");
            text = text.Replace("U", "U");
            text = text.Replace("u", "u");
            text = text.Replace("Ư", "U");
            text = text.Replace("ư", "u");
            text = text.Replace("Y", "Y");
            text = text.Replace("y", "y");

            //    'Dấu Huyền
            text = text.Replace("À", "A");
            text = text.Replace("à", "a");
            text = text.Replace("Ằ", "A");
            text = text.Replace("ằ", "a");
            text = text.Replace("Ầ", "A");
            text = text.Replace("ầ", "a");
            text = text.Replace("È", "E");
            text = text.Replace("è", "e");
            text = text.Replace("Ề", "E");
            text = text.Replace("ề", "e");
            text = text.Replace("Ì", "I");
            text = text.Replace("ì", "i");
            text = text.Replace("Ò", "O");
            text = text.Replace("ò", "o");
            text = text.Replace("Ồ", "O");
            text = text.Replace("ồ", "o");
            text = text.Replace("Ờ", "O");
            text = text.Replace("ờ", "o");
            text = text.Replace("Ù", "U");
            text = text.Replace("ù", "u");
            text = text.Replace("Ừ", "U");
            text = text.Replace("ừ", "u");
            text = text.Replace("Ỳ", "Y");
            text = text.Replace("ỳ", "y");

            //'Dấu Sắc
            text = text.Replace("Á", "A");
            text = text.Replace("á", "a");
            text = text.Replace("Ắ", "A");
            text = text.Replace("ắ", "a");
            text = text.Replace("Ấ", "A");
            text = text.Replace("ấ", "a");
            text = text.Replace("É", "E");
            text = text.Replace("é", "e");
            text = text.Replace("Ế", "E");
            text = text.Replace("ế", "e");
            text = text.Replace("Í", "I");
            text = text.Replace("í", "i");
            text = text.Replace("Ó", "O");
            text = text.Replace("ó", "o");
            text = text.Replace("Ố", "O");
            text = text.Replace("ố", "o");
            text = text.Replace("Ớ", "O");
            text = text.Replace("ớ", "o");
            text = text.Replace("Ú", "U");
            text = text.Replace("ú", "u");
            text = text.Replace("Ứ", "U");
            text = text.Replace("ứ", "u");
            text = text.Replace("Ý", "Y");
            text = text.Replace("ý", "y");

            //'Dấu Hỏi
            text = text.Replace("Ả", "A");
            text = text.Replace("ả", "a");
            text = text.Replace("Ẳ", "A");
            text = text.Replace("ẳ", "a");
            text = text.Replace("Ẩ", "A");
            text = text.Replace("ẩ", "a");
            text = text.Replace("Ẻ", "E");
            text = text.Replace("ẻ", "e");
            text = text.Replace("Ể", "E");
            text = text.Replace("ể", "e");
            text = text.Replace("Ỉ", "I");
            text = text.Replace("ỉ", "i");
            text = text.Replace("Ỏ", "O");
            text = text.Replace("ỏ", "o");
            text = text.Replace("Ổ", "O");
            text = text.Replace("ổ", "o");
            text = text.Replace("Ở", "O");
            text = text.Replace("ở", "o");
            text = text.Replace("Ủ", "U");
            text = text.Replace("ủ", "u");
            text = text.Replace("Ử", "U");
            text = text.Replace("ử", "u");
            text = text.Replace("Ỷ", "Y");
            text = text.Replace("ỷ", "y");

            //'Dấu Ngã   
            text = text.Replace("Ã", "A");
            text = text.Replace("ã", "a");
            text = text.Replace("Ẵ", "A");
            text = text.Replace("ẵ", "a");
            text = text.Replace("Ẫ", "A");
            text = text.Replace("ẫ", "a");
            text = text.Replace("Ẽ", "E");
            text = text.Replace("ẽ", "e");
            text = text.Replace("Ễ", "E");
            text = text.Replace("ễ", "e");
            text = text.Replace("Ĩ", "I");
            text = text.Replace("ĩ", "i");
            text = text.Replace("Õ", "O");
            text = text.Replace("õ", "o");
            text = text.Replace("Ỗ", "O");
            text = text.Replace("ỗ", "o");
            text = text.Replace("Ỡ", "O");
            text = text.Replace("ỡ", "o");
            text = text.Replace("Ũ", "U");
            text = text.Replace("ũ", "u");
            text = text.Replace("Ữ", "U");
            text = text.Replace("ữ", "u");
            text = text.Replace("Ỹ", "Y");
            text = text.Replace("ỹ", "y");

            //'Dẫu Nặng
            text = text.Replace("Ạ", "A");
            text = text.Replace("ạ", "a");
            text = text.Replace("Ặ", "A");
            text = text.Replace("ặ", "a");
            text = text.Replace("Ậ", "A");
            text = text.Replace("ậ", "a");
            text = text.Replace("Ẹ", "E");
            text = text.Replace("ẹ", "e");
            text = text.Replace("Ệ", "E");
            text = text.Replace("ệ", "e");
            text = text.Replace("Ị", "I");
            text = text.Replace("ị", "i");
            text = text.Replace("Ọ", "O");
            text = text.Replace("ọ", "o");
            text = text.Replace("Ộ", "O");
            text = text.Replace("ộ", "o");
            text = text.Replace("Ợ", "O");
            text = text.Replace("ợ", "o");
            text = text.Replace("Ụ", "U");
            text = text.Replace("ụ", "u");
            text = text.Replace("Ự", "U");
            text = text.Replace("ự", "u");
            text = text.Replace("Ỵ", "Y");
            text = text.Replace("ỵ", "y");
            text = text.Replace("Đ", "D");
            text = text.Replace("đ", "d");
            text = text.Replace(" ", "");

            if (removesymbol)
            {
            if (text.Contains("/"))
            {
                text = text.Replace("/", "_");
            }
            if (text.Contains("["))
            {
                text = text.Replace("[", "_");
            }
            if (text.Contains("]"))
            {
                text = text.Replace("]", "_");
            }
            if (text.Contains(":"))
            {
                text = text.Replace(":", "_");
            }
            if (text.Contains("*"))
            {
                text = text.Replace("*", "_");
            }
            }
            return text;
        }

        /// <summary>
        /// return image scr from binary
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>29/03/2013</date>
        /// <param name="byteArrayIn"></param>
        /// <returns></returns>
        public static string ConvertByteToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn == null)
            {
                return string.Empty;
            }
            return "data:image/png;base64," + @System.Convert.ToBase64String(byteArrayIn);
        }

        /// <summary>
        /// return short sms content
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>31/03/2013</date>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string ToShortSMSContent(string content, int numToShort)
        {
            if (string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            if (content.Length <= numToShort)
            {
                return content;
            }

            string[] tmp = content.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string shortContent = string.Empty;

            if (tmp.Length == 1)
            {
                if (tmp[0].Length > numToShort)
                {
                    shortContent = tmp[0].Substring(0, numToShort) + "...";
                }
                else
                {
                    shortContent = tmp[0];
                }
            }
            else
            {
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0, size = tmp.Length; i < size; i++)
                {
                    if (strBuilder.Length > numToShort)
                    {
                        shortContent = strBuilder.ToString().Trim().Substring(0, numToShort) + "...";
                        break;
                    }
                    if (i == size - 1)
                    {
                        shortContent = strBuilder.ToString().Trim();
                        break;
                    }

                    strBuilder.Append(tmp[i]).Append(" ");
                }
            }
            return shortContent;
        }

        public static string StrimContent(string content, int numToShort)
        {
            if (string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            if (content.Length <= numToShort)
            {
                return content;
            }

            string shortContent = string.Empty;
            for (int i = 0; i < content.Length; i++)
            {
                if (shortContent.Length < numToShort)
                {
                    shortContent += content[i];    
                }
                else
                {
                    shortContent += "...";
                    break;
                }
                
            }
            return shortContent;
        }

        public static string ToShortSMSContentByCharNum(string s, int num)
        {
            if (string.IsNullOrEmpty(s))
            {
                return "";
            }
            string[] arr = s.Split('.');
            string extension = arr[arr.Length - 1];
            string name = s.Remove(s.LastIndexOf('.'));
            string res = "";
            if (s.Length > num)
            {
                res = name.Substring(0, 20) + "..." + extension;
            }
            else
            {
                res = s;
            }
            return res;

        }

        /// <summary>
        /// Split String to list by char
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>01/04/2013</date>
        /// <param name="inputText">input string</param>
        /// <param name="cSlpit">char to split</param>
        /// <returns></returns>
        public static List<string> SplitStringToList(string inputText, char cSlpit)
        {
            if (string.IsNullOrEmpty(inputText))
            {
                return null;
            }
            return new List<string>(inputText.Split(new char[] { cSlpit }));
        }

        /// <summary>
        /// check mobile number is valid
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>01/04/2013</date>
        /// <param name="number"></param>
        /// <returns>true if valid mobile number</returns>
        public static bool CheckMobileNumber(string number)
        {
            List<string> headerNumbers = SplitStringToList(WebConfigurationManager.AppSettings["PreNumber"].ToString(), ',');
            List<string> headerLen9 = null;
            List<string> headerLen10 = null;
            List<string> headerLen11 = null;
            List<string> headerLen12 = null;
            if (WebConfigurationManager.AppSettings["PreNumberLen9"] != null)
            {
                var str9 = WebConfigurationManager.AppSettings["PreNumberLen9"].ToString();
                headerLen9 = new List<string>(str9.Split(new char[] { ',' }));
            }

            if (WebConfigurationManager.AppSettings["PreNumberLen10"] != null)
            {
                var str10 = WebConfigurationManager.AppSettings["PreNumberLen10"].ToString();
                headerLen10 = new List<string>(str10.Split(new char[] { ',' }));
            }

            if (WebConfigurationManager.AppSettings["PreNumberLen11"] != null)
            {
                var str11 = WebConfigurationManager.AppSettings["PreNumberLen11"].ToString();
                headerLen11 = new List<string>(str11.Split(new char[] { ',' }));
            }

            if (WebConfigurationManager.AppSettings["PreNumberLen12"] != null)
            {
                var str12 = WebConfigurationManager.AppSettings["PreNumberLen12"].ToString();
                headerLen12 = new List<string>(str12.Split(new char[] { ',' }));
            }

            if (String.IsNullOrEmpty(number))
            {
                return false;
            }
            if ((number.Length < 9) || (number.Length > 12))
            {
                return false;
            }
            if ((headerNumbers == null) || (headerNumbers.Count == 0))
            {
                return false;
            }

            if (number.Length == 9)
            {
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen9 != null)
                {
                    return headerLen9.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 10)
            {
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen10 != null)
                {
                    return headerLen10.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else if (number.Length == 11)
            {
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen11 != null)
                {
                    return headerLen11.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                string number2 = number.Substring(0, number.Length - 7);
                if (headerLen12 != null)
                {
                    return headerLen12.Exists(a => a.Contains(number2));
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Namta ham check mat khau manh
        /// </summary>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static bool CheckPassRequire(string Password)
        {
            bool characterUper = false;
            bool characterLower = false;
            bool number = false;
            bool symbol = false;
            //kiem tra co kytu khong
            foreach (char ch in Password)
            {
                if ((ch >= 65 && ch <= 90))
                {
                    characterLower = true;
                }
                if ((ch >= 97 && ch <= 122))
                {
                    characterUper = true;
                }
                if (ch >= 48 && ch <= 64)
                {
                    number = true;
                }
                if (ch >= 33 && ch <= 47 || ch >= 58 && ch <= 64)
                {
                    symbol = true;
                }
            }
            if (characterUper && characterLower && symbol && number)
                return true;
            return false;
        }


        /// <summary>
        /// check isCentive content SMS
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>01/04/2013</date>
        /// <param name="content"></param>
        /// <param name="lstSensitive"></param>
        /// <returns></returns>
        public static bool CheckContentSMS(string content)
        {
            string sensitiveWords = WebConfigurationManager.AppSettings["SensitiveKeywords"];
            sensitiveWords = string.IsNullOrEmpty(sensitiveWords) ? string.Empty : sensitiveWords;
            List<string> lstSensitive = SplitStringToList(sensitiveWords, ',');
            if (string.IsNullOrEmpty(content) || (lstSensitive == null))
            {
                return false;
            }
            return lstSensitive.Any(t => content.Contains(t));
        }


        public static string formatMark(object obj, string formatStr)
        {
            if (obj == null) return "";
            try
            {
                if (obj.GetType() == typeof(Decimal))
                {
                    decimal mark = (decimal)obj;
                    if (mark != null)
                    {
                        if (mark == (decimal)10 || mark == (decimal)0)
                        {
                            return mark.ToString("0");
                        }
                        else
                        {
                            return mark.ToString(formatStr);
                        }
                    }
                    else
                    {
                        return "";
                    }
                }

                return obj.ToString();

            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// return list 12month form currentMonth
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>30/05/2013</date>
        /// <returns></returns>
        public static IDictionary<string, object> GetListMonth(IDictionary dic = null)
        {
            IDictionary<string, object> dicMonth = new Dictionary<string, object>();
            DateTime currentDate = DateTime.Now;
            DateTime dateItem = new DateTime();
            for (int i = 12; i >= 0; i--)
            {
                if (currentDate.Month - i > 0)
                {
                    dateItem = new DateTime(currentDate.Year, currentDate.Month - i, 1);
                    dicMonth.Add(new KeyValuePair<string, object>(dateItem.Ticks.ToString(), Res.Get("Common_Label_Month") + SMAS.Web.Constants.GlobalConstants.SPACE + dateItem.ToString(SMAS.Web.Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_MMYY)));
                }
                else
                {
                    dateItem = new DateTime(currentDate.Year - 1, 12 + (currentDate.Month - i), 1);
                    dicMonth.Add(new KeyValuePair<string, object>(dateItem.Ticks.ToString(), Res.Get("Common_Label_Month") + SMAS.Web.Constants.GlobalConstants.SPACE + dateItem.ToString(SMAS.Web.Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_MMYY)));
                }
            }
            return dicMonth;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns>Xuống dòng</returns>
        public static string ConvertStrDownLine(string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                return str.Replace("\r\n", "<br/>");
            }
            else
            {
                return str;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool getSafeFileName(string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                if (c == '/' || c == '\\' || c == 0)
                {
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// convert ticks(in javascript) to DateTime
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>13/06/2013</date>
        /// <param name="value">value of milisecond in javascript</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this long value)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return dateTime.AddMilliseconds(value);
        }

        /// <summary>
        /// convert DateTime ticks(in javascript)
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>13/06/2013</date>
        /// <param name="value">value of Date in C#</param>
        public static long ToTicksJavaScript(this DateTime value)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return (value.Ticks - dateTime.Ticks) / 10000; // 10000 ticks is a millisecond
        }
        public static string htmlEndCode(object obj)
        {
            string trueEncode = "";
            if (obj == null)
                return trueEncode;
            string encode = HttpUtility.HtmlEncode(obj.ToString());
            if (encode.Contains("&#192;"))
                encode = encode.Replace("&#192;", "À");
            if (encode.Contains("&#193;"))
                encode = encode.Replace("&#193;", "Á");
            if (encode.Contains("&#194;"))
                encode = encode.Replace("&#194;", "Â");
            if (encode.Contains("&#195;"))
                encode = encode.Replace("&#195;", "Ã");
            if (encode.Contains("&#200;"))
                encode = encode.Replace("&#200;", "È");
            if (encode.Contains("&#201;"))
                encode = encode.Replace("&#201;", "É");
            if (encode.Contains("&#202;"))
                encode = encode.Replace("&#202;", "Ê");
            if (encode.Contains("&#204;"))
                encode = encode.Replace("&#204;", "Ì");
            if (encode.Contains("&#205;"))
                encode = encode.Replace("&#205;", "Í");
            if (encode.Contains("&#208;"))
                encode = encode.Replace("&#208;", "Đ");
            if (encode.Contains("&#210;"))
                encode = encode.Replace("&#210;", "Ò");
            if (encode.Contains("&#211;"))
                encode = encode.Replace("&#211;", "Ó");
            if (encode.Contains("&#212;"))
                encode = encode.Replace("&#212;", "Ô");
            if (encode.Contains("&#213;"))
                encode = encode.Replace("&#213;", "Õ");
            if (encode.Contains("&#217;"))
                encode = encode.Replace("&#217;", "Ù");
            if (encode.Contains("&#218;"))
                encode = encode.Replace("&#218;", "Ú");
            if (encode.Contains("&#221;"))
                encode = encode.Replace("&#221;", "Ý");
            if (encode.Contains("&#224;"))
                encode = encode.Replace("&#224;", "à");
            if (encode.Contains("&#225;"))
                encode = encode.Replace("&#225;", "á");
            if (encode.Contains("&#226;"))
                encode = encode.Replace("&#226;", "â");
            if (encode.Contains("&#227;"))
                encode = encode.Replace("&#227;", "ã");
            if (encode.Contains("&amp;#7841;"))
                encode = encode.Replace("&amp;#7841;", "ạ");
            if (encode.Contains("&#232;"))
                encode = encode.Replace("&#232;", "è");
            if (encode.Contains("&#233;"))
                encode = encode.Replace("&#233;", "é");
            if (encode.Contains("&#234;"))
                encode = encode.Replace("&#234;", "ê");
            if (encode.Contains("&#236;"))
                encode = encode.Replace("&#236;", "ì");
            if (encode.Contains("&#237;"))
                encode = encode.Replace("&#237;", "í");
            if (encode.Contains("&#242;"))
                encode = encode.Replace("&#242;", "ò");
            if (encode.Contains("&#243;"))
                encode = encode.Replace("&#243;", "ó");
            if (encode.Contains("&#244;"))
                encode = encode.Replace("&#244;", "ô");
            if (encode.Contains("&#245;"))
                encode = encode.Replace("&#245;", "õ");
            if (encode.Contains("&#249;"))
                encode = encode.Replace("&#249;", "ù");
            if (encode.Contains("&#250;"))
                encode = encode.Replace("&#250;", "ú");
            if (encode.Contains("&#253;"))
                encode = encode.Replace("&#253;", "ý");

            trueEncode = encode;
            return trueEncode;

        }

        public static string ConvertIsActiveSMAS(bool? IsActiveSMAS)
        {
            if (IsActiveSMAS.HasValue)
            {
                List<SelectListItem> lsSMAS = new List<SelectListItem>();
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "false" });
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Active"), Value = "true" });

                return lsSMAS.Where(o => o.Value.ToLower() == IsActiveSMAS.Value.ToString().ToLower()).FirstOrDefault().Text.ToString();
            }
            else
            {
                return "";
            }
        }
        public static string ConvertIsActiveSMS(int? IsActiveSMS)
        {
            if (IsActiveSMS.HasValue)
            {
                List<SelectListItem> lsSMS = new List<SelectListItem>();
                lsSMS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
                return lsSMS.Where(o => o.Value.ToLower() == IsActiveSMS.Value.ToString().ToLower()).FirstOrDefault().Text.ToString();
            }
            else
            {
                return "";
            }
        }

        public static DateTime FixDateTime(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }


        /// <summary>
        /// chiendd: 13/08/2014
        /// Method dung de sap xep tieng viet co dau. Cac ky tu tieng viet duoc chuyen sang 1 ky tu theo  1 thu tu
        ///Thu tu dau tieg viet: Khong dau, huyen, sac, hoi, nga, nang
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ConvertVN(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return "";
            }
            //Quy tắc đặt ký tự Ký tự thay thế+dau: ví dụ:à => a01
            //text = text.ToLower();
            //Tu khong dau
            
            text = text.Replace("a", "a00");
            text = text.Replace("A", "a00");
            text = text.Replace("ă", "a10");
            text = text.Replace("Ă", "a10");
            text = text.Replace("â", "a20");
            text = text.Replace("Â", "a20");
            text = text.Replace("e", "e00");
            text = text.Replace("E", "e00");
            text = text.Replace("ê", "e10");
            text = text.Replace("Ê", "e10");
            text = text.Replace("i", "i00");
            text = text.Replace("I", "i00");
            text = text.Replace("o", "o00");
            text = text.Replace("O", "o00");
            text = text.Replace("ô", "o10");
            text = text.Replace("Ô", "o10");
            text = text.Replace("ơ", "o20");
            text = text.Replace("Ơ", "o20");
            text = text.Replace("u", "u00");
            text = text.Replace("U", "u00");
            text = text.Replace("ư", "u10");
            text = text.Replace("Ư", "u10");
            text = text.Replace("y", "y00");
            text = text.Replace("Y", "y00");
               
            
            //    'Dấu Huyền
            text = text.Replace("à", "a01");
            text = text.Replace("À", "a01");
            text = text.Replace("ằ", "a11");
            text = text.Replace("Ằ", "a11");
            text = text.Replace("ầ", "a21");
            text = text.Replace("Ầ", "a21");
            text = text.Replace("è", "e01");
            text = text.Replace("È", "e01");
            text = text.Replace("ề", "e11");
            text = text.Replace("Ề", "e11");
            text = text.Replace("ì", "i01");
            text = text.Replace("Ì", "i01");
            text = text.Replace("ò", "o01");
            text = text.Replace("Ò", "o01");
            text = text.Replace("ồ", "o11");
            text = text.Replace("Ồ", "o11");
            text = text.Replace("ờ", "o21");
            text = text.Replace("Ờ", "o21");
            text = text.Replace("ù", "u01");
            text = text.Replace("Ù", "u01");
            text = text.Replace("ừ", "u11");
            text = text.Replace("Ừ", "u11");
            text = text.Replace("ỳ", "y01");
            text = text.Replace("Ỳ", "y01");
            

            //'Dấu Sắc            
            text = text.Replace("á", "a02");
            text = text.Replace("Á", "a02");
            text = text.Replace("ắ", "a12");
            text = text.Replace("Ắ", "a12");
            text = text.Replace("ấ", "a22");
            text = text.Replace("Ấ", "a22");
            text = text.Replace("é", "e02");
            text = text.Replace("É", "e02");
            text = text.Replace("ế", "e12");
            text = text.Replace("Ế", "e12");
            text = text.Replace("í", "i02");
            text = text.Replace("Í", "i02");
            text = text.Replace("ó", "o02");
            text = text.Replace("Ó", "o02");
            text = text.Replace("ố", "o12");
            text = text.Replace("Ố", "o12");
            text = text.Replace("ớ", "o22");
            text = text.Replace("Ớ", "o22");
            text = text.Replace("ú", "u02");
            text = text.Replace("Ú", "u02");
            text = text.Replace("ứ", "u12");
            text = text.Replace("Ứ", "u12");
            text = text.Replace("ý", "y02");
            text = text.Replace("Ý", "y02");
            

            //'Dấu Hỏi
            text = text.Replace("ả", "a03");
            text = text.Replace("Ả", "a03");
            text = text.Replace("ẳ", "a13");
            text = text.Replace("Ẳ", "a13");
            text = text.Replace("ẩ", "a23");
            text = text.Replace("Ẩ", "a23");
            text = text.Replace("ẻ", "e03");
            text = text.Replace("Ẻ", "e03");
            text = text.Replace("ể", "e13");
            text = text.Replace("Ể", "e13");
            text = text.Replace("ỉ", "i03");
            text = text.Replace("Ỉ", "i03");
            text = text.Replace("ỏ", "o03");
            text = text.Replace("Ỏ", "o03");
            text = text.Replace("ổ", "o13");
            text = text.Replace("Ổ", "o13");
            text = text.Replace("ở", "o23");
            text = text.Replace("Ở", "o23");
            text = text.Replace("ủ", "u03");
            text = text.Replace("Ủ", "u03");
            text = text.Replace("ử", "u13");
            text = text.Replace("Ử", "u13");
            text = text.Replace("ỷ", "y03");
            text = text.Replace("Ỷ", "y03");
            

            //'Dấu Ngã   
            text = text.Replace("ã", "a04");
            text = text.Replace("Ã", "a04");
            text = text.Replace("ẵ", "a14");
            text = text.Replace("Ẵ", "a14");
            text = text.Replace("ẫ", "a24");
            text = text.Replace("Ẫ", "a24");
            text = text.Replace("ẽ", "e04");
            text = text.Replace("Ẽ", "e04");
            text = text.Replace("ễ", "e14");
            text = text.Replace("Ễ", "e14");
            text = text.Replace("ĩ", "i04");
            text = text.Replace("Ĩ", "i04");
            text = text.Replace("õ", "o04");
            text = text.Replace("Õ", "o04");
            text = text.Replace("ỗ", "o14");
            text = text.Replace("Ỗ", "o14");
            text = text.Replace("ỡ", "o24");
            text = text.Replace("Ỡ", "o24");
            text = text.Replace("ũ", "u04");            
            text = text.Replace("Ũ", "u04");
            text = text.Replace("ữ", "u14");
            text = text.Replace("Ữ", "u14");
            text = text.Replace("ỹ", "y04");
            text = text.Replace("Ỹ", "y04");
            
            //'Dẫu Nặng
            text = text.Replace("ạ", "a05");
            text = text.Replace("Ạ", "a05");
            text = text.Replace("ặ", "a15");
            text = text.Replace("Ặ", "a15");
            text = text.Replace("ậ", "a25");
            text = text.Replace("Ậ", "a25");
            text = text.Replace("ẹ", "e05");
            text = text.Replace("Ẹ", "e05");
            text = text.Replace("ệ", "e15");
            text = text.Replace("Ệ", "e15");
            text = text.Replace("ị", "i05");
            text = text.Replace("Ị", "i05");
            text = text.Replace("ọ", "o05");
            text = text.Replace("Ọ", "o05");
            text = text.Replace("ộ", "o15");
            text = text.Replace("Ộ", "o15");
            text = text.Replace("ợ", "o25");
            text = text.Replace("Ợ", "o25");
            text = text.Replace("ụ", "u05");
            text = text.Replace("Ụ", "u05");
            text = text.Replace("ự", "u15");
            text = text.Replace("Ự", "u15");
            text = text.Replace("ỵ", "y05");
            text = text.Replace("Ỵ", "y05");
            text = text.Replace("đ", "dzz");
            text = text.Replace("Đ", "dzz");
            
            return text;
        }

        /// <summary>
        /// Lấy cấp học khi chọn khối. Chỉ áp dụng cho cấp 1,2,3
        /// </summary>
        /// <param name="EducationLevel"></param>
        /// <returns></returns>
        public static int EducationGrade(int EducationLevel)
        {
            if (EducationLevel >= 1 && EducationLevel <= 5)
            {
                return 1;
            }
            else if (EducationLevel >= 6 && EducationLevel <= 9)
            {
                return 2;
            }
            else if (EducationLevel >= 10 && EducationLevel <= 12)
            {
                return 3;
            }
            return 0;
        }

        /// <summary>
        /// Chiendd: Sap xep tieng viet Unicode
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string SortABC(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return "";
            }

            text = ConvertVN(text.ToLower());
            text = text.Replace("a", "01");
            text = text.Replace("b", "02");
            text = text.Replace("c", "03");
            text = text.Replace("d", "04");
            text = text.Replace("e", "05");
            text = text.Replace("f", "06");
            text = text.Replace("g", "07");
            text = text.Replace("h", "08");
            text = text.Replace("i", "09");
            text = text.Replace("j", "10");
            text = text.Replace("k", "11");
            text = text.Replace("l", "12");
            text = text.Replace("m", "13");
            text = text.Replace("n", "14");
            text = text.Replace("o", "15");
            text = text.Replace("p", "16");
            text = text.Replace("q", "17");
            text = text.Replace("r", "18");
            text = text.Replace("s", "19");
            text = text.Replace("t", "20");
            text = text.Replace("u", "21");
            text = text.Replace("v", "22");
            text = text.Replace("w", "23");
            text = text.Replace("x", "24");
            text = text.Replace("y", "25");
            text = text.Replace("z", "26");

            return text;
        }

        /// <summary>
        /// Lay tu cuoi cung trong mot chuoi.
        /// Thuong dung de lay ten, de sap xep.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>Tu cuoi cung trong mot chuoi</returns>
        public static string getLastWord(this string str)
        {
            if (str == null)
            {
                return null;
            }
            else
            {
                int posOfLastSpace = str.LastIndexOf(" ");
                string lastWord = str.Substring(posOfLastSpace + 1);
                return lastWord;
            }
        }


        public static string CheckLockMarkBySemester(int semesterID, string strLockMark, string title, int status, bool isCommenting, string strFinal)
        {
            string STR_DISABLED = "disabled='disabled'";
            string str_disabled = string.Empty;
            string strSemester = string.Empty;

            // kiem tra hoc ki
            if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                strSemester = "HK1";
                if (!string.IsNullOrEmpty(strFinal))
                {
                    strFinal = "CK1";
                }
            }
            else if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "HK2";
                if (title == "T1") title = "T6";
                else if (title == "T2") title = "T7";
                else if (title == "T3") title = "T8";
                else if (title == "T4") title = "T9";
                else if (title == "T5") title = "T10";
                if (!string.IsNullOrEmpty(strFinal))
                {
                    strFinal = "CK2";
                }
            }

            strLockMark = strLockMark.Replace("T10", "TH10");
            title = title.Replace("T10", "TH10");

            if ((!string.IsNullOrEmpty(title) && strLockMark.Contains(title)) || strLockMark.Contains(strSemester) || (!string.IsNullOrEmpty(strFinal) && strLockMark.Contains(strFinal)) || status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                str_disabled = STR_DISABLED;
            }

            return str_disabled;
        }

        public static string CheckLockCheckBoxBySemester(int semesterID, string strLockMark, int monthID,int status)
        {
            strLockMark = strLockMark.Replace("T10", "TH10");
            string STR_DISABLED = "disabled='disabled'";
            string str_disabled = string.Empty;
            string strSemester = string.Empty;
            string title = "T";
            string titleCK = string.Empty;

            if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                strSemester = "HK1";
                if (monthID == 11) // kiem tra dinh ky cuoi ky
                {
                    titleCK = "CK1";
                }
            }
            else if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "HK2";
                if (monthID == 11) // kiem tra dinh ky cuoi ky
                {
                    titleCK = "CK2";
                }
            }

            title = title + monthID;
            if (monthID == 10)
            {
                title = title.Replace("T10", "TH10");
            }

            if ((strLockMark.Contains(title) && !title.Equals("T")) || strLockMark.Contains(strSemester) || status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                str_disabled = STR_DISABLED;
            }
            else if ((strLockMark.Contains(strSemester) || strLockMark.Contains(titleCK)) && title.Equals("T11") || status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                str_disabled = STR_DISABLED;
            }

            return str_disabled;
        }

        public static string CheckVisibledCheckBox(int semesterID, string strLockMark, int monthID, int status)
        {
            strLockMark = strLockMark.Replace("T10", "TH10");
            string STR_DISABLED = "disabled='disabled'";
            string str_disabled = string.Empty;
            string strSemester = string.Empty;
            string title = "T";
            string titleCK = string.Empty;

            if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                strSemester = "HK1";
                titleCK = "CK1";
            }
            else if (semesterID == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                strSemester = "HK2";
                titleCK = "CK2";
            }

            title = title + monthID;
            if (monthID == 10)
            {
                title = title.Replace("T10", "TH10");
            }

            if (((strLockMark.Contains(title) && !title.Equals("T")) && strLockMark.Contains(titleCK)) || strLockMark.Contains(strSemester) || status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                str_disabled = STR_DISABLED;
            }
            else if ((strLockMark.Contains(strSemester) || strLockMark.Contains(titleCK)) && title.Equals("T11") || status != SMAS.Business.Common.GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                str_disabled = STR_DISABLED;
            }

            return str_disabled;
        }

        public static string ConvertStringArrayToStringJoin(string[] array)
        {
            //
            // Use string Join to concatenate the string elements.
            //
            string result = string.Join("", array);
            return result;
        }


        #region Encrypt/Decrypt
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;

        /// <summary>
        /// Encrypt using AES algorithmic 
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        public static string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        public static string Decrypt(string cipherText, string passPhrase)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        public static int SetValueDefault(int? value)
        {
            int iValue = 0;
            if (value.HasValue && value.Value > 0)
            {
                iValue = value.Value;
            }
            else
            {
                iValue = 0;
            }
            return iValue;
        }

        public static string GetNameSemester(int semester, bool toUpper = false, bool shortName = false)
        {
            string strSemester = string.Empty;
            if (shortName)
            {
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    strSemester = "HK1";
                }
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    strSemester = "HK2";
                }
                else
                {
                    strSemester = "CN";
                }
                if (toUpper)
                {
                    strSemester = strSemester.ToUpper();
                }
            }
            else
            {
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    strSemester = "Học kỳ 1";
                }
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    strSemester = "Học kỳ 2";
                }
                else
                {
                    strSemester = "Cả năm";
                }
                if (toUpper)
                {
                    strSemester = strSemester.ToUpper();
                }
            }
            return strSemester;
        }

        /// <summary>
        /// Chuyen doi thu ngay tu TA sang TV
        /// </summary>
        /// <param name="dayofweek"></param>
        /// <returns></returns>
        public static string GetDayVN(DayOfWeek dayofweek)
        {
            string retVal;
            switch (dayofweek.ToString())
            {
                case "Sunday":
                    retVal = "Chủ nhật";
                    break;
                case "Monday":
                    retVal = "Thứ hai";
                    break;
                case "Tuesday":
                    retVal = "Thứ ba";
                    break;
                case "Wednesday":
                    retVal = "Thứ tư";
                    break;
                case "Thursday":
                    retVal = "Thứ năm";
                    break;
                case "Friday":
                    retVal = "Thứ sáu";
                    break;
                case "Saturday":
                    retVal = "Thứ bảy";
                    break;
                default:
                    retVal = "";
                    break;
            }
            return retVal;
        }

        /// <summary>
        /// Lay gia tri ngay cua tuan tu thu ngay
        /// </summary>
        /// <param name="dayofweek"></param>
        /// <returns></returns>
        public static int GetDayOfWeek(DayOfWeek dayofweek)
        {
            int retVal=0;
            switch (dayofweek.ToString())
            {
                case "Sunday":
                    retVal = 1;
                    break;
                case "Monday":
                    retVal = 2;
                    break;
                case "Tuesday":
                    retVal = 3;
                    break;
                case "Wednesday":
                    retVal = 4;
                    break;
                case "Thursday":
                    retVal = 5;
                    break;
                case "Friday":
                    retVal = 6;
                    break;
                case "Saturday":
                    retVal = 7;
                    break;
                default:
                    retVal = 0;
                    break;
            }
            return retVal;
        }

        /*public static string AuthorizeAdmin
        {
            get
            {
                 string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
            return smasUrl + "/Home/AuthorizeAdmin";
            }
           
        }
        public static string LogOn
        {
            get
            {
                string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
                return smasUrl + "/Home/LogOn";
            }
           
        }

        public static string Index
        {
            get
            {
                string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
                return smasUrl + "/Home/Index";
            }
            
        }

        public static string RoleError
        {
            get
            {
                string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
                return smasUrl + "/Home/RoleError";
            }
            
        }

        public static string LogOff
        {
            get
            {
                string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
                return smasUrl + "/Home/LogOff";
            }
           
        }*/


    }
}