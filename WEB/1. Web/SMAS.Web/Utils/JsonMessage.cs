﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.Common;

// AuNH
// Đối tượng chưa thông điệp trả về cho client khi sử dụng JSON

namespace SMAS.Web.Utils
{
    public class JsonMessage
    {
        public const string ERROR = "error";
        public const string SUCCESS = "success";
        public string Type { get; set; }
        public string Message { get; set; }

        public JsonMessage()
        {
        }

        public JsonMessage(BusinessException be)
        {
            string message = "";
            if (be.MessageKey != null && be.MessageKey != "")
            {
                string msg = Res.Get(be.MessageKey);
                List<string> lst = new List<string>();
                foreach (object o in be.Params)
                {
                    lst.Add(Res.Get(o.ToString()));
                    if (Res.Get(o.ToString()) == string.Empty)
                    {
                        lst.Add(o.ToString());
                    }
                }
                message = lst.Count > 0 ? String.Format(msg, lst.ToArray()) : msg;
            }
            else
            {
                message = be.Message;
            }
            this.Type = ERROR;
            this.Message = message;
        }

        public JsonMessage(string Message, string Type = SUCCESS)
        {
            this.Type = Type;
            this.Message = Message;
        }

        public string ToJsonString()
        {
            return "{Type:'" + Type + "', Message:'" + Message + "'}";
        }
    }
}