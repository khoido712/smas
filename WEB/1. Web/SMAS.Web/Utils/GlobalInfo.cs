using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Business.Business;

namespace SMAS.Web.Utils
{
    public class GlobalInfo
    {
        private static GlobalInfo _globalInfo = new GlobalInfo();


        public static GlobalInfo getInstance()
        {
            return _globalInfo;
        }

        public GlobalInfo()
        {

        }

        public string TabIndexTC
        {
            get
            {
                return SessionObject.GetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_TC);
            }
            set
            {
                SessionObject.SetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_TC, value);
            }
        }

        public string TabIndexReport
        {
            get
            {
                return SessionObject.GetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_REPORT);
            }
            set
            {
                SessionObject.SetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_REPORT, value);
            }
        }

        public string TabIndexDOETExam
        {
            get
            {
                return SessionObject.GetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_DOET_EXAM);
            }
            set
            {
                SessionObject.SetString(SMAS.Business.Common.GlobalConstants.SESSION_TAB_DOET_EXAM, value);
            }
        }
        public bool IsShowCalendar
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.SHOW_CALENDAR);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.SHOW_CALENDAR, value);
            }
        }

        public bool IsShowPupilImage
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.SHOW_PUPIL_IMAGE);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.SHOW_PUPIL_IMAGE, value);
            }
        }

        public int UserAccountID
        {
            get
            {
                return SessionObject.GetInt(GlobalConstants.USERACCID);
            }
            set
            {
                SessionObject.SetInt(GlobalConstants.USERACCID, value);
            }
        }

        /// <summary>
        /// T�n thong tin nguoi dang nhap (Employee)
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>15/05/2013</date>
        public string EmployeeName
        {
            get
            {
                return SessionObject.GetString(GlobalConstants.EMPLOYEENAME);
            }
            set
            {
                SessionObject.SetString(GlobalConstants.EMPLOYEENAME, value);
            }
        }

        public static string TokenAuthen
        {
            get
            {
                return SessionObject.GetString("TokenAuthen");
            }
            set
            {
                SessionObject.SetString("TokenAuthen", value);
            }
        }

        /// <summary>
        /// get userAccount
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>26/04/2013</date>
        public UserAccount UserAccount
        {
            get
            {
                return SessionObject.GetUserAccount(GlobalConstants.USERACCOUNT);
            }
            set
            {
                SessionObject.SetUserAccount(GlobalConstants.USERACCOUNT, value);
            }
        }

        /// <summary>
        /// quyen ban giam hieu
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>04/04/2013</date>
        public bool IsRolePrincipal
        {
            get
            {
                bool isRolePrincipal = SessionObject.GetBool(GlobalConstants.IS_ROLE_PRINCIPAL);
                return isRolePrincipal;
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_ROLE_PRINCIPAL, value);
            }
        }

        public int? EmployeeID
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.EMPLOYEEID);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.EMPLOYEEID, value);
            }
        }

        public int RoleID
        {
            get
            {
                return SessionObject.GetInt(GlobalConstants.ROLEID);
            }
            set
            {
                SessionObject.SetInt(GlobalConstants.ROLEID, value);
            }
        }

        public int? GroupID
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.GROUPID);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.GROUPID, value);
            }
        }

        public int? SchoolID
        {
            get
            {
                Nullable<int> id = SessionObject.GetNullableInt(GlobalConstants.SCHOOLID);
                if (id.HasValue)
                {
                    return id;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.SCHOOLID, value);
            }
        }

        public int? ProductVersionID
        {
            get
            {
                Nullable<int> id = SessionObject.GetNullableInt(GlobalConstants.PRODUCT_VERSION);
                if (id.HasValue)
                {
                    return id;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.PRODUCT_VERSION, value);
            }
        }

        public Survey Survey
        {
            get
            {
                return SessionObject.GetSurvey(GlobalConstants.CURRENT_SURVEY);
            }
            set
            {
                SessionObject.SetSurvey(GlobalConstants.CURRENT_SURVEY, value);
            }
        }


        public string SchoolName
        {
            get
            {
                return SessionObject.GetString(GlobalConstants.SCHOOL_NAME);
            }
            set
            {
                SessionObject.SetString(GlobalConstants.SCHOOL_NAME, value);
            }
        }

        public string FunctionPath
        {
            get
            {
                return SessionObject.GetString(GlobalConstants.FUNCTION_PATH);
            }
            set
            {
                SessionObject.SetString(GlobalConstants.FUNCTION_PATH, value);
            }
        }

        public int? AcademicYearID
        {
            get
            {
                Nullable<int> id = SessionObject.GetNullableInt(GlobalConstants.ACADEMICYEAR_ID);
                if (id.HasValue)
                {
                    return id;
                }
                else
                {
                    return null;
                    //return id;
                }
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.ACADEMICYEAR_ID, value);
            }
        }

        public int? ClassID
        {
            get
            {
                Nullable<int> id = SessionObject.GetNullableInt(GlobalConstants.CLASSID);
                if (id.HasValue)
                {
                    return id;
                }
                else
                {
                    //  return 10;
                    return id;
                }
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.CLASSID, value);
            }
        }

        public int? Semester
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.SEMESTER);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.SEMESTER, value);
            }
        }

        public int? EducationLevel
        {
            get
            {
                return SessionObject.GetShort(GlobalConstants.EDUCATION_LEVEL);
            }
            set
            {
                SessionObject.SetShort(GlobalConstants.EDUCATION_LEVEL, value);
            }
        }

        public int? SupervisingDeptID
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.SUPERVISINGDEPT_ID);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.SUPERVISINGDEPT_ID, value);
            }
        }

        public string SuperVisingDeptName
        {
            get
            {
                return SessionObject.GetString(GlobalConstants.SUPERVISINGDEPT_NAME);
            }
            set
            {
                SessionObject.SetString(GlobalConstants.SUPERVISINGDEPT_NAME, value);
            }
        }

        public bool IsAdmin
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_ADMIN);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_ADMIN, value);
            }
        }

        public bool IsSystemAdmin
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_SYSTEM_ADMIN);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SYSTEM_ADMIN, value);
            }
        }

        public bool IsCurrentYear
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IsCurrentYear);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IsCurrentYear, value);
            }
        }
        public bool IsSuperRole
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_SUPERROLE);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SUPERROLE, value);
            }
        }

        public bool IsSchoolRole
        {
            get
            {
                bool isSchoolRole = SessionObject.GetBool(GlobalConstants.IS_SCHOOL_ROLE);
                return isSchoolRole;

            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SCHOOL_ROLE, value);
            }
        }

        public bool IsAdminSchoolRole
        {
            get
            {
                bool isSchoolAdmin = SessionObject.GetBool(GlobalConstants.IS_SCHOOL_ADMIN);
                return isSchoolAdmin;

            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SCHOOL_ADMIN, value);
            }
        }

        /// <summary>
        /// user so
        /// </summary>
        public bool IsSuperVisingDeptRole
        {
            get
            {
                bool isSuperVisingDeptRole = SessionObject.GetBool(GlobalConstants.IS_SUPERVISINGDEPT_ROLE);
                return isSuperVisingDeptRole;
                //return true;

            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SUPERVISINGDEPT_ROLE, value);
            }
        }

        /// <summary>
        /// user phong
        /// </summary>
        public bool IsSubSuperVisingDeptRole
        {
            get
            {
                bool isSuperVisingDeptRole = SessionObject.GetBool(GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE);
                return isSuperVisingDeptRole;
                //return false;

            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE, value);
            }
        }



        /// <summary>
        /// nguoi dung co phai la cap So giao duc ko? 
        /// </summary>
        public bool IsProvinceLevel
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_PROVINCE_LEVEL);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_PROVINCE_LEVEL, value);
            }
        }
        /// <summary>
        /// nguoi dung co phai cap Phong giao duc ko
        /// </summary>
        public bool IsDistrictLevel
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_DISTRICT_LEVEL);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_DISTRICT_LEVEL, value);
            }
        }

        public int Selected_academicYear_Index
        {
            get
            {
                return SessionObject.GetInt(GlobalConstants.SELECTED_ACADEMICYEAR_INDEX);
            }
            set
            {
                SessionObject.SetInt(GlobalConstants.SELECTED_ACADEMICYEAR_INDEX, value);
            }
        }

        public int Selected_appliedLevel_Index
        {
            get
            {
                return SessionObject.GetInt(GlobalConstants.SELECTED_APPLIEDLEVEL_INDEX);
            }
            set
            {
                SessionObject.SetInt(GlobalConstants.SELECTED_APPLIEDLEVEL_INDEX, value);
            }
        }



        public int? ProvinceID
        {
            get
            {
                return SessionObject.GetShort(GlobalConstants.PROVINCEID);
            }
            set
            {
                SessionObject.SetShort(GlobalConstants.PROVINCEID, value);
            }
        }

        public int? DistrictID
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.DISTRICTID);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.DISTRICTID, value);
            }
        }

        public List<EducationLevel> EducationLevels
        {
            get
            {
                return SessionObject.GetEducationLevels(GlobalConstants.EDUCATION_LEVELS);
            }
            set
            {
                SessionObject.SetEducationLevels(GlobalConstants.EDUCATION_LEVELS, value);
            }
        }

        public List<int> AppliedLevels
        {
            get
            {
                return SessionObject.GetAppliedLevels(GlobalConstants.APPLIED_LEVELS);

            }
            set
            {
                SessionObject.SetAppliedLevels(GlobalConstants.APPLIED_LEVELS, value);
            }
        }

        /*
         *  AppliedLevel
            AppliedLevels
            EducationLevels
         */
        public int? AppliedLevel
        {
            get
            {
                int? id = SessionObject.GetNullableInt(GlobalConstants.APPLIED_LEVEL);
                if (id.HasValue)
                {
                    return id.Value;
                }
                else
                {
                    // return 1;
                    return null;
                }

            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.APPLIED_LEVEL, value);
            }
        }

        public int? Grade
        {
            get
            {
                int? grade = SessionObject.GetNullableInt(GlobalConstants.GRADE);
                if (!grade.HasValue)
                {
                    // return 2;
                    return grade;
                }
                else
                {
                    return grade.Value;
                }
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.GRADE, value);
            }
        }

        public bool HasHeadTeacherPermission(int ClassID)
        {
            // TODO: Code to check HasHeadTeacherPermission
            return true;
        }

        public bool HasSubjectTeacherPermission(int ClassID, int SubjectID)
        {
            // TODO: Code to check HasSubjectTeacherPermission
            return true;
        }

        public bool HasOverSeePermission(int ClassID)
        {
            // TODO: Code to check HasOverSeePermission
            return true;
        }

        public int? TrainingType
        {
            get
            {
                return SessionObject.GetShort(GlobalConstants.TRAINING_TYPE);
            }
            set
            {
                SessionObject.SetShort(GlobalConstants.TRAINING_TYPE, value);
            }
        }

        public bool IsViewAll
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_VIEW_ALL);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_VIEW_ALL, value);
            }
        }

        public bool IsEmployeeManager
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_EMPLOYEE_MANAGER);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_EMPLOYEE_MANAGER, value);
            }
        }

        /// <summary>
        /// lay thang theo hoc ky
        /// </summary>
        /// <param name="semesterID"></param>
        /// <param name="objAcademicYear"></param>
        /// <author>HoanTV5</author>
        /// <returns></returns>
        public int GetMonthBySemester(int semesterID, AcademicYear objAcademicYear)
        {
            DateTime dateTimeNow = DateTime.Now;
            int monthID = 0;

            if (semesterID == 1)
            {

                if (dateTimeNow.Date <= objAcademicYear.FirstSemesterStartDate.Value)
                {
                    monthID = 1;
                }
                else if (dateTimeNow.Date >= objAcademicYear.FirstSemesterEndDate.Value)
                {
                    monthID = 5;
                }
                else if (dateTimeNow.Month == 8)
                {
                    monthID = 1;
                }
                else if (dateTimeNow.Month == 9)
                {
                    monthID = 2;
                }
                else if (dateTimeNow.Month == 10)
                {
                    monthID = 3;
                }
                else if (dateTimeNow.Month == 11)
                {
                    monthID = 4;
                }
                else if (dateTimeNow.Month == 12)
                {
                    monthID = 5;
                }
                else if (dateTimeNow.Month < 8)
                {
                    monthID = 1;
                }
                else
                {
                    monthID = 5;
                }
            }
            else if (semesterID == 2)
            {
                if (dateTimeNow.Date <= objAcademicYear.SecondSemesterStartDate.Value)
                {
                    monthID = 6;
                }
                else if (dateTimeNow.Date >= objAcademicYear.SecondSemesterEndDate.Value)
                {
                    monthID = 10;
                }
                if (dateTimeNow.Month == 1)
                {
                    monthID = 6;
                }
                else if (dateTimeNow.Month == 2)
                {
                    monthID = 7;
                }
                else if (dateTimeNow.Month == 3)
                {
                    monthID = 8;
                }
                else if (dateTimeNow.Month == 4)
                {
                    monthID = 9;
                }
                else if (dateTimeNow.Month == 5)
                {
                    monthID = 10;
                }
                else if (dateTimeNow.Month > 5)
                {
                    monthID = 10;
                }
                else
                {
                    monthID = 6;
                }
            }
            return monthID;
        }

        public int? MonthInAcademicYear
        {
            get
            {
                return SessionObject.GetNullableInt(GlobalConstants.MONTH);
            }
            set
            {
                SessionObject.SetNullableInt(GlobalConstants.MONTH, value);
            }
        }

        public SMAS.Business.BusinessObject.UserInfoBO GetUserLogin(string userName)
        {
            SMAS.Business.BusinessObject.UserInfoBO objUser;
            objUser = new Business.BusinessObject.UserInfoBO
            {
                UserAccountId = _globalInfo.UserAccountID,
                UserName = userName
            };
            if (_globalInfo.IsSystemAdmin)
            {
                objUser.Id = _globalInfo.UserAccountID;
                objUser.FullName = "admin";
                return objUser;
            }
            else if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                objUser.Id = _globalInfo.SupervisingDeptID ?? 0;
                objUser.FullName = _globalInfo.SuperVisingDeptName;
                return objUser;
            }
            else if (_globalInfo.IsAdminSchoolRole)
            {
                objUser.Id = _globalInfo.SchoolID ?? 0;
                objUser.FullName = _globalInfo.SchoolName;
                return objUser;
            }
            else if (_globalInfo.EmployeeID.HasValue)
            {
                objUser.Id = _globalInfo.EmployeeID ?? 0;
                objUser.FullName = _globalInfo.EmployeeName;
                return objUser;
            }
            return objUser;
        }

        /// <summary>
        /// Kiem tra co phai la ky hien tai khong
        /// </summary>
        /// <param name="academicYear"></param>
        /// <returns></returns>
        public bool isCurrentSemester(AcademicYear academicYear, int semesterId)
        {
            //Hoc ky 1
            if (academicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.FirstSemesterEndDate && semesterId==GlobalConstants.FIRST_SEMESTER)
            {
                return true;
            }
            if (academicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= academicYear.SecondSemesterEndDate && semesterId==GlobalConstants.SECOND_SEMESTER)
            {
                return true;
            }
            return false;
        }

        public bool IsNewSchoolModel
        {
            get
            {
                return SessionObject.GetBool(GlobalConstants.IS_NEW_SCHOOL_MODEL);
            }
            set
            {
                SessionObject.SetBool(GlobalConstants.IS_NEW_SCHOOL_MODEL, value);
            }
        }
    }


}
