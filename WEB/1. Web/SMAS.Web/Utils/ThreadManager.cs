﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace SMAS.Web.Utils
{
    /// <summary>
    /// Singleton class for thread management 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>12/17/2012</date>
    public class ThreadManager
    {
        private static ThreadManager instance = null;

        private List<Thread> ListThread = new List<Thread>();

        private ThreadManager()
        {
        }

        public static ThreadManager getInstance()
        {
            if (instance == null)
            {
                instance = new ThreadManager();
            }
            return instance;
        }

        /// <summary>
        /// Them mot tien trinh moi
        /// </summary>
        /// <param name="thread"></param>
        public void AddThread(Thread thread)
        {
            ListThread.Add(thread);
        }

        /// <summary>
        /// Xoa cac tien trinh da chay xong
        /// </summary>
        public void Refresh()
        {
            List<int> lstInd = new List<int>();
            int i = 0;
            foreach (Thread thread in ListThread)
            {
                if (!thread.IsAlive)
                {
                    lstInd.Add(i);
                }
                i++;
            }
            lock (ListThread)
            {
                for (int j = lstInd.Count - 1; j >= 0; j--)
                {
                    ListThread.RemoveAt(j);
                }
            }
        }

        public List<Thread> GetAllThread()
        {
            this.Refresh();
            return this.ListThread;
        }

        /// <summary>
        /// Kiem tra xem mot tien trinh co dang chay hay khong
        /// </summary>
        /// <param name="threadName"></param>
        /// <returns></returns>
        public bool IsRuning(string threadName)
        {
            foreach (Thread thread in ListThread)
            {
                if (thread.IsAlive && thread.Name == threadName)
                {
                    return true;
                }
            }

            return false;
        }
    }
}