﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Utils
{
    public class MenuExamination
    {
        public string KeyMenu { get; set; }
        public int ValueMenu { get; set; }
        public string LinkMenu { get; set; }
        public string MenuName { get; set; }
        public int? OrderNumber { get; set; }
    }
}