﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Utils
{
    public class Res
    {
        public static string Get(string name)
        {
            string res = Resources.Resource.ResourceManager.GetString(name);
            res = res == null ? name : res;
            return res;
        }

        public static string GetMenuName(string name)
        {
            string res = Resources.MenuResources.ResourceManager.GetString(name);
            res = res == null ? name : res;
            return res;
        }

        public static string Required()
        {
            return Get("Common_Label_Required");
        }

        public static string CboChoose()
        {
            return Get("SelectRK");
        }

        public static string CboAll()
        {
            return Get("All");
        }

        public static string GetJsonErrorMessage(ModelStateDictionary ModelState)
        {
            string res = "[";
            foreach (var err in ModelState.Values)
            {
                foreach (var errMsg in err.Errors)
                {
                    if(res != "[")
                        res += ", '" + errMsg.ErrorMessage + "'";
                    else
                        res += "'" + errMsg.ErrorMessage + "'";
                }
            }

            res += "]";

            return res;
        }
    }
}