﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace SMAS.Web.Utils
{
    public class Config
    {
        public static int PageSize
    {
            get
            {
                return int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            }
        }

        public static string ExceptClassSeparateID
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ExceptClassSeparateID"];
            }
        }
        public static string GetUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["UrlUpload"];
            }
        }
        public static string UrlUpload
        {
            get
            {
                string UploadFolder = ConfigurationManager.AppSettings["UploadFolder"];
                return GetUrl + UploadFolder + "/";
            }
        }

        public static string UrlHelp
        {
            get
            {
                string helpFolder = ConfigurationManager.AppSettings["HelpFolder"];
                return GetUrl + helpFolder + "/";
            }
        }

        public static IEnumerable<string> GetExtentions
        {
            get
            {
                string extenitions = ConfigurationManager.AppSettings["ExtentionsFile"];
                IEnumerable<string> result = !string.IsNullOrEmpty(extenitions) ? extenitions.Split(',') : Enumerable.Empty<string>();
                //List<string> result = extenitions.Split(new char[] { ',' }).ToList();
                return result;
            }
        }

        public static string ExtentionFile
        {
            get
            {
                string extenitions = ConfigurationManager.AppSettings["ExtentionsFile"];
                return extenitions;
            }
        }


        public static string UsernameUpload
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["UsernameUpload"];
            }
        }

        public static string PasswordUpload
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["PasswordUpload"];
            }
        }

        public static Stream DownloadFileFromHTTP(string httpurl)
        {
            // Get the object used to communicate with the server.
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpurl);
            request.Method = WebRequestMethods.Http.Get;

            // This example assumes the HTTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(Config.UsernameUpload, Config.PasswordUpload);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();

            return responseStream;
        }

        public static Stream DownloadDocumentFile(string httpurl)
        {
            MemoryStream memStream;
            using (FileStream fileStream = File.OpenRead(httpurl))
            {
                memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            }
            return memStream;
        }

        public static string UploadPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["UploadPath"];
            }
        }

        /// <summary>
        /// duong dan file template excel
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        //public static string TeamplateExcel
        //{
        //    get
        //    {
        //        return System.Configuration.ConfigurationManager.AppSettings["TemplateExcel"];
        //    }
        //}
    }
}