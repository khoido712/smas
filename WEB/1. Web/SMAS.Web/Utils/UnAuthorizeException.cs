﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SMAS.Web.Utils
{
    public class UnAuthorizeException: Exception
    {
        public string MessageKey;
        public List<object> Params;
        public UnAuthorizeException()
            : base()
        {
            // Add implementation (if required)
        }

        public UnAuthorizeException(string message)
            : base(message)
        {
            // Add implementation (if required)
            this.MessageKey = message;
        }

        public UnAuthorizeException(string message, System.Exception inner)
            : base(message, inner)
        {
            // Add implementation (if required)
        }

        protected UnAuthorizeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            // Add implementation (if required)
        }

        public UnAuthorizeException(string MessageKey, List<object> Params)
        {
            this.MessageKey = MessageKey;
            this.Params = Params;
        }
    }
}