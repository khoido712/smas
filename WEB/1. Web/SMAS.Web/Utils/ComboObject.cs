﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Utils
{
    public class ComboObject
    {
        public string key { get; set; }
        public string value { get; set; }

        public ComboObject(string key, string value)
        {
            this.key = key;
            this.value = value;
        }
        public ComboObject()
        {
        }
    }

    public class MenuObject
    {
        public string key { get; set; }
        public string value { get; set; }
        public string className {get;set;}

        public MenuObject(string key, string value,string className)
        {
            this.key = key;
            this.value = value;
            this.className=className;
        }
        public MenuObject()
        {
        }
    }
}