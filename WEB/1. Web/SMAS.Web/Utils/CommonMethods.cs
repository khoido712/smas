﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.BusinessObject;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Business.Common.Util;

namespace SMAS.Web.Utils
{
    public static class CommonMethods
    {
        /// <summary>
        /// check account Phong/So/GDTX/Truong de set prefix
        /// </summary>
        /// <author>HoanTV5</author>
        /// <returns></returns>
        public static string GetPrefix(string prefix, bool hasColon = true)
        {
            string result;
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (prefix == null)
            {
                string schoolName = GlobalInfo.SchoolName;
                if (GlobalInfo.IsSuperVisingDeptRole || GlobalInfo.IsSubSuperVisingDeptRole)
                {
                    schoolName = GlobalInfo.SuperVisingDeptName;
                }
                if (GlobalInfo.IsSuperVisingDeptRole)
                {
                    if (!Utils.StripVNSign(schoolName).ToUpper().StartsWith("SO "))
                    {
                        schoolName = "Sở " + schoolName;
                    }
                    else
                    {
                        schoolName = "Sở " + schoolName.Substring("So ".Length, schoolName.Length - "So ".Length);
                    }
                }
                else if (GlobalInfo.IsSubSuperVisingDeptRole)
                {
                    if (!Utils.StripVNSign(schoolName).ToUpper().StartsWith("PHONG "))
                    {
                        schoolName = "Phòng " + schoolName;
                    }
                    else
                    {
                        schoolName = "Phòng " + schoolName.Substring("Phong ".Length, schoolName.Length - "Phong ".Length);
                    }
                }
                else if (GlobalInfo.TrainingType == 3)
                {
                    if (!Utils.StripVNSign(schoolName).ToUpper().StartsWith("TRUNG TAM "))
                    {
                        schoolName = "Trung tâm " + schoolName;
                    }
                    else
                    {
                        schoolName = "Trung tâm " + schoolName.Substring("Trung tam ".Length, schoolName.Length - "Trung tam ".Length);
                    }
                }
                else
                {
                    if (!Utils.StripVNSign(schoolName).ToUpper().StartsWith("TRUONG "))
                    {
                        schoolName = "Trường " + schoolName;
                    }
                    else
                    {
                        schoolName = "Trường " + schoolName.Substring("Truong ".Length, schoolName.Length - "Truong ".Length);
                    }
                }

                result = schoolName + GlobalConstantsEdu.COMMON_COLON_SPACE;
                //AnhVD9 - 20151026 Bỏ dấu : đối với các bản tin # Trao đổi
                if (!hasColon) result = schoolName + GlobalConstantsEdu.SPACE;
            }
            else
            {
                if (prefix != string.Empty)
                {
                    result = prefix + GlobalConstantsEdu.COMMON_COLON_SPACE;
                    //AnhVD9 - 20151026 Bỏ dấu : đối với các bản tin # Trao đổi
                    if (!hasColon) result = prefix + GlobalConstantsEdu.SPACE;
                }
                else
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        /// <summary>
        /// get brand name for sms
        /// </summary>
        /// <returns></returns>
        public static string GetBrandName()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            string brandName = "";
            if (GlobalInfo.IsSuperVisingDeptRole) brandName = "Sở ";
            else if (GlobalInfo.IsSubSuperVisingDeptRole) brandName = "Phòng ";
            else if (GlobalInfo.TrainingType == 3) brandName = "Trung tâm ";
            else brandName = "Trường ";
            return brandName;
        }

        public static string ConvertLineToSpace(string value)
        {
            return value.Replace(Environment.NewLine, GlobalConstantsEdu.SPACE).Replace(GlobalConstantsEdu.R_CHAR, GlobalConstantsEdu.SPACE).Replace(GlobalConstantsEdu.N_CHAR, GlobalConstantsEdu.SPACE);
        }

        public static string GetStatusContractDetail(this int ID)
        {
            string strResult = string.Empty;
            if (ID == GlobalConstantsEdu.STATUS_REGISTED)
            {
                strResult = "Đang hoạt động";
            }
            else if (ID == GlobalConstantsEdu.STATUS_NOT_REGISTED)
            {
                strResult = Res.Get("SMSParentContract_Label_Not_Registed");
            }
            else if (ID == GlobalConstantsEdu.STATUS_STOP_USING)
            {
                strResult = "Tạm ngừng SD";
            }
            else if (ID == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL)
            {
                strResult = Res.Get("Lbl_Delete_Contract_Detail");
            }
            else if (ID == GlobalConstantsEdu.STATUS_UNPAID)
            {
                strResult = Res.Get("SMSParentContract_Label_Unpaid");
            }
            else if (ID == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID)
            {
                strResult = Res.Get("SMSParentContract_Label_Overdue_Unpaid");
            }
            return strResult;
        }


        /// <summary>
        /// Đếm số tin nhắn
        /// </summary>
        /// <param name="content"></param>
        /// <param name="isSign"></param>
        /// <returns></returns>
        public static int countNumSMS(this string content, bool isSign = false)
        {
            if (string.IsNullOrEmpty(content)) return 0;
            // Với tin nhắn có dấu
            if (isSign)
            {
                if (content.Length <= GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT) return 1;
                else if (content.Length % GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT == 0) return content.Length / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT;
                else return content.Length / GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT + 1;
            }
            else // tin nhắn không dấu
            {
                if (content.Length % GlobalConstantsEdu.COMMON_SMS_COUNT == 0) return content.Length / GlobalConstantsEdu.COMMON_SMS_COUNT;
                else return content.Length / GlobalConstantsEdu.COMMON_SMS_COUNT + 1;
            }
        }

        public static string FormatMoney(decimal money, char seperator = ',')
        {
            return money.ToString("#" + seperator + "##0") + " VNĐ";
        }
    }
}