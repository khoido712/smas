﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.Common;

namespace SMAS.Web.Utils
{
    public static class CommonList
    {

        public static List<ComboObject> AppliedTypeSubject()
        {
            GlobalInfo global = new GlobalInfo();
            List<ComboObject> ListAppliedTypeSubject = new List<ComboObject>();
            ListAppliedTypeSubject.Add(new ComboObject(0.ToString(), Res.Get("AppliedTypeSubject_Require")));
            ListAppliedTypeSubject.Add(new ComboObject(1.ToString(), Res.Get("AppliedTypeSubject_Choice")));
            if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ListAppliedTypeSubject.Add(new ComboObject(2.ToString(), Res.Get("AppliedTypeSubject_ChoiceAddPiority")));
                ListAppliedTypeSubject.Add(new ComboObject(3.ToString(), Res.Get("AppliedTypeSubject_ChoiceCalculatorMark")));
                ListAppliedTypeSubject.Add(new ComboObject(4.ToString(), Res.Get("AppliedTypeSubject_ApparentShip")));
            }
            return ListAppliedTypeSubject;
        }

        public static List<ComboObject> IsCommentingSubject()
        {
            List<ComboObject> ListCommentingSubject = new List<ComboObject>();
            ListCommentingSubject.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE.ToString(), Res.Get("Common_Label_IsCommentingJudge")));
            ListCommentingSubject.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_MARK.ToString(), Res.Get("Common_Label_IsCommentingMark")));
            //ListCommentingSubject.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE.ToString(), Res.Get("Common_Label_IsCommentingMark_Judge")));
            return ListCommentingSubject;
        }



        public static List<ComboObject> SchoolEducationGrade()
        {

            List<ComboObject> ListSchoolEducationGrade = new List<ComboObject>();
            ListSchoolEducationGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("APPLIED_LEVEL_PRIMARY")));
            ListSchoolEducationGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("APPLIED_LEVEL_SECONDARY")));
            ListSchoolEducationGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("APPLIED_LEVEL_TERTIARY")));
            return ListSchoolEducationGrade;
        }


        public static List<ComboObject> DisabledRefractive()
        {

            List<ComboObject> ListDisabledRefractive = new List<ComboObject>();
            ListDisabledRefractive.Add(new ComboObject(SystemParamsInFile.DISABLED_REFRACTIVE_MYOPIC.ToString(), Res.Get("DISABLED_REFRACTIVE_MYOPIC")));
            ListDisabledRefractive.Add(new ComboObject(SystemParamsInFile.DISABLED_REFRACTIVE_FARSIGHTEDNESS.ToString(), Res.Get("DISABLED_REFRACTIVE_FARSIGHTEDNESS")));
            ListDisabledRefractive.Add(new ComboObject(SystemParamsInFile.DISABLED_REFRACTIVE_ASTIGMATISM.ToString(), Res.Get("DISABLED_REFRACTIVE_ASTIGMATISM")));
            ListDisabledRefractive.Add(new ComboObject(SystemParamsInFile.DISABLED_REFRACTIVE_OTHER.ToString(), Res.Get("EyeTest_Label_OtherEyeDiseases")));
            ListDisabledRefractive.Add(new ComboObject(SystemParamsInFile.DISABLED_REFRACTIVE_NORMAL.ToString(), Res.Get("EyeTest_Label_Nomal")));
            return ListDisabledRefractive;
        }
        /// <summary>
        /// NutritionType
        /// </summary>
        /// <returns>
        /// List{ComboObject}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public static List<ComboObject> NutritionType()
        {

            List<ComboObject> ListNutritionType = new List<ComboObject>();
            ListNutritionType.Add(new ComboObject(SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT.ToString(), Res.Get("NUTRITION_TYPE_OVERWEIGHT")));
            ListNutritionType.Add(new ComboObject(SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT.ToString(), Res.Get("NUTRITION_TYPE_NORMALWEIGHT")));
            ListNutritionType.Add(new ComboObject(SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION.ToString(), Res.Get("NUTRITION_TYPE_MALNUTRITION")));

            return ListNutritionType;
        }
        /// <summary>
        /// PhysicalClassification
        /// </summary>
        /// <returns>
        /// List{ComboObject}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public static List<ComboObject> PhysicalClassification()
        {

            List<ComboObject> ListPhysicalClassification = new List<ComboObject>();
            ListPhysicalClassification.Add(new ComboObject(SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT.ToString(), Res.Get("PHYSICAL_CLASSIFICATION_EXCELLENT")));
            ListPhysicalClassification.Add(new ComboObject(SystemParamsInFile.PHYSICAL_CLASSIFICATION_GOOD.ToString(), Res.Get("PHYSICAL_CLASSIFICATION_GOOD")));
            ListPhysicalClassification.Add(new ComboObject(SystemParamsInFile.PHYSICAL_CLASSIFICATION_NORMAL.ToString(), Res.Get("PHYSICAL_CLASSIFICATION_NORMAL")));
            ListPhysicalClassification.Add(new ComboObject(SystemParamsInFile.PHYSICAL_CLASSIFICATION_WEAK.ToString(), Res.Get("PHYSICAL_CLASSIFICATION_WEAK")));
            ListPhysicalClassification.Add(new ComboObject(SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR.ToString(), Res.Get("PHYSICAL_CLASSIFICATION_POOR")));
            return ListPhysicalClassification;
        }
        /// <summary>
        /// ItemExamination
        /// </summary>
        /// <returns>
        /// List{ComboObject}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public static List<ComboObject> ItemExamination()
        {
            List<ComboObject> ListItemExamination = new List<ComboObject>();
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_PHYSICAL.ToString(), Res.Get("Item_Examination_Physical")));
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_EYE.ToString(), Res.Get("Item_Examination_Eye")));
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_TEETH.ToString(), Res.Get("Item_Examination_Teeth")));
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_ENT.ToString(), Res.Get("Item_Examination_Ent")));
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_SPINE.ToString(), Res.Get("Item_Examination_Spine")));
            ListItemExamination.Add(new ComboObject(SystemParamsInFile.ITEM_EXAMINATION_OVERRALL.ToString(), Res.Get("Item_Examination_Overrall")));
            return ListItemExamination;
        }

        public static List<ComboObject> Semester()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_Combo_FirstSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_Combo_SecondSemester")));
            return ListSemester;
        }

        public static List<ComboObject> Semester_SK()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_Combo_FirstSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_ALL.ToString(), Res.Get("Common_Label_AllYear")));
            return ListSemester;
        }

        public static List<ComboObject> Semester_GeneralSummed()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI.ToString(), Res.Get("MarkRecord_Label_MidSemester1")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI.ToString(), Res.Get("MarkRecord_Label_EndSemester1")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII.ToString(), Res.Get("MarkRecord_Label_MidSemester2")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII.ToString(), Res.Get("MarkRecord_Label_EndSemester2")));
            return ListSemester;
        }
        public static List<ComboObject> Semester_CommunicationNoteReport()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject("1", Res.Get("MarkRecord_Label_MidSemester1")));
            ListSemester.Add(new ComboObject("2", Res.Get("MarkRecord_Label_EndSemester1")));
            ListSemester.Add(new ComboObject("3", Res.Get("MarkRecord_Label_MidSemester2")));
            ListSemester.Add(new ComboObject("4", Res.Get("MarkRecord_Label_EndSemester2")));
            return ListSemester;
        }

        public static List<ComboObject> SemesterPrimary()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_Combo_FirstSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_Combo_SecondSemester_Primary")));
            return ListSemester;
        }
        public static List<ComboObject> SemesterAndAll()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_FirstSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_SecondSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_ALL.ToString(), Res.Get("Common_Label_AllYear")));
            return ListSemester;
        }

        public static List<ComboObject> SemesterForConduct()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_FirstSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_SecondSemester")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_ALL.ToString(), Res.Get("Common_Label_AllYear")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING.ToString(), Res.Get("Common_Label_BackTraining")));
            return ListSemester;
        }


        public static List<ComboObject> GenreAndAll()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_ALL.ToString(), Res.Get("Common_Combo_All")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_MALE.ToString(), Res.Get("Common_Label_Male")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_FEMALE.ToString(), Res.Get("Common_Label_Female")));
            return ListSemester;
        }


        public static List<ComboObject> GenreAndAllPS()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_ALL.ToString(), Res.Get("Common_Combo_All")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_MALE.ToString(), Res.Get("Common_Label_Male")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_FEMALE.ToString(), Res.Get("Common_Label_Female")));
            return ListSemester;
        }

        public static List<ComboObject> GenreMN()
        {
            List<ComboObject> ListGenre = new List<ComboObject>();
            ListGenre.Add(new ComboObject(SystemParamsInFile.GENRE_MALE.ToString(), Res.Get("Common_Label_Boys")));
            ListGenre.Add(new ComboObject(SystemParamsInFile.GENRE_FEMALE.ToString(), Res.Get("Common_Label_Girls")));
            return ListGenre;
        }
        public static List<ComboObject> GenreAndSelect()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_ALL.ToString(), Res.Get("Common_Combo_Choose")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_FEMALE.ToString(), Res.Get("Common_Label_Female")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.GENRE_MALE.ToString(), Res.Get("Common_Label_Male")));
            return ListSemester;
        }

        public static List<ComboObject> ListAreaName()
        {
            List<ComboObject> ListResult = new List<ComboObject>();
            ListResult.Add(new ComboObject("1", Res.Get("Đô thị")));
            ListResult.Add(new ComboObject("2", Res.Get("Đồng bằng")));
            ListResult.Add(new ComboObject("3", Res.Get("Miền núi - vùng sâu")));
            ListResult.Add(new ComboObject("4", Res.Get("Vùng cao – hải đảo")));
            return ListResult;
        }

        public static List<ComboObject> OrderType()
        {
            List<ComboObject> ListOrderType = new List<ComboObject>();
            ListOrderType.Add(new ComboObject(SystemParamsInFile.ORDER_TYPE_WEEK.ToString(), Res.Get("Common_Label_OrderTypeWeek")));
            ListOrderType.Add(new ComboObject(SystemParamsInFile.ORDER_TYPE_MONTH.ToString(), Res.Get("Common_Label_OrderTypeMonth")));
            return ListOrderType;
        }


        public static List<ComboObject> PupilStatus()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_ALL.ToString(), Res.Get("Common_Combo_All")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString(), Res.Get("Common_Label_PupilStatusStuding")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_GRADUATED.ToString(), Res.Get("Common_Label_PupilStatusGraduated")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL.ToString(), Res.Get("Common_Label_PupilStatusMoved")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF.ToString(), Res.Get("Common_Label_PupilStatusLeaved")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS.ToString(), Res.Get("Common_Label_PupilStatusMovedToOtherClass")));


            return ListSemester;
        }
        //trangdd
        public static List<ComboObject> HealthPeriod()
        {
            List<ComboObject> ListHealthPeriod = new List<ComboObject>();
            ListHealthPeriod.Add(new ComboObject(SystemParamsInFile.HEALTH_PERIOD_ONE.ToString(), Res.Get("Common_Label_HealthPeriodOne")));
            ListHealthPeriod.Add(new ComboObject(SystemParamsInFile.HEALTH_PERIOD_TWO.ToString(), Res.Get("Common_Label_HealthPeriodTwo")));
            return ListHealthPeriod;
        }
        //trangdd
        public static List<ComboObject> HealthStatus()
        {
            List<ComboObject> ListHealthStatus = new List<ComboObject>();
            ListHealthStatus.Add(new ComboObject(SystemParamsInFile.HEALTH_STATUS_ALL.ToString(), Res.Get("Common_Label_HealthStatusAll")));
            ListHealthStatus.Add(new ComboObject(SystemParamsInFile.HEALTH_STATUS_DK.ToString(), Res.Get("Common_Label_HealthStatusDK")));
            ListHealthStatus.Add(new ComboObject(SystemParamsInFile.HEALTH_STATUS_CK.ToString(), Res.Get("Common_Label_HealthStatusCK")));
            return ListHealthStatus;
        }
        //dungnt
        public static List<ComboObject> BloodType()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_ALL.ToString(), Res.Get("Common_Combo_All")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_A.ToString(), Res.Get("Common_Label_BloodTypeA")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_B.ToString(), Res.Get("Common_Label_BloodTypeB")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_AB.ToString(), Res.Get("Common_Label_BloodTypeAB")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_O.ToString(), Res.Get("Common_Label_BloodTypeO")));
            //ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_OTHER.ToString(), Res.Get("Common_Label_BloodTypeOther")));


            return ListSemester;
        }

        //Hieund
        public static List<ComboObject> BloodTypeVsOther()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_A.ToString(), Res.Get("Common_Label_BloodTypeA")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_B.ToString(), Res.Get("Common_Label_BloodTypeB")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_AB.ToString(), Res.Get("Common_Label_BloodTypeAB")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_O.ToString(), Res.Get("Common_Label_BloodTypeO")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.BLOOD_TYPE_OTHER.ToString(), Res.Get("Common_Label_BloodTypeOther")));
            return ListSemester;
        }

        public static List<ComboObject> EducationGrade()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("Common_Label_SEG_TERTIARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));

            return ListGrade;
        }

        //Danh sach cap hoc cho So
        public static List<ComboObject> AppliedLevelForProvince()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("Common_Label_SEG_TERTIARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
            return ListGrade;
        }

        //Danh sach cap hoc cho Phong
        public static List<ComboObject> AppliedLevelForDistrict()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
            ListGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
            return ListGrade;
        }

        // Loai thong tin
        public static List<ComboObject> InfoTypeOfPupil()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", "Hồ sơ"));
            ListType.Add(new ComboObject("2", "Kết quả học tập"));
            return ListType;
        }
        public static List<ComboObject> InfoTypeOfChildren()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", "Hồ sơ"));
            ListType.Add(new ComboObject("2", "Kết quả sức khỏe"));
            return ListType;
        }

        public static List<ComboObject> TypeOfReport()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", Res.Get("Theo giáo viên")));
            ListType.Add(new ComboObject("2", Res.Get("Theo lớp học")));
            return ListType;
        }

        public static List<ComboObject> TypeOfInsuranceTicket()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", Res.Get("Đã cấp thẻ")));
            ListType.Add(new ComboObject("2", Res.Get("Chưa cấp thẻ")));
            return ListType;
        }

        public static List<ComboObject> StatusGraduationAprroval()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", Res.Get("Chưa gửi xét duyệt")));
            ListType.Add(new ComboObject("2", Res.Get("Đã xét duyệt")));
            ListType.Add(new ComboObject("3", Res.Get("Chờ xét duyệt")));
            ListType.Add(new ComboObject("4", Res.Get("Hủy xét duyệt")));
            return ListType;
        }

        public static List<ComboObject> StatusSchedules()
        {
            List<ComboObject> ListType = new List<ComboObject>();
            ListType.Add(new ComboObject("1", Res.Get("Đã lên lịch")));
            ListType.Add(new ComboObject("2", Res.Get("Chưa lên lịch")));
            ListType.Add(new ComboObject("3", Res.Get("Đã phê duyệt")));
            ListType.Add(new ComboObject("4", Res.Get("Chưa phê duyệt")));
            ListType.Add(new ComboObject("5", Res.Get("Hủy phê duyệt")));
            return ListType;
        }

        public static List<ComboObject> ApliedLevelCustom() {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject("1", "1"));
            ListGrade.Add(new ComboObject("2", "2"));
            ListGrade.Add(new ComboObject("3", "3"));
            return ListGrade;
        }

        public static List<ComboObject> DayOfWeek()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject("2", "Thứ 2"));
            ListGrade.Add(new ComboObject("3", "Thứ 3"));
            ListGrade.Add(new ComboObject("4", "Thứ 4"));
            ListGrade.Add(new ComboObject("5", "Thứ 5"));
            ListGrade.Add(new ComboObject("6", "Thứ 6"));
            ListGrade.Add(new ComboObject("7", "Thứ 7"));
            ListGrade.Add(new ComboObject("8", "Chủ nhật"));
            return ListGrade;
        }
      

        public static List<ComboObject> FillingLock()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject("1", "Đã khóa"));
            ListGrade.Add(new ComboObject("2","Chưa khóa"));        
            return ListGrade;
        }

        public static List<ComboObject> AssignmentStatus()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            ListGrade.Add(new ComboObject("1", "Đã phân công"));
            ListGrade.Add(new ComboObject("2", "Chưa phân công"));
            return ListGrade;
        }

        public static List<ComboObject> MonthOfYear()
        {
            List<ComboObject> ListCommentingSubject = new List<ComboObject>();

            for (int i = 1; i < 13; i++) {
                ListCommentingSubject.Add(new ComboObject(i.ToString(),"Tháng " +i.ToString()));
            }

            
            
            //ListCommentingSubject.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE.ToString(), Res.Get("Common_Label_IsCommentingMark_Judge")));
            return ListCommentingSubject;
        }

        public static List<ComboObject> EducationGradeSchool()
        {
            List<ComboObject> ListGrade = new List<ComboObject>();
            //Trường chỉ có cấp 1 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
            //Trường chỉ có cấp 2 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
            //Trường chỉ có cấp 3 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
            //Trường có cấp 1,2 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
            //Trường có cấp 2,3 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
            //Trường có cả 3 cấp 1,2,3 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));


            //Cấp nhà trẻ 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
            //Cấp mẫu giáo 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
            //Trường mầm non (có cả cấp nhà trẻ và mẫu giáo) 
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
            // "Mầm non + Phổ thông", gồm những cấp học: mẫu giáo, nhà trẻ, 1, 2, 3.
            ListGrade.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));
            return ListGrade;
        }
        public static List<int> EducationLevelByGradeSchool(int GradeID)
        {
            List<int> ListEducationLevel = new List<int>();
            switch (GradeID)
            { 
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY:
                    ListEducationLevel = new List<int>() { 1, 2, 3, 4, 5 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY:
                    ListEducationLevel = new List<int>() { 6, 7, 8, 9 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY:
                    ListEducationLevel = new List<int>() { 10, 11, 12 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY:
                    ListEducationLevel = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY:
                    ListEducationLevel = new List<int>() { 6, 7, 8, 9, 10, 11, 12 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL:
                    ListEducationLevel = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE:
                    ListEducationLevel = new List<int>() { 13, 14, 15 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN:
                    ListEducationLevel = new List<int>() { 16, 17, 18 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL:
                    ListEducationLevel = new List<int>() { 13, 14, 15, 16, 17, 18 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS:
                    ListEducationLevel = new List<int>() { 1 , 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
                    break;
                default:
                    ListEducationLevel = new List<int>();
                    break;
            }
            return ListEducationLevel;
        }
        public static List<int?> AppliedLevelByGradeSchool(int GradeID)
        {
            List<int?> ListAppliedLevel = new List<int?>();
            switch (GradeID)
            {
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY:
                    ListAppliedLevel = new List<int?>() { 1 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY:
                    ListAppliedLevel = new List<int?>() { 2 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY:
                    ListAppliedLevel = new List<int?>() { 3 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY:
                    ListAppliedLevel = new List<int?>() { 1, 2 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY:
                    ListAppliedLevel = new List<int?>() { 2, 3 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL:
                    ListAppliedLevel = new List<int?>() { 1, 2, 3 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE:
                    ListAppliedLevel = new List<int?>() { 4 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN:
                    ListAppliedLevel = new List<int?>() { 5 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL:
                    ListAppliedLevel = new List<int?>() { 4, 5 };
                    break;
                case SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS:
                    ListAppliedLevel = new List<int?>() { 1, 2, 3, 4, 5 };
                    break;
                default:
                    ListAppliedLevel = new List<int?>();
                    break;
            }
            return ListAppliedLevel;
        }
        
        public static List<ComboObject> EmployeeStatus()
        {
            List<ComboObject> ListES = new List<ComboObject>();

            ListES.Add(new ComboObject(SystemParamsInFile.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Common_Label_EmployeeStatusWorking")));
            ListES.Add(new ComboObject(SystemParamsInFile.EMPLOYMENT_STATUS_MOVED.ToString(), Res.Get("Common_Label_EmployeeStatusWorkingChange")));
            ListES.Add(new ComboObject(SystemParamsInFile.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Common_Label_EmployeeStatusRetired")));
            //ListES.Add(new ComboObject(SystemParamsInFile.EMPLOYMENT_STATUS_TEMPORARYLY_LEAVED_OFF.ToString(), Res.Get("Common_Label_EmployeeStatusLeavedOff")));
            ListES.Add(new ComboObject(SystemParamsInFile.EMPLOYMENT_STATUS_SEVERANCE.ToString(), Res.Get("Employee_Label_EmployeeStatusSeverance")));

            return ListES;
        }

        public static List<ComboObject> ExemptType()
        {
            List<ComboObject> ListExemptScope = new List<ComboObject>();
            //ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_TYPE_PRACTICE.ToString(), Res.Get("Common_Label_ExemptTypePractice")));
            ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_TYPE_ALL.ToString(), Res.Get("Common_Label_ExemptTypeAll")));
            //ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_TYPE_OTHER.ToString(), Res.Get("Common_Label_ExemptTypeOther")));
            return ListExemptScope;
        }

        public static List<ComboObject> ExemptScope()
        {
            List<ComboObject> ListExemptScope = new List<ComboObject>();
            ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_SCOPE_FISRT_SEMESTER.ToString(), Res.Get("Common_Label_ExemptScopeFirstSemester")));
            ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_SCOPE_SECOND_SEMESTER.ToString(), Res.Get("Common_Label_ExemptScopeSecondSemester")));
            ListExemptScope.Add(new ComboObject(SystemParamsInFile.EXEMPT_SCOPE_FULL_YEAR.ToString(), Res.Get("Common_Label_ExemptScopeFullYear")));
            return ListExemptScope;
        }

        public static List<ComboObject> ForeignLanguageTraining()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS.ToString(), Res.Get("Common_Label_ForeignLanguageTraining3")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_7_YEARS.ToString(), Res.Get("Common_Label_ForeignLanguageTraining7")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_10_YEARS.ToString(), Res.Get("Common_Label_ForeignLanguageTraining10")));
            return ListSemester;
        }

        public static List<ComboObject> ForeignLanguageTrainingUnidentified()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED.ToString(), Res.Get("Common_Label_ForeignLanguageTrainingUnidentified")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS.ToString(), Res.Get("Common_Label_ForeignLanguageTraining3")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_7_YEARS.ToString(), Res.Get("Common_Label_ForeignLanguageTraining7")));
            return ListSemester;
        }

        public static List<ComboObject> EnrolmentType()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject(SystemParamsInFile.PUPIL_STATUS_ALL.ToString(), Res.Get("Common_Combo_All")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_SELECTED.ToString(), Res.Get("Common_Label_EnrolmentTypeSelected")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION.ToString(), Res.Get("Common_Label_EnrolmentTypePassExamination")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL.ToString(), Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool")));

            //ListSemester.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_OTHER.ToString(), Res.Get("Common_Label_EnrolmentTypeOther")));
            return ListSemester;
        }

        public static List<ComboObject> EnrolmentTypeAvailable()
        {
            List<ComboObject> ListEnrolmentType = new List<ComboObject>();
            ListEnrolmentType.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_PASSED_EXAMINATION.ToString(), Res.Get("Common_Label_EnrolmentTypePassExamination")));
            ListEnrolmentType.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL.ToString(), Res.Get("Common_Label_EnrolmentTypeMoveFromOtherSchool")));
            ListEnrolmentType.Add(new ComboObject(SystemParamsInFile.ENROLMENT_TYPE_SELECTED.ToString(), Res.Get("Common_Label_EnrolmentTypeSelected")));
            return ListEnrolmentType;
        }

        public static List<ComboObject> JudgeRecordMark(bool isAll)
        {
            List<ComboObject> ListJudgeRecord = new List<ComboObject>();
            if (isAll)
                ListJudgeRecord.Add(new ComboObject(string.Empty, string.Empty));
            ListJudgeRecord.Add(new ComboObject(Res.Get("D"), Res.Get("Common_Label_D")));
            ListJudgeRecord.Add(new ComboObject(Res.Get("CD"), Res.Get("Common_Label_CD")));
            return ListJudgeRecord;
        }


        public static List<ComboObject> ListMark()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.NO_MARK.ToString(), ""));
            ListSemester.Add(new ComboObject(SystemParamsInFile.MARK_A_PLUS.ToString(), Res.Get("Common_Label_MarkAPlus")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.MARK_A.ToString(), Res.Get("Common_Label_MarkA")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.MARK_B.ToString(), Res.Get("Common_Label_MarkB")));


            return ListSemester;
        }

        public static List<ComboObject> MenuType()
        {
            List<ComboObject> ListMenuType = new List<ComboObject>();
            ListMenuType.Add(new ComboObject(SystemParamsInFile.MENU_TYPE_GENERAL.ToString(), Res.Get("Common_Label_Menu_Type_General")));
            ListMenuType.Add(new ComboObject(SystemParamsInFile.MENU_TYPE_ADDITIONAL.ToString(), Res.Get("Common_Label_Menu_Type_Additional")));
            return ListMenuType;
        }
        public static List<ComboObject> MarkType()
        {
            List<ComboObject> ListMarkType = new List<ComboObject>();
            ListMarkType.Add(new ComboObject(Res.Get("V"), Res.Get("V")));
            ListMarkType.Add(new ComboObject(Res.Get("HK"), Res.Get("HK")));
            return ListMarkType;
        }

        public static List<ComboObject> IsCommenting()
        {
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_MARK.ToString(), Res.Get("Common_Label_Mark")));
            ListSemester.Add(new ComboObject(SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE.ToString(), Res.Get("Common_Label_Judge")));
            return ListSemester;
        }

        public static List<ComboObject> EvaluationHealth()
        {
            List<ComboObject> ListEvaluationHealth = new List<ComboObject>();
            ListEvaluationHealth.Add(new ComboObject(SystemParamsInFile.EVALUATION_HEALTH_0.ToString(), ""));
            ListEvaluationHealth.Add(new ComboObject(SystemParamsInFile.EVALUATION_HEALTH_A.ToString(), Res.Get("Common_Label_EvaluationHealthA")));
            ListEvaluationHealth.Add(new ComboObject(SystemParamsInFile.EVALUATION_HEALTH_B.ToString(), Res.Get("Common_Label_EvaluationHealthB")));
            ListEvaluationHealth.Add(new ComboObject(SystemParamsInFile.EVALUATION_HEALTH_C.ToString(), Res.Get("Common_Label_EvaluationHealthC")));
            return ListEvaluationHealth;
        }

        public static List<ComboObject> HealthState()
        {
            List<ComboObject> ListHealthState = new List<ComboObject>();
            ListHealthState.Add(new ComboObject(SystemParamsInFile.HEALTH_STATE_DISEASE.ToString(), Res.Get("Common_Label_HealthStateDisease")));
            ListHealthState.Add(new ComboObject(SystemParamsInFile.HEALTH_STATE_NORMAL.ToString(), Res.Get("Common_Label_HealthStateNormal")));
            return ListHealthState;
        }

        public static List<ComboObject> MessageReadStatus()
        {
            List<ComboObject> ListMessageReadStatus = new List<ComboObject>();
            ListMessageReadStatus.Add(new ComboObject("true", Res.Get("Common_Label_ReadMessage")));
            ListMessageReadStatus.Add(new ComboObject("false", Res.Get("Common_Label_UnreadMessage")));
            return ListMessageReadStatus;
        }

        public static List<ComboObject> SMSType()
        {
            List<ComboObject> ListSMSType = new List<ComboObject>();
            ListSMSType.Add(new ComboObject(SystemParamsInFile.SMS_TYPE_FOR_PARENT.ToString(), Res.Get("Common_Label_SMS_ForParent")));
            ListSMSType.Add(new ComboObject(SystemParamsInFile.SMS_TYPE_FOR_TEACHER.ToString(), Res.Get("Common_Label_SMS_ForTeacher")));
            ListSMSType.Add(new ComboObject(SystemParamsInFile.SMS_TYPE_CAPACITY.ToString(), Res.Get("Common_Label_SMS_Capacity")));
            ListSMSType.Add(new ComboObject(SystemParamsInFile.SMS_TYPE_CONDUCT.ToString(), Res.Get("Common_Label_SMS_Conduct")));
            return ListSMSType;
        }

        public static List<ComboObject> ReceiveType()
        {
            List<ComboObject> ListReceiveType = new List<ComboObject>();
            ListReceiveType.Add(new ComboObject(SystemParamsInFile.HISTORY_RECEIVE_TYPE_PUPIL.ToString(), Res.Get("Common_Label_SMS_Receiver_Pupil")));
            ListReceiveType.Add(new ComboObject(SystemParamsInFile.HISTORY_RECEIVE_TYPE_PARENTS.ToString(), Res.Get("Common_Label_SMS_Receiver_Parent")));
            ListReceiveType.Add(new ComboObject(SystemParamsInFile.HISTORY_RECEIVE_TYPE_EMPLOYEE.ToString(), Res.Get("Common_Label_SMS_Receiver_Teacher")));
            return ListReceiveType;
        }

        public static List<ComboObject> SMSStatus()
        {
            List<ComboObject> ListSMSStatus = new List<ComboObject>();
            ListSMSStatus.Add(new ComboObject(SystemParamsInFile.STATUS_FAIL.ToString(), Res.Get("Common_Label_SMS_Sending_Failed")));
            ListSMSStatus.Add(new ComboObject(SystemParamsInFile.STATUS_SUCCESS.ToString(), Res.Get("Common_Label_SMS_Sending_Succeeded")));
            return ListSMSStatus;
        }

        public static List<ComboObject> SendNotificationType()
        {
            List<ComboObject> ListSendType = new List<ComboObject>();
            ListSendType.Add(new ComboObject(SystemParamsInFile.SEND_NOTIFICATION_TYPE_SYSTEM_ACCOUNT.ToString(), Res.Get("Common_Label_NotificationSystemAccount")));
            ListSendType.Add(new ComboObject(SystemParamsInFile.SEND_NOTIFICATION_TYPE_EMAIL_ACCOUNT.ToString(), Res.Get("Common_Label_NotificationEmailAccount")));
            return ListSendType;
        }

        public static List<ComboObject> AppliedLevel()
        {
            List<ComboObject> ListAppliedLevel = new List<ComboObject>();
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("Common_Label_Primary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("Common_Label_Secondary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("Common_Label_Tertiary")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_CRECHE.ToString(), Res.Get("Common_Label_Childcare")));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN.ToString(), Res.Get("Common_Label_Children")));
            return ListAppliedLevel;
        }

        /// <summary>
        /// hien thi cap truong
        /// </summary>
        /// <returns></returns>
        public static List<ComboObject> AppliedLevelName()
        {
            List<ComboObject> ListAppliedLevel = new List<ComboObject>();
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), "Tiểu học"));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), "THCS"));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), "THPT"));
            ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_CRECHE.ToString(), "Nhà trẻ/Mẫu giáo"));
            //ListAppliedLevel.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN.ToString(), "Mẫu giáo"));
            return ListAppliedLevel;
        }

        public static List<ComboObject> PreviousGraduationLevel()
        {
            List<ComboObject> ListPreviousGraduationLevel = new List<ComboObject>();
            ListPreviousGraduationLevel.Add(new ComboObject(SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL1.ToString(), Res.Get("PupilProfile_Label_PreviousGraduationLevel1")));
            ListPreviousGraduationLevel.Add(new ComboObject(SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL2.ToString(), Res.Get("PupilProfile_Label_PreviousGraduationLevel2")));
            ListPreviousGraduationLevel.Add(new ComboObject(SystemParamsInFile.PREVIOUSGRADUATIONLEVEL_iLEVEL3.ToString(), Res.Get("PupilProfile_Label_PreviousGraduationLevel3")));
            return ListPreviousGraduationLevel;
        }

        public static List<ComboObject> GraduationGrade()
        {
            List<ComboObject> listGraduationGrade = new List<ComboObject>();
            listGraduationGrade.Add(new ComboObject(SystemParamsInFile.GRADUATION_GRADE_EXCELLENT.ToString(), Res.Get("Graduation_Grade_Excellent")));
            listGraduationGrade.Add(new ComboObject(SystemParamsInFile.GRADUATION_GRADE_GOOD.ToString(), Res.Get("Graduation_Grade_Good")));
            //listGraduationGrade.Add(new ComboObject(SystemParamsInFile.GRADUATION_GRADE_ABOVE_FAIR.ToString(), Res.Get("Graduation_Grade_Above_Fair")));
            listGraduationGrade.Add(new ComboObject(SystemParamsInFile.GRADUATION_GRADE_FAIR.ToString(), Res.Get("Graduation_Grade_Fair")));
            return listGraduationGrade;
        }

        public static List<ComboObject> GraduationStatus()
        {
            List<ComboObject> listGraduationStatus = new List<ComboObject>();
            listGraduationStatus.Add(new ComboObject(SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT.ToString(), Res.Get("Graduation_Status_ApproveSufficient")));
            listGraduationStatus.Add(new ComboObject(SystemParamsInFile.GRADUATION_STATUS_APPROVE_INSUFFICIENT.ToString(), Res.Get("Graduation_Status_ApproveInsufficient")));
            listGraduationStatus.Add(new ComboObject(SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT.ToString(), Res.Get("Graduation_Status_ExaminationSufficient")));
            listGraduationStatus.Add(new ComboObject(SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_INSUFFICIENT.ToString(), Res.Get("Graduation_Status_ExaminationInsufficient")));
            listGraduationStatus.Add(new ComboObject(SystemParamsInFile.GRADUATION_STATUS_GRADUATED.ToString(), Res.Get("Graduation_Status_Graduated")));
            return listGraduationStatus;
        }

        public static List<ComboObject> RateAdd()
        {
            List<ComboObject> ListRateAdd = new List<ComboObject>();
            ListRateAdd.Add(new ComboObject(SystemParamsInFile.SUMMEDENDING_RATE_ADD_CDG.ToString(), Res.Get("Common_Label_Combo_RateAdd_CDG")));
            ListRateAdd.Add(new ComboObject(SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT.ToString(), Res.Get("Common_Label_Combo_RateAdd_HT")));
            ListRateAdd.Add(new ComboObject(SystemParamsInFile.SUMMEDENDING_RATE_ADD_CHT.ToString(), Res.Get("Common_Label_Combo_RateAdd_CHT")));
            return ListRateAdd;
        }

        /// <summary>
        /// ClassType
        /// </summary>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public static List<ComboObject> ClassType()
        {
            List<ComboObject> listClassType = new List<ComboObject>();
            listClassType.Add(new ComboObject(SystemParamsInFile.CLASSTYPE_NOITRU.ToString(), Res.Get("PupilProfile_Label_ClassTypeNoiTru")));
            listClassType.Add(new ComboObject(SystemParamsInFile.CLASSTYPE_BANTRU.ToString(), Res.Get("PupilProfile_Label_ClassTypeBanTru")));
            listClassType.Add(new ComboObject(SystemParamsInFile.CLASSTYPE_BANTRUDANNUOI.ToString(), Res.Get("PupilProfile_Label_ClassTypeBanTruDanNuoi")));
            listClassType.Add(new ComboObject(SystemParamsInFile.CLASSTYPE_NOITRUDANNUOI.ToString(), Res.Get("PupilProfile_Label_ClassTypeNoiTruDanNuoi")));
            return listClassType;
        }

        public static List<ComboObject> HomePlace()
        {
            List<ComboObject> listHomePlace = new List<ComboObject>();
            listHomePlace.Add(new ComboObject(SystemParamsInFile.HOME_PLACE_IN_SCHOOL.ToString(), Res.Get("PupilProfile_Label_HomePlace_InSchool")));
            listHomePlace.Add(new ComboObject(SystemParamsInFile.HOME_PLACE_LODGE.ToString(), Res.Get("PupilProfile_Label_HomePlace_Lodge")));
            return listHomePlace;
        }

        public static List<ComboObject> AppliedTypeSubject1()
        {
            GlobalInfo global = new GlobalInfo();
            List<ComboObject> ListAppliedTypeSubject = new List<ComboObject>();
            ListAppliedTypeSubject.Add(new ComboObject(Res.Get("AppliedTypeSubject_Require"), 0.ToString()));
            ListAppliedTypeSubject.Add(new ComboObject(Res.Get("AppliedTypeSubject_Choice"), 1.ToString()));
            if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ListAppliedTypeSubject.Add(new ComboObject(Res.Get("AppliedTypeSubject_ChoiceAddPiority"), 2.ToString()));
                ListAppliedTypeSubject.Add(new ComboObject(Res.Get("AppliedTypeSubject_ChoiceCalculatorMark"), 3.ToString()));
                ListAppliedTypeSubject.Add(new ComboObject(Res.Get("AppliedTypeSubject_ApparentShip"), 4.ToString()));
            }
            return ListAppliedTypeSubject;
        }

        public static List<ComboObject> IsCommentingSubject1()
        {
            List<ComboObject> ListCommentingSubject = new List<ComboObject>();
            ListCommentingSubject.Add(new ComboObject(Res.Get("Common_Label_IsCommentingJudge"), SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE.ToString()));
            ListCommentingSubject.Add(new ComboObject(Res.Get("Common_Label_IsCommentingMark"), SystemParamsInFile.ISCOMMENTING_TYPE_MARK.ToString()));
            //ListCommentingSubject.Add(new ComboObject(Res.Get("Common_Label_IsCommentingMark_Judge"),SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE.ToString()));
            return ListCommentingSubject;
        }

        public static List<ComboObject> EducationHierachyLevel()
        {
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Ministry"), SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_MINISTRY.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Province_Office"), SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_District_Office"), SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_School"), SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_SCHOOL.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Class"), SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_CLASS.ToString()));
            return ListPraiseLevel;
        }

        public static List<ComboObject> DisciplineLevel()
        {
            List<ComboObject> ListPraiseLevel = new List<ComboObject>();
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Ministry"), SystemParamsInFile.DISCIPLINE_LEVEL_MINISTRY.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Province_Office"), SystemParamsInFile.DISCIPLINE_LEVEL_PROVINCE_OFFICE.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_District_Office"), SystemParamsInFile.DISCIPLINE_LEVEL_DISTRICT_OFFICE.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_School"), SystemParamsInFile.DISCIPLINE_LEVEL_SCHOOL.ToString()));
            ListPraiseLevel.Add(new ComboObject(Res.Get("Education_Hierachy_Level_Class"), SystemParamsInFile.DISCIPLINE_LEVEL_CLASS.ToString()));
            return ListPraiseLevel;
        }

        public static List<ComboObject> PupilLearningType()
        {
            //tập trung, vừa làm vừa học, tự học có hướng dẫn
            List<ComboObject> ListPupilLearningType = new List<ComboObject>();
            ListPupilLearningType.Add(new ComboObject(SystemParamsInFile.PUPIL_LEARNING_TYPE_CENTRALIZING.ToString(), Res.Get("PupilLearningType_Centralizing")));
            ListPupilLearningType.Add(new ComboObject(SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING.ToString(), Res.Get("PupilLearningType_Working_And_Learning")));
            ListPupilLearningType.Add(new ComboObject(SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING.ToString(), Res.Get("PupilLearningType_Self_Learning")));
            return ListPupilLearningType;
        }

        public static List<MenuExamination> MenuExamination(int tab)
        {
            List<MenuExamination> listResult = new List<MenuExamination>() { 
             new MenuExamination { ValueMenu= 1, KeyMenu =Res.Get("Menu_Exam_Examinations"), LinkMenu = "/ExaminationsArea/Examinations/Index?Tab=" + tab + "&Pro=1" },
             new MenuExamination { ValueMenu= 2, KeyMenu =Res.Get("Menu_Exam_Group"), LinkMenu = "/ExamGroupArea/ExamGroup/Index?Tab=" + tab + "&Pro=2" },
             new MenuExamination { ValueMenu= 3, KeyMenu =Res.Get("Menu_Exam_Subject"), LinkMenu = "/ExamSubjectArea/ExamSubject/Index?Tab=" + tab + "&Pro=3" },
             new MenuExamination { ValueMenu= 4, KeyMenu =Res.Get("Menu_Exam_Pupil"), LinkMenu = "/ExamPupilArea/ExamPupil/Index?Tab=" + tab + "&Pro=4" },
             new MenuExamination { ValueMenu= 5, KeyMenu = Res.Get("Menu_Examimee_Pupil"), LinkMenu = "/ExamineeCodeNameArea/ExamineeCodeName/Index?Tab=" + tab + "&Pro=5" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_Room"), LinkMenu = "/ExamRoomArea/ExamRoom/Index?Tab=" + tab + "&Pro=6" },
             new MenuExamination { ValueMenu= 7, KeyMenu =Res.Get("Menu_Exam_Room_Order"), LinkMenu = "/ExamRoomAssignArea/ExamRoomAssign/Index?Tab=" + tab + "&Pro=7" },
             new MenuExamination { ValueMenu= 8, KeyMenu =Res.Get("Menu_Exam_Supervisory"), LinkMenu = "/ExamSupervisoryArea/ExamSupervisory/Index?Tab=" + tab + "&Pro=8" },
             new MenuExamination { ValueMenu= 9, KeyMenu =Res.Get("Menu_Exam_Supervisory_Assigned"), LinkMenu = "/ExamSupervisoryAssignArea/ExamSupervisoryAssign/Index?Tab=" + tab + "&Pro=9" },
             new MenuExamination { ValueMenu= 10, KeyMenu =Res.Get("Menu_Exam_Beat_Code"), LinkMenu = "/ExamBagArea/ExamBag/Index?Tab=" + tab + "&Pro=10" },
             new MenuExamination { ValueMenu= 11, KeyMenu =Res.Get("Menu_Exam_Beat_Detach"), LinkMenu = "/ExamDetachableBagArea/ExamDetachableBag/Index?Tab=" + tab + "&Pro=11" }
            };

            return listResult;
        }

        public static List<MenuExamination> MenuResultExamination(int tab)
        {
            List<MenuExamination> listResult = new List<MenuExamination>() { 
             new MenuExamination { ValueMenu= 1, KeyMenu =Res.Get("Menu_Exam_Pupil_Violate"), LinkMenu = "/ExamPupilViolateArea/ExamPupilViolate/Index?Tab=" + tab + "&Pro=1" },
             new MenuExamination { ValueMenu= 2, KeyMenu =Res.Get("Menu_Exam_Supervisory_Violate"), LinkMenu = "/ExamSupervisoryViolateArea/ExamSupervisoryViolate/Index?Tab=" + tab + "&Pro=2" },
             new MenuExamination { ValueMenu= 3, KeyMenu =Res.Get("Menu_Exam_Pupil_Absence"), LinkMenu = "/ExamPupilAbsenceArea/ExamPupilAbsence/Index?Tab=" + tab + "&Pro=3" },
             new MenuExamination { ValueMenu= 4, KeyMenu =Res.Get("Menu_Exam_InputMark_Assigned"), LinkMenu = "/ExamInputMarkAssignedArea/ExamInputMarkAssigned/Index?Tab=" + tab + "&Pro=4" },
             new MenuExamination { ValueMenu= 5, KeyMenu = Res.Get("Menu_Exam_InputMark"), LinkMenu = "/ExamInputMarkArea/ExamInputMark/Index?Tab=" + tab + "&Pro=5" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_InputMark_Book"), LinkMenu = "/ExamInputMarkTransferArea/ExamInputMarkTransfer/Index?Tab=" + tab + "&Pro=6" }
            };

            return listResult;
        }

        public static List<MenuExamination> MenuExaminationsReport(int tab)
        {
            List<MenuExamination> listResult = new List<MenuExamination>() { 
             new MenuExamination { ValueMenu= 1, KeyMenu =Res.Get("Menu_Exam_Report_Exam_Pupil_By_Room"), LinkMenu = "/ExamResultReportArea/ExamPupilByRoomReport/Index?Tab=" + tab + "&Pro=1" },
             new MenuExamination { ValueMenu= 2, KeyMenu =Res.Get("Menu_Exam_Report_Exam_Pupil_Attendance"), LinkMenu = "/ExamResultReportArea/ExamPupilAttendanceReport/Index?Tab=" + tab + "&Pro=2" },
             new MenuExamination { ValueMenu= 3, KeyMenu =Res.Get("Menu_Exam_Report_Exam_Supervisory_Assign"), LinkMenu = "/ExamResultReportArea/ExamSupervisoryAssignReport/Index?Tab=" + tab + "&Pro=3" },
             new MenuExamination { ValueMenu= 4, KeyMenu =Res.Get("Menu_Exam_Report_Exam_Pupil_Absence"), LinkMenu = "/ExamResultReportArea/ExamPupilAbsenceReport/Index?Tab=" + tab + "&Pro=4" },
             new MenuExamination { ValueMenu= 5, KeyMenu =Res.Get("Menu_Exam_Report_Mark_By_Class"), LinkMenu = "/ExamResultReportArea/ExamMarkByClassReport/Index?Tab=" + tab + "&Pro=5" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_Report_Mark_By_Room"), LinkMenu = "/ExamResultReportArea/ExamMarkByRoomReport/Index?Tab=" + tab + "&Pro=6" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_Report_Mark_Statistic_By_Class"), LinkMenu = "/ExamResultReportArea/ExamMarkByClassStatisticReport/Index?Tab=" + tab + "&Pro=7" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_Report_Mark_Statistic_By_Teacher"), LinkMenu = "/ExamResultReportArea/ExamMarkByTeacherStatisticReport/Index?Tab=" + tab + "&Pro=8" },
             new MenuExamination { ValueMenu= 6, KeyMenu =Res.Get("Menu_Exam_Report_Mark_Statistic_By_Room"), LinkMenu = "/ExamResultReportArea/ExamMarkByRoomStatisticReport/Index?Tab=" + tab + "&Pro=9" },
            };

            return listResult;
        }

        /// <summary>
        /// Danh sách các vùng đặc biệt khó khăn
        /// </summary>
        /// <author date="18/12/2015">AnhVD9</author>
        /// <returns></returns>
        public static List<ComboObject> DifficultZone()
        {
            List<ComboObject> ListZone = new List<ComboObject>();
            ListZone.Add(new ComboObject(SystemParamsInFile.IN_PARTICULARLY_DIFFICULT_COMMUNE.ToString(), Res.Get("Thuộc xã đặc biệt khó khăn")));
            ListZone.Add(new ComboObject(SystemParamsInFile.IN_NORMAL_COMMUNE.ToString(), Res.Get("Thuộc bản đặc biệt khó khăn, xã bình thường")));
            ListZone.Add(new ComboObject(SystemParamsInFile.IN_OTHER_COMMUNE.ToString(), Res.Get("Khác")));
            return ListZone;
        }

        /// <summary>
        /// Danh sách các CHẾ ĐỘ HỖ TRỢ
        /// </summary>
        /// <author date="18/12/2015">AnhVD9</author>
        /// <returns></returns>
        public static List<ComboObject> SupportingPolicy()
        {
            List<ComboObject> LstSupportingPolicy = new List<ComboObject>();
            LstSupportingPolicy.Add(new ComboObject(SystemParamsInFile.POLICY_122003_GOVERNMENT_DECISION.ToString(), Res.Get("Chế độ theo QĐ số 12/2013/QĐ-TTg")));
            LstSupportingPolicy.Add(new ComboObject(SystemParamsInFile.POLICY_2123_GOVERNMENT_DECISION.ToString(), Res.Get("Chế độ theo QĐ số 2123/QĐ-TTg")));
            LstSupportingPolicy.Add(new ComboObject(SystemParamsInFile.POLICY_1672_GOVERNMENT_DECISION.ToString(), Res.Get("Chế độ theo QĐ số 1672/QĐ-TTg")));
            return LstSupportingPolicy;
        }
        /// <summary>
        /// Tìm kiếm: Có - Không
        /// </summary>
        /// <author date="10/12/2015">AnhVD9</author>
        /// <returns></returns>
        public static List<ComboObject> StatusYesNo()
        {
            List<ComboObject> lstStatus = new List<ComboObject>();
            lstStatus.Add(new ComboObject("1", "Có"));
            lstStatus.Add(new ComboObject("0", "Không"));
            return lstStatus;
        }

        /// <summary>
        /// Get dinh dang bao cao
        /// </summary>
        /// <returns></returns>
        public static List<ComboObject> GetExtentionReport()
        {
            List<ComboObject> lstStatus = new List<ComboObject>();
            lstStatus.Add(new ComboObject("1", "Excel"));
            lstStatus.Add(new ComboObject("2", "PDF"));
            return lstStatus;
        }

        public static List<ComboObject> GetLibReport()
        {
            List<ComboObject> lstStatus = new List<ComboObject>();
            lstStatus.Add(new ComboObject("1", "Stream"));
            lstStatus.Add(new ComboObject("2", "ITextSharp"));
            lstStatus.Add(new ComboObject("3", "Spire.xls"));
            return lstStatus;
        }

        public static List<ComboObject> SectionName()
        {
            List<ComboObject> listSectionName = new List<ComboObject>();
            listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_MORNING.ToString(), Res.Get("Home_Label_Morning")));
            listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_AFTERNOON.ToString(), Res.Get("Home_Label_Afternoon")));
            listSectionName.Add(new ComboObject(SystemParamsInFile.SECTION_EVENING.ToString(), Res.Get("Home_Label_Evening")));
            return listSectionName;
        }

    }
}