using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SupervisingDeptArea.Models;
using SMAS.VTUtils.Utils;
using System.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Web.Areas.NutritionalNormArea.Models;
using SMAS.Web.Areas.TeachingAssignmentArea.Models;

namespace SMAS.Web.Utils
{
    public static class CommonFunctions
    {
        private static readonly string[] VietnameseSigns = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };

        /// <summary>
        /// Lấy về danh sách các đơn vị thuộc quyền quản lý của người dùng đang đăng nhập
        /// </summary>
        /// <param name="SupervisingDeptBusiness"></param>
        /// <returns></returns>
        public static List<SupervisingDept> ListSupervisingDeptManagedByUser(ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            List<SupervisingDept> ListSupervisingDept = new List<SupervisingDept>();

            if (GlobalInfo.IsSuperRole) // Nếu người dùng là SuperAdmin
            {
                ListSupervisingDept = SupervisingDeptBusiness.Search(SearchInfo).ToList();
            }
            if (GlobalInfo.IsSuperVisingDeptRole) // Nếu người dùng là nhân viên cấp sở
            {
                int? SupervisingDeptID = GlobalInfo.SupervisingDeptID;
                if (SupervisingDeptID != null)
                {
                    SupervisingDept CurrentSupervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                    SearchInfo["TraversalPath"] = CurrentSupervisingDept.TraversalPath + CurrentSupervisingDept.SupervisingDeptID.ToString();

                    ListSupervisingDept = SupervisingDeptBusiness.Search(SearchInfo).ToList();
                    ListSupervisingDept.Add(CurrentSupervisingDept);
                }
                else
                {
                    ListSupervisingDept = new List<SupervisingDept>();
                }
            }
            if (GlobalInfo.IsSubSuperVisingDeptRole) // Nếu người dùng là nhân viên cấp phòng
            {
                int? SupervisingDeptID = GlobalInfo.SupervisingDeptID;
                if (SupervisingDeptID != null)
                {
                    SupervisingDept CurrentSupervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                    SearchInfo["TraversalPath"] = CurrentSupervisingDept.TraversalPath;

                    ListSupervisingDept = SupervisingDeptBusiness.Search(SearchInfo).ToList();
                }
                else
                {
                    ListSupervisingDept = new List<SupervisingDept>();
                }
            }
            return ListSupervisingDept.OrderBy(o => o.SupervisingDeptName.ToLower()).ToList();
        }

        /// <summary>
        /// Lấy về SupervisingDeptTreeViewModel để hiển thị trên cây đơn vị
        /// </summary>
        /// <param name="SupervisingDeptBusiness"></param>
        /// <returns></returns>
        public static SupervisingDeptTreeViewModel TreeSupervisingDeptManagedByUser(ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            List<SupervisingDept> ListSupervisingDept = ListSupervisingDeptManagedByUser(SupervisingDeptBusiness);
            GlobalInfo GlobalInfo = new GlobalInfo();

            SupervisingDeptTreeViewModel SupervisingDeptTreeViewModel = new SupervisingDeptTreeViewModel();

            foreach (SupervisingDept Dept in ListSupervisingDept)
            {
                if (GlobalInfo.IsSystemAdmin)
                {
                    if (Dept.ParentID == null)
                    {
                        SupervisingDeptTreeViewModel.SupervisingDeptID = Dept.SupervisingDeptID;
                        SupervisingDeptTreeViewModel.SupervisingDeptName = Dept.SupervisingDeptName;
                        SupervisingDeptTreeViewModel.HierachyLevel = Dept.HierachyLevel;
                        SupervisingDeptTreeViewModel.TraversalPath = Dept.TraversalPath;
                        SupervisingDeptTreeViewModel.ListChildren = new List<SupervisingDeptTreeViewModel>();
                        break;
                    }
                }
                if (GlobalInfo.IsSuperVisingDeptRole || GlobalInfo.IsSubSuperVisingDeptRole)
                {

                    if (Dept.SupervisingDeptID == GlobalInfo.SupervisingDeptID)
                    {
                        SupervisingDeptTreeViewModel.SupervisingDeptID = Dept.SupervisingDeptID;
                        SupervisingDeptTreeViewModel.SupervisingDeptName = Dept.SupervisingDeptName;
                        SupervisingDeptTreeViewModel.HierachyLevel = Dept.HierachyLevel;
                        SupervisingDeptTreeViewModel.TraversalPath = Dept.TraversalPath;
                        SupervisingDeptTreeViewModel.ListChildren = new List<SupervisingDeptTreeViewModel>();
                        break;
                    }

                }
            }



            SetListSupervisingDeptChildren(SupervisingDeptTreeViewModel, ListSupervisingDept);



            return SupervisingDeptTreeViewModel.SupervisingDeptID > 0 ? SupervisingDeptTreeViewModel : null;
        }

        /// <summary>
        /// Hàm thực hiện đệ quy để chuyển một List Đơn vị thành Cây đơn vị
        /// </summary>
        /// <param name="Node"></param>
        /// <param name="ListSupervisingDept"></param>
        public static void SetListSupervisingDeptChildren(SupervisingDeptTreeViewModel Node, List<SupervisingDept> ListSupervisingDept)
        {
            if (Node.SupervisingDeptID == 0)
            {
                return;
            }
            int i = 0;
            //List<SupervisingDept> Listso = ListSupervisingDept.Where(o => o.HierachyLevel == 3).ToList();
            //List<SupervisingDept> Listdv = ListSupervisingDept.Where(o => o.HierachyLevel == 5).ToList();
            //List<SupervisingDept> ListPs = ListSupervisingDept.Where(o => o.HierachyLevel == 4).ToList();
            List<SupervisingDept> Listpdv = ListSupervisingDept.Where(o => o.HierachyLevel == 6).OrderBy(p=>p.SupervisingDeptName.ToLower()).ToList();


            //SetListSupervisingDeptChildren(ChildNode, ListSupervisingDept);

            //Bộ giáo dục đào tạo
            #region Bộ giáo dục đào tạo
            if (Node.HierachyLevel == 1)
            {
                foreach (SupervisingDept Dept in ListSupervisingDept)
                {
                    if (Dept.ParentID == Node.SupervisingDeptID)
                    {
                        SupervisingDeptTreeViewModel ChildNode = new SupervisingDeptTreeViewModel(
                                                                    Dept.SupervisingDeptID,
                                                                    Dept.SupervisingDeptName,
                                                                    Dept.HierachyLevel,
                                                                    Dept.TraversalPath,
                                                                    new List<SupervisingDeptTreeViewModel>(), Dept.ParentID
                                                                 );
                        SupervisingDeptTreeViewModel Nodedv = new SupervisingDeptTreeViewModel(
                                                                    Dept.SupervisingDeptID,
                                                                    Res.Get("SchoolProfile_Label_SupervisingDept"),
                                                                    5,
                                                                    Dept.TraversalPath,
                                                                    new List<SupervisingDeptTreeViewModel>()
                                                                 );
                        SupervisingDeptTreeViewModel Nodeps = new SupervisingDeptTreeViewModel(
                                                               Dept.SupervisingDeptID,
                                                               Res.Get("SupervisingDept_UnitType_SubSuperDept"),
                                                               4,
                                                               Dept.TraversalPath,
                                                               new List<SupervisingDeptTreeViewModel>()
                                                            );
                        SupervisingDeptTreeViewModel Nodepp = new SupervisingDeptTreeViewModel(
                                                                        Dept.SupervisingDeptID,
                                                                        Res.Get("SupervisingDept_UnitType_SubSuperDept"),
                                                                        6,
                                                                        Dept.TraversalPath,
                                                                        new List<SupervisingDeptTreeViewModel>()
                                                                     );

                        if (ChildNode.HierachyLevel == 3)
                        {
                            Node.ListChildren.Add(ChildNode);
                            Node.ListChildren[i].ListChildren.Add(Nodedv);
                            Node.ListChildren[i].ListChildren.Add(Nodeps);


                            TreeChildView(Node.ListChildren[i].ListChildren[0], ListSupervisingDept.Where(o => o.HierachyLevel == 5 && o.TraversalPath.Contains(ChildNode.TraversalPath + ChildNode.SupervisingDeptID.ToString() + "\\")).ToList());

                            TreeChildView(Node.ListChildren[i].ListChildren[1], ListSupervisingDept.Where(o => o.HierachyLevel == 4 && o.TraversalPath.Contains(ChildNode.TraversalPath + ChildNode.SupervisingDeptID.ToString() + "\\")).ToList());

                                i++;
                           
                        }
          
                    }

                }
            #endregion
                //Sở - đơn vị quản lý + phòng bàn trực thuộc
            }


            #region sophong
            if (Node.HierachyLevel == 3)
            {

                SupervisingDeptTreeViewModel Nodedv = new SupervisingDeptTreeViewModel(
                                                                    Node.SupervisingDeptID,
                                                                    Res.Get("SchoolProfile_Label_SupervisingDept"),
                                                                    5,
                                                                    Node.TraversalPath,
                                                                    new List<SupervisingDeptTreeViewModel>()
                                                                 );
                SupervisingDeptTreeViewModel Nodeps = new SupervisingDeptTreeViewModel(
                                                       Node.SupervisingDeptID,
                                                       Res.Get("SupervisingDept_UnitType_SubSuperDept"),
                                                       4,
                                                       Node.TraversalPath,
                                                       new List<SupervisingDeptTreeViewModel>()
                                                    );
                SupervisingDeptTreeViewModel Nodepp = new SupervisingDeptTreeViewModel(
                                                                Node.SupervisingDeptID,
                                                                Res.Get("SupervisingDept_UnitType_SubSuperDept"),
                                                                6,
                                                                Node.TraversalPath,
                                                                new List<SupervisingDeptTreeViewModel>()
                                                             );

                Node.ListChildren.Add(Nodedv);
                Node.ListChildren.Add(Nodeps);
                foreach (SupervisingDept Dept in ListSupervisingDept)
                {
                    if (Dept.ParentID == Node.SupervisingDeptID)
                    {
                        SupervisingDeptTreeViewModel ChildNode = new SupervisingDeptTreeViewModel(
                                                                    Dept.SupervisingDeptID,
                                                                    Dept.SupervisingDeptName,
                                                                    Dept.HierachyLevel,
                                                                    Dept.TraversalPath,
                                                                    new List<SupervisingDeptTreeViewModel>(), Dept.ParentID
                                                                 );

                        if (ChildNode.HierachyLevel == 5 && ChildNode.ParentID == Node.SupervisingDeptID)
                            Node.ListChildren[0].ListChildren.Add(TreeChildView(ChildNode, ListSupervisingDept.Where(o => o.TraversalPath.Contains(ChildNode.TraversalPath + ChildNode.SupervisingDeptID.ToString() + "\\")).ToList()));
                        else if (ChildNode.HierachyLevel == 4 && ChildNode.ParentID == Node.SupervisingDeptID)
                            Node.ListChildren[1].ListChildren.Add(TreeChildView(ChildNode, ListSupervisingDept.Where(o => o.TraversalPath.Contains(ChildNode.TraversalPath + ChildNode.SupervisingDeptID.ToString() + "\\")).ToList()));

                    }
                }
            }
            #endregion

            if (Node.HierachyLevel == 5)
            {


                Node = TreeChildView(Node, Listpdv);
            }
        }


        public static SupervisingDeptTreeViewModel TreeChildView(SupervisingDeptTreeViewModel node, List<SupervisingDept> Listpdv)
        {
            List<SupervisingDeptTreeViewModel> listChild = new List<SupervisingDeptTreeViewModel>();
            foreach (var pdv in Listpdv)
            {
                SupervisingDeptTreeViewModel ChildNode = new SupervisingDeptTreeViewModel(
                                                                pdv.SupervisingDeptID,
                                                                pdv.SupervisingDeptName,
                                                                pdv.HierachyLevel,
                                                                pdv.TraversalPath,
                                                                new List<SupervisingDeptTreeViewModel>(), pdv.ParentID
                                                             );
                listChild.Add(ChildNode);
            }
            if (listChild != null && listChild.Count > 0)
            {
                foreach (var child in listChild)
                {
                    if (child.ParentID == node.SupervisingDeptID)
                    {
                        node.ListChildren.Add(TreeChildView(child, Listpdv.Where(o => o.TraversalPath.Contains(child.TraversalPath + child.SupervisingDeptID.ToString() + "\\")).ToList()));
                    }
                }
            }
            else
            {
                return node;
            }
            return node;
        }


        /// <summary>
        /// Lấy về danh sách Tỉnh thành tương ứng với quyền quản lý của account đang đăng nhập
        /// </summary>
        /// <param name="ProvinceBusiness"></param>
        /// <returns></returns>
        public static List<Province> UserListProvince(IProvinceBusiness ProvinceBusiness)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<Province> ListProvince = new List<Province>();

            if (GlobalInfo.IsSystemAdmin)
            {
                ListProvince = ProvinceBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.ProvinceName).ToList();
            }
            if (GlobalInfo.IsSuperVisingDeptRole || GlobalInfo.IsSubSuperVisingDeptRole)
            {
                if (GlobalInfo.ProvinceID != null && GlobalInfo.ProvinceID > 0)
                {
                    ListProvince.Add(ProvinceBusiness.Find(GlobalInfo.ProvinceID));
                }
            }

            return ListProvince;
        }

        public static List<District> UserListDistrict(int? ProvinceID, IDistrictBusiness DistrictBusiness)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<District> ListDistrict = new List<District>();

            if (GlobalInfo.IsSuperRole)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["ProvinceID"] = ProvinceID;
                ListDistrict = DistrictBusiness.Search(SearchInfo).OrderBy(o => o.DistrictName).ToList();
            }
            else
            {
                if (GlobalInfo.DistrictID != null && GlobalInfo.DistrictID > 0)
                {
                    ListDistrict.Add(DistrictBusiness.Find(GlobalInfo.DistrictID));
                }
            }

            return ListDistrict;
        }

        /// <summary>
        /// Từ giá trị Loại đơn vị trả về HierachyLevel tương ứng tùy vào vai trò của người dùng đang đăng nhập
        /// </summary>
        /// <param name="UnitType"></param>
        /// <param name="SupervisingDeptBusiness"></param>
        /// <returns></returns>
        public static int GetSupervisingDeptHierachyLevelFromUnitType(int UnitType, SupervisingDept Parent, ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            int ProvinceOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE");
            int DistrictOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE");
            int Ministry = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_MINISTRY");
            int ProvinceOfficeDept = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMEN");
            int DistrictOfficeDept = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMEN");
            int MinistryDept = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_MINISTRY_DEPARTMENT");

            int HierachyLevel = 0;
            GlobalInfo GlobalInfo = new GlobalInfo();

            if (Parent.HierachyLevel == Ministry)
            {
                if (UnitType == 1)
                {
                    HierachyLevel = (int)ProvinceOffice;
                }
                else
                {
                    HierachyLevel = (int)MinistryDept;
                }
            }

            if (Parent.HierachyLevel == ProvinceOffice)
            {
                if (UnitType == 1)
                {
                    HierachyLevel = (int)DistrictOffice;
                }
                else
                {
                    HierachyLevel = (int)ProvinceOfficeDept;
                }
            }

            if (Parent.HierachyLevel == DistrictOffice)
            {
                if (UnitType == 2)
                {
                    HierachyLevel = (int)DistrictOfficeDept;
                }
            }

            return HierachyLevel;
        }

        /// <summary>
        /// Trả về Loại đơn vị của một đơn vị dựa vào HierachyLevel
        /// </summary>
        /// <param name="SupervisingDept"></param>
        /// <returns></returns>
        public static int GetUnitTypeFromSupervisingDept(SupervisingDept SupervisingDept)
        {


            int UnitType = 0;

            // Đơn vị cấp Phòng/Sở
            if (SupervisingDept.HierachyLevel == 3)
            {
                UnitType = 1;
            }
            // Đơn vị phòng ban trực thuộc
            if (SupervisingDept.HierachyLevel == 4 || SupervisingDept.HierachyLevel == 5)
            {
                UnitType = 2;
            }

            return UnitType;
        }

        public static SelectList GetListSemester()
        {
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(GlobalConstants.FIRST_SEMESTER.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(GlobalConstants.SECOND_SEMESTER.ToString(), Res.Get("Common_Label_SecondSemester")));
            return new SelectList(lsSemester, "key", "value");
        }

        /// <summary>
        /// Lấy về TreeEatingGroupModel để hiển thị trên cây đơn vị
        /// </summary>
        /// <param name="TreeEatingGroupModel"></param>
        /// <returns></returns>
        public static List<TreeEatingGroupModel> TreeTreeEatingGroupModelByUser(INutritionalNormBusiness NutritionalNormBusiness, IEatingGroupBusiness EatingGroupBusiness)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID.Value;
            var ListNutritionalNorm = NutritionalNormBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, SearchInfo);

            if (ListNutritionalNorm != null && ListNutritionalNorm.Count() > 0)
            {
                List<TreeEatingGroupModel> ListTreeEatingGroupModel = new List<TreeEatingGroupModel>();
                var lst = ListNutritionalNorm.ToList().GroupBy(p => p.EffectDate);
                List<DateTime> lstEffectDate = lst.Select(o => o.Key).OrderByDescending(o => o).ToList();
                if (lstEffectDate != null && lstEffectDate.Count > 0)
                {
                    foreach (var date in lstEffectDate)
                    {
                        TreeEatingGroupModel TreeEatingGroupModel = new TreeEatingGroupModel();
                        TreeEatingGroupModel.EffectDate = date;
                        var ListChildren = ListNutritionalNorm.Where(o => o.EffectDate == date).Distinct().OrderBy(o => o.EatingGroupID).ToList();
                        List<ChildrenNutritionalNormViewModel> lstChildrenNutritionalNormViewModel = new List<ChildrenNutritionalNormViewModel>();
                        if (ListChildren != null && ListChildren.Count > 0)
                        {
                            foreach (var c in ListChildren)
                            {
                                ChildrenNutritionalNormViewModel ChildrenNutritionalNormViewModel = new ChildrenNutritionalNormViewModel();
                                ChildrenNutritionalNormViewModel.EatingGroupID = c.EatingGroupID;
                                ChildrenNutritionalNormViewModel.EatingGroupName = EatingGroupBusiness.All.Where(o => o.EatingGroupID == c.EatingGroupID).FirstOrDefault().EatingGroupName;
                                lstChildrenNutritionalNormViewModel.Add(ChildrenNutritionalNormViewModel);
                            }
                        }
                        TreeEatingGroupModel.ListChildren = lstChildrenNutritionalNormViewModel;
                        ListTreeEatingGroupModel.Add(TreeEatingGroupModel);
                    }
                }
                return ListTreeEatingGroupModel;
            }
            else return null;
        }
        
        public static List<TeachingAssignmentTreeViewModel> TreeTreeFacultyModelByUser(ITeachingAssignmentBusiness TeachingAssignmentBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;

            //var ListTeachingAssignment = TeachingAssignmentBusiness.GetTeachingAssigment(GlobalInfo.SchoolID.Value, SearchInfo);
            List<SchoolFaculty> ListSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, SearchInfo).ToList();
            if (ListSchoolFaculty != null && ListSchoolFaculty.Count() > 0)
            {
                List<TeachingAssignmentTreeViewModel> ListTreeTeachingAssignmentModel = new List<TeachingAssignmentTreeViewModel>();
                var lst = ListSchoolFaculty.ToList().GroupBy(p => p.SchoolFacultyID);

                List<int> lstTA = lst.Select(o => o.Key).OrderByDescending(o => o).ToList();
                if (lstTA != null && lstTA.Count() > 0)
                {
                    foreach (var teachingAssignment in lstTA)
                    {
                        TeachingAssignmentTreeViewModel teachingAssignmentTreeViewModel = new TeachingAssignmentTreeViewModel();
                        if (teachingAssignment != null)
                        {
                            teachingAssignmentTreeViewModel.FacultyID = teachingAssignment;
                            teachingAssignmentTreeViewModel.FacultyName = SchoolFacultyBusiness.Find(teachingAssignment).FacultyName;
                        }
                        //var ListChildren = ListTeachingAssignment.Where(o => o.FacultyID == teachingAssignment).Distinct().OrderBy(o => o.TeacherID).ToList();
                        var ListChildren = EmployeeBusiness.SearchWorkingTeacherByFaculty(GlobalInfo.SchoolID.Value, teachingAssignment).ToList();
                        List<ChildrenTeachingAssignmentTreeViewModel> lstChildrenTAViewModel = new List<ChildrenTeachingAssignmentTreeViewModel>();
                        if (ListChildren != null && ListChildren.Count > 0)
                        {
                            foreach (var c in ListChildren)
                            {
                                ChildrenTeachingAssignmentTreeViewModel ChildrenTAViewModel = new ChildrenTeachingAssignmentTreeViewModel();
                                if (c != null && c.EmployeeID != null)
                                {
                                    ChildrenTAViewModel.TeacherID = c.EmployeeID;
                                    ChildrenTAViewModel.TeacherName = c.FullName;
                                    lstChildrenTAViewModel.Add(ChildrenTAViewModel);
                                }

                            }
                        }
                        teachingAssignmentTreeViewModel.ListChildren = lstChildrenTAViewModel;
                        if (teachingAssignment != null)
                        {
                            ListTreeTeachingAssignmentModel.Add(teachingAssignmentTreeViewModel);
                        }
                    }
                }
                return ListTreeTeachingAssignmentModel;
            }
            else return null;
        }

        public static SelectList GetListSemesterValueDafault()
        {
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(GlobalConstants.FIRST_SEMESTER.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(GlobalConstants.SECOND_SEMESTER.ToString(), Res.Get("Common_Label_SecondSemester")));
            return new SelectList(lsSemester, "key", "value", lsSemester.FirstOrDefault().key);
        }

        public static SelectList GetListSemesterSelectedDefault()
        {
            GlobalInfo glo = new GlobalInfo();
            string semester = "1";
            semester = glo.Semester != null ? glo.Semester.Value.ToString() : "1";
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(GlobalConstants.FIRST_SEMESTER.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(GlobalConstants.SECOND_SEMESTER.ToString(), Res.Get("Common_Label_SecondSemester")));
            return new SelectList(lsSemester, "key", "value", semester);
        }

        public static string RemoveSign(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }
    }
}
