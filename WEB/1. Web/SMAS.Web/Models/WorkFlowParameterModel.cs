﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Models
{
    public class WorkFlowParameterModel
    {
        public WorkLowViewModel WorkFlowParent { get; set; }
        public int DefaultTab { get; set; }
        public List<WorkLowViewModel> lstWorkFlowChil { get; set; }
    }
    public class WorkLowViewModel
    {
        public int WorkFlowID { get; set; }
        public string WorkFlowName { get; set; }
    }
}