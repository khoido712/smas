﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;

namespace SMAS.Web.Models
{
    public class PupilProfileModel
    {
        public string EducationLevelResolution { get; set; }
        public int EducationLevelTotalPuppil { get; set; }
        public int ClassID { get; set; }
        public Dictionary<string, int> lstCountPupil { get; set; }
        public Dictionary<string, int> ClassProfiles { get; set; }
    }
    public class HomePageGroup2
    {
        public List<PupilProfileModel> ListPupilProfileModel { get; set; }
        public int TotalProfiles { get; set; }
        public int TotalEmployeeAdmin { get; set; }
        public int TotalTeacher { get; set; }
        public int TotalEmployee { get; set; }

        //Tình hình nhập điểm
        public Dictionary<EducationLevel, List<ClassProfileRefBO>> ListClassNotFullSummedUpRecord { get; set; }
        //Tình hình Tổng kết điểm
        public Dictionary<EducationLevel, List<ClassProfile>> ListClassNotFullRanking { get; set; }
        //Tình hình Xếp loại hạnh kiểm
        public Dictionary<EducationLevel, List<ClassProfile>> ListClassNotFullRankingConduct { get; set; }
        //Tình hình xếp loại học sinh
        public Dictionary<EducationLevel, List<ClassProfile>> ListClassNotFullRankingCapacityConduct { get; set; }
    }
}