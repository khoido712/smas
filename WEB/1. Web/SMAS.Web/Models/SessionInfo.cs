using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Models
{
    public class SessionInfo
    {
        public int? AcademicYearID { get; set; }

        public int? AppliedLevel { get; set; }

        public System.Collections.Generic.List<int> AppliedLevels { get; set; }

        public System.Collections.Generic.List<SMAS.Models.Models.EducationLevel> EducationLevels { get; set; }

        public int? ClassID { get; set; }

        public int? DistrictID { get; set; }

        public int? EducationLevel { get; set; }

        public int? GroupID { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsSystemAdmin { get; set; }

        public bool IsSchoolRole { get; set; }

        public bool IsSuperRole { get; set; }

        public bool isViewAll { get; set; }

        public bool IsEmployeeManager { get; set; }

        public string strIsViewAll { get; set; }

        public bool IsSuperVisingDeptRole { get; set; }

        public int RoleID { get; set; }

        public int? SchoolID { get; set; }

        public string SchoolName { get; set; }

        public int? Semester { get; set; }

        public int? SupervisingDeptID { get; set; }

        public string SuperVisingDeptName { get; set; }

        public int UserAccountID { get; set; }

        public string strIsAdmin { get; set; }

        public string strIsSystemAdmin { get; set; }

        public string strIsSchoolRole { get; set; }

        public string strIsSuperRole { get; set; }

        public string strIsSuperVisingDeptRole { get; set; }

        public int? ProvinceID { get; set; }

        public int AppliedLevelsCount { get; set; }

        public int EducationLevelsCount { get; set; }

        public int? EmployeeID { get; set; }

        public bool IsCurrentYear { get; set; }

        public string strIsCurrentYear { get; set; }

        public int? Selected_academicYear_Index { get; set; }
        
    }
}
