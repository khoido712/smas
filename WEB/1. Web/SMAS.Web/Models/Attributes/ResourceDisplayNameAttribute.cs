﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using SMAS.Web.Utils;

namespace SMAS.Web.Models.Attributes
{
    public class ResourceDisplayNameAttribute: DisplayNameAttribute
    {
        public ResourceDisplayNameAttribute(string ResourceKey)
            : base(Res.Get(ResourceKey))
        { }
    }
}