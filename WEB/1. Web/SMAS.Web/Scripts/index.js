﻿function _exportImg(__classID, __pupilID) {
    dataChart = {};
    $.ajax({
        url: window.urlLoadSvgData,
        type: 'post',
        data: { classId: __classID, pupilID: __pupilID },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (data) {
            $("#div_tmp_for_data").empty().html(data);
            var dataSVG = [];
            var dataPP = [];
            var lstWMap = [];
            function createSVG(idchart, isW, ppId) {
                //Get map data                            
                var chart = $("#" + idchart).data("tChart");
                if (chart == undefined) {
                    _exportImg(__classID, __pupilID);
                }
                //if (compress) {
                //    console.log("1---" + chart.svg());//escape(
                //    console.log("2---" + $.jSEND(chart.svg()));
                //    console.log("3---" + Base64.encode($.jSEND(chart.svg())));
                //    dataSVG.push(Base64.encode($.jSEND(chart.svg())));
                //} else {
                //    dataSVG.push(chart.svg());
                //}
                dataSVG.push(Base64.encode(chart.svg()));
                //console.log("1---" + chart.svg());//escape(
                //console.log("2---" + Base64.encode(chart.svg()));
                //Clear map
                dataPP.push(ppId);
                lstWMap.push(isW);
                $("#" + idchart).parent().css("display", "none");
                $("#" + idchart).html("");
            }
            $("[name=chart_pupil]").each(function () {
                var female = false;
                var isW = $(this).attr('mapwidth');
                var ppID = $(this).attr('pupilid');
                var chartId = $(this).attr('id');
                if ($(this).attr('female') == 1)
                    female = true;
                //Create chart
                createSVG(chartId, isW, ppID);
            });
            //Tao file images
            if (dataSVG.length > 0) {
                $.ajax({
                    url: window.urlCreateSVG,
                    type: 'post',
                    //traditional: true,
                    datatype: 'json',
                    //contentType: "application/json; charset=utf-8",
                    //data: JSON.stringify({ lstSvg: dataSVG, classId: __classID, lstPP: dataPP, lstMapW: lstWMap }),
                    data: { lstSvg: JSON.stringify(dataSVG), classId: __classID, lstPP: JSON.stringify(dataPP), lstMapW: JSON.stringify(lstWMap), compress: compress },
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (data) {
                        smas.hideProcessingDialog();
                        window.open("/PhysicalExaminationArea/GrowthChart/DowloadZipped" + "?classId=" + __classID + "&pupilId=" + __pupilID);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            }
            else {
                smas.alert({ Type: "error", Message: 'Không có dữ liệu.' });
                smas.hideProcessingDialog();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.hideProcessingDialog();
            smas.alert(XMLHttpRequest.responseText);
        }
    })
}