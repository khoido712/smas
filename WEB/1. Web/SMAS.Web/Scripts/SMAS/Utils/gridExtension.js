﻿(function ($) {
    var defaultFrozenCol = 1; //number of column to freeze
    var scrollDelay = 100; //delay in milliseconds
    var scrollStep = 5; //how big scroll step should be before taking action
    var freezeColStyle = "solid 2px #C1D7DB";

    var hiddenColCount = 0;
    var totalColWidth = 0;
    var currentScrollPos = 0;

    var gridHeaderTable;
    var gridContentTable;
    var tableHeaderGroupCol;
    var tableHeaderCol;
    var tableContentGroupCol;
    var totalColCount;

    var frozenColWidth = 0;
    var columnWidthArr = new Array();
    var colWidth = 0;
    var grid;

    var frozenContainer;
    var frozenInnerContainer;
    var frozenColCount;
    var browserVersion = parseInt($.browser.version);

    //Create freezeGridColumn function
    $.fn.freezeGridColumn = function (frozenColumnsCount) {
        initBrowsers();
        frozenColCount = (frozenColumnsCount != null) ? frozenColumnsCount : defaultFrozenCol;
        grid = $(this);

        var hasLoaded = false;

        if (!grid.data('tGrid').isAjax()) {
            initFreezeColumns();
        }
        else {
            grid.ajaxStop(function () {
                if (!hasLoaded) {
                    initFreezeColumns();
                    hasLoaded = true;
                }
                else {
                    refreshFreezeColumns();
                }
            });
        }
    };

    //Initialise all necessary grid info to begin frozen column implementation
    function initFreezeColumns() {
        getGridInfo();
        createScrollContainer();
        getHiddenColCount();
        createColumnWidthList();
        getFrozenColumnsWidth();
        showFrozenVerticalLine();

        var hasScrolled = false;

        $(frozenContainer).scroll(function (e) {
            e.preventDefault();

            var scrollDiff = Math.abs(currentScrollPos - $(frozenContainer).scrollLeft());

            if (scrollDiff >= scrollStep) {
                hasScrolled = false;

                $(frozenContainer).delay(scrollDelay).queue(function () {
                    if (!hasScrolled) {
                        hideFilterPanel();
                        performFreezeColumns();

                        hasScrolled = true;
                    }

                    $(this).dequeue();
                });
            }
        });
    }

    //refresh freeze column when ajax request
    function refreshFreezeColumns() {
        showFrozenVerticalLine();
        performFreezeColumns();
    }

    //show vertical line indicates how many columns are currently frozen
    function showFrozenVerticalLine() {
        $(tableHeaderCol).eq((frozenColCount - 1)).css("border-right", freezeColStyle);
        $(tableContentGroupCol).eq((frozenColCount - 1)).css("border-right", freezeColStyle);

        //vunx: Thay the dong code cu cua GridExtension.js la:
        // $(gridContentTable).find("tbody tr td[cellIndex=1]").css("border-right", freezeColStyle);
        //thanh doan code ben duoi

        //lay tat ca cac dong, tuong ung moi dong lay cot can frozen de gan css
        tableContentGrouptr = $(gridContentTable).find("tbody tr");
        $.each(tableContentGrouptr, function () {
            eachTd = $(this).find("td").eq((frozenColCount - 1)).css("border-right", freezeColStyle);
            // console.log(eachTd);
        });

        // console.log($(gridContentTable).find("tbody tr"));
    }

    //get grid table details 
    function getGridInfo() {
        gridHeaderTable = grid.find(".t-grid-header table");
        gridContentTable = grid.find(".t-grid-content table");
        tableHeaderGroupCol = $(gridHeaderTable).find("colGroup col");
        tableHeaderCol = $(gridHeaderTable).find("tbody tr th");
        tableContentGroupCol = $(gridContentTable).find("colGroup col");
        totalColCount = $(tableHeaderGroupCol).length;
    }

    //get how many columns are not visible behind the content container
    function getHiddenColCount() {
        var visibleWidth = grid.find("div.t-grid-header").width();
        var visibleColCount = 0;

        $.each(tableHeaderGroupCol, function (idx, value) {
            totalColWidth += getWidth($(value));

            if (visibleWidth >= totalColWidth) {
                visibleColCount = idx;
            }
        });

        hiddenColCount = totalColCount - visibleColCount;
        $(tableHeaderGroupCol).eq(totalColCount - 1).css("width", "100%");
        $(tableContentGroupCol).eq(totalColCount - 1).css("width", "100%");
    }

    //create column width array to detect which column to hide when scrolling
    function createColumnWidthList() {
        for (i = frozenColCount; i <= hiddenColCount; i++) {
            columnWidthArr.push(getWidth($(tableHeaderGroupCol).eq(i)));
        }
    }

    //get total width for frozen column
    function getFrozenColumnsWidth() {
        //determine the total width of frozen columns
        for (i = 0; i <= frozenColCount - 1; i++) {
            frozenColWidth += getWidth($(tableHeaderGroupCol).eq(i));
        }
    }

    //create frozen scrolling container
    function createScrollContainer() {
        //disable grid default scrolling
        $(grid).find(".t-grid-content").css("overflow-x", "hidden");
        if ($(frozenContainer).length == 0) {
            //create controls to handle freeze column scrolling
            frozenContainer = $("<div id='gridfrozenContainer' />");
            frozenInnerContainer = $("<div id='gridfrozenInnerContainer' />");
        }

        $(frozenContainer).css("height", "17px").css("overflow", "scroll").css("width", "100%");
        $(frozenInnerContainer).css("height", "17px").css("width", $(gridContentTable).width());

        $(frozenContainer).append($(frozenInnerContainer));
        $(frozenContainer).insertAfter('.t-grid-content');
    }

    //get width of a particular element
    function getWidth(element) {
        //chrome and safari detect width differently, getting width from style
        if ($.browser.chrome || $.browser.safari || ($.browser.msie && browserVersion > 7)) {
            if ($(element).css("width") != undefined) {
                 return eval(parseInt($(element).css("width").replace("px", "")));
            }
        }
        else {
            return $(element).width();
        }
    }

    //initialise browser detection
    function initBrowsers() {
        $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
        $.browser.safari = /safari/.test(navigator.userAgent.toLowerCase());
        $.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
        $.browser.firefox = /firefox/.test(navigator.userAgent.toLowerCase());
        $.browser.moriza = /moriza/.test(navigator.userAgent.toLowerCase());
    }

    //perform freeze columns implementation
    function performFreezeColumns() {
        var colOffset = 10;
        var adjacentColWidth = columnWidthArr[frozenColCount - 1];
        var scrollPos = $(frozenContainer).scrollLeft() + frozenColWidth + adjacentColWidth - colOffset;

        totalColWidth = frozenColWidth;

        for (i = frozenColCount; i <= hiddenColCount - 1; i++) {
            totalColWidth += columnWidthArr[i - 1];

            var showCol = (scrollPos < totalColWidth);

            $(tableHeaderCol).eq(i).children().toggle(showCol);
            $(tableHeaderGroupCol).eq(i).toggle(showCol);
            $(tableContentGroupCol).eq(i).toggle(showCol);

            if (!$.browser.msie || ($.browser.msie && browserVersion > 7)) {
                $(tableHeaderCol).eq(i).toggle(showCol);
                //thay the dong duoi bang code moi
                // $(gridContentTable).find("tbody tr td[cellIndex=" + i + "]").toggle(showCol);

                //code moi
                tableContentGrouptr = $(gridContentTable).find("tbody tr");
                $.each(tableContentGrouptr, function () {
                    eachTd = $(this).find("td").eq(i).toggle(showCol);
                });
            }
            else if ($.browser.msie && browserVersion < 8) {
                //ie 6 and 7 hack, doesn't hide column correctly
                $(tableHeaderCol).eq(i).css("border-right", showCol ? "solid 1px" : "none");
                //thay the dong duoi bang code moi
                // $(gridContentTable).find("tbody tr td[cellIndex=" + i + "]").css("border-right", showCol ? "solid 1px" : "none");
                //code moi
                tableContentGrouptr = $(gridContentTable).find("tbody tr");
                $.each(tableContentGrouptr, function () {
                    eachTd = $(this).find("td").eq(i).css("border-right", showCol ? "solid 1px" : "none");
                });

            }
        }

        currentScrollPos = $(frozenContainer).scrollLeft();
    }

    // hide filter panel when scrolling
    function hideFilterPanel() {
        var filterPanel = $(grid).find(" .t-animation-container");
        if ($(filterPanel).length > 0) {
            $(filterPanel).hide();
        }
    }

})(jQuery);

