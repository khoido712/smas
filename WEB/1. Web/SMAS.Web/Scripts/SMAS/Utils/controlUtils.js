﻿var controlUtils = {
    DisabledCopyPasteTextboxsByName: function (TextboxsName) {
        var myInput = document.getElementsByName(TextboxsName);
        for (var i = 0; i < myInput.length; i++) {
            myInput[i].onpaste = function (e) {
                e.preventDefault();
            }
        }
    },

    //disabled copy paste text box theo ten hoac id
    DisabledCopyPasteTextboxByID: function (TextboxID) {
        var myInput = document.getElementById(TextboxID);
        myInput.onpaste = function (e) {
            e.preventDefault();
        }
    },

    AddMoveButtonEvent: function (GridID) {
        //auto height for grid

        smas.autoHeightTable(GridID);

        $("input[type=text]", GridID).focus(function (event) {
            $("td", GridID).removeClass("FocusTD");
            var td = $(this).parents(GridID + " td").first();
            $(td).addClass("FocusTD");
        });

        $("td", GridID).click(function (event) {
            var input = $(this).children("input[type=text]").first();
            if (validateUtils.IsEditable(input)) {
                event.stopPropagation();
                input.focus(function () {
                    this.select();
                }).focus();
            }
        });

        $("input[type=text]", GridID).unbind("keydown").keydown(function (event) {
            event.stopPropagation();
            var tr = $(this).parents(GridID + " tr").last();
            var index = $("input[type=text]", tr).index(this);
            var max = $("input[type=text]", tr).length;
            switch (event.keyCode.toString()) {
                case "13":
                case "40": //Move Down
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.next(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("input[type=text]:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
                case "38": //Move Up
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.prev(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("input[type=text]:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);

                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
                case "39": //Move Right
                    var idx = index;
                    var next = true;
                    var input = null;
                    while (next) {
                        idx++;
                        if (idx >= 0 && idx < max) {
                            input = $("input[type=text]:eq(" + idx + ")", tr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }

                    return false;
                case "37": //Move Left
                    var idx = index;
                    var next = true;
                    var input = null;
                    while (next) {
                        idx--;
                        if (idx >= 0 && idx < max) {
                            input = $("input[type=text]:eq(" + idx + ")", tr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
            }
        });
    },


    AddMoveButtonEventByKeyCode: function (obj, GridID, keyCode, valConfig, countPupilStudying, cellLocation, countTotalPupil, codeEnter, strLockMark,admin) {
        //auto height for grid
        if (admin == "True")
        {
            strLockMark = "";
        }
        smas.autoHeightTable(GridID);
        $("input[type=text]", GridID).focus(function (event) {
            $("td", GridID).removeClass("FocusTD");
            var td = $(this).parents(GridID + " td").first();
            $(td).addClass("FocusTD");
        });

        $("td", GridID).click(function (event) {
            var input = $(this).children("input[type=text]").first();
            if (validateUtils.IsEditable(input)) {
                event.stopPropagation();
                input.focus(function () {
                    this.select();
                }).focus();
            }
        });


        //event.stopPropagation();
        var tr = $(obj).parents(GridID + " tr").last();
        var index = $("input[type=text]", tr).index($(obj));
        var max = $("input[type=text]", tr).length;

        if (cellLocation == countPupilStudying && valConfig == 1) {
            //nhay cot
            keyCode = 38;
            index += 1;
        }

        if (valConfig == 2 && index == max - 1) {
            //hay hang
            keyCode = 40;
            index = 0;
        }

        switch (keyCode.toString()) {
            case "13":
            case "39": //Move Right
                var idx = index;
                var next = true;
                var input = null;
                while (next) {
                    idx++;
                    if (idx >= 0 && idx < max) {
                        input = $("input[type=text]:eq(" + idx + ")", tr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }

                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }

                return false;
            case "37": //Move Left
                var idx = index;
                var next = true;
                var input = null;
                while (next) {
                    idx--;
                    if (idx >= 0 && idx < max) {
                        input = $("input[type=text]:eq(" + idx + ")", tr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }
                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;

            case "40": //Move Down
                var ntr = tr;
                var next = true;
                var input = null;
                while (next) {
                    if (codeEnter != 13) {
                        ntr = ntr.next(GridID + " tr");
                    }
                    if (ntr.length > 0) {
                        input = $("input[type=text]:eq(" + index + ")", ntr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }
                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;
            case "38": //Move Up
                var ntr = tr;
                var next = true;
                var input = null;
                var ntrTest = null;
                while (next) {

                    if (countTotalPupil != null && countTotalPupil != "") {//4
                        countTotalPupil = parseInt(countTotalPupil);
                        countPupilStudying = parseInt(countPupilStudying);
                        var k = 0;
                        if (countTotalPupil > 1) {//3
                            for (k; k < countTotalPupil; k++) {//2
                                ntr = ntr.prev(GridID + " tr");
                                ntrTest = ntr.prev(GridID + " tr");

                                if (ntrTest.length <= 0) {//1
                                    k = countTotalPupil;
                                }//1                             
                            }//2
                        }//3
                    }//4

                    if (ntr.length > 0) {
                        var h = 0;
                        var checkInput = null;
                        if (countTotalPupil > 1) {
                            if (countPupilStudying != countTotalPupil) {

                                checkInput = $("input[type=text]:eq(" + index + ")", ntr);
                                if (checkInput == null && checkInput == "" && !validateUtils.IsEditable(checkInput)) {
                                    for (h = 0 ; h < countTotalPupil; h++) {
                                        ntr = ntr.next(GridID + " tr");
                                        checkInput = $("input[type=text]:eq(" + index + ")", ntr);
                                        if (checkInput != null && checkInput != "" && validateUtils.IsEditable(checkInput)) {
                                            h = countTotalPupil;
                                        }
                                    }
                                }
                                else {


                                    ntr = ntr.next(GridID + " tr");
                                    checkInput = $("input[type=text]:eq(" + index + ")", ntr);
                                    //console.log(checkInput);
                                    var colName = $(checkInput).attr("ColNameDefault");
                                    if (strLockMark.indexOf(colName) >= 0) {
                                        var idx = index;
                                        idx += 1;
                                        //next qua cot khac
                                        checkInput = $("input[type=text]:eq(" + idx + ")", tr);

                                        if (checkInput.length > 0) {
                                            //Neu thay doi tuong
                                            // tro ve dau td duoc chon                                                                                

                                            var h = 0;
                                            checkInput = $("input[type=text]:eq(" + idx + ")", ntr);
                                            if (checkInput == null && checkInput == "" && !validateUtils.IsEditable(checkInput)) {
                                                for (h = 0 ; h < countTotalPupil; h++) {
                                                    ntr = ntr.next(GridID + " tr");
                                                    checkInput = $("input[type=text]:eq(" + idx + ")", ntr);
                                                    if (checkInput != null && checkInput != "" && validateUtils.IsEditable(checkInput)) {
                                                        h = countTotalPupil;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        next = false;
                                    }
                                }
                            }
                            else {

                                //Lay ten cot diem
                                checkInput = $("input[type=text]:eq(" + index + ")", ntr);

                                var colName = $(checkInput).attr("ColNameDefault");

                                if (strLockMark.indexOf(colName) >= 0) {
                                    var idx = index;
                                    idx += 1;
                                    //next qua cot khac
                                    checkInput = $("input[type=text]:eq(" + idx + ")", tr);

                                    if (checkInput.length > 0) {
                                        //Neu thay doi tuong
                                        // tro ve dau td duoc chon                                                                                

                                        var h = 0;
                                        checkInput = $("input[type=text]:eq(" + idx + ")", ntr);
                                        if (checkInput == null && checkInput == "" && !validateUtils.IsEditable(checkInput)) {
                                            for (h = 0 ; h < countTotalPupil; h++) {
                                                ntr = ntr.next(GridID + " tr");
                                                checkInput = $("input[type=text]:eq(" + idx + ")", ntr);
                                                if (checkInput != null && checkInput != "" && validateUtils.IsEditable(checkInput)) {
                                                    h = countTotalPupil;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        input = checkInput;// $("input[type=text]:eq(" + index + ")", ntr);                         
                        // next = !validateUtils.IsEditable(input);
                        next = false;

                    } else {
                        next = false;
                    }
                }


                if (input != null) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;
        }
    },

    AddMoveButtonEventByKeyMoveRight: function (obj, GridID, keyCode, valConfig, countPupilStudying, cellLocation, countTotalPupil, codeEnter) {
        //auto height for grid
        smas.autoHeightTable(GridID);

        $("input[type=text]", GridID).focus(function (event) {
            $("td", GridID).removeClass("FocusTD");
            var td = $(this).parents(GridID + " td").first();
            $(td).addClass("FocusTD");
        });

        $("td", GridID).click(function (event) {
            var input = $(this).children("input[type=text]").first();
            if (validateUtils.IsEditable(input)) {
                event.stopPropagation();
                input.focus(function () {
                    this.select();
                }).focus();
            }
        });


        //event.stopPropagation();
        var tr = $(obj).parents(GridID + " tr").last();
        var index = $("input[type=text]", tr).index($(obj));
        var max = $("input[type=text]", tr).length;

        if (cellLocation == countPupilStudying && valConfig == 1) {
            //nhay cot
            keyCode = 38;
            index += 1;
        }

        if (valConfig == 2 && index == max - 1) {
            //hay hang
            keyCode = 40;
            index = 0;
        }

        switch (keyCode.toString()) {
            case "13":
            case "39": //Move Right
                var idx = index;
                var next = true;
                var input = null;
                while (next) {

                    idx++;
                    if (idx >= 0 && idx < max) {
                        input = $("input[type=text]:eq(" + idx + ")", tr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }
                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }

                return false;
            case "37": //Move Left
                var idx = index;
                var next = true;
                var input = null;
                while (next) {
                    idx--;
                    if (idx >= 0 && idx < max) {
                        input = $("input[type=text]:eq(" + idx + ")", tr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }
                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;

            case "40": //Move Down
                var ntr = tr;
                var next = true;
                var input = null;
                while (next) {
                    if (codeEnter != 13) {
                        ntr = ntr.next(GridID + " tr");
                    }
                    if (ntr.length > 0) {
                        input = $("input[type=text]:eq(" + index + ")", ntr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }
                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;
            case "38": //Move Up
                var ntr = tr;
                var next = true;
                var input = null;
                while (next) {

                    if (countTotalPupil != null && countTotalPupil != "") {
                        countTotalPupil = parseInt(countTotalPupil);
                        countPupilStudying = parseInt(countPupilStudying);
                        var k = 0;
                        var ntrTest = null;
                        if (countTotalPupil > 1) {
                            for (k; k < countTotalPupil; k++) {
                                ntr = ntr.prev(GridID + " tr");
                                ntrTest = ntr.prev(GridID + " tr");
                                if (ntrTest.length <= 0) {
                                    k = countTotalPupil;
                                }
                            }
                        }
                    }

                    if (ntr.length > 0) {
                        var h = 0;
                        var checkInput = null;
                        if (countTotalPupil > 1) {
                            if (countPupilStudying != countTotalPupil) {
                                for (h = 0 ; h < countTotalPupil; h++) {
                                    ntr = ntr.next(GridID + " tr");
                                    checkInput = $("input[type=text]:eq(" + index + ")", ntr);
                                    if (checkInput != null && checkInput != "" && validateUtils.IsEditable(checkInput)) {
                                        h = countTotalPupil;
                                    }
                                }
                            }
                        }

                        input = $("input[type=text]:eq(" + index + ")", ntr);
                        next = !validateUtils.IsEditable(input);
                    } else {
                        next = false;
                    }
                }

                if (input != null && validateUtils.IsEditable(input)) {
                    input.focus(function () {
                        this.select();
                    }).focus();
                }
                return false;
        }
    },

    AddMoveButtonEventDownOrUp: function (GridID) {
        //auto height for grid

        smas.autoHeightTable(GridID);

        $("input[type=text]", GridID).focus(function (event) {
            $("td", GridID).removeClass("FocusTD");
            var td = $(this).parents(GridID + " td").first();
            $(td).addClass("FocusTD");
        });

        $("td", GridID).click(function (event) {
            var input = $(this).children("input[type=text]").first();
            if (validateUtils.IsEditable(input)) {
                event.stopPropagation();
                input.focus(function () {
                    this.select();
                }).focus();
            }
        });

        $("input[type=text]", GridID).unbind("keydown").keydown(function (event) {
            event.stopPropagation();
            var tr = $(this).parents(GridID + " tr").last();
            var index = $("input[type=text]", tr).index(this);
            var max = $("input[type=text]", tr).length;
            switch (event.keyCode.toString()) {
                case "13":
                case "40": //Move Down
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.next(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("input[type=text]:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
                case "38": //Move Up
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.prev(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("input[type=text]:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);

                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
            }
        });
    },

    ChangeStyleButton: function (Button) {
        smas.showProcessingDialog();
        if (!$(Button).hasClass('Active')) {
            var ul = $(Button).parent().parent();
            $(ul).find('a').each(function () {
                $(this).removeClass('Active');
            });
            $(Button).addClass('Active');
        }
    },

    DisableButton: function (Button) {
        $.each($(Button), function (index, item) {
            $(item).attr("disabled", "disabled");
            $(item).addClass("t-state-disabled");
        });
    },

    EnableButton: function (Button) {
        $.each($(Button), function (index, item) {
            $(item).removeAttr("disabled");
            $(item).removeClass("t-state-disabled");
        });
    },

    loadComboBox: function (objCombo, url, data, placeHolder, calback, param) {
        $.ajax({
            type: "post",
            dataType: "json",
            url: url,
            data: data,
            success: function (data) {
                objCombo.html("");
                if (placeHolder != undefined && placeHolder != null && placeHolder != "")
                    objCombo.html('<option value="">' + placeHolder + '</option>');
                for (i = 0; i < data.length; i++) {
                    var strval = data[i].Text;
                    strval = strval.replace(/&/g, '&amp;');
                    strval = strval.replace(/</g, '&lt;');
                    strval = strval.replace(/>/g, '&gt;');
                    if (data[i].Selected != null && data[i].Selected != '' && data[i].Selected) {
                        objCombo.append('<option value="' + data[i].Value + '" selected >' + strval + '</option>')
                    } else {
                        objCombo.append('<option value="' + data[i].Value + '">' + strval + '</option>')
                    }
                }
                setTimeout(function ()
                {
                if (calback != null && calback != '' && calback != undefined) {
                    if (param != undefined) {
                        calback(param);
                    } else {
                        calback();
                    }
                }
                }, 500);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert('Internal server error');
            }

        });
    },
    // AnhVD - Change color mark
    // Apply for Time Mark
    changeTimeMark: function (e) {
        var sumM = 0;
        var sumP = 0.0;
        var sumV = 0.0;
        /* tr - tr - tr */
        var countM = $(e).find('td[id^="markM"]').find('input:first[value!=""]').length;
        var countP = $(e).find('td[id^="markP"]').find('input:first[value!=""]').length;
        var countV = $(e).find('td[id^="markV"]').find('input:first[value!=""]').length;

        var hfCoefficientM = parseInt($('input[id*="hfCoefficientM"]').val());
        var hfCoefficientP = parseInt($('input[id*="hfCoefficientP"]').val());
        var hfCoefficientV = parseInt($('input[id*="hfCoefficientV"]').val());
        var hfCoefficientKTHK = parseInt($('input[id*="hfCoefficientHK"]').val());
        //$(e).find('input[type="checkbox"][id*="chk"]').attr('checked', 'checked');
        $(e).find('td[id^="markM"]').each(function () {
            var value = $(this).find('input').val();
            if (value != "") {
                sumM += parseFloat(value);
            }
        });
        $(e).find('td[id^="markP"]').each(function () {
            var value = $(this).find('input').val().toString();
            if (value != "") {
                sumP += parseFloat(value);
            }
        });

        $(e).find('td[id^="markV"]').each(function () {
            var value = $(this).find('input').val().toString();
            if (value != "") {
                sumV += parseFloat(value);
            }
        });

        var countKTHK = $(e).find('td[id="markKTHK"]').find('input:first[value!=""]').length;
        var KTHK = 0;
        if (countKTHK > 0) {
            KTHK = parseFloat($(e).find('td[id="markKTHK"]').find('input:first').val().toString());;
        }
        var TBM = "";
        var TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
        if (countM != 0 || countP != 0 || countV != 0 || countKTHK != 0) {
            sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
            sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;
            TBMTC = TBMTC;
            if (TBMTC >= 8.0) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.3);
            }
            else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.2);
            }
            else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.1);
            }
            else {
                TBM = Math.round(sumMark / sumCoEff * 10) / 10;
            }
            TBM = Math.round(TBM * 10) / 10;
            if (TBM > 10.0) {
                TBM = 10.0;
            }
            TBM = String(TBM)
        }
        var ctTBM = $(e).find('td[id^="TBHK"]')[0];
        $(ctTBM).addClass('StyleMarkChange');
        if (TBM == '0' || TBM == '10') {
            if (TBM == 0) {
                $(ctTBM).text('0.0');
            }
            else {
                $(ctTBM).text('10');
            }
        }
        else {
            $(ctTBM).text(TBM);
        }
    },

    changeTimeMarkGDTX: function(e)
    {
        var sumM = 0;
        var sumP = 0;
        var sumV = 0;
        /* tr - tr - tr */
        var countM = $(e).find('td[id^="markM"]').find('input:first[value!=""]').length;
        var countP = $(e).find('td[id^="markP"]').find('input:first[value!=""]').length;
        var countV = $(e).find('td[id^="markV"]').find('input:first[value!=""]').length;

        var hfCoefficientM = parseInt($('input[id*="hfCoefficientM"]').val());
        var hfCoefficientP = parseInt($('input[id*="hfCoefficientP"]').val());
        var hfCoefficientV = parseInt($('input[id*="hfCoefficientV"]').val());
        var hfCoefficientKTHK = parseInt($('input[id*="hfCoefficientHK"]').val());
        //$(e).find('input[type="checkbox"][id*="chk"]').attr('checked', 'checked');
        $(e).find('td[id^="markM"]').each(function () {
            var value = $(this).find('input').val();
            if (value != "") {
                sumM += parseFloat(value);
            }
        });
        $(e).find('td[id^="markP"]').each(function () {
            var value = $(this).find('input').val();
            if (value != "") {
                sumP += parseFloat(value);
            }
        });

        $(e).find('td[id^="markV"]').each(function () {
            var value = $(this).find('input').val();
            if (value != "") {
                sumV += parseFloat(value);
            }
        });

        var countKTHK = $(e).find('td[id="markKTHK"]').find('input:first[value!=""]').length;
        var KTHK = 0;
        if (countKTHK > 0) {
            KTHK = parseFloat($(e).find('td[id="markKTHK"]').find('input:first').val().toString());;
        }
        var TBM = "";
        var TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
        if (countM != 0 || countP != 0 || countV != 0 || countKTHK != 0) {
            sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
            sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;
            TBMTC = TBMTC;
            if (TBMTC >= 8.0) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.3);
            }
            else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.2);
            }
            else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                TBM = ((Math.round(sumMark / sumCoEff * 10) / 10) + 0.1);
            }
            else {
                TBM = Math.round(sumMark / sumCoEff * 10) / 10;
            }
            TBM = Math.round(TBM * 10) / 10;
            if (TBM > 10.0) {
                TBM = 10.0;
            }
            TBM = String(TBM)
        }
        var ctTBM = $(e).find('td[id^="TBHK"]')[0];
        $(ctTBM).addClass('StyleMarkChange');
        if (TBM == '0' || TBM == '10') {
            if (TBM == 0) {
                $(ctTBM).text('0.0');
            }
            else {
                $(ctTBM).text('10');
            }
        }
        else {
            $(ctTBM).text(TBM);
        }
    },
    // Apply for Semester Mark
    changeCommonMark: function (e, tbm1) {
        var sumM = 0;
        var sumP = 0.0;
        var sumV = 0.0;
        var KTHK = 0.0;
        var countM = $(e).find('td[id^="markM"]').find('.text_markM').find('input:first[value!=""]').length;
        var countP = $(e).find('td[id^="markP"]').find('.text_mark').find('input:first[value!=""]').length;
        var countV = $(e).find('td[id^="markV"]').find('.text_mark').find('input:first[value!=""]').length;

        var hfCoefficientM = parseInt($('input[id*="hfCoefficientM"]').val());
        var hfCoefficientP = parseInt($('input[id*="hfCoefficientP"]').val());
        var hfCoefficientV = parseInt($('input[id*="hfCoefficientV"]').val());
        var hfCoefficientKTHK = parseInt($('input[id*="hfCoefficientHK"]').val());
        var isMinSemester = $('input[id*="hdfisMinSemester"]').val()
        if (isMinSemester == 'True') {
            var countMmin = $('input[id*="hdfMmin"]').val();
            var countPmin = $('input[id*="hdfPmin"]').val();
            var countVmin = $('input[id*="hdfVmin"]').val();
            if (parseInt(countM) >= parseInt(countMmin) && parseInt(countP) >= parseInt(countPmin) && parseInt(countV) >= parseInt(countVmin)) {
                $(e).find('td[id^="markM"]').each(function () {
                    var value = $(this).find('.text_markM').find('input[type="text"]').val();
                    if (value != "") {
                        sumM += parseFloat(value);
                    }
                });
                $(e).find('td[id^="markP"]').each(function () {
                    var value = $(this).find('.text_mark').find('input[type="text"]').val().toString();
                    if (value != "") {
                        sumP += parseFloat(value);
                    }
                });

                $(e).find('td[id^="markV"]').each(function () {
                    var value = $(this).find('.text_mark').find('input[type="text"]').val().toString();
                    if (value != "") {
                        sumV += parseFloat(value);
                    }
                });
                var TBM = "";
                var TBHK = "";
                var TBMTC = "";
                var countKTHK = $(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first[value!=""]').length;

                if (countKTHK > 0) {

                    KTHK = parseFloat($(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first').val().toString());
                    sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
                    sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;

                    TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
                    TBMTC = TBMTC;
                    TBMTC = TBMTC.replaceAll(" ", "");
                    TBMTC = parseFloat(TBMTC);

                    TBM = Math.round(sumMark / sumCoEff * 10) / 10;



                    if (TBMTC >= 8.0) {
                        TBM = (TBM + 0.3);

                    }
                    else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                        TBM = (TBM + 0.2);
                    }
                    else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                        TBM = (TBM + 0.1);
                    }

                    TBM = Math.round(TBM * 10) / 10;

                    if (TBM > 10.0) {
                        TBM = 10;
                    }
                    if (tbm1 != null && typeof tbm1 != undefined && tbm1 > 0) {
                        TBCN = Math.round((TBM * 2 + tbm1) / 3 * 10) / 10;
                        $(e).find('td[id="TBCN"]').addClass('StyleMarkChange').text(TBCN.toString());
                    }
                    TBM = String(TBM)
                } else {
                    $(e).find('td[id="TBCN"]').text("");
                }

                // Hiển thị TB môn
                var TBHK = $(e).find('td[id^="TBHK"]')[0];
                $(TBHK).addClass('StyleMarkChange');
                if (TBM == '0' || TBM == '10') {
                    if (TBM == 0) {
                        $(TBHK).text('0.0');
                    }
                    else {
                        $(TBHK).text('10');
                    }
                }
                else {
                    $(TBHK).text(TBM);
                }
            }
            else {
                $($(e).find('td[id^="TBHK"]')[0]).text("");
                $(e).find('td[id="TBCN"]').text("");
            }
        }
        else {
            $(e).find('td[id^="markM"]').each(function () {
                var value = $(this).find('.text_markM').find('input').val();
                if (value != "") {
                    sumM += parseFloat(value);
                }
            });
            $(e).find('td[id^="markP"]').each(function () {
                var value = $(this).find('.text_mark').find('input').val().toString();
                if (value != "") {
                    sumP += parseFloat(value);
                }
            });

            $(e).find('td[id^="markV"]').each(function () {
                var value = $(this).find('.text_mark').find('input').val().toString();
                if (value != "") {
                    sumV += parseFloat(value);
                }
            });
            var TBM = "";
            var TBHK = "";
            var TBMTC = "";
            var countKTHK = $(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first[value!=""]').length;

            if (countKTHK > 0) {

                KTHK = parseFloat($(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first').val().toString());
                sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
                sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;

                TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
                TBMTC = TBMTC;
                TBMTC = TBMTC.replaceAll(" ", "");
                TBMTC = parseFloat(TBMTC);

                TBM = Math.round(sumMark / sumCoEff * 10) / 10;



                if (TBMTC >= 8.0) {
                    TBM = (TBM + 0.3);

                }
                else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                    TBM = (TBM + 0.2);
                }
                else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                    TBM = (TBM + 0.1);
                }

                TBM = Math.round(TBM * 10) / 10;

                if (TBM > 10.0) {
                    TBM = 10;
                }
                if (tbm1 != null && typeof tbm1 != undefined && tbm1 > 0) {
                    TBCN = Math.round((TBM * 2 + tbm1) / 3 * 10) / 10;
                    $(e).find('td[id="TBCN"]').addClass('StyleMarkChange').text(TBCN.toString());
                }
                TBM = String(TBM)
            } else {
                $(e).find('td[id="TBCN"]').text("");
            }

            // Hiển thị TB môn
            var TBHK = $(e).find('td[id^="TBHK"]')[0];
            $(TBHK).addClass('StyleMarkChange');
            if (TBM == '0' || TBM == '10') {
                if (TBM == 0) {
                    $(TBHK).text('0.0');
                }
                else {
                    $(TBHK).text('10');
                }
            }
            else {
                $(TBHK).text(TBM);
            }
            
        }
    },

    fillMarkGDTX: function (control, isTimeMark, tbm1)
    {
        var idMarkName = $(control).attr('id');
        if ($(control).val() == $(control).attr('defaultValue')) {
            $(control).removeClass('StyleMarkChange');
            $(control).closest('td').find('input[type="hidden"]').val('0')
            return;
        } else {
            $(control).addClass('StyleMarkChange');
            $(control).closest('td').find('input[type="hidden"]').val('1')
        }

        if (isTimeMark) {
            this.changeTimeMarkGDTX($(control).parents('tr').parents('tr').parents('tr')[0]);
        }
        else {
            this.changeCommonMarkGDTX($(control).parents('tr').parents('tr')[0], tbm1);
        }
    },

    changeCommonMarkGDTX: function (e, tbm1)
    {
        var sumM = 0;
        var sumP = 0;
        var sumV = 0;
        var KTHK = 0.0;
        var countM = $(e).find('td[id^="markM"]').find('.text_markM').find('input:first[value!=""]').length;
        var countP = $(e).find('td[id^="markP"]').find('.text_markM').find('input:first[value!=""]').length;
        var countV = $(e).find('td[id^="markV"]').find('.text_markM').find('input:first[value!=""]').length;

        var hfCoefficientM = parseInt($('input[id*="hfCoefficientM"]').val());
        var hfCoefficientP = parseInt($('input[id*="hfCoefficientP"]').val());
        var hfCoefficientV = parseInt($('input[id*="hfCoefficientV"]').val());
        var hfCoefficientKTHK = parseInt($('input[id*="hfCoefficientHK"]').val());
        var isMinSemester = $('input[id*="hdfisMinSemester"]').val()
        if (isMinSemester == 'True') {
            var countMmin = $('input[id*="hdfMmin"]').val();
            var countPmin = $('input[id*="hdfPmin"]').val();
            var countVmin = $('input[id*="hdfVmin"]').val();
            if (parseInt(countM) >= parseInt(countMmin) && parseInt(countP) >= parseInt(countPmin) && parseInt(countV) >= parseInt(countVmin)) {
                $(e).find('td[id^="markM"]').each(function () {
                    var value = $(this).find('.text_markM').find('input[type="text"]').val();
                    if (value != "") {
                        sumM += parseFloat(value);
                    }
                });
                $(e).find('td[id^="markP"]').each(function () {
                    var value = $(this).find('.text_markM').find('input[type="text"]').val();
                    if (value != "") {
                        sumP += parseFloat(value);
                    }
                });

                $(e).find('td[id^="markV"]').each(function () {
                    var value = $(this).find('.text_markM').find('input[type="text"]').val();
                    if (value != "") {
                        sumV += parseFloat(value);
                    }
                });
                var TBM = "";
                var TBHK = "";
                var TBMTC = "";
                var countKTHK = $(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first[value!=""]').length;

                if (countKTHK > 0) {

                    KTHK = parseFloat($(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first').val().toString());
                    sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
                    sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;

                    TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
                    TBMTC = TBMTC;
                    TBMTC = TBMTC.replaceAll(" ", "");
                    TBMTC = parseFloat(TBMTC);

                    TBM = Math.round(sumMark / sumCoEff * 10) / 10;



                    if (TBMTC >= 8.0) {
                        TBM = (TBM + 0.3);

                    }
                    else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                        TBM = (TBM + 0.2);
                    }
                    else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                        TBM = (TBM + 0.1);
                    }

                    TBM = Math.round(TBM * 10) / 10;

                    if (TBM > 10.0) {
                        TBM = 10;
                    }
                    if (tbm1 != null && typeof tbm1 != undefined && tbm1 > 0) {
                        TBCN = Math.round((TBM * 2 + tbm1) / 3 * 10) / 10;
                        $(e).find('td[id="TBCN"]').addClass('StyleMarkChange').text(TBCN.toString());
                    }
                    TBM = String(TBM)
                } else {
                    $(e).find('td[id="TBCN"]').text("");
                }

                // Hiển thị TB môn
                var TBHK = $(e).find('td[id^="TBHK"]')[0];
                $(TBHK).addClass('StyleMarkChange');
                if (TBM == '0' || TBM == '10') {
                    if (TBM == 0) {
                        $(TBHK).text('0.0');
                    }
                    else {
                        $(TBHK).text('10');
                    }
                }
                else {
                    $(TBHK).text(TBM);
                }
            }
            else {
                $($(e).find('td[id^="TBHK"]')[0]).text("");
                $(e).find('td[id="TBCN"]').text("");
            }
        }
        else {
            $(e).find('td[id^="markM"]').each(function () {
                var value = $(this).find('.text_markM').find('input').val();
                if (value != "") {
                    sumM += parseFloat(value);
                }
            });
            $(e).find('td[id^="markP"]').each(function () {
                var value = $(this).find('.text_markM').find('input').val();
                if (value != "") {
                    sumP += parseFloat(value);
                }
            });

            $(e).find('td[id^="markV"]').each(function () {
                var value = $(this).find('.text_markM').find('input').val();
                if (value != "") {
                    sumV += parseFloat(value);
                }
            });
            var TBM = "";
            var TBHK = "";
            var TBMTC = "";
            var countKTHK = $(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first[value!=""]').length;

            if (countKTHK > 0) {
                KTHK = parseFloat($(e).find('td[id^="markKTHK"]').find('.text_mark').find('input:first').val().toString());
                sumMark = hfCoefficientM * sumM + hfCoefficientP * sumP + hfCoefficientV * sumV + hfCoefficientKTHK * KTHK;
                sumCoEff = hfCoefficientM * countM + hfCoefficientP * countP + hfCoefficientV * countV + hfCoefficientKTHK * countKTHK;

                TBMTC = $(e).find('td[id^="TBMTC"]').text().trim();
                TBMTC = TBMTC;
                TBMTC = TBMTC.replaceAll(" ", "");
                TBMTC = parseFloat(TBMTC);

                TBM = Math.round(sumMark / sumCoEff * 10) / 10;



                if (TBMTC >= 8.0) {
                    TBM = (TBM + 0.3);

                }
                else if (TBMTC >= 6.5 && TBMTC < 8.0) {
                    TBM = (TBM + 0.2);
                }
                else if (TBMTC >= 5.0 && TBMTC < 6.5) {
                    TBM = (TBM + 0.1);
                }

                TBM = Math.round(TBM * 10) / 10;

                if (TBM > 10.0) {
                    TBM = 10;
                }
                if (tbm1 != null && typeof tbm1 != undefined && tbm1 > 0) {
                    TBCN = Math.round((TBM * 2 + tbm1) / 3 * 10) / 10;
                    $(e).find('td[id="TBCN"]').addClass('StyleMarkChange').text(TBCN.toString());
                }
                TBM = String(TBM)
            } else {
                $(e).find('td[id="TBCN"]').text("");
            }

            // Hiển thị TB môn
            var TBHK = $(e).find('td[id^="TBHK"]')[0];
            $(TBHK).addClass('StyleMarkChange');
            if (TBM == '0' || TBM == '10') {
                if (TBM == 0) {
                    $(TBHK).text('0.0');
                }
                else {
                    $(TBHK).text('10');
                }
            }
            else {
                $(TBHK).text(TBM);
            }

        }
    },

    fillMark: function (control, isTimeMark, tbm1) {
        var idMarkName = $(control).attr('id');
        if ($(control).val() == $(control).attr('defaultValue')) {
            $(control).removeClass('StyleMarkChange');
            $(control).closest('td').find('input[type="hidden"]').val('0')
            return;
        } else {
            $(control).addClass('StyleMarkChange');
            $(control).closest('td').find('input[type="hidden"]').val('1')
        }

        if (isTimeMark) {
            this.changeTimeMark($(control).parents('tr').parents('tr').parents('tr')[0]);
        }
        else {
            this.changeCommonMark($(control).parents('tr').parents('tr')[0], tbm1);
        }

    },

    changeMarkColor: function (control) {
        var idMarkPeriodName = $(control).attr('name');
        if ($(control).val() == $(control).attr('defaultValue')) {
            $(control).removeClass('StyleMarkChange');
            $(control).closest('td').find('input[type="hidden"]').val('0');
            return;
        }
        else {
            $(control).addClass('StyleMarkChange');
                $(control).closest('td').find('input[type="hidden"]').val('1');
        }
    }
    // End 20131212
    ,
    //20131224
    // Cap 1
    fillMarkPrimary: function (txtCK, semester) {
        var value = $(txtCK).val();
        var HLMHK = $(txtCK).parents('td').parents('td').next('td');
        $(HLMHK).text(value);
        var XL = "";
        if (value >= 9) {
            XL = "Giỏi";
        } else if (value >= 7) {
            XL = "Khá";
        } else if (value >= 5) {
            XL = "Trung Bình";
        } else if (value > 0) {
            XL = "Yếu";
        } else {
            XL = "";
        }

        $(HLMHK).next('td').text(XL);
        if (semester == 2) {
            $(HLMHK).next('td').next('td').text(value);
            //$(HLMHK).next('td').next('td').next('td').text(value);
        }
    },

    AddMoveTabEvent: function (GridID) {

        smas.autoHeightTable(GridID);

        $("textarea", GridID).unbind("keydown").keydown(function (event) {
            event.stopPropagation();
            var tr = $(this).parents(GridID + " tr").last();
            var index = $("textarea", tr).index(this);
            var max = $("textarea", tr).length;
            switch (event.keyCode.toString()) {
                case "34": //Page down Move Down 
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.next(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("textarea:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
                case "33": //Move Up pageUp                   
                    var ntr = tr;
                    var next = true;
                    var input = null;
                    while (next) {
                        ntr = ntr.prev(GridID + " tr");
                        if (ntr.length > 0) {
                            input = $("textarea:eq(" + index + ")", ntr);
                            next = !validateUtils.IsEditable(input);
                        } else {
                            next = false;
                        }
                    }
                    if (input != null && validateUtils.IsEditable(input)) {
                        input.focus(function () {
                            this.select();
                        }).focus();
                    }
                    return false;
            }
        });
    }
};