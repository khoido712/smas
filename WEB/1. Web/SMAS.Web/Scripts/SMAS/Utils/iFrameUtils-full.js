﻿/// <reference path="popupUtils.js" />
//LISTENER IFRAME MESSAGE FUNCTION TO RESIZE CONTEXT - IFRAME POST MESSAGE EVENT
$(document).ready(function () {
    //handle post event from child iframe
    if (!window.addEventListener) {
        window.attachEvent("onmessage", iFrameUtils.receiveMessageIframe);
    }
    else {
        window.addEventListener("message", iFrameUtils.receiveMessageIframe, false);
    }    
});

iFrameUtils = {
    //receiver message from iframe
    receiveMessageIframe: function (evt) {
        if (evt.data) {
            var messageObject = jQuery.parseJSON(evt.data);
            switch (messageObject.code) {
                case 0:
                    {
                        //post value of height content -> resize height iframe
                        $('#SmsEduFrame').css('height', messageObject.value)
                        break;
                    }
                case 1:
                    {
                        //post from detail mailbox -> scroll to top
                        $(window).scrollTop($('#ScrollToDiv').offset().top - 100);
                        RefreshNotificationPartial();
                        break;
                    }
                case 2:
                    {
                        //post from contactGroup -> scroll to top
                        $(window).scrollTop($('#SmsEduFrame').offset().top - 100);
                        break;
                    }
                case 3:
                    {
                        //mark tag a popup confirm
                        iFrameUtils.markPopupConfirm();
                        break;
                    }
                case -3:
                    {
                        //unbind click event 
                        iFrameUtils.unbindConfirm();
                        break;
                    }
                case 4:
                    {
                        //redirect to page
                        window.location.href = messageObject.value;
                        break;
                    }

                case 51:
                    {
                        //post from smas.alert() -> call popupUtils.alert()
                        popupUtils.alert(messageObject.value, messageObject.isShowDialogIframe);
                        break;
                    }
                case 52:
                    {
                        //post from smas.showProcessingDialog() -> call popupUtils.showProcessingDialog()
                        popupUtils.showProcessingDialog(messageObject.value);
                        break;
                    }
                case 53:
                    {
                        //post from smas.hideProcessingDialog() -> call popupUtils.hideProcessingDialog()
                        popupUtils.hideProcessingDialog();
                        break;
                    }
                case 54:
                    {
                        //post from smas.openDialog() -> call popupUtils.openDialog()
                        popupUtils.openDialog();
                        break;
                    }
                case 55:
                    {
                        //post from smas.closeDialog() -> call popupUtils.closeDialog()
                        popupUtils.closeDialog();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    },

    //post message to child iframe
    //->MessageIFrame { code, value }   
    postSpecialCode: function (messageIframe) {

        var iframe = $('#SmsEduFrame')[0];
        var myWindow = document.getElementById(iframe).contentWindow;
        myWindow.postMessage(messageIframe, "*"); // Noncompliant; how do you know what you loaded in 'myIFrame' is still there?

        //iframe.contentWindow.postMessage(messageIframe, '*');
    },

    // mark popup confirm to tag a
    markPopupConfirm: function () {
        $('a').click(function (e) {
            var url = $(this).attr('href');
            // code = 3: request popup confirm import
            if (url && (url.indexOf('#') != 0 || url != "javascript:")) {
                iFrameUtils.postSpecialCode({ code: 3, value: url });
            }
            e.preventDefault();
        });
    },

    unbindConfirm: function () {
        $('a:not([href^="#"]):not([href^="javascript:"])').unbind("click");
    },

    checkAvailableEDU: function () {
        $.cookie('isSmsEduAvailable', '1');
    }
}