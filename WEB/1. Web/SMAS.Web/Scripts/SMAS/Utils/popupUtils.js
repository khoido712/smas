﻿var isHasChildIframeEDU = false;
$(document).ready(function () {
    isHasChildIframeEDU = $(".IframeContentStyle").length > 0
});
popupUtils = {
    addIndexIframe: function () {
        $(".IframeContentStyle").addClass("IframeIndexStyle")
    },
    removeIndexIframe: function () {
        $(".IframeContentStyle").removeClass("IframeIndexStyle")
    },
    addLoadingBGStyle: function () {
        $("#OverLayLayout").addClass("DlgOverlay");
        if (isHasChildIframeEDU == false) {
            $("#OverLayLayout").addClass("DlgOverlayIndex")
        }
    }, removeLoadingBGStyle: function () {
        $("#OverLayLayout").removeClass("DlgOverlay");
        if (isHasChildIframeEDU == false) {
            $("#OverLayLayout").removeClass("DlgOverlayIndex")
        }
    },
    openDialog: function () {
        this.addIndexIframe(); this.addLoadingBGStyle()
    },
    closeDialog: function () {
        popupUtils.removeIndexIframe();
        popupUtils.removeLoadingBGStyle()
    },
    alert: function (e, t) {
        if ($("#MsgProcessPopupNF").length > 0) {
            $("#MsgProcessPopupNF").remove()
        }
        if (t != true) {
            this.hideProcessingDialog(); this.removeIndexIframe()
        }
        if (e == null || e == "") {
            return
        }
        msg = popupUtils.GetJsonObject(e);
        if (msg == null) {
            msg = {}; msg["Type"] = "success"; msg["Message"] = e
        }
        if (msg.Type == "AJAX_SESSION_ERROR") {
            window.location.reload(true)
        }
        else {
            $('<div id="MsgProcessPopupNF" class="MsgSuccess"><div class="MsgLeftSuccess ' + msg.Type + '"><div class="MsgRightSuccess"><p>' + msg.Message + "<p></div></div></div>").appendTo("body");
            if (isHasChildIframeEDU == false) {
                $("#MsgProcessPopupNF").addClass("MsgSuccessIndex")
            } $("div#MsgProcessPopupNF").fadeIn(100);
            setTimeout(function () {
                popupUtils.fadeOutMessage()
            }, 5e3)
        }
    },
    GetJsonObject: function (variable) {
        try {
            if ($.isPlainObject(variable)) {
                return variable
            }
            else {
                jsonObj = eval("(" + variable + ")");
                return jsonObj
            }
        }
        catch (e) {
            return null
        }
    }, hideProcessingDialog: function (e) {
        if (e != null) {
            if (e.status == 451) {
                popupUtils.redirect(webRoot + "SSOServices.aspx?LogOff=true")
            }
            if (e.status == 453) {
                popupUtils.alert({ Type: "error", Message: "Access denied" })
            }
            if (e.status == 454) {
                popupUtils.redirect(webRoot + "SSOServices.aspx?LogOff=true")
            } if (e.status == 450) {
                popupUtils.alert(e.responseText)
            }
        }
        $("#processingDialogLayout").hide();
        popupUtils.removeLoadingBGStyle();
        if (isHasChildIframeEDU == false) {
            $("#processingDialogLayout").removeClass("LoadingSectionIndex")
        }
    }, showProcessingDialog: function () {
        popupUtils.removeIndexIframe();
        $("#processingDialogLayout").show();
        popupUtils.addLoadingBGStyle();
        if (isHasChildIframeEDU == false) {
            $("#processingDialogLayout").addClass("LoadingSectionIndex")
        }
    }, redirect: function (e) {
        window.location = e
    }, fadeOutMessage: function () {
        $("div#MsgProcessPopupNF").fadeOut("slow", function () {
            $("div#MsgProcessPopupNF").remove()
        })
    }
}