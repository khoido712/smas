﻿var jsonUtils = {
    GetJsonObject: function (variable) {
        try {
            if ($.isPlainObject(variable)) {
                return variable;
            } else {
                jsonObj = eval('(' + variable + ')');
                return jsonObj;
            }
        } catch (e) {
            return null;
        }
    },

    /**
    * Chuyen cac du lieu trong formId thanh danh doi tuong JSON
    * VD: neu formId co 2 truong du lieu laf fieldName1, fieldName2 thi ket qua la:
    *        {fieldName1: fieldValue1, fieldName2: fieldValue2}
    **/
    serializeObject: function (formId) {
        _x = $("#" + formId).serializeArray();
        _o = {}
        $.each(_x, function (i, field) {
            _o[field.name] = field.value
        });
        return _o;
    }
};