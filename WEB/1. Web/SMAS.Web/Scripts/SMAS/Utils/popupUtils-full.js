﻿/// <reference path="../jquery-1.6.3.min.js" />
var isHasChildIframeEDU = false;
$(document).ready(function () {
    isHasChildIframeEDU = $('.IframeContentStyle').length > 0;
});
popupUtils = {
    //insert class style z-index of iframe
    addIndexIframe: function () {
        $('.IframeContentStyle').addClass('IframeIndexStyle');
    },

    //remove class style z-index of iframe
    removeIndexIframe: function () {
        $('.IframeContentStyle').removeClass('IframeIndexStyle');
    },

    //insert loading background style
    addLoadingBGStyle: function () {
        $('#OverLayLayout').addClass('DlgOverlay');
        if (isHasChildIframeEDU == false) {
            $('#OverLayLayout').addClass('DlgOverlayIndex');
        }
    },

    //remove loading background style
    removeLoadingBGStyle: function () {
        $('#OverLayLayout').removeClass('DlgOverlay');
        if (isHasChildIframeEDU == false) {
            $('#OverLayLayout').removeClass('DlgOverlayIndex');
        }
    },

    //show overLay for openDialog
    openDialog: function () {
        this.addIndexIframe();
        this.addLoadingBGStyle();
    },

    closeDialog: function () {
        popupUtils.removeIndexIframe();
        popupUtils.removeLoadingBGStyle();
    },

    /**
    * Hien thi cau thong bao
    * Nếu message là một chuỗi dạng JSON ("{Type: '....', Message: '...'}") thì parse JSON và hiển thị Message
    * Modifier: HaiVT 29/05/2013 - apply style moi
    **/
    alert: function (message, isShowDialogIframe) {
        //remove if exist popup
        if ($('#MsgProcessPopupNF').length > 0) {
            $('#MsgProcessPopupNF').remove();
        }

        if (isShowDialogIframe != true) {
            this.hideProcessingDialog();
            this.removeIndexIframe();
        }

        if (message == null || message == "") {
            return;
        }

        msg = popupUtils.GetJsonObject(message);
        if (msg == null) {
            msg = {};
            msg["Type"] = "success";
            msg["Message"] = message;
        }

        if (msg.Type == 'AJAX_SESSION_ERROR') {
            window.location.reload(true);
        }
        else {
            $('<div id="MsgProcessPopupNF" class="MsgSuccess"><div class="MsgLeftSuccess ' + msg.Type + '"><div class="MsgRightSuccess"><p>' + msg.Message + '<p></div></div></div>').appendTo("body");
            if (isHasChildIframeEDU == false) {
                $('#MsgProcessPopupNF').addClass('MsgSuccessIndex');
            }
            $("div#MsgProcessPopupNF").fadeIn(100);
            setTimeout(function () { popupUtils.fadeOutMessage(); }, 5000);
        }
    },

    //parse string to object
    GetJsonObject: function (variable) {
        try {
            if ($.isPlainObject(variable)) {
                return variable;
            } else {
                jsonObj = eval('(' + variable + ')');
                return jsonObj;
            }
        } catch (e) {
            return null;
        }
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi
    hideProcessingDialog: function (responseContext) {
        if (responseContext != null) {
            if (responseContext.status == 451) {
                popupUtils.redirect(webRoot + 'SSOServices.aspx?LogOff=true');
            }
            if (responseContext.status == 453) {
                popupUtils.alert({ Type: "error", Message: "Access denied" });
            }
            if (responseContext.status == 454) {
                popupUtils.redirect(webRoot + 'SSOServices.aspx?LogOff=true');
            }
            if (responseContext.status == 450) {
                popupUtils.alert(responseContext.responseText);
            }
        }
        $('#processingDialogLayout').hide();
        popupUtils.removeLoadingBGStyle();
        if (isHasChildIframeEDU == false) {
            $('#processingDialogLayout').removeClass('LoadingSectionIndex');
        }
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi
    showProcessingDialog: function () {
        popupUtils.removeIndexIframe();
        $('#processingDialogLayout').show();
        popupUtils.addLoadingBGStyle();
        if (isHasChildIframeEDU == false) {
            $('#processingDialogLayout').addClass('LoadingSectionIndex');
        }
    },

    redirect: function (url) {
        window.location = url;
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi 
    fadeOutMessage: function () {
        $("div#MsgProcessPopupNF").fadeOut("slow", function () {
            $("div#MsgProcessPopupNF").remove();
        });
    }
}