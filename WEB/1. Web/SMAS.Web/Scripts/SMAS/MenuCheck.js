﻿function accessClick(cbid) {
}

function viewClick(cbid) {
}

function addClick(cbid) {
    var addCheckBox = "#" + cbid;  //id selector of this checkbox
    //
    var index = cbid.toString().indexOf("_"); //vi tri dau _ trong id cua checkbox -> id co dang add_-2-4
    var subID = cbid.toString().substring(index); // subID bao gom menuPath + menu ID
    var viewID = "#view" + subID; //lay id cua view checkbox
    //
    
    var classNameOfSubMenus = getClassNameOfSubMenu(cbid); // lay ten class cua tat ca cac checkbox cua submenu
    //
    if ($(addCheckBox).is(':checked')) {
        $(viewID).attr('checked', true);
        $(classNameOfSubMenus).attr('checked', true);
        var subMenusArrayLevel1 = $(classNameOfSubMenus);
        if (subMenusArrayLevel1.length > 0) {
            for (i = 0; i < subMenusArrayLevel1.length; i++) {
                subMenuCheckBoxId = subMenusArrayLevel1[i].id;
                var classNameOfSubMenusLevel2 = getClassNameOfSubMenu(subMenuCheckBoxId);
                var subMenusArrayLevel2 = $(classNameOfSubMenusLevel2);
                //filter checkbox with name = addPermission
                subMenusArrayLevel2 = $.grep(subMenusArrayLevel2, function (element, index) {
                    //console.log(value);
                    return (element.name == "addPermission");
                });

                //$(classNameOfSubMenusLevel2).attr('checked', true);
                for (j = 0; j < subMenusArrayLevel2.length; j++) {
                    $("#" + subMenusArrayLevel2[j].id).attr('checked', true);                    
                }
            }
        }
        
    }
    else {
        $(viewID).attr('checked', false);
        $(classNameOfSubMenus).attr('checked', false);

    }
    var array = $(classNameOfSubMenus);
    for (i = 0; i < array.length; i++) {
        //console.log(array[i].id);
    }


}

function editClick(cbid) {
}

function deleteClick(cbid) {
}

//cac ham dung chung
function getClassNameOfSubMenu(checkBoxId) {
    var index2 = checkBoxId.toString().indexOf("-"); //lay vi tri phan tach giua prefix va suffix - suffix la id cua Menu chua checkbox
    var classNameOfSubMenus = ".c" + checkBoxId.toString().substring(index2); // lay ten class cua tat ca cac checkbox cua submenu
    return classNameOfSubMenus;
}