﻿/**
* Author: AuNH
* Cac ham javascript duoc chuan hoa dung chung trong toan he thong SMAS
**/
var isChange = false;
var isCheckedChange = false;
var smas = {
    /**
    * Thuc hien mo dialog
    **/
    openDialog: function (dialogName, url, data, callback) {
        w = $("#" + dialogName).data("tWindow");
        if (w != undefined) {
            w.center().open();
            if (url != "") {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: data,
                    beforeSend: function () {
                        var strLoad = '<div class="loadingAjax"><img width="16" height="11" src="/Content/images/icon_loading.gif" alt="loading..." style="vertical-align:midle;"/></div>'
                        $("#" + dialogName + " .t-content").html(strLoad);
                    },
                    success: function (data) {
                        $("#" + dialogName + " .t-content").html(data);
                        formUtils.focusForm($("#" + dialogName + " form").attr("id"));
                        validateUtils.fixCommonValidate();
                        //$("#" + dialogName + " div.t-window-content").css("height", "auto");
                        if(callback != undefined) { callback(); }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            } else {
                //$("#" + dialogName + " div.t-window-content").css("height", "auto");
            }
            formUtils.focusForm($("#" + dialogName + " form").attr("id"));

            //fix loi import het session
            var object = $("#btImport");
            if (object.length > 0) {
                if ($("#btImport").attr("onclick").toString().indexOf(";smas.checkSessionAjax();") == -1) {
                    $("#btImport").attr("onclick", "smas.checkSessionAjax();" + $("#btImport").attr("onclick"));
                }
            }
        }
    },

    /**
    * Thuc hien dong dialog
    **/
    closeDialog: function (dialogName) {
        w = $("#" + dialogName).data("tWindow");
        w.close();
    },
    /**
    * Ham duoc thuc thi khi dialog duoc mo
    **/
    onOpenDialog: function (e) {

    },

    /**
    *
    **/
    onCloseDialog: function (e) {
        $(this).data("tWindow").content(" ");
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi
    showProcessingDialog: function () {
        $('#processingDialogLayoutSMAS').fadeIn(200);
        $('#OverLayLayout').hide();
        $('#OverLayLayout').addClass('DlgOverlay');
        $('#OverLayLayout').fadeIn(400);
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi
    hideProcessingDialog: function (responseContext) {
        if (responseContext != null) {
            if (responseContext.status == 451) {
                smas.redirect("/Home/LogOn");
            }
            if (responseContext.status == 453) {
                smas.alert({ Type: "error", Message: "Access denied" });
            }
            if (responseContext.status == 454) {
                smas.redirect("/Home/LogOn");
            }
            if (responseContext.status == 450) {
                smas.alert(responseContext.responseText);
            }
        }
        $('#OverLayLayout').fadeOut(200, function () { $('#OverLayLayout').removeClass('DlgOverlay'); });
        $('#processingDialogLayoutSMAS').fadeOut(300);

    },

    /**
    * Hien thi cau thong bao
    * Nếu message là một chuỗi dạng JSON ("{Type: '....', Message: '...'}") thì parse JSON và hiển thị Message
    * Modifier: HaiVT 29/05/2013 - apply style moi
    **/
    alert: function (message) {
        //remove if exist popup
        if ($('#MsgProcessPopupNF').length > 0) {
            $('#MsgProcessPopupNF').remove();
        }

        smas.hideProcessingDialog();

        if (message == null || message == "") {
            return;
        }

        msg = jsonUtils.GetJsonObject(message);
        if (msg == null) {
            msg = {};
            msg["Type"] = "success";
            msg["Message"] = message;
        }

        $('<div id="MsgProcessPopupNF" class="MsgSuccess"><div class="MsgLeftSuccess ' + msg.Type + '"><div class="MsgRightSuccess"><p>' + msg.Message + '<p></div></div></div>').appendTo("body");
        $("div#MsgProcessPopupNF").fadeIn(100);
        setTimeout("smas.fadeOutMessage()", 5000);
    },
    alertColor: function (message, color) {
        //remove if exist popup
        if ($('#MsgProcessPopupNF').length > 0) {
            $('#MsgProcessPopupNF').remove();
        }

        smas.hideProcessingDialog();

        if (message == null || message == "") {
            return;
        }

        msg = jsonUtils.GetJsonObject(message);
        if (msg == null) {
            msg = {};
            msg["Type"] = "success";
            msg["Message"] = message;
        }

        $('<div id="MsgProcessPopupNF" class="MsgSuccess"><div class="MsgLeftSuccess ' + msg.Type + '"><div class="MsgRightSuccess"><p style="color: ' + color + '">' + message + '<p></div></div></div>').appendTo("body");
        $("div#MsgProcessPopupNF").fadeIn(100);
        setTimeout("smas.fadeOutMessage()", 5000);
    },
    //Namta - alert cho bieu mau import
    alertImport: function (e) {
        if (e.XMLHttpRequest!=null && e.XMLHttpRequest.response != null)
            $(".notVisible").html(e.XMLHttpRequest.response);
        var msg = $(".notVisible .ErrorLabel .er").html();
        smas.alert({ Type: "error", Message: msg });
        e.preventDefault();
    },
	
    //Namta - hiện thi thông báo OK.
    info: function (message) {
        $("#InfoAlertDialog .t-window-content").css("height", "auto");
        $("#InfoAlertDialog #Message").html(message);
        smas.openDialog("InfoAlertDialog", "", {});
    },

    //PhuongH1 - 14/05/2013
    error: function (message) {
        $("#ErrorAlertDialog .t-window-content").css("height", "auto");
        $("#ErrorAlertDialog #Message").html(message);
        smas.openDialog("ErrorAlertDialog", "", {});
    },

    //PhuongH1 - 14/05/2013
    confirm: function (message, func, func2) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            if (func2 != undefined) {
                func2();
            }
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },

    //Tamhm1 - 12/10/2013 - Phục vụ nghiệp vụ chuyển lớp
    confirmYesNo: function (message, func, func2) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").val("Có");
        $("#ConfirmAlertDialog #Cancel").val("Không");
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            if (func2 != undefined) {
                func2();
            }
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },

    //TUTV - 10/02/2015
    confirmProcess: function (message, func) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").val("Có");
        $("#ConfirmAlertDialog #Cancel").val("Không");
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");            
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },

    confirmDecalre: function (message, education, func) {
        $("#ConfirmAlertDialog_Declare .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog_Declare #Message").html(message).css("color","red");
        $("#ConfirmAlertDialog_Declare #Resolation").html(education);
        $("#ConfirmAlertDialog_Declare #OK").val("Áp dụng");
        $("#ConfirmAlertDialog_Declare #Cancel").val("Hủy");
        $("#ConfirmAlertDialog_Declare #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog_Declare");
            func();
        });
        $("#ConfirmAlertDialog_Declare #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog_Declare");            
        });
        smas.openDialog("ConfirmAlertDialog_Declare", "", {});
    },

    confirmFacilities: function (message, func) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").val("Có");
        $("#ConfirmAlertDialog #Cancel").val("Không");
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },
    confirmVN: function (message, func) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").val("Đồng ý");
        $("#ConfirmAlertDialog #Cancel").val("Hủy bỏ");
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },
    //QuangNN2 - 01/12/2014 - Phục vụ nghiệp vụ khai báo môn chuyên, môn tự chọn
    confirmOkCancel: function (message, func, func2) {
        $("#ConfirmAlertDialog .t-window-content").css("height", "auto");
        $("#ConfirmAlertDialog #Message").html(message);
        $("#ConfirmAlertDialog #OK").val("Đồng ý");
        $("#ConfirmAlertDialog #Cancel").val("Hủy");
        $("#ConfirmAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            func();
        });
        $("#ConfirmAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("ConfirmAlertDialog");
            if (func2 != undefined) {
                func2();
            }
        });
        smas.openDialog("ConfirmAlertDialog", "", {});
    },

    //PhuongH1 - 14/05/2013
    warning: function (message, func, func2) {
        $("#WarningAlertDialog .t-window-content").css("height", "auto");
        $("#WarningAlertDialog #Message").html(message);
        $("#WarningAlertDialog #OK").unbind("click").click(function () {
            smas.closeDialog("WarningAlertDialog");
            func();
        });
        $("#WarningAlertDialog #Cancel").unbind("click").click(function () {
            smas.closeDialog("WarningAlertDialog");
            if (func2 != undefined) {
                func2();
            }
        });
        smas.openDialog("WarningAlertDialog", "", {});
    },
    /**
    *
    **/
    reload: function () {
        location.reload();
    },

    redirect: function (url) {

        window.location = url;
    },

    //set cookie
    setCookie: function (c_name, value, exdays) {
        if (exdays == null) {
            exdays = 1;
        }
        if (value == null) {
            value = "";
        }

        var date = new Date();
        date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = c_name + "=" + value + expires;
    },

    //get cookie
    getCookie: function (c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
    },
    getParameterByName: function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    /**
    * Hien thi cau thong bao co trong ModelState khi Controller tra ve
    **/
    showJsonErrMessage: function (jsonErrList) {
        var lst = eval(jsonErrList);
        if (lst == null || lst == undefined)
            return;
        var strErrMsg = "";
        for (i = 0; i < lst.length; i++) {
            strErrMsg += lst[i] + "\n";
        }
        if (strErrMsg != "") {
            smas.alert(strErrMsg);
        }
    },


    /**
    * Tu dong focus vao truong dau tien cua form ma co the edit
    **/
    autoFocus: function () {
        $("form :input:visible:enabled:first").focus();
        if (document.forms != null && document.forms.length > 0) {
            el = document.forms[0].elements;
            for (i = 0; i < el.length; i++) {
                if (el[i].type != "hidden" &&
                    el[i].type != "image" &&
                    el[i].type != "button" &&
                    el[i].name != "" &&
                    el[i].name.indexOf("PageSize") == -1 &&
                    el[i].style.visibility != "hidden" &&
                    el[i].style.display != 'none' &&
                    el[i].readOnly == false &&
                    el[i].disabled == false) {
                    el[i].focus();
                    return;
                }
            }
        }
    },

    remove: function (url, data, callback) {
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function (data) {
                smas.alert(data);
                callback.call();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
            }
        })
    },

    bindTo: function (data, containerId) {
        $.each(data, function (key, item) {
            var value = data[key];
            if ($.type(value) == 'string') {
                value = $.trim(value);

                var dateCheck = "/Date("
                var pos = value.indexOf(dateCheck);
                if (pos != -1) {
                    try {
                        var miliSeconds = parseInt(value.substring(dateCheck.length, value.length - 2));
                        var date = new Date(miliSeconds);
                        var year = date.getFullYear();
                        var month = date.getMonth() + 1;
                        month = month.toString();
                        if (month.length == 1) {
                            month = "0" + month;
                        }
                        var day = date.getDate().toString();
                        if (day.length == 1) {
                            day = "0" + day;
                        }
                        var strDateTime = day + "/" + month + "/" + year;
                        value = strDateTime;
                    } catch (err) {
                        // ignored
                    }
                }
            }

            var item = $("#" + containerId).find('[name="' + key + '"]');

            if (item.attr("type") == "radio") {
                $.each(item, function (index, elm) {
                    if ($(elm).val() == value) $(elm).attr("checked", "checked");
                });
            }
            else {
                $("#" + containerId).find('[name="' + key + '"]').val(value);
            }
        });
    },

    //Modifier: HaiVT 29/05/2013 - apply style moi 
    fadeOutMessage: function () {
        $("div#MsgProcessPopupNF").fadeOut("slow", function () {
            $("div#MsgProcessPopupNF").remove();
        });
    },

    checkSessionAjax: function () {
        $.ajax({
            url: '/Home/CheckSessionForAjax',
            type: 'post',
            data: null,


            beforeSend: function () {
            },
            success: function (msg) {
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        })

    },

    post: function (url, data, successCallBack, failCallBack) {
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            complete: function () {
                smas.hideProcessingDialog();
            },
            success: function (data) {
                smas.alert(data);
                if (successCallBack != null)                  
                    successCallBack(data)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                if (failCallBack != null) {
                    failCallBack(XMLHttpRequest.responseText);
                }
            }
        })
    },

    getReport: function (formID, firstUrl, getNewUrl, downloadUrl) {
        $.ajax({
            url: firstUrl,
            type: 'post',
            dataType: 'json',
            data: $('#' + formID).serialize(),
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            complete: function () {
                smas.hideProcessingDialog();
            },
            success: function (data) {
                if (data.Type == "new") {
                    window.location=(downloadUrl + '?idProcessedReport=' + data.ReportID);
                } else if (data.Type == "error") {
                    smas.alert(data.Message);
                } else {
                    $('#MessageForExportDiv').html("Báo cáo này đã được tổng hợp từ ngày " + data.ProcessedDate
                                                + ". Bạn có thể tải file đã tổng hợp hoặc tổng hợp lại.");
                    //$('#DownloadExportLink').attr("href", downloadUrl + '?idProcessedReport=' + data.ReportID);
                    //transfer a href to button
                    $('#OldExportButton').unbind('click');
                    $('#OldExportButton').click(function (evt) {
                        window.location = (downloadUrl + '?idProcessedReport=' + data.ReportID);
                    });

                    $('#NewExportButton').unbind('click');
                    $('#NewExportButton').click(function (evt) {
                        evt.preventDefault();
                        $.ajax({
                            url: getNewUrl,
                            type: 'post',
                            dataType: 'json',
                            data: $('#' + formID).serialize(),
                            beforeSend: function () {
                                smas.showProcessingDialog();
                            },
                            complete: function () {
                                smas.hideProcessingDialog();
                            },
                            success: function (data) {                               
                                if (data.Type == "error") {
                                    smas.alert(data.Message);
                                }else{
                                    window.location = (downloadUrl + '?idProcessedReport=' + data.ReportID);
                                }
                                
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                smas.alert(XMLHttpRequest.responseText);
                                //if (failCallBack != null) {
                                //    failCallBack(XMLHttpRequest.responseText);
                                //}
                            }
                        });
                    });
                    smas.openDialog("ConfirmExportDialog", "", {});
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert(XMLHttpRequest.responseText);
                smas.alert(XMLHttpRequest.responseText);
                //if (failCallBack != null) {
                //    failCallBack(XMLHttpRequest.responseText);
                //}
                //$(".GridThemes").appendTo(XMLHttpRequest.responseText);

            }
        });
    },


    getReportDocument: function (formID, firstUrl, getNewUrl, downloadUrl, target) {
        $.ajax({
            url: firstUrl,
            type: 'post',
            dataType: 'json',
            data: $('#' + formID).serialize(),
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            complete: function () {
                smas.hideProcessingDialog();
            },
            success: function (data) {
                var actionUrl=downloadUrl + '?idProcessedReport=' + data.ReportID;
                if (data.Type == "new") {
                    smas.windowReport(actionUrl,target);
                } else if (data.Type == "error") {
                    smas.alert(data.Message);
                } else {
                    $('#MessageForExportDiv').html("Báo cáo này đã được tổng hợp từ ngày " + data.ProcessedDate
                                                + ". Bạn có thể tải file đã tổng hợp hoặc tổng hợp lại.");
                    //$('#DownloadExportLink').attr("href", downloadUrl + '?idProcessedReport=' + data.ReportID);
                    //transfer a href to button
                    $('#OldExportButton').unbind('click');
                    $('#OldExportButton').click(function (evt) {
                        smas.windowReport(actionUrl, target);
                    });

                    $('#NewExportButton').unbind('click');
                    $('#NewExportButton').click(function (evt) {
                        evt.preventDefault();
                        $.ajax({
                            url: getNewUrl,
                            type: 'post',
                            dataType: 'json',
                            data: $('#' + formID).serialize(),
                            beforeSend: function () {
                                smas.showProcessingDialog();
                            },
                            complete: function () {
                                smas.hideProcessingDialog();
                            },
                            success: function (data) {                               
                                if (data.Type == "error") {
                                    smas.alert(data.Message);
                                } else {
                                    actionUrl = downloadUrl + '?idProcessedReport=' + data.ReportID;
                                    smas.windowReport(actionUrl, target);
                                }
                                
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                smas.alert(XMLHttpRequest.responseText);
                                //if (failCallBack != null) {
                                //    failCallBack(XMLHttpRequest.responseText);
                                //}
                            }
                        });
                    });
                    smas.openDialog("ConfirmExportDialog", "", {});
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert(XMLHttpRequest.responseText);
                smas.alert(XMLHttpRequest.responseText);
                //if (failCallBack != null) {
                //    failCallBack(XMLHttpRequest.responseText);
                //}
                //$(".GridThemes").appendTo(XMLHttpRequest.responseText);

            }
        });
    },

    windowReport(url,target){
        if (target == '_blank') {

            //var winFeature ='location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
            //var winFeature="menubar = no, location = no, resizable = no, scrollbars = no, status = yes";
            var winFeature = "toolbar = no, location = no, directories = no, status = no, menubar = no, scrollbars = no, resizable = no, copyhistory = no";
            var win = window.open(url, target, winFeature);
            window.document.title = "SMAS-Xuất báo cáo file PDF  ";
        } else {
            window.location = url;
        }
    },

    readonlyDatePicker: function (datePickerID) {
        $(datePickerID).attr("readonly", "true");
        $(datePickerID).parent().find(".t-select").attr("style", "display:none");
    },

    disableDatePicker: function (datePickerID) {
        $(datePickerID).get(0).disabled = true;
        $(datePickerID).parent().find(".t-select").attr("style", "display:none");
    },

    enableDatePicker: function (datePickerID) {
        $(datePickerID).get(0).disabled = false;
        $(datePickerID).removeAttr("readonly");
        $(datePickerID).parent().find(".t-select").attr("style", "display:");
    },

    getReportWithData: function (dataInput, firstUrl, getNewUrl, downloadUrl) {
        $.ajax({
            url: firstUrl,
            type: 'post',
            dataType: 'json',
            data: dataInput,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            complete: function () {
                smas.hideProcessingDialog();
            },
            success: function (data) {
                if (data.Type == "new") {
                    window.open(downloadUrl + '?idProcessedReport=' + data.ReportID);
                } else {
                    console.log(1);
                    $('#MessageForExportDiv').html("Báo cáo này đã được tổng hợp từ ngày " + data.ProcessedDate
                                                + ". Bạn có thể tải file đã tổng hợp hoặc tổng hợp lại.");
                    $('#DownloadExportLink').attr("href", downloadUrl + '?idProcessedReport=' + data.ReportID);
                    $('#NewExportButton').unbind('click');
                    $('#NewExportButton').click(function (evt) {
                        evt.preventDefault();
                        $.ajax({
                            url: getNewUrl,
                            type: 'post',
                            dataType: 'json',
                            data: dataInput,
                            beforeSend: function () {
                                smas.showProcessingDialog();
                            },
                            complete: function () {
                                smas.hideProcessingDialog();
                            },
                            success: function (data) {
                                window.open(downloadUrl + '?idProcessedReport=' + data.ReportID);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                smas.alert(XMLHttpRequest.responseText);
                                if (failCallBack != null) {
                                    failCallBack(XMLHttpRequest.responseText);
                                }
                            }
                        });
                    });
                    console.log(2);
                    smas.openDialog("ConfirmExportDialog", "", {});
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                if (failCallBack != null) {
                    failCallBack(XMLHttpRequest.responseText);
                }
            }
        });
    },

    //export so diem nx cap 1namta
    getReportNotShow: function (dataInput, firstUrl, getNewUrl, downloadUrl) {
        $.ajax({
            url: firstUrl,
            type: 'post',
            dataType: 'json',
            data: dataInput,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            complete: function () {
                smas.hideProcessingDialog();
            },
            success: function (data) {
                window.location = downloadUrl + '?idProcessedReport=' + data.ReportID;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                if (failCallBack != null) {
                    failCallBack(XMLHttpRequest.responseText);
                }
            }
        });
    },

    setFocus: function (objControl) {
        setTimeout(function () { $(objControl).focus(); }, 500);
    },

    setSelect: function (objControl, value) {
        $(objControl).find("option").each(function () {
            $(this).removeAttr("selected");
        });

        var option = $(objControl).find("option[value=" + value.toString() + "]");
        if ($(option) != null) {
            $(option).attr("selected", "");
        }
    },

    setFocusDelay: function (objControl, delay) {
        setTimeout(function () { $(objControl).focus(); }, delay);
    },

    reloadPageTimeout: function (timeOut) {
        var t = 5000;
        if (timeOut != undefined && timeOut != null) {
            t = timeOut;
        }
        setTimeout("smas.reload()", 5000);
    },

    convertToLabel: function (panelName) {
        $(panelName + " input[type=text]:disabled").each(function () {
            var value = $(this).val();
            $(this).parent().html("<span class='LabelContent'>" + value + "</span>");
        });
    },

    //Fix height grid Chrome + Namta
    autoHeightTable: function (gridName) {
        var heightMin = 33;
        $(gridName + " .t-grid-content tr td table").each(function () {
            var parentNode = $(this).parent();
            if ($(parentNode) != null && $(parentNode).height() > 33) {
                
                //Height child table = height parent
                $(this).css("height", $(parentNode).height());
                var ChildTable = $(this).find("table");
                if (ChildTable != null) {
                    $(ChildTable).css("height", $(parentNode).height());
                }
            }
        });
    },

    checkStatusThread: function (threadName, time, successCallback) {

        if (threadName != null && threadName != "") {
            $.ajax({
                url: "/SummedUpRecordByEducationLevelArea/SummedUpRecordByEducationLevel/CheckStatusThread",
                type: 'post',
                dataType: 'json',
                data: { ThreadName: threadName },
                success: function (data) {
                    if (data == 0) {
                        localStorage["requestAlert"] = threadName;
                        setTimeout(function () { smas.checkStatusThread(threadName, time, successCallback) }, time);
                    } else {
                        localStorage["requestAlert"] = "";
                        //get direct
                        var path = location.pathname;
                        var SummedUpRecordUrl = ("SummedUpEducationLevelNewArea").toString().toLocaleLowerCase();
                        if (path.toString().toLowerCase().indexOf(SummedUpRecordUrl) != -1) {
                            if (threadName.toString().toLowerCase().indexOf("thread_summedup") != -1) {
                                smas.info("Tổng kết điểm học sinh cho khối thành công");
                            } else if (threadName.toString().toLowerCase().indexOf("thread_pupilranking") != -1) {
                                smas.info("Xếp loại học sinh cho khối thành công");
                            }
                            if (successCallback != undefined && successCallback != null) successCallback();
                        } else {
                            if (threadName.toString().toLowerCase().indexOf("thread_summedup") != -1) {
                                smas.info("Tổng kết điểm học sinh cho khối thành công");
                            } else if (threadName.toString().toLowerCase().indexOf("thread_pupilranking") != -1) {
                                smas.info("Xếp loại học sinh cho khối thành công");
                            }
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }

    },
    //namta + ham tu can chinh align nhung label nhay dong
    autoAlign: function () {
        var heightMin = 20;

        $(".vt-control .editor-label").each(function () {
            if ($(this).height() > heightMin) {
                $(this).addClass("editor-label-2");
            }
        });

    },

    checkBrowser:function(){
        if ($.browser.msie !=null && $.browser.msie)
        {
            return false;
        }
        if ($.browser.mozilla && $.browser.version < 22) {
            return false;
        }
        
        if ($.browser.opera || $.browser.safari) {
            return false;
        }
        return true;
    },

    checkPermissionTeacher: function (permission, clView, clCreate, clEdit, clDelete) {


        if (clDelete == "" || clDelete == null) {
            clDelete = "t-grid-vtDelete";
        }
        if (clEdit == "" || clEdit == null) {
            clEdit = "t-grid-vtEdit";
        }
        if (clCreate == "" || clCreate == null) {
            clCreate = "t-grid-vtAdd";
        }
        if (clView == "" || clView == null) {
            clView = "t-grid-vtDetails";
        }
        if (permission < 1) {
            //An button View
            if ($("." + clView).length > 0) {
                $("." + clView).css("display", "none");
            }
        }
        if (permission < 2) {
            //An button Create
            if ($("." + clCreate).length > 0) {
                $("." + clCreate).css("display", "none");
            }
        }
        if (permission < 3) {
            //An button Edit
            if ($("." + clEdit).length > 0) {
                $("." + clEdit).css("display", "none");
            }
        }
        if (permission < 4) {

            //An button delete
            if ($("." + clDelete).length > 0) {
                $("." + clDelete).css("display", "none");
            }
        }




    },
	
    // Ham xu ly ngoai le cua mot ajax request
    handleAjaxException: function (ajaxContext) {
        console.log(ajaxContext);
        var responseText = ajaxContext.responseText;
        var responseJson = JSON.parse(responseText);
        smas.alert(responseJson.Message);
    },
   CheckPhoneNumber: function (phone) {
        var vtnumber = false;
        var headerPhone = '';
        var phonearray = '';
        var i = 0;

        var regex = /^([0198]{1,2})(\d{8,11})$/;
        if (phone.match(regex))
        {
            if (phone.length == 9) {
                var strPreNumberLen10 = ["96","97","98","86","91","94","88","90","93","89","92","99", "95"];

                if (phone != '' && phone.length == 9) {
                    headerPhone = phone.substr(0, 2);
                    filter = strPreNumberLen10.indexOf(headerPhone);
                    if (filter >= 0) {
                        vtnumber = true;
                    } else {
                        vtnumber = false;
                    }
                    return vtnumber;
                }
            } else if (phone.length == 10) {
                var strPreNumberLen10 = ["096", "097", "098", "086", "091", "094", "088", "090", "093", "089", "092", "099", "095", "169", "168", "167", "166", "165", "164", "163", "162", "123", "125", "127", "122", "126", "121", "128", "120", "124", "129", "188", "186", "199"];

                if (phone != '' && phone.length == 10) {
                    headerPhone = phone.substr(0, 3);
                    filter = strPreNumberLen10.indexOf(headerPhone);
                    if (filter >= 0) {
                        vtnumber = true;
                    } else {
                        vtnumber = false;
                    }
                    return vtnumber;
                }
            } else if (phone.length == 11) {
                var strPreNumberLen10 = ["0169", "0168", "0167", "0166", "0165", "0164", "0163", "0162", "0123", "0125", "0127", "0122", "0126", "0121", "0128", "0120", "0124", "0129", "8498", "8497", "8496", "8486", "8491", "8494", "8488", "8490", "8493", "8489", "8492", "8499", "8495", "0188", "0186", "0199"];

                if (phone != '' && phone.length == 11) {
                    headerPhone = phone.substr(0, 4);
                    filter = strPreNumberLen10.indexOf(headerPhone);
                    if (filter >= 0) {
                        vtnumber = true;
                    } else {
                        vtnumber = false;
                    }
                    return vtnumber;
                }
            } else if (phone.length == 12) {
                var strPreNumberLen10 = ["84169", "84168", "84167", "84166", "84165", "84164", "84163", "84162", "84123", "84125", "84127", "84122", "84126", "84121", "84128", "84120", "84124", "84129", "84188", "84186", "84199"];

                if (phone != '' && phone.length == 12) {
                    headerPhone = phone.substr(0, 5);
                    filter = strPreNumberLen10.indexOf(headerPhone);
                    if (filter >= 0) {
                        vtnumber = true;
                    } else {
                        vtnumber = false;
                    }
                    return vtnumber;
                }
            }
            else {
                return false;
            }
        } else {
            return false;
        }
        
    },

    FormatMoney: function (nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    },
    CheckPhoneVT: function (phone) {
        var vtnumber = false;
        var headerPhone = '';
        var phonearray = '';
        var i = 0;
        var filter = 0;
        if (phone.length == 9) {
            var strPreNumberLen10 = ["96", "97", "98", "86"];
            if (phone != '' && phone.length == 9) {
                headerPhone = phone.substr(0, 2);
                filter = strPreNumberLen10.indexOf(headerPhone);
                if (filter >= 0) {
                    vtnumber = true;
                } else {
                    vtnumber = false;
                }
                return vtnumber;
            }
        } else if (phone.length == 10) {
            var strPreNumberLen10 = ["096", "097", "098", "086", "162", "163", "164", "165", "166", "167", "168", "169"];
            if (phone != '' && phone.length == 10) {
                headerPhone = phone.substr(0, 3);              
                filter = strPreNumberLen10.indexOf(headerPhone);
                if (filter >= 0) {
                    vtnumber = true;
                } else {
                    vtnumber = false;
                }
                return vtnumber;
            }
        } else if (phone.length == 11) {
            var strPreNumberLen10 = ["8496", "8497", "8498", "8486", "0162", "0163", "0164", "0165", "0166", "0167", "0168", "0169"];
            if (phone != '' && phone.length == 11) {
                headerPhone = phone.substr(0, 4);              
                filter = strPreNumberLen10.indexOf(headerPhone);
                if (filter >= 0) {
                    vtnumber = true;
                } else {
                    vtnumber = false;
                }
                return vtnumber;
            }
        } else if (phone.length == 12) {
            var strPreNumberLen10 = ["84162", "84163", "84164", "84165", "84166", "84167", "84168", "84169"];
            if (phone != '' && phone.length == 12) {
                headerPhone = phone.substr(0, 5);
                filter = strPreNumberLen10.indexOf(headerPhone);
                if (filter >= 0) {
                    vtnumber = true;
                } else {
                    vtnumber = false;
                }
                return vtnumber;
            }
        } else {
            return false;
        }
    },
    confirmWithDialogName: function (dialogName, message, func) {
        $('#' + dialogName + ' #Message').html(message);
        $('#' + dialogName + ' #OK').unbind("click").click(function () {
            smas.closeDialog(dialogName);
            func();
        });
        smas.openDialog(dialogName, "", {});
    },

};

// Tu dong focus vao truong dau tien cos the edit khi load xong man hinh
window.onload = smas.autoFocus;

/*fix attt them token cho cac action ajax*/
$(document).ready(function () {
    //chuc nang tong ket
    var threadName = localStorage["requestAlert"];
    if (threadName != null && threadName != "") {
        smas.checkStatusThread(threadName, 2000);
    }
    securityToken = $('[name=__RequestVerificationToken]').val();
    $('body').bind('ajaxSend', function (elm, xhr, s) {
        if (s.type == 'POST' && typeof securityToken != 'undefined' && s.data != null && s.data != undefined) {
            if (s.data.indexOf("__RequestVerificationToken") == -1) {
                if (s.data.length > 0) {
                    s.data += "&__RequestVerificationToken=" + encodeURIComponent(securityToken);
                }
                else {
                    s.data = "__RequestVerificationToken=" + encodeURIComponent(securityToken);
                }
            }
        }
    });
});
//Ham can trinh lai margin cho text

$(document).ready(function () {
    setTimeout('smas.autoAlign();', 1500);

});

//Ham overide nut import


//Phuc vu cho SMS Edu
//LISTENER IFRAME MESSAGE FUNCTION TO RESIZE CONTEXT - IFRAME POST MESSAGE EVENT
$(document).ready(function () {
    //handle post event from child iframe
    if (!window.addEventListener) {
        window.attachEvent("onmessage", iFrameUtils.receiveMessageIframe);
    }
    else {
        window.addEventListener("message", iFrameUtils.receiveMessageIframe, false);
    }
});

iFrameUtils = {
    //receiver message from iframe
    receiveMessageIframe: function (evt) {
        if (evt.data) {
            var messageObject = jQuery.parseJSON(evt.data);
            switch (messageObject.code) {
                case 0:
                    {
                        //post value of height content -> resize height iframe
                        $('#SmsEduFrame').css('height', messageObject.value)
                        break;
                    }
                case 1:
                    {
                        //post from detail mailbox -> scroll to top
                        $(window).scrollTop($('#ScrollToDiv').offset().top - 100);
                        RefreshNotificationPartial();
                        break;
                    }
                case 2:
                    {
                        //post from contactGroup -> scroll to top
                        $(window).scrollTop($('#SmsEduFrame').offset().top - 100);
                        break;
                    }
                case 3:
                    {
                        //mark tag a popup confirm
                        iFrameUtils.markPopupConfirm();
                        break;
                    }
                case -3:
                    {
                        //unbind click event 
                        iFrameUtils.unbindConfirm();
                        break;
                    }
                case 4:
                    {
                        //redirect to page
                        window.location.href = messageObject.value;
                        break;
                    }

                case 51:
                    {
                        //post from smas.alert() -> call popupUtils.alert()
                        popupUtils.alert(messageObject.value, messageObject.isShowDialogIframe);
                        break;
                    }
                case 52:
                    {
                        //post from smas.showProcessingDialog() -> call popupUtils.showProcessingDialog()
                        popupUtils.showProcessingDialog(messageObject.value);
                        break;
                    }
                case 53:
                    {
                        //post from smas.hideProcessingDialog() -> call popupUtils.hideProcessingDialog()
                        popupUtils.hideProcessingDialog();
                        break;
                    }
                case 54:
                    {
                        //post from smas.openDialog() -> call popupUtils.openDialog()
                        popupUtils.openDialog();
                        break;
                    }
                case 55:
                    {
                        //post from smas.closeDialog() -> call popupUtils.closeDialog()
                        popupUtils.closeDialog();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    },

    //post message to child iframe
    //->MessageIFrame { code, value }   
    postSpecialCode: function (messageIframe) {
        var iframe = $('#SmsEduFrame')[0];
        iframe.contentWindow.postMessage(messageIframe, '*');
    },

    // mark popup confirm to tag a
    markPopupConfirm: function () {
        $('a').click(function (e) {
            var url = $(this).attr('href');
            // code = 3: request popup confirm import
            if (url && (url.indexOf('#') != 0 || url != "javascript:")) {
                iFrameUtils.postSpecialCode({ code: 3, value: url });
            }
            e.preventDefault();
        });
    },

    unbindConfirm: function () {
        $('a:not([href^="#"]):not([href^="javascript:"])').unbind("click");
    },

    checkAvailableEDU: function () {
        $.cookie('isSmsEduAvailable', '1');
    },

    
}




// LT
function ExFuncLT(vars) {
    ExFunc.call(this, "<", vars);
}

ExFuncLT.prototype = Object.create(ExFunc.prototype);
ExFuncLT.prototype.constructor = ExFuncLT;

ExFuncLT.prototype.calFunc = function () {
    var var0 = this.vars[0] instanceof ExFunc ? this.vars[0].calFunc() : eval(this.vars[0]);
    var var1 = this.vars[1] instanceof ExFunc ? this.vars[1].calFunc() : eval(this.vars[1]);
    if (var0 < var1) {
        this.errs.push(this.desc());
        return true;
    }
    return false;
}
// End LT

// ExFunc
function ExFunc(name, vars) {
    this.name = name;
    this.vars = vars;
    this.errs = [];
}

ExFunc.prototype.calFunc = function () {
    return true;
}

ExFunc.prototype.desc = function () {
    var strRes = "";
    for (var i = 0; i < this.vars.length; i++) {
        if (this.vars[i] instanceof ExFunc) {
            strRes += this.vars[i].desc() + ' ' + this.name + ' ';
        } else {
            strRes += this.vars[i] + ' ' + this.name + ' ';
        }
    }

    return strRes.substring(0, strRes.length - this.name.length - 2);
}
// End ExFunc
function ExFuncGT(vars) {
    ExFunc.call(this, ">", vars);
}

ExFuncGT.prototype = Object.create(ExFunc.prototype);
ExFuncGT.prototype.constructor = ExFuncGT;

ExFuncGT.prototype.calFunc = function () {
    var var0 = this.vars[0] instanceof ExFunc ? this.vars[0].calFunc() : eval(this.vars[0]);
    var var1 = this.vars[1] instanceof ExFunc ? this.vars[1].calFunc() : eval(this.vars[1]);
    if (var0 > var1) {
        this.errs.push(this.desc());
        return true;
    }
    return false;
}

function CompileEx(expression) {
    var stack = "";
    var _s = 0;
    var arr = [];
    if (expression.substring(0, 3) == "OR(") {
        _s = 3;
        for (var i = 3; i < expression.length; i++) {
            if ((expression[i] == ',' && stack == "") || i == expression.length - 1) {
                arr.push(CompileEx(expression.substring(_s, i)));
                _s = i + 1;
            }

            if (expression[i] == '(') {
                stack += "(";
            }
            if (expression[i] == ')') {
                stack = stack.substring(0, stack.length - 1);
            }
        }
        return new ExFuncOR(arr);
    }

    else if (expression.substring(0, 4) == "AND(") {
        _s = 4;
        for (var i = 4; i < expression.length; i++) {
            if ((expression[i] == ',' && stack == "") || i == expression.length - 1) {
                arr.push(CompileEx(expression.substring(_s, i)));
                _s = i + 1;
            }

            if (expression[i] == '(') {
                stack += "(";
            }
            if (expression[i] == ')') {
                stack = stack.substring(0, stack.length - 1);
            }
        }
        return new ExFuncAND(arr);
    }

    else if (expression.indexOf(">=") >= 0) {
        var varrs = expression.split(">=");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncGTE(arr);
    }

    else if (expression.indexOf("<=") >= 0) {
        var varrs = expression.split("<=");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncLTE(arr);
    }

    else if (expression.indexOf("<>") >= 0) {
        var varrs = expression.split("<>");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncNEQ(arr);
    }

    else if (expression.indexOf("!=") >= 0) {
        var varrs = expression.split("!=");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncNEQ(arr);
    }

    else if (expression.indexOf(">") >= 0) {
        var varrs = expression.split(">");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncGT(arr);
    }

    else if (expression.indexOf("<") >= 0) {
        var varrs = expression.split("<");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncLT(arr);
    }

    else if (expression.indexOf("=") >= 0) {
        var varrs = expression.split("=");
        for (var i = 0; i < varrs.length; i++) {
            arr.push(CompileEx(varrs[i]));
        }
        return new ExFuncEQ(arr);
    }
    else if (expression.substring(0, 4) == "SUM(") {
        return new ExFuncSUM([expression.substring(4, expression.length - 1)]);
    }
    else {
        return expression;
    }
}

function ExFuncAND(vars) {
    ExFunc.call(this, "AND", vars);
}

ExFuncAND.prototype = Object.create(ExFunc.prototype);
ExFuncAND.prototype.constructor = ExFuncAND;

ExFuncAND.prototype.calFunc = function () {
    for (var i = 0; i < this.vars.length; i++) {
        if (this.vars[i] instanceof ExFunc) {
            var res = this.vars[i].calFunc();
            if (res == false) {
                return false;
            }
        } else {
            throw "Syntax Error";
        }
    }
    for (var i = 0; i < this.vars.length; i++) {
        for (var j = 0; j < this.vars[i].errs.length; j++) {
            this.errs.push(this.vars[i].errs[j]);
        }
    }

    return this.errs.length != 0;
}

AddAntiForgeryToken = function (dataform) {
    dataform.__RequestVerificationToken = $('[name=__RequestVerificationToken]').val();
    return dataform;
}