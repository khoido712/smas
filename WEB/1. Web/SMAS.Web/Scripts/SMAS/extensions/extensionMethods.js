﻿//HaiVT 26/04/2013 - extension function of string value - replace any char of string
//"x".replaceAll("x", "xyz"); =xyz
//"x".replaceAll("", "xyz"); =xyzxxyz
//"aA".replaceAll("a", "b", true); =bb
//"Hello???".replaceAll("?", "!"); =Hello!!!
String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
};
