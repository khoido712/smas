jQuery(document).ready(function () {
   

	/*$('.MySelectBoxClass').customStyle();*/
	$( ".ParentMenu" ).tabs({
		cookie: {
			expires: 100
		}
	});
	//getter
	var selected = $( ".ParentMenu" ).tabs( "option", "selected" );
	//setter
	$( ".ParentMenu" ).tabs( "option", "selected", 0 ); //.tabs( "remove" , index )
	$("#colapseFunc").click(function(){
		var cls = $(this).attr("class");
		if($("#colapseFunc").hasClass("Sclose"))
		{
			$(this).removeClass("Sclose");
			$(this).addClass("Eclose");
			$("[id*=tabs]").slideUp("slow");
			$(".TabChildMenu").hide("slow");
		}
		else
		{
			$(this).removeClass("Eclose");
			$(this).addClass("Sclose");
			$("[id*=tabs]").slideDown("slow");
			$(".TabChildMenu").show("slow");
		}
	})
	$(".LinkTab").click(function() {
		var cls = $("#colapseFunc").attr("class");
		$(".BreadcrumFunc").removeClass("NoBorder");
		if ($(".TabChildMenu").is(":visible")){
			if($("#colapseFunc").hasClass("Eclose")){
				$("#colapseFunc").removeClass("Eclose");
				$("#colapseFunc").addClass("Sclose");
				$(".TabChildMenu").show("slow");
			}
		}
		
	})
	$("#linkItem0").click(function(){
		window.location.href='home.html';
	})
	$('<div class="tl"></div><div class="tr"></div><div class="bl"></div><div class="br"></div>').appendTo(".GroupMenuLV3");
	
	/*MenuLV3Section hover*/
	jQuery('.MenuLV3Section').mouseover(function() {
	var itemId =jQuery(this).attr('id');
    var aCurrentHoverId = jQuery('#'+itemId+ ' a').attr('id');
	jQuery('#'+itemId).addClass('level2_current');
	jQuery('#'+aCurrentHoverId).addClass('level2_current');
    });
	jQuery('.MenuLV3Section').mouseleave(function() {
	var itemId =jQuery(this).attr('id');
    var aCurrentHoverId = jQuery('#'+itemId+ ' a').attr('id');
	jQuery('#'+itemId).removeClass('level2_current');
	jQuery('#'+aCurrentHoverId).removeClass('level2_current');
    });
});
