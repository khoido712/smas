﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Diagnostics;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Web;
using SMAS.Web.Constants;
using SMAS.Web.Utils;

namespace SMAS.Web.Filter
{
    public class ActionAuditAttribute : FilterAttribute, IActionFilter
    {
        public string UserActionID { get; set; }
        private Stopwatch timer;
        /*SMASEntities context;
        IActionAuditRepository auditRepo;*/
        ActionAudit actionAudit;
        string sessionID;
        public ActionAuditAttribute()
        {
            timer = new Stopwatch();
        }
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            StringBuilder sbParameters = new StringBuilder();
            timer = Stopwatch.StartNew();
            if (filterContext.ActionParameters != null)
            {
                foreach (KeyValuePair<string, object> parameter in filterContext.ActionParameters)
                {
                    if (parameter.Key.Contains("id") && parameter.Value != null) sbParameters.Append(parameter.Value.ToString());
                }
            }
            sessionID = filterContext.HttpContext.Session.SessionID;
            /*if (context == null) context = new SMASEntities();
            auditRepo = new ActionAuditRepository(context);*/

            UserAccount loggedUser = filterContext.HttpContext.Session[GlobalConstants.USERACCOUNT] as UserAccount;
            if (loggedUser == null) return;
            actionAudit = new ActionAudit();
            actionAudit.UserID = loggedUser.UserAccountID;
            actionAudit.Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            actionAudit.Action = filterContext.ActionDescriptor.ActionName;
            int length = sbParameters.Length;
            if (sbParameters.Length > 100) length = 100;
            actionAudit.Parameter = sbParameters.ToString().Substring(0, length);
            actionAudit.BeginAuditTime = DateTime.Now;

            actionAudit.IP = filterContext.HttpContext.Request.UserHostAddress;
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.HttpContext.Session.SessionID != sessionID) return;
            if (actionAudit == null) return;
            timer.Stop();
            long xRuntime = timer.ElapsedMilliseconds;
            HttpResponseBase response = filterContext.HttpContext.Response;
            //response.AppendHeader("X-Duration", string.Format("{0}ms", timer.ElapsedMilliseconds));
            actionAudit.ExecutedDuration = xRuntime;
            actionAudit.EndAuditTime = DateTime.Now;
            string oldObjectValues = string.Empty;
            string newObjectValues = string.Empty;
            string ObjectIds = string.Empty;
            string Descriptions = string.Empty;
            string Parameters = string.Empty;
            string userFunctions = string.Empty;
            string userActions = string.Empty;
            string userDescriptions = string.Empty;
            int StatusCode = filterContext.HttpContext.Response.StatusCode;
            int schoolID = filterContext.HttpContext.Session[GlobalConstants.SCHOOLID] != null ? (int)filterContext.HttpContext.Session[GlobalConstants.SCHOOLID] : 0;
            int lastDigitNumberSchool = schoolID % 100;
            string userName = filterContext.HttpContext.Session[GlobalConstants.USERNAME].ToString() ?? "";
            //timer.Reset();
            #region Insert ActionAudit
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.OldJsonObject] != null)
            {
                oldObjectValues = filterContext.Controller.ViewData[CommonKey.AuditActionKey.OldJsonObject].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.NewJsonObject] != null)
            {
                newObjectValues = filterContext.Controller.ViewData[CommonKey.AuditActionKey.NewJsonObject].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.ObjectID] != null)
            {
                ObjectIds = filterContext.Controller.ViewData[CommonKey.AuditActionKey.ObjectID].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.Description] != null)
            {
                Descriptions = filterContext.Controller.ViewData[CommonKey.AuditActionKey.Description].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.Parameter] != null)
            {
                Parameters = filterContext.Controller.ViewData[CommonKey.AuditActionKey.Parameter].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.userFunction] != null)
            {
                userFunctions = filterContext.Controller.ViewData[CommonKey.AuditActionKey.userFunction].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.userAction] != null)
            {
                userActions = filterContext.Controller.ViewData[CommonKey.AuditActionKey.userAction].ToString();
            }
            if (filterContext.Controller.ViewData[CommonKey.AuditActionKey.userDescription] != null)
            {
                userDescriptions = filterContext.Controller.ViewData[CommonKey.AuditActionKey.userDescription].ToString();
            }

            System.Net.IPAddress[] a = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            if (a != null && a.Count() > 0)
            {

                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {

                        actionAudit.Referer = a[i].ToString();
                        break;
                    }
                }
            }

            actionAudit.IP = filterContext.HttpContext.Request.UserHostAddress;
            actionAudit.LastDigitNumberSchool = lastDigitNumberSchool;

            string[] listoldObjectValues = oldObjectValues.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            string[] listnewObjectValues = newObjectValues.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            string[] listObjectIds = ObjectIds.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            string[] listDescriptions = Descriptions.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            string[] listParameters = new string[] { };
            if (!String.IsNullOrEmpty(Parameters))
            {
                listParameters = Parameters.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            }
            string[] listUserFunctions = new string[] { };
            if (!String.IsNullOrEmpty(userFunctions))
            {
                listUserFunctions = userFunctions.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            }
            string[] listUserActions = new string[] { };
            if (!String.IsNullOrEmpty(userActions))
            {
                listUserActions = userActions.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            }
            string[] listUserDescriptions = new string[] { };
            if (!String.IsNullOrEmpty(userDescriptions))
            {
                listUserDescriptions = userDescriptions.Split(new string[] { GlobalConstants.WILD_LOG }, StringSplitOptions.RemoveEmptyEntries);
            }

            //Chiendd1: bo sung trang thai loi
            string exception = "";
            int statusRequest = 1;
            if (filterContext.Exception != null || StatusCode != 200)
            {
                statusRequest = 0;
                exception = filterContext.Exception.Message;
            }
            actionAudit.StatusRequest = statusRequest;
            String url = String.Format("{0}/{1}", actionAudit.Controller + "Area", actionAudit.Controller);
            if (String.IsNullOrEmpty(userFunctions))
            {
                string strtext = string.Empty;

                Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();
                if (objMenu != null)
                {
                    strtext = Res.GetMenuName(objMenu.MenuName);
                    actionAudit.userFunction = strtext;
                }

            }

            KPILog kpi = new KPILog
            {
                Account = "SMAS",
                ActionName = url,
                ApplicationCode = "",
                Duration = (int)xRuntime,
                EndTime = DateTime.Now,
                ErrorCode = StatusCode,
                ErrorDescription = exception,
                IPPortCurrentNode = actionAudit.Referer,
                IPPortParentNode = actionAudit.IP,
                RequestContent = string.IsNullOrEmpty(actionAudit.oldObjectValue) ? "" : actionAudit.Parameter.Substring(0, 200),
                ResponseContent = string.IsNullOrEmpty(actionAudit.userDescription) ? "" : actionAudit.Description.Substring(0, 200),
                ServiceCode = actionAudit.Action,
                SessionID = sessionID,
                StartTime = DateTime.Now.AddMilliseconds(-xRuntime),
                TransactionStatus = statusRequest,
                UserName = userName

            };
            SMAS.Business.Common.UtilsBusiness.KPILog(kpi);

            //Trường hợp không cung cấp dữ liệu thay đỗi.
            if (Descriptions == string.Empty || listDescriptions.Length == 0)
            {
                if (!string.IsNullOrEmpty(actionAudit.userFunction) && this.UserActionID != null)
                {
                    actionAudit.userAction = UserActionID;
                    LogAdapter.Insert(actionAudit);
                }
            }
            else
            {
                //Cập nhật từng dữ liệu thay đổi (như lưu hồ sơ học sinh)
                for (int i = 0; i < listDescriptions.Length; i++)
                {
                    int objectID = 0;
                    if (listObjectIds.Length == listDescriptions.Length)
                    {
                        int.TryParse(listObjectIds[i].ToString(), out objectID);
                    }
                    ActionAudit acAu = new ActionAudit();
                    acAu = actionAudit;
                    if (listParameters.Length == listDescriptions.Length)// do param này mới thêm vào nên kiểm tra kiểu này cho chắc chắn ko bị eception
                    {
                        acAu.Parameter = listParameters[i].ToString();
                    }
                    if (listoldObjectValues.Length == listDescriptions.Length)
                    {
                        acAu.oldObjectValue = listoldObjectValues[i].ToString();
                    }
                    if (listnewObjectValues.Length == listDescriptions.Length)
                    {
                        acAu.newObjectValue = listnewObjectValues[i].ToString();
                    }
                    if (listUserFunctions.Length == listDescriptions.Length)
                    {
                        acAu.userFunction = listUserFunctions[i].ToString();
                    }
                    if (listUserActions.Length == listDescriptions.Length)
                    {
                        acAu.userAction = listUserActions[i].ToString();
                    }
                    if (listUserDescriptions.Length == listDescriptions.Length)
                    {
                        acAu.userDescription = listUserDescriptions[i].ToString();
                    }
                    acAu.ObjectId = objectID;
                    acAu.Description = listDescriptions[i].ToString();
                    acAu.ExecutedDuration = xRuntime;
                    acAu.StatusRequest = statusRequest;
                    LogAdapter.Insert(acAu);
                    /*auditRepo.Insert(acAu);
                    auditRepo.Save();*/
                }
            }

            #endregion
        }
    }
}
