﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Constants;
using System.Web.Security;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Models.Models.CustomModels;
using System.Diagnostics;
namespace SMAS.Web.Filter
{
    public class ActionCheckDataFilter : ActionFilterAttribute
    {

        //SMASEntities context;
        GlobalInfo glo = new GlobalInfo();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
          || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
            //neu action khong ap dung AllowAnonymousAttribute thi bat buoc phai dang nhap
            if (!skipAuthorization && filterContext.HttpContext.Session[GlobalConstants.MENUS] == null)
            {
                filterContext.Result = new JsonResult();
                return;
            }
            //Hungnd Check Logout return view
            var logout = filterContext.HttpContext.Session["Logout"];
            if (logout != null)
            {
                if ((bool)logout == true)
                {
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Home", action = "Logoff" }));
                }
                base.OnActionExecuting(filterContext);
                //End check logout
            }
            else
            {
                // QuangLM Edit 03/04/2013                 
                // Check thong ton AcademicYear, neu khong co thi chuyen ve trang quan ly thiet lap thoi gian
                string controller = filterContext.RouteData.Values["controller"].ToString();
                string action = filterContext.RouteData.Values["action"].ToString();
                // if (context == null) context = new SMASEntities();
                //MenuRepository = new MenuRepository(context);
                //lay thong tin functionPath
                if (filterContext.HttpContext.Session[GlobalConstants.MENUS] != null)
                {
                    if (!filterContext.HttpContext.Request.IsAjaxRequest()
                        && !filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipFunctionPathAttribute), false).Any())
                    {
                        List<Menu> menuByRole = (List<Menu>)SMAS.Business.Common.StaticData.ListMenu;

                        if (menuByRole != null && menuByRole.Count > 0)
                        {
                            // Neu có action
                            var ListMenu = menuByRole.Where(o => o.URL.ToLower().Contains(("Area/" + controller + "/" + action).ToString().ToLower())).Where(u => u.IsActive == true);
                            if (ListMenu == null || ListMenu.Count() == 0)
                            {
                                // AnhVD 20140812 - Lay thông tin functionPath nếu chưa biết action
                                ListMenu = menuByRole.Where(o => o.URL.ToLower().Contains(("Area/" + controller + "/").ToString().ToLower())).Where(u => u.IsActive == true);
                            }

                            string FunctionPath = string.Empty;
                            if (ListMenu != null && ListMenu.Count() > 1)
                                ListMenu = ListMenu.Where(u => u.IsVisible);
                            if (ListMenu != null && ListMenu.Count() > 0)
                            {
                                var Menu = ListMenu.FirstOrDefault();
                                int levelMenu = 0;
                                while (Menu != null)
                                {
                                    string ResourceMenu = "";
                                    if ((string)HttpContext.GetGlobalResourceObject("MenuResources", Menu.MenuName) == null)
                                    {
                                        ResourceMenu = Menu.MenuName;
                                    }
                                    else
                                    {
                                        ResourceMenu = (string)HttpContext.GetGlobalResourceObject("MenuResources", Menu.MenuName);
                                    }
                                    string temp = ResourceMenu + (levelMenu != 0 ? " >> " : "") + FunctionPath;
                                    FunctionPath = temp;
                                    Menu = SMAS.Business.Common.StaticData.ListMenu.Find(x => x.MenuID == Menu.ParentID);
                                    if (Menu != null)
                                    {
                                        levelMenu++;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            filterContext.HttpContext.Session[GlobalConstants.FUNCTION_PATH] = FunctionPath;
                        }
                    }
                }
                if (!controller.Equals("Home") && !controller.Equals("AcademicYear"))
                {
                    // Kiem tra neu la user co role cap truong moi thu hien
                    object schoolRole = filterContext.HttpContext.Session[GlobalConstants.IS_SCHOOL_ROLE];
                    if (schoolRole != null && ((bool)schoolRole))
                    {
                        object academicYearID = filterContext.HttpContext.Session[GlobalConstants.ACADEMICYEAR_ID];

                        if (academicYearID == null && !filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            // Neu co quyen chuyen ve trang quan ly thoi gian nam hoc
                            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "AcademicYearArea", controller = "AcademicYear", action = "Index" }));
                        }
                    }
                }
            }
            base.OnActionExecuting(filterContext);
            // QuangLM - End
        }

    }
}