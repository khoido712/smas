﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SMAS.Web.Filter
{
    
    public class HandleLogErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            filterContext.HttpContext.Response.StatusCode = 406;
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result = new JsonResult
                {
                    Data = filterContext.Exception.Message
                };
            }
            else
            {
                //filterContext.HttpContext.Response.StatusCode = 406; // Or any other proper status code.
                //
                //filterContext.HttpContext.Response.Write(filterContext.Exception.Message);
            }
            
        }
    }
}
