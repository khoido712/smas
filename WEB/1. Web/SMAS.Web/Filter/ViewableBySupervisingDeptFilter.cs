﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using System.Configuration;
using SMAS.Web.Areas.SchoolProfileArea;

namespace SMAS.Web.Filter
{
    public class ViewableBySupervisingDeptFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            if (globalInfo.IsSuperVisingDeptRole || globalInfo.IsSubSuperVisingDeptRole)
            {
                backupGlobalInfo(globalInfo);
                SchoolProfileDetailViewedBySupervisingDept schoolInfo = SchoolProfileDetailViewedBySupervisingDept.getGlobalInstanceFromSession();
                makeFakeGlobalInfo(globalInfo, schoolInfo);
            }
            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

            GlobalInfo globalInfo = GlobalInfo.getInstance();
            if (globalInfo.IsSuperVisingDeptRole || globalInfo.IsSubSuperVisingDeptRole)
            {
                restoreGlobalInfo(globalInfo);
            }
        }

        /// <summary>
        /// Tao cac thong tin gia de gan cho GlobalInfo
        /// </summary>
        /// <param name="globalInfo"></param>
        /// <param name="schoolInfo"></param>
        private void makeFakeGlobalInfo(GlobalInfo globalInfo, SchoolProfileDetailViewedBySupervisingDept schoolInfo)
        {
            SMAS.Business.IBusiness.ISchoolProfileBusiness SchoolProfileBusiness = DependencyResolver.Current.GetService<SMAS.Business.IBusiness.ISchoolProfileBusiness>();
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolInfo.SchoolID);
            if (sp != null)
            {
                globalInfo.SchoolID = sp.SchoolProfileID;
                globalInfo.SchoolName = sp.SchoolName;
                globalInfo.AcademicYearID = schoolInfo.AcademicYearID;
                globalInfo.AppliedLevel = schoolInfo.AppliedLevel;
                SMAS.Business.IBusiness.IEducationLevelBusiness EducationLevelBusiness = DependencyResolver.Current.GetService<SMAS.Business.IBusiness.IEducationLevelBusiness>();
                IQueryable<EducationLevel> educationlevels = EducationLevelBusiness.GetByGrade(schoolInfo.AppliedLevel);
                globalInfo.EducationLevels = educationlevels.ToList();
                globalInfo.IsAdminSchoolRole = false;
                globalInfo.IsViewAll = true;
                globalInfo.UserAccount = sp.UserAccount;
                globalInfo.UserAccountID = sp.UserAccount != null ? sp.UserAccount.UserAccountID : 0;
                globalInfo.IsCurrentYear = false;
                SMAS.Business.IBusiness.IAcademicYearBusiness AcademicYearBusiness = DependencyResolver.Current.GetService<SMAS.Business.IBusiness.IAcademicYearBusiness>();
                AcademicYear ay = AcademicYearBusiness.Find(schoolInfo.AcademicYearID);
                globalInfo.Semester = DateTime.Now < ay.SecondSemesterStartDate ? 1 : 2;
            }
        }

        /// <summary>
        /// Luu lai cac thong tin trong GlobalInfo
        /// </summary>
        /// <param name="globalInfo"></param>
        private void backupGlobalInfo(GlobalInfo globalInfo)
        {
            BackupGlobalInfo backupInfo = new BackupGlobalInfo()
            {
                SchoolID = globalInfo.SchoolID,
                SchoolName = globalInfo.SchoolName,
                AcademicYearID = globalInfo.AcademicYearID,
                AppliedLevel = globalInfo.AppliedLevel,
                EducationLevels = globalInfo.EducationLevels,
                IsAdminSchoolRole = globalInfo.IsAdminSchoolRole,
                IsViewAll = globalInfo.IsViewAll,
                UserAccount = globalInfo.UserAccount,
                UserAccountID = globalInfo.UserAccountID,
                IsCurrentYear = globalInfo.IsCurrentYear,
                Semester = globalInfo.Semester
            };
            SessionObject.Set<BackupGlobalInfo>("tannd20141106", backupInfo);
        }

        /// <summary>
        /// Phuc hoi cac thong tin cho GlobalInfo
        /// </summary>
        /// <param name="globalInfo"></param>
        private void restoreGlobalInfo(GlobalInfo globalInfo)
        {
            BackupGlobalInfo backupInfo = SessionObject.Get<BackupGlobalInfo>("tannd20141106");
            globalInfo.SchoolID = backupInfo.SchoolID;
            globalInfo.SchoolName = backupInfo.SchoolName;
            globalInfo.AcademicYearID = backupInfo.AcademicYearID;
            globalInfo.AppliedLevel = backupInfo.AppliedLevel;
            globalInfo.EducationLevels = backupInfo.EducationLevels;
            globalInfo.IsAdminSchoolRole = backupInfo.IsAdminSchoolRole;
            globalInfo.IsViewAll = backupInfo.IsViewAll;
            globalInfo.UserAccount = backupInfo.UserAccount;
            globalInfo.UserAccountID = backupInfo.UserAccountID;
            globalInfo.IsCurrentYear = backupInfo.IsCurrentYear;
            globalInfo.Semester = backupInfo.Semester;
        }
    }
}
