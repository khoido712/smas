﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace SMAS.Web.Filter
//{
//    public class TransactionAttribute : ActionFilterAttribute
//    {
//        private CommittableTransaction transaction;

//        public override void OnActionExecuting(ActionExecutingContext filterContext)
//        {
//            transaction = new CommittableTransaction();

//            base.OnActionExecuting(filterContext);
//        }

//        public override void OnResultExecuted(ResultExecutedContext filterContext)
//        {
//            base.OnResultExecuted(filterContext);

//            try
//            {
//                if ((filterContext.Exception != null) && (!filterContext.ExceptionHandled))
//                {
//                    transaction.Rollback();
//                }
//                else
//                {
//                    transaction.Commit();
//                }
//            }
//            finally
//            {
//                transaction.Dispose();
//            }
//        }
//    }
//}