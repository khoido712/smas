﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using System.Configuration;
using log4net;

namespace SMAS.Web.Filter
{
    public class AuthorizeActionFilterAttribute : FilterAttribute, IActionFilter
    {
        static readonly ILog Logger = LogManager.GetLogger("SMAS");
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string Controler = filterContext.RouteData.Values["Controller"].ToString().ToLower();
            string Action = filterContext.RouteData.Values["Action"].ToString();
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
           || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
            //neu action khong ap dung AllowAnonymousAttribute thi bat buoc phai dang nhap
            if (!skipAuthorization)
            {
                if (HttpContext.Current.Session != null && (filterContext.HttpContext.Session[GlobalConstants.USERACCID] != null && (int)filterContext.HttpContext.Session[GlobalConstants.USERACCID] == 0))
                {
                    if (Controler.ToLower().Equals("home") && Action.ToLower().Equals("logon")) return;
                    if (!HttpContext.Current.Response.IsRequestBeingRedirected)
                    {
                        //Không th?c hi?n du?c khi s? d?ng ajaxRequest.
                        //HttpContext.Current.Response.RedirectPermanent(System.Web.VirtualPathUtility.ToAbsolute("~/Home/Logon"), true);

                        HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/LogOn") + "';</script>");
                        //HttpContext.Current.Response.Write("<script>window.location='" + c + "';</script>");
                        HttpContext.Current.Response.End();
                    }
                }
            }


            //kiem tra action co ap dung SkipCheckRoleAttribute ko
            bool skipAuthorizeAction = filterContext.ActionDescriptor.IsDefined(typeof(SkipCheckRoleAttribute), true)
            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(SkipCheckRoleAttribute), true);

            if (skipAuthorization)
            {
                if (Controler.ToLower().Equals("home") && Action.ToLower().Equals("authorizeadmin"))
                {
                    if (filterContext.HttpContext.Session == null || (filterContext.HttpContext.Session != null && filterContext.HttpContext.Session.Keys.Count == 0))
                    {

                        HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/LogOn") + "';</script>");
                        HttpContext.Current.Response.End();
                    }
                }
            }


            if (!skipAuthorizeAction)
            {
                //kiem tra neu khong co session thi nem ra loi
                if (filterContext.HttpContext.Session == null 
                    ||(HttpContext.Current.Session != null && 
                    filterContext.HttpContext.Session[GlobalConstants.USERACCID] != null 
                    && (int)filterContext.HttpContext.Session[GlobalConstants.USERACCID] == 0
                ))
                {
                    //HttpContext.Current.Response.Write("<script>window.location='" + Utils.Utils.LogOn + "';</script>");
                    //HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/LogOn") + "';</script>");
                    //throw new NullReferenceException("The HttpContext.Session is missing.");
                }

                //kiem tra neu la tao moi Session
                bool isNewSession = filterContext.HttpContext.Session.IsNewSession;

                StringBuilder actionMenuUrl = new StringBuilder();


                //lay trong filterContext ra area, controller Name, action Name de build duong dan cua action
                actionMenuUrl.Append("/");

                if (filterContext.RouteData.DataTokens.ContainsKey("area"))
                    filterContext.RouteData.Values.Add("area",
                        filterContext.RouteData.DataTokens["area"]);
                //neu trong RouteData chua key= area
                if (filterContext.RouteData.Values.ContainsKey("area"))
                {
                    actionMenuUrl.Append(filterContext.RouteData.Values["area"]).Append("/");
                }
                actionMenuUrl.Append(filterContext.RouteData.Values["Controller"]);


                //lay trong session danh sach cac Menu ma nguoi dung duoc thuc hien

                //Kiem tra file day len neu co co hop le khong
                #region Kiem tra file day len neu co co hop le khong
                if (filterContext.ActionParameters != null)
                {

                    var list = filterContext.ActionParameters.Where(o => o.Key.Contains("attachments"));

                    if (list != null && list.Count() > 0)
                    {
                        var objectFile = list.FirstOrDefault();
                        if (objectFile.Value is IEnumerable<HttpPostedFileBase>)
                        {
                            //kiem tra co phai doi tuong file khong
                            IEnumerable<HttpPostedFileBase> attachments = objectFile.Value as IEnumerable<HttpPostedFileBase>;

                            if (attachments != null && attachments.Count() >= 0)
                            {
                                // The Name of the Upload component is "attachments"
                                var file = attachments.FirstOrDefault();
                                if (file != null)
                                {
                                    if (!Utils.Utils.getSafeFileName(file.FileName))
                                    {
                                        JsonMessage jm = new JsonMessage(Res.Get("Common_Error_FileImport"), "error");
                                        filterContext.Result = new JsonResult
                                        {
                                            Data = jm,
                                            JsonRequestBehavior = JsonRequestBehavior.AllowGet //allow GET requests
                                        };
                                        filterContext.HttpContext.Response.StatusCode = 450;
                                    }
                                }
                            }

                        }
                    }
                }
                #endregion

                List<Menu> menus = filterContext.HttpContext.Session[GlobalConstants.MENUS] as List<Menu>;
                if (filterContext.HttpContext.Session["MenuRoles"] != null)
                {
                    menus = filterContext.HttpContext.Session["MenuRoles"] as List<Menu>;
                }
                if (menus != null)
                {
                    GlobalInfo globalInfor = GlobalInfo.getInstance();
                    // Neu nguoi dung hien tai la nguoi dung so, phong thi them cac menu sau
                    if (globalInfor.IsSuperVisingDeptRole || globalInfor.IsSubSuperVisingDeptRole)
                    {
                        // Menu xem chi tiet ho so truong tu phong so
                        if (menus.FirstOrDefault(m => m.URL == "/SchoolProfileArea/SchoolProfileDetail/Index") == null)
                        {
                            menus.Add(new Menu() { URL = "/SchoolProfileArea/SchoolProfileDetail/Index", MenuName = "Xem chi tiet truong tu phong so" });
                        }
                    }

                    // Kiem tra them doi voi truong hop vao schoolprofilecontroller
                    string[] actionSchoolNotAllow = { "index", "search", "exportexcel", "delete", "create" };
                    if (Controler.Equals("schoolprofile"))
                    {
                        if (filterContext.HttpContext.Session[GlobalConstants.IS_SCHOOL_ROLE] != null && (bool)filterContext.HttpContext.Session[GlobalConstants.IS_SCHOOL_ROLE])
                        {
                            if (actionSchoolNotAllow.Contains(Action.ToLower().Trim()))
                            {
                                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError" }));
                            }
                        }
                    }

                    //kiem tra trong allowedUrls co chua url cua action dang thuc hien ko?

                    Dictionary<string, string[]> dicSpecial = new Dictionary<string, string[]>();
                    string[] RecordSpecial = { "markrecord", "bookmarkrecord", "judgerecord", "judgerecordsemester", "primaryjudgerecord", "markrecordofprimary", "markrecordofprimaryheadteacher", "markrecordofprimaryteacher" };
                    string[] SumupSpecial = { "pupilrankingsemester", "summeduprecord" };
                    string[] pupilranking = { "pupilrankingofsemester", "pupilrankingperiod" };
                    string[] pupilrankingnew = { "pupilranking", "pupilrankingnew" };
                    string[] SummedUpRecordEducationLevelNew = { "summeduprecordbyeducationlevel", "summedupeducationlevelbyperiod" };
                    string[] EmployeeProfile = { "employeepraisediscipline", "useraccountprofile", "employeeretire", "employeeseverance", "employeeworkmovement", "employeesalary", "employeequalification", "employeeworkinghistory" };
                    string[] ImageParent = { "employeeprofilemanagement", "employee", "pupilprofile", "help" };
                    string[] EmployeeDetail = { "employeedetail" };
                    //cac area se bo qua ngay
                    string[] MenuCancel = { "/employeeprofilemanagement", "/help", "/employeedetail" };
                    string[] SendSMSComponet = { "sendsmscommentofpupil" };

                    dicSpecial.Add("/bookrecord", RecordSpecial);
                    dicSpecial.Add("/summary", SumupSpecial);
                    dicSpecial.Add("/conductranking", pupilranking);
                    dicSpecial.Add("/pupilrankingnewarea", pupilrankingnew);
                    dicSpecial.Add("/summedupeducationlevelnew", SummedUpRecordEducationLevelNew);
                    dicSpecial.Add("/employeeprofilemanagement", EmployeeProfile);
                    dicSpecial.Add("/employeedetail", EmployeeDetail);
                    //dicSpecial.Add("/sendsmscommentofpupil",new string[] { "sendsmscommentofpupil"});
                    List<string> allArray = RecordSpecial
                        .Union(SumupSpecial)
                        .Union(pupilranking)
                        .Union(pupilrankingnew)
                        .Union(SummedUpRecordEducationLevelNew)
                        .Union(EmployeeProfile)
                        .Union(ImageParent)
                        .Union(EmployeeDetail)
                        .Union(SendSMSComponet)
                        .ToList();
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Doi voi cac controller co thuoc tinh la ViewableBySupervisingDeptFilter thi co the nhin thay duoc voi nguoi dung phong so
                    bool ViewableBySupervisingDept = filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(ViewableBySupervisingDeptFilter), true)
                                                    || filterContext.ActionDescriptor.IsDefined(typeof(ViewableBySupervisingDeptFilter), true);
                    if (ViewableBySupervisingDept && (globalInfor.IsSubSuperVisingDeptRole || globalInfor.IsSuperVisingDeptRole))
                    {
                        return;
                    }
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Xu ly truong hop rieng doi voi controller image
                    if (Controler.Equals("image"))
                    {
                        if (Controler != "home" && Controler != "error")
                        {
                            bool isRole = false;
                            foreach (string c in ImageParent)
                            {
                                // Neu nguoi dung co quyen vao cac menu trong ImageParent thi cung co quyen vao menu Image
                                var list = menus.FindAll(x => x.URL.ToLower().Contains(c));
                                if (list != null && list.Count > 0)
                                {
                                    isRole = true;
                                    break;
                                }
                            }
                            if (!isRole)
                            {
                                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                                {
                                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError" }));
                                }
                                //else
                                //{
                                //    HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/AuthorizeAdmin") + "';</script>");
                                //    HttpContext.Current.Response.End();
                                //}
                            }
                        }
                    }
                    else if (!allArray.Contains(Controler))
                    {
                        // Xu ly cho cac menu binh thuong
                        if (Controler != "home" && Controler != "error")
                        {
                            string ActionUrl = actionMenuUrl.ToString().ToLower();
                            var list = menus.FindAll(x => x.URL.ToLower().Contains(ActionUrl));
                            if (list == null || list.Count == 0)
                            {
                                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                                {
                                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError" }));
                                }
                                else
                                {
                                    HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/Index") + "';</script>");
                                    HttpContext.Current.Response.End();
                                }
                            }
                        }

                    }
                    else
                    {
                        foreach (string keySpecial in dicSpecial.Keys)
                        {
                            if (dicSpecial[keySpecial].Any(o => Controler.Equals(o)))
                            {
                                var listS = menus.FindAll(x => x.URL.ToLower().Contains(keySpecial));
                                if ((listS == null || listS.Count == 0) && !MenuCancel.Contains(keySpecial))
                                {
                                    if (!filterContext.HttpContext.Request.IsAjaxRequest() && Controler != "home" && Controler != "error")
                                    {
                                        string ActionUrl = actionMenuUrl.ToString().ToLower();
                                        var list = menus.FindAll(x => x.URL.ToLower().Contains(ActionUrl));
                                        if (list == null || list.Count == 0)
                                        {
                                            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError" }));
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
                else
                {
                    //Neu van luu cookie dang nhap ma thong tin menu trong session bi mat

                }
            }

        }




        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // do nothing 
        }
    }
}
