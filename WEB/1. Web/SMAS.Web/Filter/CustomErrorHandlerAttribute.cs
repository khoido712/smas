﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using SMAS.Business.Common;
using SMAS.Web.Utils;
using System.Diagnostics;
using System.Configuration;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Filter
{
    public class CustomErrorHandlerAttribute : HandleErrorAttribute
    {
        static readonly ILog Logger = LogManager.GetLogger("SMAS");
        public override void OnException(ExceptionContext filterContext)
        {
            string Controler = filterContext.RouteData.Values["Controller"].ToString();
            string Action = filterContext.RouteData.Values["Action"].ToString();
            string IDUser = new GlobalInfo().UserAccountID.ToString();
            int CodeError = new HttpException(null, filterContext.Exception).GetHttpCode();

            string ExStackTrace = filterContext.Exception.StackTrace;
            string ExMessage = filterContext.Exception.Message;

            //string FileError = filterContext.Exception.;
            Exception ex = filterContext.Exception;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                ExMessage += ex.Message;
            };


            string className = Controler + "/" + Action;
            //LogExtensions.ErrorExt(Logger, DateTime.Now, className, "", ExMessage, CodeError.ToString(), filterContext.Exception);
            Logger.Error(ExMessage, ex);
            //kiem tra Exception da duoc handle chua va co phai la loai BusinessException ko?
            if (!filterContext.ExceptionHandled && (filterContext.Exception is BusinessException || filterContext.Exception is HttpRequestValidationException))
            {
                bool isValidate = filterContext.Exception is HttpRequestValidationException;
                HttpRequestValidationException e = null;
                if (isValidate)
                {
                    e = (HttpRequestValidationException)filterContext.Exception;
                }
                BusinessException be = isValidate ? new BusinessException(Res.Get("Common_Validate_XSS"), new List<object>()) : (BusinessException)filterContext.Exception;

                JsonMessage jm = new JsonMessage(be);

                filterContext.HttpContext.Response.StatusCode = 450;
                filterContext.ExceptionHandled = true;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    //Neu la ajax request gay ra loi thi tra ve JsonMessage
                    filterContext.Result = new JsonResult
                    {
                        Data = jm,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet //allow GET requests
                    };
                }
                else
                {
                    //chuyen den trang thong bao loi
                    //neu ko phai la ajax request thi tra ve thong bao loi
                    filterContext.HttpContext.Session["ErrorStr"] = jm.Message;
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError", error = "1" }));

                }
            }
            if (!filterContext.ExceptionHandled && filterContext.Exception is System.Web.Mvc.HttpAntiForgeryException)
            {
                HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/Index") + "';</script>");
                filterContext.HttpContext.Response.End();
                filterContext.ExceptionHandled = true;
            }

            if (!filterContext.ExceptionHandled && filterContext.Exception is AjaxUnAuthorizeException)
            {
                /*
                filterContext.HttpContext.Response.StatusCode = 401;
                AjaxUnAuthorizeException be = (AjaxUnAuthorizeException)filterContext.Exception;

                JsonMessage jm = new JsonMessage("AjaxUnAuthorizeException","ERROR");
                filterContext.Result = new JsonResult
                {
                    Data = jm,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet //allow GET requests
                };
                 */
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 453;
                filterContext.HttpContext.Response.End();
                filterContext.ExceptionHandled = true;
            }
            else if (!filterContext.ExceptionHandled && filterContext.Exception is UnAuthorizeException)
            {
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 453;
                filterContext.HttpContext.Response.End();

                filterContext.ExceptionHandled = true;

            }
            else if (!filterContext.ExceptionHandled && CodeError == 500)
            {


                //he thong co      loi xay ra dua ra 
                //filterContext.HttpContext.Session["ErrorStr"] = Res.Get("Common_error_system");
                filterContext.HttpContext.Session["ErrorStr"] = Res.Get("Common_error_system");

                //+ ": \n"
                // + " -- Error Message: " + ExMessage  + "\n"
                // + "                                                Error StackTrace: " + ExStackTrace;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    //truong hop mat session nhay vao token xac nhan tocken
                    //if (filterContext.HttpContext.Session == null || (filterContext.HttpContext.Session != null && filterContext.HttpContext.Session.Keys.Count == 0) || (filterContext.HttpContext.Session.Keys.Count == 1 && filterContext.HttpContext.Session.Keys[0].ToString() == "ErrorStr"))
                    //{
                    //    if (!ConfigurationManager.AppSettings["IsSSOLogin"].Equals("true"))
                    //    {
                    //        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "logoff", error = "1" }));
                    //    }
                    //    else
                    //    {
                    //        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "SSOServices", action = "Index", error = "1" }));
                    //    }
                    //    filterContext.ExceptionHandled = true;
                    //    HttpContext.Current.Response.End();
                    //}
                    //else
                    {
                        //HttpContext.Current.Response.StatusCode = 200;
                        HttpContext.Current.Response.Write("<script>window.location='" + System.Web.VirtualPathUtility.ToAbsolute("~/Home/RoleError") + "';</script>");
                        HttpContext.Current.Response.End();
                    }
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { area = "", controller = "Home", action = "RoleError" }));
                    filterContext.ExceptionHandled = true;
                }
                int statusCode = filterContext.HttpContext.Response.StatusCode;
                if (statusCode == 551)
                {
                    /*
                    JsonMessage jm = new JsonMessage("AjaxUnAuthorizeException", "ERROR");
                    filterContext.Result = new JsonResult
                    {
                        Data = jm,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet //allow GET requests
                    };
                     */
                    filterContext.ExceptionHandled = true;
                }
            }

        }
    }
}