jQuery(document).ready(function () {
    $('.GeneralTable textarea, .GeneralTable input[type="text"]').focus(function () {
        $(this).parent().addClass('BgFocusInputSection');
    });
    $('.GeneralTable textarea, .GeneralTable input[type="text"]').blur(function () {
        $(this).parent().removeClass('BgFocusInputSection');
    });
    //$(".ContentMessageBox").slideUp();
    /*$('.MySelectBoxClass').customStyle();*/
    $(".ParentMenu").tabs({
        cookie: {
            expires: 100
        }
    });
    //getter
    var selected = $(".ParentMenu").tabs("option", "selected");
    //setter
    /*menu sidebar*/
    $(".MenuSection").mouseover(
		function () {
		    $(".MenuSectionList").show(200);
		});
    $(".MenuSection").mouseleave(
		function () {
		    $(".MenuSectionList").hide(200);
		});
    //New style menu js
    $(".page .BoxMenuSection").mouseover(
	function () {
	    $(".page .ChildMenuSectionList").show(200);
	});
    $(".page .BoxMenuSection").mouseleave(
	function () {
	    $(".page .ChildMenuSectionList").hide();
	});
    /*menu sidebar*/
    /*menu search*/
    $(".ParentMenu").tabs("option", "selected", 0); //.tabs( "remove" , index )
    $("#colapseFunc").click(function () {
        var cls = $(this).attr("class");
        if ($("#colapseFunc").hasClass("Sclose")) {
            $(this).removeClass("Sclose");
            $(this).addClass("Eclose");
            $("[id*=tabs]").slideUp("slow");
            $(".TabChildMenu").hide("slow");
        }
        else {
            $(this).removeClass("Eclose");
            $(this).addClass("Sclose");
            $("[id*=tabs]").slideDown("slow");
            $(".TabChildMenu").show("slow");
        }
    });

    $("#collapseMsgBox").click(function () {
        //var cls = $(this).attr("class");
        //var id = $(this).attr("id");
        //if ($("#collapseMsgBox").hasClass("Sclose1")) {
        //    $(this).removeClass("Sclose1");
        //    $(this).addClass("Eclose1");
        //    $(".ContentMessageBox").slideUp("slow");
        //    $(".ScrollSection").css("height", "898");
        //    $('.ScrollSection').jScrollPane();
        //}
        //else {
        //    $(this).removeClass("Eclose1");
        //    $(this).addClass("Sclose1");
        //    $(".ContentMessageBox").slideDown("slow");
        //    $(".ScrollSection").css("height", "500");
        //    $('.ScrollSection').jScrollPane();
        //    $(".ContentScrollSection").css("height", "875");
        //}
    });
    $(".collapseMsgBox").click(function () {
        //var cls = $(this).attr("class");
        //var id = $(this).attr("id");
        //var parent = $(this).closest(".MessageBox");
        //if ($(this).hasClass("Sclose1")) {
        //    $(this).removeClass("Sclose1");
        //    $(this).addClass("Eclose1");
        //    $(parent).find(".ContentMessageBox").slideUp("slow");
        //    $(".ScrollSection").css("height", "898");
        //    $('.ScrollSection').jScrollPane();
        //}
        //else {
        //    $(this).removeClass("Eclose1");
        //    $(this).addClass("Sclose1");
        //    $(parent).find(".ContentMessageBox").slideDown("slow");
        //    $(".ScrollSection").css("height", "500");
        //    $('.ScrollSection').jScrollPane();
        //    $(".ContentScrollSection").css("height", "875");
        //}
    });
    

    $(".LinkTab").click(function () {
        var cls = $("#colapseFunc").attr("class");
        $(".BreadcrumFunc").removeClass("NoBorder");
        if ($(".TabChildMenu").is(":visible")) {
            if ($("#colapseFunc").hasClass("Eclose")) {
                $("#colapseFunc").removeClass("Eclose");
                $("#colapseFunc").addClass("Sclose");
                $(".TabChildMenu").show("slow");
            }
        }

    });

    $('<div class="tl"></div><div class="tr"></div><div class="bl"></div><div class="br"></div>').appendTo(".GroupMenuLV3");

    /*MenuLV3Section hover*/
    jQuery('.MenuLV3Section').mouseover(function () {
        var itemId = jQuery(this).attr('id');
        var aCurrentHoverId = jQuery('#' + itemId + ' a').attr('id');
        jQuery('#' + itemId).addClass('level2_current');
        jQuery('#' + aCurrentHoverId).addClass('level2_current');
    });
    jQuery('.MenuLV3Section').mouseleave(function () {
        var itemId = jQuery(this).attr('id');
        var aCurrentHoverId = jQuery('#' + itemId + ' a').attr('id');
        jQuery('#' + itemId).removeClass('level2_current');
        jQuery('#' + aCurrentHoverId).removeClass('level2_current');
    });
    var itemId = jQuery(this).attr('id');
    var aCurrentHoverId = jQuery('#' + itemId + ' a').attr('id');
    jQuery('#' + itemId).removeClass('level2_current');
    jQuery('#' + aCurrentHoverId).removeClass('level2_current');    
    //Set same height for box in Content
    var hTable = $('.SecondColsSecion').height();
    var url = window.location.href;
    if (url.indexOf('SCSActivityPlan') < 0) {
        $('.FirstColsSecion').height(hTable - 2);
    }
    $(".GroupPerson #colapseFunc").click(function () {
        var cls = $(this).attr("class");
        if ($(this).hasClass("Sclose")) {
            $(this).removeClass("Sclose");
            $(this).addClass("Eclose");
            $('.GroupPerson').height(27);
        }
        else {
            $(this).removeClass("Eclose");
            $(this).addClass("Sclose");
            $('.GroupPerson').height('100%');
        }
    });
    $('.OnOff').click(function () {
        if ($(this).hasClass('On')) {
            $('.on', this).hide();
            $('.off', this).show();
            $(this).addClass('Off').removeClass('On').attr('title', 'Click here to OFF');

        }
        else {

            $('.off', this).hide();
            $('.on', this).show();
            $(this).addClass('On').removeClass('Off').attr('title', 'Click here to ON');
        }
    });

    $(".vt-SearchPanel .vt-SearchPanel-Title #btn-panel-collapse").click(function () {
        var panel_content = $(this).parent().siblings('.vt-SearchPanel-Content');
        var panel_footer = $(this).parent().siblings('.vt-SearchPanel-Button');
        panel_content.slideToggle(200);
        panel_footer.slideToggle(200);
        if ($(this).hasClass('icon-arrow-up')) {
            $(this).removeClass('icon-arrow-up').addClass('icon-arrow-down');
        } else {
            $(this).removeClass('icon-arrow-down').addClass('icon-arrow-up');
        }
    });


    /*Ho so hoc sinh*/
    $(".BtnColExSearch").click(function () {
        var cls = $(this).attr("class");
        if ($(this).hasClass("ExplainBoxSearch")) {
            $('.CollapseSearchSection').hide();
            $('.ExplainSearchSection').show();
        }
        else if ($(this).hasClass("CollapseBoxSearch")) {
            $('.ExplainSearchSection').hide();
            $('.CollapseSearchSection').show();
        }
    });   

    /*sidebar*/
    $(".CollapseSideBar a").click(function () {
        if ($(this).hasClass("Sclose2")) {
            console.log(1);
            $(this).removeClass("Sclose2");
            $(this).addClass("Eclose2");
            $(".SideBarTop").fadeOut("slow");
            $("#contentSecitonLarge, #contentSecitonLarge2").removeClass("ContentSectionSmallTop");
            $("#contentSecitonLarge, #contentSecitonLarge2").addClass("ContentSectionLargeTop");
            //$("#RightPanel").width("972");
            //$('#RightPanel').attr('style', 'width:98% !important');
            $('#RightPanel').addClass('col-md-12');
            $('#RightPanel').removeClass('col-sm-9 col-md-10');
          //  $(".vt-LeftPanel").width("0");
           // $("#RightPanel").css("padding-left", "3px");
            $('.Scroll1Section, .Table1Section .ScrollBodySection ').jScrollPane();
        }
        else {
            $(this).removeClass("Eclose2");
            $(this).addClass("Sclose2");
            $(".SideBarTop").fadeIn("slow");
            $("#contentSecitonLarge, #contentSecitonLarge2").removeClass("ContentSectionLargeTop");
            $("#contentSecitonLarge, #contentSecitonLarge2").addClass("ContentSectionSmallTop");
            //$("#RightPanel").width("852");
            if ($('#content').hasClass("fullscreen")) {
             //   $('#RightPanel').attr('style', 'width:89% !important');
                $('#RightPanel').addClass('col-sm-9 col-md-10');
                $('#RightPanel').removeClass('col-md-12');
            }
            else {
                //$('#RightPanel').attr('style', 'width:86% !important');
                $('#RightPanel').addClass('col-sm-9 col-md-10');
                $('#RightPanel').removeClass('col-md-12');
            }
           // $("#RightPanel").css("padding-left", "0");
          //  $(".vt-LeftPanel").width("122");
            $('.Scroll1Section, .Table1Section .ScrollBodySection ').jScrollPane();
        }
    })

   
    


    var tempHeight = $(".MainContentSection").height();
    var ctHeight = tempHeight - 6;
    //$(".SideBarLoop").height(ctHeight);
    /*sidebar*/
    $(".GeneralTabTop .BtnClose a").click(function () {
        var idBtnTab = $(this).attr("id");
        $("#box" + idBtnTab).hide();
    });
    $(".LinkTab").click(function () {
        var cls = $("#colapseFunc").attr("class");
        $(".BreadcrumFunc").removeClass("NoBorder");
        if ($(".TabChildMenu").is(":visible")) {
            if ($("#colapseFunc").hasClass("Eclose")) {
                $("#colapseFunc").removeClass("Eclose");
                $("#colapseFunc").addClass("Sclose");
                $(".TabChildMenu").show("slow");
            }
        }

    });
});

function showDialogFancy(title, nameId) {
    title += '<img src="../Content/images/loading-white.gif" width="16" height="16" alt="�ang t?i" class="LoadingStyle" />'
    var html = $('#' + nameId).html();
    $.fancybox(
		html, {
		    'title': title,
		    'closeBtn': true,
		    helpers: {
		        title: { type: 'inside' }
		    },
		    'afterShow': function () {
		        $("#fancybox-inner #s3, #fancybox-inner #s4").dropdownchecklist({ icon: { placement: 'right' }, width: 203, maxDropHeight: 100 });
		        $("#fancybox-inner #s5, #fancybox-inner #s6, #fancybox-inner #s7, #fancybox-inner #s8").dropdownchecklist({ icon: { placement: 'right' }, width: 218, maxDropHeight: 100 });
		        $("#fancybox-inner #s13").dropdownchecklist({ icon: { placement: 'right' }, width: 174, maxDropHeight: 100 });
		        $('#fancybox-inner .DScrollpane, #fancybox-inner .Scrollpane').jScrollPane();
		    }
		}
	);
    return false;
}
function openGeneralDialog(idName, titleName, addedClassName, heightNumber, widthNumber) {
    if (titleName != null) {
        titleName += '<img src=".../Content/images/loading-white.gif" width="16" height="16" alt="�ang t?i" class="LoadingStyle" />';
    }
    $('#' + idName).dialog({
        title: titleName,
        modal: true,
        resizable: false,
        width: widthNumber/*820/*514*/,
        height: heightNumber,
        open: function (event, ui) {
            $('.ui-dialog').addClass(addedClassName);
            $('.ui-dialog').append('<div class="UiJquryDialogLTCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogMTCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogRTCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogRMCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogRBCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogMBCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogLBCorner UiJquryDialogCorner"></div>');
            $('.ui-dialog').append('<div class="UiJquryDialogLMCorner UiJquryDialogCorner"></div>');
            $('.BoxDialogUi .Scrollpane').jScrollPane();
        }
    });
    return false;
};
/*colspan cac toolbar namta*/
function ChangeMe(obj) {
    //var TabSection = $(obj).closest(".TabSection");
    //$(".CollapseTab", $(TabSection)).click();
    //$(".collapseMsgBox", $(TabSection)).click();
}



function ShowOffBox(selector, type) {
    var cls = $("#collapseMsgBox").attr("class");
    var id = $("#collapseMsgBox").attr("id");
    var parent = $(selector).closest(".MessageBox");
    if (type = "open") {
        //$("#collapseMsgBox").removeClass("Sclose1");
        //$("#collapseMsgBox").addClass("Eclose1");
        $(parent).find(".ContentMessageBox").slideDown("slow");
        $(".ScrollSection").css("height", "898");
        $('.ScrollSection').jScrollPane(); 
    }
    else {
        //$("#collapseMsgBox").removeClass("Eclose1");
        //$("#collapseMsgBox").addClass("Sclose1");
        $(parent).find(".ContentMessageBox").slideUp("slow");
        $(".ScrollSection").css("height", "500");
        $('.ScrollSection').jScrollPane();
        $(".ContentScrollSection").css("height", "875");
    }
}

$(document).ready(function () {
    //$(".TabSection a").each(function () {
    //    $(this).attr("href", "javascript:void(0);");
    //    $(this).attr("onclick", "ChangeMe(this);");
    //});
});


function ViewFullMessage(){


    $("#contentID").removeClass("ClUpViewMessage");
    $("#contentID").addClass("ClDownViewMessage");

    var str = $("#contentIDFull").html();
    $("#ContentView").html($.trim(str));

    $("#LinkView").html('');
    if ($("#LinkView").length == 0) {
        $("#ContentView").after('<div id="LinkView">'+viewShort+'</div>');
    } else {
        // gan lai chu thu gon
        $("#LinkView").html(viewShort);
    }    
}

function ViewShortMessage() {
   
    $("#contentID").removeClass("ClDownViewMessage");
    $("#contentID").addClass("ClUpViewMessage");

    var str = $("#contentIDFull").html();
    $("#contentID").html($.trim(str));

    $("#LinkView").html('');
    if ($("#LinkView").length == 0) {
        $("#ContentView").after('<div id="LinkView">'+viewDetail+'</div>');
    } else {
        //gan vao chu xem tiep
        $("#LinkView").html(viewDetail);
    }
    
}
