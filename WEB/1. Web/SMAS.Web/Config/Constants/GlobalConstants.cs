using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Utils;

namespace SMAS.Web.Constants
{
    public class GlobalConstants
    {
        public static Dictionary<string, object> Parameters;

        //tanla4
        //13/11/2014
        //them bien danh dau trang thai tin da doc
        public const bool ISREADMESSAGE = true;
        public const bool ISREADNEWS = true;
        public const string NEWMESSAGENO = "NEWMESSAGENO";
        public const string NEWNEWSNO = "NEWNEWSNO";
        public const string IS_ALERT_MESSAGE = "IsAlertMessage";
        public const string USERACCID = "Id of current log on UserAccount";
        public const string USERNAME = "name of user log on";
        public const string PASSWORD_ENCRYPT = "password encrypt sent to forum";
        public const string CERTIFICATEPASS = "12345678";
        public const string IS_AUTHENTICATED_SUCCEED = "IsAuthenticatedSucceed";
        public const string USERACCOUNT = "current log on UserAccount";
        public const string ROLE_PARENTS = "role parents of this role";
        public const string ROLE_PARENT_ID = "ParentID";
        public const string GROUPS = "groups of current Log on UserAccount";
        public const string ROLES = "roles of current Log on UserAccount";
        public const string MENUS = "menus of current Log on UserAccount";
        public const string MENUS_MODEL = "menus model of current Log on UserAccount";
        public const string SELECTED_ROLE = "selected role of current Log on UserAccount";
        public const string SELECTED_GROUP = "selected group of current Log on UserAccount";
        public const string SHOW_PUPIL_IMAGE = "academicyear setting show image";
        public const string SHOW_CALENDAR = "academicyear setting show calendar";
        //
        public const int PERMISSION_LEVEL_VIEW = 1;
        public const int PERMISSION_LEVEL_CREATE = 2;
        public const int PERMISSION_LEVEL_EDIT = 3;
        public const int PERMISSION_LEVEL_DELETE = 4;
        public const int PERMISSION_LEVEL_UNIDENTIFIED = 0;

        //Product Version
        public const string PRODUCT_VERSION_ID_BASIC = "1";//phien ban rut gon
        public const string PRODUCT_VERSION_ID_ADVANCE = "2";//phien ban chuan
        public const string PRODUCT_VERSION_ID_FULL = "3";//phien ban day du


        //
        public const string ACCOUNTS = "List Account Admin Created By Admin";
        public const string NORMAL_ACCOUNTS = "List Normal Account Created By Admin";
        public const string EMPLOYEES = "ListEmployees";

        public const string MENUITEMS = "ListMenuItems";
        public const string GRIDMENUITEMS = "GridMenuItems";
        public const string MSG = "message";
        public const string LS_ROLES = "ListRoleCheck";

        public const string MENU_PARENTS = "ListMenuParents";
        public const string MENUPERMISSION = "MenuPermission";

        public const string JSON_MSG_LIST = "jsonMsgList";
        public const string TOTAL = "Total";
        public const string PAGE = "Page";


        public const string JSON_ERR_LIST = "ErrList";

        public const string LIST_PROVINCE = "ListProvince";

        public const string GROUPID = "SELECTED_GROUP_ID";

        public const string ROLEID = "SELECTED_ROLE_ID";

        public const string LOGON_MODEL = "log on model";

        public const string MENU_MODEL_LEVEL1 = "menu model level 1 to build menu";

        public const string ITEM_MENU_MODEL_TO_BUILD_MENU = " list of ItemMenuModel to build menu";

        public const string LIST_DISTRICT = "ListDistrict";
        public const string LIST_COMMUNE = "ListCommune";
        public const string LIST_FACULTY = "ListFaculty";

        public const string CHUA_XEP_PHONG_THI = "[Chưa xếp phòng thi]";


        public const int COUNT_CHILDREN = 16; // Mẫu giáo
        public const int COUNT_CHILDCARE = 8; // Nhà trẻ
        public const int COUNT_TERTIARY = 4;
        public const int COUNT_SECONDARY = 2;
        public const int COUNT_PRIMARY = 1;

        public const string SCHOOLID = "school of useraccount";
        public const string PRODUCT_VERSION = "school of product version";

        public const string SUPERVISINGDEPT_ID = "supervisingDept of employee";

        public const string LIST_ACADEMICYEAR = "list of academic year by school";

        public const string ACADEMICYEAR_ID = "current academicYearID";

        public const string OLD_ACADEMICYEAR_ID = "Old academicYearID";

        public const int PLACEHOLDER_CHOICE = 0;
        public const int PLACEHOLDER_ALL = 1;
        public const int NO_PLACEHOLDER = 2;
        public const int PLACEHOLDER_EXAM = 3;


        public const int EMPLOYEE_TYPE_TEACHER = 1;//Giáo viên 
        public const int EMPLOYEE_TYPE_EMPLOYEE = 2;// Nhân viên / chuyên viên cấp trường
        public const int EMPLOYEE_TYPE_DEPARTMENT = 3;//Cán bộ  quản lý phòng ban

        public const int FIRST_SEMESTER = 1;

        public const int SECOND_SEMESTER = 2;


        public const string CLASSID = "class if of head teacher";

        public const string APPLIED_LEVELS = "applied levels";

        public const string APPLIED_LEVEL = "applied level";

        public const string SYSTEM_PARAMS = "all system parameters";

        public const string SEMESTER = "semester";

        public const string EDUCATION_LEVEL = "Education level";
		
		public const string IS_ROLE_PRINCIPAL = "is role principal";

        public const string IS_ADMIN = "Is Admin";

        public const string IS_SYSTEM_ADMIN = "IsSystemAdmin";

        public const string IS_SUPERROLE = "Is SuperRole";

        public const string IS_SCHOOL_ROLE = "Is SchoolRole";

        public const string IS_SCHOOL_ADMIN = "Is SchoolAdmin";

        public const string IS_SUPERVISINGDEPT_ROLE = "Is SuperVisingDept Role";

        public const string IS_SUBSUPERVISINGDEPT_ROLE = "Is SubSuperVisingDept Role";

        public const string SCHOOL_NAME = "School Name of employee";

        public const string SUPERVISINGDEPT_NAME = "SuperVisingDept Name";

        public const string SELECTED_ACADEMICYEAR_INDEX = "selected index of list academicYear";

        public const string EMPLOYEEID = "employeeID corresponding to UserAccountID";
		
		public const string EMPLOYEENAME = "employee full name";

        public const string IsCurrentYear = "is current academic Year";

        public const string PROVINCEID = "province of employee of SuperVisingDept";

        public const string DISTRICTID = "district of employee of SuperVisingDept";

        public const string EDUCATION_LEVELS = "list education levels by grade";

        public const string IS_VIEW_ALL = "Is View all";

        public const string IS_EMPLOYEE_MANAGER = "Is Employee Manager";

        public const int ROLE_ADMIN_TRUONGC1 = 7;

        public const int ROLE_ADMIN_TRUONGC2 = 6;
        
        public const int ROLE_ADMIN_TRUONGC3 = 5;
        
        public const int ROLE_ADMIN_NHATRE = 8;
        
        public const int ROLE_ADMIN_MAUGIAO = 9;
        
        public const int ROLE_PHONG = 4;
        
        public const int ROLE_SO = 3;
        
        public const int ROLE_CAPCAO = 1;
        public const int ROLE_ADMIN_CAPTINH = 2;
        public const string SELECTED_APPLIEDLEVEL_INDEX = "selected appliedLevel index in List appliedLevel";

        public const string IS_PROVINCE_LEVEL = "is supervising province level";

        public const string IS_DISTRICT_LEVEL = " is supervising district level";

        //cap hoc 1,2,3
        public const string GRADE = "grade of class";

        //danh hieu thi dua cho khoa
        public const int HONOUR_ACHIVEMENT_TYPE_FACULTY = 3;

        //danh hieu thi dua cho hoc sinh
        public const int HONOUR_ACHIVEMENT_TYPE_PUPIL = 2;

        //danh hieu thi dua cho can bo
        public const int HONOUR_ACHIVEMENT_TYPE_EMPLOYEE = 1;

        public const int SUBJECTCAT_ISCOMMENTING_MARK = 0;

        public const int SUBJECTCAT_ISCOMMENTING_COMMENT = 1;

        public const int SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT = 2;

        public const int SUBJECTCAT_PRIMARY = 1;
        public const int SUBJECTCAT_SECONDARY = 2;
        public const int SUBJECTCAT_TERTIARY = 3;
        public const int SUBJECTCAT_CHILDCARE = 4;
        public const int SUBJECTCAT_CHILDREN = 5;

        public const string FUNCTION_PATH = "current function path ";

        public const int CONDUCT_ESTIMATION_TYPE_VIOLATION = 1;
        public const int CONDUCT_ESTIMATION_TYPE_CAPACITY = 2;
        public const int CONDUCT_ESTIMATION_TYPE_UNIDENTIFIED = 0;

        public const int WORK_MOVEMENT_WITHIN_SYSTEM = 1;
        public const int WORK_MOVEMENT_OTHER_ORGANIZATION = 2;
        public const int WORK_MOVEMENT_RETIREMENT = 3;
        public const int WORK_MOVEMENT_OTHER = 4;

        public const int EXEMPT_TYPE_ALL = 2;
        public const int EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE = 3;
        public const int EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE = 5;

        //
        public const int EMPLOYEE_STATUS_WORKING = 1;
        public const int EMPLOYEE_STATUS_BREATHER = 6;
        //

        public const int TEACHER_ROLE_HEADTEACHER = 1;
        public const int TEACHER_ROLE_SUBJECTTEACHER = 2;
        public const int TEACHER_ROLE_OVERSEEING = 3;
        public const int TEACHER_ROLE_HEAD_SUBJECTTEACHER = 4;
        public const int TEACHER_ROLE_HEAD_OVERSEEING = 5;
        public const int TEACHER_ROLE_SUBJECT_OVERSEEING = 6;
        public const int TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING = 7;

        //
        public const int APPLIED_LEVEL_PRIMARY = 1;
        public const int APPLIED_LEVEL_SECONDARY =2 ;
        public const int APPLIED_LEVEL_TERTIARY =3 ;
        public const int APPLIED_LEVEL_CRECHE = 4; // Nh� tr?
        public const int APPLIED_LEVEL_KINDER_GARTEN = 5; // M?u gi�o

        //
        public const int CODE_CONFIG_TYPE_PUPIL = 1;
        public const int CODE_CONFIG_TYPE_TEACHER = 2;

        /// <summary>
        /// Đang học
        /// </summary>
        public const int PUPIL_STATUS_STUDYING = 1;
        /// <summary>
        /// Đã tốt nghiệp
        /// </summary>
        public const int PUPIL_STATUS_GRADUATED = 2;
        /// <summary>
        /// Đã chuyển trường
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL = 3;
        /// <summary>
        /// Đã thôi học
        /// </summary>
        public const int PUPIL_STATUS_LEAVED_OFF = 4;
        /// <summary>
        /// Đã chuyển lớp
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_CLASS = 5;
        /// <summary>
        /// Không xác định
        /// </summary>
        public const int PUPIL_STATUS_UNIDENTIFIED = 0;

        public const int STUDYING_JUDGEMENT_RETEST = 2;

        public const int SEMESTER_OF_YEAR_FIRST = 1;

        public const int SEMESTER_OF_YEAR_SECOND = 2;

        public const int SEMESTER_OF_YEAR_ALL = 3;

        public const int SEMESTER_OF_YEAR_BACKTRAINING = 4;

        public const string MARK_TYPE_M = "M";
        public const string MARK_TYPE_P = "P";
        public const string MARK_TYPE_V = "V";

        /// <summary>
        /// Giới tính nam
        /// </summary>
        public const int GENRE_MALE = 1;
        public const string GENRE_MALE_TITLE = "Nam";
        /// <summary>
        /// Giới tính nữ
        /// </summary>
        public const int GENRE_FEMALE = 0;
        public const string GENRE_FEMALE_TITLE = "Nữ";

        public const int CONDUCT_TYPE_D = 1;
        public const int CONDUCT_TYPE_CD = 2;
        public const int CONDUCT_TYPE_GOOD_SECONDARY = 3;
        public const int CONDUCT_TYPE_FAIR_SECONDARY = 4;
        public const int CONDUCT_TYPE_NORMAL_SECONDARY = 5;
        public const int CONDUCT_TYPE_WEAK_SECONDARY = 6;
        public const int CONDUCT_TYPE_GOOD_TERTIARY = 7;
        public const int CONDUCT_TYPE_FAIR_TERTIARY = 8;
        public const int CONDUCT_TYPE_NORMAL_TERTIARY = 9;
        public const int CONDUCT_TYPE_WEAK_TERTIARY = 10;

        public const int CAPACITY_TYPE_EXCELLENT = 1;
        public const int CAPACITY_TYPE_GOOD = 2;
        public const int CAPACITY_TYPE_NORMAL = 3;
        public const int CAPACITY_TYPE_WEAK = 4;
        public const int CAPACITY_TYPE_POOR = 5;
        public const int CAPACITY_TYPE_APLUS = 6;
        public const int CAPACITY_TYPE_A = 7;
        public const int CAPACITY_TYPE_B = 8;
        public const int CAPACITY_TYPE_D = 9;
        public const int CAPACITY_TYPE_CD = 10;

        /// <summary>
        /// Đối tượng nhận tin nhắn
        /// </summary>
        public const int SMS_RECEIVER_PARENT = 1;
        public const int SEND_TYPE_MOBILE = 0;

        public const int FOOD_GROUP_TYPE_ANIMAL = 1;
        public const int FOOD_GROUP_TYPE_PLANT = 2;


        public const string PERIOD_EXAMINATION_1 = "Đợt 1";
        public const string PERIOD_EXAMINATION_2 = "Đợt 2";
        public const string ALERTMESSAGE_FOLDERPATH = "~/Uploads/AlertMessages";
        public const string HEALTHTEST_FOLDERPATH = "~/Uploads/ChartClass";
        public const string HEALTHTEST_FILEPATH = "~/Uploads/";
        public const string TRAINING_TYPE = "Training_Type";

        public const string ACADEMIC_YEAR = "AcademicYear";

        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_CLASS_PROFILE_HEAD = "listClassProfileHead";
        public const string LIST_CLASS_PROFILE_SUBJECTTEACHER = "listClassProfileSubjectTeacher";
        public const string COUNT_PUPIL_OF_CLASS = "countPupilOfClass";
        public const string COUNT_PUPIL_OF_CLASS_MALE = "countPupilOfClassMale";
        public const string COUNT_PUPIL_OF_CLASS_FEMALE = "countPupilOfClassFemale";
        public const string TEACHER_NAME = "teacherName";
        public const string ALERT_MESSAGE = "alertMessage";
        public const string OBJ_MESSAGE = "ObjMessage";
        public const string ALERT_NEWS = "ALERT_NEWS";
        public const string IS_SUPER_ROLE = "IS_SUPER_ROLE";

        public const int PAGESIZE_ALERT = 10;
        public const int PAGESIZE_NEWS = 5;
        public const string ALERT_CURRENT_PAGENO = "ALERT_CURRENT_PAGENO";
        public const string ALERT_PAGESIZE = "ALERT_PAGESIZE";
        public const string NEWS_PAGESIZE = "NEWS_PAGESIZE";
        public const string ALERT_TOTALITEM = "ALERT_TOTALITEM";
        public const string NEWS_TOTALITEM = "NEWS_TOTALITEM";
        public const string TAB_MESSAGE = "TabMessage";

        public const string BOOKMARKED_FUNCTIONS = "BOOKMARKED_FUNCTIONS";
        //tanla4
        //18/11/2014
        //hien thi noi dung cua ban tin moi tin nhat
        public const string NEWS_TITLE = "NEWS_TITLE";
        public const string NEWS_CREATE_DATE = "NEWS_CREATE_DATE";
        public const string NEWS_CREATE_USER = "NEWS_CREATE_USER_ID";
        public const string NEWS_CONTENT = "NEWS_CONTENT";
        public const string NEWS_URL = "NEWS_URL";
        public const string NEWS_ID = "NEWS_ID";
        /// <summary>
        /// nhân viên
        /// </summary>
        public const int WORKGROUPTYPE_EMPLOYEE = 1;
        /// <summary>
        /// giáo viên
        /// </summary>
        public const int WORKGROUPTYPE_TEACHER = 2;

        /// <summary>
        /// cán bộ quản lý
        /// </summary>
        public const int WORKGROUPTYPE_EMPLOYEE_ADMIN = 3;

        //hieu truong
        public const int WORKTYPE_PRINCIPAL = 4;
        public const int WORKTYPE_ASSISTANT_PRINCIPAL = 5;
		
		public const string CONDUCTLEVELRESOLUTION_D = "đ";
        public const string CONDUCTLEVELRESOLUTION_CD = "cđ";

        public const string CONDUCTLEVELDISPLAY_D = "Đ";
        public const string CONDUCTLEVELDISPLAY_CD = "CĐ";

        public const int TIME_OPEN_ACCOUNT = 15;

        public const int TIME_MONTH_CHANGE_PASS = 3;
        public const string CHANGE_PASS_REQUEST = "CHANGE_PASS_REQUEST";
		public const string REQUIRE_ACADEMICYEAR = "REQUIRE_ACADEMICYEAR";
		
		//Contact Group ID
        public const int SCS_CONTACT_GROUP_ALL = 0; //Toan truong
        public const string SCS_CONTACT_GROUP_ALL_SMSCode = "toantruong"; //Toan truong
        public const string SCS_CONTACT_GROUP_ALL_NAME = "Toàn trường";

        //Type SMSCommunication
        public const byte SMS_Communicatoin_Parent_To_Teacher = 1;
        public const byte SMS_Communicatoin_Teacher_To_Parent = 2;
        public const byte SMS_Communicatoin_Teacher_To_Teacher = 3;

        //Type HistorySMS        
        public const int HISTORYSMS_TO_TEACHER_ID = 1;
        public const int HISTORYSMS_TO_DAYMARK_ID = 11;
        public const int HISTORYSMS_TO_WEEKMARK_ID = 12;
        public const int HISTORYSMS_TO_MONTHSMS_ID = 13;
        public const int HISTORYSMS_TO_SEMESTERSMS_ID = 14;
        public const int HISTORYSMS_TO_TRAINING_ID = 15;
        public const int HISTORYSMS_TO_HEALTH_ID = 16;
        public const int HISTORYSMS_TO_PERIOD_HEALTH_ID = 17;
        public const int HISTORYSMS_TO_ACTIVITY_ID = 18;
        public const int HISTORYSMS_TO_GROWTH_ID = 19;
        public const int HISTORYSMS_TO_COMMUNICATION_ID = 20;

        public const string HISTORYSMS_TO_TEACHER_TYPECODE = "TNGV";
        public const string HISTORYSMS_TO_DAYMARK_TYPECODE = "TNDN";
        public const string HISTORYSMS_TO_WEEKMARK_TYPECODE = "TNDT";
        public const string HISTORYSMS_TO_MONTHSMS_TYPECODE = "TNDTH";
        public const string HISTORYSMS_TO_SEMESTERSMS_TYPECODE = "TNHKI";
        public const string HISTORYSMS_TO_TRAINING_TYPECODE = "TNRL";
        public const string HISTORYSMS_TO_HEALTH_TYPECODE = "TNSK";
        public const string HISTORYSMS_TO_PERIOD_HEALTH_TYPECODE = "TNSKDK";
        public const string HISTORYSMS_TO_ACTIVITY_TYPECODE = "TNHD";
        public const string HISTORYSMS_TO_GROWTH_TYPECODE = "TNTT";
        public const string HISTORYSMS_TO_COMMUNICATION_TYPECODE = "TNTD";
        public const string HISTORYSMS_TO_PERIODSMS_TYPECODE = "TNDD";
        public const string HISTORYSMS_TO_LECTUREREGISTER_TYPECODE = "TNBB";
        public const string HISTORYSMS_TO_CALENDAR_TYPECODE = "TNTKB";


        public const int HISTORY_RECEIVER_PUPIL_ID = 0;
        public const int HISTORY_RECEIVER_PARENT_ID = 1;
        public const int HISTORY_RECEIVER_TEACHER_ID = 2;

        //
        public const string LST_NOTIFICATION_INBOX = "lstNotificationInbox";

        public const int NUM_SHORT_CONTENT_SMS = 27;
        public const int NUM_SHORT_CONTENT_SMS_2 = 65;
        /// <summary>
        /// Loại sổ theo dõi sức khoẻ
        /// </summary>
        public const int MONITORINGBOOK_TYPE_DATE = 1; // theo ngày
        public const int MONITORINGBOOK_TYPE_PERIOD = 2; // theo đợt
        public const int MONITORINGBOOK_TYPE_MONTH = 3; // theo tháng

        public const string ACTION_AUDIT_OLD_JSON = "old json object";
        public const string ACTION_AUDIT_NEW_JSON = "new json object";
        public const string ACTION_AUDIT_OBJECTID = "id cua data modify";
        public const string ACTION_AUDIT_DESCRIPTION = "description";
        public const string ACTION_AUDIT_PARAMETER = "parameter";
        public const string ACTION_AUDIT_USERACTION = "UserAction";
        public const string ACTION_AUDIT_USERFUNTION = "UserFuntion";
        public const string ACTION_AUDIT_USERDESCRIPTION = "UserDescription";
        public const string ACTION_AUDIT_ISINSERTLOG = "IsInsertLog";

        //dung cho ghi log
        public const string WILD_LOG = "*#*";

        //physical test
        public const int PHYSICALTEST_MAL_NUTRITION = 1; //suy dinh duong
        public const int PHYSICALTEST_NORMAL_WEIGHT = 2; //binh thuong
        public const int PHYSICALTEST_OVER_WEIGHT = 3; //beo phi
        public const int PHYSICALTEST_UNDENTIFY_NUTRITION = 0; //khong xac dinh

        //TypeConfig Table
        public const int TypeConfig_Weight = 1;
        public const int TypeConfig_Height = 2;
        public const int TypeConfig_BMI = 3;

        public const bool ClassificationCriterialGenre_MALE = true;
        public const bool ClassificationCriterialGenre_FEMALE = false;

        public const string CAPTCHA_COUNT_LOGIN_FAIL = "captcha count login fail";
        public const string CAPTCHA = "captcha";

        // FORMAT DATETIME
        public const string CUSTOM_FORMAT_DATETIME_ES = "HH:mm dd/MM/yyyy";
        public const string CUSTOM_FORMAT_DATETIME_DDMMYY = "dd/MM/yyyy";
        public const string CUSTOM_FORMAT_DATETIME_MMYY = "MM/yyyy";

        public const string Thread_SummedUpSemester = "Thread_SummedUpSemester_";
        public const string Thread_SummedUpPeriod = "Thread_SummedUpPeriod_";
        public const string Thread_PupilRankingSemester = "Thread_PupilRankingSemester_";
        
        public const int Status_Thread_Has = 1;
        public const int Status_Thread_NoHas = 2;

        public const string SPACE = " ";

        public const string LIST_CLASS_TEACHER = "lst class";
        public const string IS_SHOW_CALENDAR = "is show calendar";
        public const string LIST_CALENDAR = "lst calendar";

        /// <summary>
        /// buoi sang
        /// </summary>
        public const byte CALENDAR_SECTION_MORNING = 1;
        /// <summary>
        /// buoi chieu
        /// </summary>
        public const byte CALENDAR_SECTION_AFTERNOON = 2;
        /// <summary>
        /// buoi toi
        /// </summary>
        public const byte CALENDAR_SECTION_EVENING = 3;

        public const byte DAY_OF_WEEK_MONDAY = 2;
        public const byte DAY_OF_WEEK_TUESDAY = 3;
        public const byte DAY_OF_WEEK_WEDNESDAY = 4;
        public const byte DAY_OF_WEEK_THURSDAY = 5;
        public const byte DAY_OF_WEEK_FRIDAY = 6;
        public const byte DAY_OF_WEEK_SATURDAY = 7;
        public const byte DAY_OF_WEEK_SUNDAY = 8;

        public const short MT_STATUS_NOT_SEND = 0;

        public const string URL_PARENT_SITE = "Link parent";

        public const string PASSWORD_REQUIRE_HIGH = "PASSWORD_REQUIRE_HIGH";

        public const int CHAO_CO_SUBJECT = -1;
        public const int SINH_HOAT_SUBJECT = -2;

        public const string WorkGroupTypeBGH = "Cán bộ quản lý";


        public const string PER_CREATE = "PERMISSION_CREATE";
        public const string PER_UPDATE = "PERMISSION_UPDATE";
        public const string PER_DELETE = "PERMISSION_DELETE";

        public const string Folder = "~/Config";
        public const string FileName = "Config.xml";

        // Quanglm bo sung them kiem tra so con diem lon nhan cua moi ky
        public const int MAX_COUNT_MARK_M = 5;
        public const int MAX_COUNT_MARK_P = 5;
        public const int MAX_COUNT_MARK_V = 8;
        public const int MAX_COUNT_MARK_HK = 1;
        //xep tip trong thong bao home
        public const string VIEW_DETAIL = "<div onclick='ViewFullMessage()' class='WanringViewDetail'>xem tiếp</div>";
        public const string VIEW_SHORT_DETAIL = "<div onclick='ViewShortMessage()' class='ShortWanringView'>thu gọn</div>";
        public const int LETTER_NUMBER_LIMIT = 500;

        public const string VIEW_PATTER_HTML = "ViewPatternHtml";
        public const string TEXT_CONTROLLER = "controller";

        public const int DOET_EXAM_APPLIED_TYPE_SCHOOL = 0;
        public const int DOET_EXAM_APPLIED_TYPE_SUB_SUPERVISORY = 1;
        public const string TYPE_SUCCESS = "success";
        public const string TYPE_ERROR = "error";

        public const int WORK_FLOW_CREATE_NEW_DATA = 1;
        public const int WORK_FLOW_MANAGEMENT_CLASS = 2;
        public const int WORK_FLOW_INPUT_SITUATION = 3;
        public const int WORK_FLOW_GENERAL_DATA = 4;
        public const int WORK_FLOW_NEWS_MESSAGE = 5;
        public const int WORK_FLOW_SCHEDULE = 6;
        public const int WORK_FLOW_BUSINESS_FLOWCHART = 7;

        public const string ALL_WORK_FLOW = "ALL_WORK_FLOW";
        public const string DEFAULT_WORK_FLOW = "DEFAULT_WORK_FLOW";
        public const string WORK_FLOW_USER_COUNT = "WORK_FLOW_USER_COUNT";
        public const string WORK_FLOW_USER = "WORK_FLOW_USER";
        public const string CHILD_WORK_FLOW = "CHILD_WORK_FLOW";
    }
}
