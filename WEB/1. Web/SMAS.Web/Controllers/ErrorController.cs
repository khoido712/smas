﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HttpError404()
        {
            return View();
        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult HttpError403()
        {
            return View();
        }

        public ActionResult General()
        {
            return View();
        }
    }
}
