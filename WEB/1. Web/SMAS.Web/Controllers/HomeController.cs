﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SMAS.Web.Models;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Web.Constants;
using AutoMapper;
using SMAS.Web.Areas.RoleArea.Models;
using SMAS.Web.Areas.GroupCatArea.Models;
using SMAS.VTUtils.Utils;
using SMAS.Models.Models.CustomModels;
using SMAS.Web.Areas.AcademicYearArea.Models;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Drawing.Imaging;
using System.IO;
using SMAS.Business.BusinessObject;
using System.Configuration;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;
using System.Net;
using SMAS.Web.Areas.AlertMessageArea;
using Microsoft.Security.Application;
using SMAS.VTUtils.Log;
using System.Data.Objects;
using SMAS.Business.Common.Util;

namespace SMAS.Web.Controllers
{

    public class HomeController : BaseController
    {
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IRoleBusiness roleBusiness;
        private readonly IRoleMenuBusiness RoleMenuBusiness;
        private readonly IGroupCatBusiness groupCatBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        private readonly IMenuBusiness menuBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private readonly IUserGroupBusiness userGroupBusiness;
        private readonly IUserRoleBusiness UserRoleBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAlertMessageBusiness AlertMessageBusiness;
        private readonly IAlertMessageDetailBusiness AlertMessageDetailBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IMIdmappingBusiness MIdmappingBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly IWorkFlowUserBusiness WorkFlowUserBusiness;
        private readonly IWorkFlowBusiness WorkFlowBusiness;
        private readonly ISurveyBusiness SurveyBusiness;
        private readonly IMTBusiness MTBusiness;
        public HomeController(IUserAccountBusiness userBusinessPara,
            IRoleBusiness roleBusinessPara,
            IGroupCatBusiness groupCatBusinessPara,
            IMenuBusiness menuBusinessPara,
            Iaspnet_UsersBusiness aspnet_UsersPara,
            IUserGroupBusiness userGroupPara,
            IGroupMenuBusiness groupMenuBusiness,
            IUserRoleBusiness userRoleBusiness,
            IRoleMenuBusiness roleMenuBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classProfileBusiness,
            ISupervisingDeptBusiness supervisingDeptBusiness,
            IEducationLevelBusiness educationLevelBusiness,
            IEmployeeBusiness employeeBusiness, IPupilProfileBusiness PupilProfileBusiness, IAlertMessageBusiness AlertMessageBusiness, IAlertMessageDetailBusiness AlertMessageDetailBusiness,
            ISchoolSubjectBusiness schoolSubjectBusiness, ITeachingAssignmentBusiness teachingAssignmentBusiness,
            IClassSubjectBusiness classSubjectBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness,
            ISummedUpRecordBusiness summedUpRecordBusiness, IPupilRankingBusiness pupilRankingBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            ICalendarBusiness calendarBusiness,
            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
            IMIdmappingBusiness MIdmappingBusiness,
            IBookmarkedFunctionBusiness bookmarkedFunctionBusiness,
            IWorkFlowUserBusiness workFlowUserBusiness,
            IWorkFlowBusiness workFlowBusiness,
            ISurveyBusiness SurveyBusiness,
            IMTBusiness MTBusiness
            )
        {
            this.UserAccountBusiness = userBusinessPara;
            this.roleBusiness = roleBusinessPara;
            this.groupCatBusiness = groupCatBusinessPara;
            this.menuBusiness = menuBusinessPara;
            this.aspnet_UsersBusiness = aspnet_UsersPara;
            this.userGroupBusiness = userGroupPara;
            this.GroupMenuBusiness = groupMenuBusiness;
            this.UserRoleBusiness = userRoleBusiness;
            this.RoleMenuBusiness = roleMenuBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AlertMessageBusiness = AlertMessageBusiness;
            this.AlertMessageDetailBusiness = AlertMessageDetailBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.PupilRankingBusiness = pupilRankingBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.MIdmappingBusiness = MIdmappingBusiness;
            this.BookmarkedFunctionBusiness = bookmarkedFunctionBusiness;
            this.WorkFlowUserBusiness = workFlowUserBusiness;
            this.WorkFlowBusiness = workFlowBusiness;
            this.SurveyBusiness = SurveyBusiness;
            this.MTBusiness = MTBusiness;
            Mapper.CreateMap<RoleViewModel, Role>();
            Mapper.CreateMap<Role, RoleViewModel>();
            Mapper.CreateMap<GroupCatViewModel, GroupCat>();
            Mapper.CreateMap<GroupCat, GroupCatViewModel>();
            Mapper.CreateMap<AcademicYearForm, AcademicYear>();
            Mapper.CreateMap<AcademicYear, AcademicYearForm>();

        }

        public const int LOGIN_FAIL_COUNT_FOR_CAPCHA = 2;

        #region Action in Page
        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult SetVariable(string key, string value)
        {
            Session[key] = value;

            return this.Json(new { success = true });
        }
        //
        [HttpGet]

        public ActionResult Index(int? productVersion = null)
        {
            //lay thong tin trong session
            SessionInfo thisSession = AddSession();
            List<SessionInfo> lsSessionInfo = new List<SessionInfo>();
            lsSessionInfo.Add(thisSession);
            ViewData["LIST_SESSION"] = lsSessionInfo;
            GlobalInfo global = new GlobalInfo();
            AcademicYear objAcademicYear = null;
            if (productVersion.HasValue && productVersion.Value > 0)
            {
                SetRoleAndMenu(_globalInfo.RoleID, ControllerContext.HttpContext);
            }
            if (_globalInfo.AcademicYearID.HasValue)
                objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            bool isChangePass = false;// (bool)Session[GlobalConstants.CHANGE_PASS_REQUEST];

            if (!_globalInfo.IsSystemAdmin && !isChangePass && (global.IsAdminSchoolRole || global.IsEmployeeManager || global.IsRolePrincipal || global.IsSuperVisingDeptRole || global.IsSubSuperVisingDeptRole)
                || global.IsSchoolRole && global.EmployeeID != null)
            {
                return HomePageOfSchoolAdmin();
            }
            return View("Index");
        }

        #region HOME PAGE ADMIN
        [HttpGet]
        public ActionResult HomePageOfSchoolAdmin()
        {
            //Hungnd neu truong chua khai bao nam hoc redirect den chuc nang khai bao nam hoc
            if (_globalInfo.AcademicYearID == null)
            {
                if ((!_globalInfo.IsSubSuperVisingDeptRole && !_globalInfo.IsSuperVisingDeptRole))
                {
                    return RedirectToAction("Index", "AcademicYear", new { area = "AcademicYearArea" });
                }
            }
            //End

            #endregion
            ViewData[GlobalConstants.BOOKMARKED_FUNCTIONS] = GetBookmarkedFunc();
            #region WorkFlow
            List<WorkFlowUserBO> lstWorkFlowUser = new List<WorkFlowUserBO>();
            IDictionary<string, object> dic;
            if (_globalInfo.IsSchoolRole)
            {
                dic = new Dictionary<string, object>()
                                        {
                                            {"UserID",_globalInfo.UserAccountID},
                                            {"AppliedLevel", _globalInfo.AppliedLevel},
                                            {"ProductVersion",_globalInfo.ProductVersionID}
                                        };
            }
            else
            {
                dic = new Dictionary<string, object>()
                                        {
                                            {"UserID",_globalInfo.UserAccountID}
                                        };
            }
            lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).OrderBy(o => o.OrderID).ToList();
            if (_globalInfo.IsAdmin)
            {
                lstWorkFlowUser = lstWorkFlowUser.Where(o => o.AvailableAdmin).ToList();
            }
            else if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                lstWorkFlowUser = lstWorkFlowUser.Where(o => o.AvailableSup).ToList();
            }
            else
            {
                lstWorkFlowUser = lstWorkFlowUser.Where(o => o.AvailableTeacher).ToList();
            }

            List<WorkFlowBO> lstWorkFlow = (from wf in Business.Common.StaticData.listWorkFlow
                                            select new WorkFlowBO
                                            {
                                                WorkFlowID = wf.WorkFlowID,
                                                WorkFlowName = wf.WorkFlowName,
                                                ParentID = wf.ParentID,
                                                AppliedLevelID = wf.AppliedLevel,
                                                ProductVersion = wf.ProductVersion,
                                                Url = wf.Url,
                                                ViewName = wf.ViewName,
                                                OrderId = wf.OrderNo
                                            }).ToList();
            ViewData["lstWorkflow"] = lstWorkFlow;
            ViewData["lstWorkFlowUser"] = lstWorkFlowUser;
            #endregion

            #region tính số tin đã đọc chưa đọc
            int countMailBox = 0;
            int countMessageBox = 0;
            DateTime fromDate = DateTime.Now.AddMonths(-2);
            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> iqAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageDetailID = iqAlertMessageDetail.Select(p => p.AlertMessageID).Distinct().ToList();

            IDictionary<string, object> dicAlert = new Dictionary<string, object>();
            dicAlert["IsActive"] = true;
            dicAlert["isAdmin"] = true;
            IQueryable<AlertMessage> iqAlertMessage = AlertMessageBusiness.Search(dicAlert).Where(o => o.PublishedDate >= fromDate && o.IsPublish == true);
            countMessageBox = iqAlertMessage.Count(p => !lstAlertMessageDetailID.Contains(p.AlertMessageID));
            ViewData[GlobalConstants.NEWMESSAGENO] = countMessageBox;
            IQueryable<AlertMessageBO> listNews = GetNews();
            countMailBox = listNews.Count(p => p.PublishedDate >= fromDate && !lstAlertMessageDetailID.Contains(p.AlertMessageID));
            ViewData[SMAS.Web.Constants.GlobalConstants.NEWNEWSNO] = countMailBox;
            ViewData[SMAS.Web.Constants.GlobalConstants.IS_ALERT_MESSAGE] = (countMessageBox > 0 || countMailBox > 0);

            #endregion
            return View("HomePageOfSchoolAdmin");
        }

        //tanla4
        //14/11/2014
        //xu li hien thi bang tin
        public void NewsBoards()
        {
            IQueryable<AlertMessageBO> listNews = GetNews();
            ViewData[GlobalConstants.NEWS_TOTALITEM] = listNews.Count();
            ViewData[GlobalConstants.NEWS_PAGESIZE] = GlobalConstants.PAGESIZE_NEWS;

            //xu li de biet tin da duoc doc hay chua
            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> lstNewsDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageID = lstNewsDetail.Select(p => p.AlertMessageID).Distinct().ToList();
            int count = 0;//dem so dong danh dau la tin da doc
            bool isNewNews = false;
            List<AlertMessageBO> lstAMtake = listNews.Take(GlobalConstants.PAGESIZE_NEWS).ToList();
            foreach (var item in lstAMtake)
            {
                count = lstNewsDetail.Count(s => s.AlertMessageID == item.AlertMessageID);
                if (count > 0)//la tin da doc
                {
                    item.isRead = true;
                }
                else//la tin chua doc
                {
                    item.isRead = false;
                    isNewNews = true;
                }
            }
            //xu li hien thi so tin moi
            ViewData[GlobalConstants.NEWNEWSNO] = listNews.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));
            //xu li mo hoac dong khung thong bao            
            ViewData["isNewNews"] = isNewNews;

            ViewData[GlobalConstants.ALERT_NEWS] = lstAMtake;

            //tanla4
            //18/11/2014
            //xu li hien thi noi dung cua bang tin duoc phat hanh gan day nhat, khong quan tam tin do da doc hay chua
            if (lstAMtake.Count > 0)
            {
                AlertMessageBO firstNews = lstAMtake.FirstOrDefault();
                ViewData[GlobalConstants.NEWS_ID] = firstNews.AlertMessageID;
                ViewData[GlobalConstants.NEWS_TITLE] = Sanitizer.GetSafeHtmlFragment(firstNews.Title);
                ViewData[GlobalConstants.NEWS_CREATE_DATE] = firstNews.PublishedDate.ToString("dd/MM/yyyy");
                ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(firstNews.CreatedUserID);//chuyen sang ten nguoi tao bang tin
                ViewData[GlobalConstants.NEWS_CONTENT] = Sanitizer.GetSafeHtmlFragment(firstNews.ContentMessage);
                ViewData[GlobalConstants.NEWS_URL] = Sanitizer.GetSafeHtmlFragment(firstNews.FileUrl);
            }
        }

        //tanla4
        //18/11/2014
        //lay username ung voi userid
        public string GetUserNameByUserID(int createUserId)
        {
            string sender = Res.Get("Message_Label_SystemAdmin");
            if (createUserId == 0)
            {
                return sender;
            }

            UserAccount userAccount = UserAccountBusiness.Find(createUserId);

            if (userAccount == null)
            {
                return sender;
            }

            if (userAccount.IsAdmin)
            {
                var rolesOfUser =
                    UserRoleBusiness.GetRolesOfUser(userAccount.UserAccountID).Select(s => s.RoleID).ToList();
                if (rolesOfUser.Count == 0)
                {
                    return sender;
                }
                //Neu la quan tri he thong
                if (rolesOfUser.Contains(GlobalConstants.ROLE_CAPCAO))
                {
                    return sender;
                }
                if (userAccount.SchoolProfiles.Any())
                {
                    return Res.Get("Message_Label_GroupUserSchoolAdmin");
                }
                if (rolesOfUser.Contains(GlobalConstants.ROLE_SO))
                {
                    return Res.Get("Message_Label_GroupUserSuperVisingDept");
                }
                if (rolesOfUser.Contains(GlobalConstants.ROLE_PHONG))
                {
                    return Res.Get("Message_Label_GroupUserDeptAdmin");
                }
            }
            else
            {
                GroupCat objGroupCat = userGroupBusiness.GetGroupsOfUser(createUserId).FirstOrDefault();
                if (objGroupCat == null)
                {
                    return "";
                }
                if (userAccount.Employee == null)
                {
                    return "";
                }

                SchoolProfile objSchoolProfile = userAccount.Employee.SchoolProfile;

                //Neu la giao vien
                if (objSchoolProfile != null)
                {
                    return userAccount.Employee.FullName;
                }

                if (objGroupCat.RoleID == GlobalConstants.ROLE_SO)
                {
                    return Res.Get("Message_Label_EmployeeSuperVisingDept");
                }
                if (objGroupCat.RoleID == GlobalConstants.ROLE_PHONG)
                {
                    return Res.Get("Message_Label_EmployeeSubSuperVisingDept");
                }
            }

            return sender;
        }

        //tanla4
        //19/11/2014
        //lay thong tin hien thi theo trang
        public PartialViewResult GetNewsPage(int pageNo, int tabID)
        {
            IQueryable<AlertMessageBO> iqMailBox = new List<AlertMessageBO>().AsQueryable();
            IQueryable<AlertMessageBO> iqMessageBox = new List<AlertMessageBO>().AsQueryable();
            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> lstAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageID = lstAlertMessageDetail.Select(p => p.AlertMessageID).Distinct().ToList();
            iqMailBox = this.GetNews();
            ViewData[GlobalConstants.NEWNEWSNO] = iqMailBox.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));
            IDictionary<string, object> dicAlert = new Dictionary<string, object>();
            dicAlert["IsActive"] = true;
            dicAlert["isAdmin"] = true;

            iqMessageBox = (from p in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish == true)
                            select new AlertMessageBO
                            {
                                AlertMessageID = p.AlertMessageID,
                                Title = p.Title,
                                TypeID = p.TypeID,
                                SendName = "SMAS",
                                ContentMessage = p.ContentMessage,
                                PublishedDate = p.PublishedDate,
                                FileUrl = p.FileUrl,
                                CreatedUserID = p.CreatedUserID
                            }).OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID);

            ViewData[GlobalConstants.NEWMESSAGENO] = iqMessageBox.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));

            int count = 0;//dem so dong danh dau la tin da doc
            List<AlertMessageBO> lstAMtake = new List<AlertMessageBO>();
            if (tabID == 1)
            {
                lstAMtake = iqMailBox.Skip((pageNo - 1) * GlobalConstants.PAGESIZE_NEWS).Take(GlobalConstants.PAGESIZE_NEWS).ToList();
            }
            else
            {
                lstAMtake = iqMessageBox.Skip((pageNo - 1) * GlobalConstants.PAGESIZE_NEWS).Take(GlobalConstants.PAGESIZE_NEWS).ToList();
            }

            foreach (var item in lstAMtake)
            {
                count = lstAlertMessageDetail.Count(s => s.AlertMessageID == item.AlertMessageID);
                if (count > 0)//la tin da doc
                {
                    item.isRead = true;
                }
                else//la tin chua doc
                {
                    item.isRead = false;
                }
            }
            //xu li hien thi so tin moi
            ViewData[GlobalConstants.TAB_MESSAGE] = tabID;
            if (iqMailBox.Any() || iqMessageBox.Any())
            {
                ViewData[GlobalConstants.NEWS_TOTALITEM] = tabID == 1 ? iqMailBox.Count() : iqMessageBox.Count();
                ViewData[GlobalConstants.ALERT_MESSAGE] = lstAMtake;
                AlertMessageBO objAlert = lstAMtake.FirstOrDefault();
                ViewData[GlobalConstants.OBJ_MESSAGE] = objAlert;
                ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(objAlert.CreatedUserID);
            }
            return PartialView("_FrameViewMailBox");

        }

        //tanla4
        //18/11/2014
        //lay noi dung cua bang tin duoc chon va load lai danh sach bang tin
        public PartialViewResult GetNewsContent(int id, bool isRead, int pageNo, int tabID)
        {
            //insert vao csdl danh dau bang tin da duoc doc
            if (!isRead)
            {
                AlertMessageDetail alertMessageDetail = new AlertMessageDetail();
                alertMessageDetail.AlertMessageID = id;
                alertMessageDetail.UserID = _globalInfo.UserAccountID;
                if (_globalInfo.IsAdmin)//admin
                {
                    alertMessageDetail.ReceiverTypeID = 1;
                }
                else if (_globalInfo.IsSuperVisingDeptRole)//so
                {
                    alertMessageDetail.ReceiverTypeID = 2;
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)//phong
                {
                    alertMessageDetail.ReceiverTypeID = 3;
                }
                else//truong
                {
                    alertMessageDetail.ReceiverTypeID = 4;
                }
                alertMessageDetail.Status = GlobalConstants.ISREADMESSAGE;
                alertMessageDetail.CreatedDate = DateTime.Now;
                //alertMessageDetail.ModifiedDate = null;
                AlertMessageDetailBusiness.Insert(alertMessageDetail);
                AlertMessageDetailBusiness.Save();
            }
            IQueryable<AlertMessageBO> iqMailBox = new List<AlertMessageBO>().AsQueryable();
            IQueryable<AlertMessageBO> iqMessageBox = new List<AlertMessageBO>().AsQueryable();
            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> lstAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageID = lstAlertMessageDetail.Select(p => p.AlertMessageID).Distinct().ToList();
            iqMailBox = this.GetNews();
            ViewData[GlobalConstants.NEWNEWSNO] = iqMailBox.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));

            IDictionary<string, object> dicAlert = new Dictionary<string, object>();
            dicAlert["IsActive"] = true;
            dicAlert["isAdmin"] = true;
            iqMessageBox = (from p in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish == true)
                            select new AlertMessageBO
                            {
                                AlertMessageID = p.AlertMessageID,
                                Title = p.Title,
                                TypeID = p.TypeID,
                                SendName = "SMAS",
                                ContentMessage = p.ContentMessage,
                                PublishedDate = p.PublishedDate,
                                FileUrl = p.FileUrl,
                                CreatedUserID = p.CreatedUserID
                            }).OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID);
            ViewData[GlobalConstants.NEWMESSAGENO] = iqMessageBox.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));

            ViewData[GlobalConstants.NEWS_PAGESIZE] = GlobalConstants.PAGESIZE_NEWS;
            int count = 0;//dem so dong danh dau la tin da doc
            int pageSize = GlobalConstants.PAGESIZE_NEWS;
            List<AlertMessageBO> lstAMtake = new List<AlertMessageBO>();
            if (tabID == 1)
            {
                lstAMtake = iqMailBox.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                lstAMtake = iqMessageBox.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            }

            foreach (var item in lstAMtake)
            {
                count = lstAlertMessageDetail.Count(s => s.AlertMessageID == item.AlertMessageID);
                if (count > 0)//la tin da doc
                {
                    item.isRead = true;
                }
                else//la tin chua doc
                {
                    item.isRead = false;
                }
            }
            ViewData[GlobalConstants.TAB_MESSAGE] = tabID;
            if (iqMailBox.Any() || iqMessageBox.Any())
            {
                ViewData[GlobalConstants.NEWS_TOTALITEM] = tabID == 1 ? iqMailBox.Count() : iqMessageBox.Count();
                ViewData[GlobalConstants.ALERT_MESSAGE] = lstAMtake;
                AlertMessageBO objAlert = lstAMtake.FirstOrDefault(p => p.AlertMessageID == id);
                ViewData[GlobalConstants.OBJ_MESSAGE] = objAlert;
                ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(objAlert.CreatedUserID);
            }
            return PartialView("_FrameViewMailBox");
        }

        public JsonResult AddShowDiv(int DivID)
        {
            //Hungnd 16/05/2013 Add showing div in to session
            List<int> listSession = (List<int>)Session["SessionShowDiv"];
            listSession.Add(DivID);
            Session["SessionShowDiv"] = listSession;
            //End Add
            return Json("");
        }

        public JsonResult RemoveShowDiv(int DivID)
        {
            //Hungnd 16/05/2013 removing show div in to session
            if (Session["SessionShowDiv"] != null)
            {
                List<int> listSession = (List<int>)Session["SessionShowDiv"];
                listSession.Remove(DivID);
                Session["SessionShowDiv"] = listSession;
            }
            ////End Remove
            return Json("");
        }


        private List<BookmarkedFunctionBO> GetBookmarkedFunc()
        {
            List<Menu> menuByRole = Session[GlobalConstants.MENUS] as List<SMAS.Models.Models.Menu>;


            List<BookmarkedFunction> iq = new List<BookmarkedFunction>();
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"UserID",_globalInfo.UserAccountID}
                };
                iq = BookmarkedFunctionBusiness.Search(dic).ToList();
            }
            else
            {
                iq = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.Value,
                _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.UserAccountID);
            }


            List<BookmarkedFunctionBO> lstBookmarkedFuncs = (from bf in iq
                                                             join mn in menuByRole on bf.MenuID equals mn.MenuID
                                                             select new BookmarkedFunctionBO()
                                                             {
                                                                 BookmarkedFunctionID = bf.BookmarkedFunctionID,
                                                                 FunctionName = bf.FunctionName,
                                                                 SchoolID = bf.SchoolID,
                                                                 MenuID = mn.MenuID,
                                                                 IconLink = mn.ImageURL == null || mn.ImageURL.Contains("info.png") ? "/Content/images/empty-icon.png" : mn.ImageURL,
                                                                 Order = bf.Order,
                                                                 Url = mn.URL
                                                             }).OrderBy(o => o.Order != 0).ThenBy(o => o.Order).ToList();
            return lstBookmarkedFuncs;

        }

        private List<ItemMenuModel> getListItemMenu(List<Menu> lsMenu)
        {
            //List<Menu> lsMenuTmp = this.MenuBusiness.All.Where(m => m.IsActive == true).ToList();
            MenuBuilder menuBuilder = new MenuBuilder(lsMenu);
            List<ItemMenuModel> menuModelLevel1 = menuBuilder.GetParentMenu().Where(p => p.MenuChildren.Count > 0).ToList();
            return menuModelLevel1;
        }


        public ActionResult LoadManagementFunc()
        {
            List<Menu> menuByRole = Session[GlobalConstants.MENUS] as List<SMAS.Models.Models.Menu>;

            List<ItemMenuModel> LsMenuModel = getListItemMenu(menuByRole.Where(o => o.IsVisible).ToList());

            ViewData[GlobalConstants.BOOKMARKED_FUNCTIONS] = GetBookmarkedFunc();
            return PartialView("_BookmarkedFuncMng", LsMenuModel);
        }

        public ActionResult _AjaxLoadMenu(Telerik.Web.Mvc.UI.TreeViewItem node)
        {
            int id = int.Parse(node.Value);
            List<int> lstBmID = GetBookmarkedFunc().Select(o => o.MenuID).ToList();
            List<Menu> result = menuBusiness.All.Where(m => m.IsActive == true && m.ParentID == id).ToList();
            List<Telerik.Web.Mvc.UI.TreeViewItemModel> ListMenu = result.Select(o => new Telerik.Web.Mvc.UI.TreeViewItemModel
            {
                Text = o.MenuName,
                Value = o.MenuID.ToString(),
                LoadOnDemand = true,
                ImageUrl = o.ImageURL,
                Checked = lstBmID.Contains(o.MenuID)
            }).ToList();

            foreach (Telerik.Web.Mvc.UI.TreeViewItemModel m in ListMenu)
            {
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", m.Text);
                m.Text = string.IsNullOrEmpty(name) ? m.Text : name;
            }
            return new JsonResult { Data = ListMenu };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveBookmarkedFunc(List<int> CheckMenu)
        {

            //Xoa cac bookmark cu
            List<BookmarkedFunction> lstDeleteBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);
            BookmarkedFunctionBusiness.DeleteAll(lstDeleteBm);

            if (CheckMenu != null && CheckMenu.Count > 0)
            {
                List<Menu> menuByRole = Session[GlobalConstants.MENUS] as List<SMAS.Models.Models.Menu>;

                for (int i = 0; i < CheckMenu.Count; i++)
                {
                    Menu menuItem = menuByRole.FirstOrDefault(o => o.MenuID == CheckMenu[i]);
                    if (menuItem != null)
                    {
                        BookmarkedFunction bf = new BookmarkedFunction();
                        bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        bf.AcademicYearID = _globalInfo.AcademicYearID;
                        bf.LevelID = _globalInfo.AppliedLevel;
                        bf.UserID = _globalInfo.UserAccountID;
                        bf.MenuID = menuItem.MenuID;
                        bf.Semester = _globalInfo.Semester;
                        string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", menuItem.MenuName);
                        bf.FunctionName = string.IsNullOrEmpty(name) ? menuItem.MenuName : name;
                        bf.Order = i + 1;
                        bf.CreatedDate = DateTime.Now;
                        BookmarkedFunctionBusiness.Insert(bf);
                    }
                }
            }
            BookmarkedFunctionBusiness.Save();



            List<BookmarkedFunctionBO> lstBm = GetBookmarkedFunc();
            return PartialView("_SideBar", lstBm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RemoveBookmarkedFunc(int? BookmarkFuncID)
        {

            BookmarkedFunctionBusiness.Delete(BookmarkFuncID);
            BookmarkedFunctionBusiness.Save();
            return Json(new { Type = "success" });


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SortBookmarkedFunc(string lstBookmarkedFuncID)
        {
            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.Value,
                   _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.UserAccountID);

            lstBm.ForEach(o => o.Order = 0);

            List<int> lstBmID = GetIDsFromString(lstBookmarkedFuncID);

            for (int i = 0; i < lstBmID.Count; i++)
            {
                BookmarkedFunction bm = lstBm.FirstOrDefault(o => o.BookmarkedFunctionID == lstBmID[i]);
                if (bm != null)
                {
                    bm.Order = i + 1;
                }

                BookmarkedFunctionBusiness.Update(bm);
            }

            BookmarkedFunctionBusiness.Save();

            return Json(new { Type = "success" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SortWorkBox(string lstWorkFlowId)
        {
            List<WorkFlowUser> lstWfu = WorkFlowUserBusiness.All.Where(o => o.UserID == _globalInfo.UserAccountID).ToList();
            List<int> lstWfuID = GetIDsFromString(lstWorkFlowId);
            for (int i = 0; i < lstWfuID.Count; i++)
            {
                WorkFlowUser wfu = lstWfu.FirstOrDefault(o => o.WorkFlowID == lstWfuID[i]);
                if (wfu != null)
                {
                    wfu.OrderID = i + 1;
                    WorkFlowUserBusiness.Update(wfu);
                }
            }

            WorkFlowUserBusiness.Save();
            return Json(new { Type = "success" });
        }

        [HttpGet]
        public PartialViewResult ConfigWorkBox(int? workFlowId)
        {
            //string appliedLevel = _globalInfo.AppliedLevel.ToString();
            //string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow = GetRootWorkFLowByUser();

            ViewData[GlobalConstants.ALL_WORK_FLOW] = lstWorkFlow;
            ViewData[GlobalConstants.DEFAULT_WORK_FLOW] = workFlowId;

            return PartialView("_ConfigWorkBox");
        }

        public PartialViewResult ConfigNewsBox()
        {
            int workFlowId = GlobalConstants.WORK_FLOW_NEWS_MESSAGE;

            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow = GetAllWorkFLowByUser();

            List<WorkFlow> lstChildWorkFlow = lstWorkFlow.Where(o => o.ParentID == workFlowId).ToList();

            ViewData[GlobalConstants.CHILD_WORK_FLOW] = lstChildWorkFlow;

            IDictionary<string, object> dic = new Dictionary<string, object>()
                                        {
                                            {"UserID",_globalInfo.UserAccountID}
                                        };
            List<WorkFlowUserBO> lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).ToList();

            ViewData[GlobalConstants.WORK_FLOW_USER_COUNT] = lstWorkFlowUser.ToList().Count;

            WorkFlowUserBO objWorkFlowUser = lstWorkFlowUser.FirstOrDefault(o => o.WorkFlowID == workFlowId);
            ViewData[GlobalConstants.WORK_FLOW_USER] = objWorkFlowUser;

            return PartialView("__ConfigNewsBox");

        }

        public PartialViewResult ConfigBeginYearDataBox()
        {
            int workFlowId = GlobalConstants.WORK_FLOW_CREATE_NEW_DATA;

            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow = GetAllWorkFLowByUser();

            List<WorkFlow> lstChildWorkFlow = lstWorkFlow.Where(o => o.ParentID == workFlowId).ToList();
            ViewData[GlobalConstants.CHILD_WORK_FLOW] = lstChildWorkFlow;

            IDictionary<string, object> dic = new Dictionary<string, object>()
                                        {
                                            {"UserID",_globalInfo.UserAccountID}
                                        };
            List<WorkFlowUserBO> lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).ToList();

            ViewData[GlobalConstants.WORK_FLOW_USER_COUNT] = lstWorkFlowUser.ToList().Count;

            WorkFlowUserBO objWorkFlowUser = lstWorkFlowUser.FirstOrDefault(o => o.WorkFlowID == workFlowId);
            ViewData[GlobalConstants.WORK_FLOW_USER] = objWorkFlowUser;

            return PartialView("_ConfigBeginYearDataBox");

        }

        public PartialViewResult ConfigScheduleBox()
        {
            int workFlowId = GlobalConstants.WORK_FLOW_SCHEDULE;

            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow = GetAllWorkFLowByUser();

            List<WorkFlow> lstChildWorkFlow = lstWorkFlow.Where(o => o.ParentID == workFlowId).ToList();
            ViewData[GlobalConstants.CHILD_WORK_FLOW] = lstChildWorkFlow;

            IDictionary<string, object> dic = new Dictionary<string, object>()
                                        {
                                            {"UserID",_globalInfo.UserAccountID}
                                        };
            List<WorkFlowUserBO> lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).ToList();

            ViewData[GlobalConstants.WORK_FLOW_USER_COUNT] = lstWorkFlowUser.ToList().Count;

            WorkFlowUserBO objWorkFlowUser = lstWorkFlowUser.FirstOrDefault(o => o.WorkFlowID == workFlowId);
            ViewData[GlobalConstants.WORK_FLOW_USER] = objWorkFlowUser;

            return PartialView("_ConfigScheduleBox");

        }

        private List<WorkFlow> GetAllWorkFLowByUser()
        {
            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow;
            if (_globalInfo.IsSchoolRole)
            {
                lstWorkFlow = WorkFlowBusiness.All.Where(o => o.IsActive
                                                                && o.AppliedLevel.Contains(appliedLevel)
                                                                && o.ProductVersion.Contains(productVersion)).OrderBy(o => o.OrderNo).ToList();
            }
            else
            {
                lstWorkFlow = WorkFlowBusiness.All.Where(o => o.IsActive).OrderBy(o => o.OrderNo).ToList();
            }

            if (_globalInfo.IsSchoolRole)
            {
                if (_globalInfo.IsAdmin)
                {
                    lstWorkFlow = lstWorkFlow.Where(o => o.AvailableAdmin == true).ToList();
                }
                else
                {
                    lstWorkFlow = lstWorkFlow.Where(o => o.AvailableTeacher == true).ToList();
                }
            }
            else if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                lstWorkFlow = lstWorkFlow.Where(o => o.AvailableSup == true).ToList();
            }

            return lstWorkFlow;
        }

        private List<WorkFlow> GetRootWorkFLowByUser()
        {
            string appliedLevel = _globalInfo.AppliedLevel.ToString();
            string productVersion = _globalInfo.ProductVersionID.ToString();
            List<WorkFlow> lstWorkFlow;
            if (_globalInfo.IsSchoolRole)
            {
                lstWorkFlow = WorkFlowBusiness.All.Where(o => o.IsActive
                                                                && o.ParentID == 0
                                                                && o.AppliedLevel.Contains(appliedLevel)
                                                                && o.ProductVersion.Contains(productVersion)).OrderBy(o => o.OrderNo).ToList();
            }
            else
            {
                lstWorkFlow = WorkFlowBusiness.All.Where(o => o.IsActive
                                                                && o.ParentID == 0).OrderBy(o => o.OrderNo).ToList();
            }

            if (_globalInfo.IsSchoolRole)
            {
                if (_globalInfo.IsAdmin)
                {
                    lstWorkFlow = lstWorkFlow.Where(o => o.AvailableAdmin == true).ToList();
                }
                else
                {
                    lstWorkFlow = lstWorkFlow.Where(o => o.AvailableTeacher == true).ToList();
                }
            }
            else if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                lstWorkFlow = lstWorkFlow.Where(o => o.AvailableSup == true).ToList();
            }

            return lstWorkFlow;
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveWorkBoxConfig(FormCollection frm)
        {
            WorkFlowUserBusiness.SetAutoDetectChangesEnabled(false);
            int WorkFlowID = int.Parse(frm["WorkFlowID"]);
            int DefaultInfo = int.Parse(frm["DefaultInfo"]);
            int OrderID = int.Parse(frm["OrderID"]);

            List<WorkFlowUser> lstWorkFlow = WorkFlowUserBusiness.All.Where(o => o.UserID == _globalInfo.UserAccountID).OrderBy(o => o.OrderID).ToList();
            WorkFlowUser wfu = lstWorkFlow.FirstOrDefault(o => o.WorkFlowID == WorkFlowID);
            bool isUpdate = wfu != null ? true : false;
            int oldOrder = wfu != null ? wfu.OrderID : 0;

            if (WorkFlowID == GlobalConstants.WORK_FLOW_CREATE_NEW_DATA || WorkFlowID == GlobalConstants.WORK_FLOW_SCHEDULE)
            {
                if (wfu != null)
                {
                    wfu.DefaultTab = DefaultInfo;
                    wfu.OrderID = OrderID;
                    wfu.ModifiedDate = DateTime.Now;
                    WorkFlowUserBusiness.Update(wfu);
                }
                else
                {
                    wfu = new WorkFlowUser();
                    wfu.CreatedDate = DateTime.Now;
                    wfu.DefaultTab = DefaultInfo;
                    wfu.OrderID = OrderID;
                    wfu.UserID = _globalInfo.UserAccountID;
                    wfu.WorkFlowID = WorkFlowID;

                    WorkFlowUserBusiness.Insert(wfu);
                }
            }
            else if (WorkFlowID == GlobalConstants.WORK_FLOW_NEWS_MESSAGE)
            {
                bool isAutoAlert = Convert.ToBoolean(frm["AutoDisplay"]);

                if (wfu != null)
                {
                    wfu.DefaultTab = DefaultInfo;
                    wfu.OrderID = OrderID;
                    wfu.IsAutoAlert = isAutoAlert;
                    wfu.ModifiedDate = DateTime.Now;
                    WorkFlowUserBusiness.Update(wfu);
                }
                else
                {
                    wfu = new WorkFlowUser();
                    wfu.CreatedDate = DateTime.Now;
                    wfu.DefaultTab = DefaultInfo;
                    wfu.IsAutoAlert = isAutoAlert;
                    wfu.OrderID = OrderID;
                    wfu.UserID = _globalInfo.UserAccountID;
                    wfu.WorkFlowID = WorkFlowID;

                    WorkFlowUserBusiness.Insert(wfu);
                }
            }

            //cap nhat lai order cho cac workflow khac
            if (isUpdate)
            {
                if (oldOrder != OrderID)
                {
                    for (int i = 0; i < lstWorkFlow.Count; i++)
                    {
                        WorkFlowUser otherWfu = lstWorkFlow[i];
                        if (otherWfu != wfu)
                        {
                            if (oldOrder < OrderID && otherWfu.OrderID <= OrderID && otherWfu.OrderID > oldOrder)
                            {
                                otherWfu.OrderID--;
                            }
                            else if (oldOrder > OrderID && otherWfu.OrderID >= OrderID && otherWfu.OrderID < oldOrder)
                            {
                                otherWfu.OrderID++;
                            }

                            WorkFlowUserBusiness.Update(otherWfu);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < lstWorkFlow.Count; i++)
                {
                    WorkFlowUser otherWfu = lstWorkFlow[i];
                    if (otherWfu.OrderID >= OrderID)
                    {
                        otherWfu.OrderID++;
                        WorkFlowUserBusiness.Update(otherWfu);
                    }

                }
            }

            WorkFlowUserBusiness.Save();
            WorkFlowUserBusiness.SetAutoDetectChangesEnabled(true);
            if (isUpdate)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkBoxConfig(FormCollection frm)
        {
            int WorkFlowID = int.Parse(frm["WorkFlowID"]);
            WorkFlowUserBusiness.SetAutoDetectChangesEnabled(false);
            WorkFlowUser wfu = WorkFlowUserBusiness.All.FirstOrDefault(o => o.UserID == _globalInfo.UserAccountID
                                                                && o.WorkFlowID == WorkFlowID);

            if (wfu != null)
            {
                WorkFlowUserBusiness.Delete(wfu.WorkFlowUserID);
            }


            List<WorkFlowUser> lstWfuAfter = WorkFlowUserBusiness.All.Where(o => o.UserID == _globalInfo.UserAccountID
                && o.OrderID > wfu.OrderID).ToList();
            for (int i = 0; i < lstWfuAfter.Count; i++)
            {
                lstWfuAfter[i].OrderID--;
                WorkFlowUserBusiness.Update(lstWfuAfter[i]);
            }

            WorkFlowUserBusiness.Save();
            WorkFlowUserBusiness.SetAutoDetectChangesEnabled(true);
            return Json(new JsonMessage(Res.Get("Home_Label_DeleteBoxMessage")));
        }

        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }


        public PartialViewResult GetAlertMessage(int? pageNo, int newsNo, int pageSize = SMAS.Web.Constants.GlobalConstants.PAGESIZE_ALERT)
        {
            if (pageNo.HasValue)
            {
                DateTime ThanDate = DateTime.Now.AddDays(-2);
                IDictionary<string, object> dicAlert = new Dictionary<string, object>();
                dicAlert["IsActive"] = true;
                dicAlert["TypeID"] = 1;
                IQueryable<AlertMessageBO> listAlertMessage = (from lst in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish && DateTime.Compare(o.PublishedDate, DateTime.Now) < 0)
                                                               select new AlertMessageBO
                                                               {
                                                                   AlertMessageID = lst.AlertMessageID,
                                                                   Title = lst.Title,
                                                                   TypeID = lst.TypeID,
                                                                   FileUrl = lst.FileUrl,
                                                                   ContentMessage = lst.ContentMessage,
                                                                   PublishedDate = lst.PublishedDate,
                                                                   IsPublish = lst.IsPublish,
                                                                   CreatedUserID = lst.CreatedUserID,
                                                                   CreatedDate = lst.CreatedDate,
                                                                   IsActive = lst.IsActive
                                                               }).OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID);
                ViewData[GlobalConstants.ALERT_TOTALITEM] = listAlertMessage.Count();
                List<AlertMessageBO> lstAlertMessageSkip = listAlertMessage.Skip((pageNo.Value - 1) * pageSize).Take(pageSize).ToList();
                ViewData[GlobalConstants.ALERT_CURRENT_PAGENO] = pageNo.HasValue ? pageNo.Value : 1;
                ViewData[GlobalConstants.ALERT_PAGESIZE] = pageSize;


                IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
                dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
                IQueryable<AlertMessageDetail> lstAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
                int count = 0;//dem so dong danh dau la tin da doc   
                foreach (var item in lstAlertMessageSkip)
                {
                    count = lstAlertMessageDetail.Count(s => s.AlertMessageID == item.AlertMessageID);
                    if (count > 0)//la tin da doc
                    {
                        item.isRead = true;
                    }
                    else//la tin chua doc
                    {
                        item.isRead = false;
                    }
                }
                ViewData[GlobalConstants.NEWMESSAGENO] = newsNo;
                ViewData[GlobalConstants.ALERT_MESSAGE] = lstAlertMessageSkip;
            }
            return PartialView("_FrameViewMessageBox");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">AlertMessage ID</param>
        /// <returns></returns>
        public PartialViewResult ShowDialogMessage(int id, bool isRead, int newsNo)
        {
            //tanla4
            //14/11/2014
            //them tham so isRead de kiem tra la tin da doc hay chua

            AlertMessage AlertMessage = AlertMessageBusiness.Find(id);

            ViewData[GlobalConstants.ALERT_MESSAGE] = AlertMessage;

            //tanla4
            //13/11/2014
            //insert vao bang AlertMessageDetail - danh dau la tin da duoc doc
            if (!isRead)
            {
                AlertMessageDetail alertMessageDetail = new AlertMessageDetail();
                alertMessageDetail.AlertMessageID = id;
                alertMessageDetail.UserID = _globalInfo.UserAccountID;
                if (_globalInfo.IsAdmin)//admin
                {
                    alertMessageDetail.ReceiverTypeID = 1;
                }
                else if (_globalInfo.IsSuperVisingDeptRole)//so
                {
                    alertMessageDetail.ReceiverTypeID = 2;
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)//phong
                {
                    alertMessageDetail.ReceiverTypeID = 3;
                }
                else//truong
                {
                    alertMessageDetail.ReceiverTypeID = 4;
                }
                alertMessageDetail.Status = GlobalConstants.ISREADMESSAGE;
                alertMessageDetail.CreatedDate = DateTime.Now;
                //alertMessageDetail.ModifiedDate = null;
                AlertMessageDetailBusiness.Insert(alertMessageDetail);
                AlertMessageDetailBusiness.Save();
                //xu li cap nhat lai so tin moi hien tai (n = n-1)
                newsNo -= 1;
            }

            ViewData[GlobalConstants.NEWMESSAGENO] = newsNo;

            return PartialView("_ShowDialog");
        }


        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogOn()
        {
            string alertMessage = GetSession(GlobalConstants.ALERT_MESSAGE_PUBLIC);
            if (string.IsNullOrEmpty(alertMessage))
            {
                using (var context = new SMASEntities())
                {
                    AlertMessage am = context.AlertMessage.FirstOrDefault(o => o.IsGlobal.HasValue && o.IsGlobal.Value && o.IsPublish);
                    if (am != null)
                    {
                        alertMessage = am.ContentMessage;
                        SetSession(GlobalConstants.ALERT_MESSAGE_PUBLIC, alertMessage);
                    }
                }
            }

            ViewData["AlertMessage"] = alertMessage;

            string userName = this.GetSession(GlobalConstants.USERNAME);
            if (!string.IsNullOrEmpty(this.GetSession(GlobalConstants.CLIENTREFUSE)))
            {
                this.SetSession(GlobalConstants.CLIENTREFUSE, null);
                ViewData["Error"] = "Tên đăng nhập hoặc mật khẩu chưa đúng. Thầy/cô vui lòng kiểm tra lại.";
            }
            else
            {
                ViewData["Error"] = null;
            }
            MembershipUser user = null;
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(this.GetSession(GlobalConstants.SUCCESSLOGIN)))
            {
                user = Membership.GetUser(userName, false);
            }
            if (user == null)
            {

                this.SetSession<bool>(GlobalConstants.LOGIN_VN_STUDY, false);
                this.SetSession(GlobalConstants.USERNAME, "");
                this.SetSession(GlobalConstants.SUCCESSLOGIN, "");
                FormsAuthentication.SignOut();
                ClearCookieSession();
                return View();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (model.LoginVnStudy == true)
            {
                SetSession<bool>(GlobalConstants.LOGIN_VN_STUDY, true);
                string urlsso = string.Format("{0}?page=Account.App.login&userId={1}", GlobalConstants.URL_VIETTEL_STUDY_REDIRECT, GlobalConstants.CLIENT_ID);
                Response.Redirect(urlsso);
                return null;

            }
            else
            {
                //this.SetSession(GlobalConstants.LOGIN_VN_STUDY, "false");
                this.SetSession<bool>(GlobalConstants.LOGIN_VN_STUDY, false);
            }
            string errMessage = "";
            DateTime eventDate = DateTime.Now;
            model.LogInFalse = false;

            //this.SetSession(GlobalConstants.TOKEN_AUTHEN, tokenAuthen);
            bool isLockedOut = false;
            DateTime TimeLogin = new DateTime();
            int countLoginFail = 0;
            if (string.IsNullOrEmpty(this.GetSession(GlobalConstants.COUNTLOGINFAIL)))
            {
                this.SetSession(GlobalConstants.COUNTLOGINFAIL, 0);
            }
            else
            {
                countLoginFail = Convert.ToInt32(this.GetSession(GlobalConstants.COUNTLOGINFAIL));
            }
            try
            {
                if (string.Compare(this.GetSession(GlobalConstants.USERNAME).ToLower(), (model.UserName ?? "").Trim().ToLower()) != 0)
                {
                    this.SetSession<string>(GlobalConstants.CAPTCHA, null);
                    this.SetSession(GlobalConstants.COUNTLOGINFAIL, null);
                    countLoginFail = 0;
                }

                this.SetSession(GlobalConstants.USERNAME, null);
                string currentCaptcha = this.GetSession(GlobalConstants.CAPTCHA);
                if (model != null && !string.IsNullOrEmpty(model.Captcha) && !string.IsNullOrEmpty(currentCaptcha))
                {
                    if (string.Compare(currentCaptcha.ToLower(), model.Captcha.ToLower()) != 0)
                    {
                        ModelState.AddModelError("", "Mã xác thực không chính xác.</br> Thầy/cô vui lòng kiểm tra lại.");
                        ViewData["Error"] = "Mã xác thực không chính xác. Thầy/cô vui lòng kiểm tra lại.";
                        model.LogInFalse = true;
                        model.CapchaDisplay = true;
                        return View(model);
                    }
                }
                else if (!string.IsNullOrEmpty(currentCaptcha) && string.IsNullOrEmpty(model.Captcha))
                {
                    ModelState.AddModelError("", "Vui lòng nhập mã xác thực.");
                    ViewData["Error"] = "Thầy/cô chưa nhập mã xác thực.";
                    model.LogInFalse = true;
                    model.CapchaDisplay = true;
                    return View(model);
                }

                if (string.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("", "Thầy/cô chưa nhập tên đăng nhập");
                    ViewData["Error"] = "Thầy/cô chưa nhập tên đăng nhập";
                    model.LogInFalse = true;
                    return View(model);
                }
                if (string.IsNullOrEmpty(model.Password))
                {
                    ModelState.AddModelError("", "Thầy/cô chưa nhập mật khẩu");
                    ViewData["Error"] = "Thầy/cô chưa nhập mật khẩu";
                    model.LogInFalse = true;
                    return View(model);
                }
                model.CapchaDisplay = false;
                bool enabledLogin = false;
                //Kiem tra user co ton tai tren SMAS
                MembershipUser objMembershipUser = Membership.GetUser(model.UserName.Trim(), false);


                if (objMembershipUser != null)
                {
                    #region Reset biến Đếm số lần đăng nhập sai
                    if (countLoginFail >= GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA && objMembershipUser.IsLockedOut == false)
                    {
                        countLoginFail = 0;
                        this.SetSession(GlobalConstants.COUNTLOGINFAIL, 0);
                    }
                    #endregion

                    if (objMembershipUser.IsLockedOut)
                    {
                        #region Kiểm tra mở khóa
                        TimeLogin = objMembershipUser.LastLockoutDate;
                        if (DateTime.Now >= TimeLogin.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT))
                        {
                            objMembershipUser.UnlockUser();
                            enabledLogin = true;
                        }
                        else
                        {
                            isLockedOut = true;
                            TimeSpan Time = TimeLogin.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT) - DateTime.Now;
                            int Minute = Time.Minutes;
                            int Second = Time.Seconds;
                            ModelState.AddModelError("", "Tài khoản của bạn đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.");
                            ViewData["Error"] = "Tài khoản của thầy/cô đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.";
                            model.LogInFalse = true;
                            model.CapchaDisplay = true;
                            return View(model);
                        }
                        #endregion
                    }
                    else enabledLogin = true;
                }
                //TokenBO accsessToken = null;
                if (ModelState.IsValid)
                {
                    this.SetSession(GlobalConstants.SUCCESSLOGIN, null);
                    this.SetSession(GlobalConstants.USERNAME, model.UserName.Trim());
                    bool IsValid = Membership.ValidateUser(model.UserName.Trim(), model.Password);

                    if (IsValid && enabledLogin)
                    {
                        return ProcessLogIn(model, returnUrl, objMembershipUser);
                    }
                    else
                    {
                        this.SetSession(GlobalConstants.SUCCESSLOGIN, null);
                        countLoginFail++;
                        this.SetSession(GlobalConstants.COUNTLOGINFAIL, countLoginFail);
                        if (countLoginFail > GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA)
                        {
                            ModelState.AddModelError("", "Thầy/cô đã đăng nhập sai quá " + GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA + " lần.");

                            if ((GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA - countLoginFail) > 0)
                            {
                                ViewData["Error"] = "Tài khoản sẽ bị khóa sau  " + GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA + " lần đăng nhập không thành công. Số lần đăng nhập còn lại là " + (GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA - countLoginFail) + ".";
                            }
                            else
                            {
                                ViewData["Error"] = "Tài khoản của thầy/cô đã bị khoá. Vui lòng đăng nhập lại sau 10 phút.";
                            }
                            model.CapchaDisplay = true;
                            return View(model);
                        }

                        if (objMembershipUser == null)
                        {
                            ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
                            ViewData["Error"] = "Tên đăng nhập hoặc mật khẩu không chính xác. Thầy/cô vui lòng kiểm tra lại.";
                        }
                        else
                        {
                            // Kiểm tra kích hoạt tài khoản
                            if (!objMembershipUser.IsApproved)
                            {
                                ModelState.AddModelError("", "Tài khoản chưa được kích hoạt.");
                                ViewData["Error"] = "Tài khoản của thầy cô chưa được kích hoạt.";
                            }

                            //case: wrong pass - 20140424 - Fix bug SMAS3 ko thong bao khi nhap sai mat khau
                            if (!IsValid)
                            {
                                ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
                                if ((GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA - countLoginFail) > 0)
                                {
                                    ViewData["Error"] = "Tài khoản sẽ bị khóa sau  " + GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA + " lần đăng nhập không thành công. Số lần đăng nhập còn lại là " + (GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA - countLoginFail) + ".";
                                }
                                else
                                {
                                    ViewData["Error"] = "Tài khoản của thầy/cô đã bị khoá. Vui lòng đăng nhập lại sau 10 phút.";
                                }
                            }
                        }
                        model.LogInFalse = true;
                    }

                    if (isLockedOut)
                    {
                        TimeSpan Time = TimeLogin.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT) - DateTime.Now;
                        int Minute = Time.Minutes;
                        int Second = Time.Seconds;
                        ModelState.AddModelError("", "Tài khoản của bạn đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.");
                        ViewData["Error"] = "Tài khoản của thầy/cô đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.";
                    }
                }

            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                logger.Error("LogOn:" + ex.Message, ex);
                model.LogInFalse = true;
                if (isLockedOut)
                {
                    TimeSpan Time = TimeLogin.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT) - DateTime.Now;
                    int Minute = Time.Minutes;
                    int Second = Time.Seconds;
                    ModelState.AddModelError("", "Tài khoản của bạn đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.");
                    ViewData["Error"] = "Tài khoản của thầy/cô đã bị khoá. Vui lòng đăng nhập lại sau " + Minute.ToString() + " phút " + Second + " giây.";
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
                    ViewData["Error"] = "Lỗi đăng nhập hệ thống. Thầy/cô vui lòng kiểm tra lại hoặc liên hệ hotline để được hỗ trợ.";
                }
            }
            finally
            {
                if (!string.IsNullOrEmpty(model.UserName))
                {

                    if (string.IsNullOrEmpty(errMessage) && ViewData["Error"] != null)
                    {
                        errMessage = ViewData["Error"].ToString();
                    }
                }

                if (!string.IsNullOrEmpty(model.UserName))
                {
                    TimeSpan span = DateTime.Now - eventDate;
                    string loginResutl = model.LogInFalse ? LogExtensions.LOGIN_FAIL : LogExtensions.LOGIN_SUCCESS;
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_LOGIN, eventDate, model.UserName, loginResutl, "");

                    int duration = span.Milliseconds;
                    string yearMonth = eventDate.ToString("yyyy") + eventDate.ToString("MM");
                    int partitionMonth = Convert.ToInt32(yearMonth);
                    if (string.IsNullOrEmpty(errMessage) && ViewData["Error"] != null)
                    {
                        errMessage = ViewData["Error"].ToString();
                    }
                    KPILog kpiLog = new KPILog
                    {
                        Account = "",
                        ActionName = "Account/LogOn",
                        ApplicationCode = "SMAS",
                        Duration = duration,
                        EndTime = DateTime.Now,
                        ErrorCode = 0,
                        ErrorDescription = errMessage,
                        IPPortCurrentNode = LogExtensions.GetClientIp(),
                        IPPortParentNode = LogExtensions.GetHostIp(),
                        RequestContent = "Đăng nhập hệ thống UserName: " + model.UserName,
                        ResponseContent = loginResutl,
                        ServiceCode = "Login",
                        StartTime = eventDate,
                        SessionID = Session.SessionID,
                        UserName = model.UserName,
                        TransactionStatus = model.LogInFalse ? 0 : 1,
                        PartitionMonth = partitionMonth
                    };

                    SMAS.Business.Common.UtilsBusiness.KPILog(kpiLog);
                    //UtilsBusiness.KPILog(kpiLog);
                }

                if (!model.LogInFalse && !(model.LoginVnStudy == true))
                {
                    //Doi mat khau doi voi tai khoan co mat khau mac dinh
                    //OAuth2.ChangePass(model.UserName, GlobalConstants.PASSWROD_DEFAULT, model.Password);
                }
            }
            return View(model);
        }

        #region ViettelStudy
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult GetAccessToken(string returnUrl, string tokenAuthen)
        {
            string authorizationCode = HttpContext.Request["code"];
            logger.Info("Logtoken " + authorizationCode);
            string userName = "";
            MembershipUser user = null;
            if (!string.IsNullOrEmpty(authorizationCode))
            {
                TokenUser tokenUser;
                TokenBO accesstoken = OAuth2.GetAccessToken(authorizationCode);
                if (accesstoken != null && !string.IsNullOrEmpty(accesstoken.access_token))
                {
                    tokenUser = OAuth2.GetUserName(accesstoken);
                    if (tokenUser != null && !string.IsNullOrEmpty(tokenUser.code))
                    {
                        SetSession(GlobalConstants.CAPTCHA, null);
                        logger.Info("login vnstudy userName=" + tokenUser.code);
                        userName = tokenUser.code;
                        SetSession(GlobalConstants.USERNAME, userName);
                        SetSession(GlobalConstants.SUCCESSLOGIN, userName);

                        if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(GetSession(GlobalConstants.SUCCESSLOGIN)))
                        {
                            user = Membership.GetUser(userName, false);

                        }
                    }
                }
            }

            if (user == null)
            {
                SetSession<bool>(GlobalConstants.LOGIN_VN_STUDY, false);
                SetSession(GlobalConstants.USERNAME, "");
                SetSession(GlobalConstants.SUCCESSLOGIN, "");
                ClearCookieSession();
                FormsAuthentication.SignOut();

            }
            LogOnModel model = new LogOnModel
            {
                UserName = userName,
                LoginVnStudy = true,
            };
            string url = "/Home/Index";
            return ProcessLogIn(model, url, user);


        }

        /// <summary>
        /// Xu ly login
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private ActionResult ProcessLogIn(LogOnModel model, string returnUrl, MembershipUser user)
        {
            Guid userId = (Guid)user.ProviderUserKey;
            UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);


            #region Cac truong hop SMAS khong ton tai account hoac account chua duoc duoc kich hoat

            if (objUserAccount == null)
            {
                //Neu ko co UserAccount nao tuong ung voi aspnet_User thi ko cho dang nhap
                ViewData["Error"] = Res.Get("UserLogin_Error_UserAccountNotActived");
                model.LogInFalse = true;
            }
            else if (!user.IsApproved || !objUserAccount.IsActive)
            {
                ViewData["Error"] = Res.Get("UserLogin_Error_UserAccountNotActived");
                model.LogInFalse = true;
            }
            #endregion


            else
            {
                #region Xac thuc thanh cong


                Session[GlobalConstants.USERNAME] = model.UserName;
                SetSession(GlobalConstants.SUCCESSLOGIN, model.UserName);
                //Session[GlobalConstants.PASSWORD_ENCRYPT] = Utils.Utils.Encrypt(objMembershipUser.GetPassword(), GlobalConstants.CERTIFICATEPASS);
                SchoolProfile objSchoolProfile = new SchoolProfile();

                #region Kiem tra va cap nhat thong tin tai khoan
                //Kiem tra tai khoan qua 3 thang doi mat khau 
                //DateTime RequestTime = objMembershipUser.LastPasswordChangedDate.AddMonths(GlobalConstants.TIME_MONTH_CHANGE_PASS);
                //if (DateTime.Now.Date < RequestTime && Utils.Utils.CheckPassRequire(UserPass))
                //{
                //    ControllerContext.HttpContext.Session[GlobalConstants.CHANGE_PASS_REQUEST] = false;

                //}
                //else
                //{
                //    if (!Utils.Utils.CheckPassRequire(UserPass))
                //    {
                //        ControllerContext.HttpContext.Session[GlobalConstants.PASSWORD_REQUIRE_HIGH] = true;
                //    }
                //    ControllerContext.HttpContext.Session[GlobalConstants.CHANGE_PASS_REQUEST] = true;
                //}


                if (objUserAccount.FirstLoginDate == null)
                {
                    objUserAccount.FirstLoginDate = DateTime.Now;
                    this.UserAccountBusiness.Update(objUserAccount);
                    this.UserAccountBusiness.Save();
                }

                ControllerContext.HttpContext.Session[GlobalConstants.USERACCID] = objUserAccount.UserAccountID;
                ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT] = objUserAccount;
                #endregion

                DateTime nowDate = DateTime.Now.Date;
                Survey sv = SurveyBusiness.All.FirstOrDefault(o => EntityFunctions.TruncateTime(o.StartDate) <= nowDate && nowDate <= EntityFunctions.TruncateTime(o.EndDate));
                Session[GlobalConstants.CURRENT_SURVEY] = sv;

                #region Cap nhat thong tin vao Session
                if (objUserAccount.EmployeeID != null)
                {
                    Employee objEmployee = objUserAccount.Employee;
                    Session[GlobalConstants.EMPLOYEENAME] = objEmployee.FullName;
                    Session[GlobalConstants.EMPLOYEEID] = objEmployee.EmployeeID;
                    if (objEmployee.SchoolID.HasValue)
                    {
                        Session[GlobalConstants.SCHOOL_NAME] = objEmployee.SchoolProfile.SchoolName;
                        Session[GlobalConstants.SCHOOLID] = objEmployee.SchoolID.Value;
                        SetSession(GlobalConstants.IS_NEW_SCHOOL_MODEL, objEmployee.SchoolProfile.IsNewSchoolModel ?? false);
                    }

                    if (objEmployee.WorkTypeID.HasValue
                        && (objEmployee.WorkGroupTypeID == GlobalConstants.WORKGROUPTYPE_EMPLOYEE_ADMIN))
                    {
                        Session[GlobalConstants.IS_ROLE_PRINCIPAL] = true;
                    }
                }
                else if (objUserAccount.SchoolProfiles.Any())
                {
                    objSchoolProfile = objUserAccount.SchoolProfiles.FirstOrDefault();
                    Session[GlobalConstants.SCHOOL_NAME] = objSchoolProfile.SchoolName;
                    Session[GlobalConstants.SCHOOLID] = objSchoolProfile.SchoolProfileID;
                    Session[GlobalConstants.PRODUCT_VERSION] = objSchoolProfile.ProductVersion;
                    SetSession(GlobalConstants.IS_NEW_SCHOOL_MODEL, objSchoolProfile.IsNewSchoolModel ?? false);
                    if (objSchoolProfile.IsActiveSMAS == false)
                    {
                        SetSession(GlobalConstants.CLIENTREFUSE, true);
                        //return RedirectPermanent("/Home/LogOff");
                        //return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
                        return RedirectToAction("LogOff","Home");
                    }
                }

                //kiem tra da kick hoat smas chua - > namta
                else if (objUserAccount.SupervisingDepts.Count() > 0)
                {
                    SupervisingDept objSupervisingDept = objUserAccount.SupervisingDepts.FirstOrDefault();
                    if (objSupervisingDept != null)
                    {
                        if (objSupervisingDept.IsActiveSMAS == false)
                        {
                            SetSession(GlobalConstants.CLIENTREFUSE, true);
                            return RedirectToAction("LogOff", "Home");
                            //return RedirectPermanent(Utils.Utils.LogOff);
                            //return RedirectPermanent("/Home/LogOff");
                            //return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
                        }
                    }
                }
                #endregion
                logger.Info("Log 6:Login success check role login " + model.UserName);
                if (objUserAccount.IsAdmin == false)
                {
                    #region Truong hop account thuong
                    #region Kiem tra tinh hop le cua du lieu
                    if (objUserAccount.Employee != null)
                    {
                        objSchoolProfile = objUserAccount.Employee.SchoolProfile;
                        SupervisingDept supervisingDept = objUserAccount.Employee.SupervisingDept;
                        //Neu la giao vien
                        if (objSchoolProfile != null)
                        {
                            if (objSchoolProfile.IsActiveSMAS == false)
                            {
                                // Thong bao loi o day
                                SetSession(GlobalConstants.CLIENTREFUSE, true);
                                //return RedirectPermanent(Utils.Utils.LogOff);
                                return RedirectToAction("LogOff", "Home");
                            }
                            Session[GlobalConstants.PRODUCT_VERSION] = objSchoolProfile.ProductVersion;
                        }
                        else if (supervisingDept != null)
                        {
                            if (supervisingDept.IsActiveSMAS == false)
                            {
                                // Thong bao loi o day
                                SetSession(GlobalConstants.CLIENTREFUSE, true);
                                //return RedirectPermanent(Utils.Utils.LogOff);
                                return RedirectToAction("LogOff", "Home");
                            }
                        }
                    }
                    #endregion
                    List<WorkFlowUserBO> lstWorkFlowUser = new List<WorkFlowUserBO>();
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                                    {
                                        {"UserID",objUserAccount.UserAccountID}
                                    };
                    lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).ToList();
                    ViewData["lstWorkFlow"] = lstWorkFlowUser;
                    //Khong la Admin -> Chon Group -> lay ra cac Group duoc gan cho UserAccount
                    IQueryable<GroupCat> groupsOfUser = userGroupBusiness.GetGroupsOfUser(objUserAccount.UserAccountID);
                    if (groupsOfUser != null)
                    {
                        List<GroupCat> groups = groupsOfUser.ToList();
                        if (groups == null || groups.Count == 0)
                        {
                            //neu nguoi dung duoc gan cho nhieu group hoac chua gan group nao-> thong bao loi
                            ModelState.AddModelError("", @Resources.Resource.ResourceManager.GetString("Home_Label_MultiGroupError"));
                            // Thong bao loi o day
                            SetSession(GlobalConstants.CLIENTREFUSE, true);
                            //return RedirectPermanent(Utils.Utils.LogOff);
                            return RedirectToAction("LogOff", "Home");
                        }
                        else
                        {
                            //Lay ra admin cua tai khoan truong
                            //Namta sua voi tai khoan da cap 
                            if (objUserAccount.Employee != null)
                            {
                                #region Account Giao vien
                                if (objSchoolProfile != null)
                                {
                                    SetSession(GlobalConstants.IS_NEW_SCHOOL_MODEL, objSchoolProfile.IsNewSchoolModel ?? false);
                                    int UaAdminSchoolID = objSchoolProfile.AdminID.Value;
                                    //lay tai khoan admin cua truong
                                    IQueryable<Role> rolesOfUser = UserRoleBusiness.GetRolesOfUser(UaAdminSchoolID);
                                    List<Role> roles = rolesOfUser.ToList();

                                    //Kiem tra nhan vien co nhieu role
                                    if (roles.Count > 1)
                                    {
                                        //Lay danh sach nhom cua nguoi dung
                                        List<int> lstGroupRoleID = new List<int>();
                                        var lstGroup = userGroupBusiness.GetGroupsOfUser(objUserAccount.UserAccountID);
                                        if (lstGroup != null && lstGroup.Any())
                                        {
                                            lstGroupRoleID = lstGroup.Select(o => o.RoleID).Distinct().ToList();
                                        }
                                        List<Role> listRoleNew = new List<Role>();
                                        foreach (Role r in roles)
                                        {
                                            int appliedLevel = Utils.Utils.MapRoleToAppliedLevel(r.RoleID);

                                            if (lstGroupRoleID.Contains(r.RoleID))
                                            {
                                                listRoleNew.Add(r);
                                            }
                                        }
                                        roles = listRoleNew;
                                        if (roles.Count > 1)
                                        {
                                            //Neu nguoi dung co nhieu hon 1 Role -> cho nguoi dung chon cap
                                            SetAppliedLevels(roles);
                                            List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();
                                            foreach (Role r in roles)
                                            {
                                                RoleViewModel RoleViewModel = new RoleViewModel();
                                                Utils.Utils.BindTo(r, RoleViewModel);
                                                rolesViewModel.Add(RoleViewModel);
                                            }
                                            TempData[GlobalConstants.ROLES] = rolesViewModel;
                                            TempData[GlobalConstants.LOGON_MODEL] = model;

                                            // Tra lai trang chon cap
                                            //return AuthorizeAdmin();
                                            return RedirectToAction("AuthorizeAdmin", "Home");
                                            //return RedirectPermanent(Utils.Utils.AuthorizeAdmin);
                                        }
                                    }
                                }
                                #endregion

                                //cac truong hop la nhan vien, giao vien chi duoc phan cong 1 cap
                                List<int> ListGroupID = groups.Select(u => u.GroupCatID).ToList();
                                GroupCat groupCat = groups.First();
                                int roleID = groupCat.RoleID;
                                int groupCatID = groupCat.GroupCatID;

                                SessionStorage(objUserAccount, roleID, groupCatID, false, objUserAccount.Employee);
                                SetGroupListAndMenu(ListGroupID, roleID, ControllerContext.HttpContext);
                                SetAppliedLevelAndEduLevels(roleID);
                                this.SetAuthCookie(model);
                                //return RedirectToAction("Index", "Home");
                                // Kiem tra neu chua co AcademicYear thi chuyen ve trang quan ly thoi gian nam hoc
                                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                {
                                    //Redirect(returnUrl);
                                    //return RedirectPermanent(Utils.Utils.Index);
                                    RedirectToAction("Index", "Home");
                                }
                                else
                                {
                                    //Neu la nhan vien phong so return Home - Hungnd 06/05/2013
                                    if (roleID == GlobalConstants.ROLE_PHONG ||
                                        roleID == GlobalConstants.ROLE_SO)
                                    {
                                        //res = RedirectLoading("Index", "Home");
                                        return Index();
                                    }
                                    // End 
                                    //res = RedirectLoading("HomePageOfTeacher", "Home");
                                    return Index();
                                }
                            }
                        }
                    }
                    return Index();
                    #endregion
                }
                else
                {

                    #region Acccount admin
                    IQueryable<Role> rolesOfUser = UserRoleBusiness.GetRolesOfUser(objUserAccount.UserAccountID);
                    List<Role> roles = rolesOfUser.ToList();
                    int ProductVersionID = objSchoolProfile.ProductVersion.HasValue ? objSchoolProfile.ProductVersion.Value : 0;
                    List<WorkFlowUserBO> lstWorkFlowUser = new List<WorkFlowUserBO>();
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                                    {
                                        {"UserID",objUserAccount.UserAccountID}
                                    };
                    lstWorkFlowUser = WorkFlowUserBusiness.Search(dic).ToList();
                    ViewData["lstWorkFlow"] = lstWorkFlowUser;
                    if (roles.Any())
                    {
                        if (roles.Count > 1)
                        {
                            logger.Info("Log 7:Check Role Account " + model.UserName);
                            //Neu nguoi dung co nhieu hon 1 Role -> cho nguoi dung chon cap
                            #region Truong hop admin co nhieu role
                            SetAppliedLevels(roles);
                            List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();
                            foreach (Role r in roles)
                            {
                                RoleViewModel roleModel = new RoleViewModel();
                                Utils.Utils.BindTo(r, roleModel);
                                rolesViewModel.Add(roleModel);
                            }
                            TempData[GlobalConstants.ROLES] = rolesViewModel;
                            TempData[GlobalConstants.LOGON_MODEL] = model;

                            if (ProductVersionID > 0)
                            {
                                return RedirectToAction("AuthorizeAdmin", "Home");
                                //return RedirectPermanent(Utils.Utils.AuthorizeAdmin);

                            }
                            else
                            {
                                return View("_ChooseVersion");
                            }

                            #endregion
                        }
                        else
                        {
                            #region Truong hop admin co 1 role
                            int roleID = roles.First().RoleID;
                            logger.Info("Log 7:One Role " + model.UserName);
                            SessionStorage(objUserAccount, roleID, null, true, null);
                            SetRoleAndMenu(roleID, ControllerContext.HttpContext);
                            this.SetAuthCookie(model);
                            SetAppliedLevelAndEduLevels(roleID);
                            if (ProductVersionID > 0 || _globalInfo.IsSystemAdmin || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
                            {
                                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                {
                                    //Redirect(returnUrl);
                                    //RedirectPermanent(Utils.Utils.Index);
                                    return RedirectToAction("Index", "Home");
                                }
                                else
                                {
                                    return Index();
                                }
                            }
                            else
                            {
                                return View("_ChooseVersion");
                            }

                            #endregion
                        }
                    }
                    #endregion

                }
                #endregion end xac thuc thanh cong

            }
            return Index();
        }
        #endregion

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            //// add to fix Session Fixation Error
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Cookies.Clear();

            // AnhVD9 - 20151125 - Fix bug ATTT - Lỗi không thay đổi giá trị Token sau mỗi phiên
            HttpCookie sessionId = new HttpCookie("ASP.NET_SessionId", "");
            sessionId.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(sessionId);

            HttpCookie token_Lw = new HttpCookie("__RequestVerificationToken_Lw__", "");
            token_Lw.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(token_Lw);
            // End - Fix bug ATTT - Lỗi không thay đổi giá trị Token sau mỗi phiên
            ////
            return RedirectToAction("Index", "Home");
        }

        // POST: /Account/LogOn


        #region Hàm logon cũ
        //public ActionResult LogOn(LogOnModel model, string returnUrl)
        //{
        //    int countLoginFail = 0;
        //    string strCountLoginFail = (string)Session["COUNTLOGINFAIL"];
        //    if (string.IsNullOrEmpty(strCountLoginFail))
        //    {
        //        Session["COUNTLOGINFAIL"] = "0";
        //    }
        //    else
        //    {
        //        countLoginFail = Convert.ToInt32(strCountLoginFail);
        //    }

        //    string curUserName = (string)Session["USERNAMESSO"];
        //    if (string.Compare(curUserName.ToLower(), (model.UserName ?? "").Trim().ToLower()) != 0)
        //    {
        //        Session["Captcha"] = null;
        //        Session["COUNTLOGINFAIL"] = null;
        //        countLoginFail = 0;
        //    }

        //    var ListUser = VTXmlPaser.GetProperty(Path.Combine(Server.MapPath(GlobalConstants.Folder), GlobalConstants.FileName), "ListUser");
        //    if (ListUser != null && ListUser.Count() > 0)
        //    {
        //        if (ListUser.Contains(model.UserName + ","))
        //        {
        //            Session["TypeError"] = 0;
        //            Session["ErrorStr"] = VTXmlPaser.GetProperty(Path.Combine(Server.MapPath(GlobalConstants.Folder), GlobalConstants.FileName), "ContentMessage");
        //            return RedirectToAction("RoleError", "Home");
        //        }
        //    }

        //    if (string.IsNullOrEmpty(model.UserName))
        //    {
        //        ModelState.AddModelError("", "Thầy/cô chưa nhập tên đăng nhập");
        //        ViewData["ErrorMessage"] = "Thầy/cô chưa nhập tên đăng nhập";
        //        model.LogInFalse = true;
        //        return View(model);
        //    }
        //    if (string.IsNullOrEmpty(model.Password))
        //    {
        //        ModelState.AddModelError("", "Thầy/cô chưa nhập mật khẩu");
        //        ViewData["ErrorMessage"] = "Thầy/cô chưa nhập mật khẩu";
        //        model.LogInFalse = true;
        //        return View(model);
        //    }

        //    ActionResult res = null;
        //    long threadID = DateTime.Now.Ticks;
        //    if (model != null && ModelState.IsValid)
        //    {

        //        if (model.Captcha != ""
        //            && Session["Captcha"] != null
        //            && !((string)Session["Captcha"]).Equals(model.Captcha))
        //        {

        //            #region Dang nhap Capcha sai
        //            ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_CapchaWrong");
        //            model.LogInFalse = true;
        //            model.CapchaDisplay = true;
        //            res = View(model);
        //            #endregion

        //        }
        //        else
        //        {
        //            model.CapchaDisplay = false;
        //            MembershipUser objMembershipUser;
        //            try
        //            {
        //                objMembershipUser = Membership.GetUser(model.UserName);
        //            }
        //            catch
        //            {
        //                return View(model);
        //            }
        //            if (objMembershipUser != null)
        //            {
        //                #region Reset biến Đếm số lần đăng nhập sai
        //                if (countLoginFail >= GlobalConstants.LOGIN_FAIL_COUNT_FOR_CAPCHA && objMembershipUser.IsLockedOut == false)
        //                {
        //                    countLoginFail = 0;
        //                    Session["COUNTLOGINFAIL"] = 0;
        //                }
        //                #endregion

        //                if (objMembershipUser.IsLockedOut
        //                && DateTime.Now <= objMembershipUser.LastLockoutDate.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT))
        //                {
        //                    #region Truong hop nguoi dung dang bi khoa tai khoan
        //                    var Time = objMembershipUser.LastLockoutDate.AddMinutes(GlobalConstants.TIME_OPEN_ACCOUNT) - DateTime.Now;
        //                    int Minute = Time.Minutes;
        //                    int Second = Time.Seconds;
        //                    ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_LockAccount") + " " + Minute.ToString() + Res.Get("Login_Minute") + " " + Second.ToString() + " " + Res.Get("Login_Second");
        //                    model.LogInFalse = true;
        //                    model.CapchaDisplay = true;
        //                    res = View(model);
        //                    #endregion
        //                }

        //            }
        //            else
        //            {

        //                #region Mo khoa cho nguoi dung neu bi khoa
        //                bool EnabledLogin = false;
        //                if (objMembershipUser != null && objMembershipUser.IsLockedOut)
        //                {
        //                    DateTime TimeLogin = objMembershipUser.LastLockoutDate;
        //                    //mo khoa
        //                    objMembershipUser.UnlockUser();
        //                    /*if (objMembershipUser.GetPassword() == model.Password)
        //                    {
        //                        EnabledLogin = true;
        //                    }*/
        //                }
        //                #endregion

        //                if (Membership.ValidateUser(model.UserName, model.Password))
        //                {
        //                    Guid userId = (Guid)objMembershipUser.ProviderUserKey;
        //                    UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
        //                    //string UserPass = objMembershipUser.GetPassword();

        //                    #region Cac truong hop SMAS khong ton tai account hoac account chua duoc duoc kich hoat

        //                    if (objUserAccount == null)
        //                    {
        //                        //Neu ko co UserAccount nao tuong ung voi aspnet_User thi ko cho dang nhap
        //                        ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_UserAccountNotActived");
        //                        model.LogInFalse = true;
        //                        res = View(model);
        //                    }
        //                    else if (!objMembershipUser.IsApproved || !objUserAccount.IsActive)
        //                    {
        //                        ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_UserAccountNotActived");
        //                        model.LogInFalse = true;
        //                        res = View(model);
        //                    }
        //                    #endregion

        //                    #region Xac thuc thanh cong
        //                    else
        //                    {
        //                        SchoolProfile schoolProfile = new SchoolProfile();
        //                        Session[GlobalConstants.USERNAME] = model.UserName;
        //                        //Session[GlobalConstants.PASSWORD_ENCRYPT] = Utils.Utils.Encrypt(objMembershipUser.GetPassword(), GlobalConstants.CERTIFICATEPASS);

        //                        #region Kiem tra va cap nhat thong tin ve tai khoan
        //                        //Kiem tra tai khoan qua 3 thang doi mat khau 
        //                        DateTime RequestTime = objMembershipUser.LastPasswordChangedDate.AddMonths(GlobalConstants.TIME_MONTH_CHANGE_PASS);
        //                        if (DateTime.Now.Date < RequestTime)// && Utils.Utils.CheckPassRequire(UserPass))
        //                        {
        //                            ControllerContext.HttpContext.Session[GlobalConstants.CHANGE_PASS_REQUEST] = false;

        //                        }
        //                        else
        //                        {
        //                            //if (!Utils.Utils.CheckPassRequire(UserPass))
        //                            //{
        //                            //    ControllerContext.HttpContext.Session[GlobalConstants.PASSWORD_REQUIRE_HIGH] = true;
        //                            //}
        //                            ControllerContext.HttpContext.Session[GlobalConstants.CHANGE_PASS_REQUEST] = true;
        //                        }


        //                        if (objUserAccount.FirstLoginDate == null)
        //                        {
        //                            objUserAccount.FirstLoginDate = DateTime.Now;
        //                            this.UserAccountBusiness.Update(objUserAccount);
        //                            this.UserAccountBusiness.Save();
        //                        }

        //                        ControllerContext.HttpContext.Session[GlobalConstants.USERACCID] = objUserAccount.UserAccountID;
        //                        ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT] = objUserAccount;
        //                        #endregion

        //                        #region Cap nhat thong tin vao Session cac loai account: thuong, admin truong, phong/so
        //                        if (objUserAccount.EmployeeID != null)
        //                        {
        //                            Employee objEmployee = objUserAccount.Employee;
        //                            Session[GlobalConstants.EMPLOYEENAME] = objEmployee.FullName;
        //                            Session[GlobalConstants.EMPLOYEEID] = objEmployee.EmployeeID;
        //                            if (objEmployee.SchoolID.HasValue)
        //                            {
        //                                Session[GlobalConstants.SCHOOL_NAME] = objEmployee.SchoolProfile.SchoolName;
        //                                Session[GlobalConstants.SCHOOLID] = objEmployee.SchoolID.Value;
        //                                Session[GlobalConstants.IS_NEW_SCHOOL_MODEL] = objEmployee.SchoolProfile.IsNewSchoolModel.GetValueOrDefault();
        //                            }
        //                            if (objEmployee.WorkTypeID.HasValue
        //                                && (objEmployee.WorkTypeID.Value == GlobalConstants.WORKTYPE_PRINCIPAL
        //                                    || objEmployee.WorkTypeID.Value == GlobalConstants.WORKTYPE_ASSISTANT_PRINCIPAL || objEmployee.WorkGroupTypeID == GlobalConstants.WORKGROUPTYPE_EMPLOYEE_ADMIN
        //                                    )
        //                                )
        //                            {
        //                                Session[GlobalConstants.IS_ROLE_PRINCIPAL] = true;
        //                            }
        //                        }

        //                        else if (objUserAccount.SchoolProfiles.Any())
        //                        {
        //                            schoolProfile = objUserAccount.SchoolProfiles.FirstOrDefault();
        //                            Session[GlobalConstants.SCHOOL_NAME] = schoolProfile.SchoolName;
        //                            Session[GlobalConstants.SCHOOLID] = schoolProfile.SchoolProfileID;
        //                            Session[GlobalConstants.IS_NEW_SCHOOL_MODEL] = schoolProfile.IsNewSchoolModel.GetValueOrDefault();
        //                            Session[GlobalConstants.PRODUCT_VERSION] = schoolProfile.ProductVersion;
        //                            if (schoolProfile.IsActiveSMAS == false)
        //                            {
        //                                // Thong bao loi o day
        //                                ViewData["ErrorMessage"] = Res.Get("Login_Label_AccountNotIsSmas");
        //                                model.LogInFalse = true;
        //                                return View(model);
        //                            }
        //                        }

        //                        //kiem tra da kick hoat smas chua - > namta
        //                        else if (objUserAccount.SupervisingDepts.Any())
        //                        {
        //                            SupervisingDept objSupervisingDept = objUserAccount.SupervisingDepts.FirstOrDefault();
        //                            if (objSupervisingDept != null)
        //                            {
        //                                if (objSupervisingDept.IsActiveSMAS == false)
        //                                {
        //                                    // Thong bao loi o day
        //                                    ViewData["ErrorMessage"] = Res.Get("Login_Label_AccountNotIsSmas");
        //                                    model.LogInFalse = true;
        //                                    return View(model);
        //                                }
        //                            }
        //                        }
        //                        #endregion

        //                        if (objUserAccount.IsAdmin == false)
        //                        {
        //                            #region Truong hop la account thuong
        //                            #region Kiem tra tinh hop le cua du lieu
        //                            if (objUserAccount.Employee != null)
        //                            {
        //                                schoolProfile = objUserAccount.Employee.SchoolProfile;
        //                                SupervisingDept supervisingDept = objUserAccount.Employee.SupervisingDept;
        //                                //Neu la giao vien
        //                                if (schoolProfile != null)
        //                                {
        //                                    if (schoolProfile.IsActiveSMAS == false)
        //                                    {
        //                                        // Thong bao loi o day
        //                                        ViewData["ErrorMessage"] = Res.Get("Login_Label_AccountNotIsSmas");
        //                                        model.LogInFalse = true;
        //                                        return View(model);
        //                                    }
        //                                    Session[GlobalConstants.PRODUCT_VERSION] = schoolProfile.ProductVersion;
        //                                }
        //                                else if (supervisingDept != null)
        //                                {
        //                                    if (supervisingDept.IsActiveSMAS == false)
        //                                    {
        //                                        // Thong bao loi o day
        //                                        ViewData["ErrorMessage"] = Res.Get("Login_Label_AccountNotIsSmas");
        //                                        model.LogInFalse = true;
        //                                        return View(model);
        //                                    }
        //                                }
        //                            }
        //                            #endregion


        //                            //Khong la Admin -> Chon Group -> lay ra cac Group duoc gan cho UserAccount
        //                            IQueryable<GroupCat> groupsOfUser = userGroupBusiness.GetGroupsOfUser(objUserAccount.UserAccountID);
        //                            if (groupsOfUser != null)
        //                            {
        //                                List<GroupCat> groups = groupsOfUser.ToList();
        //                                if (groups == null || groups.Count == 0)
        //                                {
        //                                    //neu nguoi dung duoc gan cho nhieu group hoac chua gan group nao-> thong bao loi
        //                                    ModelState.AddModelError("", @Resources.Resource.ResourceManager.GetString("Home_Label_MultiGroupError"));
        //                                    // Thong bao loi o day
        //                                    ViewData["ErrorMessage"] = Res.Get("Home_Label_MultiGroupError");
        //                                    model.LogInFalse = true;
        //                                    return View(model);
        //                                }
        //                                else
        //                                {
        //                                    //Lay ra admin cua tai khoan truong
        //                                    //Namta sua voi tai khoan da cap 
        //                                    if (objUserAccount.Employee != null)
        //                                    {
        //                                        //SchoolProfile schoolProfile = objUserAccount.Employee.SchoolProfile;
        //                                        //Neu la giao vien
        //                                        if (schoolProfile != null)
        //                                        {
        //                                            int UaAdminSchoolID = schoolProfile.AdminID.Value;
        //                                            //lay tai khoan admin cua truong
        //                                            IQueryable<Role> rolesOfUser = UserRoleBusiness.GetRolesOfUser(UaAdminSchoolID);
        //                                            List<Role> roles = rolesOfUser.ToList();

        //                                            //Kiá»ƒm tra nv cĂ³ nhiá»u roles
        //                                            if (roles.Count > 1)
        //                                            {

        //                                                //Lay danh sach nhom cua nguoi dung
        //                                                List<int> lstGroupRoleID = new List<int>();
        //                                                var lstGroup = userGroupBusiness.GetGroupsOfUser(objUserAccount.UserAccountID);
        //                                                if (lstGroup != null && lstGroup.Count() > 0)
        //                                                {
        //                                                    lstGroupRoleID = lstGroup.Select(o => o.RoleID).Distinct().ToList();
        //                                                }
        //                                                List<Role> listRoleNew = new List<Role>();
        //                                                foreach (Role r in roles)
        //                                                {
        //                                                    //int appliedLevel = Utils.Utils.MapRoleToAppliedLevel(r.RoleID);

        //                                                    if (//listAppliedLevel.Contains(appliedLevel) || 
        //                                                        lstGroupRoleID.Contains(r.RoleID))
        //                                                    {
        //                                                        listRoleNew.Add(r);
        //                                                    }
        //                                                }
        //                                                roles = listRoleNew;
        //                                                if (roles.Count > 1)
        //                                                {
        //                                                    //Neu nguoi dung co nhieu hon 1 Role -> cho nguoi dung chon cap
        //                                                    SetAppliedLevels(roles);
        //                                                    List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();
        //                                                    foreach (Role r in roles)
        //                                                    {
        //                                                        RoleViewModel RoleViewModel = new RoleViewModel();
        //                                                        Utils.Utils.BindTo(r, RoleViewModel);
        //                                                        rolesViewModel.Add(RoleViewModel);
        //                                                    }
        //                                                    TempData[GlobalConstants.ROLES] = rolesViewModel;
        //                                                    TempData[GlobalConstants.LOGON_MODEL] = model;
        //                                                    return RedirectToAction("AuthorizeAdmin", "Home");
        //                                                }
        //                                            }
        //                                        }

        //                                        //cac truong hop la nhan vien,giao vien chi Ä‘Æ°á»£c phĂ¢n cĂ´ng 1 cáº¥p
        //                                        List<int> ListGroupID = groups.Select(u => u.GroupCatID).ToList();
        //                                        GroupCat groupCat = groups.First();
        //                                        int roleID = groupCat.RoleID;
        //                                        int groupCatID = groupCat.GroupCatID;
        //                                        SessionStorage(objUserAccount, roleID, groupCatID, false, objUserAccount.Employee);
        //                                        SetGroupListAndMenu(ListGroupID, roleID, ControllerContext.HttpContext);
        //                                        SetAppliedLevelAndEduLevels(roleID);
        //                                        this.SetAuthCookie(model);
        //                                        //return RedirectToAction("Index", "Home");
        //                                        // Kiem tra neu chua co AcademicYear thi chuyen ve trang quan ly thoi gian nam hoc
        //                                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
        //                                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
        //                                        {
        //                                            return Redirect(returnUrl);
        //                                        }
        //                                        else
        //                                        {
        //                                            //Neu la nhan vien phong so return Home - Hungnd 06/05/2013
        //                                            if (roleID == GlobalConstants.ROLE_PHONG ||
        //                                                roleID == GlobalConstants.ROLE_SO)
        //                                            {
        //                                                return RedirectToAction("Index", "Home");
        //                                            }
        //                                            // End 
        //                                            return RedirectToAction("HomePageOfSchoolAdmin", "Home");
        //                                        }

        //                                    }
        //                                }
        //                            }

        //                            return RedirectLoading("Index", "Home");
        //                            #endregion
        //                        }
        //                        else
        //                        {
        //                            #region Account admin
        //                            //HoanTV5 sua lai luong neu truong chua chon Version thi chuyen toi trang chon Version
        //                            IQueryable<Role> rolesOfUser = UserRoleBusiness.GetRolesOfUser(objUserAccount.UserAccountID);
        //                            List<Role> roles = rolesOfUser.ToList();
        //                            int ProductVersionID = schoolProfile.ProductVersion.HasValue ? schoolProfile.ProductVersion.Value : 0;
        //                            if (roles.Count() > 0)
        //                            {
        //                                if (roles.Count > 1)
        //                                {
        //                                    //Neu nguoi dung co nhieu hon 1 Role -> cho nguoi dung chon cap
        //                                    #region Truong hop account admin co nhieu role
        //                                    SetAppliedLevels(roles);
        //                                    List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();
        //                                    foreach (Role r in roles)
        //                                    {
        //                                        RoleViewModel roleModel = new RoleViewModel();
        //                                        Utils.Utils.BindTo(r, roleModel);
        //                                        rolesViewModel.Add(roleModel);
        //                                    }
        //                                    TempData[GlobalConstants.ROLES] = rolesViewModel;
        //                                    TempData[GlobalConstants.LOGON_MODEL] = model;
        //                                    ViewData["AppliedID"] = 1;
        //                                    if (ProductVersionID > 0)
        //                                    {
        //                                        res = RedirectToAction("AuthorizeAdmin", "Home");
        //                                    }
        //                                    else
        //                                    {
        //                                        return View("_ChooseVersion");
        //                                    }

        //                                    #endregion
        //                                }
        //                                else
        //                                {
        //                                    #region Truong hop account co 1 role
        //                                    int roleID = roles.First().RoleID;
        //                                    //remove chon cap cu namta
        //                                    Session[GlobalConstants.APPLIED_LEVELS] = null;
        //                                    this.SetAuthCookie(model);
        //                                    SessionStorage(objUserAccount, roleID, null, true, null);
        //                                    SetRoleAndMenu(roleID, ControllerContext.HttpContext);
        //                                    SetAppliedLevelAndEduLevels(roleID);
        //                                    ViewData["AppliedID"] = 0;
        //                                    if (ProductVersionID > 0 || _globalInfo.IsSystemAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
        //                                    {
        //                                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
        //                                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
        //                                        {
        //                                            res = Redirect(returnUrl);
        //                                        }
        //                                        else
        //                                        {
        //                                            return Index();
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        return View("_ChooseVersion");
        //                                    }
        //                                    #endregion
        //                                }
        //                            }
        //                            #endregion
        //                        }

        //                        #endregion
        //                    }
        //                }
        //                else
        //                {
        //                    #region Truong hop xac thuc loi
        //                    model.LogInFalse = true;
        //                    //Hungnd8 Check user chua duoc kich hoat
        //                    if (Membership.GetUser(model.UserName) != null && !Membership.GetUser(model.UserName).IsApproved)
        //                    {
        //                        ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_UserAccountNotActived");
        //                        return View(model);
        //                    }
        //                    //Namta Check user chua duoc kich hoat
        //                    if (Membership.GetUser(model.UserName) != null)
        //                    {
        //                        Guid userId = (Guid)Membership.GetUser(model.UserName).ProviderUserKey;
        //                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);

        //                        if (!objUserAccount.IsActive)
        //                        {
        //                            ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_UserAccountNotActived");
        //                            return View(model);
        //                        }
        //                    }

        //                    //End
        //                    ViewData["ErrorMessage"] = Res.Get("UserLogin_Error_UserNameWrong");
        //                    if (Session["CountLoginFail"] == null)
        //                    {
        //                        Session["CountLoginFail"] = 0;

        //                    }
        //                    else
        //                    {
        //                        int countLoginFail = (int)Session["CountLoginFail"];
        //                        countLoginFail++;
        //                        Session["CountLoginFail"] = countLoginFail;

        //                        if (countLoginFail >= LOGIN_FAIL_COUNT_FOR_CAPCHA)
        //                        {

        //                            model.CapchaDisplay = true;
        //                        }
        //                    }
        //                    ModelState.AddModelError("", Res.Get("Home_Label_BadLogOnMessage"));
        //                    #endregion
        //                }
        //            }
        //        }
        //    }
        //    return res != null ? res : View(model);
        //} 
        #endregion


        #region Forget password
        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult ForgetPassword(ForgetPasswordModel model)
        {
            DateTime eventDate = DateTime.Now;

            try
            {
                string currentCaptcha = (string)Session["Captcha"];
                if (model != null && !string.IsNullOrEmpty(model.Captcha) && !string.IsNullOrEmpty(currentCaptcha))
                {
                    if (string.Compare(currentCaptcha.ToLower(), model.Captcha.ToLower()) != 0)
                    {
                        ModelState.AddModelError("", "Mã xác thực không chính xác.</br> Thầy/cô vui lòng kiểm tra lại.");
                        ViewData["Error"] = "Mã xác thực không chính xác. Thầy/cô vui lòng kiểm tra lại.";
                        model.LogInFalse = true;
                        model.CapchaDisplay = true;
                        return View(model);
                    }
                }
                else if (!string.IsNullOrEmpty(currentCaptcha) && string.IsNullOrEmpty(model.Captcha))
                {
                    ModelState.AddModelError("", "Vui lòng nhập mã xác thực.");
                    ViewData["Error"] = "Thầy/cô chưa nhập mã xác thực.";
                    model.LogInFalse = true;
                    model.CapchaDisplay = true;
                    return View(model);
                }

                if (string.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("", "Thầy/cô chưa nhập tên đăng nhập");
                    ViewData["Error"] = "Thầy/cô chưa nhập tên đăng nhập";
                    model.LogInFalse = true;
                    return View(model);
                }
                if (string.IsNullOrEmpty(model.Phone))
                {
                    ModelState.AddModelError("", "Thầy/cô chưa nhập số điện thoại");
                    ViewData["Error"] = "Thầy/cô chưa nhập số điện thoại";
                    model.LogInFalse = true;
                    return View(model);
                }
                model.CapchaDisplay = false;

                string username = model.UserName.Trim();
                string phone = model.Phone.Trim();

                // AnhVD 20140709 - Ko cap mat khau admin
                if (String.Compare(username, "admin") == 0)
                {
                    ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                    ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                    return View(model);
                }

                #region Gửi mật khẩu đến điện thoại của CSKH nếu hợp lệ
                string concatPhoneNumber = System.Configuration.ConfigurationManager.AppSettings["LIST_PHONE_NUMBER_RECEIVE_NEW_PASSWORD"];
                if (!string.IsNullOrEmpty(concatPhoneNumber))
                {
                    string[] listPhoneNumber = concatPhoneNumber.Split(',', ' ');
                    bool phoneInList = false;
                    string tempPhone = this.ConvertPhone(phone);
                    for (int i = 0; i < listPhoneNumber.Length; i++)
                    {
                        if (tempPhone.Equals(this.ConvertPhone(listPhoneNumber[i].Trim())))
                        {
                            phoneInList = true;
                            break;
                        }
                    }
                    if (phoneInList)
                    {
                        string newpass = UpdateUser(username);
                        if (!string.IsNullOrEmpty(newpass))
                        {
                            string content = "Mat khau cua tai khoan " + username + " la " + newpass + ". Quy thay/co vui long dang nhap va doi mat khau de dam bao tinh bao mat.";

                            Dictionary<string, object> dic = new Dictionary<string, object>();
                            dic["Mobile"] = phone;
                            dic["Content"] = content;
                            dic["TypeID"] = 0;
                            try
                            {
                                MTBusiness.SendSMS(dic);
                                ViewData["Success"] = "true";
                            }
                            catch
                            {

                                ModelState.AddModelError("", "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!");
                                ViewData["Error"] = "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!";
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                            ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                        }
                        return View(model);
                    }
                }
                #endregion

                int value = this.Check(username, phone);
                if (value == 2)
                {
                    string newpass = UpdateUser(username);
                    if (!string.IsNullOrEmpty(newpass))
                    {
                        string content = "Mat khau cua tai khoan " + username + " la " + newpass + ". Quy thay/co vui long dang nhap va doi mat khau de dam bao tinh bao mat.";
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["Mobile"] = phone;
                        dic["Content"] = content;
                        dic["TypeID"] = 0;

                        try
                        {
                            MTBusiness.SendSMS(dic);
                            ViewData["Success"] = "true";
                        }
                        catch (Exception)
                        {

                            ModelState.AddModelError("", "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!");
                            ViewData["Error"] = "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!";
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                        ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                    }

                }
                else if (value == 1)
                {
                    ModelState.AddModelError("", "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!");
                    ViewData["Error"] = "Thông tin không hợp lệ. Hãy liên hệ với quản trị hệ thống hoặc gọi vào số hot line để được cấp mật khẩu mới hoặc kích hoạt tài khoản!";
                }
                else if (value == 3)
                {
                    ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                    ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                }
                else
                {
                    ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                    ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                }
                return View(model);


            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!");
                ViewData["Error"] = "Thông tin không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                LogExtensions.ErrorExt(logger, DateTime.Now, "ForgetPassword", model.UserName, ex);
            }
            return View(model);

        }
        #endregion
        /// <summary>
        /// Ham redirect Loading truoc khi xu ly
        /// </summary>
        /// <param name="ActionName"></param>
        /// <param name="ControlerName"></param>
        /// <returns></returns>
        private RedirectToRouteResult RedirectLoading(string ActionName, string ControlerName)
        {
            return RedirectToAction("Loading", "SSOServices", new { URL = "%2" + ControlerName + "%2" + ActionName });
        }

        /// <summary>
        /// Tráº£ vá» view Lá»±a chá»n Cáº¥p há»c cho User cĂ³ nhiá»u Role 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult AuthorizeAdmin()
        {

            List<RoleViewModel> roles = TempData[GlobalConstants.ROLES] as List<RoleViewModel>;
            if (roles != null) ViewData["roles"] = new SelectList(roles.ToList(), "RoleID", "RoleName");
            GlobalInfo globalInfo = new GlobalInfo();
            ListAppliedLevel allAppliedLevel = new ListAppliedLevel();
            // lay tat ca cac AppliedLevel     
            if (globalInfo.AppliedLevels != null && globalInfo.AppliedLevels.Any())
            {
                List<AppliedLevel> lsAppliedLevel = allAppliedLevel.GetListAppliedLevel(globalInfo.AppliedLevels);
                List<ViettelCheckboxList> listCheckBox = lsAppliedLevel.Select(ap => new ViettelCheckboxList { Label = ap.AppliedLevelName, Value = ap.AppliedLevelID, cchecked = false, disabled = false }).ToList();
                ViewData[GlobalConstants.APPLIED_LEVELS] = listCheckBox;
            }
            LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, "", "Log 9: Login school is multi role", "");
            return View("AuthorizeAdmin");
        }

        /// <summary>
        /// Chon Role cua Admin co nhieu Role
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult AuthorizeAdmin(FormCollection formCollection)
        {
            // Hungnd8 23/04/2013 - Fix choose applied level when click button back in browser
            if (formCollection["RdbSelectLevel"] == null)
            {
                ModelState.AddModelError("", @Resources.Resource.ResourceManager.GetString("Common_Label_SelectLevel"));
                return AuthorizeAdmin();
            }
            // end Fix choose...

            int appliedLevel = Convert.ToInt32(formCollection["RdbSelectLevel"]);

            int RoleID = Utils.Utils.MapAppliedLevelToRole(appliedLevel);
            if (Session[GlobalConstants.USERACCID] == null)
            {
                //het session chuyen ra trang dang nhap
                return RedirectToAction("SSOServices", "Loading");
            }
            UserAccount userAccount = UserAccountBusiness.Find((int)Session[GlobalConstants.USERACCID]);
            //Namta +  Doi voi tai khoan giao vien sau khi chon cap se hien thi dung quyen cua giao vien
            if (userAccount != null)
            {
                if (!userAccount.IsAdmin)
                {
                    IQueryable<GroupCat> groupsOfUser = userGroupBusiness.GetGroupsOfUser(userAccount.UserAccountID);
                    if (groupsOfUser != null)
                    {
                        List<GroupCat> groups = groupsOfUser.Where(u => u.RoleID == RoleID).ToList();
                        if (groups.Count > 0)
                        {
                            List<int> ListGroupID = groups.Select(o => o.GroupCatID).ToList();
                            this.SetAuthCookie(TempData[GlobalConstants.LOGON_MODEL] as LogOnModel);
                            SessionStorage(userAccount, RoleID, groups.First().GroupCatID, false, userAccount.Employee);
                            SetGroupListAndMenu(ListGroupID, RoleID, ControllerContext.HttpContext);
                            SetAppliedLevelAndEduLevels(RoleID);
                            //return RedirectLoading("HomePageOfTeacher", "Home");
                            //Neu la BGH thi chuyen den trang chu quan tri. Nguoc lai Chuyen den trang chu giao vien
                            //if (_globalInfo.IsEmployeeManager || _globalInfo.IsRolePrincipal)
                            //{
                            return HomePageOfSchoolAdmin();
                            //}
                            //else
                            //{
                            //    return HomePageOfTeacher();
                            //}
                        }
                        else
                        {
                            Session["TypeError"] = 1;
                            SetGroupAndMenu(0, 0, ControllerContext.HttpContext);
                            //SessionStorage(userAccount.UserAccountID, 0, 0, false, userAccount.EmployeeID);
                            //return RedirectToAction("RoleError", "Home");
                            return RoleError();
                        }
                    }
                }
            }
            //Thiet lap cac thong tin trong Session
            SessionStorage(userAccount, RoleID, null, true, null);
            SetRoleAndMenu(RoleID, ControllerContext.HttpContext);
            this.SetAuthCookie(TempData[GlobalConstants.LOGON_MODEL] as LogOnModel);
            SetAppliedLevelAndEduLevels(RoleID);
            //
            this.SetAuthCookie(TempData[GlobalConstants.LOGON_MODEL] as LogOnModel);
            return Index();
        }
        /// <summary>
        /// Chon Group cua User co nhieu group
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult AuthorizeUser()
        {
            List<GroupCatViewModel> groups = TempData[GlobalConstants.GROUPS] as List<GroupCatViewModel>;
            if (groups != null) ViewData["groups"] = new SelectList(groups.ToList(), "GroupCatID", "GroupName");
            return View();
        }

        //
        [HttpPost]
        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult AuthorizeUser(FormCollection formCollection)
        {
            UserAccount loggedUser = (UserAccount)ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT];
            string groupIDString = formCollection["groups"];
            int groupID = Convert.ToInt32(groupIDString);
            //List<GroupCatViewModel> groups = TempData[GlobalConstants.GROUPS] as List<GroupCatViewModel>;
            //GroupCatViewModel group = groups.Where(g => g.GroupCatID == groupID).First();
            this.SetAuthCookie(TempData[GlobalConstants.LOGON_MODEL] as LogOnModel);
            SetGroupAndMenu(groupID, groupCatBusiness.Find(groupID).RoleID, ControllerContext.HttpContext);
            //Tamhm1
            GlobalInfo global = new GlobalInfo();
            ViewData[GlobalConstants.APPLIED_LEVEL] = global.AppliedLevel.Value;
            ViewData[GlobalConstants.LIST_EDUCATION_LEVEL] = GetListEducationLevelByGrade(global.AppliedLevel.Value);
            //ViewData[GlobalConstants.LIST_CLASS_PROFILE_HEAD] = GetListClassHeadByEducationLevel(global.UserAccountID, global.AcademicYearID, global.SchoolID
            //return RedirectLoading("HomePageOfTeacher", "Home");
            return HomePageOfSchoolAdmin();
        }


        [ValidateAntiForgeryToken]
        public JsonResult ChangeAcademicYear(int AcademicYearID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (globalInfo.AcademicYearID != AcademicYearID)
            {
                Session[GlobalConstants.ACADEMICYEAR_ID] = AcademicYearID;
                SetAcademicYearSelect(AcademicYearID, globalInfo.SchoolID.Value);

                AcademicYear ac = AcademicYearBusiness.All.Where(a => a.Year <= DateTime.Now.Year && a.AcademicYearID == AcademicYearID && a.IsActive == true
                && ((a.FirstSemesterStartDate < DateTime.Now && a.FirstSemesterEndDate >= DateTime.Now)
                   || (a.SecondSemesterStartDate < DateTime.Now && a.SecondSemesterEndDate >= DateTime.Now))).FirstOrDefault();

                if (ac != null)
                {
                    Session[GlobalConstants.IsCurrentYear] = true;
                    if (DateTime.Now >= ac.SecondSemesterStartDate && DateTime.Now <= ac.SecondSemesterEndDate)
                    {
                        Session[GlobalConstants.IsCurrentYear] = true; //ngay hien tai la trong nam hoc
                        if (ac.FirstSemesterStartDate < DateTime.Now && ac.FirstSemesterEndDate > DateTime.Now)
                        {
                            Session[GlobalConstants.SEMESTER] = GlobalConstants.FIRST_SEMESTER;
                        }
                        if (ac.SecondSemesterStartDate < DateTime.Now && ac.SecondSemesterEndDate > DateTime.Now)
                        {
                            Session[GlobalConstants.SEMESTER] = GlobalConstants.SECOND_SEMESTER;
                        }
                    }

                    Session[GlobalConstants.ACADEMICYEAR_ID] = ac.AcademicYearID;
                    AcademicYear objAcademicYear = AcademicYearBusiness.Find(ac.AcademicYearID);
                    if (objAcademicYear != null)
                    {
                        Session[GlobalConstants.IS_VIEW_ALL] = objAcademicYear.IsTeacherCanViewAll;
                        _globalInfo.IsShowCalendar = objAcademicYear.IsShowCalendar.HasValue ? objAcademicYear.IsShowCalendar.Value : false;
                        _globalInfo.IsShowPupilImage = objAcademicYear.IsShowAvatar.HasValue ? objAcademicYear.IsShowAvatar.Value : false;
                    }
                }
                else
                {
                    Session[GlobalConstants.IsCurrentYear] = false;
                }
                SetClassProfile(AcademicYearID);
                //SetCookie();
            }
            return Json(new JsonMessage(""));
        }

        /// <summary>
        /// Thiáº¿t láº­p láº¡i thĂ´ng tin trong Session khi ComboBox cáº¥p há»c hoáº·c nÄƒm há»c thay Ä‘á»•i giĂ¡ trá»‹
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns></returns>
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public ActionResult ResetAcademicYear(FormCollection formCollection)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            string sAppliedLevel = formCollection["cboAppliedLevel"];
            string sAcademicYear = formCollection["cboAcademicYear"];
            if (!string.IsNullOrEmpty(sAppliedLevel))
            {
                int appliedLevel = Convert.ToInt32(sAppliedLevel);
                if (globalInfo.AppliedLevel != appliedLevel)
                {
                    int RoleID = Utils.Utils.MapAppliedLevelToRole(appliedLevel);
                    UserAccount userAccount = UserAccountBusiness.Find((int)Session[GlobalConstants.USERACCID]);
                    //Namta +  Doi voi tai khoan giao vien sau khi chon cap se hien thi dung quyen cua giao vien
                    if (userAccount != null)
                    {
                        if (!userAccount.IsAdmin)
                        {
                            IQueryable<GroupCat> groupsOfUser = userGroupBusiness.GetGroupsOfUser(userAccount.UserAccountID);
                            if (groupsOfUser != null)
                            {
                                List<GroupCat> groups = groupsOfUser.Where(o => o.RoleID == RoleID).ToList();



                                if (groups != null && groups.Count > 0)
                                {
                                    List<int> ListGroupID = groups.Select(o => o.GroupCatID).ToList();
                                    SetGroupListAndMenu(ListGroupID, RoleID, ControllerContext.HttpContext);
                                    SessionStorage(userAccount, RoleID, groups.First().GroupCatID, false, userAccount.Employee);
                                    SetAppliedLevelAndEduLevels(RoleID);
                                    if (_globalInfo.IsEmployeeManager || _globalInfo.IsRolePrincipal)
                                    {
                                        //return RedirectToAction("HomePageOfTeacher", "Home");
                                        return HomePageOfSchoolAdmin();
                                    }

                                    return RedirectToAction("HomePageOfSchoolAdmin", "Home");

                                }
                                else
                                {
                                    return RedirectToAction("RoleError", "Home");
                                }
                            }
                        }
                    }
                    SetRoleAndMenu(RoleID, ControllerContext.HttpContext);
                    SessionStorage(userAccount, RoleID, null, true, null);
                    SetAppliedLevelAndEduLevels(RoleID);
                    return RedirectToAction("HomepageOfSchoolAdmin", "Home");
                }
            }

            if (!string.IsNullOrEmpty(sAcademicYear))
            {
                int academicYear = Convert.ToInt32(sAcademicYear);
                if (globalInfo.AcademicYearID != academicYear)
                {
                    Session[GlobalConstants.ACADEMICYEAR_ID] = academicYear;
                    SetAcademicYearSelect(academicYear, globalInfo.SchoolID.Value);

                    AcademicYear ac = AcademicYearBusiness.All.Where(a => a.Year <= DateTime.Now.Year && a.AcademicYearID == academicYear && a.IsActive == true
                    && ((a.FirstSemesterStartDate < DateTime.Now && a.FirstSemesterEndDate >= DateTime.Now)
                       || (a.SecondSemesterStartDate < DateTime.Now && a.SecondSemesterEndDate >= DateTime.Now))).FirstOrDefault();

                    //hath edit append nam hoc len combobox*****
                    if (ac != null)
                    {
                        Session[GlobalConstants.IsCurrentYear] = true;
                        if (DateTime.Now >= ac.SecondSemesterStartDate && DateTime.Now <= ac.SecondSemesterEndDate)
                        {
                            Session[GlobalConstants.IsCurrentYear] = true; //ngay hien tai la trong nam hoc
                            if (ac.FirstSemesterStartDate < DateTime.Now && ac.FirstSemesterEndDate > DateTime.Now)
                            {
                                Session[GlobalConstants.SEMESTER] = GlobalConstants.FIRST_SEMESTER;
                            }
                            if (ac.SecondSemesterStartDate < DateTime.Now && ac.SecondSemesterEndDate > DateTime.Now)
                            {
                                Session[GlobalConstants.SEMESTER] = GlobalConstants.SECOND_SEMESTER;
                            }

                        }

                        Session[GlobalConstants.ACADEMICYEAR_ID] = ac.AcademicYearID;
                        //SetCookie();
                        //
                        //Hungnd add teacher permission view all 17/04/2013
                        AcademicYear objAcademicYear = AcademicYearBusiness.Find(ac.AcademicYearID);
                        if (objAcademicYear != null)
                        {
                            Session[GlobalConstants.IS_VIEW_ALL] = objAcademicYear.IsTeacherCanViewAll;
                            _globalInfo.IsShowCalendar = objAcademicYear.IsShowCalendar.HasValue ? objAcademicYear.IsShowCalendar.Value : false;
                            _globalInfo.IsShowPupilImage = objAcademicYear.IsShowAvatar.HasValue ? objAcademicYear.IsShowAvatar.Value : false;
                        }
                    }
                    else
                    {
                        Session[GlobalConstants.IsCurrentYear] = false;
                    }

                    SetClassProfile(academicYear);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }
        [AllowAnonymous]
        [SkipCheckRole]
        public PartialViewResult LogonGetCapcha()
        {
            return PartialView("_Capcha");
        }
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogonCaptchaImage()
        {
            return LogonGenerateCaptcha();
        }

        public ActionResult GetEmployeeIndex()
        {
            return View();
        }

        /// <summary>
        /// Tra ve thong bao khong co quyen truy cap chuc nang
        /// </summary>
        /// <returns>Namta</returns>
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult RoleError()
        {
            ViewData["LabelRole"] = Res.Get("Common_Label_RoleError");
            if (Session["TypeError"] != null && (int)Session["TypeError"] == 1)
            {
                ViewData["LabelRole"] = Res.Get("Home_Role_ErrorTeacher");
            }
            else if (Session["ErrorStr"] != null)
            {
                ViewData["LabelRole"] = Session["ErrorStr"];
            }
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult ChooseVersionAction(int ProductVersionID)
        {
            //cap nhat schoolprofile
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            objSP.ProductVersion = ProductVersionID;
            SchoolProfileBusiness.Update(objSP);
            SchoolProfileBusiness.Save();
            List<RoleViewModel> roles = TempData[GlobalConstants.ROLES] as List<RoleViewModel>;
            if (roles != null) ViewData["roles"] = new SelectList(roles.ToList(), "RoleID", "RoleName");
            GlobalInfo globalInfo = new GlobalInfo();
            ListAppliedLevel allAppliedLevel = new ListAppliedLevel();
            globalInfo.ProductVersionID = ProductVersionID;
            // lay tat ca cac AppliedLevel     
            if (globalInfo.AppliedLevels != null && globalInfo.AppliedLevels.Count() > 0)
            {
                List<AppliedLevel> lsAppliedLevel = allAppliedLevel.GetListAppliedLevel(globalInfo.AppliedLevels);
                List<ViettelCheckboxList> listCheckBox = lsAppliedLevel.Select(ap => new ViettelCheckboxList { Label = ap.AppliedLevelName, Value = ap.AppliedLevelID, cchecked = false, disabled = false }).ToList();
                ViewData[GlobalConstants.APPLIED_LEVELS] = listCheckBox;
                return Json(new JsonMessage("", "Authorize"));
            }
            else
            {
                return Json(new JsonMessage("", "Index"));
            }

        }
        #endregion

        #region Tra ve trang khong có quyen thao thác chức năng cho PupilProfile
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult NotRole()
        {
            ViewData["LabelRole"] = Res.Get("Common_Label_RoleError");
            return View("RoleError");
        }
        #endregion



        #region Extent method
        //tanla4
        //25/11/2014
        //download file from ftp server
        public Stream DownloadFileFromFTP(string ftpurl, string ftpusername, string ftppassword)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpurl);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(ftpusername, ftppassword);

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();

            return responseStream;
        }

        //tanla4
        //26/11/2014
        //download file from http server
        public Stream DownloadFileFromHTTP(string httpurl)
        {
            // Get the object used to communicate with the server.
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpurl);
            request.Method = WebRequestMethods.Http.Get;

            // This example assumes the HTTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(Config.UsernameUpload, Config.PasswordUpload);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();

            return responseStream;
        }

        // DungVA - 15/05/2013 - Download file thông báo trên trang ch?
        public FileResult SaveFileAlert(int? id)
        {
            string templatePath = "";
            string fileName = "";
            string shortFileName = "";
            if (id.HasValue)
            {

                AlertMessage am = AlertMessageBusiness.Find(id);
                // Láº¥y file path
                templatePath = am.FileUrl;
                // Láº¥y tĂªn file
                fileName = Path.GetFileName(templatePath);

                try
                {
                    string[] arrName = templatePath.Split('.');
                    string extension = arrName[arrName.Length - 1];
                    string res = templatePath.Remove(templatePath.LastIndexOf('_'));
                    res = res + "." + extension;
                    shortFileName = Path.GetFileName(res);
                }
                catch
                {
                }

            }

            //var t = new FileStream(templatePath,FileMode.Open);
            //FileStreamResult result2 = new FileStreamResult(t, "application/octet-stream");

            //FileStream fileStream = System.IO.File.OpenRead(templatePath);  

            ////create new MemoryStream object
            //MemoryStream memStream = new MemoryStream();
            //memStream.SetLength(fileStream.Length);
            ////read file to MemoryStream
            //fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            //Stream s1 = new MemoryStream(memStream.ToArray());

            //tanla4
            //25/11/2014
            //download file from ftp server
            //Stream s1 = DownloadFileFromFTP(templatePath,"","");

            //download file from http server
            Stream s1 = DownloadFileFromHTTP(templatePath);
            FileStreamResult result2 = new FileStreamResult(s1, "application/octet-stream");
            if (!String.IsNullOrEmpty(shortFileName))
            {
                result2.FileDownloadName = shortFileName;
            }
            else
            {
                result2.FileDownloadName = fileName;
            }
            return result2;
        }
        // End DungVA
        /// <summary>
        /// Luu Group, Role, Menu vao session
        /// </summary>
        /// <param name="GroupCatID"></param>
        /// <param name="RoleID"></param>
        /// <param name="ctx"></param>
        private void SetGroupAndMenu(int GroupCatID, int RoleID, HttpContextBase ctx)
        {
            //ctx.Session[GlobalConstants.SELECTED_GROUP] = group;
            ctx.Session[GlobalConstants.GROUPID] = GroupCatID;
            ctx.Session[GlobalConstants.ROLEID] = RoleID;
            IQueryable<Menu> menus = GroupMenuBusiness.GetMenuOfGroup(GroupCatID);
            if (menus != null)
            {
                if (_globalInfo.IsAdmin)
                {
                    if (_globalInfo.IsSystemAdmin)
                    {
                        ctx.Session[GlobalConstants.MENUS] = menus.ToList();
                    }
                    else if (_globalInfo.IsSchoolRole)
                    {
                        string productVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : GlobalConstants.PRODUCT_VERSION_ID_FULL;
                        ctx.Session[GlobalConstants.MENUS] = menus.Where(p => (p.ProductVersion != null && p.ProductVersion.Contains(productVersionID))).ToList();
                    }
                }

            }
        }


        /// <summary>
        /// Luu Group, Role, Menu vao session
        /// </summary>
        /// <param name="GroupCatID"></param>
        /// <param name="RoleID"></param>
        /// <param name="ctx"></param>
        private void SetGroupListAndMenu(List<int> ListGroupCatID, int RoleID, HttpContextBase ctx)
        {
            //ctx.Session[GlobalConstants.SELECTED_GROUP] = group;
            ctx.Session[GlobalConstants.ROLEID] = RoleID;
            IQueryable<Menu> menus = GroupMenuBusiness.GetMenuOfListGroup(ListGroupCatID);
            string productVersionID = string.Empty;
            if (menus != null)
            {
                if (RoleID == GlobalConstants.ROLE_PHONG || RoleID == GlobalConstants.ROLE_SO)
                {
                    ctx.Session[GlobalConstants.MENUS] = menus.ToList();
                }
                else
                {
                    if (_globalInfo.IsAdmin)
                    {
                        if (_globalInfo.IsSystemAdmin || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
                        {
                            ctx.Session[GlobalConstants.MENUS] = menus.ToList();
                        }
                        else if (_globalInfo.IsSchoolRole)
                        {
                            productVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : "";
                            ctx.Session[GlobalConstants.MENUS] = menus.Where(p => (p.ProductVersion != null && p.ProductVersion.Contains(productVersionID))).ToList();
                        }
                    }
                    else
                    {
                        productVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : GlobalConstants.PRODUCT_VERSION_ID_FULL;//neu khong phai admin mac dinh cho phien ban full
                        ctx.Session[GlobalConstants.MENUS] = menus.Where(p => (p.ProductVersion != null && p.ProductVersion.Contains(productVersionID))).ToList();
                    }
                }
            }
        }

        private void SetRoleAndMenu(int RoleID, HttpContextBase ctx)
        {
            ctx.Session[GlobalConstants.ROLEID] = RoleID;
            List<Menu> menus = null;
            List<Menu> menus_role = null;
            string strproductVersionID = string.Empty;
            if (SMAS.Business.Common.StaticData.DicMenuRole.ContainsKey(RoleID))
            {
                menus_role = SMAS.Business.Common.StaticData.DicMenuRole[RoleID];
                if (_globalInfo.IsAdmin)
                {
                    if (_globalInfo.IsSystemAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                    {
                        menus = menus_role.Where(o => o.IsVisible).ToList();
                    }
                    else
                    {
                        strproductVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : "";
                        menus = menus_role.Where(o => o.IsVisible && (o.ProductVersion != null && o.ProductVersion.Contains(strproductVersionID))).ToList();
                    }
                }
                else
                {
                    strproductVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : GlobalConstants.PRODUCT_VERSION_ID_FULL;
                    menus = menus_role.Where(o => o.IsVisible && (o.ProductVersion != null && o.ProductVersion.Contains(strproductVersionID))).ToList();
                }
            }

            if (menus != null)
            {
                ctx.Session[GlobalConstants.MENUS] = menus;
                ctx.Session["MenuRoles"] = menus_role;
            }
        }

        private void SetAuthCookie(LogOnModel model)
        {
            if (model == null)
            {
                return;
            }
            FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
        }

        private void SessionStorage(UserAccount UserAccount, int RoleID,
            int? GroupID, bool isAdmin, Employee Employee)
        {
            int UserAccountID = UserAccount.UserAccountID;
            bool SuperVisingDeptParam = roleBusiness.IsSuperVisingDeptRole(RoleID);
            bool SubSuperVisingDeptParam = roleBusiness.IsSubSuperVisingDeptRole(RoleID);
            if (isAdmin)
            {
                Session[GlobalConstants.IS_ADMIN] = true;
                //neu nguoi dang nhap la admin
                if (roleBusiness.IsSchoolRole(RoleID))
                {
                    Session[GlobalConstants.IS_SCHOOL_ROLE] = true;
                    //neu la admin cap truong
                    //tim kiem truong cua nguoi dang nhap
                    SchoolProfile school = UserAccount.SchoolProfiles.FirstOrDefault();
                    if (school != null)
                    {
                        //Session[GlobalConstants.SCHOOLID] = school.SchoolProfileID;
                        //Session[GlobalConstants.SCHOOL_NAME] = school.SchoolName;
                        Session[GlobalConstants.TRAINING_TYPE] = school.TrainingTypeID;
                        //lay list AcademicYear theo SchoolID
                        SupervisingDept supervisingDept = school.SupervisingDept;
                        if (supervisingDept != null)
                        {
                            Session[GlobalConstants.SUPERVISINGDEPT_ID] = supervisingDept.SupervisingDeptID;
                            Session[GlobalConstants.SUPERVISINGDEPT_NAME] = supervisingDept.SupervisingDeptName;
                            Session[GlobalConstants.PROVINCEID] = supervisingDept.ProvinceID;
                            Session[GlobalConstants.DISTRICTID] = supervisingDept.DistrictID;
                        }
                        SetAcademicYearBySchool(school.SchoolProfileID);
                    }
                }
                else if (SuperVisingDeptParam || SubSuperVisingDeptParam)
                {
                    //neu la admin cap phong so
                    //tim trong bang SuperVisingDept ban ghi co AdminID = UserAccountID
                    //Neu tim thay thi thiet lap SUPERVISINGDEPT_ID va SUPERVISINGDEPT_NAME
                    if (SuperVisingDeptParam)
                    {
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = true;

                    }
                    else
                    {
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                    }
                    if (SubSuperVisingDeptParam)
                    {
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = true;
                    }
                    else
                    {
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                    }


                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AdminID"] = UserAccount.UserAccountID;
                    IQueryable<SupervisingDept> supervisingDept = SupervisingDeptBusiness.Search(dic);
                    if (supervisingDept != null)
                    {
                        SupervisingDept sd = supervisingDept.FirstOrDefault();
                        if (sd != null)
                        {
                            Session[GlobalConstants.SUPERVISINGDEPT_ID] = sd.SupervisingDeptID;
                            Session[GlobalConstants.SUPERVISINGDEPT_NAME] = sd.SupervisingDeptName;
                            Session[GlobalConstants.PROVINCEID] = sd.ProvinceID;
                            Session[GlobalConstants.DISTRICTID] = sd.DistrictID;
                            this.SetSuperVisingDeptLevel(sd);
                        }
                    }
                    if (UserAccountBusiness.IsSystemAdmin(UserAccountID))
                    {
                        Session[GlobalConstants.IS_SYSTEM_ADMIN] = true;
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                    }
                    SupervisingDept svd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(UserAccountID);
                    if (svd == null)
                    {
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                    }
                    else
                    {
                        if (UserAccountBusiness.IsSupervisingDeptAdmin(UserAccountID) && svd.HierachyLevel == 3)
                        {
                            Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = true;
                            Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                            Session[GlobalConstants.IS_SYSTEM_ADMIN] = false;
                        }
                        if (UserAccountBusiness.IsSupervisingDeptAdmin(UserAccountID) && svd.HierachyLevel == 5)
                        {
                            Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                            Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = true;
                            Session[GlobalConstants.IS_SYSTEM_ADMIN] = false;
                        }

                    }
                }
                else if (roleBusiness.IsProvinceRole(RoleID))
                {
                    //neu la admin cap tinh
                }
                else if (roleBusiness.IsSuperRole(RoleID))
                {
                    //neu la admin cap cao
                    Session[GlobalConstants.IS_SUPERROLE] = true;
                    if (UserAccountBusiness.IsSystemAdmin(UserAccountID))
                    {
                        Session[GlobalConstants.IS_SYSTEM_ADMIN] = true;
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                    }
                }

            }
            else
            {
                //neu nguoi dang nhap ko phai admin
                Session[GlobalConstants.IS_ADMIN] = false;
                //if (EmployeeID.HasValue)
                //{
                //    Session[GlobalConstants.EMPLOYEEID] = EmployeeID;
                //}
                if (roleBusiness.IsSchoolRole(RoleID))
                {
                    Session[GlobalConstants.IS_SCHOOL_ROLE] = true;
                    //neu la nguoi dung cap truong
                    if (Employee != null)
                    {
                        if (Employee.SchoolProfile != null)
                        {
                            //Session[GlobalConstants.SCHOOLID] = e.SchoolProfile.SchoolProfileID;
                            //Session[GlobalConstants.SCHOOL_NAME] = e.SchoolProfile.SchoolName;
                            //Kiá»ƒm tra cĂ³ pháº£i lĂ  cĂ¡n bá»™ quáº£n lĂ½ khĂ´ng?
                            if (Employee.WorkGroupType != null && Employee.WorkGroupType.Resolution == Res.Get("WorkGroupType_Resolution_CBQL"))
                            {
                                Session[GlobalConstants.IS_EMPLOYEE_MANAGER] = true;
                            }
                            else
                            {
                                Session[GlobalConstants.IS_EMPLOYEE_MANAGER] = false;
                            }
                            Session[GlobalConstants.TRAINING_TYPE] = Employee.SchoolProfile.TrainingTypeID;
                            SetAcademicYearBySchool(Employee.SchoolID.Value);
                            //nguoi dung la user thuong cua truong -> lay classID neu co
                            SetClassProfile((int)Session[GlobalConstants.ACADEMICYEAR_ID]);
                            // QuangLM bo sung them ID tinh de nguoi dung cap truong neu duoc gan quyen
                            // vao chuc nang xem ho so truong thi co the vao duoc
                            if (Employee.SchoolProfile.SupervisingDeptID.HasValue)
                            {
                                Session[GlobalConstants.PROVINCEID] = Employee.SchoolProfile.SupervisingDept.ProvinceID;
                                Session[GlobalConstants.DISTRICTID] = Employee.SchoolProfile.SupervisingDept.DistrictID;
                            }
                        }
                    }
                }
                else if (SuperVisingDeptParam || SubSuperVisingDeptParam)
                {
                    //neu la nguoi dung cap phong so
                    //false tá»« UserAccountID => EmployeeID (trong UserAccount) => SupervingDeptID 
                    //(trong EmployeeID)
                    Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = true;

                    if (SuperVisingDeptParam)
                    {
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = true;
                    }
                    else
                    {
                        Session[GlobalConstants.IS_SUPERVISINGDEPT_ROLE] = false;
                    }
                    if (SubSuperVisingDeptParam)
                    {
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = true;
                    }
                    else
                    {
                        Session[GlobalConstants.IS_SUBSUPERVISINGDEPT_ROLE] = false;
                    }

                    if (Employee != null)
                    {
                        if (Employee.WorkGroupType != null && Employee.WorkGroupType.Resolution == Res.Get("WorkGroupType_Resolution_CBQL"))
                        {
                            Session[GlobalConstants.IS_EMPLOYEE_MANAGER] = true;
                        }
                        else
                        {
                            Session[GlobalConstants.IS_EMPLOYEE_MANAGER] = false;
                        }
                        if (Employee.SupervisingDeptID != null)
                        {

                            Session[GlobalConstants.SUPERVISINGDEPT_ID] = Employee.SupervisingDeptID;
                            SupervisingDept s = SupervisingDeptBusiness.Find(Employee.SupervisingDeptID);
                            if (s != null)
                            {
                                Session[GlobalConstants.SUPERVISINGDEPT_NAME] = s.SupervisingDeptName;
                                Session[GlobalConstants.PROVINCEID] = s.ProvinceID;
                                Session[GlobalConstants.DISTRICTID] = s.DistrictID;
                                //neu la cap so
                                SetSuperVisingDeptLevel(s);
                            }
                        }
                    }
                }
            }
            // Luu thong tin co phai la admin truong hay khong
            bool isAdminSchool = UserAccountBusiness.IsSchoolAdmin(UserAccountID);
            Session[GlobalConstants.IS_SCHOOL_ADMIN] = isAdminSchool;
        }

        private void SetSuperVisingDeptLevel(SupervisingDept s)
        {
            if (s.HierachyLevel == SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE"))
            {
                Session[GlobalConstants.IS_PROVINCE_LEVEL] = true;
            }
            else if (s.HierachyLevel == SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE"))
            {
                Session[GlobalConstants.IS_DISTRICT_LEVEL] = true;
            }
        }

        /// <summary>
        /// Set gia tri dang chon nam hoc o trang chu
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="schoolID"></param>
        private void SetAcademicYearSelect(int AcademicYearID, int schoolID)
        {
            List<AcademicYear> acaResults = AcademicYearBusiness.SearchBySchool(schoolID).Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value)).OrderByDescending(o => o.Year).ToList();
            //neu ket qua ko rong thi luu vao session
            if (acaResults != null && acaResults.Any())
            {
                List<AcademicYearForm> listAcaForm = new List<AcademicYearForm>();
                // luu list academicYear de lua chon trong combobox
                AcademicYearForm selectedAcademicYear = null;
                foreach (var item in acaResults)
                {
                    AcademicYearForm academicYearForm = new AcademicYearForm();
                    Utils.Utils.BindTo(item, academicYearForm, true);
                    listAcaForm.Add(academicYearForm);
                    if (academicYearForm.AcademicYearID == AcademicYearID)
                    {
                        selectedAcademicYear = academicYearForm;
                    }
                }
                if (selectedAcademicYear != null)
                {
                    int pos = listAcaForm.IndexOf(selectedAcademicYear);
                    Session[GlobalConstants.SELECTED_ACADEMICYEAR_INDEX] = pos;
                    //Hungnd add teacher permission view all 17/04/2013
                    Session[GlobalConstants.IS_VIEW_ALL] = selectedAcademicYear.IsTeacherCanViewAll;
                    _globalInfo.IsShowCalendar = selectedAcademicYear.IsShowCalendar.HasValue ? selectedAcademicYear.IsShowCalendar.Value : false;
                    _globalInfo.IsShowPupilImage = selectedAcademicYear.IsShowAvatar.HasValue ? selectedAcademicYear.IsShowAvatar.Value : false;
                    //End 17/04/2013
                    //
                }
            }
        }

        private void SetAcademicYearBySchool(int schoolID)
        {
            List<AcademicYear> acaResults = AcademicYearBusiness.SearchBySchool(schoolID).Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value)).ToList();
            //neu ket qua ko rong thi luu vao session
            if (acaResults != null && acaResults.Any())
            {
                List<AcademicYearForm> listAcaForm = new List<AcademicYearForm>();
                // luu list academicYear de lua chon trong combobox
                foreach (var item in acaResults)
                {
                    AcademicYearForm academicYearForm = new AcademicYearForm();
                    Utils.Utils.BindTo(item, academicYearForm, true);
                    listAcaForm.Add(academicYearForm);
                }
                listAcaForm = listAcaForm.OrderByDescending(o => o.Year).ToList();
                //Session[GlobalConstants.LIST_ACADEMICYEAR] = listAcaForm;
                Session[GlobalConstants.LIST_ACADEMICYEAR] = new SelectList(listAcaForm, "AcademicYearID", "DisplayTitle");
                // lay academicYear theo nam hien tai va luu vao session
                /* 
                 chá»n nÄƒm mĂ  chÆ°a ngĂ y hiá»‡n táº¡i (kiá»ƒm tra trong báº£ng AcademicYear). 
                 * Náº¿u khĂ´ng cĂ³ nÄƒm nĂ o chá»©a ngĂ y hiá»‡n táº¡i thĂ¬ chá»n nÄƒm nhá» hÆ¡n 
                 * vĂ  gáº§n nháº¥t ngĂ y hiá»‡n táº¡i
                 */
                //kiem tra ngay hien tai co nam trong thoi gian 2 hoc ki cua nam hoc ko
                DateTime curDate = DateTime.Now.Date;
                AcademicYearForm currentAcademicYear = listAcaForm.Where(a => a.Year <= DateTime.Now.Year
                    && ((a.FirstSemesterStartDate.Value.Date <= curDate && a.FirstSemesterEndDate >= curDate)
                       || (a.SecondSemesterStartDate.Value.Date <= curDate && a.SecondSemesterEndDate >= curDate))).FirstOrDefault();
                Session[GlobalConstants.SEMESTER] = GlobalConstants.FIRST_SEMESTER; // hath fix 1 -> globalconstants
                //IsCurrentYear: TRUE náº¿u tá»“n táº¡i nÄƒm há»c mĂ  chá»©a ngĂ y hiá»‡n táº¡i 
                if (currentAcademicYear != null)
                {
                    Session[GlobalConstants.IsCurrentYear] = true; //ngay hien tai la trong nam hoc
                    if (currentAcademicYear.FirstSemesterStartDate.Value.Date <= curDate && currentAcademicYear.FirstSemesterEndDate.Value.Date >= curDate)
                    {
                        Session[GlobalConstants.SEMESTER] = GlobalConstants.FIRST_SEMESTER; // hath fix 1 -> globalconstants
                    }
                    if (currentAcademicYear.SecondSemesterStartDate.Value.Date <= curDate && currentAcademicYear.SecondSemesterEndDate.Value.Date >= curDate)
                    {
                        Session[GlobalConstants.SEMESTER] = GlobalConstants.SECOND_SEMESTER; // hath fix 2 -> globalconstants
                    }
                }
                else
                {
                    //lay nam hoc co year lon nhat de chon neu thoi gian hien tai khong nam trong hk nao NAMTA
                    int currentYear = DateTime.Now.Year;
                    //sap xep theo Year thu tu giam dan, nhung AcademicYear nho hon nam hien tai
                    IEnumerable<AcademicYearForm> orderList = listAcaForm.OrderByDescending(a => a.Year);
                    if (orderList != null)
                    {
                        currentAcademicYear = orderList.FirstOrDefault();
                        Session[GlobalConstants.ACADEMICYEAR_ID] = currentAcademicYear.AcademicYearID;
                        Session[GlobalConstants.SELECTED_ACADEMICYEAR_INDEX] = listAcaForm.IndexOf(currentAcademicYear);
                        //Hungnd add teacher permission view all 17/04/2013                        
                        Session[GlobalConstants.IS_VIEW_ALL] = currentAcademicYear.IsTeacherCanViewAll;
                        _globalInfo.IsShowCalendar = currentAcademicYear.IsShowCalendar.HasValue ? currentAcademicYear.IsShowCalendar.Value : false;
                        _globalInfo.IsShowPupilImage = currentAcademicYear.IsShowAvatar.HasValue ? currentAcademicYear.IsShowAvatar.Value : false;
                    }
                }
                if (currentAcademicYear != null)
                {
                    Session[GlobalConstants.ACADEMICYEAR_ID] = currentAcademicYear.AcademicYearID;
                    Session[GlobalConstants.SELECTED_ACADEMICYEAR_INDEX] = listAcaForm.IndexOf(currentAcademicYear);
                    //Hungnd add teacher permission view all 17/04/2013

                    Session[GlobalConstants.IS_VIEW_ALL] = currentAcademicYear.IsTeacherCanViewAll;
                    _globalInfo.IsShowCalendar = currentAcademicYear.IsShowCalendar.HasValue ? currentAcademicYear.IsShowCalendar.Value : false;
                    _globalInfo.IsShowPupilImage = currentAcademicYear.IsShowAvatar.HasValue ? currentAcademicYear.IsShowAvatar.Value : false;

                    //End 17/04/2013
                }

            }
            //For SSO
            // lay len ID cua Smas 2.0 set vao bien oldSchoolYear
            //AddOldAcademicYear();
            // SetCookie();
            // END - SSO
        }

        private void SetClassProfile(int AcademicYearID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            int? HeadTeacherID = globalInfo.EmployeeID;
            if (HeadTeacherID.HasValue)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("HeadTeacherID", HeadTeacherID);
                IQueryable<ClassProfile> classProfiles = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID, dic);
                ClassProfile classProfile = classProfiles.FirstOrDefault();
                if (classProfile != null)
                {
                    Session[GlobalConstants.CLASSID] = classProfile.ClassProfileID;
                    Session[GlobalConstants.EDUCATION_LEVEL] = classProfile.EducationLevelID;
                }
            }
        }

        //Lay cac cap hoc ma nguoi dung duoc gan
        private void SetAppliedLevelAndEduLevels(int RoleID)
        {

            int appliedLevel = Utils.Utils.MapRoleToAppliedLevel(RoleID);
            Session[GlobalConstants.APPLIED_LEVEL] = appliedLevel;
            IQueryable<EducationLevel> educationlevels = EducationLevelBusiness.GetByGrade(appliedLevel);
            if (educationlevels != null)
            {
                Session[GlobalConstants.EDUCATION_LEVELS] = educationlevels.ToList();
            }
            //SetCookie();
        }

        private void SetAppliedLevels(List<Role> roles)
        {
            List<int> appliedLevels = new List<int>();

            foreach (var role in roles)
            {
                int appliedLevel = Utils.Utils.MapRoleToAppliedLevel(role.RoleID);
                appliedLevels.Add(appliedLevel);
            }
            List<int> lstappliedLevels = new List<int>();
            if (appliedLevels.Contains(4))
            {
                lstappliedLevels.Add(4);
            }
            if (appliedLevels.Contains(5))
            {
                lstappliedLevels.Add(5);
            }
            if (appliedLevels.Contains(1))
            {
                lstappliedLevels.Add(1);
            }
            if (appliedLevels.Contains(2))
            {
                lstappliedLevels.Add(2);
            }
            if (appliedLevels.Contains(3))
            {
                lstappliedLevels.Add(3);
            }
            Session[GlobalConstants.APPLIED_LEVELS] = lstappliedLevels;

        }

        //Viethd4: Lay bang tin
        private IQueryable<AlertMessageBO> GetNews()
        {

            IQueryable<AlertMessage> res = new List<AlertMessage>().AsQueryable();

            //Cacs typeID truong duoc xem neu la tin cua don vi quan ly
            List<int> listSchoolTypeID = new List<int>(){
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL,
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL_SUPERVISINGDEPTSUB,
                AlertMessageConstants.TYPE_ID_IS_SCHOOL,
                AlertMessageConstants.TYPE_ID_IS_SCHOOL_SUPERVISINGDEPTSUB
            };

            //Cac typeID phong duoc xem neu la tin cua so
            List<int> listSubSupervisingTypeID = new List<int>() {
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL_SUPERVISINGDEPTSUB,
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SUPERVISINGDEPTSUB,
                AlertMessageConstants.TYPE_ID_IS_SCHOOL_SUPERVISINGDEPTSUB,
                AlertMessageConstants.TYPE_ID_IS_SUPERVISINGDEPT

            };

            //Cac typeID de xem tin noi bo
            List<int> listPrivateTypeID = new List<int>()
            {
                AlertMessageConstants.TYPE_ID_IS_PRIVATE,
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL,
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL_SUPERVISINGDEPTSUB,
                AlertMessageConstants.TYPE_ID_IS_PRIVATE_SUPERVISINGDEPTSUB
            };


            //Neu la account truong
            if (_globalInfo.SchoolID != null && _globalInfo.SchoolID > 0)
            {
                //Lay UnitID cua truong
                int schoolUnitID = _globalInfo.SchoolID.Value;

                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                SupervisingDept dept = school.SupervisingDept;
                int supervisingDeptID = school.SupervisingDeptID.GetValueOrDefault();
                List<int?> parentDeptTreeID = new List<int?>();
                //Neu don vi quan ly cua truong la phong
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    //Tin cua phong va cac phong ban truc thuoc phong
                    parentDeptTreeID.Add(supervisingDeptID);
                    parentDeptTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());

                    //Tin thuoc so va cac phong ban truc thuoc so
                    parentDeptTreeID.Add(dept.ParentID);
                    parentDeptTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == dept.ParentID
                                                            && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());
                }
                //Neu don vi quan ly cua truong la so
                else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                {
                    //Tin cua so va cac phong ban truc thuoc so
                    parentDeptTreeID.Add(supervisingDeptID);
                    parentDeptTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());
                }

                res = AlertMessageBusiness.All.Where(o => o.UnitID == schoolUnitID || (parentDeptTreeID.Contains(o.UnitID) && listSchoolTypeID.Contains(o.TypeID)));

            }
            //Account phong 
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
                List<int?> parentDeptTreeID = new List<int?>();
                List<int?> listPrivateTreeID = new List<int?>();
                //Cấp phòng, ban thuộc Phòng Giáo dục 
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    //Lay cac tin cua Phong giao duc va cac phong ban truc thuoc phong
                    SupervisingDept subSupervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    listPrivateTreeID.Add(subSupervisingDept.SupervisingDeptID);
                    listPrivateTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == subSupervisingDept.SupervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());


                    //Lay cac tin cua so va cac phong ban truc thuoc so
                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(subSupervisingDept.ParentID);
                    parentDeptTreeID.Add(supervisingDept.SupervisingDeptID);
                    parentDeptTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDept.SupervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());

                    // res = AlertMessageBusiness.All.Where(o => ((o.UnitID == supervisingDeptID||o.UnitID==subParentDeptID) && listPrivateTypeID.Contains(o.TypeID)) || (o.UnitID == parentDeptID && listSubSupervisingTypeID.Contains(o.TypeID)));
                }
                else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    listPrivateTreeID.Add(supervisingDeptID);
                    //Lay cac tin cua phong ban ben duoi
                    listPrivateTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                                                          && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());

                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    parentDeptTreeID.Add(supervisingDept.SupervisingDeptID);
                    parentDeptTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDept.SupervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());

                }

                res = AlertMessageBusiness.All.Where(o => (listPrivateTreeID.Contains(o.UnitID) && listPrivateTypeID.Contains(o.TypeID)) || (parentDeptTreeID.Contains(o.UnitID) && listSubSupervisingTypeID.Contains(o.TypeID)));

            }
            //account so
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
                List<int?> listPrivateTreeID = new List<int?>();

                //Neu la phong ban thuoc so
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    listPrivateTreeID.Add(supervisingDept.SupervisingDeptID);
                    listPrivateTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDept.SupervisingDeptID
                                                              && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());
                }
                else if (dept != null && dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                {
                    //Lay cac tin cua phong ban ben duoi
                    listPrivateTreeID.Add(supervisingDeptID);
                    //Lay cac tin cua phong ban ben duoi
                    listPrivateTreeID.AddRange(SupervisingDeptBusiness.All.Where(o => o.IsActive && o.ParentID == supervisingDeptID
                                                          && o.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT).Select(o => (int?)o.SupervisingDeptID).ToList());


                }
                res = AlertMessageBusiness.All.Where(o => (listPrivateTreeID.Contains(o.UnitID)) && listPrivateTypeID.Contains(o.TypeID));

            }

            return res.Where(o => o.IsPublish == true && o.IsActive == true).Select(p => new AlertMessageBO
            {
                AlertMessageID = p.AlertMessageID,
                Title = p.Title,
                TypeID = p.TypeID,
                ContentMessage = p.ContentMessage,
                PublishedDate = p.PublishedDate,
                FileUrl = p.FileUrl,
                UnitID = p.UnitID,
                CreatedUserID = p.CreatedUserID
            }).OrderByDescending(a => a.PublishedDate).ThenByDescending(p => p.AlertMessageID);
        }
        #endregion

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        #region Extent Login
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public FileContentResult LogonGenerateCaptcha()
        {
            FileContentResult captchaImg = null;
            string[] fonts = { "Arial", "Verdana", "Times New Roman", "Tahoma" };
            Random ran = new Random();
            int LENGTH = ran.Next(4, 6);

            // chuá»—i Ä‘á»ƒ láº¥y cĂ¡c kĂ­ tá»± sáº½ sá»­ dá»¥ng cho captcha
            const string chars = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (Bitmap bmp = new Bitmap(120, 30))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        // Táº¡o ná»n cho áº£nh dáº¡ng sĂ³ng
                        HatchBrush brush = new HatchBrush(HatchStyle.LightDownwardDiagonal, Color.Wheat, Color.Silver);
                        g.FillRegion(brush, g.Clip);
                        // LÆ°u chuá»—i captcha trong quĂ¡ trĂ¬nh táº¡o
                        StringBuilder strCaptcha = new StringBuilder();
                        Random rand = new Random();
                        for (int i = 0; i < LENGTH; i++)
                        {
                            // Láº¥y kĂ­ tá»± ngáº«u nhiĂªn tá»« máº£ng chars
                            string str = chars[rand.Next(chars.Length)].ToString();
                            strCaptcha.Append(str);
                            // Táº¡o font vá»›i tĂªn font ngáº«u nhiĂªn chá»n tá»« máº£ng fonts
                            Font font = new Font(fonts[rand.Next(fonts.Length)], 14, FontStyle.Italic | FontStyle.Bold);
                            // Láº¥y kĂ­ch thÆ°á»›c cá»§a kĂ­ tá»±
                            SizeF size = g.MeasureString(str, font);
                            // Váº½ kĂ­ tá»± Ä‘Ă³ ra áº£nh táº¡i vá»‹ trĂ­ tÄƒng dáº§n theo i, vá»‹ trĂ­ top ngáº«u nhiĂªn
                            g.DrawString(str, font,
                            Brushes.Blue, i * size.Width + 4, rand.Next(2, 10));
                            font.Dispose();
                        }
                        // LÆ°u captcha vĂ o session
                        Session["Captcha"] = strCaptcha.ToString();
                        // Ghi áº£nh trá»±c tiáº¿p ra luá»“ng xuáº¥t theo Ä‘á»‹nh dáº¡ng gif

                        bmp.Save(memoryStream, ImageFormat.Gif);
                        captchaImg = this.File(memoryStream.GetBuffer(), "image/Jpeg");
                    }
                }
            }
            return captchaImg;
        }

        public SessionInfo AddSession()
        {
            GlobalInfo global = new GlobalInfo();
            SessionInfo thisSession = new SessionInfo();
            thisSession.AcademicYearID = global.AcademicYearID;
            thisSession.AppliedLevel = global.AppliedLevel;
            thisSession.AppliedLevels = global.AppliedLevels;
            thisSession.ClassID = global.ClassID;
            thisSession.DistrictID = global.DistrictID;
            thisSession.EducationLevel = global.EducationLevel ?? (int)0;
            thisSession.EducationLevels = global.EducationLevels;
            thisSession.GroupID = global.GroupID;
            thisSession.IsAdmin = global.IsAdmin;
            thisSession.IsSchoolRole = global.IsSchoolRole;
            thisSession.IsSuperRole = global.IsSuperRole;
            thisSession.IsSuperVisingDeptRole = global.IsSuperVisingDeptRole;
            thisSession.RoleID = global.RoleID;
            thisSession.SchoolID = global.SchoolID;
            thisSession.SchoolName = global.SchoolName;
            thisSession.Semester = global.Semester;
            thisSession.SupervisingDeptID = global.SupervisingDeptID;
            thisSession.SuperVisingDeptName = global.SuperVisingDeptName;
            thisSession.UserAccountID = global.UserAccountID;
            thisSession.ProvinceID = global.ProvinceID;
            thisSession.EmployeeID = global.EmployeeID;
            thisSession.Selected_academicYear_Index = global.Selected_academicYear_Index;
            thisSession.IsSystemAdmin = global.IsSystemAdmin;
            thisSession.isViewAll = global.IsViewAll;
            thisSession.IsEmployeeManager = global.IsEmployeeManager;
            if (thisSession.isViewAll)
            {
                thisSession.strIsViewAll = "true";
            }
            else
            {
                thisSession.strIsViewAll = "false";
            }

            if (thisSession.IsAdmin)
            {
                thisSession.strIsAdmin = "true";
            }
            else
            {
                thisSession.strIsAdmin = "false";
            }

            if (thisSession.IsSystemAdmin)
            {
                thisSession.strIsSystemAdmin = "true";
            }
            else
            {
                thisSession.strIsAdmin = "false";
            }

            if (thisSession.IsSchoolRole)
            {
                thisSession.strIsSchoolRole = "true";
            }
            else
            {
                thisSession.strIsSchoolRole = "false";
            }

            if (thisSession.IsSuperRole)
            {
                thisSession.strIsSuperRole = "true";
            }
            else
            {
                thisSession.strIsSuperRole = "false";
            }

            if (thisSession.IsSuperVisingDeptRole)
            {
                thisSession.strIsSuperVisingDeptRole = "true";
            }
            else
            {
                thisSession.strIsSuperVisingDeptRole = "false";
            }

            if (thisSession.AppliedLevels != null)
            {
                thisSession.AppliedLevelsCount = thisSession.AppliedLevels.Count();
            }
            else
            {
                thisSession.AppliedLevelsCount = 0;
            }

            if (thisSession.EducationLevels != null)
            {
                thisSession.EducationLevelsCount = thisSession.EducationLevels.Count();
            }
            else
            {
                thisSession.EducationLevelsCount = 0;
            }

            if (thisSession.IsCurrentYear)
            {
                thisSession.strIsCurrentYear = "true";
            }
            else
            {
                thisSession.strIsCurrentYear = "false";
            }
            return thisSession;
        }
        //Láº¥y ra danh sĂ¡ch cĂ¡c khá»‘i há»c thuá»™c cáº¥p
        public List<EducationLevel> GetListEducationLevelByGrade(int AppliedLevel)
        {
            return EducationLevelBusiness.GetByGrade(AppliedLevel).ToList();
        }

        //Danh sĂ¡ch cĂ¡c lá»›p mĂ  giĂ¡o viĂªn cĂ³ quyá»n giĂ¡o viĂªn chá»§ nhiá»‡m
        public List<ClassProfile> GetListClassHeadTeacher(int userAccountID, int academicYearID, int schoolID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["UserAccountID"] = userAccountID;
            dic["AcademicYearID"] = academicYearID;
            dic["SchoolID"] = schoolID;
            dic["Type"] = SMAS.Business.Common.SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).ToList();
            return lstClass;
        }

        //Danh sĂ¡ch cĂ¡c lá»›p mĂ  cĂ³ quyá»n giĂ¡o viĂªn bá»™ mĂ´n
        public List<ClassProfile> GetListClassSubjectTeacher(int userAccountID, int academicYearID, int schoolID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["UserAccountID"] = userAccountID;
            dic["AcademicYearID"] = academicYearID;
            dic["SchoolID"] = schoolID;
            dic["Type"] = SMAS.Business.Common.SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchBySchool(schoolID, dic).ToList();
            return lstClass;
        }


        #endregion
        private void UpdateSession(string url, string data)
        {
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] baASCIIPostData = encoding.GetBytes(data);
                HttpWebRequest HttpWReq = (HttpWebRequest)WebRequest.Create(url);
                HttpWReq.Method = "POST";
                HttpWReq.Accept = "text/plain";
                HttpWReq.ContentType = "application/x-www-form-urlencoded";
                HttpWReq.ContentLength = baASCIIPostData.Length;
                Stream streamReq = HttpWReq.GetRequestStream();
                streamReq.Write(baASCIIPostData, 0, baASCIIPostData.Length);
                HttpWebResponse HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();
                Stream streamResponse = HttpWResp.GetResponseStream();
                StreamReader reader = new StreamReader(streamResponse);
                string response = reader.ReadToEnd();
                reader.Close();
                reader.Dispose();
            }
            catch
            {
                //TODO  write log?
            }
        }
        [HttpPost]
        public string CheckSessionForAjax()
        {
            return "OFF";
        }

        //private void SetCookie()
        //{
        //    if (ConfigurationManager.AppSettings["IsSSOLogin"].Equals("true"))
        //    {
        //        Session["CookieHasChanged"] = true;
        //        string schoolYear = (string.IsNullOrEmpty((Session[GlobalConstants.ACADEMICYEAR_ID] ?? "").ToString()) ? "-1" : Session[GlobalConstants.ACADEMICYEAR_ID].ToString());
        //        string level = (string.IsNullOrEmpty((Session[GlobalConstants.APPLIED_LEVEL] ?? "").ToString()) ? "-1" : Session[GlobalConstants.APPLIED_LEVEL].ToString());
        //        string postData = "schoolYear=" + schoolYear + "&level=" + level;
        //        HttpCookie SchoolYear = new HttpCookie("SchoolYear");
        //        SchoolYear.HttpOnly = true;
        //        SchoolYear.Value = schoolYear;
        //        SchoolYear.Expires = DateTime.Now.AddHours(10);
        //        HttpCookie SchoolLevel = new HttpCookie("SchoolLevel");
        //        SchoolLevel.HttpOnly = true;
        //        SchoolLevel.Value = level;
        //        SchoolLevel.Expires = DateTime.Now.AddHours(10);
        //        Response.Cookies.Add(SchoolYear);
        //        Response.Cookies.Add(SchoolLevel);
        //    }
        //}
        /// <summary>
        /// Thuc hien lay len SchoolYearID cua SMAS 2.0 tuong ung voi AcademicYearID cua SMAS 3.0
        /// Dung de su dung cho SSO
        /// </summary>
        private void AddOldAcademicYear()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("NewID", Session[GlobalConstants.ACADEMICYEAR_ID]);
            dic.Add("NewTableName", "ACADEMIC_YEAR");
            MIdmapping objMappingSchoolyear = new MIdmapping();
            objMappingSchoolyear = MIdmappingBusiness.Search(dic).FirstOrDefault();
            if (objMappingSchoolyear != null)
            {
                // Neu ton tai id mapping thi set lai session
                Session[GlobalConstants.OLD_ACADEMICYEAR_ID] = objMappingSchoolyear.OldID;
            }
            else
            {
                //Neu khong ton tai ID mapping thi set session ve gia tri ""
                Session[GlobalConstants.OLD_ACADEMICYEAR_ID] = "";
            }
        }
        [AllowAnonymous]
        public FileResult DownloadDocument()
        {
            string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + "/" + SMAS.Business.Common.SystemParamsInFile.FILE_DOWNLOAD_DOCUMENT;
            Stream excel = Config.DownloadDocumentFile(templatePath);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "PhienBan.pdf";
            result.FileDownloadName = ReportName;
            return result;
        }

        public PartialViewResult LoadAjaxCreateFirstYearData(int id)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["ProductVersion"] = _globalInfo.ProductVersionID.ToString();
            dic["AcademicYearId"] = _globalInfo.AcademicYearID;
            dic["Semester"] = _globalInfo.Semester;
            List<FirstDataFunctionBO> lstFunction = WorkFlowUserBusiness.GetFirstDataFunction(dic, id);
            ViewData["WorkFlowUser"] = lstFunction;

            return PartialView("_ViewResultData");

        }

        public PartialViewResult AjaxLoadCalendar(int id)
        {
            if (id == 1)//TKB Ngay
            {
                return PartialView("_CalendarDay");
            }
            else//TKB tuan
            {
                int semesterID = 0;
                DateTime dateTimeNow = DateTime.Now.Date;
                AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                if (dateTimeNow < objAca.FirstSemesterStartDate || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate) || objAca.SecondSemesterEndDate < dateTimeNow)
                {
                    semesterID = 2;
                }
                else
                {
                    semesterID = _globalInfo.Semester.Value;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["appliedLevel"] = _globalInfo.AppliedLevel;
                dic["Semester"] = semesterID;
                dic["EmployeeID"] = _globalInfo.EmployeeID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                DateTime datetimeNow = DateTime.Now;
                List<CalendarBO> iqCalendarBO = CalendarBusiness.SearchByPermissionTeacher(dic).ToList();
                if (iqCalendarBO.Count > 0)
                {
                    DateTime maxDate = iqCalendarBO.Where(c => c.StartDate.Value <= datetimeNow.Date).Max(c => c.StartDate.Value);
                    iqCalendarBO = iqCalendarBO.Where(p => p.StartDate == maxDate).ToList();
                }

                ViewData[GlobalConstants.LIST_CALENDAR] = iqCalendarBO;
                return PartialView("_CalendarWeek");
            }

        }

        public PartialViewResult AjaxLoadCalendarByDay(string ToDate)
        {
            CalendarBusiness.SetAutoDetectChangesEnabled(false);
            DateTime toDateTime = Convert.ToDateTime(ToDate);
            int academicYearId = _globalInfo.AcademicYearID.Value;
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearId);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["appliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = Business.Common.UtilsBusiness.GetSemesterbyDate(objAcademicYear, toDateTime);
            dic["EmployeeID"] = _globalInfo.EmployeeID;
            dic["AcademicYearID"] = academicYearId;
            DateTime datetimeNow = DateTime.Now;
            List<CalendarBO> iqCalendarBO = CalendarBusiness.SearchByPermissionTeacher(dic).ToList();
            List<CalendarBO> lstDayCalendar = new List<CalendarBO>();
            if (iqCalendarBO.Count > 0)
            {
                DateTime maxDate = iqCalendarBO.Where(c => c.StartDate.Value <= datetimeNow.Date).Max(c => c.StartDate.Value);
                iqCalendarBO = iqCalendarBO.Where(p => p.StartDate == maxDate).ToList();

                int dayOfWeek = Utils.Utils.GetDayOfWeek(toDateTime.DayOfWeek);
                lstDayCalendar = iqCalendarBO.Where(p => p.DayOfWeek == dayOfWeek).OrderBy(p => p.Section).ToList();
            }
            ViewData[GlobalConstants.LIST_CALENDAR] = lstDayCalendar;
            CalendarBusiness.SetAutoDetectChangesEnabled(true);
            return PartialView("_ViewCalendarDay");
        }

        public PartialViewResult AjaxLoadMailBoxOrMessageBox(int id)
        {
            IQueryable<AlertMessageBO> iqMailBox = new List<AlertMessageBO>().AsQueryable();
            IQueryable<AlertMessageBO> iqMessageBox = new List<AlertMessageBO>().AsQueryable();
            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> lstAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageID = lstAlertMessageDetail.Select(p => p.AlertMessageID).Distinct().ToList();
            int countMailBox = 0;
            int countMessageBox = 0;

            iqMailBox = this.GetNews();
            IDictionary<string, object> dicAlert = new Dictionary<string, object>();
            dicAlert["IsActive"] = true;
            dicAlert["isAdmin"] = true;

            iqMessageBox = (from p in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish == true)
                            select new AlertMessageBO
                            {
                                AlertMessageID = p.AlertMessageID,
                                Title = p.Title,
                                TypeID = p.TypeID,
                                SendName = "SMAS",
                                ContentMessage = p.ContentMessage,
                                PublishedDate = p.PublishedDate,
                                FileUrl = p.FileUrl,
                                CreatedUserID = p.CreatedUserID
                            }).OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID);

            countMailBox = iqMailBox.Count(p => !lstAlertMessageID.Contains(p.AlertMessageID));
            ViewData[GlobalConstants.NEWNEWSNO] = countMailBox;

            countMessageBox = iqMessageBox.Where(p => !lstAlertMessageID.Contains(p.AlertMessageID)).Count();
            ViewData[GlobalConstants.NEWMESSAGENO] = countMessageBox;



            int count = 0; //dem so dong danh dau la tin da doc

            List<AlertMessageBO> lstAMTake = new List<AlertMessageBO>();
            lstAMTake = id == 1
                ? iqMailBox.Take(GlobalConstants.PAGESIZE_NEWS).ToList()
                : iqMessageBox.Take(GlobalConstants.PAGESIZE_NEWS).ToList();

            foreach (var item in lstAMTake)
            {
                count = lstAlertMessageDetail.Count(s => s.AlertMessageID == item.AlertMessageID);
                if (count > 0) //la tin da doc
                {
                    item.isRead = true;
                }
                else //la tin chua doc
                {
                    item.isRead = false;
                }
                item.Title = Sanitizer.GetSafeHtmlFragment(item.Title);
                item.ContentMessage = Sanitizer.GetSafeHtmlFragment(item.ContentMessage);
            }
            ViewData[GlobalConstants.TAB_MESSAGE] = id;


            //xu li hien thi so tin moi
            ViewData[GlobalConstants.NEWS_TOTALITEM] = id == 1 ? iqMailBox.Count() : iqMessageBox.Count();
            ViewData[GlobalConstants.ALERT_MESSAGE] = lstAMTake;
            AlertMessageBO objAlert = lstAMTake.FirstOrDefault();
            if (objAlert != null)
            {
                ViewData[GlobalConstants.OBJ_MESSAGE] = objAlert;
                ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(objAlert.CreatedUserID);

            }

            return PartialView("_FrameViewMailBox");
        }

        public PartialViewResult AjaxLoadAutoAlertMessge(int id)
        {
            AlertMessageBO objAlertMessage = new AlertMessageBO();

            IDictionary<string, object> dicAlertDetail = new Dictionary<string, object>();
            dicAlertDetail["UserID"] = _globalInfo.UserAccountID;
            IQueryable<AlertMessageDetail> iqAlertMessageDetail = AlertMessageDetailBusiness.Search(dicAlertDetail);
            List<int> lstAlertMessageDetailID = iqAlertMessageDetail.Select(p => p.AlertMessageID).Distinct().ToList();
            List<AlertMessageBO> lstAlertMailBox = new List<AlertMessageBO>();
            List<AlertMessageBO> lstAlertMessage = new List<AlertMessageBO>();
            IDictionary<string, object> dicAlert = new Dictionary<string, object>();
            dicAlert["IsActive"] = true;
            dicAlert["isAdmin"] = true;
            //Chi hien thi popup thong bao moi cac tin duoc tao cach day 2 thang
            DateTime fromDate = DateTime.Now.AddMonths(-2);
            lstAlertMailBox = this.GetNews().Where(p => !lstAlertMessageDetailID.Contains(p.AlertMessageID)).Take(1).ToList();
            lstAlertMessage = (from p in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish == true)
                               select new AlertMessageBO
                               {
                                   AlertMessageID = p.AlertMessageID,
                                   Title = p.Title,
                                   TypeID = p.TypeID,
                                   SendName = "SMAS",
                                   ContentMessage = p.ContentMessage,
                                   PublishedDate = p.PublishedDate,
                                   FileUrl = p.FileUrl,
                                   UnitID = p.UnitID,
                                   CreatedUserID = p.CreatedUserID
                               }).ToList().OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID)
                                   .Where(p => !lstAlertMessageDetailID.Contains(p.AlertMessageID)).Take(1).ToList();
            objAlertMessage = lstAlertMailBox.Union(lstAlertMessage).Where(p => p.PublishedDate >= fromDate).OrderByDescending(a => a.PublishedDate).ThenByDescending(a => a.AlertMessageID).FirstOrDefault();
            objAlertMessage.Title = Sanitizer.GetSafeHtmlFragment(objAlertMessage.Title);
            objAlertMessage.ContentMessage = Sanitizer.GetSafeHtmlFragment(objAlertMessage.ContentMessage);
            ViewData[GlobalConstants.TAB_MESSAGE] = id;
            ViewData[GlobalConstants.OBJ_MESSAGE] = objAlertMessage;
            ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(objAlertMessage != null ? objAlertMessage.CreatedUserID : 0);
            return PartialView("_ViewAlertMessage");
        }

        public PartialViewResult ShowMessageBoxDetail(int MessageBoxID, int tabID)
        {
            AlertMessageBO objAlertMessage = new AlertMessageBO();
            if (tabID == 1)
            {
                objAlertMessage = this.GetNews().Where(p => p.AlertMessageID == MessageBoxID).FirstOrDefault();
            }
            else
            {
                IDictionary<string, object> dicAlert = new Dictionary<string, object>();
                dicAlert["IsActive"] = true;
                dicAlert["isAdmin"] = true;

                objAlertMessage = (from p in AlertMessageBusiness.Search(dicAlert).Where(o => o.IsPublish == true && o.AlertMessageID == MessageBoxID)
                                   select new AlertMessageBO
                                   {
                                       AlertMessageID = p.AlertMessageID,
                                       Title = p.Title,
                                       TypeID = p.TypeID,
                                       SendName = "SMAS",
                                       ContentMessage = p.ContentMessage,
                                       PublishedDate = p.PublishedDate,
                                       FileUrl = p.FileUrl,
                                       CreatedUserID = p.CreatedUserID
                                   }).FirstOrDefault();
            }

            objAlertMessage.Title = Sanitizer.GetSafeHtmlFragment(objAlertMessage.Title);
            objAlertMessage.ContentMessage = Sanitizer.GetSafeHtmlFragment(objAlertMessage.ContentMessage);

            ViewData[GlobalConstants.TAB_MESSAGE] = tabID;
            ViewData[GlobalConstants.OBJ_MESSAGE] = objAlertMessage;
            ViewData[GlobalConstants.NEWS_CREATE_USER] = GetUserNameByUserID(objAlertMessage.CreatedUserID);
            return PartialView("_ViewAlertMessage");
        }

        [HttpPost]
        public JsonResult InsertMessageViewDetail(int AlertMessageID)
        {
            AlertMessageDetail alertMessageDetail = new AlertMessageDetail();
            alertMessageDetail.AlertMessageID = AlertMessageID;
            alertMessageDetail.UserID = _globalInfo.UserAccountID;
            if (_globalInfo.IsAdmin)//admin
            {
                alertMessageDetail.ReceiverTypeID = 1;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)//so
            {
                alertMessageDetail.ReceiverTypeID = 2;
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)//phong
            {
                alertMessageDetail.ReceiverTypeID = 3;
            }
            else//truong
            {
                alertMessageDetail.ReceiverTypeID = 4;
            }
            alertMessageDetail.Status = GlobalConstants.ISREADMESSAGE;
            alertMessageDetail.CreatedDate = DateTime.Now;
            //alertMessageDetail.ModifiedDate = null;
            AlertMessageDetailBusiness.Insert(alertMessageDetail);
            AlertMessageDetailBusiness.Save();
            return Json(new JsonMessage("Insert thành công", "success"));
        }

        //Tamhm1
        //Update: 31/03/2012
        //Loại bỏ số 0, 84 ở đầu số điện thoại.
        private string ConvertPhone(string phone)
        {
            if (phone.StartsWith("0"))
            {
                phone = phone.Remove(0, 1);
            }
            else if (phone.StartsWith("84"))
            {
                phone = phone.Remove(0, 2);
            }
            return phone;
        }

        private string UpdateUser(string username)
        {
            string newPass = genRandom();
            try
            {
                MembershipUser user = null;
                user = Membership.GetUser(username, false);
                string oldPass = user.ResetPassword();
                user.UnlockUser();
                user.ChangePassword(oldPass, newPass);
                user.IsApproved = true;
                Membership.UpdateUser(user);
                //OAuth2.ChangePass(username, oldPass, newPass);

                return newPass;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdateUser", username, ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Ham sinh mat khau 
        /// </summary>
        /// <returns></returns>
        private string genRandom()
        {
            string ret = string.Empty;
            Random ran = new Random();
            int NoOfChar = ran.Next(8, 8);
            char[] ch = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z',
                          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z',
                          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            for (int i = 0; i < NoOfChar; i++)
            {
                if (i % 3 == 0)
                {
                    ret += ch[ran.Next(25, ch.Length - 1)].ToString();
                }
                else if (i % 2 == 0)
                {
                    ret += ch[ran.Next(0, 24)].ToString();
                }
                else
                {
                    ret += ch[ran.Next(0, ch.Length - 1)].ToString();
                }
            }
            return ret;
        }

        /// <summary>
        /// Kiểm tra user và số điện thoại để reset pw
        /// </summary>
        /// <param name="username"></param>
        /// <param name="phone"></param>
        /// <returns> 4: không tồn tại; 1: ko có số đt; 3: số ko hợp lệ; 2: Hợp lệ</returns>
        public int Check(string username, string phone)
        {
            int value = 1;
            string lowerUsername = username.ToLower();
            using (SMASEntities context = new SMASEntities())
            {
                var useraccount = (from us in context.aspnet_Users
                                   join ua in context.UserAccount on us.UserId equals ua.GUID
                                   where us.UserName == username || us.LoweredUserName == lowerUsername
                                   select ua).FirstOrDefault();

                if (useraccount == null) return 4;

                Employee em = null;
                if (useraccount.EmployeeID.HasValue)
                {
                    em = (from e in context.Employee
                          where e.EmployeeID == useraccount.EmployeeID
                          select e
                          ).FirstOrDefault();
                }


                if (em != null)
                {
                    string telephoneValue = ConvertPhone(em.Telephone ?? "");
                    string mobileValue = ConvertPhone(em.Mobile ?? "");
                    if (string.IsNullOrEmpty(telephoneValue) && string.IsNullOrEmpty(mobileValue))
                    {
                        value = 1;
                    }
                    else if (telephoneValue == ConvertPhone(phone) || mobileValue == ConvertPhone(phone))
                    {
                        value = 2;
                    }
                    else value = 3;

                }
                else
                {
                    SchoolProfile school = (from s in context.SchoolProfile
                                            where s.AdminID == useraccount.UserAccountID
                                            select s).FirstOrDefault();
                    if (school != null)
                    {
                        string phoneValue = ConvertPhone(school.Telephone ?? "");
                        string phoneValueOfHeadMaster = ConvertPhone(school.HeadMasterPhone ?? "");
                        if (String.IsNullOrEmpty(phoneValue) && String.IsNullOrEmpty(phoneValueOfHeadMaster))
                        {
                            value = 1;
                        }
                        else if (phoneValue == ConvertPhone(phone) || phoneValueOfHeadMaster == ConvertPhone(phone))
                        {
                            value = 2;
                        }
                        else value = 3;
                    }
                }
            }
            return value;
        }

        private void SetSession<T>(string key, T value)
        {
            Session[key] = value;
        }

        private string GetSession(string sessionName)
        {
            return (Session[sessionName] ?? "").ToString();
        }

        private void SetSession(string sessionName, object value)
        {
            Session[sessionName] = value;
        }

        private void ClearCookieSession()
        {
            Session.Clear();
            Session.RemoveAll();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(new HttpCookie("SchoolYear", ""));
            Response.Cookies["SchoolYear"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(new HttpCookie("SchoolLevel", ""));
            Response.Cookies["SchoolLevel"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Clear();
        }
    }
}
