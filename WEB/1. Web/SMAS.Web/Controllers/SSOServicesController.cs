﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Constants;
using System.Configuration;
using Core.SAML.Library;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.SAML.Library.Schema;
using SMAS.Web.Utils;
using SMAS.Web.SSO;
using Microsoft.Security.Application;

namespace SMAS.Web.Controllers
{
    public class SSOServicesController : Controller
    {
        //
        // GET: /SSOServices/
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty((Session["SSOLogin"] ?? "").ToString()))
            {
                string iddentityProvider = ConfigurationManager.AppSettings["identityProvider"].ToString();
                string returnUserUrl = Sanitizer.GetSafeHtmlFragment(System.Web.HttpContext.Current.Request.Url.ToString());
                if (iddentityProvider.Contains("https") && !returnUserUrl.Contains("https"))
                {
                    returnUserUrl = returnUserUrl.Replace("http", "https");
                }
                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.Append("<head>");
                sb.Append("<script src='/Scripts/2012.1.214/jquery-1.7.1.min.js'></script><script src='/Scripts/md5.js'></script><script src='/Scripts/jquery.browser-fingerprint-1.1.js'></script><script>$(document).ready(function () {$('#TokenAuthen').val($.fingerprint());$('#RedirectCheckLoginStatusSSO').submit()});</script>");
                sb.Append("</head>");
                sb.Append(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form name='form' action='{0}' method='post'>", "/SSOServices/RedirectCheckLoginStatusSSO");
                sb.Append("<input id='TokenAuthen' type='hidden' name='TokenAuthen' />");
                sb.AppendFormat("<input type='hidden' name='returnUserUrl' value='{0}'>", returnUserUrl);
                sb.Append("</form>");
                sb.Append("</body>");
                sb.Append("</html>");
                System.Web.HttpContext.Current.Response.Write(sb.ToString());
                System.Web.HttpContext.Current.Response.End();
                return null;
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult RedirectCheckLoginStatusSSO()
        {
            if (string.IsNullOrEmpty((Session["SSOLogin"] ?? "").ToString()))
            {
                string iddentityProvider = ConfigurationManager.AppSettings["identityProvider"].ToString();
                // For Security
                string guid = Guid.NewGuid().ToString();
                if (string.IsNullOrEmpty((Session["SSOGuid"] ?? "").ToString()))
                {
                    Session["SSOGuid"] = guid;
                }
                string assertionConsumerServiceUrl = ConfigurationManager.AppSettings["assertionConsumerServiceUrl"].ToString();
                string serviceProviderIdentity = ConfigurationManager.AppSettings["serviceProviderIdentity"].ToString();
                if (!string.IsNullOrEmpty((Session["SSOGuid"] ?? "").ToString()))
                {
                    serviceProviderIdentity = ConfigurationManager.AppSettings["serviceProviderIdentity"].ToString() + "!@#$%^&*" + Session["SSOGuid"].ToString();
                }
                string serviceProviderName = ConfigurationManager.AppSettings["serviceProviderName"].ToString();
                string certificateLocation = (ConfigurationManager.AppSettings["certificateLocation"] ?? "").ToString();
                string returnUserUrl = Sanitizer.GetSafeHtmlFragment(Request.Url.ToString());
                if (iddentityProvider.Contains("https") && !returnUserUrl.Contains("https"))
                {
                    returnUserUrl = returnUserUrl.Replace("http", "https");

                }
                GlobalInfo.TokenAuthen = System.Web.HttpContext.Current.Request["TokenAuthen"];
                X509Certificate2 signingCert = CertificateUtility.GetCertificateForSigning(@certificateLocation, GlobalConstants.CERTIFICATEPASS);
                string SAMLRequest = SAML20Assertion.CreateSAML20AuthRequest(serviceProviderIdentity, "Recipient", assertionConsumerServiceUrl, serviceProviderName, "", "", signingCert);
                Response.Clear();
                StringBuilder sb = new StringBuilder();
                sb.Append("<html>");
                sb.Append(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form name='form' action='{0}' method='post'>", iddentityProvider);
                sb.AppendFormat("<input type='hidden' name='SAMLRequest' value='{0}'>", Server.UrlEncode(SAMLRequest));
                sb.AppendFormat("<input type='hidden' name='returnUserUrl' value='{0}'>", returnUserUrl);
                sb.AppendFormat("<input type='hidden' name='returnMainUrl' value='{0}'>", returnUserUrl);
                sb.AppendFormat("<input type='hidden' name='isSmas3' value='{0}'>", true);
                sb.Append("</form>");
                sb.Append("</body>");
                sb.Append("</html>");
                Response.Write(sb.ToString());
                Response.End();
                return null;
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult CheckLoginStatus(string SAMLResponse, string returnUserUrl)
        {
            string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
            if (string.IsNullOrEmpty(SAMLResponse))
            {
                return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
            }

            XmlDocument SAMLXML = new XmlDocument();
            SAMLXML.LoadXml(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Server.UrlDecode(SAMLResponse))));
            string certificateLocation = (ConfigurationManager.AppSettings["certificateLocation"] ?? "").ToString();
            X509Certificate2 signingCert = CertificateUtility.GetCertificateForSigning(@certificateLocation, GlobalConstants.CERTIFICATEPASS);
            if (!CertificateUtility.ValidateX509CertificateSignature(SAMLXML, signingCert))
            {
                return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
            }

            XmlReader xmlReader = new XmlNodeReader(SAMLXML);
            xmlReader.Settings.ProhibitDtd = true;
            XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));
            ResponseType reponseType = (ResponseType)serializer.Deserialize(xmlReader);
            string issuer = reponseType.Issuer.Value;
            //string statusMessage = reponseType.Status.StatusMessage;
            string iddentityProvider = (ConfigurationManager.AppSettings["serviceProviderIdentity"] ?? "").ToString();
            AssertionType assertionType = Helper.GetAssertionFromXMLDoc(SAMLXML);
            ResponseAttribute responseAttribute = Helper.AssertionData(assertionType);
            if (string.IsNullOrEmpty(iddentityProvider) || string.IsNullOrEmpty(responseAttribute.UserName))
            {
                //return RedirectPermanent("/Home/LogOff");
                return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
            }
            //string userName = responseAttribute.UserName;
            //string userId = responseAttribute.UserID;
            //string isSupperUser = responseAttribute.IsSupperUser;
            int? schoolYear = Convert.ToInt32(responseAttribute.SchoolYear);
            int? schoolLevel = Convert.ToInt32(responseAttribute.SchoolLevel);
            HttpCookie SchoolYear = new HttpCookie("SchoolYear");
            SchoolYear.HttpOnly = true;
            SchoolYear.Value = schoolYear.ToString();
            SchoolYear.Expires = DateTime.Now.AddHours(10);
            HttpCookie SchoolLevel = new HttpCookie("SchoolLevel");
            SchoolLevel.HttpOnly = true;
            SchoolLevel.Value = schoolLevel.ToString();
            SchoolLevel.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(SchoolYear);
            Response.Cookies.Add(SchoolLevel);
            Nullable<int> schoolID = null;
            if (!string.IsNullOrEmpty(responseAttribute.SchoolID))
            {
                schoolID = Convert.ToInt32(responseAttribute.SchoolID);
            }
            //string site = responseAttribute.Site;
            Session["Issuer"] = issuer;
            Session["SSOLogin"] = "true";
            if (!string.IsNullOrEmpty(returnUserUrl))
            {
                Response.Redirect(Server.UrlDecode(returnUserUrl), true);
                return null;
            }
            else
            {
                return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/Index"));
            }


        }
        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult ClearSession(string SAMLResponse, string returnMainUrl)
        {
            string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
            if (string.IsNullOrEmpty(SAMLResponse))
            {
                return null;
            }

            XmlDocument SAMLXML = new XmlDocument();
            SAMLXML.LoadXml(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Server.UrlDecode(SAMLResponse))));
            XmlReader xmlReader = new XmlNodeReader(SAMLXML);
            xmlReader.Settings.ProhibitDtd = true;
            string certificateLocation = (ConfigurationManager.AppSettings["certificateLocation"] ?? "").ToString();
            X509Certificate2 signingCert = CertificateUtility.GetCertificateForSigning(@certificateLocation, GlobalConstants.CERTIFICATEPASS);
            if (!CertificateUtility.ValidateX509CertificateSignature(SAMLXML, signingCert))
            {
                //return RedirectPermanent("/Home/LogOff");
                return RedirectPermanent(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Home/LogOff"));
            }
            else
            {
                if (Session[GlobalConstants.USERACCOUNT] != null)
                {
                    SSOLoginService ssoServ = new SSOLoginService();
                    ssoServ.AuthenticateClearSession();
                }
                string mainUrl = Server.UrlDecode(returnMainUrl);
                if (!string.IsNullOrEmpty(mainUrl))
                {
                    Response.Clear();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<html>");
                    sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                    sb.AppendFormat("<form name='form' action='{0}' method='post'>", mainUrl);
                    sb.Append("</form>");
                    sb.Append("</body>");
                    sb.Append("</html>");
                    Response.Write(sb.ToString());
                    Response.End();
                }
            }
            return null;

        }

        [AllowAnonymous]
        [SkipCheckRole]
        public JsonResult UpdateSessionInfo(string schoolYear, string level)
        {
            GlobalInfo global = new GlobalInfo();
            global.AcademicYearID = Convert.ToInt32(string.IsNullOrEmpty(schoolYear) ? "-1" : schoolYear);
            global.AppliedLevel = Convert.ToInt32(string.IsNullOrEmpty(level) ? "-1" : level);
            HttpCookie SchoolYear = new HttpCookie("SchoolYear");
            SchoolYear.HttpOnly = true;
            SchoolYear.Value = schoolYear;
            SchoolYear.Expires = DateTime.Now.AddHours(10);
            HttpCookie SchoolLevel = new HttpCookie("SchoolLevel");
            SchoolLevel.HttpOnly = true;
            /*if (HttpContext.Request.IsSecureConnection)
            {
                SchoolLevel.Secure = true;
            }*/
            SchoolLevel.Value = level;
            SchoolLevel.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(SchoolYear);
            Response.Cookies.Add(SchoolLevel);
            return null;

        }
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult Loading(string URL)
        {
            ViewData["LoadingURL"] = URL != null ? URL.Replace("%2", "/") : "";
            return Index();
        }
    }
}
