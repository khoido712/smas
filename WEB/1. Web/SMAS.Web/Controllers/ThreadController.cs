﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using SMAS.Web.Utils;
using System.Diagnostics;

namespace SMAS.Web.Controllers
{
    /// <summary>
    /// Khoi tao tien trinh thuc hien cac action chay mat nhieu thoi gian
    /// </summary>
    /// <author>AuNH,NamTa</author>
    /// <date>12/14/2012</date>
    public class ThreadController : BaseController
    {
        public delegate void LongAction(IDictionary<string, object> parameters);

        private LongAction longAction = null;

        private void ThreadAction(object actionParams)
        {
            if (longAction == null)
            {
                return;
            }

            longAction((IDictionary<string, object>)actionParams);
        }

        /// <summary>
        /// Khoi tao tien trinh de thuc hien mot action ton nhieu thoi gian
        /// </summary>
        /// <param name="threadName"></param>
        /// <param name="action"></param>
        /// <param name="actionParams"></param>
        public void StartThreadForLongAction(string threadName, LongAction action, IDictionary<string, object> actionParams)
        {
            longAction = action;
            Thread thread = new Thread(new ParameterizedThreadStart(ThreadAction));
            thread.Name = threadName;
            ThreadManager.getInstance().AddThread(thread);
            thread.Start(actionParams);
        }

        //xoa thread theo ten
        public void DeleteThread(string ThreadName)
        {
            //int status = 0;
            List<string> list = new List<string>();
            List<Thread> listThread = ThreadManager.getInstance().GetAllThread();
            if (listThread != null && listThread.Count > 0)
            {
                foreach (Thread t in listThread)
                {

                    if (t.Name.Equals(ThreadName))
                    {
                        listThread.Remove(t);
                        if (listThread.Count == 0)
                        {
                            break;
                        }
                    }
                }
            }
        }

        //kiem tra trang thai cua thread Namta
        public int StatusThread(string ThreadName)
        {
            //int status = 0;
            List<string> list = new List<string>();
            List<Thread> listThread = ThreadManager.getInstance().GetAllThread();
            foreach (Thread t in listThread)
            {
                list.Add(t.Name);
            }
            if (list.Contains(ThreadName))
            {
                return Web.Constants.GlobalConstants.Status_Thread_Has;
            }
            else
                return Web.Constants.GlobalConstants.Status_Thread_NoHas;
        }

    }
}
