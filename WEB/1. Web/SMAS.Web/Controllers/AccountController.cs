﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using SMAS.Web.Models;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Constants;
using SMAS.Web.Filter;
namespace SMAS.Web.Controllers
{

    public class AccountController : Controller
    {
        private readonly IUserAccountBusiness userBusiness;
        private readonly Iaspnet_MembershipBusiness membershipBusiness;
        private readonly IRoleBusiness roleBusiness;
        private readonly IGroupCatBusiness groupCatBusiness;
        private readonly IMenuBusiness menuBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private readonly IUserGroupBusiness userGroupBusiness;

        public AccountController(IUserAccountBusiness userBusinessPara,
        Iaspnet_MembershipBusiness membershipPara,
        IRoleBusiness roleBusinessPara,
        IGroupCatBusiness groupCatBusinessPara,
        IMenuBusiness menuBusinessPara,
        Iaspnet_UsersBusiness aspnet_UsersPara,
        IUserGroupBusiness userGroupPara)
        {
            this.userBusiness = userBusinessPara;
            this.membershipBusiness = membershipPara;
            this.roleBusiness = roleBusinessPara;
            this.groupCatBusiness = groupCatBusinessPara;
            this.menuBusiness = menuBusinessPara;
            this.aspnet_UsersBusiness = aspnet_UsersPara;
            this.userGroupBusiness = userGroupPara;
        }
        //
        // GET: /Account/LogOn
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {

                //}
                //if (Membership.ValidateUser(model.UserName, model.Password))
                //{
                Guid userId = (Guid)Membership.GetUser(model.UserName).ProviderUserKey;
                aspnet_Users aspnetUser = aspnet_UsersBusiness.getUsersById(userId);
                UserAccount userAccount = aspnetUser.UserAccounts.First();
                var memberUser = Membership.GetUser(model.UserName);
                if (userAccount.IsActive == false)
                {
                    // Thong bao loi o day
                    return new HttpUnauthorizedResult(); // neu account khong active thi thong bao loi
                }
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                if (userAccount != null)
                {
                    ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT] = userAccount;
                    ControllerContext.HttpContext.Session[GlobalConstants.USERACCID] = userAccount.UserAccountID;



                    if (userAccount.IsAdmin == false)
                    {
                        //Khong la Admin -> Chon Group
                        //List<UserGroup> userGroups = userGroupBusiness.All.Where(o => o.UserID == userAccount.UserAccountID).ToList();
                        List<UserGroup> userGroups = userAccount.UserGroups.ToList();
                        if (userGroups.Any())
                        {
                            //trong UserGroup co ton tai ban ghi co UserID khong?
                            if (userGroups.Count > 1)
                            {
                                List<GroupCat> groups = new List<GroupCat>();
                                foreach (UserGroup ug in userGroups)
                                {
                                    groups.Add(ug.GroupCat);
                                    //groups.Add(groupCatBusiness.All.Where(g => g.GroupCatID == ug.GroupID).First());
                                }
                                ControllerContext.HttpContext.Session[GlobalConstants.GROUPS] = groups;
                                return RedirectToAction("AuthorizeUser", "Account");
                            }
                            else
                            {
                                SetGroupAndMenu(userGroups.First().GroupCat, ControllerContext.HttpContext);
                                RedirectToAction("Index", "Home");
                            }
                        }
                    }
                    else
                    {
                        //Neu la Admin -> Chon Role
                        List<UserRole> userRoles = userAccount.UserRoles.ToList();
                        if (userRoles.Any())
                        {
                            if (userRoles.Count > 1)
                            {
                                List<Role> roles = new List<Role>();
                                foreach (UserRole ur in userRoles)
                                {
                                    roles.Add(ur.Role);
                                }
                                ControllerContext.HttpContext.Session[GlobalConstants.ROLES] = roles;
                                return RedirectToAction("AuthorizeAdmin", "Account");
                            }
                            else
                            {
                                SetRoleAndMenu(userRoles.First().Role, ControllerContext.HttpContext);
                                RedirectToAction("Index", "Home");
                            }
                        }
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }


            // If we got this far, something failed, redisplay form
            return View();
        }

        [HttpGet]
        [SkipCheckRole]
        public ActionResult AuthorizeAdmin()
        {
            List<Role> roles = ControllerContext.HttpContext.Session[GlobalConstants.ROLES] as List<Role>;
            if (roles != null) ViewBag.roles = new SelectList(roles.ToList(), "RoleID", "RoleName");
            return View();
        }

        //
        [HttpPost]
        [SkipCheckRole]
        public ActionResult AuthorizeAdmin(FormCollection formCollection)
        {
            UserAccount loggedUser = (UserAccount)ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT];
            string roleIDString = formCollection["roles"];
            int roleID = Convert.ToInt32(roleIDString);
            List<Role> roles = ControllerContext.HttpContext.Session[GlobalConstants.ROLES] as List<Role>;
            Role role = roles.Where(r => r.RoleID == roleID).First();
            SetRoleAndMenu(role, ControllerContext.HttpContext);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [SkipCheckRole]
        public ActionResult AuthorizeUser()
        {
            List<GroupCat> groups = ControllerContext.HttpContext.Session[GlobalConstants.GROUPS] as List<GroupCat>;
            if (groups != null) ViewBag.groups = new SelectList(groups.ToList(), "GroupID", "GroupName");
            return View();
        }

        //
        [HttpPost]
        [SkipCheckRole]
        public ActionResult AuthorizeUser(FormCollection formCollection)
        {
            UserAccount loggedUser = (UserAccount)ControllerContext.HttpContext.Session[GlobalConstants.USERACCOUNT];
            string groupIDString = formCollection["groups"];
            int groupID = Convert.ToInt32(groupIDString);
            List<GroupCat> groups = ControllerContext.HttpContext.Session[GlobalConstants.GROUPS] as List<GroupCat>;
            GroupCat group = groups.Where(g => g.GroupCatID == groupID).First();
            SetGroupAndMenu(group, ControllerContext.HttpContext);
            return RedirectToAction("Index", "Home");
        }

        private void SetGroupAndMenu(GroupCat group, HttpContextBase ctx)
        {
            ctx.Session[GlobalConstants.SELECTED_GROUP] = group;
            if (group.GroupMenus.Any())
            {
                List<Menu> menus = new List<Menu>();
                foreach (GroupMenu gm in group.GroupMenus.ToList())
                {
                    menus.Add(gm.Menu);
                }
                ctx.Session[GlobalConstants.MENUS] = menus;
            }
        }
        private void SetRoleAndMenu(Role role, HttpContextBase ctx)
        {
            ctx.Session[GlobalConstants.SELECTED_ROLE] = role;
            if (role.RoleMenus.Any())
            {
                List<Menu> menus = new List<Menu>();
                foreach (RoleMenu rm in role.RoleMenus.ToList())
                {
                    menus.Add(rm.Menu);
                }
                ctx.Session[GlobalConstants.MENUS] = menus;
            }
        }
        //
        //
        // GET: /Account/LogOff
        [SkipCheckRole]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            //// add to fix Session Fixation Error
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Cookies.Clear();

            // AnhVD9 - 20151125 - Fix bug ATTT - Lỗi không thay đổi giá trị Token sau mỗi phiên
            HttpCookie sessionId = new HttpCookie("ASP.NET_SessionId", "");
            sessionId.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(sessionId);

            HttpCookie token_Lw = new HttpCookie("__RequestVerificationToken_Lw__", "");
            token_Lw.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(token_Lw);
            // End - Fix bug ATTT - Lỗi không thay đổi giá trị Token sau mỗi phiên
            ////
            return RedirectToAction("Index", "Home");
        }


        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
