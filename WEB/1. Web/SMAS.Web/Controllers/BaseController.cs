﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using log4net;
using SMAS.Web.Constants;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using System.Web.Security;
using System.Text;
using SMAS.Business.Common;
//using Com.Viettel.Framework.Utils;

namespace SMAS.Web.Controllers
{
    //[LogFilter]
    //[SqlAuthorization]
    [HandleError]
    public class BaseController : Controller
    {
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IMTBusiness MTBusiness;
        private readonly IUserGroupBusiness UserGroupBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IMenuBusiness MenuBusiness;
        private readonly IRoleMenuBusiness RoleMenuBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly ILockInputSupervisingDeptBusiness LockInputSupervisingDeptBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        protected string smasUrl = UtilsBusiness.UrlSmas;

        protected ILog logger;

        /// <summary>
        /// Store global information.
        /// </summary>
        public GlobalInfo _globalInfo = null;
        /// <summary>
        /// Contructor
        /// </summary>
        public BaseController()
            : base()
        {
            _globalInfo = GlobalInfo.getInstance();
            logger = log4net.LogManager.GetLogger("Base");
            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger);
            this.EmployeeBusiness = new EmployeeBusiness(logger);
            this.PupilProfileBusiness = new PupilProfileBusiness(logger);
            this.UserAccountBusiness = new UserAccountBusiness(logger);
            this.MTBusiness = new MTBusiness(logger);
            this.UserGroupBusiness = new UserGroupBusiness(logger);
            this.GroupMenuBusiness = new GroupMenuBusiness(logger);
            this.SupervisingDeptBusiness = new SupervisingDeptBusiness(logger);
            this.ClassProfileBusiness = new ClassProfileBusiness(logger);
            this.MenuBusiness = new MenuBusiness(logger);
            this.DOETExaminationsBusiness = new DOETExaminationsBusiness(logger);
            this.AcademicYearOfProvinceBusiness = new AcademicYearOfProvinceBusiness(logger);
            this.RoleMenuBusiness = new RoleMenuBusiness(logger);
            this.LockInputSupervisingDeptBusiness = new LockInputSupervisingDeptBusiness(logger);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger);
        }

        #region private method
        /// <summary>
        /// Set log data
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>10/05/2013</date>
        /// <param name="dic"></param>
        public void SetViewDataActionAudit(IDictionary<string, object> dic)
        {
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON];
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON];
            ViewData[CommonKey.AuditActionKey.ObjectID] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID];
            ViewData[CommonKey.AuditActionKey.Description] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION];
            ViewData[CommonKey.AuditActionKey.Parameter] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER];
            ViewData[CommonKey.AuditActionKey.userAction] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION];
            ViewData[CommonKey.AuditActionKey.userFunction] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION];
            ViewData[CommonKey.AuditActionKey.userDescription] = dic[SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION];
        }

        /// <summary>
        /// Thiết lập các tham số ghi log
        /// Set paramters to write log
        /// </summary>
        /// <param name="oldJson"></param>
        /// <param name="newJson"></param>
        /// <param name="objectID"></param>
        /// <param name="description"></param>
        /// <param name="parameter"></param>
        /// <param name="userFunction"></param>
        /// <param name="userAction"></param>
        /// <param name="userDescription"></param>
        /// Muốn lưu log chi tiết để hiển thị được trên chức năng lịch sử sử dụng hệ thống thì phải thiết lập description
        /// If you wanted to write log detail to present on "History using" function, you would set value for descrition parameter
        public void SetViewDataActionAudit(string oldJson, string newJson, string objectID, string description, string parameter = "", string userFunction = "", string userAction = "", string userDescription = "")
        {
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = oldJson;
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = newJson;
            ViewData[CommonKey.AuditActionKey.ObjectID] = objectID;
            ViewData[CommonKey.AuditActionKey.Description] = description;
            ViewData[CommonKey.AuditActionKey.Parameter] = parameter;
            ViewData[CommonKey.AuditActionKey.userAction] = userAction;
            ViewData[CommonKey.AuditActionKey.userFunction] = userFunction;
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescription;
        }

        /// <summary>
        /// fast convert string to int
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>09/04/2013</date>
        public int IntParseFast(string value)
        {
            int result = 0;
            for (int i = 0; i < value.Length; i++)
            {
                result = 10 * result + (value[i] - 48);
            }
            return result;
        }

        /// <summary>
        /// return employee image 
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>10/05/2013</date>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult GetEmployeeImage(int? id, bool isAdmin = false)
        {
            if (isAdmin)
            {
                if (_globalInfo.SchoolID.HasValue)
                {
                    byte[] schoolImage = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value).Image;

                    if (schoolImage == null)
                    {
                        return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                    }

                    return new FileStreamResult(new System.IO.MemoryStream(schoolImage), "image/jpeg");
                }
                else
                {
                    return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                }
            }

            if (id.HasValue)
            {
                byte[] image = EmployeeBusiness.Find(id).Image;
                if (image == null)
                {
                    return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                }
                return new FileStreamResult(new System.IO.MemoryStream(image), "image/jpeg");
            }
            else
            {
                return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
            }
        }

        /// <summary>
        /// return pupil image 
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>10/05/2013</date>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult GetPupilImage(int? id, bool isAdmin = false)
        {
            if (isAdmin)
            {
                if (_globalInfo.SchoolID.HasValue)
                {
                    var schoolProfile = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                    if (schoolProfile == null)
                    {
                        return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                    }
                    byte[] schoolImage = schoolProfile.Image;

                    if (schoolImage == null)
                    {
                        return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                    }

                    return new FileStreamResult(new System.IO.MemoryStream(schoolImage), "image/jpeg");
                }
                else
                {
                    return File("~/Content/images/teacher_avatar.jpg", "image/jpeg");
                }
            }

            if (id.HasValue)
            {
                var pupilProfile = PupilProfileBusiness.Find(id);
                if (pupilProfile == null)
                {
                    return File("~/Content/images/pupil_avatar.jpg", "image/jpeg");
                }
                byte[] image = pupilProfile.Image;
                if (image == null)
                {
                    return File("~/Content/images/pupil_avatar.jpg", "image/jpeg");
                }
                return new FileStreamResult(new System.IO.MemoryStream(image), "image/jpeg");
            }
            else
            {
                return File("~/Content/images/pupil_avatar.jpg", "image/jpeg");
            }
        }

        #endregion


        private bool CheckButtonPermission()
        {
            return IsCurrentYear() &&
                GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) >= SystemParamsInFile.PER_CREATE;
        }

        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }

        /// <summary>NAMTA check ATTT
        /// Kiem tra quyen cua hanh dong
        /// </summary>
        /// <param name="id">ma cua ban ghi</param>
        /// <param name="tableName">ten bang</param>
        /// <param name="typeObject">0:lay SchoolID hoac SPID;1:lay SPID;2:lay UserAccountID 3: cho th pupuilprofile</param>
        /// <param name="dicOther">neu co dieu kien khac</param>
        /// <param name="managerIdName">ten cua cot quan ly</param>
        /// <param name="idname">ten cot id</param>
        /// <param name="schemaName"></param>
        public void CheckPermissionForAction(int? id, string tableName, int typeObject = 0, Dictionary<string, object> dicOther = null, string managerIdName = "", string idname = "", string schemaName = "")
        {
            if (id != null)
            {
                IGenericRepository<District> Genric = new GenericRepository<District>();
                GlobalInfo glo = new GlobalInfo();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                Dictionary<string, object> dicOr = new Dictionary<string, object>();
                tableName = SMASUtils.ConvertToOracle(tableName);
                bool checkError = true;
                if (idname == "")
                {
                    idname = tableName + "_ID";
                }
                else
                {
                    idname = SMASUtils.ConvertToOracle(idname);
                }
                dic.Add(idname, id);
                if (managerIdName != "")
                {
                    managerIdName = SMASUtils.ConvertToOracle(managerIdName);
                }

                if (typeObject == 0 && dic != null)
                {
                    //tu nhan doi tuong truyen vao
                    if (glo.IsSchoolRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SchoolID");
                        }
                        if (glo.IsAdminSchoolRole)
                        {
                            dic.Add(managerIdName, glo.SchoolID.Value);
                        }
                        else
                        {
                            Employee Teacher = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Teacher != null)
                            {
                                dic.Add(managerIdName, Teacher.SchoolID);
                            }
                        }
                    }
                    else if ((glo.IsSubSuperVisingDeptRole || glo.IsSuperVisingDeptRole) && !glo.SchoolID.HasValue)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SupervisingDeptID");
                        }
                        //truong hop dac biet cho nhan vien phong so
                        if (tableName.Equals("EMPLOYEE"))
                        {
                            Employee Employee = EmployeeBusiness.Find(id);
                            if (Employee != null)
                            {
                                string tr = Employee.SupervisingDept.TraversalPath + Employee.SupervisingDeptID + "\\";
                                string tsLogin = SupervisingDeptBusiness.Find(glo.SupervisingDeptID).TraversalPath + glo.SupervisingDeptID + "\\";
                                if (tr.Contains(tsLogin))
                                {
                                    dic.Add(managerIdName, Employee.SupervisingDeptID.Value);
                                }
                                else
                                {
                                    dic.Add(managerIdName, glo.SupervisingDeptID);
                                }
                            }
                        }
                        else if (glo.EmployeeID != null)
                        {
                            dic.Add(managerIdName, glo.SupervisingDeptID.Value);

                        }
                        else
                        {
                            Employee Employee = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Employee != null)
                            {
                                dic.Add(managerIdName, Employee.SupervisingDeptID);
                            }
                        }
                    }
                    else if (glo.IsSystemAdmin)
                    {
                        dic = null;
                    }

                }
                else if (typeObject == 1)
                {//SupervisingDeptID
                    if (glo.IsSubSuperVisingDeptRole || glo.IsSuperVisingDeptRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SupervisingDeptID");
                        }
                        if (glo.EmployeeID != null)
                        {
                            dic.Add(managerIdName, glo.SupervisingDeptID.Value);
                        }
                        else
                        {
                            Employee Employee = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Employee != null)
                            {
                                dic.Add(managerIdName, Employee.SchoolProfile.AdminID);
                            }
                        }
                    }
                }
                else if (typeObject == 2)
                {
                    //tu nhan doi tuong truyen vao
                    if (glo.IsSchoolRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SchoolID");
                        }
                        if (glo.IsAdminSchoolRole)
                        {
                            dic.Add(managerIdName, glo.UserAccountID);
                        }
                        else
                        {
                            Employee Teacher = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Teacher != null)
                            {
                                dic.Add(managerIdName, Teacher.SchoolProfile.AdminID);
                            }
                        }
                    }
                    else if (glo.IsSubSuperVisingDeptRole || glo.IsSuperVisingDeptRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SupervisingDeptID");
                        }
                        if (glo.EmployeeID == null)
                        {
                            dic.Add(managerIdName, glo.UserAccountID);
                        }
                        else
                        {
                            Employee Employee = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Employee != null)
                            {
                                dic.Add(managerIdName, Employee.SupervisingDept.AdminID);
                            }
                        }
                    }

                }
                else if (typeObject == 3)
                {
                    //th dac biet kiem tra truong voi doi tuong hoc sinh
                    //tim kiem xem hoc sinh
                    managerIdName = SMASUtils.ConvertToOracle("SchoolID");
                    dic = new Dictionary<string, object>();
                    dic.Add(managerIdName, glo.SchoolID);
                    dic.Add("PUPIL_ID", id);
                    tableName = SMASUtils.ConvertToOracle("PupilOfClass");

                }
                else if (typeObject == 4)
                {
                    //lay tai traverserpack cua tai khoan de kiem tra
                    //dau tien chay giong luong 2
                    string TraverSerPath = "";
                    if (glo.IsSchoolRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SchoolID");
                        }
                        if (glo.IsAdminSchoolRole)
                        {
                            TraverSerPath = SchoolProfileBusiness.Find(glo.SchoolID).SupervisingDept.TraversalPath + SchoolProfileBusiness.Find(glo.SchoolID).SupervisingDept.SupervisingDeptID;
                        }
                        else
                        {
                            Employee Teacher = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Teacher != null)
                            {
                                TraverSerPath = Teacher.SchoolProfile.SupervisingDept.TraversalPath + Teacher.SchoolProfile.SupervisingDept.SupervisingDeptID;
                            }
                        }
                    }
                    else if (glo.IsSubSuperVisingDeptRole || glo.IsSuperVisingDeptRole)
                    {
                        if (managerIdName == "")
                        {
                            managerIdName = SMASUtils.ConvertToOracle("SupervisingDeptID");
                        }
                        if (glo.EmployeeID == null)
                        {
                            TraverSerPath = SupervisingDeptBusiness.Find(glo.SupervisingDeptID).TraversalPath + SupervisingDeptBusiness.Find(glo.SupervisingDeptID).SupervisingDeptID;
                        }
                        else
                        {
                            Employee Employee = EmployeeBusiness.Find(glo.EmployeeID);
                            if (Employee != null)
                            {
                                TraverSerPath = Employee.SupervisingDept.TraversalPath + Employee.SupervisingDept.SupervisingDeptID;
                                //dic.Add(managerIdName, Employee.SupervisingDept.AdminID);
                            }
                        }
                    }
                    //sau khi lay duoc duong dan cua tai khoan dang nhap
                    if (tableName.Equals("SCHOOLPROFILE"))
                    {
                        var SchoolCheck = SchoolProfileBusiness.Find(id);
                        if (SchoolCheck != null)
                        {
                            if ((glo.ProvinceID != null && glo.ProvinceID == SchoolCheck.ProvinceID) || glo.IsSuperRole)
                            {
                                dic = null;

                            }
                            else
                            {
                                dic = null;
                                checkError = false;
                            }
                        }


                    }
                }
                else if (typeObject == 5)
                {
                    //lay provinceID cua tai khoan dang nhap de so sanh
                    if (glo.ProvinceID != null)
                    {
                        dic.Add("PROVINCE_ID", glo.ProvinceID);
                    }
                }

                if (dic == null)
                {
                    dic = dicOther;
                    if (!checkError)
                    {
                        throw new Business.Common.BusinessException(Res.Get("Common_Validate_ErrorPermision"));
                    }
                }

                if (dic != null)
                {
                    bool hasRecord = Genric.ExistsRow(schemaName, tableName, dic);
                    if (!hasRecord)
                    {
                        throw new Business.Common.BusinessException(Res.Get("Common_Validate_ErrorPermision"));
                    }
                }

            }
        }

        public void SetViewDataPermission(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            // Lay quyen nguoi dung
            int permission = GetMenupermission(Controller, UserAccountID, IsAdmin, RolID);
            // Luu vao ViewData
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2; // dung de an/hien button Them moi
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3; // dung de an/hien button Luu
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;// dung de an/hien button Xoa
        }
        /// <summary>
        /// Get permission of menu
        /// </summary>
        /// <param name="Controller"></param>
        /// <param name="UserAccountID"></param>
        /// <param name="IsAdmin"></param>
        /// <returns></returns>
        public int GetMenupermission(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            if (IsAdmin) // Neu la quyen quan tri hien thi quyen cao nhat (cu the la quyen xoa)
            {
                return SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_DELETE;
            }
            else
            {
                //SMASEntities context = new SMASEntities();
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                var listGroupCatID = objUserGroup.Select(u => u.GroupCatID).ToList();
                if (objUserGroup != null && objUserGroup.Count() > 0)
                {
                    int? MaxPermission = GroupMenuBusiness.All.Where(gm => listGroupCatID.Contains(gm.GroupID)
                        && (RolID > 0 ? gm.GroupCat.Role.RoleID == RolID : RolID == 0) && gm.Menu.URL.ToLower().Contains(Controller.ToLower())).Max(gm => gm.Permission);
                    return MaxPermission == null ? 0 : MaxPermission.Value;
                }
            }
            return 0;
        }

        public bool IsEmployeeStatusWorking(int EmployeeID) 
        {
            bool IsWorking = false;
            Employee employee = this.EmployeeBusiness.Find(EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                IsWorking = true;
            }
            return IsWorking;
        }

        /// <summary>
        /// Set ViewData permission voi url menu
        /// </summary>
        /// <param name="Controller"></param>
        /// <param name="UserAccountID"></param>
        /// <param name="IsAdmin"></param>
        /// <param name="RolID"></param>
        public void SetViewDataPermissionWithArea(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            // Lay quyen nguoi dung
            int permission = GetMenupermissionWithArea(Controller, UserAccountID, IsAdmin, RolID);
            // Luu vao ViewData
            ViewData[SystemParamsInFile.PERMISSION_VIEW] = permission >= 1;
            ViewData[SystemParamsInFile.PERMISSION_CREATE] = permission >= 2; // dung de an/hien button Them moi
            ViewData[SystemParamsInFile.PERMISSION_UPDATE] = permission >= 3; // dung de an/hien button Luu
            ViewData[SystemParamsInFile.PERMISSION_DELETE] = permission >= 4;// dung de an/hien button Xoa
        }

        /// <summary>
        /// Tim phan quyen chinh xac url theo area
        /// </summary>
        /// <param name="Controller"></param>
        /// <param name="UserAccountID"></param>
        /// <param name="IsAdmin"></param>
        /// <param name="RolID"></param>
        /// <returns></returns>
        public int GetMenupermissionWithArea(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            if (IsAdmin) // Neu la quyen quan tri hien thi quyen cao nhat (cu the la quyen xoa)
            {
                return SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_DELETE;
            }
            else
            {
                string areaController = "/" + Controller + "Area/" + Controller;
                Menu menuObj = MenuBusiness.GetMenuByAreaNotVisiable(areaController);
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                var listGroupCatID = objUserGroup.Select(u => u.GroupCatID).ToList();
                if (objUserGroup != null && objUserGroup.Count() > 0)
                {
                    int? MaxPermission = GroupMenuBusiness.All.Where(gm => listGroupCatID.Contains(gm.GroupID)
                        && (RolID > 0 ? gm.GroupCat.Role.RoleID == RolID : RolID == 0) && gm.MenuID == menuObj.MenuID).Max(gm => gm.Permission);
                    return MaxPermission == null ? 0 : MaxPermission.Value;
                }
            }
            return 0;
        }

        public int GetMenupermissionWithAreaPath2(string Controller, int UserAccountID, bool IsAdmin, int RolID = 0)
        {
            if (IsAdmin) // Neu la quyen quan tri hien thi quyen cao nhat (cu the la quyen xoa)
            {
                return SMAS.Business.Common.GlobalConstants.PERMISSION_LEVEL_DELETE;
            }
            else
            {
                string areaController = "/" + Controller + "Area/" + Controller;
                Menu menuObj = MenuBusiness.GetMenuByAreaNotVisiablePath2(areaController);
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                var listGroupCatID = objUserGroup.Select(u => u.GroupCatID).ToList();
                if (objUserGroup != null && objUserGroup.Count() > 0)
                {
                    int? MaxPermission = GroupMenuBusiness.All.Where(gm => listGroupCatID.Contains(gm.GroupID)
                        && (RolID > 0 ? gm.GroupCat.Role.RoleID == RolID : RolID == 0) && gm.MenuID == menuObj.MenuID).Max(gm => gm.Permission);
                    return MaxPermission == null ? 0 : MaxPermission.Value;
                }
            }
            return 0;
        }

        //List menu quan ly thi(lay danh sach menu ben trai co phan quyen)
        public List<MenuExamination> MenuExamination(int UserAccountID, bool IsAdmin, int selected)
        {
            string areaExaminations = string.Empty;
            if (selected == SMAS.Business.Common.GlobalConstants.EXAM_ORGANIZE)
            {
                areaExaminations = System.Configuration.ConfigurationManager.AppSettings["KEY_MENU_EXAM_ORGANIZE"];
            }
            else if (selected == SMAS.Business.Common.GlobalConstants.EXAM_PROCESSING_TEST)
            {
                areaExaminations = System.Configuration.ConfigurationManager.AppSettings["KEY_MENU_EXAM_PROCESS_TEST_RESULT"];
            }
            else if (selected == SMAS.Business.Common.GlobalConstants.EXAM_STATICTIS_REPORT)
            {
                areaExaminations = System.Configuration.ConfigurationManager.AppSettings["KEY_MENU_EXAM_STATICTIS_REPORT"];
            }

            List<MenuExamination> listMenuResult = new List<MenuExamination>();
            //Lấy ID menu_id (=> parent_id menu con) cua menu cap 2 dua theo menupath va area
            Menu menuObj = MenuBusiness.GetMenuByArea(areaExaminations);
            if (menuObj != null)
            {
                listMenuResult = (from l in MenuBusiness.GetMenuExamination(UserAccountID, menuObj.MenuID, IsAdmin, _globalInfo.AppliedLevel)
                                  group l by new
                                  {
                                      l.OrderNumber,
                                      l.MenuName,
                                      l.URL
                                  } into g
                                  select new MenuExamination
                                  {
                                      LinkMenu = g.Key.URL + "?Tab=" + _globalInfo.TabIndexTC,
                                      MenuName = g.Key.MenuName,
                                      OrderNumber = g.Key.OrderNumber
                                  }).OrderBy(p => p.OrderNumber).ToList();
            }
            return listMenuResult;
        }

        //List danh sach sub menu (lay danh sach menu ben trai co phan quyen)
        public List<MenuExamination> GetSubMenu(int UserAccountID, bool IsAdmin, string url)
        {

            List<MenuExamination> listMenuResult = new List<MenuExamination>();
            //Lấy ID menu_id (=> parent_id menu con) cua menu cap 2 dua theo menupath va area
            Menu menuObj = MenuBusiness.GetMenuByArea(url);
            string productVersionID = _globalInfo.ProductVersionID.HasValue ? _globalInfo.ProductVersionID.Value.ToString() : "";
            if (menuObj != null)
            {
                listMenuResult = (from l in MenuBusiness.GetMenuByReport(UserAccountID, menuObj.MenuID, IsAdmin, _globalInfo.AppliedLevel)
                                  where l.ProductVersion.Contains(productVersionID)
                                  group l by new
                                  {
                                      l.OrderNumber,
                                      l.MenuName,
                                      l.URL
                                  } into g
                                  select new MenuExamination
                                  {
                                      LinkMenu = g.Key.URL + "?Tab=" + _globalInfo.TabIndexReport,
                                      MenuName = g.Key.MenuName,
                                      OrderNumber = g.Key.OrderNumber
                                  }).OrderBy(p => p.OrderNumber).ToList();
            }
            return listMenuResult;
        }

        public List<MenuExamination> GetSubMenuForSupervisingDept(string parentKeyMenu, string tab)
        {
            List<MenuExamination> listMenuResult = new List<MenuExamination>();
            string menuUrl = System.Configuration.ConfigurationManager.AppSettings[parentKeyMenu];
            Menu menu = MenuBusiness.GetMenuByArea(menuUrl);

            int role = 0;
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                role = 4;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                role = 3;
            }

            if (menu != null)
            {
                listMenuResult = (from l in MenuBusiness.GetAllSubMenu(menu.MenuID).ToList()
                                  join rm in RoleMenuBusiness.All.Where(o => o.RoleID == role).ToList() on l.MenuID equals rm.MenuID
                                  group l by new
                                  {
                                      l.OrderNumber,
                                      l.MenuName,
                                      l.URL
                                  } into g

                                  select new MenuExamination
                                  {
                                      LinkMenu = g.Key.URL + "?Tab=" + tab,
                                      MenuName = g.Key.MenuName,
                                      OrderNumber = g.Key.OrderNumber
                                  }).Distinct().OrderBy(p => p.OrderNumber).ToList();
            }

            return listMenuResult;

        }

        public ActionResult ViewPatternHtml(string path)
        {
            return new FilePathResult(path, "text/html");
        }

        /// <summary>
        /// lay chuoi khoa nhap lieu cua truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="AppliedLevelID"></param>
        /// <returns></returns>
        public LockInputSupervisingDept GetLockTitleBySupervisingDept(int SchoolID, int AcademicYearID, int AppliedLevelID)
        {
            LockInputSupervisingDept objLockInput = new LockInputSupervisingDept();
            objLockInput = (from l in LockInputSupervisingDeptBusiness.All
                            where l.SchoolID == SchoolID
                            && l.AcademicYearID == AcademicYearID
                            && l.AppliedLevelID == AppliedLevelID
                            select l).FirstOrDefault();
            return objLockInput;
        }
        /// <summary>
        /// lay trang thai Phong hoac So tu UserAccountID
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public int GetHierachyLevelIDByUserAccountID(int UserAccountID)
        {
            int HierachyLevelID = 0;
            UserAccount objUA = UserAccountBusiness.Find(UserAccountID);
            if (objUA != null && objUA.EmployeeID.HasValue)
            {
                int EmployeeID = objUA.EmployeeID.Value;
                Employee objEmployee = EmployeeBusiness.Find(EmployeeID);
                HierachyLevelID = objEmployee != null && objEmployee.SupervisingDept != null ? objEmployee.SupervisingDept.HierachyLevel : 0;
            }
            else
            {
                SupervisingDept objSupervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == UserAccountID).FirstOrDefault();
                HierachyLevelID = objSupervisingDept != null ? objSupervisingDept.HierachyLevel : 0;
            }


            return HierachyLevelID;
        }

        #region Tutv check permission (view allowed minimum)
        /// <summary>
        /// Check quyen co it nhat quyen xem
        /// </summary>
        public void CheckActionPermissionMinView()
        {
            string smasUrl = UtilsBusiness.UrlSmas;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int permission = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 1)
            {
                //string strHost = Request.Url.Authority.ToLower() + "/Home/NotRole";
                string strHost = UtilsBusiness.GetFullUrl(smasUrl, "Home/NotRole");
                Response.Redirect(strHost);
            }
        }

        public void CheckActionPermissionMinViewPath2()
        {
            string smasUrl = UtilsBusiness.UrlSmas;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int permission = GetMenupermissionWithAreaPath2(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 1)
            {
                //string strHost = Request.Url.Authority.ToLower() + "/Home/NotRole";
                string strHost = UtilsBusiness.GetFullUrl(smasUrl, "Home/NotRole");
                Response.Redirect(strHost);
            }
        }

        #endregion

        #region Tutv check permission (add allowed minimum)
        /// <summary>
        /// Co it nhat quyen them
        /// </summary>
        public void CheckActionPermissionMinAdd()
        {
            string smasUrl = UtilsBusiness.UrlSmas;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
            int permission = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 2)
            {
                string strHost = UtilsBusiness.GetFullUrl(smasUrl, "Home/NotRole"); //Request.Url.Authority.ToLower() + "/Home/NotRole";
                Response.Redirect(strHost);
            }
        }
        #endregion

        #region Tutv check permission (view allowed minimum)
        /// <summary>
        /// Co it nhat quyen xem (co tra ve thong bao)
        /// </summary>
        public bool CheckActionPermissionMinViewReturn()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int permission = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 1)
            {
                return false;
            }
            return true;
        }

        public bool CheckActionPerMinViewByController(string ControllerName)
        {
            int permission = GetMenupermissionWithArea(ControllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 1)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Tutv check permission (add allowed minimum)
        /// <summary>
        /// Co it nhat quyen them (co tra ve thong bao)
        /// </summary>
        public bool CheckActionPermissionMinAddReturn()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
            int permission = GetMenupermissionWithAreaPath2(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission < 2)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Kiem tra co phai mo hinh truong hoc moi hay khong
        public bool IsNewSchoolModel()
        {
            int? schoolId = _globalInfo.SchoolID;
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolId);
            bool isNewSchoolModel = false;
            if (sp != null)
            {
                isNewSchoolModel = sp.IsNewSchoolModel == true ? true : false;
            }
            return isNewSchoolModel;
        }

        #endregion
        #region Modul thi cho phong so
        public List<ComboObject> GetListYear()
        {
            var lstYear = new List<ComboObject>();
            //Lay nam hoc hien tai
            ComboObject curYear = GetCurYearObject();
            lstYear.Add(curYear);

            //Lay ra cac nam hoc co ky thi cua so
            List<int> lstExamYear = DOETExaminationsBusiness.GetExaminationsOfDept(_globalInfo.SupervisingDeptID.Value).OrderByDescending(p => p.CreateTime).Select(o => o.Year).Distinct().ToList();
            for (int i = 0; i < lstExamYear.Count; i++)
            {
                int year = lstExamYear[i];
                if (year != Int32.Parse(curYear.key))
                {
                    lstYear.Add(GetYearObject(year));
                }
            }
            return lstYear;
        }
        private int GetCurYear()
        {
            int year;
            DateTime dateNow = DateTime.Now.Date;
            DateTime midDate = new DateTime(dateNow.Year, 7, 1);
            if (DateTime.Compare(dateNow, midDate) < 0)
            {
                year = dateNow.Year - 1;
            }
            else
            {
                year = dateNow.Year;
            }
            return year;
        }
        private ComboObject GetCurYearObject()
        {
            int year = GetCurYear();

            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }
        private ComboObject GetYearObject(int year)
        {
            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        


        #endregion
        protected int Semester
        {
            get
            {
                DateTime dateTimeNow = DateTime.Now;
                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                if (aca != null)
                {
                    if (aca.FirstSemesterStartDate.Value.Date <= dateTimeNow && aca.SecondSemesterStartDate.Value.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59) >= dateTimeNow)
                    {
                        return 1;
                    }
                    // Anhvd9 - Cho phep nhan tin toi 31.8 neu nam hoc cu da ket thuc
                    if (aca.SecondSemesterStartDate.Value.Date <= dateTimeNow && new DateTime(aca.SecondSemesterEndDate.Value.Year, SMAS.Business.Common.GlobalConstantsEdu.MONTH_END_ALLOW_SEND_SMS, SMAS.Business.Common.GlobalConstantsEdu.DAYOFMONTH_END_ALLOW_SEND_SMS, 23, 59, 59) >= dateTimeNow)
                    {
                        return 2;
                    }
                }
                return 0;

            }
        }

        protected AcademicYear AcademicYear
        {
            get
            {
                return AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            }
        }

        protected int Year
        {
            get
            {
                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                return aca != null ? aca.Year : 0;
            }
        }
    }
}

