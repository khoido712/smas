﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Constants
{
    public class GlobalConstantsEdu
    {
        #region string value constants
        public const string COMMON_IMAGE_TYPE = "image/jpeg";

        public const string CONST_FALSE = "false";

        public const char COMMON_COMMA = ',';
        public const string COMMON_COLON_SPACE = ": ";
        public const string PRE_NUMBER = "PreNumber";
        public const string PRE_NUMBER_LEN_9 = "PreNumberLen9";
        public const string PRE_NUMBER_LEN_10 = "PreNumberLen10";
        public const string PRE_NUMBER_LEN_11 = "PreNumberLen11";
        public const string PRE_NUMBER_LEN_12 = "PreNumberLen12";

        public const string PREFIX_VIETTEL_10 = "PrefixViettel10";
        public const string PREFIX_VIETTEL_11 = "PrefixViettel11";
        public const string PREFIX_VIETTEL_12 = "PrefixViettel12";

        public const string TEAMPLATE_EXCEL = "TemplateExcel";

        public const string SALT_ENCRYPT = "SHA1";

        //type smsACTIVE
        public const int COMMON_SCHOOL_SMS_ACTIVE_NOT_USE = 0;//khong su dung
        public const int COMMON_SCHOOL_SMS_ACTIVE_NO_LIMIT = 2; //khong gioi han
        public const int COMMON_SCHOOL_SMS_ACTIVE_LIMIT = 1; // gioi han
        public const int COMMON_SCHOOL_SMS_ACTIVE_BLOCK = 3; //bi chan cat

        #endregion

        #region common character
        public const string CHARACTER_M = "M";

        public const string CHARACTER_T = "T";

        public const string CHARACTER_P = "P";

        public const string CHARACTER_V = "V";

        public const string CHARACTER_K = "K";

        public const string CHARACTER_E = "E";

        public const string CHARACTER_F = "F";

        public const string CHARACTER_G = "G";
        #endregion

        #region Pupil common value
        public const int COMMON_PUPIL_STATUS_STUDYING = 1;
        public const int COMMON_PUPIL_STATUS_GRADUATED = 2;
        public const int COMMON_PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL = 3;
        public const int COMMON_PUPIL_STATUS_LEAVED_OFF = 4;
        public const int COMMON_PUPIL_STATUS_MOVED_TO_OTHER_CLASS = 5;
        public const int COMMON_PUPIL_STATUS_UNIDENTIFIED = 0;
        #endregion

        public const int COMMON_LEVEL1 = 1;
        public const int COMMON_LEVEL2 = 2;
        public const int COMMON_LEVEL3 = 3;
        public const int COMMON_LEVEL4 = 4;
        public const int COMMON_LEVEL5 = 5;
        public const int COMMON_LEVEL6 = 6;
        public const int COMMON_LEVEL7 = 7;
        public const int COMMON_LEVEL8 = 8;
        public const int COMMON_LEVEL9 = 9;
        public const int COMMON_LEVEL10 = 10;
        public const int COMMON_LEVEL11 = 11;
        public const int COMMON_LEVEL12 = 12;
        public const int COMMON_LEVEL13 = 13;
        public const int COMMON_LEVEL14 = 14;

        public const int COMMON_GRADE_PRIMARY = 1;//cấp 1
        public const int COMMON_GRADE_SECONDARY = 2;//cấp 2
        public const int COMMON_GRADE_TERTIARY = 3;//cấp 3
        public const int COMMON_GRADE_GRECHER = 4;//nhà trẻ
        public const int COMMON_GRADE_KINDERGARTEN = 5;//mẫu giáo

        //Contact Group ID
        public const int COMMON_CONTACT_GROUP_ALL = 0; //Toan truong
        public const string COMMON_CONTACT_GROUP_ALL_SMSCODE = "toantruong"; //Toan truong
        public const string COMMON_CONTACT_GROUP_ALL_NAME = "Toàn trường";
        public const string COMMON_HISTORY_SCHOOL_NAME_COMPLETE = "Nhà trường";
        public const string COMMON_CONTACT_GROUP_BGH = "Ban giám hiệu";
        public const string COMMON_CONTACT_GROUP_CB = "Chi bộ";
        public const string COMMON_CONTACT_GROUP_CDT = "Công đoàn trường";
        public const string COMMON_CONTACT_GROUP_DTN = "Đoàn thanh niên";
        public const string COMMON_CONTACT_GROUP_GVCN = "Giáo viên chủ nhiệm";
        public const string COMMON_CONTACT_GROUP_GVBM = "Giáo viên bộ môn";
        public const string COMMON_CONTACT_GROUP_CBNAM = "Cán bộ nam";
        public const string COMMON_CONTACT_GROUP_CBNU = "Cán bộ nữ";
        public const string COMMON_CONTACT_GROUP_VIETTEL = "Dùng SĐT Viettel";

        /// <summary>
        /// phhs -> gv
        /// </summary>
        public const byte COMMON_SMS_COMUNICATION_PARENT_TO_TEACHER = 1;

        /// <summary>
        /// giao vien toi phhs
        /// </summary>
        public const byte COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT = 2;

        /// <summary>
        /// giao vien toi giao vien
        /// </summary>
        public const byte COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER = 3;

        // Constants for school type
        public const int COMMON__PRIMARY_EDUCATION_SCHOOL = 1;

        public const int COMMON_EMPLOYMENT_STATUS_WORKING = 1;
        public const int COMMON_EMPLOYMENT_STATUS_MOVED = 2;
        public const int COMMON_EMPLOYMENT_STATUS_RETIRED = 3;
        public const int COMMON_EMPLOYEE_TYPE_TEACHER = 1;   //loai giao vien
        public const int COMMON_EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF = 3; //nhan vien phong so

        public const int COMMON_SMS_PARENT_CONTRACT_DETAIL_AVAIABLE = 1;

        public const int COMMON_SMS_PARENT_CONTRACT_ACCEPT = 1;
        public const int COMMON_SMS_PARENT_CONTRACT_NOT_ACCEPT = 0;
        public const int COMMON_TEACHER_STATUS_WORKING = 1;

        //Type HistorySMS        
        public const int COMMON_HISTORYSMS_TO_TEACHER_ID = 1;
        public const int COMMON_HISTORYSMS_TO_COMMUNICATION_ID = 30;
        public const int COMMON_HISTORYSMS_TO_SCHOOL_ID = 5;
        public const int COMMON_HISTORYSMS_TO_EMPLOYEE_ID = 6;
        public const int COMMON_HISTORYSMS_TO_DAYMARK_ID = 25;
        public const int COMMON_HISTORYSMS_TO_WEEKMARK_ID = 22;
        public const int COMMON_HISTORYSMS_TO_MONTHSMS_ID = 18;
        public const int COMMON_HISTORYSMS_TO_SEMESTERSMS_ID = 19;

        public const string COMMON_HISTORYSMS_TO_DAYMARK_TYPECODE = "TNDN";
        public const string COMMON_HISTORYSMS_TO_PERIODSMS_TYPECODE = "TNDD";
        public const string COMMON_HISTORYSMS_TO_WEEKMARK_TYPECODE = "TNDT";
        public const string COMMON_HISTORYSMS_TO_MONTHSMS_TYPECODE = "TNDTH";
        public const string COMMON_HISTORYSMS_TO_SEMESTERSMS_TYPECODE = "TNHKI";
        public const string COMMON_HISTORYSMS_TO_CALENDAR_TYPECODE = "TNTKB";
        public const string COMMON_HISTORYSMS_TO_LECTUREREGISTER_TYPECODE = "TNBB";
        public const string COMMON_HISTORYSMS_TO_TRAINING_TYPECODE = "TNRL";
        public const string COMMON_HISTORYSMS_TO_TRAINING_WEEK_TYPECODE = "TNRLT";
        public const string COMMON_HISTORYSMS_TO_TRAINING_MONTH_TYPECODE = "TNRLTH";
        public const string COMMON_HISTORYSMS_TO_TRAINING_EXPEND_TYPECODE = "TNRLMR";
        public const string COMMON_HISTORYSMS_TO_HEALTH_TYPECODE = "TNSK";
        public const string COMMON_HISTORYSMS_TO_PERIOD_HEALTH_TYPECODE = "TNSKDK";
        public const string COMMON_HISTORYSMS_TO_ACTIVITY_TYPECODE = "TNHD";
        public const string COMMON_HISTORYSMS_TO_GROWTH_TYPECODE = "TNTT";
        public const string COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE = "TNTD";
        public const string COMMON_HISTORYSMS_TO_EXAM_MARK_SEMESTER = "DTHK";
        public const string COMMON_HISTORYSMS_TO_COMMENTS_MONTH_TYPECODE = "NXTTT30";
        public const string COMMON_HISTORYSMS_TO_SEMESTER_RESULT_TYPECODE = "KQHKTT30";
        public const string COMMON_HISTORYSMS_TO_COMMENTS_ENDING_TYPECODE = "NXCKTT30";
        public const string COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE = "NXTVNEN";
        public const string COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE = "KQHKVNEN";
        public const string COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE = "NXCKVNEN";
        public const string COMMON_HISTORYSMS_TO_EXAM_SCHEDULE_TYPECODE = "TNTBLT";
        public const string COMMON_HISTORYSMS_TO_EXAM_RESULT_TYPECODE = "TNTBKQT";
        public const string COMMON_HISTORYSMS_TO_TT22_KQCK = "TT22KQCK";
        public const string COMMON_HISTORYSMS_TO_TT22_KQGK = "TT22KQGK";
        public const string COMMON_HISTORYSMS_TO_TT22_NXCK = "TT22NXCK";
        public const string COMMON_HISTORYSMS_TO_BNTU = "BNTU";
        public const string COMMON_HISTORYSMS_TO_BNTH = "BNTH";
        public const string COMMON_HISTORYSMS_TO_TDN = "TDN";
        public const string COMMON_HISTORYSMS_TO_TDT = "TDT";
        public const string COMMON_HISTORYSMS_TO_CDSK = "CDSK";
        public const string COMMON_HISTORYSMS_TO_DGSPT = "DGSPT";
        public const string COMMON_HISTORYSMS_TO_DGHDN = "DGHDN";

        public const string COMMON_ERROR = "error";
        public const string COMMON_SUCCESS = "success";
        public const string COMMON_INFO = "info";


        #region comon syntax
        /// <summary>
        /// khoang trang
        /// </summary>
        public const string SPACE = " ";

        public const string DASH = "-";

        public const string COLON = ":";

        public const string COMMA = ",";

        public const char charCOMMA = ',';

        public const char SYNTAX_PIPE = '|';

        public const string SYNTAX_DOT = ".";

        public const string SYNTAX_CROSS_LEFT = "/";

        /// <summary>
        /// ky tu "\n"
        /// </summary>
        public const string N_CHAR = "\n";

        /// <summary>
        /// ky tu "\r"
        /// </summary>
        public const string R_CHAR = "\r";

        public const string FloatingPoint = ".0";

        public const string MarkFormat = "10.0";

        public const string Mark = "10";
        #endregion

        /// <summary>
        /// hoc ki 1
        /// </summary>
        public const int SEMESTER_OF_YEAR_FIRST = 1;
        /// <summary>
        /// hoc ki 2
        /// </summary>
        public const int SEMESTER_OF_YEAR_SECOND = 2;
        /// <summary>
        /// ca nam
        /// </summary>
        public const int SEMESTER_OF_YEAR_ALL = 3;

        /// <summary>
        /// giua ki 1
        /// </summary>
        public const int MIDDLE_SEMESTER_OF_YEAR_FIRST = -1;
        /// <summary>
        /// cuoi ki 1
        /// </summary>
        public const int END_SEMESTER_OF_YEAR_FIRST = 1;
        /// <summary>
        ///giua ki 2
        /// </summary>
        public const int MIDDLE_SEMESTER_OF_YEAR_SECOND = -2;
        /// <summary>
        ///  cuoi ki 2
        /// </summary>


        #region LOGGER
        public const int END_SEMESTER_OF_YEAR_SECOND = 2;

        public const string COMMON_AREA_TEXT = "area text";

        public const string COMMON_CONTROLLER_TEXT = "controller";

        public const string COMMON_ACTION_TEXT = "action";

        public const string DESCRIPTION = "description";

        public const string GROUPID = "groupID";

        public const string PARAMETER = "parameter";

        public const string REFERERK = "Refererk";

        public const string ROLDID = "roldeID";

        public const string OLDOBJECTVALUE = "OldObjectValue";

        public const string USER_DESCRIPTION = "userDescription";
        public const string USER_ACTION = "userAction";
        public const string USER_FUNCTION = "userFunction";
        #endregion

        #region SMSType Constants
        public const byte CONST_COMMUNICATION_PARENT_TO_TEACHER = 1;
        public const byte CONST_COMMUNICATION_TEACHER_TO_PARENT = 2;
        public const byte CONST_COMMUNICATION_TEACHER_TO_TEACHER = 3;
        #endregion

        #region SMSParentContract
        public const int STATUS_CONTRACT_NOT_APPROVAL = 0;
        public const int STATUS_CONTRACT_APPROVAL = 1;

        public const string ERROR = "error";

        public const string SUCCESS = "success";

        public const string PATH_TO_RIGHT = "\\";

        public const string NOT_USE_SMSSPARENT = "NOT_USE_SMSPARENT";

        public const string NOT_USE_MESSAGE = "NOT_USE_MESSAGE";

        /// <summary>
        /// Tra truoc
        /// </summary>
        public const short PAYMENT_TYPE_PREPAY = 1;
        /// <summary>
        /// Tra sau
        /// </summary>
        public const short PAYMENT_TYPE_POSTPAID = 2;
        #endregion

        /// <summary>
        /// Đang làm việc
        /// </summary>
        public const int EMPLOYEE_STATUS_WORKING = 1;


        #region Table
        #region SMSHistory
        /// <summary>
        /// nguoi nhan la hoc sinh
        /// </summary>
        public const int COMMON_HISTORY_RECEIVER_PUPIL_ID = 0;//nguoi nhan la hoc sinh

        /// <summary>
        /// nguoi nhan la phhs
        /// </summary>
        public const int COMMON_HISTORY_RECEIVER_PARENT_ID = 1;//nguoi nhan la phhs

        /// <summary>
        /// nguoi nhan la giao vien
        /// </summary>
        public const int COMMON_HISTORY_RECEIVER_TEACHER_ID = 2;//nguoi nhan la giao vien

        /// <summary>
        /// nguoi nhan la hieu truong truong
        /// </summary>
        public const int COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID = 5;//nguoi nhan la hieu truong truong

        /// <summary>
        /// nguoi nhan la nhan vien phong ban
        /// </summary>
        public const int COMMON_HISTORY_RECEIVER_EMPLOYEE_ID = 6; // nguoi dung nhan vien phong ban

        /// <summary>
        /// tin nhan diem ngay
        /// </summary>
        public const string HISTORYSMS_TO_DAYMARK_TYPECODE = "TNDN";

        /// <summary>
        /// tin nhan diem dot
        /// </summary>
        public const string HISTORYSMS_TO_PERIODSMS_TYPECODE = "TNDD";

        /// <summary>
        /// tin nhan diem tuan
        /// </summary>
        public const string HISTORYSMS_TO_WEEKMARK_TYPECODE = "TNDT";

        /// <summary>
        /// tin nhan diem thang
        /// </summary>
        public const string HISTORYSMS_TO_MONTHSMS_TYPECODE = "TNDTH";

        /// <summary>
        /// tin nhan hoc ky
        /// </summary>
        public const string HISTORYSMS_TO_SEMESTERSMS_TYPECODE = "TNHKI";

        /// <summary>
        /// tin nhan tkb
        /// </summary>
        public const string HISTORYSMS_TO_CALENDAR_TYPECODE = "TNTKB";

        /// <summary>
        /// tin nhan bao bai
        /// </summary>
        public const string HISTORYSMS_TO_LECTUREREGISTER_TYPECODE = "TNBB";

        /// <summary>
        /// tin nhan ren luyen
        /// </summary>
        public const string HISTORYSMS_TO_TRAINING_TYPECODE = "TNRL";


        /// <summary>
        /// tin nhan ren luyen theo tuan
        /// </summary>
        public const string HISTORYSMS_TO_TRAINING_WEEK_TYPECODE = "TNRLT";


        /// <summary>
        /// tin nhan ren luyen theo thang
        /// </summary>
        public const string HISTORYSMS_TO_TRAINING_MONTH_TYPECODE = "TNRLTH";

        /// <summary>
        /// tin nhan suc khoe
        /// </summary>
        public const string HISTORYSMS_TO_HEALTH_TYPECODE = "TNSK";

        /// <summary>
        /// tin nhan suc khoe dot
        /// </summary>
        public const string HISTORYSMS_TO_PERIOD_HEALTH_TYPECODE = "TNSKDK";

        /// <summary>
        /// tin nhan hoat dong
        /// </summary>
        public const string HISTORYSMS_TO_ACTIVITY_TYPECODE = "TNHD";

        /// <summary>
        /// tin nhan tang truong
        /// </summary>
        public const string HISTORYSMS_TO_GROWTH_TYPECODE = "TNTT";

        /// <summary>
        ///  tin nhan trao doi
        /// </summary>
        public const string HISTORYSMS_TO_COMMUNICATION_TYPECODE = "TNTD";

        /// <summary>
        /// Diem thi hoc ky
        /// </summary>
        public const string HISTORYSMS_TO_SEMESTER = "DTHK";

        /// <summary>
        /// tin nhan trao doi
        /// </summary>
        public const int COMMON_HISTORYSMS_TO_COMMUNICATION_TEACHER = 1;



        public const int COMMON_TABLE_SMSHISTORY_CONTENT_MAXLENGTH = 2000;

        /// <summary>
        /// so dien thoai khong hop le
        /// </summary>
        public const short COMMON_TABLE_SMSHISTORY_FLAG_INVALID_MOBILE = 1;

        /// <summary>
        /// noi dung khong hop le
        /// </summary>
        public const short COMMON_TABLE_SMSHISTORY_FLAG_INVALID_CONTENT = 2;
        /// <summary>
        /// ma loi tra ve cho ws thanh cong
        /// </summary>
        public const int VALIDATE_SUCESS = 1;
        /// <summary>
        /// ma loi tra ve cho ws error
        /// </summary>
        public const int VALIDATE_EXCEPTION = 0;

        /// <summary>
        /// bản tin đăng ký tài khoản
        /// </summary>
        public const int COMMON_HISTORYSMS_TO_REGISTER_ACCOUNT_SPARENT = -1;

        #endregion

        #region SupervisingDept
        /// <summary>
        ///Cấp bộ 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_MINISTRY = 1;

        /// <summary>
        ///Các phòng ban quản lý cấp bộ 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_MINISTRY_DEPARTMENT = 2;

        /// <summary>
        ///Cấp Sở GD 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE = 3;

        /// <summary>
        ///Các phòng ban quản lý cấp Sở GD 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT = 4;

        /// <summary>
        ///Cấp Phòng GD 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE = 5;

        /// <summary>
        ///Cấp phòng, ban thuộc Phòng Giáo dục 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT = 6;

        /// <summary>
        ///Cấp trường 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_SCHOOL = 7;

        /// <summary>
        ///Cấp lớp 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_CLASS = 8;

        /// <summary>
        ///Không xác định 
        /// </summary> 
        public const int COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_UNIDENTIFIED = 0;
        #endregion

        #region SMSParentContract
        public const int SMS_PARENT_CONTRACT_DETAIL_AVAILABLE = 1;

        public const int SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT = 1;

        public const int CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER = 1;

        public const int CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER = 2;

        public const int CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR = 3;
        #endregion

        #region MT
        public const short MT_STATUS_NOT_SEND = 0;
        #endregion

        #region
        /// <summary>
        /// LOG action Add
        /// </summary>
        public const string COMMON_LOG_ACTION_ADD = "Add";

        /// <summary>
        /// LOG action Update
        /// </summary>
        public const string COMMON_LOG_ACTION_UPDATE = "Update";

        /// <summary>
        /// LOG action Delete
        /// </summary>
        public const string COMMON_LOG_ACTION_DELETE = "Delete";

        /// <summary>
        /// LOG action View
        /// </summary>
        public const string COMMON_LOG_ACTION_VIEW = "View";

        #endregion
        #endregion
        #region SendSMSToSchool
        public const string COMMON_SUPERVISINGDEPT = "current unit object";
        public const string COMMON_PARENTUNIT = "current parent unit object";
        public const int COMMON_SMS_COUNT = 160;
        public const int COMMON_SIGN_SMS_COUNT = 67;
        public const int COMMON_ONLY_SIGN_SMS_COUNT = 70;

        public const string COMMON_SMS_LIMIT_SUPER_USER = "LimitSMSSuperUser";
        #endregion

        #region common const
        public const string AREA_TEXT = "area";

        public const string CONTROLLER_TEXT = "controller";

        public const string ACTION_TEXT = "action";

        public const string ERROR_AJAX_REQUEST = "error ajax request";

        public const string IS_AUTHORIZE_AJAX_ERROR = "is authorize error";
        #endregion

        #region partition
        /// <summary>
        /// partition theo cot schoolid tren smscommunication
        /// </summary>
        public const int PARTITION_SMSCOMMUNICATION_SCHOOLID = 20;

        /// <summary>
        /// partition theo cot schoolid tren smscommunicationGroup
        /// </summary>
        public const int PARTITION_SMSCOMMUNICATION_GROUP_SCHOOLID = 10;

        /// <summary>
        /// partition theo cot schoolid tren SMSHistory
        /// </summary>
        public const int PARTITION_SMSHISTORY_SCHOOLID = 20;

        /// <summary>
        /// partition theo cot schoolid tren smsparentcontract
        /// </summary>
        public const int PARTITION_SMSPARENTCONTRACT_SCHOOLID = 10;

        /// <summary>
        /// partition theo cot SMSParentContractID tren smsparentcontractdetail
        /// </summary>
        public const int PARTITION_SMSPARENTCONTRACT_DETAIL_SMSPARENTCONTRACTID = 10;
        #endregion

        #region Payment
        /// <summary>
        /// giao dich nap the cong tien
        /// </summary>
        public const byte TRANSACTION_TYPE_AMOUNT_PLUS_TOPUP = 1;

        /// <summary>
        /// Loai thanh toan qua Bankplus
        /// </summary>
        public const byte SERVICE_TYPE_AMOUNT_PLUS_BP = 2;

        /// <summary>
        /// giao dich tru tien
        /// </summary>
        public const byte TRANSACTION_TYPE_AMOUNT_SUB = 2;


        /// <summary>
        /// dich vu gui tin nhan GV
        /// </summary>
        public const byte SERVICE_TYPE_SENT_TO_TECHER = 1;

        /// <summary>
        /// dich vu dang ky hop dong sms_edu
        /// </summary>
        public const byte SERVICE_TYPE_REGIS_CONTRACT = 2;

        /// <summary>
        /// dich vu gui tin nhan cho truong
        /// </summary>
        public const byte SERVICE_TYPE_SENT_TO_SCHOOL = 3;

        /// <summary>
        /// dich vu gui tin nhan cho phong/ban
        /// </summary>
        public const byte SERVICE_TYPE_SENT_TO_DEPARTMENT = 4;

        /// <summary>
        /// Thanh toan No cuoc
        /// </summary>
        public const byte SERVICE_TYPE_CHECKOUT_DEFERRED_PAYMENT = 5;

        /// <summary>
        /// Thanh toan dich vu Brandname
        /// </summary>
        public const byte SERVICE_TYPE_BRANDNAME_PAYMENT = 6;

        /// <summary>
        /// Mua them tin nhan
        /// </summary>
        public const byte SERVICE_TYPE_BUY_EXTRA = 7;

        #endregion

        #region SMS Content const

        /// <summary>
        /// Hoc sinh khong ton tai
        /// </summary>
        public const string SMS_ERR_PUPIL_NOT_EXISTS = "Ma hoc sinh {0} khong ton tai. Quy Phu huynh truy cap http://smsedu.smas.vn hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Nha truong chua kich hoat dich vu SLLDT
        /// </summary>
        public const string SMS_ERR_SCHOOL_NOT_ACTIVE_SMSPARENT = "Nha truong chua ho tro DV So lien lac dien tu. Quy Phu huynh vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Nha truong ko dong y dang ky SP2 Online
        /// </summary>
        public const string SMS_ERR_SCHOOL_NOT_ACTIVE_SP2ONLINE = "Cu phap khong dung. Quy Phu huynh vui long lien he 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Dang ky Het Thoi Gian Nam Hoc
        /// </summary>
        public const string SMS_REG_TIMEOUT_SEMESTER1 = "Thoi gian dang ky cho hoc ky I da ket thuc. Quy Phu huynh vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Thuoc thoi gian dang ky cho hoc ky 2 chua den
        /// </summary>
        public const string SMS_REG_SEMESTER2_NOT_START = "Thoi gian dang ky cho hoc ky II chua den. Quy Phu huynh vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Thuoc thoi gian trong nam hoc nhung ko thuoc hoc ky nao
        /// </summary>
        public const string SMS_REG_NOT_INT_SEMESTER = "Nha truong chua cap nhat du lieu nam hoc moi tren phan mem SMAS. Quy Phu huynh vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Gia han Het Thoi Gian Nam Hoc
        /// </summary>
        public const string SMS_GH_TIMEOUT_ACADEMIC_YEAR = "Goi cuoc Quy Phu huynh muon gia han da het hieu luc. Vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de biet them chi tiet";

        /// <summary>
        /// Goi cuoc da duoc dang ky
        /// </summary>
        public const string SMS_REG_DUPLICATE = "Quy Phu huynh dang su dung DV So lien lac dien tu cho HS {0}. Chi tiet LH 18008000 nhanh 1 (mien phi)";

        /// <summary>
        /// Gia han cho nhieu hoc sinh
        /// </summary>
        public const string SMS_GH_MORE_SUB_VT = "Quy Phu huynh dang su dung DV So lien lac dien tu cho nhieu hoc sinh. De {0}, thuc hien tuong tu cho cac HS con lai. Phi gui tin den 8062: 500d/tin. LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_GH_MORE_SUB_OTHER = "Quy Phu huynh dang su dung DV So lien lac dien tu cho nhieu hoc sinh. Vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de duoc huong dan";

        public const string SMS_GH_MORE_SUB_PUPIL = "gia han DV cho HS {0} soan GH SLL {1} gui 8062";

        public const string SMS_CANCEL_MORE_SUB_PUPIL = "huy DV cho HS {0}, soan HUY SLL {1} gui 8062";

        public const string SMS_REG_SUCCESS_SUB = "bang SDT: {0} va mat khau: {1} ";

        public const string SMS_REG_SUCCESS_SUB_SERVICE_PRICE = "Phi DV: {0}d/{1}. ";

        public const string SMS_REG_SUCCESS = "Quy Phu huynh dang ky thanh cong DV So lien lac dien tu cua HS {0}. {1}Huy DV, soan HUY SLL gui 8062(500d). LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_REG_SUCCESS_SMAS = "SMAS: Quy Phu huynh da dang ky thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao dinh ky ket qua hoc tap va thong tin khac tu nha truong qua SMS hoac truy cap http://smsedu.smas.vn {1}de trao doi voi giao vien va tra cuu ket qua hoc tap cua con em minh";

        public const string SMS_GH_SUCCESS = "Quy Phu huynh da gia han thanh cong DV So lien lac dien tu cua HS {0}. Chi tiet LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_GH_SUCCESS_SMAS = "SMAS: Quy Phu huynh da gia han thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao dinh ky ket qua hoc tap va thong tin khac tu nha truong qua SMS hoac truy cap http://smsedu.smas.vn {1}de trao doi voi giao vien va tra cuu ket qua hoc tap cua con em minh";

        public const string SMS_CANCEL_NOT_EXIST = "Quy Phu huynh chua dang ky DV So lien lac dien tu. Chi tiet LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_GH_NOT_EXIST = "Quy Phu huynh chua dang ky DV So lien lac dien tu. Chi tiet LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_CANCEL_MULTI_PUPIL_VT = "Quy Phu huynh dang su dung DV So lien lac dien tu cho nhieu hoc sinh. De {0}, thuc hien tuong tu cho cac HS con lai. Phi gui tin den 8062: 500d/tin. LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_CANCEL_MULTI_PUPIL_OTHER = "Quy Phu huynh dang su dung DV So lien lac dien tu cho nhieu hoc sinh. Vui long lien he nha truong hoac goi 18008000 nhanh 1 (mien phi) de duoc huong dan";

        public const string SMS_CANCEL_SUC_SUB = "Tai khoan {0} tai http://smsedu.smas.vn da bi khoa. ";

        public const string SMS_CANCEL_SUC = "Quy Phu huynh da huy thanh cong DV So lien lac dien tu cua HS {0}. {1}De dang ky lai, lien he 18008000 nhanh 1 (mien phi)";

        public const string SMS_REG_NOT_ENOUGH_MONEY = "Tai khoan cua Quy Phu huynh khong du de {0} DV So lien lac dien tu. Phi DV: {1}d/{2}. Vui long nap the de kich hoat DV thanh cong";

        public const string SMS_REG_NOT_VALID_CARD = "Ma the cao khong hop le hoac da duoc su dung. Quy Phu huynh vui long kiem tra va dang ky lai";

        public const string SMS_REG_NOT_ENOUGH_MONEY_CARD = "Menh gia the cao khong dung {0}d de {1} DV So lien lac dien tu. Quy Phu huynh vui long kiem tra va dang ky lai";

        public const string SMS_REG_NOT_SP2_NOT_CN = "Quy phu huynh khong the dang ky goi ca nam cho thue bao ngoai mang khi da ket thuc hoc ky I hoac dang su dung DV So lien lac dien tu. Chi tiet LH 18008000 nhanh 1 (mien phi)";

        public const string SMS_HD_SUC = "DV So lien lac dien tu giup Phu huynh cap nhat ket qua hoc tap cua con em minh tai truong. Truy cap http://smsedu.smas.vn de tra cuu ma hoc sinh. De dang ky DV, lien he 18008000 nhanh 1 (mien phi)";

        public const string PRICE_190018098_VT = "100";

        public const string PRICE_190018098_OTHER = "1000";
        /// <summary>
        /// Charge tien thanh cong
        /// </summary>
        public const int VCGW_CODE_SUC = 0;

        /// <summary>
        /// Tai khoan khong du tien
        /// </summary>
        public const int VCGW_CODE_NOT_ENOUGH_MONEY = 401;

        /// <summary>
        /// VCGW loi chung
        /// </summary>
        public const int VCGW_CODE_ERROR_GENERAL = 440;

        /// <summary>
        /// VCGW loi format
        /// </summary>
        public const int VCGW_CODE_ERROR_FORMAT = 201;

        /// <summary>
        /// VCGW loi VAS
        /// </summary>
        public const int VCGW_CODE_ERROR_VAS = 202;

        /// <summary>
        /// VCGW loi exception, se bat o phia ung dung de sau do co co che dong bo doi soat sau
        /// Loi nay co the do timeout ws, hoac cac ly do khac, tuy nhien ko chac chan duoc la
        /// da charge tien thanh cong hay chua
        /// </summary>
        public const int VCGW_CODE_EXCEPTION = -1;

        #endregion

        #region SMS Registration Const

        /// <summary>
        /// Goi SLLDT - AnhVD9 - Ngung goi SLLDT - Doi dang goi SMS90
        /// </summary>
        public const string PACKAGECONTRACT_CODE_SLLDT = "SMS90";

        /// <summary>
        /// Goi SP2
        /// </summary>
        public const string PACKAGECONTRACT_CODE_SP2 = "SP2";

        /// <summary>
        /// lenh goc
        /// </summary>
        public const string COMMANT_CODE_ORG = "SLL";

        /// <summary>
        /// Goi SLL1
        /// </summary>
        public const string PACKAGECONTRACT_CODE_SLL1 = "SLL1";
        /// <summary>
        /// Goi SLL2
        /// </summary>
        public const string PACKAGECONTRACT_CODE_SLL2 = "SLL2";
        /// <summary>
        /// Goi SLL3
        /// </summary>
        public const string PACKAGECONTRACT_CODE_SLL3 = "SLL3";

        /// <summary>
        /// Goi GH
        /// </summary>
        public const string PACKAGECONTRACT_CODE_GH = "GH";

        /// <summary>
        /// Goi Huy
        /// </summary>
        public const string PACKAGECONTRACT_CODE_HUY = "HUY";

        /// <summary>
        /// Goi Huy
        /// </summary>
        public const string PACKAGECONTRACT_CODE_HD = "HD";

        /// <summary>
        /// SMS
        /// </summary>
        public const int REGISTRATION_TYPE_SMS = 2;

        /// <summary>
        /// Truc tiep
        /// </summary>
        public const int REGISTRATION_TYPE_LIVE = 1;

        /// <summary>
        /// Dang ky goi SP2
        /// </summary>
        public const int SERVICE_TYPE_SP2 = 1;
        #endregion

        #region  ID cac thang trong nam
        public const int MonthID1 = 1;
        public const int MonthID2 = 2;
        public const int MonthID3 = 3;
        public const int MonthID4 = 4;
        public const int MonthID5 = 5;
        public const int MonthID6 = 6;
        public const int MonthID7 = 7;
        public const int MonthID8 = 8;
        public const int MonthID9 = 9;
        public const int MonthID10 = 10;

        /// <summary>
        /// ket qua hoc ky
        /// </summary>
        public const int MonthID11 = 11;
        public const int MonthID12 = 12;

        /// <summary>
        /// Ket qua hoc ky VNEN
        /// </summary>
        public const int MonthID_EndSemester1 = 15;
        public const int MonthID_EndSemester2 = 16;
        #endregion

        public const int REGISTRATION_TYPE_BY_SCHOOL = 1;
        public const int REGISTRATION_TYPE_BY_SMS = 2;

        /// <summary>
        /// da dang ky sms
        /// </summary>
        public const short STATUS_REGISTED = 1;
        /// <summary>
        /// chua dang ky
        /// </summary>
        public const short STATUS_NOT_REGISTED = 2;
        /// <summary>
        /// Tam ngung su dung
        /// </summary>
        public const short STATUS_STOP_USING = 3;
        /// <summary>
        /// Da huy
        /// </summary>
        public const short STATUS_DELETE_CONTRACT_DETAIL = 4;

        /// <summary>
        /// Nợ cước
        /// </summary>
        public const short STATUS_UNPAID = 5;
        /// <summary>
        /// Nợ cước quá hạn
        /// </summary>
        public const short STATUS_OVERDUE_UNPAID = 6;

        public const short STATUS_IN_ACTIVE = 7;

        /// <summary>
        /// gui tin nhan cho truong
        /// </summary>
        public const int SENT_SMS_SCHOOL = 1;

        public const string EXTENSION_MOBILE = "ExtensionMobile";


        public const int SUBSCRIBER_TYPE_SMS = 0;
        public const int SUBSCRIBER_TYPE_EMAIL = 1;

        public const string LIST_SPARENT = "listsparent";

        #region Các loại thay đổi thuê bao
        public const short SMSPARENT_CONTRACT_CHANGETYPE_SERVICE_PACKAGE = 1;
        public const short SMSPARENT_CONTRACT_CHANGETYPE_SUBSCRIBER = 2;
        #endregion

        #region Phiên bản
        public const byte PRODUCT_BASIC_VERSION = 1;
        public const short PRODUCT_STANDARD_VERSION = 2;
        public const short PRODUCT_FULL_VERSION = 3;
        #endregion

        #region config const parameters contact group
        // Đối tượng
        public const int WORKGROUP_EMPLOYEE = 1;
        public const int WORKGROUP_TEACHER = 2;
        public const int WORKGROUP_MANAGER = 3;

        // Đoàn thể
        public const int UNIONTYPE_MEMBER_OF_TRADE_UNION = 1;
        public const int UNIONTYPE_MEMBER_OF_COMMUNIST_PARTY = 2;
        public const int UNIONTYPE_OTHER = 3;

        // Giới tính
        public const int GENDER_MALE = 1;
        public const int GENDER_FEMALE = 2;

        // Mạng
        public const int TELECOMMUNICATIONS_NETWORK_VIETTEL = 1;
        public const int TELECOMMUNICATIONS_NETWORK_VINAPHONE = 2;
        public const int TELECOMMUNICATIONS_NETWORK_MOBIFONE = 3;
        public const int TELECOMMUNICATIONS_NETWORK_OTHERS = 4;
        #endregion

        #region Viettel Study Package
        public const string PACKAGE_SMSEDU_VTS1 = "EDU_VTS1";
        public const string PACKAGE_SMSEDU_VTS3 = "EDU_VTS3";
        #endregion

        /// <summary>
        /// Tin nhan khong dau
        /// </summary>
        public const byte SMS_CONTENT_TYPE_NOT_SIGN = 0;
        public const byte SMS_CONTENT_TYPE_UNICODE = 1;

        /// <summary>
        /// Goi cuoc mien phi
        /// </summary>
        public const string SMS_FREE = "SMSFree";

        public const int STATUS_REGISSMSFREE_PENDING_APROVE = 1;//cho duyet
        public const int STATUS_REGISSMSFREE_ISAPROVE = 2;//da duyet
        public const int STATUS_REGISSMSFREE_AGAIN = 3;// dang ky lai
        public const int STATUS_REGISSMSFREE_NOTAPROVE = 4;//tu choi duyet

        /// <summary>
        /// Chờ phê duyệt
        /// </summary>
        public const int STATUS_WAIT_FOR_APPROVAL = 1;

        /// <summary>
        /// Đang hiệu lực
        /// </summary>
        public const int STATUS_IS_ACTIVE = 2;

        /// <summary>
        /// Tạm khóa
        /// </summary>
        public const int STATUS_IS_LOCKED = 3;

        /// <summary>
        /// Đã hủy
        /// </summary>
        public const int STATUS_IS_CANCELED = 4;

        /// <summary>
        /// Chờ gia hạn
        /// </summary>
        public const int STATUS_WAIT_FOR_EXTENDING = 5;

        /// <summary>
        /// Hết hạn
        /// </summary>
        public const int STATUS_IS_EXPIRED = 6;

        public const string UNICODE_MESSAGE_NOTES = "(67 ký tự/SMS. Chỉ gửi được tin nhắn có dấu cho các mạng Viettel, Vina, Mobi.)";

        public const string BRANDNAME_DEFAULT = "SMAS";

        public const int TIMER_TRANSACTION_SENT_DETAIL = 1;
        public const int TIMER_TRANSACTION_CONTRACT_IN_CLASS = 2;


        /// <summary>
        /// So ngay cho phep No cuoc
        /// </summary>
        public const string DEFERRED_PAYMENT_DAYS_KEY = "DeferredPaymentDays";

        #region contactGroup.cs
        public const string BUSINESS_CONTACTGROUP_DATE_FORMAT_DD_MM_YYYY = "ddMMyyyy";
        public const int CONTACT_GROUP_ALL = 0; //Toan truong
        public const string CONTACT_GROUP_ALL_SMSCode = "toantruong"; //Toan truong
        public const string CONTACT_GROUP_ALL_NAME = "Toàn trường";
        public const string HISTORY_SCHOOL_NAME_COMPLETE = "Nhà trường";
        #endregion

        #region SendSMSToParent
        public const string BUSINESS_CUSTOM_FORMAT_DATETIME_ES = "HH:mm dd/MM/yyyy";
        public const string BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY = "dd/MM/yyyy";
        public const string BUSINESS_CUSTOM_FORMAT_DATETIME_MMDDYY = "MM/dd/yyyy";
        public const string BUSINESS_CUSTOM_FORMAT_DATETIME_MMYY = "MM/yyyy";
        public const string DATE_FORMAT_DDMM = "dd/MM";
        public const string DATE_FORMAT_DD = "dd";
        public const string DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";
        public const string DATE_FORMAT_HHmm = "HH:mm";
        public const int TIMER_CONFIG_STATUS_ACTIVE = 1;
        public const int TIMER_CONFIG_STATUS_SENT = 2;
        public const int TIMER_CONFIG_STATUS_CANCEL = 3;
        public const int TIMER_CONFIG_TYPE_SCHOOL = 1;
        public const int TIMER_CONFIG_TYPE_EDU = 2;
        public const int TIMER_CONFIG_TYPE_CLASS = 3;

        // Cac loai goi dich vu
        /// <summary>
        /// Goi SP1
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SP1 = "SP1";
        /// <summary>
        /// Goi SP2
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SP2 = "SP2";
        /// <summary>
        /// Goi SP3
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SP3 = "SP3";
        /// <summary>
        /// Goi SP4
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SP4 = "SP4";
        /// <summary>
        /// Goi SMASKIT
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SMASKIT = "SMASKIT";
        public const string SERVICE_PACKAGE_CODE_SP24 = "SP24";
        public const string SERVICE_PACKAGE_CODE_SP22 = "SP22";
        public const string SERVICE_PACKAGE_CODE_SP33 = "SP33";
        public const string SERVICE_PACKAGE_CODE_SP44 = "SP44";
        /// <summary>
        /// Goi dung thu. Thue bao bi gioi han 5 tin nhan noi mang trong vong 30 ngay.
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_TRIAL = "SMS_DT";
        /// <summary>
        /// Goi So lien lac dien tu
        /// </summary>
        public const string SERVICE_PACKAGE_CODE_SLLDT = "SLLDT";
        #endregion

        #region excel file type
        public const string EXCEL_TYPE_XLS = ".XLS";

        public const string EXCEL_TYPE_XLSX = ".XLSX";
        #endregion

        #region PAGESIZE
        public const int PAGESIZE = 20;
        #endregion

        #region qui dinh mon nhan xet va tinh diem
        public const int ISCOMMENTING_TYPE_MARK = 0;

        public const int ISCOMMENTING_TYPE_JUDGE = 1;

        public const int ISCOMMENTING_TYPE_MARK_JUDGE = 2;
        #endregion

        #region LastDigitSchoolID
        public const int LAST_DIGIT_SCHOOL = 20;


        #endregion

        #region Cach hien thi ten trong tin nhan
        public const int NAME_DISPLAY_FULLNAME = 1;
        public const int NAME_DISPLAY_ACRONYM = 2;
        public const int NAME_DISPLAY_NAME = 3;
        #endregion

        public const int SERVICE_PACKAGE_APPLY_TYPE_SCHOOL = 1;
        public const int SERVICE_PACKAGE_APPLY_TYPE_PROVINCE = 2;

        #region LogFilter Constants (for write log Filter)
        public const string UTILITY_OLD_LOG_JSON = "oldJsonObject";
        public const string UTILITY_NEW_LOG_JSON = "newJsonObject";
        public const string UTILITY_OLD_OBJECT_ID = "objectID";
        public const string UTILITY_OLD_DESCRIPTION = "description";
        #endregion
    }
}