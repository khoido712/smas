﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SMAS.DAL.Repository;
using SMAS.Business.Business;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.DI;
using Autofac.Integration.Mvc;
using Autofac;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.VTUtils.Utils;
using SMAS.Web.Controllers;
using log4net;
using SMAS.Web.Constants;
using SMAS.Models.Models;
using System.Configuration;
using System.Web.Optimization;
using System.Web.Configuration;
//using SMAS.Web.Areas.PriorityTypeArea.Models;



namespace SMAS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogonAuthorize());
            filters.Add(new AuthorizeActionFilterAttribute());
            filters.Add(new ActionCheckDataFilter());
            filters.Add(new BusinessExceptionHandleAttribute());
            filters.Add(new CustomErrorHandlerAttribute());

        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Error", // Route name
                "Error/{action}/{id}", // URL with parameters
                new { controller = "Error", action = "HttpError404", id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
               "NotFound",
               "{*url}",
               new { Controller = "Error", Action = "Http404" }
            );

        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            #region layout
            bundles.Add(new ScriptBundle("~/bundles/scriptlayoutTop").Include(
                "~/Scripts/JScriptTable.js",
                "~/Scripts/jquery-1.5.1.min.js",
                "~/Scripts/jquery-ui-1.8.11.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/MicrosoftAjax.js",
                "~/Scripts/MicrosoftMvcAjax.js",
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/2012.1.214/jquery-1.7.1.min.js",
                "~/Scripts/1.8.2/jquery.min.js",
                "~/Scripts/jquery.jsend.min.js",
                "~/Scripts/base64.js",
                "~/Content/scripts/jquery.cookie.js",
                "~/Content/scripts/jquery-ui-1.8.24.custom.min.js",
                "~/Scripts/jquery.tablednd.js",
                "~/Content/jScrollPane/jquery.mousewheel.js",
                "~/Content/jScrollPane/jScrollPane.js",
                "~/Content/fancybox/jquery.fancybox.js",
                "~/Content/Scripts/main.js",
                "~/Scripts/SMAS/extensions/extensionMethods.js",
                "~/Scripts/SMAS/smas.js",
                "~/Scripts/SMAS/SessionChange.js",
                "~/Scripts/SMAS/Utils/arrUtils.js",
                "~/Scripts/SMAS/Utils/byteUtils.js",
                "~/Scripts/SMAS/Utils/controlUtils.js",
                "~/Scripts/SMAS/Utils/dateUtils.js",
                "~/Scripts/SMAS/Utils/formUtils.js",
                "~/Scripts/SMAS/Utils/jsonUtils.js",
                "~/Scripts/SMAS/Utils/numberUtils.js",
                "~/Scripts/SMAS/Utils/stringUtils.js",
                "~/Scripts/SMAS/Utils/iFrameUtils.js",
                "~/Scripts/SMAS/Utils/popupUtils.js",
                "~/Scripts/SMAS/Utils/gridExtension.js",
                "~/Scripts/moment.js",
                "~/Content/plugins/d3.min.js",
                "~/Content/plugins/nv.d3.min.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/scriptlayoutBottom").Include(
                "~/Content/scripts/jquery.cookie.js",
                "~/Content/scripts/jquery-ui-1.8.24.custom.min.js",
                "~/Scripts/customSelect.jquery.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/jquery-ui-1.8.11.min.js",
                "~/Content/plugins/ui.dropdownchecklist-1.4-min.js",
                "~/Scripts/SMAS/Utils/validateUtils.js",
                "~/Scripts/SMAS/extensions/extensionjQuery.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
            bundles.Add(new Bundle("~/bundles/csslayout").Include(
                "~/Content/main.min.css",
                "~/Content/2012.1.214/telerik.common.min.css",
                "~/Content/2012.1.214/telerik.Vista.min.css",
                "~/Content/Site.css",
                "~/Content/smas.css",
                "~/Content/vt_custom.css",
                "~/Content/common.css",
                "~/Content/pages.css",
                "~/Content/themes/theme-1.css",
                "~/Content/themes/theme-2.css",
                "~/Content/plugins/nv.d3.min.css"
                ));

            bundles.Add(new Bundle("~/bundles/fontawesomecss").IncludeDirectory("~/Content/font-awesome-4.6.3", "*.css", true));
            #endregion

            #region SMS_SCHOOL
            bundles.Add(new ScriptBundle("~/bundles/SchoolInfoConfig").Include("~/Areas/SMSEduArea/Scripts/SchoolInfoConfig/Index.js"));
           
            bundles.Add(new Bundle("~/bundles/SchoolInfoConfigcss").Include("~/Areas/SMSEduArea/Content/SchoolInfoConfig/Index.css"));

            bundles.Add(new ScriptBundle("~/bundles/SMSConfig").Include("~/Areas/SMSEduArea/Scripts/SMSConfig/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/ContactGroupConfig").Include("~/Areas/SMSEduArea/Scripts/ContactGroupConfig/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SendSMSToTeacher").Include("~/Areas/SMSEduArea/Scripts/SendSMSToTeacher/Index.js"));

            bundles.Add(new Bundle("~/bundles/DeclarePackagecss").Include("~/Areas/SMSEduArea/Content/DeclarePackage/Index.css"));

            bundles.Add(new ScriptBundle("~/bundles/SMSParentManageContract").Include("~/Areas/SMSEduArea/Scripts/SMSParentManageContract/Index.js"));

            bundles.Add(new Bundle("~/bundles/PromotionProgramcss").Include("~/Areas/SMSEduArea/Content/PromotionProgram/Index.css"));

            bundles.Add(new ScriptBundle("~/bundles/SendSMSToSchool").Include("~/Areas/SMSEduArea/Scripts/SendSMSToSchool/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/BrandnameRegistration").Include("~/Areas/SMSEduArea/Scripts/BrandnameRegistration/Index.js"));

            bundles.Add(new Bundle("~/bundles/BrandnameRegistrationCss").Include("~/Areas/SMSEduArea/Content/BrandnameRegistration/Index.css"));

            bundles.Add(new Bundle("~/bundles/SMSFreeRegistrationCss").Include("~/Areas/SMSEduArea/Content/SMSFreeRegistration/Index.css"));

            bundles.Add(new ScriptBundle("~/bundles/SMSFreeRegistration").Include("~/Areas/SMSEduArea/Scripts/SMSFreeRegistration/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SendSMSToSupervisingDept").Include("~/Areas/SMSEduArea/Scripts/SendSMSToSupervisingDept/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Payment").Include("~/Areas/SMSEduArea/Scripts/Payment/Index.js"));
            bundles.Add(new Bundle("~/bundles/Paymentcss").Include("~/Areas/SMSEduArea/Content/Payment/Index.css"));

            bundles.Add(new ScriptBundle("~/bundles/SendSMSCommentOfPupil").Include("~/Areas/SMSEduArea/Scripts/SendSMSCommentOfPupil/Index.js"));
            #endregion
        }

        //Namta+ Redirect neu duong dan khong hop le
        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext ctx = HttpContext.Current;
            Exception ex = ctx.Server.GetLastError();
            string smasUrl = SMAS.Business.Common.UtilsBusiness.UrlSmas;
            if (ex.GetType() == typeof(HttpException))
            {
                HttpException httpEx = (HttpException)ex;
                
                if (httpEx.GetHttpCode() == 404)
                {
                    //ctx.Response.Redirect("/Error/HttpError404");

                    ctx.Response.Redirect(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Error/HttpError404"));
                }
                if (httpEx.GetHttpCode() == 403)
                {
                    //ctx.Response.Redirect("/Error/HttpError403");
                    ctx.Response.Redirect(SMAS.Business.Common.UtilsBusiness.GetFullUrl(smasUrl, "Error/HttpError403"));
                }
            }

        }

        protected void Application_Start()
        {

            EncryptConfig();
            #region DI

            var builder = new ContainerBuilder();

            #region Business
            builder.RegisterType<SMASEntities>().As<SMASEntities>().InstancePerHttpRequest();

            //builder.RegisterType<StatisticTertiaryStartYearBusiness>().As<IStatisticTertiaryStartYearBusiness>().InstancePerDependency();
            //builder.RegisterType<StatisticOfTertiaryStartYearBusiness>().As<IStatisticOfTertiaryStartYearBusiness>().InstancePerDependency();
            builder.RegisterType<OtherEthnicBusiness>().As<IOtherEthnicBusiness>().InstancePerDependency();
            builder.RegisterType<TrainingProgramBusiness>().As<ITrainingProgramBusiness>().InstancePerDependency();
            builder.RegisterType<ClassSpecialtyCatBusiness>().As<IClassSpecialtyCatBusiness>().InstancePerDependency();
            builder.RegisterType<ClassifiedConductByPeriodBusiness>().As<IClassifiedConductByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<InputStatisticsBusiness>().As<IInputStatisticsBusiness>().InstancePerDependency();
            builder.RegisterType<ReportJudgeRecordBusiness>().As<IReportJudgeRecordBusiness>().InstancePerDependency();
            builder.RegisterType<ReportMarkRecordBusiness>().As<IReportMarkRecordBusiness>().InstancePerDependency();
            builder.RegisterType<ReportPupilEmulationBusiness>().As<IReportPupilEmulationBusiness>().InstancePerDependency();
            builder.RegisterType<ReportPupilRankingBusiness>().As<IReportPupilRankingBusiness>().InstancePerDependency();
            builder.RegisterType<ReportPupilRetestRegistrationBusiness>().As<IReportPupilRetestRegistrationBusiness>().InstancePerDependency();
            builder.RegisterType<ReportSummedUpRecordBusiness>().As<IReportSummedUpRecordBusiness>().InstancePerDependency();
            builder.RegisterType<SynthesisClassifiedSubjectByPeriodBusiness>().As<ISynthesisClassifiedSubjectByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<SynthesisSubjectByPeriodBusiness>().As<ISynthesisSubjectByPeriodBusiness>().InstancePerDependency();

            builder.RegisterType<AcademicYearBusiness>().As<IAcademicYearBusiness>().InstancePerDependency();
            builder.RegisterType<AcademicYearOfProvinceBusiness>().As<IAcademicYearOfProvinceBusiness>().InstancePerDependency();
            builder.RegisterType<ActionAuditBusiness>().As<IActionAuditBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityBusiness>().As<IActivityBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityPlanBusiness>().As<IActivityPlanBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityPlanClassBusiness>().As<IActivityPlanClassBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityTypeBusiness>().As<IActivityTypeBusiness>().InstancePerDependency();
            builder.RegisterType<AddMenuForChildrenBusiness>().As<IAddMenuForChildrenBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipClassBusiness>().As<IApprenticeshipClassBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipGroupBusiness>().As<IApprenticeshipGroupBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipSubjectBusiness>().As<IApprenticeshipSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingAbsenceBusiness>().As<IApprenticeshipTrainingAbsenceBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingBusiness>().As<IApprenticeshipTrainingBusiness>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingScheduleBusiness>().As<IApprenticeshipTrainingScheduleBusiness>().InstancePerDependency();
            builder.RegisterType<AreaBusiness>().As<IAreaBusiness>().InstancePerDependency();
            builder.RegisterType<aspnet_ApplicationsBusiness>().As<Iaspnet_ApplicationsBusiness>().InstancePerDependency();
            builder.RegisterType<aspnet_MembershipBusiness>().As<Iaspnet_MembershipBusiness>().InstancePerDependency();
            //builder.RegisterType<aspnet_SchemaVersionsBusiness>().As<Iaspnet_SchemaVersionsBusiness>().InstancePerDependency();
            builder.RegisterType<aspnet_UsersBusiness>().As<Iaspnet_UsersBusiness>().InstancePerDependency();
            builder.RegisterType<AverageMarkBySubjectBusiness>().As<IAverageMarkBySubjectBusiness>().InstancePerDependency();
            builder.RegisterType<CalendarBusiness>().As<ICalendarBusiness>().InstancePerDependency();
            builder.RegisterType<CallDetailRecordBusiness>().As<ICallDetailRecordBusiness>().InstancePerDependency();
            builder.RegisterType<CandidateAbsenceBusiness>().As<ICandidateAbsenceBusiness>().InstancePerDependency();
            builder.RegisterType<CandidateBusiness>().As<ICandidateBusiness>().InstancePerDependency();
            builder.RegisterType<CapacityLevelBusiness>().As<ICapacityLevelBusiness>().InstancePerDependency();
            builder.RegisterType<CapacityStatisticBusiness>().As<ICapacityStatisticBusiness>().InstancePerDependency();
            builder.RegisterType<ClassAssigmentBusiness>().As<IClassAssigmentBusiness>().InstancePerDependency();
            builder.RegisterType<ClassEmulationBusiness>().As<IClassEmulationBusiness>().InstancePerDependency();
            builder.RegisterType<ClassEmulationDetailBusiness>().As<IClassEmulationDetailBusiness>().InstancePerDependency();
            builder.RegisterType<ClassForwardingBusiness>().As<IClassForwardingBusiness>().InstancePerDependency();
            builder.RegisterType<ClassificationCriteriaBusiness>().As<IClassificationCriteriaBusiness>().InstancePerDependency();
            builder.RegisterType<ClassificationCriteriaDetailBusiness>().As<IClassificationCriteriaDetailBusiness>().InstancePerDependency();
            builder.RegisterType<ClassificationLevelHealthBusiness>().As<IClassificationLevelHealthBusiness>().InstancePerDependency();
            builder.RegisterType<ClassifiedCapacityBusiness>().As<IClassifiedCapacityBusiness>().InstancePerDependency();
            builder.RegisterType<ClassifiedCapacityBySubjectBusiness>().As<IClassifiedCapacityBySubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ClassifiedConductBusiness>().As<IClassifiedConductBusiness>().InstancePerDependency();
            builder.RegisterType<ClassMovementBusiness>().As<IClassMovementBusiness>().InstancePerDependency();
            builder.RegisterType<ClassProfileBusiness>().As<IClassProfileBusiness>().InstancePerDependency();
            builder.RegisterType<ClassPropertyCatBusiness>().As<IClassPropertyCatBusiness>().InstancePerDependency();
            builder.RegisterType<ClassSubjectBusiness>().As<IClassSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ClassSupervisorAssignmentBusiness>().As<IClassSupervisorAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<CodeConfigBusiness>().As<ICodeConfigBusiness>().InstancePerDependency();
            builder.RegisterType<CommuneBusiness>().As<ICommuneBusiness>().InstancePerDependency();
            builder.RegisterType<CommunicationNoteReportBusiness>().As<ICommunicationNoteReportBusiness>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkAssignmentBusiness>().As<IConcurrentWorkAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkReplacementBusiness>().As<IConcurrentWorkReplacementBusiness>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkTypeBusiness>().As<IConcurrentWorkTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ConductConfigByCapacityBusiness>().As<IConductConfigByCapacityBusiness>().InstancePerDependency();
            builder.RegisterType<ConductConfigByViolationBusiness>().As<IConductConfigByViolationBusiness>().InstancePerDependency();
            builder.RegisterType<ConductEvaluationDetailBusiness>().As<IConductEvaluationDetailBusiness>().InstancePerDependency();
            builder.RegisterType<ConductLevelBusiness>().As<IConductLevelBusiness>().InstancePerDependency();
            builder.RegisterType<ConductStatisticBusiness>().As<IConductStatisticBusiness>().InstancePerDependency();
            builder.RegisterType<ContractBusiness>().As<IContractBusiness>().InstancePerDependency();
            builder.RegisterType<ContractTypeBusiness>().As<IContractTypeBusiness>().InstancePerDependency();
            builder.RegisterType<DailyDishCostBusiness>().As<IDailyDishCostBusiness>().InstancePerDependency();
            builder.RegisterType<DailyExpenseBusiness>().As<IDailyExpenseBusiness>().InstancePerDependency();
            builder.RegisterType<DailyFoodInspectionBusiness>().As<IDailyFoodInspectionBusiness>().InstancePerDependency();
            builder.RegisterType<DailyMenuBusiness>().As<IDailyMenuBusiness>().InstancePerDependency();
            builder.RegisterType<DailyMenuDetailBusiness>().As<IDailyMenuDetailBusiness>().InstancePerDependency();
            builder.RegisterType<DailyMenuFoodBusiness>().As<IDailyMenuFoodBusiness>().InstancePerDependency();
            builder.RegisterType<DailyOtherServiceInspectionBusiness>().As<IDailyOtherServiceInspectionBusiness>().InstancePerDependency();
            builder.RegisterType<DefaultGroupBusiness>().As<IDefaultGroupBusiness>().InstancePerDependency();
            builder.RegisterType<DefaultGroupMenuBusiness>().As<IDefaultGroupMenuBusiness>().InstancePerDependency();
            builder.RegisterType<DentalTestBusiness>().As<IDentalTestBusiness>().InstancePerDependency();
            builder.RegisterType<DetachableHeadBagBusiness>().As<IDetachableHeadBagBusiness>().InstancePerDependency();
            builder.RegisterType<DetachableHeadMappingBusiness>().As<IDetachableHeadMappingBusiness>().InstancePerDependency();
            builder.RegisterType<DetailClassifiedBySubjectBusiness>().As<IDetailClassifiedBySubjectBusiness>().InstancePerDependency();
            builder.RegisterType<DevelopmentOfChildrenBusiness>().As<IDevelopmentOfChildrenBusiness>().InstancePerDependency();
            builder.RegisterType<DevelopmentOfChildrenReportBusiness>().As<IDevelopmentOfChildrenReportBusiness>().InstancePerDependency();
            builder.RegisterType<DisabledTypeBusiness>().As<IDisabledTypeBusiness>().InstancePerDependency();
            builder.RegisterType<DisciplineTypeBusiness>().As<IDisciplineTypeBusiness>().InstancePerDependency();
            builder.RegisterType<DiseasesTypeBusiness>().As<IDiseasesTypeBusiness>().InstancePerDependency();
            builder.RegisterType<DishCatBusiness>().As<IDishCatBusiness>().InstancePerDependency();
            builder.RegisterType<DishDetailBusiness>().As<IDishDetailBusiness>().InstancePerDependency();
            builder.RegisterType<DishInspectionBusiness>().As<IDishInspectionBusiness>().InstancePerDependency();
            builder.RegisterType<DistrictBusiness>().As<IDistrictBusiness>().InstancePerDependency();
            builder.RegisterType<EatingGroupBusiness>().As<IEatingGroupBusiness>().InstancePerDependency();
            builder.RegisterType<EatingGroupClassBusiness>().As<IEatingGroupClassBusiness>().InstancePerDependency();
            builder.RegisterType<EducationalManagementGradeBusiness>().As<IEducationalManagementGradeBusiness>().InstancePerDependency();
            builder.RegisterType<EducationLevelBusiness>().As<IEducationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<EducationProgramBusiness>().As<IEducationProgramBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeBusiness>().As<IEmployeeBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeHistoryStatusBusiness>().As<IEmployeeHistoryStatusBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeePraiseDisciplineBusiness>().As<IEmployeePraiseDisciplineBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeQualificationBusiness>().As<IEmployeeQualificationBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeSalaryBusiness>().As<IEmployeeSalaryBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeScaleBusiness>().As<IEmployeeScaleBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkingHistoryBusiness>().As<IEmployeeWorkingHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkMovementBusiness>().As<IEmployeeWorkMovementBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkTypeBusiness>().As<IEmployeeWorkTypeBusiness>().InstancePerDependency();
            builder.RegisterType<EmulationCriteriaBusiness>().As<IEmulationCriteriaBusiness>().InstancePerDependency();
            builder.RegisterType<EmulationTitleBusiness>().As<IEmulationTitleBusiness>().InstancePerDependency();
            builder.RegisterType<EnergyDistributionBusiness>().As<IEnergyDistributionBusiness>().InstancePerDependency();
            builder.RegisterType<ENTTestBusiness>().As<IENTTestBusiness>().InstancePerDependency();
            builder.RegisterType<EthnicBusiness>().As<IEthnicBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationConfigBusiness>().As<IEvaluationConfigBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationDevelopmentBusiness>().As<IEvaluationDevelopmentBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationDevelopmentGroupBusiness>().As<IEvaluationDevelopmentGroupBusiness>().InstancePerDependency();
            builder.RegisterType<ExaminationBusiness>().As<IExaminationBusiness>().InstancePerDependency();
            builder.RegisterType<ExaminationRoomBusiness>().As<IExaminationRoomBusiness>().InstancePerDependency();
            builder.RegisterType<ExaminationSubjectBusiness>().As<IExaminationSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ExamViolationTypeBusiness>().As<IExamViolationTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ExemptedObjectTypeBusiness>().As<IExemptedObjectTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ExemptedSubjectBusiness>().As<IExemptedSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ExperienceTypeBusiness>().As<IExperienceTypeBusiness>().InstancePerDependency();
            builder.RegisterType<EyeTestBusiness>().As<IEyeTestBusiness>().InstancePerDependency();
            builder.RegisterType<FamilyTypeBusiness>().As<IFamilyTypeBusiness>().InstancePerDependency();
            builder.RegisterType<FaultCriteriaBusiness>().As<IFaultCriteriaBusiness>().InstancePerDependency();
            builder.RegisterType<FaultGroupBusiness>().As<IFaultGroupBusiness>().InstancePerDependency();
            builder.RegisterType<FlowSituationBusiness>().As<IFlowSituationBusiness>().InstancePerDependency();
            builder.RegisterType<FoodCatBusiness>().As<IFoodCatBusiness>().InstancePerDependency();
            builder.RegisterType<FoodGroupBusiness>().As<IFoodGroupBusiness>().InstancePerDependency();
            builder.RegisterType<FoodHabitOfChildrenBusiness>().As<IFoodHabitOfChildrenBusiness>().InstancePerDependency();
            builder.RegisterType<FoodMineralBusiness>().As<IFoodMineralBusiness>().InstancePerDependency();
            builder.RegisterType<FoodPackingBusiness>().As<IFoodPackingBusiness>().InstancePerDependency();
            builder.RegisterType<ForeignLanguageGradeBusiness>().As<IForeignLanguageGradeBusiness>().InstancePerDependency();
            builder.RegisterType<GoodChildrenTicketBusiness>().As<IGoodChildrenTicketBusiness>().InstancePerDependency();
            builder.RegisterType<GraduationLevelBusiness>().As<IGraduationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<GroupCatBusiness>().As<IGroupCatBusiness>().InstancePerDependency();
            builder.RegisterType<GroupMenuBusiness>().As<IGroupMenuBusiness>().InstancePerDependency();
            builder.RegisterType<HabitDetailBusiness>().As<IHabitDetailBusiness>().InstancePerDependency();
            builder.RegisterType<HabitGroupBusiness>().As<IHabitGroupBusiness>().InstancePerDependency();
            builder.RegisterType<HabitOfChildrenBusiness>().As<IHabitOfChildrenBusiness>().InstancePerDependency();
            builder.RegisterType<HeadTeacherSubstitutionBusiness>().As<IHeadTeacherSubstitutionBusiness>().InstancePerDependency();
            builder.RegisterType<HealthPeriodBusiness>().As<IHealthPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<HealthTestReportBusiness>().As<IHealthTestReportBusiness>().InstancePerDependency();
            builder.RegisterType<HistorySMSBusiness>().As<IHistorySMSBusiness>().InstancePerDependency();
            builder.RegisterType<HonourAchivementBusiness>().As<IHonourAchivementBusiness>().InstancePerDependency();
            builder.RegisterType<HonourAchivementTypeBusiness>().As<IHonourAchivementTypeBusiness>().InstancePerDependency();
            builder.RegisterType<InfectiousDiseasBusiness>().As<IInfectiousDiseasBusiness>().InstancePerDependency();
            builder.RegisterType<InvigilatorAssignmentBusiness>().As<IInvigilatorAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<InvigilatorBusiness>().As<IInvigilatorBusiness>().InstancePerDependency();
            builder.RegisterType<ITQualificationLevelBusiness>().As<IITQualificationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<JudgeRecordBusiness>().As<IJudgeRecordBusiness>().InstancePerDependency();
            builder.RegisterType<JudgeRecordHistoryBusiness>().As<IJudgeRecordHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<LearningResultReportBusiness>().As<ILearningResultReportBusiness>().InstancePerDependency();
            builder.RegisterType<LeavingReasonBusiness>().As<ILeavingReasonBusiness>().InstancePerDependency();
            builder.RegisterType<LockedMarkDetailBusiness>().As<ILockedMarkDetailBusiness>().InstancePerDependency();
            builder.RegisterType<LookupInfoReportBusiness>().As<ILookupInfoReportBusiness>().InstancePerDependency();
            builder.RegisterType<MarkInputSituationBusiness>().As<IMarkInputSituationBusiness>().InstancePerDependency();
            builder.RegisterType<MarkOfSemesterBusiness>().As<IMarkOfSemesterBusiness>().InstancePerDependency();
            builder.RegisterType<MarkOfSemesterByJudgeSubjectBusiness>().As<IMarkOfSemesterByJudgeSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<MarkOfSemesterBySubjectBusiness>().As<IMarkOfSemesterBySubjectBusiness>().InstancePerDependency();
            builder.RegisterType<MarkRecordBusiness>().As<IMarkRecordBusiness>().InstancePerDependency();
            builder.RegisterType<MarkRecordHistoryBusiness>().As<IMarkRecordHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<MarkStatisticBusiness>().As<IMarkStatisticBusiness>().InstancePerDependency();
            builder.RegisterType<MarkTypeBusiness>().As<IMarkTypeBusiness>().InstancePerDependency();
            builder.RegisterType<MasterBookBusiness>().As<IMasterBookBusiness>().InstancePerDependency();
            builder.RegisterType<MealCatBusiness>().As<IMealCatBusiness>().InstancePerDependency();
            builder.RegisterType<MenuBusiness>().As<IMenuBusiness>().InstancePerDependency();
            builder.RegisterType<MenuPermissionBusiness>().As<IMenuPermissionBusiness>().InstancePerDependency();
            builder.RegisterType<MessageBusiness>().As<IMessageBusiness>().InstancePerDependency();
            builder.RegisterType<MinenalCatBusiness>().As<IMinenalCatBusiness>().InstancePerDependency();
            builder.RegisterType<MonitoringBookBusiness>().As<IMonitoringBookBusiness>().InstancePerDependency();
            builder.RegisterType<MonthlyBalanceBusiness>().As<IMonthlyBalanceBusiness>().InstancePerDependency();
            builder.RegisterType<MonthlyLockBusiness>().As<IMonthlyLockBusiness>().InstancePerDependency();
            builder.RegisterType<MovementAcceptanceBusiness>().As<IMovementAcceptanceBusiness>().InstancePerDependency();
            builder.RegisterType<NotEatingChildrenBusiness>().As<INotEatingChildrenBusiness>().InstancePerDependency();
            builder.RegisterType<NutritionalNormBusiness>().As<INutritionalNormBusiness>().InstancePerDependency();
            builder.RegisterType<NutritionalNormMineralBusiness>().As<INutritionalNormMineralBusiness>().InstancePerDependency();
            builder.RegisterType<OtherServiceBusiness>().As<IOtherServiceBusiness>().InstancePerDependency();
            builder.RegisterType<OverallTestBusiness>().As<IOverallTestBusiness>().InstancePerDependency();
            builder.RegisterType<PackageBusiness>().As<IPackageBusiness>().InstancePerDependency();
            builder.RegisterType<ParticularPupilBusiness>().As<IParticularPupilBusiness>().InstancePerDependency();
            builder.RegisterType<ParticularPupilCharacteristicBusiness>().As<IParticularPupilCharacteristicBusiness>().InstancePerDependency();
            builder.RegisterType<ParticularPupilTreatmentBusiness>().As<IParticularPupilTreatmentBusiness>().InstancePerDependency();
            builder.RegisterType<PeriodDeclarationBusiness>().As<IPeriodDeclarationBusiness>().InstancePerDependency();
            builder.RegisterType<PhysicalTestBusiness>().As<IPhysicalTestBusiness>().InstancePerDependency();
            builder.RegisterType<ReportMarkSubjectByTeacherBusiness>().As<IReportMarkSubjectByTeacherBusiness>().InstancePerDependency();
            builder.RegisterType<PointReportBusiness>().As<IPointReportBusiness>().InstancePerDependency();
            builder.RegisterType<PolicyRegimeBusiness>().As<IPolicyRegimeBusiness>().InstancePerDependency();
            builder.RegisterType<PolicyTargetBusiness>().As<IPolicyTargetBusiness>().InstancePerDependency();
            builder.RegisterType<PoliticalGradeBusiness>().As<IPoliticalGradeBusiness>().InstancePerDependency();
            builder.RegisterType<PraiseTypeBusiness>().As<IPraiseTypeBusiness>().InstancePerDependency();
            builder.RegisterType<PriorityTypeBusiness>().As<IPriorityTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ProcessedReportBusiness>().As<IProcessedReportBusiness>().InstancePerDependency();
            builder.RegisterType<PropertyOfClassBusiness>().As<IPropertyOfClassBusiness>().InstancePerDependency();
            builder.RegisterType<PropertyOfSchoolBusiness>().As<IPropertyOfSchoolBusiness>().InstancePerDependency();
            builder.RegisterType<ProvinceBusiness>().As<IProvinceBusiness>().InstancePerDependency();
            builder.RegisterType<ProvinceSubjectBusiness>().As<IProvinceSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<PupilAbsenceBusiness>().As<IPupilAbsenceBusiness>().InstancePerDependency();
            builder.RegisterType<PupilDisciplineBusiness>().As<IPupilDisciplineBusiness>().InstancePerDependency();
            builder.RegisterType<PupilEmulationBusiness>().As<IPupilEmulationBusiness>().InstancePerDependency();
            builder.RegisterType<PupilFaultBusiness>().As<IPupilFaultBusiness>().InstancePerDependency();
            builder.RegisterType<PupilGraduationBusiness>().As<IPupilGraduationBusiness>().InstancePerDependency();
            builder.RegisterType<PupilLeavingOffBusiness>().As<IPupilLeavingOffBusiness>().InstancePerDependency();
            builder.RegisterType<PupilOfClassBusiness>().As<IPupilOfClassBusiness>().InstancePerDependency();
            builder.RegisterType<PupilOfSchoolBusiness>().As<IPupilOfSchoolBusiness>().InstancePerDependency();
            builder.RegisterType<PupilPraiseBusiness>().As<IPupilPraiseBusiness>().InstancePerDependency();
            builder.RegisterType<PupilProfileBusiness>().As<IPupilProfileBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRankingBusiness>().As<IPupilRankingBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRepeatedBusiness>().As<IPupilRepeatedBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRetestBusiness>().As<IPupilRetestBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRetestRegistrationBusiness>().As<IPupilRetestRegistrationBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRewardReportBusiness>().As<IPupilRewardReportBusiness>().InstancePerDependency();
            builder.RegisterType<QualificationGradeBusiness>().As<IQualificationGradeBusiness>().InstancePerDependency();
            builder.RegisterType<QualificationLevelBusiness>().As<IQualificationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<QualificationTypeBusiness>().As<IQualificationTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ReligionBusiness>().As<IReligionBusiness>().InstancePerDependency();
            builder.RegisterType<ReportDefinitionBusiness>().As<IReportDefinitionBusiness>().InstancePerDependency();
            builder.RegisterType<ReportEmployeeProfileBusiness>().As<IReportEmployeeProfileBusiness>().InstancePerDependency();
            builder.RegisterType<ReportMarkInputSituationBusiness>().As<IReportMarkInputSituationBusiness>().InstancePerDependency();
            builder.RegisterType<ReportNumberOfPupilByTeacherBusiness>().As<IReportNumberOfPupilByTeacherBusiness>().InstancePerDependency();
            builder.RegisterType<ReportPupilSynthesisBusiness>().As<IReportPupilSynthesisBusiness>().InstancePerDependency();
            builder.RegisterType<ReportResultEndOfYearBusiness>().As<IReportResultEndOfYearBusiness>().InstancePerDependency();
            builder.RegisterType<ReportSituationOfViolationBusiness>().As<IReportSituationOfViolationBusiness>().InstancePerDependency();
            builder.RegisterType<ReportSynthesisCapacityConductByPeriodBusiness>().As<IReportSynthesisCapacityConductByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<ReportTeachingAssignmentBusiness>().As<IReportTeachingAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<RoleBusiness>().As<IRoleBusiness>().InstancePerDependency();
            builder.RegisterType<RoleMenuBusiness>().As<IRoleMenuBusiness>().InstancePerDependency();
            builder.RegisterType<SalaryLevelBusiness>().As<ISalaryLevelBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolCodeConfigBusiness>().As<ISchoolCodeConfigBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolFacultyBusiness>().As<ISchoolFacultyBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolMovementBusiness>().As<ISchoolMovementBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolProfileBusiness>().As<ISchoolProfileBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolPropertyCatBusiness>().As<ISchoolPropertyCatBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolSubjectBusiness>().As<ISchoolSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolSubsidiaryBusiness>().As<ISchoolSubsidiaryBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolTypeBusiness>().As<ISchoolTypeBusiness>().InstancePerDependency();
            builder.RegisterType<SemeterDeclarationBusiness>().As<ISemeterDeclarationBusiness>().InstancePerDependency();
            builder.RegisterType<SendInfoBusiness>().As<ISendInfoBusiness>().InstancePerDependency();
            builder.RegisterType<SendResultBusiness>().As<ISendResultBusiness>().InstancePerDependency();
            builder.RegisterType<ServiceBusiness>().As<IServiceBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentContractBusiness>().As<ISMSParentContractBusiness>().InstancePerDependency();
            builder.RegisterType<SMSTeacherContractBusiness>().As<ISMSTeacherContractBusiness>().InstancePerDependency();
            builder.RegisterType<SpecialityCatBusiness>().As<ISpecialityCatBusiness>().InstancePerDependency();
            builder.RegisterType<SpineTestBusiness>().As<ISpineTestBusiness>().InstancePerDependency();
            builder.RegisterType<StaffPositionBusiness>().As<IStaffPositionBusiness>().InstancePerDependency();
            builder.RegisterType<StateManagementGradeBusiness>().As<IStateManagementGradeBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticOfTertiaryBusiness>().As<IStatisticOfTertiaryBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticsConfigBusiness>().As<IStatisticsConfigBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticsForUnitBusiness>().As<IStatisticsForUnitBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticSubcommitteeOfTertiaryBusiness>().As<IStatisticSubcommitteeOfTertiaryBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticSubjectOfTertiaryBusiness>().As<IStatisticSubjectOfTertiaryBusiness>().InstancePerDependency();
            builder.RegisterType<StudyingJudgementBusiness>().As<IStudyingJudgementBusiness>().InstancePerDependency();
            builder.RegisterType<SubCommitteeBusiness>().As<ISubCommitteeBusiness>().InstancePerDependency();
            builder.RegisterType<SubjectCatBusiness>().As<ISubjectCatBusiness>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordBusiness>().As<ISummedUpRecordBusiness>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordHistoryBusiness>().As<ISummedUpRecordHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordClassBusiness>().As<ISummedUpRecordClassBusiness>().InstancePerDependency();
            builder.RegisterType<SupervisingDeptBusiness>().As<ISupervisingDeptBusiness>().InstancePerDependency();
            builder.RegisterType<SupplierBusiness>().As<ISupplierBusiness>().InstancePerDependency();
            builder.RegisterType<SymbolConfigBusiness>().As<ISymbolConfigBusiness>().InstancePerDependency();
            builder.RegisterType<SynthesisCapacityConductBusiness>().As<ISynthesisCapacityConductBusiness>().InstancePerDependency();
            builder.RegisterType<SynthesisSubjectByEducationLevelBusiness>().As<ISynthesisSubjectByEducationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<sysdiagramBusiness>().As<IsysdiagramBusiness>().InstancePerDependency();
            builder.RegisterType<SystemParameterBusiness>().As<ISystemParameterBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherGradeBusiness>().As<ITeacherGradeBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherGradingBusiness>().As<ITeacherGradingBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherOfFacultyBusiness>().As<ITeacherOfFacultyBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingAssignmentBusiness>().As<ITeachingAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingExperienceBusiness>().As<ITeachingExperienceBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingRegistrationBusiness>().As<ITeachingRegistrationBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingTargetRegistrationBusiness>().As<ITeachingTargetRegistrationBusiness>().InstancePerDependency();
            builder.RegisterType<TrainingLevelBusiness>().As<ITrainingLevelBusiness>().InstancePerDependency();
            builder.RegisterType<TrainingTypeBusiness>().As<ITrainingTypeBusiness>().InstancePerDependency();
            builder.RegisterType<TranscriptOfPrimaryClassBusiness>().As<ITranscriptOfPrimaryClassBusiness>().InstancePerDependency();
            builder.RegisterType<TranscriptsBusiness>().As<ITranscriptsBusiness>().InstancePerDependency();
            builder.RegisterType<TranscriptsByPeriodBusiness>().As<ITranscriptsByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<TranscriptsBySemesterBusiness>().As<ITranscriptsBySemesterBusiness>().InstancePerDependency();
            builder.RegisterType<TypeConfigBusiness>().As<ITypeConfigBusiness>().InstancePerDependency();
            builder.RegisterType<TypeOfDishBusiness>().As<ITypeOfDishBusiness>().InstancePerDependency();
            builder.RegisterType<TypeOfFoodBusiness>().As<ITypeOfFoodBusiness>().InstancePerDependency();
            builder.RegisterType<UserAccountBusiness>().As<IUserAccountBusiness>().InstancePerDependency();
            builder.RegisterType<UserGroupBusiness>().As<IUserGroupBusiness>().InstancePerDependency();
            builder.RegisterType<UserProvinceBusiness>().As<IUserProvinceBusiness>().InstancePerDependency();
            builder.RegisterType<UserRoleBusiness>().As<IUserRoleBusiness>().InstancePerDependency();
            builder.RegisterType<ViolatedCandidateBusiness>().As<IViolatedCandidateBusiness>().InstancePerDependency();
            builder.RegisterType<ViolatedInvigilatorBusiness>().As<IViolatedInvigilatorBusiness>().InstancePerDependency();
            builder.RegisterType<WeeklyMenuBusiness>().As<IWeeklyMenuBusiness>().InstancePerDependency();
            builder.RegisterType<WeeklyMenuDetailBusiness>().As<IWeeklyMenuDetailBusiness>().InstancePerDependency();
            builder.RegisterType<WorkGroupTypeBusiness>().As<IWorkGroupTypeBusiness>().InstancePerDependency();
            builder.RegisterType<WorkTypeBusiness>().As<IWorkTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ReportExamBusiness>().As<IReportExamBusiness>().InstancePerDependency();
            builder.RegisterType<ReportExamResultBusiness>().As<IReportExamResultBusiness>().InstancePerDependency();
            builder.RegisterType<ReportCapacityPupilByTeacherBusiness>().As<IReportCapacityPupilByTeacherBusiness>().InstancePerDependency();
            builder.RegisterType<ReportEmployeeByGraduationLevelBusiness>().As<IReportEmployeeByGraduationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<ReportEmployeeSalaryBusiness>().As<IReportEmployeeSalaryBusiness>().InstancePerDependency();
            builder.RegisterType<AverageMarkByPeriodBusiness>().As<IAverageMarkByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<DetailClassifiedSubjectByPeriodBusiness>().As<IDetailClassifiedSubjectByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<ClassifiedCapacityByPeriodBusiness>().As<IClassifiedCapacityByPeriodBusiness>().InstancePerDependency();
            builder.RegisterType<AlertMessageBusiness>().As<IAlertMessageBusiness>().InstancePerDependency();
            builder.RegisterType<AlertMessageDetailBusiness>().As<IAlertMessageDetailBusiness>().InstancePerDependency();
            /*SCS*/
            builder.RegisterType<SMSTypeBusiness>().As<ISMSTypeBusiness>().InstancePerDependency();
            builder.RegisterType<LocationBusiness>().As<ILocationBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityOfClassBusiness>().As<IActivityOfClassBusiness>().InstancePerDependency();
            builder.RegisterType<ActivityOfPupilBusiness>().As<IActivityOfPupilBusiness>().InstancePerDependency();
            builder.RegisterType<ContactGroupBusiness>().As<IContactGroupBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeContactBusiness>().As<IEmployeeContactBusiness>().InstancePerDependency();
            builder.RegisterType<LogMOBusiness>().As<ILogMOBusiness>().InstancePerDependency();
            builder.RegisterType<MTBusiness>().As<IMTBusiness>().InstancePerDependency();
            //builder.RegisterType<SMSCommunicationBusiness>().As<ISMSCommunicationBusiness>().InstancePerDependency();
            //builder.RegisterType<SMSCommunicationGroupBusiness>().As<ISMSCommunicationGroupBusiness>().InstancePerDependency();
            builder.RegisterType<SMSTypeDetailBusiness>().As<ISMSTypeDetailBusiness>().InstancePerDependency();

            builder.RegisterType<CalendarScheduleBusiness>().As<ICalendarScheduleBusiness>().InstancePerDependency();
            builder.RegisterType<SkillBusiness>().As<ISkillBusiness>().InstancePerDependency();
            builder.RegisterType<SkillTitleBusiness>().As<ISkillTitleBusiness>().InstancePerDependency();
            builder.RegisterType<SkillTitleClassBusiness>().As<ISkillTitleClassBusiness>().InstancePerDependency();
            builder.RegisterType<SkillOfClassBusiness>().As<ISkillOfClassBusiness>().InstancePerDependency();
            builder.RegisterType<SkillOfPupilBusiness>().As<ISkillOfPupilBusiness>().InstancePerDependency();
            builder.RegisterType<MIdmappingBusiness>().As<IMIdmappingBusiness>().InstancePerDependency();
            builder.RegisterType<VillageBusiness>().As<IVillageBusiness>().InstancePerDependency();
            builder.RegisterType<ProcessedCellDataBusiness>().As<IProcessedCellDataBusiness>().InstancePerDependency();
            builder.RegisterType<VJudgeRecordBusiness>().As<IVJudgeRecordBusiness>().InstancePerDependency();
            builder.RegisterType<VMarkRecordBusiness>().As<IVMarkRecordBusiness>().InstancePerDependency();
            builder.RegisterType<VPupilRankingBusiness>().As<IVPupilRankingBusiness>().InstancePerDependency();
            builder.RegisterType<VSummedUpRecordBusiness>().As<IVSummedUpRecordBusiness>().InstancePerDependency();
            builder.RegisterType<LockTrainingBusiness>().As<ILockTrainingBusiness>().InstancePerDependency();
            builder.RegisterType<LockMarkPrimaryBusiness>().As<ILockMarkPrimaryBusiness>().InstancePerDependency();
            builder.RegisterType<ReportPreliminarySendSGDBusiness>().As<IReportPreliminarySendSGDBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticLevelReportBusiness>().As<IStatisticLevelReportBusiness>().InstancePerDependency();
            builder.RegisterType<StatisticLevelConfigBusiness>().As<IStatisticLevelConfigBusiness>().InstancePerDependency();
            //builder.RegisterType<PupilReviewEvaluateBusiness>().As<IPupilReviewEvaluateBusiness>().InstancePerDependency();
            // builder.RegisterType<MarkBookPrimaryConfigBusiness>().As<IMarkBookPrimaryConfigBusiness>().InstancePerDependency();
            builder.RegisterType<MonthCommentsBusiness>().As<IMonthCommentsBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationCommentsBusiness>().As<IEvaluationCommentsBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationCommentsHistoryBusiness>().As<IEvaluationCommentsHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationCriteriaBusiness>().As<IEvaluationCriteriaBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationBusiness>().As<IEvaluationBusiness>().InstancePerDependency();
            builder.RegisterType<SummedEvaluationBusiness>().As<ISummedEvaluationBusiness>().InstancePerDependency();
            builder.RegisterType<SummedEvaluationHistoryBusiness>().As<ISummedEvaluationHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<SummedEndingEvaluationBusiness>().As<ISummedEndingEvaluationBusiness>().InstancePerDependency();
            builder.RegisterType<RewardFinalBusiness>().As<IRewardFinalBusiness>().InstancePerDependency();
            builder.RegisterType<RewardCommentFinalBusiness>().As<IRewardCommentFinalBusiness>().InstancePerDependency();
            builder.RegisterType<RegisterSubjectSpecializeBusiness>().As<IRegisterSubjectSpecializeBusiness>().InstancePerDependency();

            builder.RegisterType<ExaminationsBusiness>().As<IExaminationsBusiness>().InstancePerDependency();
            builder.RegisterType<ExamGroupBusiness>().As<IExamGroupBusiness>().InstancePerDependency();
            builder.RegisterType<ExamSubjectBusiness>().As<IExamSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<ExamPupilBusiness>().As<IExamPupilBusiness>().InstancePerDependency();
            builder.RegisterType<ExamRoomBusiness>().As<IExamRoomBusiness>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryBusiness>().As<IExamSupervisoryBusiness>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryAssignmentBusiness>().As<IExamSupervisoryAssignmentBusiness>().InstancePerDependency();
            builder.RegisterType<ExamBagBusiness>().As<IExamBagBusiness>().InstancePerDependency();
            builder.RegisterType<ExamDetachableBagBusiness>().As<IExamDetachableBagBusiness>().InstancePerDependency();
            builder.RegisterType<ExamPupilViolateBusiness>().As<IExamPupilViolateBusiness>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryViolateBusiness>().As<IExamSupervisoryViolateBusiness>().InstancePerDependency();
            builder.RegisterType<ExamPupilAbsenceBusiness>().As<IExamPupilAbsenceBusiness>().InstancePerDependency();
            builder.RegisterType<ExamInputMarkAssignedBusiness>().As<IExamInputMarkAssignedBusiness>().InstancePerDependency();
            builder.RegisterType<ExamInputMarkBusiness>().As<IExamInputMarkBusiness>().InstancePerDependency();
            builder.RegisterType<ExamCandenceBagBusiness>().As<IExamCandenceBagBusiness>().InstancePerDependency();
            builder.RegisterType<ThreadMarkBusiness>().As<IThreadMarkBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeBreatherBusiness>().As<IEmployeeBreatherBusiness>().InstancePerDependency();
            builder.RegisterType<EducationQualityStatisticBusiness>().As<IEducationQualityStatisticBusiness>().InstancePerDependency();
            builder.RegisterType<ConfigConductRankingBusiness>().As<IConfigConductRankingBusiness>().InstancePerDependency();
            builder.RegisterType<PushNotifyRequestBusiness>().As<IPushNotifyRequestBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExamPupilBusiness>().As<IDOETExamPupilBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExamMarkBusiness>().As<IDOETExamMarkBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExaminationsBusiness>().As<IDOETExaminationsBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExamGroupBusiness>().As<IDOETExamGroupBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExamSubjectBusiness>().As<IDOETExamSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<DOETSubjectBusiness>().As<IDOETSubjectBusiness>().InstancePerDependency();
            builder.RegisterType<DOETExamSupervisoryBusiness>().As<IDOETExamSupervisoryBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherNoteBookMonthBusiness>().As<ITeacherNoteBookMonthBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherNoteBookSemesterBusiness>().As<ITeacherNoteBookSemesterBusiness>().InstancePerDependency();
            builder.RegisterType<ReviewBookPupilBusiness>().As<IReviewBookPupilBusiness>().InstancePerDependency();
            builder.RegisterType<UpdateRewardBusiness>().As<IUpdateRewardBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationBookReportBusiness>().As<IEvaluationBookReportBusiness>().InstancePerDependency();
            builder.RegisterType<TranscriptsVNENReportBusiness>().As<ITranscriptsVNENReportBusiness>().InstancePerDependency();
            builder.RegisterType<TemplateBusiness>().As<ITemplateBusiness>().InstancePerDependency();
            builder.RegisterType<HtmlContentCodeBusiness>().As<IHtmlContentCodeBusiness>().InstancePerDependency();
            builder.RegisterType<HtmlTemplateBusiness>().As<IHtmlTemplateBusiness>().InstancePerDependency();
            builder.RegisterType<IndicatorBusiness>().As<IIndicatorBusiness>().InstancePerDependency();
            builder.RegisterType<IndicatorDataBusiness>().As<IIndicatorDataBusiness>().InstancePerDependency();
            builder.RegisterType<VacationReasonBusiness>().As<IVacationReasonBusiness>().InstancePerDependency();
            builder.RegisterType<ThreadMovedDataBusiness>().As<IThreadMovedDataBusiness>().InstancePerDependency();
            builder.RegisterType<PupilRankingHistoryBusiness>().As<IPupilRankingHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<BookmarkedFunctionBusiness>().As<IBookmarkedFunctionBusiness>().InstancePerDependency();
            builder.RegisterType<WorkFlowUserBusiness>().As<IWorkFlowUserBusiness>().InstancePerDependency();
            builder.RegisterType<WorkFlowBusiness>().As<IWorkFlowBusiness>().InstancePerDependency();
            builder.RegisterType<DistributeProgramBusiness>().As<IDistributeProgramBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingScheduleBusiness>().As<ITeachingScheduleBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolWeekBusiness>().As<ISchoolWeekBusiness>().InstancePerDependency();
            builder.RegisterType<RatedCommentPupilBusiness>().As<IRatedCommentPupilBusiness>().InstancePerDependency();
            builder.RegisterType<RatedCommentPupilHistoryBusiness>().As<IRatedCommentPupilHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<CommentOfPupilBusiness>().As<ICommentOfPupilBusiness>().InstancePerDependency();
            builder.RegisterType<CommentOfPupilHistoryBusiness>().As<ICommentOfPupilHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<LockRatedCommentPupilBusiness>().As<ILockRatedCommentPupilBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationRewardBusiness>().As<IEvaluationRewardBusiness>().InstancePerDependency();
            builder.RegisterType<CollectionCommentsBusiness>().As<ICollectionCommentsBusiness>().InstancePerDependency();
            builder.RegisterType<DiplomaTemplateBusiness>().As<IDiplomaTemplateBusiness>().InstancePerDependency();
            builder.RegisterType<DiplomaImageBusiness>().As<IDiplomaImageBusiness>().InstancePerDependency();
            builder.RegisterType<KPIFunctionBusiness>().As<KPIFunctionBusiness>().InstancePerDependency();
            builder.RegisterType<KPILogBusiness>().As<KPILogBusiness>().InstancePerDependency();
            builder.RegisterType<DeclareEvaluationIndexBusiness>().As<IDeclareEvaluationIndexBusiness>().InstancePerDependency();
            builder.RegisterType<DeclareEvaluationGroupBusiness>().As<IDeclareEvaluationGroupBusiness>().InstancePerDependency();
            builder.RegisterType<PhysicalExaminationBusiness>().As<IPhysicalExaminationBusiness>().InstancePerDependency();
            builder.RegisterType<CommentsOfKindergartenBusiness>().As<ICommentsOfKindergartenBusiness>().InstancePerDependency();
            builder.RegisterType<GrowthEvaluationBusiness>().As<IGrowthEvaluationBusiness>().InstancePerDependency();
            builder.RegisterType<MealCategoryBusiness>().As<IMealCategoryBusiness>().InstancePerDependency();
            builder.RegisterType<WeeklyMealMenuBusiness>().As<IWeeklyMealMenuBusiness>().InstancePerDependency();
            builder.RegisterType<QuickHintBusiness>().As<IQuickHintBusiness>().InstancePerDependency();
            builder.RegisterType<DevelopmentStandardRepository>().As<IDevelopmentStandardRepository>().InstancePerDependency();
            builder.RegisterType<AssignSubjectConfigBusiness>().As<IAssignSubjectConfigBusiness>().InstancePerDependency();
            builder.RegisterType<DistributeProgramSystemBusiness>().As<IDistributeProgramSystemBusiness>().InstancePerDependency();
            builder.RegisterType<TeachingScheduleApprovalBusiness>().As<ITeachingScheduleApprovalBusiness>().InstancePerDependency();
			builder.RegisterType<LockInputSupervisingDeptBusiness>().As<ILockInputSupervisingDeptBusiness>().InstancePerDependency();
            builder.RegisterType<RestoreDataBusiness>().As<IRestoreDataBusiness>().InstancePerDependency();
            builder.RegisterType<RestoreDataDetailBusiness>().As<IRestoreDataDetailBusiness>().InstancePerDependency();
            builder.RegisterType<QiSubjectMappingBusiness>().As<IQiSubjectMappingBusiness>().InstancePerDependency();
            builder.RegisterType<UsingMarkbookCommentBusiness>().As<IUsingMarkbookCommentBusiness>().InstancePerDependency();
            builder.RegisterType<ManagementHealthInsuranceBusiness>().As<IManagementHealthInsuranceBusiness>().InstancePerDependency();
            builder.RegisterType<SurveyBusiness>().As<ISurveyBusiness>().InstancePerDependency();
            builder.RegisterType<ColumnDescriptionBusiness>().As<IColumnDescriptionBusiness>().InstancePerDependency();
            builder.RegisterType<EmployeeEvaluationBusiness>().As<IEmployeeEvaluationBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationFieldBusiness>().As<IEvaluationFieldBusiness>().InstancePerDependency();
            builder.RegisterType<EvaluationLevelBusiness>().As<IEvaluationLevelBusiness>().InstancePerDependency();
            builder.RegisterType<ForeignLanguageCatBusiness>().As<IForeignLanguageCatBusiness>().InstancePerDependency();
            builder.RegisterType<ProfileTemplateBusiness>().As<IProfileTemplateBusiness>().InstancePerDependency();
            builder.RegisterType<GraduationApprovalBusiness>().As<IGraduationApprovalBusiness>().InstancePerDependency();
            builder.RegisterType<VRatedCommentPupilBusiness>().As<IVRatedCommentPupilBusiness>().InstancePerDependency();
            builder.RegisterType<SchoolConfigBusiness>().As<ISchoolConfigBusiness>().InstancePerDependency();
            builder.RegisterType<SMSTypeBusiness>().As<ISMSTypeBusiness>().InstancePerDependency();
            builder.RegisterType<SMSHistoryBusiness>().As<ISMSHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentContractBusiness>().As<ISMSParentContractBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentContractInClassDetailBusiness>().As<ISMSParentContractInClassDetailBusiness>().InstancePerDependency();
            builder.RegisterType<TimerConfigBusiness>().As<ITimerConfigBusiness>().InstancePerDependency();
            builder.RegisterType<CustomSMSTypeBusiness>().As<ICustomSMSTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ContentTypeBusiness>().As<IContentTypeBusiness>().InstancePerDependency();
            builder.RegisterType<CustomSMSTypeBusiness>().As<ICustomSMSTypeBusiness>().InstancePerDependency();
            builder.RegisterType<TeacherContactBusiness>().As<ITeacherContactBusiness>().InstancePerDependency();
            builder.RegisterType<EWalletBusiness>().As<IEWalletBusiness>().InstancePerDependency();
            builder.RegisterType<PromotionBusiness>().As<IPromotionBusiness>().InstancePerDependency();
            builder.RegisterType<PromotionDetailBusiness>().As<IPromotionDetailBusiness>().InstancePerDependency();
            builder.RegisterType<ServicePackageBusiness>().As<IServicePackageBusiness>().InstancePerDependency();
            builder.RegisterType<ServicePackageDeclareBusiness>().As<IServicePackageDeclareBusiness>().InstancePerDependency();
            builder.RegisterType<TimerTransactionBusiness>().As<ITimerTransactionBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentSentDetailBusiness>().As<ISMSParentSentDetailBusiness>().InstancePerDependency();
            builder.RegisterType<ServicePackageSchoolDetailBusiness>().As<IServicePackageSchoolDetailBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentContractDetailBusiness>().As<ISMSParentContractDetailBusiness>().InstancePerDependency();
            builder.RegisterType<SMSParentContractExtraBusiness>().As<ISMSParentContractExtraBusiness>().InstancePerDependency();
            builder.RegisterType<BrandnameProviderBusiness>().As<IBrandnameProviderBusiness>().InstancePerDependency();
            builder.RegisterType<BrandnameRegistrationBusiness>().As<IBrandnameRegistrationBusiness>().InstancePerDependency();
            builder.RegisterType<RegisterSMSFreeBusiness>().As<IRegisterSMSFreeBusiness>().InstancePerDependency();
            builder.RegisterType<AffectedHistoryBusiness>().As<IAffectedHistoryBusiness>().InstancePerDependency();
            builder.RegisterType<TopupBusiness>().As<ITopupBusiness>().InstancePerDependency();
            builder.RegisterType<EWalletTransactionBusiness>().As<IEWalletTransactionBusiness>().InstancePerDependency();
            builder.RegisterType<EwalletTransactionTypeBusiness>().As<IEwalletTransactionTypeBusiness>().InstancePerDependency();
            builder.RegisterType<ReceivedRecordBusiness>().As<IReceivedRecordBusiness>().InstancePerDependency();
            //builder.RegisterType<CBReportBusiness>().As<ICBReportBusiness>().InstancePerDependency();

            #endregion

            #region Repository
            //builder.RegisterType<StatisticTertiaryStartYearRepository>().As<IStatisticTertiaryStartYearRepository>().InstancePerDependency();
            builder.RegisterType<OtherEthnicRepository>().As<IOtherEthnicRepository>().InstancePerDependency();
            builder.RegisterType<TrainingProgramRepository>().As<ITrainingProgramRepository>().InstancePerDependency();
            builder.RegisterType<AcademicYearOfProvinceRepository>().As<IAcademicYearOfProvinceRepository>().InstancePerDependency();
            builder.RegisterType<AcademicYearRepository>().As<IAcademicYearRepository>().InstancePerDependency();
            builder.RegisterType<ActionAuditRepository>().As<IActionAuditRepository>().InstancePerDependency();
            builder.RegisterType<ActivityPlanClassRepository>().As<IActivityPlanClassRepository>().InstancePerDependency();
            builder.RegisterType<ActivityPlanRepository>().As<IActivityPlanRepository>().InstancePerDependency();
            builder.RegisterType<ActivityRepository>().As<IActivityRepository>().InstancePerDependency();
            builder.RegisterType<ActivityTypeRepository>().As<IActivityTypeRepository>().InstancePerDependency();
            builder.RegisterType<AddMenuForChildrenRepository>().As<IAddMenuForChildrenRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipClassRepository>().As<IApprenticeshipClassRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipGroupRepository>().As<IApprenticeshipGroupRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipSubjectRepository>().As<IApprenticeshipSubjectRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingAbsenceRepository>().As<IApprenticeshipTrainingAbsenceRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingRepository>().As<IApprenticeshipTrainingRepository>().InstancePerDependency();
            builder.RegisterType<ApprenticeshipTrainingScheduleRepository>().As<IApprenticeshipTrainingScheduleRepository>().InstancePerDependency();
            builder.RegisterType<AreaRepository>().As<IAreaRepository>().InstancePerDependency();
            builder.RegisterType<aspnet_ApplicationsRepository>().As<Iaspnet_ApplicationsRepository>().InstancePerDependency();
            builder.RegisterType<aspnet_MembershipRepository>().As<Iaspnet_MembershipRepository>().InstancePerDependency();
            //  builder.RegisterType<aspnet_SchemaVersionsRepository>().As<Iaspnet_SchemaVersionsRepository>().InstancePerDependency();
            builder.RegisterType<aspnet_UsersRepository>().As<Iaspnet_UsersRepository>().InstancePerDependency();
            builder.RegisterType<CalendarRepository>().As<ICalendarRepository>().InstancePerDependency();
            builder.RegisterType<CallDetailRecordRepository>().As<ICallDetailRecordRepository>().InstancePerDependency();
            builder.RegisterType<CandidateAbsenceRepository>().As<ICandidateAbsenceRepository>().InstancePerDependency();
            builder.RegisterType<CandidateRepository>().As<ICandidateRepository>().InstancePerDependency();
            builder.RegisterType<CapacityLevelRepository>().As<ICapacityLevelRepository>().InstancePerDependency();
            builder.RegisterType<CapacityStatisticRepository>().As<ICapacityStatisticRepository>().InstancePerDependency();
            builder.RegisterType<CellDefinitionRepository>().As<ICellDefinitionRepository>().InstancePerDependency();
            builder.RegisterType<ClassAssigmentRepository>().As<IClassAssigmentRepository>().InstancePerDependency();
            builder.RegisterType<ClassEmulationDetailRepository>().As<IClassEmulationDetailRepository>().InstancePerDependency();
            builder.RegisterType<ClassEmulationRepository>().As<IClassEmulationRepository>().InstancePerDependency();
            builder.RegisterType<ClassForwardingRepository>().As<IClassForwardingRepository>().InstancePerDependency();
            builder.RegisterType<ClassificationCriteriaDetailRepository>().As<IClassificationCriteriaDetailRepository>().InstancePerDependency();
            builder.RegisterType<ClassificationCriteriaRepository>().As<IClassificationCriteriaRepository>().InstancePerDependency();
            builder.RegisterType<ClassificationLevelHealthRepository>().As<IClassificationLevelHealthRepository>().InstancePerDependency();
            builder.RegisterType<ClassMovementRepository>().As<IClassMovementRepository>().InstancePerDependency();
            builder.RegisterType<ClassProfileRepository>().As<IClassProfileRepository>().InstancePerDependency();
            builder.RegisterType<ClassPropertyCatRepository>().As<IClassPropertyCatRepository>().InstancePerDependency();
            builder.RegisterType<ClassSubjectRepository>().As<IClassSubjectRepository>().InstancePerDependency();
            builder.RegisterType<ClassSupervisorAssignmentRepository>().As<IClassSupervisorAssignmentRepository>().InstancePerDependency();
            builder.RegisterType<CodeConfigRepository>().As<ICodeConfigRepository>().InstancePerDependency();
            builder.RegisterType<CommuneRepository>().As<ICommuneRepository>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkAssignmentRepository>().As<IConcurrentWorkAssignmentRepository>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkReplacementRepository>().As<IConcurrentWorkReplacementRepository>().InstancePerDependency();
            builder.RegisterType<ConcurrentWorkTypeRepository>().As<IConcurrentWorkTypeRepository>().InstancePerDependency();
            builder.RegisterType<ConductConfigByCapacityRepository>().As<IConductConfigByCapacityRepository>().InstancePerDependency();
            builder.RegisterType<ConductConfigByViolationRepository>().As<IConductConfigByViolationRepository>().InstancePerDependency();
            builder.RegisterType<ConductEvaluationDetailRepository>().As<IConductEvaluationDetailRepository>().InstancePerDependency();
            builder.RegisterType<ConductLevelRepository>().As<IConductLevelRepository>().InstancePerDependency();
            builder.RegisterType<ConductStatisticRepository>().As<IConductStatisticRepository>().InstancePerDependency();
            builder.RegisterType<ContractRepository>().As<IContractRepository>().InstancePerDependency();
            builder.RegisterType<ContractTypeRepository>().As<IContractTypeRepository>().InstancePerDependency();
            builder.RegisterType<DailyDishCostRepository>().As<IDailyDishCostRepository>().InstancePerDependency();
            builder.RegisterType<DailyExpenseRepository>().As<IDailyExpenseRepository>().InstancePerDependency();
            builder.RegisterType<DailyFoodInspectionRepository>().As<IDailyFoodInspectionRepository>().InstancePerDependency();
            builder.RegisterType<DailyMenuDetailRepository>().As<IDailyMenuDetailRepository>().InstancePerDependency();
            builder.RegisterType<DailyMenuFoodRepository>().As<IDailyMenuFoodRepository>().InstancePerDependency();
            builder.RegisterType<DailyMenuRepository>().As<IDailyMenuRepository>().InstancePerDependency();
            builder.RegisterType<DailyOtherServiceInspectionRepository>().As<IDailyOtherServiceInspectionRepository>().InstancePerDependency();
            builder.RegisterType<DefaultGroupMenuRepository>().As<IDefaultGroupMenuRepository>().InstancePerDependency();
            builder.RegisterType<DefaultGroupRepository>().As<IDefaultGroupRepository>().InstancePerDependency();
            builder.RegisterType<DentalTestRepository>().As<IDentalTestRepository>().InstancePerDependency();
            builder.RegisterType<DetachableHeadBagRepository>().As<IDetachableHeadBagRepository>().InstancePerDependency();
            builder.RegisterType<DetachableHeadMappingRepository>().As<IDetachableHeadMappingRepository>().InstancePerDependency();
            builder.RegisterType<DevelopmentOfChildrenRepository>().As<IDevelopmentOfChildrenRepository>().InstancePerDependency();
            builder.RegisterType<DisabledTypeRepository>().As<IDisabledTypeRepository>().InstancePerDependency();
            builder.RegisterType<DisciplineTypeRepository>().As<IDisciplineTypeRepository>().InstancePerDependency();
            builder.RegisterType<DiseasesTypeRepository>().As<IDiseasesTypeRepository>().InstancePerDependency();
            builder.RegisterType<DishCatRepository>().As<IDishCatRepository>().InstancePerDependency();
            builder.RegisterType<DishDetailRepository>().As<IDishDetailRepository>().InstancePerDependency();
            builder.RegisterType<DishInspectionRepository>().As<IDishInspectionRepository>().InstancePerDependency();
            builder.RegisterType<DistrictRepository>().As<IDistrictRepository>().InstancePerDependency();
            builder.RegisterType<EatingGroupClassRepository>().As<IEatingGroupClassRepository>().InstancePerDependency();
            builder.RegisterType<EatingGroupRepository>().As<IEatingGroupRepository>().InstancePerDependency();
            builder.RegisterType<EducationalManagementGradeRepository>().As<IEducationalManagementGradeRepository>().InstancePerDependency();
            builder.RegisterType<EducationLevelRepository>().As<IEducationLevelRepository>().InstancePerDependency();
            builder.RegisterType<EducationProgramRepository>().As<IEducationProgramRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeHistoryStatusRepository>().As<IEmployeeHistoryStatusRepository>().InstancePerDependency();
            builder.RegisterType<EmployeePraiseDisciplineRepository>().As<IEmployeePraiseDisciplineRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeQualificationRepository>().As<IEmployeeQualificationRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeSalaryRepository>().As<IEmployeeSalaryRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeScaleRepository>().As<IEmployeeScaleRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkingHistoryRepository>().As<IEmployeeWorkingHistoryRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkMovementRepository>().As<IEmployeeWorkMovementRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeWorkTypeRepository>().As<IEmployeeWorkTypeRepository>().InstancePerDependency();
            builder.RegisterType<EmulationCriteriaRepository>().As<IEmulationCriteriaRepository>().InstancePerDependency();
            builder.RegisterType<EnergyDistributionRepository>().As<IEnergyDistributionRepository>().InstancePerDependency();
            builder.RegisterType<ENTTestRepository>().As<IENTTestRepository>().InstancePerDependency();
            builder.RegisterType<EthnicRepository>().As<IEthnicRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationConfigRepository>().As<IEvaluationConfigRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationDevelopmentGroupRepository>().As<IEvaluationDevelopmentGroupRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationDevelopmentRepository>().As<IEvaluationDevelopmentRepository>().InstancePerDependency();
            builder.RegisterType<ExaminationRepository>().As<IExaminationRepository>().InstancePerDependency();
            builder.RegisterType<ExaminationRoomRepository>().As<IExaminationRoomRepository>().InstancePerDependency();
            builder.RegisterType<ExaminationSubjectRepository>().As<IExaminationSubjectRepository>().InstancePerDependency();
            builder.RegisterType<ExamViolationTypeRepository>().As<IExamViolationTypeRepository>().InstancePerDependency();
            builder.RegisterType<ExemptedObjectTypeRepository>().As<IExemptedObjectTypeRepository>().InstancePerDependency();
            builder.RegisterType<ExemptedSubjectRepository>().As<IExemptedSubjectRepository>().InstancePerDependency();
            builder.RegisterType<ExperienceTypeRepository>().As<IExperienceTypeRepository>().InstancePerDependency();
            builder.RegisterType<EyeTestRepository>().As<IEyeTestRepository>().InstancePerDependency();
            builder.RegisterType<FamilyTypeRepository>().As<IFamilyTypeRepository>().InstancePerDependency();
            builder.RegisterType<FaultCriteriaRepository>().As<IFaultCriteriaRepository>().InstancePerDependency();
            builder.RegisterType<FaultGroupRepository>().As<IFaultGroupRepository>().InstancePerDependency();
            builder.RegisterType<FoodCatRepository>().As<IFoodCatRepository>().InstancePerDependency();
            builder.RegisterType<FoodGroupRepository>().As<IFoodGroupRepository>().InstancePerDependency();
            builder.RegisterType<FoodHabitOfChildrenRepository>().As<IFoodHabitOfChildrenRepository>().InstancePerDependency();
            builder.RegisterType<FoodMineralRepository>().As<IFoodMineralRepository>().InstancePerDependency();
            builder.RegisterType<FoodPackingRepository>().As<IFoodPackingRepository>().InstancePerDependency();
            builder.RegisterType<ForeignLanguageGradeRepository>().As<IForeignLanguageGradeRepository>().InstancePerDependency();
            builder.RegisterType<GoodChildrenTicketRepository>().As<IGoodChildrenTicketRepository>().InstancePerDependency();
            builder.RegisterType<GraduationLevelRepository>().As<IGraduationLevelRepository>().InstancePerDependency();
            builder.RegisterType<GroupCatRepository>().As<IGroupCatRepository>().InstancePerDependency();
            builder.RegisterType<GrowthEvaluationRepository>().As<IGrowthEvaluationRepository>().InstancePerDependency();
            builder.RegisterType<GroupMenuRepository>().As<IGroupMenuRepository>().InstancePerDependency();
            builder.RegisterType<HabitDetailRepository>().As<IHabitDetailRepository>().InstancePerDependency();
            builder.RegisterType<HabitGroupRepository>().As<IHabitGroupRepository>().InstancePerDependency();
            builder.RegisterType<HabitOfChildrenRepository>().As<IHabitOfChildrenRepository>().InstancePerDependency();
            builder.RegisterType<HeadTeacherSubstitutionRepository>().As<IHeadTeacherSubstitutionRepository>().InstancePerDependency();
            builder.RegisterType<HealthCheckPeriodRepository>().As<IHealthCheckPeriodRepository>().InstancePerDependency();
            builder.RegisterType<HealthPeriodRepository>().As<IHealthPeriodRepository>().InstancePerDependency();
            builder.RegisterType<HistorySMSRepository>().As<IHistorySMSRepository>().InstancePerDependency();
            builder.RegisterType<HonourAchivementRepository>().As<IHonourAchivementRepository>().InstancePerDependency();
            builder.RegisterType<HonourAchivementTypeRepository>().As<IHonourAchivementTypeRepository>().InstancePerDependency();
            builder.RegisterType<InfectiousDiseasRepository>().As<IInfectiousDiseasRepository>().InstancePerDependency();
            builder.RegisterType<InvigilatorAssignmentRepository>().As<IInvigilatorAssignmentRepository>().InstancePerDependency();
            builder.RegisterType<InvigilatorRepository>().As<IInvigilatorRepository>().InstancePerDependency();
            builder.RegisterType<ITQualificationLevelRepository>().As<IITQualificationLevelRepository>().InstancePerDependency();
            builder.RegisterType<JudgeRecordRepository>().As<IJudgeRecordRepository>().InstancePerDependency();
            builder.RegisterType<LeavingReasonRepository>().As<ILeavingReasonRepository>().InstancePerDependency();
            builder.RegisterType<LevelHealthConfigDetailRepository>().As<ILevelHealthConfigDetailRepository>().InstancePerDependency();
            builder.RegisterType<LevelHealthConfigRepository>().As<ILevelHealthConfigRepository>().InstancePerDependency();
            builder.RegisterType<LockedMarkDetailRepository>().As<ILockedMarkDetailRepository>().InstancePerDependency();
            builder.RegisterType<MarkRecordRepository>().As<IMarkRecordRepository>().InstancePerDependency();
            builder.RegisterType<MarkRecordHistoryRepository>().As<IMarkRecordHistoryRepository>().InstancePerDependency();
            builder.RegisterType<MarkStatisticRepository>().As<IMarkStatisticRepository>().InstancePerDependency();
            builder.RegisterType<MarkTypeRepository>().As<IMarkTypeRepository>().InstancePerDependency();
            builder.RegisterType<MealCatRepository>().As<IMealCatRepository>().InstancePerDependency();
            builder.RegisterType<MenuPermissionRepository>().As<IMenuPermissionRepository>().InstancePerDependency();
            builder.RegisterType<MenuRepository>().As<IMenuRepository>().InstancePerDependency();
            builder.RegisterType<MessageRepository>().As<IMessageRepository>().InstancePerDependency();
            builder.RegisterType<MinenalCatRepository>().As<IMinenalCatRepository>().InstancePerDependency();
            builder.RegisterType<MonitoringBookRepository>().As<IMonitoringBookRepository>().InstancePerDependency();
            builder.RegisterType<MonthlyBalanceRepository>().As<IMonthlyBalanceRepository>().InstancePerDependency();
            builder.RegisterType<MonthlyLockRepository>().As<IMonthlyLockRepository>().InstancePerDependency();
            builder.RegisterType<MovementAcceptanceRepository>().As<IMovementAcceptanceRepository>().InstancePerDependency();
            builder.RegisterType<NotEatingChildrenRepository>().As<INotEatingChildrenRepository>().InstancePerDependency();
            builder.RegisterType<NutritionalNormMineralRepository>().As<INutritionalNormMineralRepository>().InstancePerDependency();
            builder.RegisterType<NutritionalNormRepository>().As<INutritionalNormRepository>().InstancePerDependency();
            builder.RegisterType<OtherServiceRepository>().As<IOtherServiceRepository>().InstancePerDependency();
            builder.RegisterType<OverallTestRepository>().As<IOverallTestRepository>().InstancePerDependency();
            builder.RegisterType<PackageRepository>().As<IPackageRepository>().InstancePerDependency();
            builder.RegisterType<ParticularPupilCharacteristicRepository>().As<IParticularPupilCharacteristicRepository>().InstancePerDependency();
            builder.RegisterType<ParticularPupilRepository>().As<IParticularPupilRepository>().InstancePerDependency();
            builder.RegisterType<ParticularPupilTreatmentRepository>().As<IParticularPupilTreatmentRepository>().InstancePerDependency();
            builder.RegisterType<PeriodDeclarationRepository>().As<IPeriodDeclarationRepository>().InstancePerDependency();
            builder.RegisterType<PhysicalTestRepository>().As<IPhysicalTestRepository>().InstancePerDependency();
            builder.RegisterType<PolicyRegimeRepository>().As<IPolicyRegimeRepository>().InstancePerDependency();
            builder.RegisterType<PolicyTargetRepository>().As<IPolicyTargetRepository>().InstancePerDependency();
            builder.RegisterType<PoliticalGradeRepository>().As<IPoliticalGradeRepository>().InstancePerDependency();
            builder.RegisterType<PraiseTypeRepository>().As<IPraiseTypeRepository>().InstancePerDependency();
            builder.RegisterType<PriorityTypeRepository>().As<IPriorityTypeRepository>().InstancePerDependency();
            builder.RegisterType<ProcessedCellDataRepository>().As<IProcessedCellDataRepository>().InstancePerDependency();
            builder.RegisterType<ProcessedReportParameterRepository>().As<IProcessedReportParameterRepository>().InstancePerDependency();
            builder.RegisterType<ProcessedReportRepository>().As<IProcessedReportRepository>().InstancePerDependency();
            builder.RegisterType<PropertyOfClassRepository>().As<IPropertyOfClassRepository>().InstancePerDependency();
            builder.RegisterType<PropertyOfSchoolRepository>().As<IPropertyOfSchoolRepository>().InstancePerDependency();
            builder.RegisterType<ProvinceRepository>().As<IProvinceRepository>().InstancePerDependency();
            builder.RegisterType<ProvinceSubjectRepository>().As<IProvinceSubjectRepository>().InstancePerDependency();
            builder.RegisterType<PupilAbsenceRepository>().As<IPupilAbsenceRepository>().InstancePerDependency();
            builder.RegisterType<PupilDisciplineRepository>().As<IPupilDisciplineRepository>().InstancePerDependency();
            builder.RegisterType<PupilEmulationRepository>().As<IPupilEmulationRepository>().InstancePerDependency();
            builder.RegisterType<PupilFaultRepository>().As<IPupilFaultRepository>().InstancePerDependency();
            builder.RegisterType<PupilGraduationRepository>().As<IPupilGraduationRepository>().InstancePerDependency();
            builder.RegisterType<PupilLeavingOffRepository>().As<IPupilLeavingOffRepository>().InstancePerDependency();
            builder.RegisterType<PupilOfClassRepository>().As<IPupilOfClassRepository>().InstancePerDependency();
            builder.RegisterType<PupilOfSchoolRepository>().As<IPupilOfSchoolRepository>().InstancePerDependency();
            builder.RegisterType<PupilPraiseRepository>().As<IPupilPraiseRepository>().InstancePerDependency();
            builder.RegisterType<PupilProfileRepository>().As<IPupilProfileRepository>().InstancePerDependency();
            builder.RegisterType<PupilRankingRepository>().As<IPupilRankingRepository>().InstancePerDependency();
            builder.RegisterType<PupilRepeatedRepository>().As<IPupilRepeatedRepository>().InstancePerDependency();
            builder.RegisterType<PupilRetestRegistrationRepository>().As<IPupilRetestRegistrationRepository>().InstancePerDependency();
            builder.RegisterType<QualificationGradeRepository>().As<IQualificationGradeRepository>().InstancePerDependency();
            builder.RegisterType<QualificationLevelRepository>().As<IQualificationLevelRepository>().InstancePerDependency();
            builder.RegisterType<QualificationTypeRepository>().As<IQualificationTypeRepository>().InstancePerDependency();
            builder.RegisterType<ReligionRepository>().As<IReligionRepository>().InstancePerDependency();
            builder.RegisterType<ReportCategoryRepository>().As<IReportCategoryRepository>().InstancePerDependency();
            builder.RegisterType<ReportDefinitionRepository>().As<IReportDefinitionRepository>().InstancePerDependency();
            builder.RegisterType<ReportParameterRepository>().As<IReportParameterRepository>().InstancePerDependency();
            builder.RegisterType<RoleMenuRepository>().As<IRoleMenuRepository>().InstancePerDependency();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerDependency();
            builder.RegisterType<SalaryLevelRepository>().As<ISalaryLevelRepository>().InstancePerDependency();
            builder.RegisterType<SchoolCodeConfigRepository>().As<ISchoolCodeConfigRepository>().InstancePerDependency();
            builder.RegisterType<SchoolFacultyRepository>().As<ISchoolFacultyRepository>().InstancePerDependency();
            builder.RegisterType<SchoolMovementRepository>().As<ISchoolMovementRepository>().InstancePerDependency();
            builder.RegisterType<SchoolProfileRepository>().As<ISchoolProfileRepository>().InstancePerDependency();
            builder.RegisterType<SchoolPropertyCatRepository>().As<ISchoolPropertyCatRepository>().InstancePerDependency();
            builder.RegisterType<SchoolSubjectRepository>().As<ISchoolSubjectRepository>().InstancePerDependency();
            builder.RegisterType<SchoolSubsidiaryRepository>().As<ISchoolSubsidiaryRepository>().InstancePerDependency();
            builder.RegisterType<SchoolTypeRepository>().As<ISchoolTypeRepository>().InstancePerDependency();
            builder.RegisterType<SemeterDeclarationRepository>().As<ISemeterDeclarationRepository>().InstancePerDependency();
            builder.RegisterType<SendInfoRepository>().As<ISendInfoRepository>().InstancePerDependency();
            builder.RegisterType<SendResultRepository>().As<ISendResultRepository>().InstancePerDependency();
            builder.RegisterType<ServiceRepository>().As<IServiceRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentContractRepository>().As<ISMSParentContractRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentNumSMSSentRepository>().As<ISMSParentNumSMSSentRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentSubscriberRepository>().As<ISMSParentSubscriberRepository>().InstancePerDependency();
            builder.RegisterType<SMSTeacherContractRepository>().As<ISMSTeacherContractRepository>().InstancePerDependency();
            builder.RegisterType<SpecialityCatRepository>().As<ISpecialityCatRepository>().InstancePerDependency();
            builder.RegisterType<SpineTestRepository>().As<ISpineTestRepository>().InstancePerDependency();
            builder.RegisterType<StaffPositionRepository>().As<IStaffPositionRepository>().InstancePerDependency();
            builder.RegisterType<StateManagementGradeRepository>().As<IStateManagementGradeRepository>().InstancePerDependency();
            builder.RegisterType<StatisticOfTertiaryRepository>().As<IStatisticOfTertiaryRepository>().InstancePerDependency();
            builder.RegisterType<StatisticsConfigRepository>().As<IStatisticsConfigRepository>().InstancePerDependency();
            builder.RegisterType<StatisticSubcommitteeOfTertiaryRepository>().As<IStatisticSubcommitteeOfTertiaryRepository>().InstancePerDependency();
            builder.RegisterType<StatisticSubjectOfTertiaryRepository>().As<IStatisticSubjectOfTertiaryRepository>().InstancePerDependency();
            builder.RegisterType<StudyingJudgementRepository>().As<IStudyingJudgementRepository>().InstancePerDependency();
            builder.RegisterType<SubCommitteeRepository>().As<ISubCommitteeRepository>().InstancePerDependency();
            builder.RegisterType<SubjectCatRepository>().As<ISubjectCatRepository>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordClassRepository>().As<ISummedUpRecordClassRepository>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordRepository>().As<ISummedUpRecordRepository>().InstancePerDependency();
            builder.RegisterType<SummedUpRecordHistoryRepository>().As<ISummedUpRecordHistoryRepository>().InstancePerDependency();
            builder.RegisterType<SupervisingDeptRepository>().As<ISupervisingDeptRepository>().InstancePerDependency();
            builder.RegisterType<SupplierRepository>().As<ISupplierRepository>().InstancePerDependency();
            builder.RegisterType<SymbolConfigRepository>().As<ISymbolConfigRepository>().InstancePerDependency();
            builder.RegisterType<sysdiagramRepository>().As<IsysdiagramRepository>().InstancePerDependency();
            builder.RegisterType<SystemParameterRepository>().As<ISystemParameterRepository>().InstancePerDependency();
            builder.RegisterType<TeacherGradeRepository>().As<ITeacherGradeRepository>().InstancePerDependency();
            builder.RegisterType<TeacherGradingRepository>().As<ITeacherGradingRepository>().InstancePerDependency();
            builder.RegisterType<TeacherOfFacultyRepository>().As<ITeacherOfFacultyRepository>().InstancePerDependency();
            builder.RegisterType<TeachingAssignmentRepository>().As<ITeachingAssignmentRepository>().InstancePerDependency();
            builder.RegisterType<TeachingExperienceRepository>().As<ITeachingExperienceRepository>().InstancePerDependency();
            builder.RegisterType<TeachingRegistrationRepository>().As<ITeachingRegistrationRepository>().InstancePerDependency();
            builder.RegisterType<TeachingTargetRegistrationRepository>().As<ITeachingTargetRegistrationRepository>().InstancePerDependency();
            builder.RegisterType<TrainingLevelRepository>().As<ITrainingLevelRepository>().InstancePerDependency();
            builder.RegisterType<TrainingTypeRepository>().As<ITrainingTypeRepository>().InstancePerDependency();
            builder.RegisterType<TypeConfigRepository>().As<ITypeConfigRepository>().InstancePerDependency();
            builder.RegisterType<TypeOfDishRepository>().As<ITypeOfDishRepository>().InstancePerDependency();
            builder.RegisterType<TypeOfFoodRepository>().As<ITypeOfFoodRepository>().InstancePerDependency();
            builder.RegisterType<UserAccountRepository>().As<IUserAccountRepository>().InstancePerDependency();
            builder.RegisterType<UserGroupRepository>().As<IUserGroupRepository>().InstancePerDependency();
            builder.RegisterType<UserProvinceRepository>().As<IUserProvinceRepository>().InstancePerDependency();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>().InstancePerDependency();
            builder.RegisterType<ViolatedCandidateRepository>().As<IViolatedCandidateRepository>().InstancePerDependency();
            builder.RegisterType<ViolatedInvigilatorRepository>().As<IViolatedInvigilatorRepository>().InstancePerDependency();
            builder.RegisterType<WeeklyMenuDetailRepository>().As<IWeeklyMenuDetailRepository>().InstancePerDependency();
            builder.RegisterType<WeeklyMenuRepository>().As<IWeeklyMenuRepository>().InstancePerDependency();
            builder.RegisterType<WorkGroupTypeRepository>().As<IWorkGroupTypeRepository>().InstancePerDependency();
            builder.RegisterType<WorkTypeRepository>().As<IWorkTypeRepository>().InstancePerDependency();
            builder.RegisterType<AlertMessageRepository>().As<IAlertMessageRepository>().InstancePerDependency();
            builder.RegisterType<AlertMessageDetailRepository>().As<IAlertMessageDetailRepository>().InstancePerDependency();
            builder.RegisterType<MIdmappingRepository>().As<IMIdmappingRepository>().InstancePerDependency();
            builder.RegisterType<VillageRepository>().As<IVillageRepository>().InstancePerDependency();
            builder.RegisterType<VJudgeRecordRepository>().As<IVJudgeRecordRepository>().InstancePerDependency();
            builder.RegisterType<VMarkRecordRepository>().As<IVMarkRecordRepository>().InstancePerDependency();
            builder.RegisterType<VSummedUpRecordRepository>().As<IVSummedUpRecordRepository>().InstancePerDependency();
            builder.RegisterType<VPupilRankingRepository>().As<IVPupilRankingRepository>().InstancePerDependency();
            builder.RegisterType<LockTrainingRepository>().As<ILockTrainingRepository>().InstancePerDependency();
            builder.RegisterType<LockMarkPrimaryRepository>().As<ILockMarkPrimaryRepository>().InstancePerDependency();
            builder.RegisterType<StatisticLevelReportRepository>().As<IStatisticLevelReportRepository>().InstancePerDependency();
            builder.RegisterType<StatisticLevelConfigRepository>().As<IStatisticLevelConfigRepository>().InstancePerDependency();
            //builder.RegisterType<MarkBookPrimaryConfigRepository>().As<IMarkBookPrimaryConfigRepository>().InstancePerDependency();
            //builder.RegisterType<PupilReviewEvaluateRepository>().As<IPupilReviewEvaluateRepository>().InstancePerDependency();
            builder.RegisterType<MonthCommentsRepository>().As<IMonthCommentsRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationCriteriaRepository>().As<IEvaluationCriteriaRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationRepository>().As<IEvaluationRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationCommentsRepository>().As<IEvaluationCommentsRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationCommentsHistoryRepository>().As<IEvaluationCommentsHistoryRepository>().InstancePerDependency();
            builder.RegisterType<SummedEvaluationRepository>().As<ISummedEvaluationRepository>().InstancePerDependency();
            builder.RegisterType<SummedEvaluationHistoryRepository>().As<ISummedEvaluationHistoryRepository>().InstancePerDependency();
            builder.RegisterType<SummedEndingEvaluationRepository>().As<ISummedEndingEvaluationRepository>().InstancePerDependency();
            builder.RegisterType<RewardFinalRepository>().As<IRewardFinalRepository>().InstancePerDependency();
            builder.RegisterType<RewardCommentFinalRepository>().As<IRewardCommentFinalRepository>().InstancePerDependency();
            builder.RegisterType<RegisterSubjectSpecializeRepository>().As<IRegisterSubjectSpecializeRepository>().InstancePerDependency();

            builder.RegisterType<ExaminationsRepository>().As<IExaminationsRepository>().InstancePerDependency();
            builder.RegisterType<ExamGroupRepository>().As<IExamGroupRepository>().InstancePerDependency();
            builder.RegisterType<ExamSubjectRepository>().As<IExamSubjectRepository>().InstancePerDependency();
            builder.RegisterType<ExamPupilRepository>().As<IExamPupilRepository>().InstancePerDependency();
            builder.RegisterType<ExamRoomRepository>().As<IExamRoomRepository>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryRepository>().As<IExamSupervisoryRepository>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryAssignmentRepository>().As<IExamSupervisoryAssignmentRepository>().InstancePerDependency();
            builder.RegisterType<ExamBagRepository>().As<IExamBagRepository>().InstancePerDependency();
            builder.RegisterType<ExamDetachableBagRepository>().As<IExamDetachableBagRepository>().InstancePerDependency();
            builder.RegisterType<ExamPupilViolateRepository>().As<IExamPupilViolateRepository>().InstancePerDependency();
            builder.RegisterType<ExamSupervisoryViolateRepository>().As<IExamSupervisoryViolateRepository>().InstancePerDependency();
            builder.RegisterType<ExamPupilAbsenceRepository>().As<IExamPupilAbsenceRepository>().InstancePerDependency();
            builder.RegisterType<ExamInputMarkAssignedRepository>().As<IExamInputMarkAssignedRepository>().InstancePerDependency();
            builder.RegisterType<ExamInputMarkRepository>().As<IExamInputMarkRepository>().InstancePerDependency();
            builder.RegisterType<ExamCandenceBagRepository>().As<IExamCandenceBagRepository>().InstancePerDependency();
            builder.RegisterType<ThreadMarkRepository>().As<IThreadMarkRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeBreatherRepository>().As<IEmployeeBreatherRepository>().InstancePerDependency();
            builder.RegisterType<ConfigConductRankingRepository>().As<IConfigConductRankingRepository>().InstancePerDependency();
            builder.RegisterType<PushNotifyRequestRepository>().As<IPushNotifyRequestRepository>().InstancePerDependency();
            builder.RegisterType<DOETExamPupilRepository>().As<IDOETExamPupilRepository>().InstancePerDependency();
            builder.RegisterType<DOETExamMarkRepository>().As<IDOETExamMarkRepository>().InstancePerDependency();
            builder.RegisterType<DOETExaminationsRepository>().As<IDOETExaminationsRepository>().InstancePerDependency();
            builder.RegisterType<DOETExamGroupRepository>().As<IDOETExamGroupRepository>().InstancePerDependency();
            builder.RegisterType<DOETExamSubjectRepository>().As<IDOETExamSubjectRepository>().InstancePerDependency();
            builder.RegisterType<DOETSubjectRepository>().As<IDOETSubjectRepository>().InstancePerDependency();
            builder.RegisterType<DOETExamSupervisoryRepository>().As<IDOETExamSupervisoryRepository>().InstancePerDependency();
            builder.RegisterType<TeacherNoteBookMonthRepository>().As<ITeacherNoteBookMonthRepository>().InstancePerDependency();
            builder.RegisterType<TeacherNoteBookSemesterRepository>().As<ITeacherNoteBookSemesterRepository>().InstancePerDependency();
            builder.RegisterType<ReviewBookPupilRepository>().As<IReviewBookPupilRepository>().InstancePerDependency();
            builder.RegisterType<UpdateRewardRepository>().As<IUpdateRewardRepository>().InstancePerDependency();
            builder.RegisterType<TemplateRepository>().As<ITemplateRepository>().InstancePerDependency();
            builder.RegisterType<HtmlContentCodeRepository>().As<IHtmlContentCodeRepository>().InstancePerDependency();
            builder.RegisterType<HtmlTemplateRepository>().As<IHtmlTemplateRepository>().InstancePerDependency();
            builder.RegisterType<IndicatorRepository>().As<IIndicatorRepository>().InstancePerDependency();
            builder.RegisterType<IndicatorDataRepository>().As<IIndicatorDataRepository>().InstancePerDependency();
            builder.RegisterType<VacationReasonRepository>().As<IVacationReasonRepository>().InstancePerDependency();
            builder.RegisterType<ThreadMovedDataRepository>().As<IThreadMovedDataRepository>().InstancePerDependency();
            builder.RegisterType<PupilRankingHistoryRepository>().As<IPupilRankingHistoryRepository>().InstancePerDependency();
            builder.RegisterType<BookmarkedFunctionRepository>().As<IBookmarkedFunctionRepository>().InstancePerDependency();
            builder.RegisterType<WorkFlowUserRepository>().As<IWorkFlowUserRepository>().InstancePerDependency();
            builder.RegisterType<WorkFlowRepository>().As<IWorkFlowRepository>().InstancePerDependency();
            builder.RegisterType<DistributeProgramRepository>().As<IDistributeProgramRepository>().InstancePerDependency();
            builder.RegisterType<TeachingScheduleRepository>().As<ITeachingScheduleRepository>().InstancePerDependency();
            builder.RegisterType<SchoolWeekRepository>().As<ISchoolWeekRepository>().InstancePerDependency();
            builder.RegisterType<RatedCommentPupilRepository>().As<IRatedCommentPupilRepository>().InstancePerDependency();
            builder.RegisterType<RatedCommentPupilHistoryRepository>().As<IRatedCommentPupilHistoryRepository>().InstancePerDependency();
            builder.RegisterType<CommentOfPupilRepository>().As<ICommentOfPupilRepository>().InstancePerDependency();
            builder.RegisterType<CommentOfPupilHistoryRepository>().As<ICommentOfPupilHistoryRepository>().InstancePerDependency();
            builder.RegisterType<LockRatedCommentPupilRepository>().As<ILockRatedCommentPupilRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationRewardRepository>().As<IEvaluationRewardRepository>().InstancePerDependency();
            builder.RegisterType<CollectionCommentsRepository>().As<ICollectionCommentsRepository>().InstancePerDependency();
            builder.RegisterType<DiplomaTemplateRepository>().As<IDiplomaTemplateRepository>().InstancePerDependency();
            builder.RegisterType<DiplomaImageRepository>().As<IDiplomaImageRepository>().InstancePerDependency();
            builder.RegisterType<KPIFunctionRepository>().As<KPIFunctionRepository>().InstancePerDependency();
            builder.RegisterType<KPILogRepository>().As<KPILogRepository>().InstancePerDependency();
            builder.RegisterType<DeclareEvaluationIndexRepository>().As<IDeclareEvaluationIndexRepository>().InstancePerDependency();
            builder.RegisterType<DeclareEvaluationGroupRepository>().As<IDeclareEvaluationGroupRepository>().InstancePerDependency();
            builder.RegisterType<PhysicalExaminationRepository>().As<IPhysicalExaminationRepository>().InstancePerDependency();
            builder.RegisterType<CommentsOfKindergartenRepository>().As<ICommentsOfKindergartenRepository>().InstancePerDependency();
            builder.RegisterType<DevelopmentStandardRepository>().As<IDevelopmentStandardRepository>().InstancePerDependency();
            builder.RegisterType<DevelopmentStandardRepository>().As<IDevelopmentStandardRepository>().InstancePerDependency();
            builder.RegisterType<MealCategoryRepository>().As<IMealCategoryRepository>().InstancePerDependency();
            builder.RegisterType<WeeklyMealMenuRepository>().As<IWeeklyMealMenuRepository>().InstancePerDependency();
            builder.RegisterType<QuickHintRepository>().As<IQuickHintRepository>().InstancePerDependency();
            builder.RegisterType<AssignSubjectConfigRepository>().As<IAssignSubjectConfigRepository>().InstancePerDependency();
            builder.RegisterType<DistributeProgramSystemRepository>().As<IDistributeProgramSystemRepository>().InstancePerDependency();
            builder.RegisterType<TeachingScheduleApprovalRepository>().As<ITeachingScheduleApprovalRepository>().InstancePerDependency();
			builder.RegisterType<LockInputSupervisingDeptRepository>().As<ILockInputSupervisingDeptRepository>().InstancePerDependency();
            builder.RegisterType<RestoreDataRepository>().As<IRestoreDataRepository>().InstancePerDependency();
            builder.RegisterType<RestoreDataDetailRepository>().As<IRestoreDataDetailRepository>().InstancePerDependency();
            builder.RegisterType<QiSubjectMappingRepository>().As<IQiSubjectMappingRepository>().InstancePerDependency();
            builder.RegisterType<UsingMarkbookCommentRepository>().As<IUsingMarkbookCommentRepository>().InstancePerDependency();
            builder.RegisterType<ManagementHealthInsuranceRepository>().As<IManagementHealthInsuranceRepository>().InstancePerDependency();
            builder.RegisterType<SurveyRepository>().As<ISurveyRepository>().InstancePerDependency();
            builder.RegisterType<ColumnDescriptionRepository>().As<IColumnDescriptionRepository>().InstancePerDependency();
            builder.RegisterType<EmployeeEvaluationRepository>().As<IEmployeeEvaluationRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationFieldRepository>().As<IEvaluationFieldRepository>().InstancePerDependency();
            builder.RegisterType<EvaluationLevelRepository>().As<IEvaluationLevelRepository>().InstancePerDependency();
            builder.RegisterType<ForeignLanguageCatRepository>().As<IForeignLanguageCatRepository>().InstancePerDependency();
            builder.RegisterType<ProfileTemplateRepository>().As<IProfileTemplateRepository>().InstancePerDependency();
            builder.RegisterType<GraduationApprovalRepository>().As<IGraduationApprovalRepository>().InstancePerDependency();
            builder.RegisterType<VRatedCommentPupilRepository>().As<IVRatedCommentPupilRepository>().InstancePerDependency();

            builder.RegisterType<SchoolConfigRepository>().As<ISchoolConfigRepository>().InstancePerDependency();
            builder.RegisterType<SMSTypeRepository>().As<ISMSTypeRepository>().InstancePerDependency();
            builder.RegisterType<SMSHistoryRepository>().As<ISMSHistoryRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentContractRepository>().As<ISMSParentContractRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentContractInClassDetailRepository>().As<ISMSParentContractInClassDetailRepository>().InstancePerDependency();
            builder.RegisterType<TimerConfigRepository>().As<ITimerConfigRepository>().InstancePerDependency();
            builder.RegisterType<CustomSMSTypeRepository>().As<ICustomSMSTypeRepository>().InstancePerDependency();
            builder.RegisterType<ContentTypeRepository>().As<IContentTypeRepository>().InstancePerDependency();
            builder.RegisterType<CustomSMSTypeRepository>().As<ICustomSMSTypeRepository>().InstancePerDependency();
            builder.RegisterType<TeacherContactRepository>().As<ITeacherContactRepository>().InstancePerDependency();
            builder.RegisterType<EWalletRepository>().As<IEWalletRepository>().InstancePerDependency();
            builder.RegisterType<PromotionRepository>().As<IPromotionRepository>().InstancePerDependency();
            builder.RegisterType<PromotionDetailRepository>().As<IPromotionDetailRepository>().InstancePerDependency();
            builder.RegisterType<ServicePackageRepository>().As<IServicePackageRepository>().InstancePerDependency();
            builder.RegisterType<ServicePackageDetailRepository>().As<IServicePackageDetailRepository>().InstancePerDependency();
            builder.RegisterType<TimerTransactionRepository>().As<ITimerTransactionRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentSentDetailRepository>().As<ISMSParentSentDetailRepository>().InstancePerDependency();
            builder.RegisterType<ServicePackageSchoolDetailRepository>().As<IServicePackageSchoolDetailRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentContractDetailRepository>().As<ISMSParentContractDetailRepository>().InstancePerDependency();
            builder.RegisterType<SMSParentContractExtraRepository>().As<ISMSParentContractExtraRepository>().InstancePerDependency();
            builder.RegisterType<BrandnameProviderRepository>().As<IBrandnameProviderRepository>().InstancePerDependency();
            builder.RegisterType<BrandnameRegistrationRepository>().As<IBrandnameRegistrationRepository>().InstancePerDependency();
            builder.RegisterType<RegisterSMSFreeRepository>().As<IRegisterSMSFreeRepository>().InstancePerDependency();
            builder.RegisterType<AffectedHistoryRepository>().As<IAffectedHistoryRepository>().InstancePerDependency();
            builder.RegisterType<TopupRepository>().As<ITopupRepository>().InstancePerDependency();
            builder.RegisterType<EWalletTransactionRepository>().As<IEWalletTransactionRepository>().InstancePerDependency();
            builder.RegisterType<EwalletTransactionTypeRepository>().As<IEwalletTransactionTypeRepository>().InstancePerDependency();
            builder.RegisterType<ReceivedRecordRepository>().As<IReceivedRecordRepository>().InstancePerDependency();
            #endregion

            builder.RegisterType<Aspose.Cells.Workbook>().As<Aspose.Cells.Workbook>().InstancePerDependency();
            builder.RegisterType<Aspose.Pdf.Facades.PdfFileEditor>().As<Aspose.Pdf.Facades.PdfFileEditor>().InstancePerDependency();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule(new LogInjectionModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            

            #endregion

            string DefaultLanguage = ConfigurationManager.AppSettings["DefaultLanguage"];
            Application["DefaultLanguage"] = DefaultLanguage;
            MvcHandler.DisableMvcResponseHeader = true;
            //PhuongH1 - 22/05/2013 
            using (SMASEntities context = new SMASEntities())
            {
                SMAS.Business.Common.StaticData.ListMenu = new MenuRepository(context).All.ToList();
                //Danh sach workflow
                SMAS.Business.Common.StaticData.listWorkFlow = new WorkFlowRepository(context).All.Where(c => c.IsActive).ToList();
                // QuangLM - 17/07/2013
                SMAS.Business.Common.StaticData.ListRole = new RoleRepository(context).All.ToList();
                // Load menu theo role
                Dictionary<int, List<Menu>> dicRoleMenu = new Dictionary<int, List<Menu>>();
                foreach (Role role in SMAS.Business.Common.StaticData.ListRole)
                {
                    int roleID = role.RoleID;
                    if (!dicRoleMenu.ContainsKey(roleID))
                    {
                        List<Menu> roleMenus = new RoleMenuRepository(context).All.Where(o => o.RoleID == roleID
                            && o.Menu.IsActive).Select(o => o.Menu).ToList();
                        dicRoleMenu.Add(roleID, roleMenus);
                    }
                }
                SMAS.Business.Common.StaticData.DicMenuRole = dicRoleMenu;
                //Danh sach cac chuc nang do KPI
                SMAS.Business.Common.StaticData.lstKPIFunction = new KPIFunctionRepository(context).All.ToList();
            }

            #region Register
            //log
            log4net.Config.XmlConfigurator.Configure();

            //area            
            AreaRegistration.RegisterAllAreas();

            //global filter
            RegisterGlobalFilters(GlobalFilters.Filters);

            //routes
            RegisterRoutes(RouteTable.Routes);

            // Bundles
            //BundleTable.EnableOptimizations = true;
            RegisterBundles(BundleTable.Bundles);

            #endregion

            #region System parameters

            GlobalConstants.Parameters = ReadSystemParameters();

            #endregion

            // Schedule auto for push notification to SMAS App
            System.Threading.Tasks.Task.Run((Action)StartScheduler); 

        }
        #region Schedule auto for push notification to SMAS App
       
        private void StartScheduler()
        {
            if (!schedulerStarted)
            {
                //Start Schedule Task
                //SMAS.Library.ScheduleTaskAuto.ScheduleTaskExcute.StartTaskSchedulerGlobal();
            }
        }

        //Scheduler start only one time
        private static bool schedulerStarted = false;

        #endregion

        public Dictionary<string, object> ReadSystemParameters()
        {
            Dictionary<string, object> lsPara = new Dictionary<string, object>();
            SystemParameterBusiness systemPara = new SystemParameterBusiness();
            IQueryable<SystemParameter> allParams = systemPara.All;
            if (allParams != null && allParams.Count() > 0)
            {
                foreach (var item in allParams)
                {
                    lsPara.Add(item.KeyName, item.KeyValue);
                }
            }
            SystemParams.listParams = lsPara;
            return lsPara;
        }

        protected void Session_Start()
        {
            try
            {
                if (Request.IsSecureConnection == true)
                {
                    Response.Cookies["ASP.NET_SessionId"].Secure = true;
                }
            }
            catch
            {
            }
        }
        protected void Session_End()
        {
            VTUtils.LockAction.LockManager.ReleaseSessionLocks(Session.SessionID);
            //// add to fix Session Fixation Error
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
        }

        /// <summary>
        /// Ma hoa web config
        /// </summary>
        private void EncryptConfig()
        {
            try
            {
                string encrypt = WebConfigurationManager.AppSettings["encrypt"].ToString();
                if (encrypt.Equals("true"))
                {
                    Configuration objConfig = WebConfigurationManager.OpenWebConfiguration("~");
                    ConfigurationSection objSection = objConfig.GetSection("connectionStrings");
                    if (!objSection.SectionInformation.IsProtected)
                    {
                        objSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                        objConfig.Save();
                    }
                }
                else
                {
                    Configuration objConfig = WebConfigurationManager.OpenWebConfiguration("~");
                    ConfigurationSection section = objConfig.GetSection("connectionStrings");
                    if (section.SectionInformation.IsProtected)
                    {
                        section.SectionInformation.UnprotectSection();
                        objConfig.Save();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}