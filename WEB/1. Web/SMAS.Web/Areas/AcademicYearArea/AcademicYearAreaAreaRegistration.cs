﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.AcademicYearArea
{
    public class AcademicYearAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AcademicYearArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AcademicYearArea_default",
                "AcademicYearArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
