﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.AcademicYearArea.Models;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using System.Configuration;
using SMAS.Web.Filter;
using AutoMapper;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;

/// Author: Aunh
/// 22-08-2012
namespace SMAS.Web.Areas.AcademicYearArea.Controllers
{
    public class AcademicYearController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly GlobalInfo globalInfo = new GlobalInfo();
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        public AcademicYearController(IAcademicYearBusiness AcademicYearBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubject,
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            ISemeterDeclarationBusiness SemeterDeclarationBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IRestoreDataBusiness RestoreDataBusiness,
            IRestoreDataDetailBusiness RestoreDataDetailBusiness
            )
        {
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
        }

        public ViewResult Index()
        {
            Paginate<AcademicYearForm> paging = this._Search();
            SetViewDataPermission("AcademicYear", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            ViewData["IsVNEN"] = objSP.IsNewSchoolModel.HasValue && objSP.IsNewSchoolModel.Value;
            return View(paging);
        }

        private AcademicYearForm PrepareCreate()
        {

            AcademicYearForm frm = new AcademicYearForm();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = globalInfo.SchoolID;

            IQueryable<AcademicYear> lst = AcademicYearBusiness.Search(dic);
            lst = lst.OrderByDescending(o => o.Year);

            AcademicYear obj = lst.FirstOrDefault();
            int curYear = (int)DateTime.Now.Year;

            IQueryable<AcademicYear> lsAca = AcademicYearBusiness.SearchBySchool(globalInfo.SchoolID.Value).Where(o => o.Year < curYear);
            frm.hasLastYear = false;
            frm.canEdit = true;
            if (lsAca.Count() > 0)
            {
                frm.hasLastYear = true;
            }
            if (obj != null)
            {
                frm.Year = curYear;
                AcademicYearOfProvince proAca = GetAcademicYearOfProvince(curYear);
                if (proAca != null)
                {
                    Utils.Utils.BindTo(proAca, frm);
                    if (proAca.IsSchoolEdit.HasValue)
                    {
                        if (!proAca.IsSchoolEdit.Value)
                        {
                            frm.canEdit = false;
                        }
                    }
                }
                else
                {
                    int yearStartFirst = frm.Year.Value;
                    int monthStartFirst = obj.FirstSemesterStartDate.Value.Month;
                    int dayStartFirst = obj.FirstSemesterStartDate.Value.Day;
                    if (monthStartFirst == 2 && dayStartFirst > DateTime.DaysInMonth(yearStartFirst, monthStartFirst))
                    {
                        dayStartFirst = DateTime.DaysInMonth(yearStartFirst, monthStartFirst);
                    }

                    frm.FirstSemesterStartDate = new DateTime(yearStartFirst, monthStartFirst, dayStartFirst);
                    int yearFirst = frm.Year.Value + obj.FirstSemesterEndDate.Value.Year - obj.FirstSemesterStartDate.Value.Year;
                    int monthFirst = obj.FirstSemesterEndDate.Value.Month;
                    int dayFirst = obj.FirstSemesterEndDate.Value.Day;
                    if (monthFirst == 2 && dayFirst > DateTime.DaysInMonth(yearFirst, monthFirst))
                    {
                        dayFirst = DateTime.DaysInMonth(yearFirst, monthFirst);
                    }
                    frm.FirstSemesterEndDate = new DateTime(yearFirst, monthFirst, dayFirst);

                    int yearStartSecond = frm.FirstSemesterEndDate.Value.Year + obj.SecondSemesterStartDate.Value.Year - obj.FirstSemesterEndDate.Value.Year;
                    int monthStartSecond = obj.SecondSemesterStartDate.Value.Month;
                    int dayStartSecond = obj.SecondSemesterStartDate.Value.Day;
                    if (monthStartSecond == 2 && dayStartSecond > DateTime.DaysInMonth(yearStartSecond, monthStartSecond))
                    {
                        dayStartSecond = DateTime.DaysInMonth(yearStartSecond, monthStartSecond);
                    }
                    frm.SecondSemesterStartDate = new DateTime(yearStartSecond, monthStartSecond, dayStartSecond);

                    int yearSecond = frm.SecondSemesterStartDate.Value.Year + obj.SecondSemesterEndDate.Value.Year - obj.SecondSemesterStartDate.Value.Year;
                    int monthSecond = obj.SecondSemesterEndDate.Value.Month;
                    int daySecond = obj.SecondSemesterEndDate.Value.Day;
                    if (monthSecond == 2 && dayFirst > DateTime.DaysInMonth(yearSecond, monthSecond))
                    {
                        daySecond = DateTime.DaysInMonth(yearSecond, monthSecond);
                    }
                    frm.SecondSemesterEndDate = new DateTime(yearSecond, monthSecond, daySecond);
                }
                ViewBag.SchoolProfile = obj.SchoolProfile;
            }
            else
            {
                // khong co du lieu cu thi thiet lap du lieu mac dinh
                setDefaulDateTime(frm, curYear);
            }
            return frm;
        }

        private AcademicYearOfProvince GetAcademicYearOfProvince(int Year)
        {
            int? ProvinceID = SchoolProfileBusiness.Find(globalInfo.SchoolID).ProvinceID;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = Year;
            dic["IsActive"] = true;
            IQueryable<AcademicYearOfProvince> lst = AcademicYearOfProvinceBusiness.SearchByProvince(ProvinceID.Value, dic);

            AcademicYearOfProvince obj = lst.FirstOrDefault();

            return obj;
        }

        /**
         * Hien thi trang Tao moi
         **/

        public ActionResult Create()
        {
            return View(PrepareCreate());
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadProvinceAcademicYear(int Year)
        {
            AcademicYearForm frm = new AcademicYearForm();
            IQueryable<AcademicYear> lsAca = AcademicYearBusiness.SearchBySchool(globalInfo.SchoolID.Value).Where(o => o.Year < Year);
            frm.hasLastYear = false;
            frm.canEdit = true;
            if (lsAca.Count() > 0)
            {
                frm.hasLastYear = true;
            }
            AcademicYearOfProvince obj = GetAcademicYearOfProvince(Year);
            if (obj != null)
            {
                Utils.Utils.BindTo(obj, frm);
                if (obj.IsSchoolEdit.HasValue)
                {
                    frm.canEdit = obj.IsSchoolEdit.Value;
                }
            }
            else
            {
                frm.canEdit = true;
                setDefaulDateTime(frm, Year);

            }
            return Json(frm);
        }

        /// <summary>
        /// Quanglm
        /// Lay thoi gian mac dinh
        /// </summary>
        /// <param name="frm"></param>
        private void setDefaulDateTime(AcademicYearForm frm, int Year)
        {
            if (frm != null)
            {
                // Lay thoi gian mac dinh
                String StringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"] + Year;
                String StringFirstEndDate = ConfigurationManager.AppSettings["EndFirstAcademicYear"] + (Year + 1).ToString();
                String StringSecondStartDate = ConfigurationManager.AppSettings["StartSecondAcademicYear"] + (Year + 1).ToString();
                String StringSecondEndDate = ConfigurationManager.AppSettings["EndSecondAcademicYear"] + (Year + 1).ToString();
                frm.FirstSemesterStartDate = DateTime.Parse(StringFirstStartDate);
                frm.FirstSemesterEndDate = DateTime.Parse(StringFirstEndDate);
                frm.SecondSemesterStartDate = DateTime.Parse(StringSecondStartDate);
                frm.SecondSemesterEndDate = DateTime.Parse(StringSecondEndDate);
            }
        }

        #region Action
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        public JsonResult Create(AcademicYearForm frm)
        {
            //CheckPermissionForAction(frm.AcademicYearID, "AcademicYear");

            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("AcademicYear", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            AcademicYear obj = new AcademicYear();
            //Đổ dữ liệu vào obj
            Utils.Utils.BindTo(frm, obj);

            obj.DisplayTitle = obj.Year + " - " + (obj.Year + 1);
            obj.SchoolID = GlobalInfo.SchoolID == null ? 0 : GlobalInfo.SchoolID.Value;
            obj.IsActive = true;

            //Lấy ra thông tin năm học của sở
            AcademicYearOfProvince pro = GetAcademicYearOfProvince(frm.Year.Value);

            //Áp dụng theo sở
            if (pro != null && pro.IsSchoolEdit.HasValue && !pro.IsSchoolEdit.Value)
            {
                obj.FirstSemesterStartDate = SMAS.Web.Utils.Utils.FixDateTime(pro.FirstSemesterStartDate.Value);

                obj.FirstSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(pro.FirstSemesterEndDate.Value);

                obj.SecondSemesterStartDate = SMAS.Web.Utils.Utils.FixDateTime(pro.SecondSemesterStartDate.Value);

                obj.SecondSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(pro.SecondSemesterEndDate.Value);
            }
            //Gan lai gio
            obj.FirstSemesterStartDate = obj.FirstSemesterStartDate.Value.Date;

            obj.FirstSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(obj.FirstSemesterEndDate.Value);

            obj.SecondSemesterStartDate = obj.SecondSemesterStartDate.Value.Date;

            obj.SecondSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(obj.SecondSemesterEndDate.Value);

            IQueryable<PeriodDeclaration> lspdhk1 = PeriodDeclarationBusiness.All.Where(o => o.AcademicYearID == frm.AcademicYearID && o.SchoolID == GlobalInfo.SchoolID && o.Semester == 1);
            IQueryable<PeriodDeclaration> lspdhk2 = PeriodDeclarationBusiness.All.Where(o => o.AcademicYearID == frm.AcademicYearID && o.SchoolID == GlobalInfo.SchoolID && o.Semester == 1);

            //HK 1
            if (lspdhk1.Count() > 0)
            {

                List<PeriodDeclaration> lstpdhk1 = lspdhk1.ToList();
                PeriodDeclaration pd1 = lstpdhk1.OrderBy(o => o.FromDate).FirstOrDefault();
                PeriodDeclaration pd2 = lstpdhk1.OrderByDescending(o => o.EndDate).FirstOrDefault();

                //Ngày đầu tiên của đợt 
                DateTime stardate = pd1.FromDate.Value;
                //Ngày kết thúc của đợt
                DateTime enddate = pd2.EndDate.Value;
                List<object> Params1 = new List<object>();
                Params1.Add(pd1.Resolution + "(" + stardate.Date.ToString("dd/MM/yyyy") + ")");
                //Năm học phải chứa đợt
                if (frm.FirstSemesterStartDate > stardate)
                {
                    throw new BusinessException("Validate_Period_First_1", Params1);
                }
                List<object> Params2 = new List<object>();
                Params2.Add(pd2.Resolution + "(" + enddate.Date.ToString("dd/MM/yyyy") + ")");
                if (frm.FirstSemesterEndDate < enddate)
                {
                    throw new BusinessException("Validate_Period_Second_1", Params2);
                }
            }
            if (lspdhk2.Count() > 0)
            {
                List<PeriodDeclaration> lstpdhk2 = lspdhk2.ToList();
                PeriodDeclaration pd1 = lstpdhk2.OrderBy(o => o.FromDate).FirstOrDefault();
                PeriodDeclaration pd2 = lstpdhk2.OrderByDescending(o => o.EndDate).FirstOrDefault();

                //Ngày đầu tiên của đợt 
                DateTime stardate = pd1.FromDate.Value;
                //Ngày kết thúc của đợt
                DateTime enddate = pd2.EndDate.Value;
                List<object> Params1 = new List<object>();
                Params1.Add(pd1.Resolution + "(" + stardate.Date.ToString("dd/MM/yyyy") + ")");
                //Năm học phải chứa đợt
                if (frm.SecondSemesterStartDate > stardate)
                {
                    throw new BusinessException("Validate_Period_First_2", Params1);
                }
                List<object> Params2 = new List<object>();
                Params2.Add(pd2.Resolution + "(" + enddate.Date.ToString("dd/MM/yyyy") + ")");
                if (frm.SecondSemesterEndDate < enddate)
                {
                    throw new BusinessException("Validate_Period_Second_2", Params2);
                }
            }
            List<object> Params = new List<object>();
            Params.Add(obj.Year.ToString());
            Params.Add((obj.Year + 1).ToString());
            //Năm học sau khi sửa phải nằm trong khoảng năm học hiển thị
            if (frm.FirstSemesterStartDate.Value.Year < obj.Year || frm.FirstSemesterStartDate.Value.Year > (obj.Year + 1))
            {
                throw new BusinessException("Validate_StartDate_Year", Params);
            }

            if (frm.SecondSemesterEndDate.Value.Year < obj.Year || frm.SecondSemesterEndDate.Value.Year > (obj.Year + 1))
            {
                throw new BusinessException("Validate_SecondDate_Year", Params);
            }

            //Lấy ra năm học cũ
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = obj.SchoolID;
            IQueryable<AcademicYear> lst = AcademicYearBusiness.Search(dic).Where(o => o.Year < obj.Year).OrderByDescending(o => o.Year);
            AcademicYear lastYear = lst.FirstOrDefault();

            // Ke thua  cau hinh so con diem
            if (lastYear != null)
            {
                // Lay thông tin khai báo số con điểm
                IQueryable<SemeterDeclaration> lssd = SemeterDeclarationBusiness.All.Where(o => o.AcademicYearID == lastYear.AcademicYearID && o.SchoolID == _globalInfo.SchoolID).OrderByDescending(o => o.AppliedDate);
                if (lssd.Count() > 0)
                {
                    SemeterDeclaration sd = lssd.FirstOrDefault();
                    // Them cau hinh diem cho hoc ky 1
                    SemeterDeclaration sdnew = new SemeterDeclaration();
                    sdnew.CoefficientInterview = sd.CoefficientInterview;
                    sdnew.CoefficientSemester = sd.CoefficientSemester;
                    sdnew.CoefficientTwice = sd.CoefficientTwice;
                    sdnew.CoefficientWriting = sd.CoefficientWriting;
                    sdnew.InterviewMark = sd.InterviewMark;
                    sdnew.SchoolID = sd.SchoolID;
                    sdnew.TwiceCoeffiecientMark = sd.TwiceCoeffiecientMark;
                    sdnew.WritingMark = sd.WritingMark;
                    sdnew.SemesterMark = sd.SemesterMark;
                    sdnew.Year = sd.Year + 1;
                    sdnew.AppliedDate = sd.AppliedDate;
                    sdnew.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    sdnew.Resolution = SystemParamsInFile.SEMESTER_I;
                    obj.SemeterDeclarations.Add(sdnew);
                    // Them cau hinh diem cho hoc ky 2
                    sdnew = new SemeterDeclaration();
                    sdnew.CoefficientInterview = sd.CoefficientInterview;
                    sdnew.CoefficientSemester = sd.CoefficientSemester;
                    sdnew.CoefficientTwice = sd.CoefficientTwice;
                    sdnew.CoefficientWriting = sd.CoefficientWriting;
                    sdnew.InterviewMark = sd.InterviewMark;
                    sdnew.SchoolID = sd.SchoolID;
                    sdnew.TwiceCoeffiecientMark = sd.TwiceCoeffiecientMark;
                    sdnew.WritingMark = sd.WritingMark;
                    sdnew.SemesterMark = sd.SemesterMark;
                    sdnew.Year = sd.Year + 1;
                    sdnew.AppliedDate = sd.AppliedDate;
                    sdnew.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    sdnew.Resolution = SystemParamsInFile.SEMESTER_II;
                    obj.SemeterDeclarations.Add(sdnew);
                }
            }



            // Neu la truong mau giao, mam non thi se thuc hien
            int? appliedLevel = GlobalInfo.AppliedLevel;
            if (appliedLevel.HasValue)
            {
                if (appliedLevel.Value.Equals(SystemParamsInFile.APPLIED_LEVEL_CRECHE)
                    || appliedLevel.Value.Equals(SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN))
                {
                    var itemSaved1 = AcademicYearBusiness.InsertAcademicYearOfChildren(obj, frm.chkGetlast);
                    this.AcademicYearBusiness.Save();
                    if (!GlobalInfo.AcademicYearID.HasValue)
                    {
                        globalInfo.AcademicYearID = itemSaved1.AcademicYearID;
                    }
                    this.SetListAcademicYear2Session();

                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
            }



            //Nếu năm học cũ có giá trị và người dùng check chọn kế thừa dữ liệu cũ
            //Thực hiện chuyển dữ liệu của năm học vừa kết thúc sang năm học mới


            if (lastYear != null)
            {
                if (frm.chkGetlast)
                {
                    //Ke thua lai cau hinh chung

                    obj.RankingCriteria = lastYear.RankingCriteria;
                    obj.IsSecondSemesterToSemesterAll = lastYear.IsSecondSemesterToSemesterAll;
                    obj.IsTeacherCanViewAll = lastYear.IsTeacherCanViewAll;
                    obj.IsHeadTeacherCanSendSMS = lastYear.IsHeadTeacherCanSendSMS;
                    obj.IsSubjectTeacherCanSendSMS = lastYear.IsSubjectTeacherCanSendSMS;
                    obj.IsShowAvatar = lastYear.IsShowAvatar;
                    obj.IsShowCalendar = lastYear.IsShowCalendar;

                    // Doi voi cac cap hoc con lai
                    IDictionary<string, object> dic2 = new Dictionary<string, object>();
                    dic2["AcademicYearID"] = lastYear.AcademicYearID;
                    dic2["SchoolID"] = obj.SchoolID;
                    dic["AppliedLevel"] = globalInfo.AppliedLevel;

                    //Thông tin  lớp học và thông tin môn cho lớp học
                    IQueryable<ClassProfile> classProfiles = this.ClassProfileBusiness.Search(dic2);
                    List<ClassProfile> lstCP = classProfiles.ToList();
                    obj.ClassProfiles = new List<ClassProfile>();
                    foreach (ClassProfile cp in lstCP)
                    {
                        ClassProfile newcp = new ClassProfile();
                        this.CopyClassProfile(cp, newcp);
                        obj.ClassProfiles.Add(newcp);

                    }
                    //Thông tin tổ bộ môn tự động kế thừa do ko lưu tổ bộ môn theo năm học


                    //Thông tin môn học
                    List<SchoolSubject> schoolSubject = this.SchoolSubjectBusiness.Search(dic2).ToList();
                    foreach (var ss in schoolSubject)
                    {
                        SchoolSubject newss = new SchoolSubject();
                        newss.AcademicYearID = globalInfo.AcademicYearID.Value;
                        newss.AppliedType = ss.AppliedType;
                        newss.EducationLevelID = ss.EducationLevelID;
                        newss.EndDate = ss.EndDate;
                        newss.FirstSemesterCoefficient = ss.FirstSemesterCoefficient;
                        newss.IsCommenting = ss.IsCommenting;
                        newss.SchoolID = ss.SchoolID;
                        newss.SecondSemesterCoefficient = ss.SecondSemesterCoefficient;
                        newss.SectionPerWeekFirstSemester = ss.SectionPerWeekFirstSemester;
                        newss.SectionPerWeekSecondSemester = ss.SectionPerWeekFirstSemester;
                        newss.StartDate = ss.StartDate;
                        newss.SubjectID = ss.SubjectID;
                        obj.SchoolSubjects.Add(newss);
                    }
                    #region bỏ luồng kế thừa đợt
                    //Đợt
                    //IDictionary<string, object> dicperi = new Dictionary<string, object>();
                    //dicperi["AcademicYearID"] = lastYear.AcademicYearID;
                    //List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicperi).ToList();
                    //foreach (PeriodDeclaration pd in lstPeriod)
                    //{
                    //    if (pd.FromDate.Value.AddYears(1) >= obj.FirstSemesterStartDate && pd.EndDate.Value.AddYears(1) <= obj.FirstSemesterEndDate)
                    //    {
                    //        PeriodDeclaration newpd = new PeriodDeclaration();
                    //        newpd.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                    //        newpd.ContaintSemesterMark = pd.ContaintSemesterMark;
                    //        newpd.EndDate = pd.EndDate.HasValue ? pd.EndDate.Value.AddYears(1) : pd.EndDate;
                    //        newpd.FromDate = pd.FromDate.HasValue ? pd.FromDate.Value.AddYears(1) : pd.FromDate;
                    //        newpd.InterviewMark = pd.InterviewMark;
                    //        newpd.IsLock = false;
                    //        newpd.Resolution = pd.Resolution;
                    //        newpd.SchoolID = pd.SchoolID;
                    //        newpd.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    //        //newpd.StartIndexOfInterviewMark = pd.StartIndexOfInterviewMark;
                    //        //newpd.StartIndexOfTwiceCoeffiecientMark = pd.StartIndexOfTwiceCoeffiecientMark;
                    //        //newpd.StartIndexOfWritingMark = pd.StartIndexOfWritingMark;
                    //        newpd.TwiceCoeffiecientMark = pd.TwiceCoeffiecientMark;
                    //        newpd.WritingMark = pd.WritingMark;
                    //        newpd.StrInterviewMark = pd.StrInterviewMark;
                    //        newpd.StrWritingMark = pd.StrWritingMark;
                    //        newpd.StrTwiceCoeffiecientMark = pd.StrTwiceCoeffiecientMark;
                    //        newpd.Year = pd.Year + 1;
                    //        obj.PeriodDeclarations.Add(newpd);
                    //    }
                    //    if (pd.FromDate.Value.AddYears(1) >= obj.SecondSemesterStartDate && pd.EndDate.Value.AddYears(1) <= obj.SecondSemesterEndDate)
                    //    {
                    //        PeriodDeclaration newpd = new PeriodDeclaration();
                    //        newpd.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                    //        newpd.ContaintSemesterMark = pd.ContaintSemesterMark;
                    //        newpd.EndDate = pd.EndDate.HasValue ? pd.EndDate.Value.AddYears(1) : pd.EndDate;
                    //        newpd.FromDate = pd.FromDate.HasValue ? pd.FromDate.Value.AddYears(1) : pd.FromDate;
                    //        newpd.InterviewMark = pd.InterviewMark;
                    //        newpd.IsLock = false;
                    //        newpd.Resolution = pd.Resolution;
                    //        newpd.SchoolID = pd.SchoolID;
                    //        newpd.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    //        //newpd.StartIndexOfInterviewMark = pd.StartIndexOfInterviewMark;
                    //        //newpd.StartIndexOfTwiceCoeffiecientMark = pd.StartIndexOfTwiceCoeffiecientMark;
                    //        //newpd.StartIndexOfWritingMark = pd.StartIndexOfWritingMark;
                    //        newpd.TwiceCoeffiecientMark = pd.TwiceCoeffiecientMark;
                    //        newpd.WritingMark = pd.WritingMark;
                    //        newpd.StrInterviewMark = pd.StrInterviewMark;
                    //        newpd.StrWritingMark = pd.StrWritingMark;
                    //        newpd.StrTwiceCoeffiecientMark = pd.StrTwiceCoeffiecientMark;
                    //        newpd.Year = pd.Year + 1;
                    //        obj.PeriodDeclarations.Add(newpd);
                    //    }
                    //}
                    #endregion
                }
            }


            AcademicYear itemSaved = this.AcademicYearBusiness.Insert(obj);
            this.AcademicYearBusiness.Save();


            if (!GlobalInfo.AcademicYearID.HasValue)
            {
                globalInfo.AcademicYearID = itemSaved.AcademicYearID;
            }

            this.SetListAcademicYear2Session();


            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }

        public ActionResult Edit(int id)
        {
            CheckPermissionForAction(id, "AcademicYear");
            AcademicYear model = this.AcademicYearBusiness.Find(id);
            AcademicYearForm frm = new AcademicYearForm();
            if (model.ClassProfiles.Count == 0)
            {
                frm.hasLastYear = true;
            }

            Utils.Utils.BindTo(model, frm);
            frm.canEdit = true;
            AcademicYearOfProvince obj = GetAcademicYearOfProvince(model.Year);
            if (obj != null)
            {
                //Utils.Utils.BindTo(obj, frm);
                if (obj.IsSchoolEdit.HasValue)
                {
                    frm.canEdit = obj.IsSchoolEdit.Value;
                }
            }
            ViewBag.SchoolProfile = this.SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            return View(frm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Edit(AcademicYearForm frm)
        {
            CheckPermissionForAction(frm.AcademicYearID, "AcademicYear");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("AcademicYear", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            AcademicYear obj = this.AcademicYearBusiness.Find(frm.AcademicYearID);
            int y = obj.Year;


            IQueryable<PeriodDeclaration> lspdhk1 = PeriodDeclarationBusiness.All.Where(o => o.AcademicYearID == frm.AcademicYearID && o.SchoolID == GlobalInfo.SchoolID && o.Semester == 1);
            IQueryable<PeriodDeclaration> lspdhk2 = PeriodDeclarationBusiness.All.Where(o => o.AcademicYearID == frm.AcademicYearID && o.SchoolID == GlobalInfo.SchoolID && o.Semester == 2);

            //HK 1
            if (lspdhk1.Count() > 0)
            {

                List<PeriodDeclaration> lstpdhk1 = lspdhk1.ToList();
                PeriodDeclaration pd1 = lstpdhk1.OrderBy(o => o.FromDate).FirstOrDefault();
                PeriodDeclaration pd2 = lstpdhk1.OrderByDescending(o => o.EndDate).FirstOrDefault();

                //Ngày đầu tiên của đợt 
                DateTime stardate = pd1.FromDate.Value;
                //Ngày kết thúc của đợt
                DateTime enddate = pd2.EndDate.Value;
                List<object> Params1 = new List<object>();
                Params1.Add(pd1.Resolution + "(" + stardate.Date.ToString("dd/MM/yyyy") + ")");
                //Năm học phải chứa đợt
                if (frm.FirstSemesterStartDate.Value.Date > stardate.Date)
                {
                    throw new BusinessException("Validate_Period_First_1", Params1);
                }
                List<object> Params2 = new List<object>();
                Params2.Add(pd2.Resolution + "(" + enddate.Date.ToString("dd/MM/yyyy") + ")");
                if (frm.FirstSemesterEndDate.Value.Date < enddate.Date)
                {
                    throw new BusinessException("Validate_Period_Second_1", Params2);
                }
            }
            //HK2
            if (lspdhk2.Count() > 0)
            {
                List<PeriodDeclaration> lstpdhk2 = lspdhk2.ToList();
                PeriodDeclaration pd1 = lstpdhk2.OrderBy(o => o.FromDate).FirstOrDefault();
                PeriodDeclaration pd2 = lstpdhk2.OrderByDescending(o => o.EndDate).FirstOrDefault();

                //Ngày đầu tiên của đợt 
                DateTime stardate = pd1.FromDate.Value;
                //Ngày kết thúc của đợt
                DateTime enddate = pd2.EndDate.Value;
                List<object> Params1 = new List<object>();
                Params1.Add(pd1.Resolution + "(" + stardate.Date.ToString("dd/MM/yyyy") + ")");
                //Năm học phải chứa đợt
                if (frm.SecondSemesterStartDate.Value.Date > stardate.Date)
                {
                    throw new BusinessException("Validate_Period_First_2", Params1);
                }
                List<object> Params2 = new List<object>();
                Params2.Add(pd2.Resolution + "(" + enddate.Date.ToString("dd/MM/yyyy") + ")");
                if (frm.SecondSemesterEndDate.Value.Date < enddate.Date)
                {
                    throw new BusinessException("Validate_Period_Second_2", Params2);
                }
            }
            List<object> Params = new List<object>();
            Params.Add(obj.Year.ToString());
            Params.Add((obj.Year + 1).ToString());
            //Năm học sau khi sửa phải nằm trong khoảng năm học hiển thị
            if (frm.FirstSemesterStartDate.Value.Year < obj.Year || frm.FirstSemesterStartDate.Value.Year > (obj.Year + 1))
            {
                throw new BusinessException("Validate_StartDate_Year", Params);
            }

            if (frm.SecondSemesterEndDate.Value.Year < obj.Year || frm.SecondSemesterEndDate.Value.Year > (obj.Year + 1))
            {
                throw new BusinessException("Validate_SecondDate_Year", Params);
            }

            obj.FirstSemesterStartDate = frm.FirstSemesterStartDate.Value.Date;

            obj.FirstSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(frm.FirstSemesterEndDate.Value);

            obj.SecondSemesterStartDate = frm.SecondSemesterStartDate.Value.Date;

            obj.SecondSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(frm.SecondSemesterEndDate.Value);



            //Utils.Utils.BindTo(frm, obj);

            obj.Year = y;
            obj.DisplayTitle = obj.Year + "-" + (obj.Year + 1);
            obj.SchoolID = GlobalInfo.SchoolID == null ? 0 : GlobalInfo.SchoolID.Value;

            AcademicYearOfProvince pro = GetAcademicYearOfProvince(obj.Year);

            if (pro != null && pro.IsSchoolEdit.Value == false)
            {
                obj.FirstSemesterStartDate = SMAS.Web.Utils.Utils.FixDateTime(pro.FirstSemesterStartDate.Value);

                obj.FirstSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(pro.FirstSemesterEndDate.Value);

                obj.SecondSemesterStartDate = SMAS.Web.Utils.Utils.FixDateTime(pro.SecondSemesterStartDate.Value);

                obj.SecondSemesterEndDate = SMAS.Web.Utils.Utils.FixDateTime(pro.SecondSemesterEndDate.Value);
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = obj.SchoolID;
            dic["Year"] = obj.Year - 1;
            dic["isActive"] = true;
            IQueryable<AcademicYear> lst = AcademicYearBusiness.Search(dic);

            AcademicYear lastYear = lst.FirstOrDefault();
            if (lastYear != null && frm.chkGetlast && obj.ClassProfiles.Count == 0)
            {
                IDictionary<string, object> dic2 = new Dictionary<string, object>();
                dic2["AcademicYearID"] = lastYear.AcademicYearID;
                IQueryable<ClassProfile> classProfiles = this.ClassProfileBusiness.Search(dic2);
                List<ClassProfile> lstCP = classProfiles.ToList();
                obj.ClassProfiles = new List<ClassProfile>();
                foreach (ClassProfile cp in lstCP)
                {
                    ClassProfile newcp = new ClassProfile();
                    this.CopyClassProfile(cp, newcp);
                    obj.ClassProfiles.Add(newcp);
                }

            }
            else if (lastYear == null && frm.chkGetlast)
            {
                frm.hasLastYear = false;
                return Json(new JsonMessage(Res.Get("AcademicYear_Message_NoLastYear"), JsonMessage.ERROR));
            }

            this.AcademicYearBusiness.Update(obj);
            this.AcademicYearBusiness.Save();

            if (obj.ClassProfiles.Count == 0)
            {
                frm.hasLastYear = true;
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        public JsonResult Delete(int id)
        {
            if (GetMenupermission("AcademicYear", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id, "AcademicYear");
            AcademicYear objAcademicYear = new AcademicYear();
            objAcademicYear = AcademicYearBusiness.Find(id);
            objAcademicYear.IsActive = false;
            this.AcademicYearBusiness.Save();
            #region xoa nam hoc thuc hien xoa du lieu cac bang lien quan
            //Xoa hoc sinh trong pupilofclass
            List<int> lstPupilID = PupilOfClassBusiness.All.Where(p => p.AcademicYearID == id).Select(p => p.PupilID).Distinct().ToList();

            //Luu thong tin phuc vu viec phuc hoi du lieu
            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = objAcademicYear.AcademicYearID,
                SCHOOL_ID = objAcademicYear.SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_YEAR,
                SHORT_DESCRIPTION = string.Format("Năm học {0}", objAcademicYear.DisplayTitle),
            };

            List<RESTORE_DATA_DETAIL> lstRestoreDetail = PupilProfileBusiness.DeleteListPupilProfile(_globalInfo.UserAccountID, lstPupilID, _globalInfo.SchoolID.Value, id, objRes);

            ///TH tra ve danh sach rong. khoi tao lai danh sach
            if (lstRestoreDetail == null)
            {
                lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
            }

            //update lai truong isActive trong bang ClassProfile
            List<ClassProfile> lstCP = ClassProfileBusiness.SearchByAcademicYear(id, new Dictionary<string, object>()).ToList();
            List<int> lstClassID = lstCP.Select(p => p.ClassProfileID).Distinct().ToList();
            ClassProfile objCP = null;
            int partitionId = UtilsBusiness.GetPartionId(objAcademicYear.SchoolID);
            for (int i = 0; i < lstCP.Count; i++)
            {
                objCP = lstCP[i];
                objCP.IsActive = false;
                //Tao du lieu de phuc hoi lop hoc
                RestoreClassProfileBO objClassRes = new RestoreClassProfileBO
                {
                    ClassId = objCP.ClassProfileID,
                    ClassName = objCP.DisplayName,
                    ReplaceClassName = "",
                    SqlUndo = RestoreDataConstant.SQL_UNDO_CLASS
                };
                RESTORE_DATA_DETAIL objRestoreDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = partitionId,
                    ORDER_ID = 3,
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRes.SCHOOL_ID,
                    SQL_DELETE = "",
                    SQL_UNDO = JsonConvert.SerializeObject(objClassRes),
                    TABLE_NAME = RestoreDataConstant.TABLE_CLASS_PROFILE
                };
                lstRestoreDetail.Add(objRestoreDetail);

            }
            ClassProfileBusiness.Save();

            RESTORE_DATA_DETAIL objRestoreYear = new RESTORE_DATA_DETAIL
            {
                ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                CREATED_DATE = DateTime.Now,
                END_DATE = null,
                IS_VALIDATE = 1,
                LAST_2DIGIT_NUMBER_SCHOOL = partitionId,
                ORDER_ID = 4,
                RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                SCHOOL_ID = objRes.SCHOOL_ID,
                SQL_DELETE = "",
                SQL_UNDO = string.Format(RestoreDataConstant.SQL_UNDO_ACADEMIC_YEAR, objRes.ACADEMIC_YEAR_ID),
                TABLE_NAME = RestoreDataConstant.TABLE_ACADEMIC_YEAR
            };
            lstRestoreDetail.Add(objRestoreYear);
            if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
            {
                RestoreDataBusiness.Insert(objRes);
                RestoreDataBusiness.Save();
                RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
            }

            #endregion
            this.SetListAcademicYear2Session();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion

        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(int page = 1, string orderBy = "")
        {
            SetViewDataPermission("AcademicYear", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Paginate<AcademicYearForm> paging = this._Search(page, orderBy);
            GridModel<AcademicYearForm> gm = new GridModel<AcademicYearForm>(paging.List);
            gm.Total = paging.total;
            return View(gm);
        }

        private Paginate<AcademicYearForm> _Search(int page = 1, string orderBy = "")
        {
            SetViewDataPermission("AcademicYear", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = GlobalInfo.SchoolID == null ? 0 : GlobalInfo.SchoolID.Value;

            IQueryable<AcademicYear> lst = AcademicYearBusiness.Search(dic).Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value));

            // Sap xep
            string sort = "";
            string order = "";
            if (orderBy != "")
            {
                sort = orderBy.Split('-')[0];
                order = orderBy.Split('-')[1];
            }
            if (sort == "")
                lst = lst.OrderByDescending(o => o.Year);
            else if (sort == "DisplayTitle")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.DisplayTitle);
                }
                else
                {
                    lst = lst.OrderBy(o => o.DisplayTitle);
                }
            }


            IQueryable<AcademicYearForm> res = lst.Select(o => new AcademicYearForm
            {
                AcademicYearID = o.AcademicYearID,
                Year = o.Year,
                DisplayTitle = o.DisplayTitle,
                FirstSemesterStartDate = o.FirstSemesterStartDate,
                FirstSemesterEndDate = o.FirstSemesterEndDate,
                SecondSemesterStartDate = o.SecondSemesterStartDate,
                SecondSemesterEndDate = o.SecondSemesterEndDate
            });

            // Thuc hien phan trang tung phan
            res = res.OrderByDescending(o => o.Year);
            Paginate<AcademicYearForm> paging = new Paginate<AcademicYearForm>(res);
            paging.page = page;
            paging.paginate();

            return paging;
        }

        private void CopyClassProfile(ClassProfile source, ClassProfile dest)
        {
            Utils.Utils.BindTo(source, dest, true);

            dest.ClassProfileID = 0;
            dest.AcademicYearID = 0;
            dest.AcademicYear = null;
            //Chiendd1: 07/07/2015, Bo sung partitionId cho bang ClassSubject
            int partitionId = UtilsBusiness.GetPartionId(source.SchoolID);
            dest.ClassSubjects = new List<ClassSubject>();
            foreach (ClassSubject cs in source.ClassSubjects)
            {
                ClassSubject ncs = new ClassSubject();
                Utils.Utils.BindTo(cs, ncs, true);

                ncs.ClassSubjectID = 0;
                ncs.ClassID = 0;
                ncs.ClassProfile = null;
                ncs.Last2digitNumberSchool = partitionId;
                dest.ClassSubjects.Add(ncs);
            }
        }

        private void SetListAcademicYear2Session()
        {
            List<AcademicYear> acaResults = AcademicYearBusiness.SearchBySchool(globalInfo.SchoolID.Value).Where(p => !p.IsActive.HasValue || (p.IsActive.HasValue && p.IsActive.Value))
               .OrderByDescending(o => o.Year).ToList();
            // List danh sach nam hoc luu trong session
            List<AcademicYearForm> listAcaForm = new List<AcademicYearForm>();
            if (acaResults != null && acaResults.Count() > 0)
            {
                int Selected_academicYear_Index = 0;
                // Kiem tra xem co nam hoc ung voi thoi gian hien tai hay khong
                DateTime curDate = DateTime.Now.Date;
                AcademicYear curAca = null;
                int count = acaResults.Count;
                for (int i = 0; i < count; i++)
                {
                    AcademicYear item = acaResults[i];
                    AcademicYearForm itemAcadForm = new AcademicYearForm();
                    Utils.Utils.BindTo(item, itemAcadForm);
                    listAcaForm.Add(itemAcadForm);
                    if (item.FirstSemesterStartDate.Value.Date <= curDate && item.SecondSemesterEndDate >= curDate)
                    {
                        Selected_academicYear_Index = i;
                        curAca = item;
                        Session[SMAS.Web.Constants.GlobalConstants.ACADEMICYEAR_ID] = item.AcademicYearID;
                    }
                }
                if (curAca != null)
                {
                    // Day la nam hoc hien tai                    
                    Session[SMAS.Web.Constants.GlobalConstants.IsCurrentYear] = true;
                    int Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    if ((curAca.FirstSemesterStartDate.Value.Date <= curDate) && (curAca.FirstSemesterStartDate >= curDate))
                    {
                        Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    }
                    else if ((curAca.SecondSemesterStartDate.Value.Date <= curDate) && (curAca.SecondSemesterEndDate >= curDate))
                    {
                        Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    }
                    Session[SMAS.Web.Constants.GlobalConstants.SEMESTER] = Semester;
                }
                else
                {
                    Session[SMAS.Web.Constants.GlobalConstants.IsCurrentYear] = false;
                }
                Session[SMAS.Web.Constants.GlobalConstants.SELECTED_ACADEMICYEAR_INDEX] = Selected_academicYear_Index;
                Session[SMAS.Web.Constants.GlobalConstants.LIST_ACADEMICYEAR] = new SelectList(listAcaForm, "AcademicYearID", "DisplayTitle");
            }
            else
            {
                Session[SMAS.Web.Constants.GlobalConstants.LIST_ACADEMICYEAR] = null;
                Session[SMAS.Web.Constants.GlobalConstants.ACADEMICYEAR_ID] = null;
            }
        }
    }
}
