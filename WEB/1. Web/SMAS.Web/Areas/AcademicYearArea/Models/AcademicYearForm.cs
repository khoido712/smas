using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.AcademicYearArea.Models
{
    public class AcademicYearForm
    {
        [ResourceDisplayName("AcademicYear_Label_Year")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int AcademicYearID { get; set; }

        [ResourceDisplayName("School")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SchoolID { get; set; }

        [ResourceDisplayName("AcademicYear_Control_Year")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        //[StringLength(4, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("class", "integer")]
        public Nullable<int> Year { get; set; }

        [ResourceDisplayName("AcademicYear_Control_FirstSemesterStartDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "FirstSemesterEndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FirstSemesterStartDate { get; set; }

        [ResourceDisplayName("AcademicYear_Control_FirstSemesterEndDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "SecondSemesterStartDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FirstSemesterEndDate { get; set; }

        [ResourceDisplayName("AcademicYear_Control_SecondSemesterStartDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS, "SecondSemesterEndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? SecondSemesterStartDate { get; set; }

        [ResourceDisplayName("AcademicYear_Control_SecondSemesterEndDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? SecondSemesterEndDate { get; set; }

        [ResourceDisplayName("AcademicYear_Control_GetLast")]
        public bool chkGetlast { get; set; }

        [ResourceDisplayName("AcademicYear_Control_Year")]
        public string DisplayTitle { get; set; }

        public bool hasLastYear { get; set; }
        public bool canEdit { get; set; }

        // QuangLM - 17/07/2013
        public Nullable<bool> IsShowAvatar { get; set; }
        public Nullable<bool> IsShowCalendar { get; set; }
        public bool IsTeacherCanViewAll { get; set; }
    }
}
