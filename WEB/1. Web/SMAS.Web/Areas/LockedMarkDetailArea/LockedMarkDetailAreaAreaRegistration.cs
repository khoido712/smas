﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LockedMarkDetailArea
{
    public class LockedMarkDetailAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LockedMarkDetailArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LockedMarkDetailArea_default",
                "LockedMarkDetailArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
