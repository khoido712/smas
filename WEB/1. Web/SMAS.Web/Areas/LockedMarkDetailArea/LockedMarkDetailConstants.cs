﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LockedMarkDetailArea
{
    public class LockedMarkDetailConstants
    {
        public const string LS_LockedMarkDetail = "ListLockedMarkDetail";
        public const string LS_EducationLevel = "ListEducationLevel";
        public const string LS_Class = "ListClass";
        public const string LS_Semester = "ListSemester";
        public const string LS_MarkType = "ListMarkType";
        public const string LS_ClassSubject = "ListClassSubject";
        public const string LS_SemesterDeclaration = "ListSemesterDeclaration";
        public const string M = "M";
        public const string P = "P";        
        public const string V = "V";
        public const string KTHK = "HK";
        public const string HK = "LHK";
        public const int AppliedClassType = 1;
        public const int AppliedEducationLevelType = 2;
        public const int AppliedSchoolType = 3;
        public const string MAX_MARKTYPEID = "MAX_MARKTYPEID";
    }
}