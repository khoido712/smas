﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.LockedMarkDetailArea.Models;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.LockedMarkDetailArea.Controllers
{
    public class LockedMarkDetailController : BaseController
    {
        //
        // GET: /LockedMarkDetailArea/LockedMarkDetail/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public LockedMarkDetailController(IClassProfileBusiness classProfileBusiness,
                                          ILockedMarkDetailBusiness lockedMarkDetailBusiness,
                                          IClassSubjectBusiness classSubjectBusiness,
                                          IMarkTypeBusiness markTypeBusiness,
                                          ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                          IEducationLevelBusiness educationLevelBusiness,
                                          ISchoolProfileBusiness schoolProfileBusiness
                                         )
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.LockedMarkDetailBusiness = lockedMarkDetailBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
            this.SemeterDeclarationBusiness = semeterDeclarationBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            SetViewDataPermission("LockedMarkDetail", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

        public void SetViewData()
        {
            //Đổ dữ liệu vào EducationLevelcombobox
            IEnumerable<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels;
            ViewData[LockedMarkDetailConstants.LS_EducationLevel] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            //Đổ dữ liệu vào ClassCombobox
            IEnumerable<ClassProfile> lsClass = new List<ClassProfile>();

            ViewData[LockedMarkDetailConstants.LS_Class] = new SelectList(lsClass, "ClassProfileID", "DisplayName");

            //Đổ dữ liệu vào SemesterCombobox
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.FIRST_SEMESTER.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SECOND_SEMESTER.ToString(), Res.Get("Common_Label_SecondSemester")));
            ViewData[LockedMarkDetailConstants.LS_Semester] = new SelectList(lsSemester, "key", "value");
        }


        [SkipCheckRole]

        //[ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingClassProfile(int? educationLevelID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (educationLevelID.HasValue)
            {
                dic["EducationLevelID"] = educationLevelID.Value;

                int? SchoolID = globalInfo.SchoolID;
                if (SchoolID.HasValue)
                {
                    dic["SchoolID"] = SchoolID.Value;
                }
                int? AcademicYearID = globalInfo.AcademicYearID;
                if (AcademicYearID.HasValue)
                {
                    dic["AcademicYearID"] = AcademicYearID.Value;
                }

                lstClass = this.ClassProfileBusiness.Search(dic).ToList();
            }
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }



        //[ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int ClassID, int SemesterID)
        {
            CheckPermissionForAction(ClassID, "ClassProfile");
            //GlobalInfo globalInfo = _globalInfo;
            int? schoolId = _globalInfo.SchoolID.Value;
            int? appliedLevel = _globalInfo.AppliedLevel;
            int? AcademicYearID = _globalInfo.AcademicYearID;
            int maxMarkTypeID = 0;
            maxMarkTypeID = MarkTypeBusiness.All.Where(o => o.AppliedLevel == appliedLevel).OrderByDescending(o => o.MarkTypeID).FirstOrDefault().MarkTypeID;
            ViewData[LockedMarkDetailConstants.MAX_MARKTYPEID] = maxMarkTypeID;
            List<ClassSubjectBO> lsClassSubject = ClassSubjectBusiness.SearchByClass(ClassID,
                new Dictionary<string, object> { { "Semester", SemesterID }, { "SchoolID", schoolId } })
                .Select(o => new ClassSubjectBO
                {
                    ClassSubjectID = o.ClassSubjectID,
                    SubjectID = o.SubjectID,
                    ClassID = o.ClassID,
                    DisplayName = o.SubjectCat.DisplayName,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                })
                .OrderBy(i => i.OrderInSubject).ThenBy(i => i.DisplayName).ToList();

            ViewData[LockedMarkDetailConstants.LS_ClassSubject] = lsClassSubject;

            Dictionary<string, object> searchInfo = new Dictionary<string, object>();
            if (appliedLevel.HasValue)
            {
                searchInfo["AppliedLevel"] = appliedLevel;
            }
            List<MarkType> ListMarkType = MarkTypeBusiness.Search(searchInfo).OrderBy(o => o.MarkTypeID).ToList();

            ViewData[LockedMarkDetailConstants.LS_MarkType] = ListMarkType;

            searchInfo.Clear();
            searchInfo["Semester"] = SemesterID;
            searchInfo["ClassID"] = ClassID;
            //int? SchoolID = globalInfo.SchoolID;
            if (schoolId.HasValue)
            {
                searchInfo["SchoolID"] = schoolId.Value;
            }

            if (AcademicYearID.HasValue)
            {
                searchInfo["AcademicYearID"] = AcademicYearID.Value;
            }

            List<SemeterDeclaration> ListSemesterDeclaretion = SemeterDeclarationBusiness.Search(searchInfo).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).ToList();
            ViewData[LockedMarkDetailConstants.LS_SemesterDeclaration] = ListSemesterDeclaretion;
            // Danh sach co trong DB
            List<LockedMarkDetail> ListLockedMarkDetail = LockedMarkDetailBusiness.SearchBySchool(schoolId.Value, searchInfo).ToList();
            ViewData[LockedMarkDetailConstants.LS_LockedMarkDetail] = ListLockedMarkDetail;
            return PartialView("_GridLockedMarkDetail");
        }

        [SkipCheckRole]
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult SaveLockedMarkDetail(int cboClass, int cboSemester, string[] ArrClassSubjectMarkType, int AppliedType)
        {
            // Kiem tra quyen
            if (GetMenupermission("LockedMarkDetail", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ClassProfile cp = ClassProfileBusiness.Find(cboClass);
            // Lay map danh sach ID va Doi tuong trong bang ClassSubject
            Dictionary<int, ClassSubject> dicClassSubject = new Dictionary<int, ClassSubject>();
            //GlobalInfo globalInfo = new GlobalInfo();
            //Chiendd: 23/06/2015. Sua lai luong insert bo sung gia tri partitionId
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            int partitionId = UtilsBusiness.GetPartionId(schoolId, 100);
            List<LockedMarkDetail> ListLockedMarkDetail = new List<LockedMarkDetail>();
            if (ArrClassSubjectMarkType != null && ArrClassSubjectMarkType.Length > 0)
            {
                foreach (string info in ArrClassSubjectMarkType)
                {
                    string[] arrInfo = info.Split('_');
                    int ClassSubjectID = int.Parse(arrInfo[0]);
                    if (!dicClassSubject.ContainsKey(ClassSubjectID))
                    {
                        dicClassSubject.Add(ClassSubjectID, ClassSubjectBusiness.Find(ClassSubjectID));
                    }
                }
                LockedMarkDetail LockedMarkDetail;
                ClassSubject ClassSubject;
                // Lay du lieu truyen len
                foreach (string info in ArrClassSubjectMarkType)
                {
                    string[] arrInfo = info.Split('_');
                    LockedMarkDetail = new LockedMarkDetail();
                    LockedMarkDetail.AcademicYearID = academicYearId;
                    LockedMarkDetail.SchoolID = schoolId;
                    LockedMarkDetail.Semester = cboSemester;
                    LockedMarkDetail.Last2digitNumberSchool = partitionId;
                    ClassSubject = dicClassSubject[int.Parse(arrInfo[0])];
                    LockedMarkDetail.SubjectID = ClassSubject.SubjectCat.SubjectCatID;
                    LockedMarkDetail.MarkTypeID = int.Parse(arrInfo[1]);
                    LockedMarkDetail.MarkIndex = int.Parse(arrInfo[2]);
                    ListLockedMarkDetail.Add(LockedMarkDetail);
                }
            }
            // Insert vao DB
            // Mac dinh ban dau se xu ly cho lop dang duoc lua chon, sau do se apply cho khoi va truong sau (neu chon)
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = schoolId;
            SearchInfo["AcademicYearID"] = academicYearId;
            SearchInfo["ClassID"] = cboClass;
            SearchInfo["Semester"] = cboSemester;
            List<LockedMarkDetail> listLockMark = this.Load4Class(cboClass, ListLockedMarkDetail, SearchInfo);
            LockedMarkDetailBusiness.InsertLockedMarkDetail(listLockMark, schoolId, academicYearId, appliedLevel, cboSemester);
            LockedMarkDetailBusiness.Save();
            #region Ap dung cho khoi va truong neu co
            if (AppliedType == LockedMarkDetailConstants.AppliedEducationLevelType)
            {
                LockedMarkDetailBusiness.ApplyLockMark(schoolId, academicYearId, cboSemester, appliedLevel, cboClass, cp.EducationLevelID);
            }
            if (AppliedType == LockedMarkDetailConstants.AppliedSchoolType)
            {
                LockedMarkDetailBusiness.ApplyLockMark(schoolId, academicYearId, cboSemester, appliedLevel, cboClass, 0);
            }
            #endregion

            return Json(new JsonMessage(Res.Get("LockedMarkDetail_Label_SaveSuc")));
        }

        /// <summary>
        /// Luu thong tin cho lop
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="ListLockedMarkDetail"></param>
        /// <param name="dic"></param>
        /// <param name="listSubjectId">ID mon hoc cua lop hoc ban dau duoc chon</param>
        private List<LockedMarkDetail> Load4Class(int ClassID, List<LockedMarkDetail> ListLockedMarkDetail, IDictionary<string, object> dic, List<int> listSubjectId = null)
        {
            int SchoolID = SMAS.Business.Common.Utils.GetInt(dic, "SchoolID");
            int EducationLevelID = SMAS.Business.Common.Utils.GetInt(dic, "EducationLevelID");
            int AcademicYearID = SMAS.Business.Common.Utils.GetInt(dic, "AcademicYearID");
            int Semester = SMAS.Business.Common.Utils.GetInt(dic, "Semester");
            dic["ClassID"] = ClassID;
            // Lay ra danh sach thong tin khoa diem da luu tu truoc va xoa di
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("LockedMarkDetail", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            List<ClassSubject> lstClassSub = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
            {
                {"Semester", Semester},
                {"SchoolID", SchoolID}
            }).ToList();

            List<LockedMarkDetail> ListLockedMarkDetailInDbClass = LockedMarkDetailBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).ToList();
            if (listSubjectId != null && listSubjectId.Count > 0)
            {
                ListLockedMarkDetailInDbClass.RemoveAll(o => !listSubjectId.Contains(o.SubjectID));
            }
            #region new
            List<LockedMarkDetail> lstDelete = new List<LockedMarkDetail>();
            List<LockedMarkDetail> lstInsert = new List<LockedMarkDetail>();
            //list Delete
            foreach (LockedMarkDetail lmdd in ListLockedMarkDetailInDbClass)
            {
                if (!ListLockedMarkDetail.Any(o => o.SubjectID == lmdd.SubjectID && o.MarkTypeID == lmdd.MarkTypeID && o.MarkIndex == lmdd.MarkIndex))
                {
                    lstDelete.Add(lmdd);
                }
            }

            //List Insert
            foreach (LockedMarkDetail lmdi in ListLockedMarkDetail)
            {
                if (!ListLockedMarkDetailInDbClass.Any(o => o.SubjectID == lmdi.SubjectID && o.MarkTypeID == lmdi.MarkTypeID && o.MarkIndex == lmdi.MarkIndex))
                {
                    lstInsert.Add(lmdi);
                }
            }
            //Xoa tat ca du lieu cu
            if (lstDelete.Count > 0)
            {
                LockedMarkDetailBusiness.DeleteAll(lstDelete);
            }
            List<LockedMarkDetail> ListLockedMarkDetailNew = new List<LockedMarkDetail>();
            foreach (LockedMarkDetail lmd in lstInsert)
            {
                // Thoi gian khoa la thoi gian hien tai
                LockedMarkDetail LockedMarkDetail = new LockedMarkDetail();
                if (lstClassSub.Where(o => o.ClassID == ClassID && o.SubjectID == lmd.SubjectID).Count() > 0)
                {
                    LockedMarkDetail.AcademicYearID = AcademicYearID;
                    LockedMarkDetail.SchoolID = lmd.SchoolID;
                    LockedMarkDetail.Semester = lmd.Semester;
                    LockedMarkDetail.SubjectID = lmd.SubjectID;
                    LockedMarkDetail.MarkTypeID = lmd.MarkTypeID;
                    LockedMarkDetail.MarkIndex = lmd.MarkIndex;
                    LockedMarkDetail.LockedDate = DateTime.Now;
                    LockedMarkDetail.ClassID = ClassID;
                    LockedMarkDetail.LockedMarkDetailID = 0;
                    LockedMarkDetail.Last2digitNumberSchool = lmd.Last2digitNumberSchool;
                    ListLockedMarkDetailNew.Add(LockedMarkDetail);
                }

            }
            #endregion
            return ListLockedMarkDetailNew;
        }
    }
}
