﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.LockedMarkDetailArea.Models
{
    public class LockedMarkDetailForm
    {
        //
        // GET: /LockedMarkDetailArea/LockedMarkDetailForm/
        [ResourceDisplayName("LockedMarkDetail_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LockedMarkDetailConstants.LS_EducationLevel)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassByEduLevel(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cboEducationLevel { get; set; }

        [ResourceDisplayName("LockedMarkDetail_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LockedMarkDetailConstants.LS_Class)]
        [AdditionalMetadata("OnChange", "AjaxLoadLockedMarkDetail()")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int cboClass { get; set; }

        [ResourceDisplayName("LockedMarkDetail_Label_Semester")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LockedMarkDetailConstants.LS_Semester)]
        [AdditionalMetadata("OnChange", "AjaxLoadLockedMarkDetail()")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int cboSemester { get; set; }
    }
}
