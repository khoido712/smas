﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportExamViolateArea
{
    public class ReportExamViolateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportExamViolateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportExamViolateArea_default",
                "ReportExamViolateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
