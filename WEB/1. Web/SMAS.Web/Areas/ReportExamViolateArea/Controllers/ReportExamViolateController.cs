﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.ReportExamViolateArea.Controllers
{
    public class ReportExamViolateController : Controller
    {
        //
        // GET: /ReportExamViolateArea/ReportExamViolate/
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IReportExamBusiness ReportExamBusiness;
        private readonly IDetachableHeadBagBusiness DetachableHeadBagBusiness;
        public ReportExamViolateController(IClassProfileBusiness classProfileBusiness,
            IEducationLevelBusiness educationLevelBusiness,

            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IExaminationBusiness examinationBusiness,
            IExaminationSubjectBusiness examinationSubjectBusiness,
            ISubjectCatBusiness subjectCatBusiness,
             IExaminationRoomBusiness examinationRoomBusiness,
            IReportExamBusiness reportExamBusiness,
            IDetachableHeadBagBusiness detachableHeadBagBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;

            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.ReportExamBusiness = reportExamBusiness;
            this.DetachableHeadBagBusiness = detachableHeadBagBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();

            //Danh sách khối học            
            ViewData[ReportExamViolateConstants.LIST_EDUCATION_LEVEL] = new List<EducationLevel>();
            //Danh sách kỳ thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = Global.AppliedLevel.GetValueOrDefault();
            int schoolID = Global.SchoolID.Value;
            List<Examination> lstExamination = ExaminationBusiness.SearchBySchool(schoolID, dic).OrderByDescending(o => o.ToDate).ToList();
            ViewData[ReportExamViolateConstants.LIST_EXAMINATION] = lstExamination;
            ViewData[ReportExamViolateConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubject>();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationSubject(int? idExaminationID, int? idEducationLevelID)
        {
            ViewData[ReportExamViolateConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubject>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["ExaminationID"] = idExaminationID;
            dic["EducationLevelID"] = idEducationLevelID;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolID"] = GlobalInfo.SchoolID.GetValueOrDefault();
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel.GetValueOrDefault();

            List<ExaminationSubject> lstExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.GetValueOrDefault(), dic).Distinct()
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                .ToList();


            if (lstExaminationSubject.Count() != 0)
            {
                ViewData[ReportExamViolateConstants.LIST_EXAMINATION_SUBJECT] = lstExaminationSubject;
            }
            return Json(new SelectList(lstExaminationSubject.ToList(), "ExaminationSubjectID", "SubjectCat.SubjectName"), JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution })
                .OrderBy(o => o.EducationLevelID)
                .Distinct();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }
        //Xuất báo cáo
        [HttpPost]
        public FileResult ExportExcel(FormCollection col)
        {
            int ExaminationID = 0;
            int EducationLevelID = 0;
            if (col["ExaminationID"] == null || col["ExaminationID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["ExaminationID"], out ExaminationID))
            {
                throw new BusinessException("Examination_Label_SetExaminationStageErrorMessage");
            }
            if (col["EducationLevelID"] == null || col["EducationLevelID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["EducationLevelID"].Trim(), out EducationLevelID))
            {
                throw new BusinessException("InputExaminationMark_Label_EducationLevelNotAvailable");
            }
            GlobalInfo global = new GlobalInfo();
            ExaminationID = Convert.ToInt32(col["ExaminationID"]);
            EducationLevelID = Convert.ToInt32(col["EducationLevelID"]);
            int? ExaminationSubjectID;
            if (col["ExaminationSubjectID"] == "")
            {
                ExaminationSubjectID = 0;
            }
            else
            {
                ExaminationSubjectID = Convert.ToInt32(col["ExaminationSubjectID"]);
            }


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID.GetValueOrDefault();
            dic["AcademicYearID"] = global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = global.AppliedLevel.GetValueOrDefault();
            dic["ExaminationID"] = ExaminationID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ExaminationSubjectID"] = ExaminationSubjectID;

            if (col["rdoReportType"] == "1")
            {
                Stream excel = ReportExamBusiness.CreateViolatedCandidateReport(dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "THI_[SchoolLevel]_ThiSinhVPQC_[EducationLevel].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
                ReportName = ReportName.Replace("[EducationLevel]", EducationLevelBusiness.Find(EducationLevelID).Resolution);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else if (col["rdoReportType"] == "2")
            {
                Stream excel = ReportExamBusiness.ViolatedInvilgilatorReport(dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

                string ReportName = "THI_[SchoolLevel]_GiamThiVPQC_[EducationLevel].xls";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(global.AppliedLevel.Value);
                //string semester = ReportUtils.ConvertSemesterForReportName(Semester);
                ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);

                ReportName = ReportName.Replace("[EducationLevel]", EducationLevelBusiness.Find(EducationLevelID).Resolution);
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

                result.FileDownloadName = ReportName;

                return result;
            }
            return null;
        }

    }
}
