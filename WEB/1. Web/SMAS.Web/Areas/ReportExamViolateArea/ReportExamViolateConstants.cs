﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportExamViolateArea
{
    public class ReportExamViolateConstants
    {
        public const string LIST_EXAMINATION = "List_Examination";
        public const string LIST_EDUCATION_LEVEL = "ListEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "ListExaminationSubject";
    }
}