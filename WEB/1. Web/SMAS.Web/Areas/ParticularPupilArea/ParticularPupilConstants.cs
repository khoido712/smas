/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ParticularPupilArea
{
    public class ParticularPupilConstants
    {
        public const string LIST_PARTICULARPUPIL = "listParticularPupil";
        public const string LIST_EducationLevel = "lstEducationLevel";
        public const string LIST_CLASS_NUll = "lstClassNull";
        public const string LIST_ClASS = "lstClass";
        public const string LIST_PUPIL = "lstPupil";
        public const string LIST_PARTICULARPUPILCHARACTERISTIC = "listCharacteristic";
        public const string LIST_PARTICULARPUPILTreatment = "listTreatment";
    }
}