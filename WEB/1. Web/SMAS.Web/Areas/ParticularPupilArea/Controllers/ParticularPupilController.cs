﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ParticularPupilArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ParticularPupilArea.Controllers
{
    public class ParticularPupilController : BaseController
    {
        private readonly IParticularPupilBusiness ParticularPupilBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IParticularPupilCharacteristicBusiness ParticularPupilCharacteristicBusiness;
        private readonly IParticularPupilTreatmentBusiness ParticularPupilTreatmentBusiness;

        public ParticularPupilController(IParticularPupilBusiness particularpupilBusiness
            , IClassProfileBusiness classprofilebusiness
            , IPupilOfClassBusiness pupilofclassbusiness
            , IParticularPupilCharacteristicBusiness particularpupilcharacteristicBusiness
            , IParticularPupilTreatmentBusiness particularpupiltreatmentBusiness)
        {
            this.ParticularPupilBusiness = particularpupilBusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.PupilOfClassBusiness = pupilofclassbusiness;
            this.ParticularPupilCharacteristicBusiness = particularpupilcharacteristicBusiness;
            this.ParticularPupilTreatmentBusiness = particularpupiltreatmentBusiness;
        }

        #region SetViewData
        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<ParticularPupilViewModel> lst = this._Search(SearchInfo);
            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPIL] = lst;


            IDictionary<string, object> SearchInfoCharac = new Dictionary<string, object>();
            IEnumerable<ParticularPupilCharacteristicViewModel> lstCharacteristic = this._SearchCharacteristic(SearchInfoCharac);
            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPILCHARACTERISTIC] = lstCharacteristic;

            ViewData[ParticularPupilConstants.LIST_CLASS_NUll] = new SelectList(new string[] { });
            ViewData[ParticularPupilConstants.LIST_PUPIL] = new SelectList(new string[] { });

            ViewData[ParticularPupilConstants.LIST_EducationLevel] = new SelectList(new GlobalInfo().EducationLevels, "EducationLevelID", "Resolution");


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetClassList(int? educationLevelID)
        {
            IEnumerable<ClassProfile> lsClassID = new List<ClassProfile>();
            if (educationLevelID != 0)
            {
                IDictionary<string, object> Dictionnary = new Dictionary<string, object>();
                Dictionnary["EducationLevelID"] = educationLevelID;
                Dictionnary["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                lsClassID = this.ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, Dictionnary).ToList();
                ViewData[ParticularPupilConstants.LIST_ClASS] = new SelectList(lsClassID, "ClassProfileID", "DisplayName");
            }

            return Json(new SelectList(lsClassID, "ClassProfileID", "DisplayName"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetPupilList(int? classID)
        {
            IEnumerable<PupilOfClass> lsPupilID = new List<PupilOfClass>();
            List<PupilProfile> lsPupil = new List<PupilProfile>();

            if (classID != 0)
            {
                IDictionary<string, object> Dictionnary = new Dictionary<string, object>();
                Dictionnary["ClassID"] = classID;
                Dictionnary["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                Dictionnary["Status"] = 1;
                lsPupilID = this.PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, Dictionnary).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ToList();

                IDictionary<string, object> dic = new Dictionary<string, object>();

                foreach (PupilOfClass poc in lsPupilID)
                {
                    PupilProfile PupilProfile = new PupilProfile();
                    PupilProfile.PupilProfileID = poc.PupilID;
                    PupilProfile.FullName = poc.PupilProfile.FullName;
                    lsPupil.Add(PupilProfile);

                }

            }

            return Json(new SelectList(lsPupil, "PupilProfileID", "FullName"));
        }

        #endregion

        #region Index
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #endregion

        #region Creat ParticularPupil

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(ParticularPupilViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { 
                                                                            { "AcademicYearID", glo.AcademicYearID.Value }, 
                                                                            { "PupilID", model.PupilID }, 
                                                                            { "Check", "Check" } })
                                                        .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                        .SingleOrDefault();

                if (poc==null)
                    return Json(new JsonMessage(Res.Get("PupilProfile_Label_Not_Pupil_Status_Studying"), JsonMessage.ERROR));

                Utils.Utils.TrimObject(model);

                ParticularPupil pp = new ParticularPupil();
                pp.RecordedDate = DateTime.Now;
                pp.UpdatedDate = DateTime.Now;
                pp.RealUpdatedDate = model.RealUpdatedDate;
                pp.PupilID = model.PupilID;
                pp.ClassID = poc.ClassID;
                pp.EducationLevelID = poc.ClassProfile.EducationLevelID;
                pp.AcademicYearID = glo.AcademicYearID.Value;
                pp.SchoolID = glo.SchoolID.Value;

                ParticularPupilCharacteristic ppc = new ParticularPupilCharacteristic();
                ppc.RealUpdatedDate = model.RealUpdatedDate;
                ppc.RecordedDate = DateTime.Now;
                ppc.UpdatedDate = DateTime.Now;
                ppc.FamilyCharacteristic = model.FamilyCharacteristic;
                ppc.PsychologyCharacteristic = model.PsychologyCharacteristic;

                pp.ParticularPupilCharacteristics.Add(ppc);

                this.ParticularPupilBusiness.InsertParticularPupil(glo.UserAccountID, pp);
                this.ParticularPupilBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }

            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, JsonMessage.ERROR));
        }
        #endregion

        #region Delete ParticularPupil

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ParticularPupilBusiness.DeleteParticularPupil(new GlobalInfo().UserAccountID, id, new GlobalInfo().SchoolID.Value);
            this.ParticularPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion

        #region Search ParticularPupil
        private IEnumerable<ParticularPupilViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<ParticularPupil> query = this.ParticularPupilBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            List<ParticularPupil> list = query.ToList();

            List<ParticularPupilViewModel> lst = new List<ParticularPupilViewModel>();
            foreach (var item in list)
            {
                ParticularPupilViewModel pp = new ParticularPupilViewModel();

                pp.ClassID = item.ClassID;
                pp.DisplayName = item.ClassProfile.DisplayName;
                pp.EducationLevelID = item.EducationLevelID;
                pp.ParticularPupilID = item.ParticularPupilID;
                pp.PupilCode = item.PupilProfile.PupilCode;
                pp.PupilID = item.PupilID;
                pp.PupilName = item.PupilProfile.Name;
                pp.RealUpdatedDate = item.RealUpdatedDate;
                pp.UpdatedDate = item.UpdatedDate;
                pp.FullName = item.PupilProfile.FullName;

                ParticularPupilCharacteristic ppc = item.ParticularPupilCharacteristics.OrderByDescending(u => u.RealUpdatedDate).FirstOrDefault();

                if (ppc != null)
                {
                    pp.ParticularPupilCharacteristicID = ppc.ParticularPupilCharacteristicID;
                    pp.FamilyCharacteristic = ppc.FamilyCharacteristic;
                    pp.PsychologyCharacteristic = ppc.PsychologyCharacteristic;
                }

                lst.Add(pp);
            }
            return lst;
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            GlobalInfo glo = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = glo.AppliedLevel;
            SearchInfo["SchoolID"] = glo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["ClassID"] = frm.cboClass;
            SearchInfo["EducationLevelID"] = frm.cboEducationLevel;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.PupilName;

            IEnumerable<ParticularPupilViewModel> lst = this._Search(SearchInfo);
            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPIL] = lst;

            return PartialView("_List");
        }
        #endregion

        #region ParticularPupilCharacteristic
        /// <summary>
        /// search lấy ra danh sách Đặc điểm
        /// </summary>
        /// <param name="ParticularPupilID"></param>
        /// <returns></returns>
        /// 
        private IEnumerable<ParticularPupilCharacteristicViewModel> _SearchCharacteristic(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ParticularPupilCharacteristic> query = this.ParticularPupilCharacteristicBusiness.Search(SearchInfo);
            IQueryable<ParticularPupilCharacteristicViewModel> lst = query.Select(o => new ParticularPupilCharacteristicViewModel
            {
                ParticularPupilCharacteristicID = o.ParticularPupilCharacteristicID,
                ParticularPupilID = o.ParticularPupilID,
                FamilyCharacteristic = o.FamilyCharacteristic,
                PsychologyCharacteristic = o.PsychologyCharacteristic,
                RealUpdatedDate = o.RealUpdatedDate


            });

            return lst.ToList();
        }



        [ValidateAntiForgeryToken]
        public PartialViewResult SearchCharacteristic(string FamilyCharacteristic, int? ParticularPupilCharacteristicID, int? ParticularPupilID, string PsychologyCharacteristic, DateTime? RealUpdatedDateCharacteristic)
        {
            //Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["ParticularPupilID"] = ParticularPupilID;
            SearchInfo["FamilyCharacteristic"] = FamilyCharacteristic;
            SearchInfo["ParticularPupilCharacteristicID"] = ParticularPupilCharacteristicID;
            SearchInfo["PsychologyCharacteristic"] = PsychologyCharacteristic;
            SearchInfo["RealUpdatedDateCharacteristic"] = RealUpdatedDateCharacteristic;

            IEnumerable<ParticularPupilCharacteristicViewModel> lst = this._SearchCharacteristic(SearchInfo);

            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPILCHARACTERISTIC] = lst;

            return PartialView("_ParticularPupilCharacteristic");

        }


        [ValidateAntiForgeryToken]
        public ActionResult SearchCharacteristicList(int ParticularPupilID)
        {
            //Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["ParticularPupilID"] = ParticularPupilID;
            IEnumerable<ParticularPupilCharacteristicViewModel> lst = this._SearchCharacteristic(SearchInfo);

            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPILCHARACTERISTIC] = lst;

            return PartialView("_ListCharacteristic"); ;

        }

        #endregion

        #region CreatOrEditCharacteristic
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult CreateOrEditCharacteristic(string FamilyCharacteristic, int? ParticularPupilCharacteristicID, int? ParticularPupilID, string PsychologyCharacteristic, DateTime? RealUpdatedDateCharacteristic)
        {
            ParticularPupilCharacteristic particularpupilcharacteristic = new ParticularPupilCharacteristic();
            if (ModelState.IsValid)
            {

                particularpupilcharacteristic.RecordedDate = DateTime.Now;

                particularpupilcharacteristic.PsychologyCharacteristic = PsychologyCharacteristic;
                particularpupilcharacteristic.FamilyCharacteristic = FamilyCharacteristic;
                particularpupilcharacteristic.ParticularPupilID = ParticularPupilID.Value;
                particularpupilcharacteristic.ParticularPupilCharacteristicID = ParticularPupilCharacteristicID.Value;

                if (ParticularPupilCharacteristicID == 0)
                {
                    Utils.Utils.TrimObject(particularpupilcharacteristic);
                    this.ParticularPupilCharacteristicBusiness.Insert(new GlobalInfo().UserAccountID, particularpupilcharacteristic);
                    this.ParticularPupilCharacteristicBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    ParticularPupilCharacteristic particularpupilcharacteristicUpdate = this.ParticularPupilCharacteristicBusiness.Find(ParticularPupilCharacteristicID);
                    //if (RealUpdatedDateCharacteristic.Value > DateTime.Now)
                    //{
                    //    return Json(new JsonMessage(Res.Get("ParticularPupil_Label_FailedDateTime1"), JsonMessage.ERROR));
                    //}
                    //else
                    //{
                    particularpupilcharacteristicUpdate.UpdatedDate = RealUpdatedDateCharacteristic.Value;
                    particularpupilcharacteristicUpdate.RealUpdatedDate = RealUpdatedDateCharacteristic.Value;
                    //}
                    //particularpupilcharacteristicUpdate.RealUpdatedDate = RealUpdatedDateCharacteristic.Value;
                    particularpupilcharacteristicUpdate.PsychologyCharacteristic = PsychologyCharacteristic;
                    particularpupilcharacteristicUpdate.FamilyCharacteristic = FamilyCharacteristic;
                    particularpupilcharacteristicUpdate.ParticularPupilID = ParticularPupilID.Value;
                    particularpupilcharacteristicUpdate.ParticularPupilCharacteristicID = ParticularPupilCharacteristicID.Value;
                    Utils.Utils.TrimObject(particularpupilcharacteristicUpdate);
                    this.ParticularPupilCharacteristicBusiness.Update(new GlobalInfo().UserAccountID, particularpupilcharacteristicUpdate);
                    this.ParticularPupilCharacteristicBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

                }

            }

            string jsonErrList = Res.GetJsonErrorMessage(ModelState);

            return Json(new JsonMessage(jsonErrList, "error"));

        }

        #endregion

        #region DeleteCharacteristic
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCharacteristic(int id)
        {
            this.ParticularPupilCharacteristicBusiness.Delete(new GlobalInfo().UserAccountID, id, new GlobalInfo().SchoolID.Value);
            this.ParticularPupilCharacteristicBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion

        #region ParticularPupilTreatment

        public PartialViewResult SearchTreatment(int ParticularPupilID)
        {
            this.CheckPermissionForAction(ParticularPupilID, "ParticularPupil");
            ParticularPupil pp = ParticularPupilBusiness.Find(ParticularPupilID);
            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPILTreatment] = pp.ParticularPupilTreatments.Select(o => new ParticularPupilTreatmentViewModel
                                                                                    {
                                                                                        ParticularPupilTreatmentID = o.ParticularPupilTreatmentID,
                                                                                        ParticularPupilID = o.ParticularPupilID,
                                                                                        TreatmentResolution = o.TreatmentResolution,
                                                                                        Result = o.Result,
                                                                                        AppliedDate = o.AppliedDate
                                                                                    }).OrderBy(u => u.AppliedDate);
            return PartialView("_ParticularPupilTreatment");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchTreatmentList(int ParticularPupilID)
        {
            this.CheckPermissionForAction(ParticularPupilID, "ParticularPupil");
            ParticularPupil pp = ParticularPupilBusiness.Find(ParticularPupilID);
            ViewData[ParticularPupilConstants.LIST_PARTICULARPUPILTreatment] = pp.ParticularPupilTreatments.Select(o => new ParticularPupilTreatmentViewModel
                                                                                {
                                                                                    ParticularPupilTreatmentID = o.ParticularPupilTreatmentID,
                                                                                    ParticularPupilID = o.ParticularPupilID,
                                                                                    TreatmentResolution = o.TreatmentResolution,
                                                                                    Result = o.Result,
                                                                                    AppliedDate = o.AppliedDate
                                                                                }).OrderBy(u => u.AppliedDate);
            return PartialView("_ListTreatment");
        }
        #endregion

        #region CreatOrEditTreatment
        /// <summary>
        /// Create Or Edit
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult CreateOrEditTreatment(ParticularPupilTreatmentViewModel model)
        {
            Utils.Utils.TrimObject(model);

            ParticularPupilTreatment particularpupiltreatment = null;
            ParticularPupil particularPupil = ParticularPupilBusiness.Find(model.ParticularPupilID);

            if (model.ParticularPupilTreatmentID.HasValue && model.ParticularPupilTreatmentID.Value > 0)
            {
                particularpupiltreatment = ParticularPupilTreatmentBusiness.Find(model.ParticularPupilTreatmentID);
                if (particularpupiltreatment == null)
                    particularpupiltreatment = new ParticularPupilTreatment();
            }
            else
            {
                particularpupiltreatment = new ParticularPupilTreatment();
            }
            
            particularpupiltreatment.UpdatedDate = DateTime.Now;
            particularpupiltreatment.AppliedDate = model.AppliedDate;
            particularpupiltreatment.TreatmentResolution = model.TreatmentResolution;
            particularpupiltreatment.Result = model.Result;
            particularpupiltreatment.ParticularPupilID = particularPupil.ParticularPupilID;
            particularpupiltreatment.PupilID = particularPupil.PupilID;

            if (model.ParticularPupilTreatmentID.HasValue && model.ParticularPupilTreatmentID.Value > 0)
            {
                this.ParticularPupilTreatmentBusiness.UpdateParticularPupilTreatment(new GlobalInfo().UserAccountID, particularpupiltreatment);
                this.ParticularPupilTreatmentBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                this.ParticularPupilTreatmentBusiness.InsertParticularPupilTreatment(new GlobalInfo().UserAccountID, particularpupiltreatment);
                this.ParticularPupilTreatmentBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }

        #endregion

        #region DeleteTreatment
        /// <summary>
        /// Delete Treatment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteTreatment(int id)
        {
            this.ParticularPupilTreatmentBusiness.DeleteParticularPupilTreatment(new GlobalInfo().UserAccountID, id, new GlobalInfo().SchoolID.Value);
            this.ParticularPupilTreatmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion
    }
}





