﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;

namespace SMAS.Web.Areas.ParticularPupilArea.Models
{
    public class ParticularPupilTreatmentViewModel
    {
        [ResourceDisplayName("ParticularPupilTreatment_Label_ParticularPupilTreatmentID")]
        public int? ParticularPupilTreatmentID { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_ParticularPupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ParticularPupilID { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_TreatmentResolution")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        public string TreatmentResolution { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_Result")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string Result { get; set; }

        [ResourceDisplayName("ParticularPupilTreatment_Label_AppliedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? AppliedDate { get; set; }
    }
}
