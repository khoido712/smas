/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ParticularPupilArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ParticularPupil_Label_EducationLevel")]

   
        public System.Nullable<int> cboEducationLevel { get; set; }

        [ResourceDisplayName("ParticularPupil_Label_Class")]

        public System.Nullable<int> cboClass { get; set; }

        [ResourceDisplayName("ParticularPupil_Label_Pupil")]

        public System.Nullable<int> cboPupil { get; set; }

        [ResourceDisplayName("ParticularPupil_Label_PupilName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String PupilName { get; set; }

        [ResourceDisplayName("ParticularPupil_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String PupilCode { get; set; }

    }
}