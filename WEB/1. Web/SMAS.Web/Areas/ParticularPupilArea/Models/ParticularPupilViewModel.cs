/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
namespace SMAS.Web.Areas.ParticularPupilArea.Models
{
    public class ParticularPupilViewModel
    {
        [ScaffoldColumn(false)]
        public int ParticularPupilID { get; set; }

        [ScaffoldColumn(false)]
        public int? ParticularPupilCharacteristicID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ParticularPupil_Column_ListPupilName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int PupilID { get; set; }

        [ScaffoldColumn(false)]
        public int? ClassID { get; set; }

        [ScaffoldColumn(false)]
        public int? EducationLevelID { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_RealUpdatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]	
        public DateTime RealUpdatedDate { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_FamilyCharacteristic")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string FamilyCharacteristic { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_PsychologyCharacteristic")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string PsychologyCharacteristic { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_PupilName")]
        public string PupilName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_Class")]
        public string DisplayName { get; set; }

        [ResourceDisplayName("ParticularPupil_Column_Year")]
        public string Year { get; set; }
    }
}

