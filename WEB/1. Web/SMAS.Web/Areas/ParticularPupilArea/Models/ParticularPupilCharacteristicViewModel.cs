﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;

namespace SMAS.Web.Areas.ParticularPupilArea.Models
{
    public class ParticularPupilCharacteristicViewModel
    {
        public System.Int32 ParticularPupilCharacteristicID { get; set; }
        public System.Int32 ParticularPupilID { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_FamilyCharacteristic")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String FamilyCharacteristic { get; set; }
        
        
       
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ParticularPupilCharacteristic_Label_PsychologyCharacteristic")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DataType(DataType.MultilineText)]
        public System.String PsychologyCharacteristic { get; set; }

        [ResourceDisplayName("ParticularPupilCharacteristic_Label_RealUpdatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataConstraint(DataConstraintAttribute.LESS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public System.DateTime RealUpdatedDate { get; set; }


    }
}
