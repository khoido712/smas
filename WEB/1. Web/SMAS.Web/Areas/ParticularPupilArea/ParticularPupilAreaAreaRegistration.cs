﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ParticularPupilArea
{
    public class ParticularPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ParticularPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ParticularPupilArea_default",
                "ParticularPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
