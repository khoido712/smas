/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ForeignLanguageGradeArea.Models
{
    public class ForeignLanguageGradeViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 ForeignLanguageGradeID { get; set; }

        [ResourceDisplayName("ForeignLanguageGrade_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }

        [ResourceDisplayName("ForeignLanguageGrade_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        [ResourceDisplayName("ForeignLanguageGrade_Label_EncouragePoint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("class", "decimal")]
        public decimal? EncouragePoint { get; set; }
    }
}


