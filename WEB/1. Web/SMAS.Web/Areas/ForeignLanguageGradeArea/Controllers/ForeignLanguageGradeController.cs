/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ForeignLanguageGradeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ForeignLanguageGradeArea.Controllers
{
    public class ForeignLanguageGradeController : BaseController
    {
        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;

        public ForeignLanguageGradeController(IForeignLanguageGradeBusiness foreignlanguagegradeBusiness)
        {
            this.ForeignLanguageGradeBusiness = foreignlanguagegradeBusiness;
        }

        //
        // GET: /ForeignLanguageGrade/

        public ActionResult Index()
        {
            SetViewDataPermission("ForeignLanguageGrade", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<ForeignLanguageGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[ForeignLanguageGradeConstants.LIST_FOREIGNLANGUAGEGRADE] = lst;
            return View();
        }

        //
        // GET: /ForeignLanguageGrade/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            IEnumerable<ForeignLanguageGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[ForeignLanguageGradeConstants.LIST_FOREIGNLANGUAGEGRADE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            ForeignLanguageGrade foreignlanguagegrade = new ForeignLanguageGrade();
            foreignlanguagegrade.IsActive = true;
            TryUpdateModel(foreignlanguagegrade);
            Utils.Utils.TrimObject(foreignlanguagegrade);

            this.ForeignLanguageGradeBusiness.Insert(foreignlanguagegrade);
            this.ForeignLanguageGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ForeignLanguageGradeID)
        {
            ForeignLanguageGrade foreignlanguagegrade = this.ForeignLanguageGradeBusiness.Find(ForeignLanguageGradeID);
            TryUpdateModel(foreignlanguagegrade);
            Utils.Utils.TrimObject(foreignlanguagegrade);
            this.ForeignLanguageGradeBusiness.Update(foreignlanguagegrade);
            this.ForeignLanguageGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            ForeignLanguageGradeBusiness.Delete(id);
            this.ForeignLanguageGradeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ForeignLanguageGradeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ForeignLanguageGrade> query = this.ForeignLanguageGradeBusiness.Search(SearchInfo);
            IQueryable<ForeignLanguageGradeViewModel> lst = query.Select(o => new ForeignLanguageGradeViewModel
            {
                ForeignLanguageGradeID = o.ForeignLanguageGradeID,
                Resolution = o.Resolution,
                Description = o.Description,
                EncouragePoint = o.EncouragePoint
            });

            return lst.ToList();
        }
    }
}





