﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ForeignLanguageGradeArea
{
    public class ForeignLanguageGradeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ForeignLanguageGradeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ForeignLanguageGradeArea_default",
                "ForeignLanguageGradeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
