﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author namdv3
* @version $Revision: 1.0$
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DailyDishCostArea.Models;

using SMAS.Models.Models;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.DailyDishCostArea.Controllers
{
    public class DailyDishCostController : BaseController
    {
        private readonly IDailyDishCostBusiness DailyDishCostBusiness;

        public DailyDishCostController(IDailyDishCostBusiness dailydishcostBusiness)
        {
            this.DailyDishCostBusiness = dailydishcostBusiness;
        }

        //
        // GET: /DailyDishCost/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<DailyDishCostViewModel> lst = this._Search(SearchInfo);
            ViewData[DailyDishCostConstants.LIST_DAILYDISHCOST] = lst.OrderByDescending(o => o.EffectDate);
            return View();
        }

        //
        // GET: /DailyDishCost/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<DailyDishCostViewModel> lst = this._Search(SearchInfo);
            ViewData[DailyDishCostConstants.LIST_DAILYDISHCOST] = lst.OrderByDescending(o => o.EffectDate);
            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            DailyDishCost dailydishcost = new DailyDishCost();
            TryUpdateModel(dailydishcost);
            Utils.Utils.TrimObject(dailydishcost);

            string pattern = "^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)[0-9]{2}$";
            Match match = Regex.Match(dailydishcost.EffectDate.ToShortDateString(), pattern);
            if (!match.Success)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_DateTime2"), "error"));
            }   
          
            dailydishcost.SchoolID = new GlobalInfo().SchoolID.Value;
            dailydishcost.IsActive = true;
            this.DailyDishCostBusiness.Insert(dailydishcost);
            this.DailyDishCostBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DailyDishCostID, string EffectDate)
        {
            DailyDishCost dailydishcost = this.DailyDishCostBusiness.Find(DailyDishCostID);
            DateTime oldEffectDate = dailydishcost.EffectDate;
            string pattern = "^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)[0-9]{2}$";

            bool updateModel = TryUpdateModel(dailydishcost);
            if (!updateModel)
            {
                Match matchEffectDate = Regex.Match(EffectDate, pattern);
                if (!matchEffectDate.Success)
                {
                    return Json(new JsonMessage(Res.Get("Common_Validate_DateTime2"), "error"));
                }
            }

            Utils.Utils.TrimObject(dailydishcost);
            Match match = Regex.Match(dailydishcost.EffectDate.ToShortDateString(), pattern);
            if (!match.Success)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_DateTime2"), "error"));
            }
            this.DailyDishCostBusiness.Update(dailydishcost, oldEffectDate);
            this.DailyDishCostBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.DailyDishCostBusiness.Delete(id, new GlobalInfo().SchoolID.Value);
            this.DailyDishCostBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DailyDishCostViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<DailyDishCost> query = this.DailyDishCostBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            if (query != null && query.Count() > 0)
            {
                IQueryable<DailyDishCostViewModel> lst = query.Select(o => new DailyDishCostViewModel
                {
                    DailyDishCostID = o.DailyDishCostID,
                    Note = o.Note,
                    SchoolID = o.SchoolID,
                    EffectDate = o.EffectDate,
                    Cost = o.Cost,
                    CreatedDate = o.CreatedDate,
                    IsActive = o.IsActive,
                    ModifiedDate = o.ModifiedDate
                });

                return lst.ToList();
            }
            return new List<DailyDishCostViewModel>();
        }
    }
}





