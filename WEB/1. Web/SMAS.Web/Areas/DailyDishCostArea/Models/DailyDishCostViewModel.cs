﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Security;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.DailyDishCostArea.Models
{
    public class DailyDishCostViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 DailyDishCostID { get; set; }

        [ResourceDisplayName("DailyDishCost_Label_EffectDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
       // [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[DataType(DataType.DateTime)]
        //[UIHint("DateTimePicker")]
        //[DefaultValue(DateTime.Now.AddDays(1).ToShortDateString())]
        public System.DateTime EffectDate { get; set; }


        [ResourceDisplayName("DailyDishCost_Label_Cost")]
        [RegularExpression(@"\d+", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 Cost { get; set; }


        [ResourceDisplayName("DailyDishCost_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Note { get; set; }


        [ScaffoldColumn(false)]
        public System.Int32 SchoolID { get; set; }
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}


