﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DailyDishCostArea
{
    public class DailyDishCostAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DailyDishCostArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DailyDishCostArea_default",
                "DailyDishCostArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
