﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SendSMSToTeacherConstants
    {
        public const string LST_CONTACTGROUP = "lst contactGroup";

        public const string NO_GROUP = "no group";

        public const string LIST_TEACHER_RECEIVER = "lst teacher receiver";

        public const string SCHOOL_NOT_ACTIVE_SMSTEACHER = "school not active sms teacher";

        public const string IS_ALLOW_SEND_UNICODE_MSG = "allow send unicode message";
        public const string PREFIX_NAME_OF_SCHOOL = "GET_PREFIX_NAME_OF_SCHOOL";
    }
}