﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class StatisticsCommunicationSupervisingDeptConstants
    {
        public const string VISIBLE = "Visible";

        public const string LISTEMPLOYEE = "ListEmployee";

        public const string PAGE = "Page";

        public const string TOTAL = "Total";

        public const string SEND_SUCCESS = "1";

        public const string SEND_ERROR = "0";

        public const string LISTMESSAGE = "ListMessage";

        public const string DATEROLE = "DataRole";

        public const string TEMPLATE_NAME = "BM_ThongKeTinNhanPhongSo.xls";

        public const int PAGE_SIZE = 20;
    }
}