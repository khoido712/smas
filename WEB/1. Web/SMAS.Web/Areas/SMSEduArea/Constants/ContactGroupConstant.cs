﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class ContactGroupConstant
    {
        public const string LIST_CONTACT_GROUP = "lst contact group";
        public const string CONTACT_GROUP = "contact group";
        public const string COUNT_TEACHER = "count teacher";
        public const string TEACHER_IN_GROUP = "teacher in group";
        public const string TEACHER_OUT_GROUP = "teacher out group";

        public const string CBO_SCHOOL_FALCULTY = "cbo_school_falculty";
        public const string CBO_WORK_GROUP_TYPE = "cbo_work_group_type";
        public const string CBO_WORK_TYPE = "cbo_work_type";
        public const string SEARCH_VIEW_MODEL = "search_view_model";
        public const string RESULT_SIDE = "result_side";

        public const int COMUNITY_YOUTH_LEAGE_MEMBER = 1;
        public const int COMUNITY_SYNDICATE = 2;
        public const int COMUNITY_COMMUNIST_PARTY_MEMBER = 3;
        public const int WORK_TYPE_HEAD_TEACHER = 1;
        public const int WORK_TYPE_SUBJECT_TEACHER = 2;
        public const int PHONE_NETWORK_VIETTEL = 1;
        public const int PHONE_NETWORK_OTHER = 2;


        //Assign
        public const string CBO_CONTACT_GROUP = "cbo_contact_group";
        public const string LIST_ASSIGN_TEACHER = "list_assign_teacher";
        public const string LIST_ASSIGNED_TEACHER_ID = "list_assigned_teacher_id";
        public const string STR_ASSIGNED_TEACHER_ID = "str_assigned_teacher_id";
    }
}