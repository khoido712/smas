﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SMSParentManageContractConstant
    {
        public const string ListPayment = "ListPayment";

        public const string ListStatusContract = "ListStatusContract";

        public const string ListLevel = "ListLevel";//khoi hoc

        public const string ListClass = "ListClass";

        public const string ListSearch = "ListSearch";

        public const string ListPhoneNumber = "ListPhoneNumber";

        public const string ListPupilRegisterSMSFree = "ListPupilRegisterSMSFree";

        public const string ClassID = "ClassID";
        public const string CLASSNAME = "ClassName";

        public const string Level = "Level";

        public const string ListServicePackage = "ListServicePackage";
        public const string ListServicePackageFull = "ListServicePackageFull";
        public const string ListServicePackageAdd = "ListServicePackageAdd";
        public const string ListServicePackageEdit = "ListServicePackageEdit";

        public const string SchoolID = "SchoolID";

        public const string PupilName = "PupilName";
        public const string PupilCode = "PupilCode";
        public const string MobilePhone = "MobilePhone";

        public const string PUPILFILE_ID = "pupilfile to update";

        public const string PaymentType = "PaymentType";

        public const string Status = "Status";

        public const string Year = "Year";

        public const int SubscriperType = 0;

        public const string MESSAGE = "MESSAGE";

        public const string CURRENTYEAR = "CURRENTYEAR";

        public const string NOTCURRENTYEAR = "NOTCURRENTYEAR";

        public const string HKI = "HKI";

        public const string HKII = "HKII";

        public const string STR_SEMESTER_ALL = "Cả năm";

        public const string TOTAL = "TOTAK";

        public const string CURRENT_PAGE = "current page";

        public const string PAGING = "PAGING";

        public const int PAGE_SIZE = 20;

        public const string ON = "on";

        public const string STATUS_CONTRACT_PAY = "1";

        public const string CHECKBOX_TRUE = "true";

        public const string LIST_PUPIL = "list pupil select";

        public const string CURRENT_SEMESTER = "current semester";

        public const string CHOOSE_TEXT = "Lựa chọn";

        public const int PageSize = 20;

        public const string IS_SMS_PARENT = "isSMSParent";

        public const string IS_PAGING = "isPaging";

        public const string IS_FATHER = "Father";

        public const string IS_MOTHER = "Mother";

        public const string IS_OTHER = "Other";
        public const string BALANCE = "Balance";
        public const string EWALLETID = "EwalletID";

        public const int COLUMN_PUPIL_CODE = 2;
        public const int COLUMN_PUPIL_NAME = 3;
        public const int COLUMN_CLASS_NAME = 4;
        public const int COLUMN_PUPIL_RELATIONSHIP = 5;
        public const int COLUMN_RECEIVER_NAME = 6;
        public const int COLUMN_PHONE_RECEIVER = 7;
        public const int COLUMN_SERVICE_PACKAGE = 8;
        public const int COLUMN_SUBSCRIPTION_TIME = 9;

        public const string LIST_IMPORT = "ListImport";
        public const string LIST_IMPORT_EXTRA = "ListImportExtra";
        public const string LIST_REGIS = "ListRegis";

        public const string LIST_PAY_POSTAGE = "ListPayPostage";

        public const string LIST_SYNC_SMAS3 = "ListSyncSMAS3";

        public const bool IS_REGIS_CONTRACT = true;

        public const bool IS_DELETE_REGIS_CONTRACT = true;
        public const string PRICE_PAY = "PricePay";
        public const string PRICE_PAY_EXTRA = "PricePayExtra";
        public const string PRICE_PAY_POSTAGE = "postage to pay";
        public const string OBJ_EDIT = "ObjEdit";
        public const string LIST_ACCOUNT_SPARENT = "listaccountsparent";

        public const string DEFERRED_PAYMENT_DAYS_KEY = "DeferredPaymentDays";

        public const string CONTRACT_ID = "UpdateContractID";
        public const string PHONE = "UpdatePhone";
        public const string SERVICE_PACKAGE_ID = "UpdateServicepackageID";
        public const string SERVICE_PACKAGE_CODE = "SERVICE_PACKAGE_CODE";
        public const string CONTRACT_DETAIL_ID = "CONTRACT_DETAIL_ID";
        public const string IS_LIMIT = "IS_LIMIT";
        public const string IS_WHOLE_YEAR = "IS_WHOLE_YEAR";
        public const string IS_REGIS_SMS = "IS_REGIS_SMS";
    }
}