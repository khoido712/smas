﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class DeclarePackageConstant
    {
        public const string LIST_YEAR = "listAcademicYear";

        public const string LIST_PACKAGE = "listPackage";

        public const string LIST_SERVICE_PACKAGE = "listServicePackage";

        public const string PAGING = "PAGING";

        public const int PAGE_SIZE = 20;

        public const int SEMESTER_I = 1;

        public const int SEMESTER_II = 2;

        public const int SEMESTER_ALL = 3;

        public const string INFO_SERVICE_PACKAGE = "servicepackageObj";

        public const string INFO_PACKAGE_DETAIL = "packagedetailObj";

        public const int TYPE = 30;

        // Bo sung phan goi cuoc tra sau
        public const string LIST_PROVINCE_APPLY = "ListProvinceApply";

        public const string LIST_PROVINCE = "list province in apply screen";
        public const string LIST_DISTRICT = "list district in apply screen";

        public const string NUM_OF_SCHOOL = "number of school apply";
        public const string LIST_SCHOOL = "list school";

        public const string Page = "page";
        public const string Total = "total";


        public const string SERVICE_PACKAGE_ID = "service package id";
    }
}