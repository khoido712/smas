﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class TransactionHistoryConstants
    {
        public const string LIST_YEAR = "ListYear";
        public const string LIST_APPLIED_LEVEL = "ListAppliedLevel";
        public const string LIST_EDUCATION_LEVEL = "ListEducationLevel";
        public const string LIST_TRANSACTION = "ListTransaction";
        public const string LIST_CLASS = "ListClass";
        public const string SCHOOL_ID = "SCHOOL_ID";
        public const string UNIT_NAME = "UNIT_NAME";
        public const string TITLE = "TITLE";
        public const string SENDER_EW_ID = "SENDER_EW_ID";
        public const string RECEIVER_EW_ID = "RECEIVER_EW_ID";
        public const string SENDER_ID = "SENDER_ID";
        public const string SENDER_NAME = "SENDER_NAME";
        public const string RECEIVER_NAME = "RECEIVER_NAME";
        public const string TOTAL = "total";
        public const string PAGE = "page";

        public const int UNIT_TYPE_SCHOOL = 1;
        public const int UNIT_TYPE_SUP = 2;
        public const int AFFECTED_TYPE_REFUND = 1;
        public const int AFFECTED_TYPE_TRANSFER = 2;
        public const int PAGE_SIZE = 200;
    }
}