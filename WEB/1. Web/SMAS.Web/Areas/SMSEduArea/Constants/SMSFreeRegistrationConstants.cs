﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SMSFreeRegistrationConstants
    {
        public const string EducationLevelList = "EducationLevelList";
        public const string ClassList = "ClassList";
        public const string StatusList = "StatusList";
        public const string ListData = "ListData";
        public const string TotalData = "TotalData";
        public const string SizeData = "SizeData";
        public const string PageData = "PageData";
        public const string Message = "Message";
        public const string CurrentYear = "CurrentYear";
    }
}