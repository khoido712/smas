﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SendSMSCommentOfPupilConstants
    {
        public const string ClassID = "ClassID";
        public const string SubjectID = "SubjectID";
        public const string DAY_OF_MONTH = "DayOfMonth";
        public const string SEMESTER_ID = "SemesterID";
        public const string FROM_DATE = "FromDate";
        public const string TO_DATE = "ToDate";
        public const string ALLOW_SEND_UNICODE_MESSAGE = "IsAllowSendUnicodeMessage";
        public const string SUBJECT_NAME = "SubjectName";
    }
}