﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SchoolConfigConstant
    {
        public const string OBJECT_SCHOOLCONFIGS2 = "Object_SchoolConfigS2";

        public const string OBJECT_SCHOOLCONFIGS3 = "Object_SchoolConfigS3";

        public const string LINK_REQUEST = "LINK_REQUEST";

        public const string MESSAGE = "MESSAGE";

        public const string SHOW_HIDE_BTNSAVE = "SHOW_HIDE_BTNSAVE";

        public const string NotIs_Admin_SchoolRole = "Is_Admin_SchoolRole";

        public const string SHOW_SP2_ONLINE = "SHOW_SP2_ONLINE";

        public const string DISABLE_OPTION_PAYMENT_TYPE = "disable option of payment type";
    }
}