﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class PromotionProgramConstant
    {
        public const string LST_PROMOTION_PROGRAM = "LstPromotionProgram";
        public const string LST_PROVINCE = "LstProvince";
        public const string LIST_PROVINCE_APPLY = "ListProvinceApply";
        public const string LIST_PROVINCE_APPLY_EDIT = "ListProvinceApplyEdit";
        public const string PROMOTION_PROGRAM_TO_UPDATE = "PromotionProgramToUpdate";
        public const string LIST_SCHOOL_APPLIED = "ListSchoolApplied";
        public const string PROMOTION_ID = "PromotionID";
        public const string PROMOTION_PROGRAM_DETAIL = "PromotionProgramDetail";
        public const string CURRENT_PAGE = "current page";
        public const string PAGING = "PAGING";
        public const int PAGE_SIZE = 15;
        public const string TOTAL = "Total";
    }
}