﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SendSMSToSuperVisingDeptConstants
    {
        public const string SUPER_USER_TYPE = "super user type";

        //public const string NUM_SMS_ALLOW_SEND = "num of sms allow send";

        //public const string SENT_RECORD = "sent record";

        public const string LIST_EMPLOYEE = "lst employee";

        public const string LIST_LEVEL = "lst super unit";

        public const int PAGE_SIZE = 20;

        //public const int SMS_LIMIT_SUPER_USER = 100;

        public const string LBL_MESSAGE_NOT_USE_SMS = "do not use sms";
        //public const string LIMIT_SEND_SMS = "Limit send sms";

        // AnhVD9 20150629
        public const string BALANCE_OF_EWALLET = "balance of ewallet";

        public const string NUMBER_PROMOTION_SMS = "number of internal promotion message";
        public const string CONTENT_PROMOTION = "CONTENT_PROMOTION";
    }
}