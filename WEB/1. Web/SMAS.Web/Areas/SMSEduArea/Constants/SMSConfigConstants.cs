﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Constants
{
    public class SMSConfigConstants
    {
        public const string LIST_CONTENT_TYPE = "LIST_CONTENT_TYPE";

        public const int PERIOD_TYPE_DAILY = 1;
        public const int PERIOD_TYPE_WEEKLY = 2;
        public const int PERIOD_TYPE_MONTHLY = 3;
        public const int PERIOD_TYPE_SEMESTER = 4;
        public const int PERIOD_TYPE_CUSTOM = 5;
    }
}