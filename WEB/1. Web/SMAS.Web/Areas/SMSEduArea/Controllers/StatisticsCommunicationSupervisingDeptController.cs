﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using SMAS.Business.Common.Extension;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class StatisticsCommunicationSupervisingDeptController : BaseController
    {
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        public StatisticsCommunicationSupervisingDeptController(IEmployeeBusiness EmployeeBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness,
            ISMSHistoryBusiness SMSHistoryBusiness)
        {
            this.EmployeeBusiness = EmployeeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
        }
        private static int m_TotalRecord = 0;
        public ActionResult Index()
        {
            LoadViewData();
            return View();
        }
        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchHistoryAjax(string FromDate, string ToDate, int MsgType, string senderID, GridCommand command, int total)
        {
            List<SMSCommunicationBO> lstSMS = SearchParentSMS(ExtensionMethods.ConvertStringToDate(FromDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY), ExtensionMethods.ConvertStringToDate(ToDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY), MsgType, senderID, command);
            return View(new GridModel<SMSCommunicationBO>()
            {
                Data = lstSMS != null && lstSMS.Count > 0 ? lstSMS : new List<SMSCommunicationBO>(),
                Total = m_TotalRecord
            });
        }

        [HttpPost]
        public PartialViewResult SearchHistory(string FromDate, string ToDate, int MsgType, string senderID, GridCommand command)
        {
            var fromDate = ExtensionMethods.ConvertStringToDate(FromDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
            var toDate = ExtensionMethods.ConvertStringToDate(ToDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
            var lst = SearchParentSMS(fromDate, toDate, MsgType, senderID, command);
            ViewData[StatisticsCommunicationSupervisingDeptConstants.LISTMESSAGE] = lst;
            return PartialView("GridMessage");
        }
        public List<SMSCommunicationBO> SearchParentSMS(DateTime? fromDate, DateTime? toDate, int smsType, string senderID, GridCommand command)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("fromDate", fromDate);
            dic.Add("toDate", toDate);
            dic.Add("page", command.Page);
            dic.Add("type", smsType);
            dic.Add("senderID", senderID);
            SupervisingDept objSD = this.GetSupervisingDeptParent();
            int unitID = objSD.SupervisingDeptID;

            List<SMSCommunicationBO> lstSMSParent = SMSHistoryBusiness.SearchUnitHistorySMS(dic, unitID, ref m_TotalRecord);

            if (lstSMSParent != null && lstSMSParent.Count > 0)
            {
                ViewData[StatisticsCommunicationSupervisingDeptConstants.VISIBLE] = "Enable";
            }
            ViewData[StatisticsCommunicationSupervisingDeptConstants.PAGE] = command.Page;
            ViewData[StatisticsCommunicationSupervisingDeptConstants.TOTAL] = m_TotalRecord;
            return lstSMSParent;
        }
        [HttpPost]
        public string CreateExportExcel(string FromDate, string ToDate, int MsgType, string senderID)
        {
            try
            {
                if (!_globalInfo.IsSubSuperVisingDeptRole && !_globalInfo.IsSuperVisingDeptRole)
                {
                    //Khong xu ley khi khong phai user phong/so
                    return string.Empty;
                }
                SupervisingDept objSP = this.GetSupervisingDeptParent();
                //BM_ThongKeTinNhanPhongSo.xls
                string fileName = StatisticsCommunicationSupervisingDeptConstants.TEMPLATE_NAME;
                string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + fileName; ;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                // Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                DateTime? startDate = ExtensionMethods.ConvertStringToDate(FromDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                DateTime? endDate = ExtensionMethods.ConvertStringToDate(ToDate, GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);


                string fromTo = string.Format("Từ ngày {0} đến ngày {1}", startDate.HasValue ? startDate.Value.ToString(GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY) : string.Empty, endDate.HasValue ? endDate.Value.ToString(GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY) : string.Empty);
                firstSheet.SetCellValue("A2", objSP.SupervisingDeptName.ToUpper());
                firstSheet.SetCellValue("A4", fromTo);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("fromDate", startDate);
                dic.Add("toDate", endDate);
                dic.Add("page", 1);
                dic.Add("numRecord", 9000000);
                dic.Add("type", MsgType);
                dic.Add("senderID", senderID);

                int unitID = objSP.SupervisingDeptID;
                List<SMSCommunicationBO> lstSMSParentContract = SMSHistoryBusiness.SearchUnitHistorySMS(dic, unitID, ref m_TotalRecord);
                //Group theo nhom theo yeu cau GP 09/10/2013
                List<GroupExportData> lstGroup = lstSMSParentContract.GroupBy(a => new { a.TeacherSenderName, a.TeacherReceiverName, a.ParentReceiverName }).Select(b => new GroupExportData
                {
                    TeacherSenderName = b.Key.TeacherSenderName,
                    TeacherReceiverName = b.Key.TeacherReceiverName,
                    ParentReceiverName = b.Key.ParentReceiverName,
                    ContentCount = b.Sum(a => a.ContentCount),
                    Subscriber = b.FirstOrDefault().Subscriber,
                }).ToList();
                //Export to file
                int starRows = 7;
                int starRowsPhone = starRows;

                List<string> lstPhoneMainContract = new List<string>();
                var tmpDataRng = sheet2.GetRange("A1", "F1");
                var tmpTotal = sheet2.GetRange("A2", "F2");
                GroupExportData ParentContract = null;
                int sumActive = 0;
                for (int i = 0; i < lstGroup.Count; i++)
                {
                    ParentContract = lstGroup[i];
                    firstSheet.CopyPasteSameSize(tmpDataRng, starRows, 1);
                    firstSheet.SetCellValue(starRows, 1, i + 1);
                    firstSheet.SetCellValue(starRows, 2, ParentContract.TeacherSenderName);
                    firstSheet.SetCellValue(starRows, 3, ParentContract.TeacherReceiverName);
                    firstSheet.SetCellValue(starRows, 4, ParentContract.Subscriber);
                    firstSheet.SetCellValue(starRows, 5, ParentContract.ParentReceiverName);
                    firstSheet.SetCellValue(starRows, 6, ParentContract.ContentCount);
                    sumActive += (ParentContract.ContentCount.HasValue) ? ParentContract.ContentCount.Value : 0;
                    starRows++;
                }
                int starRowsSum = 7 + lstGroup.Count;
                firstSheet.CopyPasteSameSize(tmpTotal, starRowsSum, 1);
                firstSheet.SetCellValue("F" + starRowsSum + "", sumActive);
                sheet2.Delete();
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "BM_ThongKeTinNhanPhongSo.xls";
                result.FileDownloadName = ReportName;
                //SuperUnitBusiness.EnableTracking();
                string fileKey = string.Format("Export.{0}", DateTime.Now.ToString("dd.MM.yyyy.ss.ff.ff"));
                Session[fileKey] = result;
                return fileKey;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return string.Empty;
            }
        }
        /// <summary>
        /// Export file to client
        /// </summary>
        /// <param name="fileKey"></param>
        /// <returns></returns>
        public FileResult ExportExcel(string fileKey)
        {
            if (Session[fileKey] != null)
            {
                var result = (FileStreamResult)Session[fileKey];
                Session[fileKey] = null; //Release resource <-- auto release stream
                return result;
            }
            return null;
        }
        private void LoadViewData()
        {
            ViewData[StatisticsCommunicationSupervisingDeptConstants.VISIBLE] = false;
            //Load danh sach Employee      
            SupervisingDept objSD = this.GetSupervisingDeptParent();
            List<string> lstEmployeeName = new List<string>();
            if (objSD.SMSActiveType != 0)
            {
                ViewData[StatisticsCommunicationSupervisingDeptConstants.DATEROLE] = true;
                List<EmployeeBO> lstEmp = SupervisingDeptBusiness.GetEmployeeBySuperVisingDeptID(objSD.SupervisingDeptID, _globalInfo.IsSuperVisingDeptRole, _globalInfo.IsSubSuperVisingDeptRole);

                if (lstEmp != null)
                {
                    lstEmployeeName = (from em in lstEmp select em.FullName).OrderBy(a => a).ToList();
                    lstEmployeeName.Add(objSD.SupervisingDeptName);
                }
            }
            else
            {
                ViewData[StatisticsCommunicationSupervisingDeptConstants.DATEROLE] = false;
            }

            ViewData[StatisticsCommunicationSupervisingDeptConstants.LISTEMPLOYEE] = lstEmployeeName;
        }
        private SupervisingDept GetSupervisingDeptParent()
        {
            SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

            if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }

            return null;
        }
        /// <summary>
        /// For export data only
        /// </summary>
        private class GroupExportData
        {
            public string TeacherSenderName { get; set; }
            public string TeacherReceiverName { get; set; }
            public string Subscriber { get; set; }
            public string ParentReceiverName { get; set; }
            public int? ContentCount { get; set; }
        }
    }
}