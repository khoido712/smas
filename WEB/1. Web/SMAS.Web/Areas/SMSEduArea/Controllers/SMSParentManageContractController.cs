﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Configuration;
using SMAS.Business.Common.Extension;
using SMS_SCHOOL.Utility.BusinessObject;
using System.Text.RegularExpressions;
using SMAS.Web.Areas.SMSEduArea.Models;
using System.Data.Objects;
using System.Threading.Tasks;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class SMSParentManageContractController : BaseController
    {
        #region contructor
        private readonly ISMSParentContractBusiness smsParentContractBusiness;
        private readonly ISMSParentContractDetailBusiness smsParentContractDetailBusiness;
        private readonly ISchoolConfigBusiness schoolConfigBusiness;
        private readonly IServicePackageBusiness servicePackageBusiness;
        private readonly IEWalletBusiness ewalletBusiness;
        private readonly ISMSParentContractExtraBusiness smsParentContractExtraBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        public SMSParentManageContractController(
           ISMSParentContractBusiness SMSParentContractBusiness,
           ISMSParentContractDetailBusiness SMSParentContractDetailBusiness,
            ISchoolConfigBusiness schoolConfigBusiness,
            IServicePackageBusiness servicePackageBusiness,
            IEWalletBusiness ewalletBusiness,
            ISMSParentContractExtraBusiness smsParentContractExtraBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IPupilProfileBusiness PupilProfileBusiness)
        {
            this.smsParentContractBusiness = SMSParentContractBusiness;
            this.smsParentContractDetailBusiness = SMSParentContractDetailBusiness;
            this.schoolConfigBusiness = schoolConfigBusiness;
            this.servicePackageBusiness = servicePackageBusiness;
            this.ewalletBusiness = ewalletBusiness;
            this.smsParentContractExtraBusiness = smsParentContractExtraBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }
        #endregion

        #region index
        /// <summary>
        /// index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            PrepareData();
            return View();
        }
        #endregion

        /// <summary>
        /// Khoi tao du lieu
        /// </summary>
        private void PrepareData()
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            int semester = Semester;
            //Danh sach khối học
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["Level"] = _globalInfo.AppliedLevel;
            List<LevelBO> ListLevel = EducationLevelBusiness.GetByGrade(_globalInfo.AppliedLevel.Value).Select(o =>
                   new LevelBO
                   {
                       EducationLevelID = o.EducationLevelID,
                       EducationLevelName = o.Resolution
                   }).OrderBy(o => o.EducationLevelID).ToList();

            ViewData[SMSParentManageContractConstant.ListLevel] = ListLevel;

            //Danh sach trang thai thue bao da dang ky hay chua
            List<SelectListItem> status = new List<SelectListItem>();
            status.Add(new SelectListItem { Text = Res.Get("All"), Value = "0" });
            status.Add(new SelectListItem { Text = Res.Get("SMSParentContract_Label_Registed"), Value = GlobalConstantsEdu.STATUS_REGISTED.ToString() });
            status.Add(new SelectListItem { Text = Res.Get("SMSParentContract_Label_Not_Registed"), Value = GlobalConstantsEdu.STATUS_NOT_REGISTED.ToString() });
            status.Add(new SelectListItem { Text = Res.Get("Đang hoạt động"), Value = GlobalConstantsEdu.STATUS_IN_ACTIVE.ToString() });
            status.Add(new SelectListItem { Text = Res.Get("SMSParentContract_Label_Stop_Using"), Value = GlobalConstantsEdu.STATUS_STOP_USING.ToString() });
            status.Add(new SelectListItem { Text = Res.Get("Lbl_Delete_Contract_Detail"), Value = GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL.ToString() });
            // Anhvd9 20150901 - Bổ sung Trả chậm
            status.Add(new SelectListItem { Text = Res.Get("SMSParentContract_Label_Unpaid"), Value = GlobalConstantsEdu.STATUS_UNPAID.ToString() });
            status.Add(new SelectListItem { Text = Res.Get("SMSParentContract_Label_Overdue_Unpaid"), Value = GlobalConstantsEdu.STATUS_OVERDUE_UNPAID.ToString() });
            ViewData[SMSParentManageContractConstant.ListStatusContract] = status;

            //Danh sach goi cuoc ap dung cho truong (khong bao gom goi Ap dung cho thue bao phu)         
            ViewData[SMSParentManageContractConstant.ListServicePackage] = servicePackageBusiness.GetListServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value).ToList();

            // Anhvd9 20160219 - Danh sach goi cuoc bao gom ca goi cuoc ap dung cho thue bao phu  
            Session["servicePackage"] = servicePackageBusiness.GetListServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);


            //kiem tra truong co su dung dich vu sms parent hay khong?
            ViewData[SMSParentManageContractConstant.IS_SMS_PARENT] = SchoolProfileBusiness.IsSMSParentActive(_globalInfo.SchoolID.Value);
            //Vua vao chua load lop hoc
            ViewData[SMSParentManageContractConstant.ListClass] = new SelectList(new string[] { });
            //Lay thong tin so du tai khoan truong
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);
            ViewData[SMSParentManageContractConstant.BALANCE] = ewalletObj.Balance;
            ViewData[SMSParentManageContractConstant.EWALLETID] = ewalletObj.EWalletID;
        }

        #region load class
        /// <summary>
        /// Lay danh sach lop theo Khoi
        /// </summary>
        /// <param name="educationLevelID"></param>
        /// <returns></returns>
        public ActionResult GetListClassByEducationLevelID(int educationLevelID)
        {
            int academicYearID = _globalInfo.AcademicYearID.Value;
            List<ClassBO> listClass = new List<ClassBO>();
            if (_globalInfo.EmployeeID > 0)
            {
                listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"TeacherByRoleID", _globalInfo.EmployeeID},
                {"Type", SMAS.Business.Common.SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Select(o => new ClassBO
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                }).Where(p => p.EducationLevelID == educationLevelID).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderID).ThenBy(o => o.ClassName).ToList();
            }
            else
            {
                listClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, new Dictionary<string, object>(){
                {"EducationLevelID", educationLevelID}
                    }).Select(o => new ClassBO
                    {
                        Grade = o.EducationLevel.Grade,
                        EducationLevelID = o.EducationLevelID,
                        ClassID = o.ClassProfileID,
                        ClassName = o.DisplayName,
                        OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                    }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderID).ThenBy(o => o.ClassName).ToList();
            }
            ViewData[SMSParentManageContractConstant.ListClass] = listClass;
            return Json(new SelectList(listClass, "ClassID", "ClassName"));
        }


        //[OutputCache(Duration = 600, Location = OutputCacheLocation.Client)]
        public JsonResult AjaxLoadClass(int educationID)
        {
            List<ClassBO> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, new Dictionary<string, object>(){
                {"EducationLevelID", educationID}
                    }).Select(o => new ClassBO
                    {
                        Grade = o.EducationLevel.Grade,
                        EducationLevelID = o.EducationLevelID,
                        ClassID = o.ClassProfileID,
                        ClassName = o.DisplayName,
                        OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                    }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderID).ThenBy(o => o.ClassName).ToList();

            if (lstClass != null) { return Json(new SelectList(lstClass, "ClassID", "ClassName")); }
            return Json(new SelectList(new List<ClassBO>(), "ClassID", "ClassName"), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region search smsparent contract
        /// <summary>
        /// search smsparent contract
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Search(FormCollection form)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            int totalRecord = 0;
            int schoolID = _globalInfo.SchoolID.Value;
            int year = aca.Year;
            int educationLevelID = 0;
            if (!string.IsNullOrEmpty(form["CboEducationLevel"]))
            {
                educationLevelID = Convert.ToInt32(form["CboEducationLevel"]);
            }
            int classID = 0;
            if (!string.IsNullOrEmpty(form["CboClassID"]))
            {
                classID = Convert.ToInt32(form["CboClassID"]);
            }
            string pupilName = "";
            if (!string.IsNullOrEmpty(form["PupuilName"]))
            {
                pupilName = form["PupuilName"].Trim();
            }

            string mobilePhone = "";
            if (!string.IsNullOrEmpty(form["MobilePhone"]))
            {
                mobilePhone = form["MobilePhone"].Trim();
            }

            int status = -1;
            if (!string.IsNullOrEmpty(form["CboStatus"]))
            {
                status = Convert.ToInt32(form["CboStatus"]);
            }

            string pupilCode = string.Empty;
            if (!string.IsNullOrEmpty(form["txtPupilCode"]))
            {
                pupilCode = form["txtPupilCode"].Trim();
            }

            int currentPage = 1;
            if (classID > 0)
            {
                ViewData[SMSParentManageContractConstant.PAGING] = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(form["CurrentPage"]))
                {
                    currentPage = Convert.ToInt32(form["CurrentPage"]);
                }
                ViewData[SMSParentManageContractConstant.PAGING] = true;
            }
            string className = string.Empty;
            if (!string.IsNullOrEmpty(form["HfSClassName"]))
            {
                className = form["HfSClassName"].ToString();
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["Year"] = year;
            dic["LevelID"] = educationLevelID; //khối
            dic["ClassID"] = classID;
            dic["PupilName"] = pupilName;
            dic["MobilePhone"] = mobilePhone;
            dic["SubscriptionStatus"] = status;
            dic["Level"] = _globalInfo.AppliedLevel;
            //dic["levelType"] = GlobalInfo.GetLoginLevelType();
            dic["page"] = currentPage;
            dic["pageSize"] = SMSParentManageContractConstant.PAGE_SIZE;

            // tim theo hop dong co phan trang hoac khong phan trang
            List<SMSParentContractGroupBO> lstSMSParentContractBO = smsParentContractBusiness.GetListSMSParentContract(schoolID, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault(), classID, educationLevelID, pupilCode, pupilName, mobilePhone, year, status, currentPage, GlobalConstantsEdu.PAGESIZE, ref totalRecord);
            //tim theo hoc sinh
            if (classID > 0)
            {
                ViewData[SMSParentManageContractConstant.IS_PAGING] = "false";
            }
            else
            {
                ViewData[SMSParentManageContractConstant.IS_PAGING] = "true";
            }

            if (((int)totalRecord == 0 && classID <= 0) || lstSMSParentContractBO.Count == 0)
            {
                return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = Res.Get("SMSParentManageContract_Label_SearchEmpty"), Status = true });
            }

            ViewData[SMSParentManageContractConstant.ListSearch] = lstSMSParentContractBO;
            ViewData[SMSParentManageContractConstant.TOTAL] = (int)totalRecord > 0 ? (int)totalRecord : lstSMSParentContractBO.Count;
            ViewData[SMSParentManageContractConstant.CURRENT_PAGE] = currentPage;

            int semester = this.Semester;
            // 20160219 Anhvd9 - Danh sach khong bao gom goi cuoc Ap dung cho thue bao phu
            ViewData[SMSParentManageContractConstant.ListServicePackage] = servicePackageBusiness.GetListServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value, false)
                                                                                .ToList();
            ViewData[SMSParentManageContractConstant.ListServicePackageFull] = servicePackageBusiness.GetListServicesPackageFull(aca.Year);

            //lay danh sach hoc sinh duoc dang ky goi SMSFREE
            List<int> lstPupilID = lstSMSParentContractBO.Select(p => p.PupilID).Distinct().ToList();
            Dictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"YearID", _globalInfo.AcademicYearID},
                {"lstPupilID",lstPupilID},
                {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE}
            };
            List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();

            ViewData[SMSParentManageContractConstant.ListPupilRegisterSMSFree] = lstPupilRegisterSMSFreeID;
            ViewData[SMSParentManageContractConstant.SchoolID] = schoolID;
            ViewData[SMSParentManageContractConstant.Year] = year;
            ViewData[SMSParentManageContractConstant.Level] = educationLevelID;
            ViewData[SMSParentManageContractConstant.ClassID] = classID;
            ViewData[SMSParentManageContractConstant.Status] = status;
            ViewData[SMSParentManageContractConstant.PupilName] = pupilName;
            ViewData[SMSParentManageContractConstant.PupilCode] = pupilCode;
            ViewData[SMSParentManageContractConstant.CLASSNAME] = className;
            ViewData[SMSParentManageContractConstant.MobilePhone] = mobilePhone;

            DateTime datenow = DateTime.Now;
            AcademicYear academicyear = aca;
            if ((academicyear.FirstSemesterStartDate <= datenow && datenow <= academicyear.FirstSemesterEndDate)
                || (academicyear.SecondSemesterStartDate <= datenow && datenow <= academicyear.SecondSemesterEndDate))
            {
                ViewData[SMSParentManageContractConstant.CURRENTYEAR] = true;
            }
            else
            {
                ViewData[SMSParentManageContractConstant.CURRENTYEAR] = false;
            }

            return PartialView("_GirdSMSParentContract");
        }

        /// <summary>
        /// Chuyển sang giao diện thanh toán Nợ cước
        /// </summary>
        /// <returns></returns>
        public ActionResult RedirectToCheckout()
        {
            PrepareData();
            return PartialView("DeferredPayment");
        }

        /// <summary>
        /// Tim kiem thue bao No cuoc
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchPostage(FormCollection form)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            int schoolID = _globalInfo.SchoolID.Value;
            int year = aca.Year;
            int educationLevelID = 0;
            if (!string.IsNullOrEmpty(form["CboEducationLevel"]))
            {
                educationLevelID = Convert.ToInt32(form["CboEducationLevel"]);
            }
            int classID = 0;
            if (!string.IsNullOrEmpty(form["CboClassID"]))
            {
                classID = Convert.ToInt32(form["CboClassID"]);
            }
            string pupilName = "";
            if (!string.IsNullOrEmpty(form["PupuilName"]))
            {
                pupilName = form["PupuilName"].Trim();
            }

            string mobile = "";
            if (!string.IsNullOrEmpty(form["MobilePhone"]))
            {
                mobile = form["MobilePhone"].Trim();
            }
            int status = -1;
            if (!string.IsNullOrEmpty(form["CboStatus"]))
            {
                status = Convert.ToInt32(form["CboStatus"]);
            }

            string pupilCode = string.Empty;
            if (!string.IsNullOrEmpty(form["txtPupilCode"]))
            {
                pupilCode = form["txtPupilCode"].Trim();
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["Year"] = year;
            dic["LevelID"] = educationLevelID; //khối
            dic["ClassID"] = classID;
            dic["PupilCode"] = pupilCode;
            dic["PupilName"] = pupilName;
            dic["Mobile"] = mobile;
            dic["Status"] = status;
            dic["Level"] = _globalInfo.AppliedLevel;
            dic["Status"] = status;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EmployeeID"] = _globalInfo.EmployeeID;

            int semester = this.Semester;

            List<SMSParentContractGroupBO> lstSMSParentContractBO = smsParentContractBusiness.getListContractUnPaid(dic);
            ViewData[SMSParentManageContractConstant.ListSearch] = lstSMSParentContractBO;

            ViewData[SMSParentManageContractConstant.ListServicePackage] = servicePackageBusiness.GetListServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value, false);
            ViewData[SMSParentManageContractConstant.ListServicePackageFull] = servicePackageBusiness.GetListServicesPackageFull(aca.Year);


            ViewData[SMSParentManageContractConstant.SchoolID] = _globalInfo.SchoolID;
            ViewData[SMSParentManageContractConstant.Year] = aca.Year;


            DateTime datenow = DateTime.Now;
            AcademicYear academicyear = aca;
            if ((academicyear.FirstSemesterStartDate <= datenow && datenow <= academicyear.FirstSemesterEndDate)
                || (academicyear.SecondSemesterStartDate <= datenow && datenow <= academicyear.SecondSemesterEndDate))
            {
                ViewData[SMSParentManageContractConstant.CURRENTYEAR] = true;
            }
            else
            {
                ViewData[SMSParentManageContractConstant.CURRENTYEAR] = false;
            }

            return PartialView("_GridPayPostage");
        }
        #endregion

        #region xuat excel/import
        public FileResult ExportExcelRegisted(FormCollection frm)
        {

            // Lấy toàn bộ gói cước
            List<SMS_SERVICE_PACKAGE> lstPackages = servicePackageBusiness.All.ToList();
            int schoolID = _globalInfo.SchoolID.Value;
            int year = Convert.ToInt32(frm["HYear"]);
            int educationLevel = Convert.ToInt32(frm["HEducationLevel"]);
            int classID = Convert.ToInt32(frm["HClassID"]);
            string pupilName = frm["HPupilName"];
            string pupilCode = frm["HPupilCode"];
            string mobilephone = (frm["HMobilePhone"]).Trim();
            int subscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
            string className = frm["HfClassName"];
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["Year"] = year;
            dic["LevelID"] = educationLevel; //khối
            dic["ClassID"] = classID;
            dic["PupilName"] = pupilName;
            dic["PupilCode"] = pupilCode;
            dic["SubscriptionStatus"] = subscriptionStatus;
            dic["level"] = _globalInfo.AppliedLevel;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + "/DSThuebaoSMSParent.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            // Lấy sheet template                  
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            // Ten truong
            firstSheet.SetCellValue("A2", _globalInfo.SchoolName);
            // Tieu de
            firstSheet.SetCellValue("A4", "DANH SÁCH LIÊN LẠC");
            // Nam hoc
            string schoolYearTitle = string.Format(Res.Get("SMSParentManageContract_Label_SchoolYearTitle"), aca.Year, aca.Year + 1);
            firstSheet.SetCellValue("A5", schoolYearTitle.ToUpper());
            // Khoi lop
            string levelName = string.Empty;
            if (educationLevel != 0)
            {
                EducationLevel levelObj = null;

                List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(_globalInfo.AppliedLevel.Value).OrderBy(o => o.EducationLevelID).ToList();

                if (listEducationLevel != null)
                {
                    levelObj = listEducationLevel.Where(p => p.EducationLevelID == educationLevel).FirstOrDefault();
                }
                if (levelObj != null)
                {
                    levelName = levelObj.Resolution.Replace(Res.Get("Common_Label_EducationShort"), string.Empty).Trim();
                }
            }
            StringBuilder sbText = new StringBuilder();
            sbText.Append(Res.Get("Common_Label_EducationShort"))
                .Append(GlobalConstantsEdu.COLON)
                .Append(GlobalConstantsEdu.SPACE)
                .Append(educationLevel == 0 ? Res.Get("Common_Label_HealthStatusAll") : levelName)
                .Append(GlobalConstantsEdu.SPACE)
                .Append(Res.Get("Common_Label_ClassShort"))
                .Append(GlobalConstantsEdu.COLON)
                .Append(GlobalConstantsEdu.SPACE)
                .Append(classID == 0 ? Res.Get("Common_Label_HealthStatusAll") : className);
            firstSheet.SetCellValue("A6", sbText.ToString());
            int startRow = 9;
            int beginRow = 9;
            int semester = this.Semester;
            List<SMSParentContractGroupBO> listResult = smsParentContractBusiness.GetListExcelRegisted(schoolID, _globalInfo.AcademicYearID.Value, year, subscriptionStatus, educationLevel, semester, classID, pupilName, pupilCode, mobilephone, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault());
            SMSParentContractGroupBO groupObj = null;
            ContractDetailList contractDetailObj = null;
            int orderNumber = 0;
            int? subscriptionTime = 0;
            bool checkRegistration = false;// kiem tra co dang ky 2 hoc ky nhung hinh thuc dang ky khac nhau
            string relationship = string.Empty;
            if (listResult != null && listResult.Count > 0)
            {
                for (int i = 0, CountRs = listResult.Count; i < CountRs; i++)
                {
                    groupObj = listResult[i];
                    firstSheet.SetCellValue("A" + startRow, orderNumber + 1); orderNumber++;
                    firstSheet.SetCellValue("B" + startRow, groupObj.PupilCode);
                    firstSheet.SetCellValue("C" + startRow, groupObj.PupilName);
                    firstSheet.SetCellValue("D" + startRow, groupObj.ClassName);
                    if (groupObj.contractDetailList != null && groupObj.contractDetailList.Count > 0)
                    {
                        checkRegistration = false;
                        firstSheet.MergeColumn('A', startRow, startRow + groupObj.contractDetailList.Count - 1);
                        firstSheet.MergeColumn('B', startRow, startRow + groupObj.contractDetailList.Count - 1);
                        firstSheet.MergeColumn('C', startRow, startRow + groupObj.contractDetailList.Count - 1);
                        firstSheet.MergeColumn('D', startRow, startRow + groupObj.contractDetailList.Count - 1);

                        for (int j = 0; j < groupObj.contractDetailList.Count; j++)
                        {
                            contractDetailObj = groupObj.contractDetailList[j];
                            //cot quan he
                            relationship = contractDetailObj.RelationShip;
                            if (relationship == SMSParentManageContractConstant.IS_FATHER)
                            {
                                firstSheet.SetCellValue("E" + startRow, Res.Get("Lbl_Father"));
                            }
                            else if (relationship == SMSParentManageContractConstant.IS_MOTHER)
                            {
                                firstSheet.SetCellValue("E" + startRow, Res.Get("Lbl_Mother"));
                            }
                            else
                            {
                                firstSheet.SetCellValue("E" + startRow, Res.Get("Lbl_Other"));
                            }
                            firstSheet.SetCellValue("F" + startRow, "'" + contractDetailObj.PhoneReceiver.Trim().ExtensionMobile());

                            // Cot thoi han
                            if (contractDetailObj.AllSemester == GlobalConstants.SEMESTER_OF_YEAR_ALL || (contractDetailObj.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && contractDetailObj.SecondSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                subscriptionTime = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                firstSheet.SetCellValue("G" + startRow, Res.Get("Common_Label_AllYear"));
                            }
                            else if (contractDetailObj.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                subscriptionTime = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                                firstSheet.SetCellValue("G" + startRow, Res.Get("Common_Label_Semester1"));
                            }
                            else if (contractDetailObj.SecondSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                subscriptionTime = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                                firstSheet.SetCellValue("G" + startRow, Res.Get("Common_Label_Semester2"));
                            }

                            //cot goi cuoc                            
                            SMS_SERVICE_PACKAGE tmpPackage = lstPackages.FirstOrDefault(a => a.SERVICE_PACKAGE_ID == contractDetailObj.ServicesPakageID);
                            firstSheet.SetCellValue("H" + startRow, tmpPackage != null ? tmpPackage.SERVICE_CODE : "");

                            #region Tinh tien cot I va J
                            // Xet Hinh thuc dang ky
                            //HInh thuc dang ky ca nam
                            if (subscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                            {
                                // Cung hinh thuc dang ky
                                if (contractDetailObj.RegistrationTypeAllSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS
                                || (contractDetailObj.RegistrationTypeFirstSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS && contractDetailObj.RegistrationTypeSecondSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS))
                                {
                                    //Bang SMS
                                    // So hoc ky dang ky
                                    decimal amountPlus = subscriptionTime > 2 ? 2 : 1;

                                    // So tien khach hang thanh toan
                                    decimal MoneyCustomerPayVT = tmpPackage.PRICE_FOR_CUSTOMER * amountPlus;
                                    // firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);

                                    // So tien truong thanh toan lai cho Viettel
                                    decimal? MoneyVTPayForSchool = amountPlus * (tmpPackage.PRICE_FOR_CUSTOMER - (contractDetailObj.PhoneReceiver.CheckViettelNumber() ? tmpPackage.VIETTEL_PRICE : tmpPackage.OTHER_PRICE));

                                    // Cot So tien Viettel thanh toan cho truong
                                    //firstSheet.SetCellValue("I" + startRow, MoneyVTPayForSchool);
                                }
                                else
                                {
                                    //khac hinh thuc dang ky
                                    if (contractDetailObj.RegistrationTypeFirstSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS || contractDetailObj.RegistrationTypeSecondSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS)
                                    {
                                        //Bang SMS
                                        // So hoc ky dang ky
                                        decimal amountPlus = subscriptionTime > 2 ? 2 : 1; // So hoc ky dang ky

                                        // So tien khach hang thanh toan
                                        decimal MoneyCustomerPayVT = tmpPackage.PRICE_FOR_CUSTOMER * amountPlus;
                                        // firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);

                                        // So tien truong thanh toan lai cho Viettel
                                        decimal? MoneyVTPayForSchool = tmpPackage.PRICE_FOR_CUSTOMER - (contractDetailObj.PhoneReceiver.CheckViettelNumber() ? tmpPackage.VIETTEL_PRICE : tmpPackage.OTHER_PRICE);

                                        // Cot So tien Viettel thanh toan cho truong
                                        //firstSheet.SetCellValue("I" + startRow, MoneyVTPayForSchool);

                                        checkRegistration = true;
                                    }

                                    if (contractDetailObj.RegistrationTypeFirstSemester != GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS || contractDetailObj.RegistrationTypeSecondSemester != GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS)
                                    {

                                        //truc tiep voi truong
                                        // So hoc ky dang ky
                                        decimal amountPlus = subscriptionTime > 2 ? 2 : 1;

                                        // So tien khach hang thanh toan
                                        decimal MoneyCustomerPayVT = tmpPackage.PRICE_FOR_CUSTOMER * amountPlus;
                                        //firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);
                                        if (checkRegistration)
                                        {
                                            amountPlus = 1;// Neu da co dang ky la SMS o 1 ky thi ki con lai so lan dang ky se la 1
                                        }
                                        // So tien truong thanh toan lai cho Viettel
                                        decimal? MoneyVTPayForSchool = amountPlus * (contractDetailObj.PhoneReceiver.CheckViettelNumber() ? tmpPackage.VIETTEL_PRICE : tmpPackage.OTHER_PRICE);

                                        // Cot So tien truong thanh toan cho Viettel
                                        firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);


                                    }
                                }
                            }
                            else
                            {
                                //Dang ky qua SMS
                                if (contractDetailObj.RegistrationTypeFirstSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS || contractDetailObj.RegistrationTypeSecondSemester == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS)// Neu hinh thuc dang ky la qua SMS thi dien Cot So tien thanh toan cho Truong
                                {
                                    // So hoc ky dang ky
                                    decimal amountPlus = subscriptionTime > 2 ? 2 : 1; // So hoc ky dang ky

                                    // So tien khach hang thanh toan
                                    decimal MoneyCustomerPayVT = tmpPackage.PRICE_FOR_CUSTOMER * amountPlus;
                                    // firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);

                                    // So tien truong thanh toan lai cho Viettel
                                    decimal? MoneyVTPayForSchool = amountPlus * (tmpPackage.PRICE_FOR_CUSTOMER - (contractDetailObj.PhoneReceiver.CheckViettelNumber() ? tmpPackage.VIETTEL_PRICE : tmpPackage.OTHER_PRICE));

                                    // Cot So tien Viettel thanh toan cho truong
                                    //firstSheet.SetCellValue("I" + startRow, MoneyVTPayForSchool);
                                }
                                //Dang ky truc tiep voi truong
                                else
                                {
                                    // So hoc ky dang ky
                                    decimal amountPlus = subscriptionTime > 2 ? 2 : 1;

                                    // So tien khach hang thanh toan
                                    decimal MoneyCustomerPayVT = tmpPackage.PRICE_FOR_CUSTOMER * amountPlus;
                                    //firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);

                                    // So tien truong thanh toan lai cho Viettel
                                    decimal? MoneyVTPayForSchool = amountPlus * (contractDetailObj.PhoneReceiver.CheckViettelNumber() ? tmpPackage.VIETTEL_PRICE : tmpPackage.OTHER_PRICE);

                                    // Cot So tien truong thanh toan cho Viettel
                                    firstSheet.SetCellValue("I" + startRow, MoneyCustomerPayVT);
                                }
                            }
                            #endregion

                            #region Cot hinh thuc dang ky
                            if (contractDetailObj.RegistrationType == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS)
                            {
                                firstSheet.SetCellValue("J" + startRow, "SMS");
                            }
                            else
                            {
                                firstSheet.SetCellValue("J" + startRow, "Trực tiếp");
                            }
                            #endregion

                            //cot trang thai
                            firstSheet.SetCellValue("K" + startRow, contractDetailObj.SubscriptionStatus.GetStatusContractDetail());//Trang thai
                            firstSheet.GetRange(startRow, 1, startRow, 12).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                            startRow += 1;

                        }
                    }
                    else
                    {
                        startRow += 1;
                    }
                }
            }

            firstSheet.SetCellValue("J6", "=SUM(J" + beginRow + ":J" + startRow + ")");
            firstSheet.Worksheet.PageSetup.Orientation = NativeExcel.XlPageOrientation.xlLandscape;
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string reportName = "DS lien lac.xls";
            result.FileDownloadName = reportName;
            //VTODO
            //WriteLogInfo("Xuất excel thông tin hợp đồng đã đăng ký", 0, string.Empty, string.Empty, 0, string.Empty);
            return result;

        }

        public FileResult ExportExcelRegis(FormCollection frm)
        {
            try
            {
                int schoolID = _globalInfo.SchoolID.Value;
                int year = Convert.ToInt32(frm["HYear"]);
                int educationLevel = Convert.ToInt32(frm["HEducationLevel"]);
                int classID = Convert.ToInt32(frm["HClassID"]);
                string pupilName = frm["HPupilName"];
                string pupilCode = frm["HPupilCode"];
                string mobilephone = frm["HMobilePhone"];
                int subscriptionStatus = GlobalConstantsEdu.STATUS_NOT_REGISTED;
                string className = frm["HfClassName"];
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["Year"] = year;
                dic["LevelID"] = educationLevel; //khối
                dic["ClassID"] = classID;
                dic["PupilName"] = pupilName;
                dic["PupilCode"] = pupilCode;
                dic["SubscriptionStatus"] = subscriptionStatus;
                dic["level"] = _globalInfo.AppliedLevel;

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + "/MaudanhsachdangkySMSParent.xls";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                // Lấy sheet template                  
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                // Ten truong
                firstSheet.SetCellValue("A2", _globalInfo.SchoolName);
                // Tieu de
                firstSheet.SetCellValue("A4", "DANH SÁCH LIÊN LẠC");
                // Nam hoc
                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                string schoolYearTitle = string.Format(Res.Get("SMSParentManageContract_Label_SchoolYearTitle"), aca.Year, aca.Year + 1);
                firstSheet.SetCellValue("A5", schoolYearTitle.ToUpper());

                int startRow = 10;
                // Anhvd9 20160224 -- Các gói cước cho danh sách PHHS chưa đăng ký (bao gồm cả các gói phụ)
                int semester = this.Semester;
                List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value, false);
                //.Where(o => o.IsAllowRegisterSideSubscriber != true).ToList();
                //Danh sach hoc sinh cua truong theo dk tiem kiem 
                List<PupilProfileBO> listPupilProfile = PupilProfileBusiness.GetPupilsformatString(schoolID, _globalInfo.AcademicYearID.Value, classID, pupilCode, pupilName, educationLevel, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault());
                if (listPupilProfile != null && listPupilProfile.Count > 0)
                {
                    listPupilProfile = listPupilProfile.Where(p => p.ProfileStatus == GlobalConstantsEdu.COMMON_PUPIL_STATUS_STUDYING).ToList();
                }

                List<SMSParentContractGroupBO> listResult;
                if (!string.IsNullOrWhiteSpace(mobilephone))
                {
                    listResult = new List<SMSParentContractGroupBO>();
                }
                else
                {
                    listResult = smsParentContractBusiness.GetListExcelRegis(listPupilProfile, schoolID, year, subscriptionStatus);
                }

                SMSParentContractGroupBO groupObj = null;
                int orderNumber = 0;
                string relationship = string.Empty;
                List<VTDataValidation> lstValidation = new List<VTDataValidation>();
                VTDataValidation objValidation;
                string serviceCode = null;

                if (listService.Count > 0)
                {
                    serviceCode = listService.FirstOrDefault().ServiceCode;
                }
                string[] servicePackageList = listService.Select(p => p.ServiceCode).ToArray();
                string[] servicetmp = null;
                List<int> lstPupilID = listResult.Select(p => p.PupilID).Distinct().ToList();
                //lay ra danh sach hoc sinh duoc dang ky goi SMSFree
                Dictionary<string, object> dicRegister = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"YearID",_globalInfo.AcademicYearID},
                    {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE},
                    {"lstPupilID",lstPupilID}
                };
                List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();
                if (listResult != null && listResult.Count > 0)
                {
                    for (int i = 0; i < listResult.Count; i++)
                    {
                        servicetmp = servicePackageList;
                        groupObj = listResult[i];
                        objValidation = new VTDataValidation();
                        objValidation.FromRow = 10;
                        objValidation.ToRow = 10 + i;
                        objValidation.FromColumn = 8;
                        objValidation.ToColumn = 8;
                        objValidation.SheetIndex = 1;
                        if (!lstPupilRegisterSMSFreeID.Contains(groupObj.PupilID))
                        {
                            servicetmp = servicePackageList.Where(p => !p.Equals(GlobalConstantsEdu.SMS_FREE)).ToArray();
                        }
                        objValidation.Contrains = servicetmp;
                        objValidation.Type = VTValidationType.LIST;
                        lstValidation.Add(objValidation);

                        firstSheet.SetCellValue("A" + startRow, orderNumber + 1); orderNumber++;
                        firstSheet.SetCellValue("B" + startRow, groupObj.PupilCode);
                        firstSheet.SetCellValue("C" + startRow, groupObj.PupilName);
                        firstSheet.SetCellValue("D" + startRow, groupObj.ClassName);
                        //cot quan he voi hoc sinh
                        firstSheet.SetCellValue("E" + startRow, Res.Get("Lbl_Mother"));
                        //Ten nguoi nhan tin
                        firstSheet.SetCellValue("F" + startRow, groupObj.MotherName);
                        //SDT nhan tin
                        ContractDetailList cd = groupObj.contractDetailList.FirstOrDefault();
                        firstSheet.SetCellValue("G" + startRow, cd != null ? cd.PhoneReceiver : string.Empty);

                        //Goi cuoc combobox
                        firstSheet.SetCellValue("H" + startRow, serviceCode);

                        //Thoi han combobox
                        firstSheet.SetCellValue("I" + startRow, SMSParentManageContractConstant.STR_SEMESTER_ALL); // Mặc định chọn Cả năm

                        firstSheet.GetRange(startRow, 1, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                        startRow += 1;
                    }
                }

                string[] subscriptionTime = new string[] { SMSParentManageContractConstant.HKI, SMSParentManageContractConstant.HKII, SMSParentManageContractConstant.STR_SEMESTER_ALL };
                objValidation = new VTDataValidation();
                objValidation.FromRow = 10;
                objValidation.ToRow = 10 + listResult.Count;
                objValidation.FromColumn = 9;
                objValidation.ToColumn = 9;
                objValidation.SheetIndex = 1;
                objValidation.Contrains = subscriptionTime;
                objValidation.Type = VTValidationType.LIST;
                lstValidation.Add(objValidation);

                string[] relationshiparr = new string[] { Res.Get("Lbl_Father"), Res.Get("Lbl_Mother"), Res.Get("Lbl_Other") };
                objValidation = new VTDataValidation();
                objValidation.FromRow = 10;
                objValidation.ToRow = 10 + listResult.Count;
                objValidation.FromColumn = 5;
                objValidation.ToColumn = 5;
                objValidation.SheetIndex = 1;
                objValidation.Contrains = relationshiparr;
                objValidation.Type = VTValidationType.LIST;
                lstValidation.Add(objValidation);

                //Stream excel = oBook.ToStream();
                Stream excel = oBook.ToStreamValidationData(lstValidation);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string reportName = "Mau dang ky.xls";
                result.FileDownloadName = reportName;
                //VTODO WriteLogInfo("Xuất excel danh sách chưa đăng ký", 0, string.Empty, string.Empty, 0, string.Empty);
                return result;
            }
            catch (NullReferenceException ex)
            {
                //VTODO
                //
                string para = "";
                LogExtensions.ErrorExt(logger, DateTime.Now, "ExportExcelRegis", para, ex);
                return null;
            }
            catch (Exception ex)
            {
                //VTODO
                //
                LogExtensions.ErrorExt(logger, DateTime.Now, "ExportExcelRegis", "", ex);
                return null;
            }
        }

        public FileResult ExportExtra(FormCollection frm)
        {

            int schoolID = _globalInfo.SchoolID.Value;
            int year = Convert.ToInt32(frm["HYear"]);
            int educationLevel = Convert.ToInt32(frm["HEducationLevel"]);
            int classID = Convert.ToInt32(frm["HClassID"]);
            string pupilName = frm["HPupilName"];
            string pupilCode = frm["HPupilCode"];
            string mobilephone = (frm["HMobilePhone"]).Trim();
            int subscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
            string className = frm["HfClassName"];

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + "/MuaThemTinNhan.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            // Lấy sheet template                  
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            // Ten truong
            firstSheet.SetCellValue("A2", _globalInfo.SchoolName);
            // Tieu de
            //firstSheet.SetCellValue("A4", "DANH SÁCH LIÊN LẠC");
            // Nam hoc
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string schoolYearTitle = string.Format(Res.Get("SMSParentManageContract_Label_SchoolYearTitle"), aca.Year, aca.Year + 1);
            firstSheet.SetCellValue("A5", schoolYearTitle.ToUpper());

            int startRow = 10;
            // Anhvd9 20160224 -- Các gói cước cho danh sách PHHS chưa đăng ký (bao gồm cả các gói phụ)
            int semester = this.Semester;
            List<ServicePackageBO> listService = servicePackageBusiness.GetListExtraServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            //.Where(o => o.IsAllowRegisterSideSubscriber != true).ToList();
            //Danh sach hoc sinh cua truong theo dk tiem kiem 
            List<PupilProfileBO> listPupilProfile = PupilProfileBusiness.GetPupilsformatString(schoolID, _globalInfo.AcademicYearID.Value, classID, pupilCode, pupilName, educationLevel, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault());

            if (listPupilProfile != null && listPupilProfile.Count > 0)
            {
                listPupilProfile = listPupilProfile.Where(p => p.ProfileStatus == GlobalConstantsEdu.COMMON_PUPIL_STATUS_STUDYING).ToList();
            }
            List<SMSParentContractGroupBO> listResult = smsParentContractBusiness.GetListExcelExtra(listPupilProfile, schoolID, year, subscriptionStatus, mobilephone);
            SMSParentContractGroupBO groupObj = null;
            int orderNumber = 0;
            string relationship = string.Empty;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation;
            string serviceCode = listService.Count > 0 ? listService.FirstOrDefault().ServiceCode : string.Empty;
            string[] servicePackageList = listService.Select(p => p.ServiceCode).ToArray();
            string[] servicetmp = null;
            List<int> lstPupilID = listResult.Select(p => p.PupilID).Distinct().ToList();

            if (listResult != null && listResult.Count > 0)
            {
                for (int i = 0; i < listResult.Count; i++)
                {
                    servicetmp = servicePackageList;
                    groupObj = listResult[i];
                    objValidation = new VTDataValidation();
                    objValidation.FromRow = 10;
                    objValidation.ToRow = 10 + i;
                    objValidation.FromColumn = 8;
                    objValidation.ToColumn = 8;
                    objValidation.SheetIndex = 1;

                    objValidation.Contrains = servicetmp;
                    objValidation.Type = VTValidationType.LIST;
                    lstValidation.Add(objValidation);

                    firstSheet.SetCellValue("A" + startRow, orderNumber + 1); orderNumber++;
                    firstSheet.SetCellValue("B" + startRow, groupObj.PupilCode);
                    firstSheet.SetCellValue("C" + startRow, groupObj.PupilName);
                    firstSheet.SetCellValue("D" + startRow, groupObj.ClassName);
                    //cot quan he voi hoc sinh
                    firstSheet.SetCellValue("E" + startRow, Res.Get("Lbl_Mother"));
                    //Ten nguoi nhan tin
                    firstSheet.SetCellValue("F" + startRow, groupObj.ReceiverName);
                    //SDT nhan tin

                    firstSheet.SetCellValue("G" + startRow, groupObj.Subscriber);
                    //Goi cuoc combobox
                    firstSheet.SetCellValue("H" + startRow, serviceCode);

                    //Thoi han combobox
                    firstSheet.SetCellValue("I" + startRow, groupObj.SubciptionTime == 3 ? SMSParentManageContractConstant.STR_SEMESTER_ALL :
                        groupObj.SubciptionTime == 2 ? SMSParentManageContractConstant.HKII : SMSParentManageContractConstant.HKI); // Mặc định chọn Cả năm

                    firstSheet.GetRange(startRow, 1, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                    startRow += 1;
                }
            }


            string[] subscriptionTime = new string[] { SMSParentManageContractConstant.HKI, SMSParentManageContractConstant.HKII, SMSParentManageContractConstant.STR_SEMESTER_ALL };
            objValidation = new VTDataValidation();
            objValidation.FromRow = 10;
            objValidation.ToRow = 10 + listResult.Count;
            objValidation.FromColumn = 9;
            objValidation.ToColumn = 9;
            objValidation.SheetIndex = 1;
            objValidation.Contrains = subscriptionTime;
            objValidation.Type = VTValidationType.LIST;
            lstValidation.Add(objValidation);

            string[] relationshiparr = new string[] { Res.Get("Lbl_Father"), Res.Get("Lbl_Mother"), Res.Get("Lbl_Other") };
            objValidation = new VTDataValidation();
            objValidation.FromRow = 10;
            objValidation.ToRow = 10 + listResult.Count;
            objValidation.FromColumn = 5;
            objValidation.ToColumn = 5;
            objValidation.SheetIndex = 1;
            objValidation.Contrains = relationshiparr;
            objValidation.Type = VTValidationType.LIST;
            lstValidation.Add(objValidation);

            //Stream excel = oBook.ToStream();

            Stream excel = oBook.ToStreamValidationData(lstValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string reportName = "Mua them tin nhan.xls";
            result.FileDownloadName = reportName;
            //VTODO WriteLogInfo("Xuất excel danh sách mua thêm", 0, string.Empty, string.Empty, 0, string.Empty);
            return result;

        }


        [ValidateAntiForgeryToken]
        /// <summary>
        /// Import thông tin lên Grid
        /// </summary>
        /// <returns></returns>
        public ActionResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int importType)
        {
            #region Dang ky
            if (importType == 1)
            {
                #region validate
                //-------------------------------------------
                //  Check data post has exist 
                //-------------------------------------------
                if (attachments == null || attachments.Count() <= 0)
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));

                // The Name of the Upload component is "attachments"
                HttpPostedFileBase file = attachments.FirstOrDefault();
                //-------------------------------------------
                //  Check the excel mime types
                //-------------------------------------------
                if (file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), GlobalConstants.TYPE_ERROR));
                //-------------------------------------------
                //  Check the excel extension
                //-------------------------------------------
                string extension = Path.GetExtension(file.FileName);
                if (extension.ToUpper() != GlobalConstantsEdu.EXCEL_TYPE_XLS && extension.ToUpper() != GlobalConstantsEdu.EXCEL_TYPE_XLSX)
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), GlobalConstants.TYPE_ERROR));
                //-------------------------------------------
                //  Attempt to read the file and check the first bytes
                //-------------------------------------------
                if (!file.InputStream.CanRead)
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));

                //-------------------------------------------
                //  Check the excel file max size is 1024 KB
                //-------------------------------------------
                if (file.ContentLength > 1024 * 1024) return Json(new JsonMessage(Res.Get("Validate_MaxSizeExcel_1MB"), GlobalConstants.TYPE_ERROR));
                #endregion

                string FilePath = string.Empty;
                if (file != null)
                {
                    try
                    {
                        string fileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid(), extension);
                        string UploadPath = (WebConfigurationManager.AppSettings["UploadFilePath"] ?? string.Empty).ToString();
                        string physicalPath = Path.Combine(UploadPath, fileName);
                        file.SaveAs(physicalPath);
                        FilePath = physicalPath;
                        IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                        IVTWorksheet sheet = oBook.GetSheet(1);
                        IVTRange range = sheet.GetRow(1);

                        if (sheet.GetCellValue("A2") != null && sheet.GetCellValue("A4") != null && sheet.GetCellValue("A5") != null
                            && sheet.GetCellValue("A8") != null && sheet.GetCellValue("B8") != null && sheet.GetCellValue("C8") != null && sheet.GetCellValue("D8") != null
                            && sheet.GetCellValue("E8") != null && sheet.GetCellValue("E9") != null && sheet.GetCellValue("F9") != null && sheet.GetCellValue("G9") != null
                            && sheet.GetCellValue("H9") != null && sheet.GetCellValue("I9") != null)
                        {
                            #region validate template
                            string schoolNameExcel = sheet.GetCellValue("A2").ToString().Trim().ToUpper();
                            string schoolName = _globalInfo.SchoolName.ToUpper();
                            if (!schoolNameExcel.Equals(schoolName))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }

                            string titleExcel = sheet.GetCellValue("A4").ToString().Trim().ToUpper();
                            string title = Res.Get("Title_Excel_Import_SMSParent").Trim().ToUpper();
                            if (!titleExcel.Equals(title))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }

                            string schoolYearExcel = sheet.GetCellValue("A5").ToString().ToUpper();
                            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                            string schoolYear = (Res.Get("ReportCodeOnline_Label_SchoolYearTitle") + "" + aca.Year + "-" + (aca.Year + 1)).ToUpper();
                            if (!schoolYear.Equals(schoolYearExcel))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }
                            #endregion

                            #region Khoi tao gia tri hien tai

                            int schoolID = _globalInfo.SchoolID.Value;
                            int semester = this.Semester;
                            int year = aca.Year;
                            int academicYearID = _globalInfo.AcademicYearID.Value;
                            int provinceID = _globalInfo.ProvinceID.Value;
                            AcademicYear academicYear = aca;
                            // Anhvd9 20160224 - Danh sách đã bao gồm các gói phụ
                            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(year, semester, academicYear, provinceID, schoolID, false);


                            // Validate excel file
                            List<SMSParentContractGroupBO> listExcel = this.ValidateDataFromExcel(sheet, listService);
                            #endregion

                            // remove after imported
                            System.IO.File.Delete(FilePath);

                            if (listExcel.Count <= 0) return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));

                            List<SMSParentContractGroupBO> listError = listExcel.Where(p => p.Error).ToList();
                            if (listError.Count > 0)
                            {
                                #region tra ve loi
                                //tra ve pop loi
                                ViewData[SMSParentManageContractConstant.ListStatusContract] = listError;
                                ViewData[SMSParentManageContractConstant.ListServicePackage] = servicePackageBusiness.GetListServicesPackage(year, semester, academicYear, provinceID, _globalInfo.SchoolID.Value).ToList();
                                //return PartialView("_ViewError");
                                return Json(new
                                {
                                    Type = "_ViewError",
                                    Message = Res.Get("Common_Button_ImportData"),
                                    PartialViewHtml = RenderPartialViewToString("_ViewError", null)
                                });

                                #endregion
                            }
                            else
                            {
                                #region Luu excel
                                List<int> pupilIDList = listExcel.Select(p => p.PupilID).Distinct().ToList();
                                List<SMSParentContractBO> listContractInsert = new List<SMSParentContractBO>();
                                SMSParentContractGroupBO contractGroupObj = null;
                                SMSParentContractBO contactObj = null;
                                //chi tiet hop dong
                                List<SMSParentContractDetailBO> listContractDetailInsert = new List<SMSParentContractDetailBO>();
                                List<ContractDetailList> listContractDetailInsertTmp = new List<ContractDetailList>();
                                SMSParentContractDetailBO contractDetail = null;
                                ContractDetailList detailObjTmp = null;
                                //Lay thong tin so du tai khoan truong
                                EwalletBO ewalletObj = ewalletBusiness.GetEWallet(schoolID, true, false);
                                // Lay thong tin tu file excel
                                List<SMSParentContractGroupBO> listContractTmp = new List<SMSParentContractGroupBO>();
                                decimal Balance = ewalletObj.Balance;
                                decimal? totalPay = 0;
                                decimal? moneyContract = 0;
                                string phone = string.Empty;
                                int countVT = 0;
                                int countOther = 0;
                                int quantityPhone = 0;
                                int pupilID = 0;
                                //insert xuong DB
                                if (pupilIDList != null && pupilIDList.Count > 0)
                                {
                                    for (int i = 0; i < pupilIDList.Count; i++)
                                    {
                                        pupilID = pupilIDList[i];
                                        contactObj = new SMSParentContractBO();
                                        listContractTmp = listExcel.Where(p => p.PupilID == pupilID).ToList();// danh sach hoc sinh co nhieu thue bao
                                        contractGroupObj = listContractTmp.FirstOrDefault();
                                        contactObj.SchoolID = schoolID;
                                        contactObj.PhoneMainContract = contractGroupObj.MotherMobile;
                                        contactObj.ContractName = contractGroupObj.MotherName;
                                        contactObj.RelationShip = contractGroupObj.RelationshipContract;
                                        contactObj.PupilFileID = contractGroupObj.PupilID;
                                        contactObj.StatusContract = contractGroupObj.StatusContract.Value;
                                        contactObj.CreateTime = contractGroupObj.CreateTime;
                                        contactObj.AcademicYearID = academicYearID;
                                        contactObj.ClassID = contractGroupObj.ClassID;
                                        contactObj.SMSParentContractID = contractGroupObj.SMSParentContractID;
                                        contactObj.FullName = contractGroupObj.PupilName;
                                        contactObj.PupilOfClassID = contractGroupObj.PupilOfClassID;
                                        contactObj.ClassID = contractGroupObj.ClassID;
                                        contactObj.PupilFileID = contractGroupObj.PupilID;
                                        listContractDetailInsert = new List<SMSParentContractDetailBO>();

                                        #region chi tiet hop dong
                                        //chi tiet hop dong
                                        for (int j = 0; j < listContractTmp.Count; j++)
                                        {
                                            listContractDetailInsertTmp = listContractTmp[j].contractDetailList;
                                            if (listContractDetailInsertTmp != null && listContractDetailInsertTmp.Count > 0)
                                            {
                                                for (int k = 0; k < listContractDetailInsertTmp.Count; k++)
                                                {
                                                    detailObjTmp = listContractDetailInsertTmp[k];
                                                    phone = detailObjTmp.PhoneReceiver;

                                                    if (phone.CheckMobileNumberVT())
                                                    {
                                                        countVT += 1;
                                                        quantityPhone += 1;
                                                    }
                                                    else
                                                    {
                                                        countOther += 1;
                                                        quantityPhone += 1;
                                                    }

                                                    if (detailObjTmp.AllSemester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                    {
                                                        contractDetail = new SMSParentContractDetailBO();
                                                        contractDetail.Subscriber = detailObjTmp.PhoneReceiver;
                                                        contractDetail.SubscriberType = contractDetail.Subscriber.CheckMobileNumberVT() ? (short)0 : (short)1;
                                                        contractDetail.SubscriptionTime = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                                        contractDetail.SubscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
                                                        contractDetail.Year = year;
                                                        contractDetail.Money = detailObjTmp.Money;
                                                        contractDetail.IsActive = true;
                                                        contractDetail.CreatedTime = DateTime.Now;
                                                        contractDetail.ServicePackageID = detailObjTmp.ServicesPakageID;
                                                        contractDetail.ReceiverName = detailObjTmp.ReceiverName;
                                                        contractDetail.Relationship = detailObjTmp.RelationShip == "Mẹ" ? "Mother" :
                                                            detailObjTmp.RelationShip == "Bố" ? "Father" : "Other";
                                                        //tinh tien dang ky thue bao
                                                        moneyContract = this.GetMoneyByServiceID(detailObjTmp.ServicesPakageID.Value, detailObjTmp.PhoneReceiver, listService);
                                                        //khi dang ky 2 ki nhung thoi gian hien tai la hoc ki 2 thi chi tinh tien cho hoc ky 2

                                                        totalPay += moneyContract * 2;

                                                    }
                                                    else if (detailObjTmp.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || detailObjTmp.SecondSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                                    {
                                                        contractDetail = new SMSParentContractDetailBO();
                                                        contractDetail.Subscriber = detailObjTmp.PhoneReceiver;
                                                        contractDetail.SubscriberType = contractDetail.Subscriber.CheckMobileNumberVT() ? (short)0 : (short)1;
                                                        contractDetail.SubscriptionTime = detailObjTmp.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? GlobalConstants.SEMESTER_OF_YEAR_FIRST : GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                                                        contractDetail.SubscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
                                                        contractDetail.Year = year;
                                                        contractDetail.Money = detailObjTmp.Money;
                                                        contractDetail.IsActive = true;
                                                        contractDetail.CreatedTime = DateTime.Now;
                                                        contractDetail.ServicePackageID = detailObjTmp.ServicesPakageID;
                                                        contractDetail.ReceiverName = detailObjTmp.ReceiverName;
                                                        contractDetail.Relationship = detailObjTmp.RelationShip == "Mẹ" ? "Mother" :
                                                            detailObjTmp.RelationShip == "Bố" ? "Father" : "Other";
                                                        //tinh tien dang ky thue baos
                                                        moneyContract = this.GetMoneyByServiceID(detailObjTmp.ServicesPakageID.Value, detailObjTmp.PhoneReceiver, listService);
                                                        totalPay += moneyContract;
                                                    }
                                                    listContractDetailInsert.Add(contractDetail);
                                                }
                                            }
                                        }
                                        #endregion

                                        contactObj.SMSParentContractDetailList = listContractDetailInsert;
                                        listContractInsert.Add(contactObj);
                                    }
                                }

                                if (listContractInsert != null && listContractInsert.Count > 0)
                                {
                                    // tra ve cleint de kiem tra
                                    Session[SMSParentManageContractConstant.LIST_IMPORT] = listContractInsert;
                                    Session[SMSParentManageContractConstant.PRICE_PAY] = totalPay;
                                    return Json(new { Type = GlobalConstants.TYPE_SUCCESS, totalPay = totalPay, Balance = Balance, EWalletID = ewalletObj.EWalletID, countVT = countVT, countOther = countOther, quantityPhone = quantityPhone, pupilNumber = listContractInsert.Count, importType = 1 });
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            // remove if invalid template
                            System.IO.File.Delete(FilePath);
                            return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                        }
                    }
                    catch (Exception ex)
                    {
                        //VTODO
                        //
                        string para = "";
                        LogExtensions.ErrorExt(logger, DateTime.Now, "ImportExcel", para, ex);
                    }
                    finally
                    {
                        // remove if exist
                        if (!string.IsNullOrEmpty(FilePath) && System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }

                    return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                }
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));
            }
            #endregion

            #region Mua them
            else
            {
                #region validate
                //-------------------------------------------
                //  Check data post has exist 
                //-------------------------------------------
                if (attachments == null || attachments.Count() <= 0)
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));

                // The Name of the Upload component is "attachments"
                HttpPostedFileBase file = attachments.FirstOrDefault();
                //-------------------------------------------
                //  Check the excel mime types
                //-------------------------------------------
                if (file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), GlobalConstants.TYPE_ERROR));
                //-------------------------------------------
                //  Check the excel extension
                //-------------------------------------------
                string extension = Path.GetExtension(file.FileName);
                if (extension.ToUpper() != GlobalConstantsEdu.EXCEL_TYPE_XLS && extension.ToUpper() != GlobalConstantsEdu.EXCEL_TYPE_XLSX)
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), GlobalConstants.TYPE_ERROR));
                //-------------------------------------------
                //  Attempt to read the file and check the first bytes
                //-------------------------------------------
                if (!file.InputStream.CanRead)
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));

                //-------------------------------------------
                //  Check the excel file max size is 1024 KB
                //-------------------------------------------
                if (file.ContentLength > 1024 * 1024) return Json(new JsonMessage(Res.Get("Validate_MaxSizeExcel_1MB"), GlobalConstants.TYPE_ERROR));
                #endregion

                string FilePath = string.Empty;
                if (file != null)
                {
                    try
                    {
                        string fileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid(), extension);
                        string UploadPath = (WebConfigurationManager.AppSettings["UploadFilePath"] ?? string.Empty).ToString();
                        string physicalPath = Path.Combine(UploadPath, fileName);
                        file.SaveAs(physicalPath);
                        FilePath = physicalPath;
                        IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                        IVTWorksheet sheet = oBook.GetSheet(1);
                        IVTRange range = sheet.GetRow(1);

                        if (sheet.GetCellValue("A2") != null && sheet.GetCellValue("A4") != null && sheet.GetCellValue("A5") != null
                            && sheet.GetCellValue("A8") != null && sheet.GetCellValue("B8") != null && sheet.GetCellValue("C8") != null && sheet.GetCellValue("D8") != null
                            && sheet.GetCellValue("E8") != null && sheet.GetCellValue("E9") != null && sheet.GetCellValue("F9") != null && sheet.GetCellValue("G9") != null
                            && sheet.GetCellValue("H9") != null && sheet.GetCellValue("I9") != null)
                        {
                            #region validate template
                            string schoolNameExcel = sheet.GetCellValue("A2").ToString().Trim().ToUpper();
                            string schoolName = _globalInfo.SchoolName.ToUpper();
                            if (!schoolNameExcel.Equals(schoolName))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }

                            string titleExcel = sheet.GetCellValue("A4").ToString().Trim().ToUpper();
                            string title = "DANH SÁCH MUA THÊM TIN NHẮN".Trim().ToUpper();
                            if (!titleExcel.Equals(title))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }

                            string schoolYearExcel = sheet.GetCellValue("A5").ToString().ToUpper();
                            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                            string schoolYear = (Res.Get("ReportCodeOnline_Label_SchoolYearTitle") + "" + aca.Year + "-" + (aca.Year + 1)).ToUpper();
                            if (!schoolYear.Equals(schoolYearExcel))
                            {
                                // remove if invalid
                                System.IO.File.Delete(FilePath);
                                return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                            }
                            #endregion

                            #region Khoi tao gia tri hien tai
                            int schoolID = _globalInfo.SchoolID.Value;
                            int semester = this.Semester;
                            int year = aca.Year;
                            int academicYearID = _globalInfo.AcademicYearID.Value;
                            int provinceID = _globalInfo.ProvinceID.Value;
                            AcademicYear academicYear = aca;
                            // Anhvd9 20160224 - Danh sách đã bao gồm các gói phụ
                            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(year, semester, academicYear, provinceID, schoolID);


                            // Validate excel file
                            List<SMSParentContractGroupBO> listExcel = this.ValidateDataFromExcelExtra(sheet, listService);
                            #endregion

                            // remove after imported
                            System.IO.File.Delete(FilePath);

                            if (listExcel.Count <= 0) return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));

                            List<SMSParentContractGroupBO> listError = listExcel.Where(p => p.Error).ToList();
                            if (listError.Count > 0)
                            {
                                #region tra ve loi
                                //tra ve pop loi
                                ViewData[SMSParentManageContractConstant.ListStatusContract] = listError;
                                ViewData[SMSParentManageContractConstant.ListServicePackage] = servicePackageBusiness.GetListServicesPackage(year, semester, academicYear, provinceID, _globalInfo.SchoolID.Value).ToList();
                                //return PartialView("_ViewError");
                                return Json(new
                                {
                                    Type = "_ViewError",
                                    Message = Res.Get("Common_Button_ImportData"),
                                    PartialViewHtml = RenderPartialViewToString("_ViewError", null)
                                });

                                #endregion
                            }
                            else
                            {
                                #region Luu excel
                                List<SMSParentContractDetailBO> listContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(schoolID, aca.Year);

                                List<int> pupilIDList = listExcel.Select(p => p.PupilID).Distinct().ToList();

                                //Lay thong tin so du tai khoan truong
                                EwalletBO ewalletObj = ewalletBusiness.GetEWallet(schoolID, true, false);
                                // Lay thong tin tu file excel
                                List<SMSParentContractGroupBO> listContractTmp = new List<SMSParentContractGroupBO>();
                                decimal Balance = ewalletObj.Balance;

                                List<SMS_PARENT_CONTRACT_EXTRA> lstSceInsert = new List<SMS_PARENT_CONTRACT_EXTRA>();
                                SMS_PARENT_CONTRACT_EXTRA obj;
                                //ServicePackage sp = servicePackageBusiness.Find(extraServicePackageID);
                                List<ServicePackageBO> lstService = servicePackageBusiness.GetListExtraServicesPackage(aca.Year, semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
                                List<SMSParentContractBO> lstContract = smsParentContractBusiness.GetListSMSParentContractDetailForExtra(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, 0, aca.Year, 0, 0);


                                decimal amount = 0;
                                int countVT = 0;
                                int countOther = 0;
                                int quantityPhone = 0;
                                for (int i = 0; i < listExcel.Count; i++)
                                {
                                    SMSParentContractGroupBO objContract = listExcel[i];
                                    ContractDetailList detail = objContract.contractDetailList[0];
                                    ServicePackageBO sp = lstService.FirstOrDefault(o => o.ServicePackageID == detail.ServicesPakageID);


                                    obj = new SMS_PARENT_CONTRACT_EXTRA();
                                    obj.CREATED_TIME = DateTime.Now;
                                    obj.IS_ACTIVE = true;

                                    int iSemester = 0;


                                    if (detail.AllSemester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                    {
                                        iSemester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                                    }
                                    else
                                    {
                                        if (detail.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                        {
                                            iSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                                        }

                                        if (detail.SecondSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                        {
                                            iSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                                        }
                                    }

                                    if (iSemester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                    {
                                        if (objContract.contractDetailList[0].PhoneReceiver.CheckMobileNumberVT())
                                        {
                                            obj.AMOUNT = (long)(sp.ViettelPrice.HasValue ? sp.ViettelPrice.Value * 2 : 0);
                                            countVT++;
                                            quantityPhone++;
                                        }
                                        else
                                        {
                                            obj.AMOUNT = (long)(sp.OtherPrice.HasValue ? sp.OtherPrice.Value * 2 : 0);
                                            countOther++;
                                            quantityPhone++;
                                        }
                                    }
                                    else
                                    {
                                        if (detail.PhoneReceiver.CheckMobileNumberVT())
                                        {
                                            obj.AMOUNT = (long)(sp.ViettelPrice.HasValue ? sp.ViettelPrice.Value : 0);
                                            countVT++;
                                            quantityPhone++;
                                        }
                                        else
                                        {
                                            obj.AMOUNT = (long)(sp.OtherPrice.HasValue ? sp.OtherPrice.Value : 0);
                                            countOther++;
                                            quantityPhone++;
                                        }
                                    }

                                    SMSParentContractBO contract = lstContract.Where(p => p.SMSParentContractID == objContract.SMSParentContractID
                                                       && p.Subcriber == detail.PhoneReceiver
                                                       && p.SubscriptionTime == iSemester).FirstOrDefault();

                                    obj.SERVICE_PACKAGE_ID = sp.ServicePackageID;
                                    obj.SMS_PARENT_CONTRACT_DETAIL_ID = contract.SMSParentContractDetailID;
                                    obj.SMS_PARENT_CONTRACT_ID = contract.SMSParentContractID;
                                    obj.SUB_SCRIPTION_TIME = contract.SubscriptionTime.Value;

                                    lstSceInsert.Add(obj);

                                    amount = amount + (obj.SUB_SCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL ? this.GetMoneyByServiceID(sp.ServicePackageID, detail.PhoneReceiver, listService) * 2 :
                                        this.GetMoneyByServiceID(sp.ServicePackageID, detail.PhoneReceiver, listService));
                                }

                                if (lstSceInsert != null && lstSceInsert.Count > 0)
                                {
                                    // tra ve cleint de kiem tra
                                    Session[SMSParentManageContractConstant.LIST_IMPORT_EXTRA] = lstSceInsert;
                                    Session[SMSParentManageContractConstant.PRICE_PAY_EXTRA] = amount;
                                    return Json(new { Type = GlobalConstants.TYPE_SUCCESS, totalPay = amount, Balance = Balance, EWalletID = ewalletObj.EWalletID, countVT = countVT, countOther = countOther, quantityPhone = quantityPhone, pupilNumber = lstSceInsert.Count, importType = 2 });
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            // remove if invalid template
                            System.IO.File.Delete(FilePath);
                            return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                        }
                    }
                    catch (Exception ex)
                    {
                        //VTODO
                        //
                        string para = "";
                        LogExtensions.ErrorExt(logger, DateTime.Now, "ImportExcel", para, ex);
                    }
                    finally
                    {
                        // remove if exist
                        if (!string.IsNullOrEmpty(FilePath) && System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }

                    return Json(new JsonMessage(Res.Get("Validate_Error_Excel"), GlobalConstants.TYPE_ERROR));
                }
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), GlobalConstants.TYPE_ERROR));
            }

            #endregion
        }



        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName)) viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// review luu tien trong session
        /// </summary>
        /// <param name="saveImport"></param>
        /// <param name="eWalletID"></param>
        /// <param name="totalPay"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SaveImport(bool saveImport, int eWalletID, decimal totalPay, int quantity, bool allowDeferredPayment = false)
        {
            if (!saveImport)
            {
                //huy session
                Session.Remove(SMSParentManageContractConstant.LIST_IMPORT);
                Session.Remove(SMSParentManageContractConstant.PRICE_PAY);
            }
            else
            {
                if (Session[SMSParentManageContractConstant.LIST_IMPORT] != null)
                {
                    try
                    {
                        AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                        List<SMSParentContractBO> listSourceExcel = (List<SMSParentContractBO>)Session[SMSParentManageContractConstant.LIST_IMPORT];
                        // list insert vao contract va contractdetail
                        List<SMSParentContractBO> listContractInsert = listSourceExcel.Where(p => p.SMSParentContractID == 0).ToList();

                        // Số tiển trừ
                        decimal? price = (decimal)Session[SMSParentManageContractConstant.PRICE_PAY];
                        if (allowDeferredPayment)
                        {

                            // Kiem tra danh sach chua co thue bao tra cham
                            if (smsParentContractBusiness.checkExistUnpaidSubscriber(listContractInsert))
                            {
                                Session.Remove(SMSParentManageContractConstant.LIST_IMPORT);
                                Session.Remove(SMSParentManageContractConstant.PRICE_PAY);
                                return Json(new JsonMessage(Res.Get("Validate_DeferredPayment_ExistSubscriber"), GlobalConstants.TYPE_ERROR));
                            }
                            // Ghi nhan giao dich (tru tien = 0)
                            price = 0;
                            // Trường hợp Trả chậm
                            foreach (SMSParentContractBO contract in listContractInsert)
                            {
                                if (contract != null && contract.SMSParentContractDetailList.Count > 0)
                                {
                                    foreach (SMSParentContractDetailBO detail in contract.SMSParentContractDetailList)
                                    {
                                        // Trạng thái Nợ cước
                                        detail.SubscriptionStatus = GlobalConstantsEdu.STATUS_UNPAID;
                                    }
                                }
                            }
                        }
                        // Cap nhat cac hoc sinh dk hop dong
                        if (listContractInsert != null && listContractInsert.Count > 0) Session[SMSParentManageContractConstant.LIST_SYNC_SMAS3] = listContractInsert.Select(p => p.PupilOfClassID).ToList();

                        List<SMSParentContractBO> listContractGroupInsert = listSourceExcel.Where(p => p.SMSParentContractID > 0).ToList();
                        List<SMS_PARENT_CONTRACT_DETAIL> listDetailInsert = new List<SMS_PARENT_CONTRACT_DETAIL>();

                        if (listContractGroupInsert != null && listContractGroupInsert.Count > 0)
                        {
                            SMSParentContractBO ContractObjTmp = null;
                            SMS_PARENT_CONTRACT_DETAIL detailInsertObj = null;
                            SMSParentContractDetailBO detailBOTmp = null;
                            int partitionID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                            for (int i = 0; i < listContractGroupInsert.Count; i++)
                            {
                                ContractObjTmp = listContractGroupInsert[i];
                                for (int j = 0; j < ContractObjTmp.SMSParentContractDetailList.Count; j++)
                                {
                                    detailBOTmp = ContractObjTmp.SMSParentContractDetailList[j];
                                    detailInsertObj = new SMS_PARENT_CONTRACT_DETAIL();
                                    detailInsertObj.SMS_PARENT_CONTRACT_ID = ContractObjTmp.SMSParentContractID;
                                    detailInsertObj.PARTITION_ID = partitionID;
                                    detailInsertObj.SUBSCRIBER = detailBOTmp.Subscriber.ExtensionMobile();
                                    detailInsertObj.SUBSCRIBER_TYPE = detailInsertObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                                    detailInsertObj.SUBSCRIPTION_TIME = detailBOTmp.SubscriptionTime;
                                    if (allowDeferredPayment) detailInsertObj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_UNPAID;
                                    else detailInsertObj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_REGISTED;

                                    detailInsertObj.YEAR_ID = aca.Year;
                                    detailInsertObj.AMOUNT = (int)detailBOTmp.Money;
                                    detailInsertObj.IS_ACTIVE = true;
                                    detailInsertObj.CREATED_TIME = DateTime.Now;
                                    detailInsertObj.SERVICE_PACKAGE_ID = detailBOTmp.ServicePackageID.Value;
                                    detailInsertObj.RELATION_SHIP = detailBOTmp.Relationship;
                                    detailInsertObj.SCHOOL_ID = _globalInfo.SchoolID.Value;
                                    detailInsertObj.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                                    detailInsertObj.CLASS_ID = ContractObjTmp.ClassID;
                                    detailInsertObj.PUPIL_ID = ContractObjTmp.PupilFileID;
                                    listDetailInsert.Add(detailInsertObj);
                                }
                            }
                        }

                        Dictionary<string, object> dic = smsParentContractBusiness.SaveRegisContract(listContractInsert, aca.Year);
                        // Hợp đồng đã có thông tin thì chỉ thêm Detail
                        smsParentContractDetailBusiness.SaveDetail(listDetailInsert, aca.Year, _globalInfo.SchoolID.Value);
                        // Trừ tiền tài khoản Payment
                        ewalletBusiness.ChangeAmountSub(eWalletID, price.Value, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, quantity, GlobalConstantsEdu.SERVICE_TYPE_REGIS_CONTRACT);
                        // Ghi log
                        StringBuilder builder = new StringBuilder();
                        builder.Append("Import:").Append(DateTime.Now.ToString())
                            .Append("-Loại giao dịch: GD thanh toán -Nội dung: Đăng ký dịch vụ SLLĐT, số lượng: ")
                            .Append(quantity).Append("PHHS, số tiền ").Append(price).Append(" VNĐ -Dịch vụ: SLLĐT");
                        if (allowDeferredPayment) builder.Append(" - HÌNH THỨC TRẢ CHẬM");

                        //VTODO WriteLogInfo(builder.ToString(), null, string.Empty, string.Empty, null, string.Empty);

                        // Tạo tài khoản SParent
                        if (dic.ContainsKey(GlobalConstantsEdu.LIST_SPARENT) && dic[GlobalConstantsEdu.LIST_SPARENT] != null)
                            Session[SMSParentManageContractConstant.LIST_ACCOUNT_SPARENT] = (List<SParentAccountBO>)dic[GlobalConstantsEdu.LIST_SPARENT];
                        // Hủy Session sau Lưu
                        Session.Remove(SMSParentManageContractConstant.LIST_IMPORT);
                        Session.Remove(SMSParentManageContractConstant.PRICE_PAY);
                        // Dong bo isregiscontract smas3                                       
                        UpdatePupilOfClass();
                        return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), GlobalConstants.TYPE_SUCCESS));
                    }
                    catch (Exception ex)
                    {
                        //VTODO
                        //
                        string para = string.Format("eWalletID={0}, quantity={1}, totalPay={2}", eWalletID, quantity, totalPay);
                        LogExtensions.ErrorExt(logger, DateTime.Now, "SaveImport", para, ex);
                        return Json(new JsonMessage(Res.Get("SendSMS_Label_CoreExceptionError"), GlobalConstants.TYPE_ERROR));
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("SendSMS_Label_CoreExceptionError"), GlobalConstants.TYPE_ERROR));
                }
            }
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS });
        }

        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SaveImportExtra(bool saveImport, int eWalletID, decimal totalPay, int quantity)
        {
            if (!saveImport)
            {
                //huy session
                Session.Remove(SMSParentManageContractConstant.LIST_IMPORT_EXTRA);
                Session.Remove(SMSParentManageContractConstant.PRICE_PAY_EXTRA);
            }
            else
            {
                if (Session[SMSParentManageContractConstant.LIST_IMPORT_EXTRA] != null)
                {
                    try
                    {
                        List<SMS_PARENT_CONTRACT_EXTRA> listSourceExcel = (List<SMS_PARENT_CONTRACT_EXTRA>)Session[SMSParentManageContractConstant.LIST_IMPORT_EXTRA];

                        decimal? price = (decimal)Session[SMSParentManageContractConstant.PRICE_PAY_EXTRA];

                        // Trừ tiền tài khoản Payment
                        ewalletBusiness.ChangeAmountSub(eWalletID, price.Value, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, quantity, GlobalConstantsEdu.SERVICE_TYPE_BUY_EXTRA);
                        // Ghi log
                        StringBuilder builder = new StringBuilder();
                        builder.Append("Import:").Append(DateTime.Now.ToString())
                            .Append("-Loại giao dịch: GD thanh toán -Nội dung: Đăng ký dịch vụ SLLĐT, số lượng: ")
                            .Append(quantity).Append("PHHS, số tiền ").Append(price).Append(" VNĐ -Dịch vụ: SLLĐT");

                        //VTODO WriteLogInfo(builder.ToString(), null, string.Empty, string.Empty, null, string.Empty);

                        AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                        smsParentContractExtraBusiness.SaveDetail(listSourceExcel, aca.Year);

                        // Hủy Session sau Lưu
                        Session.Remove(SMSParentManageContractConstant.LIST_IMPORT_EXTRA);
                        Session.Remove(SMSParentManageContractConstant.PRICE_PAY_EXTRA);

                        return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), GlobalConstants.TYPE_SUCCESS));
                    }
                    catch (Exception ex)
                    {
                        //VTODO
                        //
                        string para = string.Format("eWalletID={0}, quantity={1}, totalPay={2}", eWalletID, quantity, totalPay);
                        LogExtensions.ErrorExt(logger, DateTime.Now, "SaveImportExtra", para, ex);
                        return Json(new JsonMessage(Res.Get("SendSMS_Label_CoreExceptionError"), GlobalConstants.TYPE_ERROR));
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("SendSMS_Label_CoreExceptionError"), GlobalConstants.TYPE_ERROR));
                }
            }
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS });
        }

        private List<SMSParentContractGroupBO> ValidateDataFromExcelExtra(IVTWorksheet sheet, List<ServicePackageBO> listService)
        {
            int schoolID = _globalInfo.SchoolID.Value;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string message = string.Empty;
            // Danh sach chi tiet hoc sinh       
            List<PupilProfileBO> listPupilProfile = PupilProfileBusiness.GetPupilsformatString(schoolID, _globalInfo.AcademicYearID.Value, 0, string.Empty, string.Empty, null, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault());
            //Danh sach hoc sinh chua co hop dong cua truong  
            List<SMSParentContractGroupBO> pupilNotContractList = smsParentContractBusiness.GetListExcelRegis(listPupilProfile, schoolID, aca.Year, 0);
            //Danh sach hoc sinh da co hop dong 
            List<SMSParentContractDetailBO> listContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(schoolID, aca.Year);

            List<SMSParentContractGroupBO> listResult = smsParentContractBusiness.GetListExcelExtra(listPupilProfile, schoolID, aca.Year, GlobalConstantsEdu.STATUS_REGISTED, string.Empty);

            //lay du lieu trong file excel
            List<SMSParentContractGroupBO> smsParentContractBOList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupOBj = null;
            List<ContractDetailList> contractDetailList = new List<ContractDetailList>();
            ContractDetailList contractDetailObj = null;
            List<SMSParentContractGroupBO> listCheckExist = new List<SMSParentContractGroupBO>();
            List<ContractDetailList> ctDetailCheckExist = new List<ContractDetailList>();
            List<ContractDetailList> listdetailTmp = new List<ContractDetailList>();
            SMSParentContractDetailBO contractObjExist = null;
            PupilProfileBO pupilProfileByPupilCode = null;

            ServicePackageBO objServicePackege = null;
            int pupilID = 0;
            int educationGrade = 0;
            string strvalue = string.Empty;
            List<int> listPupilIDExcel = new List<int>();
            StringBuilder strBuilder = null;
            bool IsMatch = false;
            string pupilCode = string.Empty;
            SMSParentContractGroupBO dataFromExcelObj = null;
            ContractDetailList contractDetailObjExcell = null;
            List<SMSParentContractGroupBO> listDatafromExcel = this.GetDataFromExcel(sheet);
            int currentSemester = this.Semester;
            int currentEducationGrade = _globalInfo.AppliedLevel.Value;

            Regex objHtml = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:""[^""]*""|'[^']*'|[^'"">\s]+))?)+\s*|\s*)/?>");

            List<int> lstPupilID = listPupilProfile.Select(p => p.PupilProfileID).Distinct().ToList();
            Dictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"YearID",_globalInfo.AcademicYearID},
                {"lstPupilID",lstPupilID},
                {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE}
            };
            List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();

            List<objServiceCode> lstServiceCode = new List<objServiceCode>();

            for (int i = 0; i < listDatafromExcel.Count; i++)
            {
                dataFromExcelObj = listDatafromExcel[i];
                pupilCode = dataFromExcelObj.PupilCode;
                contractDetailObjExcell = dataFromExcelObj.contractDetailList.FirstOrDefault();// 1 hoc sinh co 1 detail trong excel
                strBuilder = new StringBuilder();
                smsParentContractGroupOBj = new SMSParentContractGroupBO();
                #region Ma hoc sinh
                if (!string.IsNullOrEmpty(pupilCode))
                {
                    pupilProfileByPupilCode = listPupilProfile.FirstOrDefault(p => p.PupilCode == pupilCode);
                    if (pupilProfileByPupilCode != null)
                    {
                        smsParentContractGroupOBj.PupilCode = pupilCode;
                        pupilID = pupilProfileByPupilCode.PupilProfileID;
                        educationGrade = pupilProfileByPupilCode.EducationLevelID;
                        educationGrade = educationGrade.GetEducationGradeByLevelID();
                        smsParentContractGroupOBj.PupilID = pupilID;
                        smsParentContractGroupOBj.MotherMobile = pupilProfileByPupilCode.MotherMobile;
                        smsParentContractGroupOBj.MotherName = pupilProfileByPupilCode.MotherFullName;
                        smsParentContractGroupOBj.RelationshipContract = SMSParentManageContractConstant.IS_MOTHER;
                        smsParentContractGroupOBj.StatusContract = GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT;
                        smsParentContractGroupOBj.CreateTime = DateTime.Now;
                        smsParentContractGroupOBj.ClassID = pupilProfileByPupilCode.CurrentClassID;
                        smsParentContractGroupOBj.PupilOfClassID = pupilProfileByPupilCode.PupilOfClassID;
                        contractObjExist = listContract.FirstOrDefault(o => o.PupilID == pupilID);
                        if (contractObjExist != null) smsParentContractGroupOBj.SMSParentContractID = contractObjExist.SMSParentContractID;

                        IsMatch = objHtml.IsMatch(pupilCode.StripVNSignAndSpace());
                        if (IsMatch)
                        {
                            smsParentContractGroupOBj.PupilCode = pupilCode;
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_PupilCode_Html")).Append(";");
                        }

                        if (educationGrade != currentEducationGrade)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_EducationGrade_Import")).Append(";");
                        }

                        if (pupilProfileByPupilCode.ProfileStatus != GlobalConstantsEdu.COMMON_PUPIL_STATUS_STUDYING)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Valdiate_Pupil_Code_Import")).Append(";");
                        }
                        if (listResult.Where(o => o.PupilCode == pupilCode).Count() == 0)
                        {
                            smsParentContractGroupOBj.PupilCode = pupilCode;
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(string.Format(Res.Get("Mã học sinh chưa có thuê bao hoặc chỉ có thuê bao gói cước miễn phí"), strvalue)).Append(";");
                        }
                    }
                    else
                    {
                        smsParentContractGroupOBj.PupilCode = pupilCode;
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append("Mã học sinh không tồn tại").Append(";");
                    }


                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    strBuilder.Append(Res.Get("PupilCode_Empty")).Append(";");
                }
                #endregion

                #region ho va ten hoc sinh
                strvalue = dataFromExcelObj.PupilName;
                smsParentContractGroupOBj.PupilName = strvalue;

                #endregion

                #region Lop hoc
                strvalue = dataFromExcelObj.ClassName;
                smsParentContractGroupOBj.ClassName = strvalue;

                #endregion

                contractDetailObj = new ContractDetailList();
                contractDetailList = new List<ContractDetailList>();

                #region Quan he voi hoc sinh
                strvalue = contractDetailObjExcell.RelationShip;
                contractDetailObj.RelationShip = strvalue;

                #endregion

                //// add pupilID vao list de kiem tra cho vong lap sau
                //// add chi tiet vao detail
                ////chi tiet hop dong

                // So dien thoai nhan tin
                string phoneReceiver = contractDetailObjExcell.PhoneReceiver;
                if (!string.IsNullOrEmpty(phoneReceiver))
                {
                    phoneReceiver = phoneReceiver.Trim();
                    if (CheckPhoneNumber(phoneReceiver))
                    {
                        contractDetailObj.PhoneReceiver = phoneReceiver.ExtensionMobile();
                        IsMatch = objHtml.IsMatch(phoneReceiver.StripVNSignAndSpace());
                        if (IsMatch)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_Phone_Receiver_Html")).Append(";");
                        }

                        if (listResult.Where(o => o.PupilCode == pupilCode).Count() > 0)
                        {
                            if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver).Count() == 0)
                            {
                                contractDetailObj.PhoneReceiver = phoneReceiver;
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Số điện thoại chưa được đăng ký gói cước hoặc đăng ký gói cước miễn phí")).Append(";");
                            }
                        }
                    }
                    else
                    {
                        contractDetailObj.PhoneReceiver = phoneReceiver;
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("blb_Message_Phone_Invalid")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_Phone_Receiver")).Append(";");
                }


                #region Kiem tra Goi cuoc
                strvalue = contractDetailObjExcell.ServiceCode;

                if (!string.IsNullOrEmpty(strvalue))
                {
                    objServicePackege = listService.Where(p => p.ServiceCode == strvalue).FirstOrDefault();
                    if (objServicePackege != null)
                    {

                        contractDetailObj.ServicesPakageID = objServicePackege.ServicePackageID;
                        contractDetailObj.ServiceCode = objServicePackege.ServiceCode;
                        // 20151119 - AnhVD9 - Kiem tra goi cuoc chi cho phep dau noi mang
                        if (objServicePackege.IsForOnlyInternal == true && !contractDetailObj.PhoneReceiver.CheckMobileNumberVT())
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlyInternalNetwork")).Append(";");
                        }

                        if (objServicePackege.IsForOnlyExternal == true && contractDetailObj.PhoneReceiver.CheckMobileNumberVT())
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlyExternalNetwork")).Append(";");
                        }
                    }
                    else
                    {
                        contractDetailObj.ServiceCode = strvalue;
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("Validate_ServicesPackage_InValid")).Append(";");
                    }

                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        contractDetailObj.ServiceCode = strvalue;
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_ServicePackage_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_ServicePackage_Empty")).Append(";");
                }
                #endregion

                //Thoi han            
                int semester = 0;
                strvalue = contractDetailObjExcell.subscriptionTimeExcel;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    if (SMSParentManageContractConstant.STR_SEMESTER_ALL.Equals(strvalue))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null &&
                        //        (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        //        || !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND)))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219

                        contractDetailObj.AllSemester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;

                        if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver).Count() > 0)
                        {
                            if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver && o.SubciptionTime == semester).Count() == 0)
                            {
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                            }
                        }
                    }
                    else if (SMSParentManageContractConstant.HKI.Equals(strvalue))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null && !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219

                        contractDetailObj.FirstSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;

                        if (this.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                        }

                        if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver).Count() > 0)
                        {
                            if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver && o.SubciptionTime == semester).Count() == 0)
                            {
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                            }

                        }
                    }
                    else if (SMSParentManageContractConstant.HKII.Equals(strvalue))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null && !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219
                        contractDetailObj.SecondSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;

                        if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver).Count() > 0)
                        {
                            if (listResult.Where(o => o.PupilCode == pupilCode && o.Subscriber == phoneReceiver && o.SubciptionTime == semester).Count() == 0)
                            {
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                            }
                        }
                    }
                    else
                    {
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                    }

                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_SubsciptionTime_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_Subscription_Time_Empty")).Append(";");
                }

                // Neu den hoc ky 2 nhung chon hoc ky 1 de dang ky thi khong hop le
                if (currentSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_SubsciptionTime_By_Current_Semester")).Append(";");
                }


                contractObjExist = listContract.FirstOrDefault(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone());
                //add vao list detail neu co hoc sinh trong hop dong
                if (contractObjExist != null)
                {
                    smsParentContractGroupOBj.SMSParentContractID = contractObjExist.SMSParentContractID;
                }

                contractDetailObj.MessageDetail = strBuilder.ToString();

                contractDetailList.Add(contractDetailObj);
                smsParentContractGroupOBj.contractDetailList = contractDetailList;
                smsParentContractBOList.Add(smsParentContractGroupOBj);
            }

            return smsParentContractBOList;
        }

        /// <summary>
        /// Kiem tra du lieu import
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="listService"></param>
        /// <returns></returns>
        private List<SMSParentContractGroupBO> ValidateDataFromExcel(IVTWorksheet sheet, List<ServicePackageBO> listService)
        {
            int schoolID = _globalInfo.SchoolID.Value;
            string message = string.Empty;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            // Danh sach chi tiet hoc sinh       
            List<PupilProfileBO> listPupilProfile = PupilProfileBusiness.GetPupilsformatString(schoolID, _globalInfo.AcademicYearID.Value, 0, string.Empty, string.Empty, null, _globalInfo.AppliedLevel.Value, _globalInfo.EmployeeID.GetValueOrDefault());
            //Danh sach hoc sinh chua co hop dong cua truong  
            List<SMSParentContractGroupBO> pupilNotContractList = smsParentContractBusiness.GetListExcelRegis(listPupilProfile, schoolID, aca.Year, 0);
            //Danh sach hoc sinh da co hop dong 
            List<SMSParentContractDetailBO> listContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(schoolID, aca.Year);

            //lay du lieu trong file excel
            List<SMSParentContractGroupBO> smsParentContractBOList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupOBj = null;
            List<ContractDetailList> contractDetailList = new List<ContractDetailList>();
            ContractDetailList contractDetailObj = null;
            List<SMSParentContractGroupBO> listCheckExist = new List<SMSParentContractGroupBO>();
            List<ContractDetailList> ctDetailCheckExist = new List<ContractDetailList>();
            List<ContractDetailList> listdetailTmp = new List<ContractDetailList>();
            SMSParentContractDetailBO contractObjExist = null;
            PupilProfileBO pupilProfileByPupilCode = null;

            ServicePackageBO objServicePackege = null;
            int? semester1Exist = 0;
            int? semester2Exist = 0;
            int? semesterAllExist = 0;
            int pupilID = 0;
            int educationGrade = 0;
            string strvalue = string.Empty;
            List<int> listPupilIDExcel = new List<int>();
            StringBuilder strBuilder = null;
            bool IsMatch = false;
            string pupilCode = string.Empty;
            SMSParentContractGroupBO dataFromExcelObj = null;
            ContractDetailList contractDetailObjExcell = null;
            List<SMSParentContractGroupBO> listDatafromExcel = this.GetDataFromExcel(sheet);
            int currentSemester = this.Semester;
            int currentEducationGrade = _globalInfo.AppliedLevel.Value;

            bool isRegistered2Sem = false;
            bool isRegisteredSameSem = false;
            Regex objHtml = new Regex(@"</?\w+((\s+\w+(\s*=\s*(?:""[^""]*""|'[^']*'|[^'"">\s]+))?)+\s*|\s*)/?>");

            List<int> lstPupilID = listPupilProfile.Select(p => p.PupilProfileID).Distinct().ToList();
            Dictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID.Value},
                {"YearID",_globalInfo.AcademicYearID.Value},
                {"lstPupilID",lstPupilID},
                {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE}
            };
            List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();

            List<objServiceCode> lstServiceCode = new List<objServiceCode>();

            for (int i = 0; i < listDatafromExcel.Count; i++)
            {
                dataFromExcelObj = listDatafromExcel[i];
                pupilCode = dataFromExcelObj.PupilCode;
                contractDetailObjExcell = dataFromExcelObj.contractDetailList.FirstOrDefault();// 1 hoc sinh co 1 detail trong excel
                strBuilder = new StringBuilder();
                smsParentContractGroupOBj = new SMSParentContractGroupBO();
                #region Ma hoc sinh
                if (!string.IsNullOrEmpty(pupilCode))
                {
                    pupilProfileByPupilCode = listPupilProfile.FirstOrDefault(p => p.PupilCode == pupilCode);
                    if (pupilProfileByPupilCode != null)
                    {
                        smsParentContractGroupOBj.PupilCode = pupilCode;
                        pupilID = pupilProfileByPupilCode.PupilProfileID;
                        educationGrade = pupilProfileByPupilCode.EducationLevelID;
                        educationGrade = educationGrade.GetEducationGradeByLevelID();
                        smsParentContractGroupOBj.PupilID = pupilID;
                        smsParentContractGroupOBj.MotherMobile = pupilProfileByPupilCode.MotherMobile;
                        smsParentContractGroupOBj.MotherName = pupilProfileByPupilCode.MotherFullName;
                        smsParentContractGroupOBj.RelationshipContract = SMSParentManageContractConstant.IS_MOTHER;
                        smsParentContractGroupOBj.StatusContract = GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT;
                        smsParentContractGroupOBj.CreateTime = DateTime.Now;
                        smsParentContractGroupOBj.ClassID = pupilProfileByPupilCode.CurrentClassID;
                        smsParentContractGroupOBj.PupilOfClassID = pupilProfileByPupilCode.PupilOfClassID;
                        contractObjExist = listContract.FirstOrDefault(o => o.PupilID == pupilID);
                        if (contractObjExist != null) smsParentContractGroupOBj.SMSParentContractID = contractObjExist.SMSParentContractID;

                        IsMatch = objHtml.IsMatch(pupilCode.StripVNSignAndSpace());
                        if (IsMatch)
                        {
                            smsParentContractGroupOBj.PupilCode = pupilCode;
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_PupilCode_Html")).Append(";");
                        }

                        if (educationGrade != currentEducationGrade)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_EducationGrade_Import")).Append(";");
                        }

                        if (pupilProfileByPupilCode.ProfileStatus != GlobalConstantsEdu.COMMON_PUPIL_STATUS_STUDYING)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Valdiate_Pupil_Code_Import")).Append(";");
                        }
                    }
                    else
                    {
                        smsParentContractGroupOBj.PupilCode = pupilCode;
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(string.Format(Res.Get("Validate_Pupil_Code"), strvalue)).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    strBuilder.Append(Res.Get("PupilCode_Empty")).Append(";");
                }
                #endregion

                #region ho va ten hoc sinh
                strvalue = dataFromExcelObj.PupilName;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    smsParentContractGroupOBj.PupilName = strvalue;
                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_PupilName_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    strBuilder.Append(Res.Get("Validate_Pupil_Name_Import")).Append(";");
                }
                #endregion

                #region Lop hoc
                strvalue = dataFromExcelObj.ClassName;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    smsParentContractGroupOBj.ClassName = strvalue;
                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_ClassName_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    strBuilder.Append(Res.Get("Validate_Class_Name_Empty")).Append(";");
                }
                #endregion

                //// add pupilID vao list de kiem tra cho vong lap sau
                //// add chi tiet vao detail
                ////chi tiet hop dong
                contractDetailObj = new ContractDetailList();
                contractDetailList = new List<ContractDetailList>();

                #region Quan he voi hoc sinh
                strvalue = contractDetailObjExcell.RelationShip;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    contractDetailObj.RelationShip = strvalue;
                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_RelationShip_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_RelatioShip_Import")).Append(";");
                }
                #endregion

                ////Ten nguoi nhan tin //khong can kiem tra chi validate ki tu html
                strvalue = contractDetailObjExcell.ReceiverName;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    contractDetailObj.ReceiverName = strvalue;
                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_ReceiverName_Html")).Append(";");
                    }
                }


                // So dien thoai nhan tin
                string phoneReceiver = contractDetailObjExcell.PhoneReceiver;
                if (!string.IsNullOrEmpty(phoneReceiver))
                {
                    phoneReceiver = phoneReceiver.Trim();
                    if (CheckPhoneNumber(phoneReceiver))
                    {
                        contractDetailObj.PhoneReceiver = phoneReceiver.ExtensionMobile();
                        IsMatch = objHtml.IsMatch(phoneReceiver.StripVNSignAndSpace());
                        if (IsMatch)
                        {
                            smsParentContractGroupOBj.Error = true;
                            strBuilder.Append(Res.Get("Validate_Phone_Receiver_Html")).Append(";");
                        }
                    }
                    else
                    {
                        contractDetailObj.PhoneReceiver = phoneReceiver;
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("blb_Message_Phone_Invalid")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_Phone_Receiver")).Append(";");
                }


                #region Kiem tra Goi cuoc
                strvalue = contractDetailObjExcell.ServiceCode;

                if (!string.IsNullOrEmpty(strvalue))
                {
                    objServicePackege = listService.Where(p => p.ServiceCode == strvalue).FirstOrDefault();
                    if (objServicePackege != null)
                    {
                        contractDetailObj.ServiceCode = objServicePackege.ServiceCode;

                        if (objServicePackege.ServiceCode == "SMSFree")
                        {
                            if (!lstPupilRegisterSMSFreeID.Contains(pupilID))
                            {
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Validate_ServicesPackage_NotRegisterSMSFree")).Append(";");
                            }
                            else
                            {
                                int countDouble = 0;
                                if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.STR_SEMESTER_ALL))
                                {
                                    countDouble = listContract.Where(p => p.PupilID == pupilID && "SMSFree".Equals(p.ServiceCode)).Count();
                                }
                                else if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.HKI))
                                {
                                    countDouble = listContract.Where(p => p.PupilID == pupilID && "SMSFree".Equals(p.ServiceCode)).Count();
                                }
                                else if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.HKII))
                                {
                                    countDouble = listContract.Where(p => p.PupilID == pupilID && "SMSFree".Equals(p.ServiceCode)).Count();
                                }

                                //int countDouble = listContract.Where(p => p.PupilID == pupilID && "SMSFree".Equals(p.ServiceCode)).Count();
                                objServiceCode objSC = new objServiceCode();
                                objSC.PupilID = pupilID;
                                objSC.ServiceCode = objServicePackege.ServiceCode;
                                lstServiceCode.Add(objSC);
                                if (lstServiceCode.Where(p => p.PupilID == pupilID && p.ServiceCode.Equals("SMSFree")).Count() > 1 || countDouble > 0)
                                {
                                    smsParentContractGroupOBj.Error = true;
                                    contractDetailObj.ErrorDetail = true;
                                    strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlySMSFree")).Append(";");
                                }
                            }
                        }

                        //viethd31: kiem tra voi goi 0 dong
                        else if ((!objServicePackege.ViettelPrice.HasValue || objServicePackege.ViettelPrice == 0) && (!objServicePackege.OtherPrice.HasValue || objServicePackege.OtherPrice == 0))
                        {

                            int countDouble = 0;
                            if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.STR_SEMESTER_ALL))
                            {
                                countDouble = listContract.Where(p => p.PupilID == pupilID && ((!objServicePackege.ViettelPrice.HasValue || objServicePackege.ViettelPrice == 0) && (!objServicePackege.OtherPrice.HasValue || objServicePackege.OtherPrice == 0))).Count();
                            }
                            else if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.HKI))
                            {
                                countDouble = listContract.Where(p => p.PupilID == pupilID && ((!objServicePackege.ViettelPrice.HasValue || objServicePackege.ViettelPrice == 0) && (!objServicePackege.OtherPrice.HasValue || objServicePackege.OtherPrice == 0))).Count();
                            }
                            else if (contractDetailObjExcell.subscriptionTimeExcel.Equals(SMSParentManageContractConstant.HKII))
                            {
                                countDouble = listContract.Where(p => p.PupilID == pupilID && ((!objServicePackege.ViettelPrice.HasValue || objServicePackege.ViettelPrice == 0) && (!objServicePackege.OtherPrice.HasValue || objServicePackege.OtherPrice == 0))).Count();
                            }

                            //int countDouble = listContract.Where(p => p.PupilID == pupilID && "SMSFree".Equals(p.ServiceCode)).Count();
                            objServiceCode objSC = new objServiceCode();
                            objSC.PupilID = pupilID;
                            objSC.ServiceCode = objServicePackege.ServiceCode;
                            lstServiceCode.Add(objSC);
                            if (lstServiceCode.Where(p => p.PupilID == pupilID &&
                                ((!p.VIETTEL_PRICE.HasValue || p.VIETTEL_PRICE == 0) && (!p.OTHER_PRICE.HasValue || p.OTHER_PRICE == 0))).Count() > 1 || countDouble > 0)
                            {
                                smsParentContractGroupOBj.Error = true;
                                contractDetailObj.ErrorDetail = true;
                                strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlyOneFree")).Append(";");
                            }

                        }

                        contractDetailObj.ServicesPakageID = objServicePackege.ServicePackageID;

                        // 20151119 - AnhVD9 - Kiem tra goi cuoc chi cho phep dau noi mang
                        if (objServicePackege.IsForOnlyInternal == true && !contractDetailObj.PhoneReceiver.CheckMobileNumberVT())
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlyInternalNetwork")).Append(";");
                        }

                        if (objServicePackege.IsForOnlyExternal == true && contractDetailObj.PhoneReceiver.CheckMobileNumberVT())
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_ServicesPackage_OnlyExternalNetwork")).Append(";");
                        }

                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (pupilNotContractList.Count(o => o.PupilID == pupilID) > 0)
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //    else
                        //    {
                        //        List<int> lstCurrentSPID = listContract.Where(o => o.PupilID == pupilID).Select(o => o.ServicePackageID.Value).ToList();
                        //        if (!listService.Any(o => lstCurrentSPID.Contains(o.ServicePackageID) && o.VIETTEL_PRICE > 0 && o.OTHER_PRICE > 0))
                        //        {
                        //            smsParentContractGroupOBj.Error = true;
                        //            contractDetailObj.ErrorDetail = true;
                        //            strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        contractDetailObj.ServiceCode = strvalue;
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("Validate_ServicesPackage_InValid")).Append(";");
                    }

                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        contractDetailObj.ServiceCode = strvalue;
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_ServicePackage_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_ServicePackage_Empty")).Append(";");
                }
                #endregion

                //Thoi han            
                int semester = 0;
                strvalue = contractDetailObjExcell.subscriptionTimeExcel;
                if (!string.IsNullOrEmpty(strvalue))
                {
                    if (SMSParentManageContractConstant.STR_SEMESTER_ALL.Equals(strvalue) || (objServicePackege != null && objServicePackege.IsWholeYearPackage == true))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null &&
                        //        (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        //        || !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND)))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219

                        contractDetailObj.AllSemester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                        if (objServicePackege != null && objServicePackege.ServicePackageID > 0 && !string.IsNullOrEmpty(phoneReceiver))
                        {
                            contractDetailObj.Money = (this.GetMoneyByServiceID(objServicePackege.ServicePackageID, phoneReceiver, listService) * 2);
                        }
                    }
                    else if (SMSParentManageContractConstant.HKI.Equals(strvalue))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null && !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219

                        contractDetailObj.FirstSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        if (objServicePackege != null && objServicePackege.ServicePackageID > 0 && !string.IsNullOrEmpty(phoneReceiver))
                        {
                            contractDetailObj.Money = this.GetMoneyByServiceID(objServicePackege.ServicePackageID, phoneReceiver, listService);
                        }
                    }
                    else if (SMSParentManageContractConstant.HKII.Equals(strvalue))
                    {
                        // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                        //if (objServicePackege.IsAllowRegisterSideSubscriber == true)
                        //{
                        //    if (contractObjExist != null && !smsParentContractDetailBusiness.CheckExistFirstSubscriber(contractObjExist.SMSParentContractID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                        //    {
                        //        smsParentContractGroupOBj.Error = true;
                        //        contractDetailObj.ErrorDetail = true;
                        //        strBuilder.Append("Thuê bao chưa đủ điều kiện áp dụng cho gói cước này").Append(";");
                        //    }
                        //}
                        // End 20160219
                        contractDetailObj.SecondSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        if (objServicePackege != null && objServicePackege.ServicePackageID > 0 && !string.IsNullOrEmpty(phoneReceiver))
                        {
                            contractDetailObj.Money = this.GetMoneyByServiceID(objServicePackege.ServicePackageID, phoneReceiver, listService);
                        }
                        else
                        {
                            contractDetailObj.Money = 0;
                        }
                    }
                    else
                    {
                        contractDetailObj.InputSemester = strvalue;
                        smsParentContractGroupOBj.Error = true;
                        contractDetailObj.ErrorDetail = true;
                        strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                    }

                    IsMatch = objHtml.IsMatch(strvalue.StripVNSignAndSpace());
                    if (IsMatch)
                    {
                        smsParentContractGroupOBj.Error = true;
                        strBuilder.Append(Res.Get("Validate_SubsciptionTime_Html")).Append(";");
                    }
                }
                else
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_Subscription_Time_Empty")).Append(";");
                }

                // Neu den hoc ky 2 nhung chon hoc ky 1 de dang ky thi khong hop le
                if (currentSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Validate_SubsciptionTime_By_Current_Semester")).Append(";");
                }

                //kiem tra 1 hoc sinh khong trung thue bao theo thoi han trong excel
                listCheckExist = smsParentContractBOList.Where(p => p.PupilID == pupilID).ToList();//list danh sach hoc sinh co nhieu detail
                if (listCheckExist != null && listCheckExist.Count > 0)
                {
                    for (int j = 0; j < listCheckExist.Count; j++)
                    {
                        listdetailTmp = listCheckExist[j].contractDetailList.Where(p => p.PhoneReceiver == phoneReceiver && p.IsLimitPackage != true).ToList();
                        semester1Exist = listdetailTmp.Where(p => p.FirstSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.FirstSemester).FirstOrDefault();
                        semester2Exist = listdetailTmp.Where(p => p.SecondSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.SecondSemester).FirstOrDefault();
                        semesterAllExist = listdetailTmp.Where(p => p.AllSemester == GlobalConstants.SEMESTER_OF_YEAR_ALL).Select(p => p.AllSemester).FirstOrDefault();
                        if (semester == semester1Exist || semester == semester2Exist || semester == semesterAllExist)
                        {
                            smsParentContractGroupOBj.Error = true;
                            contractDetailObj.ErrorDetail = true;
                            strBuilder.Append(Res.Get("Validate_Subscription_Time_InValid")).Append(";");
                        }
                    }
                }

                // AnhVD9 - Bổ sung 20151012 - Kiem tra thue bao dang ky khong trung voi thue bao da dang ky voi cung hoc sinh 
                // Kiem tra HS da dag ky Ca nam
                isRegistered2Sem = listContract.Any(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone() && p.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL && p.SubscriptionStatus != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL && p.IsLimit != true)
                                        || (listContract.Any(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone() && p.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_FIRST && p.SubscriptionStatus != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL && p.IsLimit != true)
                                            && listContract.Any(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone() && p.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_SECOND && p.SubscriptionStatus != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL && p.IsLimit != true));
                isRegisteredSameSem = listContract.Any(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone() && p.SubscriptionStatus != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL && p.IsLimit != true &&
                      ((p.SubscriptionTime == semester && semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST) || (p.SubscriptionTime == semester && semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)));

                if (isRegistered2Sem || isRegisteredSameSem)
                {
                    smsParentContractGroupOBj.Error = true;
                    contractDetailObj.ErrorDetail = true;
                    strBuilder.Append(Res.Get("Valdiate_Import_Dulicate_Subscriber")).Append(";");
                }

                contractObjExist = listContract.FirstOrDefault(p => p.PupilID == pupilID && p.Subscriber.SubstringPhone() == phoneReceiver.SubstringPhone() && p.SubscriptionTime != semester && p.IsLimit != true);
                //add vao list detail neu co hoc sinh trong hop dong
                if (contractObjExist != null)
                {
                    smsParentContractGroupOBj.SMSParentContractID = contractObjExist.SMSParentContractID;
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        if (contractObjExist.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            contractDetailObj.AllSemester = null;
                            contractDetailObj.SecondSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                            contractDetailObj.Money = contractDetailObj.Money / 2;
                        }
                        else if (contractObjExist.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            contractDetailObj.AllSemester = null;
                            contractDetailObj.FirstSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                            contractDetailObj.Money = contractDetailObj.Money / 2;
                        }
                    }
                }

                contractDetailObj.MessageDetail = strBuilder.ToString();

                contractDetailList.Add(contractDetailObj);
                smsParentContractGroupOBj.contractDetailList = contractDetailList;
                smsParentContractBOList.Add(smsParentContractGroupOBj);
            }

            return smsParentContractBOList;
        }

        private List<SMSParentContractGroupBO> GetDataFromExcel(IVTWorksheet sheet)
        {
            int startRow = 10;// thue bao chua dang ky         
            //lay du lieu trong file excel
            List<SMSParentContractGroupBO> smsParentContractBOList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupOBj = null;
            List<ContractDetailList> contractDetailList = new List<ContractDetailList>();
            ContractDetailList contractDetailObj = null;
            string strvalue = string.Empty;
            object locationCell;
            string strEmpty = string.Empty;
            while (sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PUPIL_RELATIONSHIP) != null
                || sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_SERVICE_PACKAGE) != null
                || sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_SUBSCRIPTION_TIME) != null
                || sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PUPIL_CODE) != null
                 || sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PHONE_RECEIVER) != null
             )
            {
                try
                {
                    smsParentContractGroupOBj = new SMSParentContractGroupBO();
                    //Ma hoc sinh
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PUPIL_CODE);
                    smsParentContractGroupOBj.PupilCode = locationCell != null ? locationCell.ToString() : strEmpty; ;

                    #region Add vao list hop dong
                    //ho ten hoc sinh
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PUPIL_NAME);
                    smsParentContractGroupOBj.PupilName = locationCell != null ? locationCell.ToString() : strEmpty;

                    // Lop hoc
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_CLASS_NAME);
                    smsParentContractGroupOBj.ClassName = locationCell != null ? locationCell.ToString() : strEmpty;

                    // add chi tiet vao detail
                    //chi tiet hop dong
                    contractDetailObj = new ContractDetailList();
                    contractDetailList = new List<ContractDetailList>();

                    //Quan he voi ho sinh
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PUPIL_RELATIONSHIP);
                    contractDetailObj.RelationShip = locationCell != null ? locationCell.ToString() : strEmpty;

                    //Ten nguoi nhan tin
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_RECEIVER_NAME);
                    contractDetailObj.ReceiverName = locationCell != null ? locationCell.ToString() : strEmpty;

                    //So dien thoai nhan tin
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_PHONE_RECEIVER);
                    contractDetailObj.PhoneReceiver = locationCell != null ? locationCell.ToString() : strEmpty;

                    //Chon goi cuoc
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_SERVICE_PACKAGE);
                    contractDetailObj.ServiceCode = locationCell != null ? locationCell.ToString() : strEmpty;

                    //Thoi han         
                    locationCell = sheet.GetCellValue(startRow, SMSParentManageContractConstant.COLUMN_SUBSCRIPTION_TIME);
                    contractDetailObj.subscriptionTimeExcel = locationCell != null ? locationCell.ToString() : strEmpty;

                    #endregion

                    contractDetailList.Add(contractDetailObj);
                    smsParentContractGroupOBj.contractDetailList = contractDetailList;
                    smsParentContractBOList.Add(smsParentContractGroupOBj);
                    startRow += 1;
                }
                catch (Exception ex)
                {
                    //VTODO
                    //
                    string para = "";
                    LogExtensions.ErrorExt(logger, DateTime.Now, "GetDataFromExcel", para, ex);
                    return new List<SMSParentContractGroupBO>();
                }
            }

            return smsParentContractBOList;
        }

        [ValidateAntiForgeryToken]
        public JsonResult CreateAccountSparent()
        {
            //VTODO 
            DateTime eventDate = DateTime.Now;
            List<SParentAccountBO> listAccountSparent = new List<SParentAccountBO>();
            if (Session[SMSParentManageContractConstant.LIST_ACCOUNT_SPARENT] != null)
            {
                listAccountSparent = (List<SParentAccountBO>)Session[SMSParentManageContractConstant.LIST_ACCOUNT_SPARENT];
                if (listAccountSparent != null && listAccountSparent.Count > 0)
                {
                    this.smsParentContractBusiness.CreateListSParentAccount(listAccountSparent);
                    //LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, eventDate, "Null", "Tao tai khoan Sparent: " + listAccountSparent.Count, GlobalInfo.UserName);
                }
            }
            Session.Remove(SMSParentManageContractConstant.LIST_ACCOUNT_SPARENT);
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS });
        }
        #endregion

        #region Xu ly nghiep vu
        public JsonResult GetMoneyAndTotalSMS(int servicespackageID, string phoneNumber)
        {
            //kiem tra so dien thoai co hop le khong, chi cho so di dong
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                MoneyMaxSMSBO obj = servicePackageBusiness.GetSMSByServicesPackageID(servicespackageID, aca.Year, this.Semester, aca);
                if (phoneNumber.CheckMobileNumber())
                {
                    if (phoneNumber.CheckMobileNumberVT())
                    {
                        return Json(new { message = GlobalConstants.TYPE_SUCCESS, MaxSMS = obj.MaxSMS, Money = obj.MoneyViettel.Value.ToString("#,##0") });
                    }
                    else
                    {
                        return Json(new { message = GlobalConstants.TYPE_SUCCESS, MaxSMS = obj.MaxSMS, Money = obj.MoneyOther.Value.ToString("#,##0") });
                    }
                }
                else
                {
                    return Json(new { message = Res.Get("blb_Message_Phone_Invalid"), MaxSMS = 0, Money = 0 });
                }
            }
            else
            {
                return Json(new { message = Res.Get("blb_Message_Phone_Invalid"), MaxSMS = 0, Money = 0 });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(FormCollection frm)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string[] arrpupilID = frm["pupilID"].Split(GlobalConstantsEdu.charCOMMA);
            int pupilID = 0;
            int servicePackageID = 0;
            SMSParentContractBO smsParentContractBO = null;
            List<SMSParentContractBO> smsParentContractBOList = new List<SMSParentContractBO>();
            //Lay thong tin goi cuoc duoc ap dung cho truong
            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(aca.Year, this.Semester, aca, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            ServicePackageBO servicePackage = null;
            //Lay thong tin so du tai khoan truong
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);

            //Danh sach hoc sinh da co hop dong 
            List<SMSParentContractDetailBO> listContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(_globalInfo.SchoolID.Value, aca.Year);

            List<string> lsttmp = arrpupilID.ToList();
            List<int> lstPupilID = lsttmp.Select(p => int.Parse(p)).ToList();
            Dictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"YearID",_globalInfo.AcademicYearID},
                {"lstPupilID",lstPupilID},
                {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE}
            };
            List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();

            decimal Balance = ewalletObj.Balance;
            decimal? totalPay = 0;
            decimal moneyContract = 0;
            int countVT = 0;
            int countOther = 0;
            int quantityPhone = 0;
            int pupilNumber = 0;
            int countdouble = 0;
            for (int i = 0; i < arrpupilID.Length; i++)
            {
                SMSParentContractDetailBO smsParentContractDetailObj = null;
                pupilID = Convert.ToInt32(arrpupilID[i]);
                servicePackageID = 0;
                string regisChecked = frm["Register_" + pupilID];
                string phone = string.Empty;
                string contractorName = string.Empty;
                string contractorRelationship = string.Empty;
                string pupilName = string.Empty;
                string servicesPackageID = frm["ServicesPackage-" + pupilID];
                string classIDFrm = frm["ClassID-" + pupilID];
                int classID = 0;
                int pupilOfClassID = 0;
                if (!string.IsNullOrWhiteSpace(classIDFrm))
                {
                    classID = Convert.ToInt32(classIDFrm);
                }

                if (!string.IsNullOrWhiteSpace(servicesPackageID))
                {
                    servicePackageID = Convert.ToInt32(servicesPackageID);
                }

                if ("on".Equals(regisChecked))
                {
                    pupilNumber++;
                    phone = frm["PhoneReceiver-" + pupilID];
                    // 20151119 - AnhVD9 - Kiem tra goi cuoc chi duoc dau noi mang
                    servicePackage = listService.FirstOrDefault(o => o.ServicePackageID == servicePackageID);
                    if (servicePackage != null && servicePackage.IsForOnlyInternal == true && !phone.CheckMobileNumberVT()) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Tồn tại gói cước chỉ cho phép đăng ký nội mạng nhưng thuê bao là ngoại mạng" });

                    if (servicePackage != null && servicePackage.IsForOnlyExternal == true && phone.CheckMobileNumberVT()) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Tồn tại gói cước chỉ cho phép đăng ký ngoại mạng nhưng thuê bao là nội mạng" });

                    if (servicePackage.ServiceCode == "SMSFree")
                    {
                        countdouble = listContract.Where(p => p.PupilID == pupilID && p.ServiceCode.Equals("SMSFree")).Count();
                        if (!lstPupilRegisterSMSFreeID.Contains(pupilID))
                        {
                            return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_CreateNotRegisterSMSFree") });
                        }
                        if (countdouble > 0)
                        {
                            return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlySMSFree") });
                        }

                    }

                    if ((!servicePackage.ViettelPrice.HasValue || servicePackage.ViettelPrice == 0) && (!servicePackage.OtherPrice.HasValue || servicePackage.OtherPrice == 0))
                    {
                        countdouble = listContract.Where(p => p.PupilID == pupilID &&
                            ((!servicePackage.ViettelPrice.HasValue || servicePackage.ViettelPrice == 0) && (!servicePackage.OtherPrice.HasValue || servicePackage.OtherPrice == 0))).Count();
                        if (countdouble > 0)
                        {
                            return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyOneFree") });
                        }
                    }

                    contractorName = frm["ContractorName-" + pupilID];
                    contractorRelationship = frm["Relationship-" + pupilID];
                    pupilName = frm["pupilName-" + pupilID];
                    if (frm["PupilOfClassID-" + pupilID] != null) Int32.TryParse(frm["PupilOfClassID-" + pupilID].ToString(), out pupilOfClassID);
                    //hop dong
                    smsParentContractBO = new SMSParentContractBO();
                    smsParentContractBO.SchoolID = _globalInfo.SchoolID.Value;
                    smsParentContractBO.PhoneMainContract = phone;
                    smsParentContractBO.ContractName = contractorName;
                    smsParentContractBO.RelationShip = contractorRelationship;
                    smsParentContractBO.PupilFileID = pupilID;
                    smsParentContractBO.StatusContract = GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT;
                    smsParentContractBO.CreateTime = DateTime.Now;
                    smsParentContractBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    smsParentContractBO.ClassID = classID;
                    smsParentContractBO.FullName = pupilName;
                    smsParentContractBO.PupilOfClassID = pupilOfClassID;
                    //chi tiet hop dong
                    //kiem tra thoi gian hop dong
                    string semeseter1 = frm["Semester1-" + pupilID];
                    string semeseter2 = frm["Semester2-" + pupilID];
                    string semeseter3 = frm["Semester3-" + pupilID];
                    smsParentContractBO.SMSParentContractDetailList = new List<SMSParentContractDetailBO>();

                    //neu dang ky ca 2 hoc ki
                    if ("on".Equals(semeseter3) || servicePackage.IsWholeYearPackage == true)
                    {
                        //if (servicePackage.ServiceCode.Equals("SMSFree"))
                        //{
                        //    countdouble = listContract.Where(p => p.PupilID == pupilID && p.ServiceCode.Equals("SMSFree") && (p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND)).Count();
                        //}

                        smsParentContractDetailObj = new SMSParentContractDetailBO();
                        smsParentContractDetailObj.Subscriber = phone.ExtensionMobile();
                        smsParentContractDetailObj.SubscriberType = smsParentContractDetailObj.Subscriber.CheckMobileNumberVT() ? (short)0 : (short)1;
                        smsParentContractDetailObj.SubscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
                        smsParentContractDetailObj.Year = aca.Year;
                        smsParentContractDetailObj.IsActive = true;
                        smsParentContractDetailObj.CreatedTime = DateTime.Now;
                        smsParentContractDetailObj.ServicePackageID = servicePackageID;
                        smsParentContractDetailObj.SubscriptionTime = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                        smsParentContractDetailObj.Relationship = contractorRelationship;
                        smsParentContractBO.SMSParentContractDetailList.Add(smsParentContractDetailObj);
                        moneyContract = this.GetMoneyByServiceID(Int32.Parse(servicesPackageID), phone, listService);
                        smsParentContractDetailObj.Money = moneyContract * 2;
                        totalPay += moneyContract * 2;
                    }
                    else if ("on".Equals(semeseter2) || "on".Equals(semeseter1)) // Neu chi dk hoc ky 1 hoac hoc ky 2
                    {
                        smsParentContractDetailObj = new SMSParentContractDetailBO();
                        smsParentContractDetailObj.Subscriber = phone.ExtensionMobile();
                        smsParentContractDetailObj.SubscriberType = smsParentContractDetailObj.Subscriber.CheckMobileNumberVT() ? (short)0 : (short)1;
                        smsParentContractDetailObj.SubscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
                        smsParentContractDetailObj.Year = aca.Year;
                        smsParentContractDetailObj.IsActive = true;
                        smsParentContractDetailObj.CreatedTime = DateTime.Now;
                        smsParentContractDetailObj.ServicePackageID = servicePackageID;
                        smsParentContractDetailObj.SubscriptionTime = "on".Equals(semeseter2) ? GlobalConstants.SEMESTER_OF_YEAR_SECOND : GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        smsParentContractDetailObj.Relationship = contractorRelationship;
                        smsParentContractBO.SMSParentContractDetailList.Add(smsParentContractDetailObj);
                        moneyContract = this.GetMoneyByServiceID(Int32.Parse(servicesPackageID), phone, listService);
                        smsParentContractDetailObj.Money = moneyContract;
                        totalPay += moneyContract;
                    }

                    if (phone.CheckMobileNumberVT())
                    {
                        countVT += 1;
                        quantityPhone += 1;
                    }
                    else
                    {
                        countOther += 1;
                        quantityPhone += 1;
                    }
                    smsParentContractBOList.Add(smsParentContractBO);
                }
            }

            Session[SMSParentManageContractConstant.LIST_REGIS] = smsParentContractBOList;
            Session[SMSParentManageContractConstant.PRICE_PAY] = totalPay;
            return Json(new { message = "Đăng ký thành công", Balance = Balance, totalPay = totalPay, eWalletID = ewalletObj.EWalletID, countVT = countVT, countOther = countOther, quantityPhone = quantityPhone, PupilNumber = pupilNumber });
        }

        /// <summary>
        ///Chuẩn bị Thanh toán Trả chậm
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayPostage(FormCollection frm)
        {
            string[] arrpupilID = frm["pupilID"].Split(GlobalConstantsEdu.charCOMMA);
            SMSParentContractBO contractBO = null;
            SMS_PARENT_CONTRACT contractEntity = null;
            SMS_PARENT_CONTRACT_DETAIL contractDetailEntity = null;
            List<SMSParentContractBO> smsParentContractBOList = new List<SMSParentContractBO>();
            //Lay thong tin goi cuoc duoc ap dung cho truong
            List<ServicePackageBO> listSP = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            SMS_SERVICE_PACKAGE servicePackage = null;
            ServicePackageBO spBO = null;
            //Lay thong tin so du tai khoan truong
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);
            decimal totalPay = 0;
            int countVT = 0;
            int countOther = 0;
            int quantityPhone = 0;
            int pupilNumber = 0;
            int pupilID = 0;
            bool isCountParent = false;
            for (int i = 0; i < arrpupilID.Length; i++)
            {
                SMSParentContractDetailBO contractDetail = null;
                pupilID = Convert.ToInt32(arrpupilID[i]);

                string classStr = frm["ClassID-" + pupilID];
                string contractStr = frm["SMSParentContract-" + pupilID];

                int contractID = 0;
                int contractDetailID = 0;
                int classID = 0;
                int pupilOfClassID = 0;
                if (!string.IsNullOrWhiteSpace(classStr)) classID = Convert.ToInt32(classStr);

                if (!string.IsNullOrWhiteSpace(contractStr)) contractID = Convert.ToInt32(contractStr);

                contractEntity = smsParentContractBusiness.Find(contractID);
                if (contractEntity != null)
                {
                    isCountParent = false;

                    if (frm["PupilOfClassID-" + pupilID] != null) Int32.TryParse(frm["PupilOfClassID-" + pupilID].ToString(), out pupilOfClassID);

                    contractBO = new SMSParentContractBO();
                    contractBO.SMSParentContractID = contractID;
                    contractBO.SchoolID = contractEntity.SCHOOL_ID;
                    contractBO.PupilFileID = contractEntity.PUPIL_ID;
                    contractBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    contractBO.ClassID = classID;
                    contractBO.PupilOfClassID = pupilOfClassID;

                    if (frm["HfSMSParentContractDetailID-" + pupilID] == null) continue;
                    string[] ContractDetailArr = frm["HfSMSParentContractDetailID-" + pupilID].Split(GlobalConstantsEdu.charCOMMA);
                    if (ContractDetailArr.Length > 0)
                    {
                        contractBO.SMSParentContractDetailList = new List<SMSParentContractDetailBO>();
                        for (int j = 0; j < ContractDetailArr.Length; j++)
                        {
                            contractDetailID = Convert.ToInt32(ContractDetailArr[j]);
                            if (string.IsNullOrEmpty(frm["Checkout-" + contractDetailID])) continue;

                            contractDetailEntity = smsParentContractDetailBusiness.Find(contractDetailID);
                            if (contractDetailEntity != null)
                            {
                                contractDetail = new SMSParentContractDetailBO();
                                contractDetail.Subscriber = contractDetailEntity.SUBSCRIBER.ExtensionMobile(); ;
                                contractDetail.SubscriberType = (short)contractDetailEntity.SUBSCRIBER_TYPE;
                                contractDetail.SubscriptionStatus = GlobalConstantsEdu.STATUS_REGISTED;
                                contractDetail.Year = contractDetailEntity.YEAR_ID;
                                contractDetail.ServicePackageID = contractDetailEntity.SERVICE_PACKAGE_ID;
                                contractDetail.SubscriptionTime = contractDetailEntity.SUBSCRIPTION_TIME;
                                contractDetail.SMSParentContractDetailID = contractDetailEntity.SMS_PARENT_CONTRACT_DETAIL_ID;
                                contractBO.SMSParentContractDetailList.Add(contractDetail);
                                spBO = listSP.FirstOrDefault(o => o.ServicePackageID == contractDetailEntity.SERVICE_PACKAGE_ID);

                                // Anhvd9 20160119 - Bổ sung Thanh toán Nợ cho các gói đã ngừng áp dụng
                                if (spBO == null)
                                {
                                    servicePackage = servicePackageBusiness.Find(contractDetailEntity.SERVICE_PACKAGE_ID);
                                    spBO = new ServicePackageBO();
                                    spBO.ServicePackageID = servicePackage.SERVICE_PACKAGE_ID;
                                    spBO.ViettelPrice = servicePackage.VIETTEL_PRICE;
                                    spBO.OtherPrice = servicePackage.OTHER_PRICE;
                                    listSP.Add(spBO);
                                }
                                // End 20160119
                                if (contractDetailEntity.SUBSCRIBER.CheckMobileNumberVT())
                                {
                                    countVT += 1;
                                    if (contractDetailEntity.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                        totalPay += 2 * (spBO.ViettelPrice.HasValue ? spBO.ViettelPrice.Value : 0);
                                    else totalPay += spBO.ViettelPrice.HasValue ? spBO.ViettelPrice.Value : 0;
                                }
                                else
                                {
                                    countOther += 1;
                                    if (contractDetailEntity.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                        totalPay += 2 * (spBO.OtherPrice.HasValue ? spBO.OtherPrice.Value : 0);
                                    else totalPay += spBO.OtherPrice.HasValue ? spBO.OtherPrice.Value : 0;

                                }
                                isCountParent = true;
                                quantityPhone++;
                            }
                        }
                    }
                    smsParentContractBOList.Add(contractBO);
                    if (isCountParent) pupilNumber++;
                }
            }

            Session[SMSParentManageContractConstant.LIST_PAY_POSTAGE] = smsParentContractBOList;
            Session[SMSParentManageContractConstant.PRICE_PAY_POSTAGE] = totalPay;
            return Json(new { message = "Chuẩn bị thanh toán thành công", Balance = ewalletObj.Balance, totalPay = totalPay, eWalletID = ewalletObj.EWalletID, countVT = countVT, countOther = countOther, quantityPhone = quantityPhone, PupilNumber = pupilNumber });
        }

        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SaveFormRegis(bool regised, decimal totalPay, int eWalletID, int quantity, bool allowDeferredPayment = false)
        {
            //VTODO 
            DateTime eventDate = DateTime.Now;
            string para = string.Format("regised={0}, totalPay={1}, eWalletID={2}, quantity={3}, allowDeferredPayment={4}", regised, totalPay, eWalletID, quantity, allowDeferredPayment);
            if (!regised)
            {
                Session.Remove(SMSParentManageContractConstant.LIST_REGIS);
                Session.Remove(SMSParentManageContractConstant.PRICE_PAY);
            }
            else
            {
                if (Session[SMSParentManageContractConstant.LIST_REGIS] != null)
                {
                    List<SMSParentContractBO> smsParentContractBOList = (List<SMSParentContractBO>)Session[SMSParentManageContractConstant.LIST_REGIS];
                    decimal? price = (decimal)Session[SMSParentManageContractConstant.PRICE_PAY];
                    if (allowDeferredPayment)
                    {
                        // Ghi nhan giao dich(tru tien = 0)
                        price = 0;
                        // Trường hợp Trả chậm
                        foreach (SMSParentContractBO contract in smsParentContractBOList)
                        {
                            if (contract != null && contract.SMSParentContractDetailList.Count > 0)
                            {
                                foreach (SMSParentContractDetailBO detail in contract.SMSParentContractDetailList)
                                {
                                    if (detail.Money > 0)
                                    {
                                        // Trạng thái Nợ cước
                                        detail.SubscriptionStatus = GlobalConstantsEdu.STATUS_UNPAID;
                                    }
                                }
                            }
                        }
                    }
                    Dictionary<string, object> dic = smsParentContractBusiness.SaveRegisContract(smsParentContractBOList, this.Year);
                    // Trừ tiền tài khoản Payment
                    ewalletBusiness.ChangeAmountSub(eWalletID, price.Value, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, quantity, GlobalConstantsEdu.SERVICE_TYPE_REGIS_CONTRACT);
                    // Ghi log
                    StringBuilder builder = new StringBuilder();
                    builder.Append("Lưu: ").Append(DateTime.Now.ToString())
                        .Append("-Loại giao dịch: GD thanh toán -Nội dung: Đăng ký dịch vụ SLLĐT, số lượng: ")
                        .Append(quantity).Append("PHHS, số tiền ").Append(price).Append(" VNĐ -Dịch vụ: SLLĐT");
                    if (allowDeferredPayment) builder.Append(" - HÌNH THỨC TRẢ CHẬM");
                    //VTODO WriteLogInfo(builder.ToString(), null, string.Empty, string.Empty, null, string.Empty);

                    // Tạo tài khoản SParent
                    if (dic.ContainsKey(GlobalConstantsEdu.LIST_SPARENT) && dic[GlobalConstantsEdu.LIST_SPARENT] != null)
                    {
                        List<SParentAccountBO> listAccountSParent = (List<SParentAccountBO>)dic[GlobalConstantsEdu.LIST_SPARENT];
                        if (listAccountSParent != null && listAccountSParent.Count > 0)
                        {
                            //VTODO
                            this.smsParentContractBusiness.CreateListSParentAccount(listAccountSParent);
                            //LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, eventDate, para, "Tao tai khoan Sparent: " + listAccountSParent.Count, GlobalInfo.UserName);
                        }
                    }

                    // Cap nhat thong tin o phia SMAS3
                    if (smsParentContractBOList != null && smsParentContractBOList.Count > 0)
                    {
                        //Tao list session de update cot isregiscontract smas3
                        Session[SMSParentManageContractConstant.LIST_SYNC_SMAS3] = smsParentContractBOList.Select(p => p.PupilOfClassID).ToList();
                    }

                    Session.Remove(SMSParentManageContractConstant.LIST_REGIS);
                    Session.Remove(SMSParentManageContractConstant.PRICE_PAY);
                    UpdatePupilOfClass();
                    return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Message = "Đăng ký thành công." });

                }
                else
                {
                    return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get("SendSMS_Label_CoreExceptionError") });
                }
            }
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS });
        }

        /// <summary>
        /// Thanh toán trả chậm
        /// </summary>
        /// <param name="isCheckout"></param>
        /// <param name="totalPay"></param>
        /// <param name="eWalletID"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult Checkout(bool isCheckout, decimal totalPay, int eWalletID, int quantity)
        {
            if (!isCheckout)
            {
                Session.Remove(SMSParentManageContractConstant.LIST_PAY_POSTAGE);
                Session.Remove(SMSParentManageContractConstant.PRICE_PAY_POSTAGE);
            }
            else
            {
                if (Session[SMSParentManageContractConstant.LIST_PAY_POSTAGE] != null && Session[SMSParentManageContractConstant.PRICE_PAY_POSTAGE] != null)
                {
                    List<SMSParentContractBO> smsParentContractBOList = (List<SMSParentContractBO>)Session[SMSParentManageContractConstant.LIST_PAY_POSTAGE];
                    decimal? price = (decimal)Session[SMSParentManageContractConstant.PRICE_PAY_POSTAGE];
                    if (smsParentContractBOList != null && smsParentContractBOList.Count > 0)
                    {
                        try
                        {
                            // Anhvd9 20160201 - Cập nhật mới thực hiện trừ tiền thanh toán
                            // Cập nhật lại các thuê bao Trả chậm sau khi được Thanh toán
                            smsParentContractBusiness.UpdateDeferredContract(smsParentContractBOList, this.Year);
                            // Trừ tiền tài khoản Payment
                            ewalletBusiness.ChangeAmountSub(eWalletID, price.Value, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, quantity, GlobalConstantsEdu.SERVICE_TYPE_CHECKOUT_DEFERRED_PAYMENT);

                            StringBuilder builder = new StringBuilder();
                            builder.Append("Lưu: ").Append(DateTime.Now.ToString())
                                .Append("-Loại giao dịch: GD thanh toán Nợ cước -Nội dung: Đăng ký dịch vụ SLLĐT, số lượng: ")
                                .Append(quantity).Append("PHHS, số tiền ").Append(price).Append(" VNĐ -Dịch vụ: SLLĐT");
                            //VTODO WriteLogInfo(builder.ToString(), null, string.Empty, string.Empty, null, string.Empty);

                            Session.Remove(SMSParentManageContractConstant.LIST_PAY_POSTAGE);
                            Session.Remove(SMSParentManageContractConstant.PRICE_PAY_POSTAGE);
                            UpdatePupilOfClass();
                            return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Message = "Thanh toán thành công." });
                        }
                        catch (Exception ex)
                        {
                            //VTODO 
                            //
                            string para = string.Format("totalPay={0}, eWalletID={1},quantity={2}", totalPay, eWalletID, quantity);
                            LogExtensions.ErrorExt(logger, DateTime.Now, "Checkout", para, ex);
                            return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get("SendSMS_Label_CoreExceptionError") });
                        }
                    }
                    else
                    {
                        return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get("SendSMS_Label_CoreExceptionError") });
                    }
                }
                else
                {
                    return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get("SendSMS_Label_CoreExceptionError") });
                }
            }
            return Json(new { });
        }
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult AddSubscriber(InsertAddNewViewModel objInsert)
        {
            List<SMS_PARENT_CONTRACT_DETAIL> listInsert = new List<SMS_PARENT_CONTRACT_DETAIL>();
            SMS_PARENT_CONTRACT_DETAIL smsParentContactDetailObj = null;
            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            //Danh sach hoc sinh da co hop dong 
            List<SMSParentContractDetailBO> listContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(_globalInfo.SchoolID.Value, this.Year);
            decimal amount = 0;
            int partitionID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //check validate neu thue bao da dang ky voi hoc ky trung voi hoc ky da chon thi khong cho them
            string phone = objInsert.PhoneReceiver;
            if (string.IsNullOrEmpty(phone))
            {
                return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("blb_Message_Phone_Invalid") });
            }
            else
            {
                if (!phone.CheckMobileNumber())
                {
                    return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("blb_Message_Phone_Invalid") });
                }
            }
            int countdouble = 0;

            //lay danh sach hoc sinh duoc dang ky goi SMSFREE
            int pupilID = 0;
            int classID = 0;
            SMSParentContractDetailBO objCD = listContract.Where(p => p.SMSParentContractID == objInsert.SMSParentContactID).FirstOrDefault();
            pupilID = objCD != null ? objCD.PupilID : 0;
            classID = objCD != null ? objCD.ClassID : 0;

            // 20151119 - AnhVD9 - Kiem tra goi cuoc chi duoc dau noi mang
            ServicePackageBO servicePackage = listService.FirstOrDefault(o => o.ServicePackageID == objInsert.ServicesPackageAdd);

            if (servicePackage != null)
            {
                if (servicePackage.IsForOnlyInternal == true && !phone.CheckMobileNumberVT())
                {
                    return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyInternalNetwork") });
                }

                if (servicePackage.IsForOnlyExternal == true && phone.CheckMobileNumberVT())
                {
                    return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyExternalNetwork") });
                }

                if (servicePackage.ServiceCode.Equals("SMSFree"))
                {

                    List<int> lstPupilID = new List<int>();
                    lstPupilID.Add(pupilID);
                    Dictionary<string, object> dicRegister = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID.Value},
                        {"YearID",_globalInfo.AcademicYearID.Value},
                        {"lstPupilID",lstPupilID},
                        {"StatusRegisterID",GlobalConstantsEdu.STATUS_REGISSMSFREE_ISAPROVE}
                    };
                    List<int> lstPupilRegisterSMSFreeID = smsParentContractBusiness.GetListRegisterSMSFree(dicRegister).Select(p => p.PUPIL_ID).Distinct().ToList();
                    countdouble = listContract.Where(p => p.SMSParentContractID == objInsert.SMSParentContactID && p.ServiceCode.Equals("SMSFree")).Count();
                    if (!lstPupilRegisterSMSFreeID.Contains(pupilID))
                    {
                        return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_NotRegisterSMSFree") });
                    }
                    else if (countdouble > 0)
                    {
                        return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlySMSFree") });
                    }
                }

                //viethd31: Neu la goi co gia 0 dong thi check da co dang ky goi 0 dong truoc do hay chua
                if ((!servicePackage.ViettelPrice.HasValue || servicePackage.ViettelPrice == 0) && (!servicePackage.OtherPrice.HasValue || servicePackage.OtherPrice == 0))
                {
                    countdouble = listContract.Where(p => p.SMSParentContractID == objInsert.SMSParentContactID &&
                        ((!p.ViettelPrice.HasValue || p.ViettelPrice == 0) && (!p.OtherPrice.HasValue || p.OtherPrice == 0))).Count();
                    if (countdouble > 0)
                    {
                        return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyOneFree") });
                    }
                }
            }

            if (objInsert.Semester3 == GlobalConstants.SEMESTER_OF_YEAR_ALL || servicePackage.IsWholeYearPackage == true)
            {
                // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                //if (servicePackage != null && servicePackage.IsAllowRegisterSideSubscriber == true)
                //{
                //    if (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(objInsert.SMSParentContactID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                //        return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Chưa đủ điều kiện áp dụng cho gói cước này với thuê bao thứ 2" });
                //}
                //check validate 
                objInsert.Semester3 = GlobalConstants.SEMESTER_OF_YEAR_ALL;

                List<int> lstExistSemester = new List<int>();
                if (this.checkSubscriptionTime(phone, objInsert.Semester3, objInsert.SMSParentContactID, lstExistSemester))
                {
                    string strSemester = string.Empty;
                    for (int i = 0; i < lstExistSemester.Count; i++)
                    {
                        strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                        if (i < lstExistSemester.Count - 1)
                        {
                            strSemester += ", ";
                        }


                    }
                    return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objInsert.PupilName, strSemester) });
                }
                smsParentContactDetailObj = new SMS_PARENT_CONTRACT_DETAIL();
                smsParentContactDetailObj.SMS_PARENT_CONTRACT_ID = objInsert.SMSParentContactID;
                smsParentContactDetailObj.PARTITION_ID = partitionID;
                smsParentContactDetailObj.SUBSCRIBER = objInsert.PhoneReceiver.ExtensionMobile();
                smsParentContactDetailObj.SUBSCRIBER_TYPE = smsParentContactDetailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                smsParentContactDetailObj.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                smsParentContactDetailObj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_REGISTED;
                smsParentContactDetailObj.YEAR_ID = this.Year;
                smsParentContactDetailObj.AMOUNT = (int)objInsert.Money;
                smsParentContactDetailObj.IS_ACTIVE = true;
                smsParentContactDetailObj.CREATED_TIME = DateTime.Now;
                smsParentContactDetailObj.SERVICE_PACKAGE_ID = objInsert.ServicesPackageAdd;
                smsParentContactDetailObj.RECEIVER_NAME = objInsert.ReceiverName;
                smsParentContactDetailObj.RELATION_SHIP = objInsert.RelationshipAdd;
                smsParentContactDetailObj.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                smsParentContactDetailObj.SCHOOL_ID = _globalInfo.SchoolID.Value;
                smsParentContactDetailObj.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                smsParentContactDetailObj.CLASS_ID = classID;
                smsParentContactDetailObj.PUPIL_ID = pupilID;
                listInsert.Add(smsParentContactDetailObj);

                amount = this.GetMoneyByServiceID(objInsert.ServicesPackageAdd, objInsert.PhoneReceiver, listService);
            }
            else
            {
                if (objInsert.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                    //if (servicePackage != null && servicePackage.IsAllowRegisterSideSubscriber == true)
                    //{
                    //    if (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(objInsert.SMSParentContactID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST))
                    //        return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Chưa đủ điều kiện áp dụng cho gói cước này với thuê bao thứ 2" });
                    //}

                    //check validate 
                    List<int> lstExistSemester = new List<int>();
                    if (this.checkSubscriptionTime(phone, objInsert.Semester1, objInsert.SMSParentContactID, lstExistSemester))
                    {
                        string strSemester = string.Empty;
                        for (int i = 0; i < lstExistSemester.Count; i++)
                        {
                            strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                            if (i < lstExistSemester.Count - 1)
                            {
                                strSemester += ", ";
                            }


                        }
                        return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objInsert.PupilName, strSemester) });
                    }

                    smsParentContactDetailObj = new SMS_PARENT_CONTRACT_DETAIL();
                    smsParentContactDetailObj.SMS_PARENT_CONTRACT_ID = objInsert.SMSParentContactID;
                    smsParentContactDetailObj.PARTITION_ID = partitionID;
                    smsParentContactDetailObj.SUBSCRIBER = objInsert.PhoneReceiver.ExtensionMobile();
                    smsParentContactDetailObj.SUBSCRIBER_TYPE = smsParentContactDetailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                    smsParentContactDetailObj.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                    smsParentContactDetailObj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_REGISTED;
                    smsParentContactDetailObj.YEAR_ID = this.Year;
                    smsParentContactDetailObj.AMOUNT = (int)objInsert.Money;
                    smsParentContactDetailObj.IS_ACTIVE = true;
                    smsParentContactDetailObj.CREATED_TIME = DateTime.Now;
                    smsParentContactDetailObj.SERVICE_PACKAGE_ID = objInsert.ServicesPackageAdd;
                    smsParentContactDetailObj.RECEIVER_NAME = objInsert.ReceiverName;
                    smsParentContactDetailObj.RELATION_SHIP = objInsert.RelationshipAdd;
                    smsParentContactDetailObj.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                    smsParentContactDetailObj.SCHOOL_ID = _globalInfo.SchoolID.Value;
                    smsParentContactDetailObj.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                    smsParentContactDetailObj.CLASS_ID = classID;
                    smsParentContactDetailObj.PUPIL_ID = pupilID;
                    listInsert.Add(smsParentContactDetailObj);
                    amount = this.GetMoneyByServiceID(objInsert.ServicesPackageAdd, objInsert.PhoneReceiver, listService);
                    //amount = objInsert.Money;
                }

                if (objInsert.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
                    //if (servicePackage != null && servicePackage.IsAllowRegisterSideSubscriber == true)
                    //{
                    //    if (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(objInsert.SMSParentContactID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                    //        return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Chưa đủ điều kiện áp dụng cho gói cước này với thuê bao thứ 2" });
                    //}
                    //check validate 
                    List<int> lstExistSemester = new List<int>();
                    if (this.checkSubscriptionTime(phone, objInsert.Semester2, objInsert.SMSParentContactID, lstExistSemester))
                    {
                        string strSemester = string.Empty;
                        for (int i = 0; i < lstExistSemester.Count; i++)
                        {
                            strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                            if (i < lstExistSemester.Count - 1)
                            {
                                strSemester += ", ";
                            }


                        }
                        return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objInsert.PupilName, strSemester) });
                    }

                    smsParentContactDetailObj = new SMS_PARENT_CONTRACT_DETAIL();
                    smsParentContactDetailObj.SMS_PARENT_CONTRACT_ID = objInsert.SMSParentContactID;
                    smsParentContactDetailObj.PARTITION_ID = partitionID;
                    smsParentContactDetailObj.SUBSCRIBER = objInsert.PhoneReceiver.ExtensionMobile();
                    smsParentContactDetailObj.SUBSCRIBER_TYPE = smsParentContactDetailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                    smsParentContactDetailObj.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                    smsParentContactDetailObj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_REGISTED;
                    smsParentContactDetailObj.YEAR_ID = this.Year;
                    smsParentContactDetailObj.AMOUNT = (int)objInsert.Money;
                    smsParentContactDetailObj.IS_ACTIVE = true;
                    smsParentContactDetailObj.CREATED_TIME = DateTime.Now;
                    smsParentContactDetailObj.SERVICE_PACKAGE_ID = objInsert.ServicesPackageAdd;
                    smsParentContactDetailObj.RECEIVER_NAME = objInsert.ReceiverName;
                    smsParentContactDetailObj.RELATION_SHIP = objInsert.RelationshipAdd;
                    smsParentContactDetailObj.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                    smsParentContactDetailObj.SCHOOL_ID = _globalInfo.SchoolID.Value;
                    smsParentContactDetailObj.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                    smsParentContactDetailObj.CLASS_ID = classID;
                    smsParentContactDetailObj.PUPIL_ID = pupilID;
                    listInsert.Add(smsParentContactDetailObj);

                    amount = this.GetMoneyByServiceID(objInsert.ServicesPackageAdd, objInsert.PhoneReceiver, listService);
                }
            }


            if (objInsert.Semester3 == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                amount = amount * 2;
            }

            //viethd31: Fix loi kiem tra so tien tai khoan
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);

            if (ewalletObj.Balance < amount) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Tài khoản không đủ tiền để thanh toán!" });

            //tong so tien thay doi
            //update thong tin so du cua tai khoan ben Payment
            //Vi ham su dung cho them thue bao tren hop dong nen chi truyen quaity = 1
            EW_EWALLET ewalletOBj = new EW_EWALLET();
            if (amount > 0)
            {
                ewalletOBj = ewalletBusiness.ChangeAmountSub(objInsert.EwalletID, amount, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, 1, GlobalConstantsEdu.SERVICE_TYPE_REGIS_CONTRACT);
            }
            smsParentContractDetailBusiness.SaveDetail(listInsert, this.Year, _globalInfo.SchoolID.Value);
            //VTODO WriteLogInfo(DateTime.Now.ToString() + "-Loại giao dịch: GD thanh toán " + "-Nội dung: Đăng ký dịch vụ SLLĐT cho SĐT " + objInsert.PhoneReceiver + ", số tiền " + amount + " VNĐ" + "-Số tiền: " + amount + "-Dịch vụ: SLLĐT", null, string.Empty, string.Empty, null, string.Empty);
            return Json(new { type = GlobalConstants.TYPE_SUCCESS, message = "Thêm thuê bao thành công.", ewalletOBj = ewalletOBj });
        }

        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult DeleteContractDetail(int contractID, string phone, int classID, int pupilID, int servicePackageID)
        {
            if (!this.CheckPhoneNumber(phone)) return Json(new { message = Res.Get("blb_Message_Phone_Invalid") });
            if (!smsParentContractDetailBusiness.CheckIDSMSParentContract(_globalInfo.SchoolID.Value, this.Year, contractID)) return Json(new { message = Res.Get("blb_Message_Phone_Invalid") });
            smsParentContractDetailBusiness.DeleteContractDetail(contractID, phone, servicePackageID);
            //VTODO WriteLogInfo(DateTime.Now.ToString() + "Hủy thuê bao: " + phone, null, string.Empty, string.Empty, null, string.Empty);
            return Json(new { message = "Hủy thuê bao thành công" });
        }

        /// <summary>
        /// Hủy nhiều thuê bao
        /// </summary>
        /// <author date="19/02/2016">Anhvd9</author>
        /// <param name="contractID"></param>
        /// <param name="phone"></param>
        /// <param name="classID"></param>
        /// <param name="pupilID"></param>
        /// <param name="servicePackageID"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult CancelListSubscriber(List<long> lstContractID, List<string> lstPhone, List<int> lstServicePackageID, List<long> lstContractDetailID)
        {
            if (lstContractID.Count != lstPhone.Count || lstContractID.Count != lstServicePackageID.Count)
                return Json(new { message = "Danh sách thuê bao hủy không hợp lệ", type = GlobalConstants.TYPE_ERROR });
            smsParentContractDetailBusiness.CancelListContractDetail(lstContractID, lstPhone, lstServicePackageID, lstContractDetailID);
            //VTODO WriteLogInfo("Hủy danh sách thuê bao: " + String.Join(", ", lstPhone.ToArray()), null, String.Join(", ", lstContractID.ToArray()), string.Empty, null, string.Empty);
            return Json(new { message = "Đã hủy danh sách thuê bao thành công", type = GlobalConstants.TYPE_SUCCESS });
        }

        public EditViewModel GetContractDetailExtra(int contractID, string phone, int servicePackageID)
        {
            //Lay list service dang su dung voi thoi gian hien tai
            List<ServicePackageBO> servicePackageList = servicePackageBusiness.GetListExtraServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);

            /*
                * Nếu sửa trên chính duy nhất gói cước đang hoạt động thì bỏ các gói phụ
                * Nếu sửa trên gói chính đã có 1 gói phụ cũng ko cho phép
                * Nếu sửa trên gói khác đang Nợ cước cũng k cho
                * 
             */

            //if (!smsParentContractDetailBusiness.All.Any(o => o.SMSParentContractID == contractID && o.PartitionSMSParentContractID == contractID % GlobalConstants.PARTITION_SMSPARENTCONTRACT_DETAIL_SMSPARENTCONTRACTID && o.IS_ACTIVE && o.SubscriptionStatus == GlobalConstants.STATUS_REGISTED
            //       && (o.SUBSCRIBER != phone || o.ServicePackageID != servicePackageID) && o.ServicePackage.IsAllowRegisterSideSubscriber != true))
            //{
            //    servicePackageList = servicePackageList.Where(o => o.IsAllowRegisterSideSubscriber != true).ToList();
            //}

            List<SMS_PARENT_CONTRACT_DETAIL> detailList = smsParentContractDetailBusiness.GetContractDetailByContactID(contractID, phone, servicePackageID);
            EditViewModel objResult = null;
            if (detailList != null && detailList.Count > 0)
            {
                SMS_PARENT_CONTRACT_DETAIL obj = detailList.FirstOrDefault();
                objResult = new EditViewModel();
                objResult.SMSParentContractDetailID = (int)obj.SMS_PARENT_CONTRACT_DETAIL_ID;
                objResult.SMSParentContractID = (int)obj.SMS_PARENT_CONTRACT_ID;
                objResult.Subscriber = obj.SUBSCRIBER;
                objResult.SubscriberType = (short)obj.SUBSCRIBER_TYPE;
                objResult.SubscriptionStatus = (short)obj.SUBSCRIPTION_STATUS;
                objResult.Year = obj.YEAR_ID;

                objResult.IsActive = obj.IS_ACTIVE;
                objResult.ServicePackageID = obj.SERVICE_PACKAGE_ID;
                objResult.Relationship = obj.RELATION_SHIP;
                objResult.ReceiverName = obj.RECEIVER_NAME;
                objResult.CreateTime = obj.CREATED_TIME;

                TimeSpan Diff = DateTime.Now - objResult.CreateTime;

                //Thoi gian hoc ky dang ky
                if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {
                    objResult.Semester3 = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                }
                else
                {
                    if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_FIRST) objResult.Semester1 = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                    if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_SECOND) objResult.Semester2 = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }

                //lay hinh thuc dang ky cuoi cung          
                SMS_PARENT_CONTRACT_DETAIL objDetail = detailList.OrderByDescending(p => p.CREATED_TIME).FirstOrDefault();
                short? RegistrationType = null;
                if (objDetail != null) RegistrationType = objDetail.REGISTRATION_TYPE.HasValue ? (short?)objDetail.REGISTRATION_TYPE : null;

                if (RegistrationType == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS) objResult.RegistrationType = "SMS";
                else objResult.RegistrationType = Res.Get("Lbl_Regis_By_School");


                StringBuilder strAppdend = new StringBuilder();
                //Anhvd9 20151120 - Nếu Đang Nợ cước thì không cho đổi sang các gói 0 đồng
                if (obj.SUBSCRIPTION_STATUS >= GlobalConstantsEdu.STATUS_UNPAID) servicePackageList = servicePackageList.Except(servicePackageList.Where(o => o.ViettelPrice == 0 || o.OtherPrice == 0).ToList()).ToList();
                // End 20151120


                for (int i = 0; i < servicePackageList.Count; i++)
                {
                    strAppdend.Append("<option value=" + servicePackageList[i].ServicePackageID + ">" + servicePackageList[i].ServiceCode + "</option>");
                }
                objResult.ListServicePackage = strAppdend.ToString();

            }
            return objResult;
        }

        public EditViewModel GetContractDetailEdit(int contractID, string phone, int servicePackageID)
        {
            //Lay list service dang su dung voi thoi gian hien tai
            List<ServicePackageBO> servicePackageList = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);

            /*
                * Nếu sửa trên chính duy nhất gói cước đang hoạt động thì bỏ các gói phụ
                * Nếu sửa trên gói chính đã có 1 gói phụ cũng ko cho phép
                * Nếu sửa trên gói khác đang Nợ cước cũng k cho
                * 
             */

            //if (!smsParentContractDetailBusiness.All.Any(o => o.SMSParentContractID == contractID && o.PartitionSMSParentContractID == contractID % GlobalConstants.PARTITION_SMSPARENTCONTRACT_DETAIL_SMSPARENTCONTRACTID && o.IS_ACTIVE && o.SubscriptionStatus == GlobalConstants.STATUS_REGISTED
            //       && (o.SUBSCRIBER != phone || o.ServicePackageID != servicePackageID) && o.ServicePackage.IsAllowRegisterSideSubscriber != true))
            //{
            //    servicePackageList = servicePackageList.Where(o => o.IsAllowRegisterSideSubscriber != true).ToList();
            //}

            // Giới hạn số ngày còn được đổi Học kỳ
            int dateLimitRegis = 30;
            if (!string.IsNullOrEmpty(this.GetString("IsLimitRegisChangeFirstSemester"))) dateLimitRegis = this.GetInt("IsLimitRegisChangeFirstSemester");

            List<SMS_PARENT_CONTRACT_DETAIL> detailList = smsParentContractDetailBusiness.GetContractDetailByContactID(contractID, phone, servicePackageID);
            EditViewModel objResult = null;
            if (detailList != null && detailList.Count > 0)
            {
                SMS_PARENT_CONTRACT_DETAIL obj = detailList.FirstOrDefault();
                objResult = new EditViewModel();
                objResult.SMSParentContractDetailID = (int)obj.SMS_PARENT_CONTRACT_DETAIL_ID;
                objResult.SMSParentContractID = (int)obj.SMS_PARENT_CONTRACT_ID;
                objResult.Subscriber = obj.SUBSCRIBER;
                objResult.SubscriberType = (short)obj.SUBSCRIBER_TYPE;
                objResult.SubscriptionStatus = (short)obj.SUBSCRIPTION_STATUS;
                objResult.Year = obj.YEAR_ID;

                objResult.IsActive = obj.IS_ACTIVE;
                objResult.ServicePackageID = obj.SERVICE_PACKAGE_ID;
                objResult.Relationship = obj.RELATION_SHIP;
                objResult.ReceiverName = obj.RECEIVER_NAME;
                objResult.CreateTime = obj.CREATED_TIME;

                TimeSpan Diff = DateTime.Now - objResult.CreateTime;
                if ((DateTime.Now - objResult.CreateTime).TotalDays > dateLimitRegis) objResult.LimitChangeFirstSemester = true;
                else objResult.LimitChangeFirstSemester = false;

                //Thoi gian hoc ky dang ky
                if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {

                    objResult.Semester3 = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                }
                else if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_FIRST) objResult.Semester1 = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                if (detailList.Where(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.SUBSCRIPTION_TIME).FirstOrDefault() == GlobalConstants.SEMESTER_OF_YEAR_SECOND) objResult.Semester2 = GlobalConstants.SEMESTER_OF_YEAR_SECOND;

                //lay hinh thuc dang ky cuoi cung          
                SMS_PARENT_CONTRACT_DETAIL objDetail = detailList.OrderByDescending(p => p.CREATED_TIME).FirstOrDefault();
                int? RegistrationType = null;
                if (objDetail != null) RegistrationType = objDetail.REGISTRATION_TYPE.HasValue ? objDetail.REGISTRATION_TYPE : null;

                if (RegistrationType == GlobalConstantsEdu.REGISTRATION_TYPE_BY_SMS) objResult.RegistrationType = "SMS";
                else objResult.RegistrationType = Res.Get("Lbl_Regis_By_School");


                StringBuilder strAppdend = new StringBuilder();
                //Anhvd9 20151120 - Nếu Đang Nợ cước thì không cho đổi sang các gói 0 đồng
                if (obj.SUBSCRIPTION_STATUS >= GlobalConstantsEdu.STATUS_UNPAID) servicePackageList = servicePackageList.Except(servicePackageList.Where(o => o.ViettelPrice == 0 || o.OtherPrice == 0).ToList()).ToList();
                // End 20151120

                //Kiem tra goi cuoc con han su dung hay khong?
                if (servicePackageList.Exists(p => p.ServicePackageID == obj.SERVICE_PACKAGE_ID))
                {
                    // Gói cước còn áp dụng
                    objResult.ExpiryDate = false;
                    for (int i = 0; i < servicePackageList.Count; i++) strAppdend.Append("<option value=" + servicePackageList[i].ServicePackageID + ">" + servicePackageList[i].ServiceCode + "</option>");
                    objResult.ListServicePackage = strAppdend.ToString();
                }
                else
                {
                    // Gói cước đã hết áp dụng
                    objResult.ExpiryDate = true;
                    // Anhvd9 20160301 - Danh sách gói cước màn hình sửa đã bao gồm gói hiện tại dù hết áp dụng
                    List<ServicePackageBO> listServiceEdit = servicePackageBusiness.GetListServicesPackageEdit(this.Year, this.Semester, this.AcademicYear, servicePackageID, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
                    for (int i = 0; i < listServiceEdit.Count; i++) strAppdend.Append("<option value=" + listServiceEdit[i].ServicePackageID + ">" + listServiceEdit[i].ServiceCode + "</option>");
                    objResult.ListServicePackage = strAppdend.ToString();
                }

                //Tong so tien da thanh toan
                MoneyMaxSMSBO moneymaxSMSBOObj = servicePackageBusiness.GetSMSByServicesPackageIDEdit(obj.SERVICE_PACKAGE_ID, this.AcademicYear);
                if (obj.SUBSCRIBER.CheckMobileNumber())
                {
                    if (obj.SUBSCRIBER.CheckMobileNumberVT())
                    {
                        objResult.Money = moneymaxSMSBOObj.MoneyViettel.HasValue ? (int)moneymaxSMSBOObj.MoneyViettel.Value : 0;

                        if (objResult.Semester3 == GlobalConstants.SEMESTER_OF_YEAR_ALL || (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST && objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            objResult.TotalMoney = moneymaxSMSBOObj.MoneyViettel * 2;
                        else if (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST || objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            objResult.TotalMoney = moneymaxSMSBOObj.MoneyViettel;
                    }
                    else
                    {
                        objResult.Money = moneymaxSMSBOObj.MoneyOther.HasValue ? (int)moneymaxSMSBOObj.MoneyOther.Value : 0;

                        if (objResult.Semester3 == GlobalConstants.SEMESTER_OF_YEAR_ALL || (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST && objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            objResult.TotalMoney = moneymaxSMSBOObj.MoneyOther * 2;
                        else if (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST || objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            objResult.TotalMoney = moneymaxSMSBOObj.MoneyOther;
                    }
                }
                objResult.MaxSMS = moneymaxSMSBOObj.MaxSMS;
            }
            return objResult;
        }

        public PartialViewResult LoadCreateView(int tab, int contractID, string phone, int servicePackageID)
        {

            if (tab == 1)
            {
                SMS_SERVICE_PACKAGE sp = servicePackageBusiness.Find(servicePackageID);

                ViewData[SMSParentManageContractConstant.ListServicePackageAdd] = Session["servicePackage"];
                return PartialView("_Create");
            }
            else
            {
                ViewData[SMSParentManageContractConstant.ListServicePackageAdd] = servicePackageBusiness.GetListExtraServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
                EditViewModel objResult = GetContractDetailExtra(contractID, phone, servicePackageID);

                return PartialView("_Extra", objResult);
            }
        }

        /// <summary>
        /// Viethd31: Mua them tin nhan
        /// </summary>
        /// <param name="contractID"></param>
        /// <param name="phone"></param>
        /// <param name="servicePackageID"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SaveExtra(int classId, int contractID, int EwalletID, string phone, int servicePackageID, int extraServicePackageID, bool isAllClass, bool semester1, bool semester2, bool semester3)
        {
            List<SMS_PARENT_CONTRACT_EXTRA> lstSceInsert = new List<SMS_PARENT_CONTRACT_EXTRA>();
            SMS_PARENT_CONTRACT_EXTRA obj;
            SMS_SERVICE_PACKAGE sp = servicePackageBusiness.Find(extraServicePackageID);
            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            List<int> lstSemester = new List<int>();
            decimal amount = 0;

            if (semester3)
            {
                lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_ALL);
            }
            else
            {
                if (semester1 && this.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                }

                if (semester2)
                {
                    lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                }
            }

            if (lstSemester.Count == 0)
            {
                return Json(new { message = "Học kỳ không hợp lệ", type = GlobalConstants.TYPE_ERROR });
            }

            List<SMSParentContractBO> lstContract = smsParentContractBusiness.GetListSMSParentContractDetailForExtra(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classId, this.Year, 0, servicePackageID)
                .Where(o => lstSemester.Contains(o.SubscriptionTime.GetValueOrDefault())).ToList();

            List<SMSParentContractBO> lstContractGroup = (from c in lstContract
                                                          group c by new { c.Subcriber, c.SMSParentContractID, c.ServicePackageID }
                                                              into des
                                                          select new SMSParentContractBO
                                                          {
                                                              Subcriber = des.Key.Subcriber,
                                                              ServicePackageID = des.Key.ServicePackageID,
                                                              //SMSParentContractDetailID = des.Key.SMSParentContractDetailID,
                                                              SMSParentContractID = des.Key.SMSParentContractID,
                                                              ListSemester = des.Select(o => o.SubscriptionTime).ToList()
                                                          }).ToList();

            if (lstSemester.Count > 1)
            {
                lstContractGroup = lstContractGroup.Where(o => o.ListSemester.Contains(GlobalConstants.SEMESTER_OF_YEAR_FIRST) && o.ListSemester.Contains(GlobalConstants.SEMESTER_OF_YEAR_SECOND)).ToList();
            }

            if (!isAllClass)
            {

                lstContractGroup = lstContractGroup.Where(p => p.SMSParentContractID == contractID
                   && p.Subcriber == phone
                   && p.ServicePackageID == servicePackageID).ToList();

            }

            int quantity = 0;
            for (int i = 0; i < lstContractGroup.Count; i++)
            {
                SMSParentContractBO objContractGroup = lstContractGroup[i];
                List<SMSParentContractBO> lstContractEach = lstContract.Where(o => o.Subcriber == objContractGroup.Subcriber && o.SMSParentContractID == objContractGroup.SMSParentContractID).ToList();

                for (int j = 0; j < lstContractEach.Count; j++)
                {
                    SMSParentContractBO contractToExtra = lstContractEach[j];

                    obj = new SMS_PARENT_CONTRACT_EXTRA();
                    obj.CREATED_TIME = DateTime.Now;
                    obj.IS_ACTIVE = true;

                    if (contractToExtra.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        if (contractToExtra.Subcriber.CheckMobileNumberVT())
                        {
                            obj.AMOUNT = sp.VIETTEL_PRICE.HasValue ? sp.VIETTEL_PRICE.Value * 2 : 0;
                        }
                        else
                        {
                            obj.AMOUNT = sp.VIETTEL_PRICE.HasValue ? sp.OTHER_PRICE.Value * 2 : 0;
                        }
                    }
                    else
                    {
                        if (contractToExtra.Subcriber.CheckMobileNumberVT())
                        {
                            obj.AMOUNT = sp.VIETTEL_PRICE.HasValue ? sp.VIETTEL_PRICE.Value : 0;
                        }
                        else
                        {
                            obj.AMOUNT = sp.VIETTEL_PRICE.HasValue ? sp.OTHER_PRICE.Value : 0;
                        }
                    }
                    obj.SERVICE_PACKAGE_ID = extraServicePackageID;
                    obj.SMS_PARENT_CONTRACT_DETAIL_ID = contractToExtra.SMSParentContractDetailID;
                    obj.SMS_PARENT_CONTRACT_ID = contractToExtra.SMSParentContractID;
                    obj.SUB_SCRIPTION_TIME = contractToExtra.SubscriptionTime.Value;

                    lstSceInsert.Add(obj);
                    quantity++;

                    amount = amount + (contractToExtra.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL ? this.GetMoneyByServiceID(extraServicePackageID, contractToExtra.Subcriber, listService) * 2 :
                        this.GetMoneyByServiceID(extraServicePackageID, contractToExtra.Subcriber, listService));
                }
            }

            //viethd31: Fix loi kiem tra so tien tai khoan
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);
            if (ewalletObj.Balance < amount) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Tài khoản không đủ tiền để thanh toán!" });

            EW_EWALLET ewalletOBj = new EW_EWALLET();
            if (amount > 0)
            {
                ewalletOBj = ewalletBusiness.ChangeAmountSub(EwalletID, amount, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, quantity, GlobalConstantsEdu.SERVICE_TYPE_BUY_EXTRA);
            }

            smsParentContractExtraBusiness.SaveDetail(lstSceInsert, this.Year);
            //VTODO WriteLogInfo(DateTime.Now.ToString() + "-Loại giao dịch: GD thanh toán " + "-Nội dung: Mua thêm tin nhắn cho SĐT " + phone + ", số tiền " + amount + " VNĐ" + "-Số tiền: " + amount + "-Dịch vụ: SLLĐT", null, string.Empty, string.Empty, null, string.Empty);
            return Json(new { type = GlobalConstants.TYPE_SUCCESS, message = "Mua thêm gói cước thành công.", ewalletOBj = ewalletOBj });

        }

        public ActionResult Edit(int contractID, string phone, int servicePackageID)
        {
            if (!CheckPhoneNumber(phone))
            {
                return Json(new { message = Res.Get("blb_Message_Phone_Invalid"), type = GlobalConstants.TYPE_ERROR });
            }
            if (!smsParentContractDetailBusiness.CheckIDSMSParentContract(_globalInfo.SchoolID.Value, this.Year, contractID))
            {
                return Json(new { message = Res.Get("blb_Message_Phone_Invalid"), type = GlobalConstants.TYPE_ERROR });
            }
            EditViewModel objResult = GetContractDetailEdit(contractID, phone, servicePackageID);
            SMS_SERVICE_PACKAGE serviceBO = servicePackageBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == servicePackageID).FirstOrDefault();
            decimal? Price = 0;
            if (phone.CheckMobileNumberVT()) Price = serviceBO.VIETTEL_PRICE;
            else Price = serviceBO.OTHER_PRICE;

            if (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST && objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                objResult.Price = Price * 2;
            else if (objResult.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST || objResult.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                objResult.Price = Price;
            return Json(new { objResult });
        }

        public JsonResult LoadExtraMoney(int classId, int contractID, int servicePackageID, int extraServicePackageID, bool semester1, bool semester2, bool semester3)
        {
            List<SMS_PARENT_CONTRACT_EXTRA> lstSceInsert = new List<SMS_PARENT_CONTRACT_EXTRA>();
            SMS_PARENT_CONTRACT_EXTRA obj;
            SMS_SERVICE_PACKAGE sp = servicePackageBusiness.Find(extraServicePackageID);
            List<ServicePackageBO> listService = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            List<int> lstSemester = new List<int>();
            decimal amount = 0;

            if (semester3)
            {
                lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_ALL);
            }
            else
            {
                if (semester1 && this.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                }

                if (semester2)
                {
                    lstSemester.Add(GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                }
            }

            if (lstSemester.Count == 0)
            {
                return Json(new { message = "Học kỳ không hợp lệ", type = GlobalConstants.TYPE_ERROR });
            }

            List<SMSParentContractBO> lstContract = smsParentContractBusiness.GetListSMSParentContractDetailForExtra(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classId, this.Year, 0, servicePackageID)
                .Where(o => lstSemester.Contains(o.SubscriptionTime.GetValueOrDefault())).ToList();

            List<SMSParentContractBO> lstContractGroup = (from c in lstContract
                                                          group c by new { c.Subcriber, c.SMSParentContractID, c.ServicePackageID }
                                                              into des
                                                          select new SMSParentContractBO
                                                          {
                                                              Subcriber = des.Key.Subcriber,
                                                              ServicePackageID = des.Key.ServicePackageID,
                                                              //SMSParentContractDetailID = des.Key.SMSParentContractDetailID,
                                                              SMSParentContractID = des.Key.SMSParentContractID,
                                                              ListSemester = des.Select(o => o.SubscriptionTime).ToList()
                                                          }).ToList();

            if (lstSemester.Count > 1)
            {
                lstContractGroup = lstContractGroup.Where(o => o.ListSemester.Contains(GlobalConstants.SEMESTER_OF_YEAR_FIRST) && o.ListSemester.Contains(GlobalConstants.SEMESTER_OF_YEAR_SECOND)).ToList();
            }

            int vtCount = 0;
            int otherCount = 0;
            decimal priceVT = sp.VIETTEL_PRICE.HasValue ? lstSemester[0] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? sp.VIETTEL_PRICE.Value * 2 : sp.VIETTEL_PRICE.Value : 0;
            decimal priceOther = sp.OTHER_PRICE.HasValue ? lstSemester[0] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? sp.OTHER_PRICE.Value * 2 : sp.OTHER_PRICE.Value : 0;

            for (int i = 0; i < lstContractGroup.Count; i++)
            {
                SMSParentContractBO objContractGroup = lstContractGroup[i];
                List<SMSParentContractBO> lstContractEach = lstContract.Where(o => o.Subcriber == objContractGroup.Subcriber && o.SMSParentContractID == objContractGroup.SMSParentContractID).ToList();

                for (int j = 0; j < lstContractEach.Count; j++)
                {
                    SMSParentContractBO contractToExtra = lstContractEach[j];

                    obj = new SMS_PARENT_CONTRACT_EXTRA();
                    obj.CREATED_TIME = DateTime.Now;
                    obj.IS_ACTIVE = true;

                    if (contractToExtra.Subcriber.CheckMobileNumberVT())
                    {
                        vtCount++;
                    }
                    else
                    {
                        otherCount++;
                    }

                    amount = amount + (contractToExtra.SubscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL ? this.GetMoneyByServiceID(extraServicePackageID, contractToExtra.Subcriber, listService) * 2 :
                        this.GetMoneyByServiceID(extraServicePackageID, contractToExtra.Subcriber, listService));
                }
            }

            int pupilNum = lstContractGroup.Select(o => o.SMSParentContractID).Distinct().Count();
            string message = string.Empty;
            //if (vtCount > 0 && otherCount > 0)
            //{
            //    if (priceVT != priceOther)
            //    {
            //        message = string.Format("<span id='TotalPay'>{0} VNĐ x {1} + {2} VNĐ x {3} = {4} VNĐ</span><span style='padding-left:10px'>(Áp dụng cho {5} học sinh)</span>",
            //            priceVT.ToString("#,##0"), vtCount, priceOther.ToString("#,##0"), otherCount, amount.ToString("#,##0"), pupilNum);
            //    }
            //    else
            //    {
            //        message = string.Format("<span id='TotalPay'>{0} VNĐ x {1} = {2} VNĐ</span><span style='padding-left:10px'>(Áp dụng cho {3} học sinh)</span>",
            //           priceVT.ToString("#,##0"), vtCount + otherCount, amount.ToString("#,##0"), pupilNum);
            //    }
            //}
            //else if (vtCount > 0)
            //{
            //    message = string.Format("<span id='TotalPay'>{0} VNĐ x {1} = {2} VNĐ</span><span style='padding-left:10px'>(Áp dụng cho {3} học sinh)</span>",
            //            priceVT.ToString("#,##0"), vtCount, amount.ToString("#,##0"), pupilNum);
            //}
            //else
            //{
            //    message = string.Format("<span id='TotalPay'>{0} VNĐ x {1} = {2} VNĐ</span><span style='padding-left:10px'>(Áp dụng cho {3} học sinh)</span>",
            //           priceOther.ToString("#,##0"), otherCount, amount.ToString("#,##0"), pupilNum);
            //}

            message = string.Format("<span id='TotalPay'>{0} VNĐ</span><span style='padding-left:10px'>(Áp dụng cho {1} học sinh)</span>",
                       amount.ToString("#,##0").Replace(',', '.'), pupilNum);


            return Json(new JsonMessage(message));

        }

        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SaveEdit(EditViewModel objEdit)
        {
            int smsParentContactID = objEdit.SMSParentContractID;
            SMS_PARENT_CONTRACT objContract = smsParentContractBusiness.Find(objEdit.SMSParentContractID);
            string phoneOld = objEdit.HfSubscriber;
            string phoneNew = objEdit.Subscriber;
            int semester = 0;

            //viethd31: Fix loi dung firebug sua goi cuoc (khong duoc phep sua goi cuoc)
            SMS_PARENT_CONTRACT_DETAIL contractDetail = smsParentContractDetailBusiness.All.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_ID == objEdit.SMSParentContractID && o.YEAR_ID == this.Year && o.IS_ACTIVE && o.SUBSCRIBER == objEdit.HfSubscriber && o.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL);
            objEdit.ServicePackageID = contractDetail.SERVICE_PACKAGE_ID;

            SMS_SERVICE_PACKAGE serviceBO = servicePackageBusiness.All.Where(p => p.SERVICE_PACKAGE_ID == objEdit.ServicePackageID).FirstOrDefault();
            //Danh sach hoc sinh da co hop dong 
            SMSParentContractDetailBO objParentContract = smsParentContractBusiness.GetListSMSParentContractByShoolID(_globalInfo.SchoolID.Value, this.Year).Where(p => p.SMSParentContractID == smsParentContactID && p.SubscriptionStatus != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL).FirstOrDefault();

            if (serviceBO.SERVICE_CODE.Equals("SMSFree") && !objParentContract.ServiceCode.Equals("SMSFree"))
            {
                return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_NotRegisterSMSFree") });
            }
            if (!this.CheckPhoneNumber(phoneOld) || !this.CheckPhoneNumber(phoneNew)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("blb_Message_Phone_Invalid") });
            if (!smsParentContractDetailBusiness.CheckIDSMSParentContract(_globalInfo.SchoolID.Value, this.Year, smsParentContactID)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Lưu không thành công, hãy thực hiện lại." });
            if (objEdit.StatusPupil != GlobalConstantsEdu.COMMON_PUPIL_STATUS_STUDYING) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Trạng thái học sinh không hợp lệ, lưu không thành công" });

            // Anhvd9 20160113 - Kiểm tra thay đổi thời gian đăng ký HK khi đã quá số ngày cho phép
            int numDaysAllowChange = 30;
            if (!string.IsNullOrEmpty(this.GetString("IsLimitRegisChangeFirstSemester"))) numDaysAllowChange = this.GetInt("IsLimitRegisChangeFirstSemester");

            bool NotAllowChange = smsParentContractDetailBusiness.All.Where(o => o.SMS_PARENT_CONTRACT_ID == smsParentContactID && o.YEAR_ID == this.Year && o.SUBSCRIBER == phoneOld
               && o.IS_ACTIVE && (o.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST || o.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL)
               && EntityFunctions.DiffDays(o.CREATED_TIME, DateTime.Now) > numDaysAllowChange).Count() > 0;
            if ((NotAllowChange && objEdit.Semester1 != GlobalConstants.SEMESTER_OF_YEAR_FIRST && objEdit.UsedSemester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                (this.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && objEdit.UsedSemester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST && objEdit.Semester1 != GlobalConstants.SEMESTER_OF_YEAR_FIRST))
                return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Thời gian đăng ký học kỳ 1 không được phép thay đổi!" });
            // End - Anhvd9 20160113

            // 20160119 Anhvd9 - Không cho gia hạn các gói đã khoá
            List<ServicePackageBO> servicePackageList = servicePackageBusiness.GetListServicesPackage(this.Year, this.Semester, this.AcademicYear, _globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
            if (!servicePackageList.Any(o => o.ServicePackageID == objEdit.ServicePackageID) &&
                ((objEdit.UsedSemester1 != GlobalConstants.SEMESTER_OF_YEAR_FIRST && objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                || (objEdit.UsedSemester2 != GlobalConstants.SEMESTER_OF_YEAR_SECOND && objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)))
                return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Gói cước đã hết thời gian áp dụng!" });
            // End 20160119 Anhvd9

            // 20151119 - AnhVD9 - Kiem tra goi cuoc chi duoc dau noi mang
            ServicePackageBO servicePackage = servicePackageList.FirstOrDefault(o => o.ServicePackageID == objEdit.ServicePackageID);
            if (servicePackage != null && servicePackage.IsForOnlyInternal == true && !phoneNew.CheckMobileNumberVT()) return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyInternalNetwork") });
            if (servicePackage != null && servicePackage.IsForOnlyExternal == true && phoneNew.CheckMobileNumberVT()) return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("Validate_ServicesPackage_OnlyExternalNetwork") });
            // End 20151119

            // 20160219 Anhvd9 - Kiểm tra đã có thuê bao thứ nhất Đang hoạt động và phát sinh doanh thu khi Thêm thuê bao phụ
            //if (servicePackage != null && servicePackage.IsAllowRegisterSideSubscriber == true)
            //{
            //    if (objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            //    {
            //        if (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(smsParentContactID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST))
            //            return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Chưa đủ điều kiện áp dụng cho gói cước này!" });
            //    }

            //    if (objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            //    {
            //        if (!smsParentContractDetailBusiness.CheckExistFirstSubscriber(smsParentContactID, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
            //            return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Chưa đủ điều kiện áp dụng cho gói cước này!" });
            //    }
            //}
            // End 20160219
            List<SMS_PARENT_CONTRACT_DETAIL> updateList = new List<SMS_PARENT_CONTRACT_DETAIL>();
            List<SMS_PARENT_CONTRACT_DETAIL> insertList = new List<SMS_PARENT_CONTRACT_DETAIL>();
            SMS_PARENT_CONTRACT_DETAIL objUpdate = null;
            if (objEdit != null)
            {
                if (objEdit.Semester3 == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {
                    if (!phoneNew.Equals(phoneOld))
                    {
                        List<int> lstExistSemester = new List<int>();
                        if (this.checkSubscriptionTime(phoneNew, objEdit.Semester3, objEdit.SMSParentContractID, lstExistSemester))
                        {
                            string strSemester = string.Empty;
                            for (int i = 0; i < lstExistSemester.Count; i++)
                            {
                                strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                                if (i < lstExistSemester.Count - 1)
                                {
                                    strSemester += ", ";
                                }

                            }
                            return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, strSemester) });
                        }

                    }
                    else
                    {
                        if (this.checkSubscriptionTimeUpdate(phoneNew, objEdit.Semester3, objEdit.SMSParentContractID)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, objEdit.Semester3) });

                    }

                    objUpdate = new SMS_PARENT_CONTRACT_DETAIL();
                    objUpdate.SMS_PARENT_CONTRACT_ID = objEdit.SMSParentContractID;
                    objUpdate.PARTITION_ID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);

                    objUpdate.RELATION_SHIP = objEdit.Relationship;
                    objUpdate.RECEIVER_NAME = objEdit.ReceiverName;
                    objUpdate.SUBSCRIBER = objEdit.Subscriber.ExtensionMobile();
                    objUpdate.SUBSCRIBER_TYPE = objUpdate.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                    objUpdate.SERVICE_PACKAGE_ID = objEdit.ServicePackageID.Value;
                    objUpdate.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                    objUpdate.SUBSCRIPTION_STATUS = objEdit.SubscriptionStatus;
                    objUpdate.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                    objUpdate.AMOUNT = (int)objEdit.Money;
                    objUpdate.SCHOOL_ID = _globalInfo.SchoolID.Value;
                    objUpdate.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                    objUpdate.CLASS_ID = objContract.CLASS_ID;
                    objUpdate.PUPIL_ID = objContract.PUPIL_ID;
                    updateList.Add(objUpdate);
                }
                else if (objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST || objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        if (!phoneNew.Equals(phoneOld))
                        {
                            List<int> lstExistSemester = new List<int>();
                            if (this.checkSubscriptionTime(phoneNew, semester, objEdit.SMSParentContractID, lstExistSemester))
                            {
                                string strSemester = string.Empty;
                                for (int i = 0; i < lstExistSemester.Count; i++)
                                {
                                    strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                                    if (i < lstExistSemester.Count - 1)
                                    {
                                        strSemester += ", ";
                                    }


                                }
                                return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, strSemester) });
                            }
                        }
                        else
                        {
                            if (this.checkSubscriptionTimeUpdate(phoneNew, semester, objEdit.SMSParentContractID)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, semester) });
                        }

                        objUpdate = new SMS_PARENT_CONTRACT_DETAIL();
                        objUpdate.SMS_PARENT_CONTRACT_ID = objEdit.SMSParentContractID;
                        objUpdate.PARTITION_ID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                        objUpdate.RELATION_SHIP = objEdit.Relationship;
                        objUpdate.RECEIVER_NAME = objEdit.ReceiverName;
                        objUpdate.SUBSCRIBER = objEdit.Subscriber.ExtensionMobile();
                        objUpdate.SUBSCRIBER_TYPE = objUpdate.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                        objUpdate.SERVICE_PACKAGE_ID = objEdit.ServicePackageID.Value;
                        objUpdate.SUBSCRIPTION_TIME = objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? GlobalConstants.SEMESTER_OF_YEAR_FIRST : GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        objUpdate.SUBSCRIPTION_STATUS = objEdit.SubscriptionStatus;
                        objUpdate.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                        objUpdate.AMOUNT = (int)objEdit.Money;
                        objUpdate.SCHOOL_ID = _globalInfo.SchoolID.Value;
                        objUpdate.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                        objUpdate.CLASS_ID = objContract.CLASS_ID;
                        objUpdate.PUPIL_ID = objContract.PUPIL_ID;
                        updateList.Add(objUpdate);
                    }

                    if (objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        if (!phoneNew.Equals(phoneOld))
                        {
                            List<int> lstExistSemester = new List<int>();
                            if (this.checkSubscriptionTime(phoneNew, semester, objEdit.SMSParentContractID, lstExistSemester))
                            {
                                string strSemester = string.Empty;
                                for (int i = 0; i < lstExistSemester.Count; i++)
                                {
                                    strSemester = (lstExistSemester[i] == GlobalConstants.SEMESTER_OF_YEAR_ALL ? "Cả năm" : lstExistSemester[i].ToString());
                                    if (i < lstExistSemester.Count - 1)
                                    {
                                        strSemester += ", ";
                                    }


                                }
                                return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, strSemester) });
                            }
                        }
                        else
                        {
                            if (this.checkSubscriptionTimeUpdate(phoneNew, semester, objEdit.SMSParentContractID)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = string.Format(Res.Get("Validate_SubscriptionTime_Duplicate"), objEdit.PupilName, semester) });
                        }

                        objUpdate = new SMS_PARENT_CONTRACT_DETAIL();
                        objUpdate.SMS_PARENT_CONTRACT_ID = objEdit.SMSParentContractID;
                        objUpdate.PARTITION_ID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                        objUpdate.RELATION_SHIP = objEdit.Relationship;
                        objUpdate.RECEIVER_NAME = objEdit.ReceiverName;
                        objUpdate.SUBSCRIBER = objEdit.Subscriber.ExtensionMobile();
                        objUpdate.SUBSCRIBER_TYPE = objUpdate.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                        objUpdate.SERVICE_PACKAGE_ID = objEdit.ServicePackageID.Value;
                        objUpdate.SUBSCRIPTION_TIME = semester;
                        objUpdate.SUBSCRIPTION_STATUS = objEdit.SubscriptionStatus;
                        objUpdate.REGISTRATION_TYPE = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
                        objUpdate.AMOUNT = (int)objEdit.Money;
                        objUpdate.SCHOOL_ID = _globalInfo.SchoolID.Value;
                        objUpdate.ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value;
                        objUpdate.CLASS_ID = objContract.CLASS_ID;
                        objUpdate.PUPIL_ID = objContract.PUPIL_ID;
                        updateList.Add(objUpdate);
                    }
                }
                // Anhvd9 20160113 - Tính số tiền trừ chênh lệch
                // Tru tien chenh lech (KHONG tru tien voi cac goi dang No cuoc) - Đv các HĐ Nợ cước vẫn cho sửa Gói cước, Số ĐT
                if (objEdit.SubscriptionStatus < GlobalConstantsEdu.STATUS_UNPAID)
                {
                    decimal payMoney = calculatePriceDifference(objEdit);
                    if (payMoney > 0)
                    {
                        if (objEdit.Money != payMoney) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Thanh toán không hợp lệ!" });
                        else
                        {
                            if (objEdit.SubscriptionStatus == GlobalConstantsEdu.STATUS_REGISTED)
                            {
                                EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);
                                if (ewalletObj.Balance < objEdit.Money) return Json(new { type = GlobalConstants.TYPE_ERROR, message = "Tài khoản không đủ tiền để thanh toán!" });
                                ewalletBusiness.ChangeAmountSub(ewalletObj.EWalletID, objEdit.Money, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, 1, GlobalConstantsEdu.SERVICE_TYPE_REGIS_CONTRACT);
                            }
                        }
                    }
                }
                // End 20160113
                this.smsParentContractDetailBusiness.SaveEditDetail(smsParentContactID, phoneOld, phoneNew, this.Year, updateList);
                //VTODO WriteLogInfo(DateTime.Now.ToString() + "-Loại giao dịch: GD thanh toán " + "-Nội dung: Đăng ký dịch vụ SLLĐT cho SĐT " + phoneNew + ", số tiền " + objEdit.Money + " VNĐ" + "-Số tiền: " + objEdit.Money + "-Dịch vụ: SLLĐT", null, string.Empty, string.Empty, null, string.Empty);
                logger.Info("aaaaaaaaaaaaaaaaaaa");
                return Json(new { message = "Cập nhật thành công.", type = GlobalConstants.TYPE_SUCCESS });
            }
            return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("SendSMS_Label_CoreExceptionError") });
        }

        public JsonResult AjaxCheckMobile(string phone)
        {
            int isVT = 0;
            try
            {
                if (string.IsNullOrEmpty(phone)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("blb_Message_Phone_Invalid") });
                if (!CheckPhoneNumber(phone)) return Json(new { type = GlobalConstants.TYPE_ERROR, message = Res.Get("blb_Message_Phone_Invalid") });

                if (phone.CheckMobileNumberVT()) isVT = 1;
            }
            catch (Exception ex)
            {
                //VTODO
                //
                string para = string.Format("phone={0}", phone);
                LogExtensions.ErrorExt(logger, DateTime.Now, "AjaxCheckMobile", para, ex);
                isVT = -1;
            }
            return Json(new { type = GlobalConstants.TYPE_SUCCESS, isVT = isVT });
        }

        public JsonResult GetEwallet()
        {
            EwalletBO ewalletObj = ewalletBusiness.GetEWallet(_globalInfo.SchoolID.Value, true, false);
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Balance = ewalletObj.Balance, EWalletID = ewalletObj.EWalletID });
        }

        public ActionResult OpenUpdateContractDialog(int contractID, string phone, int servicePackageID, int pupilID, string serviceCode,
             int classID, int ContractDetailID, bool IsLimitPackage, bool IsWholeYearPackage, int isRegisSMS, int status)
        {
            PupilProfile pp = PupilProfileBusiness.Find(pupilID);
            ClassProfile cp = ClassProfileBusiness.Find(classID);
            string pupilName = pp != null ? pp.FullName : string.Empty;
            string className = cp != null ? cp.DisplayName : string.Empty;

            ViewData[SMSParentManageContractConstant.CONTRACT_ID] = contractID;
            ViewData[SMSParentManageContractConstant.PHONE] = phone;
            ViewData[SMSParentManageContractConstant.SERVICE_PACKAGE_ID] = servicePackageID;
            ViewData[SMSParentManageContractConstant.PUPILFILE_ID] = pupilID;
            ViewData[SMSParentManageContractConstant.PupilName] = pupilName;
            ViewData[SMSParentManageContractConstant.SERVICE_PACKAGE_CODE] = serviceCode;
            ViewData[SMSParentManageContractConstant.ClassID] = classID;
            ViewData[SMSParentManageContractConstant.CLASSNAME] = className;
            ViewData[SMSParentManageContractConstant.CONTRACT_DETAIL_ID] = ContractDetailID;
            ViewData[SMSParentManageContractConstant.IS_LIMIT] = IsLimitPackage;
            ViewData[SMSParentManageContractConstant.IS_WHOLE_YEAR] = IsWholeYearPackage;
            ViewData[SMSParentManageContractConstant.IS_REGIS_SMS] = isRegisSMS;
            ViewData[SMSParentManageContractConstant.Status] = status;
            return PartialView("_UpdateContractDialog");
        }
        #endregion

        #region Cac ham ho tro xu ly
        private bool CheckPhoneNumber(string phone)
        {
            /**
             * 
             * Số điện thoại: Bắt đầu bằng 0 hoặc 84
             * Có thể ko có 0 hoặc 84 thì bắt đầu bằng 9 hoặc 1 hoặc 8
             * Chiều dài từ 8 (9.xx000000) - 10 (84.xxx.xxxx.xxx) - 08x.xxxx.xxx
            */
            string pattern = @"^([01984]{1,2})(\d{8,10})$";
            bool isChecked = false;
            if (Regex.IsMatch(phone, pattern) && (phone.CheckMobileNumberVT() || phone.CheckMobileNumber())) return true;
            return isChecked;
        }

        /// <summary>
        /// lấy giá tiền theo gói cước
        /// </summary>
        /// <param name="servicePackageID"></param>
        /// <param name="phone"></param>
        /// <param name="serviceList"></param>
        /// <returns></returns>
        private decimal GetMoneyByServiceID(int servicePackageID, string phone, List<ServicePackageBO> serviceList)
        {
            decimal? money = 0;
            ServicePackageBO serviceBO = serviceList.Where(p => p.ServicePackageID == servicePackageID).FirstOrDefault();
            if (phone.CheckMobileNumberVT()) money = serviceBO.ViettelPrice;
            else if (phone.CheckMobileNumber()) money = serviceBO.OtherPrice;
            return money.Value;
        }

        /// <summary>
        /// Cập nhật thông tin HS đã đăng ký
        /// </summary>
        private void UpdatePupilOfClass()
        {
            bool result = false;
            try
            {
                List<int> listPupilOfClassID = new List<int>();
                if (Session[SMSParentManageContractConstant.LIST_SYNC_SMAS3] != null)
                {
                    listPupilOfClassID = (List<int>)Session[SMSParentManageContractConstant.LIST_SYNC_SMAS3];
                }
                result = PupilProfileBusiness.UpdateRegisterContractPupil(listPupilOfClassID);
                // remove after success
                Session.Remove(SMSParentManageContractConstant.LIST_SYNC_SMAS3);
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdatePupilOfClass", "", ex);
            }
        }

        private bool checkSubscriptionTime(string phone, int subscriptionTime, int contractID, List<int> existSemesters)
        {
            bool ret = false;
            if (subscriptionTime == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                if (smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, GlobalConstants.SEMESTER_OF_YEAR_ALL, contractID, true).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 0)
                {
                    existSemesters.Add(GlobalConstants.SEMESTER_OF_YEAR_ALL);
                    ret = true;
                }

                if (smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, GlobalConstants.SEMESTER_OF_YEAR_FIRST, contractID, true).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 0)
                {
                    existSemesters.Add(GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                    ret = true;
                }

                if (smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, GlobalConstants.SEMESTER_OF_YEAR_SECOND, contractID, true).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 0)
                {
                    existSemesters.Add(GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                    ret = true;
                }
            }
            else
            {
                if (smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, subscriptionTime, contractID, true).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 0)
                {
                    existSemesters.Add(subscriptionTime);
                    ret = true;
                }

                if (smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, GlobalConstants.SEMESTER_OF_YEAR_ALL, contractID, true).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 0)
                {
                    existSemesters.Add(GlobalConstants.SEMESTER_OF_YEAR_ALL);
                    ret = true;
                }
            }

            return ret;
        }

        private bool checkSubscriptionTimeUpdate(string phone, int subscriptionTime, int contractID)
        {
            return smsParentContractBusiness.getListContractDetailByContractID(_globalInfo.SchoolID.Value, this.Year, subscriptionTime, contractID).Where(p => p.SUBSCRIBER.SubstringPhone() == phone.SubstringPhone()).ToList().Count > 1;
        }

        /// <summary>
        /// Tính toán tiền chênh lệch khi cập nhật thuê bao
        /// </summary>
        /// <author date="14/01/2016">Anhvd9</author>
        /// <returns></returns>
        private decimal calculatePriceDifference(EditViewModel objEdit)
        {
            decimal newPrice = 0;
            decimal oldPrice = 0;
            SMS_SERVICE_PACKAGE servicePackage = servicePackageBusiness.Find(objEdit.ServicePackageID);
            if (objEdit.Subscriber.CheckMobileNumberVT())
            {
                if (objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST) newPrice += servicePackage.VIETTEL_PRICE.Value;
                if (objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND) newPrice += servicePackage.VIETTEL_PRICE.Value;
            }
            else
            {
                if (objEdit.Semester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST) newPrice += servicePackage.OTHER_PRICE.Value;
                if (objEdit.Semester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND) newPrice += servicePackage.OTHER_PRICE.Value;
            }
            int semester = 0;
            if (objEdit.UsedSemester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST) semester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            if (objEdit.UsedSemester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND) semester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            if (objEdit.UsedSemester1 == GlobalConstants.SEMESTER_OF_YEAR_FIRST && objEdit.UsedSemester2 == GlobalConstants.SEMESTER_OF_YEAR_SECOND) semester = GlobalConstants.SEMESTER_OF_YEAR_ALL;

            List<SMS_PARENT_CONTRACT_DETAIL> lstContractDetail = smsParentContractDetailBusiness.All.Where(o => o.SMS_PARENT_CONTRACT_ID == objEdit.SMSParentContractID && o.YEAR_ID == this.Year && o.IS_ACTIVE && o.SUBSCRIBER == objEdit.HfSubscriber &&
                                                ((semester < GlobalConstants.SEMESTER_OF_YEAR_ALL && o.SUBSCRIPTION_TIME == semester) || semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)).ToList();
            foreach (SMS_PARENT_CONTRACT_DETAIL item in lstContractDetail)
            {
                servicePackage = servicePackageBusiness.Find(item.SERVICE_PACKAGE_ID);
                if (item.SUBSCRIBER.CheckMobileNumberVT())
                {
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST) oldPrice += servicePackage.VIETTEL_PRICE.Value;
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND) oldPrice += servicePackage.VIETTEL_PRICE.Value;
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL) oldPrice += 2 * servicePackage.VIETTEL_PRICE.Value;
                }
                else
                {
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST) oldPrice += servicePackage.OTHER_PRICE.Value;
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND) oldPrice += servicePackage.OTHER_PRICE.Value;
                    if (item.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_ALL) oldPrice += 2 * servicePackage.OTHER_PRICE.Value;
                }
            }
            return newPrice - oldPrice;
        }



        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        private string GetString(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        private int GetInt(string key)
        {
            return Convert.ToInt32(WebConfigurationManager.AppSettings[key]);
        }


        public async Task SaveFileContentsAsync(string filePath, Stream stream)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await stream.CopyToAsync(fileStream);
            }
        }
        #endregion
        private class objServiceCode
        {
            public int PupilID { get; set; }
            public string ServiceCode { get; set; }
            public int SemesterID { get; set; }
            public decimal? VIETTEL_PRICE { get; set; }
            public decimal? OTHER_PRICE { get; set; }
        }
    }
}