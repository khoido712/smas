﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Telerik.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMS_SCHOOL.Utility.Common;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Areas.SMSEduArea.Models;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class DeclarePackageController : BaseController
    {
        private readonly IServicePackageBusiness ServicePackageBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IServicePackageSchoolDetailBusiness ServicePackageSchoolDetailBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public DeclarePackageController(IServicePackageBusiness ServicePackageBusiness,
            IProvinceBusiness ProvinceBusiness,
            IDistrictBusiness DistrictBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IServicePackageSchoolDetailBusiness ServicePackageSchoolDetailBusiness,
            IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ServicePackageBusiness = ServicePackageBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ServicePackageSchoolDetailBusiness = ServicePackageSchoolDetailBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        private static int Total = 0;

        #region index/setdata
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData(bool isEdit = false)
        {
            //Năm học
            List<int> lstYear = new List<int>();
            lstYear = AcademicYearBusiness.All.Select(p => p.Year).Distinct().ToList();
            lstYear = lstYear.Where(o => o < DateTime.Now.Year + 2 && o >= 2012).OrderBy(o => o).ToList();
            List<SelectListItem> listAcademicYear = new List<SelectListItem>();
            if (lstYear != null && lstYear.Count > 0)
            {
                for (int i = lstYear.Count - 1; i >= 0; i--)
                {
                    listAcademicYear.Add(new SelectListItem { Text = lstYear[i] + " - " + (lstYear[i] + 1).ToString(), Value = lstYear[i].ToString() });
                }
            }
            ViewData[DeclarePackageConstant.LIST_YEAR] = listAcademicYear;

            // List package on combobox
            List<ServicePackageBO> lstSearchPackage = ServicePackageBusiness.GetAllPackage();
            ViewData[DeclarePackageConstant.LIST_PACKAGE] = lstSearchPackage;

            if (!isEdit)
            {
                ServicePackageBO firstSP = new ServicePackageBO();
                if (lstSearchPackage.Count > 0)
                {
                    firstSP = lstSearchPackage.FirstOrDefault();
                    ViewData[DeclarePackageConstant.INFO_SERVICE_PACKAGE] = firstSP;
                }

                ViewData[DeclarePackageConstant.NUM_OF_SCHOOL] = ServicePackageSchoolDetailBusiness.CountSchoolApply(firstSP.ServicePackageID, 1);
            }

            // Danh sach Quan/Huyen
            PrepareData();
        }

        public JsonResult GetPackageInfo(string ServiceCode)
        {
            ServicePackageBO objServicepackageBO = ServicePackageBusiness.SearchDetailPackage(ServiceCode, this.Year);

            return Json(new { data = objServicepackageBO, num = ServicePackageSchoolDetailBusiness.CountSchoolApply(objServicepackageBO.ServicePackageID, objServicepackageBO.ApplyType.GetValueOrDefault()) });
        }

        public JsonResult GetPackageInfoEdit(string ServiceCode, int Year)
        {
            ServicePackageBO objServicepackageBO = ServicePackageBusiness.SearchDetailPackage(ServiceCode, Year);

            return Json(new { data = objServicepackageBO, num = ServicePackageSchoolDetailBusiness.CountSchoolApply(objServicepackageBO.ServicePackageID, objServicepackageBO.ApplyType.GetValueOrDefault()) });
        }

        #endregion

        #region Tìm kiếm
        public ActionResult Search(FormCollection frm)
        {
            int? AcademicYear = 0;
            string ServiceCode = string.Empty;
            int? ApplyType = 0;

            if (frm["CboAcademicYear"] != string.Empty && frm["CboAcademicYear"] != null)
            {
                AcademicYear = Convert.ToInt32(frm["CboAcademicYear"]);
            }

            if (frm["CboPackage"] != string.Empty && frm["CboPackage"] != null)
            {
                ServiceCode = frm["CboPackage"].Trim();
            }

            if (frm["ApplyType"] != string.Empty && frm["ApplyType"] != null)
            {
                ApplyType = Convert.ToInt32(frm["ApplyType"]);
            }

            List<ServicePackageBO> listServicePackage = _SearchPackage(AcademicYear, ServiceCode, ApplyType);


            ViewData[DeclarePackageConstant.LIST_SERVICE_PACKAGE] = listServicePackage;
            return PartialView("_GridPackage");
        }
        public List<ServicePackageBO> _SearchPackage(int? AcademicYear, string ServiceCode, int? ApplyType)
        {
            List<ServicePackageBO> listService = ServicePackageBusiness.GetGridServicePackage(AcademicYear, ServiceCode, ApplyType)
                                .OrderBy(p => p.Year).ThenBy(p => p.ServiceCode).ThenByDescending(p => p.UpdatedTime).ToList();
            return listService;
        }
        #endregion Search

        #region Khai bao goi cuoc
        /// <summary>
        /// Them chi tiet cua goi cuoc
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection frm, string ListProvinceApply, string ListSchoolApply, string ListSchoolNotApply,
            int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status, int ServicePackageID, bool? CheckAllSchool)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;
            int? Effectiveday = null;
            int Year = Convert.ToInt32(frm["CboAcademicYear"]);
            string PackageCode = frm["CboPackage"].Trim();
            string Semester_I = frm["checkI"];
            string Semester_II = frm["checkII"];
            string frdate = frm["FromDate"];
            string todate = frm["ToDate"];
            string EffectiveDay = frm["EffectiveDay"];
            string ApplyType = frm["ApplyType"];
            List<int> lstAppliedProvinceID = GetIDsFromString(ListProvinceApply);
            List<int> LstSchoolIDCheck = GetIDsFromString(ListSchoolApply);
            List<int> LstSchoolIDUnCheck = GetIDsFromString(ListSchoolNotApply);
            if (!string.IsNullOrEmpty(frdate))
            {
                FromDate = ConvertDatetime(frdate);
            }

            if (!string.IsNullOrEmpty(todate))
            {
                ToDate = ConvertDatetime(todate);
            }

            int numDay = 0;
            if (!string.IsNullOrEmpty(EffectiveDay) && Int32.TryParse(EffectiveDay, out numDay)) Effectiveday = numDay;

            ServicePackageBO servicepack = new ServicePackageBO();
            TryUpdateModel(servicepack);

            servicepack.ServiceCode = PackageCode;
            servicepack.Description = PackageCode;
            servicepack.Year = Year;
            servicepack.FromDate = FromDate;
            servicepack.ToDate = ToDate;
            servicepack.EffectiveDays = Effectiveday;
            servicepack.ApplyType = Int32.Parse(ApplyType);
            servicepack.ListAppliedProvince = lstAppliedProvinceID;
            //viethd31 Bo sung cau hinh nam hoc 2017-2018
            servicepack.IsForOnlyInternal = !string.IsNullOrEmpty(frm["IsForOnlyInternal"]);
            servicepack.IsForOnlyExternal = !string.IsNullOrEmpty(frm["IsForOnlyExternal"]);
            servicepack.IsExtraPackage = !string.IsNullOrEmpty(frm["IsExtraPackage"]);
            servicepack.IsWholeYearPackage = !string.IsNullOrEmpty(frm["IsWholeYearPackage"]);

            if (!string.IsNullOrEmpty(Semester_I) && !string.IsNullOrEmpty(Semester_II))
            {
                servicepack.Semester = DeclarePackageConstant.SEMESTER_ALL;
            }
            else
            {
                if (!string.IsNullOrEmpty(Semester_I))
                {
                    servicepack.Semester = DeclarePackageConstant.SEMESTER_I;
                }
                else
                {
                    servicepack.Semester = DeclarePackageConstant.SEMESTER_II;
                }
            }

            if (Effectiveday > 0)
            {
                servicepack.IsLimit = true;
            }

            if (servicepack.ApplyType == 1)
            {
                if (CheckAllSchool == true)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ProvinceID"] = ProvinceID;
                    dic["DistrictID"] = DistrictID;
                    dic["SchoolName"] = SchoolName;
                    dic["SchoolUserName"] = UserName;
                    dic["Status"] = Status;
                    dic["ServicePackageID"] = ServicePackageID;
                    ServicePackageSchoolDetailBusiness.Apply(dic);

                }
                else
                {
                    ServicePackageSchoolDetailBusiness.Apply(ServicePackageID, LstSchoolIDCheck, LstSchoolIDUnCheck);
                }
            }

            if (ServicePackageBusiness.InsertServicePackageDetail(servicepack))
            {
                // Ghi log 
                //WriteLogInfo("Thêm mới gói cước thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Thêm mới gói cước thành công");
                return Json(new JsonMessage("Thêm mới gói cước thành công", GlobalConstants.TYPE_SUCCESS));
            }
            else
            {
                return Json(new JsonMessage("Gói cước đã được khai báo, vui lòng kiểm tra lại", GlobalConstants.TYPE_ERROR));
            }
        }

        /// <summary>
        /// Them goi cuoc
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult CreatePackage(FormCollection frm)
        {
            string ServiceCode = frm["ServicePackageCode"];
            int CountSMS = Convert.ToInt32(frm["CountMessage"]);
            decimal ViettelPrice = Convert.ToDecimal(frm["PriceIn"]);
            decimal OtherPrice = Convert.ToDecimal(frm["PriceOut"]);
            decimal PricePHHS = Convert.ToDecimal(frm["PricePHHS"]);

            decimal tmp = 0;

            decimal? InternalSharingRatio = null;
            decimal? ExternalSharingRatio = null;
            if (frm["InternalSharingRatio"] != null && decimal.TryParse(frm["InternalSharingRatio"], out tmp))
                InternalSharingRatio = tmp / 100;
            if (frm["ExternalSharingRatio"] != null && decimal.TryParse(frm["ExternalSharingRatio"], out tmp))
                ExternalSharingRatio = tmp / 100;

            bool isFreePackage = !string.IsNullOrEmpty(frm["IsFreePackage"]);

            ServicePackageBO objSerPac = new ServicePackageBO();
            TryUpdateModel(objSerPac);

            objSerPac.ServiceCode = ServiceCode;
            objSerPac.Description = ServiceCode;
            objSerPac.Createdtime = DateTime.Now;
            objSerPac.ViettelPrice = ViettelPrice;
            objSerPac.OtherPrice = OtherPrice;
            objSerPac.PriceForCustomer = PricePHHS;
            //objSerPac.IsLimitSchool = true; // mặc định ko có trường
            objSerPac.IsActive = true;
            //objSerPac.Indefinitely = true; // Mac dinh goi vo thoi han
            objSerPac.IsLimit = false; // Mac dinh KO gioi han
            objSerPac.MaxSMS = CountSMS;
            objSerPac.Type = DeclarePackageConstant.TYPE;
            objSerPac.IsFreePackage = isFreePackage;

            objSerPac.InternalSharingRatio = InternalSharingRatio;
            objSerPac.ExternalSharingRatio = ExternalSharingRatio;

            if (ServicePackageBusiness.InsertServicePackage(objSerPac))
            {
                //WriteLogInfo("Thêm mới định nghĩa gói cước thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Thêm mới định nghĩa gói cước thành công");
                return Json(new JsonMessage("Thêm mới định nghĩa gói cước thành công", GlobalConstants.TYPE_SUCCESS));
            }
            else
            {
                return Json(new JsonMessage("Tên gói cước đã tồn tại", GlobalConstants.TYPE_ERROR));
            }
        }
        #endregion

        #region Sua goi cuoc
        /// <summary>
        /// Cap nhat goi cuoc
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult EditPackage(FormCollection frm)
        {
            int ServicePackageID = Convert.ToInt32(frm["ServicePackageID"]);
            int ServicePackageDetailID = Convert.ToInt32(frm["ServicePackageDetailID"]);
            string ServiceCode = frm["ServicePackageCode"];
            int CountSMS = Convert.ToInt32(frm["CountMessage"]);
            decimal ViettelPrice = Convert.ToDecimal(frm["PriceIn"]);
            decimal OtherPrice = Convert.ToDecimal(frm["PriceOut"]);
            decimal PricePHHS = Convert.ToDecimal(frm["PricePHHS"]);

            decimal tmp = 0;

            decimal? InternalSharingRatio = null;
            decimal? ExternalSharingRatio = null;
            if (frm["InternalSharingRatio"] != null && decimal.TryParse(frm["InternalSharingRatio"], out tmp))
                InternalSharingRatio = tmp / 100;
            if (frm["ExternalSharingRatio"] != null && decimal.TryParse(frm["ExternalSharingRatio"], out tmp))
                ExternalSharingRatio = tmp / 100;

            bool isFreePackage = !string.IsNullOrEmpty(frm["IsFreePackage"]);

            ServicePackageBO objSerPac = new ServicePackageBO();
            TryUpdateModel(objSerPac);

            objSerPac.ServicePackageID = ServicePackageID;
            objSerPac.ServicePackageDetailID = ServicePackageDetailID;
            objSerPac.ServiceCode = ServiceCode;
            objSerPac.Description = ServiceCode;
            objSerPac.ViettelPrice = ViettelPrice;
            objSerPac.OtherPrice = OtherPrice;
            objSerPac.PriceForCustomer = PricePHHS;
            objSerPac.MaxSMS = CountSMS;
            objSerPac.IsFreePackage = isFreePackage;

            objSerPac.InternalSharingRatio = InternalSharingRatio;
            objSerPac.ExternalSharingRatio = ExternalSharingRatio;
            if (ServicePackageBusiness.UpdateServicePackage(objSerPac))
            {
                // Ghi log 
                //WriteLogInfo("Cập nhật định nghĩa gói cước thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Cập nhật định nghĩa gói cước thành công");
                return Json(new JsonMessage("Cập nhật định nghĩa gói cước thành công", GlobalConstants.TYPE_SUCCESS));
            }
            else
            {
                return Json(new JsonMessage("Tên gói cước đã tồn tại", GlobalConstants.TYPE_ERROR));
            }
        }

        /// <summary>
        /// Cap nhat chi tiet cua goi cuoc
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(FormCollection frm, string ListProvinceApply, string ListSchoolApply, string ListSchoolNotApply,
            int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status, int ServicePackageID, bool? CheckAllSchool)
        {
            string message = string.Empty;
            int? ServicePackageDeclareDetailID = null;
            if (frm["ServicePackageDeclareDetailID"] != null && frm["ServicePackageDeclareDetailID"] != string.Empty)
            {
                ServicePackageDeclareDetailID = Convert.ToInt32(frm["ServicePackageDeclareDetailID"]);
            }
            DateTime? FromDate = null;
            DateTime? ToDate = null;
            int? Effectiveday = null;
            int Year = Convert.ToInt32(frm["CboAcademicYear"]);
            string PackageCode = frm["CboPackage"].Trim();
            string Semester_I = frm["checkI"];
            string Semester_II = frm["checkII"];
            string frdate = frm["FromDate"];
            string todate = frm["ToDate"];
            string EffectiveDay = frm["EffectiveDay"];
            string ApplyType = frm["ApplyType"];
            List<int> lstAppliedProvinceID = GetIDsFromString(ListProvinceApply);
            List<int> LstSchoolIDCheck = GetIDsFromString(ListSchoolApply);
            List<int> LstSchoolIDUnCheck = GetIDsFromString(ListSchoolNotApply);

            if (!string.IsNullOrEmpty(frdate))
            {
                FromDate = ConvertDatetime(frdate);
            }

            if (!string.IsNullOrEmpty(todate))
            {
                ToDate = ConvertDatetime(todate);
            }

            int numDay = 0;
            if (!string.IsNullOrEmpty(EffectiveDay) && Int32.TryParse(EffectiveDay, out numDay))
            {
                Effectiveday = numDay;
            }

            ServicePackageBO servicepack = new ServicePackageBO();
            TryUpdateModel(servicepack);

            servicepack.ServiceCode = PackageCode;
            servicepack.Description = PackageCode;
            servicepack.Createdtime = DateTime.Now;
            servicepack.Year = Year;
            servicepack.FromDate = FromDate;
            servicepack.ToDate = ToDate;
            servicepack.EffectiveDays = Effectiveday;
            servicepack.ApplyType = Int32.Parse(ApplyType);
            servicepack.ServicePackageID = ServicePackageID;
            servicepack.ListAppliedProvince = lstAppliedProvinceID;


            // End 20160218
            if (!string.IsNullOrEmpty(Semester_I) && !string.IsNullOrEmpty(Semester_II)) servicepack.Semester = DeclarePackageConstant.SEMESTER_ALL;
            else
            {
                if (!string.IsNullOrEmpty(Semester_I)) servicepack.Semester = DeclarePackageConstant.SEMESTER_I;
                else servicepack.Semester = DeclarePackageConstant.SEMESTER_II;
            }

            if (Effectiveday > 0) servicepack.IsLimit = true;
            else servicepack.IsLimit = false;

            //viethd31 Bo sung cau hinh nam hoc 2017-2018
            servicepack.IsForOnlyInternal = !string.IsNullOrEmpty(frm["IsForOnlyInternal"]);
            servicepack.IsForOnlyExternal = !string.IsNullOrEmpty(frm["IsForOnlyExternal"]);
            servicepack.IsExtraPackage = !string.IsNullOrEmpty(frm["IsExtraPackage"]);
            servicepack.IsWholeYearPackage = !string.IsNullOrEmpty(frm["IsWholeYearPackage"]);

            if (servicepack.ApplyType == 1)
            {
                if (CheckAllSchool == true)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ProvinceID"] = ProvinceID;
                    dic["DistrictID"] = DistrictID;
                    dic["SchoolName"] = SchoolName;
                    dic["SchoolUserName"] = UserName;
                    dic["Status"] = Status;
                    dic["ServicePackageID"] = ServicePackageID;
                    ServicePackageSchoolDetailBusiness.Apply(dic);

                }
                else
                {
                    ServicePackageSchoolDetailBusiness.Apply(ServicePackageID, LstSchoolIDCheck, LstSchoolIDUnCheck);
                }
            }

            if (ServicePackageBusiness.UpdateServicePackageDetail(servicepack))
            {
                // Ghi log 
                //WriteLogInfo("Cập nhật gói cước thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Cập nhật gói cước thành công");
                return Json(new JsonMessage("Cập nhật gói cước thành công", GlobalConstants.TYPE_SUCCESS));
            }
            else return Json(new JsonMessage("Gói cước đã được khai báo, vui lòng kiểm tra lại", GlobalConstants.TYPE_ERROR));
        }

        public PartialViewResult GetEdit(int ServicePackageID, int? ServicePackageDeclareDetailID)
        {
            SetViewData();
            ViewData[DeclarePackageConstant.INFO_SERVICE_PACKAGE] = ServicePackageBusiness.SearchDetail(ServicePackageID, ServicePackageDeclareDetailID);
            ViewData[DeclarePackageConstant.NUM_OF_SCHOOL] = ServicePackageSchoolDetailBusiness.CountSchoolApply(ServicePackageID, 1);
            return PartialView("_Edit");
        }

        public PartialViewResult GetCreate(int year)
        {
            SetViewData();
            ViewData["SelectYear"] = year;
            return PartialView("_Create");
        }
        #endregion

        #region Huy goi cuoc
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int ServicePackageID, int? ServicePackageDeclareDetailID)
        {
            string message = string.Empty;
            if (ServicePackageBusiness.DeactiveServicePackage(ServicePackageID, ServicePackageDeclareDetailID))
            {
                message = "Xóa thành công";
                // Ghi log 
                //WriteLogInfo("Xóa thành công gói cước", null, "ServicePackageID: " + ServicePackageID.ToString(), string.Empty, null, string.Empty, string.Empty, string.Empty, "Xóa thành công gói cước");
            }
            else
            {
                message = "Không được phép xóa khai báo";
            }

            return Json(new JsonMessage(Res.Get(message)));
        }
        #endregion

        #region Load ajax
        public JsonResult AjaxLoadServicePackage(string ServiceCode)
        {
            List<ServicePackageBO> lstSearchPackage = ServicePackageBusiness.GetAllPackage();
            if (lstSearchPackage.Count > 0)
            {
                ViewData[DeclarePackageConstant.LIST_PACKAGE] = lstSearchPackage;
                return Json(new SelectList(lstSearchPackage, "ServiceCode", "ServiceCode", ServiceCode.ToUpper()));
            }
            return Json(new SelectList(new string[] { }));
        }

        public PartialViewResult GetPackageDetail(string ServiceCodeUpdate)
        {
            ServicePackageBO objSerDetail = ServicePackageBusiness.SearchDetailPackage(ServiceCodeUpdate, 0);
            ViewData[DeclarePackageConstant.INFO_PACKAGE_DETAIL] = objSerDetail;
            return PartialView("_EditPackage");
        }

        public JsonResult AjaxLoadDistrict(int? provinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId.HasValue && provinceId.Value > 0)
            {
                lst = DistrictBusiness.GetListDistrict(provinceId.Value)
                            .OrderBy(o => o.DistrictName).ToList();
            }
            if (lst == null) lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }
        #endregion

        #region Ultils function
        /// <summary>
        /// Chuyen dang cuoi sang dang ngay thang
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private DateTime? ConvertDatetime(string date)
        {
            try
            {
                string[] splitDate = date.Split('/');
                string dd = splitDate[0];
                string mm = splitDate[1];
                string yyyy = splitDate[2];
                string dateformat = yyyy + "/" + mm + "/" + dd;

                return Convert.ToDateTime(dateformat);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Áp dụng cấu hình gói cước
        /// <summary>
        /// Màn hình áp dụng tới mức trường
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewSchoolApplyScreen(string ServiceCode)
        {
            PrepareData();
            if (!string.IsNullOrEmpty(ServiceCode))
                ViewData[DeclarePackageConstant.SERVICE_PACKAGE_ID] = ServicePackageBusiness.GetServicePackageIDByServiceCode(ServiceCode);

            return PartialView("_SchoolApply");
        }

        /// <summary>
        /// Khởi gán dữ liệu cho màn hình áp dụng
        /// </summary>
        private void PrepareData()
        {
            ViewData[DeclarePackageConstant.LIST_PROVINCE] = ProvinceBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.ProvinceName).ToList();
        }

        /// <summary>
        /// Tìm kiếm trường để áp dụng
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public PartialViewResult _GridSchool(int? HfServicePackageID, int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status, GridCommand Command)
        {
            ViewData[DeclarePackageConstant.LIST_SCHOOL] = this._SearchSchool(HfServicePackageID, ProvinceID, DistrictID, SchoolName, UserName, Status, Command, ref Total);
            return PartialView();
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchSchoolAjax(int? HfServicePackageID, int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status, GridCommand Command)
        {
            List<SchoolApplyModel> lstSchool = this._SearchSchool(HfServicePackageID, ProvinceID, DistrictID, SchoolName, UserName, Status, Command, ref Total);
            return View(new GridModel<SchoolApplyModel>()
            {
                Data = lstSchool,
                Total = Total
            }); ;
        }

        public PartialViewResult GetProvinceList(string ServiceCode)
        {

            List<Province> lstProvince = ProvinceBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.ProvinceName).ToList();
            int ServicePackageID = ServicePackageBusiness.GetServicePackageIDByServiceCode(ServiceCode);
            List<int> lstAppliedProvinceID = ServicePackageSchoolDetailBusiness.All.Where(o => o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE
                && o.SERVICE_PACKAGE_ID == ServicePackageID).Select(o => o.UNIT_ID).ToList();

            ViewData["lstAppliedProvinceID"] = lstAppliedProvinceID;
            return PartialView("_ApplyProvince", lstProvince);
        }

        public List<SchoolApplyModel> _SearchSchool(int? HfServicePackageID, int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status, GridCommand command, ref int Total)
        {
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ProvinceID"] = ProvinceID;
            search["DistrictID"] = DistrictID;
            if (!string.IsNullOrEmpty(SchoolName)) SchoolName = SchoolName.Trim();
            search["SchoolName"] = SchoolName;
            if (!string.IsNullOrEmpty(UserName)) UserName = UserName.Trim();
            search["SchoolUserName"] = UserName.Trim();

            search["page"] = command.Page;
            search["numRecord"] = DeclarePackageConstant.PAGE_SIZE;
            List<SchoolApplyModel> LstViewModel = new List<SchoolApplyModel>();
            SchoolApplyModel model = null;
            Total = 0;
            SMS_SERVICE_PACKAGE sp = ServicePackageBusiness.Find(HfServicePackageID.Value);
            //bool isApplyAll = sp.IsLimitProvince != true && sp.IsLimitDistrict != true && sp.IsLimitSchool != true;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ServicePackageID"] = HfServicePackageID != 0 ? HfServicePackageID : -1;
            IQueryable<SMS_SERVICE_PACKAGE_UNIT> iqSp = ServicePackageSchoolDetailBusiness.Search(dic).Where(o => o.PACKAGE_UNIT_TYPE == SMAS.Business.Common.GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL);

            if (Status == 0)
            {
                List<SchoolBO> lstSchool = SchoolProfileBusiness.GetListSchoolSP(search, iqSp, null, ref Total);
                
                foreach (SchoolBO item in lstSchool)
                {
                    model = new SchoolApplyModel();
                    model.SchoolID = item.SchoolID;
                    model.ProvinceName = item.ProvinceName;
                    model.DistrictName = item.DistrictName;
                    model.SchoolName = item.SchoolName;
                    model.UserName = item.UserName;
                    model.IsApply = item.HasBeenApplied;
                    LstViewModel.Add(model);
                }
            }
            else if (Status == 1)
            {

                List<SchoolBO> lstSchool = SchoolProfileBusiness.GetListSchoolSP(search, iqSp, true, ref Total);
                foreach (SchoolBO item in lstSchool)
                {
                    model = new SchoolApplyModel();
                    model.SchoolID = item.SchoolID;
                    model.ProvinceName = item.ProvinceName;
                    model.DistrictName = item.DistrictName;
                    model.SchoolName = item.SchoolName;
                    model.UserName = item.UserName;
                    model.IsApply = true;
                    LstViewModel.Add(model);
                }
            }
            else if (Status == 2)
            {
                List<SchoolBO> lstSchool = SchoolProfileBusiness.GetListSchoolSP(search, iqSp, false, ref Total);
                foreach (SchoolBO item in lstSchool)
                {
                    model = new SchoolApplyModel();
                    model.SchoolID = item.SchoolID;
                    model.ProvinceName = item.ProvinceName;
                    model.DistrictName = item.DistrictName;
                    model.SchoolName = item.SchoolName;
                    model.UserName = item.UserName;
                    model.IsApply = false;
                    LstViewModel.Add(model);
                }

            }

            ViewData[DeclarePackageConstant.Page] = command.Page;
            ViewData[DeclarePackageConstant.Total] = Total;
            return LstViewModel;
        }

        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public JsonResult ApplySchool(bool IsApplyAll, int ServicePackageID, List<int> LstSchoolIDCheck, List<int> LstSchoolIDUnCheck, int? ProvinceID, int? DistrictID, string SchoolName, string UserName, int? Status)
        {
            SMS_SERVICE_PACKAGE sp = ServicePackageBusiness.Find(ServicePackageID);
            if (sp != null)
            {
                #region Cập nhật gói
                if (IsApplyAll)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ProvinceID"] = ProvinceID;
                    dic["DistrictID"] = DistrictID;
                    dic["SchoolName"] = SchoolName;
                    dic["SchoolUserName"] = UserName;
                    dic["Status"] = Status;
                    dic["ServicePackageID"] = ServicePackageID;
                    ServicePackageSchoolDetailBusiness.Apply(dic);

                }
                else
                {
                    ServicePackageSchoolDetailBusiness.Apply(ServicePackageID, LstSchoolIDCheck, LstSchoolIDUnCheck);
                }
                #endregion

                ServicePackageBusiness.Save();
                return Json(new { Message = "Áp dụng thành công", Type = GlobalConstants.TYPE_SUCCESS, num = ServicePackageSchoolDetailBusiness.CountSchoolApply(ServicePackageID, 1) });
            }
            else return Json(new JsonMessage("Không tìm thấy gói cước", GlobalConstants.TYPE_ERROR));

        }


        public JsonResult countSchoolApply(int ServicePackageID)
        {
            return Json(new { num = ServicePackageSchoolDetailBusiness.CountSchoolApply(ServicePackageID, 1) });
        }

        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }
        #endregion
    }
}