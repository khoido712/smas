﻿using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common.Extension;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class SendSMSCommentOfPupilController:BaseController
    {
        private readonly ISMSParentContractBusiness SMSParentContractBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ICommentOfPupilBusiness CommentOfPupilBusiness;
        public SendSMSCommentOfPupilController(ISMSParentContractBusiness smsParentContractBusiness, ISMSHistoryBusiness SMSHistoryBusiness,
                ISchoolConfigBusiness schoolConfigBusiness, IClassProfileBusiness ClassProfileBusiness, IPupilProfileBusiness PupilProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness, ICommentOfPupilBusiness CommentOfPupilBusiness)
        {
            this.SMSParentContractBusiness = smsParentContractBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.SchoolConfigBusiness = schoolConfigBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.CommentOfPupilBusiness = CommentOfPupilBusiness;
        }

        [HttpPost]
        public ActionResult Index(int? ClassID, int? SubjectID, int? SemesterID, int? DayOfMonthID, string FromDate, string ToDate, string SubjectName)
        {
            ViewData[SendSMSCommentOfPupilConstants.SubjectID] = SubjectID;
            ViewData[SendSMSCommentOfPupilConstants.ClassID] = ClassID;
            ViewData[SendSMSCommentOfPupilConstants.SEMESTER_ID] = SemesterID;
            ViewData[SendSMSCommentOfPupilConstants.FROM_DATE] = Convert.ToDateTime(FromDate);
            ViewData[SendSMSCommentOfPupilConstants.TO_DATE] = Convert.ToDateTime(ToDate);
            ViewData[SendSMSCommentOfPupilConstants.DAY_OF_MONTH] = DayOfMonthID;
            ViewData[SendSMSCommentOfPupilConstants.SUBJECT_NAME] = SubjectName;
            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            ViewData[SendSMSCommentOfPupilConstants.ALLOW_SEND_UNICODE_MESSAGE] = cfg != null && cfg.IS_ALLOW_SEND_UNICODE;
            return View();
        }
        public FileResult ExportExcel(int ClassID, int SubjectID, int SemesterID, int DayOfMonthID, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject, int AllowSignMessage)
        {
            string fileName = "NoiDungTraoDoi.xls";
            string template = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + fileName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet templateSheet = oBook.GetSheet(2);
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            string className = objCP != null ? objCP.DisplayName : "";
            templateSheet.SetCellValue("A2", ClassID);
            templateSheet.SetCellValue("B2", className);
            //fill thong tin chung
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            string strTitle = "Khối " + objCP.EducationLevelID + " Lớp: " + objCP.DisplayName;
            sheet.SetCellValue("A5", strTitle);
            sheet.SetCellValue("E6", _globalInfo.SchoolName);
            List<PupilProfileBO> lstPP = PupilProfileBusiness.GetListPupilByClassID(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID)
                .OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.FullName).ToList();
            List<int> lstPupilID = lstPP.Select(p => p.PupilProfileID).Distinct().ToList();
            //lay danh sach hoc sinh co hop dong
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<PupilProfileBO> lstSMSParentContract = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, aca.Year, SemesterID, lstPP);
            PupilProfileBO objPP = null;
            //lay danh sach nhan xet cua hoc sinh
            List<CommentOfPupilBO> lstCommentOfPupil = CommentOfPupilBusiness.GetCommentOfPupil(lstPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SubjectID, SemesterID, DayOfMonthID, FromDate, ToDate, NoDate, NoSubject);
            CommentOfPupilBO objCommentBO = null;
            int startRow = 9;
            string phoneNumber = string.Empty;
            string Fomula = string.Empty;
            for (int i = 0; i < lstSMSParentContract.Count; i++)
            {
                objPP = lstSMSParentContract[i];
                sheet.PasteRange(templateSheet.GetRange("A5", "G5"), sheet.GetRange("A" + startRow, "G" + startRow));
                phoneNumber = string.Empty;
                objCommentBO = lstCommentOfPupil.Where(p => p.PupilID == objPP.PupilProfileID).FirstOrDefault();
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objPP.PupilCode);
                sheet.SetCellValue(startRow, 3, objPP.FullName);
                sheet.SetCellValue(startRow, 4, objCP.DisplayName);
                for (int j = 0; j < objPP.PhoneReceiver.Count; j++)
                {
                    if (j < objPP.PhoneReceiver.Count - 1)
                    {
                        phoneNumber += objPP.PhoneReceiver[j] + "\r\n";
                    }
                    else
                    {
                        phoneNumber += objPP.PhoneReceiver[j];
                    }

                }
                sheet.SetCellValue(startRow, 5, phoneNumber);
                if (objCommentBO != null)
                {
                    sheet.SetCellValue(startRow, 6, AllowSignMessage == 1 ? objCommentBO.Comment : objCommentBO.Comment.StripVNSign());
                }
                if (AllowSignMessage == 1)
                {
                    Fomula = "=IF(LEN(F" + startRow + ")=0,\"0\",INT(IF(MOD(LEN(F" + startRow + ")+LEN(E6),67)=0,(LEN(F" + startRow + ")+LEN(E6))/67,(LEN(F" + startRow + ")+LEN(E6))/67+1)))";
                }
                else
                {
                    Fomula = "=IF(LEN(F" + startRow + ")=0,\"0\",INT(IF(MOD(LEN(F" + startRow + ")+LEN(E6),160)=0,(LEN(F" + startRow + ")+LEN(E6))/160,(LEN(F" + startRow + ")+LEN(E6))/160+1)))";
                }


                sheet.SetFormulaValue(startRow, 7, Fomula);
                startRow++;
            }
            sheet.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string reportName = string.Format("Noidungtraodoi_{0}.xls", className.StripVNSignAndSpace());
            result.FileDownloadName = reportName;
            return result;
        }

        [ValidateAntiForgeryToken]
        public JsonResult SendSMSCommentOfPupil(int ClassID, int SubjectID, int SemesterID, int DayOfMonthID, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject, int AllowSignMessage)
        {
            List<PupilProfileBO> lstPP = PupilProfileBusiness.GetListPupilByClassID(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID);
            List<int> lstPupilID = lstPP.Select(p => p.PupilProfileID).Distinct().ToList();
            //lay danh sach hoc sinh co hop dong
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<PupilProfileBO> lstSMSParentContract = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, aca.Year, SemesterID, lstPP);
            List<int> lstPupilContractID = lstSMSParentContract.Select(p => p.PupilProfileID).Distinct().ToList();
            List<int> lstPupilSendSMS = new List<int>();
            PupilProfileBO objPP = null;
            //lay danh sach nhan xet cua hoc sinh
            List<CommentOfPupilBO> lstCommentOfPupil = CommentOfPupilBusiness.GetCommentOfPupil(lstPupilContractID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, SubjectID, SemesterID, DayOfMonthID, FromDate, ToDate, NoDate, NoSubject);
            CommentOfPupilBO objCommentBO = null;
            List<string> lstContent = new List<string>();
            List<string> lstShortContent = new List<string>();
            string ContentSMS = string.Empty;
            string shortContentSMS = string.Empty;
            string tmp = string.Empty;
            string branchName = _globalInfo.SchoolName + ": ";
            ResultBO result = new ResultBO();
            for (int i = 0; i < lstSMSParentContract.Count; i++)
            {
                objPP = lstSMSParentContract[i];
                objCommentBO = lstCommentOfPupil.Where(p => p.PupilID == objPP.PupilProfileID).FirstOrDefault();
                tmp = objCommentBO != null ? objCommentBO.Comment : "";
                ContentSMS = branchName + tmp;
                shortContentSMS = tmp;
                if (!string.IsNullOrEmpty(tmp))
                {
                    lstContent.Add(ContentSMS);
                    lstShortContent.Add(shortContentSMS);
                    lstPupilSendSMS.Add(objPP.PupilProfileID);
                }
            }
            if (lstContent.Count == 0)
            {
                return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = string.Format(Res.Get("Communication_SMSToParent_Result"), 0) });
            }
            else
            {
                bool isSignMsg = false;
                SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
                if (cfg != null) isSignMsg = cfg.IS_ALLOW_SEND_UNICODE && AllowSignMessage == 1;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                //Nếu là admin trường thì EmployeeID = 0
                dic["senderID"] = _globalInfo.EmployeeID;
                dic["lstReceiverID"] = lstPupilSendSMS;
                dic["schoolID"] = _globalInfo.SchoolID;
                dic["classID"] = ClassID;
                dic["isSignMsg"] = isSignMsg;
                dic["academicYearID"] = _globalInfo.AcademicYearID;
                dic["type"] = GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT;
                dic["typeHistory"] = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
                dic["isAdmin"] = _globalInfo.IsAdmin;
                dic["lstContent"] = lstContent;
                dic["lstShortContent"] = lstShortContent;
                dic["isSendSMSComment"] = true;
                dic["semester"] = this.Semester;
                dic["iLevelID"] = _globalInfo.AppliedLevel;
                result = SMSHistoryBusiness.SendSMS(dic);
                if (result.isError)
                {
                    return Json(new { Type = GlobalConstantsEdu.ERROR, Message = Res.Get(result.ErrorMsg) });
                }
            }
            return Json(new JsonMessage(string.Format(Res.Get("Communication_SMSToParent_Result"), result.NumSendSuccess), GlobalConstantsEdu.COMMON_SUCCESS));
        }
    }
}