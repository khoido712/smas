﻿using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Web.Mvc;
using SMAS.VTUtils.Utils;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Utils;
using System.Drawing.Imaging;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class TopupController:BaseController
    {
        string SK_NumberOfTopupFailure = "SK_NumberOfTopupFailure";
        string SK_LastTimeTopupFailure = "SK_LastTimeTopupFailure";
        string SK_Captcha = "SK_Captcha";

        private IEWalletBusiness EwalletBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private ITopupBusiness TopupBusiness;
        private IUserGroupBusiness UserGroupBusiness;
        private IGroupMenuBusiness GroupMenuBusiness;

        private EwalletBO getEWallet()
        {
            EwalletBO eWallet;
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                ISupervisingDeptBusiness SupervisingDeptBusiness = new SupervisingDeptBusiness(logger);
                SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
                if (sup == null)
                {
                    throw new BusinessException("Không tìm thấy thông tin phòng sở.");
                }
                else
                {
                    //int userID = _globalInfo.SupervisingDept.SupervisingDeptID;
                    int userId = sup.SupervisingDeptID;
                    eWallet = this.EwalletBusiness.GetEWalletIncludePromotion(userId, false, true);
                }
            }
            else
            {
                if (_globalInfo.SchoolID <= 0)
                {
                    throw new BusinessException("Không tìm thấy thông tin nhà trường.");
                }
                else
                {
                    int userID = _globalInfo.SchoolID.Value;
                    eWallet = this.EwalletBusiness.GetEWalletIncludePromotion(userID, true, false);
                }
            }
            if (eWallet == null)
            {
                throw new BusinessException("Không tìm thấy thông tin tài khoản.");
            }
            return eWallet;
        }

        public TopupController(
            IEWalletBusiness EWalletBusiness, IUserAccountBusiness UserAccountBusiness, IUserGroupBusiness UserGroupBusiness, IGroupMenuBusiness GroupMenuBusiness,
            ITopupBusiness TopupBusiness)
        {
            this.EwalletBusiness = EWalletBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.UserGroupBusiness = UserGroupBusiness;
            this.GroupMenuBusiness = GroupMenuBusiness;
            this.TopupBusiness = TopupBusiness;
        }
        //
        // GET: /Payment/Topup/
        public ActionResult Index()
        {
            return Topup();
        }

        //
        // GET: /Payment/Topup/topup
        public ActionResult Topup()
        {
            
            var ewallet = this.getEWallet();
            // Kiem tra co bi khoa nap the khong
            if (ewallet.IsLockedOut)
            {
                // Neu bi khoa thi redirect toi /Payment/Payment/TopupBlocked
                string TopupBlockedMethodName = StaticReflection.GetMemberName<TopupController>(m => new Func<long, ActionResult>(m.TopupBlocked));
                return this.RedirectToAction(TopupBlockedMethodName);
            }
            // Lay quyen nguoi dung trong SMAS3
            var per = this.GetMenuPermission("Payment", _globalInfo.UserAccount.UserAccountID);
            ViewBag.PermissionOfUser = per;
            
            return View("Topup");
        }

        //
        // POST: /Payment/Topup/Topup
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Topup(TopupViewModel cardInfo)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay quyen nguoi dung trong SMAS 3
            var per = this.GetMenuPermission("Payment", _globalInfo.UserAccount.UserAccountID);
            ViewBag.PermissionOfUser = per;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Kiem tra quyen nguoi dung trong SMAS3
            if (!((per & EPermission.Create) == EPermission.Create || (per & EPermission.Edit) == EPermission.Edit || (per & EPermission.Delete) == EPermission.Delete))
            {
                // Nguoi dung khong co quyen nap the => Hien thi thong bao loi
                ViewBag.FatalError = "Thầy cô không có quyền thực hiện nạp thẻ.";
                return this.View();
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            #region Kiem tra xem thong tin nhap co dung dinh dang khong. Neu khong dung thi hien thi lai trang de nhap lai
            // Kiem tra so serial
            string CardSerialFieldName = StaticReflection.GetMemberName<TopupViewModel>(m => m.CardSerial);
            if (ModelState.IsValidField(CardSerialFieldName))
            {
                if (string.IsNullOrWhiteSpace(cardInfo.CardSerial)) // Kiem tra da nhap hay chua
                {
                    ModelState.AddModelError(CardSerialFieldName, Res.Get("CardSerial_Is_Required_Field"));
                }
                else
                {
                    cardInfo.CardSerial = cardInfo.CardSerial.Replace(" ", "");// Xoa het cac khoang trang
                    if (cardInfo.CardSerial.Length > 15) // Kiem tra do dai
                    {
                        ModelState.AddModelError(CardSerialFieldName, Res.Get("CardSerial_Length_Is_Invalid"));
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Kiem tra ma pin
            string PinCardFieldName = StaticReflection.GetMemberName<TopupViewModel>(m => m.PinCard);
            if (ModelState.IsValidField(PinCardFieldName))
            {
                if (string.IsNullOrWhiteSpace(cardInfo.PinCard)) // Kiem tra da nhap hay chua
                {
                    ModelState.AddModelError(PinCardFieldName, Res.Get("PinCard_Is_Required_Field"));
                }
                else
                {
                    cardInfo.PinCard = cardInfo.PinCard.Replace(" ", "");// Xoa het cac khoang trang
                    if (cardInfo.PinCard.Length != 15 && cardInfo.PinCard.Length != 13) // Kiem tra do dai
                    {
                        ModelState.AddModelError(PinCardFieldName, Res.Get("PinCard_Length_Is_Invalid"));
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Kiem tra ma xac nhan da nhap hay chua
            string ConfirmationCodeFieldName = StaticReflection.GetMemberName<TopupViewModel>(m => m.ConfirmationCode);
            if (ModelState.IsValidField(ConfirmationCodeFieldName))
            {
                if (string.IsNullOrWhiteSpace(cardInfo.ConfirmationCode)) // Kiem tra da nhap hay chua
                {
                    ModelState.AddModelError(ConfirmationCodeFieldName, Res.Get("ConfirmationCode_Is_Required_Field"));
                }
                else if (!confirmationCodeIsValid(cardInfo.ConfirmationCode)) // Kiem tra ma xac nhan co dung khong
                {
                    ModelState.Remove(ConfirmationCodeFieldName);
                    ModelState.AddModelError(ConfirmationCodeFieldName, "Mã xác nhận không đúng");
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Neu cac thong tin nhap khong hop le thi bao loi
            if (!ModelState.IsValid)
            {
                return this.View();
            }
            #endregion

            #region Tiến hành nạp thẻ

            EwalletBO eWallet;

            eWallet = this.getEWallet(); // tao InvalidDataException khi khong tim thay ewallet

            // Kiem tra co bi khoa nap the khong
            if (eWallet.IsLockedOut)
            {
                // Neu bi khoa thi redirect toi /Payment/Payment/TopupBlocked
                string TopupBlockedMethodName = StaticReflection.GetMemberName<TopupController>(m => new Func<long, ActionResult>(m.TopupBlocked));
                return this.RedirectToAction(TopupBlockedMethodName);
            }
            // Thuc hien goi gateway de nap the

            TopupResponseBO trans = this.TopupBusiness.TopUp(eWallet.EWalletID, cardInfo.CardSerial, cardInfo.PinCard);

            // Nap the thanh cong thi redirect toi /Payment/Payment/TopupSuccessfully
            if (trans.Success)
            {
                string TopupSuccessfullyMethodName = StaticReflection.GetMemberName<TopupController>(m => new Func<long, decimal, ActionResult>(m.TopupSuccessfully));
                return this.RedirectToAction(TopupSuccessfullyMethodName, new { id = DateTime.Now.Ticks, amount = trans.TransactionAmount });
            }
            else
            {
                // Hien thi thong bao loi
                ViewBag.MessageOfError = trans.ErrorMessage;
                // Xoa ma xac nhan
                ModelState.Remove(ConfirmationCodeFieldName);
                return this.View();
            }


            #endregion

        }


        //
        // GET: /Payment/Topup/TopupSuccessfully
        /// <summary>
        /// Thông báo nạp thẻ thành công
        /// </summary>
        /// <param name="id">So ticks cua thoi diem redirect</param>
        /// <returns></returns>
        public ActionResult TopupSuccessfully(long id = 1000, decimal amount = 0)
        {
            DateTime redirectionTime = new DateTime(id);
            // Neu thoi diem redirect cach thoi diem hien tai hon 20 giay thi redirect tro lai trang topup
            if (redirectionTime >= DateTime.Now || redirectionTime.AddSeconds(20) < DateTime.Now)
            {
                string TopupMethodName = StaticReflection.GetMemberName<TopupController>(m => new Func<ActionResult>(m.Topup));
                return this.RedirectToAction(TopupMethodName);
            }
            var ewallet = this.getEWallet();
            var per = this.GetMenuPermission("Payment", _globalInfo.UserAccount.UserAccountID);
            ViewBag.PermissionOfUser = per;
            ViewBag.MessageOfSuccess = string.Format("Thầy/cô đã nạp thành công {0} đ vào tài khoản.", amount.ToString("#,##0"));
            return View("Topup");
        }

        //
        // GET: /Payment/Topup/TopupBlocked
        public ActionResult TopupBlocked(long id = 1000)
        {
            EwalletBO ewallet = this.getEWallet();
            if (!ewallet.IsLockedOut) // Neu tai khoan khong bi khoa thi redirect trở lại trang nạp thẻ
            {
                string TopupMethodName = StaticReflection.GetMemberName<TopupController>(m => new Func<ActionResult>(m.Topup));
                return this.RedirectToAction(TopupMethodName);
            }
            ViewBag.MessageOfError = string.Format("Chức năng nạp thẻ đã bị khóa do nhà trường nạp sai thẻ cào quá 3 lần. Thầy/cô vui lòng nạp thẻ lại sau {0} phút, {1} giây."
                , ewallet.RemainOfTimeToUnlock.Minutes, ewallet.RemainOfTimeToUnlock.Seconds);
            return View();
        }

        public ActionResult Captcha(int id = 0)
        {
            var captcha = new CaptchaBO();
            setCaptchaValue(captcha.Value);
            FileContentResult streamResult;
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                // Ghi ảnh trực tiếp ra luồng xuất theo định dạng gif
                captcha.Image.Save(memoryStream, ImageFormat.Jpeg);
                streamResult = this.File(memoryStream.GetBuffer(), "image/jpeg");
            }
            return streamResult;
        }


        /// <summary>
        /// Lay so lan nap the khong hop le
        /// </summary>
        /// <returns></returns>
        private int getNumberOfTopupFailure()
        {
            int numberOfFailureTopup = this.Session[this.SK_NumberOfTopupFailure] is int ? (int)this.Session[this.SK_NumberOfTopupFailure] : 0;
            return numberOfFailureTopup;
        }

        /// <summary>
        /// Luu so lan nap the khong hop le
        /// </summary>
        /// <param name="numberOfFailureTopup"></param>
        /// <returns></returns>
        private int setNumberOfTopupFailure(int numberOfFailureTopup)
        {
            this.Session[this.SK_NumberOfTopupFailure] = numberOfFailureTopup;
            return numberOfFailureTopup;
        }

        /// <summary>
        /// Lay thoi gian lan nap the khong hop le cuoi cung
        /// </summary>
        /// <returns></returns>
        private DateTime? getLastTimeTopupFailure()
        {
            DateTime? numberOfFailureTopup = this.Session[this.SK_LastTimeTopupFailure] as DateTime?;
            return numberOfFailureTopup;
        }

        /// <summary>
        /// Luu thoi gian lan nap the khong hop le cuoi cung
        /// </summary>
        /// <param name="lastTimeFailureTopup"></param>
        /// <returns></returns>
        private DateTime? setLastTimeFailureTopup(DateTime? lastTimeFailureTopup)
        {
            this.Session[this.SK_LastTimeTopupFailure] = lastTimeFailureTopup;
            return lastTimeFailureTopup;
        }

        /// <summary>
        /// Tang so lan dang nhap sai
        /// </summary>
        /// <returns>So lan dang nhap sai</returns>


        private string getCaptchaValue()
        {
            string value = this.Session[this.SK_Captcha] == null ? null : (this.Session[this.SK_Captcha]).ToString();
            return value;
        }

        private string setCaptchaValue(string value)
        {
            this.Session[this.SK_Captcha] = value;
            return value;
        }

        private bool confirmationCodeIsValid(string confirmationCode)
        {
            string captcha = getCaptchaValue();
            return captcha == confirmationCode;
        }

        public EPermission GetMenuPermission(string Controller, int UserAccountID)
        {
            EPermission per = EPermission.None;

            if (this.UserAccountBusiness.IsAdmin(UserAccountID)) // Neu la quyen quan tri hien thi quyen cao nhat (cu the la quyen xoa)
            {
                per = EPermission.View | EPermission.Create | EPermission.Edit | EPermission.Delete;
            }
            else
            {
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                var listGroupCatID = objUserGroup.Select(u => u.GroupCatID).ToList();
                if (objUserGroup != null && objUserGroup.Count() > 0)
                {
                    List<GroupMenu> lstGroupMenu = GroupMenuBusiness.All
                        .Where(gm => listGroupCatID.Contains(gm.GroupID)
                            && gm.Menu.URL.ToLower().Contains(Controller.ToLower()))
                        .ToList();
                    foreach (var groupMenu in lstGroupMenu)
                    {
                        switch (groupMenu.Permission)
                        {
                            case SystemParamsInFile.PER_VIEW:
                                per = per | EPermission.View;
                                break;
                            case SystemParamsInFile.PER_CREATE:
                                per = per | EPermission.Create;
                                break;
                            case SystemParamsInFile.PER_UPDATE:
                                per = per | EPermission.Edit;
                                break;
                            case SystemParamsInFile.PER_DELETE:
                                per = per | EPermission.Delete;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return per;
        }
    }

    public enum EPermission
    {
        None = 0,
        View = 1,
        Create = 2,
        Edit = 4,
        Delete = 8
    }
}