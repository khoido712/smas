﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class SendSMSToSupervisingDeptController:BaseController
    {
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEWalletBusiness EwalletBusiness;
        private readonly IPromotionBusiness PromotionProgramBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        public SendSMSToSupervisingDeptController(ISMSHistoryBusiness SMSHistoryBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IEWalletBusiness EwalletBusiness,
            IPromotionBusiness PromotionProgramBusiness,
            ISchoolConfigBusiness SchoolConfigBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.EwalletBusiness = EwalletBusiness;
            this.PromotionProgramBusiness = PromotionProgramBusiness;
            this.SchoolConfigBusiness = SchoolConfigBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }
        //
        // GET: /SendSMSToSupervisingDept/SendSMSToSupervisingDept/
        [HttpGet]
        public ActionResult Index()
        {
            //lay don vi thuoc phong/so
            //Nếu người dùng thuôc sở hoặc phòng thuộc sở thì lấy các đơn vi thuộc sở quán lý
            // Nếu ngươid dùng thuộc phòng hoặc phòng thuộc phòng thì lấy các đơn vị thuộc phòng quản lý
            SupervisingDept SupervisingDept = this.GetSupervisingDeptParent();
            var ewallet = EwalletBusiness.GetEWalletIncludePromotion(SupervisingDept.SupervisingDeptID, false, true);
            if (ewallet != null)
            {
                ViewData[SendSMSToSuperVisingDeptConstants.BALANCE_OF_EWALLET] = ewallet.Balance;
                ViewData[SendSMSToSuperVisingDeptConstants.NUMBER_PROMOTION_SMS] = ewallet.InternalSMS;
            }
            else
            {
                ViewData[SendSMSToSuperVisingDeptConstants.BALANCE_OF_EWALLET] = 0;
                ViewData[SendSMSToSuperVisingDeptConstants.NUMBER_PROMOTION_SMS] = 0;
            }
            ViewData[SendSMSToSuperVisingDeptConstants.LIST_LEVEL] = SupervisingDeptBusiness.GetListSuperVisingDeptByID(SupervisingDept.SupervisingDeptID);
            ViewData["SupervisingDeptParentID"] = this.GetSupervisingDeptParent().SupervisingDeptID;
            
            LoadContentPromotion(SupervisingDept.ProvinceID.Value, SupervisingDept.SupervisingDeptID);
            return View();
        }

        #region load detail view
        /// <summary>
        /// load contentDetail level
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDetailLevelToSMS(int supervisingDeptID)
        {
            int numOfSchool = 0;
            List<EmployeeSMSBO> employeeBOList = GetEmployeeBySuperVisingDeptID(supervisingDeptID, ref numOfSchool, 1);
            return Json(new { numOfReceiverTotal = numOfSchool });
        }
        #endregion

        #region load content detail view
        /// <summary>
        /// load content detail view
        /// </summary>
        /// <author>HaiVT 24/09/2013</author>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadContentDetailView(int supervisingDeptID)
        {
            int totalRecord = 0;
            List<EmployeeSMSBO> lstEmployeeBO = GetEmployeeBySuperVisingDeptID(supervisingDeptID, ref totalRecord, 1);
            if (lstEmployeeBO != null)
            {
                ViewData[SendSMSToSuperVisingDeptConstants.LIST_EMPLOYEE] = lstEmployeeBO;
            }
            else
            {
                ViewData[SendSMSToSuperVisingDeptConstants.LIST_EMPLOYEE] = new List<SchoolBO>();
            }

            return PartialView("_ContentDetail");
        }
        #endregion

        #region load to scroll
        /// <summary>
        /// load content detail when scroll
        /// </summary>7
        /// <author>HaiVT 24/09/2013</author>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadContentDetailScoll(int supervisingDeptID, string searchContent, string arrReceiverIDIsLoad, int currentPage)
        {
            int totalRecord = 0;
            List<EmployeeSMSBO> lstEmployeeBO = GetEmployeeBySuperVisingDeptID(supervisingDeptID, ref totalRecord, currentPage, searchContent);
            return Json(new { lstReceiver = lstEmployeeBO });
        }
        #endregion

        #region search receiver
        /// <summary>
        /// search receiver
        /// </summary>
        /// <author>HaiVT 24/09/2013</author>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SearchReceiver(int supervisingDeptID, string searchContent, int currentPage)
        {
            int totalRecord = 0;
            List<EmployeeSMSBO> lstEmployeeBO = GetEmployeeBySuperVisingDeptID(supervisingDeptID, ref totalRecord, currentPage, searchContent);
            return Json(new { lstReceiver = lstEmployeeBO, totalReceiver = totalRecord });
        }
        #endregion

        #region sendSMS
        /// <summary>
        /// send SMS to school, content duoc replace dau '&' = %26amp; nen phai replace lai
        /// </summary>
        /// <author>HaiVT 25/09/2013</author>
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult SendSMS(SendSMSToSuperVisingDeptViewModel objRequestModel)
        {
            string brandname = CommonMethods.GetBrandName().Trim().ToUpper();
            string prefix = CommonMethods.GetPrefix(null).Trim().ToUpper();
            if (objRequestModel.Content.Trim().ToUpper().Equals(brandname) || objRequestModel.Content.Trim().ToUpper().Equals(prefix))
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = Res.Get("Send_SMS_Group_Empty_SMS_Error") });
            }

            #region get list receiver
            if (objRequestModel.SuperVisingDeptID != 0)
            {
                SupervisingDept superVisingDept = SupervisingDeptBusiness.Find(objRequestModel.SuperVisingDeptID);
                if (superVisingDept != null && (superVisingDept.HierachyLevel != GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE
                    && superVisingDept.HierachyLevel != GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE))
                {
                    if (!superVisingDept.TraversalPath.Contains("\\" + this.GetSupervisingDept().ParentID + "\\"))
                    {
                        return Json(new { Type = GlobalConstantsEdu.ERROR, Message = Res.Get("lbl_Not_SuperVisingDept_Deparment") });
                    }
                }
            }

            List<int> lstReceiver = objRequestModel.ArrIsCheckReceiverID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            
            #endregion
            SupervisingDept SupervisingDept = this.GetSupervisingDeptParent();

            //SupervisingDept objSuperUnit = SupervisingDeptBusiness.;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["lstReceiver"] = lstReceiver;
            dic["isToSchool"] = false;
            dic["isSignMsg"] = objRequestModel.AllowSignMessage;
            dic["content"] = objRequestModel.Content.Trim();
            dic["shortContent"] = objRequestModel.Content;
            Employee objEmployee = EmployeeBusiness.Find(_globalInfo.EmployeeID.GetValueOrDefault());
            string EmployeeName = objEmployee != null ? objEmployee.FullName : string.Empty;
            dic["superUserName"] = !string.IsNullOrEmpty(EmployeeName) ? EmployeeName : SupervisingDept.SupervisingDeptName;
            dic["unitID"] = SupervisingDept.SupervisingDeptID;
            dic["SelectedUnitID"] = objRequestModel.SuperVisingDeptID;
            dic["ProvinceID"] = SupervisingDept.ProvinceID;
            dic["EmployeeID"] = _globalInfo.EmployeeID;
            dic[GlobalConstantsEdu.COMMON_SUPERVISINGDEPT] = this.GetSupervisingDeptParent();

            //Lay chuong trinh KM
            PromotionProgramBO promotion = LoadContentPromotion(SupervisingDept.ProvinceID.Value, SupervisingDept.SupervisingDeptID);
            dic["Promotion"] = promotion;

            SMSEduResultBO result = SMSHistoryBusiness.SendSMSFromSuperUser(dic);
            if (result.isError)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = Res.Get(result.ErrorMsg) });
            }

            int counter = result.NumSendSuccess;
            return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = "Gửi thành công đến " + counter + " cán bộ.", counter = counter, BalanceOfEWallet = result.Balance, NumOfPromotionInternalSMS = result.NumOfPromotionInternalSMS });
        }
        #endregion

        #region private method
        /// <summary>
        /// load employee by unitID
        /// </summary>
        /// <author>HaiVT 23/09/2013</author>
        /// <returns></returns>
        private List<EmployeeSMSBO> GetEmployeeBySuperVisingDeptID(int supervisingDeptID, ref int totalRecord, int page, string searchContent = "")
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            List<EmployeeSMSBO> employeeBOList = SupervisingDeptBusiness.GetEmployeeBySuperVisingDept(this.GetSupervisingDeptParent().ProvinceID.Value, this.GetSupervisingDeptParent().SupervisingDeptID, _globalInfo.EmployeeID.GetValueOrDefault(), searchContent, supervisingDeptID);
            totalRecord = employeeBOList.Count;
            return employeeBOList;
        }
        #endregion

        private PromotionProgramBO LoadContentPromotion(int provinceId, int unitId)
        {
            //Lay chuong trinh khuyen mai
            PromotionProgramBO objPromotion = PromotionProgramBusiness.GetPromotionProgramByProvinceId(provinceId, unitId);
            if (objPromotion != null)
            {
                ViewData[SendSMSToSuperVisingDeptConstants.CONTENT_PROMOTION] = !String.IsNullOrEmpty(objPromotion.Note) ? objPromotion.Note : objPromotion.PromotionName;
            }

            return objPromotion;
        }

        private SupervisingDept GetSupervisingDept()
        {
            return SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
        }

        private SupervisingDept GetSupervisingDeptParent()
        {
            SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

            if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }

            return null;
        }
    }
}