﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Business.Common;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.SMSEduArea.Models.StatisticsCommunication;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Utils;
using System.IO;
using System.Text;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class StatisticsCommunicationController: BaseController
    {
        #region Constants
        private const string _SMAS_CONTROL_NAME = "#smsedu/statictiscommunucation#";

        private const string _CONST_VISIBLE = "Visible";

        private const string _CONST_PUPILFILE_ID = "PupilProfileID";

        private const string _CONST_FULL_NAME = "FullName";

        private const string _CONST_EMPLOYEE_ID = "EmployeeID";

        private const string _CONST_ENABLE = "Enable";

        private const string _CONST_LEFT_PARENTHESIS = " (";

        private const string _CONST_RIGHT_PARENTHESIS = ")";

        private const string _CONST_A2 = "A2";

        private const string _CONST_SEMESTER = "Học kỳ ";

        private const string _CONST_SEMESTER_1 = "I";

        private const string _CONST_SEMESTER_2 = "II";

        private const string _CONST_COMMA_SPACE = ", ";

        private const string _CONST_SCHOOL_YEAR = "năm học ";

        private const string _CONST_FORMAT_DATE = "Từ ngày {0} đến ngày {1}";

        private const string _CONST_TEMPLATE_NAME_TEACHER = "BM_ThongKeTinNhanTheoGiaoVien.xls";

        private const string _CONST_TEMPLATE_NAME_PARENT = "BM_ThongKeTinNhanTheoHocSinh.xls";

        private const string _CONST_LOG_TEXT_TEACHER = "Xuất excel báo cáo số lương tin nhắn gửi đến giáo viên.";

        private const string _CONST_LOG_TEXT_PARENT = "Xuất excel báo cáo số lương tin nhắn gửi đến PHHS.";

        private const string _CONST_FILE_KEY_FORMAT = "Export.{0}";

        private const string _CONST_FILE_KEY_DATE_FORMAT = "dd.MM.yyyy.ss.ff.ff";

        private const string _CONST_FILE_STREAM = "application/octet-stream";

        private const string _CONST_A5 = "A5";

        private const string _CONST_A6 = "A6";

        private const string _CONST_A9 = "A9";

        private const string _CONST_E9 = "E9";

        private const string _CONST_A10 = "A10";

        private const string _CONST_E10 = "E10";

        private const string _CONST_G10 = "G10";

        private const string _CONST_A11 = "A11";

        private const string _CONST_G11 = "G11";
        private static int total = 0;
        #endregion
        #region Register Contructor
        private readonly ISMSHistoryBusiness smsHistoryBusiness;
        private readonly IContactGroupBusiness contactGroupBusiness;
        private readonly ITeacherContactBusiness teacherContactBusiness;
        private readonly ISMSTypeBusiness smsTypeBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusines;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ICustomSMSTypeBusiness customSMSTypeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISMSParentContractBusiness SMSParentContractBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public StatisticsCommunicationController(IContactGroupBusiness contactGroupBusiness,
               ISMSHistoryBusiness smsHistorySMSBusiness, ITeacherContactBusiness teacherContactBusiness, ISMSTypeBusiness smsTypeBusiness,
            IPupilProfileBusiness pupilProfileBusiness, IEmployeeBusiness employeeBusiness, ICustomSMSTypeBusiness customSMSTypeBusiness,
            IClassProfileBusiness classProfileBusiness, IAcademicYearBusiness academicYearBusiness, ISMSParentContractBusiness SMSParentContractBusiness,
            ISchoolProfileBusiness schoolProfileBusiness)
        {
            this.contactGroupBusiness = contactGroupBusiness;
            this.smsHistoryBusiness = smsHistorySMSBusiness;
            this.teacherContactBusiness = teacherContactBusiness;
            this.smsTypeBusiness = smsTypeBusiness;
            this.PupilProfileBusines = pupilProfileBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.customSMSTypeBusiness = customSMSTypeBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SMSParentContractBusiness = SMSParentContractBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }
        #endregion
        #region Action
        public ActionResult Index()
        {
            setViewData();
            return View();
        }
        private void setViewData()
        {
            int schoolID = _globalInfo.SchoolID.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int appliedLevelID = _globalInfo.AppliedLevel.Value;
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"schoolID", schoolID},
                {"applyLevel", appliedLevelID},
                {"academicYearID", academicYearID}
            };
            List<SMS_TEACHER_CONTACT_GROUP> contactGroupList = contactGroupBusiness.Search(dic).OrderByDescending(p => p.IS_DEFAULT).ToList();
            IDictionary<string, object> dicClass = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel.Value}
            };
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            ClassProfile objCP = null;
            List<SelectListItem> classLs = new List<SelectListItem>();
            List<SelectListItem> contactGroupLs = new List<SelectListItem>();
            SelectListItem objSelectListItem = null;
            SMS_TEACHER_CONTACT_GROUP objContactGroup = null;
            for (int i = 0, size = listClass.Count; i < size; i++)
            {
                objSelectListItem = new SelectListItem();
                objCP = listClass[i];
                objSelectListItem.Text = objCP.DisplayName;
                objSelectListItem.Value = objCP.ClassProfileID.ToString();
                classLs.Add(objSelectListItem);
            }
            for (int i = 0, size = contactGroupList.Count; i < size; i++)
            {
                objSelectListItem = new SelectListItem();
                objContactGroup = contactGroupList[i];
                objSelectListItem.Text = objContactGroup.CONTACT_GROUP_NAME;
                objSelectListItem.Value = objContactGroup.CONTACT_GROUP_ID.ToString();
                contactGroupLs.Add(objSelectListItem);
            }
            //Lay danh sach giao vien trong truong
            List<TeacherBO> teacherList = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID, false);

            //Fix cứng nhà trường là user trường gửi
            List<string> teacherAutoCompleteList = new List<string>() { GlobalConstantsEdu.HISTORY_SCHOOL_NAME_COMPLETE };
            if (teacherList != null) teacherAutoCompleteList.AddRange(teacherList.Select(a => a.FullName).OrderBy(a => a).ToList());
            List<SMSTypeBO> smsTypeList = smsTypeBusiness.GetSMSTypeBySchool(GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT, schoolID, _globalInfo.AppliedLevel.Value, false, true);
            if (_globalInfo.IsNewSchoolModel)
            {

            }
            else
            {
                // Loai bo cac ban tin VNEN
                smsTypeList = smsTypeList.Where(o => o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE &&
                            o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE && o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE).ToList();
            }

            //viethd31: lay cac ban tin truong tu dinh nghia
            List<SMS_CUSTOM_TYPE> lstCustomType = customSMSTypeBusiness.All.Where(o => o.SCHOOL_ID == _globalInfo.SchoolID && o.GRADE == _globalInfo.AppliedLevel.Value).OrderBy(o => o.CREATE_TIME).ToList();
            SMS_CUSTOM_TYPE customType;
            for (int i = 0, size = lstCustomType.Count; i < size; i++)
            {
                SMSTypeBO typeBO = new SMSTypeBO();
                customType = lstCustomType[i];
                typeBO.Name = customType.TYPE_NAME;
                typeBO.TypeID = customType.CUSTOM_SMS_TYPE_ID;
                typeBO.TypeCode = customType.TYPE_CODE;
                typeBO.IsCustom = true;
                smsTypeList.Add(typeBO);
            }


            ViewData[StatisticsCommunicationConstants.Visible] = _CONST_VISIBLE;
            ViewData[StatisticsCommunicationConstants.LIST_CONTACTGROUP] = contactGroupLs;
            ViewData[StatisticsCommunicationConstants.LIST_CLASS] = classLs;
            ViewData[StatisticsCommunicationConstants.LIST_SMS_TYPE] = smsTypeList;
            ViewData[StatisticsCommunicationConstants.LIST_PUPIL] = new SelectList(new string[] { });
            ViewData[StatisticsCommunicationConstants.LIST_TEACHER] = teacherAutoCompleteList;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal && _globalInfo.EmployeeID != 0)
            {
                TeacherBO tc = teacherList.FirstOrDefault(o => o.TeacherID == _globalInfo.EmployeeID);
                string tcName = tc != null ? tc.FullName : null;
                ViewData[StatisticsCommunicationConstants.CURRENT_TEACHER_NAME] = tcName;
            }

        }
        [HttpPost]
        public ActionResult ViewReceiverPopup(string senderGroup)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID;
            dic["senderGroup"] = long.Parse(senderGroup);
            IQueryable<TeacherBO> iq = smsHistoryBusiness.GetListReceiverBySenderGroup(dic);
            int receiveSMSNum = iq.Where(o => !string.IsNullOrEmpty(o.Mobile)).Count();
            int TotalRecord = iq.Count();
            List<TeacherBO> lstTeacherBO = new List<TeacherBO>();
            if (TotalRecord > 0)
            {
                ViewData[StatisticsCommunicationConstants.NUM_TEACHER_SMS] = receiveSMSNum;
                ViewData[StatisticsCommunicationConstants.Total] = TotalRecord;
            }
            ViewData[StatisticsCommunicationConstants.SENDER_GROUP] = senderGroup;
            return PartialView("_ListReceiverPopup");
        }
        public ActionResult ViewReceiverGrid(string senderGroup)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID;
            dic["senderGroup"] = long.Parse(senderGroup);
            IQueryable<TeacherBO> iquery = smsHistoryBusiness.GetListReceiverBySenderGroup(dic);
            int TotalRecord = iquery.Count();

            if (TotalRecord > 0)
            {
                List<TeacherBO> ReceiverBOList = iquery.Skip(0).Take(StatisticsCommunicationConstants.PageSizePopUp).ToList();
                ViewData[StatisticsCommunicationConstants.LST_RECEIVER] = ReceiverBOList;
                ViewData[StatisticsCommunicationConstants.Total] = TotalRecord;
            }
            else
            {
                ViewData[StatisticsCommunicationConstants.LST_RECEIVER] = new List<TeacherBO>();
                ViewData[StatisticsCommunicationConstants.Total] = 0;
            }
            ViewData[StatisticsCommunicationConstants.SENDER_GROUP] = senderGroup;

            return PartialView("_gridReceiverList");
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult ShowReceiverList(string senderGroup, GridCommand command)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID;
            dic["senderGroup"] = long.Parse(senderGroup);
            ViewData[StatisticsCommunicationConstants.SENDER_GROUP] = senderGroup;
            IQueryable<TeacherBO> iquery = smsHistoryBusiness.GetListReceiverBySenderGroup(dic);
            List<TeacherBO> lstSMS = iquery.Skip((command.Page - 1) * StatisticsCommunicationConstants.PageSizePopUp).Take(StatisticsCommunicationConstants.PageSizePopUp).ToList();
            return View(new GridModel<TeacherBO>()
            {
                Data = lstSMS,
                Total = iquery.Count()
            });
        }
        public JsonResult AjaxLoadPupil(int classID = 0)
        {
            List<PupilProfileBO> pupilProfileList = new List<PupilProfileBO>();
            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevel",_globalInfo.AppliedLevel}
            };
            if (classID > 0)
            {
                List<int> lstClassID = new List<int>();
                lstClassID.Add(classID);
                dic.Add("lstClassID",lstClassID);
                pupilProfileList = PupilProfileBusines.GetListPupilProfile(dic).ToList();
                if (pupilProfileList != null && pupilProfileList.Count > 0)
                {
                    pupilProfileList = pupilProfileList.Where(p => p.ProfileStatus != GlobalConstantsEdu.COMMON_PUPIL_STATUS_MOVED_TO_OTHER_CLASS).ToList();
                }
            }
            else
            {
                pupilProfileList = PupilProfileBusines.GetListPupilProfile(dic)
                    .Where(p => p.ProfileStatus != GlobalConstantsEdu.COMMON_PUPIL_STATUS_MOVED_TO_OTHER_CLASS).ToList();
            }
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<PupilProfileBO> lstParentContractBO = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, pupilProfileList).ToList();
            return Json(new SelectList(lstParentContractBO, _CONST_PUPILFILE_ID, _CONST_FULL_NAME));
        }
        public PartialViewResult AjaxLoadTeacher(int contactGroupID)
        {
            //danh sach giao vien nam trong contact group           
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ContactGroupID",contactGroupID},
                    {"SchoolID",_globalInfo.SchoolID}
                };
                List<EmployeeBO> TeacherInGroupList = EmployeeBusiness.GetListTeacherByContractGroup(dic);
                ViewData[StatisticsCommunicationConstants.LIST_TEACHER] = TeacherInGroupList.Count > 0 ? TeacherInGroupList.Select(p => p.FullName).Distinct().ToList() : new List<string>();
            }
            else
            {
                Employee objE = EmployeeBusiness.Find(_globalInfo.EmployeeID);
                ViewData[StatisticsCommunicationConstants.CURRENT_TEACHER_NAME] = objE != null ? objE.FullName : "";
                ViewData[StatisticsCommunicationConstants.LIST_TEACHER] = new List<string>();
            }
            
            return PartialView("_TeacherAutoComplete");
        }
        #endregion
        #region Thống kê tin nhắn cho phụ huynh học sinh
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchParentSMSAjax(DateTime FromDate, DateTime ToDate, int ClassSchool, int PupilSchool, int SMSTypeID, GridCommand command, int total, bool Flag, int isCustomType)
        {
            List<SMSCommunicationBO> lstSMS = this.SearchParentSMS(FromDate, ToDate, ClassSchool, PupilSchool, SMSTypeID, command, Flag, isCustomType,ref total);
            return View(new GridModel<SMSCommunicationBO>()
            {
                Data = lstSMS,
                Total = total
            });
        }

        /// <summary>
        /// nhấn tìm kiếm
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="ClassSchool"></param>
        /// <param name="PupilSchool"></param>
        /// <param name="SMSTypeID"></param>
        /// <param name="command"></param>
        /// <param name="Flag"></param>
        /// <returns></returns>
        public PartialViewResult _GridParentSMS(DateTime FromDate, DateTime ToDate, int ClassSchool, int PupilSchool, int SMSTypeID, GridCommand command, bool Flag, int IsCustomType)
        {
            ViewData[StatisticsCommunicationConstants.LIST_SMSPARENT] = this.SearchParentSMS(FromDate, ToDate, ClassSchool, PupilSchool, SMSTypeID, command, Flag, IsCustomType,ref total);
            return PartialView();
        }

        /// <summary>
        /// Search thống kê tin nhắn cho phụ huynh học sinh
        /// </summary>
        /// <modifier date="2013/11/25" by="HaiVT">Tuning</modifier>
        public List<SMSCommunicationBO> SearchParentSMS(DateTime fromDate, DateTime toDate, int classID, int ProfileID, int SMSTypeID, GridCommand command, bool flag, int isCustomType, ref int total)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["classID"] = classID;
            dic["Type"] = GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT;
            dic["pupilID"] = ProfileID;
            dic["fromDate"] = fromDate;
            dic["toDate"] = toDate;
            dic["page"] = command.Page;
            dic["TypeID"] = SMSTypeID;
            dic["year"] = objAy.Year;
            dic["level"] = _globalInfo.AppliedLevel;
            dic["pupilID"] = ProfileID;
            dic["isCustomType"] = isCustomType;

            IDictionary<string, object> dicSearchTeacher = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            List<Employee> teacherList = EmployeeBusiness.Search(dicSearchTeacher).ToList();
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                Employee tc = teacherList.FirstOrDefault(o => o.EmployeeID == _globalInfo.EmployeeID);
                string tcName = tc != null ? tc.FullName : string.Empty;
                dic["teacherID"] = tcName != null ? tcName.Trim() : string.Empty;
            }

            //  dic["levelType"] = GlobalInfo.GetLoginLevelType(); // danh cho cap mam non
            List<SMSCommunicationBO> lstSMSParent = smsHistoryBusiness.GetListNotification(dic, ref total);
            if (lstSMSParent.Count > 0)
            {
                ViewData[StatisticsCommunicationConstants.Visible] = _CONST_ENABLE;
            }
            ViewData[StatisticsCommunicationConstants.Page] = command.Page;
            ViewData[StatisticsCommunicationConstants.Total] = total;
            return lstSMSParent;
        }
        #endregion
        #region Thống kê tin nhắn của giáo viên
        //hàm load dữ liệu trong grid
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchTeacherSMSAjax(int? ContactGroupID, string teacherID, DateTime FromDate, DateTime ToDate, GridCommand command)
        {
            List<SMSCommunicationBO> lstSMS = this.SearchTeacherSMS(ContactGroupID, teacherID, FromDate, ToDate, command, ref total);
            return View(new GridModel<SMSCommunicationBO>()
            {
                Data = lstSMS,
                Total = total
            });
        }
        //Hàm sử dụng cho button tìm kiếm
        public PartialViewResult _GridTeacherSMS(int? ContactGroupID, string teacherID, DateTime FromDate, DateTime ToDate, GridCommand command)
        {
            ViewData[StatisticsCommunicationConstants.LIST_SMSTEACHER] = this.SearchTeacherSMS(ContactGroupID, teacherID, FromDate, ToDate, command, ref total);
            return PartialView();
        }
        public List<SMSCommunicationBO> SearchTeacherSMS(int? ContactGroupID, string teacherID, DateTime FromDate, DateTime ToDate, GridCommand command, ref int total)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("schoolID", _globalInfo.SchoolID);
            dic.Add("fromDate", FromDate);
            dic.Add("toDate", ToDate);
            dic.Add("Type", GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER);
            dic.Add("contactGroupID", ContactGroupID);
            dic.Add("page", command.Page);
            dic.Add("year", objAy.Year);
            if (teacherID == GlobalConstantsEdu.HISTORY_SCHOOL_NAME_COMPLETE)
            {
                dic.Add("IsAdmin", true);
            }
            else
            {
                dic.Add("teacherID", teacherID);
            }

            IDictionary<string, object> dicSearchTeacher = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID}
            };
            List<Employee> teacherList = EmployeeBusiness.Search(dicSearchTeacher).ToList();
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                Employee tc = teacherList.FirstOrDefault(o => o.EmployeeID == _globalInfo.EmployeeID);
                string tcName = tc != null ? tc.FullName : string.Empty;
                dic["teacherID"] = tcName != null ? tcName.Trim() : string.Empty;
            }

            var lstContactGroup = teacherContactBusiness.All;
            List<SMSCommunicationBO> lstSMSTeacher = smsHistoryBusiness.GetListNotification(dic, ref total);
            int defaultGroupID = contactGroupBusiness.All.Where(p => p.SCHOOL_ID == _globalInfo.SchoolID && p.IS_DEFAULT == true).Select(p => p.CONTACT_GROUP_ID).FirstOrDefault();

            if (lstSMSTeacher.Count > 0)
            {
                SMSCommunicationBO tmpObj = null;
                int contactGroupIDTemp;
                for (int i = 0, size = lstSMSTeacher.Count; i < size; i++)
                {
                    tmpObj = lstSMSTeacher[i];
                    contactGroupIDTemp = tmpObj.ContactGroupID.HasValue ? tmpObj.ContactGroupID.Value : 0;
                    if (contactGroupIDTemp == defaultGroupID)
                    {
                        tmpObj.ContactGroupName = tmpObj.ContactGroupName + _CONST_LEFT_PARENTHESIS + tmpObj.TotalReceived + _CONST_RIGHT_PARENTHESIS;
                    }
                    else
                    {
                        tmpObj.ContactGroupName = tmpObj.ContactGroupName + _CONST_LEFT_PARENTHESIS + tmpObj.TotalReceived + _CONST_RIGHT_PARENTHESIS;
                    }
                }
            }
            ViewData[StatisticsCommunicationConstants.Page] = command.Page;
            ViewData[StatisticsCommunicationConstants.Total] = total;
            return lstSMSTeacher;
        }
        #endregion
        #region Load SummarySMS
        public PartialViewResult LoadSummarySMS(DateTime FromDate, DateTime ToDate, int ClassID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("schoolID", _globalInfo.SchoolID);
            dic.Add("fromDate", FromDate);
            dic.Add("toDate", ToDate);
            dic.Add("Type", GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER);
            dic.Add("classID", ClassID);
            SummarySMSBO SummarySMS = smsHistoryBusiness.GetSummarySMS(dic);
            StatisticViewModel statistic = new StatisticViewModel();
            if (total != 0)
            {
                statistic.totalSMS = total;
                statistic.Exchange = SummarySMS.Exchange;
                statistic.HealthDate = SummarySMS.HealthDate;
                statistic.TrainingDate = SummarySMS.TrainingDate;
                statistic.ActivitiDate = SummarySMS.ActivitiDate;
                statistic.DayMark = SummarySMS.DayMark;
                statistic.WeekMark = SummarySMS.WeekMark;
                statistic.MonthMark = SummarySMS.MonthMark;
                statistic.GrowthMonth = SummarySMS.GrowthMonth;
                statistic.PeriodicHealth = SummarySMS.PeriodicHealth;
                statistic.SemesterResult = SummarySMS.SemesterResult;
                statistic.TeacherSMS = SummarySMS.Teacher;
                statistic.ParentSMS = SummarySMS.Parent;
                statistic.System = SummarySMS.System;
            }
            return PartialView("_SummarySMS", statistic);
        }
        #endregion
        #region export excel
        public string CreateExceltoTeacher(int? ContactGroupID, string teacherID, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                string SchoolName = _globalInfo.SchoolName;
                string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + _CONST_TEMPLATE_NAME_TEACHER;
                string templateYear = objAy.Year.ToString() + "-" + (objAy.Year + 1).ToString();
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                // Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                firstSheet.SetCellValue(_CONST_A2, SchoolName.ToUpper());
                string InfoHeader = _CONST_SEMESTER + ((_globalInfo.Semester == 1) ? _CONST_SEMESTER_1 : _CONST_SEMESTER_2) + _CONST_COMMA_SPACE + _CONST_SCHOOL_YEAR + templateYear;
                string fromTo = string.Format(_CONST_FORMAT_DATE, FromDate.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMMYYYY), ToDate.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMMYYYY));
                firstSheet.SetCellValue(_CONST_A5, InfoHeader);
                firstSheet.SetCellValue(_CONST_A6, fromTo);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("schoolID", _globalInfo.SchoolID);
                dic.Add("fromDate", FromDate);
                dic.Add("toDate", ToDate);
                dic.Add("Type", GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER);
                if (teacherID == GlobalConstantsEdu.HISTORY_SCHOOL_NAME_COMPLETE)
                {
                    dic.Add("IsAdmin", true);
                }
                else
                {
                    dic.Add("teacherID", teacherID);
                }
                dic.Add("contactGroupID", ContactGroupID);
                dic.Add("year", objAy.Year);
                dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
                List<SMSCommunicationBO> lstData = smsHistoryBusiness.GetListNotificationTeacherExecl(dic);
                //Xu ly so lieu bao cao
                List<ReportTeacherViewModel> lstSMSTeacher = lstData.GroupBy(x => new { x.ContactGroupName, x.TeacherSenderName, x.UserName, x.EmployeeShortName })
               .Select(y => new ReportTeacherViewModel
               {
                   UserName = y.Key.UserName,
                   ContactGroupName = y.Key.ContactGroupName,
                   TeacherSenderName = y.Key.TeacherSenderName,
                   TotalSMS = y.Sum(a => a.ContentCount),
                   ShortName = y.Key.EmployeeShortName
               }).ToList().OrderBy(p => p.ShortName).ToList();

                string userName = SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value);
                ReportTeacherViewModel TeacherSMS = null;
                var tmpDataRng = sheet2.GetRange(_CONST_A9, _CONST_E9);
                var tmpTotal = sheet2.GetRange(_CONST_A10, _CONST_E10);
                int starRows = 9;
                int SumSMS = 0;
                for (int i = 0, size = lstSMSTeacher.Count; i < size; i++)
                {
                    TeacherSMS = lstSMSTeacher[i];
                    TeacherSMS.TotalSMS = TeacherSMS.TotalSMS.HasValue ? TeacherSMS.TotalSMS.Value : 0;
                    firstSheet.CopyPasteSameSize(tmpDataRng, starRows, 1);
                    firstSheet.SetCellValue(starRows, 1, i + 1);
                    firstSheet.SetCellValue(starRows, 2, string.IsNullOrEmpty(TeacherSMS.TeacherSenderName) ? Res.Get("Common_Label_School") : TeacherSMS.TeacherSenderName);
                    firstSheet.SetCellValue(starRows, 3, ((string.IsNullOrEmpty(TeacherSMS.UserName) && string.IsNullOrEmpty(TeacherSMS.TeacherSenderName)) ? userName : TeacherSMS.UserName));
                    firstSheet.SetCellValue(starRows, 4, TeacherSMS.ContactGroupName);
                    firstSheet.SetCellValue(starRows, 5, TeacherSMS.TotalSMS);
                    SumSMS = SumSMS + TeacherSMS.TotalSMS.Value;
                    starRows++;
                }
                int starRowsSum = 9 + lstSMSTeacher.Count;
                firstSheet.CopyPasteSameSize(tmpTotal, starRowsSum, 1);
                firstSheet.SetCellValue(GlobalConstantsEdu.CHARACTER_E + starRowsSum + string.Empty, SumSMS);
                sheet2.Delete();
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, _CONST_FILE_STREAM);
                string ReportName = _CONST_TEMPLATE_NAME_TEACHER;
                result.FileDownloadName = ReportName;
                string fileKey = string.Format(_CONST_FILE_KEY_FORMAT, DateTime.Now.ToString(_CONST_FILE_KEY_DATE_FORMAT));
                logger.Info(_CONST_LOG_TEXT_TEACHER);
                Session[fileKey] = result;
                return fileKey;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return string.Empty;
            }
        }
        public FileResult ExportExcel(string fileKey)
        {
            if (Session[fileKey] != null)
            {
                var result = (FileStreamResult)Session[fileKey];
                Session[fileKey] = null; //Release resource <-- auto release stream
                return result;
            }
            return null;
        }
        public string ExportExceltoParent(DateTime fromDate, DateTime toDate, int? classID, int? pupilID, int? SMSTypeID)
        {
            try
            {
                AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                string SchoolName = _globalInfo.SchoolName;
                string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + _CONST_TEMPLATE_NAME_PARENT;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                string templateYear = objAy.Year.ToString() + "-" + (objAy.Year + 1).ToString();
                // Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                firstSheet.SetCellValue(_CONST_A2, SchoolName.ToUpper());
                string InfoHeader = _CONST_SEMESTER + ((_globalInfo.Semester == 1) ? _CONST_SEMESTER_1 : _CONST_SEMESTER_2) + _CONST_COMMA_SPACE + _CONST_SCHOOL_YEAR + templateYear;
                string fromTo = string.Format(_CONST_FORMAT_DATE, fromDate.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMMYYYY), toDate.ToString(GlobalConstantsEdu.DATE_FORMAT_DDMMYYYY));
                firstSheet.SetCellValue(_CONST_A5, InfoHeader);
                firstSheet.SetCellValue(_CONST_A6, fromTo);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["schoolID"] = _globalInfo.SchoolID;
                dic["fromDate"] = fromDate;
                dic["toDate"] = toDate;
                dic["classID"] = classID;
                dic["pupilID"] = pupilID;
                dic["TypeID"] = SMSTypeID;
                dic["level"] = _globalInfo.AppliedLevel;
                dic["year"] = objAy.Year;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                int starRows = 10;
                int starRowsPhone = starRows;
                List<SMSCommunicationBO> lstSMSParentContract = smsHistoryBusiness.GetListParentContract(dic);
                //Xu ly so lieu bao cao
                List<ReportParentViewModel> lstSMSParent = lstSMSParentContract.GroupBy(x => new { x.PupilID, x.PupilFileName, x.ClassName, 
                                                                                x.ContractorName, x.PhoneMainContract, x.EmployeeShortName, x.EducaitonLevelID, 
                                                                                x.NumberOrderClass, x.OrderInClass })
               .Select(y => new ReportParentViewModel
               {
                   PupilFileName = y.Key.PupilFileName,
                   ClassName = y.Key.ClassName,
                   ContractorName = y.Key.ContractorName,
                   PhoneMainContract = y.Key.PhoneMainContract,
                   TotalActive = y.Where(p => p.PupilID == y.Key.PupilID).Select(p => p.TotalActive).FirstOrDefault(),
                   TotalManual = y.Where(p => p.PupilID == y.Key.PupilID).Select(p => p.TotalManual).FirstOrDefault(),
                   ShortName = y.Key.EmployeeShortName,
                   EducationLevel = y.Key.EducaitonLevelID,
                   OrderNumber = y.Key.NumberOrderClass,
                   OrderInClass = y.Key.OrderInClass
               }).ToList().OrderBy(p => p.EducationLevel).ThenBy(p => p.OrderNumber).ThenBy(p => p.ClassName)
               .ThenBy(p => p.OrderInClass).ThenBy(p => p.ShortName).ThenBy(p => p.PupilFileName).ToList();

                List<string> lstPhoneMainContract = new List<string>();
                var tmpDataRng = sheet2.GetRange(_CONST_A10, _CONST_G10);
                var tmpTotal = sheet2.GetRange(_CONST_A11, _CONST_G11);
                ReportParentViewModel ParentContract = null;
                StringBuilder phoneMain = null;
                int sumActive = 0;
                int sumManual = 0;
                for (int i = 0; i < lstSMSParent.Count; i++)
                {
                    phoneMain = new StringBuilder();
                    ParentContract = lstSMSParent[i];
                    firstSheet.CopyPasteSameSize(tmpDataRng, starRows, 1);
                    firstSheet.SetCellValue(starRows, 1, i + 1);
                    firstSheet.SetCellValue(starRows, 2, ParentContract.PupilFileName);
                    firstSheet.SetCellValue(starRows, 3, ParentContract.ClassName);
                    firstSheet.SetCellValue(starRows, 4, ParentContract.ContractorName);
                    firstSheet.SetCellValue(starRows, 5, "'" + ParentContract.PhoneMainContract);
                    firstSheet.SetCellValue(starRows, 6, ParentContract.TotalActive);
                    firstSheet.SetCellValue(starRows, 7, ParentContract.TotalManual);
                    sumActive += ParentContract.TotalActive;
                    sumManual += ParentContract.TotalManual;
                    starRows++;
                }
                int starRowsSum = 10 + lstSMSParent.Count;
                firstSheet.CopyPasteSameSize(tmpTotal, starRowsSum, 1);
                firstSheet.SetCellValue(GlobalConstantsEdu.CHARACTER_F + starRowsSum + string.Empty, sumActive);
                firstSheet.SetCellValue(GlobalConstantsEdu.CHARACTER_G + starRowsSum + string.Empty, sumManual);
                sheet2.Delete();
                Stream excel = oBook.ToStream();
                FileStreamResult result = new FileStreamResult(excel, _CONST_FILE_STREAM);
                string ReportName = _CONST_TEMPLATE_NAME_PARENT;
                result.FileDownloadName = ReportName;
                string fileKey = string.Format(_CONST_FILE_KEY_FORMAT, DateTime.Now.ToString(_CONST_FILE_KEY_DATE_FORMAT));
                Session[fileKey] = result;
                return fileKey;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return string.Empty;
            }
        }
        #endregion
    }
}