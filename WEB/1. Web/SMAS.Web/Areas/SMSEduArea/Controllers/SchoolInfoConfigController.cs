﻿using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using System.Text;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class SchoolInfoConfigController:BaseController
    {
        private const string SMAS_CONTROL_NAME = "#smsedu/schoolinfoconfig#";
        private readonly ISchoolConfigBusiness schoolConfigBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISMSParentContractBusiness smsParentContractBusiness;
        public SchoolInfoConfigController(ISchoolConfigBusiness SchoolConfigBusiness, 
            ISMSParentContractBusiness SMSParentContractBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.schoolConfigBusiness = SchoolConfigBusiness;
            this.smsParentContractBusiness = SMSParentContractBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        [HttpGet]
        public ActionResult Index()
        {

            int schoolID = _globalInfo.SchoolID.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;

            //Kiểm tra trường có đăng kí dịch vụ SMS Parent hay không
            if (!SchoolProfileBusiness.IsSMSParentActive(schoolID))
            {
                ViewData[SchoolConfigConstant.MESSAGE] = Res.Get("Label_Error_School_Not_Use_SMS_Parent");
            }
            else
            {
                SetViewData(schoolID);
            }
            // Kiem tra co thuoc tinh cho phep dang kys SP2 online hay ko
            ViewData[SchoolConfigConstant.SHOW_SP2_ONLINE] = false;

            if (!_globalInfo.IsAdmin) ViewData[SchoolConfigConstant.NotIs_Admin_SchoolRole] = SchoolConfigConstant.NotIs_Admin_SchoolRole;
            if (this.Semester > 0) ViewData[SchoolConfigConstant.SHOW_HIDE_BTNSAVE] = "Enabled";
            else ViewData[SchoolConfigConstant.SHOW_HIDE_BTNSAVE] = "Disabled";

            return View();
        }

        /// <summary>
        /// Set default for Index page
        /// </summary>
        /// <param name="schoolID"></param>
        public void SetViewData(int? schoolID)
        {
            int? education = _globalInfo.AppliedLevel;
            ViewData[SchoolConfigConstant.OBJECT_SCHOOLCONFIGS3] = schoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
        }

        /// <summary>
        /// Insert or Update School Config
        /// </summary>
        /// <param name="schoolInfoConfigViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult InsertOrUpdateSearchAllMark(SchoolInfoConfigViewModel schoolInfoConfigViewModel)
        {
            //bool AllMark, bool SearchHKI, bool SearchHKII, bool RegisSLLDT, short PaymentType (default Prepay)
            int schoolID = _globalInfo.SchoolID.Value;

            // AnhVD9 20150819 - Tạm thời chưa sử dụng
            //if (schoolInfoConfigViewModel.PaymentType == GlobalConstants.PAYMENT_TYPE_PREPAY)
            //{
            //    if (smsParentContractBusiness.ExistPaymentTypeContract(GlobalConstants.PAYMENT_TYPE_POSTPAID, schoolID, GlobalInfo.Year))
            //        return Json(new { Type = GlobalConstants.ERROR, Message = Res.Get("Validate_SchoolConfig_ExistContractWithPaymentType") });
            //}
            //else if (schoolInfoConfigViewModel.PaymentType == GlobalConstants.PAYMENT_TYPE_POSTPAID)
            //{
            //    if (smsParentContractBusiness.ExistPaymentTypeContract(GlobalConstants.PAYMENT_TYPE_PREPAY, schoolID, GlobalInfo.Year))
            //        return Json(new { Type = GlobalConstants.ERROR, Message = Res.Get("Validate_SchoolConfig_ExistContractWithPaymentType") });
            //}

            if (schoolInfoConfigViewModel.Prefix != null && schoolInfoConfigViewModel.Prefix.Length > 100)
            {
                return Json(new JsonMessage { Type = GlobalConstantsEdu.ERROR, Message = "Tiếp đầu ngữ không được vượt quá 100 ký tự" });
            }

            SMS_SCHOOL_CONFIG schoolconfigObj = new SMS_SCHOOL_CONFIG();
            schoolconfigObj.SCHOOL_ID = schoolID;
            schoolconfigObj.IS_SHOW_ALL_MARK = schoolInfoConfigViewModel.AllMark;
            schoolconfigObj.IS_SHOW_CAPACITY1 = schoolInfoConfigViewModel.SearchHKI;
            schoolconfigObj.IS_SHOW_CAPACITY2 = schoolInfoConfigViewModel.SearchHKII;
            schoolconfigObj.IS_SHOW_LOCK_MARK = schoolInfoConfigViewModel.SearchLock;
            schoolconfigObj.IS_SHOW_TEACHER_MOBILE = schoolInfoConfigViewModel.ShowTeacherMobile;
            // Dang ky SLLDT qua SMS
            schoolconfigObj.IS_SHOW_REGISTER_SLL = schoolInfoConfigViewModel.RegisterSLL;
            // Kiem tra neu khong phai thuoc tinh cho phep SL2 online thi thiet lap la null
            schoolconfigObj.IS_ALLOW_SEND_HEADTEACHER = schoolInfoConfigViewModel.AllowSendSMSToHeadTeacher;
            schoolconfigObj.IS_ALLOW_SEND_UNICODE = schoolInfoConfigViewModel.AllowSendUnicodeMessage;
            if (schoolInfoConfigViewModel.Prefix == null)
            {
                schoolInfoConfigViewModel.Prefix = string.Empty;
            }

            schoolconfigObj.PRE_FIX = schoolInfoConfigViewModel.Prefix.Trim();

            schoolconfigObj.NAME_DISPLAYING = schoolInfoConfigViewModel.NameDisplay;
            
            bool insertorupdate = schoolConfigBusiness.CheckExitSchoolIDInSchoolConfig(schoolID);
            StringBuilder builder = new StringBuilder();
            if (schoolInfoConfigViewModel.AllMark)
            {
                builder.Append("Tra cứu tất cả con điểm");
            }
            if (schoolInfoConfigViewModel.SearchHKI)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append("Tra cứu kết quả học kỳ I");
            }
            if (schoolInfoConfigViewModel.SearchHKII)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append("Tra cứu kết quả học kỳ II/CN");
            }
            if (schoolInfoConfigViewModel.RegisterSLL)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append("Cho phép PHHS đăng ký SLLĐT qua SMS");
            }
            if (schoolInfoConfigViewModel.AllowSendSMSToHeadTeacher)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append("Cho phép gửi cả GVCN khi nhắn tin trao đổi");
            }
            if (schoolInfoConfigViewModel.AllowSendUnicodeMessage)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append("Cho phép gửi tin nhắn có dấu");
            }
            if (!insertorupdate)
            {
                //VTODO WriteLogInfo("Cấu hình thông tin trường lần đầu thành công.", 0, string.Empty, string.Empty, 0, string.Empty, "Liên lạc - Cấu hình", "3", builder.ToString());
                schoolConfigBusiness.InsertSchoolConfig(schoolconfigObj);
            }
            else
            {
                //VTODO WriteLogInfo("Cập nhật thông tin cấu hình trường.", 0, string.Empty, string.Empty, 0, string.Empty, "Liên lạc - Cấu hình", "3", builder.ToString());
                schoolConfigBusiness.UpdateSchoolConfig(schoolconfigObj);
            }
            return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Message = Res.Get("success_Save") });
        }
    }
}