﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using System.Globalization;
using SMAS.Business.Common.Extension;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class ContactGroupConfigController : BaseController
    {

        //
        // GET: /ContactGroupConfig/ContactGroupConfig/

        /// <summary>
        /// Kiem tra phan quyen tren SMAS2.0 bang control Name
        /// </summary>
        private const string _SMAS_CONTROL_NAME = "#smsedu/teachercontactgroup#";

        private const string _CONST_LOG_TEXT = "them/xoa/sua nhom lien lac";

        private readonly IContactGroupBusiness ContactGroupBusiness;
        private readonly ITeacherContactBusiness TeacherContactBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="contactGroupBusiness"></param>
        /// <param name="teacherContactBusiness"></param>
        public ContactGroupConfigController
            (
            IContactGroupBusiness contactGroupBusiness,
            ITeacherContactBusiness teacherContactBusiness,
            IEmployeeBusiness EmployeeBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IWorkGroupTypeBusiness WorkGroupTypeBusiness
            )
        {
            this.ContactGroupBusiness = contactGroupBusiness;
            this.TeacherContactBusiness = teacherContactBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
        }
        /// <summary>
        /// default page contact group
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>26/04/2013</date>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (_globalInfo.IsAdmin || _globalInfo.IsRolePrincipal)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["schoolID"] = _globalInfo.SchoolID.Value;
                dic["guid"] = _globalInfo.UserAccount.GUID;
                List<SMS_TEACHER_CONTACT_GROUP> lstContactGroup = ContactGroupBusiness.Search(dic).OrderByDescending(o => o.IS_DEFAULT == true ? 1 : 0).ThenBy(o => o.CREATED_DATE).ToList();

                SMS_TEACHER_CONTACT_GROUP objContactGroup = null;
                //mac dinh load default group
                for (int i = 0, size = lstContactGroup.Count; i < size; i++)
                {
                    objContactGroup = lstContactGroup[i];
                    if (objContactGroup.IS_DEFAULT)
                    {
                        ViewData[ContactGroupConstant.CONTACT_GROUP] = new ContactGroupViewModel
                            {
                                AssignTeachersID = objContactGroup.ASSIGN_TEACHERS_ID,
                                ContactGroupID = objContactGroup.CONTACT_GROUP_ID,
                                IsAllowMemberSend = objContactGroup.IS_ALLOW_MEMBER_SEND == true ? true : false,
                                IsAllowSchoolBoardSend = objContactGroup.IS_ALLOW_SCHOOL_BOARD_SEND == true ? true : false,
                                IsDefault = objContactGroup.IS_DEFAULT == true ? true : false,
                                IsLock = objContactGroup.IS_LOCK == true ? true : false,
                                Name = objContactGroup.CONTACT_GROUP_NAME
                            };
                        break;
                    }
                }
                ViewData[ContactGroupConstant.LIST_CONTACT_GROUP] = lstContactGroup;
                var teacherBOList = EmployeeBusiness.GetTeachersOfSchool(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
                if (teacherBOList != null && teacherBOList.Count > 0)
                {
                    ViewData[ContactGroupConstant.COUNT_TEACHER] = teacherBOList.Count();
                }
                else
                {
                    ViewData[ContactGroupConstant.COUNT_TEACHER] = 0;
                }

            }

            SetCboViewData();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateDefaultGroup()
        {
            //20160517_viethd4: Them cac nhom mac dinh truong hop chua co nhom lien lac nao ngoai nhom Toan truong

            SMSEduResultBO rs = ContactGroupBusiness.CreateDefaultContactGroups(_globalInfo.SchoolID.Value, _globalInfo.UserAccount.GUID.Value);
            if (rs.isError)
            {
                return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get(rs.ErrorMsg) });
            }
            else
            {
                return Json(new { Type = GlobalConstants.TYPE_SUCCESS });
            }


        }

        /// <summary>
        /// View detail contact group - update and insert view
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>26/04/2013</date>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ViewDetail(int id)
        {

            SetCboViewData();

            int schoolID = _globalInfo.SchoolID.Value;
            if (id != 0) //update view
            {
                SMS_TEACHER_CONTACT_GROUP objContactGroup = (from cg in ContactGroupBusiness.All
                                                             where cg.CONTACT_GROUP_ID == id
                                                             select cg).FirstOrDefault();
                ContactGroupViewModel cgModel = new ContactGroupViewModel
                {
                    AssignTeachersID = objContactGroup.ASSIGN_TEACHERS_ID,
                    ContactGroupID = objContactGroup.CONTACT_GROUP_ID,
                    IsAllowMemberSend = objContactGroup.IS_ALLOW_MEMBER_SEND,
                    IsAllowSchoolBoardSend = objContactGroup.IS_ALLOW_SCHOOL_BOARD_SEND,
                    IsDefault = objContactGroup.IS_DEFAULT,
                    IsLock = objContactGroup.IS_LOCK,
                    Name = objContactGroup.CONTACT_GROUP_NAME
                };

                List<int> lstAssignedTeacherId = GetIDsFromString(objContactGroup.ASSIGN_TEACHERS_ID);
                List<TeacherBO> lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(schoolID, _globalInfo.AcademicYearID.Value).Where(p => p.IsActive == true).OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();
                List<TeacherBO> lstAssignedTeacher = lstTeacherOfSchool.Where(o => lstAssignedTeacherId.Contains(o.TeacherID)).ToList();
                string strAssignedTeacher = String.Empty;

                for (int i = 0; i < lstAssignedTeacher.Count; i++)
                {
                    strAssignedTeacher += lstAssignedTeacher[i].FullName + " - " + lstAssignedTeacher[i].TeacherCode;
                    if (i < lstAssignedTeacher.Count - 1)
                    {
                        strAssignedTeacher += ", ";
                    }
                }
                cgModel.strAssignedTeacher = strAssignedTeacher;
                ViewData[ContactGroupConstant.CONTACT_GROUP] = cgModel;
                if ((objContactGroup != null) && (objContactGroup.IS_DEFAULT == false))
                {

                    //danh sach giao vien nam trong contact group   
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["schoolID"] = _globalInfo.SchoolID.Value;
                    dic["contactGroupID"] = id;
                    dic["teacherID"] = _globalInfo.EmployeeID;
                    dic["isIgnoringTeacher"] = false;
                    List<TeacherBO> lstTeacherInGroup = TeacherContactBusiness.GetListTeacherInGroup(dic);
                    //danh sach giao vien nam ngoai contact group
                    if (lstTeacherInGroup != null && lstTeacherInGroup.Count > 0)
                    {
                        List<int> lstTeacherID = lstTeacherInGroup.Select(p => p.TeacherID).ToList();
                        // giao vien ngoai nhom
                        ViewData[ContactGroupConstant.TEACHER_OUT_GROUP] = new List<TeacherBO>();
                        // giao vien trong nhom                                                                                    
                        lstTeacherID = lstTeacherOfSchool.Select(p => p.TeacherID).ToList();
                        lstTeacherInGroup = (from i in lstTeacherInGroup
                                             join o in lstTeacherOfSchool on i.TeacherID equals o.TeacherID
                                             select o).ToList();
                        ViewData[ContactGroupConstant.TEACHER_IN_GROUP] = lstTeacherInGroup;
                        ViewData[ContactGroupConstant.COUNT_TEACHER] = lstTeacherInGroup.Count;
                    }
                    else
                    {
                        ViewData[ContactGroupConstant.TEACHER_IN_GROUP] = new List<TeacherBO>();
                        ViewData[ContactGroupConstant.TEACHER_OUT_GROUP] = new List<TeacherBO>();
                        ViewData[ContactGroupConstant.COUNT_TEACHER] = 0;
                    }
                }
                else
                {
                    ViewData[ContactGroupConstant.COUNT_TEACHER] = EmployeeBusiness.GetTeachersOfSchool(schoolID, _globalInfo.AcademicYearID.Value).Where(p => p.IsActive == true).Count();
                }
            }
            else //insert view
            {
                List<TeacherBO> lstTeacherOutGroup = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(schoolID, _globalInfo.AcademicYearID.Value).Where(p => p.IsActive == true).OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();
                ViewData[ContactGroupConstant.CONTACT_GROUP] = new ContactGroupViewModel();
                ViewData[ContactGroupConstant.TEACHER_IN_GROUP] = new List<TeacherBO>();
                ViewData[ContactGroupConstant.TEACHER_OUT_GROUP] = new List<TeacherBO>();
                ViewData[ContactGroupConstant.COUNT_TEACHER] = 0;
            }

            ViewData[ContactGroupConstant.SEARCH_VIEW_MODEL] = new ContactGroupSearchViewModel();

            return PartialView("_detailContactGroupNew");
        }

        [HttpPost]
        public ActionResult Search(ContactGroupSearchViewModel model)
        {
            List<TeacherBO> lstTeacherOfSchool = new List<TeacherBO>();
            ViewData[ContactGroupConstant.RESULT_SIDE] = 1;

            lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value).Where(p => p.IsActive == true).OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();
            lstTeacherOfSchool = FilterData(lstTeacherOfSchool, model);

            return PartialView("_List", lstTeacherOfSchool);
        }

        [HttpPost]
        public ActionResult AddOrCancel(string strTopCheckedId, string strBotCheckedId, string strBotId, int contactGroupId, int type, ContactGroupSearchViewModel searchModel)
        {
            List<TeacherBO> lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value).Where(p => p.IsActive == true).ToList();

            List<TeacherBO> lstTopList;
            List<TeacherBO> lstBotList;


            List<int> lstTopCheckedId = GetIDsFromString(strTopCheckedId);
            List<int> lstBotCheckedId = GetIDsFromString(strBotCheckedId);
            List<int> lstBotId = GetIDsFromString(strBotId);



            //them
            if (type == 1)
            {
                lstBotId.AddRange(lstTopCheckedId);

            }
            //loai
            else
            {
                foreach (int i in lstBotCheckedId)
                {
                    lstBotId.Remove(i);
                }
            }


            lstBotList = lstTeacherOfSchool.Where(o => lstBotId.Contains(o.TeacherID)).ToList();

            lstTopList = FilterData(lstTeacherOfSchool, searchModel).Where(o => !lstBotId.Contains(o.TeacherID)).ToList();

            SMS_TEACHER_CONTACT_GROUP objContactGroup;
            if (contactGroupId != 0)
            {
                objContactGroup = (from cg in ContactGroupBusiness.All
                                   where cg.CONTACT_GROUP_ID == contactGroupId
                                   select cg).FirstOrDefault();
            }
            else
            {
                objContactGroup = new SMS_TEACHER_CONTACT_GROUP();
            }
            ViewData[ContactGroupConstant.CONTACT_GROUP] = new ContactGroupViewModel
            {
                AssignTeachersID = objContactGroup.ASSIGN_TEACHERS_ID,
                ContactGroupID = objContactGroup.CONTACT_GROUP_ID,
                IsAllowMemberSend = objContactGroup.IS_ALLOW_MEMBER_SEND == true ? true : false,
                IsAllowSchoolBoardSend = objContactGroup.IS_ALLOW_SCHOOL_BOARD_SEND == true ? true : false,
                IsDefault = objContactGroup.IS_DEFAULT == true ? true : false,
                IsLock = objContactGroup.IS_LOCK == true ? true : false,
                Name = objContactGroup.CONTACT_GROUP_NAME
            };
            ViewData[ContactGroupConstant.TEACHER_OUT_GROUP] = lstTopList;
            ViewData[ContactGroupConstant.TEACHER_IN_GROUP] = lstBotList;
            ViewData[ContactGroupConstant.SEARCH_VIEW_MODEL] = searchModel;
            SetCboViewData();

            return PartialView("_detailContent");
        }
        /// <summary>
        /// Save contact group
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>26/04/2013</date>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        //[Log]      
        public JsonResult Save(ContactGroupViewModel obj)
        {
            if (ModelState.IsValid)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["schoolID"] = _globalInfo.SchoolID.Value;
                dic["contactGroupID"] = obj.ContactGroupID;
                dic["name"] = obj.Name;
                dic["isLock"] = obj.IsLock;
                dic["isDefault"] = obj.IsDefault;
                dic["lstTeacherInGroupId"] = obj.lstTeacherInGroupId;
                dic["isAllowMemberSend"] = obj.IsAllowMemberSend;
                dic["isAllowSchoolBoardSend"] = obj.IsAllowSchoolBoardSend;
                dic["assignTeachersID"] = obj.AssignTeachersID;
                dic["guid"] = _globalInfo.UserAccount.GUID;
                dic["academicYearID"] = _globalInfo.AcademicYearID.Value;

                SMSEduResultBO objResultBO = ContactGroupBusiness.SaveContactGroup(dic);

                //WriteLogInfo(obj.ArrTeacherInsert + obj.ArrTeacherDelete, 0, string.Empty, string.Empty, 0, string.Empty, "Liên lạc - Nhóm liên lạc", "3", "Lưu nhóm liên lạc " + obj.Name + " Khóa: " + (obj.IsLock ? "Có" : "Không"));

                if (objResultBO.isError)
                {
                    return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get(objResultBO.ErrorMsg) });
                }
                else
                {
                    return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Message = Res.Get("Label_Save_Sussess_ContactGroup"), IsCreate = obj.ContactGroupID == 0, ContactGroupId = objResultBO.Value });
                }
            }
            string jsonErrList = this.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        [HttpGet]
        public ActionResult Assign(int contactGroupId, string strAssignedTeacherID)
        {
            //Lay danh sach giao vien
            List<TeacherBO> lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value)
                .OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();

            List<int> lstAssignedTeacherID = GetIDsFromString(strAssignedTeacherID);
            List<AssignListViewModel> lstModel = lstTeacherOfSchool.Select(o => new AssignListViewModel
                                                                        {
                                                                            EmployeeCode = o.TeacherCode,
                                                                            EmployeeId = o.TeacherID,
                                                                            EmployeeName = o.FullName,
                                                                            SchoolFacultyName = o.SchoolFacultyName,
                                                                            AccountName = o.AccountName,
                                                                            isCheck = lstAssignedTeacherID.Contains(o.TeacherID)
                                                                        }).ToList();
            //Lay danh sach cac nhom da duoc phan cong
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID.Value;
            dic["guid"] = _globalInfo.UserAccount.GUID;
            dic["isGetDefault"] = false;
            List<SMS_TEACHER_CONTACT_GROUP> lstContactGroup = ContactGroupBusiness.Search(dic).OrderBy(o => o.CREATED_DATE).ToList();

            for (int i = 0; i < lstModel.Count; i++)
            {
                AssignListViewModel model = lstModel[i];
                List<SMS_TEACHER_CONTACT_GROUP> lstAssignedGroup = lstContactGroup.Where(o => GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(model.EmployeeId)).ToList();
                List<int> lstAssignedGroupId = lstAssignedGroup.Select(o => o.CONTACT_GROUP_ID).ToList();
                model.lstAssignedGroupId = lstAssignedGroupId;

                string str = String.Empty;
                for (int j = 0; j < lstAssignedGroup.Count; j++)
                {
                    SMS_TEACHER_CONTACT_GROUP cg = lstAssignedGroup[j];
                    str = str + cg.CONTACT_GROUP_NAME;
                    if (j < lstAssignedGroup.Count - 1)
                    {
                        str = str + ", ";
                    }
                }
                model.strAssignedGroup = str;

            }

            //Lay danh sach cac nhom cho combobox
            ViewData[ContactGroupConstant.CBO_CONTACT_GROUP] = new SelectList(lstContactGroup, "CONTACT_GROUP_ID", "CONTACT_GROUP_NAME");
            ViewData[ContactGroupConstant.LIST_ASSIGN_TEACHER] = lstModel;
            ViewData[ContactGroupConstant.STR_ASSIGNED_TEACHER_ID] = strAssignedTeacherID;
            return PartialView("_Assign");
        }

        [HttpPost]
        public ActionResult AssignSearch(string searchText, int? cgId, string strAssignedTeacherID)
        {
            //Lay danh sach giao vien
            List<TeacherBO> lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value)
                .OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();

            //loc du lieu
            if (cgId != null && cgId != 0)
            {
                SMS_TEACHER_CONTACT_GROUP cg = ContactGroupBusiness.Find(cgId);
                List<int> lstAssigedTeacherId = GetIDsFromString(cg.ASSIGN_TEACHERS_ID);

                lstTeacherOfSchool = lstTeacherOfSchool.Where(o => lstAssigedTeacherId.Contains(o.TeacherID)).ToList();
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                lstTeacherOfSchool = lstTeacherOfSchool.Where(o => o.TeacherCode.ToUpper().Contains(searchText.ToUpper())
                    || o.FullName.ToUpper().Contains(searchText.ToUpper())).ToList();
            }

            List<int> lstAssignedTeacherID = GetIDsFromString(strAssignedTeacherID);
            List<AssignListViewModel> lstModel = lstTeacherOfSchool.Select(o => new AssignListViewModel
            {
                EmployeeCode = o.TeacherCode,
                EmployeeId = o.TeacherID,
                EmployeeName = o.FullName,
                SchoolFacultyName = o.SchoolFacultyName,
                AccountName = o.AccountName,
                isCheck = lstAssignedTeacherID.Contains(o.TeacherID)
            }).ToList();



            //Lay danh sach cac nhom da duoc phan cong
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID.Value;
            dic["guid"] = _globalInfo.UserAccount.GUID;
            List<SMS_TEACHER_CONTACT_GROUP> lstContactGroup = ContactGroupBusiness.Search(dic).ToList();

            for (int i = 0; i < lstModel.Count; i++)
            {
                AssignListViewModel model = lstModel[i];
                List<SMS_TEACHER_CONTACT_GROUP> lstAssignedGroup = lstContactGroup.Where(o => GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(model.EmployeeId)).ToList();
                List<int> lstAssignedGroupId = lstAssignedGroup.Select(o => o.CONTACT_GROUP_ID).ToList();
                model.lstAssignedGroupId = lstAssignedGroupId;

                string str = String.Empty;
                for (int j = 0; j < lstAssignedGroup.Count; j++)
                {
                    SMS_TEACHER_CONTACT_GROUP cg = lstAssignedGroup[j];
                    str = str + cg.CONTACT_GROUP_NAME;
                    if (j < lstAssignedGroup.Count - 1)
                    {
                        str = str + ",";
                    }
                }
                model.strAssignedGroup = str;

            }

            //Lay danh sach cac nhom cho combobox
            return PartialView("_AssignList", lstModel);
        }

        [HttpPost]
        public JsonResult SaveAssign(string strAssignedTeacherId, string strCheckedId, string strUncheckedId)
        {
            try
            {
                List<int> lstAssignedId = GetIDsFromString(strAssignedTeacherId);
                List<int> lstCheckedId = GetIDsFromString(strCheckedId);
                List<int> lstUncheckedId = GetIDsFromString(strUncheckedId);

                List<int> lstTeacherId = lstAssignedId.Except(lstUncheckedId).ToList();
                lstTeacherId.AddRange(lstCheckedId);

                List<TeacherBO> lstTeacherOfSchool = EmployeeBusiness.GetListTeacherOfSchoolForContactGroup(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value)
                   .OrderBy(p => p.Name, StringComparer.Create(CultureInfo.InvariantCulture, true)).ThenBy(p => p.FullName, StringComparer.Create(CultureInfo.InvariantCulture, true)).ToList();

                List<TeacherBO> lstTeacher = lstTeacherOfSchool.Where(o => lstTeacherId.Contains(o.TeacherID)).ToList();
                string strName = "";
                string strId = "";

                for (int i = 0; i < lstTeacher.Count; i++)
                {
                    TeacherBO obj = lstTeacher[i];
                    strId += obj.TeacherID + ",";

                    strName += obj.FullName + " - " + obj.TeacherCode;
                    if (i < lstTeacher.Count - 1)
                    {
                        strName += ", ";
                    }
                }

                return Json(new { Type = GlobalConstants.TYPE_SUCCESS, StrId = strId, StrName = strName });
            }
            catch
            {
                return Json(new { Type = GlobalConstants.TYPE_ERROR, StrId = String.Empty, StrName = String.Empty });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int contactGroupId)
        {

            SMSEduResultBO objResultBO = ContactGroupBusiness.DeleteContactGroup(contactGroupId, _globalInfo.SchoolID.Value);

            //WriteLogInfo(obj.ArrTeacherInsert + obj.ArrTeacherDelete, 0, string.Empty, string.Empty, 0, string.Empty, "Liên lạc - Nhóm liên lạc", "3", "Lưu nhóm liên lạc " + obj.Name + " Khóa: " + (obj.IsLock ? "Có" : "Không"));

            if (objResultBO.isError)
            {
                return Json(new { Type = GlobalConstants.TYPE_ERROR, Message = Res.Get(objResultBO.ErrorMsg) });
            }
            else
            {
                return Json(new { Type = GlobalConstants.TYPE_SUCCESS, Message = Res.Get("ContactGroup_Delete_Success") });
            }
        }

        [HttpPost]
        public JsonResult AjaxLoadWorkType(int? workGroupType)
        {
            List<WorkTypeBO> lstWorkType;
            if (workGroupType == 2)//Giao vien
            {
                lstWorkType = new List<WorkTypeBO> { new WorkTypeBO { WorkTypeID = 1, Resolution = "Giáo viên CN" }, new WorkTypeBO { WorkTypeID = 2, Resolution = "Giáo viên BM" } };
            }
            else
            {
                lstWorkType = new List<WorkTypeBO>();
            }

            return Json(new SelectList(lstWorkType, "WorkTypeID", "Resolution"));
        }

        private void SetCboViewData()
        {
            //Lay to bo mon
            List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { }).ToList();
            ViewData[ContactGroupConstant.CBO_SCHOOL_FALCULTY] = new SelectList(lstSf, "SchoolFacultyID", "FacultyName");

            //Doi tuong
            List<WorkGroupType> lstWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>())
                                                                .OrderBy(o => o.Resolution).ToList();
            ViewData[ContactGroupConstant.CBO_WORK_GROUP_TYPE] = new SelectList(lstWorkGroupType, "WorkGroupTypeID", "Resolution");

        }

        private List<TeacherBO> FilterData(List<TeacherBO> lstTeacher, ContactGroupSearchViewModel model)
        {
            var lst = lstTeacher.AsEnumerable<TeacherBO>();
            if (model.SchoolFacultyID != null && model.SchoolFacultyID != 0)
            {
                lst = lst.Where(o => o.SchoolFacultyID == model.SchoolFacultyID);
            }

            if (model.WorkGroupTypeID != null && model.WorkGroupTypeID != 0)
            {
                lst = lst.Where(o => o.WorkGroupTypeID == model.WorkGroupTypeID);
            }

            //truong hop loc GVCN,GVBM
            if (model.WorkTypeID != null && model.WorkTypeID != 0)
            {
                //GVCN
                if (model.WorkTypeID == 1)
                {
                    lst = lst.Where(o => o.IsHeadTeacher);
                }
                else
                {
                    lst = lst.Where(o => o.IsSubjectTecher);
                }
            }

            if (model.ComunityID == ContactGroupConstant.COMUNITY_COMMUNIST_PARTY_MEMBER)
            {
                lst = lst.Where(o => o.IsCommunistPartyMember);
            }
            else if (model.ComunityID == ContactGroupConstant.COMUNITY_SYNDICATE)
            {
                lst = lst.Where(o => o.IsSyndicate);
            }
            else if (model.ComunityID == ContactGroupConstant.COMUNITY_YOUTH_LEAGE_MEMBER)
            {
                lst = lst.Where(o => o.IsYouthLeageMember);
            }

            if (!String.IsNullOrWhiteSpace(model.EmployeeFullName))
            {
                lst = lst.Where(o => o.FullName.ToUpper().Contains(model.EmployeeFullName.Trim().ToUpper()));
            }

            if (!String.IsNullOrWhiteSpace(model.EmployeeCode))
            {
                lst = lst.Where(o => o.TeacherCode.ToUpper().Contains(model.EmployeeCode.Trim().ToUpper()));
            }

            if (model.Genre != null && model.Genre != 0)
            {
                if (model.Genre == GlobalConstantsEdu.GENDER_MALE)
                {
                    lst = lst.Where(o => o.Genre == true);
                }
                else
                {
                    lst = lst.Where(o => o.Genre == false);
                }
            }

            if (model.PhoneNetwork == ContactGroupConstant.PHONE_NETWORK_VIETTEL)
            {
                lst = lst.Where(o => o.Mobile.CheckMobileNumberVT());
            }
            else if (model.PhoneNetwork == ContactGroupConstant.PHONE_NETWORK_OTHER)
            {
                lst = lst.Where(o => !String.IsNullOrEmpty(o.Mobile) && o.Mobile.CheckMobileNumberVT() == false);
            }

            if (!string.IsNullOrEmpty(model.StrInGroupId))
            {
                lst = lst.Where(o => !GetIDsFromString(model.StrInGroupId).Contains(o.TeacherID));
            }

            return lst.ToList();
        }

        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }
        private string GetStringFromIds(List<int> lstId)
        {
            string str = String.Empty;
            for (int i = 0; i < lstId.Count; i++)
            {
                str = str + i;
                if (i < lstId.Count - 1)
                {
                    str = str + ",";
                }
            }
            return str;
        }

        private string GetJsonErrorMessage(ModelStateDictionary ModelState)
        {
            string res = "[";
            foreach (var err in ModelState.Values)
            {
                foreach (var errMsg in err.Errors)
                {
                    if (res != "[")
                        res += ", '" + errMsg.ErrorMessage + "'";
                    else
                        res += "'" + errMsg.ErrorMessage + "'";
                }
            }

            res += "]";

            return res;
        }
    }

}