﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using System.Threading.Tasks;
using SMAS.Web.Areas.SMSEduArea.Models.SendSMSToParentModel;
using SMAS.Business.Common;
using SMAS.Business.Common.Extension;
using System.Text;
using SMAS.VTUtils.Log;
using System.Globalization;
using Telerik.Web.Mvc;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class SendSMSToParentController : BaseController
    {
        #region register contructor
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISMSTypeBusiness SMSTypeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly IMonthCommentsBusiness MonthCommentsBusiness;
        private readonly ITimerConfigBusiness TimerConfigBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly ISMSParentContractBusiness SMSParentContractBusiness;
        private readonly ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly ICustomSMSTypeBusiness CustomSMSTypeBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISMSTypeDetailBusiness SMSTypeDetailBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ITimerTransactionBusiness TimerTransactionBusiness;
        private readonly ISMSParentSentDetailBusiness SMSParentSentDetailBusiness;
        public SendSMSToParentController(ISchoolProfileBusiness schoolProfileBusiness, IAcademicYearBusiness academicYearBusiness, ISMSTypeBusiness SMSTypeBusiness
            , IClassProfileBusiness classProfileBusiness, ISchoolConfigBusiness schoolConfigBusiness, IMonthCommentsBusiness monthCommentsBusiness,
            ITimerConfigBusiness timerConfigBusiness, IPupilProfileBusiness pupilProfileBusiness, ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness,
            ISMSHistoryBusiness SMSHistoryBusiness, ISMSParentContractBusiness SMSParentContractBusiness, IPeriodDeclarationBusiness periodDeclarationBusiness,
            IMarkRecordBusiness markRecordBusiness, ICalendarBusiness calendarBusiness, IPupilRankingBusiness pupilRankingBusiness, IExaminationBusiness examinationBusiness,
            ICustomSMSTypeBusiness customSMSTypeBusiness, IEmployeeBusiness employeeBusiness, ISMSTypeDetailBusiness SMSTypeDetailBusiness, IPupilOfClassBusiness pupilOfClassBusiness,
            IEvaluationCommentsBusiness evaluationCommentsBusiness, IRatedCommentPupilBusiness ratedCommentPupilBusiness, IGoodChildrenTicketBusiness goodChildrenTicketBusiness,
            IEducationLevelBusiness educationLevelBusiness, ITimerTransactionBusiness timerTransactionBusiness, ISMSParentSentDetailBusiness SMSParentSentDetailBusiness)
        {
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SMSTypeBusiness = SMSTypeBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolConfigBusiness = schoolConfigBusiness;
            this.MonthCommentsBusiness = monthCommentsBusiness;
            this.TimerConfigBusiness = timerConfigBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.SMSParentContractBusiness = SMSParentContractBusiness;
            this.SMSParentContractInClassDetailBusiness = SMSParentContractInClassDetailBusiness;
            this.PeriodDeclarationBusiness = periodDeclarationBusiness;
            this.MarkRecordBusiness = markRecordBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.PupilRankingBusiness = pupilRankingBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.CustomSMSTypeBusiness = customSMSTypeBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SMSTypeDetailBusiness = SMSTypeDetailBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EvaluationCommentsBusiness = evaluationCommentsBusiness;
            this.RatedCommentPupilBusiness = ratedCommentPupilBusiness;
            this.GoodChildrenTicketBusiness = goodChildrenTicketBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.TimerTransactionBusiness = timerTransactionBusiness;
            this.SMSParentSentDetailBusiness = SMSParentSentDetailBusiness;
        }
        #endregion
        #region Action View
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        private void SetViewData()
        {

            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            List<SMSTypeBO> lstSMSType = SMSTypeBusiness.GetSMSTypeBySchool(SMAS.Business.Common.GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);
            ViewData[SendSMSToParentConstants.LIST_SMS_TYPE] = lstSMSType;
            if (!objSP.IsNewSchoolModel.HasValue || (objSP.IsNewSchoolModel.HasValue && !objSP.IsNewSchoolModel.Value))
            {
                // Loai bo cac ban tin VNEN
                ViewData[SendSMSToParentConstants.LIST_SMS_TYPE] = lstSMSType.Where(o => o.TypeCode.Trim() != SMAS.Business.Common.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE &&
                            o.TypeCode.Trim() != SMAS.Business.Common.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE && o.TypeCode.Trim() != SMAS.Business.Common.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE).ToList();
            }
            //Lay cac ban tin tu dinh nghia
            List<SMSTypeBO> lstCustomType = SMSTypeBusiness.GetCustomSMSTypeBySchool(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, false);

            if (!objSP.IsNewSchoolModel.HasValue && !objSP.IsNewSchoolModel.Value)
            {
                // Loai bo cac ban tin VNEN
                lstCustomType = lstCustomType.Where(o => o.IsForVNEN == false).ToList();
            }
            ViewData[SendSMSToParentConstants.LIST_CUSTOM_TYPE] = lstCustomType;

            //Load EducationLevel
            IQueryable<ClassBO> iquery = null;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    HeadTeacherID = o.HeadTeacherID,
                    EducationName = o.EducationLevel.Resolution,
                });
            }
            else // GVCN 
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"TeacherByRoleID", _globalInfo.EmployeeID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    HeadTeacherID = o.HeadTeacherID,
                    EducationName = o.EducationLevel.Resolution,
                });
            }
            var lstEducationLevel = (from cp in iquery
                                     group cp by new
                                     {
                                         cp.EducationLevelID,
                                         cp.EducationName
                                     } into g
                                     select new
                                     {
                                         g.Key.EducationLevelID,
                                         g.Key.EducationName
                                     }).OrderBy(p => p.EducationLevelID).ToList();
            ViewData[SendSMSToParentConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "EducationName");
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            if (sc != null) ViewData[SendSMSToParentConstants.IS_ALLOW_SEND_UNICODE_MSG] = sc.IS_ALLOW_SEND_UNICODE;
            ViewData[SendSMSToParentConstants.PREFIX_NAME_OF_SCHOOL] = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, true);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime fromDate = new DateTime(aca.FirstSemesterStartDate.Value.Year, aca.FirstSemesterStartDate.Value.Month, 1);
            DateTime toDate = new DateTime(aca.SecondSemesterEndDate.Value.Year, aca.SecondSemesterEndDate.Value.Month, 1);
            ViewData[SendSMSToParentConstants.FROM_DATE] = fromDate;
            ViewData[SendSMSToParentConstants.TO_DATE] = toDate;
        }
        public PartialViewResult AjaxLoadGridClass(int EducationLevelID, int TypeID, bool isCustomType)
        {
            List<SMSClassViewModel> lstResult = new List<SMSClassViewModel>();
            SMSClassViewModel objResult = new SMSClassViewModel();
            IQueryable<ClassBO> iquery = null;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    HeadTeacherID = o.HeadTeacherID,
                    OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0,
                    IsVNEN = o.IsVnenClass.HasValue ? o.IsVnenClass.Value : false
                });
            }
            else // GVCN 
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"TeacherByRoleID", _globalInfo.EmployeeID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    HeadTeacherID = o.HeadTeacherID,
                    OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0,
                    IsVNEN = o.IsVnenClass.HasValue ? o.IsVnenClass.Value : false
                });
            }
            if (EducationLevelID > 0)
            {
                iquery = iquery.Where(p => p.EducationLevelID == EducationLevelID);
            }
            if (isCustomType)
            {
                SMS_CUSTOM_TYPE objCT = CustomSMSTypeBusiness.Find(TypeID);
                if (objCT.FOR_VNEN)
                {
                    iquery = iquery.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value);
                }
            }
            else
            {
                SMS_TYPE objType = SMSTypeBusiness.Find(TypeID);
                if (objType != null && ("NXTVNEN".Equals(objType.TYPE_CODE.Trim()) || "KQHKVNEN".Equals(objType.TYPE_CODE.Trim()) || "NXCKVNEN".Equals(objType.TYPE_CODE.Trim())))
                {
                    iquery = iquery.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value);
                }
            }

            List<ClassBO> listClass = iquery.OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderID).ThenBy(p => p.ClassName).ToList();
            List<int?> lstHeadTeacherID = listClass.Select(p => p.HeadTeacherID).Distinct().ToList();
            List<Employee> lstEmployee = EmployeeBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID && lstHeadTeacherID.Contains(p.EmployeeID)).ToList();
            Employee objE = null;
            ClassBO objCP = null;
            List<int> lstClassID = listClass.Select(p => p.ClassID).Distinct().ToList();
            // Thông tin về quỹ tin của lớp
            ViewData[SendSMSToParentConstants.CURRENT_ACADEMIC_YEAR] = true;
            var contractInClassDetail = SMSParentContractInClassDetailBusiness.GetListSMSParentContractClass(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstClassID);
            var contractInClassDetailOfAll = SMSParentContractInClassDetailBusiness.GetListSMSParentContractClass(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL, lstClassID);
            List<SMSParentContractClassBO> lstPhoneNumber = SMSParentContractInClassDetailBusiness.GetListPhoneByListClass(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, _globalInfo.Semester.Value);
            int countTotalSMS = 0;
            int countSendTotal = 0;
            int countPhone = 0;
            int countPhoneWithStatus = 0;
            for (int i = 0; i < listClass.Count; i++)
            {
                objCP = listClass[i];
                objResult = new SMSClassViewModel();
                var contracttmp = contractInClassDetail.Where(p => p.CLASS_ID == objCP.ClassID).FirstOrDefault();
                var contractalltmp = contractInClassDetailOfAll.Where(p => p.CLASS_ID == objCP.ClassID).FirstOrDefault();
                countPhone = lstPhoneNumber.Where(p => p.CLASS_ID == objCP.ClassID).Select(p => p.PUPIL_ID).Distinct().Count();
                countPhoneWithStatus = lstPhoneNumber.Where(p => p.CLASS_ID == objCP.ClassID && (p.STATUS == SMAS.Web.Constants.GlobalConstantsEdu.STATUS_REGISTED
                                        || p.STATUS == SMAS.Web.Constants.GlobalConstantsEdu.STATUS_UNPAID)).Select(p => p.PUPIL_ID).Distinct().Count();
                countTotalSMS = (contracttmp != null ? contracttmp.TOTAL_SMS : 0) + (contractalltmp != null ? contractalltmp.TOTAL_SMS : 0);
                countSendTotal = (contracttmp != null ? contracttmp.SENT_TOTAL : 0) + (contractalltmp != null ? contractalltmp.SENT_TOTAL : 0);
                objE = lstEmployee.Where(p => p.EmployeeID == objCP.HeadTeacherID).FirstOrDefault();
                objResult.ClassID = objCP.ClassID;
                objResult.ClassName = objCP.ClassName;
                objResult.HeadTeacherName = objE != null ? objE.FullName : "";
                objResult.TotalSMS = countTotalSMS;
                objResult.SendTotal = countSendTotal;
                objResult.RegisterNumber = countPhone;
                objResult.RegisterNumberStatus = countPhoneWithStatus;
                objResult.SMSRemain = countTotalSMS - countSendTotal;
                lstResult.Add(objResult);
            }
            ViewData[SendSMSToParentConstants.LIST_PUPIL] = lstResult;
            return PartialView("_GridSMSClass");
        }
        public PartialViewResult AjaxLoadGridViewDetail(int EducationLevelID, int TypeID, string StrClassID, int? semester, int? periodID, int? examID, bool? IsSignMsg, bool? IsSendImport, string FromDate, string ToDate, bool? IsTimerUsed, Guid? timerConfigId, bool? IsSendSMSToNotViolatePupil, string SMSToNotViolateContent)
        {
            if (TypeID == 0)
            {
                throw new BusinessException("Thầy/cô chưa chọn bản tin");
            }
            bool isPaging = true;
            List<SendSMSToParentViewModel> lstPupilResult = new List<SendSMSToParentViewModel>();
            int Total = 0;
            if (!string.IsNullOrEmpty(StrClassID))
            {
                List<int> lstClassID = StrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
                if (lstClassID.Count == 1)
                {
                    isPaging = false;
                    ViewData[SendSMSToParentConstants.CLASS_ID] = lstClassID.FirstOrDefault();
                }
                lstPupilResult = this.LoadSMS(EducationLevelID, TypeID, StrClassID, semester, periodID, examID, FromDate, ToDate, 1, out Total, isPaging, SMSToNotViolateContent, IsSendImport, IsSignMsg, IsTimerUsed, timerConfigId, IsSendSMSToNotViolatePupil);
            }
            bool isNewMessage = lstPupilResult.Where(p => p.isNewMessage).Count() > 0 ? true : false;
            ViewData[SendSMSToParentConstants.IS_NEW_MESSAGE] = isNewMessage;
            ViewData[SendSMSToParentConstants.LIST_PUPIL] = lstPupilResult;
            ViewData[SendSMSToParentConstants.IS_PAGING] = isPaging;
            ViewData[SendSMSToParentConstants.IS_CUSTOM_PREFIX] = false;
            ViewData[SendSMSToParentConstants.IS_TIMMER_CONFIG] = timerConfigId.HasValue ? true : false;
            ViewData[SendSMSToParentConstants.STR_CLASS_ID] = StrClassID;
            return PartialView("_GridViewDetailPupil");
        }
        public PartialViewResult AjaxLoadGridViewDetailCustomize(string strClassID, int EducationLevelID, int TypeID, int? MonthID, int Semester = 0, bool? IsSignMsg = false, bool iscustom = false, string fromDate = null, string toDate = null, int? year = null, string monthMN = null, int week = 0, string dateMN = null)
        {
            bool isPaging = true;
            List<SendSMSToParentViewModel> lstPupilResult = new List<SendSMSToParentViewModel>();
            int Total = 0;
            if (!string.IsNullOrEmpty(strClassID))
            {
                List<int> lstClassID = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
                if (lstClassID.Count == 1)
                {
                    isPaging = false;
                    ViewData[SendSMSToParentConstants.CLASS_ID] = lstClassID.FirstOrDefault();
                }
                lstPupilResult = this.LoadSMSCustomize(strClassID, EducationLevelID, TypeID, MonthID, 1, out Total, isPaging, Semester, IsSignMsg, iscustom, fromDate, toDate, year, monthMN, week, dateMN);
            }
            bool isNewMessage = lstPupilResult.Where(p => p.isNewMessage).Count() > 0 ? true : false;
            ViewData[SendSMSToParentConstants.IS_NEW_MESSAGE] = isNewMessage;
            ViewData[SendSMSToParentConstants.LIST_PUPIL] = lstPupilResult;
            ViewData[SendSMSToParentConstants.IS_PAGING] = isPaging;
            ViewData[SendSMSToParentConstants.IS_CUSTOM_PREFIX] = true;
            ViewData[SendSMSToParentConstants.STR_CLASS_ID] = strClassID;
            return PartialView("_GridViewDetailPupil");
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(GridCommand command, int EducationLevelID, int TypeID, string StrClassID, int? semester, int? periodID, int? examID, bool? IsSignMsg, bool? IsSendImport, string FromDate, string ToDate, bool? IsTimerUsed, Guid? timerConfigId, bool? IsSendSMSToNotViolatePupil, string SMSToNotViolateContent)
        {
            List<SendSMSToParentViewModel> list = null;
            int PageNum = command.Page;
            int Total = 0;
            if (!string.IsNullOrEmpty(StrClassID))
            {
                List<int> lstClassID = StrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
                if (lstClassID.Count == 1)
                {
                    ViewData[SendSMSToParentConstants.CLASS_ID] = lstClassID.FirstOrDefault();
                }
                list = this.LoadSMS(EducationLevelID, TypeID, StrClassID, semester, periodID, examID, FromDate, ToDate, PageNum, out Total, true, SMSToNotViolateContent, IsSendImport, IsSignMsg, IsTimerUsed, timerConfigId, IsSendSMSToNotViolatePupil);
            }
            return View(new GridModel<SendSMSToParentViewModel>()
            {
                Data = list,
                Total = Total
            });
        }
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjaxCustomize(GridCommand command, string strClassID, int EducationLevelID, int TypeID, int? MonthID, int Page, int Semester = 0, bool? IsSignMsg = false, bool iscustom = false, string fromDate = null, string toDate = null, int? year = null, string monthMN = null, int week = 0, string dateMN = null)
        {
            List<SendSMSToParentViewModel> list = null;
            int PageNum = command.Page;
            int Total = 0;
            if (!string.IsNullOrEmpty(strClassID))
            {
                List<int> lstClassID = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
                if (lstClassID.Count == 1)
                {
                    ViewData[SendSMSToParentConstants.CLASS_ID] = lstClassID.FirstOrDefault();
                }
                list = this.LoadSMSCustomize(strClassID, EducationLevelID, TypeID, MonthID, PageNum, out Total, true, Semester, IsSignMsg, iscustom, fromDate, toDate, year, monthMN, week, dateMN);
            }
            return View(new GridModel<SendSMSToParentViewModel>()
            {
                Data = list,
                Total = Total
            });
        }
        [HttpPost]
        public JsonResult LoadJsonMarkConfigTime()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"Semester",_globalInfo.Semester}
            };
            List<PeriodDeclaration> data = PeriodDeclarationBusiness.Search(dic).OrderBy(p => p.FromDate).ToList();
            // lay dot diem gan nhat
            PeriodDeclaration markConfigLately = data.Where(p => p.Semester == _globalInfo.Semester && p.Year <= DateTime.Now.Year && p.FromDate.Value.Day <= DateTime.Now.Day)
                                    .OrderByDescending(p => p.FromDate).FirstOrDefault();
            int? MarkConfigIDLately = 0;
            if (markConfigLately != null) MarkConfigIDLately = markConfigLately.PeriodDeclarationID;
            List<MarkTimeConfig> lst = new List<MarkTimeConfig>();
            PeriodDeclaration objMarkConfigByTime = null;
            for (int i = 0, iSize = data.Count; i < iSize; i++)
            {
                objMarkConfigByTime = data[i];
                lst.Add(new MarkTimeConfig()
                {
                    MarkTimeConfigID = objMarkConfigByTime.PeriodDeclarationID,
                    MarkTimeName = objMarkConfigByTime.Resolution,
                    IsNearest = (MarkConfigIDLately.HasValue && MarkConfigIDLately == objMarkConfigByTime.PeriodDeclarationID ? true : false)
                });
            }
            return Json(lst);
        }
        public JsonResult CheckAutoSendSMSType(int TypeID)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime endTimeSemester1 = objAy.FirstSemesterEndDate.Value;
            DateTime endTimeSemester2 = objAy.SecondSemesterEndDate.Value;
            // kiem tra loai ban tin gui tu dong
            SMS_TYPE_DETAIL smsTypeDetail = SMSTypeDetailBusiness.All.Where(a => a.SMS_TYPE_ID == TypeID && a.SCHOOL_ID == _globalInfo.SchoolID).FirstOrDefault();
            SMS_TYPE objType = SMSTypeBusiness.Find(TypeID);
            if (smsTypeDetail != null)
            {
                if (smsTypeDetail.IS_AUTO_SEND)
                {
                    DateTime dateTimeNow = DateTime.Now;
                    string SendInfo = String.Empty;
                    if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DAYMARK_TYPECODE)) // diem ngay
                    {
                        if (dateTimeNow.Hour >= smsTypeDetail.SEND_TIME.Value.Hour && dateTimeNow.Minute >= smsTypeDetail.SEND_TIME.Value.Minute)
                        {
                            dateTimeNow = dateTimeNow.AddDays(1);
                        }
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE +
                            dateTimeNow.ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                    }
                    else if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_WEEKMARK_TYPECODE)
                            || objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_WEEK_TYPECODE)) // diem tuan,chuyen can tuan
                    {
                        DateTime temp = DateTime.Now;
                        for (int i = 0; i <= 7; i++)
                        {
                            if ((int)temp.DayOfWeek == 0 && smsTypeDetail.SEND_BEFORE == 8)
                            {
                                break;
                            }
                            else
                            {
                                if ((int)temp.DayOfWeek == smsTypeDetail.SEND_BEFORE - 1)
                                {
                                    break;
                                }
                            }
                            temp = temp.AddDays(1);
                        }
                        //int diff = smsType.SendTime.Value.DayOfWeek - DateTime.Now.DayOfWeek;
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE +
                            temp.ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                    }
                    else if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_MONTHSMS_TYPECODE)
                            || objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_MONTH_TYPECODE)) // diem thang,chuyen can thang
                    {
                        int DayInMon = DateTime.DaysInMonth(dateTimeNow.Year, dateTimeNow.Month);
                        int Day = smsTypeDetail.SEND_BEFORE > DayInMon ? DayInMon : smsTypeDetail.SEND_BEFORE;
                        if (dateTimeNow.Day > Day)
                        {
                            dateTimeNow = dateTimeNow.AddMonths(1);
                        }
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE +
                            Day + SMAS.Web.Constants.GlobalConstantsEdu.SYNTAX_CROSS_LEFT + dateTimeNow.Month + SMAS.Web.Constants.GlobalConstantsEdu.SYNTAX_CROSS_LEFT + dateTimeNow.Year;
                    }
                    else if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_SEMESTERSMS_TYPECODE)
                        || (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EXAM_MARK_SEMESTER))) // diem hoc ky
                    {
                        if (_globalInfo.Semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                        {
                            dateTimeNow = endTimeSemester1;
                        }
                        else if (_globalInfo.Semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                        {
                            dateTimeNow = endTimeSemester2;
                        }
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE
                            + dateTimeNow.AddDays(smsTypeDetail.SEND_BEFORE * -1).ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                    }
                    else if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_TYPECODE)//ren luyen|| bao bai
                        || objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_LECTUREREGISTER_TYPECODE))
                    {
                        if (dateTimeNow.Hour >= smsTypeDetail.SEND_TIME.Value.Hour && dateTimeNow.Minute >= smsTypeDetail.SEND_TIME.Value.Minute)
                        {
                            dateTimeNow = dateTimeNow.AddDays(1);
                        }
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE
                            + dateTimeNow.ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                    }
                    else if (objType.TYPE_CODE.Trim().Equals(SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_CALENDAR_TYPECODE))//thoi khoa bieu
                    {
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_HHmm) + SMAS.Web.Constants.GlobalConstantsEdu.SPACE
                         + dateTimeNow.ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                    }
                    else
                    {
                        SendInfo = smsTypeDetail.SEND_TIME.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_ES);
                    }

                    string thongbao = String.Format(Res.Get("Communication_SMSToParentPupil_AutoSendInformation"), SendInfo);
                    return Json(new { success = true, IsAuto = true, content = thongbao });
                }
                else return Json(new { success = true, IsAuto = false });
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Communication_SMSToParentPupil_InvalidMessageType"), SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
            }
        }
        [ValidateAntiForgeryToken]
        public JsonResult SendSMSToParent(SMSDataModel data)
        {
            string TypeCode = string.Empty;
            SMS_TYPE SMSType;
            SMS_CUSTOM_TYPE customType = null;
            if (!data.IsCustom)
            {
                SMSType = SMSTypeBusiness.Find(data.TypeID);
                if (SMSType == null) return Json(new JsonMessage(Res.Get("Communication_Message_NotActive"), SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                TypeCode = SMSType.TYPE_CODE.Trim();
            }
            else
            {
                customType = CustomSMSTypeBusiness.All.FirstOrDefault(o => o.CUSTOM_SMS_TYPE_ID == data.TypeID && o.IS_ACTIVE);
                if (customType == null) return Json(new JsonMessage(Res.Get("Communication_Message_NotActive"), SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                TypeCode = customType.TYPE_CODE.Trim();
            }
            Dictionary<string, object> DicSendSMS = new Dictionary<string, object>();

            try
            {
                bool res;
                ResultBO result = new ResultBO();
                if (String.Compare(TypeCode, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE) == 0)
                {
                    return CreateSMSTraoDoi(data, out res, data.IsSendImport.GetValueOrDefault());
                }
                else
                {
                    SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
                    string prefixTemplate = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
                    string content = string.Empty;
                    string shortContent = string.Empty;

                    List<string> lstContent = new List<string>();
                    List<string> lstShortContent = new List<string>();
                    AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                    int schoolID = _globalInfo.SchoolID.Value;
                    int academicYearID = _globalInfo.AcademicYearID.Value;
                    int grade = _globalInfo.AppliedLevel.Value;
                    int year = objAy.Year;
                    int semester = _globalInfo.Semester.Value;
                    List<int> lstClassID = data.strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
                    if (!data.isLoadSMS)
                    {
                        List<PupilOfClassBO> lstPupil = PupilProfileBusiness.getListPupilByListPupilID(data.PupilIDs, _globalInfo.AcademicYearID.Value).ToList();

                        if (data.RemoveSpecialChar)
                        {
                            for (int i = 0; i < data.Contents.Count; i++)
                            {
                                data.Contents[i] = data.Contents[i].RemoveUnprintableChar();
                            }
                        }

                        bool isContentError = false;
                        List<ContentErrorViewModel> lstError = new List<ContentErrorViewModel>();

                        //kiem tra noi dung tin nhan chua ky tu dac biet
                        for (int i = 0; i < data.Contents.Count; i++)
                        {
                            int pupilId = data.PupilIDs[i];
                            PupilOfClassBO pp = lstPupil.FirstOrDefault(o => o.PupilID == data.PupilIDs[i]);
                            string pupilCode = pp.PupilCode;
                            string pupilName = pp.PupilFullName;
                            string dicContent = data.Contents[i];
                            string unsignedContent = dicContent.StripVNSign();
                            List<char> lstChar = new List<char>();
                            if (!this.IsPrintableString(unsignedContent, lstChar))
                            {
                                isContentError = true;
                                ContentErrorViewModel err = new ContentErrorViewModel();
                                err.PupilCode = pupilCode;
                                err.PupilName = pupilName;
                                err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                                lstError.Add(err);
                            }
                        }

                        if (!isContentError)
                        {
                            for (int i = 0, size = data.Contents.Count; i < size; i++)
                            {


                                content = data.Contents[i];
                                if (!content.StripVNSign().StartsWith(prefixTemplate.StripVNSign()))
                                {
                                    //Dieu chinh lai template noi dung tin nhan theo mẫu [Tên_trường] [Noi dung SMS]
                                    data.Contents[i] = String.Format(SendSMSToParentConstants.SMSFORMAT_SEND, prefixTemplate, content);
                                    content = data.Contents[i];
                                }
                                shortContent = content.Remove(0, prefixTemplate.Length);
                                data.ShortContents.Add(shortContent);
                            }
                            // Apply các loại bản tin # trao đổi
                            result = SendData(data.PupilIDs, data.ClassID, lstClassID, string.Empty, data.TypeID, data.Contents, data.ShortContents, 0, data.AllowSignMessage, false, "", DateTime.Now, true, false, false, false, false);

                            // ghi log goi ws SMAS3
                            //WriteLogInfo("Gửi tin nhắn PHHS" + TypeCode, 0, "ClassID = " + data.ClassID, string.Empty, null, string.Empty);
                        }
                        else
                        {
                            Session[SendSMSToParentConstants.LIST_CONTENT_ERROR] = lstError;
                            return Json(new JsonMessage(string.Empty, "ContentError"));
                        }
                    }
                    else
                    {
                        #region gui nhieu hon 1 lop
                        if (customType != null && customType.PERIOD_TYPE == 4 && data.Semester != 0)
                        {
                            semester = data.Semester;
                        }
                        int Total = 0;
                        List<SendSMSToParentViewModel> lstPupilModel;
                        if (TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_BNTU
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_BNTH
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TDN
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TDT
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_CDSK
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DGSPT
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DGHDN
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_KQGK
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_KQCK
                            || TypeCode == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_NXCK
                            || data.IsCustom)
                        {
                            lstPupilModel = this.LoadSMSCustomize(data.strClassID, data.EducationlevelID, data.TypeID, data.MonthIDLevel, 1, out Total, false, Semester, data.AllowSignMessage, data.IsCustom, data.FromDate, data.ToDate, year, data.MonthMNEdu, data.ComboWeekEdu, data.DateMN);
                        }
                        else
                        {
                            List<int> lstTmp = new List<int>();
                            lstPupilModel = this.LoadSMS(data.EducationlevelID, data.TypeID, data.strClassID, semester, data.ComboTimeMark, data.ComboExamID, data.FromDate, data.ToDate, 1, out Total, false, data.SMSToNotViolateContent, data.IsSendImport, data.AllowSignMessage, data.IsTimerUsed, data.TimerConfigID, data.SendSMSToNotViolatePupil);
                        }

                        if (data.RemoveSpecialChar)
                        {
                            for (int i = 0; i < lstPupilModel.Count; i++)
                            {
                                lstPupilModel[i].Content = lstPupilModel[i].Content.RemoveUnprintableChar();
                            }
                        }
                        List<int> lstPupilID = lstPupilModel.Select(p => p.PupilFileID).Distinct().ToList();
                        IDictionary<string, object> dicPupil = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID},
                            {"lstClassID",lstClassID},
                            {"lstPupilID",lstPupilID}
                        };
                        List<PupilOfClass> lstPupil = PupilOfClassBusiness.Search(dicPupil).ToList();
                        bool isContentError = false;
                        List<ContentErrorViewModel> lstError = new List<ContentErrorViewModel>();
                        //kiem tra noi dung tin nhan chua ky tu dac biet
                        for (int i = 0; i < lstPupilModel.Count; i++)
                        {
                            PupilOfClass pp = lstPupil.FirstOrDefault(o => o.PupilID == lstPupilModel[i].PupilFileID);
                            string pupilCode = pp.PupilProfile.PupilCode;
                            string pupilName = pp.PupilProfile.FullName;
                            string dicContent = lstPupilModel[i].Content;
                            string unsignedContent = dicContent.StripVNSign();
                            List<char> lstChar = new List<char>();
                            if (!this.IsPrintableString(unsignedContent, lstChar))
                            {
                                isContentError = true;
                                ContentErrorViewModel err = new ContentErrorViewModel();
                                err.PupilCode = pupilCode;
                                err.PupilName = pupilName;
                                err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                                lstError.Add(err);
                            }
                        }

                        if (!isContentError)
                        {
                            SendSMSToParentViewModel objSendSMS = null;
                            lstPupilID = new List<int>();
                            bool isTrainingTypeCode = false;
                            string SMSFormat = string.Empty;
                            if (String.Compare(TypeCode, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_TYPECODE, true) == 0) // chuyen can ngay
                            {
                                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_INFO, prefixTemplate, DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                                isTrainingTypeCode = true;
                            }
                            else if (String.Compare(TypeCode, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_WEEK_TYPECODE, true) == 0) // chuyen can tuan
                            {
                                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_WEEK_INFO, prefixTemplate, DateTime.Now.AddDays(-6).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                                isTrainingTypeCode = true;
                            }
                            else if (String.Compare(TypeCode, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_MONTH_TYPECODE, true) == 0) // chuyen can thang
                            {
                                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_MONTH_INFO, prefixTemplate, DateTime.Now.AddMonths(-1).AddDays(1).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                                isTrainingTypeCode = true;

                            }
                            else if (String.Compare(TypeCode, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_EXPEND_TYPECODE, true) == 0) //chuyen can mo rong
                            {
                                DateTime? dateStart = ExtensionMethods.ConvertStringToDate(data.FromDate, SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                                DateTime? dateEnd = ExtensionMethods.ConvertStringToDate(data.ToDate, SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);

                                if (dateStart.HasValue && dateEnd.HasValue)
                                {
                                    if (dateStart.Value.Date == dateEnd.Value.Date)
                                        SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_INFO, prefixTemplate, dateStart.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                                    else SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_EXPEND_INFO, prefixTemplate, dateStart.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), dateEnd.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                                }

                                isTrainingTypeCode = true;
                            }

                            for (int i = 0, count = lstPupilModel.Count; i < count; i++)
                            {
                                objSendSMS = lstPupilModel[i];
                                if (!objSendSMS.isNewMessage) //không gửi cho những học sinh không có tin nhắn mới
                                {
                                    if (isTrainingTypeCode && data.SendSMSToNotViolatePupil)
                                    {
                                        if (!string.IsNullOrWhiteSpace(data.SMSToNotViolateContent))
                                        {
                                            if (data.AllowSignMessage)
                                            {
                                                objSendSMS.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objSendSMS.PupilName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, data.SMSToNotViolateContent);
                                            }
                                            else objSendSMS.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objSendSMS.PupilName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, data.SMSToNotViolateContent).StripVNSign();
                                        }
                                        else
                                        {
                                            return Json(new JsonMessage(Res.Get("Thầy/cô chưa nhập nội dung tin nhắn các trường hợp không vi phạm"), SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                                        }
                                    }
                                    else
                                    {
                                        continue;

                                    }
                                }

                                content = objSendSMS.Content;
                                if (!content.StripVNSign().StartsWith(prefixTemplate.StripVNSign()))
                                {
                                    //Dieu chinh lai template noi dung tin nhan theo mẫu [Tên_trường] [Noi dung SMS]
                                    lstPupilModel[i].Content = String.Format(SendSMSToParentConstants.SMSFORMAT_SEND, prefixTemplate, content);
                                    content = lstPupilModel[i].Content;
                                }
                                shortContent = content.Remove(0, prefixTemplate.Length);
                                lstContent.Add(content);
                                lstShortContent.Add(shortContent);
                                lstPupilID.Add(objSendSMS.PupilFileID);
                            }
                            if (lstPupilID.Count == 0)
                            {
                                return Json(new JsonMessage("Gửi thành công đến 0 phụ huynh học sinh", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SUCCESS));
                            }
                            // Apply các loại bản tin # trao đổi
                            result = SendData(lstPupilID, 0, lstClassID, string.Empty, data.TypeID, lstContent, lstShortContent, data.EducationlevelID, data.AllowSignMessage, false, "", default(DateTime), true, false, false, data.IsCustom, false);

                            //WriteLogInfo("Gửi toàn khối tới PHHS " + TypeCode, 0, "Khối " + data.EducationlevelID, string.Empty, null, string.Empty);
                            // Thong bao cac lop chua gui duoc

                            // Hien thi danh sach cac lop chua gui
                            if (result.LstClassNotSent != null && result.LstClassNotSent.Count > 0)
                            {
                                StringBuilder strClassInfo = new StringBuilder();
                                List<ClassBO> LstClassNotSend = result.LstClassNotSent.OrderByDescending(o => o.ClassName).ToList();
                                for (int i = LstClassNotSend.Count - 1; i >= 0; i--)
                                {
                                    strClassInfo.Append(LstClassNotSend[i].ClassName);
                                    if (i != 0)
                                    {
                                        strClassInfo.Append(SMAS.Web.Constants.GlobalConstantsEdu.COMMA).Append(SMAS.Web.Constants.GlobalConstantsEdu.SPACE);
                                    }
                                }
                                return Json(new { Type = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_INFO, TotalParent = result.NumSendSuccess, ListClass = strClassInfo.ToString() });
                            }
                        }
                        else
                        {
                            Session[SendSMSToParentConstants.LIST_CONTENT_ERROR] = lstError;
                            return Json(new JsonMessage(string.Empty, "ContentError"));
                        }
                        #endregion
                    }
                }
                return Json(new JsonMessage(string.Format("Gửi tin nhắn thành công đến {0} PHHS", result.NumSendSuccess), "success"));
            }
            catch (Exception ex)
            {
                string para = "";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSMSToParent", para, ex);
                return Json(new JsonMessage(SendSMSToParentConstants.CONST_STR_ERROR_SEND_SMS, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
            }
        }
        [HttpPost]
        public JsonResult LoadListExam()
        {
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["SemesterID"] = _globalInfo.Semester;
            SearchInfo["AppliedLevelID"] = _globalInfo.AppliedLevel;
            return Json(ExaminationBusiness.GetExamOfSchool(SearchInfo));
        }
        public JsonResult AjaxLoadMonth()
        {
            List<MonthEvaluationCommentsBO> lstMonth = new List<MonthEvaluationCommentsBO>();
            if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_GRADE_PRIMARY)
            {
                lstMonth = (from m in MonthCommentsBusiness.getMonthCommentsList()
                            where m.MonthID != SMAS.Web.Constants.GlobalConstantsEdu.MonthID11
                            select new MonthEvaluationCommentsBO
                            {
                                MonthID = m.MonthID,
                                MonthName = m.Name
                            }).ToList();
            }
            else if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_GRADE_SECONDARY)
            {
                lstMonth = this.getMonthsReview();
            }
            return Json(lstMonth);
        }
        public JsonResult AjaxLoadMonthMN()
        {
            List<MonthMNBO> lstMonthMN = new List<MonthMNBO>();
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime fromDate = new DateTime(aca.FirstSemesterStartDate.Value.Year, aca.FirstSemesterStartDate.Value.Month, 1);
            DateTime toDate = new DateTime(aca.SecondSemesterEndDate.Value.Year, aca.SecondSemesterEndDate.Value.Month, 1);
            MonthMNBO objMonthMN = null;
            DateTime datetimeNow = DateTime.Now;
            for (DateTime date = fromDate; date.CompareTo(toDate) <= 0; date = date.AddMonths(1))
            {
                objMonthMN = new MonthMNBO();
                objMonthMN.MonthID = date.ToString("MM/yyyy");
                objMonthMN.MonthName = "Tháng " + date.Month;
                objMonthMN.isSelected = datetimeNow.Month == date.Month;
                lstMonthMN.Add(objMonthMN);
            }
            return Json(lstMonthMN);
        }
        public JsonResult LoadWeek(string monthAndYear, int? week)
        {

            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);

            List<KeyValuePair<int, string>> lstWeek = this.GetListWeek(month, year);

            List<object> lst = new List<object>();

            int curWeek = week.HasValue ? week.Value : this.GetCurrentWeek();
            for (int i = 0; i < lstWeek.Count; i++)
            {
                KeyValuePair<int, string> kvp = lstWeek[i];
                lst.Add(new
                {
                    Key = kvp.Key,
                    Value = kvp.Value,
                    IsSelected = kvp.Key == curWeek
                });
            }

            DateTime fromDate, toDate;
            this.GetDateRangeOfWeek(year, month, curWeek, out fromDate, out toDate);
            string dateNote = string.Format("Từ ngày {0} - {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));
            return Json(new { list = lst, dateNote = dateNote });

        }
        public JsonResult GetDateNote(string monthAndYear, int week)
        {
            string[] arrMonthAndYear = monthAndYear.Split(new char[] { '/' });
            int month = Int32.Parse(arrMonthAndYear[0]);
            int year = Int32.Parse(arrMonthAndYear[1]);
            DateTime fromDate, toDate;
            this.GetDateRangeOfWeek(year, month, week, out fromDate, out toDate);
            string dateNote = string.Format("Từ ngày {0} - {1}", fromDate.ToString("dd/MM/yyyy"), toDate.ToString("dd/MM/yyyy"));
            return Json(new { dateNote = dateNote });
        }
        public JsonResult AjaxLoadEducationLevel(int TypeID, bool isCustomType)
        {
            IQueryable<ClassBO> iquery = null;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    EducationName = o.EducationLevel.Resolution,
                    HeadTeacherID = o.HeadTeacherID,
                    OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0,
                    IsVNEN = o.IsVnenClass.HasValue ? o.IsVnenClass.Value : false
                });
            }
            else // GVCN 
            {
                iquery = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"TeacherByRoleID", _globalInfo.EmployeeID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel).Select(o => new ClassBO
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    HeadTeacherID = o.HeadTeacherID,
                    EducationName = o.EducationLevel.Resolution,
                    OrderID = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0,
                    IsVNEN = o.IsVnenClass.HasValue ? o.IsVnenClass.Value : false
                });
            }

            if (isCustomType)
            {
                SMS_CUSTOM_TYPE objCT = CustomSMSTypeBusiness.Find(TypeID);
                if (objCT.FOR_VNEN)
                {
                    iquery = iquery.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value);
                }
            }
            else
            {
                SMS_TYPE objType = SMSTypeBusiness.Find(TypeID);
                if (objType != null && ("NXTVNEN".Equals(objType.TYPE_CODE.Trim()) || "KQHKVNEN".Equals(objType.TYPE_CODE.Trim()) || "NXCKVNEN".Equals(objType.TYPE_CODE.Trim())))
                {
                    iquery = iquery.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value);
                }
            }
            var lstEducationLevel = (from cp in iquery
                                     group cp by new
                                     {
                                         cp.EducationLevelID,
                                         cp.EducationName
                                     } into g
                                     select new
                                     {
                                         g.Key.EducationLevelID,
                                         g.Key.EducationName
                                     }).OrderBy(p => p.EducationLevelID).ToList();
            return Json(new SelectList(lstEducationLevel, "EducationLevelID", "EducationName"));
        }
        private List<SendSMSToParentViewModel> LoadSMS(int EducationLevelID, int TypeID, string strClassID, int? semester, int? periodID, int? examID, string FromDate, string ToDate, int Page, out int Total, bool isPaging, string SMSToNotViolateContent, bool? IsImport = false, bool? IsSignMsg = false, bool? isTimerUsed = false, Guid? timerConfigId = null, bool? IsSendSMSToNotViolatePupil = false)
        {
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<int> lstClassID = new List<int>();
            if (string.IsNullOrEmpty(strClassID))
            {
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicClass["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dicClass["EducationLevelID"] = EducationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                    dicClass["Type"] = SMAS.Business.Common.SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                List<ClassBO> listClass = (from cp in ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                           select new ClassBO
                                           {
                                               ClassID = cp.ClassProfileID,
                                               ClassName = cp.DisplayName,
                                               HeadTeacherID = cp.HeadTeacherID,
                                               EducationLevelID = cp.EducationLevelID,
                                               OrderID = cp.OrderNumber,
                                           }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderID).ThenBy(p => p.ClassName).ToList();
                lstClassID = listClass.Select(p => p.ClassID).Distinct().ToList();
            }
            else
            {
                lstClassID = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList();
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"SemesterID",_globalInfo.Semester},
                {"lstClassID",lstClassID},
                {"Year",objAy.Year}
            };
            IQueryable<PupilProfileBO> iquery = SMSParentContractBusiness.GetSMSParentContract(dic);
            Total = iquery.Distinct().Count();
            ViewData[SendSMSToParentConstants.TOTAL] = Total;
            List<PupilProfileBO> lstPupil = new List<PupilProfileBO>();
            if (isPaging)
            {
                lstPupil = iquery.Skip((Page - 1) * SendSMSToParentConstants.PAGE_SIZE).Take(SendSMSToParentConstants.PAGE_SIZE).ToList();
            }
            else
            {
                lstPupil = iquery.ToList();
            }

            List<PupilProfileBO> lstParentContractBO = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();
            List<SendSMSToParentViewModel> lstModel = new List<SendSMSToParentViewModel>();
            SMS_TYPE objType = SMSTypeBusiness.Find(TypeID);
            ViewData[SendSMSToParentConstants.TYPE_NAME] = objType.SMS_TYPE_NAME;
            string TypeCode = objType != null ? objType.TYPE_CODE : "";
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefix = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, TypeCode.Trim() == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE);
            switch (TypeCode.Trim())
            {
                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DAYMARK_TYPECODE: // diem ngay
                    lstModel = this.GetMarkByType(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstParentContractBO, lstClassID, EducationLevelID, objType, sc, false, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_WEEKMARK_TYPECODE: // diem tuan
                    lstModel = this.GetMarkByType(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstParentContractBO, lstClassID, EducationLevelID, objType, sc, false, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_MONTHSMS_TYPECODE: // diem thang
                    lstModel = this.GetMarkByType(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstParentContractBO, lstClassID, EducationLevelID, objType, sc, true, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_SEMESTERSMS_TYPECODE: // diem HK
                    if (semester.HasValue && semester.Value > 0)
                        lstModel = this.GetSemesterResult(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, EducationLevelID, sc, semester.Value, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_PERIODSMS_TYPECODE: // diem dot
                    lstModel = this.GetMarkRecordTime(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, periodID.HasValue ? periodID.Value : 0, sc, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_CALENDAR_TYPECODE: // TKB
                    lstModel = this.GetCalendarOfPupil(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.Semester.Value, lstParentContractBO, lstClassID, sc, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_TYPECODE: // ren luyen
                    lstModel = this.GetTraningInfo(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, objType, sc, prefix, IsSignMsg.Value, IsSendSMSToNotViolatePupil, SMSToNotViolateContent);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_WEEK_TYPECODE: // ren luyen tuan
                    lstModel = this.GetTraningInfo(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, objType, sc, prefix, IsSignMsg.Value, IsSendSMSToNotViolatePupil, SMSToNotViolateContent);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_MONTH_TYPECODE: // ren luyen thang
                    lstModel = this.GetTraningInfo(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, objType, sc, prefix, IsSignMsg.Value, IsSendSMSToNotViolatePupil, SMSToNotViolateContent);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TRAINING_EXPEND_TYPECODE: // ren luyen mo rong
                    lstModel = this.GetTraningInfoExpend(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, objType, sc, FromDate, ToDate, prefix, IsSignMsg.Value, IsSendSMSToNotViolatePupil, SMSToNotViolateContent);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TYPECODE: // trao doi
                    {
                        if (prefix.EndsWith(SMAS.Web.Constants.GlobalConstantsEdu.SPACE)) prefix = string.Format("{0} ", prefix.Remove(prefix.Length - 1));
                        lstModel = this.Communicate(_globalInfo.SchoolID.Value, lstParentContractBO, prefix, IsImport, IsSignMsg.Value, isTimerUsed, timerConfigId);
                        break;
                    }
                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EXAM_MARK_SEMESTER: // diem HK
                    if (semester.HasValue)
                        lstModel = this.GetExamMarkSemester(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, sc, semester.Value, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EXAM_SCHEDULE_TYPECODE: // Lịch thi
                    lstModel = this.GetInfoExamSchedule(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semester.Value, lstParentContractBO, lstClassID, sc, examID.HasValue ? examID.Value : 0, prefix, IsSignMsg.Value);
                    break;

                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EXAM_RESULT_TYPECODE: // Kết quả kỳ thi
                    lstModel = this.GetExamResult(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstParentContractBO, lstClassID, sc, examID.HasValue ? examID.Value : 0, prefix, IsSignMsg.Value);
                    break;
                case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE:
                    break;
            }
            return lstModel;
        }
        private List<SendSMSToParentViewModel> LoadSMSCustomize(string strClassID, int EducationLevelID, int TypeID, int? MonthID, int Page, out int Total, bool isPaging, int Semester = 0, bool? IsSignMsg = false, bool iscustom = false, string fromDate = null, string toDate = null, int? year = null, string monthMN = null, int week = 0, string dateMN = null)
        {
            List<SendSMSToParentViewModel> lstModel = new List<SendSMSToParentViewModel>();
            List<int> lstClassID = new List<int>();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (string.IsNullOrEmpty(strClassID))
            {
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicClass["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dicClass["EducationLevelID"] = EducationLevelID;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                    dicClass["Type"] = SMAS.Business.Common.SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                List<ClassBO> listClass = (from cp in ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                           select new ClassBO
                                           {
                                               ClassID = cp.ClassProfileID,
                                               ClassName = cp.DisplayName,
                                               HeadTeacherID = cp.HeadTeacherID,
                                               EducationLevelID = cp.EducationLevelID,
                                               OrderID = cp.OrderNumber,
                                           }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderID).ThenBy(p => p.ClassName).ToList();
                lstClassID = listClass.Select(p => p.ClassID).Distinct().ToList();
            }
            else
            {
                lstClassID = strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstClassID",lstClassID},
                {"SemesterID",_globalInfo.Semester},
                {"Year",objAy.Year}
            };
            IQueryable<PupilProfileBO> iquery = SMSParentContractBusiness.GetSMSParentContract(dic);
            Total = iquery.Count();
            ViewData[SendSMSToParentConstants.TOTAL] = Total;
            List<PupilProfileBO> lstPupil = new List<PupilProfileBO>();
            if (isPaging)
            {
                lstPupil = iquery.Skip((Page - 1) * SendSMSToParentConstants.PAGE_SIZE).Take(SendSMSToParentConstants.PAGE_SIZE).ToList();
            }
            else
            {
                lstPupil = iquery.ToList();
            }

            List<PupilProfileBO> lstParentContractBO = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();

            if (lstParentContractBO == null || lstParentContractBO.Count == 0)
            {
                return lstModel;
            }
            if (!iscustom)
            {
                SMS_TYPE SMSType = SMSTypeBusiness.Find(TypeID);
                if (SMSType != null)
                {
                    string TypeCode = SMSType.TYPE_CODE.Trim();
                    DateTime? date;
                    switch (TypeCode)
                    {
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMENTS_MONTH_TYPECODE: // TT30 - Nhan xet thang
                            lstModel = this.GetCommentsByMonth(lstParentContractBO, lstClassID, MonthID.HasValue ? MonthID.Value : 0, Semester, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_SEMESTER_RESULT_TYPECODE: // TT30 - Ket qua hoc ki
                            lstModel = this.GetCommentsByMonth(lstParentContractBO, lstClassID, MonthID.HasValue ? MonthID.Value : 0, Semester, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMENTS_ENDING_TYPECODE: // TT30 - Nhan xet cuoi ky
                            lstModel = this.GetCommentsByMonth(lstParentContractBO, lstClassID, MonthID.HasValue ? MonthID.Value : 0, Semester, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE: // VNEN - - Nhan xet thang
                            lstModel = this.GetCommentVNENSMSType(lstParentContractBO, lstClassID, MonthID.HasValue ? MonthID.Value : 0, _globalInfo.Semester.Value, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE: // VNEN - Ket qua hoc ki
                            lstModel = this.GetCommentVNENSMSType(lstParentContractBO, lstClassID, 0, _globalInfo.Semester.Value, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE: // VNEN - Nhan xet cuoi ky
                            {
                                int monthID = _globalInfo.Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 15 : 16;
                                lstModel = this.GetCommentVNENSMSType(lstParentContractBO, lstClassID, monthID, _globalInfo.Semester.Value, IsSignMsg.Value);
                                break;
                            }
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_KQGK:
                            lstModel = this.GetTT22Result(lstParentContractBO, lstClassID, Semester, 1, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_KQCK:
                            lstModel = this.GetTT22Result(lstParentContractBO, lstClassID, Semester, 2, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TT22_NXCK:
                            lstModel = this.GetTT22Result(lstParentContractBO, lstClassID, Semester, 3, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_BNTU:
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 1, monthMN, week, null, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_BNTH:
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 2, monthMN, week, null, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TDN:
                            date = dateMN.ConvertStringToDate(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 3, monthMN, week, date, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TDT:
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 4, monthMN, week, null, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_CDSK:
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 5, monthMN, week, null, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DGSPT:
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 6, monthMN, week, null, IsSignMsg.Value);
                            break;
                        case SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_DGHDN:
                            date = dateMN.ConvertStringToDate(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                            lstModel = this.GetPreschoolResult(lstParentContractBO, EducationLevelID, lstClassID, 7, monthMN, week, date, IsSignMsg.Value);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    lstModel = this.GetCommonInfo(lstParentContractBO, lstClassID, SendSMSToParentConstants.CHOISE_SMS_MESSAGE, false);
                }
                ViewData[SendSMSToParentConstants.TYPE_NAME] = SMSType.SMS_TYPE_NAME;
            }
            else
            {
                DateTime? FromDate = fromDate.ConvertStringToDate(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                DateTime? ToDate = toDate.ConvertStringToDate(SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
                SMS_CUSTOM_TYPE customType = CustomSMSTypeBusiness.All.FirstOrDefault(o => o.CUSTOM_SMS_TYPE_ID == TypeID && o.IS_ACTIVE);
                if (customType != null)
                {
                    int yearID = MonthID.HasValue && MonthID > 0 ? (MonthID.Value.ToString().Length >= 4 ? Int32.Parse(MonthID.Value.ToString().Substring(0, 4)) : objAy.Year) : objAy.Year;
                    int monthVNEN = MonthID.HasValue && MonthID > 0 ? Int32.Parse(MonthID.Value.ToString().Substring(4, MonthID.Value.ToString().Length - 4)) : 0;
                    lstModel = this.GetCustomSMS(lstParentContractBO, lstClassID, Semester, monthVNEN, yearID, FromDate, ToDate, customType, IsSignMsg.GetValueOrDefault());
                }
                else
                {
                    lstModel = this.GetCommonInfo(lstParentContractBO, lstClassID, SendSMSToParentConstants.CHOISE_SMS_MESSAGE, false);
                }
                ViewData[SendSMSToParentConstants.TYPE_NAME] = customType.TYPE_NAME;
            }
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);

            lstModel.ForEach(o => { if (string.IsNullOrEmpty(prefixName)) { o.Content = FirstCharToUpper(o.Content); } });
            return lstModel;
        }
        public ActionResult LoadTimerConfigCombobox(int? EducationLevelID)
        {
            List<SMS_TIMER_CONFIG> lstTimerConfig;
            lstTimerConfig = GetListTimerConfig();
            JsonResult result = new JsonResult();

            result.Data = lstTimerConfig;
            return result;
        }
        public ActionResult LoadTimerConfig(Guid? id)
        {
            SMS_TIMER_CONFIG tc = TimerConfigBusiness.Find(id);

            //Lay danh sach cau hinh
            List<SMS_TIMER_CONFIG> lstTimerConfig = new List<SMS_TIMER_CONFIG>();//GetListTimerConfig(type, targetId);
            lstTimerConfig = GetListTimerConfig();
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            int count = 0;
            int numSMS = 0;
            if (tc != null && tc.IS_IMPORT.HasValue && tc.IS_IMPORT.Value)
            {
                IQueryable<SMS_HISTORY> iquery = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == partition
                    && o.SCHOOL_ID == _globalInfo.SchoolID
                    && o.SMS_TIMER_CONFIG_ID == id.Value
                    && o.RECEIVE_TYPE == SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID);
                count = iquery.Select(o => o.RECEIVER_ID).Distinct().Count();

                numSMS = count > 0 ? iquery.Sum(o => o.SMS_CNT) : 0;
            }

            if (tc != null)
            {

                return Json(new
                {
                    Type = SMAS.Web.Constants.GlobalConstantsEdu.SUCCESS,
                    SendDate = tc.SEND_TIME.ToString("dd/MM/yyyy"),
                    SendTime = tc.SEND_TIME.ToString("HH:mm"),
                    TimerConfigName = tc.CONFIG_NAME,
                    Content = tc.CONTENT,
                    SendAllClass = tc.SEND_ALL_CLASS,
                    SendUnicode = tc.SEND_UNICODE,
                    IsImport = tc.IS_IMPORT,
                    Number = count,
                    ClassSendID = tc.CLASS_SEND_ID,
                    NumberSMS = numSMS
                });
            }
            else
            {
                return Json(new
                {
                    Type = SMAS.Web.Constants.GlobalConstantsEdu.SUCCESS,
                    SendDate = DateTime.Now.ToString("dd/MM/yyyy"),
                    SendTime = DateTime.Now.AddMinutes(5).ToString("HH:mm"),
                    Content = string.Empty,
                    SendAllClass = true,
                    SendUnicode = false,
                    TimerConfigName = "Hẹn giờ " + (lstTimerConfig.Count + 1),
                    IsImport = false,
                    ClassSendID = "",
                    Number = 0,
                    NumberSMS = numSMS
                });
            }
        }
        [ValidateAntiForgeryToken]
        public ActionResult SaveTimerConfig(SMSDataModel data)
        {
            try
            {
                SMS_TIMER_CONFIG tc = TimerConfigBusiness.Find(data.TimerConfigID);
                if (tc != null)
                {
                    if (DateTime.Compare(DateTime.Now, tc.SEND_TIME) >= 0)
                    {
                        return Json(new JsonMessage("Đã quá thời gian có thể sửa đợt", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                    }
                }

                if (string.IsNullOrWhiteSpace(data.TimerConfigName))
                {
                    return Json(new JsonMessage("Tên đợt là bắt buộc", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));

                }

                if (!tc.IS_IMPORT.HasValue || (tc.IS_IMPORT.HasValue && tc.IS_IMPORT.Value == false))
                {
                    //Thuc hien cong lai quy tin
                    AcademicYear objay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                    List<SMS_TIMER_TRANSACTION> lstTimerTransaction = TimerTransactionBusiness.All.Where(o => o.TIMER_CONFIG_ID == data.TimerConfigID
                    && o.SCHOOL_ID == _globalInfo.SchoolID
                    && o.IS_ACTIVE == true).ToList();
                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = SMSParentContractInClassDetailBusiness.All.Where(c => c.ACADEMIC_YEAR_ID == _globalInfo.AcademicYearID
                                                             && c.SCHOOL_ID == _globalInfo.SchoolID).ToList();

                    int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                    List<SMS_PARENT_SENT_DETAIL> lstSendDetail = SMSParentSentDetailBusiness.All.Where(o => o.PARTITION_ID == partition
                        && o.SCHOOL_ID == _globalInfo.SchoolID && o.YEAR == objay.Year).ToList();

                    for (int i = 0; i < lstTimerTransaction.Count; i++)
                    {
                        SMS_TIMER_TRANSACTION tt = lstTimerTransaction[i];

                        if (tt.TRANSACTION_TYPE == SMAS.Web.Constants.GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS)
                        {
                            SMS_PARENT_CONTRACT_CLASS sc = lstClassDetails.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_CLASS_ID == tt.SMS_PARENT_CONTRACT_CLASS_ID);
                            if (sc != null)
                            {
                                sc.SENT_TOTAL -= tt.SMS_NUMBER_ADDED;
                            }
                        }
                        else
                        {
                            SMS_PARENT_SENT_DETAIL sd = lstSendDetail.FirstOrDefault(o => o.SMS_PARENT_SENT_DETAIL_ID == tt.SMS_PARENT_SENT_DETAIL_ID);
                            if (sd != null && sd.SENT_TOTAL > 0)
                            {
                                sd.SENT_TOTAL -= tt.SENT_TOTAL;
                                if (sd.SENT_TOTAL < 0) sd.SENT_TOTAL = 0;
                            }
                        }
                    }

                    SMSParentContractInClassDetailBusiness.Save();
                    SMSParentSentDetailBusiness.Save();

                    bool res;
                    JsonResult result = this.CreateSMSTraoDoi(data, out res, data.IsSendImport.HasValue ? data.IsSendImport.Value : false);
                    if (res)
                    {
                        IDictionary<string, object> dic = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID}
                        };
                        TimerConfigBusiness.UpdateTimerConfig(dic, data.TimerConfigID, lstTimerTransaction);
                        return result;
                    }
                    else
                    {
                        //tru lai quy tin nhu ban dau
                        for (int i = 0; i < lstTimerTransaction.Count; i++)
                        {
                            SMS_TIMER_TRANSACTION tt = lstTimerTransaction[i];

                            if (tt.TRANSACTION_TYPE == SMAS.Web.Constants.GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS)
                            {
                                SMS_PARENT_CONTRACT_CLASS sc = lstClassDetails.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_CLASS_ID == tt.SMS_PARENT_CONTRACT_CLASS_ID);
                                if (sc != null)
                                {
                                    sc.SENT_TOTAL += tt.SMS_NUMBER_ADDED;
                                }
                            }
                            else
                            {
                                SMS_PARENT_SENT_DETAIL sd = lstSendDetail.FirstOrDefault(o => o.SMS_PARENT_SENT_DETAIL_ID == tt.SMS_PARENT_SENT_DETAIL_ID);
                                if (sd != null)
                                {
                                    sd.SENT_TOTAL = sd.SENT_TOTAL + tt.SENT_TOTAL;
                                }
                            }
                        }

                        SMSParentContractInClassDetailBusiness.Save();
                        SMSParentSentDetailBusiness.Save();

                        return result;
                    }
                }
                else
                {
                    tc.CONFIG_NAME = data.TimerConfigName;

                    DateTime sendTime = default(DateTime);

                    if (!DateTime.TryParseExact(data.SendDate + " " + data.SendTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out sendTime))
                    {
                        return Json(new JsonMessage("Ngày giờ gửi không đúng định dạng.", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                    }

                    if (sendTime < DateTime.Now)
                    {
                        return Json(new JsonMessage("Thời gian này ở trong quá khứ. Thầy/cô đổi sang ngày hoặc giờ khác.", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                    }
                    tc.SEND_TIME = sendTime;
                    tc.CLASS_SEND_ID = data.strClassID;
                    TimerConfigBusiness.Update(tc);
                    TimerConfigBusiness.Save();

                    return Json(new JsonMessage(string.Format(Res.Get("Communication_SMSToParent_Result"), 0)));

                }

            }
            catch (Exception ex)
            {
                
                string para = "";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SaveTimerConfig", para, ex);
                return Json(new JsonMessage(SendSMSToParentConstants.CONST_STR_ERROR_SEND_SMS, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
            }
        }
        public PartialViewResult OpenCancelDialog(Guid timerConfigId)
        {
            SMS_TIMER_CONFIG tc = TimerConfigBusiness.Find(timerConfigId);
            string name = tc != null ? tc.CONFIG_NAME : string.Empty;
            return PartialView("_Cancel", name);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelTimerConfig(Guid timerConfigId)
        {
            SMS_TIMER_CONFIG tc = TimerConfigBusiness.Find(timerConfigId);
            if (tc != null)
            {
                if (DateTime.Compare(DateTime.Now, tc.SEND_TIME) >= 0)
                {
                    return Json(new JsonMessage("Đã quá thời gian có thể sửa đợt", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                }
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            TimerConfigBusiness.DeleteTimerConfig(dic, timerConfigId);

            return Json(new JsonMessage() { Type = SMAS.Web.Constants.GlobalConstantsEdu.SUCCESS, Message = "Hủy cấu hình hẹn giờ thành công" });
        }
        #endregion
        #region Ban tin thuong
        private List<SendSMSToParentViewModel> Communicate(int schoolId, List<PupilProfileBO> lstPupil, string prefix, bool? IsImport = false, bool? IsSignMsg = false, bool? isTimerUsed = false, Guid? timerConfigId = null)
        {

            int partition = UtilsBusiness.GetPartionId(schoolId);
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            string SMSFormat = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
            if (!IsSignMsg.HasValue || (IsSignMsg.HasValue && !IsSignMsg.Value)) SMSFormat = SMSFormat.StripVNSign();
            PupilProfileBO objPupilProfileBO = null;

            List<SMS_HISTORY> lstSmsHistory = new List<SMS_HISTORY>();


            if (isTimerUsed.HasValue && isTimerUsed.Value && timerConfigId != null)
            {
                lstSmsHistory = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == partition
                    && o.SCHOOL_ID == schoolId
                    && o.SMS_TIMER_CONFIG_ID == timerConfigId).ToList();
            }
            List<SendSMSToParentViewModel> lstImportModel = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel objImportModel = null;
            if (IsImport.HasValue && IsImport.Value)
            {
                lstImportModel = (List<SendSMSToParentViewModel>)Session[SendSMSToParentConstants.LIST_IMPORT_ALL];
            }
            for (int i = 0, iSize = lstPupil.Count; i < iSize; i++)
            {
                objPupilProfileBO = lstPupil[i];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.SMSTemplate = SMSFormat;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (isTimerUsed.HasValue && isTimerUsed.Value)
                {
                    SMS_HISTORY sms = lstSmsHistory.Where(o => o.RECEIVER_ID == objPupilProfileBO.PupilProfileID).FirstOrDefault();
                    string smsContent = string.Empty;
                    if (sms != null)
                    {
                        smsContent = sms.SMS_CONTENT;
                    }
                    else
                    {
                        smsContent = SMSFormat;
                    }

                    model.Content = smsContent;
                    model = countSMSCommunicate(model, IsSignMsg.Value);
                }
                else
                {
                    if (IsImport.HasValue && IsImport.Value)
                    {
                        objImportModel = lstImportModel.Where(p => p.PupilFileID == model.PupilFileID).FirstOrDefault();
                        model.Content = objImportModel != null ? objImportModel.Content : SMSFormat;
                    }
                    else
                    {
                        model.Content = SMSFormat;
                    }
                }
                model = countSMSCommunicate(model, IsSignMsg.Value);
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.EducationLevelID = objPupilProfileBO.EducationLevelID;
                // Review:  Recommend recount

                if (IsSignMsg.Value)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetMarkByType(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, List<int> lstClassID, int educationLevelID, SMS_TYPE smsType, SMS_SCHOOL_CONFIG sc, bool checkAbsent, string prefix, bool IsSignMsg)
        {
            DateTime dateTimeNow = DateTime.Now;
            DateTime dateStart = new DateTime();
            DateTime dateEnd = new DateTime();
            // Format tin nhan
            string SMSFormat = string.Empty;
            string SMSContent = string.Empty;
            if (smsType == null) return new List<SendSMSToParentViewModel>();
            string typecode = smsType.TYPE_CODE.Trim();
            if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_DAYMARK_TYPECODE, true) == 0) // diem ngay
            {
                dateStart = dateEnd = dateTimeNow;
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_DAYMARK, prefix, DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }
            else if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_WEEKMARK_TYPECODE, true) == 0) // diem tuan
            {
                dateStart = dateTimeNow.AddDays(-6).Date;
                dateEnd = dateTimeNow.Date;
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_WEEKMARK, prefix, DateTime.Now.AddDays(-6).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DD), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }
            else if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_MONTHSMS_TYPECODE, true) == 0) // diem thang
            {
                dateStart = dateTimeNow.AddMonths(-1).AddDays(1).Date;
                dateEnd = dateTimeNow.Date;
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_MONTHMARK, prefix, DateTime.Now.AddMonths(-1).AddDays(1).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }

            List<PupilMarkBO> lstMarkRecordByClass = MarkRecordBusiness.GetMarkRecordByClass(schoolID, lstClassID, educationLevelID, academicYearID, dateStart, dateEnd, checkAbsent, semester);

            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            int STT = 1;
            if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY) // cap 1
            {
                PupilProfileBO objPupilProfileBO = null;
                for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
                {
                    SMSContent = string.Empty;
                    objPupilProfileBO = lstPupil[k];
                    model = new SendSMSToParentViewModel();
                    model.STT = STT++;
                    model.PupilFileID = objPupilProfileBO.PupilProfileID;
                    model.FullName = objPupilProfileBO.FullName;
                    model.Status = objPupilProfileBO.ProfileStatus;
                    model.ClassName = objPupilProfileBO.ClassName;
                    model.ClassID = objPupilProfileBO.CurrentClassID;
                    model.SMSTemplate = SMSFormat;
                    model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                    model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                    model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                    model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                    if (lstMarkRecordByClass != null) SMSContent = lstMarkRecordByClass.Where(p => p.PupilProfileID == objPupilProfileBO.PupilProfileID).Select(p => p.Content).FirstOrDefault();
                    model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                    if (IsSignMsg)
                    {
                        if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                        {
                            model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                            model.NumOfSubscriber = 0;
                        }
                        else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                    }
                    lstPupilRet.Add(model);
                }
            }
            else if (_globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY) // Cap 2 3
            {
                PupilProfileBO objPupilProfileBO = null;
                for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
                {
                    SMSContent = string.Empty;
                    objPupilProfileBO = lstPupil[k];
                    model = new SendSMSToParentViewModel();
                    model.PupilFileID = objPupilProfileBO.PupilProfileID;
                    model.FullName = objPupilProfileBO.FullName;
                    model.Status = objPupilProfileBO.ProfileStatus;
                    model.ClassName = objPupilProfileBO.ClassName;
                    model.ClassID = objPupilProfileBO.CurrentClassID;
                    model.SMSTemplate = SMSFormat;
                    model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                    model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                    model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                    model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                    if (lstMarkRecordByClass != null) SMSContent = lstMarkRecordByClass.Where(p => p.PupilProfileID == objPupilProfileBO.PupilProfileID).Select(p => p.Content).FirstOrDefault();
                    model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                    if (IsSignMsg)
                    {
                        if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                        {
                            model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                            model.NumOfSubscriber = 0;
                        }
                        else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                    }
                    lstPupilRet.Add(model);
                }
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetSemesterResult(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, int educationLevelID, SMS_SCHOOL_CONFIG sc, int semester, string prefix, bool IsSignMsg)
        {
            // set dot instead of comma
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = SMAS.Web.Constants.GlobalConstantsEdu.SYNTAX_DOT;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            string semesterName = String.Empty;
            if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST || semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
            {
                if (educationLevelID >= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_LEVEL1 && educationLevelID <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_LEVEL6) // Cấp 1
                {
                    if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                    {
                        semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_DASH_CN);
                    }
                    else
                    {
                        semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER1_I);
                    }
                }
                else
                {
                    if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                    {
                        semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER1_I);
                    }
                    else
                    {
                        semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER2_II);
                    }
                }
            }
            else if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL) semesterName = "cả năm";

            String SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_RESULT, prefix, semesterName);

            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            int STT = 1;
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;
            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).Distinct().ToList();
            List<PupilMarkBO> lstMarkRecordSemesterList = MarkRecordBusiness.GetListMarkRecordSemester(schoolID, lstClassID, lstPupilID, academicYearID, semester, _globalInfo.AppliedLevel.Value);
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.SMSTemplate = SMSFormat;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (lstMarkRecordSemesterList != null) SMSContent = lstMarkRecordSemesterList.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetMarkRecordTime(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, int periodID, SMS_SCHOOL_CONFIG sc, string prefix, bool IsSignMsg)
        {
            string semesterName = String.Empty;
            String SMSFormat = String.Empty;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicYearID},
                {"PeriodID",periodID}
            };
            PeriodDeclaration timeMark = PeriodDeclarationBusiness.SearchBySchool(schoolID, dic).FirstOrDefault();
            if (timeMark != null)
            {
                // Format tin nhan
                if (timeMark.Semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST) semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER1_I);
                else semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER2_II);
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_MARKTIME, prefix, timeMark.Resolution, semesterName);
            }
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentViewModel model = null;
            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).Distinct().ToList();
            List<PupilMarkBO> lstMarkRecordTimeList = MarkRecordBusiness.GetListMarkRecordTimeByAllClass(lstClassID, lstPupilID, schoolID, academicYearID, periodID);
            string SMSContent = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                if (lstMarkRecordTimeList != null) SMSContent = lstMarkRecordTimeList.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetCalendarOfPupil(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_SCHOOL_CONFIG sc, string prefix, bool IsSignMsg)
        {
            string SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_CALENDAR, prefix);
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            int STT = 1;
            SendSMSToParentViewModel model = null;
            PupilProfileBO objPupilProfileBO = null;
            string SMSContent = string.Empty;
            List<CalendarBO> lstCalendarBO = CalendarBusiness.GetListCalendarOfPupil(academicYearID, lstClassID, semester);
            CalendarBO objCalendarBO = null;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                objCalendarBO = lstCalendarBO.Where(p => p.ClassID == objPupilProfileBO.CurrentClassID).FirstOrDefault();
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                model.Content = SMSFormat;
                SMSContent = objCalendarBO != null ? objCalendarBO.strCalendar : "";
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetTraningInfo(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_TYPE smsType, SMS_SCHOOL_CONFIG sc, string prefix, bool IsSignMsg, bool? IsSendSMSToNotViolatePupil, string SMSToNotViolateContent)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            DateTime dateTimeNow = DateTime.Now;
            DateTime dateStart = new DateTime();
            DateTime dateEnd = new DateTime();
            // Format tin nhan
            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).Distinct().ToList();
            string SMSFormat = string.Empty;
            List<PupilMarkBO> lstTraningInfo = null;
            string typecode = smsType.TYPE_CODE.Trim();
            if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_TYPECODE, true) == 0) // chuyen can ngay
            {
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_INFO, prefix, DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                lstTraningInfo = PupilRankingBusiness.GetListTraningInfoToday(schoolID, academicYearID, lstClassID, lstPupilID);
            }
            else if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_WEEK_TYPECODE, true) == 0) // chuyen can tuan
            {
                dateStart = dateTimeNow.AddDays(-6).Date;
                dateEnd = dateTimeNow.Date;
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_WEEK_INFO, prefix, DateTime.Now.AddDays(-6).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                if (dateStart.Date >= academicYear.FirstSemesterStartDate.Value.Date) lstTraningInfo = PupilRankingBusiness.GetListTraningInfoByTime(schoolID, academicYearID, lstClassID, lstPupilID, dateStart, dateEnd);
                else lstTraningInfo = PupilRankingBusiness.GetListTraningInfoByTime(schoolID, academicYearID, lstClassID, lstPupilID, academicYear.FirstSemesterStartDate.Value.Date, dateEnd);
            }
            else if (String.Compare(typecode, SMAS.Web.Constants.GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_MONTH_TYPECODE, true) == 0) // chuyen can thang
            {
                dateStart = dateTimeNow.AddMonths(-1).AddDays(1).Date;
                dateEnd = dateTimeNow.Date;
                SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_MONTH_INFO, prefix, DateTime.Now.AddMonths(-1).AddDays(1).ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), DateTime.Now.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                if (dateStart.Date >= academicYear.FirstSemesterStartDate.Value.Date) lstTraningInfo = PupilRankingBusiness.GetListTraningInfoByTime(schoolID, academicYearID, lstClassID, lstPupilID, dateStart, dateEnd);
                else lstTraningInfo = PupilRankingBusiness.GetListTraningInfoByTime(schoolID, academicYearID, lstClassID, lstPupilID, academicYear.FirstSemesterStartDate.Value.Date, dateEnd);
            }

            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            PupilProfileBO objPupilProfileBO = null;
            string SMSContent = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                if (lstTraningInfo != null) SMSContent = lstTraningInfo.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                if (IsSendSMSToNotViolatePupil.HasValue && IsSendSMSToNotViolatePupil.Value)
                {
                    if (string.IsNullOrEmpty(SMSContent))
                    {
                        SMSContent = SMSToNotViolateContent;
                    }
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg, SendSMSToParentConstants.SMS_BREAK_LINE);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else
                    {
                        model.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, SMSContent);
                        model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                    }
                }
                else model.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, SMSContent).StripVNSign();
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetTraningInfoExpend(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_TYPE smsType, SMS_SCHOOL_CONFIG sc, string fromDate, string toDate, string prefix, bool IsSignMsg, bool? IsSendSMSToNotViolatePupil, string SMSToNotViolateContent)
        {
            DateTime? dateStart = ExtensionMethods.ConvertStringToDate(fromDate, SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
            DateTime? dateEnd = ExtensionMethods.ConvertStringToDate(toDate, SMAS.Web.Constants.GlobalConstantsEdu.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY);
            string SMSFormat = string.Empty;
            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).Distinct().ToList();
            if (dateStart.HasValue && dateEnd.HasValue)
            {
                if (dateStart.Value.Date == dateEnd.Value.Date)
                    SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_INFO, prefix, dateStart.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
                else SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_TRAINING_EXPEND_INFO, prefix, dateStart.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM), dateEnd.Value.ToString(SMAS.Web.Constants.GlobalConstantsEdu.DATE_FORMAT_DDMM));
            }
            List<PupilMarkBO> lstTraningInfo = null;
            if (dateStart.HasValue && dateEnd.HasValue) lstTraningInfo = PupilRankingBusiness.GetListTraningInfoByTime(schoolID, academicYearID, lstClassID, lstPupilID, dateStart.Value.Date, dateEnd.Value.Date);

            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            int STT = 1;

            PupilProfileBO objPupilProfileBO = null;
            string SMSContent = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                if (lstTraningInfo != null) SMSContent = lstTraningInfo.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                if (IsSendSMSToNotViolatePupil.HasValue && IsSendSMSToNotViolatePupil.Value)
                {
                    if (string.IsNullOrEmpty(SMSContent))
                    {
                        SMSContent = SMSToNotViolateContent;
                    }
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg, SendSMSToParentConstants.SMS_BREAK_LINE);

                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else
                    {
                        model.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, SMSContent);
                        model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                    }
                }
                else model.Content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), SendSMSToParentConstants.SMS_BREAK_LINE, SMSContent).StripVNSign();
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetExamMarkSemester(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_SCHOOL_CONFIG sc, int semester, string prefix, bool IsSignMsg)
        {
            // set dot instead of comma
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = SMAS.Web.Constants.GlobalConstantsEdu.SYNTAX_DOT;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            List<int> lstPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            var lstMark = MarkRecordBusiness.GetListExamMarkSemester(lstPupil, schoolID, lstClassID, academicYearID, semester, _globalInfo.AppliedLevel.Value);

            string semesterName = String.Empty;
            if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST || semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
            {
                if (semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST) semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER1_I);
                else semesterName = String.Format(SendSMSToParentConstants.SEMESTER_FORMAT_TWO, "HK", SendSMSToParentConstants.CONST_SEMESTER2_II);
            }
            String SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_MARK_SEMESTER, prefix, semesterName);
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                objPupilProfileBO = lstPupil[k];
                if (lstMark != null) SMSContent = lstMark.Where(p => p.PupilProfileID == model.PupilFileID).Select(p => p.Content).FirstOrDefault();
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg, SendSMSToParentConstants.SMS_BREAK_LINE);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetInfoExamSchedule(int schoolID, int academicYearID, int semester, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_SCHOOL_CONFIG sc, int examID, string prefix, bool IsSignMsg)
        {

            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentViewModel model = null;
            List<int> listPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            List<ExamSubjectBO> listResult = ExaminationBusiness.getExamScheduleInfo(listPupilID, schoolID, academicYearID, lstClassID, examID);
            ExamSubjectBO examSubjectBO = null;
            string SMSFormat = String.Empty;
            string SMSContent = String.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSFormat = String.Empty;
                SMSContent = String.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                examSubjectBO = listResult.FirstOrDefault(p => p.PupilID == model.PupilFileID);
                if (examSubjectBO != null && !string.IsNullOrEmpty(examSubjectBO.ScheduleContent))
                {
                    SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_EXAM_SCHEDULE, prefix, examSubjectBO.ExaminationsName);
                    SMSContent = examSubjectBO.ScheduleContent;
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetExamResult(int schoolID, int academicYearID, List<PupilProfileBO> lstPupil, List<int> lstClassID, SMS_SCHOOL_CONFIG sc, int examID, string prefix, bool IsSignMsg)
        {
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            PupilProfileBO objPupilProfileBO = null;
            SendSMSToParentViewModel model = null;
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();
            List<ExamMarkPupilBO> listResult = ExaminationBusiness.getExamResultInfo(listPupilID, schoolID, academicYearID, lstClassID, examID);
            string SMSFormat = string.Empty;
            string SMSContent = string.Empty;
            ExamMarkPupilBO examMarkPupilBO = null;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSFormat = string.Empty;
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT, prefix);
                examMarkPupilBO = listResult.FirstOrDefault(p => p.PupilID == model.PupilFileID);
                if (examMarkPupilBO != null && !string.IsNullOrEmpty(examMarkPupilBO.ExamResult))
                {
                    SMSFormat = String.Format(SendSMSToParentConstants.SMSFORMAT_EXAM_RESULT, prefix, examMarkPupilBO.ExaminationName);
                    SMSContent = examMarkPupilBO.ExamResult;
                }
                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), IsSignMsg);
                if (IsSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        #endregion
        #region Ban tin tu dinh nghia
        private List<SendSMSToParentViewModel> GetCommentsByMonth(List<PupilProfileBO> lstPupil, List<int> lstClassID, int monthID, int semester, bool isSignMsg)
        {
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            SendSMSToParentViewModel model = null;
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            List<int> listPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            List<EvaluationCommentsBO> listEvaluationComments = EvaluationCommentsBusiness.getEvaluationCommnetsBySchoolOfPrimary(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, semester, monthID, _globalInfo.UserAccountID, true);
            int STT = 1;
            string SMSFormat = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSFormat = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.SMSTemplate = SMSFormat;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (listEvaluationComments != null) SMSContent = listEvaluationComments.Where(p => p.PupilID == model.PupilFileID).Select(p => p.EvaluationMonth).FirstOrDefault();

                if (monthID < SMAS.Web.Constants.GlobalConstantsEdu.MonthID11 && monthID > 0)
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENTS_MONTH, prefixName, monthID);
                else if (monthID == SMAS.Web.Constants.GlobalConstantsEdu.MonthID11)
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENTS_RESULT_SEMESTER, prefixName, semester);
                else SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENTS_ENDING, prefixName, semester);

                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), isSignMsg);
                if (isSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetCommonInfo(List<PupilProfileBO> lstPupil, List<int> lstClassID, string smscontent, bool IsImport)
        {
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            int STT = 1;
            PupilProfileBO objPupilFileBO = null;
            for (int i = 0, size = lstPupil.Count; i < size; i++)
            {
                objPupilFileBO = lstPupil[i];
                // khoi tao thong tin 
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilFileBO.PupilProfileID;
                model.FullName = objPupilFileBO.FullName;
                model.PupilCode = objPupilFileBO.PupilCode;
                model.Status = objPupilFileBO.ProfileStatus;
                model.ClassName = objPupilFileBO.ClassName;
                model.ClassID = objPupilFileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilFileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilFileBO.RemainSMS;
                model.ExpireTrial = objPupilFileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilFileBO.NumOfSMSSubscriber;
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetCommentVNENSMSType(List<PupilProfileBO> lstPupil, List<int> lstClassID, int monthID, int semester, bool isSignMsg)
        {
            List<int> listPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
            if (lstClassID.Count != 0)
            {
                listResult = EvaluationCommentsBusiness.getCommentSchoolVNEN(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, semester, monthID);
            }
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            SendSMSToParentViewModel model = null;
            int STT = 1;
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;
            string SMSFormat = string.Empty;
            string month;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                SMSContent = string.Empty;
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = STT++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                model.SMSTemplate = prefixName;
                if (listResult.Count > 0) SMSContent = listResult.Where(p => p.PupilID == model.PupilFileID).Select(p => p.EvaluationMonth).FirstOrDefault();

                if (monthID > 0 && monthID != SMAS.Web.Constants.GlobalConstantsEdu.MonthID_EndSemester1 && monthID != SMAS.Web.Constants.GlobalConstantsEdu.MonthID_EndSemester2)
                {
                    month = monthID.ToString().Substring(4, monthID.ToString().Length - 4);
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENT_MONTH_VNEN, prefixName, month);
                }
                else if (monthID == SMAS.Web.Constants.GlobalConstantsEdu.MonthID_EndSemester1 || monthID == SMAS.Web.Constants.GlobalConstantsEdu.MonthID_EndSemester2)
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENT_ENDING_VNEN, prefixName, semester);
                else SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_COMMENT_RESULT_SEMESTER_VNEN, prefixName, semester);

                model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), isSignMsg);
                if (isSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }
            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetTT22Result(List<PupilProfileBO> lstPupil, List<int> lstClassID, int Semester, int type, bool isSignMsg)
        {
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            SendSMSToParentViewModel model = null;
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            List<long> listPupilID = lstPupil.Select(p => (long)p.PupilProfileID).ToList();
            List<RatedCommentPupilBO> lstRatedCommentPuipil = RatedCommentPupilBusiness.GetListRatedCommentPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, type, Semester);
            int index = 1;
            string SMSFormat = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                if (type == 1)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_TT22_RESULT_MID_SEMESTER, prefixName, Semester);
                }
                else if (type == 2)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_TT22_RESULT_END_SEMESTER, prefixName, (Semester == 2 ? "năm học" : "học kỳ 1"));
                }
                else if (type == 3)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_TT22_COMMENT_SEMESTER, prefixName, Semester);
                }

                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = index++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.SMSTemplate = prefixName;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (lstRatedCommentPuipil != null) SMSContent = lstRatedCommentPuipil.Where(p => p.PupilID == model.PupilFileID).Select(p => p.SMSContent).FirstOrDefault();

                model = countSMSBreakLine(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), isSignMsg);
                if (isSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }
        private List<SendSMSToParentViewModel> GetPreschoolResult(List<PupilProfileBO> lstPupil, int educationLevel, List<int> lstClassID, int type, string month, int week, DateTime? date, bool isSignMsg)
        {
            List<int> listPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();
            List<PreschoolResultBO> lstResult = new List<PreschoolResultBO>();

            DateTime dtMonth = new DateTime();
            DateTime dateFrom = new DateTime();
            DateTime dateTo = new DateTime();
            int physMonth = 0;
            if (!string.IsNullOrEmpty(month))
            {
                string[] arr = month.Split(new char[] { '/' });
                int m = int.Parse(arr[0]);
                int y = int.Parse(arr[1]);

                dtMonth = new DateTime(y, m, 1);

                physMonth = int.Parse(dtMonth.ToString("yyyyMM"));

                if (week > 0)
                {
                    this.GetDateRangeOfWeek(y, m, week, out dateFrom, out dateTo);
                }
            }

            List<PreschoolResultBO> listTmp = new List<PreschoolResultBO>();

            //Be ngoan tuan
            if (type == 1)
            {
                listTmp = GoodChildrenTicketBusiness.GetGoodTicketOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, 1, dtMonth, week);
            }
            else if (type == 2)
            {
                listTmp = GoodChildrenTicketBusiness.GetGoodTicketOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, 2, dtMonth, week);
            }
            //Thuc don ngay
            else if (type == 3)
            {
                listTmp = GoodChildrenTicketBusiness.GetMealMenuOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, 1, date.GetValueOrDefault(), date.GetValueOrDefault());
            }
            //thuc don tuan
            else if (type == 4)
            {
                listTmp = GoodChildrenTicketBusiness.GetMealMenuOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, 2, dateFrom, dateTo);
            }
            //Can do suc khoe
            else if (type == 5)
            {
                listTmp = GoodChildrenTicketBusiness.GetPhysicalResultOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, physMonth);
            }
            //danh gia su phat trien
            else if (type == 6)
            {
                listTmp = GoodChildrenTicketBusiness.GetGrowthEvaluationOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID);
            }
            //danh gia hoat dong ngay
            else if (type == 7)
            {
                listTmp = GoodChildrenTicketBusiness.GetDailyActivityOfPupil(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, date.GetValueOrDefault());
            }

            lstResult = listTmp.Distinct().ToList();

            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            SendSMSToParentViewModel model = null;
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();

            int index = 1;
            string SMSFormat = string.Empty;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                if (type == 1 || type == 2)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_BNTU, prefixName);
                }
                else if (type == 3)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_TDN, prefixName, date.Value.ToString("dd/MM"));
                }
                else if (type == 4)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_TDT, prefixName, dateFrom.ToString("dd/MM"), dateTo.ToString("dd/MM"));
                }
                //can do suc khoe
                else if (type == 5)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_CDSK, prefixName, "tháng " + dtMonth.Month);
                }
                //danh gia su phat trien
                else if (type == 6)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_DGSPT, prefixName);
                }
                //danh gia hoat dong ngay
                else if (type == 7)
                {
                    SMSFormat = string.Format(SendSMSToParentConstants.TEMPLATE_MN_DGHDN, prefixName, date.Value.ToString("dd/MM"));
                }

                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = index++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.SMSTemplate = prefixName;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (lstResult != null) SMSContent = lstResult.Where(p => p.PupilID == model.PupilFileID).Select(p => p.SMSContent).FirstOrDefault();

                if (type == 1 || type == 2)
                {
                    model = countSMS(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), isSignMsg, " ");
                }
                else
                {
                    model = countSMSBreakLine(model, SMSContent, SMSFormat, objPupilProfileBO.FullName.FormatName(sc != null ? sc.NAME_DISPLAYING : null), isSignMsg);
                }
                if (isSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }
        #endregion
        #region Export Import
        public FileResult ExportAll(int hdfEducationLevelID, string hdfstrClassID)
        {
            string fileName = SendSMSToParentConstants.REPORTNAME;
            string template = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + SMAS.Web.Constants.GlobalConstantsEdu.SYNTAX_CROSS_LEFT + fileName;
            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet templateSheet = book.GetSheet(2);

            #region Fill data for sheet
            //templateSheet.SetCellValue("A2", "aa");
            //templateSheet.SetCellValue("B2", className);
            string SchoolName = _globalInfo.SchoolName;
            sheet.SetCellValue("A2", SchoolName.ToUpper());
            string Tittle = SendSMSToParentConstants.CONST_TITTLE_EXCEL;
            sheet.SetCellValue("A4", Tittle);
            string LevelClass = hdfEducationLevelID != 0 ? "Khối " + hdfEducationLevelID : "Khối: Tất cả";
            sheet.SetCellValue("A5", LevelClass);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            sheet.SetCellValue("E6", String.Format("{0}", CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null)));
            List<int> lstClassID = !string.IsNullOrEmpty(hdfstrClassID) ? hdfstrClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList() : new List<int>();
            List<PupilProfileBO> lstPupil = new List<PupilProfileBO>();
            if (lstClassID.Count > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"lstClassID",lstClassID},
                    {"EducationLevelID",hdfEducationLevelID}
                };
                lstPupil = PupilProfileBusiness.GetListPupilProfile(dic).ToList();
            }


            lstPupil = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();

            PupilProfileBO obj;
            List<string> LstContractEachPupil = null;
            int START_ROW = 9;
            int ROW = START_ROW;
            StringBuilder mobile;
            string Fomula;

            for (int i = 0, Count = lstPupil.Count; i < Count; i++)
            {
                obj = lstPupil[i];
                sheet.PasteRange(templateSheet.GetRange("A5", "G5"), sheet.GetRange("A" + ROW, "G" + ROW));
                sheet.SetCellValue("A" + ROW, i + 1);
                sheet.SetCellValue("B" + ROW, obj.PupilCode);
                sheet.SetCellValue("C" + ROW, obj.FullName);
                sheet.SetCellValue(SendSMSToParentConstants.CONST_D + ROW, obj.ClassName);

                mobile = new StringBuilder();
                LstContractEachPupil = obj.PhoneReceiver;
                for (int j = 0; j < LstContractEachPupil.Count; j++)
                {
                    mobile.Append(LstContractEachPupil[j]).Append(SendSMSToParentConstants.CONST_SEMICOLON_SPACE);
                }
                if (mobile.Length > 1) mobile.Remove(mobile.Length - 2, 2);
                sheet.SetCellValue("E" + ROW, "'" + mobile.ToString());
                sheet.SetCellValue("F" + ROW, String.Empty);
                Fomula = "=IF(LEN(F" + ROW + ")=0,\"0\",INT(IF(MOD(LEN(F" + ROW + ")+LEN(E6),160)=0,(LEN(F" + ROW + ")+LEN(E6))/160,(LEN(F" + ROW + ")+LEN(E6))/160+1)))";
                sheet.SetFormulaValue("G" + ROW, Fomula);
                sheet.SetCellValue("H" + ROW, obj.CurrentClassID);
                sheet.HideColumn(8);
                ROW++;
            }
            sheet.GetRange(ROW - 1, 1, ROW - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            // Lock các cột trừ 2 cột: Mã HS, Nội dung
            sheet.GetRange(START_ROW, 1, ROW - 1, 1).IsLock = true;
            sheet.GetRange(START_ROW, 3, ROW - 1, 5).IsLock = true;
            sheet.GetRange(START_ROW, 7, ROW - 1, 7).IsLock = true;
            templateSheet.Delete();
            //templateSheet.ProtectSheet();
            sheet.ProtectSheet();
            #endregion
            Stream excel = book.ToStream();
            FileStreamResult result = new FileStreamResult(excel, SendSMSToParentConstants.CONST_FILE_STREAM);
            result.FileDownloadName = SendSMSToParentConstants.REPORTNAME;
            result.FileDownloadName = string.Format("{0}_{1}.xls", SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value), hdfEducationLevelID != 0 ? "Khoi" + hdfEducationLevelID.ToString() : "Tatca");
            return result;
        }
        public ActionResult ImportExcelEdu(IEnumerable<HttpPostedFileBase> attachmentsEdu, bool isSignMsg, int educationLevel, string strClassID)
        {
            //-------------------------------------------
            //  Check data post has exist 
            //-------------------------------------------
            if (attachmentsEdu == null || attachmentsEdu.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));

            HttpPostedFileBase file = attachmentsEdu.FirstOrDefault();
            //-------------------------------------------
            //  Check the excel mime types
            //-------------------------------------------
            //if (file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            //    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), GlobalConstants.ERROR));
            //-------------------------------------------
            //  Check the excel extension
            //-------------------------------------------
            string extension = Path.GetExtension(file.FileName);
            if (extension.ToUpper() != SMAS.Web.Constants.GlobalConstantsEdu.EXCEL_TYPE_XLS && extension.ToUpper() != SMAS.Web.Constants.GlobalConstantsEdu.EXCEL_TYPE_XLSX)
                return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));
            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            if (!file.InputStream.CanRead)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));

            //-------------------------------------------
            //  Check the excel file max size is 1024 KB
            //-------------------------------------------
            if (file.ContentLength > 1024 * 3072) return Json(new JsonMessage(Res.Get("Validate_MaxSizeExcel_1MB"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));

            string FilePath = string.Empty;
            try
            {
                string UserAccountID = _globalInfo.UserAccount.UserAccountID.ToString();
                string fileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccount.UserAccountID, Guid.NewGuid(), extension);
                string UploadPath = AppDomain.CurrentDomain.BaseDirectory + "/Uploads/Excels"; //(Config.UploadPath ?? string.Empty).ToString();
                FilePath = Path.Combine(UploadPath, fileName);
                file.SaveAs(FilePath);
                IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                //IVTWorksheet templateSheet = oBook.GetSheet(2);
                IVTRange range = sheet.GetRow(1);
                int TITLE_ROW_NUMBER = 8;
                // Check template file import
                string SchoolNameExcel = (string)sheet.GetCellValue(2, 1);
                string Title = (string)sheet.GetCellValue(4, 1);
                string LevelClassExcel = (string)sheet.GetCellValue(5, 1);
                string OrderNumberExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 1);
                string PupilCodeExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 2);
                string PupilNameExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 3);
                string ClassExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 4);
                string PhoneExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 5);
                string ContentExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 6);
                string NumberSMSExcel = (string)sheet.GetCellValue(TITLE_ROW_NUMBER, 7);
                string SchoolName = _globalInfo.SchoolName;
                AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                List<int> lstClassID = !string.IsNullOrEmpty(strClassID) ? strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList() : new List<int>();

                if ((String.Compare(SchoolNameExcel.ToUpper(), SchoolName.ToUpper()) == 0) &&
                    Title.Equals(SendSMSToParentConstants.CONST_TITTLE_EXCEL) &&
                    OrderNumberExcel.Equals(SendSMSToParentConstants.CONST_STT) &&
                    PupilCodeExcel.Equals(SendSMSToParentConstants.CONST_PUPIL_CODE) &&
                    PupilNameExcel.Equals(SendSMSToParentConstants.CONST_PUPIL_NAME) &&
                    ClassExcel.Equals(SendSMSToParentConstants.CONST_CLASS) &&
                    PhoneExcel.Equals(SendSMSToParentConstants.CONST_PHONE) &&
                    ContentExcel.Equals(SendSMSToParentConstants.CONST_CONTENT) &&
                    NumberSMSExcel.Equals(SendSMSToParentConstants.CONST_SUBSCRIBER)
                    )
                {

                    #region Khởi tạo dữ liệu
                    int SchoolID = _globalInfo.SchoolID.Value;
                    int academicYearID = _globalInfo.AcademicYearID.Value;
                    Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["SchoolID"] = SchoolID;
                    SearchInfo["Year"] = objAy.Year;
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"SchoolID",_globalInfo.SchoolID},
                        {"EducationLevel",educationLevel},
                        {"lstClassID",lstClassID}
                    };
                    List<PupilProfileBO> lstPupil = PupilProfileBusiness.GetListPupilProfile(dic).ToList();

                    lstPupil = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();
                    List<string> LstPupilCode = lstPupil.Select(o => o.PupilCode).ToList();
                    #endregion

                    SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
                    string SMSFormat = String.Format(SendSMSToParentConstants.SCHOOLNAME_FORMAT.Trim(), CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null));
                    List<SendSMSToParentViewModel> LstImportPupil = GetCommonInfo(lstPupil, null, SMSFormat, true).ToList();
                    int countPupil = LstImportPupil.Count + 1;
                    SendSMSToParentViewModel PupilViewModel;
                    int START = 8;
                    int numRow = START;
                    string stt = String.Empty;
                    string pupilCode = String.Empty;
                    string fullName = String.Empty;
                    string className = String.Empty;
                    int classId = 0;
                    string mobile = String.Empty;
                    string content = String.Empty;
                    string numberOfSMS = String.Empty;
                    string error = String.Empty;

                    List<string> LstPupilCodeImport = new List<string>();
                    List<string> LstMobileEachPupil = new List<string>();
                    while (sheet.GetCellValue(numRow, 2) != null && sheet.GetCellValue(numRow, 3) != null && sheet.GetCellValue(numRow, 4) != null
                        && sheet.GetCellValue(numRow, 5) != null)
                    {
                        pupilCode = Convert.ToString(sheet.GetCellValue(numRow, 2));
                        fullName = Convert.ToString(sheet.GetCellValue(numRow, 3));
                        className = Convert.ToString(sheet.GetCellValue(numRow, 4));
                        mobile = Convert.ToString(sheet.GetCellValue(numRow, 5));
                        content = Convert.ToString(sheet.GetCellValue(numRow, 6));
                        classId = Convert.ToInt32(sheet.GetCellValue(numRow, 8));

                        if (LstPupilCodeImport.Contains(pupilCode))
                        {
                            PupilViewModel = LstImportPupil.Where(o => o.PupilCode == pupilCode).FirstOrDefault();
                            if (PupilViewModel != null)
                            {
                                PupilViewModel = countSMSImport(PupilViewModel, SMSFormat, "", "", isSignMsg, "");
                                PupilViewModel.IsImport = true;
                                if (isSignMsg)
                                {
                                    if (!checkSupportSendUnicodeSMS(PupilViewModel.LstPhoneReceiver))
                                    {
                                        PupilViewModel.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                                        PupilViewModel.NumOfSubscriber = 0;
                                    }
                                    else PupilViewModel.NumOfSubscriber = PupilViewModel.LstPhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                                }
                            }
                        }
                        else LstPupilCodeImport.Add(pupilCode);

                        if (!string.IsNullOrWhiteSpace(fullName) && !string.IsNullOrEmpty(className) //&& String.Compare(ClassName, className.Trim()) != 0
                                    && LstPupilCode.Contains(pupilCode) && !content.CheckContentSMS())
                        {
                            PupilViewModel = LstImportPupil.FirstOrDefault(o => o.PupilCode == pupilCode);
                            if (PupilViewModel != null)
                            {
                                String data = String.Format("{0}{1}", SMSFormat, content);
                                if (string.IsNullOrEmpty(SMSFormat))
                                {
                                    data = FirstCharToUpper(data);
                                }

                                if (data.Length > 2000) data = data.Substring(0, 2000);
                                else PupilViewModel.Content = data;

                                PupilViewModel = countSMSImport(PupilViewModel, data, "", "", isSignMsg, "");
                                PupilViewModel.IsImport = true;
                                PupilViewModel.IsSent = true;
                                PupilViewModel.ClassID = classId;
                                PupilViewModel.ClassName = className;
                                PupilViewModel.ShortContent = content;
                            }
                        }

                        numRow++;
                    }

                    // delete file after finish
                    System.IO.File.Delete(FilePath);

                    Session[SendSMSToParentConstants.LIST_IMPORT_ALL] = LstImportPupil;


                    return Json(new
                    {
                        Type = SMAS.Web.Constants.GlobalConstantsEdu.SUCCESS,
                        Message = string.Format("Import nội dung tin nhắn thành công cho {0} học sinh", LstImportPupil.Where(o => !string.IsNullOrWhiteSpace(o.ShortContent)).Count()),
                        Number = LstImportPupil.Where(o => !string.IsNullOrWhiteSpace(o.ShortContent)).Count(),
                        SMSNumber = LstImportPupil.Where(o => !string.IsNullOrWhiteSpace(o.ShortContent)).Sum(o => o.NumSMS * o.NumOfSubscriber),
                        AllMsgJoined = string.Join("", LstImportPupil.Select(x => x.ShortContent).ToArray())
                    });
                }

                // remove if invalid
                System.IO.File.Delete(FilePath);
                return Json(new JsonMessage(Res.Get("ImportClass_Label_FailedTemplated"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));
            }
            catch (Exception ex)
            {
                string para = "";
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportExcelEdu", para, ex);
            }
            // remove after validate
            if (System.IO.File.Exists(FilePath)) System.IO.File.Delete(FilePath);
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));
        }
        public PartialViewResult ShowImportList(Guid? timerConfigID, int educationLevel, string strClassID)
        {
            int currentPage = 1;
            int pageSize = SendSMSToParentConstants.PageSize;
            List<SendSMSToParentViewModel> iq;
            if (timerConfigID.HasValue)
            {
                AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                List<int> lstClassID = !string.IsNullOrEmpty(strClassID) ? strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList() : new List<int>();
                IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"SchoolID",_globalInfo.SchoolID},
                        {"lstClassID",lstClassID},
                        {"SemesterID",_globalInfo.Semester},
                        {"Year",objAy.Year}
                    };
                IQueryable<PupilProfileBO> iquery = SMSParentContractBusiness.GetSMSParentContract(dic);
                int totalRecord = iquery.Count();
                ViewData[SendSMSToParentConstants.TOTAL] = totalRecord;
                List<PupilProfileBO> lstPupil = new List<PupilProfileBO>();
                lstPupil = iquery.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                List<PupilProfileBO> lstResult = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();

                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                List<SMS_HISTORY> lstSmsHistory = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == partition
                    && o.SCHOOL_ID == _globalInfo.SchoolID
                    && o.ACADEMIC_YEAR_ID == _globalInfo.AcademicYearID
                    && o.SMS_TIMER_CONFIG_ID == timerConfigID.Value).ToList();


                iq = (from p in lstResult
                      join s in lstSmsHistory on p.PupilProfileID equals s.RECEIVER_ID into l
                      from l1 in l.DefaultIfEmpty()
                      select new
                      {
                          PupilCode = p.PupilCode,
                          FullName = p.FullName,
                          ClassName = p.ClassName,
                          LstPhoneReceiver = p.PhoneReceiver,
                          ShortContent = l1 != null ? l1.SHORT_CONTENT : ""
                      }).Distinct().Select(o => new SendSMSToParentViewModel
                      {
                          PupilCode = o.PupilCode,
                          FullName = o.FullName,
                          ClassName = o.ClassName,
                          LstPhoneReceiver = o.LstPhoneReceiver,
                          ShortContent = o.ShortContent
                      }).ToList();
            }
            else
            {
                //Add search info - Navigate to Search function in business
                iq = ((List<SendSMSToParentViewModel>)Session[SendSMSToParentConstants.LIST_IMPORT_ALL]).Where(o => !string.IsNullOrEmpty(o.ShortContent)).ToList();
                ViewData[SendSMSToParentConstants.TOTAL] = iq.Count;
                iq = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            ViewData["lstResult"] = iq;
            ViewData["CurrentTimerConfigID"] = timerConfigID;

            return PartialView("_ImportedList", iq);
        }
        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchImportAjax(GridCommand command, Guid? timerConfigID, int educationLevel, string strClassID)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            List<SendSMSToParentViewModel> lstResult = new List<SendSMSToParentViewModel>();
            int totalRecord = 0;
            if (timerConfigID.HasValue)
            {
                AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                List<int> lstClassID = !string.IsNullOrEmpty(strClassID) ? strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList() : new List<int>();
                IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"SchoolID",_globalInfo.SchoolID},
                        {"lstClassID",lstClassID},
                        {"SemesterID",_globalInfo.Semester},
                        {"Year",objAy.Year}
                    };
                IQueryable<PupilProfileBO> iquery = SMSParentContractBusiness.GetSMSParentContract(dic);
                totalRecord = iquery.Count();
                ViewData[SendSMSToParentConstants.TOTAL] = totalRecord;
                List<PupilProfileBO> lstPupil = iquery.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                List<PupilProfileBO> lstParentContract = SMSParentContractBusiness.getSMSParentContract(_globalInfo.SchoolID.Value, objAy.Year, _globalInfo.Semester.Value, lstPupil).ToList();

                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                List<SMS_HISTORY> lstSmsHistory = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == partition
                    && o.SCHOOL_ID == _globalInfo.SchoolID
                    && o.SMS_TIMER_CONFIG_ID == timerConfigID.Value).ToList();


                lstResult = (from p in lstParentContract
                             join s in lstSmsHistory on p.PupilProfileID equals s.RECEIVER_ID into l
                             from l1 in l.DefaultIfEmpty()
                             select new
                             {
                                 PupilCode = p.PupilCode,
                                 FullName = p.FullName,
                                 ClassName = p.ClassName,
                                 LstPhoneReceiver = p.PhoneReceiver,
                                 ShortContent = l1 != null ? l1.SHORT_CONTENT : ""
                             }).Distinct().Select(o => new SendSMSToParentViewModel
                             {
                                 PupilCode = o.PupilCode,
                                 FullName = o.FullName,
                                 ClassName = o.ClassName,
                                 LstPhoneReceiver = o.LstPhoneReceiver,
                                 ShortContent = o.ShortContent
                             }).ToList();
            }
            else
            {
                //Add search info - Navigate to Search function in business
                lstResult = ((List<SendSMSToParentViewModel>)Session[SendSMSToParentConstants.LIST_IMPORT_ALL]).Where(o => !string.IsNullOrEmpty(o.ShortContent)).ToList();
                totalRecord = lstResult.Count;
                lstResult = lstResult.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            //Kiem tra button
            return View(new GridModel<SendSMSToParentViewModel>
            {
                Total = totalRecord,
                Data = lstResult
            });
        }
        public PartialViewResult ShowErrorConfirm(bool isSendAll, bool isCustom, bool isTimer, bool isTimerClass)
        {
            List<ContentErrorViewModel> lstErr = (List<ContentErrorViewModel>)Session[SendSMSToParentConstants.LIST_CONTENT_ERROR];
            ViewData[SendSMSToParentConstants.IS_SEND_ALL] = isSendAll;
            ViewData[SendSMSToParentConstants.IS_SEND_CUSTOM] = isCustom;
            ViewData[SendSMSToParentConstants.IS_SAVE_TIMER] = isTimer;
            ViewData[SendSMSToParentConstants.IS_SAVE_TIMER_CLASS] = isTimerClass;
            return PartialView("_ViewErrorConfirm", lstErr);
        }
        public PartialViewResult ShowErrorList(bool isSendAll, bool isCustom, bool isTimer, bool isTimerClass)
        {
            List<ContentErrorViewModel> lstErr = (List<ContentErrorViewModel>)Session[SendSMSToParentConstants.LIST_CONTENT_ERROR];
            return PartialView("_ErrorList", lstErr);
        }
        #endregion
        #region privite method
        private SendSMSToParentViewModel countSMSCommunicate(SendSMSToParentViewModel model, bool isSignMsg = false)
        {
            if (string.IsNullOrEmpty(model.Content))
            {
                return model;
            }
            model.isNewMessage = true;
            int numberChac = model.Content.Trim().Length;
            int numSMS = 0;
            if (isSignMsg)
            {
                if (numberChac <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                else
                {
                    numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }
            }
            else
            {
                numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COUNT);
                model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
            }

            return model;
        }
        private JsonResult CreateSMSTraoDoi(SMSDataModel data, out bool success, bool isSendImport = false)
        {
            success = false;
            bool isOneClass = data.isOneClass;
            bool IsAllGrid = data.IsAllClass.HasValue ? data.IsAllClass.Value : false;
            int EducatinlevelID = data.EducationlevelID;
            List<int> lstClassID = !string.IsNullOrEmpty(data.strClassID) ? data.strClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).Distinct().ToList() : new List<int>();
            int counter = 0;
            bool isSignMsg = false;
            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            if (cfg != null) isSignMsg = cfg.IS_ALLOW_SEND_UNICODE && data.AllowSignMessage;
            bool isTimerUsed = data.IsTimerUsed;
            DateTime sendTime = default(DateTime);
            if (isTimerUsed)
            {
                if (!DateTime.TryParseExact(data.SendDate + " " + data.SendTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out sendTime))
                {
                    return Json(new JsonMessage("Ngày giờ gửi không đúng định dạng.", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                }

                if (sendTime < DateTime.Now)
                {
                    return Json(new JsonMessage("Thời gian này ở trong quá khứ. Thầy/cô đổi sang ngày hoặc giờ khác.", SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                }
            }
            #region Xử lý Prefix
            string content = data.Contents.FirstOrDefault(); // Nội dung trao đổi

            if (data.RemoveSpecialChar)
            {
                content = content.RemoveUnprintableChar();
            }
            string brandName = CommonMethods.GetBrandName();
            string prefixNoColon = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null, false);
            string prefixWithColon = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null);
            string shortContent = string.Empty;
            if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
            {
                shortContent = content.Remove(0, prefixWithColon.Length);
            }
            else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
            {
                shortContent = content.Remove(0, prefixNoColon.Length);
            }
            else if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign().Trim())) // Format có dấu : nhưng ko có khoảng trắng
            {
                shortContent = content.Remove(0, prefixWithColon.Trim().Length);
            }
            else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign().Trim())) // Format ko có dấu : và ko có khoẳng trắng
            {
                shortContent = content.Remove(0, prefixNoColon.Trim().Length);
            }
            else
            {
                shortContent = content;
            }
            #endregion
            if (isOneClass && !isSendImport)
            {
                #region Gui cho 1 lop
                if (IsAllGrid)
                {
                    data.ClassID = lstClassID.FirstOrDefault();
                    List<int> lstPupilID = PupilOfClassBusiness.GetPupilInClass(data.ClassID).Where(p => p.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING).Select(p => p.PupilID).Distinct().ToList();
                    bool isContentError = false;
                    List<ContentErrorViewModel> lstError = new List<ContentErrorViewModel>();

                    string dicContent = content.Trim();
                    string unsignedContent = dicContent.StripVNSign();
                    List<char> lstChar = new List<char>();
                    if (!this.IsPrintableString(unsignedContent, lstChar))
                    {
                        isContentError = true;
                        ContentErrorViewModel err = new ContentErrorViewModel();
                        err.PupilCode = "Toàn lớp";
                        err.PupilName = "Toàn lớp";
                        err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                        lstError.Add(err);
                    }

                    if (!isContentError)
                    {
                        ResultBO result = SendData(lstPupilID, data.ClassID, lstClassID, content, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID, new List<string>(), new List<string>(), 0, isSignMsg,
                            isTimerUsed, data.TimerConfigName, sendTime, false, false, true, data.IsCustom, data.IsSendImport.HasValue ? data.IsSendImport.Value : false);
                        if (result.isError)
                        {
                            return Json(new JsonMessage(result.ErrorMsg, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                        }
                        counter += result.NumSendSuccess;
                        success = true;
                    }
                    else
                    {
                        Session[SendSMSToParentConstants.LIST_CONTENT_ERROR] = lstError;
                        return Json(new JsonMessage(string.Empty, "ContentError"));
                    }
                }
                else  // Gửi Trao đổi với nội dung khác nhau
                {
                    lstClassID.Add(data.ClassID);
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"SchoolID",_globalInfo.SchoolID},
                        {"lstClassID",lstClassID},
                        {"lstPupilID",data.PupilIDs}
                    };
                    List<PupilProfileBO> lstPupil = PupilProfileBusiness.GetListPupilProfile(dic).ToList();

                    if (data.RemoveSpecialChar)
                    {
                        for (int i = 0; i < data.Contents.Count; i++)
                        {
                            data.Contents[i] = data.Contents[i].RemoveUnprintableChar();
                        }
                    }

                    bool isContentError = false;
                    List<ContentErrorViewModel> lstError = new List<ContentErrorViewModel>();

                    //kiem tra noi dung tin nhan chua ky tu dac biet
                    for (int i = 0; i < data.Contents.Count; i++)
                    {
                        int pupilId = data.PupilIDs[i];
                        PupilProfileBO pp = lstPupil.FirstOrDefault(o => o.PupilProfileID == data.PupilIDs[i]);
                        string pupilCode = pp.PupilCode;
                        string pupilName = pp.FullName;
                        string dicContent = data.Contents[i];
                        string unsignedContent = dicContent.StripVNSign();
                        List<char> lstChar = new List<char>();
                        if (!this.IsPrintableString(unsignedContent, lstChar))
                        {
                            isContentError = true;
                            ContentErrorViewModel err = new ContentErrorViewModel();
                            err.PupilCode = pupilCode;
                            err.PupilName = pupilName;
                            err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                            lstError.Add(err);
                        }
                    }

                    if (!isContentError)
                    {

                        #region Kiểm tra nội dung tin
                        //check prefix va tao shortContent truong truong hop gui toan lop voi noi dung tin nhan khac nhau tung hoc sinh
                        // Prefix có dạng [TRUONG]\_/  
                        for (int i = 0, size = data.Contents.Count; i < size; i++)
                        {
                            content = data.Contents[i];
                            #region format ShortContent
                            if (content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
                            {
                                shortContent = content.Remove(0, prefixWithColon.Length);
                            }
                            else if (content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
                            {
                                shortContent = content.Remove(0, prefixNoColon.Length);
                            }
                            else if (content.StripVNSign().StartsWith(prefixWithColon.Trim().StripVNSign())) // Format có dấu : nhưng ko có khoảng trắng
                            {
                                shortContent = content.Remove(0, prefixWithColon.Trim().Length);
                            }
                            else if (content.StripVNSign().StartsWith(prefixNoColon.Trim().StripVNSign())) // Format ko có dấu : và ko có khoẳng trắng
                            {
                                shortContent = content.Remove(0, prefixNoColon.Trim().Length);
                            }
                            else
                            {
                                shortContent = content;
                            }
                            #endregion

                            data.ShortContents.Add(shortContent);
                        }
                        #endregion

                        ResultBO result = SendData(data.PupilIDs, data.ClassID, null, string.Empty, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID, data.Contents, data.ShortContents, 0, isSignMsg,
                            isTimerUsed, data.TimerConfigName, sendTime);
                        if (result.isError)
                        {
                            return Json(new JsonMessage(result.ErrorMsg, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                        }
                        counter += result.NumSendSuccess;
                        success = true;
                    }
                    else
                    {
                        Session[SendSMSToParentConstants.LIST_CONTENT_ERROR] = lstError;
                        return Json(new JsonMessage(string.Empty, "ContentError"));
                    }
                }
                #endregion
            }
            else
            {
                #region gui nhieu hon 1 lop
                Dictionary<string, object> dic = new Dictionary<string, object>();
                //Nếu là admin trường thì EmployeeID = 0
                dic["senderID"] = _globalInfo.EmployeeID;
                dic["schoolID"] = _globalInfo.SchoolID;
                dic["lstClassID"] = lstClassID;
                dic["content"] = content.Trim();
                dic["UserName"] = SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value);
                dic["isSignMsg"] = isSignMsg;
                dic["shortContent"] = shortContent;
                dic["academicYearID"] = _globalInfo.AcademicYearID;
                dic["type"] = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT;
                dic["typeHistory"] = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
                dic["isAdmin"] = _globalInfo.IsAdminSchoolRole;
                dic["BoolSendAllSchool"] = data.IsAllSchool;
                dic["isSendAllEdu"] = data.IsAllLevel;
                dic["iEducationLevelID"] = EducatinlevelID;
                dic["iLevelID"] = _globalInfo.AppliedLevel;
                dic["isTimerUsed"] = data.IsTimerUsed;
                dic["sendTime"] = sendTime;
                dic["timerConfigName"] = data.TimerConfigName;
                dic["semester"] = _globalInfo.Semester;
                dic["lstReceiverID"] = data.PupilIDs;
                dic["IsCustomType"] = data.IsCustom;
                dic["isSendImport"] = false;
                dic["IsAllGrid"] = data.IsAllGrid;
                dic["iLevelID"] = _globalInfo.AppliedLevel;
                if (isSendImport)
                {
                    List<SendSMSToParentViewModel> lstImportModel = (List<SendSMSToParentViewModel>)Session[SendSMSToParentConstants.LIST_IMPORT_ALL];
                    for (int i = 0; i < lstImportModel.Count; i++)
                    {
                        if (data.RemoveSpecialChar)
                        {
                            lstImportModel[i].Content = lstImportModel[i].Content.RemoveUnprintableChar();
                        }

                        if (!isSignMsg)
                        {
                            lstImportModel[i].Content = lstImportModel[i].Content.StripVNSign();
                        }


                        string contentImport = lstImportModel[i].Content;
                        string shortContentImport;
                        if (contentImport.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
                        {
                            shortContentImport = contentImport.Remove(0, prefixWithColon.Length);
                        }
                        else if (contentImport.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
                        {
                            shortContentImport = contentImport.Remove(0, prefixNoColon.Length);
                        }
                        else if (contentImport.StripVNSign().StartsWith(prefixWithColon.Trim().StripVNSign())) // Format có dấu : nhưng ko có khoảng trắng
                        {
                            shortContentImport = contentImport.Remove(0, prefixWithColon.Trim().Length);
                        }
                        else if (contentImport.StripVNSign().StartsWith(prefixNoColon.Trim().StripVNSign())) // Format ko có dấu : và ko có khoẳng trắng
                        {
                            shortContentImport = contentImport.Remove(0, prefixNoColon.Trim().Length);
                        }
                        else
                        {
                            shortContentImport = contentImport;
                        }

                        lstImportModel[i].ShortContent = shortContentImport;
                    }

                    lstImportModel = lstImportModel.Where(o => !string.IsNullOrWhiteSpace(o.ShortContent)).ToList();
                    dic["isSendImport"] = true;
                    dic["ListPupilImport"] = lstImportModel.Select(o => new SendSMSByImportBO
                    {
                        ClassID = o.ClassID,
                        Content = o.Content,
                        PupilCode = o.PupilCode,
                        PupilFileID = o.PupilFileID,
                        ShortContent = o.ShortContent,
                        FullName = o.FullName
                    }).ToList();
                }

                //kiem tra noi dung tin nhan 
                bool isContentError = false;
                List<ContentErrorViewModel> lstError = new List<ContentErrorViewModel>();
                if (!isSendImport)
                {
                    string dicContent = content.Trim();
                    string unsignedContent = dicContent.StripVNSign();
                    List<char> lstChar = new List<char>();
                    if (!this.IsPrintableString(unsignedContent, lstChar))
                    {
                        isContentError = true;
                        ContentErrorViewModel err = new ContentErrorViewModel();
                        err.PupilCode = EducatinlevelID != 0 ? "Toàn khối" : "Toàn trường";
                        err.PupilName = EducatinlevelID != 0 ? "Toàn khối" : "Toàn trường";
                        err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                        lstError.Add(err);
                    }
                }
                else
                {
                    List<SendSMSByImportBO> lstImport = (List<SendSMSByImportBO>)dic["ListPupilImport"];
                    if (lstImport != null)
                    {
                        for (int i = 0; i < lstImport.Count; i++)
                        {
                            string pupilCode = lstImport[i].PupilCode;
                            string pupilName = lstImport[i].FullName;
                            string dicContent = lstImport[i].Content;
                            string unsignedContent = dicContent.StripVNSign();
                            List<char> lstChar = new List<char>();
                            if (!this.IsPrintableString(unsignedContent, lstChar))
                            {
                                isContentError = true;
                                ContentErrorViewModel err = new ContentErrorViewModel();
                                err.PupilCode = pupilCode;
                                err.PupilName = pupilName;
                                err.Content = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
                                lstError.Add(err);
                            }
                        }
                    }
                }

                if (!isContentError)
                {
                    ResultBO result = SMSHistoryBusiness.SendSMS(dic);
                    if (result.isError)
                    {
                        return Json(new JsonMessage(result.ErrorMsg, SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ERROR));
                    }

                    counter += result.NumSendSuccess;
                    //WriteLogInfo("Gửi tin nhắn trao đổi toàn trường(or khối) PHHS", 0, string.Empty, string.Empty, null, string.Empty);
                    // Hien thi danh sach cac lop chua gui
                    if (result.LstClassNotSent != null && result.LstClassNotSent.Count > 0)
                    {
                        StringBuilder strClassInfo = new StringBuilder();
                        List<ClassBO> LstClassNotSend = result.LstClassNotSent.OrderByDescending(o => o.ClassName).ToList();
                        for (int i = LstClassNotSend.Count - 1; i >= 0; i--)
                        {
                            strClassInfo.Append(LstClassNotSend[i].ClassName);
                            if (i != 0)
                            {
                                strClassInfo.Append(SMAS.Web.Constants.GlobalConstantsEdu.COMMA).Append(SMAS.Web.Constants.GlobalConstantsEdu.SPACE);
                            }
                        }
                        return Json(new { Type = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_INFO, TotalParent = counter, ListClass = strClassInfo.ToString() });
                    }
                    else
                    {
                        success = true;
                        return Json(new JsonMessage(string.Format(Res.Get("Communication_SMSToParent_Result"), counter)));
                    }
                }
                else
                {
                    Session[SendSMSToParentConstants.LIST_CONTENT_ERROR] = lstError;
                    return Json(new JsonMessage(string.Empty, "ContentError"));
                }
                #endregion
            }
            return Json(new JsonMessage(string.Format("Gửi tin nhắn thành công đến {0} PHHS", counter), "success"));
        }
        private ResultBO SendData(List<int> lstReceiverID, int ClassID, List<int> lstClassID, string Content, int TypeID, List<string> lstContent, List<string> lstShortContent, int? EducationLevelID = 0, bool isSignMsg = false,
            bool isTimerUsed = false, string timerConfigName = "", DateTime sendTime = default(DateTime), bool sendAllSchool = false, bool sendAllEdu = false, bool isAllClass = false, bool isCustomType = false, bool isSendImport = false)
        {
            //Tin nhan SMS          
            string shortContent = string.Empty;
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefixNoColon = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            string prefixWithColon = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null);

            #region get short content
            if (lstContent.Count == 0)
            {
                string brandName = CommonMethods.GetBrandName();
                if (Content.StripVNSign().StartsWith(prefixWithColon.StripVNSign())) // Format có dấu :
                {
                    shortContent = Content.Remove(0, prefixWithColon.Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixNoColon.StripVNSign())) // Format ko có dấu :
                {
                    shortContent = Content.Remove(0, prefixNoColon.Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixWithColon.Trim().StripVNSign())) // Format có dấu : nhưng ko có khoảng trắng
                {
                    shortContent = Content.Remove(0, prefixWithColon.Trim().Length);
                }
                else if (Content.StripVNSign().StartsWith(prefixNoColon.Trim().StripVNSign())) // Format ko có dấu : và ko có khoẳng trắng
                {
                    shortContent = Content.Remove(0, prefixNoColon.Trim().Length);
                }
                else
                {
                    shortContent = Content;
                }
            }
            #endregion

            //kiem tra noi dung tin nhan chua ky tu dac biet

            Dictionary<string, object> dic = new Dictionary<string, object>();
            //Nếu là admin trường thì EmployeeID = 0
            dic["senderID"] = _globalInfo.EmployeeID;
            dic["lstReceiverID"] = lstReceiverID;
            dic["lstClassID"] = lstClassID;
            dic["schoolID"] = _globalInfo.SchoolID;
            dic["classID"] = ClassID;
            dic["UserName"] = SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value);
            dic["content"] = Content.Trim();
            dic["isSignMsg"] = isSignMsg;
            dic["shortContent"] = shortContent;
            dic["academicYearID"] = _globalInfo.AcademicYearID;
            dic["type"] = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT;
            dic["typeHistory"] = TypeID;
            dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
            dic["isAdmin"] = _globalInfo.IsAdminSchoolRole;
            dic["lstContent"] = lstContent;
            dic["lstShortContent"] = lstShortContent;
            dic["iEducationLevelID"] = EducationLevelID;
            dic["isTimerUsed"] = isTimerUsed;
            dic["sendTime"] = sendTime;
            dic["timerConfigName"] = timerConfigName;
            dic["BoolSendAllSchool"] = sendAllSchool;
            dic["IsAllClass"] = isAllClass;
            dic["IsCustomType"] = isCustomType;
            dic["semester"] = _globalInfo.Semester;
            dic["isSendAllEdu"] = sendAllEdu;
            dic["isSendImport"] = isSendImport;
            dic["iLevelID"] = _globalInfo.AppliedLevel;
            return SMSHistoryBusiness.SendSMS(dic);
        }
        private List<SendSMSToParentViewModel> GetCustomSMS(List<PupilProfileBO> lstPupil, List<int> lstClassID, int? Semester, int? Month, int? Year, DateTime? fromDate, DateTime? toDate, SMS_CUSTOM_TYPE type, bool isSignMsg)
        {
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            string prefixName = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null, false);
            string SMSContent = string.Empty;
            PupilProfileBO objPupilProfileBO = null;

            SendSMSToParentViewModel model = null;
            List<SendSMSToParentViewModel> lstPupilRet = new List<SendSMSToParentViewModel>();
            List<int> listPupilID = lstPupil.Select(p => p.PupilProfileID).ToList();

            string template = type.TEMPLATE_CONTENT;

            string pattern = @"\{(.+?)\}";
            MatchCollection matches = Regex.Matches(template, pattern);
            List<string> lstContentCode = new List<string>();

            foreach (Match match in matches)
            {
                if (match.Value.Length > 3)
                {
                    lstContentCode.Add(match.Value.Substring(1, match.Value.Length - 2));
                }
            }
            //Loc cac lop VNEN
            if (type.FOR_VNEN)
            {
                var lstClassVNEN = ClassProfileBusiness.GetListClassBySchool(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value)
                    .Where(o => o.EducationLevel.Grade == _globalInfo.AppliedLevel && o.IsVnenClass.HasValue && o.IsVnenClass.Value).ToList();

                lstClassID = lstClassID.Where(x => lstClassVNEN.Exists(o => o.ClassProfileID == x)).ToList();
            }
            List<CustomSMSBO> listResult = new List<CustomSMSBO>();
            if (lstClassID.Count > 0)
            {
                listResult = SMSHistoryBusiness.GetCustomSMS(listPupilID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, lstClassID, _globalInfo.AppliedLevel.Value, type.TEMPLATE_CONTENT, type.PERIOD_TYPE, type.FOR_VNEN, lstContentCode, sc != null ? sc.NAME_DISPLAYING : null, Semester, Month, Year, fromDate, toDate);
            }

            int index = 1;
            string SMSFormat = prefixName;
            for (int k = 0, kSize = lstPupil.Count; k < kSize; k++)
            {
                objPupilProfileBO = lstPupil[k];
                model = new SendSMSToParentViewModel();
                model.STT = index++;
                model.PupilFileID = objPupilProfileBO.PupilProfileID;
                model.FullName = objPupilProfileBO.FullName;
                model.Status = objPupilProfileBO.ProfileStatus;
                model.ClassName = objPupilProfileBO.ClassName;
                model.ClassID = objPupilProfileBO.CurrentClassID;
                model.SMSTemplate = SMSFormat;
                model.LstPhoneReceiver = objPupilProfileBO.PhoneReceiver;
                model.RemainSMSTxt = objPupilProfileBO.RemainSMS;
                model.ExpireTrial = objPupilProfileBO.ExpireTrial;
                model.NumOfSubscriber = objPupilProfileBO.NumOfSMSSubscriber;
                if (listResult.Count > 0) SMSContent = listResult.Where(p => p.PupilID == model.PupilFileID).Select(p => p.SMSContent).FirstOrDefault();

                model = countSMSCustom(model, SMSContent, SMSFormat, isSignMsg);
                if (isSignMsg)
                {
                    if (!checkSupportSendUnicodeSMS(objPupilProfileBO.PhoneReceiver))
                    {
                        model.Content = SendSMSToParentConstants.NO_SUPPORT_UNICODE;
                        model.NumOfSubscriber = 0;
                    }
                    else model.NumOfSubscriber = objPupilProfileBO.PhoneReceiver.Count(o => o.isAllowSendSignSMSPhone());
                }
                lstPupilRet.Add(model);
            }

            return lstPupilRet;
        }
        private SendSMSToParentViewModel countSMSCustom(SendSMSToParentViewModel model, string content, string formatSMS, bool isSignMsg = false)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format("{0}{1}", formatSMS, content).Trim();
                int numberChac = content.Trim().Length;
                int numSMS = 0;
                if (isSignMsg)
                {
                    model.Content = content;
                    if (numberChac <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                    {
                        numSMS = 1;
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    }
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    model.Content = content.StripVNSign();
                    numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COUNT);
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }

                model.NumSMS = numSMS;
            }
            else
            {

                model.Content = SendSMSToParentConstants.NO_SMS_MESSAGE;

            }
            return model;
        }
        private bool checkSupportSendUnicodeSMS(List<string> lstPhone)
        {
            if (lstPhone.Count > 0)
            {
                foreach (string phone in lstPhone)
                    if (phone.isAllowSendSignSMSPhone()) return true;
                return false;
            }
            else return false;
        }
        private SendSMSToParentViewModel countSMS(SendSMSToParentViewModel model, string content, string formatSMS, string pupilName, bool isSignMsg = false, string SEPARATOR = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_COLON_SPACE, bool returnNoSMSMSg = true)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, formatSMS, pupilName, SEPARATOR, content);
                int numberChac = content.Trim().Length;
                int numSMS = 0;
                if (isSignMsg)
                {
                    model.Content = content;
                    if (numberChac <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                    {
                        numSMS = 1;
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    }
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    model.Content = content.StripVNSign();
                    numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COUNT);
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }

                model.NumSMS = numSMS;
            }
            else
            {
                if (returnNoSMSMSg)
                {
                    model.Content = SendSMSToParentConstants.NO_SMS_MESSAGE;
                }
            }
            return model;
        }
        private bool IsPrintableString(string text, List<char> lstChar)
        {
            bool isPrintable = true;
            for (int i = 0; i < text.Length; i++)
            {
                string s = text[i].ToString().StripVNSign();
                if (!string.IsNullOrWhiteSpace(s) && (s[0] < 32 || s[0] > 126))
                {
                    isPrintable = false;
                    lstChar.Add(text[i]);
                }
            }

            return isPrintable;
        }
        private string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                return string.Empty;
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
        private SendSMSToParentViewModel countSMSBreakLine(SendSMSToParentViewModel model, string content, string formatSMS, string pupilName, bool isSignMsg = false, string SEPARATOR = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_COLON_SPACE)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, formatSMS, pupilName, SEPARATOR + "\n", content);
                int numberChac = content.Trim().Length;
                int numSMS = 0;
                if (isSignMsg)
                {
                    model.Content = content;
                    if (numberChac <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    model.Content = content.StripVNSign();
                    numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COUNT);
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }
            }
            else
            {
                model.Content = SendSMSToParentConstants.NO_SMS_MESSAGE;
            }
            return model;
        }
        private void GetDateRangeOfWeek(int year, int month, int week, out DateTime fromDate, out DateTime toDate)
        {
            try
            {
                DateTime dateMonth = new DateTime(year, month, 1);
                DateTime firstMonday = dateMonth;
                while (firstMonday.DayOfWeek != DayOfWeek.Monday) firstMonday = firstMonday.AddDays(1);

                fromDate = new DateTime(year, month, firstMonday.Day + (7 * (week - 1)));
                toDate = fromDate.AddDays(5);
            }
            catch (ArgumentOutOfRangeException)
            {
                week = 1;
                DateTime dateMonth = new DateTime(year, month, 1);
                DateTime firstMonday = dateMonth;
                while (firstMonday.DayOfWeek != DayOfWeek.Monday) firstMonday = firstMonday.AddDays(1);

                fromDate = new DateTime(year, month, firstMonday.Day + (7 * (week - 1)));
                toDate = fromDate.AddDays(5);
            }
        }
        public List<MonthEvaluationCommentsBO> getMonthsReview()
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (_globalInfo.Semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
            {
                FromDate = new DateTime(objAy.FirstSemesterStartDate.Value.Year, objAy.FirstSemesterStartDate.Value.Month, 1);
                ToDate = new DateTime(objAy.FirstSemesterEndDate.Value.Year, objAy.FirstSemesterEndDate.Value.Month, 1);
            }
            else if (_globalInfo.Semester == SMAS.Web.Constants.GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
            {
                FromDate = new DateTime(objAy.SecondSemesterStartDate.Value.Year, objAy.SecondSemesterStartDate.Value.Month, 1);
                ToDate = new DateTime(objAy.SecondSemesterEndDate.Value.Year, objAy.SecondSemesterEndDate.Value.Month, 1);
            }
            List<MonthEvaluationCommentsBO> lstMonths = new List<MonthEvaluationCommentsBO>();
            MonthEvaluationCommentsBO objMonth = null;
            DateTime datetimeNow = DateTime.Now;
            while (FromDate <= ToDate)
            {
                objMonth = new MonthEvaluationCommentsBO();
                objMonth.MonthID = Int32.Parse(FromDate.Date.Year.ToString() + FromDate.Date.Month.ToString());
                objMonth.MonthName = "Tháng " + FromDate.Month;
                objMonth.Year = FromDate.Year;
                objMonth.Selected = FromDate.Month == datetimeNow.Month;
                lstMonths.Add(objMonth);
                FromDate = FromDate.AddMonths(1);
            }
            return lstMonths;
        }
        private SendSMSToParentViewModel countSMSImport(SendSMSToParentViewModel model, string content, string formatSMS, string pupilName, bool isSignMsg = false, string SEPARATOR = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_COLON_SPACE)
        {
            if (!string.IsNullOrEmpty(content))
            {
                model.isNewMessage = true;
                content = String.Format(SendSMSToParentConstants.CONTENT_FORMAT, formatSMS, pupilName, SEPARATOR, content);
                int numberChac = content.Trim().Length;
                int numSMS = 0;

                model.Content = content;
                if (isSignMsg)
                {
                    if (numberChac <= SMAS.Web.Constants.GlobalConstantsEdu.COMMON_ONLY_SIGN_SMS_COUNT)
                    {
                        numSMS = 1;
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, 1);
                    }
                    else
                    {
                        numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SIGN_SMS_COUNT);
                        model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                    }
                }
                else
                {
                    numSMS = (int)Math.Ceiling((double)numberChac / SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COUNT);
                    model.SMSLength = String.Format(SendSMSToParentConstants.SMSLENGTH_FORMAT, numberChac, numSMS);
                }

                model.NumSMS = numSMS;

            }

            return model;
        }
        private List<SMS_TIMER_CONFIG> GetListTimerConfig()
        {
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            return TimerConfigBusiness.All.Where(u => u.SCHOOL_ID == _globalInfo.SchoolID
                    && u.PARTITION_ID == partitionID
                    && u.STATUS == SendSMSToParentConstants.TIMER_CONFIG_STATUS_ACTIVE
                    && u.APPLIED_LEVEL == _globalInfo.AppliedLevel)
                    .OrderByDescending(o => o.CREATE_TIME).ToList();
        }
        private List<KeyValuePair<int, string>> GetListWeek(int month, int year)
        {
            DateTime dateMonth = new DateTime(year, month, 1);
            int mondayNum = this.GetMondaysInMonth(dateMonth);
            List<KeyValuePair<int, string>> lstWeek = new List<KeyValuePair<int, string>>();
            DateTime firstMonday = dateMonth;
            while (firstMonday.DayOfWeek != DayOfWeek.Monday) firstMonday = firstMonday.AddDays(1);

            for (int i = 0; i < mondayNum; i++)
            {
                KeyValuePair<int, string> kvp = new KeyValuePair<int, string>(i + 1, "Tuần " + (i + 1));

                lstWeek.Add(kvp);
                firstMonday = firstMonday.AddDays(7);
            }

            return lstWeek;
        }
        private int GetCurrentWeek()
        {
            int curMonth = DateTime.Now.Month;
            int curYear = DateTime.Now.Year;
            DateTime curDate = DateTime.Now.Date;

            List<KeyValuePair<int, string>> lstWeek = this.GetListWeek(curMonth, curYear);
            for (int i = 0; i < lstWeek.Count; i++)
            {
                KeyValuePair<int, string> week = lstWeek[i];
                DateTime fromDate, toDate;
                this.GetDateRangeOfWeek(curYear, curMonth, week.Key, out fromDate, out toDate);

                if (DateTime.Compare(curDate, fromDate.Date) >= 0 && DateTime.Compare(curDate, toDate.Date) <= 0)
                {
                    return week.Key;
                }
            }

            return 0;
        }
        private int GetMondaysInMonth(DateTime thisMonth)
        {
            int mondays = 0;
            int month = thisMonth.Month;
            int year = thisMonth.Year;
            int daysThisMonth = DateTime.DaysInMonth(year, month);
            DateTime beginingOfThisMonth = new DateTime(year, month, 1);
            for (int i = 0; i < daysThisMonth; i++)
                if (beginingOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                    mondays++;

            return mondays;
        }
        #endregion
        public class MonthMNBO
        {
            public string MonthID { get; set; }
            public string MonthName { get; set; }
            public bool isSelected { get; set; }
        }
    }
}