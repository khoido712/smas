﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class SMSConfigController:BaseController
    {
        #region Page constants
        /// <summary>
        /// Kiem tra phan quyen tren SMAS bang control Name
        /// </summary>
        private const string _SMAS_CONTROL_NAME = "#smsedu/messagemanagement#";

        private const string _CONST_NUM_ZERO_TEXT = "0";

        private const string _CONST_JSON_FORMAT = "SendBefore: {0}, SendTime: {1}, IsAutoSend: {2}, IsLock: {3}";

        private const string _CONST_LOG_TEXT = "Cập nhật cấu hình bản tin trường thành công.";
        #endregion

        #region Page private parameters
        private readonly ISMSTypeBusiness SMSTypeBusiness;
        private readonly ISMSTypeDetailBusiness SMSTypeDetailBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly IContentTypeBusiness ContentTypeBusiness;
        private readonly ICustomSMSTypeBusiness CustomSMSTypeBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        #endregion

        /// <summary>
        /// Contructor
        /// </summary>
        /// <returns></returns>
        public SMSConfigController(ISMSTypeBusiness smsTypeBusiness, ISMSTypeDetailBusiness detail, ISchoolConfigBusiness schoolConfigBusiness,
            IContentTypeBusiness ContentTypeBusiness, ICustomSMSTypeBusiness CustomSMSTypeBusiness, ISMSHistoryBusiness SMSHistoryBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.SMSTypeBusiness = smsTypeBusiness;
            this.SMSTypeDetailBusiness = detail;
            this.SchoolConfigBusiness = schoolConfigBusiness;
            this.ContentTypeBusiness = ContentTypeBusiness;
            this.CustomSMSTypeBusiness = CustomSMSTypeBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        /// <summary>
        /// Index View, show the default page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (!(_globalInfo.IsRolePrincipal || _globalInfo.IsAdmin))
            {
                //Neu khong phai la QTHT/BGH truong thi khong cho vao chuc nang nay
                ViewData["NOT_IN_ROLE"] = 1;
            }
            else
            {
                SMSTypeBusiness.ValidateSMSType(_globalInfo.SchoolID.Value);
                List<SMSTypeBO> lstSMSType = SMSTypeBusiness.GetSMSTypeBySchool(GlobalConstantsEdu.CONST_COMMUNICATION_TEACHER_TO_PARENT, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, false, true);
                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                if (sp.IsNewSchoolModel == null || sp.IsNewSchoolModel == false) lstSMSType = lstSMSType.Where(o => o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE &&
                                     o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE && o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE).ToList();

                //viethd31: Lay ra cac ban tin tu dinh nghia
                List<SMSTypeBO> lstCustomSMSType = CustomSMSTypeBusiness.GetCustomSMSTypeBySchool(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);

                lstSMSType.AddRange(lstCustomSMSType);

                ViewData["lstSMSType"] = lstSMSType;
            }
            return View();
        }
        /// <summary>
        /// Show SMSConfig Editor
        /// </summary>
        /// <returns></returns>
        public PartialViewResult ShowMessage()
        {
            SMSTypeViewModel mdl = null;

            int typeID = string.IsNullOrWhiteSpace(Request["TypeID"]) ? 0 : int.Parse(Request["TypeID"]);
            var item = SMSTypeDetailBusiness.All.Where(a => a.SMS_TYPE_ID == typeID && a.SCHOOL_ID == _globalInfo.SchoolID.Value).FirstOrDefault();

            var smsType = SMSTypeBusiness.All.Where(a => a.SMS_TYPE_ID == typeID).FirstOrDefault();
            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);
            if (cfg != null) ViewData["AllowSendUnicodeSMS"] = cfg.IS_ALLOW_SEND_UNICODE;

            DateTime semesterStartDate;
            DateTime semesterEndDate;

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime dateNow = DateTime.Now;
            if (DateTime.Compare(dateNow, aca.FirstSemesterStartDate.Value) >= 0
                && DateTime.Compare(dateNow, aca.FirstSemesterEndDate.Value) <= 0)
            {
                semesterStartDate = aca.FirstSemesterStartDate.Value;
                semesterEndDate = aca.FirstSemesterEndDate.Value;
            }
            else
            {
                semesterStartDate = aca.SecondSemesterStartDate.Value;
                semesterEndDate = aca.SecondSemesterEndDate.Value;
            }

            DateTime defaultSendTime = DateTime.Now;
            if (smsType.TYPE_CODE.Trim() == "TT22KQGK")
            {
                defaultSendTime = semesterStartDate.AddMonths(2);
            }
            else if (smsType.TYPE_CODE.Trim() == "TT22KQCK")
            {
                defaultSendTime = semesterEndDate;
            }

            if (smsType != null)
            {
                mdl = new SMSTypeViewModel();
                mdl.SendTime = item.SEND_TIME;

                mdl.Template = item.SMS_TEMPLATE;
                if (item.SEND_TIME == null)
                {
                    if (smsType.TYPE_CODE.Trim() == "TT22KQCK" || smsType.TYPE_CODE.Trim() == "TT22KQGK" || smsType.TYPE_CODE.Trim() == "TT22NXCK")
                    {
                        item.SEND_TIME = defaultSendTime;
                    }
                    else if (smsType.TYPE_CODE.Trim() == "NXTVNEN")
                    {
                        item.SEND_TIME = new DateTime(aca.SecondSemesterEndDate.Value.Year,
                            aca.SecondSemesterEndDate.Value.Month,
                            aca.SecondSemesterEndDate.Value.Day
                            , 7, 0, 0);
                    }
                    else
                    {
                        item.SEND_TIME = aca.SecondSemesterEndDate.Value;
                    }
                }
                ViewData["ItemData"] = item;
                ViewData["TypeData"] = smsType;
            }


            return PartialView("Editor", mdl);
        }

        public PartialViewResult GetSMSTypeList(string code)
        {
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            List<SMSTypeBO> lstSMSType = SMSTypeBusiness.GetSMSTypeBySchool(GlobalConstantsEdu.CONST_COMMUNICATION_TEACHER_TO_PARENT, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, false, true);
            if (sp.IsNewSchoolModel == null || sp.IsNewSchoolModel == false) lstSMSType = lstSMSType.Where(o => o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE &&
                            o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_ENDING_TYPECODE && o.TypeCode.Trim() != GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_SEMESTER_RESULT_TYPECODE).ToList();

            //viethd31: Lay ra cac ban tin tu dinh nghia
            List<SMSTypeBO> lstCustomSMSType = CustomSMSTypeBusiness.GetCustomSMSTypeBySchool(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value);

            lstSMSType.AddRange(lstCustomSMSType);

            ViewData["lstSMSType"] = lstSMSType;
            ViewData["DefaultTypeCode"] = code;

            return PartialView("List");
        }

        public PartialViewResult CreateNewType()
        {
            SMSConfigCreateViewModel model = new SMSConfigCreateViewModel();
            model.Template = "Thông báo kết quả của em [Tên học sinh] [Thời gian định kỳ]:";
            model.ForVNEN = false;

            //Lay content type
            List<SMS_CONTENT_TYPE> lstContentType = GetListContentType(SMSConfigConstants.PERIOD_TYPE_DAILY, false);
            ViewData[SMSConfigConstants.LIST_CONTENT_TYPE] = lstContentType;

            return PartialView("Create", model);
        }

        public JsonResult GetContentType(int periodType, bool? forVNEN)
        {
            List<SMS_CONTENT_TYPE> lstContentType = GetListContentType(periodType, forVNEN.GetValueOrDefault());
            return Json(lstContentType);
        }

        public JsonResult GetCustomSMSEx(string template, int periodType, bool? isForVNEN)
        {
            string pattern = @"\[(.+?)\]";
            MatchCollection matches = Regex.Matches(template, pattern);

            List<string> lstContentName = new List<string>();

            foreach (Match match in matches)
            {
                lstContentName.Add(match.Value);
            }

            List<SMS_CONTENT_TYPE> lstContentType = GetListContentType(periodType, isForVNEN.GetValueOrDefault());

            for (int i = 0; i < lstContentName.Count; i++)
            {
                string contentName = lstContentName[i];
                if (!string.IsNullOrEmpty(contentName) && contentName.Length > 3)
                {
                    contentName = contentName.Substring(1, contentName.Length - 2);
                }
                else
                {
                    contentName = string.Empty;
                }
                SMS_CONTENT_TYPE ct = lstContentType.FirstOrDefault(o => o.CONTENT_TYPE_NAME == contentName);

                if (ct != null)
                {
                    template = template.Replace("[" + contentName + "]", "{" + ct.CONTENT_TYPE_CODE + "}");
                }
            }

            pattern = @"\{(.+?)\}";
            matches = Regex.Matches(template, pattern);
            List<string> lstContentCode = new List<string>();

            foreach (Match match in matches)
            {
                if (match.Value.Length > 3)
                {
                    lstContentCode.Add(match.Value.Substring(1, match.Value.Length - 2));
                }
            }

            string content = template;
            for (int j = 0; j < lstContentCode.Count; j++)
            {
                string code = lstContentCode[j];

                string contentOfCode = this.GetCustomContentEx(code, periodType, isForVNEN.GetValueOrDefault());


                content = Regex.Replace(content.Replace("{" + code + "}", contentOfCode), @"(\r\n)+", "\r\n", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
            }

            content = content.Replace("\n", "<br/>");

            return Json(new { Content = content });
        }

        private string GetCustomContentEx(string code, int periodType, bool isForVNEN)
        {
            string s = string.Empty;
            switch (code)
            {
                case "THS":
                    s = "Nguyễn Văn A";
                    break;
                case "TGDK":
                    switch (periodType)
                    {
                        case 1:
                            s = "ngày [ngày gửi tin]";
                            break;
                        case 2:
                            s = "từ [ngày hiện tại - 6] đến [ngày hiện tại]";
                            break;
                        case 3:
                            if (isForVNEN)
                            {
                                s = "tháng 9";
                            }
                            else
                            {
                                s = "từ [ngày hiện tại - 1 tháng] đến [ngày hiện tại]";
                            }
                            break;
                        case 4:
                            s = "[Học kỳ]";
                            break;
                        case 5:
                            s = "từ [từ ngày] đến [đến ngày]";
                            break;

                    }
                    break;
                case "DD":
                    s = "Nghỉ học có phép: 1";
                    break;
                case "VP":
                    s = "Vi phạm: không thuộc bài môn toán, đi học trễ";
                    break;
                case "DIEM":
                    s = "- Môn toán M: 7, 9 15P: 8, 9<br/>- Môn thể dục M: D, CD 15P: CD, D";
                    break;
                case "TBM":
                    s = "Toán: 9.5<br/>Vật lí: 9.8<br/>Sinh học: 8.8<br/>Ngữ văn: 8.8";
                    break;
                case "TBCM":
                    s = "Trung bình các môn: 9.3";
                    break;
                case "HL":
                    s = "Học lực: Giỏi";
                    break;
                case "HK":
                    s = "Hạnh kiểm: Tốt";
                    break;
                case "XH":
                    s = "Xếp hạng: 1";
                    break;
                case "DHTD":
                    s = "Danh hiệu thi đua: Xuất sắc";
                    break;
                case "MHHDGD":
                    s = "Toán: T,8; Tiếng Việt: T,10; Thể dục: H;";
                    break;
                case "NL":
                    if (isForVNEN)
                    {
                        s = "Năng lực: D";
                    }
                    else
                    {
                        s = "Năng lực: T,T,D;";
                    }
                    break;
                case "PC":
                    if (isForVNEN)
                    {
                        s = "Phẩm chất: D";
                    }
                    else
                    {
                        s = "Phẩm chất: T,D,T,T;";
                    }
                    break;
                case "KQNH":
                    s = "Được lên lớp";
                    break;
                case "KT":
                    if (isForVNEN)
                    {
                        s = "Khen thưởng: HSG toàn diện; Hóa điểm mười";
                    }
                    else
                    {
                        s = "Khen thưởng: Học sinh xuất sắc;";
                    }
                    break;
                case "NXT":
                    s = "1/ Toán: Học tập tốt<br/>2/ Văn: Làm đầy đủ bài về nhà";
                    break;
                case "NXCK":
                    s = "1/ Toán: Học tập tốt<br/>2/ Văn: Làm đầy đủ bài về nhà";
                    break;
                case "DGMH":
                    s = "Toán: 8; Tin học: 5; Hoạt động giáo dục: D;";
                    break;
                case "DGCN":
                    s = "Được lên lớp";
                    break;
            }

            return s.Trim();
        }

        private List<SMS_CONTENT_TYPE> GetListContentType(int periodType, bool forVNEN)
        {
            bool forGrade1 = _globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_PRIMARY;
            bool forGrade2 = _globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_SECONDARY;
            bool forGrade3 = _globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_TERTIARY;

            var lstContentType = ContentTypeBusiness.All;

            if (_globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_PRIMARY)
            {
                lstContentType = lstContentType.Where(o => o.FORG_RADE1);
            }

            if (_globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_SECONDARY)
            {
                if (forVNEN)
                {
                    lstContentType = lstContentType.Where(o => o.FORG_RADE2 && o.FOR_VNEN);
                }
                else
                {
                    lstContentType = lstContentType.Where(o => o.FORG_RADE2 && o.FOR_NOTVNEN);
                }
            }

            if (_globalInfo.AppliedLevel.Value == GlobalConstantsEdu.COMMON_GRADE_TERTIARY)
            {
                lstContentType = lstContentType.Where(o => o.FORG_RADE3);
            }

            if (periodType == SMSConfigConstants.PERIOD_TYPE_DAILY)
            {
                lstContentType = lstContentType.Where(o => o.PERIOD_DAILY);
            }

            if (periodType == SMSConfigConstants.PERIOD_TYPE_WEEKLY)
            {
                lstContentType = lstContentType.Where(o => o.PERIOD_WEEKLY);
            }

            if (periodType == SMSConfigConstants.PERIOD_TYPE_MONTHLY)
            {
                lstContentType = lstContentType.Where(o => o.PERIOD_MONTHLY);
            }

            if (periodType == SMSConfigConstants.PERIOD_TYPE_SEMESTER)
            {
                lstContentType = lstContentType.Where(o => o.PERIOD_SEMESTER);
            }

            if (periodType == SMSConfigConstants.PERIOD_TYPE_CUSTOM)
            {
                lstContentType = lstContentType.Where(o => o.PERIOD_CUSTOM);
            }

            return lstContentType.OrderBy(o => o.ORDER_NUMBER).ToList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult CreateOrEdit(SMSConfigCreateViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.SMSTypeName) || string.IsNullOrWhiteSpace(model.Template))
            {
                return Json(new JsonMessage("Tên bản tin/Nội dung là bắt buộc", GlobalConstantsEdu.ERROR));
            }

            IQueryable<string> lstTypeOfSchool = from st in SMSTypeBusiness.All
                                                    join sd in SMSTypeDetailBusiness.All on st.SMS_TYPE_ID equals sd.SMS_TYPE_ID
                                                    where sd.SCHOOL_ID == _globalInfo.SchoolID.Value
                                                    select st.SMS_TYPE_NAME;
            //Kiem tra ten ban tin co trung hay khong
            if (lstTypeOfSchool.Where(o => o == model.SMSTypeName).Count() > 0
                || CustomSMSTypeBusiness.All.Where(o => o.SCHOOL_ID == _globalInfo.SchoolID.Value && o.GRADE == _globalInfo.AppliedLevel.Value && o.IS_ACTIVE && o.TYPE_NAME == model.SMSTypeName &&
                    (model.Id == 0 || o.CUSTOM_SMS_TYPE_ID != model.Id)).Count() > 0)
            {
                return Json(new JsonMessage("Tên bản tin đã tồn tại", GlobalConstantsEdu.ERROR));
            }

            model.SMSTypeName = model.SMSTypeName.Trim();
            model.Template = model.Template.Trim();

            string template = model.Template;

            string pattern = @"\[(.+?)\]";
            MatchCollection matches = Regex.Matches(template, pattern);

            List<string> lstContentName = new List<string>();

            foreach (Match match in matches)
            {
                lstContentName.Add(match.Value);
            }

            List<SMS_CONTENT_TYPE> lstContentType = GetListContentType(model.PeriodType, model.ForVNEN);

            for (int i = 0; i < lstContentName.Count; i++)
            {
                string contentName = lstContentName[i];
                if (!string.IsNullOrEmpty(contentName) && contentName.Length > 3)
                {
                    contentName = contentName.Substring(1, contentName.Length - 2);
                }
                else
                {
                    contentName = string.Empty;
                }
                SMS_CONTENT_TYPE ct = lstContentType.FirstOrDefault(o => o.CONTENT_TYPE_NAME == contentName);

                if (ct != null)
                {
                    template = template.Replace("[" + contentName + "]", "{" + ct.CONTENT_TYPE_CODE + "}");
                }
            }

            SMS_CUSTOM_TYPE cs = CustomSMSTypeBusiness.Find(model.Id);
            string log;

            string message;
            string code;
            if (cs == null)
            {
                cs = new SMS_CUSTOM_TYPE();
                cs.CREATE_TIME = DateTime.Now;
                cs.DISPLAY_TEMPLATE = model.Template;
                cs.TEMPLATE_CONTENT = template;
                cs.FOR_VNEN = model.ForVNEN;
                cs.IS_LOCKED = model.IsLocked;
                cs.PERIOD_TYPE = model.PeriodType;
                cs.SCHOOL_ID = _globalInfo.SchoolID.Value;
                cs.GRADE = _globalInfo.AppliedLevel.Value;
                Guid guid = Guid.NewGuid();
                cs.TYPE_CODE = guid.ToString();
                cs.TYPE_NAME = model.SMSTypeName;
                cs.IS_ACTIVE = true;

                CustomSMSTypeBusiness.Insert(cs);

                message = "Thêm mới thông tin thành công.";
                code = "THEMMOI";

                log = string.Format("Thêm mới bản tin:Tên bản tin {0}, Khóa {1}, VNEN {2}, Định kỳ {3}, Nội dung bản tin {4}.", cs.TYPE_NAME,
                    cs.IS_LOCKED ? "Có" : "Không", cs.FOR_VNEN ? "Có" : "Không", GetPeriodName(cs.PERIOD_TYPE), cs.DISPLAY_TEMPLATE);
            }
            else
            {
                string beforeLog = string.Format("Nội dung trước khi sửa: Tên bản tin {0}, Khóa {1}, VNEN {2}, Định kỳ {3}, Nội dung bản tin {4}.",
                    cs.TYPE_NAME, cs.IS_LOCKED ? "Có" : "Không", cs.FOR_VNEN ? "Có" : "Không", GetPeriodName(cs.PERIOD_TYPE), cs.DISPLAY_TEMPLATE);

                cs.DISPLAY_TEMPLATE = model.Template;
                cs.TEMPLATE_CONTENT = template;
                cs.FOR_VNEN = model.ForVNEN;
                cs.IS_LOCKED = model.IsLocked;
                cs.PERIOD_TYPE = model.PeriodType;
                cs.TYPE_NAME = model.SMSTypeName;

                message = "Cập nhật thông tin thành công.";
                code = cs.TYPE_CODE;

                string afterLog = string.Format("Tên bản tin {0}, Khóa {1}, VNEN {2}, Định kỳ {3}, Nội dung bản tin {4}.",
                    cs.TYPE_NAME, cs.IS_LOCKED ? "Có" : "Không", cs.FOR_VNEN ? "Có" : "Không", GetPeriodName(cs.PERIOD_TYPE), cs.DISPLAY_TEMPLATE);

                log = string.Format("+ {0}\n+ {1}", afterLog, beforeLog);
            }


            CustomSMSTypeBusiness.Save();

            //save log
            //VTODO WriteLogInfo(log, null, string.Empty, string.Empty, null, string.Empty);

            return Json(new { Message = message, Type = GlobalConstantsEdu.SUCCESS, Code = code });
        }

        private string GetPeriodName(int periodType)
        {
            switch (periodType)
            {
                case 1:
                    return "Ngày";
                case 2:
                    return "Tuần";
                case 3:
                    return "Tháng";
                case 4:
                    return "Học kỳ";
                case 5:
                    return "Mở rộng";
                default:
                    return string.Empty;

            }
        }
        public PartialViewResult OpenDeleteDialog(int id)
        {
            return PartialView("_Delete", id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            SMS_CUSTOM_TYPE type = CustomSMSTypeBusiness.Find(id);

            //Kiem tra ban tin da duoc gui tin hay chua
            int partition = _globalInfo.SchoolID.Value % GlobalConstantsEdu.PARTITION_SMSHISTORY_SCHOOLID;
            int count = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == partition
                && o.SCHOOL_ID == _globalInfo.SchoolID.Value
                && o.RECEIVE_TYPE == 1
                && o.IS_SMS
                && o.SMS_TYPE_ID == type.CUSTOM_SMS_TYPE_ID
                && o.IS_CUSTOM_TYPE).Count();

            if (count > 0)
            {
                type.IS_ACTIVE = false;
            }
            else
            {
                CustomSMSTypeBusiness.Delete(type.CUSTOM_SMS_TYPE_ID);
            }


            CustomSMSTypeBusiness.Save();


            return Json(new { Message = "Xóa thông tin thành công.", Type = GlobalConstantsEdu.SUCCESS });
        }

        public PartialViewResult LoadCustomType(int id)
        {
            SMS_CUSTOM_TYPE type = CustomSMSTypeBusiness.Find(id);

            SMSConfigCreateViewModel model = new SMSConfigCreateViewModel();
            model.ForVNEN = type.FOR_VNEN;
            model.Id = type.CUSTOM_SMS_TYPE_ID;
            model.IsLocked = type.IS_LOCKED;
            model.PeriodType = type.PERIOD_TYPE;
            model.SMSTypeName = type.TYPE_NAME;
            model.Template = type.DISPLAY_TEMPLATE;

            //Lay content type
            List<SMS_CONTENT_TYPE> lstContentType = GetListContentType(model.PeriodType, model.ForVNEN);
            ViewData[SMSConfigConstants.LIST_CONTENT_TYPE] = lstContentType;

            return PartialView("Create", model);

        }
        /// <summary>
        /// Save Message
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSMSType(FormCollection col, SMSTypeViewModel model)
        {
            int typeID = 0;
            int.TryParse((col["hidTypeID"] != null) ? col["hidTypeID"] : _CONST_NUM_ZERO_TEXT, out typeID);
            int autoSend = 0;
            int.TryParse((col["hidAutoSend"] != null) ? col["hidAutoSend"] : _CONST_NUM_ZERO_TEXT, out autoSend);

            DateTime Now = DateTime.Now;
            DateTime sendTime = Now;
            bool isNew = false;
            SMS_TYPE smsType = SMSTypeBusiness.All.Where(a => a.SMS_TYPE_ID == typeID).FirstOrDefault();
            SMS_TYPE_DETAIL type = SMSTypeDetailBusiness.All.Where(a => a.SMS_TYPE_ID == typeID && a.SCHOOL_ID == _globalInfo.SchoolID.Value).FirstOrDefault();
            if (smsType == null) return Json(new JsonMessage(Res.Get("Message_Error_NotValid")));
            else if (smsType.TYPE_CODE.Trim().Equals(SMSTypeEnum.TNHKI) || smsType.TYPE_CODE.Trim().Equals(SMSTypeEnum.TNDTHK))
            {
                if (autoSend == 1)
                {
                    int day = Now.Day;
                    int month = Now.Month;
                    int year = Now.Year;
                    if (col["SendDate"] != null)
                    {
                        string[] sendDateStr = col["SendDate"].ToString().Split('/');

                        if (!(int.TryParse(sendDateStr[0], out day) && int.TryParse(sendDateStr[1], out month) && int.TryParse(sendDateStr[2], out year)))
                            return Json(new JsonMessage(Res.Get("Invalid_Date"), GlobalConstantsEdu.ERROR));
                    }
                    sendTime = new DateTime(year, month, day, 0, 0, 0);
                    if (model.SendTime != null)
                        sendTime = sendTime.AddHours(model.SendTime.Value.Hour).AddMinutes(model.SendTime.Value.Minute).AddSeconds(model.SendTime.Value.Second);

                    if (sendTime.Date < Now.Date) return Json(new JsonMessage("Ngày gửi phải lớn hơn ngày hiện tại", GlobalConstantsEdu.ERROR));

                    if (sendTime.Date > this.AcademicYear.SecondSemesterEndDate.Value.Date)
                        return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));

                    if (this.Semester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (sendTime.Date < this.AcademicYear.FirstSemesterEndDate.Value.AddDays(-10).Date)
                            return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                        if (sendTime.Date > this.AcademicYear.FirstSemesterEndDate.Value.Date)
                            return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                        type.SEND_BEFORE = (byte)this.AcademicYear.FirstSemesterEndDate.Value.Date.Subtract(sendTime.Date).TotalDays;
                    }
                    else
                    {
                        if (sendTime.Date < this.AcademicYear.SecondSemesterEndDate.Value.AddDays(-10).Date)
                            return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                        type.SEND_BEFORE = (byte)this.AcademicYear.SecondSemesterEndDate.Value.Date.Subtract(sendTime.Date).TotalDays;
                    }
                }
            }
            else if (smsType.TYPE_CODE.Trim().Equals("TT22KQCK") || smsType.TYPE_CODE.Trim().Equals("TT22KQGK"))
            {
                if (autoSend == 1)
                {
                    int day = Now.Day;
                    int month = Now.Month;
                    int year = Now.Year;
                    if (col["SendDate"] != null)
                    {
                        string[] sendDateStr = col["SendDate"].ToString().Split('/');

                        if (!(int.TryParse(sendDateStr[0], out day) && int.TryParse(sendDateStr[1], out month) && int.TryParse(sendDateStr[2], out year)))
                            return Json(new JsonMessage(Res.Get("Invalid_Date"), GlobalConstantsEdu.ERROR));
                    }
                    sendTime = new DateTime(year, month, day, 0, 0, 0);
                    if (model.SendTime != null)
                        sendTime = sendTime.AddHours(model.SendTime.Value.Hour).AddMinutes(model.SendTime.Value.Minute).AddSeconds(model.SendTime.Value.Second);

                    if (sendTime.Date < Now.Date) return Json(new JsonMessage("Ngày gửi phải lớn hơn hoặc bằng ngày hiện tại", GlobalConstantsEdu.ERROR));

                    if (smsType.TYPE_CODE.Trim().Equals("TT22KQCK"))
                    {
                        if (sendTime.Date > this.AcademicYear.SecondSemesterEndDate.Value.Date)
                            return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));

                        if (this.Semester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (sendTime.Date < this.AcademicYear.FirstSemesterEndDate.Value.AddDays(-10).Date)
                                return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                            if (sendTime.Date > this.AcademicYear.FirstSemesterEndDate.Value.Date)
                                return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                            type.SEND_BEFORE = (byte)this.AcademicYear.FirstSemesterEndDate.Value.Date.Subtract(sendTime.Date).TotalDays;
                        }
                        else
                        {
                            if (sendTime.Date < this.AcademicYear.SecondSemesterEndDate.Value.AddDays(-10).Date)
                                return Json(new JsonMessage("Ngày gửi phải trong khoảng 10 ngày trước ngày kết thúc học kỳ", GlobalConstantsEdu.ERROR));
                            type.SEND_BEFORE = (byte)this.AcademicYear.SecondSemesterEndDate.Value.Date.Subtract(sendTime.Date).TotalDays;
                        }
                    }
                }
            }
            else
            {
                if (model.SendTime != null) sendTime = model.SendTime.Value;
            }
            if (type == null)
            {
                type = new SMS_TYPE_DETAIL();
                isNew = true;
            }
            string beforeChangeLog = string.Format(_CONST_JSON_FORMAT, type.SEND_BEFORE, type.SEND_TIME, type.IS_AUTO_SEND, type.IS_LOCK);
            int locked = 0;
            int.TryParse((col["hidOnOff"] != null) ? col["hidOnOff"] : _CONST_NUM_ZERO_TEXT, out locked);
            type.IS_LOCK = locked == 1;

            type.IS_AUTO_SEND = autoSend == 1;

            bool isSendUnicodeMsg = false;
            bool.TryParse(col["IsSendUnicodeMessage"] != null ? col["IsSendUnicodeMessage"] : "false", out isSendUnicodeMsg);

            int dateBefore = 0;
            if (col["dateBefore"] != null)
            {
                int.TryParse((col["dateBefore"] != null) ? col["dateBefore"] : _CONST_NUM_ZERO_TEXT, out dateBefore);
                type.SEND_BEFORE = (byte)dateBefore;
            }

            type.IS_SEND_UNICODE = isSendUnicodeMsg;
            type.SEND_TIME = sendTime;
            //type.Template = model.Template; //Edit not allow.
            type.UPDATE_TIME = Now;
            if (isNew) SMSTypeDetailBusiness.Insert(type);
            else SMSTypeDetailBusiness.Save();
            SMSTypeBusiness.Save();
            SMS_TYPE stype = SMSTypeBusiness.Find(type.SMS_TYPE_ID);
            String userDescription = "Cấu hình bản tin " + stype.SMS_TYPE_NAME
                + (type.IS_LOCK ?" Khóa: Có" : " Khóa: Không") 
                + (type.IS_AUTO_SEND ?" Tự động: Có":" Tự động: Không" )
                + (type.SEND_TIME.HasValue ? (" Giờ gửi: " + type.SEND_TIME.Value.ToString("HH:mm:ss")) : "")
                + ("Gửi tin có dấu: " + (isSendUnicodeMsg ? "Có" : "Không"))
                + (" Ngày gửi: " + type.SEND_BEFORE);
            //VTODO WriteLogInfo(_CONST_LOG_TEXT, 0, string.Empty, string.Empty, 0, string.Empty, "Liên lạc - Bản tin", "3", userDescription);
            ViewData[GlobalConstantsEdu.UTILITY_OLD_LOG_JSON] = beforeChangeLog;
            ViewData[GlobalConstantsEdu.UTILITY_NEW_LOG_JSON] = string.Format(_CONST_JSON_FORMAT, type.SEND_BEFORE, type.SEND_TIME, type.IS_AUTO_SEND, type.IS_LOCK);
            ViewData[GlobalConstantsEdu.UTILITY_OLD_OBJECT_ID] = type.SMS_TYPE_ID;
            return Json(new JsonMessage(Res.Get("Message_Succeed_Saved")));
        }
    }
}