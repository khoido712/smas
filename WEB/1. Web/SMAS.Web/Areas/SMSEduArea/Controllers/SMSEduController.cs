﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class SMSEduController : Controller
    {
        
        /// <summary>
        /// Hộp thư
        /// </summary>
        /// <returns></returns>
        public ActionResult MailBox()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Gửi tin nhắn cho giáo viên
        /// </summary>
        /// <returns></returns>
        public ActionResult SendSMSToTeacher()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Gửi tin nhắn cho phụ huynh
        /// </summary>
        /// <returns></returns>
        public ActionResult SendSMSToParent()
        {
            GetsmsEduUrl();
            return View();
        }
	
        /// <summary>
        /// Gửi tin nhắn cho phụ huynh
        /// </summary>
        /// <returns></returns>
        public ActionResult SendSMSToParentRegister()
        {
            GetsmsEduUrl();
            return View();
        }
		
		/// <summary>
        /// Tra cuu ma 
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportCodeOnline()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Cấu hình
        /// </summary>
        /// <returns></returns>
        public ActionResult SchoolInfoConfig()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Bản tin
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageManagement()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Nhóm liên lạc
        /// </summary>
        /// <returns></returns>
        public ActionResult TeacherContractGroup()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Quản lý hợp đồng
        /// </summary>
        /// <returns></returns>
        public ActionResult SchoolContractManagement()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Thống kê liên lạc
        /// </summary>
        /// <returns></returns>
        public ActionResult SMSMessageHistory()
        {
            GetsmsEduUrl();
            return View();
        }
        /// <summary>
        /// Gui tin nhan cho truong
        /// </summary>
        /// <returns></returns>
        public ActionResult SendSMSToSchool()
        {
            GetsmsEduUrl();
            return View();
        }
        /// <summary>
        /// gui tin nhan cho phong/so
        /// </summary>
        /// <returns></returns>
        public ActionResult SendSMSToSupervisingDept()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// thong ke tin nhan phong/so
        /// </summary>
        /// <returns></returns>
        public ActionResult StatisticsCommunicationSupervisingDept()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// Thanh toan the cao
        /// </summary>
        /// <returns></returns>
        /// GET /SMSEdu/Payment
        public ActionResult Payment(int? topup)
        {
            GetsmsEduUrl();
            ViewBag.Topup = false;
            if (topup.HasValue)
            {
                ViewBag.Topup = true;
            }

            return View();
        }

        /// <summary>
        /// khai bao goi cuoc
        /// </summary>
        /// <returns></returns>
        public ActionResult DeclarePackage()
        {
            GetsmsEduUrl();
            return View();
        }

        /// <summary>
        /// khai bao goi cuoc
        /// </summary>
        /// <returns></returns>
        public ActionResult TransactionHistory()
        {
            GetsmsEduUrl();
            return View();
        }

        // <summary>
        /// Bao cao su dung
        /// </summary>
        /// <returns></returns>
        public ActionResult UsageStatistic()
        {
            GetsmsEduUrl();
            return View();
        }

        public ActionResult ConSumptionReport()
        {
            GetsmsEduUrl();
            return View();
        }

        public ActionResult PromotionProgram()
        {
            GetsmsEduUrl();
            return View();
        }

        public ActionResult RegisterSMSFree()
        {
            GetsmsEduUrl();
            return View();
        }

        public ActionResult BrandnameRegistration()
        {
            GetsmsEduUrl();
            return View();
        }

        private void GetsmsEduUrl()
        {
            ViewData[SMSEduConstanst.SMS_SCHOOL_URL] = System.Configuration.ConfigurationManager.AppSettings["smsEduUrl"];
        }
    }
}
