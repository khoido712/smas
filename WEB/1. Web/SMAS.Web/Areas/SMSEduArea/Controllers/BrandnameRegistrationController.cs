﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class BrandnameRegistrationController:BaseController
    {
        #region Private fields
        private readonly IBrandnameRegistrationBusiness BrandnameRegistrationBusiness;
        private readonly IEWalletBusiness EwalletBusiness;
        private readonly IBrandnameProviderBusiness BrandnameProviderBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        #endregion

        #region Constructor
        public BrandnameRegistrationController(IBrandnameRegistrationBusiness BrandnameRegistrationBusiness, IEWalletBusiness EwalletBusiness,
            IBrandnameProviderBusiness BrandnameProviderBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.BrandnameRegistrationBusiness = BrandnameRegistrationBusiness;
            this.EwalletBusiness = EwalletBusiness;
            this.BrandnameProviderBusiness = BrandnameProviderBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            UpdateRegistrations();

            List<BrandnameRegisListViewModel> model = this._Search();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search()
        {
            List<BrandnameRegisListViewModel> model = this._Search();
            return PartialView("_DataGrid", model);
        }

        public ActionResult Regist()
        {
            BrandnameRegisCreateViewModel model = new BrandnameRegisCreateViewModel();

            //Lay thong tin so du tai khoan truong
            int AccountID = 0;
            bool isSchool, isSup;
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                AccountID = _globalInfo.SupervisingDeptID.Value;
                isSup = true;
                isSchool = false;
            }
            else
            {
                AccountID = _globalInfo.SchoolID.Value;
                isSup = false;
                isSchool = true;
            }
            EwalletBO ewalletObj = EwalletBusiness.GetEWallet(AccountID, isSchool, isSup);
            model.Balance = ewalletObj.Balance;

            //Generate so hop dong
            string contractNumber = string.Format("{0}/VT-", DateTime.Now.Date.ToString("ddMMyyyy"));
            model.ContractNumber = contractNumber;

            //Lay ra danh sach cac brandname da dang ky
            List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();

            //Lay ra cac nha mang da dang ky
            List<int> lstRegistedProviderId = lstBr.Where(o => o.STATUS != GlobalConstantsEdu.STATUS_IS_CANCELED
                && o.STATUS != GlobalConstantsEdu.STATUS_IS_EXPIRED).Select(o => o.PROVIDER_ID).ToList();

            //Lay danh sach nha mang chua dang ky hoac da dang ky nhung da het han hoac da huy
            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.OrderBy(o => o.ORDER_NUMBER)
                .Where(o => !lstRegistedProviderId.Contains(o.BRANDNAME_PROVIDER_ID)).ToList();

            model.ListProvider = lstProvider.Select(o => new ProviderSelectionModel
            {
                ProviderId = o.BRANDNAME_PROVIDER_ID,
                ProviderName = o.PROVIDER_NAME,
                RegistFee = o.REGIST_FEE,
                MaintainFee = o.MAINTAIN_FEE,
                ExtendFee = o.EXTEND_FEE
            }).ToList();

            decimal registFee = 0;
            decimal maintainFee = 0;
            for (int i = 0; i < lstProvider.Count; i++)
            {
                registFee += lstProvider[i].REGIST_FEE;
                maintainFee += lstProvider[i].MAINTAIN_FEE;
            }
            model.RegistFee = registFee;
            model.MaintainFee = maintainFee;
            model.TotalFee = registFee + maintainFee;

            return PartialView("_Registration", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Regist(BrandnameRegisCreateViewModel model)
        {
            //Kiem tra brandname
            if (string.IsNullOrEmpty(model.Brandname)
                || model.Brandname.Length < 3
                || model.Brandname.Length > 11)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Brandname xin cấp phải chứa từ 3-11 ký tự." });
            }

            Regex regex = new Regex("^[A-Za-z0-9-_\\s]+$");
            Match match = regex.Match(model.Brandname);
            if (!match.Success)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Brandname không được chứa các ký tự đặc biệt." });
            }

            List<ProviderSelectionModel> lstSelectedProvider = model.ListProvider.Where(o => o.IsCheck).ToList();
            if (lstSelectedProvider.Count == 0)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Chưa có nhà mạng được chọn." });
            }

            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();
            //Tinh lai tong phi gia han
            decimal totalFee = 0;
            foreach (ProviderSelectionModel p in lstSelectedProvider)
            {
                SMS_BRANDNAME_PROVIDER bp = lstProvider.First(o => o.BRANDNAME_PROVIDER_ID == p.ProviderId);
                totalFee += bp.MAINTAIN_FEE * p.Duration + bp.REGIST_FEE;
            }

            SMS_BRANDNAME_REGISTRATION br;
            for (int i = 0; i < lstSelectedProvider.Count; i++)
            {
                ProviderSelectionModel pr = lstSelectedProvider[i];
                if (pr.IsCheck)
                {
                    br = new SMS_BRANDNAME_REGISTRATION();
                    br.BRAND_NAME = model.Brandname;
                    br.CONTRACT_NUMBER = model.ContractNumber;
                    br.CREATE_TIME = DateTime.Now;
                    br.DURATION = pr.Duration;
                    br.PROVIDER_ID = pr.ProviderId;
                    br.UNIT_ID = this.GetUnitID();
                    br.STATUS = GlobalConstantsEdu.STATUS_WAIT_FOR_APPROVAL;
                    br.IS_SUPER_VISING_DEPT = _globalInfo.IsSuperVisingDeptRole;

                    BrandnameRegistrationBusiness.Insert(br);
                }
            }

            BrandnameRegistrationBusiness.Save();

            return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = string.Format("Đăng ký thành công. Thời gian xem xét phê duyệt đăng ký là 07 ngày. Để phê duyệt thành công, số dư tài khoản cần phải có tối thiểu {0} VNĐ.", totalFee) });
        }

        public ActionResult Extend(int id)
        {
            BrandnameRegisCreateViewModel model = new BrandnameRegisCreateViewModel();

            //Lay thong tin so du tai khoan truong
            int AccountID = 0;
            bool isSchool, isSup;
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                AccountID = _globalInfo.SupervisingDeptID.Value;
                isSup = true;
                isSchool = false;
            }
            else
            {
                AccountID = _globalInfo.SchoolID.Value;
                isSup = false;
                isSchool = true;
            }
            EwalletBO ewalletObj = EwalletBusiness.GetEWallet(AccountID, isSchool, isSup);
            model.Balance = ewalletObj.Balance;

            //Lay brandname gia han
            SMS_BRANDNAME_REGISTRATION br = BrandnameRegistrationBusiness.Find(id);

            if (br == null)
            {
                throw new ArgumentNullException();
            }

            model.BrandnameRegistrationId = br.BRANDNAME_REGISTRATION_ID;
            model.ContractNumber = br.CONTRACT_NUMBER;
            model.Brandname = br.BRAND_NAME;

            //Lay ra danh sach cac brandname da dang ky
            List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();

            //Lay danh sach nha mang chua dang ky hoac da dang ky nhung da het han hoac da huy
            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();

            model.ListProvider = (from b in lstBr
                                  join p in lstProvider on b.PROVIDER_ID equals p.BRANDNAME_PROVIDER_ID
                                  where (b.STATUS == GlobalConstantsEdu.STATUS_WAIT_FOR_EXTENDING
                                  || b.STATUS == GlobalConstantsEdu.STATUS_IS_LOCKED
                                  || b.STATUS == GlobalConstantsEdu.STATUS_IS_ACTIVE)
                                  && b.CONTRACT_NUMBER.Equals(br.CONTRACT_NUMBER)
                                  && b.BRAND_NAME.Equals(br.BRAND_NAME)
                                  orderby p.ORDER_NUMBER

                                  select new ProviderSelectionModel
                                  {
                                      ProviderId = b.PROVIDER_ID,
                                      ProviderName = p.PROVIDER_NAME,
                                      RegistFee = p.REGIST_FEE,
                                      MaintainFee = p.MAINTAIN_FEE,
                                      ExtendFee = p.EXTEND_FEE,
                                      EndDate = b.TO_DATE,
                                      IsDefault = b.PROVIDER_ID == br.PROVIDER_ID,
                                      BrandnameRegistrationId = b.BRANDNAME_REGISTRATION_ID
                                  }).ToList();

            decimal maintainFee = 0;
            for (int i = 0; i < model.ListProvider.Count; i++)
            {
                maintainFee += model.ListProvider[i].MaintainFee;
            }

            model.MaintainFee = maintainFee;

            return PartialView("_Extending", model);
        }

        [HttpPost]
        //[Log]
        [ValidateAntiForgeryToken]
        public ActionResult Extend(BrandnameRegisCreateViewModel model)
        {
            List<ProviderSelectionModel> lstSelectedProvider = model.ListProvider.Where(o => o.IsCheck || o.BrandnameRegistrationId == model.BrandnameRegistrationId).ToList();

            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();
            //Tinh lai tong phi gia han
            int totalFee = 0;
            foreach (ProviderSelectionModel p in lstSelectedProvider)
            {
                SMS_BRANDNAME_PROVIDER bp = lstProvider.First(o => o.BRANDNAME_PROVIDER_ID == p.ProviderId);
                totalFee += bp.EXTEND_FEE * p.Duration;
            }

            //Kiem tra so du tai khoan
            int AccountID = 0;
            bool isSchool, isSup;
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                AccountID = _globalInfo.SupervisingDeptID.Value;
                isSup = true;
                isSchool = false;
            }
            else
            {
                AccountID = _globalInfo.SchoolID.Value;
                isSup = false;
                isSchool = true;
            }
            EwalletBO ewalletObj = EwalletBusiness.GetEWallet(AccountID, isSchool, isSup);
            int eWalletID = ewalletObj.EWalletID;
            int balance = ewalletObj.Balance;

            if (balance >= totalFee)
            {
                List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();

                for (int i = 0; i < lstSelectedProvider.Count; i++)
                {
                    ProviderSelectionModel ps = lstSelectedProvider[i];
                    SMS_BRANDNAME_REGISTRATION br = lstBr.First(o => o.BRANDNAME_REGISTRATION_ID == ps.BrandnameRegistrationId);
                    br.UPDATE_TIME = DateTime.Now;
                    br.TO_DATE = ConvertToLastDayOfMonth(br.TO_DATE.Value.AddMonths(ps.Duration));
                    br.STATUS = GlobalConstantsEdu.STATUS_IS_ACTIVE;
                }

                BrandnameRegistrationBusiness.Save();

                //Tru tien tai khoan
                EW_EWALLET eo = EwalletBusiness.ChangeAmountSub(eWalletID, totalFee, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB, 1, GlobalConstantsEdu.SERVICE_TYPE_BRANDNAME_PAYMENT);

                //Ghi log
                string detail = string.Empty;
                for (int i = 0; i < lstSelectedProvider.Count; i++)
                {
                    detail = string.Format("mạng {0} ({1})", lstSelectedProvider[i].ProviderName, lstSelectedProvider[i].Duration);
                    if (i < lstSelectedProvider.Count - 1)
                    {
                        detail += ", ";
                    }
                }

                string logContent = string.Format("Thanh toán gia hạn dịch vụ Brandname, {0}. Tổng số tiền: {1}."
                    , detail, totalFee);

                //VTODO WriteLogInfo(logContent, null, string.Empty, string.Empty, null, string.Empty);

                return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = "Gia hạn thành công." });
            }
            else
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Số dư tài khoản không đủ để thực hiện gia hạn." });
            }
        }

        public ActionResult Update(int id)
        {
            BrandnameRegisCreateViewModel model = new BrandnameRegisCreateViewModel();

            //Lay thong tin so du tai khoan truong
            int AccountID = 0;
            bool isSchool, isSup;
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                AccountID = _globalInfo.SupervisingDeptID.Value;
                isSup = true;
                isSchool = false;
            }
            else
            {
                AccountID = _globalInfo.SchoolID.Value;
                isSup = false;
                isSchool = true;
            }
            EwalletBO ewalletObj = EwalletBusiness.GetEWallet(AccountID, isSchool, isSup);
            model.Balance = ewalletObj.Balance;

            //Lay brandname cap nhat
            SMS_BRANDNAME_REGISTRATION br = BrandnameRegistrationBusiness.Find(id);

            if (br == null)
            {
                throw new ArgumentNullException();
            }

            model.BrandnameRegistrationId = br.BRANDNAME_REGISTRATION_ID;
            model.ContractNumber = br.CONTRACT_NUMBER;
            model.Brandname = br.BRAND_NAME;

            //Lay ra danh sach cac brandname da dang ky
            List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();

            //Lay danh sach nha mang chua dang ky hoac da dang ky nhung da het han hoac da huy
            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();

            model.ListProvider = (from b in lstBr
                                  join p in lstProvider on b.PROVIDER_ID equals p.BRANDNAME_PROVIDER_ID
                                  where b.STATUS == GlobalConstantsEdu.STATUS_WAIT_FOR_APPROVAL
                                  && b.CONTRACT_NUMBER.Equals(br.CONTRACT_NUMBER)
                                  && b.BRAND_NAME.Equals(br.BRAND_NAME)
                                  orderby p.ORDER_NUMBER
                                  select new ProviderSelectionModel
                                  {
                                      ProviderId = b.PROVIDER_ID,
                                      ProviderName = p.PROVIDER_NAME,
                                      RegistFee = p.REGIST_FEE,
                                      MaintainFee = p.MAINTAIN_FEE,
                                      ExtendFee = p.EXTEND_FEE,
                                      EndDate = b.TO_DATE,
                                      Duration = b.DURATION,
                                      IsDefault = b.PROVIDER_ID == br.PROVIDER_ID,
                                      BrandnameRegistrationId = b.BRANDNAME_REGISTRATION_ID
                                  }).ToList();

            decimal registFee = 0;
            decimal maintainFee = 0;
            for (int i = 0; i < model.ListProvider.Count; i++)
            {
                registFee += model.ListProvider[i].RegistFee;
                maintainFee += model.ListProvider[i].MaintainFee * model.ListProvider[i].Duration;
            }

            model.RegistFee = registFee;
            model.MaintainFee = maintainFee;
            model.TotalFee = registFee + maintainFee;

            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(BrandnameRegisCreateViewModel model)
        {
            //Kiem tra brandname
            if (string.IsNullOrEmpty(model.Brandname)
                || model.Brandname.Length < 3
                || model.Brandname.Length > 11)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Brandname xin cấp phải chứa từ 3-11 ký tự." });
            }

            Regex regex = new Regex("^[A-Za-z0-9-_\\s]+$");
            Match match = regex.Match(model.Brandname);
            if (!match.Success)
            {
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = "Brandname không được chứa các ký tự đặc biệt." });
            }

            List<ProviderSelectionModel> lstSelectedProvider = model.ListProvider.Where(o => o.IsCheck || o.BrandnameRegistrationId == model.BrandnameRegistrationId).ToList();

            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();
            //Tinh lai tong phi 
            decimal totalFee = 0;
            foreach (ProviderSelectionModel p in lstSelectedProvider)
            {
                SMS_BRANDNAME_PROVIDER bp = lstProvider.First(o => o.BRANDNAME_PROVIDER_ID == p.ProviderId);
                totalFee += bp.MAINTAIN_FEE * p.Duration + bp.REGIST_FEE;
            }

            List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();

            for (int i = 0; i < lstSelectedProvider.Count; i++)
            {
                ProviderSelectionModel ps = lstSelectedProvider[i];
                SMS_BRANDNAME_REGISTRATION br = lstBr.First(o => o.BRANDNAME_REGISTRATION_ID == ps.BrandnameRegistrationId);
                br.UPDATE_TIME = DateTime.Now;
                br.CONTRACT_NUMBER = model.ContractNumber;
                br.BRAND_NAME = model.Brandname;
                br.DURATION = ps.Duration;
            }

            BrandnameRegistrationBusiness.Save();

            return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = string.Format("Đăng ký thành công. Thời gian xem xét phê duyệt đăng ký là 07 ngày. Để phê duyệt thành công, số dư tài khoản cần phải có tối thiểu {0} VNĐ.", totalFee) });

        }

        public PartialViewResult Cancel(int id, int? temp)
        {
            return PartialView("_Cancel", id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancel(int id)
        {
            SMS_BRANDNAME_REGISTRATION br = BrandnameRegistrationBusiness.Find(id);

            if (br == null)
            {
                throw new ArgumentNullException();
            }

            br.UPDATE_TIME = DateTime.Now;
            br.STATUS = GlobalConstantsEdu.STATUS_IS_CANCELED;

            BrandnameRegistrationBusiness.Save();

            return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = "Hủy đăng ký thành công." });
        }

        public ActionResult Policy()
        {
            return View();
        }
        #endregion

        #region private method
        private void UpdateRegistrations()
        {
            List<SMS_BRANDNAME_REGISTRATION> lstBr = this.GetAllRegistration();
            for (int i = 0; i < lstBr.Count; i++)
            {
                SMS_BRANDNAME_REGISTRATION br = lstBr[i];

                if (br.STATUS == GlobalConstantsEdu.STATUS_IS_ACTIVE)
                {
                    if (DateTime.Compare(br.TO_DATE.Value, DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_WAIT_FOR_EXTENDING;
                    }

                    if (DateTime.Compare(br.TO_DATE.Value.AddDays(3), DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_IS_LOCKED;
                    }

                    if (DateTime.Compare(br.TO_DATE.Value.AddDays(25), DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_IS_EXPIRED;
                    }
                }
                else if (br.STATUS == GlobalConstantsEdu.STATUS_WAIT_FOR_EXTENDING)
                {
                    if (DateTime.Compare(br.TO_DATE.Value.AddDays(3), DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_IS_LOCKED;
                    }

                    if (DateTime.Compare(br.TO_DATE.Value.AddDays(25), DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_IS_EXPIRED;
                    }
                }
                else if (br.STATUS == GlobalConstantsEdu.STATUS_IS_LOCKED)
                {
                    if (DateTime.Compare(br.TO_DATE.Value.AddDays(25), DateTime.Now.Date) < 0)
                    {
                        br.STATUS = GlobalConstantsEdu.STATUS_IS_EXPIRED;
                    }
                }
            }

            BrandnameRegistrationBusiness.Save();
        }

        private List<BrandnameRegisListViewModel> _Search()
        {
            List<SMS_BRANDNAME_PROVIDER> lstProvider = BrandnameProviderBusiness.All.ToList();
            List<BrandnameRegisListViewModel> list =(from b in this.GetAllRegistration()
                join p in lstProvider on b.PROVIDER_ID equals p.BRANDNAME_PROVIDER_ID
                    select new BrandnameRegisListViewModel
                    {
                        Brandname = b.BRAND_NAME,
                        BrandnameRegistrationId = b.BRANDNAME_REGISTRATION_ID,
                        ContractNumber = b.CONTRACT_NUMBER,
                        CreateTime = b.CREATE_TIME,
                        FromDate = b.FROM_DATE,
                        ProviderId = b.PROVIDER_ID,
                        ProviderName = p.PROVIDER_NAME,
                        Status = b.STATUS,
                        ToDate = b.TO_DATE
                    }).ToList();


            return list;
        }

        private List<SMS_BRANDNAME_REGISTRATION> GetAllRegistration()
        {
            int unitId = this.GetUnitID();
            return BrandnameRegistrationBusiness.GetListByUnitID(unitId, _globalInfo.IsSuperVisingDeptRole).ToList();
        }

        private int GetUnitID()
        {
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                int unitId;

                SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
                if (sup == null)
                {
                    throw new SMAS.Business.Common.BusinessException("Không tìm thấy thông tin phòng sở.");
                }
                else
                {
                    // Nhân viên thuộc Phòng trực thuộc (không có tài khoản) thì sử dụng tài khoản Sở
                    // Nhân viên thuộc Phòng ban thuộc phòng (không có tài khoản) thì sử dụng tài khoản Phòng Quản lý
                    if ((sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT ||
                        sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT) &&
                        sup.ParentID.HasValue)
                    {
                        SupervisingDept parent = SupervisingDeptBusiness.Find(sup.ParentID);
                        unitId = parent.SupervisingDeptID;
                    }
                    else
                    {
                        unitId = sup.SupervisingDeptID;
                    }
                }

                return unitId;
            }
            else
            {
                return _globalInfo.SchoolID.Value;
            }
        }
        private DateTime ConvertToLastDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }
        #endregion
    }
}