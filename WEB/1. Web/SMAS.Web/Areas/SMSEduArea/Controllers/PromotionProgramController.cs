﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class PromotionProgramController:BaseController
    {
        private readonly IPromotionBusiness PromotionProgramBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IPromotionDetailBusiness PromotionDetailBusiness;
        public PromotionProgramController(IPromotionBusiness PromotionProgramBsn,
            IProvinceBusiness ProvinceBsn, IDistrictBusiness districtBusiness, IPromotionDetailBusiness promotionDetailBusiness)
        {
            this.PromotionProgramBusiness = PromotionProgramBsn;
            this.ProvinceBusiness = ProvinceBsn;
            this.DistrictBusiness = districtBusiness;
            this.PromotionDetailBusiness = promotionDetailBusiness;
        }

        //private static int Total = 0;
        //
        // GET: /PromotionProgramArea/PromotionProgram/
        public ActionResult Index()
        {
            ViewData[PromotionProgramConstant.LST_PROMOTION_PROGRAM] = GetListPromotionProgram();

            return View();
        }

        public List<PromotionProgramBO> GetListPromotionProgram()
        {
            List<PromotionProgramBO> res = new List<PromotionProgramBO>();
            res = PromotionProgramBusiness.GetLstPromotionProgram();

            return res;
        }

        public List<ProvinceApplyBO> GetLstProvince(int prID)
        {
            var res = ProvinceBusiness.GetListProvince(prID).OrderBy(o => o.ProvinceName).ToList();

            return res;
        }

        public ActionResult Search(FormCollection frm)
        {
            ViewData[PromotionProgramConstant.LST_PROMOTION_PROGRAM] = GetListPromotionProgram();

            return PartialView("_List");
        }

        public PartialViewResult AjaxLoadSchoolApplied(int PromotionID)
        {
            ViewData[PromotionProgramConstant.LIST_PROVINCE_APPLY] = GetLstProvince(0);
            ViewData[PromotionProgramConstant.PROMOTION_ID] = PromotionID;
            return PartialView("_SchoolApplied");
        }

        public PartialViewResult SearchSchoolApplied(FormCollection frm)
        {
            int ProvinceID = !string.IsNullOrEmpty(frm["ProvinceID"]) ? int.Parse(frm["ProvinceID"]) : 0;
            int DistrictID = !string.IsNullOrEmpty(frm["DistrictID"]) ? int.Parse(frm["DistrictID"]) : 0;
            string SchoolName = frm["SchoolName"];
            string Account = frm["Account"];
            int TypeID = !string.IsNullOrEmpty(frm["TypeID"]) ? int.Parse(frm["TypeID"]) : 0;
            int StatusID = !string.IsNullOrEmpty(frm["StatusID"]) ? int.Parse(frm["StatusID"]) : 0;
            int currentPage = 1;
            int PromotionID = !string.IsNullOrEmpty(frm["PromotionID"]) ? int.Parse(frm["PromotionID"]) : 0;
            //lay danh sach nhung truong dang ap dung chuong trinh
            Dictionary<string, object> dicSearchDetail = new Dictionary<string, object>()
            {
                {"PromotionID",PromotionID},
                {"ProvinceID",ProvinceID}
            };
            IQueryable<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.Search(dicSearchDetail);
            IQueryable<PromotionProgramDetailBO2> iqResult = this.GetPromotionProgramDetailBO(lstPromotionDetail, ProvinceID, DistrictID, SchoolName, Account, TypeID, StatusID, PromotionID);

            ViewData[PromotionProgramConstant.TOTAL] = iqResult.Count();
            ViewData[PromotionProgramConstant.CURRENT_PAGE] = currentPage;
            List<SchoolAppliedViewModel> lstResult = (from r in iqResult
                                                      select new SchoolAppliedViewModel
                                                      {
                                                          UnitID = r.UnitID,
                                                          UnitName = r.UnitName,
                                                          ProvinceID = r.ProvinceID,
                                                          ProvinceName = r.ProvinceName,
                                                          DistrictID = r.DistrictID,
                                                          DistrictName = r.DistrictName,
                                                          Account = r.Account,
                                                          HasbeenApplied = r.HasBeenApplied
                                                      }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName).ThenBy(p => p.UnitName)
                                                      .Skip((currentPage - 1) * PromotionProgramConstant.PAGE_SIZE).Take(PromotionProgramConstant.PAGE_SIZE).ToList();
            SchoolAppliedViewModel objResult = null;
            SMS_PROMOTION_DETAIL objPromotionDetail = null;
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                objPromotionDetail = lstPromotionDetail.Where(p => p.PROMOTION_ID == PromotionID && p.PROVINCE_ID == objResult.ProvinceID && p.UNIT_ID == objResult.UnitID).FirstOrDefault();
                objResult.PromotionDetailID = objPromotionDetail != null ? objPromotionDetail.SMS_PROMOTION_DETAIL_ID : 0;
                if (objResult.HasbeenApplied)
                {
                    objResult.isEnablechkApp = false;
                    objResult.isEnablechkNApp = true;
                }
                else
                {
                    objResult.isEnablechkApp = true;
                    objResult.isEnablechkNApp = false;
                }
            }

            ViewData[PromotionProgramConstant.LIST_SCHOOL_APPLIED] = lstResult;
            return PartialView("_ListSchoolApplied");
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchSchoolAppliedAjax(int PromotionID, int? ProvinceID, int? DistrictID, int TypeID, int StatusID, string SchoolName, string Account, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;
            //lay danh sach nhung truong dang ap dung chuong trinh
            Dictionary<string, object> dicSearchDetail = new Dictionary<string, object>()
            {
                {"PromotionID",PromotionID},
                {"ProvinceID",ProvinceID}
            };
            IQueryable<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.Search(dicSearchDetail);
            IQueryable<PromotionProgramDetailBO2> iqResult = this.GetPromotionProgramDetailBO(lstPromotionDetail, ProvinceID, DistrictID, SchoolName, Account, TypeID, StatusID, PromotionID);
            List<SchoolAppliedViewModel> lstResult = (from r in iqResult
                                                      select new SchoolAppliedViewModel
                                                      {
                                                          UnitID = r.UnitID,
                                                          UnitName = r.UnitName,
                                                          ProvinceID = r.ProvinceID,
                                                          ProvinceName = r.ProvinceName,
                                                          DistrictID = r.DistrictID,
                                                          DistrictName = r.DistrictName,
                                                          Account = r.Account,
                                                          HasbeenApplied = r.HasBeenApplied
                                                      }).OrderBy(p => p.ProvinceName).ThenBy(p => p.DistrictName).ThenBy(p => p.UnitName)
                                                      .Skip((currentPage - 1) * PromotionProgramConstant.PAGE_SIZE).Take(PromotionProgramConstant.PAGE_SIZE).ToList();
            SchoolAppliedViewModel objResult = null;
            SMS_PROMOTION_DETAIL objPromotionDetail = null;
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                if (objResult.HasbeenApplied)
                {
                    objPromotionDetail = lstPromotionDetail.Where(p => p.PROMOTION_ID == PromotionID && p.PROVINCE_ID == objResult.ProvinceID && p.UNIT_ID == objResult.UnitID).FirstOrDefault();
                    objResult.isEnablechkApp = false;
                    objResult.isEnablechkNApp = true;
                    objResult.PromotionDetailID = objPromotionDetail != null ? objPromotionDetail.SMS_PROMOTION_DETAIL_ID : 0;
                }
                else
                {
                    objResult.isEnablechkApp = true;
                    objResult.isEnablechkNApp = false;
                }
            }
            return View(new GridModel<SchoolAppliedViewModel>
            {
                Total = iqResult.Count(),
                Data = lstResult
            });
        }
        private IQueryable<PromotionProgramDetailBO2> GetPromotionProgramDetailBO(IQueryable<SMS_PROMOTION_DETAIL> lstUnit, int? ProvinceID, int? DistrictID, string SchoolName, string Account, int TypeID, int StatusID, int PromotionID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ProvinceID",ProvinceID},
                {"DistrictID",DistrictID},
                {"SchoolName",SchoolName},
                {"Account",Account}
            };
            IQueryable<PromotionProgramDetailBO> iquery = PromotionProgramBusiness.GetlistSchool(dic);
            IQueryable<PromotionProgramDetailBO> iquerySup = PromotionProgramBusiness.GetlistSupervisingDept(dic);
            IQueryable<PromotionProgramDetailBO> iqResult = null;
            if (TypeID == 1)//Tim theo truong
            {
                iqResult = iquery;
            }
            else if (TypeID == 2)//Tim theo phong so
            {
                iqResult = iquerySup;
            }
            else//tim tat ca
            {
                iqResult = iquery.Union(iquerySup);
            }

            var result = from r in iqResult
                       join u in lstUnit on r.UnitID equals u.UNIT_ID into des
                       from g in des.DefaultIfEmpty()
                       select new 
                       {
                           UnitID = r.UnitID,
                           UnitName = r.UnitName,
                           ProvinceID = r.ProvinceID,
                           ProvinceName = r.ProvinceName,
                           DistrictID = r.DistrictID,
                           DistrictName = r.DistrictName,
                           Account = r.Account,
                           HasBeenApplied = g != null ? true : false
                       };

            if (StatusID == 1)//tim theo trang thai dang ap dung
            {

                result = result.Where(p => p.HasBeenApplied);
            }
            else if (StatusID == 2)//tim theo chua ap dung
            {
                result = result.Where(p => !p.HasBeenApplied);
            }
            return result.Select(o => new PromotionProgramDetailBO2
            {
                UnitID = o.UnitID,
                UnitName = o.UnitName,
                ProvinceID = o.ProvinceID,
                ProvinceName = o.ProvinceName,
                DistrictID = o.DistrictID,
                DistrictName = o.DistrictName,
                Account = o.Account,
                HasBeenApplied = o.HasBeenApplied
            });
        }
        public ActionResult AjaxLoadDistrictID(int ProvinceID)
        {
            List<District> lstDistrict = DistrictBusiness.GetListDistrict(ProvinceID).ToList();
            if (lstDistrict != null) { return Json(new SelectList(lstDistrict, "DistrictID", "DistrictName")); }
            return Json(new SelectList(new List<District>(), "DistrictID", "DistrictName"), JsonRequestBehavior.AllowGet);
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveUnitApplied(string ArrStringID, int PromotionID, int? ProvinceID, int? DistrictID, int TypeID, int StatusID, string UnitName, string Account, int isAllList)
        {
            List<string> lstStringID = new List<string>();
            List<SMS_PROMOTION_DETAIL> lstPromotionDetail = new List<SMS_PROMOTION_DETAIL>();
            SMS_PROMOTION_DETAIL objPromotionDetail = null;
            List<int> lstResultID = new List<int>();
            string strStringID = string.Empty;
            if (isAllList == 0)
            {
                lstStringID = ArrStringID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                for (int i = 0; i < lstStringID.Count; i++)
                {
                    strStringID = lstStringID[i];
                    objPromotionDetail = new SMS_PROMOTION_DETAIL();
                    lstResultID = strStringID.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    objPromotionDetail.PROMOTION_ID = PromotionID;
                    objPromotionDetail.PROVINCE_ID = lstResultID[0];
                    objPromotionDetail.UNIT_ID = lstResultID[1];
                    objPromotionDetail.CREATE_TIME = DateTime.Now;
                    lstPromotionDetail.Add(objPromotionDetail);
                }
            }
            else//ap dung toan bo danh sach
            {
                //lay danh sach nhung truong dang ap dung chuong trinh
                Dictionary<string, object> dicSearchDetail = new Dictionary<string, object>()
                {
                    {"PromotionID",PromotionID},
                    {"ProvinceID",ProvinceID}
                };

                IQueryable<SMS_PROMOTION_DETAIL> iq = PromotionDetailBusiness.Search(dicSearchDetail);
                IQueryable<PromotionProgramDetailBO2> iqPPDBO = this.GetPromotionProgramDetailBO(iq, ProvinceID, DistrictID, UnitName, Account, TypeID, StatusID, PromotionID);
                List<PromotionProgramDetailBO2> lstPPDBO = iqPPDBO.Where(p => !p.HasBeenApplied).ToList();
                PromotionProgramDetailBO2 objPPDBO = null;
                for (int i = 0; i < lstPPDBO.Count; i++)
                {
                    objPPDBO = lstPPDBO[i];
                    objPromotionDetail = new SMS_PROMOTION_DETAIL();
                    objPromotionDetail.PROMOTION_ID = PromotionID;
                    objPromotionDetail.PROVINCE_ID = objPPDBO.ProvinceID.HasValue ? objPPDBO.ProvinceID.Value : 0;
                    objPromotionDetail.UNIT_ID = objPPDBO.UnitID;
                    objPromotionDetail.CREATE_TIME = DateTime.Now;
                    lstPromotionDetail.Add(objPromotionDetail);
                }
            }
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"PromotionID",PromotionID}
            };
            string Message = PromotionDetailBusiness.InsertPromotionDetail(lstPromotionDetail, dic);
            bool isError = !string.IsNullOrEmpty(Message);
            return Json(new JsonMessage(isError ? Message : "Cập nhật thành công", isError ? "error" : "success"));
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteUnitApplied(string ArrStringID, int PromotionID, int? ProvinceID, int isAllList)
        {
            List<long> lstPromotionDetailID = new List<long>();
            if (isAllList == 0)//khong ap dung toan bo danh sach
            {
                lstPromotionDetailID = ArrStringID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => long.Parse(p)).ToList();
            }
            else
            {
                //lay danh sach nhung truong dang ap dung chuong trinh
                Dictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"PromotionID",PromotionID},
                    {"ProvinceID",ProvinceID}
                };
                List<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.GetlistPromotionDetail(dic);
                lstPromotionDetailID = lstPromotionDetail.Select(p => p.SMS_PROMOTION_DETAIL_ID).Distinct().ToList();
            }

            PromotionDetailBusiness.DeletePromotionDetail(lstPromotionDetailID);
            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }

        public JsonResult AjaxLoadNumberApplied(int PromotionID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"PromotionID",PromotionID}
            };
            List<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.GetlistPromotionDetail(dic);
            return Json(new JsonMessage(string.Format("{0} đơn vị", lstPromotionDetail.Count), "success"));
        }

        public PartialViewResult GetCreate()
        {
            ViewData[PromotionProgramConstant.LIST_PROVINCE_APPLY] = GetLstProvince(0);
            return PartialView("_Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection frm)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;
            bool isFreeSMS = false;
            bool isFreeExSMS = false;
            int FreeSMS = 0;
            int FreeExSMS = 0;
            if (!string.IsNullOrEmpty(frm["chkFreeSMS"]))
            {
                isFreeSMS = Convert.ToInt32(frm["chkFreeSMS"]) == 1 ? true : false;
            }
            else
            {
                FreeSMS = Convert.ToInt32(frm["FreeSMS"]);
                if (FreeSMS < 0)
                {
                    return Json(new JsonMessage("Chưa nhập Số SMS miễn phí", GlobalConstantsEdu.ERROR));
                }
            }

            if (!string.IsNullOrEmpty(frm["chkFreeExSMS"]))
            {
                isFreeExSMS = Convert.ToInt32(frm["chkFreeExSMS"]) == 1 ? true : false;
            }
            else
            {
                FreeExSMS = Convert.ToInt32(frm["FreeExSMS"]);
                if (FreeExSMS < 0)
                {
                    return Json(new JsonMessage("Chưa nhập Số SMS ngoại mạng miễn phí", GlobalConstantsEdu.ERROR));
                }
            }

            string ProgramName = frm["ProgramName"].Trim();
            if (string.IsNullOrEmpty(ProgramName))
            {
                return Json(new JsonMessage("Chưa nhập Tên chương trình", GlobalConstantsEdu.ERROR));
            }
            else
            {
                if (ProgramName.Length > 300)
                {
                    return Json(new JsonMessage("Tên chương trình không quá 300 kí tự", GlobalConstantsEdu.ERROR));
                }
            }

            string frdate = frm["FromDate"];
            if (string.IsNullOrEmpty(frdate))
            {
                return Json(new JsonMessage("Chưa nhập ngày bắt đầu áp dụng", GlobalConstantsEdu.ERROR));
            }

            string todate = frm["ToDate"];
            string Note = frm["Note"].Trim();

            if (!string.IsNullOrEmpty(Note))
            {
                if (Note.Length > 500)
                {
                    return Json(new JsonMessage("Ghi chú không quá 500 kí tự", GlobalConstantsEdu.ERROR));
                }
            }

            string lstProvince = frm["LstProvince"];

            if (!string.IsNullOrEmpty(frdate))
            {
                FromDate = ConvertDatetime(frdate);
                if (FromDate == null)
                {
                    return Json(new JsonMessage("Ngày áp dụng không hợp lệ", GlobalConstantsEdu.ERROR));
                }
            }

            if (!string.IsNullOrEmpty(todate))
            {
                ToDate = ConvertDatetime(todate);
            }

            if (FromDate != null)
            {
                if (ToDate < FromDate)
                {
                    return Json(new JsonMessage("Ngày áp dụng phải nhỏ hơn hoặc bằng đến ngày", GlobalConstantsEdu.ERROR));
                }
                if (FromDate < DateTime.Now.Date)
                {
                    return Json(new JsonMessage("Ngày áp dụng phải lớn hơn hoặc bằng ngày hiện tại", GlobalConstantsEdu.ERROR));
                }
            }

            PromotionProgramBO promotionprogram = new PromotionProgramBO();
            TryUpdateModel(promotionprogram);


            promotionprogram.PromotionName = ProgramName;
            promotionprogram.StartDate = FromDate.Value;
            promotionprogram.Note = Note;
            if (ToDate != null)
            {
                promotionprogram.EndDate = ToDate.Value;
            }
            promotionprogram.UpdateTime = DateTime.Now;
            if (isFreeSMS)//Dam bao luong cu dung
            {
                FreeSMS = FreeExSMS;
            }
            promotionprogram.FromOfExternalSMS = FreeExSMS;
            promotionprogram.FromOfInternalSMS = FreeSMS;
            promotionprogram.IsFreeSMS = isFreeSMS;
            promotionprogram.IsFreeExSMS = isFreeExSMS;

            long PromotionID = 0;
            string res = PromotionProgramBusiness.Insert(promotionprogram, lstProvince, ref PromotionID);

            if (res == "")
            {
                // Ghi log 
                //WriteLogInfo("Thêm mới chương trình khuyến mại thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Thêm mới chương trình khuyến mại thành công");
                return Json(new { Type = "success", Message = "Lưu thành công", idPromotion = PromotionID });
            }
            else
            {
                return Json(new JsonMessage(res, GlobalConstantsEdu.ERROR));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(FormCollection frm)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;
            bool isFreeSMS = false;
            bool isFreeExSMS = false;
            int FreeSMS = 0;
            int FreeExSMS = 0;
            if (!string.IsNullOrEmpty(frm["chkEditFreeSMS"]))
            {
                isFreeSMS = Convert.ToInt32(frm["chkEditFreeSMS"]) == 1 ? true : false;
            }
            else
            {
                FreeSMS = Convert.ToInt32(frm["FreeSMS"]);
                if (FreeSMS < 0)
                {
                    return Json(new JsonMessage("Chưa nhập Số SMS miễn phí", GlobalConstantsEdu.ERROR));
                }
            }

            if (!string.IsNullOrEmpty(frm["chkEditFreeExSMS"]))
            {
                isFreeExSMS = Convert.ToInt32(frm["chkEditFreeExSMS"]) == 1 ? true : false;
            }
            else
            {
                FreeExSMS = Convert.ToInt32(frm["FreeExSMS"]);
                if (FreeExSMS < 0)
                {
                    return Json(new JsonMessage("Chưa nhập Số SMS ngoại mạng miễn phí", GlobalConstantsEdu.ERROR));
                }
            }

            string ProgramName = frm["ProgramName"].Trim();
            if (string.IsNullOrEmpty(ProgramName))
            {
                return Json(new JsonMessage("Chưa nhập Tên chương trình", GlobalConstantsEdu.ERROR));
            }
            else
            {
                if (ProgramName.Length > 300)
                {
                    return Json(new JsonMessage("Tên chương trình không quá 300 kí tự", GlobalConstantsEdu.ERROR));
                }
            }

            string frdate = frm["FromDateEdit"];
            if (string.IsNullOrEmpty(frdate))
            {
                return Json(new JsonMessage("Chưa nhập ngày bắt đầu áp dụng", GlobalConstantsEdu.ERROR));
            }

            string todate = frm["ToDateEdit"];
            string Note = frm["Note"].Trim();
            if (!String.IsNullOrEmpty(Note))
            {
                if (Note.Length > 500)
                {
                    return Json(new JsonMessage("Ghi chú không quá 500 kí tự", GlobalConstantsEdu.ERROR));
                }
            }

            string lstProvince = frm["LstProvinceEdit"];

            if (!string.IsNullOrEmpty(frdate))
            {
                FromDate = ConvertDatetime(frdate);
                if (FromDate == null)
                {
                    return Json(new JsonMessage("Ngày áp dụng không hợp lệ", GlobalConstantsEdu.ERROR));
                }
            }

            if (!string.IsNullOrEmpty(todate))
            {
                ToDate = ConvertDatetime(todate);
            }

            if (FromDate != null)
            {
                if (ToDate < FromDate)
                {
                    return Json(new JsonMessage("Ngày áp dụng phải nhỏ hơn hoặc bằng đến ngày", GlobalConstantsEdu.ERROR));
                }
                if (FromDate < DateTime.Now.Date)
                {
                    return Json(new JsonMessage("Ngày áp dụng phải lớn hơn hoặc bằng ngày hiện tại", GlobalConstantsEdu.ERROR));
                }
            }

            PromotionProgramBO promotionprogram = new PromotionProgramBO();
            TryUpdateModel(promotionprogram);


            promotionprogram.PromotionName = ProgramName;
            promotionprogram.StartDate = FromDate.Value;
            promotionprogram.Note = Note;
            if (ToDate != null)
            {
                promotionprogram.EndDate = ToDate.Value;
            }
            promotionprogram.UpdateTime = DateTime.Now;

            if (isFreeSMS)//Dam bao luong cu dung
            {
                FreeSMS = FreeExSMS;
            }

            promotionprogram.FromOfExternalSMS = FreeExSMS;
            promotionprogram.FromOfInternalSMS = FreeSMS;
            promotionprogram.IsFreeSMS = isFreeSMS;
            promotionprogram.IsFreeExSMS = isFreeExSMS;

            string res = PromotionProgramBusiness.Update(promotionprogram, lstProvince);

            if (res == "")
            {
                // Ghi log 
                //WriteLogInfo("Lưu thành công", null, string.Empty, string.Empty, null, string.Empty, string.Empty, string.Empty, "Cập nhật chương trình khuyến mại thành công");
                return Json(new JsonMessage("Lưu thành công", GlobalConstantsEdu.SUCCESS));
            }
            else
            {
                return Json(new JsonMessage(res, GlobalConstantsEdu.ERROR));
            }
        }

        public PartialViewResult GetEdit(int PromotionID)
        {
            ViewData[PromotionProgramConstant.LIST_PROVINCE_APPLY_EDIT] = GetLstProvince(PromotionID);
            PromotionProgramBO res = PromotionProgramBusiness.GetPromotionProgramById(PromotionID);
            ViewData[PromotionProgramConstant.PROMOTION_PROGRAM_TO_UPDATE] = res;
            //lay danh sach nhung truong dang ap dung chuong trinh
            Dictionary<string, object> dicSearchDetail = new Dictionary<string, object>()
            {
                {"PromotionID",PromotionID},
            };

            ViewData[PromotionProgramConstant.PROMOTION_PROGRAM_DETAIL] = PromotionDetailBusiness.GetlistPromotionDetail(dicSearchDetail).Count;
            return PartialView("_Edit");
        }

        [ValidateAntiForgeryToken]
        public JsonResult Delete(int PromotionID)
        {
            string message = string.Empty;

            string res = PromotionProgramBusiness.DeletePromotion(PromotionID);
            if (res == "")
            {
                message = "Xóa thành công";
                // Ghi log 
                //WriteLogInfo("Xóa thành công chương trình khuyến mại", null, "ServicePackageID: " + PromotionID.ToString(), string.Empty, null, string.Empty, string.Empty, string.Empty, "Xóa thành công chương trình khuyến mại");
                return Json(new JsonMessage(message, GlobalConstantsEdu.SUCCESS));
            }
            else
            {
                message = res;
                return Json(new JsonMessage(message, GlobalConstantsEdu.ERROR));
            }
        }

        private DateTime? ConvertDatetime(string date)
        {
            try
            {
                string[] splitDate = date.Split('/');
                string dd = splitDate[0];
                string mm = splitDate[1];
                string yyyy = splitDate[2];
                string dateformat = yyyy + "/" + mm + "/" + dd;

                return Convert.ToDateTime(dateformat);
            }
            catch
            {
                return null;
            }
        }
    }
}