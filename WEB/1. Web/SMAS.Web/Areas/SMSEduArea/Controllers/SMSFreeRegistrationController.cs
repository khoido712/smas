﻿using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using Newtonsoft.Json;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Constants;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class SMSFreeRegistrationController:BaseController
    {
        private const string TemplateName = "RegisterSMSFree.xls";
        private const string ResultFileName = "Danh sach  dang ky nhan SMS mien phi.xls";
        private IRegisterSMSFreeBusiness _registerSmsFreeBusiness;
        private IServicePackageBusiness _servicePackageBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        /// <summary>
        /// Khởi tạo
        /// </summary>
        /// <param name="registerSMSFreeBusiness"></param>
        /// <param name="systemLoaderBusiness"></param>
        /// <param name="pupilProfileLoaderBusiness"></param>
        public SMSFreeRegistrationController(
            IRegisterSMSFreeBusiness registerSMSFreeBusiness,
            IServicePackageBusiness servicePackageBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IPupilProfileBusiness PupilProfileBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness
        )
        {
            this._registerSmsFreeBusiness = registerSMSFreeBusiness;
            this._servicePackageBusiness = servicePackageBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        /// <summary>
        /// Trang mặc định
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string smsFreeCode = WebConfigurationManager.AppSettings["SMSFreeCode"] ?? "SMSFree";
            int schoolId = _globalInfo.SchoolID.Value;
            int provinceId = _globalInfo.ProvinceID.Value;
            if (!_servicePackageBusiness.SchooolHasService(schoolId, provinceId, smsFreeCode))
            {
                return View("NoService");
            }
            int academicYearId = _globalInfo.AcademicYearID.Value;
            var aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            var now = DateTime.Now;
            if ((DateTime.Compare(now, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(now, aca.SecondSemesterEndDate.Value) <= 0))
            {
                if ((DateTime.Compare(now, aca.FirstSemesterEndDate.Value) > 0 &&
                     DateTime.Compare(now, aca.SecondSemesterStartDate.Value) < 0))
                {
                    ViewData[SMSFreeRegistrationConstants.CurrentYear] = false;
                }
                else
                {
                    ViewData[SMSFreeRegistrationConstants.CurrentYear] = true;
                }
            }
            else
            {
                ViewData[SMSFreeRegistrationConstants.CurrentYear] = false;
            }
            // Phải tách hàm
            int gradeId = _globalInfo.AppliedLevel.Value;
            //int academicYearId = _globalInfo.AcademicYearID.Value;
            var educationLevelList = EducationLevelBusiness.All.Where(o => o.Grade == gradeId).Select(x => new Selector()
            {
                Id = x.EducationLevelID,
                Name = x.Resolution
            }).ToList();
            ViewData[SMSFreeRegistrationConstants.EducationLevelList] = educationLevelList;

            educationLevelList.Insert(0, new Selector()
            {
                Id = 0,
                Name = Res.Get("All")//"[Tất cả]"
            });

            List<Selector> classList = new List<Selector>();
            classList.Insert(0, new Selector()
            {
                Id = 0,
                Name = Res.Get("All")//"[Tất cả]"
            });
            ViewData[SMSFreeRegistrationConstants.ClassList] = classList;

            var statusList = new List<Selector>
            {
                new Selector()
                {
                    Id = 0,
                    Name = Res.Get("All")//"[Tất cả]"
                },
                new Selector()
                {
                    Id = 6,//Khong co trong db
                    Name = Res.Get("RegisterSMSFree_CHUA_DANG_KY")//"Chưa đăng ký"
                },
                new Selector()
                {
                    Id = 2,//Trang thai 2 trong db
                    Name = Res.Get("RegisterSMSFree_DA_DUYET")//"Đã duyệt"
                },
                new Selector()
                {
                    Id = 5,//Trang thai 1 va 3 trong db
                    Name = Res.Get("RegisterSMSFree_CHO_DUYET")//"Chờ duyệt"
                },
                new Selector()
                {
                    Id = 4,//Trang thai 4 trong db
                    Name = Res.Get("RegisterSMSFree_TU_CHOI_DUYET")//"Từ chối duyệt"
                }

            };
            ViewData[SMSFreeRegistrationConstants.StatusList] = statusList;
            return View();
        }

        /// <summary>
        /// Lấy dữ liệu và trả về ViewData để đưa vào grid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="paging"></param>
        /// <param name="setViewData"></param>
        private List<RegisterSMSFreeBO> SearchToViewData(SMSFreeSearchViewModel model, bool paging = true, bool setViewData = true)
        {
            int gradeId = _globalInfo.AppliedLevel.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            var educationLevelList = new List<int>();
            if (model.EducationLevel == null || model.EducationLevel == 0)
            {
                educationLevelList = EducationLevelBusiness.All.Where(o=>o.Grade == gradeId).Select(x => x.EducationLevelID).ToList();
            }
            else
            {
                educationLevelList.Add((int)model.EducationLevel);
            }
            int total;
            int page = paging ? model.Page : 1;
            int size = paging ? model.Size : Int16.MaxValue - 1;
            List<RegisterSMSFreeBO> result;
            if (model.Status == null || model.Status == 0)
            {
                List<int> forceInPupilIds = _registerSmsFreeBusiness.GetAllPupilId(schoolId, academicYearId);
                List<PupilProfile> list = PupilProfileBusiness.GetPupilWithPaging(page, size, academicYearId, schoolId, educationLevelList, model.Class, model.PupilCode, model.Fullname, null, forceInPupilIds, null, out total);
                Dictionary<int, SMS_REGISTER_FREE> registerDic = (_registerSmsFreeBusiness.GetByPupil(list.Select(o => o.PupilProfileID).ToList(), academicYearId, schoolId) ?? new List<SMS_REGISTER_FREE>()).ToDictionary(x => x.PUPIL_ID);
                result = Mapping(list, registerDic);
            }
            else if (model.Status == 6)//Chưa đăng ký
            {
                // Lấy tất cả id trong csdl
                // truyền vào service để đọ
                List<int> notInPupilIds = _registerSmsFreeBusiness.GetAllPupilIdRegisterStatus(schoolId, academicYearId);
                List<int> forceInPupilIds = _registerSmsFreeBusiness.GetAllPupilId(schoolId, academicYearId);
                List<PupilProfile> list = PupilProfileBusiness.GetPupilWithPaging(page, size, academicYearId, schoolId, educationLevelList, model.Class, model.PupilCode, model.Fullname, notInPupilIds, forceInPupilIds, null, out total);
                Dictionary<int, SMS_REGISTER_FREE> registerDic = (_registerSmsFreeBusiness.GetByPupil(list.Select(o => o.PupilProfileID).ToList(), academicYearId, schoolId) ?? new List<SMS_REGISTER_FREE>()).ToDictionary(x => x.PUPIL_ID);
                result = Mapping(list, registerDic);
            }
            else if (model.Status == 5)//Chờ duyệt
            {
                List<int> statusRegisterList = new List<int>() { 1, 3 };
                List<int> searchInPupilIds = _registerSmsFreeBusiness.GetAllPupilId(schoolId, academicYearId, statusRegisterList);
                List<PupilProfile> list = PupilProfileBusiness.GetPupilWithPaging(page, size, academicYearId, schoolId, educationLevelList, model.Class, model.PupilCode, model.Fullname, null, searchInPupilIds, searchInPupilIds, out total);
                Dictionary<int, SMS_REGISTER_FREE> registerDic = (_registerSmsFreeBusiness.GetByPupil(list.Select(o => o.PupilProfileID).ToList(), academicYearId, schoolId) ?? new List<SMS_REGISTER_FREE>()).ToDictionary(x => x.PUPIL_ID);
                result = Mapping(list, registerDic);
            }
            else
            {
                List<int> statusRegisterList = new List<int>() { (int)model.Status };
                List<int> searchInPupilIds = _registerSmsFreeBusiness.GetAllPupilId(schoolId, academicYearId, statusRegisterList);
                List<PupilProfile> list = PupilProfileBusiness.GetPupilWithPaging(page, size, academicYearId, schoolId, educationLevelList, model.Class, model.PupilCode, model.Fullname, null, searchInPupilIds, searchInPupilIds, out total);
                Dictionary<int, SMS_REGISTER_FREE> registerDic = (_registerSmsFreeBusiness.GetByPupil(list.Select(o => o.PupilProfileID).ToList(), academicYearId, schoolId) ?? new List<SMS_REGISTER_FREE>()).ToDictionary(x => x.PUPIL_ID);
                result = Mapping(list, registerDic);
            }
            if (setViewData)
            {
                ViewData[SMSFreeRegistrationConstants.ListData] = result;
                ViewData[SMSFreeRegistrationConstants.TotalData] = total;
                ViewData[SMSFreeRegistrationConstants.PageData] = model.Page;
                ViewData[SMSFreeRegistrationConstants.SizeData] = model.Size;
            }
            return result;
        }

        /// <summary>
        /// Mapping dữ liệu giứ smas và sms
        /// </summary>
        /// <param name="list"></param>
        /// <param name="registerDic"></param>
        /// <returns></returns>
        public List<RegisterSMSFreeBO> Mapping(List<PupilProfile> list, Dictionary<int, SMS_REGISTER_FREE> registerDic)
        {
            var result = new List<RegisterSMSFreeBO>();
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<ClassProfile> classList = ClassProfileBusiness.GetListClassBySchool(schoolId, academicYearId);
            foreach (PupilProfile pupil in list)
            {
                string className = null;
                int? educationLevelId = null;
                var findClass = classList.FirstOrDefault(o => o.ClassProfileID == pupil.CurrentClassID);
                if (findClass != null)
                {
                    className = findClass.DisplayName;               
                }
                var findEducationLevel = classList.FirstOrDefault(o => o.ClassProfileID == pupil.CurrentClassID);
                if (findEducationLevel != null)
                {
                    educationLevelId = findEducationLevel.EducationLevelID;
                }
                if (registerDic.ContainsKey(pupil.PupilProfileID))
                {
                    result.Add(new RegisterSMSFreeBO()
                    {
                        SchoolID = registerDic[pupil.PupilProfileID].SCHOOL_ID,
                        YearID = registerDic[pupil.PupilProfileID].ACADEMIC_YEAR_ID,
                        PupilID = pupil.PupilProfileID,
                        ClassID = pupil.CurrentClassID,
                        StatusRegister = registerDic[pupil.PupilProfileID].STATUS_REGISTER,
                        Fullname = pupil.FullName,
                        Gender = pupil.Genre == 1 ? true : false,
                        PupilCode = pupil.PupilCode,
                        Last2DigitNumberSchool = registerDic[pupil.PupilProfileID].PARTITION_ID,
                        ClassName = className,
                        Birthday = pupil.BirthDate,
                        IsActive = registerDic[pupil.PupilProfileID].IS_ACTIVE,
                        EducationLevelID = educationLevelId,
                        Name = pupil.FullName.Split(' ').LastOrDefault(),
                        RegisterSMSFreeID = registerDic[pupil.PupilProfileID].REGISTER_SMS_FREE_ID,
                        CreateTime = registerDic[pupil.PupilProfileID].CREATE_TIME,
                        UpdateTime = registerDic[pupil.PupilProfileID].UPDATE_TIME
                    });
                }
                else
                {
                    result.Add(new RegisterSMSFreeBO()
                    {
                        SchoolID = schoolId,
                        YearID = academicYearId,
                        PupilID = pupil.PupilProfileID,
                        ClassID = pupil.CurrentClassID,
                        StatusRegister = null,
                        Fullname = pupil.FullName,
                        Gender = pupil.Genre == 1 ? true : false,
                        PupilCode = pupil.PupilCode,
                        Last2DigitNumberSchool = schoolId % 20,
                        Year = DateTime.Now.Year,
                        ClassName = className,
                        Birthday = pupil.BirthDate,
                        IsActive = true,
                        EducationLevelID = educationLevelId,
                        Name = pupil.FullName.Split(' ').LastOrDefault()
                    });
                }
            }
            return result;
        }

        /// <summary>
        /// Lấy danh sách lớp khi khối thay đổi
        /// </summary>
        /// <param name="educationLevelId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetClassList(int educationLevelId)
        {
            int academicYearId = _globalInfo.AcademicYearID.Value;
            var classList = ClassProfileBusiness.GetListClass(academicYearId, educationLevelId).ToList().Select(x => new Selector()
            {
                Id = x.ClassProfileID,
                Name = x.DisplayName
            }).ToList();
            return Json(new { status = 200, data = classList }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thực hiện tìm kiếm học sinh với các điều kiện từ client
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search(SMSFreeSearchViewModel model)
        {
            SearchToViewData(model);
            return PartialView("_List");
        }

        /// <summary>
        /// Xuất excel danh sách tất cả học sinh chờ đăng ký
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public FileResult Export(SMSFreeSearchViewModel model)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            string templatePath = Path.Combine(SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER, TemplateName);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            List<RegisterSMSFreeBO> list = SearchToViewData(model, false, false);
            List<Dictionary<string, object>> exportList = list.Select((x, index) => new Dictionary<string, object>()
            {
                {"A", index + 1},
                {"B", x.Fullname},
                {"C", x.PupilCode},
                {"D", x.Birthday == null? "" : ((DateTime)x.Birthday).ToString("dd/MM/yyyy")},
                {"E", x.Gender == true ? "Nam" : "Nữ"},
                {"F", x.ClassName},
                {"G", x.StatusRegister == 1 || x.StatusRegister == 3 ? "Chờ duyệt" : (x.StatusRegister == 2 ? "Đã duyệt" : (x.StatusRegister == 4 ? "Từ chối duyệt" : "Chưa đăng ký"))}
            }).ToList();
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);
            var otherValue = new Dictionary<string, object>()
            {
                {
                    "A3", _globalInfo.SchoolName.ToUpper()
                },
                {
                    "A2", school.SupervisingDept.SupervisingDeptName.ToUpper()
                },
                {
                    "A6", string.Format("Năm học {0} - {1}", this.AcademicYear.Year, this.AcademicYear.Year + 1)
                }
            };
            oBook.GetSheet(1).Binding(9, exportList).Binding(otherValue);
            IVTRange range = oBook.GetSheet(1).GetRange(9, 1, 9 + exportList.Count - 1, 7);
            range.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            //thong tin ngay thang nam
            int rowDate = 9 + exportList.Count + 1;
            IVTRange rangeDate = oBook.GetSheet(1).GetRange(rowDate, 4, rowDate, 7);
            rangeDate.Merge();
            String strDate = String.Format("{0}, ngày {1} tháng {2} năm {3}", school.Province.ProvinceName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            oBook.GetSheet(1).SetCellValue(rowDate, 4, strDate);

            //Thong tin hieu truong
            IVTRange rangeHead = oBook.GetSheet(1).GetRange(rowDate + 1, 4, rowDate + 1, 7);
            rangeHead.Merge();
            oBook.GetSheet(1).SetCellValue(rowDate + 1, 4, "HIỆU TRƯỞNG");

            //Thong tin hieu truong
            int rowHeadMaster = rowDate + 1 + 5;
            IVTRange rangeHeadMaster = oBook.GetSheet(1).GetRange(rowHeadMaster, 4, rowHeadMaster, 7);
            rangeHeadMaster.Merge();
            oBook.GetSheet(1).SetCellValue(rowHeadMaster, 4, String.IsNullOrEmpty(school.HeadMasterName) ? "" : school.HeadMasterName.ToUpper());
            rangeHeadMaster.SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream")
            {
                FileDownloadName = ResultFileName
            };
            return result;
        }

        /// <summary>
        /// Thực hiện đăng ký
        /// </summary>
        /// <param name="registerListJson"></param>
        /// <param name="modelJson"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(string registerListJson, string modelJson)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<SMS_REGISTER_FREE> registerList = null;
            if (!string.IsNullOrWhiteSpace(registerListJson))
            {
                List<RegisterSMSFreeBO> lstReg = JsonConvert.DeserializeObject<List<RegisterSMSFreeBO>>(registerListJson);
                registerList = new List<SMS_REGISTER_FREE>();
                for (int i = 0; i < lstReg.Count; i++)
                {
                    RegisterSMSFreeBO objreg = lstReg[i];
                    SMS_REGISTER_FREE obj = new SMS_REGISTER_FREE
                    {
                        ACADEMIC_YEAR_ID = _globalInfo.AcademicYearID.Value,
                        CLASS_ID = objreg.ClassID,
                        CREATE_TIME = DateTime.Now,
                        IS_ACTIVE = true,
                        PARTITION_ID = SMAS.Business.Common.UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value),
                        PUPIL_ID = objreg.PupilID,
                        SCHOOL_ID = _globalInfo.SchoolID.Value,

                    };
                    registerList.Add(obj);
                }
            }
            SMSFreeSearchViewModel model = null;
            if (!string.IsNullOrWhiteSpace(modelJson))
            {
                model = JsonConvert.DeserializeObject<SMSFreeSearchViewModel>(modelJson);
            }
            if (registerList != null && registerList.Any())
            {
                List<int> pupilIds = _registerSmsFreeBusiness.GetPupilIdByPupil(registerList.Select(o => o.PUPIL_ID).ToList(), academicYearId, schoolId) ?? new List<int>();

                foreach (SMS_REGISTER_FREE register in registerList)
                {
                    if (!pupilIds.Contains(register.PUPIL_ID))
                    {
                        // Thêm mới
                        register.CREATE_TIME = DateTime.Now;
                        register.STATUS_REGISTER = 1;
                        _registerSmsFreeBusiness.Insert(register);
                    }
                    else
                    {
                        _registerSmsFreeBusiness.UpdateStatus(register.PUPIL_ID, register.ACADEMIC_YEAR_ID, schoolId, register.STATUS_REGISTER == 4 ? 3 : 1);
                    }
                }
                ViewData[SMSFreeRegistrationConstants.Message] = string.Format(Res.Get("RegisterSMSFree_DANG_KY_TC"), registerList.Count);
            }
            _registerSmsFreeBusiness.Save();

            SearchToViewData(model);
            return PartialView("_List");
        }

        /// <summary>
        /// Thực hiện hủy đăng ký
        /// </summary>
        /// <param name="cancelListJson"></param>
        /// <param name="modelJson"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancel(string cancelListJson, string modelJson)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            List<RegisterSMSFreeBO> cancelList = null;
            if (!string.IsNullOrWhiteSpace(cancelListJson))
            {
                cancelList = JsonConvert.DeserializeObject<List<RegisterSMSFreeBO>>(cancelListJson);
            }
            SMSFreeSearchViewModel model = null;
            if (!string.IsNullOrWhiteSpace(modelJson))
            {
                model = JsonConvert.DeserializeObject<SMSFreeSearchViewModel>(modelJson);
            }
            if (cancelList != null && cancelList.Any())
            {
                // goi ham update voi ca danh sach hoc sinh (Da viet) se toi uu hon, nhung ko quan tam
                foreach (RegisterSMSFreeBO cancel in cancelList)
                {
                    _registerSmsFreeBusiness.Delete(cancel.RegisterSMSFreeID);
                }
                ViewData[SMSFreeRegistrationConstants.Message] = string.Format(Res.Get("RegisterSMSFree_HUY_TC"), cancelList.Count);
            }
            _registerSmsFreeBusiness.Save();

            SearchToViewData(model);
            return PartialView("_List");
        }

        /// <summary>
        /// Hàm thực hiện đăng ký cho cả khối
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterAll(SMSFreeSearchViewModel model)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            List<RegisterSMSFreeBO> list = SearchToViewData(model, false, false);
            if (list.Any())
            {
                int academicYearId = _globalInfo.AcademicYearID.Value;
                List<int> pupilIds = _registerSmsFreeBusiness.GetPupilIdByPupil(list.Select(o => o.PupilID).ToList(), academicYearId, schoolId) ?? new List<int>();
                int i = 0;
                foreach (RegisterSMSFreeBO register in list)
                {
                    if (!pupilIds.Contains(register.PupilID))
                    {
                        SMS_REGISTER_FREE obj = new SMS_REGISTER_FREE();
                        obj.CREATE_TIME = DateTime.Now;
                        obj.STATUS_REGISTER = 1;
                        obj.ACADEMIC_YEAR_ID = register.YearID;
                        obj.CLASS_ID = register.ClassID;
                        obj.IS_ACTIVE = register.IsActive.GetValueOrDefault();
                        obj.PARTITION_ID = register.Last2DigitNumberSchool;
                        obj.PUPIL_ID = register.PupilID;
                        obj.SCHOOL_ID = register.SchoolID;
                        obj.REGISTER_SMS_FREE_ID = register.RegisterSMSFreeID;
                        _registerSmsFreeBusiness.Insert(obj);
                        i++;
                    }
                    else
                    {
                        // không đăng ký cho từ chối duyệt và đã duyệt
                        if (register.StatusRegister != 4 && register.StatusRegister != 2 && register.StatusRegister != 1 && register.StatusRegister != 3)
                        {
                            _registerSmsFreeBusiness.UpdateStatus(register.PupilID, register.YearID, schoolId, register.StatusRegister == 4 ? 3 : 1);
                            i++;
                        }
                    }
                }
                ViewData[SMSFreeRegistrationConstants.Message] = string.Format(Res.Get("RegisterSMSFree_DANG_KY_TC"), i);
                _registerSmsFreeBusiness.Save();
            }

            SearchToViewData(model);
            return PartialView("_List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelAll(SMSFreeSearchViewModel model)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            List<RegisterSMSFreeBO> list = SearchToViewData(model, false, false);
            if (list.Any())
            {
                //int academicYearId = _globalInfo.AcademicYearID.Value;
                //List<int> pupilIds = _registerSmsFreeBusiness.GetPupilIdByPupilAndStatus(list.Select(o => o.PupilID).ToList(), academicYearId, schoolId, null, null) ?? new List<int>();
                int i = 0;
                foreach (RegisterSMSFreeBO cancel in list)
                {
                    // Không hủy đăng ký cho các bạn đã được duyệt hoặc đã hủy
                    if (cancel.StatusRegister == 1 || cancel.StatusRegister == 3)
                    {
                        _registerSmsFreeBusiness.Delete(cancel.RegisterSMSFreeID);
                        i++;
                    }
                }
                _registerSmsFreeBusiness.Save();
                ViewData[SMSFreeRegistrationConstants.Message] = string.Format(Res.Get("RegisterSMSFree_HUY_TC"), i);
            }

            SearchToViewData(model);
            return PartialView("_List");
        }

        /// <summary>
        /// Mapping dữ liệu giứ smas và sms
        /// </summary>
        /// <param name="list"></param>
        /// <param name="registerDic"></param>
        /// <returns></returns>
        public List<SMS_REGISTER_FREE> Mapping(List<PupilProfileBO> list, Dictionary<int, SMS_REGISTER_FREE> registerDic)
        {
            var result = new List<SMS_REGISTER_FREE>();
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<ClassProfile> classList = ClassProfileBusiness.GetListClassBySchool(schoolId, academicYearId);
            foreach (PupilProfileBO pupil in list)
            {
                string className = null;
                int? educationLevelId = null;
                var findClass = classList.FirstOrDefault(o => o.ClassProfileID == pupil.PupilOfClassID);
                if (findClass != null)
                {
                    className = findClass.DisplayName;
                }
                var findEducationLevel = classList.FirstOrDefault(o => o.ClassProfileID == pupil.PupilOfClassID);
                if (findEducationLevel != null)
                {
                    educationLevelId = findEducationLevel.EducationLevelID;
                }
                if (registerDic.ContainsKey(pupil.PupilProfileID))
                {
                    result.Add(new SMS_REGISTER_FREE()
                    {
                        SCHOOL_ID = registerDic[pupil.PupilProfileID].SCHOOL_ID,
                        ACADEMIC_YEAR_ID = registerDic[pupil.PupilProfileID].ACADEMIC_YEAR_ID,
                        PUPIL_ID = pupil.PupilProfileID,
                        CLASS_ID = pupil.PupilOfClassID,
                        STATUS_REGISTER = registerDic[pupil.PupilProfileID].STATUS_REGISTER,
                        PARTITION_ID = registerDic[pupil.PupilProfileID].PARTITION_ID,
                        IS_ACTIVE = registerDic[pupil.PupilProfileID].IS_ACTIVE,
                        REGISTER_SMS_FREE_ID = registerDic[pupil.PupilProfileID].REGISTER_SMS_FREE_ID,
                        CREATE_TIME = registerDic[pupil.PupilProfileID].CREATE_TIME,
                        UPDATE_TIME = registerDic[pupil.PupilProfileID].UPDATE_TIME
                    });
                }
                else
                {
                    result.Add(new SMS_REGISTER_FREE()
                    {
                        SCHOOL_ID = schoolId,
                        ACADEMIC_YEAR_ID = academicYearId,
                        PUPIL_ID = pupil.PupilProfileID,
                        CLASS_ID = pupil.PupilOfClassID,
                        STATUS_REGISTER = null,
                        PARTITION_ID = schoolId % 20,
                        IS_ACTIVE = true,
                    });
                }
            }
            return result;
        }
    }

    public class Selector
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}