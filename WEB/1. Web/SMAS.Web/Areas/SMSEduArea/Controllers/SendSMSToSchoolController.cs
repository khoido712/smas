﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common.Extension;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class SendSMSToSchoolController:BaseController
    {
        #region declare/contructor
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IEWalletBusiness EWalletBusiness;
        private readonly IPromotionBusiness PromotionBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly IPromotionBusiness PromotionProgramBusiness;
        private readonly IReceivedRecordBusiness ReceivedRecordBusiness;
        public SendSMSToSchoolController(ISupervisingDeptBusiness SupervisingDeptBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IEWalletBusiness EWalletBusiness,
            IPromotionBusiness PromotionBusiness,
            ISchoolConfigBusiness SchoolConfigBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISMSHistoryBusiness SMSHistoryBusiness,
            IPromotionBusiness PromotionProgramBusiness,
            IReceivedRecordBusiness ReceivedRecordBusiness)
        {
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.PromotionBusiness = PromotionBusiness;
            this.SchoolConfigBusiness = SchoolConfigBusiness;
            this.EWalletBusiness = EWalletBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.PromotionProgramBusiness = PromotionProgramBusiness;
            this.ReceivedRecordBusiness = ReceivedRecordBusiness;
        }
        #endregion

        #region index page
        /// <summary>
        /// index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SupervisingDept supervisingDeptBO = this.GetSupervisingDeptParent();
            // phan biet nguoi dung cap so và cap phong (so: 3|4 nguoc lai la phong, cap phong khong hien thi cap 3)
            ViewData[SendSMSToSchoolConstants.SUPER_USER_TYPE] = supervisingDeptBO.HierachyLevel;

            var ewallet = EWalletBusiness.GetEWalletIncludePromotion(supervisingDeptBO.SupervisingDeptID, false, true);
            if (ewallet != null)
            {
                ViewData[SendSMSToSchoolConstants.BALANCE_OF_EWALLET] = ewallet.Balance;
                ViewData[SendSMSToSchoolConstants.NUMBER_PROMOTION_SMS] = ewallet.InternalSMS;
            }
            else
            {
                ViewData[SendSMSToSchoolConstants.BALANCE_OF_EWALLET] = 0;
                ViewData[SendSMSToSchoolConstants.NUMBER_PROMOTION_SMS] = 0;
            }

            //if (supervisingDeptBObj.SMSActiveType == GlobalConstants.COMMON_SCHOOL_SMS_ACTIVE_LIMIT)
            //{
            //    ViewData[SendSMSToSchoolConstants.NUM_SMS_ALLOW_SEND] = SendSMSToSchoolConstants.SMS_LIMIT_SUPER_USER;
            //    ViewData[SendSMSToSchoolConstants.SENT_RECORD] = sentRecordBusiness.GetSentRecordOfUnit(supervisingDeptBObj.SupervisingDeptID);
            //}
            //ViewData[SendSMSToSchoolConstants.IS_LIMIT_SMS] = supervisingDeptBObj.SMSActiveType;
            LoadContentPromotion(supervisingDeptBO.ProvinceID.Value, supervisingDeptBO.SupervisingDeptID);
            return View();
        }
        #endregion

        #region load detail view
        /// <summary>
        /// load contentDetail level
        /// </summary>
        /// <author>HaiVT 23/09/2013</author>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDetailLevelToSchool(int educationGrade)
        {
            int numOfSchool = 0;
            List<SchoolBO> schoolList = GetSchoolByRegion(educationGrade, ref numOfSchool, 1);
            LoadContentPromotion(this.GetSupervisingDeptParent().ProvinceID.Value, this.GetSupervisingDeptParent().SupervisingDeptID);
            return Json(new { numOfReceiverTotal = schoolList.Count });
        }
        #endregion

        #region load content detail view
        /// <summary>
        /// load content detail view
        /// </summary>
        /// <author>HaiVT 25/03/2014</author>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadContentDetailView(int educationGrade)
        {
            int totalReceiver = 0;
            List<SchoolBO> schoolBOList = GetSchoolByRegion(educationGrade, ref totalReceiver, 1);
            if (schoolBOList != null)
            {
                ViewData[SendSMSToSchoolConstants.LIST_SCHOOL] = schoolBOList.ToList();
            }
            else
            {
                ViewData[SendSMSToSchoolConstants.LIST_SCHOOL] = new List<SchoolBO>();
            }
            LoadContentPromotion(this.GetSupervisingDeptParent().ProvinceID.Value, this.GetSupervisingDeptParent().SupervisingDeptID);
            return PartialView("_ContentDetail");
        }
        #endregion

        #region load to scroll
        /// <summary>
        /// load content detail when scroll
        /// </summary>
        /// <author>HaiVT 25/03/2014</author>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadContentDetailScoll(int educationGrade, string searchContent, int currentPage)
        {
            int totalReceiver = 0;
            List<SchoolBO> schoolBOList = GetSchoolByRegion(educationGrade, ref totalReceiver, currentPage, searchContent);
            return Json(new { lstReceiver = schoolBOList });
        }
        #endregion

        #region search receiver
        /// <summary>
        /// search receiver
        /// </summary>
        /// <author>HaiVT 25/03/2014</author>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SearchReceiver(int educationGrade, string searchContent, int currentPage)
        {
            int totalReceiver = 0;
            List<SchoolBO> schoolBOList = GetSchoolByRegion(educationGrade, ref totalReceiver, currentPage, searchContent);
            return Json(new { lstReceiver = schoolBOList, totalReceiver = totalReceiver });
        }
        #endregion

        #region sendSMS
        /// <summary>
        /// gui tin nhan
        /// </summary>
        /// <param name="objRequestModel">object truyen vao</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult SendSMS(SendSMSToschoolViewModel objRequestModel)
        {
            string brandname = CommonMethods.GetBrandName().Trim().ToUpper();
            string prefix = CommonMethods.GetPrefix(null).Trim().ToUpper();
            string content = objRequestModel.Content.Trim().ToUpper();
            if (content.Equals(brandname) || content.Equals(prefix))
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho trường", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage { Type = GlobalConstantsEdu.ERROR, Message = Res.Get("Send_SMS_Group_Empty_SMS_Error") });
            }

            int superVisingDeptID = this.GetSupervisingDeptParent().SupervisingDeptID;

            #region get list receiver
            List<int> lstReceiver = objRequestModel.ArrIsCheckReceiverID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

            #endregion

            objRequestModel.Content = objRequestModel.Content.Trim();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["lstReceiver"] = lstReceiver;
            dic["isToSchool"] = true;
            dic["isSignMsg"] = objRequestModel.AllowSignMessage;
            dic["content"] = objRequestModel.Content;
            dic["shortContent"] = objRequestModel.Content;
            dic["superUserType"] = this.GetSupervisingDept().HierachyLevel;
            dic["EducationLevel"] = objRequestModel.EducationGrade;
            Employee objEmployee = EmployeeBusiness.Find(_globalInfo.EmployeeID);
            string EmployeeName = objEmployee != null ? objEmployee.FullName : string.Empty;
            dic["superUserName"] = !string.IsNullOrEmpty(EmployeeName) ? EmployeeName : this.GetSupervisingDeptParent().SupervisingDeptName;
            //neu la phong ban don vi thi lay smsactive cua cha la cap phong/so gan nhat
            //Check session
            dic["unitID"] = superVisingDeptID;
            dic[GlobalConstantsEdu.COMMON_SUPERVISINGDEPT] = this.GetSupervisingDeptParent();

            //Lay chuong trinh KM
            PromotionProgramBO promotion = LoadContentPromotion(this.GetSupervisingDept().ProvinceID.Value, superVisingDeptID);
            dic["Promotion"] = promotion;
            //smsCommunicationBusiness.
            //Start thread for send data
            SMSEduResultBO result = SMSHistoryBusiness.SendSMSFromSuperUser(dic);
            if (result.isError)
            {
                //WriteLogInfo("Gửi tin nhắn không thành công cho trường", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new { Type = GlobalConstantsEdu.ERROR, Message = Res.Get(result.ErrorMsg) });
            }
            //WriteLogInfo("Gửi tin nhắn thành công cho trường", null, string.Empty, string.Empty, null, string.Empty);
            //int numOfSent = sentRecordBusiness.GetSentRecordOfUnit(superVisingDeptID);//lay lai so tin nhan da gui
            int counter = result.NumSendSuccess;
            string message = string.Format(Res.Get("lbl_Message_To_School_Success"), counter);
            return Json(new { Type = GlobalConstantsEdu.SUCCESS, Message = message, BalanceOfEWallet = result.Balance, NumOfPromotionInternalSMS = result.NumOfPromotionInternalSMS });
        }
        #endregion

        #region private method
        /// <summary>
        /// load school by region
        /// </summary>
        /// <author>HaiVT 25/03/2014</author>
        /// <returns></returns>
        private List<SchoolBO> GetSchoolByRegion(int educationGrade, ref int totalReceiver, int currentPage, string searchContent = "")
        {
            List<SchoolBO> list = SchoolProfileBusiness.GetSchoolByRegionNotPaging(educationGrade, this.GetSupervisingDeptParent().SupervisingDeptID, searchContent);

            var objPromotion = PromotionProgramBusiness.GetPromotionProgramByProvinceId(this.GetSupervisingDeptParent().ProvinceID.GetValueOrDefault(), this.GetSupervisingDeptParent().SupervisingDeptID);
            List<int> lstSchoolID = list.Select(p => p.SchoolID).Distinct().ToList();
            Dictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"Type",GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID},
                    {"lstSchoolID",lstSchoolID},
                    {"dateTimeNow",DateTime.Now}
                };
            List<SMS_RECEIVED_RECORD> lstReceivedRecord = ReceivedRecordBusiness.GetListReceiRecord(dicSearch);
            SMS_RECEIVED_RECORD objReceivedRecord;
            int fromOfInternalSMS = objPromotion != null ? objPromotion.FromOfInternalSMS : 0;
            int fromOfExternalSMS = objPromotion != null ? objPromotion.FromOfExternalSMS : 0;
            foreach (var objSchoolBO in list)
            {
                objReceivedRecord = lstReceivedRecord.FirstOrDefault(e => e.SCHOOL_ID == objSchoolBO.SchoolID);
                objSchoolBO.isFreeSMS = objPromotion != null ? objPromotion.IsFreeSMS : null;
                objSchoolBO.isFreeExSMS = objPromotion != null ? objPromotion.IsFreeExSMS : null;
                if (objReceivedRecord != null)
                {
                    int totalInternalSMS = objReceivedRecord.TOTAL_INTERNAL_SMS;
                    int totalExternalSMS = objReceivedRecord.TOTAL_EXTERNAL_SMS;
                    objSchoolBO.TotalInternalSMS = totalInternalSMS;
                    objSchoolBO.TotalExternalSMS = totalExternalSMS;
                    objSchoolBO.isNumberVT = false;
                    if (objSchoolBO.HeadMasterPhone.CheckMobileNumberVT())
                    {
                        objSchoolBO.TotalSMSPromotion = (totalInternalSMS + totalExternalSMS >= fromOfInternalSMS) ? 0 : fromOfInternalSMS - totalExternalSMS - totalInternalSMS;
                        objSchoolBO.isNumberVT = true;
                    }
                    else if ((totalInternalSMS + totalExternalSMS >= fromOfInternalSMS))// Thue bao ngoai mang
                    {
                        objSchoolBO.TotalSMSPromotion = 0;
                    }
                    else if (totalExternalSMS >= fromOfExternalSMS)//if K>=N
                    {
                        objSchoolBO.TotalSMSPromotion = 0;
                    }
                    else //Min(M- V-K, N- K)
                    {
                        objSchoolBO.TotalSMSPromotion = Math.Min(fromOfInternalSMS - totalExternalSMS - totalInternalSMS, fromOfExternalSMS - totalExternalSMS);
                    }
                }
                else
                {
                    objSchoolBO.TotalExternalSMS = 0;
                    objSchoolBO.TotalInternalSMS = 0;
                    if (objSchoolBO.HeadMasterPhone.CheckMobileNumberVT())
                    {
                        objSchoolBO.TotalSMSPromotion = fromOfInternalSMS;
                        objSchoolBO.isNumberVT = true;
                    }
                    else
                    {
                        objSchoolBO.TotalSMSPromotion = fromOfExternalSMS;
                        objSchoolBO.isNumberVT = false;
                    }
                }
            }
            return list;
        }

        private SupervisingDept GetSupervisingDept()
        {
            return SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
        }

        private SupervisingDept GetSupervisingDeptParent()
        {
            SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

            if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                return sup;
            }
            else if (sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                return SupervisingDeptBusiness.Find(sup.ParentID);
            }

            return null;
        }
        #endregion

        private PromotionProgramBO LoadContentPromotion(int provinceId, int unitId)
        {
            //Lay chuong trinh khuyen mai
            PromotionProgramBO objPromotion = PromotionBusiness.GetPromotionProgramByProvinceId(provinceId, unitId);
            if (objPromotion != null)
            {
                ViewData[SendSMSToSchoolConstants.CONTENT_PROMOTION] = !String.IsNullOrEmpty(objPromotion.Note) ? objPromotion.Note : objPromotion.PromotionName;
            }
            return objPromotion;
        }
    }
}