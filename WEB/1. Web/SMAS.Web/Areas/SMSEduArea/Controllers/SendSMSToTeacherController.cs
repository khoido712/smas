﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.IBusiness;
using SMAS.Web.Constants;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common.Extension;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class SendSMSToTeacherController:BaseController
    {
        #region contructor
        private const string SMAS_CONTROL_NAME = "#smsedu/sendsmstoteacher#";

        private readonly IContactGroupBusiness contactGroupBusiness;
        private readonly ISMSHistoryBusiness smsHistoryBusiness;
        private readonly IMTBusiness mTBusiness;
        private readonly ISMSTypeBusiness smsTypeBusiness;
        private readonly ITeacherContactBusiness teacherContactBusiness;
        private readonly IEWalletBusiness EWalletBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPromotionBusiness PromotionProgramBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        private const int ALL_SCHOOL_CONTACT_GROUP_ID = GlobalConstantsEdu.COMMON_CONTACT_GROUP_ALL;
        private const string SMS_TYPE_CODE = "TNGV";
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="employeeBusiness"></param>
        /// <param name="contactGroupBusiness"></param>
        /// <param name="smsCommunicationBusiness"></param>
        /// <param name="smsHistoryBusiness"></param>
        /// <param name="mtBusiness"></param>
        /// <param name="employeeContactBusiness"></param>
        /// <param name="smsTypeBusiness"></param>
        /// <param name="academicYearBusiness"></param>
        public SendSMSToTeacherController(
            IContactGroupBusiness contactGroupBusiness,
            ISMSHistoryBusiness smsHistoryBusiness,
            IMTBusiness mtBusiness,
            ITeacherContactBusiness employeeContactBusiness,
            ISMSTypeBusiness smsTypeBusiness,
            IEWalletBusiness ewalletBusiness,
            ISchoolConfigBusiness SchoolConfigBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
             IPromotionBusiness PromotionProgramBusiness,
            IEmployeeBusiness EmployeeBusiness
            )
        {
            this.contactGroupBusiness = contactGroupBusiness;
            this.smsHistoryBusiness = smsHistoryBusiness;
            this.mTBusiness = mtBusiness;
            this.teacherContactBusiness = employeeContactBusiness;
            this.smsTypeBusiness = smsTypeBusiness;
            this.EWalletBusiness = ewalletBusiness;
            this.SchoolConfigBusiness = SchoolConfigBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.PromotionProgramBusiness = PromotionProgramBusiness;
        }
        #endregion

        #region load index view
        /// <summary>
        /// Index View
        /// </summary>        
        /// <modify date="2013/11/12" author="HaiVT">load toan bo nhom lien lac khong quan tam la trang thai nhom co bi khoa hay khong</modify>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (!SchoolProfileBusiness.IsSMSTeacherActive(_globalInfo.SchoolID.Value))
            {
                ViewData[SendSMSToTeacherConstants.SCHOOL_NOT_ACTIVE_SMSTEACHER] = Res.Get("Message_School_Not_Active_SMSTeacher");
                return View();
            }
            else
            {
                int schoolID = _globalInfo.SchoolID.Value;
                List<SMS_TEACHER_CONTACT_GROUP> lst = new List<SMS_TEACHER_CONTACT_GROUP>();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["schoolID"] = schoolID;
                //Doi voi BGH va QT se load toan bo nhom
                if (_globalInfo.IsAdmin)
                {
                    contactGroupBusiness.CheckDefaultGroup(schoolID, _globalInfo.UserAccount.GUID.Value);
                    lst = contactGroupBusiness.Search(dic).ToList();
                }
                else if (_globalInfo.IsRolePrincipal)
                {
                    contactGroupBusiness.CheckDefaultGroup(schoolID, _globalInfo.UserAccount.GUID.Value);
                    dic["employeeID"] = _globalInfo.EmployeeID.GetValueOrDefault();
                    dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
                    lst = contactGroupBusiness.Search(dic).ToList();

                    //Lay luon cac nhom duoc phep nhan tin
                    List<SMS_TEACHER_CONTACT_GROUP> lstCgOfSchool = contactGroupBusiness.All.Where(o => o.SCHOOL_ID == schoolID).ToList();
                    List<SMS_TEACHER_CONTACT_GROUP> lstCg = lstCgOfSchool.Where(o => (o.IS_ALLOW_SCHOOL_BOARD_SEND == true) || GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(_globalInfo.EmployeeID.GetValueOrDefault())).ToList();

                    lst = lst.Union(lstCg).ToList();
                }
                else
                {
                    dic["guid"] = _globalInfo.UserAccount.GUID.Value;
                    dic["employeeID"] = _globalInfo.EmployeeID.GetValueOrDefault();
                    dic["isPrincipal"] = _globalInfo.IsRolePrincipal;// BGH (quan ly truong)
                    var query = contactGroupBusiness.Search(dic);
                    lst = query.ToList();

                    List<SMS_TEACHER_CONTACT_GROUP> lstCgOfSchool = contactGroupBusiness.All.Where(o => o.SCHOOL_ID == schoolID).ToList();
                    List<SMS_TEACHER_CONTACT_GROUP> lstCg = lstCgOfSchool.Where(o => GetIDsFromString(o.ASSIGN_TEACHERS_ID).Contains(_globalInfo.EmployeeID.GetValueOrDefault())).ToList();

                    lst = lst.Union(lstCg).OrderByDescending(a => a.IS_DEFAULT).ThenBy(a => a.CONTACT_GROUP_NAME).ToList();
                }
                //Neu khong co nhom nao thi hien thi thong tin
                if ((lst == null) || (lst != null && lst.Count == 0))
                {
                    ViewData[SendSMSToTeacherConstants.NO_GROUP] = 1;
                }
                ViewData[SendSMSToTeacherConstants.LST_CONTACTGROUP] = lst;
                //Load gia tri mat dinh           
                dic["isGetDefault"] = true;
                SMS_TEACHER_CONTACT_GROUP objContactGroup = contactGroupBusiness.Search(dic).FirstOrDefault();
                SendSMSToTeacherViewModel model = null;
                if (objContactGroup != null) model = loadModel(objContactGroup.CONTACT_GROUP_ID);

                SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);

                ViewData[SendSMSToTeacherConstants.PREFIX_NAME_OF_SCHOOL] = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null);

                return View(model);
            }
        }
        #endregion

        #region load SMSEditor
        /// <summary>
        /// Load SMS Editor
        /// </summary>
        /// <returns></returns>
        public PartialViewResult LoadSMSEditor(int contactGroupID)
        {
            var model = loadModel(contactGroupID);

            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);

            ViewData[SendSMSToTeacherConstants.PREFIX_NAME_OF_SCHOOL] = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null);

            return PartialView("_SMSEditor", model);
        }
        #endregion

        #region load list teacher in default group
        /// <summary>
        /// get list teacher receiver
        /// </summary>
        /// <author>HaiVT 28/08/2013</author>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetListTeacherReceiver(int contactGroupID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["schoolID"] = _globalInfo.SchoolID.Value;
            dic["contactGroupID"] = contactGroupID;
            dic["teacherID"] = _globalInfo.EmployeeID.GetValueOrDefault();
            dic["ProvinceID"] = _globalInfo.ProvinceID.Value ;
            dic["academicYearID"] = _globalInfo.AcademicYearID;
            List<TeacherBO> teacherBOList = teacherContactBusiness.GetListTeacherInGroup(dic).Where(p => p.EmployeeStatus == GlobalConstantsEdu.COMMON_EMPLOYMENT_STATUS_WORKING && p.IsActive).ToList();
            if (teacherBOList != null)
            {
                ViewData[SendSMSToTeacherConstants.LIST_TEACHER_RECEIVER] = teacherBOList.ToList();
            }
            else
            {
                ViewData[SendSMSToTeacherConstants.LIST_TEACHER_RECEIVER] = new List<TeacherBO>();
            }
            return PartialView("_ListTeacherReceiver");
        }
        #endregion

        #region send SMS
        /// <summary>
        /// Send SMS to Teacher
        /// </summary>
        /// <modifider date="2012/11/12" author="HaiVT">Sua cau thong bao truong hop nhom lien lac bi khoa va gui tin nhan theo confirm GP</modifider>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        //[Log]
        public JsonResult SendSMS(SendSMSToTeacherViewModel model, bool RemoveSpecialChar = false, string LstTeacherReceiverID = "", bool CheckAllReceiver = false)
        {
            var fullMdl = loadModel(model.ContactGroupID);

            //viethd4: kiem tra co nam trong danh sach gui tin cho nhom
            bool isAllow = false;
            SMS_TEACHER_CONTACT_GROUP cg = contactGroupBusiness.Find(fullMdl.ContactGroupID);
            //Neu la nhom toan truong
            if (fullMdl.isDefaultGroup) isAllow = true;
            //Admin truong
            if (_globalInfo.IsAdmin) isAllow = true;

            //La can bo quan ly
            if (cg.IS_ALLOW_SCHOOL_BOARD_SEND == true && (_globalInfo.IsRolePrincipal)) isAllow = true;


            //Neu la thanh vien cua nhom
            if (cg.IS_ALLOW_MEMBER_SEND == true)
            {
                //Lay danh sach thanh vien cua nhom
                SMS_TEACHER_CONTACT tc = teacherContactBusiness.All.Where(o => o.IS_ACTIVE == true && o.CONTACT_GROUP_ID == fullMdl.ContactGroupID && o.TEACHER_ID == (_globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0)).FirstOrDefault();
                if (tc != null)
                {
                    isAllow = true;
                }
            }

            if (GetIDsFromString(cg.ASSIGN_TEACHERS_ID).Contains(_globalInfo.EmployeeID.GetValueOrDefault())) isAllow = true;

            if (!isAllow)
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage(String.Format(Res.Get("Send_SMS_Not_Allow"), cg.CONTACT_GROUP_NAME), JsonMessage.ERROR));
            }


            //if (!CheckUser(fullMdl.ContactGroupID))
            //{
            //    WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
            //    return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_LockContractGroup_Teacher"), JsonMessage.ERROR));
            //}

            if (RemoveSpecialChar)
            {
                model.SMSContent = model.SMSContent.RemoveUnprintableChar();
            }


            LstTeacherReceiverID = string.IsNullOrEmpty(LstTeacherReceiverID) ? string.Empty : LstTeacherReceiverID;
            //string smsTemplate = "<tên trường>-<tên tổ/nhóm>:<nội dung trao đổi>";
            if (string.IsNullOrEmpty(model.SMSContent))
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_Empty_SMS_Error"), JsonMessage.ERROR));
            }
            else model.SMSContent = model.SMSContent.Trim();

            if (fullMdl.NumOfTeacher == 0)
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_No_Teacher_In_Group"), JsonMessage.ERROR));
            }
            // Gan noi dung
            fullMdl.SMSContent = model.SMSContent;
            //kiem tra gui tin khi chua nhap tin nhan
            if (fullMdl.SMSContent.ToUpper().Equals(fullMdl.BrandName.ToUpper())
                || fullMdl.SMSContent.ToUpper().Equals(fullMdl.BrandName.ToUpper() + " " + fullMdl.SMSTemplate.ToUpper()))
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_Empty_SMS_Error"), JsonMessage.ERROR));
            }
            
            //Gửi tin nhan den giao vien
            var counter = 0;
            SMS_TYPE smsType = smsTypeBusiness.All.FirstOrDefault(a => a.TYPE_CODE.StartsWith(SMS_TYPE_CODE));
            if (smsType == null)
            {
                //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_SMS_Not_Active"), JsonMessage.ERROR));
            }

            #region Short content
            string shortContent = fullMdl.SMSContent.Trim();

            if (!fullMdl.SMSTemplate.Trim().StartsWith(fullMdl.BrandName.Trim()))
            {
                fullMdl.SMSTemplate = fullMdl.BrandName.Trim() + " " + fullMdl.SMSTemplate.Trim();
            }
            if (shortContent.Trim().StartsWith(fullMdl.SMSTemplate.Trim()))
            {
                if (fullMdl.SMSTemplate.StartsWith(fullMdl.BrandName))
                {
                    shortContent = fullMdl.SMSContent.Trim().Remove(0, fullMdl.SMSTemplate.Trim().Length);
                }
                else
                {
                    shortContent = fullMdl.SMSContent.Trim().Remove(0, fullMdl.SMSTemplate.Trim().Length + fullMdl.BrandName.Trim().Length);
                }
            }
            else if (shortContent.Trim().StartsWith(fullMdl.BrandName.Trim()))
            {
                shortContent = fullMdl.SMSContent.Remove(0, fullMdl.BrandName.Length);
            }
            #endregion

            //Gui tin nhan den GV
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["contactGroupID"] = fullMdl.ContactGroupID;
            //Nếu là admin trường thì TeacherID = 0
            dic["senderID"] = _globalInfo.EmployeeID.GetValueOrDefault();

            //
            bool isSendSignMessage = false;
            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);
            if (cfg != null) isSendSignMessage = cfg.IS_ALLOW_SEND_UNICODE && model.AllowSignMessage;
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["schoolID"] = _globalInfo.SchoolID.Value;
            dictionary["isLock"] = false;
            if (fullMdl.isDefaultGroup)
            {
                dictionary["isGetDefault"] = true;
            }
            else
            {
                dictionary["contactGroupID"] = fullMdl.ContactGroupID;
            }
            //if (fullMdl.isDefaultGroup)
            //{
            SMS_TEACHER_CONTACT_GROUP objContractGroup = null;
            if (contactGroupBusiness.Search(dictionary) != null)
            {
                objContractGroup = contactGroupBusiness.Search(dictionary).FirstOrDefault();
            }
            if (objContractGroup == null)
            {
                //Thong bao loi
                if (_globalInfo.IsRolePrincipal)
                {
                    //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                    return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_LockContractGroup_Teacher"), GlobalConstantsEdu.ERROR));
                }
                else
                {
                    //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                    return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_LockContractGroup_Admin"), GlobalConstantsEdu.ERROR));
                }
            }
            else
            {
                if (CheckAllReceiver)
                {
                    dic["lstReceiverID"] = new List<int>();
                }
                else
                {
                    List<int> lstTeacherReceiverIDToSend = LstTeacherReceiverID.Split(new char[] { GlobalConstantsEdu.charCOMMA }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    if (lstTeacherReceiverIDToSend != null && lstTeacherReceiverIDToSend.Count > 0)
                    {
                        dic["lstReceiverID"] = lstTeacherReceiverIDToSend;
                    }
                    else
                    {
                        //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                        return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_EmptyReceiver"), JsonMessage.ERROR));
                    }
                }
            }

            //viethd31: Check ky tu dac biet
            bool isContentError = false;
            string dicContent = fullMdl.SMSContent.Trim();
            string unsignedContent = dicContent.StripVNSign();
            List<char> lstChar = new List<char>();
            string contentError = string.Empty;
            if (!this.IsPrintableString(unsignedContent, lstChar))
            {
                isContentError = true;
                contentError = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
            }

            if (!isContentError)
            {
                dic["schoolID"] = _globalInfo.SchoolID.Value;
                dic["isSignMsg"] = isSendSignMessage;
                dic["content"] = fullMdl.SMSContent;
                dic["shortContent"] = shortContent;
                dic["academicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["type"] = GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER;
                dic["typeHistory"] = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TEACHER_ID;
                dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
                dic["isAdmin"] = _globalInfo.IsAdmin;
                dic["Promotion"] = fullMdl.Promotion;
                string username = SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value);
                dic["UserName"] = username;

                ResultBO result = smsHistoryBusiness.SendSMS(dic);
                if (result.isError)
                {
                    //VTODO WriteLogInfo("Gửi tin nhắn không thành công cho giáo viên", null, string.Empty, string.Empty, null, string.Empty);
                    return Json(new { Message = Res.Get(result.ErrorMsg), Type = "error", OutOfBalance = result.OutOfBalance });
                }

                counter = result.NumSendSuccess;
                string desLog = string.Format("Gửi tin nhắn thành công cho {0} GV. ", counter);

                //Nếu giao dịch gửi tin gv có thay đổi tài khoản chính mới ghi log
                if (result.TransAmount > 0)
                {
                    desLog += ("Ngày giao dịch: " + DateTime.Now + ". Loại GD: Giao dịch thanh toán. Số lượng " + result.TotalSMSSentInMainBalance + " SMS. Số tiền: " + result.TransAmount + "VNĐ. Dịch vụ: Tin nhắn GV");
                }
                //VTODO WriteLogInfo(desLog, null, string.Empty, string.Empty, null, string.Empty);
                return Json(new { Message = string.Format(Res.Get("Send_SMS_Group_SMS_Success_Msg"), counter), Type = "success", Balance = result.Balance, NumOfPromotionInternalSMS = result.NumOfPromotionInternalSMS });
            }
            else
            {
                return Json(new JsonMessage(contentError, "ContentError"));
            }
        }

        public PartialViewResult ShowErrorConfirm(string contentError)
        {
            return PartialView("_ViewErrorConfirm", contentError);
        }
        #endregion

        #region load school image
        /// <summary>
        /// return pupil image 
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>10/05/2013</date>
        /// <param name="id"></param>
        [HttpGet]
        [OutputCache(Duration = 604800, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, VaryByParam = "id")]
        public ActionResult GetSchoolImage(int id)
        {
            try
            {
                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                byte[] image = sp.Image;
                if (image != null && image.Length > 0)
                {
                    return new FileStreamResult(new System.IO.MemoryStream(image), GlobalConstantsEdu.COMMON_IMAGE_TYPE);
                }

                return File("~/Content/images/teacher_avatar.jpg", GlobalConstantsEdu.COMMON_IMAGE_TYPE);
            }
            catch
            {
                return File("~/Content/images/teacher_avatar.jpg", GlobalConstantsEdu.COMMON_IMAGE_TYPE);
            }
        }
        #endregion

        private bool IsPrintableString(string text, List<char> lstChar)
        {
            bool isPrintable = true;
            for (int i = 0; i < text.Length; i++)
            {
                string s = text[i].ToString().StripVNSign();
                if (!string.IsNullOrWhiteSpace(s) && (s[0] < 32 || s[0] > 126))
                {
                    isPrintable = false;
                    lstChar.Add(text[i]);
                }
            }

            return isPrintable;
        }

        #region load Model
        private SendSMSToTeacherViewModel loadModel(int contactGroupID)
        {
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int schoolID = _globalInfo.SchoolID.Value;
            int provinceId = _globalInfo.ProvinceID.Value;
            if (provinceId == 0)
            {
                SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
                provinceId = (school != null) ? school.ProvinceID.Value : 0;
            }
            SendSMSToTeacherViewModel model = new SendSMSToTeacherViewModel();
            //Noi dung tin nhan
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID.Value);
            model.SMSContent = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null);
            model.BrandName = CommonMethods.GetBrandName();
            //Kiem tra, neu la group toan truong thi add tay giao vien vao
            SMS_TEACHER_CONTACT_GROUP contactGroupObj = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["contactGroupID"] = contactGroupID;
            if (contactGroupBusiness.Search(dic) != null)
            {
                contactGroupObj = contactGroupBusiness.Search(dic).FirstOrDefault();
            }
            if (contactGroupObj != null)
            {
                if (contactGroupObj.IS_DEFAULT)
                {
                    List<TeacherBO> teacherBOList = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID);
                    if (teacherBOList != null && teacherBOList.Count > 0)
                    {
                        teacherBOList = teacherBOList.Where(p => p.EmployeeStatus == GlobalConstantsEdu.EMPLOYEE_STATUS_WORKING && p.IsActive).ToList();
                    }

                    model.isDefaultGroup = true;
                    model.ContactGroupID = contactGroupObj.CONTACT_GROUP_ID;
                    model.GroupName = contactGroupObj.CONTACT_GROUP_NAME;
                    if (teacherBOList != null)
                    {
                        if (!_globalInfo.IsAdmin)
                        {
                            model.NumOfTeacher = teacherBOList.Count() - 1;
                        }
                        else
                        {
                            model.NumOfTeacher = teacherBOList.Count();
                        }
                    }
                    else
                    {
                        model.NumOfTeacher = 0;
                    }
                }
                else
                {
                    model.ContactGroupID = contactGroupObj.CONTACT_GROUP_ID;
                    model.GroupName = contactGroupObj.CONTACT_GROUP_NAME;
                    //danh sach giao vien nam trong contact group   
                    dic["teacherID"] = _globalInfo.EmployeeID.GetValueOrDefault();
                    dic["isIgnoringTeacher"] = true;
                    dic["bIsHasContractGroupObject"] = true;
                    dic["objContactGroup"] = contactGroupObj;
                    List<TeacherBO> teacherBOListTmp = teacherContactBusiness.GetListTeacherInGroup(dic);
                    if (teacherBOListTmp != null && teacherBOListTmp.Count > 0)
                    {
                        teacherBOListTmp = teacherBOListTmp.Where(p => p.EmployeeStatus == GlobalConstantsEdu.COMMON_EMPLOYMENT_STATUS_WORKING && p.IsActive == true).ToList();
                    }
                    if (teacherBOListTmp != null)
                    {
                        model.TeacherBOList = teacherBOListTmp;
                        model.NumOfTeacher = teacherBOListTmp.Count;
                    }
                }

                // balance
                var ewallet = EWalletBusiness.GetEWalletIncludePromotion(schoolID, true, false);
                if (ewallet != null)
                {
                    model.Balance = ewallet.Balance;
                    //model.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                }

                //Lay chuong trinh khuyen mai
                PromotionProgramBO objPromotion = PromotionProgramBusiness.GetPromotionProgramByProvinceId(provinceId, schoolID);
                if (objPromotion != null)
                {
                    model.ContentPromotion = !String.IsNullOrEmpty(objPromotion.Note) ? objPromotion.Note : objPromotion.PromotionName;
                    model.Promotion = objPromotion;
                }

                //Config allow send unicode msg
                SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(schoolID);
                if (cfg != null) ViewData[SendSMSToTeacherConstants.IS_ALLOW_SEND_UNICODE_MSG] = cfg.IS_ALLOW_SEND_UNICODE;
            }
            else throw new ArgumentNullException();

            model.SMSTemplate = (string.Format("{0}:{1}", _globalInfo.SchoolName, string.Empty));
            return model;
        }
        #endregion

        /// <summary>
        /// kiểm tra tai khoan thuoc nhom
        /// </summary>
        /// <param name="contactGroupID"></param>
        /// <returns></returns>
        private bool CheckUser(int contactGroupID)
        {
            if (_globalInfo.IsAdmin || _globalInfo.IsRolePrincipal) return true;
            else
            {
                if (contactGroupBusiness.CheckUserSendSMSInContactGroup(_globalInfo.SchoolID.Value, _globalInfo.EmployeeID.GetValueOrDefault(), contactGroupID))
                    return true;
                else return false;
            }
        }

        private List<int> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                IDArr = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                IDArr = new string[] { };
            }
            List<int> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return listID;
        }
    }
}