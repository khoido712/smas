﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Areas.SMSEduArea.Constants;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using System.Globalization;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class TransactionHistoryController:BaseController
    {
        private ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness;
        private IAffectedHistoryBusiness AffectedHistoryBusiness;
        private IEWalletBusiness EwalletBusiness;
        private ITopupBusiness TopupBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IClassProfileBusiness ClassProfileBusiness;

        public TransactionHistoryController(
            ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness, IAffectedHistoryBusiness AffectedHistoryBusiness,
            IEWalletBusiness EwalletBusiness, ITopupBusiness TopupBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IClassProfileBusiness ClassProfileBusiness)
        {
            this.SMSParentContractInClassDetailBusiness = SMSParentContractInClassDetailBusiness;
            this.AffectedHistoryBusiness = AffectedHistoryBusiness;
            this.EwalletBusiness = EwalletBusiness;
            this.TopupBusiness = TopupBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }
        public PartialViewResult AjaxLoadGridRefund(int Year, string AccountName, int AppliedLevelID, int EducationLevelID, int Type)
        {


            if (Type == 1)
            {

                SchoolBO objSP = SchoolProfileBusiness.GetUnitByAdminAccount(AccountName);
                AcademicYear objAcademicYear = objSP != null ? AcademicYearBusiness.All.Where(o => o.SchoolID == objSP.SchoolID && o.Year == Year).FirstOrDefault() : null;

                List<ClassProfile> lstClassBO = new List<ClassProfile>();

                if (objSP != null && objSP.SchoolID != 0 && objAcademicYear!=null)
                {
                    lstClassBO = ClassProfileBusiness.GetListClassBySchool(objSP.SchoolID, objAcademicYear.AcademicYearID)
                        .OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList(); ;
                    if (AppliedLevelID > 0)
                    {
                        lstClassBO = lstClassBO.Where(o => o.EducationLevel.Grade == AppliedLevelID).ToList();
                    }

                    if (EducationLevelID > 0)
                    {
                        lstClassBO = lstClassBO.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    }
                }

                ViewData[TransactionHistoryConstants.SCHOOL_ID] = objSP != null ? objSP.SchoolID : 0;
                ViewData[TransactionHistoryConstants.UNIT_NAME] = objSP != null ? objSP.SchoolName : string.Empty;
                ViewData[TransactionHistoryConstants.TITLE] = objSP != null ? objSP.SchoolName + " - " + objSP.ProvinceName : string.Empty;
                return PartialView("_GridSMSRefund", lstClassBO);
            }
            else
            {
                List<MoneyRefundModel> lstModel = new List<MoneyRefundModel>();
                SchoolBO objSP = SchoolProfileBusiness.GetUnitByAdminAccount(AccountName);

                if (objSP != null)
                {
                    MoneyRefundModel model = new MoneyRefundModel();
                    //Lay thong tin so du tai khoan truong
                    int AccountID = 0;
                    if (objSP.SupervisingDeptID != 0)
                    {
                        AccountID = objSP.SupervisingDeptID;
                        model.UnitId = objSP.SupervisingDeptID;
                        model.UnitName = objSP.SupervisingDeptName;
                        model.ProvinceName = objSP.ProvinceName;
                        model.UnitType = TransactionHistoryConstants.UNIT_TYPE_SUP;
                    }
                    else if (objSP.SchoolID != 0)
                    {
                        AccountID = objSP.SchoolID;
                        model.UnitId = objSP.SchoolID;
                        model.UnitName = objSP.SchoolName;
                        model.ProvinceName = objSP.ProvinceName;
                        model.UnitType = TransactionHistoryConstants.UNIT_TYPE_SCHOOL;
                    }

                    EwalletBO ewalletObj = EwalletBusiness.GetEWallet(AccountID, objSP.SchoolID != 0, objSP.SupervisingDeptID != 0);

                    if (ewalletObj != null)
                    {
                        model.Balance = ewalletObj.Balance;
                        model.EwalletID = ewalletObj.EWalletID;
                    }

                    lstModel.Add(model);
                }

                return PartialView("_GridMoneyRefund", lstModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSMSRefund(List<SMSRefundModel> lstModel, int schoolId, string schoolName)
        {
            List<SMSRefundModel> lstCheckedModel = lstModel.Where(o => o.IsChecked).ToList();
            if (lstCheckedModel.Count() == 0)
            {
                return Json(new JsonMessage("Cần chọn lớp để hoàn tin trước khi lưu", "error"));
            }

            List<int> lstClassId = lstCheckedModel.Select(o => o.ClassID).ToList();
            List<SMS_PARENT_CONTRACT_CLASS> lstContractInClass = SMSParentContractInClassDetailBusiness.All.Where(o => o.SCHOOL_ID == schoolId
                && lstClassId.Contains(o.CLASS_ID)).ToList();

            List<SMSRefundModel> lstRefundedModel = new List<SMSRefundModel>();
            int totalSMS = 0;
            for (int i = 0; i < lstCheckedModel.Count; i++)
            {
                SMSRefundModel model = lstCheckedModel[i];
                if (!model.SMSNum.HasValue || string.IsNullOrWhiteSpace(model.Content))
                {
                    return Json(new JsonMessage("Cần nhập đủ thông tin trước khi lưu", "error"));
                }

                SMS_PARENT_CONTRACT_CLASS cd = lstContractInClass.FirstOrDefault(o => o.CLASS_ID == model.ClassID
                    && o.SEMESTER == model.Semester);
                if (cd != null)
                {
                    cd.TOTAL_SMS = cd.TOTAL_SMS + model.SMSNum.GetValueOrDefault();
                    totalSMS = totalSMS + model.SMSNum.Value;

                    lstRefundedModel.Add(model);
                }
            }

            if (lstRefundedModel.Count > 0)
            {
                EW_AFFTECTED_HISTORY ah = new EW_AFFTECTED_HISTORY();
                ah.AFFTECTED_HISTORY_ID = Guid.NewGuid();
                ah.UNIT_TYPE = TransactionHistoryConstants.UNIT_TYPE_SCHOOL;
                ah.UNIT_ID = schoolId;
                ah.UNIT_NAME = schoolName;
                ah.AFFECTED_TYPE = TransactionHistoryConstants.AFFECTED_TYPE_REFUND;
                ah.CAL_UNIT = "SMS";
                ah.QUANTITY = totalSMS;
                ah.HISTORY_CONTENT = string.Join("; ", lstRefundedModel.Select(o => o.ClassName + ": " + o.SMSNum + ". Lý do: " + o.Content.Trim()));
                ah.AFFECTED_TIME = DateTime.Now;

                AffectedHistoryBusiness.Insert(ah);
            }

            SMSParentContractInClassDetailBusiness.Save();
            AffectedHistoryBusiness.Save();
            
            return Json(new JsonMessage("Lưu thành công", "success"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveMoneyRefund(List<MoneyRefundModel> lstModel)
        {
            MoneyRefundModel model = lstModel.FirstOrDefault();

            if (!model.Money.HasValue || string.IsNullOrWhiteSpace(model.Content))
            {
                return Json(new JsonMessage("Cần nhập đủ thông tin trước khi lưu", "error"));
            }

            EwalletBO eo = EwalletBusiness.ChangesAmountAddDirectly(model.EwalletID, model.Money.Value);

            EW_AFFTECTED_HISTORY ah = new EW_AFFTECTED_HISTORY();
            ah.AFFTECTED_HISTORY_ID = Guid.NewGuid();
            ah.UNIT_TYPE = model.UnitType;
            ah.UNIT_NAME = model.UnitName;
            ah.UNIT_ID = model.UnitId;
            ah.AFFECTED_TYPE = TransactionHistoryConstants.AFFECTED_TYPE_REFUND;
            ah.CAL_UNIT = "VNĐ";
            ah.QUANTITY = (long)model.Money.Value;
            ah.HISTORY_CONTENT = "Lý do: " + model.Content;
            ah.AFFECTED_TIME = DateTime.Now;

            AffectedHistoryBusiness.Insert(ah);

            AffectedHistoryBusiness.Save();

            return Json(new JsonMessage("Lưu thành công", "success"));
        }

        [HttpPost]
        public PartialViewResult SearchTransfer(string SenderAccountName, string ReceiverAccountName)
        {
            List<TransferMoneyModel> lstModel = new List<TransferMoneyModel>();
            int ewalletId = 0;
            int receiverEwId = 0;
            string senderName = string.Empty;
            string receiverName = string.Empty;
            int senderId = 0;
            int total = 0;

            SchoolBO objSP = SchoolProfileBusiness.GetUnitByAdminAccount(SenderAccountName);

            if (objSP != null && objSP.SchoolID != 0)
            {
                senderName = objSP.SchoolName.Replace("'", "&#34;");
                senderId = objSP.SchoolID;
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SchoolID, true, false);
                if (ewalletObj != null)
                {
                    ewalletId = ewalletObj.EWalletID;
                }
            }
            else if (objSP != null && objSP.SupervisingDeptID != 0)
            {
                senderName = objSP.SupervisingDeptName.Replace("'", "&#34;");
                senderId = objSP.SupervisingDeptID;
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SupervisingDeptID, false, true);
                if (ewalletObj != null)
                {
                    ewalletId = ewalletObj.EWalletID;
                }
            }

            if (ewalletId != 0)
            {
                List<EwalletTransactionBO> lstTopupBO = TopupBusiness.FindTransaction(ewalletId, null, null, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_PLUS_TOPUP, ref total);

                lstModel = lstTopupBO.Select(o => new TransferMoneyModel
                {
                    Money = o.TransactionAmount.ToString("#,###"),
                    TransactionDate = o.TransactionDateTime.ToString("dd/MM/yyyy"),
                    Content = "Nạp thẻ: " + o.CardSerial,
                    Amount = (int)o.TransactionAmount,
                    EwalletTransactionID = o.TransactionID
                }).ToList();
            }


            objSP = SchoolProfileBusiness.GetUnitByAdminAccount(ReceiverAccountName);
            if (objSP != null && objSP.SchoolID != 0)
            {
                receiverName = objSP.SchoolName.Replace("'", "&#34;");
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SchoolID, true, false);
                if (ewalletObj != null)
                {
                    receiverEwId = ewalletObj.EWalletID;
                }
            }

            ViewData[TransactionHistoryConstants.SENDER_EW_ID] = ewalletId;
            ViewData[TransactionHistoryConstants.RECEIVER_EW_ID] = receiverEwId;
            ViewData[TransactionHistoryConstants.SENDER_NAME] = senderName;
            ViewData[TransactionHistoryConstants.SENDER_ID] = senderId;
            ViewData[TransactionHistoryConstants.RECEIVER_NAME] = receiverName;
            ViewData[TransactionHistoryConstants.TOTAL] = total;

            return PartialView("_GridTransfer", lstModel);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchTransferAjax(string SenderAccountName, string ReceiverAccountName, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;
            int total = 0;

            List<TransferMoneyModel> lstModel = new List<TransferMoneyModel>();
            int ewalletId = 0;

            SchoolBO objSP = SchoolProfileBusiness.GetUnitByAdminAccount(SenderAccountName);

            if (objSP != null && objSP.SchoolID != 0)
            {
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SchoolID, true, false);
                if (ewalletObj != null)
                {
                    ewalletId = ewalletObj.EWalletID;
                }
            }

            if (ewalletId != 0)
            {
                List<EwalletTransactionBO> lstTopupBO = TopupBusiness.FindTransaction(ewalletId, null, null, GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_PLUS_TOPUP, ref total);

                lstModel = lstTopupBO.Select(o => new TransferMoneyModel
                {
                    Money = o.TransactionAmount.ToString("#,###"),
                    TransactionDate = o.TransactionDateTime.ToString("dd/MM/yyyy"),
                    Content = "Nạp thẻ: " + o.CardSerial,
                    Amount = (int)o.TransactionAmount,
                    EwalletTransactionID = o.TransactionID
                }).ToList();
            }


            ViewData[TransactionHistoryConstants.TOTAL] = total;

            return View(new GridModel<TransferMoneyModel>
            {
                Total = total,
                Data = lstModel
            });
        }

        public JsonResult GetSchoolAccountInfo(string userName)
        {
            string schoolName = string.Empty;
            string provinceName = string.Empty;
            string balance = string.Empty;
            int ewalletId = 0;
            SchoolBO objSP = SchoolProfileBusiness.GetUnitByAdminAccount(userName);
            if (objSP != null && objSP.SchoolID != 0)
            {
                schoolName = objSP.SchoolName;
                provinceName = objSP.ProvinceName;
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SchoolID, true, false);
                if (ewalletObj != null)
                {
                    balance = (ewalletObj.Balance == 0 ? "0" : ewalletObj.Balance.ToString("#,###")) + " VNĐ";
                    ewalletId = ewalletObj.EWalletID;
                }
            }
            else if (objSP != null && objSP.SupervisingDeptID != 0)
            {
                schoolName = objSP.SupervisingDeptName;
                provinceName = objSP.ProvinceName;
                EwalletBO ewalletObj = EwalletBusiness.GetEWallet(objSP.SupervisingDeptID,false, true);
                if (ewalletObj != null)
                {
                    balance = (ewalletObj.Balance == 0 ? "0" : ewalletObj.Balance.ToString("#,###")) + " VNĐ";
                    ewalletId = ewalletObj.EWalletID;
                }
            }

            return Json(new { SchoolName = schoolName, ProvinceName = provinceName, Balance = balance, EwalletID = ewalletId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveTransferMoney(List<TransferMoneyModel> lstModel, int SenderSchoolD, int SenderEwalletID, int ReceiverEwallerID, string SenderName, string ReceiverName, string TransferContent)
        {
            lstModel = lstModel.Where(o => o.IsChecked).ToList();

            if (lstModel.Count == 0)
            {
                return Json(new JsonMessage("Cần chọn giao dịch trước khi chuyển", "error"));
            }

            if (SenderEwalletID == 0 || ReceiverEwallerID == 0)
            {
                return Json(new JsonMessage("Không tìm thấy trường gửi hoặc trường nhận", "error"));
            }

            if (string.IsNullOrWhiteSpace(TransferContent))
            {
                return Json(new JsonMessage("Cần nhập lý do chuyển tiền", "error"));
            }

            int amount = lstModel.Sum(o => o.Amount);

            //Kiem tra truong truong con du tien hay khong
            EwalletBO ewalletObj = EwalletBusiness.GetEWallet(SenderSchoolD, true, false);
            if (ewalletObj.Balance < amount)
            {
                return Json(new JsonMessage("Số dư tài khoản của trường gửi không đủ", "error"));
            }

            //Cong cho truong nhan
            EwalletBusiness.ChangesAmountAddDirectly(ReceiverEwallerID, amount);

            //Tru cua truong gui
            EwalletBusiness.ChangesAmountAddDirectly(SenderEwalletID, 0 - amount);

            //Chuyen cac giao dich sang truong nhan
            List<long> lstEwalletTransactionId = lstModel.Select(o => o.EwalletTransactionID).ToList();
            TopupBusiness.MoveTransaction(SenderEwalletID, ReceiverEwallerID, lstEwalletTransactionId);

            //Lưu log
            EW_AFFTECTED_HISTORY ah = new EW_AFFTECTED_HISTORY();
            ah.AFFTECTED_HISTORY_ID = Guid.NewGuid();
            ah.UNIT_TYPE = TransactionHistoryConstants.UNIT_TYPE_SCHOOL;
            ah.UNIT_ID = SenderSchoolD;
            ah.UNIT_NAME = SenderName;
            ah.AFFECTED_TYPE = TransactionHistoryConstants.AFFECTED_TYPE_TRANSFER;
            ah.CAL_UNIT = "VNĐ";
            ah.QUANTITY = (long)amount;
            ah.HISTORY_CONTENT = string.Format("Chuyển sang trường {0}. Lý do: {1}", ReceiverName, TransferContent);
            ah.AFFECTED_TIME = DateTime.Now;

            AffectedHistoryBusiness.Insert(ah);

            AffectedHistoryBusiness.Save();

            return Json(new JsonMessage("Chuyển tiền thành công", "success"));
        }

        [HttpPost]
        public ActionResult SearchTransaction(int TransactionType, string FromDate, string ToDate)
        {
            DateTime fromDate;
            DateTime toDate;



            if (!DateTime.TryParseExact(FromDate,
                       "dd/MM/yyyy",
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out fromDate))
            {
                return Json(new JsonMessage("Ngày tháng không hợp lệ", "error"));
            }

            if (!DateTime.TryParseExact(ToDate,
                       "dd/MM/yyyy",
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out toDate))
            {
                toDate = DateTime.Now;
            }

            IQueryable<EW_AFFTECTED_HISTORY> Iquery = this.GetQueryAffected(TransactionType, fromDate, toDate);
            List<TransactionViewModel> lstTransaction = Iquery.Take(TransactionHistoryConstants.PAGE_SIZE)
                .Select(o => new TransactionViewModel
                {
                    AffectedHistoryID = o.AFFTECTED_HISTORY_ID,
                    AffectedTime = o.AFFECTED_TIME,
                    AffectedType = o.AFFECTED_TYPE,
                    CalUnit = o.CAL_UNIT,
                    Content = o.HISTORY_CONTENT,
                    Quantity = o.QUANTITY,
                    UnitID = o.UNIT_ID,
                    UnitName = o.UNIT_NAME,
                    UnitType = o.UNIT_TYPE
                }).ToList();

            ViewData[TransactionHistoryConstants.TOTAL] = Iquery.Count();
            ViewData[TransactionHistoryConstants.PAGE] = 1;
            return PartialView("_GridTransaction", lstTransaction);

        }


        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchTransactionAjax(int TransactionType, string FromDate, string ToDate, GridCommand command)
        {
            int currentPage = command.Page;
            DateTime fromDate;
            DateTime toDate;
            if (!DateTime.TryParseExact(FromDate,
                       "dd/MM/yyyy",
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out fromDate))
            {
                return Json(new JsonMessage("Ngày tháng không hợp lệ", "error"));
            }

            if (!DateTime.TryParseExact(ToDate,
                       "dd/MM/yyyy",
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None,
                       out toDate))
            {
                toDate = DateTime.Now;
            }

            IQueryable<EW_AFFTECTED_HISTORY> Iquery = this.GetQueryAffected(TransactionType, fromDate, toDate);
            List<EW_AFFTECTED_HISTORY> lstTransaction = Iquery.Skip((currentPage - 1) * TransactionHistoryConstants.PAGE_SIZE)
                        .Take(TransactionHistoryConstants.PAGE_SIZE).ToList();
            return View(new GridModel<EW_AFFTECTED_HISTORY>()
            {
                Data = lstTransaction != null && lstTransaction.Count > 0 ? lstTransaction : new List<EW_AFFTECTED_HISTORY>(),
                Total = Iquery.Count()
            });
        }
        private IQueryable<EW_AFFTECTED_HISTORY> GetQueryAffected(int TransactionType, DateTime fromDate, DateTime toDate)
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999);

            IQueryable<EW_AFFTECTED_HISTORY> Iquery = (from a in AffectedHistoryBusiness.All
                                                  where (TransactionType == 0 || a.AFFECTED_TYPE == TransactionType)
                                                  && a.AFFECTED_TIME <= toDate
                                                  && a.AFFECTED_TIME >= fromDate
                                                  orderby a.AFFECTED_TIME descending
                                                  select a);
            return Iquery;
        }
        private void SetViewData()
        {
            //get year
            int curYear = DateTime.Now.Year;
            var lstYear = new List<KeyValuePair<int, string>> 
            {
                new KeyValuePair<int,string>(curYear - 1, string.Format("{0} - {1}", curYear - 1, curYear)),
                new KeyValuePair<int,string>(curYear, string.Format("{0} - {1}", curYear, curYear + 1))
            };

            ViewData[TransactionHistoryConstants.LIST_YEAR] = lstYear;
        }
    }
}