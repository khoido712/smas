﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class ConsumptionReportController:BaseController
    {
        private ISMSHistoryBusiness SMSHistoryBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;

        public ConsumptionReportController(ISMSHistoryBusiness SMSHistoryBusiness, ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        // GET: UsageStatistic/ConsumptionReport
        public ActionResult Index()
        {
            return View();
        }

        public FileResult Export(ConsumptionModel model)
        {

            string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + "ThongKeTieuDungSMSEdu.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            string SchoolName = _globalInfo.SchoolName;
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string SupervisingDeptName = String.Empty;
            if (school != null)
            {
                SupervisingDeptName = school.SupervisingDept.SupervisingDeptName;
            }

            sheet.SetCellValue("A2", SupervisingDeptName.ToUpper());

            sheet.SetCellValue("A3", SchoolName.ToUpper());

            string date = String.Format("Từ ngày {0} đến ngày {1}", model.fromdate.ToString("dd/MM/yyyy"), model.todate.ToString("dd/MM/yyyy"));
            sheet.SetCellValue("A6", date);
            List<ConsumptionReportBO> lstConsumption = SMSHistoryBusiness.GetReportConsumption(_globalInfo.SchoolID.Value, model.fromdate, model.todate);


            int curRow = 10;
            int curCol = 1;

            IVTRange ranTemp = sheet.GetRange("A10", "F10");
            if (lstConsumption != null)
            {
                for (int i = 0; i < lstConsumption.Count; i++)
                {
                    curCol = 1;
                    ConsumptionReportBO objComsumption = lstConsumption[i];
                    if (i > 0)
                    {
                        sheet.CopyAndInsertARow(ranTemp, curRow, true);
                    }
                    //STT
                    sheet.SetCellValue(curRow, curCol, i + 1);
                    curCol++;
                    //Khoan chi phi
                    sheet.SetCellValue(curRow, curCol, objComsumption.Content);
                    curCol++;
                    //don gia
                    sheet.SetCellValue(curRow, curCol, objComsumption.Price);
                    curCol++;

                    //So luong
                    sheet.SetCellValue(curRow, curCol, objComsumption.Quantity);
                    curCol++;

                    //Tong tien
                    sheet.SetCellValue(curRow, curCol, objComsumption.Amount);
                    curCol++;

                    //Ngay giao dich
                    sheet.SetCellValue(curRow, curCol, objComsumption.CreatedTime.ToString("dd/MM/yyyy"));
                    curCol++;
                    curRow++;
                }
            }
            //ve border
            //sheet.GetRange(startRow, startCol, curRow - 1, lastCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

            /*IVTWorksheet tSheet = oBook.GetSheet(2);
            IVTRange tRange = tSheet.GetRange("B2", "J10");

            sheet.CopyPaste(tRange, curRow + 1, 1);

            tSheet.Delete();*/

            sheet.SetCellValue(curRow + 3, 2, school.HeadMasterName);
            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "ThongKeTieuDungSMSEdu.xls";


            return result;


        }
    }
}