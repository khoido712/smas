﻿using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Web.Mvc;
using SMAS.Web.Areas.SMSEduArea.Models;
using System.IO;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class EWalletController:BaseController
    {
        private readonly IEWalletBusiness EwalletBusiness;
        private readonly IPromotionBusiness PromotionPackageBusiness;
        private int _userID;
        private int userID
        {
            get
            {
                if (_userID <= 0)
                {
                    if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                    {
                        ISupervisingDeptBusiness SupervisingDeptBusiness = new SupervisingDeptBusiness(this.logger, new SMAS.Models.Models.SMASEntities());
                        SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

                        if (sup == null)
                        {
                            throw new BusinessException("Không tìm thấy thông tin phòng sở.");
                        }
                        else
                        {
                            // Nhân viên thuộc Phòng trực thuộc (không có tài khoản) thì sử dụng tài khoản Sở
                            // Nhân viên thuộc Phòng ban thuộc phòng (không có tài khoản) thì sử dụng tài khoản Phòng Quản lý
                            SupervisingDept supParent = SupervisingDeptBusiness.Find(sup.ParentID);
                            if ((sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT ||
                                sup.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT) &&
                                supParent != null)
                            {
                                _userID = supParent.SupervisingDeptID;
                            }
                            else
                            {
                                _userID = sup.SupervisingDeptID;
                            }
                        }
                    }
                    else
                    {
                        if (_globalInfo.SchoolID.Value <= 0)
                        {
                            throw new BusinessException("Không tìm thấy thông tin nhà trường.");
                        }
                        else
                        {
                            _userID = _globalInfo.SchoolID.Value;
                        }
                    }
                }
                if (_userID <= 0)
                {
                    throw new BusinessException("Không tìm thấy thông tin tài khoản.");
                }
                return _userID;
            }
            set
            {
                _userID = value;
            }
        }

        private UserType _userType;
        private UserType userType
        {
            get
            {
                if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                {
                    ISupervisingDeptBusiness SupervisingDeptBusiness = new SupervisingDeptBusiness(this.logger, new SMAS.Models.Models.SMASEntities());
                    SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

                    if (sup == null)
                    {
                        throw new BusinessException("Không tìm thấy thông tin phòng sở.");
                    }
                    else
                    {
                        _userType = UserType.SupervisingDeptUser;
                    }
                }
                else
                {
                    if (_globalInfo.SchoolID.Value <= 0)
                    {
                        throw new BusinessException("Không tìm thấy thông tin nhà trường.");
                    }
                    else
                    {
                        _userType = UserType.SchoolUser;
                    }
                }
                return _userType;
            }
            set
            {
                _userType = value;
            }
        }

        private EwalletBO __eWallet;
        private EwalletBO eWallet
        {
            get
            {
                if (__eWallet == null)
                {
                    __eWallet = this.EwalletBusiness.GetEWalletIncludePromotion(userID, this.userType == UserType.SchoolUser, this.userType == UserType.SupervisingDeptUser);
                }
                if (__eWallet == null)
                {
                    throw new BusinessException("Không tìm thấy thông tin tài khoản.");
                }
                return __eWallet;
            }
            set
            {
                __eWallet = value;
            }
        }

        public EWalletController(
            IEWalletBusiness eWalletBusiness,
            IPromotionBusiness promotionPackageBusiness
            )
        {
            this.EwalletBusiness = eWalletBusiness;
            this.PromotionPackageBusiness = promotionPackageBusiness;
        }
        //
        // GET: /Payment/EWallet/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Payment/EWallet/Info
        public PartialViewResult Info()
        {
            try
            {
                ISupervisingDeptBusiness SupervisingDeptBusiness = new SupervisingDeptBusiness(this.logger, new SMAS.Models.Models.SMASEntities());
                SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);

                //Lay chuong trinh khuyen mai
                PromotionProgramBO objPromotion = null;

                if (_globalInfo.IsSuperVisingDeptRole && sup != null)
                {
                    objPromotion = PromotionPackageBusiness.GetPromotionProgramByProvinceId(_globalInfo.ProvinceID.Value, sup.SupervisingDeptID);
                }
                else if (_globalInfo.SchoolID > 0)
                {
                    objPromotion = PromotionPackageBusiness.GetPromotionProgramByProvinceId(_globalInfo.ProvinceID.Value, _globalInfo.SchoolID.Value);
                }

                string contentPromotion = "";
                if (objPromotion != null)
                {
                    contentPromotion = (!String.IsNullOrEmpty(objPromotion.Note)) ? objPromotion.Note : objPromotion.PromotionName;
                }
                EWalletInfoViewModel info;
                info = new EWalletInfoViewModel()
                {
                    EWalletID = this.eWallet.EWalletID,
                    Balance = this.eWallet.Balance,
                    NumOfPromotionInternalSMS = this.eWallet.InternalSMS,
                    ContentPromotion = contentPromotion
                };
                return PartialView("_Info", info);
            }
            catch (InvalidDataException ex)
            {
                ViewBag.MessageOfError = ex.Message;
                return PartialView("_Info");
            }
            catch (Exception ex)
            {
                ViewBag.MessageOfError = "Không tìm thấy thông tin tài khoản.";
                return PartialView("_Info");
            }
        }

        //VTODO
        //public JsonResult GetNewestPromotionPackage()
        //{
        //    try
        //    {
        //        PromotionDetailViewModel info = new PromotionDetailViewModel();
        //        PromotionPackageBO obj = PromotionPackageBusiness.GetNewestPromotionPackage(userID, userType);
        //        if (obj != null)
        //        {
        //            info.PromotionPackageID = obj.PromotionPackageID;
        //            info.PromotionPackageDetailID = obj.PromotionPackageDetailID;
        //            info.PromotionContent = obj.DetailDescription;
        //            info.PromotionTime = String.Format("từ ngày {0} đến {1}", obj.StartDate.Value.ToString(GlobalConstants.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY),
        //                                obj.EndDate.Value.ToString(GlobalConstants.BUSINESS_CUSTOM_FORMAT_DATETIME_DDMMYY));
        //        };
        //        return Json(new { Type = "success", PromotionContent = info.PromotionContent, PromotionTime = info.PromotionTime });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Type = "error", PromotionContent = "", PromotionTime = "" });
        //    }
        //}
    }
}