﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Web.Mvc;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMS_SCHOOL.Utility.Common;
using SMAS.Web.Utils;
using SMAS.Web.Areas.SMSEduArea.Constants;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    [SkipCheckRole]
    public class TransactionController:BaseController
    {
        private IEWalletBusiness EwalletBusiness;
        private readonly IEWalletTransactionBusiness EWalletTransactionBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEwalletTransactionTypeBusiness EwalletTransactionTypeBusiness;
        private readonly ITopupBusiness TopupBusiness;

        private EwalletBO __eWallet;
        private EwalletBO eWallet
        {
            get
            {
                EwalletBO eWallet;
                if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                {
                    SupervisingDept sup = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
                    if (sup == null)
                    {
                        throw new BusinessException("Không tìm thấy thông tin phòng sở.");
                    }
                    else
                    {
                        //int userID = _globalInfo.SupervisingDept.SupervisingDeptID;
                        int userId = sup.SupervisingDeptID;
                        eWallet = this.EwalletBusiness.GetEWalletIncludePromotion(userId, false, true);
                    }
                }
                else
                {
                    if (_globalInfo.SchoolID <= 0)
                    {
                        throw new BusinessException("Không tìm thấy thông tin nhà trường.");
                    }
                    else
                    {
                        int userID = _globalInfo.SchoolID.Value;
                        eWallet = this.EwalletBusiness.GetEWalletIncludePromotion(userID, true, false);
                    }
                }
                if (eWallet == null)
                {
                    throw new BusinessException("Không tìm thấy thông tin tài khoản.");
                }
                return eWallet;
            }
            set
            {
                __eWallet = value;
            }
        }

        public TransactionController(
            IEWalletBusiness eWalletBusiness,
            IEWalletTransactionBusiness TransactionBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IEwalletTransactionTypeBusiness EwalletTransactionTypeBusiness,
            ITopupBusiness TopupBusiness)
        {
            this.EwalletBusiness = eWalletBusiness;
            this.EWalletTransactionBusiness = TransactionBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.EwalletTransactionTypeBusiness = EwalletTransactionTypeBusiness;
            this.TopupBusiness = TopupBusiness;
        }
        //
        // GET: /Payment/Transaction/
        public ActionResult Index(TransactionQueryModel queryModel)
        {
            SetViewData();
            return View();
        }

        #region SetViewData
        /// <summary>
        /// Khoi tao du lieu tim kiem
        /// </summary>
        public void SetViewData()
        {
            List<EW_TRANSACTION_TYPE> lstTransType = EwalletTransactionTypeBusiness.GetTransactionType();
           
            lstTransType = lstTransType.OrderByDescending(o => o.TRANSACTION_TYPE_ID).ToList();
            lstTransType.Insert(0, new EW_TRANSACTION_TYPE() { TRANSACTION_TYPE_ID = 0, DESCRIPTION = "Tất cả" });
          
            ViewData["ListTransactionType"] = lstTransType;
        }

        #endregion SetViewData
        //
        // GET: /Payment/Transactions/?fromday=25/4/2014&today=25/5/2014&page=2
        public ActionResult Transactions(TransactionQueryModel query)
        {
            try
            {
                #region validate
                string fromdateField = StaticReflection.GetMemberName<TransactionQueryModel>(p => p.fromdate);
                ModelState fromdateModelState = this.ModelState[fromdateField];
                if (fromdateModelState != null)
                {
                    if (!string.IsNullOrEmpty(fromdateModelState.Value.AttemptedValue))
                    {
                        fromdateModelState.Errors.Clear();
                        try
                        {
                            query.fromdate = Convert.ToDateTime(fromdateModelState.Value.AttemptedValue);
                            if (query.fromdate.Value > DateTime.Now.Date)
                            {
                                fromdateModelState.Errors.Add("Từ ngày không được lớn hơn ngày hiện tại");
                            }
                        }
                        catch (FormatException)
                        {
                            fromdateModelState.Errors.Add(Res.Get("Invalid_Date"));
                        }
                        catch (InvalidCastException)
                        {
                            fromdateModelState.Errors.Add(Res.Get("Invalid_Date"));
                        }
                    }
                }

                string todateField = StaticReflection.GetMemberName<TransactionQueryModel>(p => p.todate);
                ModelState todateModelState = this.ModelState[todateField];
                if (todateModelState != null)
                {
                    if (!string.IsNullOrEmpty(todateModelState.Value.AttemptedValue))
                    {
                        todateModelState.Errors.Clear();
                        try
                        {
                            query.todate = Convert.ToDateTime(todateModelState.Value.AttemptedValue);
                            if (query.todate.Value > DateTime.Now.Date)
                            {
                                todateModelState.Errors.Add("Đến ngày không được lớn hơn ngày hiện tại");
                            }
                        }
                        catch (FormatException)
                        {
                            todateModelState.Errors.Add(Res.Get("Invalid_Date"));
                        }
                        catch (InvalidCastException)
                        {
                            todateModelState.Errors.Add(Res.Get("Invalid_Date"));
                        }
                    }
                }
                if (ModelState.IsValidField(fromdateField) && ModelState.IsValidField(todateField))
                {
                    if (query.fromdate.HasValue && query.todate.HasValue)
                    {
                        if (query.fromdate.Value > query.todate.Value)
                        {
                            fromdateModelState.Errors.Add("Đến ngày phải lớn hơn hoặc bằng từ ngày");
                        }
                    }
                }
                #endregion

                if (!this.ModelState.IsValid)
                {
                    return View("_Transactions");
                }

                int totalTransaction = 0;

                if (query != null)
                {
                    query.page = 1;
                    query.pagesize = PaymentConstants.PAGESIZE_TRANSACTIONS;
                }

                List<EwalletTransactionBO> queryResult = this.GetDataTransaction(query, ref totalTransaction);
                ViewBag.queryResult = queryResult;
                ViewBag.totalTransaction = totalTransaction;
                return PartialView("_Transactions", query);
            }
            catch (Exception)
            {
                ViewBag.FatalError = "Không thể tìm được lịch sử giao dịch. Vui lòng thử lại sau";
                return PartialView("_Transactions");
            }
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult CustomBinding(TransactionQueryModel query, GridCommand command)
        {
            query.page = command.Page;
            query.pagesize = PaymentConstants.PAGESIZE_TRANSACTIONS;
            //query.transTypeID = transTypeID;
            int Total = 0;
            List<EwalletTransactionBO> lstTransaction = this.GetDataTransaction(query, ref Total);
            return View(new GridModel<EwalletTransactionBO>
            {
                Data = lstTransaction,
                Total = Total
            });
        }

        private List<EwalletTransactionBO> GetDataTransaction(TransactionQueryModel query, ref int totalTransaction)
        {
            return this.TopupBusiness.FindTransactionPaging(eWallet.EWalletID, query.fromdate, query.todate, (query.page - 1) * PaymentConstants.PAGESIZE_TRANSACTIONS, PaymentConstants.PAGESIZE_TRANSACTIONS, query.transTypeID, ref totalTransaction);

        }
    }
}