﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.SMSEduArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea.Controllers
{
    public class UsageStatisticController:BaseController
    {
        //
        // GET: /UsageStatistic/UsageStatistic/
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public UsageStatisticController(IEmployeeBusiness EmployeeBusiness,
            ISMSHistoryBusiness SMSHistoryBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.EmployeeBusiness = EmployeeBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }
        public ActionResult Index()
        {
            return View();
        }


        public FileResult Export(UsageStatisticModel model)
        {

            string templatePath = SMAS.Business.Common.SystemParamsInFile.TEMPLATE_FOLDER + GlobalConstantsEdu.SYNTAX_CROSS_LEFT + "BCSMSEduToanTruong.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            string SchoolName = _globalInfo.SchoolName;

            //string SupervisingDeptName = GlobalInfo.SupervisingDept.SupervisingDeptName;

            // 

            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string SupervisingDeptName = String.Empty;
            if (school != null)
            {
                SupervisingDeptName = school.SupervisingDept.SupervisingDeptName;
            }

            sheet.SetCellValue("A2", SupervisingDeptName.ToUpper());

            sheet.SetCellValue("A3", SchoolName.ToUpper());

            string date = String.Format("Từ ngày {0} đến ngày {1}", model.fromdate.ToString("dd/MM/yyyy"), model.todate.ToString("dd/MM/yyyy"));
            sheet.SetCellValue("A5", date);

            sheet.SetCellValue("C6", DateTime.Now.ToString("dd/MM/yyyy"));

            //Lay danh sach GV cua truong
            List<TeacherBO> teacherBOList = EmployeeBusiness.GetListTeacherOfSchoolForReport(_globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AcademicYearID.GetValueOrDefault());

            //Lay cac tin nhan phat sinh trong khoang thoigian
            List<SMSCommunicationBO> q = SMSHistoryBusiness.GetListSMSForReport(_globalInfo.SchoolID.Value);

            q = q.Where(o => o.CreateDate.Date >= model.fromdate.Date && o.CreateDate.Date <= model.todate.Date).ToList();
            //Lay danh sach GV co gui tin nhan
            List<int> lstTeacherId = q.Select(o => o.SenderID).ToList();
            List<TeacherBO> lstTeacherSentMessage = teacherBOList.Where(o => lstTeacherId.Contains(o.TeacherID)).ToList();

            int startRow = 10;
            int startCol = 1;
            int curRow = startRow;
            int curCol = startCol;
            int lastCol = 9;
            int count;
            for (int i = 0; i < lstTeacherSentMessage.Count; i++)
            {
                TeacherBO teacher = lstTeacherSentMessage[i];
                List<SMSCommunicationBO> lstCom = q.Where(o => o.SenderID == teacher.TeacherID).ToList();
                //STT
                sheet.SetCellValue(curRow, curCol, i + 1);
                curCol++;

                //Ho ten
                sheet.SetCellValue(curRow, curCol, teacher.FullName);
                curCol++;

                //To bo mon
                sheet.SetCellValue(curRow, curCol, teacher.SchoolFacultyName);
                curCol++;

                //Loai cong viec
                sheet.SetCellValue(curRow, curCol, teacher.WorkGroupTypeName);
                curCol++;

                //ten tai khoan
                sheet.SetCellValue(curRow, curCol, teacher.AccountName);
                curCol++;

                //Tong so PHHS da duoc gui tin nhan
                count = lstCom.Where(o => o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID).Select(o => o.ReceiverID).Distinct().Count();
                sheet.SetCellValue(curRow, curCol, count);
                curCol++;

                //tong so luong tin nhan da gui cho PHHS
                count = lstCom.Where(o => o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID).Sum(o => o.SmsCount);
                sheet.SetCellValue(curRow, curCol, count);
                curCol++;

                //Tổng số CBGV đã được gửi tin nhắn
                count = lstCom.Where(o => o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID || o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID).Select(o => o.ReceiverID).Distinct().Count();
                sheet.SetCellValue(curRow, curCol, count);
                curCol++;

                //Tổng số lượng tin nhắn đã gửi cho CBGV
                count = lstCom.Where(o => o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID || o.Type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID).Sum(o => o.SmsCount);
                sheet.SetCellValue(curRow, curCol, count);
                curCol++;

                curRow++;
                curCol = startCol;
            }

            //ve border
            sheet.GetRange(startRow, startCol, curRow - 1, lastCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

            IVTWorksheet tSheet = oBook.GetSheet(2);
            IVTRange tRange = tSheet.GetRange("B2", "J10");

            sheet.CopyPaste(tRange, curRow + 1, 1);

            tSheet.Delete();

            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "BC chi tiet dung SMS Edu toan truong.xls";


            return result;


        }
    }
}