﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class BrandnameRegisCreateViewModel
    {
        public BrandnameRegisCreateViewModel()
        {
            this.ListProvider = new List<ProviderSelectionModel>();
        }

        public int BrandnameRegistrationId { get; set; }

        public string ContractNumber { get; set; }

        private string _brandname;
        public string Brandname
        {
            get
            {
                return this._brandname;
            }
            set
            {
                if (value != null)
                {
                    this._brandname = value.Trim();
                }
                else
                {
                    this._brandname = null;
                }
            }
        }

        public decimal Balance { get; set; }

        public decimal RegistFee { get; set; }

        public decimal MaintainFee { get; set; }

        public decimal ExtendFee { get; set; }

        public decimal TotalFee { get; set; }

        public List<ProviderSelectionModel> ListProvider { get; set; }
    }
}