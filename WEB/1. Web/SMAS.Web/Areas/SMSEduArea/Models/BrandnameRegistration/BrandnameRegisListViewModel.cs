﻿using SMAS.Web.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class BrandnameRegisListViewModel
    {
        public int BrandnameRegistrationId { get; set; }

        public string ContractNumber { get; set; }

        public string Brandname { get; set; }

        public int ProviderId { get; set; }

        public string ProviderName { get; set; }

        public DateTime? CreateTime { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int Status { get; set; }

        public string StatusName
        {
            get
            {
                switch (this.Status)
                {
                    case GlobalConstantsEdu.STATUS_IS_ACTIVE:
                        return "Đang hiệu lực";
                    case GlobalConstantsEdu.STATUS_IS_CANCELED:
                        return "Đã hủy";
                    case GlobalConstantsEdu.STATUS_IS_EXPIRED:
                        return "Hết hạn";
                    case GlobalConstantsEdu.STATUS_IS_LOCKED:
                        return "Tạm khóa";
                    case GlobalConstantsEdu.STATUS_WAIT_FOR_APPROVAL:
                        return "Chờ phê duyệt";
                    case GlobalConstantsEdu.STATUS_WAIT_FOR_EXTENDING:
                        return "Chờ gia hạn";
                    default:
                        return string.Empty;
                }
            }
        }
    }
}