﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class ProviderSelectionModel
    {
        public int? BrandnameRegistrationId { get; set; }

        public int ProviderId { get; set; }

        public string ProviderName { get; set; }

        public bool IsCheck { get; set; }

        public int Duration { get; set; }

        public int RegistFee { get; set; }

        public int MaintainFee { get; set; }

        public int ExtendFee { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsDefault { get; set; }
    }
}