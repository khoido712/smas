﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SMSFreeSearchViewModel
    {
        public int? EducationLevel { get; set; }

        public int? Class { get; set; }

        public int? Status { get; set; }

        public int Page { get; set; }
        public int Size { get; set; }

        public string PupilCode { get; set; }

        public string Fullname { get; set; }
    }
}