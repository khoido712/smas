﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class ContactGroupViewModel
    {
        public int ContactGroupID { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Display(Name = "Tên nhóm")]
        public string Name { get; set; }
        public bool IsLock { get; set; }
        public bool IsDefault { get; set; }
        public List<int> lstTeacherInGroupId { get; set; }
        public bool IsAllowMemberSend { get; set; }
        public bool IsAllowSchoolBoardSend { get; set; }
        public string AssignTeachersID { get; set; }
        public string strAssignedTeacher { get; set; }
    }
}