﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class ContactGroupSearchViewModel
    {
        public int? SchoolFacultyID { get; set; }
        public int? WorkGroupTypeID { get; set; }
        public int? WorkTypeID { get; set; }
        public int? ComunityID { get; set; }
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("Employee_Column_FullName")]
        public string EmployeeFullName { get; set; }
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }
        public int? Genre { get; set; }
        public int? PhoneNetwork { get; set; }
        public string StrInGroupId { get; set; }
    }
}