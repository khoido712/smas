﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class AssignListViewModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeCode { get; set; }
        public string AccountName { get; set; }
        public string EmployeeName { get; set; }
        public string SchoolFacultyName { get; set; }
        public List<int> lstAssignedGroupId { get; set; }
        public string strAssignedGroup { get; set; }
        public bool isCheck { get; set; }
    }
}