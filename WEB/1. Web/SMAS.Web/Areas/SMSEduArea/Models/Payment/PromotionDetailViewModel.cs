﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class PromotionDetailViewModel
    {
        public int PromotionPackageDetailID { get; set; }
        public int PromotionPackageID { get; set; }

        [ResourceDisplayName("Promotion_Content_Label")]
        public string PromotionContent { get; set; }

        [ResourceDisplayName("Promotion_Time_Label")]
        public string PromotionTime { get; set; }

        public DateTime? FromPromotionDate { get; set; }
        public DateTime? ToPromotionDate { get; set; }
    }
}