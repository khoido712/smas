﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class TopupViewModel
    {
        /// <summary>
        /// Mã thẻ
        /// </summary>
        [ResourceDisplayName("PinCard")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PinCard_Is_Required_Field")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PinCard_Length_Is_Invalid")]
        [MaxLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PinCard_Length_Is_Invalid")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PinCard_Is_Not_Match_Pattern")]
        public string PinCard { get; set; }

        /// <summary>
        /// So seri của thẻ
        /// </summary>
        [ResourceDisplayName("CardSerial")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardSerial_Is_Required_Field")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardSerial_Length_Is_Invalid")]
        [MaxLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardSerial_Length_Is_Invalid")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardSerial_Is_Not_Match_Pattern")]
        public string CardSerial { get; set; }

        /// <summary>
        /// Mã xác thực
        /// </summary>
        [ResourceDisplayName("ConfirmationCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ConfirmationCode_Is_Required_Field")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ConfirmationCode_Length_Is_Invalid")]
        [MaxLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ConfirmationCode_Length_Is_Invalid")]
        public string ConfirmationCode { get; set; }
    }
}