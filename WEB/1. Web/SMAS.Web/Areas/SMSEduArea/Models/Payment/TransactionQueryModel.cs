﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class TransactionQueryModel
    {
        /// <summary>
        /// Tim kiem bat dau tu ngay...
        /// </summary>
        [ResourceDisplayName("Message_Label_FromDate")]
        [DataType(DataType.Date, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "Invalid_Date", ErrorMessageResourceName = "Invalid_Date")]
        public DateTime? fromdate { get; set; }

        /// <summary>
        /// ...den ngay
        /// </summary>
        [ResourceDisplayName("Message_Label_ToDate")]
        [DataType(DataType.Date, ErrorMessageResourceType = typeof(Resource), ErrorMessage = "Invalid_Date", ErrorMessageResourceName = "Invalid_Date")]
        public DateTime? todate { get; set; }

        /// <summary>
        /// Chi so trang hien thi
        /// </summary>
        public int page { get; set; }

        /// <summary>
        /// Kich thuoc trang hien thi
        /// </summary>
        public int pagesize { get; set; }

        /// <summary>
        /// Thư tu săp xep
        /// </summary>
        public string orderby { get; set; }

        public int transTypeID { get; set; }
    }
}