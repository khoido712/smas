﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class EWalletInfoViewModel
    {
        public int EWalletID { get; set; }

        [ResourceDisplayName("Balance_Of_EWallet")]
        public decimal Balance { get; set; }

        [ResourceDisplayName("SendSMSToTeacher_Label_NumberOfPromotionInternalSMS")]
        public int NumOfPromotionInternalSMS { get; set; }
        public string ContentPromotion { get; set; }
    }
}