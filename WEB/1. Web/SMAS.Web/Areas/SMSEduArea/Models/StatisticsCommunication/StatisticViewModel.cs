﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.StatisticsCommunication
{
    public class StatisticViewModel
    {
        public int? totalSMS { get; set; }
        public int? System { get; set; }
        public int? TeacherSMS { get; set; }
        public int? ParentSMS { get; set; }
        public int? Exchange { get; set; }
        public int? HealthDate { get; set; }
        public int? TrainingDate { get; set; }
        public int? ActivitiDate { get; set; }
        public int? DayMark { get; set; }
        public int? WeekMark { get; set; }
        public int? MonthMark { get; set; }
        public int? GrowthMonth { get; set; }
        public int? PeriodicHealth { get; set; }
        public int? SemesterResult { get; set; }
        public int? TypeHistory { get; set; }
        public int? ContentCount { get; set; }
    }
}