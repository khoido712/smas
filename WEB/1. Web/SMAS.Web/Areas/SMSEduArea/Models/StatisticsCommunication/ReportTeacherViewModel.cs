﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.StatisticsCommunication
{
    public class ReportTeacherViewModel
    {
        /// <summary>
        /// Tong so tin nhan
        /// </summary>
        public int? TotalSMS { get; set; }
        /// <summary>
        /// Nhom lien lac
        /// </summary>
        public string ContactGroupName { get; set; }
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Teacher Name
        /// </summary>
        public string TeacherSenderName { get; set; }

        public string ShortName { get; set; }
    }
}