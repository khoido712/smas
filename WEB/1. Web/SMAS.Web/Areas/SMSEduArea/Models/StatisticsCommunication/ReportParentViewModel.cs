﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.StatisticsCommunication
{
    public class ReportParentViewModel
    {
        public string PupilFileName { get; set; }
        public string ClassName { get; set; }
        public string ContractorName { get; set; }
        public string PhoneMainContract { get; set; }
        public int TotalActive { get; set; }
        public int TotalManual { get; set; }
        public string ShortName { get; set; }

        public int EducationLevel { get; set; }

        public int OrderNumber { get; set; }

        public int OrderInClass { get; set; }
    }
}