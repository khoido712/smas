﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SchoolInfoConfigViewModel
    {
        public bool AllMark { get; set; }

        public bool SearchHKI { get; set; }

        public bool SearchHKII { get; set; }
        public bool SearchLock { get; set; }
        public bool ShowTeacherMobile { get; set; }

        public bool RegisterSLL { get; set; }
        public bool AllowSp2Online { get; set; }

        public short PaymentType { get; set; }

        public bool AllowSendSMSToHeadTeacher { get; set; }

        public bool AllowSendUnicodeMessage { get; set; }

        public string Prefix { get; set; }

        public int NameDisplay { get; set; }
    }
}