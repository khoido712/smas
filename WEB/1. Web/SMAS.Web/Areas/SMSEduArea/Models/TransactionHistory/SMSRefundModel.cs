﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SMSRefundModel
    {
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int Semester { get; set; }
        public int? SMSNum { get; set; }
        public string Content { get; set; }
        public bool IsChecked { get; set; }
    }
}