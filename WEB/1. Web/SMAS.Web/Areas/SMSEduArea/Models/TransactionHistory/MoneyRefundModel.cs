﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class MoneyRefundModel
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public int UnitType { get; set; }
        public string ProvinceName { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Money { get; set; }
        public string Content { get; set; }
        public int EwalletID { get; set; }
    }
}