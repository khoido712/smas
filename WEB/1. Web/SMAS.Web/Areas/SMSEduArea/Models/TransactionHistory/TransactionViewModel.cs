﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class TransactionViewModel
    {
        public Guid AffectedHistoryID { get; set; }
        public int UnitType { get; set; }
        public int UnitID { get; set; }
        public int AffectedType { get; set; }
        public string StrAffectedType
        {
            get
            {
                return AffectedType == 1 ? "Hoàn tiền" : "Chuyển tiền";
            }
        }
        public string CalUnit { get; set; }
        public decimal Quantity { get; set; }
        public string StrQuantity
        {
            get
            {
                return Quantity.ToString("#,###");
            }
        }
        public string Content { get; set; }
        public System.DateTime AffectedTime { get; set; }
        public string StrAffectedTime
        {
            get
            {
                return AffectedTime.ToString("HH:mm dd/MM/yyyy");
            }
        }
        public string UnitName { get; set; }
    }
}