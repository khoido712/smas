﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class TransferMoneyModel
    {
        public long EwalletTransactionID { get; set; }
        public string TransactionDate { get; set; }
        public string Content { get; set; }
        public int Amount { get; set; }
        public string Money { get; set; }
        public bool IsChecked { get; set; }
    }
}