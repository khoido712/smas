﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SendSMSToSuperVisingDeptViewModel
    {
        public int SuperVisingDeptID { get; set; }

    
        /// <summary>
        /// danh sach nguoi nhan da duoc check
        /// </summary>
        public string ArrIsCheckReceiverID { get; set; }

        /// <summary>
        /// trang thai check all
        /// </summary>
        public bool CheckAll { get; set; }

        /// <summary>
        /// noi dung tin nhan
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// tin nhan co dau
        /// </summary>
        public bool AllowSignMessage { get; set; }

        /// <summary>
        /// danh sach search object
        /// </summary>
        public List<SearchObject> LstSearchObject { get; set; }

        public SendSMSToSuperVisingDeptViewModel()
        {
            this.ArrIsCheckReceiverID = string.Empty;
            this.CheckAll = true;
            this.Content = string.Empty;
            this.LstSearchObject = new List<SearchObject>();
        }

        public class SearchObject
        {
            /// <summary>
            /// thu tu uu tien key search
            /// </summary>
            public int OrderID { get; set; }

            /// <summary>
            /// tu khoa search
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// trang thai check all
            /// </summary>
            public bool CheckAll { get; set; }

            /// <summary>
            /// trang thai check all tat ca phan tu chua load
            /// </summary>
            public bool CheckAllNotLoad { get; set; }

            /// <summary>
            /// tong so tin nhan cua key search
            /// </summary>
            public int Total { get; set; }

            /// <summary>
            /// so phan tu duoc load tren client
            /// </summary>
            public int TotalLoad { get; set; }

            /// <summary>
            /// trang thai co xay ra su kien check all doi voi key word         
            /// </summary>
            public bool HasCheckAll { get; set; }
        }
    }   
}