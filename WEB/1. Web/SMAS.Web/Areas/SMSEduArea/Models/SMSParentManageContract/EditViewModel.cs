﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class EditViewModel
    {
        public int PupilID { get; set; }
        public int SMSParentContractDetailID { get; set; }
        public int SMSParentContractID { get; set; }
        public string Subscriber { get; set; }
        public string HfSubscriber { get; set; }
        public short SubscriberType { get; set; }
        public int Semester1 { get; set; }
        public int Semester2 { get; set; }
        public int Semester3 { get; set; }
        public short SubscriptionStatus { get; set; }
        public bool IsActiveSubscriber { get; set; }
        public int Year { get; set; }
        public int Money { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> ServicePackageID { get; set; }
        public string Relationship { get; set; }
        public string ReceiverName { get; set; }
        public int? MaxSMS { get; set; }
        public decimal? TotalMoney { get; set; }
        public int UsedSemester1 { get; set; }
        public int UsedSemester2 { get; set; }
        public string PupilName { get; set; }
        public string RegistrationType { get; set; }
        public int StatusPupil { get; set; }
        public DateTime CreateTime { get; set; }
        public bool LimitChangeFirstSemester { get; set; }
        public decimal? Price { get; set; }

        public bool ExpiryDate { get; set; }

        public string ServiceCode { get; set; }

        public string ListServicePackage { get; set; }
    }
}