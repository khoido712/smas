﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class InsertAddNewViewModel
    {
        public string PupilName { get; set; }
        public string RelationshipAdd { get; set; }
        public string ReceiverName { get; set; }
        public string PhoneReceiver { get; set; }
        public int ServicesPackageAdd { get; set; }
        public int Semester1 { get; set; }
        public int Semester2 { get; set; }
        public int Semester3 { get; set; }
        public int SMSParentContactID { get; set; }
        public decimal Money { get; set; }
        public int EwalletID { get; set; }
    }
}