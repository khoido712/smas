﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SchoolAppliedViewModel
    {
        public int? ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int? DistrictID { get; set; }
        public string DistrictName { get; set; }
        public string Account { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public bool isEnablechkApp { get; set; }
        public bool isEnablechkNApp { get; set; }
        public long PromotionDetailID { get; set; }
        public bool HasbeenApplied { get; set; }
    }
}