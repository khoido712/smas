﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class ConsumptionModel
    {
        /// <summary>
        /// Tim kiem bat dau tu ngay...
        /// </summary>
        [ResourceDisplayName("Message_Label_FromDate")]
        public DateTime fromdate { get; set; }

        /// <summary>
        /// ...den ngay
        /// </summary>
        [ResourceDisplayName("Message_Label_ToDate")]
        public DateTime todate { get; set; }
    }
}