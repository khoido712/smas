﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SMSConfigCreateViewModel
    {
        public int Id { get; set; }

        public string SMSTypeName { get; set; }

        public bool IsLocked { get; set; }

        public bool ForVNEN { get; set; }

        public int PeriodType { get; set; }

        public string PeriodName
        {
            get
            {
                switch (PeriodType)
                {
                    case 1:
                        return "Ngày";
                    case 2:
                        return "Tuần";
                    case 3:
                        return "Tháng";
                    case 4:
                        return "Học kỳ";
                    case 5:
                        return "Mở rộng";
                    default:
                        return string.Empty;

                }
            }
        }

        public string Template { get; set; }
    }
}