﻿using SMAS.Web.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SMSTypeViewModel
    {
        public string Template { get; set; }

        public DateTime? SendTime { get; set; }
    }
    /// <summary>
    /// SMS Type
    /// </summary>
    public class SMSTypeEnum
    {
        public const string TNTD = GlobalConstantsEdu.HISTORYSMS_TO_COMMUNICATION_TYPECODE;//x
        public const string TNDD = GlobalConstantsEdu.HISTORYSMS_TO_PERIODSMS_TYPECODE;//x
        public const string TNDN = GlobalConstantsEdu.HISTORYSMS_TO_DAYMARK_TYPECODE;//x
        public const string TNDT = GlobalConstantsEdu.HISTORYSMS_TO_WEEKMARK_TYPECODE;//x
        public const string TNRL = GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_TYPECODE;//x
        public const string TNDTH = GlobalConstantsEdu.HISTORYSMS_TO_MONTHSMS_TYPECODE;//x
        public const string TNHKI = GlobalConstantsEdu.HISTORYSMS_TO_SEMESTERSMS_TYPECODE;//x
        public const string TNBB = GlobalConstantsEdu.HISTORYSMS_TO_LECTUREREGISTER_TYPECODE;//x
        public const string TNTKB = GlobalConstantsEdu.HISTORYSMS_TO_CALENDAR_TYPECODE;//x
        public const string TNRLT = GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_WEEK_TYPECODE;//x
        public const string TNRLTH = GlobalConstantsEdu.HISTORYSMS_TO_TRAINING_MONTH_TYPECODE;//x
        public const string TNDTHK = GlobalConstantsEdu.HISTORYSMS_TO_SEMESTER;// tin nhan diem thi hoc ky
        public const string NXTVNEN = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_VNEN_COMMENT_MONTH_TYPECODE;
    }
}