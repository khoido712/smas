﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.SendSMSToParentModel
{
    public class SMSDataModel
    {
        public int EducationlevelID { get; set; }
        public List<string> Contents { get; set; }

        public List<string> ShortContents { get; set; }

        public List<int> PupilIDs { get; set; }

        public bool isOneClass { get; set; }

        public int TypeID { get; set; }

        public int ClassID { get; set; }

        public string SMSPrefix { get; set; }

        /// <summary>
        /// gửi toàn khối
        /// </summary>
        public bool? IsAllLevel { get; set; }

        /// <summary>
        /// gửi toàn lớp
        /// </summary>
        public bool? IsAllClass { get; set; }

        public bool? IsAllSchool { get; set; }

        /// <summary>
        /// Dành cho các loại tin nhắn chuyên cần.
        /// Đánh dấu có gửi tin nhắn cho các học sinh không có vi phạm không.
        /// </summary>
        public bool? guiTinNhanDenCacHocSinhKhongViPham { get; set; }

        /// <summary>
        /// Dành cho các loại tin nhắn chuyên cần.
        /// Nội dung tin nhắn gửi cho các học sinh không vi phạm.s
        /// </summary>
        public String noiDungTinNhanGuiCacHocSinhKhongViPham { get; set; }

        public bool IsSendSMSToEducationLevelID { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public bool AllowSignMessage { get; set; }

        public bool IsTimerUsed { get; set; }

        public bool IsCustom { get; set; }

        public bool IsAllGrid { get; set; }

        public bool RemoveSpecialChar { get; set; }

        public string SendDate { get; set; }

        public string SendTime { get; set; }

        public string TimerConfigName { get; set; }

        public Guid TimerConfigID { get; set; }

        public int ComboTimeMarklevel { get; set; }

        public int ComboTimeMark { get; set; }

        public int ComboExamID { get; set; }

        public int MonthIDLevel { get; set; }

        public int? Year { get; set; }

        public int Semester { get; set; }

        public bool? IsSendImport { get; set; }

        public string ImportXSSAnti { get; set; }

        public string MonthMNEdu { get; set; }

        public int ComboWeekEdu { get; set; }

        public string DateMN { get; set; }

        public string SMSToNotViolateContent { get; set; }

        public bool SendSMSToNotViolatePupil { get; set; }
        public string strClassID { get; set; }
        public string strPupilID { get; set; }
        public SMSDataModel()
        {
            SMSPrefix = string.Empty;
            Contents = new List<string>();
            ShortContents = new List<string>();
            PupilIDs = new List<int>();
            TypeID = 0;
            ClassID = 0;
            IsAllLevel = false;
            IsAllClass = false;
            EducationlevelID = 0;
        }
        public bool isLoadSMS { get; set; }
    }
    public class SchoolNode
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<LevelNode> ListLevel { get; set; }
    }

    public class LevelNode
    {
        public int LevelID { get; set; }
        public string LevelName { get; set; }
        public List<ClassNode> ListClass { get; set; }
    }

    public class ClassNode
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
    }

    public class MarkTimeConfig
    {
        public int MarkTimeConfigID { get; set; }
        public string MarkTimeName { get; set; }
        public bool? IsNearest { get; set; }
    }
}