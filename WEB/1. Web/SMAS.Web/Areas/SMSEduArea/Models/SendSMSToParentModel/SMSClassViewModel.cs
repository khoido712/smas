﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.SendSMSToParentModel
{
    public class SMSClassViewModel
    {
        public int ClassID { get; set; }
        [ResourceDisplayName("Lớp")]
        public string ClassName { get; set; }
        public int HeadTeacherID { get; set; }
        [ResourceDisplayName("Giáo viên chủ nhiệm")]
        public string HeadTeacherName { get; set; }
        [ResourceDisplayName("Số PHHS đăng ký")]
        public int RegisterNumber { get; set; }
        public int RegisterNumberStatus { get; set; }
        public int TotalSMS { get; set; }
        public int SendTotal { get; set; }
        public int SMSRemain { get; set; }
    }
}