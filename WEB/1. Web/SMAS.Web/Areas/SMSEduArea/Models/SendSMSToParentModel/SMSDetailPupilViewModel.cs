﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.SendSMSToParentModel
{
    public class SMSDetailPupilViewModel
    {
        public int PupilID { get; set; }
        public string FullName { get; set; }
        public string SMSTemplate { get; set; }
        public string Content { get; set; }
        public int TotalSend { get; set; }
        public int TotalSMS { get; set; }
        public string Subcriber { get; set; }
        public int Status { get; set; }
        public string RemainSMSTxt { get; set; }
        public string SMSLength { get; set; }
    }
}