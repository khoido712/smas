﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models.SendSMSToParentModel
{
    public class ContentErrorViewModel
    {
        public string PupilCode { get; set; }

        public string PupilName { get; set; }

        public string Content { get; set; }
    }
}