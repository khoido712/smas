﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SendSMSToschoolViewModel
    {

        public int EducationGrade { get; set; }

        /// <summary>
        /// danh sach nguoi nhan da duoc check
        /// </summary>
        public string ArrIsCheckReceiverID { get; set; }

        /// <summary>
        /// trang thai check all
        /// </summary>
        public bool CheckAll { get; set; }

        /// <summary>
        /// noi dung tin nhan
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// tin nhan co dau
        /// </summary>
        public bool AllowSignMessage { get; set; }

        public SendSMSToschoolViewModel()
        {
            this.ArrIsCheckReceiverID = string.Empty;
            this.CheckAll = true;
            this.Content = string.Empty;
        }

    }
}