﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SchoolApplyModel
    {
        public int SchoolID { get; set; }

        public string SchoolCode { get; set; }

        public string SchoolName { get; set; }

        public int ProvinceID { get; set; }

        public int DistrictID { get; set; }

        public string ProvinceName { get; set; }

        public string DistrictName { get; set; }

        public string UserName { get; set; }

        public bool IsApply { get; set; }
    }
}