﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class SearchViewModel
    {
        //Nam hoc
        [UIHint("Combobox")]
        public int AcademicYearID { get; set; }

        //Goi cuoc
        [UIHint("Combobox")]
        public int PackageID { get; set; }
    }
}