﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea.Models
{
    public class ProvinceApplyModel
    {
        public int Province1ID { get; set; }
        public string Province1Name { get; set; }
        public bool Province1IsChecked { get; set; }
        public int Province2ID { get; set; }
        public string Province2Name { get; set; }
        public bool Province2IsChecked { get; set; }
    }
}