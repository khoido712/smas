﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea
{
    public class StatisticsCommunicationConstants
    {
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_PUPIL = "LIST_PUPIL";
        public const string LIST_SMSTEACHER = "LIST_SMSTEACHER";
        public const string LIST_SMSPARENT = "LIST_SMSPARENT";
        public const string LIST_CONTACTGROUP = "LIST_CONTACTGROUP";
        public const string LIST_TEACHER = "LIST_TEACHER";
        public const string LIST_SMS_TYPE = "LIST_SMS_TYPE";
        public const string Page = "page";
        public const string Total = "total";
        public const int FirstPage = 1;
        public const string List_Count = "list_count";
        public const string Visible = "Visible";
        public const string LST_RECEIVER = "lst receiver";
        public const string SENDER_GROUP = "sender Group of view";
        public const string NUM_TEACHER_SMS = "num of teacher SMS";
        public const int PageSize = 20;
        public const int TYPE_BAOBAI = 29;// ID bản tin báo bài
        public const int PageSizePopUp = 10;
        public const string CURRENT_TEACHER_NAME = "CURRENT_TEACHER_NAME";
    }
}