﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SMSEduArea
{
    public class SendSMSToParentConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";

        public const string LIST_CLASS = "LIST_CLASS";

        public const string LIST_SEMESTER = "LIST_SEMESTER";

        public const string LIST_PUPIL = "LIST_PUPILS";

        public const string LIST_PUPIL_HAS_SMS = "LIST_PUPIL_HAS_SMS";

        public const string LIST_IMPORT_ALL = "LIST_IMPORT_ALL";

        public const string LIST_PUPIL_CONTRACT = "LIST_PUPIL_CONTRACT";

        public const string LIST_CONTENT_ERROR = "LIST_CONTENT_ERROR";

        public const string IS_SEND_ALL = "IS_SEND_ALL";

        public const string IS_SEND_CUSTOM = "IS_SEND_CUSTOM";

        public const string IS_SAVE_TIMER = "IS_SAVE_TIMER";

        public const string IS_SAVE_TIMER_CLASS = "IS_SAVE_TIMER_CLASS";

        public const string CLASS_NAME = "CLASS_NAME";

        public const string CLASS_ID = "CLASS_ID";

        public const string STR_CLASS_ID = "strClassID";

        public const string CLASS_ID_SELECT = "class id select default class";

        public const string LIST_SMS_TYPE = "LIST_SMS_TYPE";

        public const string LIST_CUSTOM_TYPE = "LIST_CUSTOM_TYPE";

        public const string LIST_COMBOBOX_SELECTOR = "LIST_COMBOBOX_SELECTOR";

        public const string LIST_TIMER_CONFIG = "LIST_TIMER_CONFIG";

        public const string TIMER_CONFIG_ID = "TIMER_CONFIG_ID";

        public const string SUM_OF_PUPIL = "SUM_OF_PUPIL";

        public const string IS_LOCK_COMMUNICATE_TYPE = "communicate type lock is true";

        public const string IS_ALLOW_SEND_UNICODE_MSG = "allow send unicode message";

        public const string PREFIX_NAME_OF_SCHOOL = "GET_PREFIX_NAME_OF_SCHOOL";

        public const string IS_CHECKED_SEND_UNICODE_MSG = "checked send unicode message";

        public const string SMAS_CONTROL_NAME = "#smsedu/sendsmstoparent#";

        public const string NO_SMS_MESSAGE = "Không có tin mới.";
        public const string UNICODE_MESSAGE_NOTES = "(67 ký tự/SMS. Chỉ gửi được tin nhắn có dấu cho các mạng Viettel, Vina, Mobi.)";

        public const string CHOISE_SMS_MESSAGE = "Vui lòng chọn bản tin.";

        public const string NO_SUPPORT_UNICODE = "Không hỗ trợ gửi tin nhắn có dấu.";
        public const string IS_PAGING = "isPaging";

        public const string SMSLENGTH_TEMPLATE_ZERO = "0/0";

        public const string SMSLENGTH_FORMAT = "{0}/{1}";

        public const string SMSFORMAT_SEND = "{0}{1}";



        public const string SMSFORMAT_ABSENT_P = "-Nghỉ có phép: {0}";

        public const string SMSFORMAT_ABSENT_P_SEMESTER = "Nghỉ có phép: {0}";

        public const string SMSFORMAT_ABSENT_K = "-Nghỉ không phép: {0}";

        public const string SMSFORMAT_ABSENT_K_SEMESTER = "Nghỉ không phép: {0}";



        public const string SCHOOLNAME_FORMAT = "{0}";

        public const string CONST_MIDDLE_SEMESTER1 = "MiddleSemester1";

        public const string CONST_GK = "GK:";

        public const string CONST_SEMESTER1 = "Semester1";

        public const string CONST_CK = "CK:";

        public const string CONST_MIDDLE_SEMESTER2 = "MiddleSemester2";

        public const string CONST_SEMESTER2 = "Semester2";

        public const string CONST_DASH_SPACE = "- ";

        public const string CONST_J1 = "J1";

        public const string CONST_NX = "NX";

        public const string CONST_D = "D";

        public const string CONST_CD = "CD";

        public const string CONST_J2 = "J2";

        public const string CONST_HK = "HK:";

        public const string CONST_P_LOWER = "p";

        public const string CONST_K_LOWER = "k";

        public const string CONTENT_FORMAT = "{0}{1}{2}{3}";

        public const string CONTENT_FORMAT_MARK = "{0}";

        public const string CONST_M = "M:";

        public const string CONST_15P = "15P:";

        public const string CONST_1T = "1T:";

        public const string CONST_SEMESTER_TEST = "SemesterTest";

        public const string CONST_KTHK = "KTHK:";

        public const string CONST_AVG_SEMESTER = "AvgSemester";

        public const string CONST_AVG_OF_YEAR = "AvgOfYear";

        public const string CONST_SEMESTER = "Semester";

        public const string CONST_COMMA_SPACE = ", ";

        public const string SEMESTER_FORMAT_TWO = "{0}{1}";

        public const string CONST_DASH_CN = "II/CN";

        public const string CONST_SEMESTER1_I = "I";

        public const string CONST_SEMESTER2_II = "II";

        public const string CONST_CAPACITY = "Capacity";

        public const string CONST_CAPACITY2 = "Capacity2";

        public const string CONST_BOTH = "Both";

        public const string CONST_COLON_SPACE = ": ";

        public const string CONST_SEMICOLON_SPACE = "; ";

        public const string CONST_DOT_SPACE = ". ";

        public const string CONST_VPKL = "VPKL: ";

        public const string CONST_LOG_TEXT1 = "Gửi tin nhắn trao đổi đến ";

        public const string CONST_LOG_TEXT2 = " học sinh.";

        public const string CONST_LOG_TEXT3 = "Xuất file thông tin tin nhắn.";

        public const string CONST_LOG_TEXT4 = "Gửi tin nhắn đến ";

        public const string CONST_LOG_TEXT5 = "Gửi tin nhắn đến {0} học sinh.";

        public const string CONST_TITTLE_EXCEL = "DANH SÁCH PHỤ HUYNH HỌC SINH NHẬN TIN NHẮN";

        public const string CONST_STT = "STT";

        public const string CONST_PUPIL_CODE = "Mã học sinh";

        public const string CONST_PUPIL_NAME = "Họ tên HS";

        public const string CONST_CLASS = "Lớp";

        public const string CONST_PHONE = "Số điện thoại";

        public const string CONST_CONTENT = "Nội dung";

        public const string CONST_SUBSCRIBER = "Số SMS/Thuê bao";

        public const string CONST_STR_L_PARENTHESIS = "(";

        public const string CONST_STR_R_PARENTHESIS = ")";

        public const string CONST_STR_R_PARENTHESIS_COMMA_SPACE = "), ";

        public const string CONST_STR_DAY_MARK = "Điểm ngày";

        public const string CONST_STR_WEEK_MARK = "Điểm tuần";

        public const string CONST_STR_MONTH_MARK = "Điểm tháng";

        public const string CONST_STR_TRAINING_INFO = "Thông tin rèn luyện";

        public const string CONST_STR_LECTURE_INFO = "Thông tin báo bài";

        public const string CONST_STR_SENDED = "Đã gửi";

        public const string CONST_STR_UPDATE_SMS_STATUS = "Cập nhật trạng thái gửi tin nhắn";

        public const string CONST_STR_UPDATE_SMS_STATUS_TRAINING = "Cập nhật trạng thái gửi tin nhắn rèn luyện";

        public const string CONST_STR_UPDATE_SMS_STATUS_LECTURE = "Cập nhật học sinh đã gửi tin nhắn báo bài";

        public const string CONST_STR_ERROR_SEND_SMS = "Có lỗi trong quá trình gửi tin";

        public const string REPORTNAME = "BM_Gui tin nhan.xls";

        public const string SMS_BREAK_LINE = "\n";

        public const string BREAK_LINE = "</br>";

        public const string ISSENT = "Issent";

        public const string CONST_FILE_STREAM = "application/octet-stream";

        public const string LIST_MONTH = "listMonth";

        public const string SMSFORMAT_SEND_PRIMARY = "{0}{1}";

        public const string SEMESTER_DEFAULT = "semesterdefault";

        public const string MESSAGE_BUDGET_TOTAL = "total of message budget in a class";
        public const string SENT_MESSAGE_TOTAL = "total of message in a class sent";
        public const string CURRENT_ACADEMIC_YEAR = "current academic year of school";
        public const string ACADEMIC_YEAR = "AcademicYear";
        public const string GRADE = "Grade";
        public const string IS_NEW_SCHOOL_MODEL = "IsNewSchoolModel";

        public const int TIMER_CONFIG_SCHOOL = 1;
        public const int TIMER_CONFIG_EDU = 2;
        public const int TIMER_CONFIG_CLASS = 3;

        public const int TIMER_CONFIG_STATUS_ACTIVE = 1;
        public const int TIMER_CONFIG_STATUS_SENT = 2;
        public const int TIMER_CONFIG_STATUS_CANCEL = 3;
        public const int PAGE_SIZE = 15;

        #region TEMPLATE FORMAT
        public const string SMSFORMAT_DAYMARK = "{0}thông báo điểm trong ngày {1} của em ";
        public const string SMSFORMAT_WEEKMARK = "{0}thông báo điểm tuần từ {1}-{2} của em ";
        public const string SMSFORMAT_MONTHMARK = "{0}thông báo điểm tháng & điểm danh từ {1}-{2} của em ";
        public const string SMSFORMAT_RESULT = "{0}thông báo kết quả {1} của em ";
        public const string SMSFORMAT_MARKTIME = "{0}thông báo kết quả {1}-{2} của em ";
        public const string SMSFORMAT_CALENDAR = "{0}thông báo thời khóa biểu của em ";
        public const string SMSFORMAT_TRAINING_INFO = "{0}thông báo chuyên cần ngày {1} của em ";
        public const string SMSFORMAT_TRAINING_WEEK_INFO = "{0}thông báo chuyên cần từ ngày {1}-{2} của em ";
        public const string SMSFORMAT_TRAINING_MONTH_INFO = "{0}thông báo chuyên cần từ ngày {1}-{2} của em ";
        public const string SMSFORMAT_TRAINING_EXPEND_INFO = "{0}thông báo chuyên cần từ ngày {1}-{2} của em ";
        public const string SMSFORMAT_EXAM_SCHEDULE = "{0}thông báo lịch thi {1} ";
        public const string SMSFORMAT_EXAM_RESULT = "{0}thông báo kết quả kỳ thi {1} em ";
        public const string SMSFORMAT_MARK_SEMESTER = "{0}thông báo kết quả thi {1} của em ";

        /// <summary>
        /// Nhận xét tháng
        /// {0} tiền tố 
        /// {1} thứ tự tháng "nhất, hai, ba...."
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENTS_MONTH = "{0}thông báo nhận xét tháng thứ {1} em ";

        /// <summary>
        /// nhận xét cuối kỳ
        /// {0} tiền tố 
        /// {1} học kỳ "1,2"
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENTS_ENDING = "{0}thông báo nhận xét học kỳ {1} em ";

        /// <summary>
        /// nhận xét kết quả học kỳ
        /// {0} tiền tố 
        /// {1} học kỳ "1,2"
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENTS_RESULT_SEMESTER = "{0}thông báo kết quả học kỳ {1} em ";
        #endregion

        #region VNEN
        /// <summary>
        /// Nhận xét tháng
        /// {0} tiền tố 
        /// {1} thứ tự tháng "nhất, hai, ba...."
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENT_MONTH_VNEN = "{0}thông báo nhận xét tháng {1} của em ";

        /// <summary>
        /// nhận xét cuối kỳ
        /// {0} tiền tố 
        /// {1} học kỳ "1,2"
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENT_ENDING_VNEN = "{0}thông báo nhận xét cuối kỳ {1} của em ";

        /// <summary>
        /// nhận xét kết quả học kỳ
        /// {0} tiền tố 
        /// {1} học kỳ "1,2"
        /// {2} họ và tên học sinh.
        /// </summary>
        public const string TEMPLATE_COMMENT_RESULT_SEMESTER_VNEN = "{0}thông báo kết quả học kỳ {1} của em ";
        #endregion

        #region TT22 Tieu hoc
        public const string TEMPLATE_TT22_RESULT_END_SEMESTER = "{0}thông báo kết quả cuối {1} của em ";
        public const string TEMPLATE_TT22_RESULT_MID_SEMESTER = "{0}thông báo kết quả giữa học kỳ {1} của em ";
        public const string TEMPLATE_TT22_COMMENT_SEMESTER = "{0}thông báo kết quả nhận xét cuối học kỳ {1} của em ";
        #endregion

        #region Mam non
        public const string TEMPLATE_MN_BNTU = "{0}thông báo em ";
        public const string TEMPLATE_MN_TDN = "{0}thông báo thực đơn ngày {1} của em ";
        public const string TEMPLATE_MN_TDT = "{0}thông báo thực đơn từ ngày {1} - {2} của em ";
        public const string TEMPLATE_MN_CDSK = "{0}thông báo kết quả cân đo sức khỏe đợt {1} của em ";
        public const string TEMPLATE_MN_DGSPT = "{0}thông báo kết quả đánh giá sự phát triển của em ";
        public const string TEMPLATE_MN_DGHDN = "{0}thông báo kết quả đánh giá hoạt động ngày {1} của em ";
        #endregion

        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const int PageSize = 10;

        public const string IS_CUSTOM_PREFIX = "IS_CUSTOM_PREFIX";

        public const string LIST_MONTH_MN = "LIST_MONTH_MN";
        public const string LIST_WEEK = "LIST_WEEK";
        public const string DEFAULT_WEEK = "DEFAULT_WEEK";
        public const string TYPE_CODE = "TypeCode";
        public const string TYPE_NAME = "TypeName";
        public const string FROM_DATE = "FromDate";
        public const string TO_DATE = "ToDate";
        public const string IS_TIMMER_CONFIG = "isTimmerConfig";
        public const string IS_NEW_MESSAGE = "isNewMessage";
    }
}