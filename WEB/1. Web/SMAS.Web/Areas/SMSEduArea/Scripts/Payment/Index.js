﻿//CLOSE POPUP PROMOTION DETAIL - ONCLICK BUTTON EVENT
function closePopupPromotionDetail() {
    $('.t-window').find('.t-header').find('.t-close').click();
}

//ONCLOSE POPUP PROMOTION DETAIL CALL CLOSE IFRAME FUNTION - ONCLICK BUTTON CLOSE EVENT
function onClosePopupPromotionDetail() {
    smas.closeDialogIframe();
}

//SHOW PROMOTION DETAIL NEWSEST PROMOTION PACKAGE - ONCLICK DETAIL HREF
function showPromotionDetailPopup() {
    $.ajax({
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        url: '/Payment/EWallet/GetNewestPromotionPackage',
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (data) {
            smas.hideProcessingDialog();
            if (data.PromotionContent != '') {
                $('#PromotionDetail #PromotionContent').html(data.PromotionContent);
                $('#PromotionDetail #PromotionTime').html(data.PromotionTime);
            } else {
                $('#PromotionDetail p').remove();
                $('#PromotionDetail').html('Chưa có thông tin khuyến mãi');
            }
            smas.openDialog('PromotionDetailPopUp', "", {});
            if (smas.isIframe) {
                iFrameUtils.autoResize(true);
            }
        },
        error: function (xmlHttpRequest, status, throwError) {
            smas.hideProcessingDialog();
        }
    });
}

//CHECK FORMAT DATE
function CheckValidate()
{
    var checkVali = true;
    var fromD = $('#fromdate').val().toString();
    var toD = $('#ToDate').val().toString();
    var fDate = parseDate(fromD).getTime();
    var tDate = parseDate(toD).getTime();
    var d = new Date();
    var day = d.getDate().toString();
    var month = (d.getMonth() + 1).toString();
    if (day.length == 1)
    {
        day = '0' + day;
    }
    if (month.length == 1)
    {
        month = '0' + month;
    }
    var nDate = day + '/' + month + '/' + d.getFullYear();
    
    if (fromD == '') {       
        checkVali = true;
    }

    if (toD == '') {
        smas.alert("Ngày tháng không hợp lệ");
        $('#ToDate').val(nDate);
        checkVali = false;
    }

    if (fDate > tDate) {
        smas.alert("Từ ngày phải nhỏ hơn hoặc bằng đến ngày");
        checkVali = false;
    }
    if (isDate(fromD) == 0 && fromD != '')
    {
        smas.alert("Ngày tháng không hợp lệ");
        $('#fromdate').val('');
        $('#ToDate').val(nDate);
        checkVali = false;
    }
    else if (isDate(fromD) == 1) {
        smas.alert("Ngày tháng không hợp lệ");
        $('#fromdate').val('');
        $('#ToDate').val(nDate);
        checkVali = false;
    }

    if (isDate(toD) == 0) {
        smas.alert("Ngày tháng không hợp lệ");
        $('#ToDate').val(nDate);
        checkVali = false;
    }
    else if (isDate(toD) == 1) {
        smas.alert("Ngày tháng không hợp lệ");
        $('#ToDate').val(nDate);
        checkVali = false;
    }
      
    if (checkVali == true)
    {
        $("#frmSearch input[type='submit']").click();
    }
    return checkVali;
}

function isDate(txtDate) {   
    var currVal = txtDate;
    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return 0;

    //Checks for dd/mm/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return 1;
    else if (dtDay < 1 || dtDay > 31)
        return 1;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return 1;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return 1;
    }
    return 2;
}

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1] - 1, mdy[0]);
}

$(document).bind('keypress', function (e) {    
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13)
    { 
        //Enter keycode        
        return CheckValidate();
    }
});