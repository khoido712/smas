﻿$(document).ready(function () {
    var remove = function (list, item) {
        if (!list || list.length <= 0 || !item) {
            return list;
        }
        var index = -1;
        for (var i = 0; i < list.length; i++) {
            if (item.PupilID == list[i].PupilID) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            list.splice(index, 1);
        }
        return list;
    }

    var add = function (list, item) {
        if (!item) {
            return list;
        }
        if (!list || list.length <= 0) {
            list = [];
            list.push(item);
            return list;
        }
        var index = -1;
        for (var i = 0; i < list.length; i++) {
            if (item.PupilID == list[i].PupilID) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            list.push(item);
        }
        return list;
    }
    var registerList = [];
    var cancelList = [];
    var $educationLevelList = $('#education-level-list');
    var $classList = $('#class-list');
    var $rsfSearch = $('#rsf-search');
    var $registerSMSFreeList = $('#RegisterSMSFreeList');
    var $pupilCode = $('#pupil-code');
    var $fullname = $('#pupil-fullname');
    var $refBtnRegister = $('#rsf-btn-register');
    var $rsfBtnCancel = $('#rsf-btn-cancel');
    var $statusList = $('#status-list');
    var $rsfBtnExcel = $('#rsf-btn-excel');
    var searchObj = {
        Size: 15,
        Page: 1,
        EducationLevel: $educationLevelList.val(),
        Class: $classList.val(),
        PupilCode: $pupilCode.val(),
        Fullname: $fullname.val(),
        Status: $statusList.val()
    };

    var autoCheckAllRegister = function() {
        var checked = true;
        var $arr = $('.rsf-register');
        for (var i = 0; i < $arr.length; i++) {
            if ($($arr[i]).is(":checked") == false) {
                checked = false;
                break;
            }
        }
        if (checked) {
            $('#rsf-register-check-all').attr('checked', true);
        }
    }

    var autoCheckAllCancel = function () {
        var checked = true;
        var $arr = $('.rsf-cacel');
        for (var i = 0; i < $arr.length; i++) {
            if ($($arr[i]).is(":checked") == false) {
                checked = false;
                break;
            }
        }
        if (checked) {
            $('#rsf-cancel-check-all').attr('checked', true);
        }
    }

    var buttonShowHide = function () {
        if (_count > 0) {
            $('.vt-button-fn').show();
        } else {
            $('.vt-button-fn').hide();
        }
    }

    var search = function() {
        $.ajax({
            url: "/SMSEduArea/SMSFreeRegistration/Search",
            type: 'get',
            xhrFields: {
                withCredentials: true
            },
            data: searchObj,
            datatype: 'application/json',
            beforeSend: function() {
                smas.showProcessingDialog();
            },
            complete: function() {
                smas.hideProcessingDialog();
            },
            success: function(response) {
                // this code is same shit
                // more ram
                $registerSMSFreeList.html(response);
                buttonShowHide();
                $('.n-total').html(_total);
            },
            error: function(jqXMLHttpRequest, textStatus, errorThrown) {
                smas.alert(jqXMLHttpRequest.responseText);
            }
        });
    };
    var showMessage = function() {
        setTimeout(function() {
            if (_message) {
                smas.alert(_message);
            }
        }, 300);

    }

    // Khi chọn khối thì lấy dữ liệu để đổ vào lớp
    $educationLevelList.bind('change', function (event) {
        if (event.target.value == 0) {
            $classList.html("<option value='0'>"+ _res_all + "</option>");
        } else {
            $.ajax({
                url: "/SMSEduArea/SMSFreeRegistration/GetClassList",
                type: 'get',
                xhrFields: {
                    withCredentials: true
                },
                data: { educationLevelId: event.target.value },
                datatype: 'application/json',
                beforeSend: function () {
                    //smas.showProcessingDialog();
                },
                complete: function () {
                    //smas.hideProcessingDialog();
                },
                success: function (response) {
                    if (response.status == 200) {
                        $classList.html("<option value='0'>"+ _res_all + "</option>");
                        for (var i = 0; i < response.data.length; i++) {
                            $classList.append([
                                "<option value='",
                                    response.data[i].Id,
                                "'>",
                                    response.data[i].Name,
                                "</option>"
                            ].join(''));
                        }
                    } else {
                        $classList.html("<option value='0'>"+ _res_all + "</option>");
                    }
                },
                error: function (jqXMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(jqXMLHttpRequest.responseText);
                }
            });
        }
    });

    // Trang đầu tiên
    $('.t-arrow-first').live('click', function (event) {
        event.preventDefault();
        if (_page <= 1) {
            return false;
        } else {
            searchObj.Page = 1;
            search();
        }
        return false;
    });
    // Trang trước
    $('.t-arrow-prev').live('click', function (event) {
        event.preventDefault();
        if (_page <= 1) {
            return false;
        } else {
            var p = _page - 1;
            if (p <= 0 || p > _total_page) {
                return false;
            }
            searchObj.Page = p;
            search();
        }
        return false;
    });
    // Trang có số
    $('a.t-link').live('click', function(event) {
        event.preventDefault();
        if (!isNaN(event.target.text)) {
            searchObj.Page = event.target.text - 0;
        } else {
            var split = event.target.href.split("page=");
            searchObj.Page = split[split.length - 1] - 0;
        }

        search();
        return false;
    });
    // trang tiep
    $('.t-arrow-next').live('click', function(event) {
        event.preventDefault();
        if (_page >= _total_page) {
            return false;
        } else {
            var p = _page + 1;
            if (p <= 0 || p > _total_page) {
                return false;
            }
            searchObj.Page = p;
            search();
        }
        return false;
    });
    $('.t-arrow-last').live('click', function(event) {
        event.preventDefault();
        if (_page >= _total_page) {
            return false;
        } else {
            searchObj.Page = _total_page;
            search();
        }
        return false;
    });

    // Chọn tất cả học sinh để đăng ký (trong grid)
    $('#rsf-register-check-all').live('click', function (event) {
        if ($('#rsf-register-check-all').is(':checked')) {
            $('.rsf-register').attr('checked', true);
        } else {
            $('.rsf-register').removeAttr('checked');
        }
        $('.rsf-register').trigger('change');
    });

    // Chọn học sinh để đăng ký (trong grid)
    $('.rsf-register').live('change', function (event) {
        if ($(event.target).is(':checked')) {
            registerList = add(registerList, JSON.parse($('#pupil-' + event.target.value).val()));
            autoCheckAllRegister();
        } else {
            registerList = remove(registerList, JSON.parse($('#pupil-' + event.target.value).val()));
            $('#rsf-register-check-all').removeAttr('checked');
        }
    });

    // Chọn tất cả học sinh để hủy đăng ký (trong grid)
    $('#rsf-cancel-check-all').live('click', function (event) {
        if ($('#rsf-cancel-check-all').is(':checked')) {
            $('.rsf-cacel').attr('checked', true);
        } else {
            $('.rsf-cacel').removeAttr('checked');
        }
        $('.rsf-cacel').trigger('change');
    });

    // Khi chọn học sinh để hủy đăng ký (trong grid)
    $('.rsf-cacel').live('change', function(event) {
        if ($(event.target).is(':checked')) {
            cancelList = add(cancelList, JSON.parse($('#pupil-' + event.target.value).val()));
            autoCheckAllCancel();
        } else {
            cancelList = remove(cancelList, JSON.parse($('#pupil-' + event.target.value).val()));
            $('#rsf-cancel-check-all').removeAttr('checked');
        }
    });

    // Ấn nút đăng ký
    $refBtnRegister.bind('click', function (event) {
        var applyAll = $('#rsf-apply-all').is(':checked');
        var token = $('[name=__RequestVerificationToken]').val();
        if (applyAll) {
            searchObj.EducationLevel = $educationLevelList.val();
            searchObj.Class = $classList.val();
            searchObj.PupilCode = $pupilCode.val();
            searchObj.Fullname = $fullname.val();
            searchObj.Status = $statusList.val();
            searchObj.__RequestVerificationToken = token;
            $.ajax({
                url: "/SMSEduArea/SMSFreeRegistration/RegisterAll",
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                data: searchObj,
                datatype: 'application/json',
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                complete: function () {
                    smas.hideProcessingDialog();
                },
                success: function (response) {
                    // this code is same shit
                    // more ram
                    $registerSMSFreeList.html(response);
                    $('#rsf-apply-all').removeAttr('checked');
                    $('.n-total').html(_total);
                    showMessage();
                    buttonShowHide();
                    registerList = [];
                    cancelList = [];
                },
                error: function (jqXMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(jqXMLHttpRequest.responseText);
                }
            });
        }
        else{
            if (!registerList || registerList.length <= 0) {
                smas.alertColor(_res_chon_hs, 'red');
                return;
            }
            $.ajax({
                url: "/SMSEduArea/SMSFreeRegistration/Register",
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                data: { registerListJson: JSON.stringify(registerList), modelJson: JSON.stringify(searchObj), __RequestVerificationToken: token },
                datatype: 'application/json',
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                complete: function () {
                    smas.hideProcessingDialog();
                },
                success: function (response) {
                    // this code is same shit
                    // more ram
                    $registerSMSFreeList.html(response);
                    $('#rsf-apply-all').removeAttr('checked');
                    $('.n-total').html(_total);
                    showMessage();
                    buttonShowHide();
                    registerList = [];
                    cancelList = [];
                },
                error: function (jqXMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(jqXMLHttpRequest.responseText);
                }
            });
        }
    });

    // Ấn nút hủy đăng ký
    $rsfBtnCancel.bind('click', function (event) {
        var applyAll = $('#rsf-apply-all').is(':checked');
        var token = $('[name=__RequestVerificationToken]').val();
        if (applyAll) {
            searchObj.EducationLevel = $educationLevelList.val();
            searchObj.Class = $classList.val();
            searchObj.PupilCode = $pupilCode.val();
            searchObj.Fullname = $fullname.val();
            searchObj.Status = $statusList.val();
            searchObj.__RequestVerificationToken = token;
            $.ajax({
                url: "/SMSEduArea/SMSFreeRegistration/CancelAll",
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                data: searchObj,
                datatype: 'application/json',
                beforeSend: function() {
                    smas.showProcessingDialog();
                },
                complete: function() {
                    smas.hideProcessingDialog();
                },
                success: function(response) {
                    // this code is same shit
                    // more ram
                    $registerSMSFreeList.html(response);
                    $('#rsf-apply-all').removeAttr('checked');
                    $('.n-total').html(_total);
                    showMessage();
                    buttonShowHide();
                    registerList = [];
                    cancelList = [];
                },
                error: function(jqXMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(jqXMLHttpRequest.responseText);
                }
            });
        } else {
            if (!cancelList || cancelList.length <= 0) {
                smas.alertColor(_res_chon_hs_huy, 'red');
                return;
            }
            $.ajax({
                url: "/SMSEduArea/SMSFreeRegistration/Cancel",
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                data: { cancelListJson: JSON.stringify(cancelList), modelJson: JSON.stringify(searchObj), __RequestVerificationToken: token },
                datatype: 'application/json',
                beforeSend: function() {
                    smas.showProcessingDialog();
                },
                complete: function() {
                    smas.hideProcessingDialog();
                },
                success: function(response) {
                    // this code is same shit
                    // more ram
                    $registerSMSFreeList.html(response);
                    $('#rsf-apply-all').removeAttr('checked');
                    $('.n-total').html(_total);
                    showMessage();
                    buttonShowHide();
                    registerList = [];
                    cancelList = [];
                },
                error: function(jqXMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(jqXMLHttpRequest.responseText);
                }
            });
        }
    });

    // Ấn nút tìm kiếm
    $rsfSearch.bind('click', function(event) {
        event.preventDefault();
        searchObj.Page = 1;
        searchObj.EducationLevel = $educationLevelList.val();
        searchObj.Class = $classList.val();
        searchObj.PupilCode = $pupilCode.val().trim();
        searchObj.Fullname = $fullname.val().trim();
        searchObj.Status = $statusList.val();
        $pupilCode.val(searchObj.PupilCode);
        $fullname.val(searchObj.Fullname);
        var url = "/SMSEduArea/SMSFreeRegistration/Export?Size=" + searchObj.Size + "&Page=" + searchObj.Page + "&EducationLevel=" + searchObj.EducationLevel + "&Class=" + searchObj.Class + "&PupilCode=" + searchObj.PupilCode + "&Fullname=" + searchObj.Fullname + "&Status=" + searchObj.Status;
        $rsfBtnExcel.attr('href', url);
        search();
    });

    //$rsfBtnExcel.bind('click', function(event) {
    //    var url = $(event.target).attr("href") + "?Size=" + searchObj.Size + "&Page=" + searchObj.Page + "&EducationLevel=" + searchObj.EducationLevel + "&Class=" + searchObj.Class + "&PupilCode=" + searchObj.PupilCode + "&Fullname=" + searchObj.Fullname + "&Status=" + searchObj.Status;
    //    window.open(url);
    //    return false;
    //});
});

var onDataBinding = function (e) {
}

var LoadComplete = function(e) {
}