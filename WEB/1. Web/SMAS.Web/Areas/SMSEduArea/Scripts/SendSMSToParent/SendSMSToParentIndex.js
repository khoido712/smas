﻿var PREFIX = '';
var BRAND = '';
var numOfSubscriber = 0;
var educationLevelIDSelected = 0;
var isLoadMonth = true;
var isSelectEducation = false;

Number.prototype.padLeft = function (n, str) {
    return Array(n - String(this).length + 1).join(str || '0') + this;
}
$(function () {
    hideAddtionalPanel();
    $('#FromDate').mask("99/99/9999");
    $('#ToDate').mask("99/99/9999");

    PREFIX = stringUtils.HtmlDecode(PREFIX_NAME);
    BRAND = stringUtils.HtmlDecode(BRAND_NAME);
    if (LoadDefaultAll == 'True') {
        // mac dinh chon toan truong
        var item = $('#toankhoi_truong').get();
        LoadGrid(item);
        // collapse all
        $('ul[class="ResetList"]').hide();
    }
    else {
        // expanse first - collapse other
        $('ul[class="ResetList"]:not(:first)').hide();
        // select first class
        var item = document.getElementsByName("itemClass");
        if (item && item.length > 0) {
            if (classID_Select == 0) {
                if (typeof item[0].onclick == "function") {
                    $(item[0]).click();
                    //var itemParent = document.getElementById('toankhoi' + $(item).attr('levelid'));                    
                    $('#' + $(item).attr('levelid')).show();
                }
            }
            else {
                $('.SidebarSection').find('.SendSMSToParent').find('a').each(function () {
                    if ($.trim($(this).attr('classid')) == classID_Select) {
                        // cap toan khoi
                        var levelid = $(item).attr('levelid');
                        $(this).click();
                    }
                });
            }
        }
    }


});

function ShowHideTree(item) {
    var value = $(item).attr('id');

    // cap Toan truong
    if (value == 'toankhoi_truong') {
        $('ul[class="ResetList"]').hide("slow");
    }
    // cap toan khoi
    var levelid = $(item).attr('levelid');
    if (levelid != null && (typeof levelid) != undefined && levelid != '') {
        if (value == 'toankhoi' + levelid) {
            var selected = $('ul[class="ResetList"][id="' + levelid + '"]').get();
            if ($(selected).is(':hidden')) {
                // hidden other
                $('ul[class="ResetList"]').hide('slow');
                $(selected).show("slow");
            } else {
                $(selected).hide("slow");
            }
        }
    }
}

//VALIDATE CONTENT INPUT - ONKEYUP/ONBLUR INPUT EVENT
function countSMS(txt, stopped) {
    //#region Đếm
    if ($(txt).attr('smsTextArea') != "1") {
        return;
    }
    var content = $(txt).val();
    if (content == null || (typeof content) == undefined || content == '') {
        return;
    }
    if ($.trim(content).length > 2000) {
        content = content.substring(0, 2000);
        $(txt).val(content);
    }
    
    var pupilID = $(txt).attr("pupilID");
    var ck = $("#checkedSend" + pupilID).get();
    var typeCode = $('select[name="SMSType"] option:selected').attr("typeCode");
    if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
        var SubPrefix = content.substring(0, BRAND.length);
        //if (stringUtils.StripVNSign(SubPrefix.toUpperCase()) != stringUtils.StripVNSign(BRAND.toUpperCase())) {
        //    if (stopped != true) {
        //        if ($('#AllowSignMessage').is(':checked')) {
        //            content = BRAND + content.substring(BRAND.length, content.length);
        //            smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + BRAND });
        //        } else {
        //            content = stringUtils.StripVNSign(BRAND) + content.substring(BRAND.length, content.length);
        //            smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + stringUtils.StripVNSign(BRAND) });
        //        }
        //    }
        //}
        var lengOfSMS = $.trim(content).length;
        var numOfSMS = 0

        if ($('#AllowSignMessage').is(':checked')) {
            if (lengOfSMS <= 70) {
                numOfSMS = 1;
            }
            else if (lengOfSMS % 67 == 0) {
                numOfSMS = lengOfSMS / 67;
            }
            else {
                numOfSMS = parseInt(lengOfSMS / 67) + 1;
            }
        } else {
            if (lengOfSMS % 160 == 0) {
                numOfSMS = lengOfSMS / 160;
            }
            else {
                numOfSMS = parseInt(lengOfSMS / 160) + 1;
            }
        }
        var trObject;
        //check toanlop-> neu dc check thi thay doi noi dung toan bo nguoc lai chi thay doi chinh no
        if ($('#toanlop').is(':checked')) {
            if (SubPrefix != PREFIX) {
                $('#GridPupil').find('.TxtContentParent').val(content);
            }
            else {
                $('#GridPupil').find('.TxtContentParent:gt(0)').val(content);
            }
            $('#GridPupil').find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
            //enabled button send
            //if ($.trim(content).length > $.trim(PREFIX).length) {
            if ($.trim(content) != $.trim(PREFIX)) {
                $('.chkPupilToSend').attr('checked', 'checked');
                $('.chkPupilToSend').removeAttr("disabled");
                $('#chkAll').removeAttr("disabled");
            }
            else {
                $('.chkPupilToSend').removeAttr('checked', 'checked');
                $('.chkPupilToSend').attr("disabled", "disabled");
                $('#chkAll').attr("disabled", "disabled");
                $('#chkAll').removeAttr('checked', 'checked');
            }
        }
        else {
            trObject = $(txt).closest('tr');
            //if (content.length > PREFIX.length) {
            trObject.find('.TxtContentParent').val(content);
            //} else {
            //    trObject.find('.TxtContentParent').val(PREFIX);
            //}
            //if (SubPrefix != PREFIX) {
            //    trObject.find('.TxtContentParent').val(content);
            //}
            trObject.find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
            if ($.trim(content) != $.trim(PREFIX)) {
                //if ($.trim(content).length > $.trim(PREFIX).length) {
                trObject.find('.chkPupilToSend').attr('checked', 'checked');
                trObject.find('.chkPupilToSend').removeAttr("disabled");
            }
            else {
                trObject.find('.chkPupilToSend').removeAttr('checked', 'checked');
                trObject.find('.chkPupilToSend').attr("disabled", "disabled");
            }
        }

        //set tong so tin nhan/check button save
        var totalSMSSend = 0;
        var numOfSMSTr;
        var numOfSubscriberTr;
        var bButtonSave = false;
        $('.chkPupilToSend:checked').each(function () {
            trObject = $(this).closest('tr');
            numOfSMSTr = trObject.find('.SMSLengthContent').text().split('/')[1];
            numOfSubscriberTr = trObject.find('.NumOfSubscriberStyle').html();
            totalSMSSend += numOfSubscriberTr * numOfSMSTr;
            if (bButtonSave == false) {
                bButtonSave = true;
            }
        });
        $('#numOfSMS').html(totalSMSSend);
        if (bButtonSave) {
            $('#btnSend').removeAttr("disabled");
        }
        else {
            $('#btnSend').attr("disabled", "disabled");
        }
        if ($('.chkPupilToSend:enabled').length != 0) {
            $('#chkAll').removeAttr("disabled");
            if (($('.chkPupilToSend:checked').length > 0 && $('.chkPupilToSend:checked').length == $('.chkPupilToSend:enabled').length)) {
                $('#chkAll').attr('checked', true);
            }
            else {
                $('#chkAll').attr('checked', false);
            }
        }
        else {
            $('#chkAll').attr('checked', false);
            $('#chkAll').attr("disabled", "disabled");
        }
    } else { // Bản tin khác trao đổi
        // 20160530 Hotfix Anhvd9 - Recount sms when check allow send sign message
        if (content == NO_MESSAGE) return;

        pupilID = $(txt).attr("pupilID");
        var template = $("#hidSMS" + pupilID).val();
        var SubPrefix = content.substring(0, template.length);
        if (SubPrefix != template) {
            content = template;
        }
        //set content
        $(txt).val(content);
        var lengOfSMS = $.trim(content).length;
        var numOfSMS = 0
        if ($('#AllowSignMessage').is(':checked')) {
            if (lengOfSMS <= 70) {
                numOfSMS = 1;
            }
            else if (lengOfSMS % 67 == 0) {
                numOfSMS = lengOfSMS / 67;
            }
            else {
                numOfSMS = parseInt(lengOfSMS / 67) + 1;
            }
        } else {
            if (lengOfSMS % 160 == 0) {
                numOfSMS = lengOfSMS / 160;
            }
            else {
                numOfSMS = parseInt(lengOfSMS / 160) + 1;
            }
        }

        $(txt).closest('tr').find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
        if ($.trim(content).length > $.trim(template).length) {
            $(ck).attr('checked', true);
        }
        else {
            $(ck).removeAttr('checked', 'checked');
        }

        if ($('.chkPupilToSend:enabled').length != 0) {
            $('#chkAll').removeAttr("disabled");
        }
        else {
            $('#chkAll').attr("disabled", "disabled");
        }

        ChkChange(ck);
    }
}

function countSMSTimer() {
   
    var trObject;
       
    //set tong so tin nhan/check button save
    var totalSMSSend = 0;
    var numOfSMSTr;
    var numOfSubscriberTr;

    $('.chkPupilToSend:checked').each(function () {
        trObject = $(this).closest('tr');
        numOfSMSTr = trObject.find('.SMSLengthContent').text().split('/')[1];
        numOfSubscriberTr = trObject.find('.NumOfSubscriberStyle').html();
        totalSMSSend += numOfSubscriberTr * numOfSMSTr;
           
    });
    $('#numOfSMS').html(totalSMSSend);
       
    
}

//VALIDATE TEXTBOX - ONPASTE/ONCUT INPUT EVENT
function onPasteCutValidate(thisObject, isEditor) {
    setTimeout(function () {
        if (isEditor) {
            validationSMSToGroup('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
            enableButtonSend();
        } else {
            countSMS(thisObject);
        }
    }, 200);
}

function CheckAllClass(ckAll) {
    Action = 1;
    CheckConfirmImporting(function () {
        if (ckAll.checked) {
            var trObject;
            var firstContent;
            $("#GridPupil").find('textarea').each(function (index) {
                if (index != 0) {
                    $(this).attr("disabled", "disabled");
                    $(this).css("background-color", "#EBEBE4");
                    $(this).val(firstContent);
                }
                else {
                    firstContent = $(this).val();
                }
                trObject = $(this).closest('tr');
                trObject.find('.chkPupilToSend').attr('checked', false);
                if ($.trim($(this).val()) == $.trim(PREFIX)) {
                    trObject.find('.chkPupilToSend').attr('disabled', 'disabled');
                }
                else {
                    trObject.find('.chkPupilToSend').removeAttr('disabled', 'disabled');
                }
            });

        } else {
            var trObject;
            $("#GridPupil").find('textarea').each(function () {
                $(this).removeAttr("disabled", "disabled");
                $(this).css("background-color", "#FFF");
                trObject = $(this).closest('tr');
                trObject.find('.chkPupilToSend').attr('checked', false);
                if ($.trim($(this).val()) == $.trim(PREFIX)) {
                    trObject.find('.chkPupilToSend').attr('disabled', 'disabled');
                }
                else {
                    trObject.find('.chkPupilToSend').removeAttr('disabled', 'disabled');
                }
            });
        }

        if ($('.chkPupilToSend:enabled').length > 0) {
            $("#chkAll").removeAttr("disabled");
            $('.chkPupilToSend:enabled').attr("checked", true);

            var lengOfSMS = $('.TxtContentParent').val().length;
            var numOfSMS = 0
            if ($('#AllowSignMessage').is(':checked')) {
                if (lengOfSMS <= 70) {
                    numOfSMS = 1;
                }
                else if (lengOfSMS % 67 == 0) {
                    numOfSMS = lengOfSMS / 67;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 67) + 1;
                }
            } else {
                if (lengOfSMS % 160 == 0) {
                    numOfSMS = lengOfSMS / 160;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 160) + 1;
                }
            }

            $('#GridPupil').find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);

        }
        else {
            $("#chkAll").attr("disabled", "disabled");
            $('.chkPupilToSend:enabled').attr("checked", false);
            $("#btnSend").attr("disabled", "disabled");
        }

        var chkenabled = $('.chkPupilToSend:enabled');
        var chkchecked = $('.chkPupilToSend:checked');

        if (chkenabled.length > 0 && chkchecked.length > 0 && chkenabled.length == chkchecked.length) {
            $("#chkAll").attr("checked", true);
        }
    });
};


function CheckAllClassTimer(ckAll) {
    Action = 1;
    CheckConfirmImporting(function () {
        if (ckAll.checked) {
            var trObject;
            var firstContent;
            $("#GridPupil").find('textarea').each(function (index) {
                if (index != 0) {
                    $(this).attr("disabled", "disabled");
                    $(this).css("background-color", "#EBEBE4");
                    $(this).val(firstContent);
                }
                else {
                    firstContent = $(this).val();
                }
               
            });

        } else {
            var trObject;
            $("#GridPupil").find('textarea').each(function () {
                $(this).removeAttr("disabled", "disabled");
                $(this).css("background-color", "#FFF");
               
            });
        }
    });
}


function hideAddtionalPanel() {
    $("#SMSTypeClass").removeAttr("style");
    $('#SemesterID').hide();
    $('#MonthID').hide();
    $('#ComboTimeMark').hide();
    $('#ComboExamID').hide();
    $("#ComboMonthMN").hide();
    $("#ComboWeek").hide();
    $("#ComboWeek2").hide();
    $("#WeekNote").hide();
    $("#AddtionalPanel").hide();
}

function showAddtionalPanel(val) {
    if (val == 1) {
        $('#SemesterID').show();
        $('#SemesterID option[value="1"]').text("Học kỳ I");
        $('#SemesterID option[value="2"]').text("Học kỳ II");
        $('#SemesterID option[value="3"]').remove();
        $('#SemesterID option[value="4"]').remove();
        $('#SemesterID option[value="5"]').remove();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#MonthID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").hide();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    } else if (val == 2) {
        $('#SemesterID').hide();
        $('#MonthID').show();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").hide();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    } else if (val == 3) {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').show();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").hide();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    } else if (val == 4) {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').show();
        $('#notes').show();
        $("#ComboMonthMN").hide();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    }
    else if(val==5){
        //$("#TimerConfigClass").show();
    }
    else if (val == 6) {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").show();
        $("#ComboWeek").show();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    }
    else if (val == 7) {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").show();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    }
    else if (val == 8) {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").show();
        $("#ComboWeek").hide();
        $("#ComboWeek2").show();
        $("#WeekNote").show();
    }
    else {
        $('#SemesterID').hide();
        $('#MonthID').hide();
        $('#ComboTimeMark').hide();
        $('#ComboExamID').hide();
        $('#notes').hide();
        $("#ComboMonthMN").hide();
        $("#ComboWeek").hide();
        $("#ComboWeek2").hide();
        $("#WeekNote").hide();
    }
	// fix bug: 520px -> 517px
    //$("#SMSTypeClass").attr("style", "float: left; margin-left: 517px; padding-right: 10px;");
    $("#AddtionalPanel").show();
}

function hideComTypeInfo() {
    $("#commtype").hide();
    $("#toanlop").attr("checked", false);
    $('#CalendarSelector').hide();
    $('#CalendarMNSelector').hide();
}

function SelectType(val, isEducationLevel, dontLoadTimerConfig, isLoadForTimer) {
    CheckConfirmImporting(function () {
        var typecheck;
        var typeCode;
        var iscustom;
        var periodType;
        var forVNEN;
        //$('#isTimerUsed').removeAttr('checked');
        if (isEducationLevel) {
            HideSMSTimerUsedEducation();
            typecheck = $('select[name="SMSTypeToEducationLevel"]').val();
            typeCode = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("typeCode");

            iscustom = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("iscustom");
            periodType = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("periodtype");
            forVNEN = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("forvnen");

            $("#frmMessage #TimerConfigID").val("");
            $("#frmMessage #TimerConfigID").change();

            $('#divInfoNeedSend').hide();
            $('#CalendarSelector').hide();
            $('#CalendarMNSelector').hide();
            $("#ComboTimeMarklevel").hide();
            $("#MonthIDLevel").hide();
            $("#SemesterEdu").hide();
            $("#MonthMNEdu").hide();
            $("#ComboWeekEdu").hide();
            $("#ComboWeek2Edu").hide();
            $("#WeekNoteEdu").hide();
            $("#SendSMSToNotViolatePupilWrapper").hide();
            $('#SMSContent').attr('disabled', 'disabled');
            
            if (typeCode == SMS_TYPE_COMUNICATION_CODE)
            {
                $("#frmMessage").find(".TimerInput").show();
                $("#frmMessage").find("#importDetail").show();
            }
            else
            {
                $("#frmMessage #IsTimerUsed").attr('checked', false);
                $("#frmMessage").find(".TimerInput").hide();
                $("#frmMessage").find("#importDetail").hide();
            }
            if (typecheck == '0') {
                $('.BtnCenterSection .t-button').attr('disabled', 'disabled');
                $('#SMSContent').val(PREFIX);
            } else if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
                // hien thi thong tin gui
                $('#divInfoNeedSend').show();

                if ($("#frmMessage #IsSendImport").val() == 'true') {
                    $("#frmMessage #SMSContent").attr("disabled", "disabled");
                    $("#SendButton .SendButton").find(".t-button").removeAttr("disabled");
                    checkAllowSignMsgCommunicate($("frmMessage #AllowSignMsgCommunicate")[0]);
                }
                else {
                    $('#SMSContent').removeAttr('disabled', 'disabled');

                    if ($.trim($('#SMSContent').val()) == $.trim(PREFIX) || $('#numOfSMS').html() == '0') { //prefix nam o layout
                        $('.BtnCenterSection .t-button').attr('disabled', 'disabled');
                    }
                    else {
                        $('.BtnCenterSection .t-button').removeAttr('disabled');
                    }
                }
                
            }
            else if (typeCode == SMS_TYPE_RLN || typeCode == SMS_TYPE_RLT || typeCode == SMS_TYPE_RLTH) {
                $("#SendSMSToNotViolatePupilWrapper").show();
            }
            else if (typeCode == SMS_TYPE_RLMR) {
                $("#SendSMSToNotViolatePupilWrapper").show();
                $('#CalendarSelector').show();
                $('#FromDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                $('#ToDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_PERIOD_MARK) {
                $("#ComboTimeMarklevel").show();
                $.ajax({
                    url: urlLoadJsonMarkConfigTime,
                    xhrFields: {
                        withCredentials: true
                    },
                    type: 'post',
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (data) {
                        hideComTypeInfo();
                        $("#ComboTimeMarklevel").empty();
                        var obj;
                        for (var i = 0; i < data.length; i++) {
                            obj = data[i];
                            if (obj.IsNearest == true) {
                                $("#ComboTimeMarklevel").append('<option name="' + obj.MarkTimeName + '" value="' + obj.MarkTimeConfigID + '" selected="selected">' + obj.MarkTimeName + '</option>');
                            } else {
                                $("#ComboTimeMarklevel").append('<option name="' + obj.MarkTimeName + '" value="' + obj.MarkTimeConfigID + '">' + obj.MarkTimeName + '</option>');
                            }
                        }
                        smas.hideProcessingDialog();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                        smas.hideProcessingDialog();
                    }
                });

                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_COMMENT_MONTH_VNEN) {
                $("#MonthIDLevel").show();
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }


            else if (typeCode == SMS_TYPE_BNTU) {
                var month = parseInt((new Date()).getMonth() + 1);
                var year = parseInt((new Date()).getFullYear());
                var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                $('#MonthMNEdu option[value="' + monthAndYear + '"]').attr("selected", "selected");

                $("#MonthMNEdu").show();
                $("#ComboWeekEdu").show();
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_BNTH) {
                var month = parseInt((new Date()).getMonth() + 1);
                var year = parseInt((new Date()).getFullYear());
                var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                $('#MonthMNEdu option[value="' + monthAndYear + '"]').attr("selected", "selected");

                $("#MonthMNEdu").show();
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_TDN) {
                $('#CalendarMNSelector').show();
                $('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_TDT) {
                var month = parseInt((new Date()).getMonth() + 1);
                var year = parseInt((new Date()).getFullYear());
                var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                $('#MonthMNEdu option[value="' + monthAndYear + '"]').attr("selected", "selected");
                var week = $("#ComboWeek2Edu").val();

                LoadWeek(null);

                $("#MonthMNEdu").show();
                $("#ComboWeek2Edu").show();
                $("#WeekNoteEdu").show();
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_CDSK) {
                var month = parseInt((new Date()).getMonth() + 1);
                var year = parseInt((new Date()).getFullYear());
                var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                $('#MonthMNEdu option[value="' + monthAndYear + '"]').attr("selected", "selected");

                $("#MonthMNEdu").show();
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (typeCode == SMS_TYPE_DGHDN) {
                $('#CalendarMNSelector').show();
                $('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            else if (iscustom == 'true') {
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                if (periodType == 3) {
                    if (forVNEN == 'True') {
                        $("#MonthIDLevel").show();
                        $('.BtnCenterSection .t-button').removeAttr('disabled');
                        $('#SMSContent').val(PREFIX);
                    }
                }
                else if (periodType == 4) {
                    if (grade == 1) {
                        $("#SemesterEdu").empty();
                        $("#SemesterEdu").append('<option value="3">Giữa kỳ 1</option>');
                        $("#SemesterEdu").append('<option value="1">Cuối kỳ 1</option>');
                        $("#SemesterEdu").append('<option value="4">Giữa kỳ 2</option>');
                        $("#SemesterEdu").append('<option value="2">Cuối kỳ 2</option>');
                    }
                    else {
                        $("#SemesterEdu").empty();
                        $("#SemesterEdu").append('<option value="1">Học kỳ 1</option>');
                        $("#SemesterEdu").append('<option value="2">Học kỳ 2</option>');
                        $("#SemesterEdu").append('<option value="5">CN</option>');
                    }

                    $('#SemesterEdu').show();

                    $('#SemesterEdu option[value= ' + semester + ']').attr("selected", "selected");
                }
                else if (periodType == 5) {
                    $('#CalendarSelector').show();
                    $('#FromDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    $('#ToDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    $('.BtnCenterSection .t-button').removeAttr('disabled');
                    $('#SMSContent').val(PREFIX);
                }

            }
            else {
                $('.BtnCenterSection .t-button').removeAttr('disabled');
                $('#SMSContent').val(PREFIX);
            }
            // 20160530 Anhvd9 - Recount sms in editor
            validationSMSToGroup('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
        }
        else {
            HideSMSTimerUsed();
            typecheck = $('#SMSType').val();
            typeCode = $('#SMSType option:selected').attr("typeCode");
            iscustom = $('#SMSType option:selected').attr("iscustom");
            periodType = $('#SMSType option:selected').attr("periodtype");
            forVNEN = $('#SMSType option:selected').attr("forvnen");
            if (typecheck == '0') {
                $("#GridPupil tbody tr").find('input:checkbox').attr({ checked: false, disabled: "disabled" }); // uncheck, disable
                $("#GridPupil tbody tr").find('textarea').attr({ value: 'Vui lòng chọn bản tin', disabled: "disabled" });
            }
            var cls = null;
            var clsArr = $('a[name="itemClass"]');
            if (clsArr.length > 0) {
                for (var i = 0; i < clsArr.length; i++) {
                    if ($(clsArr[i]).hasClass('Active')) {
                        cls = clsArr[i];
                        break;
                    }
                }
            }
            var EducationLevelID = $(cls).attr("levelid");
            var classId = $("#hidClassID").val();
            if (classId == "") {
                classId = $(cls).attr("classid");
            }
            var Type = $('#SMSType').val();
            var semester_id = 0;

            var IsSignMsg = $('#AllowSignMessage').is(':checked');

                ////////////// Bản tin ở TT30
            if (typeCode == SMS_TYPE_EVALUATION_MONTH) {
                showAddtionalPanel(2);
                if (isLoadMonth) {
                    monthSelected = GetAutoMonth(semester);
                    isLoadMonth = false;
                    semester_id = semester;
                } else {
                    monthSelected = $('#MonthID').val();
                    semester_id = GetSemesterByMonth(monthSelected);
                }

                $('#MonthID option[value= ' + monthSelected + ']').attr("selected", "selected");

                //Load danh sach hoc sinh can gui tin nhan
                $.ajax({
                    url: urlLoadGridCustomize,
                    xhrFields: {
                        withCredentials: true
                    },
                    type: 'post',
                    data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (data) {
                        hideComTypeInfo();
                        smas.hideProcessingDialog();
                        $("#fmlstPupil").html(data);
                        CheckChkAllIsLoad();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                        smas.hideProcessingDialog();
                    }
                });

            } else if (typeCode == SMS_TYPE_EVALUATION_RESULT_SEMESTER) {
                showAddtionalPanel(1);
                isLoadMonth = false;
                // Thong tin cuoi ky: Month = 11
                monthSelected = 11;
                $('#SemesterID option[value= ' + semester + ']').attr("selected", "selected");
                $.ajax({
                    url: urlLoadGridCustomize,
                    xhrFields: {
                        withCredentials: true
                    },
                    type: 'post',
                    data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (data) {
                        hideComTypeInfo();
                        smas.hideProcessingDialog();
                        $("#fmlstPupil").html(data);
                        CheckChkAllIsLoad();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                        smas.hideProcessingDialog();
                    }
                });
            } else if (typeCode == SMS_TYPE_COMMENTS_ENDING) {
                showAddtionalPanel(1);
                semester_id = semester;
                $('#SemesterID option[value= ' + semester_id + ']').attr("selected", "selected");
                isLoadMonth = false;
                monthSelected = -1;
                $.ajax({
                    url: urlLoadGridCustomize,
                    xhrFields: {
                        withCredentials: true
                    },
                    type: 'post',
                    data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (data) {
                        hideComTypeInfo();
                        smas.hideProcessingDialog();
                        $("#fmlstPupil").html(data);
                        CheckChkAllIsLoad();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                        smas.hideProcessingDialog();
                    }
                });
            } // end - 3 bản tin ở TT30

            else  /////////////////////////////// Bản tin VNEN
                if (typeCode == SMS_TYPE_COMMENT_MONTH_VNEN) {
                    showAddtionalPanel(2);
                    var month = parseInt((new Date()).getMonth() + 1);
                    $('#MonthID option[value=' + month + ']').attr("selected", "selected");
                    monthSelected = $('#MonthID').val();

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });

                } else if (typeCode == SMS_TYPE_RESULT_SEMESTER_VNEN) {
                    hideAddtionalPanel();
                    monthSelected = 0;
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                } else if (typeCode == SMS_TYPE_COMMENT_ENDING_VNEN) {
                    hideAddtionalPanel();
                    if (semester == 1) {
                        monthSelected = 15;
                    } else if (semester == 2) {
                        monthSelected = 16;
                    }

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                } // end - 3 bản tin VNEN

                else if (typeCode == SMS_TYPE_SEMESTER_MARK) { // Ban tin Ket qua hoc ky
                    isLoadMonth = false;
                    hideComTypeInfo();
                    showAddtionalPanel(1);

                    if (grade == 2 || grade == 3) {
                        $("#SemesterID").append('<option value="3">CN</option>');
                    }
                    $('#SemesterID').val(semester);
                    SelectSemester();
                } else if (typeCode == SMS_TYPE_PERIOD_MARK) {  // Ban tin Diem dot
                    showAddtionalPanel(3);
                    isLoadMonth = false;
                    //Load danh sach dot
                    $.ajax({
                        url: urlLoadJsonMarkConfigTime,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            $("#ComboTimeMark").empty();
                            var obj;
                            for (var i = 0; i < data.length; i++) {
                                obj = data[i];
                                if (obj.IsNearest == true) {
                                    $("#ComboTimeMark").append('<option name="' + obj.MarkTimeName + '" value="' + obj.MarkTimeConfigID + '" selected="selected">' + obj.MarkTimeName + '</option>');
                                } else {
                                    $("#ComboTimeMark").append('<option name="' + obj.MarkTimeName + '" value="' + obj.MarkTimeConfigID + '">' + obj.MarkTimeName + '</option>');
                                }
                            }
                            smas.hideProcessingDialog();
                            // Chon dot gan nhat
                            SelectTimeMark();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });

                }
                    /////////////////////////////// 20160303 - Bản tin liên quan kỳ thi
                else if (typeCode == SMS_TYPE_EXAM_SCHEDULE || typeCode == SMS_TYPE_EXAM_RESULT) {
                    showAddtionalPanel(4);
                    isLoadMonth = false;
                    //Load danh ky thi
                    $.ajax({
                        url: urlLoadListExam,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            $("#ComboExamID").empty();
                            var obj;
                            var isFirst = true;
                            for (var i = 0; i < data.length; i++) {
                                obj = data[i];
                                if (typeCode == SMS_TYPE_EXAM_RESULT && !obj.MarkClosing) continue;
                                if (isFirst) {
                                    isFirst = false;
                                    $("#ComboExamID").append('<option name="' + obj.ExaminationID + '" value="' + obj.ExaminationID + '" selected="selected">' + obj.ExaminationName + '</option>');
                                } else {
                                    $("#ComboExamID").append('<option name="' + obj.ExaminationID + '" value="' + obj.ExaminationID + '">' + obj.ExaminationName + '</option>');
                                }
                            }
                            smas.hideProcessingDialog();
                            SelectComboExam();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                    ///////////// End - Bản tin liên quan kỳ thi
                } else if (typeCode == SMS_TYPE_RLMR) {
                    hideAddtionalPanel();
                    hideComTypeInfo();
                    isLoadMonth = false;
                    $('#CalendarSelector').show();
                    $('#FromDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    $('#ToDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    LoadSMSByType();
                } else if (typeCode == SMS_TYPE_EXAM_ENDING_MARK) {
                    showAddtionalPanel(1);
                    hideComTypeInfo();
                    isLoadMonth = false;
                    $('#SemesterID').val(semester);
                    SelectSemester();
                }
                    //Start: TT22
                else if (typeCode == SMS_TYPE_TT22_KQCK
                    || typeCode == SMS_TYPE_TT22_KQGK
                    || typeCode == SMS_TYPE_TT22_NXCK) {
                    showAddtionalPanel(1);
                    isLoadMonth = false;
                    monthSelected = 0;
                    $('#SemesterID option[value= ' + semester + ']').attr("selected", "selected");
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_BNTU) {
                    showAddtionalPanel(6);
                    isLoadMonth = false;
                    var month = parseInt((new Date()).getMonth() + 1);
                    var year = parseInt((new Date()).getFullYear());
                    var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                    $('#ComboMonthMN option[value="' + monthAndYear + '"]').attr("selected", "selected");
                    monthSelected = 0;
                    monthSelectedMN = $("#ComboMonthMN").val();
                    var week = $("#ComboWeek").val();
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_BNTH) {
                    showAddtionalPanel(7);
                    isLoadMonth = false;
                    var month = parseInt((new Date()).getMonth() + 1);
                    var year = parseInt((new Date()).getFullYear());
                    var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                    $('#ComboMonthMN option[value="' + monthAndYear + '"]').attr("selected", "selected");
                    monthSelected = 0;
                    monthSelectedMN = $("#ComboMonthMN").val();
                    var week = 0;
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_TDN) {
                    hideAddtionalPanel();
                    hideComTypeInfo();
                    $('#CalendarMNSelector').show();
                    $('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    isLoadMonth = false;
                    var dateMN = $('#DateMN').val();
                    monthSelected = 0;

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, dateMN: dateMN, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            //hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_TDT) {
                    showAddtionalPanel(8);
                    isLoadMonth = false;
                    var month = parseInt((new Date()).getMonth() + 1);
                    var year = parseInt((new Date()).getFullYear());
                    var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                    $('#ComboMonthMN option[value="' + monthAndYear + '"]').attr("selected", "selected");
                    monthSelected = 0;
                    monthSelectedMN = $("#ComboMonthMN").val();

                    $.ajax({
                        url: urlLoadWeek,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { monthAndYear: monthSelectedMN },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            $("#ComboWeek2").empty();
                            var obj;
                            for (var i = 0; i < data.list.length; i++) {
                                obj = data.list[i];
                                if (obj.IsSelected == true) {
                                    $("#ComboWeek2").append('<option name="' + obj.Value + '" value="' + obj.Key + '" selected="selected">' + obj.Value + '</option>');
                                } else {
                                    $("#ComboWeek2").append('<option name="' + obj.Value + '" value="' + obj.Key + '">' + obj.Value + '</option>');
                                }
                            }

                            $("#WeekNote").text(data.dateNote);
                            smas.hideProcessingDialog();
                            
                            var week = $("#ComboWeek2").val();
                            $.ajax({
                                url: urlLoadGridCustomize,
                                xhrFields: {
                                    withCredentials: true
                                },
                                type: 'post',
                                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                                beforeSend: function () {
                                    smas.showProcessingDialog();
                                },
                                success: function (data) {
                                    hideComTypeInfo();
                                    smas.hideProcessingDialog();
                                    $("#fmlstPupil").html(data);
                                    CheckChkAllIsLoad();
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    smas.alert(XMLHttpRequest.responseText);
                                    smas.hideProcessingDialog();
                                }
                            });
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_CDSK) {
                    showAddtionalPanel(7);
                    isLoadMonth = false;
                    var month = parseInt((new Date()).getMonth() + 1);
                    var year = parseInt((new Date()).getFullYear());
                    var monthAndYear = month.padLeft(2) + '/' + year.padLeft(4);
                    $('#ComboMonthMN option[value="' + monthAndYear + '"]').attr("selected", "selected");
                    monthSelected = 0;
                    monthSelectedMN = $("#ComboMonthMN").val();
                    var week = 0;
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_DGSPT) {
                    hideAddtionalPanel();
                    hideComTypeInfo();
                    
                    isLoadMonth = false;
                    monthSelected = 0;

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (typeCode == SMS_TYPE_DGHDN) {
                    hideAddtionalPanel();
                    hideComTypeInfo();
                    $('#CalendarMNSelector').show();
                    $('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    isLoadMonth = false;
                    var dateMN = $('#DateMN').val();
                    monthSelected = 0;

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, dateMN: dateMN, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            //hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }
                else if (iscustom == 'true') {
                    isLoadMonth = false;
                    monthSelected = 0;
                    var year = 0;
                    var fromDate;
                    var toDate;
                    hideAddtionalPanel();
                    if (periodType == 3) {
                        if (forVNEN == 'True') {
                            showAddtionalPanel(2);
                            var month = parseInt((new Date()).getMonth() + 1);
                            $('#MonthID option[value=' + month + ']').attr("selected", "selected");
                            monthSelected = $('#MonthID').val();
                            year = $('#MonthID option:selected').attr('year');
                        }
                    }
                    else if (periodType == 4) {
                        if (grade == 1) {
                            $("#SemesterID").empty();
                            $("#SemesterID").append('<option value="3">Giữa kỳ 1</option>');
                            $("#SemesterID").append('<option value="1">Cuối kỳ 1</option>');
                            $("#SemesterID").append('<option value="4">Giữa kỳ 2</option>');
                            $("#SemesterID").append('<option value="2">Cuối kỳ 2</option>');
                        }
                        else {
                            $("#SemesterID").empty();
                            $("#SemesterID").append('<option value="1">Học kỳ 1</option>');
                            $("#SemesterID").append('<option value="2">Học kỳ 2</option>');
                            $("#SemesterID").append('<option value="5">CN</option>');
                        }

                        $('#SemesterID').show();
                        $('#ComboTimeMark').hide();
                        $('#ComboExamID').hide();
                        $('#MonthID').hide();
                        $('#notes').hide();
                        $("#AddtionalPanel").show();

                        $('#SemesterID option[value= ' + semester + ']').attr("selected", "selected");


                    }
                    else if (periodType == 5) {
                        hideAddtionalPanel();
                        hideComTypeInfo();
                        isLoadMonth = false;
                        $('#CalendarSelector').show();
                        $('#FromDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                        $('#ToDate').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));

                        fromDate = $('#FromDate').val();
                        toDate = $('#ToDate').val();
                    }

                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, year: year, type: Type, semester: semester, isSignMsg: IsSignMsg, iscustom: iscustom, fromDate: fromDate, toDate: toDate },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            if (periodType != 5) {
                                hideComTypeInfo();
                            }
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                }

                else {
                    hideAddtionalPanel();
                    isLoadMonth = false;
                    hideComTypeInfo();
                    if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
                        $("#AddtionalPanel").show();
                        $("#commtype").show();
                        $("#toanlop").attr("checked", true);

                        CheckAllClass(document.getElementById("toanlop"));
                        //Xoa noi dung textbox
                        $("#GridPupil tbody textarea").attr({ value: "" });
                    }
                    LoadSMSByType();
                }
        } // end selected class
    });
}

function SelectSemester() {
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    if (cls) {
        var classId = $(cls).attr("classid");
        var EducationLevelID = $(cls).attr("levelid");
        var typeid = $('#SMSType').val();
        var semester = $('#SemesterID').val();
        var IsSignMsg = $('#AllowSignMessage').is(':checked');
        $.ajax({
            url: urlLoadGrid,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            data: { classID: classId, EducationLevelID: EducationLevelID, typeID: typeid, Semester: semester, IsSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (msg) {
                $("#fmlstPupil").html();
                $("#fmlstPupil").html(msg);
                countSumSMS();
                smas.hideProcessingDialog();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
}

function SelectTimeMark() { // Diem theo dot
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    if (cls) {
        var classId = $(cls).attr("classid");
        var EducationLevelID = $(cls).attr("levelid");
        var typeid = $('#SMSType').val();
        var timeID = $('#ComboTimeMark').val();
        var IsSignMsg = $('#AllowSignMessage').is(':checked');
        //Load data
        $.ajax({
            url: urlLoadGrid,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            data: { classID: classId, EducationLevelID: EducationLevelID, typeID: typeid, ComboID: timeID, IsSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (msg) {
                smas.hideProcessingDialog();
                $("#fmlstPupil").html();
                $("#fmlstPupil").html(msg);
                countSumSMS();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
}

/////////////////////////  20160303 Anhvd9 - 2 SMSType related Examination//////////////////////
function SelectComboExam() {
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    if (cls) {
        var classId = $(cls).attr("classid");
        var EducationLevelID = $(cls).attr("levelid");
        var typeid = $('#SMSType').val();
        var examID = $('#ComboExamID').val();
        var IsSignMsg = $('#AllowSignMessage').is(':checked');
        //Load data
        $.ajax({
            url: urlLoadGrid,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            data: { classID: classId, EducationLevelID: EducationLevelID, typeID: typeid, ComboID: examID, IsSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (msg) {
                smas.hideProcessingDialog();
                $("#fmlstPupil").html();
                $("#fmlstPupil").html(msg);
                countSumSMS();
                // view notes
                $('#notes').show();
                var typeCode = $('#SMSType').find('option:selected').attr("typeCode");
                if (typeCode == SMS_TYPE_EXAM_SCHEDULE) {
                    $("span#notesText").html('Lưu ý: Lịch thi chỉ hiển thị khi kỳ thi đã lập danh sách thí sinh');
                }
                else {
                    $("span#notesText").html('Lưu ý: Chỉ cho phép gửi kết quả của các kỳ thi đã chốt điểm thi');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
}

function LoadSMSByType(callBack, forTimer) {
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    if (cls) {
        $('#FromDate').removeClass('t-state-error');
        $('#ToDate').removeClass('t-state-error');
        var typeCode = $('#SMSType').find('option:selected').attr("typeCode");
        var classId = $(cls).attr("classid");
        var EducationLevelID = $(cls).attr("levelid");
        var typeid = $('#SMSType').val();
        var timeID = $('#ComboTimeMark').val();
        var semester = $('#SemesterID').val();
        var fromDate = $('#FromDate').val();
        var toDate = $('#ToDate').val();
        var IsSignMsg = $('#AllowSignMessage').is(':checked');
        var timerConfigId = $('#TimerConfigID').val();
        var isTimerUsed = $('#isTimerUsed').is(':checked');

        $.ajax({
            url: urlLoadGrid,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            data: { typeCode: typeCode, ClassID: classId, EducationLevelID: EducationLevelID, typeID: typeid, ComboID: timeID, Semester: semester, FromDate: fromDate, ToDate: toDate, IsSignMsg: IsSignMsg, IsTimerUsed: isTimerUsed, timerConfigId: timerConfigId },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (msg) {
                smas.hideProcessingDialog();
                $("#fmlstPupil").html();
                $("#fmlstPupil").html(msg);
                countSumSMS();
                //Vaidate Message
                if (forTimer == null || forTimer == false) {
                    if ($("#toanlop").is(":visible") && typeCode == SMS_TYPE_COMUNICATION_CODE) {
                        $("#toanlop").attr("checked", true);
                        CheckAllClass(document.getElementById("toanlop"));
                        $('#chkAll').attr("disabled", "disabled");
                        $('.chkPupilToSend').attr("disabled", "disabled");
                        $('#chkAll').attr("checked", false);
                        $('.chkPupilToSend').attr("checked", false);
                        $('#numOfSMS').html(0);
                        $('#btnSend').attr('disabled', 'disabled');
                    }

                }

                if(callBack!=null && callBack!='undefined')
                {
                    callBack();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
}

function LoadSMSType(ClassID) {
    $.ajax({
        url: urlLoadSMSType,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        data: { ClassID: ClassID },
        beforeSend: function () {
            //smas.showProcessingDialog();
        },
        success: function (msg) {
            $('#SMSTypeClass').html(msg);
            //smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });
}

function ReloadSMSTypeClass() {
    $('#SMSTypeClass #SMSType').val("0");
}
function LoadGrid(item) {
    CheckConfirmImporting(function () {
        // show/hide class tree
        ShowHideTree(item);
        var classid = $(item).attr("classid");
        var levelid = $(item).attr("levelid");

        // luu lai toan khoi de lay toan khoi khi gui toan khoi
        educationLevelIDSelected = levelid;
        
        var fromDate = $('#FromDate').val();
        var toDate = $('#ToDate').val();

        // Clear the list
        $('.NavigationList').find('a').removeClass('Active');
        $('.NavigationList').find('div').removeAttr('selected');

        $('#ComboTimeMarklevel').hide();
        $("#MonthIDLevel").hide();
        $("#SemesterEdu").hide();
        $("#MonthMNEdu").hide();
        $("#ComboWeekEdu").hide();
        $("#ComboWeek2Edu").hide();
        $("#WeekNoteEdu").hide();
        /* - Chon LOP */
        if (classid != null && typeof classid != undefined && classid != '') {
            $('#isTimerUsed').removeAttr('checked');

            typeCode = $('#SMSType option:selected').attr("typeCode");
            if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
                TimerUsedClick('#isTimerUsed');
            }

            $(item).attr("class", "ClassStyle Active");
            $('#SMSTypeToEducationLevel').hide();
            $('#SMSTypeClass').show();
            $('#SMSTypeClass').closest("td").show();
            var typeid = $('#SMSType').val();
            var typeCode = $('#SMSType').find('option:selected').attr("typeCode");
            var IsSignMsg = $('#AllowSignMessage').is(':checked');
            // Thay doi ban tin voi truong theo mo hinh VNEN
            //if (IS_NEW_SCHOOL_MODEL.toLowerCase() == "true" && grade == 2 && isReady == 0) {
            if (IS_NEW_SCHOOL_MODEL.toLowerCase() == "true" && grade == 2) {
                // Lay danh sach ban tin doi voi lop tuong ung
                LoadSMSType(classid);
                //$("#SMSType").append('<option typecode="NXTVNEN" value="40">Nhận xét tháng (VNEN) </option>');
                //$("#SMSType").append('<option typecode="KQHKVNEN" value="41">Kết quả học kỳ (VNEN) </option>');
                //$("#SMSType").append('<option typecode="NXCKVNEN" value="42">Nhận xét cuối kỳ (VNEN) </option>');

                if ($("#SMSType option[value=" + typeid + "]").length) {
                    $('#SMSType').val(typeid);
                }
                typeid = $('#SMSType').val();
                typeCode = $('#SMSType option:selected').attr("typeCode");
                isReady = 1;
            }
            
            var timeID = $('#ComboTimeMark').val();
            if (timeID == null || timeID == ''){
                timeID = $('#ComboExamID').val();
            }
            var semester = $('#SemesterID').val();
            var IsSignMsg = $('#AllowSignMessage').is(':checked');

            if (typeid == 0) {
                hideAddtionalPanel();
                hideComTypeInfo();
            }
            if (typeCode == SMS_TYPE_COMUNICATION_CODE) {

            }
            if (typeCode == SMS_TYPE_EVALUATION_MONTH || typeCode == SMS_TYPE_EVALUATION_RESULT_SEMESTER || typeCode == SMS_TYPE_COMMENTS_ENDING || typeCode == SMS_TYPE_EXAM_ENDING_MARK
                || typeCode == SMS_TYPE_COMMENT_MONTH_VNEN || typeCode == SMS_TYPE_RESULT_SEMESTER_VNEN || typeCode == SMS_TYPE_COMMENT_ENDING_VNEN
                || typeCode == SMS_TYPE_TT22_KQCK || typeCode == SMS_TYPE_TT22_KQGK || typeCode == SMS_TYPE_TT22_NXCK
                || typeCode == SMS_TYPE_BNTU || typeCode == SMS_TYPE_BNTH || typeCode == SMS_TYPE_TDN || typeCode == SMS_TYPE_TDT
                || typeCode == SMS_TYPE_CDSK || typeCode == SMS_TYPE_DGSPT || typeCode == SMS_TYPE_DGHDN) {
                ///////////// Ban tin TT30
                if (typeCode == SMS_TYPE_EVALUATION_MONTH) {
                    showAddtionalPanel(2);
                    LoadDataSendSMS();
                } else if (typeCode == SMS_TYPE_EVALUATION_RESULT_SEMESTER) {
                    showAddtionalPanel(1);
                    LoadDataSendSMS();
                } else if (typeCode == SMS_TYPE_COMMENTS_ENDING) {
                    showAddtionalPanel(1);
                    LoadDataSendSMS();
                } else if (typeCode == SMS_TYPE_EXAM_ENDING_MARK) {
                    showAddtionalPanel(1);
                    SelectSemester();
                } else //////////////// Bản tin VNEN
                if (typeCode == SMS_TYPE_COMMENT_MONTH_VNEN) {
                    showAddtionalPanel(2);
                    LoadDataSendSMS();
                } else if (typeCode == SMS_TYPE_RESULT_SEMESTER_VNEN) {
                    LoadDataSendSMS();
                } else if (typeCode == SMS_TYPE_COMMENT_ENDING_VNEN) {
                    LoadDataSendSMS();
                }
                //TT22
                else if (typeCode == SMS_TYPE_TT22_KQCK) {
                    showAddtionalPanel(1);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_TT22_KQGK) {
                    showAddtionalPanel(1);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_TT22_NXCK) {
                    showAddtionalPanel(1);
                    LoadDataSendSMS();
                }
                //mam non
                else if (typeCode == SMS_TYPE_BNTU) {
                    showAddtionalPanel(6);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_BNTH) {
                    showAddtionalPanel(7);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_TDN) {
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_TDT) {
                    showAddtionalPanel(8);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_CDSK) {
                    showAddtionalPanel(7);
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_DGSPT) {
                    LoadDataSendSMS();
                }
                else if (typeCode == SMS_TYPE_DGHDN) {
                    $('#CalendarMNSelector').show();
                    $('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
                    LoadDataSendSMS();
                }
                
                setTimeout(function () {
                    $('#SMSType option[value="' + typeid + '"]').attr('selected', 'selected');
                }, 1000);
            } else {
                $.ajax({
                    url: urlLoadGrid,
                    xhrFields: {
                        withCredentials: true
                    },
                    type: 'post',

                    data: { typeCode: typeCode, ClassID: classid, EducationLevelID: levelid, TypeID: typeid, ComboID: timeID, Semester: semester, FromDate: fromDate, ToDate: toDate, IsSignMsg: IsSignMsg },
                    beforeSend: function () {
                        smas.showProcessingDialog();
                    },
                    success: function (msg) {
                        smas.hideProcessingDialog();
                        $("#fmlstPupil").css({ 'visibility': 'visible', display: '' });
                        $('#fmlstPupil').html(msg);
                        if (typeid == 0) {
                            $('#chkAll').attr("disabled", "disabled");
                        } else if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
                            $("#commtype").show();
                            $("#toanlop").attr("checked", true);
                            CheckAllClass(document.getElementById("toanlop"));
                        }               
                        if ($('#numOfSMS').html() == '0') {
                            $('#btnSend').attr('disabled', 'disabled');
                        }

                        if (checkHideSMSType == "false") {
                            hideAddtionalPanel();
                            hideComTypeInfo();
                            if (typeCode == SMS_TYPE_RLMR) {
                                $('#CalendarSelector').show();
                            }
                        } else {
                            if (typeCode != SMS_TYPE_RLMR && typeCode == SMS_TYPE_COMUNICATION_CODE) {
                                $("#commtype").show();
                            } else if (typeCode == SMS_TYPE_RLMR) {
                                $("#CalendarSelector").show();
                            }
                        }
                        // 20160511 Anhvd9 - Fixbug - Hiển thị thông tin bổ sung danh sách HS không vi phạm
                        if (typeCode == SMS_TYPE_RLN || typeCode == SMS_TYPE_RLT || typeCode == SMS_TYPE_RLTH || typeCode == SMS_TYPE_RLMR) {
                            $("#divTinNhanDenCacHocSinhKhongViPham").show();
                        }
                        // End 20160511

                        // 20160315 - bản tin liên quan tới kỳ thi 
                        if (typeCode == SMS_TYPE_EXAM_SCHEDULE) {
                            showAddtionalPanel(4);
                            $("span#notesText").html('Lưu ý: Lịch thi chỉ hiển thị khi kỳ thi đã lập danh sách thí sinh');
                        }
                        else if (typeCode == SMS_TYPE_EXAM_RESULT) {
                            showAddtionalPanel(4);
                            $("span#notesText").html('Lưu ý: Chỉ cho phép gửi kết quả của các kỳ thi đã chốt điểm thi');
                        }
                        // end 20160311

                        setTimeout(function () {
                            $('#SMSType option[value="' + typeid + '"]').attr('selected', 'selected');
                        }, 1000);
                        
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                        smas.hideProcessingDialog();
                    }
                });
            }
        }
            /* - Cap toan KHOI */
        else if (levelid != null && typeof levelid != undefined && levelid != '') {
            // bat co khi gui toan khoi - ap dung khi gui tin
            isSelectEducation = true;
            $(item).attr("selected", "selected");
            hideComTypeInfo();
            hideAddtionalPanel();
            $('#SMSTypeClass').hide();
            $('#SMSTypeClass').closest("td").hide();
            $("#TimerConfigClass").hide();
            $('#SMSTypeToEducationLevel').show();
            $('#SMSTypeToEducationLevel').find('option').eq(0).attr('selected', 'selected');
            $('#SMSTypeToEducationLevel option[typecode="TNTD"]').attr('selected', 'selected');
            
            $.ajax({
                url: urlLoadGrid,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { typeCode: "TNTD", EducationLevelID: levelid, TypeID: communicatetypeID, NoLoadList: true, FromDate: fromDate, ToDate: toDate },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (msg) {
                    $("#fmlstPupil").css({ 'visibility': 'visible', display: '' });
                    $('#fmlstPupil').html(msg);
                    loadEditorComplete(true);
                    smas.hideProcessingDialog();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
            /* - Cap toan TRUONG */
        else {
            educationLevelIDSelected = 0;
            isSelectEducation = true;
            $(item).attr("selected", "selected");
            hideComTypeInfo();
            hideAddtionalPanel();
            $('#SMSTypeClass').hide();
            $('#SMSTypeClass').closest("td").hide();
            $("#TimerConfigClass").hide();
            $('#SMSTypeToEducationLevel').show();
            $('#SMSTypeToEducationLevel').find('option').eq(0).attr('selected', 'selected');
            $('#SMSTypeToEducationLevel option[typecode="TNTD"]').attr('selected', 'selected');
            $.ajax({
                url: urlLoadGrid,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { typeCode: "TNTD", TypeID: communicatetypeID, NoLoadList: true, FromDate: fromDate, ToDate: toDate },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (msg) {
                    $("#fmlstPupil").css({ 'visibility': 'visible', display: '' });
                    $('#fmlstPupil').html(msg);
                    loadEditorComplete(false);
                    smas.hideProcessingDialog();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            }); // end load communicate all school
        }
    }); // End check importing
}

function LoadDataSendSMS() {
    var typecheck = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    if (typecheck == '0') {
        $("#GridPupil tbody tr").find('input:checkbox').attr({ checked: false, disabled: "disabled" }); // uncheck, disable
        $("#GridPupil tbody tr").find('textarea').attr({ value: 'Vui lòng chọn bản tin', disabled: "disabled" });
    }
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    var EducationLevelID = $(cls).attr("levelid");
    var classId = classId = $(cls).attr("classid");
    var Type = $('#SMSType').val();
    var semester_id = 0;

    var IsSignMsg = $('#AllowSignMessage').is(':checked');

    var iscustom = $('#SMSType option:selected').attr("iscustom");
    var periodType = $('#SMSType option:selected').attr("periodtype");
    var forVNEN = $('#SMSType option:selected').attr("forvnen");

    // TT30
    if (typeCode == SMS_TYPE_EVALUATION_MONTH) {
        showAddtionalPanel(2);
        if (isLoadMonth) {
            monthSelected = GetAutoMonth(semester);
            isLoadMonth = false;
        } else {
            monthSelected = $('#MonthID').val();
            semester_id = GetSemesterByMonth(monthSelected);
        }
        $('#MonthID option[value= ' + monthSelected + ']').attr("selected", "selected");

        $.ajax({
            url: urlLoadGridCustomize,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                hideComTypeInfo();
                smas.hideProcessingDialog();
                $("#fmlstPupil").html(data);
                CheckChkAllIsLoad();
                if (checkHideSMSType == "false") {
                    hideAddtionalPanel();
                } else {
                    showAddtionalPanel(2);
                }
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });

    } else if (typeCode == SMS_TYPE_EVALUATION_RESULT_SEMESTER) {
        showAddtionalPanel(1);
        isLoadMonth = false;
        semester_id = $("#SemesterID").val();
        monthSelected = 11; // truyen thang 11 lay thong tin cuoi ky
        $.ajax({
            url: urlLoadGridCustomize,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                hideComTypeInfo();
                smas.hideProcessingDialog();
                $("#fmlstPupil").html(data);
                CheckChkAllIsLoad();
                if (checkHideSMSType == "false") {
                    hideAddtionalPanel();
                } else {
                    showAddtionalPanel(1);
                }
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });

    } else if (typeCode == SMS_TYPE_COMMENTS_ENDING) {
        showAddtionalPanel(1);
        isLoadMonth = false;
        monthSelected = -1;
        semester_id = $("#SemesterID").val();
        $.ajax({
            url: urlLoadGridCustomize,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                hideComTypeInfo();
                smas.hideProcessingDialog();
                $("#fmlstPupil").html(data);
                CheckChkAllIsLoad();
                if (checkHideSMSType == "false") {
                    hideAddtionalPanel();
                } else {
                    showAddtionalPanel(1);
                }
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    } //// end TT30

    else //////////////////////// VNEN
        if (typeCode == SMS_TYPE_COMMENT_MONTH_VNEN) {
            showAddtionalPanel(2);
            monthSelected = $('#MonthID').val();
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                    if (checkHideSMSType == "false") {
                        hideAddtionalPanel();
                    } else {
                        showAddtionalPanel(2);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });

        } else if (typeCode == SMS_TYPE_RESULT_SEMESTER_VNEN) {
            hideAddtionalPanel();
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                    if (checkHideSMSType == "false") {
                        hideAddtionalPanel();
                    } else {
                        hideAddtionalPanel();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });

        } else if (typeCode == SMS_TYPE_COMMENT_ENDING_VNEN) {
            hideAddtionalPanel();
            if (semester == 1) {
                monthSelected = 15;
            } else if (semester == 2) {
                monthSelected = 16;
            }
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                   smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                    if (checkHideSMSType == "false") {
                        hideAddtionalPanel();
                    } else {
                        hideAddtionalPanel();
                    }
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        } ///// end - VNEN
        else if (typeCode == SMS_TYPE_SEMESTER_MARK || typeCode == SMS_TYPE_EXAM_ENDING_MARK) {
            SelectSemester();
        }
        //Start TT22
        else if (typeCode == SMS_TYPE_TT22_KQCK
                     || typeCode == SMS_TYPE_TT22_KQGK
                     || typeCode == SMS_TYPE_TT22_NXCK) {
            showAddtionalPanel(1);
            isLoadMonth = false;
            monthSelected = -1;
            semester_id = $("#SemesterID").val();
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, monthID: monthSelected, educationLevelID: EducationLevelID, type: Type, semester: semester_id, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                    if (checkHideSMSType == "false") {
                        hideAddtionalPanel();
                    } else {
                        showAddtionalPanel(1);
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_BNTU) {
            showAddtionalPanel(6);
            isLoadMonth = false;
            var month = parseInt((new Date()).getMonth());
            $('#ComboMonthMN option[value=' + month + ']').attr("selected", "selected");
            monthSelected = 0;
            monthSelectedMN = $("#ComboMonthMN").val();
            var week = $("#ComboWeek").val();
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_BNTH) {
            showAddtionalPanel(7);
            isLoadMonth = false;
            var month = parseInt((new Date()).getMonth());
            $('#ComboMonthMN option[value=' + month + ']').attr("selected", "selected");
            monthSelected = 0;
            monthSelectedMN = $("#ComboMonthMN").val();
            var week = 0;
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_TDN) {
            hideAddtionalPanel();
            //hideComTypeInfo();
            //$('#CalendarMNSelector').show();
            //$('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
            isLoadMonth = false;
            var dateMN = $('#DateMN').val();
            monthSelected = 0;
           
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, dateMN: dateMN, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    //hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_TDT) {
            showAddtionalPanel(8);
            isLoadMonth = false;
            //var month = parseInt((new Date()).getMonth());
            //$('#ComboMonthMN option[value=' + month + ']').attr("selected", "selected");
            monthSelected = 0;
            monthSelectedMN = $("#ComboMonthMN").val();
            var week = $("#ComboWeek2").val();

            $.ajax({
                url: urlLoadWeek,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { monthAndYear: monthSelectedMN, week: week },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    $("#ComboWeek2").empty();
                    var obj;
                    for (var i = 0; i < data.list.length; i++) {
                        obj = data.list[i];
                        if (obj.IsSelected == true) {
                            $("#ComboWeek2").append('<option name="' + obj.Value + '" value="' + obj.Key + '" selected="selected">' + obj.Value + '</option>');
                        } else {
                            $("#ComboWeek2").append('<option name="' + obj.Value + '" value="' + obj.Key + '">' + obj.Value + '</option>');
                        }
                    }
                    smas.hideProcessingDialog();

                    var week = $("#ComboWeek2").val();
                    $("#WeekNote").text(data.dateNote);
                    $.ajax({
                        url: urlLoadGridCustomize,
                        xhrFields: {
                            withCredentials: true
                        },
                        type: 'post',
                        data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                        beforeSend: function () {
                            smas.showProcessingDialog();
                        },
                        success: function (data) {
                            hideComTypeInfo();
                            smas.hideProcessingDialog();
                            $("#fmlstPupil").html(data);
                            CheckChkAllIsLoad();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                            smas.hideProcessingDialog();
                        }
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_CDSK) {
            showAddtionalPanel(7);
            isLoadMonth = false;
            //var month = parseInt((new Date()).getMonth());
            //$('#ComboMonthMN option[value=' + month + ']').attr("selected", "selected");
            monthSelected = 0;
            monthSelectedMN = $("#ComboMonthMN").val();
            var week = 0;
            console.log(EducationLevelID);
            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, monthMN: monthSelectedMN, week: week, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_DGSPT) {
            hideAddtionalPanel();
            hideComTypeInfo();

            isLoadMonth = false;
            monthSelected = 0;

            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (typeCode == SMS_TYPE_DGHDN) {
            hideAddtionalPanel();
            //hideComTypeInfo();
            //$('#CalendarMNSelector').show();
            //$('#DateMN').val(dateUtils.convertDateTime(Date.now(), 'dd/MM/yyyy'));
            isLoadMonth = false;
            var dateMN = $('#DateMN').val();
            monthSelected = 0;

            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, type: Type, semester: semester, dateMN: dateMN, isSignMsg: IsSignMsg },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    //hideComTypeInfo();
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        else if (iscustom == 'true') {
            isLoadMonth = false;
            monthSelected = 0;
            var year = 0;
            var fromDate;
            var toDate;
            if (periodType == 3) {
                if (forVNEN == 'True') {
                    showAddtionalPanel(2);
                    
                    monthSelected = $('#MonthID').val();
                    year = $('#MonthID option:selected').attr('year');
                }
            }
            else if (periodType == 4) {
                semester_id = $("#SemesterID").val();

            }
            else if (periodType == 5) {
                hideAddtionalPanel();
                //hideComTypeInfo();
                isLoadMonth = false;
                

                fromDate = $('#FromDate').val();
                toDate = $('#ToDate').val();
            }

            $.ajax({
                url: urlLoadGridCustomize,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { classID: classId, educationLevelID: EducationLevelID, monthID: monthSelected, year: year, type: Type, semester: semester_id, isSignMsg: IsSignMsg, iscustom: iscustom, fromDate: fromDate, toDate: toDate },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    if (periodType != 5) {
                        
                        hideComTypeInfo();
                    }
                    smas.hideProcessingDialog();
                    $("#fmlstPupil").html(data);
                    CheckChkAllIsLoad();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
        //End TT22
}

// check import
function CheckConfirmImporting(handler) {
    if (Imported && typeof (PopupImportedNoSend) == "function") {
        PopupImportedNoSend(handler);
    } else {
        handler();
    }
}
function ShowImportPopup() {
    smas.openDialog("SelectImport", "", {});
    // Bind close click
    $('#SelectImport div.t-window-actions').find('a.t-link').click(function () {
        smas.closeDialog("SelectImport");
    });
    // Replace name Select.. by Chọn file 
    $('#attachments').closest('div.t-upload').find('.t-button.t-upload-button').find('span:first').text('Chọn file');
}

function ShowImportPopupEdu() {
    smas.openDialog("SelectImportEdu", "", {});
    // Bind close click
    $('#SelectImportEdu div.t-window-actions').find('a.t-link').click(function () {
        smas.closeDialog("SelectImportEdu");
    });
    // Replace name Select.. by Chọn file 
    $('#SelectImportEdu #attachments').closest('div.t-upload').find('.t-button.t-upload-button').find('span:first').text('Chọn file');
}

function ExportToFile() {
    var cls = null;
    var clsArr = $('a[name="itemClass"]');
    if (clsArr.length > 0) {
        for (var i = 0; i < clsArr.length; i++) {
            if ($(clsArr[i]).hasClass('Active')) {
                cls = clsArr[i];
                break;
            }
        }
    }
    var educationLevelID = $(cls).attr("levelid");
    var ClassID = $(' .ClassStyle.Active').attr('classid');
    var ClassName = $('.ClassStyle.Active').html();
    var urlExport = '/SendSMSToParent/SendSMSToParent/Export?classID=' + ClassID
                    + '&className=' + ClassName + '&educationlevel=' + educationLevelID;

    window.open(urlExport);
    //$.ajax({
    //    url: "/SendSMSToParent/SendSMSToParent/ReadToExcel",
    //    type: 'post',
    //    data: { classID: ClassID, className: ClassName, educationlevel: educationLevelID },
    //    success: function (data) {
    //        smas.hideProcessingDialog();
            
    //        $("#frm1").attr("action", "/SendSMSToParent/SendSMSToParent/Export" + '?fileKey=' + data);
    //        $("#frm1").attr("method", "post");
    //        $("#frm1").submit();
    //    },
    //    error: function (jxhr, status, errorThrow) {
    //        smas.alert(jxhr.responseText);
    //    }
    //});
}

function ExportToFileEdu() {
  
    var urlExport = '/SendSMSToParent/SendSMSToParent/ExportAll?educationLevel=' + educationLevelIDSelected;

    window.open(urlExport);
    
}

function countNotRecursion(txt) {
    var stt = $(txt).attr("STT");
    var counter = document.getElementById('SMSCounter' + stt);
    var pupilID = $(txt).attr("pupilID");
    var content = txt.value;
    if (content == null || (typeof content) == undefined || content == '') {
        return;
    }
    var len = txt.value.length;
    if (len > 2000) {
        txt.value = txt.value.substring(0, 2000);
    }
    var prfixOrderTNTD = $(txt).val();
    var prefixSMS = PREFIX;
    var typecheck = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"] option:selected').attr("typeCode");
    if (typeCode == 'TNTD') {

        if (stringUtils.StripVNSign(txt.value.substr(0, BRAND.length)) != stringUtils.StripVNSign(BRAND)) {
            if ($('#AllowSignMessage').is(':checked')) {
                txt.value = BRAND;
                smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + BRAND });
            } else {
                txt.value = stringUtils.StripVNSign(BRAND);
                smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + stringUtils.StripVNSign(BRAND) });
            }
        }

        // cap nhat tong so tin nhan
        var ck = $("#checkedSend" + pupilID).get();
        //if ($('#SMS' + pupilID).val().length > PREFIX.length) {
        if ($('#SMS' + pupilID).val() != PREFIX) {
            $(ck).attr("checked", true);
            $(ck).removeAttr("disabled");
            // kiem tra checkall
            $("#chkAll").removeAttr("disabled");
        } else {
            $(ck).attr("checked", false);
            $(ck).attr("disabled", "disabled");
            // kiem tra check all
            if ($('input[name="checkedSend"]:disabled').length == $('input[name="checkedSend"]').length) {
                $("#chkAll").attr("checked", false);
                $("#chkAll").attr("disabled", "disabled");
            }
        }
    }
    else {
        var pupilID = $(txt).attr("pupilID");
        var template = $("#hidSMS" + pupilID).val();
        var item = txt.value;
        prefixSMS = $(txt).val();
        if (item.substr(0, prefixSMS.length).length < template.length) {
            txt.value = template;
        }
        else {
            $(txt).val(template + item.substring(template.length, item.length));
        }
        var ck = $("#checkedSend" + pupilID).get();
        if ($('#SMS' + pupilID).val().length > template.length) {
            $(ck).attr("checked", true);
            $(ck).removeAttr("disabled");
            // kiem tra checkall
            $("#chkAll").removeAttr("disabled");
        } else {
            $(ck).attr("checked", false);
            $(ck).attr("disabled", "disabled");
            // kiem tra check all
            if ($('input[name="checkedSend"]:disabled').length == $('input[name="checkedSend"]').length) {
                $("#chkAll").attr("checked", false);
                $("#chkAll").attr("disabled", "disabled");
            }
        }

    }
    var countNum = 0;
    // hien thi thong tin ve tin nhan
    if ($('#AllowSignMessage').is(':checked')) {
        var sms = len / 67;
        if (len > 70 && len % 67 > 0) sms++;
        if (counter) {
            countNum = parseInt(sms);
            counter.innerHTML = len + "/" + parseInt(sms);
        }
    } else {
        var sms = len / 160;
        if (len % 160 > 0) sms++;
        if (counter) {
            countNum = parseInt(sms);
            counter.innerHTML = len + "/" + parseInt(sms);
        }
    }
    
    //Kiem tra so luong tin nhan
    ChkChange(ck);
}

function RecordAction() {
    Action = 2;
    previousItemType = $('select[name="SMSType"]').find('option:selected').text();
}

function changeChkAllClass() {
    // Neu Checkbox toan lop dua ve trang thai ban dau
    if ($("#toanlop").is(":checked")) {
        $("#toanlop").attr("checked", false);
    } else {
        $("#toanlop").attr("checked", true);
    }
}

function loadEditorComplete(isSelectedEdu) {
    //validateUtils.lockKey('SMSContent');
    CountSMS();
    // hien thi thong tin
    var name = $('div[id^="toankhoi"][selected="selected"]').text();
    $('#InfoTitle').html(name);
    enableButtonSend(isSelectedEdu);
}

function CountSMS() {
    $('.ContentSMS #SMSContent').html(PREFIX);
    $('.Text1Style #smsCounter').html(PREFIX.length + '/' + '1');
}
function enableButtonSend(isSelectEducation) {
    typecheck = $('select[name="SMSTypeToEducationLevel"]').val();
    if (isSelectEducation != 'undefined' && isSelectEducation && typecheck == '0') {
        $('#SMSContent').attr('disabled', 'disabled');
        $('.BtnCenterSection .t-button').attr('disabled', 'disabled');
        $('.BtnCenterSection .t-button').attr('disabled', 'disabled');
    }
        //neu khong thay doi noi dung thi disable button send
    else if ($.trim($('#SMSContent').val()) == $.trim(PREFIX) || $('#numOfSMS').html() == '0') { //prefix nam o layout
        $('.BtnCenterSection .t-button').attr('disabled', 'disabled');
    }
    else {
        $('.BtnCenterSection .t-button').removeAttr('disabled');
    }
}

function SaveSuccess(msg) {
    smas.alert(msg);
    if (msg.Type == "success") {
        //Reset SMS content
        $('#SMSContent').val(PREFIX);
    }
}
function SaveFail(ajaxContext) {
    smas.alert(ajaxContext.responseText);
}

//HaiVT 04/06/2013 validate sms
function validationSMSToGroup(idTextarea, idMsg, smsCounter, detailSentSMS) {
    //fix prefix
    //preFixSendSMS = BRAND;
    //var content = $('#' + idTextarea).val();
    ////maxlength: 2000
    //if (content.length > 2000) {
    //    content = content.substring(0, 2000);
    //    $('#' + idTextarea).val(content);
    //}
    //var SubPrefix = content.substring(0, preFixSendSMS.length);
    //if (content.length < preFixSendSMS.length) {
    //    $('#' + idTextarea).val(preFixSendSMS);
    //} else if (SubPrefix != preFixSendSMS) {
    //    $('#' + idTextarea).val(preFixSendSMS + ' ' + content.substring(preFixSendSMS.length, content.length));
    //}

    //count sms
    var lengthContent = $.trim($('#' + idTextarea).val()).length;
    var sumSMS = 0;

    // Nếu là tin nhắn có dấu
    if ($('#AllowSignMsgCommunicate').is(':checked')) {
        if (lengthContent <= 70) {
            sumSMS = 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else if (lengthContent % 67 == 0) {
            sumSMS = lengthContent / 67;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else {
            sumSMS = parseInt(lengthContent / 67) + 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
    } else { // Tin nhắn KHÔNG dấu
        if (lengthContent % 160 == 0) {
            sumSMS = lengthContent / 160;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else {
            sumSMS = parseInt(lengthContent / 160) + 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
    }
    // total sms send to all subscriber
    $('#' + detailSentSMS).html(sumSubscriber * sumSMS);
}

function countSMSEdu(idTextarea, idMsg, smsCounter, detailSentSMS) {
   
    //count sms
    var lengthContent = $.trim($('#' + idTextarea).val()).length;
    var sumSMS = 0;

    // Nếu là tin nhắn có dấu
    if ($('#AllowSignMsgCommunicate').is(':checked')) {
        if (lengthContent <= 70) {
            sumSMS = 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else if (lengthContent % 67 == 0) {
            sumSMS = lengthContent / 67;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else {
            sumSMS = parseInt(lengthContent / 67) + 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
    } else { // Tin nhắn KHÔNG dấu
        if (lengthContent % 160 == 0) {
            sumSMS = lengthContent / 160;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
        else {
            sumSMS = parseInt(lengthContent / 160) + 1;
            $('#' + smsCounter).html(lengthContent + '/' + sumSMS);
        }
    }
    // total sms send to all subscriber
    $('#' + detailSentSMS).html(sumSubscriber * sumSMS);
}

function LoadWeekEdu() {
    var week = $("#ComboWeek2Edu").val();
    LoadWeek(week);
}
function LoadWeek(week) {
   
    var monthSelectedMN = $("#MonthMNEdu").val();

    $.ajax({
        url: urlLoadWeek,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        data: { monthAndYear: monthSelectedMN, week: week },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (data) {
            
            $("#ComboWeek2Edu").empty();
            var obj;
            for (var i = 0; i < data.list.length; i++) {
                obj = data.list[i];

                if (obj.IsSelected == true) {
                    $("#ComboWeek2Edu").append('<option name="' + obj.Value + '" value="' + obj.Key + '" selected="selected">' + obj.Value + '</option>');
                } else {
                    $("#ComboWeek2Edu").append('<option name="' + obj.Value + '" value="' + obj.Key + '">' + obj.Value + '</option>');
                }
            }

            $("#WeekNoteEdu").text(data.dateNote);
            smas.hideProcessingDialog();

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });
}

    function SendCommunicationSMS() {
        var typeid;
        var typeCode;
        var fromDate;
        var toDate;
        var dateMN;
        var monthMN;
        var weekMN;
        var ComboTimeMarklevel;
        var isSendImport = $("#frmMessage #IsSendImport").val();
        var importXssAnti = $("#frmMessage #ImportXssAnti").val();
        var iscustom = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("iscustom");
        var semester = $("#SemesterEdu").val();
        var year;

        if (isSelectEducation) {
            typeid = $('select[name="SMSTypeToEducationLevel"]').val();
            typeCode = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("typeCode");
            fromDate = $('#FromDate').val();
            toDate = $('#ToDate').val();
            ComboTimeMarklevel = $("#ComboTimeMarklevel").val();
            MonthIDLevel = $("#MonthIDLevel").val();
            year = $('#MonthIDLevel option:selected').attr('year');
            dateMN = $('#DateMN').val();
            monthMN = $("#MonthMNEdu").val();
            if (typeCode == SMS_TYPE_TDT) {
                weekMN = $("#ComboWeek2Edu").val();
            }
            else {
                weekMN = $("#ComboWeekEdu").val();
            }
        }
        else {
            typeid = $('#CommunicateTypeID').val();
        }

        var content = $('#SMSContent').val();
        if ((content == "" || content == undefined) && !isSendImport) {
            smas.alert({ Type: "error", Message: sendSMSError });
            return;
        }
        // Gửi có dấu
        var isSignMsg = $('#AllowSignMsgCommunicate').is(':checked');

        //Gui hen gio
        var isTimerUsed = $('#frmMessage #IsTimerUsed').is(':checked');
        var timerConfigName = $('#frmMessage #TimerConfigName').val();
        var sendDate = $('#frmMessage #SendDate').val();
        var sendTime = $('#frmMessage #SendTime').val();
        var timerConfigID = $('#frmMessage #TimerConfigID').val();
        //Gui tin nhan
        $.ajax({
            url: urlSendSMSToParent,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            data: {
                Contents: content
                , TypeID: typeid
                , IsAllLevel: true
                , EducationlevelID: educationLevelIDSelected
                , IsSendSMSToEducationLevelID: isSelectEducation
                , FromDate: fromDate
                , ToDate: toDate
                , AllowSignMessage: isSignMsg
                , IsTimerUsed: isTimerUsed
                , TimerConfigName: timerConfigName
                , SendDate: sendDate
                , SendTime: sendTime
                , TimerConfigID: timerConfigID
                , ComboTimeMarklevel: ComboTimeMarklevel
                , MonthIDLevel: MonthIDLevel
                , IsSendImport: isSendImport
                , IsCustom: iscustom
                , Semester: semester
                , ImportXSSAnti: importXssAnti
                , Year: year
                , MonthMNEdu: monthMN
                , ComboWeekEdu: weekMN
                , DateMN: dateMN
                , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            success: function (data) {
                smas.hideProcessingDialog();
                // Hien thi thong tin lop khong gui duoc 
                if (data.Type == "info") {
                    showPopupListClassNotSend(data);
                    //Reset SMS content
                    $('#SMSContent').val(PREFIX);
                    $('#smsCounter').html(PREFIX.length + '/' + 1);
                    if (isTimerUsed) {
                        $(".TitleStyle[selected=selected]").click();
                    }
                }
                else if (data.Type == "ContentError") {
                    smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { isSendAll: true, isCustom: false, isTimer: false, isTimerClass: false });
                }
                else {
                    smas.alert(data);
                    if (data.Type == "success") {
                        $(".TitleStyle[selected=selected]").click();
                    }
                }

            
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.hideProcessingDialog();
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    }

    function SendCommunicationSMSWithoutSpecialChar() {
        var typeid;
        var typeCode;
        var fromDate;
        var toDate;
        var dateMN;
        var monthMN;
        var weekMN;
        var ComboTimeMarklevel;
        var isSendImport = $("#frmMessage #IsSendImport").val();
        var importXssAnti = $("#frmMessage #ImportXssAnti").val();
        var iscustom = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("iscustom");
        var semester = $("#SemesterEdu").val();
        var year;

        if (isSelectEducation) {
            typeid = $('select[name="SMSTypeToEducationLevel"]').val();
            typeCode = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("typeCode");
            fromDate = $('#FromDate').val();
            toDate = $('#ToDate').val();
            ComboTimeMarklevel = $("#ComboTimeMarklevel").val();
            MonthIDLevel = $("#MonthIDLevel").val();
            year = $('#MonthIDLevel option:selected').attr('year');
            dateMN = $('#DateMN').val();
            monthMN = $("#MonthMNEdu").val();
            if (typeCode == SMS_TYPE_TDT) {
                weekMN = $("#ComboWeek2Edu").val();
            }
            else {
                weekMN = $("#ComboWeekEdu").val();
            }
        }
        else {
            typeid = $('#CommunicateTypeID').val();
        }

        var content = $('#SMSContent').val();
        if ((content == "" || content == undefined) && !isSendImport) {
            smas.alert({ Type: "error", Message: sendSMSError });
            return;
        }
        // Gửi có dấu
        var isSignMsg = $('#AllowSignMsgCommunicate').is(':checked');

        //Gui hen gio
        var isTimerUsed = $('#frmMessage #IsTimerUsed').is(':checked');
        var timerConfigName = $('#frmMessage #TimerConfigName').val();
        var sendDate = $('#frmMessage #SendDate').val();
        var sendTime = $('#frmMessage #SendTime').val();
        var timerConfigID = $('#frmMessage #TimerConfigID').val();
        //Gui tin nhan
        $.ajax({
            url: urlSendSMSToParent,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            data: {
                Contents: content
                , TypeID: typeid
                , IsAllLevel: true
                , EducationlevelID: educationLevelIDSelected
                , IsSendSMSToEducationLevelID: isSelectEducation
                , FromDate: fromDate
                , ToDate: toDate
                , AllowSignMessage: isSignMsg
                , IsTimerUsed: isTimerUsed
                , TimerConfigName: timerConfigName
                , SendDate: sendDate
                , SendTime: sendTime
                , TimerConfigID: timerConfigID
                , ComboTimeMarklevel: ComboTimeMarklevel
                , MonthIDLevel: MonthIDLevel
                , IsSendImport: isSendImport
                , IsCustom: iscustom
                , Semester: semester
                , ImportXSSAnti: importXssAnti
                , Year: year
                , MonthMNEdu: monthMN
                , ComboWeekEdu: weekMN
                , DateMN: dateMN
                , RemoveSpecialChar : true
                , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            success: function (data) {
                smas.hideProcessingDialog();
                smas.closeDialog("ShowErrorList");
                smas.closeDialog("ShowErrorConfirm");
                // Hien thi thong tin lop khong gui duoc 
                if (data.Type == "info") {
                    showPopupListClassNotSend(data);
                    //Reset SMS content
                    $('#SMSContent').val(PREFIX);
                    $('#smsCounter').html(PREFIX.length + '/' + 1);
                    if (isTimerUsed) {
                        $(".TitleStyle[selected=selected]").click();
                    }
                }
                else {
                    smas.alert(data);
                    if (data.Type == "success") {
                        $(".TitleStyle[selected=selected]").click();
                    }
                }


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.hideProcessingDialog();
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    }

    function OnBegin() {
        var IsOK = true;
        var content = $.trim($('#SMSContent').val());
        for (var i = 0; i < content.length; i++) {
            var KeyCode = content[i].charCodeAt();
            if (KeyCode == 60 || KeyCode == 62) {
                smas.alert({ Type: "error", Message: MessageXss });
                IsOK = false;
                return false;
            }
        };
        if (!IsOK) return IsOK;
        smas.showProcessingDialog;
        return true;
    }

    function CheckValidate(thisObject) {
        if (!validateUtils.isDate($('#FromDate').val())) {
            $('#FromDate').addClass('t-state-error');
            smas.alert({ Type: "error", Message: "Từ ngày không hợp lệ" });
            return false;
        }
        if (!validateUtils.isDate($('#ToDate').val())) {
            $('#ToDate').addClass('t-state-error');
            smas.alert({ Type: "error", Message: "Đến ngày không hợp lệ" });
            return false;
        }

        var fDate = $('#FromDate').val();
        var tDate = $('#ToDate').val();

        arrfromdate = fDate.split("/");
        arrtodate = tDate.split("/");

        var dd1 = arrfromdate[0];
        var mm1 = arrfromdate[1];
        var yyyy1 = arrfromdate[2];
        var strdate1 = yyyy1 + "-" + mm1 + "-" + dd1;
        var fromday = strdate1.split('-');

        var dd = arrtodate[0];
        var mm = arrtodate[1]; //January is 0!
        var yyyy = arrtodate[2];
        var strdate = yyyy + "-" + mm + "-" + dd;
        var CurrentDate = strdate.split('-');

        var firstDate = new Date();
        firstDate.setFullYear(fromday[0], (fromday[1] - 1), fromday[2]);

        var secondDate = new Date();
        secondDate.setFullYear(CurrentDate[0], (CurrentDate[1] - 1), CurrentDate[2]);

        if (firstDate > secondDate) {
            $('#FromDate').addClass('t-state-error');
            smas.alert({ Type: "error", Message: "Từ ngày phải nhỏ hơn Đến ngày" });
            return false;
        }

        var iscustom = $('#SMSType option:selected').attr("iscustom");
        if (iscustom == 'true') {
            LoadDataSendSMS();
            $("#CalendarSelector").show();
        }
        else {
            LoadSMSByType();
        }
    }

    function CheckValidateMN(thisObject) {
        if (!validateUtils.isDate($('#DateMN').val())) {
            $('#DateMN').addClass('t-state-error');
            smas.alert({ Type: "error", Message: "Ngày tháng không hợp lệ" });
            return false;
        }
        var cls = null;
        var clsArr = $('a[name="itemClass"]');
        if (clsArr.length > 0) {
            for (var i = 0; i < clsArr.length; i++) {
                if ($(clsArr[i]).hasClass('Active')) {
                    cls = clsArr[i];
                    break;
                }
            }
        }
        if (cls != null) {
            LoadDataSendSMS();
        }
    }

    function GetAutoMonth(semesterID) {
        var monthID = 0;
        var dateCurrent = new Date();
        var formatDate = "yyyy/MM/dd";
        var monthCurrent = dateCurrent.getMonth() + 1;
        var dateTimeNow = dateUtils.convertDateTime(dateCurrent, formatDate);

        if (semesterID == 1) {

            if (dateTimeNow <= dateUtils.convertDateTime(FirtSemesterStartDate, formatDate)) {
                monthID = 1;
            }
            else if (dateTimeNow >= dateUtils.convertDateTime(FirtSemesterEndDate, formatDate)) {
                monthID = 5;
            }
            else if (monthCurrent == 8) {
                monthID = 1;
            }
            else if (monthCurrent == 9) {
                monthID = 2;
            }
            else if (monthCurrent == 10) {
                monthID = 3;
            }
            else if (monthCurrent == 11) {
                monthID = 4;
            }
            else if (monthCurrent == 12) {
                monthID = 5;
            }
            else if (monthCurrent < 8) {
                monthID = 1;
            }
            else {
                monthID = 5;
            }
        }
        else if (semesterID == 2) {
            if (dateTimeNow <= dateUtils.convertDateTime(SecondSemesterStartDate)) {
                monthID = 6;
            }
            else if (dateTimeNow >= dateUtils.convertDateTime(SecondSemesterEndDate)) {
                monthID = 10;
            }
            else if (monthCurrent == 1) {
                monthID = 6;
            }
            else if (monthCurrent == 2) {
                monthID = 7;
            }
            else if (monthCurrent == 3) {
                monthID = 8;
            }
            else if (monthCurrent == 4) {
                monthID = 9;
            }
            else if (monthCurrent == 5) {
                monthID = 10;
            }
            else if (monthCurrent > 5) {
                monthID = 10;
            }
            else {
                monthID = 6;
            }
        }
        return monthID;
    }

    function countSMSCustomize(txt, stopped) {
        if ($(txt).attr('smsTextArea') != "1") {
            return;
        }
        var content = $(txt).val();
        if (content == null || (typeof content) == undefined) {
            return;
        }
        PREFIX = stringUtils.HtmlDecode(PREFIX_NAME);
        BRAND = stringUtils.HtmlDecode(BRAND_NAME);
        if ($.trim(content).length > 2000) {
            content = content.substring(0, 2000);
            $(txt).val(content);
        }

        var pupilID = $(txt).attr("pupilID");
        var ck = $("#checkedSend" + pupilID).get();
        var typeCode = $('select[name="SMSType"] option:selected').attr("typeCode");

        if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            var SubPrefix = content.substring(0, BRAND.length);
            //if (stringUtils.StripVNSign(SubPrefix.toUpperCase()) != stringUtils.StripVNSign(BRAND.toUpperCase())) {
            //    if (stopped != true) {
            //        if ($('#AllowSignMessage').is(':checked')) {
            //            content = BRAND + content.substring(BRAND.length, content.length);
            //            smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + BRAND });
            //        } else {
            //            content = stringUtils.StripVNSign(BRAND) + content.substring(BRAND.length, content.length);
            //            smas.alert({ Type: "error", Message: 'Nội dung tin nhắn phải bắt đầu bằng: ' + stringUtils.StripVNSign(BRAND) });
            //        }
            //    }
            //}

            var lengOfSMS = $.trim(content).length;
            var numOfSMS = 0
        
            // Nếu là tin nhắn có dấu
            if ($('#AllowSignMessage').is(':checked')) {
                if (lengOfSMS <= 70) {
                    numOfSMS = 1;
                }
                else if (lengOfSMS % 67 == 0) {
                    numOfSMS = lengOfSMS / 67;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 67) + 1;
                }
            } else { // Tin nhắn KHÔNG dấu
                if (lengOfSMS % 160 == 0) {
                    numOfSMS = lengOfSMS / 160;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 160) + 1;
                }
            }
            var trObject;
            //check toanlop-> neu dc check thi thay doi noi dung toan bo nguoc lai chi thay doi chinh no
            if ($('#toanlop').is(':checked')) {
                if (SubPrefix != PREFIX) {
                    $('#GridPupil').find('.TxtContentParent').val(content);
                }
                else {
                    $('#GridPupil').find('.TxtContentParent:gt(0)').val(content);
                }
                $('#GridPupil').find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
                //enabled button send
                if ($.trim(content) != $.trim(PREFIX)) {
                    $('.chkPupilToSend').attr('checked', 'checked');
                    $('.chkPupilToSend').removeAttr("disabled");
                    $('#chkAll').removeAttr("disabled");
                }
                else {
                    $('.chkPupilToSend').removeAttr('checked', 'checked');
                    $('.chkPupilToSend').attr("disabled", "disabled");
                    $('#chkAll').attr("disabled", "disabled");
                    $('#chkAll').removeAttr('checked', 'checked');
                }
            }
            else {
                trObject = $(txt).closest('tr');
                trObject.find('.TxtContentParent').val(content);
                trObject.find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
                if ($.trim(content) != $.trim(PREFIX)) {
                    trObject.find('.chkPupilToSend').attr('checked', 'checked');
                    trObject.find('.chkPupilToSend').removeAttr("disabled");
                }
                else {
                    trObject.find('.chkPupilToSend').removeAttr('checked', 'checked');
                    trObject.find('.chkPupilToSend').attr("disabled", "disabled");
                }
            }

            //set tong so tin nhan/check button save
            var totalSMSSend = 0;
            var numOfSMSTr;
            var numOfSubscriberTr;
            var bButtonSave = false;
            $('.chkPupilToSend:checked').each(function () {
                trObject = $(this).closest('tr');
                numOfSMSTr = trObject.find('.SMSLengthContent').text().split('/')[1];
                numOfSubscriberTr = trObject.find('.NumOfSubscriberStyle').html();
                totalSMSSend += numOfSubscriberTr * numOfSMSTr;
                if (bButtonSave == false) {
                    bButtonSave = true;
                }
            });
            $('#numOfSMS').html(totalSMSSend);
            if (bButtonSave) {
                $('#btnSend').removeAttr("disabled");
            }
            else {
                $('#btnSend').attr("disabled", "disabled");
            }
            if ($('.chkPupilToSend:enabled').length != 0) {
                if (($('.chkPupilToSend:checked').length > 0 && $('.chkPupilToSend:checked').length == $('.chkPupilToSend:enabled').length)) {
                    $('#chkAll').attr('checked', true);
                    $('#chkAll').removeAttr("disabled");
                }
                else {
                    $('#chkAll').attr('checked', false);
                }
            }
            else {
                $('#chkAll').attr('checked', false);
                $('#chkAll').attr("disabled", "disabled");
            }
        } else {
            //set content
            //if ($.trim(content).length == 0 || !content.toUpperCase().startsWith($.trim(BRAND.toUpperCase()))) {
            //    content = BRAND + content;
            //}
            $(txt).val(content);
            var lengOfSMS = $.trim(content).length;
            var numOfSMS = 0
            // Nếu là tin nhắn có dấu
            if ($('#AllowSignMessage').is(':checked')) {
                if (lengOfSMS <= 70) {
                    numOfSMS = 1;
                }
                else if (lengOfSMS % 67 == 0) {
                    numOfSMS = lengOfSMS / 67;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 67) + 1;
                }
            } else { // Tin nhắn KHÔNG dấu
                if (lengOfSMS % 160 == 0) {
                    numOfSMS = lengOfSMS / 160;
                }
                else {
                    numOfSMS = parseInt(lengOfSMS / 160) + 1;
                }
            }

            $(txt).closest('tr').find('.SMSLengthContent').html(lengOfSMS + '/' + numOfSMS);
            if ($.trim(content).length > 0) {
                $(ck).attr('checked', true);
                ReCountSMSChangeText();
            }
            else {
                $(ck).removeAttr('checked', 'checked');
                ReCountSMSChangeText();
            }
        }
    }

    //TT30
    function ReCountSMSChangeText() {
        //var sum = 0;
        //$.each($("input:checkbox[name='checkedSend']"), function () {
        //    var dis = $(this).attr('disabled');
        //    if (dis != 'disabled') {
        //        //$(this).attr("checked", obj.checked);
        //        //$("#numOfSMS").text();
        //        var part = $(this).parent().parent().find('td:eq(5)').find('span:first').text().split('/');
        //        var numOfSub = $.trim($(this).parent().parent().find('td:eq(5)').find('span:eq(1)').text());
        //        var chck = $("#chkAll").is(':checked');
        //        if (chck) {
        //            sum = parseInt(sum) + parseInt(part[1] * numOfSub);
        //            $("#numOfSMS").text(sum);
        //            if (sum == 0) {
        //                $('#btnSend').attr("disabled", "disabled");
        //            } else {
        //                $('#btnSend').removeAttr("disabled");
        //            }
        //        }
        //        else {
        //            ChkChange($(this));
        //        }
        //    }
        //});

        ChkChange();
    }

    function GetSemesterByMonth(monthID) {
        var semesterID = 0;
        if (monthID >= 1 && monthID <= 5) {
            semesterID = 1;
        } else if (monthID >= 6 && monthID <= 10) {
            semesterID = 2;
        }
        return semesterID;
    }

    function onPasteCutValidateCustomize(thisObject, isEditor) {
        setTimeout(function () {
            if (isEditor) {
                validationSMSToGroup('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
                enableButtonSend();
            } else {
                countSMSCustomize(thisObject);
            }
        }, 200);
    }

    function CheckChkAllIsLoad() {
        var countchckdisable = 0;
        $('.classChckBox').each(function () {
            countchckdisable++;
        });
        if (countChekDisable == countchckdisable) {
            $('#chkAll').attr("disabled", "disabled");
        }
    }

    //////////////// POPUP LIST CLASS NOT SEND - OUT OF MESSAGE BUDGET
    function closeDLGListClassNotSend() {
        $('.t-window').find('.t-header').find('.t-close').click();
    }

    //////////////// ONCLOSE POPUP LIST CLASS NOT SEND CALL CLOSE IFRAME FUNTION - ONCLICK BUTTON CLOSE EVENT
    function onCloseDLGListClassNotSend() {
        smas.closeDialogIframe();
        $(".TitleStyle[selected=selected]").click();
    }

    //////////////// SHOW CLASS CLASS NOT SEND
    function showPopupListClassNotSend(data) {
        smas.openDialog("DLGListClassNotSend", "", data);
        $("#DivListClassNotSend #lblTotalPHHS").html(data.TotalParent);
        $("#DivListClassNotSend #lblTotalClassNotSend").html(data.ListClass);
    }
    /// END 



    ///////////////////////////////////// SEND UNICODE MESSAGE  \\\\\\\\\\\\\\\\\
    function checkAllowSignMsgCommunicate(chk) {
        var isCheck = $(chk).is(':checked');
        var EducationLevelID = 0;
        $('div[id^="toankhoi"]').each(function () {
            var levelid = $(this).attr('levelid');
            if (levelid != null && (typeof levelid) != undefined && levelid != '' && $(this).attr('selected') == 'selected') {
                EducationLevelID = levelid;
            }
        });

        var isImport = $("#frmMessage #IsSendImport").val();
        if (isCheck) {
            $('#EditorContentInfoMsg').text(stringUtils.HtmlDecode(UNICODE_MESSAGE_NOTES));
            //Load lai so tin can gui
            $.ajax({
                url: urlLoadInfoNeedSend,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { EducationLevelID: EducationLevelID, isSignSMS: true, isImport: isImport },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    smas.hideProcessingDialog();
                    $("#SumOfSentSMS").text(data.TotalSubscriber);
                
                    if (isImport == 'false') {
                        sumSubscriber = data.TotalSubscriber;
                        validationSMSToGroup('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });

        } else {
            $('#EditorContentInfoMsg').text('');
            //Load lai so tin can gui
            $.ajax({
                url: urlLoadInfoNeedSend,
                xhrFields: {
                    withCredentials: true
                },
                type: 'post',
                data: { EducationLevelID: EducationLevelID, isSignSMS: false, isImport: isImport },
                beforeSend: function () {
                    smas.showProcessingDialog();
                },
                success: function (data) {
                    smas.hideProcessingDialog();
                    $("#SumOfSentSMS").text(data.TotalSubscriber);
                    if (isImport == 'false') {
                        sumSubscriber = data.TotalSubscriber;
                        validationSMSToGroup('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                    smas.hideProcessingDialog();
                }
            });
        }
    }

    //function onSendDateChange(e) {
    //    if (e.value != null && e.value != 'undefined') {
    //        $("#LabelSendDate").text(formatDate(e.value));
    //    }
    //};

    //function onSendTimeChange(e) {
    //    if (e.value != null && e.value != 'undefined') {
    //        $("#LabelSendTime").text(formatTime(e.value));
    //    }
    //};

    function SelectTimer(sl) {
        var timerConfigId = $(sl).val();
        var type;
        if (educationLevelIDSelected != 0) {
            type = 2;
        }
        else {
            type = 1;
        }
        $.ajax({
            url: urlLoadTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { id: timerConfigId, type: type, targetId: educationLevelIDSelected },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {

                if (data.Type == 'success') {
                    $('#frmMessage #SendDate').val(data.SendDate);
                    $('#frmMessage #SendTime').val(data.SendTime);
                    $('#frmMessage #TimerConfigName').val(data.TimerConfigName);
                
 
                    if (data.Content != '') {
                        $('#SMSContent').val(data.Content);
                    }
                    else {
                        $('#SMSContent').val(PREFIX);
                    }

                    if (timerConfigId != '') {
                        $('#frmMessage #AllowSignMsgCommunicate').attr("checked", data.SendUnicode);
                        $("#frmMessage .SendButton").hide();
                        $("#frmMessage .TimerButton").show();
                        $('#frmMessage #AllowSignMsgCommunicate').attr("disabled", "disabled");
                        countSMSEdu('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');
                        $("#btnExport").hide();
                        $("#btnImport").hide();
                   
                        if (data.IsImport!=null && data.IsImport!='undefined' && data.IsImport == true) {
                            $('#SMSContent').val(PREFIX);
                            $("#frmMessage #SMSContent").attr("disabled", "disabled");
                        
                            $("#frmMessage #importDetail").html('<b>Import nội dung tin nhắn thành công cho ' + data.Number + ' học sinh</b> <a href="#" onclick="showImportList(\'' + timerConfigId + '\')">[Xem chi tiết]</a>');
                            $("#importDetail").css("visibility", "visible");

                            $("#SumOfSentSMS").text(data.NumberSMS);
                        }
                        else {
                            $("#frmMessage #SMSContent").removeAttr("disabled");
                       
                            $("#frmMessage #IsSendImport").val(false);
                            $("#importDetail").css("visibility", "hidden");
                        }

                        isImportEdu = false;
                    }
                    else {
                        $("#frmMessage .SendButton").show();
                        $("#frmMessage .TimerButton").hide();

                        if (!isImportEdu) {

                            $('#frmMessage #AllowSignMsgCommunicate').removeAttr("disabled");
                            countSMSEdu('SMSContent', 'Msg', 'smsCounter', 'SumOfSentSMS');

                            $("#frmMessage #SMSContent").removeAttr("disabled");
                            $("#btnExport").show();
                            $("#btnImport").show();
                            $("#frmMessage #IsSendImport").val(false);
                            $("#importDetail").css("visibility", "hidden");
                        }
                        //$("#frmMessage .TimerInfo").hide();
                    }
                }
                else {
                    smas.alert(data);
                }
                smas.hideProcessingDialog();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    };

    function SelectTimerClass(sl) {
        var timerConfigId = $(sl).val();
        var cls = null;
        var clsArr = $('a[name="itemClass"]');
        if (clsArr.length > 0) {
            for (var i = 0; i < clsArr.length; i++) {
                if ($(clsArr[i]).hasClass('Active')) {
                    cls = clsArr[i];
                    break;
                }
            }
        }
        var EducationLevelID = $(cls).attr("levelid");
        var classId = $("#hidClassID").val();
        if (classId == "") {
            classId = $(cls).attr("classid");
        }

        $.ajax({
            url: urlLoadTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { id: timerConfigId, type: 3, targetId: classId },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {

                if (data.Type == 'success') {
                    $('#TimerConfigClass #SendDateClass').val(data.SendDate);
                    $('#TimerConfigClass #SendTimeClass').val(data.SendTime);
                    $('#TimerConfigClass #TimerConfigName').val(data.TimerConfigName);


                    if (timerConfigId != '') {
                        $('#IsTimerUsed').attr("checked", "checked");
                        $("#frmMessage .SendButton").hide();
                        $("#frmMessage .TimerButton").show();
                    }
                    else {
                        $('#IsTimerUsed').removeAttr("checked");
                        $("#frmMessage .SendButton").show();
                        $("#frmMessage .TimerButton").hide();
                    }

                
                }
                else {
                    smas.alert(data);
                }
                smas.hideProcessingDialog();
           
                if (timerConfigId != '') {
                    isImportClass = false;
                    LoadDataForTimer(data.SendAllClass, data.SendUnicode,true);
                }
                else {
                    if (!isImportClass) {
                        LoadDataForTimer(data.SendAllClass, $("#fmlstPupil #AllowSignMessage").is(":checked"), false);
                    }
                }
            
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });

    };

    function LoadDataForTimer(sendAllClass, isUnicode, disableUnicode) {
        var typecheck;
        var typeCode;

        typecheck = $('#SMSType').val();
        typeCode = $('#SMSType option:selected').attr("typeCode");

        if (typecheck == '0') {
            $("#GridPupil tbody tr").find('input:checkbox').attr({ checked: false, disabled: "disabled" }); // uncheck, disable
            $("#GridPupil tbody tr").find('textarea').attr({ value: 'Vui lòng chọn bản tin', disabled: "disabled" });
        }
        var cls = null;
        var clsArr = $('a[name="itemClass"]');
        if (clsArr.length > 0) {
            for (var i = 0; i < clsArr.length; i++) {
                if ($(clsArr[i]).hasClass('Active')) {
                    cls = clsArr[i];
                    break;
                }
            }
        }
        var EducationLevelID = $(cls).attr("levelid");
        var classId = $("#hidClassID").val();
        if (classId == "") {
            classId = $(cls).attr("classid");
        }

        isLoadMonth = false;
        
        $("#AddtionalPanel").show();
        $("#commtype").show();
       
        $("#AllowSignMessage").attr("checked",isUnicode);
        $("#toanlop").attr("checked", sendAllClass);
        
        //CheckAllClass(document.getElementById("toanlop"));
        //Xoa noi dung textbox
        $("#GridPupil tbody textarea").attr({ value: "" });
        
        LoadSMSByType(function () {
            if (sendAllClass) {
                $("#toanlop").attr("checked", true);
                //CheckAllClass(document.getElementById("toanlop"));
                $('#chkAll').attr("disabled", "disabled");
                $('.chkPupilToSend').attr("disabled", "disabled");
                $('#numOfSMS').html(0);
            }
            else {
                $("#toanlop").attr("checked", false);
                //CheckAllClass(document.getElementById("toanlop"));
                $('#chkAll').removeAttr("disabled");
                $('.chkPupilToSend').removeAttr("disabled");
                $('#numOfSMS').html(0);
            };
            if (disableUnicode) {
                $("#fmlstPupil #AllowSignMessage").attr("disabled", "disabled");
            }
            CheckAllClassTimer(document.getElementById("toanlop"));
            countSMSTimer();
        }, true);

    };


    function SaveTimerConfig() {
        var typeid;
        var typeCode;
        var fromDate;
        var toDate;
        var ComboTimeMarklevel;
        if (isSelectEducation) {
            typeid = $('select[name="SMSTypeToEducationLevel"]').val();
            fromDate = $('#FromDate').val();
            toDate = $('#ToDate').val();
            ComboTimeMarklevel = $("#ComboTimeMarklevel").val();
            MonthIDLevel = $("#MonthIDLevel").val();
        }
        else {
            typeid = $('#CommunicateTypeID').val();
        }

        var content = $('#SMSContent').val();
        if (content == "" || content == undefined) {
            smas.alert({ Type: "error", Message: sendSMSError });
            return;
        }
        // Gửi có dấu
        var isSignMsg = $('#AllowSignMsgCommunicate').is(':checked');

        //Gui hen gio
        var isTimerUsed = $('#frmMessage #IsTimerUsed').is(':checked');
        var timerConfigName = $('#frmMessage #TimerConfigName').val();
        var sendDate = $('#frmMessage #SendDate').val();
        var sendTime = $('#frmMessage #SendTime').val();
        var timerConfigID = $('#frmMessage #TimerConfigIDEdu').val();

        //Gui tin nhan
        $.ajax({
            url: urlSaveTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            data: {
                Contents: content
                , TypeID: typeid
                , IsAllLevel: true
                , EducationlevelID: educationLevelIDSelected
                , IsSendSMSToEducationLevelID: isSelectEducation
                , FromDate: fromDate
                , ToDate: toDate
                , AllowSignMessage: isSignMsg
                , IsTimerUsed: isTimerUsed
                , TimerConfigName: timerConfigName
                , SendDate: sendDate
                , SendTime: sendTime
                , TimerConfigID: timerConfigID
                , ComboTimeMarklevel: ComboTimeMarklevel
                , MonthIDLevel: MonthIDLevel
                , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            success: function (data) {
                smas.hideProcessingDialog();
                // Hien thi thong tin lop khong gui duoc 
                if (data.Type == "info") {
                    showPopupListClassNotSend(data);
                    //Reset SMS content
                    $('#SMSContent').val(PREFIX);
                    $('#smsCounter').html(PREFIX.length + '/' + 1);
                }
                else if (data.Type == "ContentError") {
                    smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { isSendAll: true, isCustom: false, isTimer: true, isTimerClass: false });
                }
                else {
                
                    if (data.Type == "success") {
                        smas.alert("Lưu thành công");
                        $(".TitleStyle[selected=selected]").click();
                    }
                    else {
                        smas.alert(data);
                    }
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.hideProcessingDialog();
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    };

    function SaveTimerConfigWithoutSpecialChar() {
        var typeid;
        var typeCode;
        var fromDate;
        var toDate;
        var ComboTimeMarklevel;
        if (isSelectEducation) {
            typeid = $('select[name="SMSTypeToEducationLevel"]').val();
            fromDate = $('#FromDate').val();
            toDate = $('#ToDate').val();
            ComboTimeMarklevel = $("#ComboTimeMarklevel").val();
            MonthIDLevel = $("#MonthIDLevel").val();
        }
        else {
            typeid = $('#CommunicateTypeID').val();
        }

        var content = $('#SMSContent').val();
        if (content == "" || content == undefined) {
            smas.alert({ Type: "error", Message: sendSMSError });
            return;
        }
        // Gửi có dấu
        var isSignMsg = $('#AllowSignMsgCommunicate').is(':checked');

        //Gui hen gio
        var isTimerUsed = $('#frmMessage #IsTimerUsed').is(':checked');
        var timerConfigName = $('#frmMessage #TimerConfigName').val();
        var sendDate = $('#frmMessage #SendDate').val();
        var sendTime = $('#frmMessage #SendTime').val();
        var timerConfigID = $('#frmMessage #TimerConfigIDEdu').val();

        //Gui tin nhan
        $.ajax({
            url: urlSaveTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            traditional: true,
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            data: {
                Contents: content
                , TypeID: typeid
                , IsAllLevel: true
                , EducationlevelID: educationLevelIDSelected
                , IsSendSMSToEducationLevelID: isSelectEducation
                , FromDate: fromDate
                , ToDate: toDate
                , AllowSignMessage: isSignMsg
                , IsTimerUsed: isTimerUsed
                , TimerConfigName: timerConfigName
                , SendDate: sendDate
                , SendTime: sendTime
                , TimerConfigID: timerConfigID
                , ComboTimeMarklevel: ComboTimeMarklevel
                , MonthIDLevel: MonthIDLevel
                , RemoveSpecialChar: true
                , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            },
            success: function (data) {
                smas.hideProcessingDialog();
                smas.closeDialog("ShowErrorList");
                smas.closeDialog("ShowErrorConfirm");
                // Hien thi thong tin lop khong gui duoc 
                if (data.Type == "info") {
                    showPopupListClassNotSend(data);
                    //Reset SMS content
                    $('#SMSContent').val(PREFIX);
                    $('#smsCounter').html(PREFIX.length + '/' + 1);
                }
                else {

                    if (data.Type == "success") {
                        smas.alert("Lưu thành công");
                        $(".TitleStyle[selected=selected]").click();
                    }
                    else {
                        smas.alert(data);
                    }
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.hideProcessingDialog();
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    };

    function OpenCancelDialog() {
        var timerConfigId = $("#TimerConfigIDEdu").val();
        smas.openDialog("CancelPopup", urlCancel, { timerConfigId: timerConfigId });
    }

    function OpenCancelDialogClass() {
        var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
        smas.openDialog("CancelPopup", urlCancelClass, { timerConfigId: timerConfigId });
    }
    function showImportList(id) {
        smas.openDialog("ShowImportList", urlShowImportList, { timerConfigID: id, educationLevel: educationLevelIDSelected });
    }

    function CancelTimerConfig() {
        var timerConfigId = $("#TimerConfigIDEdu").val();
        var token = $('[name=__RequestVerificationToken]').val();
        $.ajax({
            url: urlCancelTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { timerConfigId: timerConfigId, __RequestVerificationToken: token },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                smas.alert(data);
                smas.closeDialog("CancelPopup");
                //ReloadTimerConfig(true);
                if (data.Type == "success") {
                    //viethd31: uncheck checkbox
                    $(".TitleStyle[selected=selected]").click();
                }

                smas.hideProcessingDialog();
            
            
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    };
    function CancelTimerConfigClass() {
        var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
        var token = $('[name=__RequestVerificationToken]').val();
        $.ajax({
            url: urlCancelTimerConfig,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { timerConfigId: timerConfigId, __RequestVerificationToken: token },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                smas.hideProcessingDialog();
                smas.closeDialog("CancelPopup");
                // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
                smas.alert(data);
                $('#TimerConfigID').val('');
                $("#commtype").find("#isTimerUsed").attr('checked', false);
                $("#commtype").find("#isTimerUsed").change();
                var callBack = function () {
                    $("#fmlstPupil").find("#AllowSignMessage").attr('checked', false);
                    $("#fmlstPupil").find("#AllowSignMessage").change();
                };
                LoadSMSByType(callBack);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    };
    function ReloadTimerConfig() {
  
        $.ajax({
            url: urlLoadTimerConfigCombobox,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { EducationlevelID: educationLevelIDSelected },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (timers) {
                // states is your JSON array
                var $select = $('#TimerConfigIDEdu');
                $select.empty();
                $('<option>', {
                    value: ""
                }).html("[Tin mới]").appendTo($select);
                $.each(timers, function (i, timer) {
                    $('<option>', {
                        value: timer.TimerConfigID
                    }).html(timer.TimerConfigName).appendTo($select);
                });
                SelectTimer($("#TimerConfigIDEdu"));
                smas.hideProcessingDialog();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    };

    function ReloadTimerConfigClass() {

        var cls = null;
        var clsArr = $('a[name="itemClass"]');
        if (clsArr.length > 0) {
            for (var i = 0; i < clsArr.length; i++) {
                if ($(clsArr[i]).hasClass('Active')) {
                    cls = clsArr[i];
                    break;
                }
            }
        }
        var EducationLevelID = $(cls).attr("levelid");
        var classId = $("#hidClassID").val();
        if (classId == "") {
            classId = $(cls).attr("classid");
        }
        $.ajax({
            url: urlLoadTimerConfigComboboxClass,
            xhrFields: {
                withCredentials: true
            },
            type: 'post',
            data: { targetId: classId, type: 3 },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (timers) {
                // states is your JSON array
                var $select = $('#TimerConfigID');
                $select.empty();
                $('<option>', {
                    value: ""
                }).html("[Tin mới]").appendTo($select);
                $.each(timers, function (i, timer) {
                    $('<option>', {
                        value: timer.TimerConfigID
                    }).html(timer.TimerConfigName).appendTo($select);
                });
                smas.hideProcessingDialog();

                SelectTimerClass($("#TimerConfigClass #TimerConfigID"));


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    
    };

    function OnTimerCheckboxChange(cb) {
        if ($(cb).is(":checked")) {
            ReloadTimerConfig();
            $("#frmMessage .TimerInfo").show();
        }
        else {
            $(".TitleStyle[selected=selected]").click();
        }
    }

    function TimerUsedClick(cb) {

        if ($(cb).is(":checked")) {
            ReloadTimerConfigClass();

            $("#TimerConfigClass").show();
        }
        else {
            $("#TimerConfigClass").hide();
            SelectType($("#SMSTypeClass #SMSType")[0], false);
        }
    };
    function HideSMSTimerUsed()
    {
        var typeCode = $('#SMSType option:selected').attr("typeCode");
        var isChecked = $('#isTimerUsed').is(':checked');
        if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            if (isChecked) {
                $('#TimerConfigClass').show();
            }
            $('#lblTimerUsed').show();
        }
        else {
            $('#TimerConfigClass').hide();
            $('#lblTimerUsed').hide();
        }
    }
    function HideSMSTimerUsedEducation()
    {
        var typeCode = $('select[name="SMSTypeToEducationLevel"]').find('option:selected').attr("typeCode");
        var isChecked = $('#frmMessage #IsTimerUsed').is(':checked');
        if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            $('#TimerConfigEdiInput').show();
            if (isChecked) {
                $('#frmMessage .TimerInfo').show();
            }
            else {
                $('#frmMessage .TimerInfo').hide();
            }
        }
        else {
            $('#TimerConfigEdiInput').hide();
        }
    
    }

    function cancelImport()
    {
        $(".TitleStyle[selected=selected]").click();
    }


    var lstInd = 0;
    function Grid_onDataBinding(e) {
        smas.showProcessingDialog();
        var grid = $('#GridImported').data('tGrid');
        var timerConfigID = $('#GridImported').find("#hidTimerConfigID").val();
        lstInd = (grid.currentPage - 1) * grid.pageSize + 1;
        e.data =
            {
                timerConfigID : timerConfigID,
                educationLevel : educationLevelIDSelected
            }
  
    }

    function Grid_onDataBound(e) {
        smas.hideProcessingDialog();

    }