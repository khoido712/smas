﻿// css - focus textarea
$('.GeneralTable textarea, .GeneralTable input[type="text"]').focus(function () {
    $(this).parent().addClass('BgFocusInputSection');
});
$('.GeneralTable textarea, .GeneralTable input').blur(function () {
    $(this).parent().removeClass('BgFocusInputSection');
});

function omov(e) {
    var hideText = $(e).parent().find('p');
    var imgElement = $(e).parent().find('img');
    if (hideText.html() == '') {
        $.ajax({
            url: urlLoadSentSMSPupil,
            type: 'get',
            xhrFields: {
                withCredentials: true
            },
            data: { pupilID: hideText.attr('pupilID'), typeSMS: $('select[name="SMSType"]').val() },
            beforeSend: function () {
                imgElement.show();
            },
            success: function (data) {
                hideText.html(data);
                imgElement.hide();
            },
            error: function (xhr, status, error) {
                smas.alert(xhr.responseText);
                imgElement.hide();
            }
        });
    }
    $(hideText).css('display', 'block');
}

function omou(e) {
    var hideText = $(e).parent().find('p');
    $(hideText).css('display', 'none');
}

//check truong hop hoc sinh chuyen truong/ chuyen lop khi load gird
function OnLoad() {
    $.each($(this).find('tbody').find('tr'), function () {
        $(this).removeClass('t-alt');
        if (($(this).find('input')).length == 0) {
            $(this).addClass('OutOfSchollTr');
            //disable cac textbox
            $(this).find('input').attr("disabled", "disabled");
        }
    });
}

var SMS_MAX_LEN = 2000;

function ChkChange() {
    var ckItms = document.getElementsByName("checkedSend");
    var sum = 0;
    var IsCheckAll = true;
    var soluongduoccheckvaenabled = 0;
    $(ckItms).each(function () {
        var td4 = $(this).parent().parent().find('td:eq(5)');
        var part = $(td4).find('span:first').text().split('/');
        var numOfSub = $.trim($(td4).find('span:eq(1)').text());
        if ($(this).is(":checked")) {
            sum += parseInt(part[1] * numOfSub);
            if (!$(this).is(":disabled")) {
                soluongduoccheckvaenabled++;
            }
        } else {
            if (!$(this).is(":disabled")) {
                IsCheckAll = false;
            }
        }
    });
    if (IsCheckAll && soluongduoccheckvaenabled > 0) {
        $("#chkAll").attr("checked", true);
    } else {
        $("#chkAll").removeAttr("checked");
    }

    //Fill text Number of sms
    $("#numOfSMS").text(sum);
    if (sum == 0) {
        $('#btnSend').attr("disabled", "disabled");
    } else {
        $('#btnSend').removeAttr("disabled");
    }
}

function countSumSMS() {
    var sum = 0;
    $('#GridPupil table:eq(1) tr').each(function () {
        var part = $(this).find('td:eq(5)').find('span:first').text().split('/');
        var numOfSub = $.trim($(this).find('td:eq(5)').find('span:eq(1)').text());
        var partChk = $(this).find('td:eq(6)').find('input');
        var partChkHid = $(this).find('td:eq(2)').find('p');
        if (partChkHid.length > 0) {
            partChk.checked = false;
            $(partChk).attr("disabled", "disabled");
        } else if (parseInt(part[1]) == 0) {
            partChk.checked = false;
            $(partChk).attr("disabled", "disabled");
        } else {
            $(partChk).removeAttr("disabled");
            if (partChk.checked) {
                sum = parseInt(sum + parseInt(part[1] * numOfSub));
            }
        }
    });
    // checkbox all
    if ($('input[name="checkedSend"]:disabled').length == $('input[name="checkedSend"]').length) {
        $("#chkAll").attr("checked", false);
        $("#chkAll").attr("disabled", "disabled");
    }

    $('#numOfSMS').text(sum);
    if (sum == 0) {
        $('#btnSend').attr("disabled", "disabled");
    } else {
        $('#btnSend').removeAttr("disabled");
    }
}

function checkAllSend(obj) {
    var sum = 0;
    $.each($("input:checkbox[name='checkedSend']"), function () {
        var dis = $(this).attr('disabled');
        if (dis != 'disabled') {
            $(this).attr("checked", obj.checked);
            //$("#numOfSMS").text();
            var part = $(this).parent().parent().find('td:eq(5)').find('span:first').text().split('/');
            var numOfSub = $.trim($(this).parent().parent().find('td:eq(5)').find('span:eq(1)').text());
            if (obj.checked) {
                sum = parseInt(sum) + parseInt(part[1] * numOfSub);
                $("#numOfSMS").text(sum);
                if (sum == 0) {
                    $('#btnSend').attr("disabled", "disabled");
                } else {
                    $('#btnSend').removeAttr("disabled");
                }
            }
            else {
                //sum = parseInt(sum) - parseInt(part[1]);
                $("#numOfSMS").text(sum);
                if (sum == 0) {
                    $('#btnSend').attr("disabled", "disabled");
                } else {
                    $('#btnSend').removeAttr("disabled");
                }
            }
        }
    });
}

function CheckAutoSendType(isSendSMSToEducationLevel) {
    var ckSend = $("input:checkbox[name=checkedSend]:checked");
    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    var TimerConfigName = $('#TimerConfigName').val();
    if (TimerConfigName == "" || TimerConfigName == null) {
        smas.alert({ Type: "error", Message: 'Tên đợt là bắt buộc' });
        return;
    }

    var typeid = $('select[name="SMSType"]').val();
    smas.showProcessingDialog();
    $.ajax({
        url: urlCheckAutoSendSMSType,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        data: { TypeID: typeid },
        success: function (data) {
            if (data.success == true && data.IsAuto == true) {
                smas.hideProcessingDialog();
                smas.openDialog("PopupDialog", "", data.content);
                $('#tbThongTin').text(data.content);
                //bind close click
                $('#PopupDialog div.t-window-actions').find('a.t-link').click(function () {
                    smas.closeDialog("SelectImport");
                });
            } else {
                SendToParent(isSendSMSToEducationLevel);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
}


function SendToParent(isSendSMSToEducationLevel) {
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");

    //Lay cau hinh hen gio
    var timerConfigName = $("#TimerConfigClass #TimerConfigName").val();
    var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
    var sendDate = $("#TimerConfigClass #SendDateClass").val();
    var sendTime = $("#TimerConfigClass #SendTimeClass").val();
    var isTimerUsed = $("#isTimerUsed").is(":checked");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();
        var template = tItem.value;
        //Doi voi tin nhan Trao doi neu chon toan lop thi lay gia tri dau tien
        if (typeCode == SMS_TYPE_COMUNICATION_CODE && ckToanLop.checked == true) {
            content = $('#SMS' + $(ckSend[0]).attr("value")).val();
        }
        //if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            //Neu noi dung chi chua template thi bao loi noi dung tin nhan ko hop le
            if (template == content || content == "" || content == undefined) {
                $('#SMS' + ppID).css("border", "1px solid red");
                $('#SMS' + ppID).focus();

                smas.alert({ Type: "error", Message: message });
                return;
            }
        //}
        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Get ClassID
    var classID = document.getElementById("hidClassID").value;
    // Check Toan lop - If MessageType is Communication
    if (ckToanLop.checked && $('#toanlop').is(':visible') == true) {
        chkToanlop = true;
    }
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');
    var guiTinNhanDenCacHocSinhKhongViPham = $("#guiTinNhanDenCacHocSinhKhongViPham").val();
    var noiDungTinNhanGuiCacHocSinhKhongViPham = $("#noiDungTinNhanGuiCacHocSinhKhongViPham").val();
    //Gui tin nhan
    $.ajax({
        url: urlSendSMSToParent,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , IsSendSMSToEducationLevelID: isSendSMSToEducationLevel
            , guiTinNhanDenCacHocSinhKhongViPham: guiTinNhanDenCacHocSinhKhongViPham
            , noiDungTinNhanGuiCacHocSinhKhongViPham: noiDungTinNhanGuiCacHocSinhKhongViPham
            , AllowSignMessage: isSignMsg
            , IsTimerUsed: isTimerUsed
            , TimerConfigName: timerConfigName
            , SendDate: sendDate
            , SendTime: sendTime
            , TimerConfigID: timerConfigId
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();

            if (data.Type == "ContentError") {
                smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { isSendAll: false, isCustom: false, isTimer: false, isTimerClass: false });
            }
            else {
                // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
                smas.alert(data);
                // remove confirm
                unbindConfirm();
                //Refresh lai data

                var callBack = null;
                if (data.Type == "success") {
                    callBack = function () {
                        $("#fmlstPupil").find("#AllowSignMessage").attr('checked', false);
                        $("#fmlstPupil").find("#AllowSignMessage").change();
                    };
                }

                LoadSMSByType(callBack);

                if (data.Type == "success") {

                    $("#commtype").find("#isTimerUsed").attr('checked', false);
                    $("#commtype").find("#isTimerUsed").change();
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
};


function SendToParentWithoutSpecialChar(isSendSMSToEducationLevel) {
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");

    //Lay cau hinh hen gio
    var timerConfigName = $("#TimerConfigClass #TimerConfigName").val();
    var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
    var sendDate = $("#TimerConfigClass #SendDateClass").val();
    var sendTime = $("#TimerConfigClass #SendTimeClass").val();
    var isTimerUsed = $("#isTimerUsed").is(":checked");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();
        var template = tItem.value;
        //Doi voi tin nhan Trao doi neu chon toan lop thi lay gia tri dau tien
        if (typeCode == SMS_TYPE_COMUNICATION_CODE && ckToanLop.checked == true) {
            content = $('#SMS' + $(ckSend[0]).attr("value")).val();
        }
        //if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
        //Neu noi dung chi chua template thi bao loi noi dung tin nhan ko hop le
        if (template == content || content == "" || content == undefined) {
            $('#SMS' + ppID).css("border", "1px solid red");
            $('#SMS' + ppID).focus();

            smas.alert({ Type: "error", Message: message });
            return;
        }
        //}
        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Get ClassID
    var classID = document.getElementById("hidClassID").value;
    // Check Toan lop - If MessageType is Communication
    if (ckToanLop.checked && $('#toanlop').is(':visible') == true) {
        chkToanlop = true;
    }
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');
    var guiTinNhanDenCacHocSinhKhongViPham = $("#guiTinNhanDenCacHocSinhKhongViPham").val();
    var noiDungTinNhanGuiCacHocSinhKhongViPham = $("#noiDungTinNhanGuiCacHocSinhKhongViPham").val();
    //Gui tin nhan
    $.ajax({
        url: urlSendSMSToParent,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , IsSendSMSToEducationLevelID: isSendSMSToEducationLevel
            , guiTinNhanDenCacHocSinhKhongViPham: guiTinNhanDenCacHocSinhKhongViPham
            , noiDungTinNhanGuiCacHocSinhKhongViPham: noiDungTinNhanGuiCacHocSinhKhongViPham
            , AllowSignMessage: isSignMsg
            , IsTimerUsed: isTimerUsed
            , TimerConfigName: timerConfigName
            , SendDate: sendDate
            , SendTime: sendTime
            , TimerConfigID: timerConfigId
            , RemoveSpecialChar: true
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();
            smas.closeDialog("ShowErrorList");
            smas.closeDialog("ShowErrorConfirm");
            // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
            smas.alert(data);
            // remove confirm
            unbindConfirm();
            //Refresh lai data

            var callBack = null;
            if (data.Type == "success") {
                callBack = function () {
                    $("#fmlstPupil").find("#AllowSignMessage").attr('checked', false);
                    $("#fmlstPupil").find("#AllowSignMessage").change();
                };
            }

            LoadSMSByType(callBack);

            if (data.Type == "success") {

                $("#commtype").find("#isTimerUsed").attr('checked', false);
                $("#commtype").find("#isTimerUsed").change();
            }
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
};

function SendToParentCustomize() {
    smas.showProcessingDialog();
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");
    var iscustom = $('#SMSType option:selected').attr("iscustom");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();

        if (content == "" || content == undefined) {
            $('#SMS' + ppID).css("border", "1px solid red");
            $('#SMS' + ppID).focus();
            smas.alert({ Type: "error", Message: message });
            continue;
        }

        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }

    var classID = document.getElementById("hidClassID").value;
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');
    $.ajax({
        url: urlSendSMSToParentCustomize,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , AllowSignMessage: isSignMsg
            , IsCustom: iscustom
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();
            if (data.Type == "ContentError") {
                smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { isSendAll: false, isCustom: true, isTimer: false, isTimerClass: false });
            }
            else {
                // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
                smas.alert(data);
                // remove confirm
                unbindConfirm();
                //Refresh lai data
                LoadDataSendSMS();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
}

function SendToParentCustomizeWithoutSpecialChar() {
    smas.showProcessingDialog();
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");
    var iscustom = $('#SMSType option:selected').attr("iscustom");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();

        if (content == "" || content == undefined) {
            $('#SMS' + ppID).css("border", "1px solid red");
            $('#SMS' + ppID).focus();
            smas.alert({ Type: "error", Message: message });
            continue;
        }

        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }

    var classID = document.getElementById("hidClassID").value;
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');
    $.ajax({
        url: urlSendSMSToParentCustomize,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , AllowSignMessage: isSignMsg
            , IsCustom: iscustom
            , RemoveSpecialChar: true
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();
            smas.closeDialog("ShowErrorList");
            smas.closeDialog("ShowErrorConfirm");
            // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
            smas.alert(data);
            // remove confirm
            unbindConfirm();
            //Refresh lai data
            LoadDataSendSMS();
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
}

///////////////////////////////////// SEND UNICODE MESSAGE  \\\\\\\\\\\\\\\\\
function checkAllowSignMsg(chk) {

    SelectType($('#SMSType'), false);
    var isCheck = $(chk).is(':checked');
    if (isCheck) {
        $('#ContentInfoMsg').text(stringUtils.HtmlDecode(UNICODE_MESSAGE_NOTES));
    } else {
        $('#ContentInfoMsg').text('');
    }
    $('#GridPupil').find('textarea[id^="SMS"]').each(function () {
        countSMS($(this));
    });
};

function CheckAutoSendTypeTimer() {
    //var ckSend = $("input:checkbox[name=checkedSend]:checked");
    //if (ckSend.length < 1) {
    //    smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
    //    return;
    //}

    SendToParentTimer();
}


function SendToParentTimer(isSendSMSToEducationLevel) {
    smas.showProcessingDialog();
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");

    //Lay cau hinh hen gio
    var timerConfigName = $("#TimerConfigClass #TimerConfigName").val();
    var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
    var sendDate = $("#TimerConfigClass #SendDateClass").val();
    var sendTime = $("#TimerConfigClass #SendTimeClass").val();
    var isTimerUsed = $("#isTimerUsed").is(":checked");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();
        var template = tItem.value;
        //Doi voi tin nhan Trao doi neu chon toan lop thi lay gia tri dau tien
        if (typeCode == SMS_TYPE_COMUNICATION_CODE && ckToanLop.checked == true) {
            content = $('#SMS' + $(ckSend[0]).attr("value")).val();
        }
        if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            //Neu noi dung chi chua template thi bao loi noi dung tin nhan ko hop le
            if (template == content || content == "" || content == undefined) {
                $('#SMS' + ppID).css("border", "1px solid red");
                $('#SMS' + ppID).focus();

                smas.alert({ Type: "error", Message: message });
                return;
            }
        }
        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Get ClassID
    var classID = document.getElementById("hidClassID").value;
    // Check Toan lop - If MessageType is Communication
    if (ckToanLop.checked && $('#toanlop').is(':visible') == true) {
        chkToanlop = true;
    }
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');

    //Gui tin nhan
    $.ajax({
        url: urlUpdateTimerConfigClass,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , IsSendSMSToEducationLevelID: isSendSMSToEducationLevel
            , AllowSignMessage: isSignMsg
            , IsTimerUsed: isTimerUsed
            , TimerConfigName: timerConfigName
            , SendDate: sendDate
            , SendTime: sendTime
            , TimerConfigID: timerConfigId
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();
            // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
            if (data.Type == "ContentError") {
                smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { isSendAll: false, isCustom: false, isTimer: false, isTimerClass: true });
            }
            else {
                // remove confirm
                unbindConfirm();
                //Refresh lai data

                console.log(data);
                if (data.Type == "success") {
                    smas.alert("Lưu thành công");

                    var callBack = null;
                    if (data.Type == "success") {
                        callBack = function () {
                            $("#fmlstPupil").find("#AllowSignMessage").attr('checked', false);
                            $("#fmlstPupil").find("#AllowSignMessage").change();
                        };
                    }

                    LoadSMSByType(callBack);

                    if (data.Type == "success") {

                        $("#commtype").find("#isTimerUsed").attr('checked', false);
                        $("#commtype").find("#isTimerUsed").change();
                    }
                }
                else {
                    smas.alert(data);
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
};

function SendToParentTimerWithoutSpecialChar(isSendSMSToEducationLevel) {
    smas.showProcessingDialog();
    var typeid = $('select[name="SMSType"]').val();
    var typeCode = $('select[name="SMSType"]').find('option:selected').attr("typeCode");
    var ckToanLop = document.getElementById("toanlop");
    var ckSend = document.getElementsByName("checkedSend");

    //Lay cau hinh hen gio
    var timerConfigName = $("#TimerConfigClass #TimerConfigName").val();
    var timerConfigId = $("#TimerConfigClass #TimerConfigID").val();
    var sendDate = $("#TimerConfigClass #SendDateClass").val();
    var sendTime = $("#TimerConfigClass #SendTimeClass").val();
    var isTimerUsed = $("#isTimerUsed").is(":checked");

    $("#GridPupil tbody textarea").css("border", "none");

    if (ckSend.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Xu ly noi dung tin nhan
    var listContent = [];
    var lstPupilID = [];
    var chkToanlop = false;
    for (var i = 0; i < ckSend.length; i++) {
        //Tin nhan ko dc chon
        if (ckSend[i].checked == false) continue;
        //Lay PPID
        var ppID = $(ckSend[i]).attr("value");
        //Lay Content
        var contentItm = $('#SMS' + ppID);
        var tItem = document.getElementById('itemTemplate' + ppID);
        if (!tItem || !contentItm) {
            continue;
        }
        var content = $('#SMS' + ppID).val();
        var template = tItem.value;
        //Doi voi tin nhan Trao doi neu chon toan lop thi lay gia tri dau tien
        if (typeCode == SMS_TYPE_COMUNICATION_CODE && ckToanLop.checked == true) {
            content = $('#SMS' + $(ckSend[0]).attr("value")).val();
        }
        if (typeCode == SMS_TYPE_COMUNICATION_CODE) {
            //Neu noi dung chi chua template thi bao loi noi dung tin nhan ko hop le
            if (template == content || content == "" || content == undefined) {
                $('#SMS' + ppID).css("border", "1px solid red");
                $('#SMS' + ppID).focus();

                smas.alert({ Type: "error", Message: message });
                return;
            }
        }
        lstPupilID.push(ppID);
        listContent.push(content);
    }
    //Kiem tra so luong tin duoc gui
    if (lstPupilID.length < 1) {
        smas.alert({ Type: "error", Message: 'Bạn chưa chọn phụ huynh gửi' });
        return;
    }
    //Get ClassID
    var classID = document.getElementById("hidClassID").value;
    // Check Toan lop - If MessageType is Communication
    if (ckToanLop.checked && $('#toanlop').is(':visible') == true) {
        chkToanlop = true;
    }
    // Gửi có dấu
    var isSignMsg = $('#AllowSignMessage').is(':checked');

    //Gui tin nhan
    $.ajax({
        url: urlUpdateTimerConfigClass,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        traditional: true,
        data: {
            PupilIDs: lstPupilID
            , Contents: listContent
            , TypeID: typeid
            , ClassID: classID
            , IsAllClass: chkToanlop
            , IsSendSMSToEducationLevelID: isSendSMSToEducationLevel
            , AllowSignMessage: isSignMsg
            , IsTimerUsed: isTimerUsed
            , TimerConfigName: timerConfigName
            , SendDate: sendDate
            , SendTime: sendTime
            , TimerConfigID: timerConfigId
            , RemoveSpecialChar: true
            , __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            smas.hideProcessingDialog();
            smas.closeDialog("ShowErrorList");
            smas.closeDialog("ShowErrorConfirm");
            // pop up gui thanh cong toi may thue bao, bao nhieu tin nhan
            
            // remove confirm
            unbindConfirm();
            //Refresh lai data

            console.log(data);
            if (data.Type == "success") {
                smas.alert("Lưu thành công");

                var callBack = null;
                if (data.Type == "success") {
                    callBack = function () {
                        $("#fmlstPupil").find("#AllowSignMessage").attr('checked', false);
                        $("#fmlstPupil").find("#AllowSignMessage").change();
                    };
                }

                LoadSMSByType(callBack);

                if (data.Type == "success") {

                    $("#commtype").find("#isTimerUsed").attr('checked', false);
                    $("#commtype").find("#isTimerUsed").change();
                }
            }
            else {
                smas.alert(data);
            }
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    })
};

