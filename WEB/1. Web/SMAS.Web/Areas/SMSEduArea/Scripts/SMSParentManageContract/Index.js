﻿/* Page Search*/
$(document).ready(function () {
    $('.BtnDelete').hide();
    $('.BtnExcel').hide();

    // Load class
    AjaxLoadClass($('#CboEducationLevel'));
});

function searchSuccess(data) {
    if (data.Status) {
        $('#Search').html('<div id="msg" style="color: #FF0000; font-size: 11px;font-style: italic;">' + data.Message + '</div><div class="clear"></div>');
        $(".IframeContentStyle").css("height", 208);
        $("#SmsEduFrame").css("height", 206);
        $('.BtnExcel').hide();
    } else {
        $('#Search').html(data);
        $('.BtnExcel').show();
        // Redisplay information
    }
}

//////////////////////////  Search Postage success \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
function searchPostageSuccess(data) {
    $('#DeferredPaymentPopup #ContentPostage').html(data);
    GetEWallet();
}
///////////////////////////////////////////////////////////////////////////////////////////

function PrepareCreate() {
    smas.openDialog("createDialog", "", {});
    formUtils.resetForm("ContentUpdate");
}

function ExportExcel() {
    $("#ExcelPop").removeAttr("style");
    $("#ExcelPop").attr("style", "display:block");
}
function ExportOut() {
    $("#ExcelPop").removeAttr("style");
    $("#ExcelPop").attr("style", "display:none");
}

function ExportExcelRegisted() {
    document.forms['FormGridContract'].action = "/SMSEduArea/SMSParentManageContract/ExportExcelRegisted";
    document.forms['FormGridContract'].submit();
}

function ExportExtra() {
    document.forms['FormGridContract'].action = "/SMSEduArea/SMSParentManageContract/ExportExtra";
    document.forms['FormGridContract'].submit();
}

function ExportExcelRegis() {
    document.forms['FormGridContract'].action = "/SMSEduArea/SMSParentManageContract/ExportExcelRegis";
    document.forms['FormGridContract'].submit();
}
/*Page Index*/

//check is resetSearch - onclick event button search
function checkResetSearch() {
    if (isResetSearchContract) {
        $('#CurrentPage').val('');
    }
    isResetSearchContract = true;
}

function onDataBinding(e) {
   
    var grid = $('#GridSearchSMSParent').data('tGrid')
    e.preventDefault();
    $('#CurrentPage').val(e.page);
    isResetSearchContract = false;
    var count = 0;
    var message ='';
    $(".chekbox").each(function () {
        var cheked = $(this).is(":checked");
        if (cheked) {
            count++;
        }
    });
   
    if (count > 0)//duoc check tren grid
    {
        smas.openDialog("ConfirmAlertRedirect", "", null);
    }
    else
    {
        $('#BtnSearch').click();
        GetEWallet();
    }

}

function ConfirmRedirect() {
    smas.closeDialog("ConfirmAlertRedirect");
}

function OKRedirect() {
    $('#BtnSearch').click();
    smas.closeDialog("ConfirmAlertRedirect");
}

function AjaxLoadClass(cboLevel) {
    var educationLevelID = $(cboLevel).val();
    var cboClass = $(cboLevel).closest("form").find("#CboClassID");
    if (educationLevelID == "") {
        educationLevelID = -1;
        $(cboClass).attr("disabled", "disabled");
    } else {
        $(cboClass).removeAttr("disabled");
    }
    controlUtils.loadComboBox($(cboClass), urlLoadClassList, { 'educationLevelID': educationLevelID }, getAll);
}

////////////////////////////// BINDING GRID UNPAID CONTRACT ////////////////////////////
function onGridUnpaidBinding(e) {
    var grid = $('#GridUnpaidContract').data('tGrid')
    e.preventDefault();
    $('#CurrentPage').val(e.page);
}

//////////////////////////////// END  - BINDING GRID UNPAID CONTRACT
//SHOW ERROR MESSAGE - ONFAILURE SUBMIT FORM EVENT
function failureRequest(xmlHttpRequest) {
    smas.alert(xmlHttpRequest.responseText);
}

function onCancelConfirm() {
    smas.closeDialog('ConfirmAlertDialog');
    $.ajax({
        url: urlCheckout,
        data: { isCheckout: false, totalPay: 0, eWalletID: 0, quantity: 0, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        success: function (msg) {
            if (msg.message != "success") {
                // donot anything
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

// hủy Đăng ký
function onCancelRegis() {
    smas.closeDialog('ConfirmAllowDeferredPayment');

    if (IMPORTING) {
        $.ajax({
            url: urlSaveImport,
            data: { saveImport: false, eWalletID: 0, totalPay: 0, quantity: 0, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            success: function (msg) {
                if (msg.Type != "success") {
                    Search();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    } else {
        $.ajax({
            url: urlSaveRegis,
            data: { regised: false, totalPay: 0, eWalletID: 0, quantity: 0, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            success: function (msg) {
                if (msg.message != "success") {
                    Search();
                    smas.alert(msg.Message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    }
}

// cho phép Trả chậm
function enableDeferedPayment(ck) {
    if ($(ck).is(":checked")) {
        $("#ConfirmAllowDeferredPayment #OK").removeAttr("disabled");
    } else {
        $("#ConfirmAllowDeferredPayment #OK").attr("disabled", "disabled");
    }
}

/*Page Grid*/

$(document).ready(function () {
    if (count > 0) {
        $('.BtnExcel').show();
    }
});
function loadWaiting() {
    smas.showProcessingDialog();
}

// Trường hợp Lưu Đăng ký
function SaveComplete(e) {
    // Đăng ký qua qua màn hình QLHĐ
    IMPORTING = false;
    smas.hideProcessingDialog();
    var obj = eval("(" + e.responseText + ")");
    if (obj.type == 'error') {
        smas.alert({ Type: 'error', Message: obj.message });
    } else {
        // Hợp lệ
        var totalPay = obj.totalPay;
        var balance = obj.Balance;
        var eWalletID = obj.eWalletID;
        var quantityPhone = obj.quantityPhone;
        var countVT = obj.countVT;
        var countOther = obj.countOther;
        var pupilNumber = obj.PupilNumber;
        if (balance < totalPay) {
            //hien thi confirm khong du tien sau do remove session
            var confirm = messageConfirmRegisNotSuccess;
            confirm = confirm.replace('{0}', pupilNumber);
            confirm = confirm.replace('{1}', countVT);
            confirm = confirm.replace('{2}', countOther);
            confirm = confirm.replace('{3}', smas.FormatMoney(totalPay));
            confirm = confirm.replace('{4}', smas.FormatMoney(balance));
            // Bo sung tra cham
            mesg_ToDeferPayment_Ndays = mesg_ToDeferPayment_Ndays.replace('{0}', DEFERRED_PAYMENT_DAYS);
            mesg_ToDeferPayment_Notes = mesg_ToDeferPayment_Notes.replace('{0}', DEFERRED_PAYMENT_DAYS);

            // Confirm cho phép Trả chậm
            smas.confirmWithDialogName("ConfirmAllowDeferredPayment", confirm, function () {
                smas.showProcessingDialog();
                // Trường hợp chọn Trả chậm
                $.ajax({
                    url: urlSaveRegis,
                    data: { regised: true, totalPay: totalPay, eWalletID: eWalletID, quantity: quantityPhone, allowDeferredPayment: true, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
                    type: 'post',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (msg) {
                        if (msg.Type == "success") {
                            Search();
                            smas.hideProcessingDialog();
                            GetEWallet();
                            var ms = messageRegisSuccess;
                            ms = ms.replace('{0}', pupilNumber);
                            smas.warning(ms, function () { });
                        }
                        else {
                            console.log(msg);
                            smas.alert(msg);
                            smas.hideProcessingDialog();

                        }


                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            });

            // Bind information
            $("#ConfirmAllowDeferredPayment #DeferredPayment #DeferedPaymentDays").html('(' + mesg_ToDeferPayment_Ndays + ')');
            $("#ConfirmAllowDeferredPayment #DeferredPayment #DeferedPaymentNotes").html(mesg_ToDeferPayment_Notes);
            $('#DeferredChk').attr("checked", false);
            $('#ConfirmAllowDeferredPayment #OK').attr("disabled", "disabled");


        } else {
            // Nếu đủ tiền thanh toán bình thường
            var messageConfirm = messageConfirmProcess;
            messageConfirm = messageConfirm.replace('{0}', pupilNumber);
            messageConfirm = messageConfirm.replace('{1}', countVT);
            messageConfirm = messageConfirm.replace('{2}', countOther);
            messageConfirm = messageConfirm.replace('{3}', smas.FormatMoney(totalPay));
            messageConfirm = messageConfirm.replace('{4}', smas.FormatMoney(balance));
            smas.confirm(messageConfirm, function () {
                smas.showProcessingDialog();
                $.ajax({
                    url: urlSaveRegis,
                    data: { regised: true, totalPay: totalPay, eWalletID: eWalletID, quantity: quantityPhone, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
                    type: 'post',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (msg) {
                        if (msg.Type == "success") {
                            // UpdatePupilOfClassS3();//khi thuc hien them moi hop dong thanh cong thi se sync
                            //update hien thi giao dien
                            $("#TotalMoney").html(smas.FormatMoney(balance - totalPay));
                            Search();
                            smas.hideProcessingDialog();
                            GetEWallet();
                            var ms = messageRegisSuccess;
                            ms = ms.replace('{0}', pupilNumber);
                            smas.warning(ms, function () { });

                        }
                        else {
                            console.log(msg);
                            smas.alert(msg);
                            smas.hideProcessingDialog();

                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            });
        }
    }  
}

////////////////////////////////// COMPLETE CHEKOUT //////////////
function CheckoutComplete(e) {
    smas.hideProcessingDialog();
    var obj = eval("(" + e.responseText + ")");
    var totalPay = obj.totalPay;
    var balance = obj.Balance;
    var eWalletID = obj.eWalletID;
    var quantityPhone = obj.quantityPhone;
    var countVT = obj.countVT;
    var countOther = obj.countOther;
    var pupilNumber = obj.PupilNumber;
    if (balance < totalPay) {
        //hien thi confirm khong du tien sau do remove session
        var confirm = msg_SMSParentContract_CheckoutNotSuccess;
        confirm = confirm.replace('{0}', pupilNumber);
        confirm = confirm.replace('{1}', countVT);
        confirm = confirm.replace('{2}', countOther);
        confirm = confirm.replace('{3}', smas.FormatMoney(totalPay));
        confirm = confirm.replace('{4}', smas.FormatMoney(balance));
        // announce information 
        smas.warning(confirm, function () {
            smas.showProcessingDialog();
            // push request cancel checkout
            $.ajax({
                url: urlCheckout,
                data: {
                    isCheckout: false, totalPay: totalPay, eWalletID: eWalletID, quantity: quantityPhone, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
                },
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function (msg) {
                    // donot anything
                    smas.hideProcessingDialog();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                }
            });
        });
       

    } else {
        // checkout valid
        var messageConfirm = msg_SMSParentContract_ConfirmCheckout;
        messageConfirm = messageConfirm.replace('{0}', pupilNumber);
        messageConfirm = messageConfirm.replace('{1}', countVT);
        messageConfirm = messageConfirm.replace('{2}', countOther);
        messageConfirm = messageConfirm.replace('{3}', smas.FormatMoney(totalPay));
        messageConfirm = messageConfirm.replace('{4}', smas.FormatMoney(balance));

        smas.confirmWithDialogName("ConfirmAlertDialog", messageConfirm, function () {
            smas.showProcessingDialog();
            $.ajax({
                url: urlCheckout,
                data: { isCheckout: true, totalPay: totalPay, eWalletID: eWalletID, quantity: quantityPhone, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function (msg) {
                    if (msg.Type == "success") {
                        $("#TotalMoney").html(smas.FormatMoney(balance - totalPay));
                        SearchPostage();
                        smas.hideProcessingDialog();
                        GetEWallet();
                        var ms = messageRegisSuccess;
                        ms = ms.replace('{0}', pupilNumber);
                        smas.warning(ms, function () { });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                }
            });
        });
    }
}
/////////////////////////////////////////////////////////////////


function SelectTab(tab, li) {
    $("#CreateDialog").find("li a").removeClass("Active");
    $(li).addClass("Active");
    LoadTab(tab);
}
function LoadTab(tab) {
    if (tab == 1) {
        var puilName = $("#pupilName-" + iPupilID).val();
        $.ajax({
            url: urlGetDataCreate,
            data: { tab: 1, contractID: 0, phone: '', servicePackageID: iServicesPakageID },
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                $('#CreateForm').html(data);
                $("#SMSParentContractID").val(iContractDetailID);
                $("#PupilNameAdd").val(puilName);
                $("#RelationshipAdd option[value='Mother']").attr("selected", "selected");
                $("#RelationshipAdd").focus();
                if (iServicesPakageCode == 'SMSFree') {
                    $('#ServicesPackageAdd').find('option').each(function () {
                        if ($(this).text() == "SMSFree") {
                            $(this).remove();
                        }
                    });
                }
                else if (isRegis == '0') {
                    $('#ServicesPackageAdd').find('option').each(function () {
                        if ($(this).text() == "SMSFree") {
                            $(this).remove();
                        }
                    });
                }
                var servicespackageID = $("#ServicesPackageAdd").val();
                var phoneNumber = $.trim($("#PhoneReceiver").val());
                var check1 = $("#Semester1").is(":checked");
                var check2 = $("#Semester2").is(":checked");
                var priceVT = $.trim($("#SVPVT-" + servicespackageID).html());
                var priceOther = $.trim($("#SVPOther-" + servicespackageID).html());
                var maxSMS = $.trim($("#SVPMaxSMS-" + servicespackageID).html());
                var messageError = PricePaiedEditOrCretae;
                messageError = messageError.replaceAll('{0}', priceVT);
                messageError = messageError.replaceAll('{1}', priceOther);
                messageError = messageError.replaceAll('{2}', maxSMS);
                $("#PaiedSMS").html(messageError);
                $("#TotalPay").html(0);

                // Tên lớp
                var className = $("#ClassName-" + iPupilID).val();
                $("#CreateDialog").find(".t-header").find(".t-window-title").html(titeInsert + " - " + puilName + " - " + className);

                if (currentSemester == SecondSemester) {
                    // Neu HK2 thi HK1 disabled
                    $("#Semester1").attr("disabled", "disabled");
                    $("#Semester2").attr("checked", "checked");
                } else if (currentSemester == FirstSemester) {
                    $("#Semester1").attr("checked", "checked");
                    $("#Semester1").removeAttr("disabled");
                    $("#Semester2").removeAttr("checked");
                }



                smas.hideProcessingDialog();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
    else {
        $.ajax({
            url: urlGetDataCreate,
            data: { tab: 2, contractID: iContractID, phone: iPhone, servicePackageID: iServicesPakageID },
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            beforeSend: function () {
                smas.showProcessingDialog();
            },
            success: function (data) {
                $('#CreateForm').html(data);
                               
                smas.hideProcessingDialog();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
                smas.hideProcessingDialog();
            }
        });
    }
}

function OKAddSubsciber()
{
    $("#CreateDialog").find("li a").removeClass("Active");
    $($("#CreateDialog").find("li a")[0]).addClass("Active");
    LoadTab(1);

    smas.openDialog("CreateDialog", "", null);

    smas.closeDialog("ConfirmAlertAddSubsciber");
}

function ConfirmAddSubscriber()
{
    smas.closeDialog("ConfirmAlertAddSubsciber");
}

function OpenCreateOrExtraDialog(classID, pupilID, ContractDetailID, ServicesPakageID, ServicesPakageCode, IsLimitPackage, IsWholeYearPackage, isRegisSMS, contractID, phone, status) {
    iPupilID = pupilID;
    iContractDetailID = ContractDetailID;
    iServicesPakageID = ServicesPakageID;
    iServicesPakageCode = ServicesPakageCode;
    iIsWholeYearPackage = IsWholeYearPackage;
    isRegis = isRegisSMS;
    iContractID = contractID;
    iPhone = phone;
    iClassID = classID;
    var count = 0;
    var message = '';
    $(".chekbox").each(function () {
        var cheked = $(this).is(":checked");
        if (cheked) {
            count++;
        }
    });

    if (count > 0)//duoc check tren grid
    {
        smas.openDialog("ConfirmAlertAddSubsciber", "", null);
    }
    else {
        $("#CreateDialog").find("li a").removeClass("Active");
        $($("#CreateDialog").find("li a")[0]).addClass("Active");
        smas.showProcessingDialog();
        smas.openDialog("CreateDialog", "", null);
        LoadTab(1);
    }

    if (status != 1 || IsLimitPackage == 'True') {
        $($("#CreateDialog").find("li")[1]).hide();
    }
    else {
        $($("#CreateDialog").find("li")[1]).show();
    }
}

function OpenUpdateDialog(classID, pupilID, ContractDetailID, ServicesPakageID, ServicesPakageCode, IsLimitPackage, IsWholeYearPackage, isRegisSMS, contractID, phone, status) {
    var pupilName = $("#pupilName-" + pupilID).val();
    var data = {
        contractID: contractID, phone: phone, servicePackageID: ServicesPakageID, pupilID: pupilID, serviceCode: ServicesPakageCode,
        classID: classID, ContractDetailID: ContractDetailID, IsLimitPackage: IsLimitPackage, IsWholeYearPackage: IsWholeYearPackage, isRegisSMS: isRegisSMS, status: status
    };
    smas.openDialog("UpdateDialog", urlUpdateDialog, data, null);
}

function EditRow(contractID, phone, servicePackageID, pupilID, servicePackageCode) {
    var pupilName = $("#pupilName-" + pupilID).val();
    smas.showProcessingDialog();
    $.ajax({
        url: urlGetDataEdit,
        data: { contractID: contractID, phone: phone, servicePackageID: servicePackageID },
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data.type != "error") {
                if (data != null) {
                    var SubscriptionStatus = data.objResult.SubscriptionStatus;// ==3  thi disable cac control
                    var strlabelMessageEdit = strMaxSMSTotalMoney;
                    strlabelMessageEdit = strlabelMessageEdit.replaceAll("{0}", smas.FormatMoney(data.objResult.Money));
                    strlabelMessageEdit = strlabelMessageEdit.replaceAll("{1}", data.objResult.MaxSMS);
                    //do du lieu vao comobobox
                    $('#ServicesPackageEdit').find('option').remove();
                    $("#ServicesPackageEdit").append(data.objResult.ListServicePackage);
                    if (servicePackageCode != 'SMSFree') {
                        $('#ServicesPackageEdit').find('option').each(function () {
                            if ($(this).text() == "SMSFree")
                            {
                                $(this).remove();
                            }
                        });
                    }
                    $("#RelationshipEdit option[value=" + data.objResult.Relationship + "]").attr("selected", "selected");
                    $("#HfRelationshipEdit").val(data.Relationship);
                    $("#ReceiverNameEdit").val(data.objResult.ReceiverName);
                    $("#HfReceiverNameEdit").val(data.objResult.ReceiverName);
                    $("#PhoneReceiverEdit").val(data.objResult.Subscriber);
                    $("#HfPhoneReceiverEditOld").val(data.objResult.Subscriber);
                    $("#ServicesPackageEdit option[value=" + data.objResult.ServicePackageID + "]").attr("selected", "selected");
                    $("#PaiedSMSEdit").html(strlabelMessageEdit);
                    $("#PupilNameEdit").val($.trim(pupilName));
                    //ClassName
                    var className = $("#ClassName-" + pupilID).val();                    
                    $("#EditDialog").find(".t-header").find(".t-window-title").html(titelUpdate + " - " + pupilName + " - " + className);

                    if (data.objResult.Semester1 == 1) {
                        $("#Semester1Edit").attr("checked", "checked");
                        $("#UsingSemester1").html(1);
                        $("#ServicesPackageEdit").attr("disabled", "disabled");
                    }

                    if (data.objResult.Semester2 == 2) {
                        $("#Semester2Edit").attr("checked", "checked");
                        $("#UsingSemester2").html(2);
                    } else {
                        $("#Semester2Edit").removeAttr("checked");
                    }

                    if (data.objResult.Semester3 == 3) {
                        $("#Semester3Edit").attr("checked", "checked");
                        $("#UsingSemester3").html(3);
                    }
                    else {
                        $("#Semester3Edit").removeAttr("checked");
                    }

                    $("#HfServicePackage").val(servicePackageID);
                    $("#TotalPayEdit").html(0);
                    $("#SMSParentContractIDEdit").val(contractID);
                    $("#Formality").html(data.objResult.RegistrationType);
                    $("#PupilEdit").val(pupilID);
                    smas.openDialog("EditDialog", "", null);
                    $("#RelationshipEdit").focus();
                    $("#IsChangeRegis").val(data.objResult.LimitChangeFirstSemester);
                    $("#PriceMonitoring").val(data.objResult.Price);

                    $("#SubscriptionStatus").val(data.objResult.SubscriptionStatus);

                    // Trạng thái sử dụng
                    $("#NotUsing").val(data.objResult.SubscriptionStatus);

                    var pupilStatus = $("#StatusPupil-" + pupilID).val();
                    if (pupilStatus != "") {
                        pupilStatus = parseInt(pupilStatus);
                    }
                    if (pupilStatus != 1)//khac trang thai dang hoc
                    {
                        $("#NotUsing").attr("disabled", "disabled");
                        $("#btnSaveEdit").attr("disabled", "disabled");
                    } else {
                        $("#NotUsing").removeAttr("disabled");
                        $("#btnSaveEdit").removeAttr("disabled");
                    }
                    //Trang thai thue bao tam ngung su dung (1; Da dang ky; 2 chua dang ky, 3 tam ngung su dung, 4 da huy)
                    if (data.objResult.SubscriptionStatus == 3) {
                        $("#NotUsing").attr("checked", "checked");
                        $("#RelationshipEdit").attr("disabled", "disabled");
                        $("#ReceiverNameEdit").attr("disabled", "disabled");
                        $("#PhoneReceiverEdit").attr("disabled", "disabled");
                        $("#ServicesPackageEdit").attr("disabled", "disabled");
                        $("#Semester1Edit").attr("disabled", "disabled");
                        $("#Semester2Edit").attr("disabled", "disabled");
                    }
                
                    else {
                        $("#RelationshipEdit").removeAttr("disabled");
                        $("#ReceiverNameEdit").removeAttr("disabled");
                        $("#PhoneReceiverEdit").removeAttr("disabled");
                        //$("#ServicesPackageEdit").removeAttr("disabled");

                        if (data.objResult.LimitChangeFirstSemester || currentSemester == SecondSemester) {
                            $("#Semester1Edit").attr("disabled", "disabled");
                        } else {
                            $("#Semester1Edit").removeAttr("disabled");
                        }
                        $("#Semester2Edit").removeAttr("disabled");

                        // Hk2 không cho thay đổi gói cước của HK1
                        if (currentSemester == SecondSemester && data.objResult.Semester2 == 1) {
                            $("#ServicesPackageEdit").attr("disabled", "disabled");
                        } else {
                            //$("#ServicesPackageEdit").removeAttr("disabled");
                        }

                        // Neu thue bao No cuoc thi khong cho tam ngung
                        if (data.objResult.SubscriptionStatus >= 5) {
                            $("#NotUsing").attr("checked", false);
                            $("#NotUsing").attr("disabled", "disabled");
                        }

                        if (data.objResult.SubscriptionStatus == 6) {
                            if (data.objResult.Semester1 == 1) {
                                $("#Semester2Edit").attr("disabled", "disabled");
                                $("#Semester3Edit").attr("disabled", "disabled");
                            }
                            else if (data.objResult.Semester2 == 2) {
                                $("#Semester1Edit").attr("disabled", "disabled");
                                $("#Semester3Edit").attr("disabled", "disabled");
                            }
                            else if (data.objResult.Semester3 == 3) {
                                $("#Semester1Edit").attr("disabled", "disabled");
                                $("#Semester2Edit").attr("disabled", "disabled");
                            }
                        }
                    }

                    //Neu goi cuoc het han su dung
                    if (data.objResult.ExpiryDate) {
                        $("#Semester1Edit").attr("disabled", "disabled");
                        $("#Semester2Edit").attr("disabled", "disabled");
                        $("#ServicesPackageEdit option[value='" + servicePackageID + "']").attr("selected", "selected");
                    }

                    if (data.objResult.Semester3 == 3) {
                        $("#Semester1Edit").attr("disabled", "disabled");
                        $("#Semester2Edit").attr("disabled", "disabled");
                        $("#Semester3Edit").attr("disabled", "disabled");
                    }

                    smas.hideProcessingDialog();
                }
            } else {
                smas.alert({ Type: 'error', Message: data.message });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

function GetDataDefaultNotUsing(obj) {
    var relationship = $("#HfRelationshipEdit").val();
    $("#RelationshipEdit option[value='" + relationship + "']").attr("selected", "selected");
    $("#ReceiverNameEdit").val($("#HfReceiverNameEdit").val());
    $("#PhoneReceiverEdit").val($("#HfPhoneReceiverEditOld").val());
    var servicePackageID = $("#HfServicePackage").val();
    $("#ServicesPackageEdit option[value='" + servicePackageID + "']").attr("selected", "selected");

    var priceVT = $.trim($("#SVPVT-" + servicePackageID).html());
    var priceOther = $.trim($("#SVPOther-" + servicePackageID).html());
    var maxSMS = $.trim($("#SVPMaxSMS-" + servicePackageID).html());
    //var messageError = PricePaiedEditOrCretae;
    //messageError = messageError.replaceAll('{0}', priceVT);
    //messageError = messageError.replaceAll('{1}', priceOther);
    //messageError = messageError.replaceAll('{2}', maxSMS);

    var strlabelMessage = strMaxSMSTotalMoney;
    var semester1 = $("#UsingSemester1").html();
    var semeseter2 = $("#UsingSemester2").html();
    if (semester1 != "") {
        semester1 = parseInt(semester1);
    }
    if (semeseter2 != "") {
        semeseter2 = parseInt(semeseter2);
    }
    if (semester1 == FirstSemester) {
        $("#Semester1Edit").attr("checked", "checked");
    } else {
        $("#Semester1Edit").removeAttr("checked");
    }
    if (semeseter2 == SecondSemester) {
        $("#Semester2Edit").attr("checked", "checked");
    } else {
        $("#Semester2Edit").removeAttr("checked");
    }
    var ischecked = $(obj).is(":checked");
    if (ischecked) {
        $("#NotUsing").attr("checked", "checked");
        $("#RelationshipEdit").attr("disabled", "disabled");
        $("#ReceiverNameEdit").attr("disabled", "disabled");
        $("#PhoneReceiverEdit").attr("disabled", "disabled");
        $("#ServicesPackageEdit").attr("disabled", "disabled");
        $("#Semester1Edit").attr("disabled", "disabled");
        
        $("#Semester2Edit").attr("disabled", "disabled");
        $("#TotalPayEdit").html(0);

        if (smas.CheckPhoneVT($("#HfPhoneReceiverEditOld").val())) {
            strlabelMessage = strlabelMessage.replaceAll("{0}", priceVT);
            strlabelMessage = strlabelMessage.replaceAll("{1}", maxSMS);
            $("#PaiedSMSEdit").html(strlabelMessage);
        }
        else {
            strlabelMessage = strlabelMessage.replaceAll("{0}", priceOther);
            strlabelMessage = strlabelMessage.replaceAll("{1}", maxSMS);
            $("#PaiedSMSEdit").html(strlabelMessage);
        }
    } else {
        $("#RelationshipEdit").removeAttr("disabled");
        $("#ReceiverNameEdit").removeAttr("disabled");
        $("#PhoneReceiverEdit").removeAttr("disabled");
        $("#ServicesPackageEdit").removeAttr("disabled");
        $("#Semester2Edit").removeAttr("disabled");

        if (currentSemester == SecondSemester) {
            $("#Semester1Edit").attr("disabled", "disabled");
            $("#ServicesPackageEdit").attr("disabled", "disabled");
        } else {
            if ($.trim($("#IsChangeRegis").val()) == "true") {
                $("#Semester1Edit").attr("disabled", "disabled");
            } else {
                $("#Semester1Edit").removeAttr("disabled");
            }
            $("#ServicesPackageEdit").removeAttr("disabled");
        }
    }
}

function OnCloseDialogCreate() {
    smas.closeDialog("UpdateDialog");
    ResetDialogCreate();
}

function OnCloseDialogEdit() {
    smas.closeDialog("UpdateDialog");
    ResetDialogEdit();
}

function LoadMaxSMS() {
    var servicespackageID = $("#ServicesPackageAdd").val();
    var phoneNumber = $.trim($("#PhoneReceiver").val());
    var check1 = $("#Semester1").is(":checked");
    var check2 = $("#Semester2").is(":checked");
    var check3 = $("#Semester3").is(":checked");
    var priceVT = $.trim($("#SVPVT-" + servicespackageID).html());
    var priceOther = $.trim($("#SVPOther-" + servicespackageID).html());
    var maxSMS = $.trim($("#SVPMaxSMS-" + servicespackageID).html());
    var isWholeYearPackage = $.trim($("#SVPIsWholeYear-" + servicespackageID).html());
    var messageError = PricePaiedEditOrCretae;
    messageError = messageError.replaceAll('{0}', priceVT);
    messageError = messageError.replaceAll('{1}', priceOther);
    messageError = messageError.replaceAll('{2}', maxSMS);
    
    if (phoneNumber != "") {
        if (smas.CheckPhoneNumber(phoneNumber)) {
            if (check1 || check2 || check3) {
                var strlabelMessage = strMaxSMSTotalMoney;
                //lay so tin nhan goi cuoc va tien cua goi cuoc
                maxSMS = $.trim($("#SVPMaxSMS-" + servicespackageID).html());
                var Price = 0;
                if (smas.CheckPhoneVT(phoneNumber)) {
                    Price = $.trim($("#SVPVT-" + servicespackageID).html());
                } else {
                    Price = $.trim($("#SVPOther-" + servicespackageID).html());
                }
                strlabelMessage = strlabelMessage.replaceAll("{0}", Price);
                strlabelMessage = strlabelMessage.replaceAll("{1}", maxSMS);
                $("#PaiedSMS").html(strlabelMessage)
                $("#MoneyByServicespackage").html(Price.replaceAll(".", ""));
                //kiem tra co check hoc ky thi moi tinh tien
                if (check3 || (check1 && check2 && currentSemester == 1) || isWholeYearPackage == 'True') {
                    Price = Price.replace(".", "");
                    $("#TotalPay").html(smas.FormatMoney(Price * 2));
                }
                else if (check1) {
                    $("#TotalPay").html(smas.FormatMoney(Price));
                }
                else if (check2) {
                    $("#TotalPay").html(smas.FormatMoney(Price));
                } else {
                    $("#TotalPay").html(0);
                }
            } else {
                smas.alert({ Type: 'error', Message: messgeSubscriberTimeEmpty });
                $("#TotalPay").html(0);
            }
        } else {
            $("#PaiedSMS").html(messageError);
            smas.alert({ Type: 'error', Message: messagePhoneInValid });
            $("#TotalPay").html(0);
        }
    } else {
        smas.alert({ Type: 'error', Message: messagePhoneEmpty });
        $("#PaiedSMS").html(messageError);
        $("#TotalPay").html(0);
    }

    $("#ExtraAllClass").change();
};

function OnSemester1Change(cb) {
    if ($("#Semester1").is(":checked") && $("#Semester2").is(":checked")) {
        
        $("#Semester1").prop("checked", false);
        $("#Semester2").prop("checked", false);
        $("#Semester1").attr("disabled", "disabled");
        $("#Semester2").attr("disabled", "disabled");
        $("#Semester3").prop("checked", true);
    }
   
    LoadMaxSMS();
}
function OnSemester2Change(cb) {
    if ($("#Semester1").is(":checked") && $("#Semester2").is(":checked")) {
       
        $("#Semester1").prop("checked", false);
        $("#Semester2").prop("checked", false);
        $("#Semester1").attr("disabled", "disabled");
        $("#Semester2").attr("disabled", "disabled");
        $("#Semester3").prop("checked", true);
    }
    LoadMaxSMS();
}

function OnSemester3Change(cb) {
    if ($(cb).is(":checked")) {
        $("#Semester1").prop("checked", false);
        $("#Semester2").prop("checked", false);
        $("#Semester1").attr("disabled", "disabled");
        $("#Semester2").attr("disabled", "disabled");
    }
    else {
        if (currentSemester == 1) {
            $("#Semester1").removeAttr("disabled");
        }
        $("#Semester2").removeAttr("disabled");
    }
    LoadMaxSMS();
}


function LoadMaxSMSEdit() {
    var servicespackageID = $("#ServicesPackageEdit").val();
    var phoneNumber = $.trim($("#PhoneReceiverEdit").val());
    var check1 = $("#Semester1Edit").is(":checked");
    var check2 = $("#Semester2Edit").is(":checked");
    //lay thoi gian da dang ky
    var semester1 = $.trim($("#UsingSemester1").html());
    var semester2 = $.trim($("#UsingSemester2").html());
    var checkdayregis = $.trim($("#IsChangeRegis").val());
    var _Price = 0;
    var serviceIDOld = $("#HfServicePackage").val();
    var phoneOld = $.trim($("#HfPhoneReceiverEditOld").val());
    var priceVT = $.trim($("#SVPVT-" + servicespackageID).html());
    var priceOther = $.trim($("#SVPOther-" + servicespackageID).html());
    var maxSMS = $.trim($("#SVPMaxSMS-" + servicespackageID).html());
    var messageError = PricePaiedEditOrCretae;
    messageError = messageError.replaceAll('{0}', priceVT);
    messageError = messageError.replaceAll('{1}', priceOther);
    messageError = messageError.replaceAll('{2}', maxSMS);
    if (phoneNumber != "") {
        if (smas.CheckPhoneNumber(phoneNumber)) {

            var Price = 0;
            if (smas.CheckPhoneVT(phoneNumber)) {
                Price = $.trim($("#SVPVT-" + servicespackageID).html());
            } else {
                Price = $.trim($("#SVPOther-" + servicespackageID).html());
            }
            var strlabelMessage = strMaxSMSTotalMoney;
            // Thong bao: Don gia + so tin nhan goi cuoc duoc chon
            strlabelMessage = strlabelMessage.replaceAll("{0}", Price);
            strlabelMessage = strlabelMessage.replaceAll("{1}", maxSMS);
            $("#PaiedSMSEdit").html(strlabelMessage);

            var giamanhinhcu = $("#PriceMonitoring").val();
            var giamanhinhmoi = 0;
            Price = Price.replaceAll(".", "");
            if (check1 && check2) {
                giamanhinhmoi = Price * 2;
            }
            if (check1 && !check2 || !check1 && check2) {
                giamanhinhmoi = Price;
            }
            giamanhinhmoi = parseInt(giamanhinhmoi);
            giamanhinhcu = parseInt(giamanhinhcu);
            var giachung = 0;
            if (giamanhinhmoi > giamanhinhcu) {
                giachung = giamanhinhmoi - giamanhinhcu;
            }

            // Neu la thue bao no cuoc thi khong tru tien
            if ($("#SubscriptionStatus").val() >= 5) {
                giachung = 0;
            }
            $("#TotalPayEdit").html(smas.FormatMoney(giachung));
            $("#MoneyByServicespackageEdit").html(giachung);

            //kiem tra neu con du 30 ngay hoc ki 1 va ma uncheck HK1 check HK2 thi khong tinh tien
            // qua 30 ngay
            if (checkdayregis == "true") {
                //tinh tien
                $("#TotalPayEdit").html(smas.FormatMoney(giachung));
                $("#MoneyByServicespackageEdit").html(giachung);
            } else {

                //khong tinh tien
                if (servicespackageID == serviceIDOld) {
                    if (semester1 == 1 && semester2 != 2) {
                        if (check2) {
                            $("#TotalPayEdit").html(smas.FormatMoney(giachung));
                            $("#MoneyByServicespackageEdit").html(giachung);
                        } else {
                            $("#TotalPayEdit").html(0);
                            $("#MoneyByServicespackageEdit").html(0);
                        }
                    } else {
                        $("#TotalPayEdit").html(smas.FormatMoney(giachung));
                        $("#MoneyByServicespackageEdit").html(giachung);
                    }
                } else {
                    $("#TotalPayEdit").html(smas.FormatMoney(giachung));
                    $("#MoneyByServicespackageEdit").html(giachung);
                }

                //thay doi so dien thoai
                if (phoneOld != phoneNumber)
                {
                    $("#TotalPayEdit").html(smas.FormatMoney(giachung));
                    $("#MoneyByServicespackageEdit").html(giachung);
                }

            }
        } else {
            smas.alert({ Type: 'error', Message: messagePhoneInValid });
            $("#PaiedSMSEdit").html(messageError);
            $("#TotalPayEdit").html(0);
        }
    } else {
        smas.alert({ Type: 'error', Message: messagePhoneEmpty });
        $("#PaiedSMSEdit").html(messageError);
        $("#TotalPayEdit").html(0);
    }
}

function ResetDialogCreate() {
    $("#RelationshipAdd option[value='Mother']").attr("selected", "selected");
    $("#ReceiverName").val('');
    $("#PhoneReceiver").val('');
    $('#ServicesPackageAdd option:first-child').attr("selected", "selected");
    $("#Semester1").removeAttr("checked");
    $("#Semester2").removeAttr("checked");
    $("#TotalPay").html("");
    $("#MoneyByServicespackage").html('');
    $("#SMSParentContractID").html('');
    $("#PaiedSMS").html('');
}

function ResetDialogEdit() {
    $("#RelationshipEdit option[value='Mother']").attr("selected", "selected");
    $("#ReceiverNameEdit").val('');
    $("#PhoneReceiverEdit").val('');
    $('#ServicesPackageEdit option:first-child').attr("selected", "selected");
    $("#Semester1Edit").removeAttr("checked");
    $("#Semester2Edit").removeAttr("checked");
    $("#TotalPayEdit").html("");
    $("#MoneyByServicespackageEdit").html('');
    $("#SMSParentContractIDEdit").html('');
    $("#HfPhoneReceiverEditOld").val('');
    $("#UsingSemester1").html('');
    $("#UsingSemester2").html('');
    $("#PaiedSMSEdit").html('');
    $("#NotUsing").removeAttr("checked");
    $("#HfServicePackage").val('');
}

function OnCloseDialogConfirm() {
    smas.closeDialog("ConfirmDialog");
}

// Thêm thuê bao cho 1 Hợp đồng
function AddNewSubsciber() {
    var semester1Check = $("#Semester1").is(":checked");
    var semester1 = -1;
    if (semester1Check) {
        semester1 = 1;
    }

    var semester2Check = $("#Semester2").is(":checked");
    var semester2 = -1;
    if (semester2Check) {
        semester2 = 2;
    }

    var semester3Check = $("#Semester3").is(":checked");
    var semester3 = -1;
    if (semester3Check) {
        semester3 = 3;
    }

    var phone = $.trim($("#PhoneReceiver").val());
    //keim tra so dien thoai
    if (phone == "") {
        smas.alert({ Type: 'error', Message: messagePhoneEmpty });
        return false;
    } else if (!semester1Check && !semester2Check && !semester3Check) {
        smas.alert({ Type: 'error', Message: messgeSubscriberTimeEmpty });
        return false;
    } else if (!smas.CheckPhoneNumber(phone)) {
        smas.alert({ Type: 'error', Message: messageCreatePhoneError });
        return false;
    }
    else {
        //so sanh so tien tai khoan so du va so can phai thanh toan
        //So du tai khoan
        var isWholeYearPackage = $.trim($("#SVPIsWholeYear-" + $("#ServicesPackageAdd").val()).html());
        var Balance = $.trim($("#TotalMoney").html());
        Balance = Balance.replaceAll(".", "");
        Balance = parseInt(Balance);
        //So tien can phai thanh toan
        var PayMoney = $.trim($("#MoneyByServicespackage").html());
        //tinh tien khi chon 2 hoc ky
        if (semester3 == 3 || (semester1 == 1 && semester2 == 2 && currentSemester == 1) || isWholeYearPackage == 'True') {
            PayMoney = PayMoney * 2;
        }
        PayMoney = parseInt(PayMoney);
        if (Balance < PayMoney) {
            var confirm = confirmAdd;
            confirm = confirm.replace('{0}', smas.FormatMoney(PayMoney));
            confirm = confirm.replace('{1}', smas.FormatMoney(Balance));
            smas.warning(confirm, function () {
            });
            return false;
        } else {
            //neu so du tai khoan lon hon so tien can thanh toan thi se hien popup thong bao co muon thanh toan hay khong
            var messageConfirm = confirmAddSuccess;
            messageConfirm = messageConfirm.replace('{0}', smas.FormatMoney(PayMoney));
            messageConfirm = messageConfirm.replace('{1}', smas.FormatMoney(Balance));
            var token = $('#FormGridContract').find('input[name="__RequestVerificationToken"]').val();
            smas.confirm(messageConfirm, function () {
                var objInsert = {
                    RelationshipAdd: $("#RelationshipAdd").val(),
                    ReceiverName: $.trim($("#ReceiverName").val()),
                    PhoneReceiver: phone,
                    ServicesPackageAdd: $("#ServicesPackageAdd").val(),
                    Semester1: semester1,
                    Semester2: semester2,
                    Semester3: semester3,
                    SMSParentContactID: $.trim($("#SMSParentContractID").val()),
                    Money: PayMoney,
                    EwalletID: $("#EWallet").val(),
                    PupilName: $.trim($("#PupilNameAdd").val()),
                    __RequestVerificationToken: token
                };
                smas.showProcessingDialog();
                $.ajax({
                    url: urlSaveAddNewSubscriber,
                    data: objInsert,
                    type: 'post',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (msg) {
                        if (msg.type == "success") {
                            $(".SideBarListMenu li a.Active").click();
                            Search();
                            smas.alert({ Type: 'success', Message: msg.message });
                            GetEWallet();
                            smas.hideProcessingDialog();
                        } else
                            if (msg.type == "error") {
                                smas.alert({ Type: 'error', Message: msg.message });
                                smas.hideProcessingDialog();
                            }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            });
            return true;
        }
    }
};

function AddExtra() {
    var semester1Check = $("#Semester1").is(":checked");
    var semester1 = 'False';
    if (semester1Check) {
        semester1 = 'True';
    }

    var semester2Check = $("#Semester2").is(":checked");
    var semester2 = 'False';
    if (semester2Check) {
        semester2 = 'True';
    }

    var semester3Check = $("#Semester3").is(":checked");
    var semester3 = 'False';
    if (semester3Check) {
        semester3 = 'True';
    }

    var isAllClassCheck = $("#ExtraAllClass").is(":checked");
    var isAllClass = 'False';
    if (isAllClassCheck) {
        isAllClass = 'True';
    }

    var extraServicePackageID = $("#ServicesPackageAdd").val();
    if (extraServicePackageID == '') {
        smas.alert({ Type: 'error', Message: "Chưa chọn gói cước" });
        return false;
    }

    var phone = $.trim($("#PhoneReceiver").val());
    //keim tra so dien thoai
    if (phone == "") {
        smas.alert({ Type: 'error', Message: messagePhoneEmpty });
        return false;
    } else if (!semester1Check && !semester2Check && !semester3Check) {
        smas.alert({ Type: 'error', Message: messgeSubscriberTimeEmpty });
        return false;
    } else if (!smas.CheckPhoneNumber(phone)) {
        smas.alert({ Type: 'error', Message: messageCreatePhoneError });
        return false;
    }
    else {
        //so sanh so tien tai khoan so du va so can phai thanh toan
        //So du tai khoan
        var Balance = $.trim($("#TotalMoney").html());
        Balance = Balance.replaceAll(".", "");
        Balance = parseInt(Balance);
        //So tien can phai thanh toan
        var PayMoney;
        if (!$("#ExtraAllClass").is(":checked")) {
            PayMoney = $.trim($("#MoneyByServicespackage").html());
            //tinh tien khi chon 2 hoc ky
            if (semester3 == 'True' || (semester1 == 'True' && semester2 == 'True' && currentSemester == 1)) {
                PayMoney = PayMoney * 2;
            }
            PayMoney = parseInt(PayMoney);
        }
        else {
            PayMoney = $.trim($("#TotalPayForAll #TotalPay").html());
            PayMoney = PayMoney.replaceAll(".", "");
            PayMoney = PayMoney.replaceAll("VNĐ", "");
            PayMoney = $.trim(PayMoney);
            PayMoney = parseInt(PayMoney);
            console.log(PayMoney);
            console.log($.trim($("#TotalPayForAll #TotalPay").html()));
        }

        if (Balance < PayMoney) {
            var confirm = confirmAdd;
            confirm = confirm.replace('{0}', smas.FormatMoney(PayMoney));
            confirm = confirm.replace('{1}', smas.FormatMoney(Balance));
            smas.warning(confirm, function () {
            });
            return false;
        } else {
            //neu so du tai khoan lon hon so tien can thanh toan thi se hien popup thong bao co muon thanh toan hay khong
            var messageConfirm = confirmAddSuccess;
            messageConfirm = messageConfirm.replace('{0}', smas.FormatMoney(PayMoney));
            messageConfirm = messageConfirm.replace('{1}', smas.FormatMoney(Balance));
            var token = $('#FormGridContract').find('input[name="__RequestVerificationToken"]').val();
                
            smas.confirm(messageConfirm, function () {
                var objInsert = {
                    classId: iClassID,
                    contractID: iContractID,
                    phone: iPhone,
                    servicePackageID: iServicesPakageID,
                    extraServicePackageID: extraServicePackageID,
                    isAllClass: isAllClass,
                    semester1: semester1,
                    semester2: semester2,
                    semester3: semester3,
                    Money: PayMoney,
                    EwalletID: $("#EWallet").val(),
                    __RequestVerificationToken: token
                };
                smas.showProcessingDialog();
                $.ajax({
                    url: urlSaveAddExtra,
                    data: objInsert,
                    type: 'post',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (msg) {
                        if (msg.type == "success") {
                            $(".SideBarListMenu li a.Active").click();
                            Search();
                            smas.alert({ Type: 'success', Message: msg.message });
                            GetEWallet();
                            smas.hideProcessingDialog();
                        } else
                            if (msg.type == "error") {
                                smas.alert({ Type: 'error', Message: msg.message });
                                smas.hideProcessingDialog();
                            }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });
            });
            return true;
            
        }
    }
}

// Trường hợp Sửa trên 1 Hợp đồng
function SaveEdit() {
    var semester1Check = $("#Semester1Edit").is(":checked");
    var semester1 = -1;
    if (semester1Check) {
        semester1 = 1;
    }

    var semester2Check = $("#Semester2Edit").is(":checked");
    var semester2 = -1;
    if (semester2Check) {
        semester2 = 2;
    }

    var semester3Check = $("#Semester3Edit").is(":checked");
    var semester3 = -1;
    if (semester3Check) {
        semester3 = 3;
    }

    var phone = $.trim($("#PhoneReceiverEdit").val());
    //kiem tra so dien thoai
    if (phone == "") {
        smas.alert({ Type: 'error', Message: messagePhoneEmpty });
        return false;
    } else if (!semester1Check && !semester2Check && !semester3Check) {
        smas.alert({ Type: 'error', Message: messgeSubscriberTimeEmpty });
        return false;
    } else {
        var _notusing = $("#NotUsing").val();
        if (_notusing == 3) {
            _notusing = 1;
        }
        if ($("#NotUsing").is(":checked")) {
            _notusing = 3; // tam ngung su dung
        }
        //so du tai khoan
        var Balance = $.trim($("#TotalMoney").html());
        if (Balance != '') {
            Balance = Balance.replaceAll(".", "");
            Balance = parseInt(Balance);
        }
        //so tien thanh toan them
        var PayEdit = $.trim($("#TotalPayEdit").html());
        PayEdit = PayEdit.replace(".", "");
        if (PayEdit != '') {
            PayEdit = PayEdit.replaceAll(".", "");
            PayEdit = parseInt(PayEdit);
        }
        if (Balance < PayEdit) {
            //hien popup confirm
            var confirm = MessageUpdateContactFail;
            confirm = confirm.replace('{0}', smas.FormatMoney(PayEdit));
            confirm = confirm.replace('{1}', smas.FormatMoney(Balance));
            smas.warning(confirm);
            smas.closeDialog("EditDialog");
            ResetDialogEdit();
            return false;
        } else {
            var pupilStatus = $("#StatusPupil-" + $("#PupilEdit").val()).val();
            if (pupilStatus != "") {
                pupilStatus = parseInt(pupilStatus);
            }
            if (PayEdit > 0) {
                LoadMaxSMSEdit();
                //hien thong bao tai khoan         
                var messageConfirm = MessgaeUpdateContractConfirm;
                messageConfirm = messageConfirm.replace('{0}', smas.FormatMoney(PayEdit));
                messageConfirm = messageConfirm.replace('{1}', smas.FormatMoney(Balance));
                smas.confirm(messageConfirm, function () {
                    smas.showProcessingDialog();
                    var objEdit = {
                        Relationship: $("#RelationshipEdit").val(),
                        ReceiverName: $.trim($("#ReceiverNameEdit").val()),
                        Subscriber: $.trim($("#PhoneReceiverEdit").val()),
                        HfSubscriber: $.trim($("#HfPhoneReceiverEditOld").val()),
                        ServicePackageID: $("#ServicesPackageEdit").val(),
                        Semester1: semester1,
                        Semester2: semester2,
                        Semester3: semester3,
                        SMSParentContractID: $.trim($("#SMSParentContractIDEdit").val()),
                        Money: $("#MoneyByServicespackageEdit").html(),
                        SubscriptionStatus: _notusing,
                        UsedSemester1: $.trim($("#UsingSemester1").html()),
                        UsedSemester2: $.trim($("#UsingSemester2").html()),
                        PupilName: $.trim($("#PupilNameEdit").val()),
                        StatusPupil: pupilStatus,
                        PupilID: $("#PupilEdit").val(),
                        __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
                    };

                    $.ajax({
                        url: urlSaveEdit,
                        data: objEdit,
                        type: 'post',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: function (msg) {
                            if (msg.type != "error") {
                                $("#CurrentPhoneUpdate").val($.trim($("#PhoneReceiverEdit").val()));
                                $(".SideBarListMenu li a.Active").click();
                                Search();
                                smas.alert({ Type: 'success', Message: msg.message });
                                GetEWallet();
                            }
                            else {
                                smas.alert({ Type: 'error', Message: msg.message });
                            }
                            smas.hideProcessingDialog();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            smas.alert(XMLHttpRequest.responseText);
                        }
                    });
                });
                return true;
            } else {
                smas.showProcessingDialog();
                LoadMaxSMSEdit();
                var objEdit = {
                    Relationship: $("#RelationshipEdit").val(),
                    ReceiverName: $.trim($("#ReceiverNameEdit").val()),
                    Subscriber: $.trim($("#PhoneReceiverEdit").val()),
                    HfSubscriber: $.trim($("#HfPhoneReceiverEditOld").val()),
                    ServicePackageID: $("#ServicesPackageEdit").val(),
                    Semester1: semester1,
                    Semester2: semester2,
                    Semester3: semester3,
                    SMSParentContractID: $.trim($("#SMSParentContractIDEdit").val()),
                    Money: $("#MoneyByServicespackageEdit").html(),
                    SubscriptionStatus: _notusing,
                    UsedSemester1: $.trim($("#UsingSemester1").html()),
                    UsedSemester2: $.trim($("#UsingSemester2").html()),
                    PupilName: $.trim($("#PupilNameEdit").val()),
                    StatusPupil: pupilStatus,
                    PupilID: $("#PupilEdit").val(),
                    __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
                };

                $.ajax({
                    url: urlSaveEdit,
                    data: objEdit,
                    type: 'post',
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (msg) {
                        console.log("aaaa");
                        console.log(msg);
                        if (msg.type != "error") {
                            $("#CurrentPhoneUpdate").val($.trim($("#PhoneReceiverEdit").val()));
                            $(".SideBarListMenu li a.Active").click();
                            Search();
                            smas.alert({ Type: 'success', Message: msg.message });
                            GetEWallet();
                        }
                        else {
                            smas.alert({ Type: 'error', Message: msg.message });
                        }
                        smas.hideProcessingDialog();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        smas.alert(XMLHttpRequest.responseText);
                    }
                });

                return true;
            }
        }
    }
}

function DeleteRow(contractID, phone, classID, pupilID, servicePackageID) {
    smas.confirm(messageDeleteContractDetail, function () {
        $.ajax({
            url: urlDeleteContract,
            data: { contractID: contractID, phone: phone, classID: classID, pupilID: pupilID, servicePackageID: servicePackageID, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            success: function (msg) {
                if (msg.message != "success") {
                    Search();                    
                    smas.alert({ Type: 'success', Message: msg.message });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                smas.alert(XMLHttpRequest.responseText);
            }
        });
    });
}

function ClassChange(cbClass) {
    var className = $(cbClass).find("option:selected").text();
    $(cbClass).closest("form").find("#HfClassName").val(className);
    $(cbClass).closest("form").find("#HfSClassName").val(className);
}

function ShowDialogImport() {
    smas.openDialog("SelectImport", "", null);
    // Bind to close popup (close in Frame)
    $('#SelectImport div.t-header a.t-link').unbind('click').click(function () {
        smas.closeDialog("SelectImport");
    });
}

function Search() {
    $("#BtnSearch").click();
}

function SearchPostage() {
    $("#BtnSearchPostage").click();
}

var phoneOld = '';
function OnFocusCheckValidate(obj, pupilID, index) {
    phoneOld = $(obj).val();
}

function CheckValidatePhoneGrid(obj, pupilID, index) {
    var phone = $(obj).val();
    $(obj).val($.trim(phone));
    if (phone != "") {
        if (!smas.CheckPhoneNumber(phone)) {
            $("#MessageError-" + pupilID + "-" + index).html(messagevalidatephone);
            return false;
        } else {
            $("#MessageError-" + pupilID + "-" + index).html('&nbsp;');
            var serviceID = $("#ServicesPackage-" + pupilID + "-" + index).val();
            var isdisableds1 = $("#Semester1-" + pupilID + "-" + index).is(":disabled");
            var ischeckS1 = false;
            if (!isdisableds1)
            {
                ischeckS1 = $("#Semester1-" + pupilID + "-" + index).is(":checked");
            }

            var isdisableds2 = $("#Semester2-" + pupilID + "-" + index).is(":disabled");
            var ischeckS2 = false;
            if (!isdisableds2) {
                ischeckS2 = $("#Semester2-" + pupilID + "-" + index).is(":checked");
            }
            if (serviceID > 0 && (ischeckS1 || ischeckS2)) {
                $("#Register-" + pupilID + "-" + index).attr("checked", "checked");
            }
        }
    } else {
        $("#MessageError-" + pupilID + "-" + index).html(messageValidatePhoneEmpty);
    }
}

function CheckPhoneOld(phone, serviceID, pupilID, index) {
    //tru di so dien thoai
    var CountnumberVT = $.trim($("#Viettel-" + serviceID).html());
    var CountnumberOther = $.trim($("#Other-" + serviceID).html());

    if (smas.CheckPhoneVT(phone)) {
        //Neu la so viettel
        $("#Viettel-" + serviceID).html(CountnumberVT - 1);
    } else {
        //Khong phai so viettel
        $("#Other-" + serviceID).html(CountnumberOther - 1);
    }
}

function CountPhoneNumber() {
    var VTcount = 0;
    var Othercount = 0;
    var count = 0;
    $(".chekbox").each(function () {
        var cheked = $(this).is(":checked");
        if (cheked) {
            count++;
            var pupilID = $(this).closest("tr").find(".pupilID").val();
            var phone = $.trim($(this).closest("tr").find(".PhoneReceiver").val());
            var servicePackageID = $(this).closest("tr").find(".ServicesPackage").val();
            var checkSemester1 = $(this).closest("tr").find(".Semester1").is(":checked");
            var checkSemester2 = $(this).closest("tr").find(".Semester2").is(":checked");
            if ((checkSemester1 || checkSemester2) && phone.length > 0 && servicePackageID > 0) {
                if (smas.CheckPhoneVT(phone)) {
                    VTcount += 1;
                    $("#Viettel-" + servicePackageID).html(VTcount);
                } else {
                    Othercount += 1;
                    $("#Other-" + servicePackageID).html(Othercount);
                }
            }
        }
    });

    if (count == 0) {
        $(".CountDescriptiorViettel").html(0);
        $("#CountParent").html(0);
        $("#PayMoney").html(0);
    }

}

function CheckValidSubmit() {
    var count = 0;
    var isSubmit = true;
    var message = '';
    $(".chekbox").each(function () {
        var cheked = $(this).is(":checked");
        if (cheked) {
            var pupilID = $(this).closest("tr").find(".pupilID").val();
            var pupilName =$(this).parents('tr').last().find(".pupilName").val();
            var phone = $.trim($(this).closest("tr").find(".PhoneReceiver").val());
            var servicePackageID = $(this).closest("tr").find(".ServicesPackage").val();
            var checkSemester1 = $(this).closest("tr").find(".Semester1").is(":checked");
            var checkSemester2 = $(this).closest("tr").find(".Semester2").is(":checked");
            var checkSemester3 = $(this).closest("tr").find(".Semester3").is(":checked");
            if (!smas.CheckPhoneNumber(phone)) {
                message = messageValidateButtonSaveGrid;
                message = message.replaceAll('{0}', phone);
                message = message.replaceAll('{1}', pupilName);
                isSubmit = false;
                return false;
            } else if (!checkSemester1 && !checkSemester2 && !checkSemester3) {
                message = messageValidateSubscritioTime;
                message = message.replaceAll('{0}', phone);
                message = message.replaceAll('{1}', pupilName);
                isSubmit = false;
                return false;
            } else {
                count++;
                isSubmit = true;
            }
        }
    });
    if (!isSubmit) {       
        smas.alert({ Type: 'error', Message: message });
        return false;
    } else if (count == 0) {        
        smas.alert({ Type: 'error', Message: 'Thầy cô phải chọn danh sách các thuê bao để thực hiện đăng ký' });
        return false;
    } else {
        //  $("#FormGridContract-submit").click();
        document.forms['FormGridContract'].action = "/SMSEduArea/SMSParentManageContract/Save";
        $("#FormGridContract-submit").click();
        return true;
    }
}

//////////////  CHECK OUT UNPAID CONTRACT   //////////////////
function ValidateCheckoutSubmit() {
    var count = 0;
    var isSubmit = true;
    var message = '';
    $(".chekbox").each(function () {
        var cheked = $(this).is(":checked");
        if (cheked) {
            count++;
            isSubmit = true;
        }
    });
    if (!isSubmit) {
        smas.alert({ Type: 'error', Message: message });
        return false;
    } else if (count == 0) {
        smas.alert({ Type: 'error', Message: 'Thầy cô phải chọn danh sách các thuê bao để thực hiện thanh toán' });
        return false;
    } else {
        document.forms['FormGridUnpaidContract'].action = "/SMSEduArea/SMSParentManageContract/PayPostage";
        $("#FormGridUnpaidContract-submit").click();
        return true;
    }
}

//////////////////////// END - CHECK OUT UNPAID CONTRACT ///////////////////////////////
function UpdatePupilOfClassS3() {
    $.ajax({
        url: urlUpdatePupilOfClass,
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        success: function (msg) {
            if (msg.Type == "success") {
                //update thanh cong se khong lam gi nua ca
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

function ValidateForm() {
    var Price = 0;
    var totalPrice = 0;
    var countVT = 0;
    var countOther = 0;
    var countRegis = 0;
    var balance = $.trim($("#Balance").val()).replaceAll(".", "");
    $(".CountDescriptiorViettel").html(0);
    $(".chekbox").each(function () {
        var pupilID = $(this).closest("tr").find(".pupilID").val();
        var pupilName = $(this).closest("tr").find(".pupilName").val();
        var phone = $.trim($(this).closest("tr").find(".PhoneReceiver").val());
        var servicePackageID = $(this).closest("tr").find(".ServicesPackage").val();
        var iswholeYearPackage = $(this).closest("tr").find(".ServicesPackage").find(":selected").attr("wholeyear");
        var checkSemester1 = $(this).closest("tr").find(".Semester1").is(":checked");
        var checkSemester2 = $(this).closest("tr").find(".Semester2").is(":checked");
        var checkSemester3 = $(this).closest("tr").find(".Semester3").is(":checked");
        servicePackageID = parseInt(servicePackageID);      
        var cheked = $(this).is(":checked");
        if (cheked) {
            Price = 0;
            countVT = 0;
            countOther = 0;
            countRegis++;            
            //neu la so viettel           
            if (phone != "") {
                if (smas.CheckPhoneNumber(phone)) {
                    $(this).closest("tr").find(".ClassMessage").html("&nbsp;");
                    if (smas.CheckPhoneVT(phone)) {
                        //lay gia goi cuoc theo sdt
                        Price = $.trim($("#SVPVT-" + servicePackageID).html());
                        Price = Price.replaceAll(".", "");
                        Price = parseInt(Price);
                        if (checkSemester3 || iswholeYearPackage == "True") {
                            totalPrice += Price * 2;
                        } else if (checkSemester1 || checkSemester2) {
                            totalPrice += Price;
                        }

                        if ((checkSemester1 || checkSemester2 || checkSemester3) && servicePackageID > 0) {
                            //lay gia tri                       
                            countVT = $.trim($("#Viettel-" + servicePackageID).html());
                            if (countVT == "" || countVT == "0") {
                                countVT = 0;
                            }
                            countVT = parseInt(countVT);
                            // Cong them gia tri
                            $("#Viettel-" + servicePackageID).html(countVT + 1);
                        }
                    }
                    else {
                        //lay gia goi cuoc theo sdt
                        Price = $.trim($("#SVPOther-" + servicePackageID).html());
                        Price = Price.replaceAll(".", "");
                        Price = parseInt(Price);
                        if (checkSemester3 || iswholeYearPackage == "True") {
                            totalPrice += Price * 2;
                        } else if (checkSemester1 || checkSemester2) {
                            totalPrice += Price;
                        }

                        if ((checkSemester1 || checkSemester2 || checkSemester3) && servicePackageID > 0) {
                            //lay gia tri

                            countOther = $.trim($("#Other-" + servicePackageID).html());
                            if (countOther == "" || countOther == "0") {
                                countOther = 0;
                            }
                            countOther = parseInt(countOther);
                            // Cong them gia tri
                            $("#Other-" + servicePackageID).html(countOther + 1);
                        }
                    }
                } else {
                    $(this).closest("tr").find(".ClassMessage").html(messagevalidatephone);
                }
            } else {
                $(this).closest("tr").find(".ClassMessage").html(messageValidatePhoneEmpty);
            }
        } else {
            $(this).closest("tr").find(".ClassMessage").html("&nbsp;");
        }
    });
    // cac thong tin chung
    $("#CountParent").html(countRegis);
    $("#PayMoney").html(smas.FormatMoney(totalPrice));
    $("#TotalMoney").html(smas.FormatMoney(balance));
}

//////////////////////// DISPLAY INFORMATION - CHECK OUT UNPAID CONTRACT ///////////////////////////////
function DisplayCheckoutInfo() {
    var Price = 0;
    var totalPrice = 0;
    var countVT = 0;
    var countOther = 0;
    var countCheckout = 0;
    var balance = $.trim($("#DeferredPaymentPopup #Balance").val()).replaceAll(".", "");
    $("#DeferredPaymentPopup .CountDescriptiorViettel").html(0);
    $("#DeferredPaymentPopup #PayMoney").html('');
    $("#DeferredPaymentPopup #TotalMoney").html('');
    $("#DeferredPaymentPopup .chekbox").each(function () {
        var index = $(this).val();
        var pupilID = $(this).closest(".tmpPupilRow").find(".pupilID").val();
        var pupilName = $(this).closest(".tmpPupilRow").find(".pupilName").val();
        var contractDetailID = $("#SMSParentContractDetailID-" + pupilID + "-" + index).val();
        var phone = $.trim($("#DeferredPaymentPopup #PhoneReceiver-" + contractDetailID).html());
        var servicePackageID = parseInt($("#ServicesPackage-" + contractDetailID).val());      
        var Semester = $("#Semester-" + contractDetailID).val();
        if ($(this).is(":checked")) {
            Price = 0;
            countVT = 0;
            countOther = 0;
             
            if (smas.CheckPhoneNumber(phone)) {
                countCheckout++;
                $(this).closest(".tmpPupilRow").find(".ClassMessage").html("&nbsp;");
                if (smas.CheckPhoneVT(phone)) { //neu la so viettel  
                    Price = $.trim($("#DeferredPaymentPopup #SVPVT-" + servicePackageID).html());
                    Price = Price.replaceAll(".", "");
                    Price = parseInt(Price);
                    // Kiem tra so luong goi cuoc
                    countVT = $.trim($("#DeferredPaymentPopup #Viettel-" + servicePackageID).html());
                    if (countVT == "" || countVT == "0") {
                        countVT = 0;
                    }
                    countVT = parseInt(countVT);
                    if (Semester == 3) {
                        totalPrice += Price * 2;
                    } else if (Semester == 1 || Semester == 2) {
                        totalPrice += Price;
                    }
                    $("#DeferredPaymentPopup #Viettel-" + servicePackageID).html(countVT + 1);
                }
                else
                {
                    Price = $.trim($("#DeferredPaymentPopup #SVPOther-" + servicePackageID).html());
                    Price = Price.replaceAll(".", "");
                    Price = parseInt(Price);
                    countOther = $.trim($("#DeferredPaymentPopup #Other-" + servicePackageID).html());
                    if (countOther == "" || countOther == "0") {
                        countOther = 0;
                    }
                    countOther = parseInt(countOther);
                    if (Semester == 3) {
                        totalPrice += Price * 2;
                    } else if (Semester == 1 || Semester == 2) {
                        totalPrice += Price;  
                    }
                    $("#DeferredPaymentPopup #Other-" + servicePackageID).html(countOther + 1);
                }
            } else {
                $(this).closest(".tmpPupilRow").find(".ClassMessage").html(messagevalidatephone);
            }
           
        } else {
            $(this).closest(".tmpPupilRow").find(".ClassMessage").html("&nbsp;");
        }
    });
    // cac thong tin chung
    $("#DeferredPaymentPopup #CountParent").html(countCheckout);
    $("#DeferredPaymentPopup #PayMoney").html(smas.FormatMoney(totalPrice));
    $("#DeferredPaymentPopup #TotalMoney").html(smas.FormatMoney(balance));
}
////////////////////////    END - DISPLAY INFORMATION - CHECK OUT UNPAID CONTRACT ///////////////////////////////

function CheckAll() {
    var isCheckAll = $("#RegisAll").is(":checked");
    if (isCheckAll) {
        $(".chekbox").attr("checked", "checked");
    } else {
        $(".chekbox").removeAttr("checked");
    }
    ValidateForm();
}

//////////////  CHECK OUT ALL UNPAID CONTRACT   //////////////////
function CheckoutAll() {
    var isCheckAll = $("#ChkoutAll").is(":checked");
    if (isCheckAll) {
        $(".chekbox").attr("checked", "checked");
    } else {
        $(".chekbox").removeAttr("checked");
    }
    DisplayCheckoutInfo();
}

function CheckoutItem() {
    var countItem = $(".chekbox").length;
    var countEach = 0;
    $(".chekbox").each(function () {
        var checkedItem = $(this).is(":checked");
        if (checkedItem) {
            countEach++;
        }
    });

    if (countEach != countItem) {
        $("#ChkoutAll").removeAttr("checked");
    } else {
        $("#ChkoutAll").attr("checked", "checked");
    }
    DisplayCheckoutInfo();
}
//////////////  END - CHECK OUT ALL UNPAID CONTRACT   //////////////////

function CheckChangeItem() {
    var countItem = $(".chekbox").length;
    var countEach = 0;
    $(".chekbox").each(function () {
        var checkedItem = $(this).is(":checked");
        if (checkedItem) {
            countEach++;
        }
    });

    if (countEach != countItem) {
        $("#RegisAll").removeAttr("checked");
    } else {
        $("#RegisAll").attr("checked", "checked");
    }
    ValidateForm();
}

function ChangeSemeseter1All() {
    var chckAll = $("#CheckAllSemeseter1").is(":checked");
    if (chckAll) {
        $(".Semester1").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).attr("checked", "checked");
                ChangeSemesterItem1($(this)[0]);
            }
        });
    } else {
        $(".Semester1").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).removeAttr("checked");
                ChangeSemesterItem1($(this)[0]);
            }
        });
    }
    ValidateForm();
}

function ChangeSemeseter2All() {
    var chckAll = $("#CheckAllSemeseter2").is(":checked");
    if (chckAll) {
        $(".Semester2").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).attr("checked", "checked");
                ChangeSemesterItem2($(this)[0]);
                //$(this).click();
            }
        });
    } else {
        $(".Semester2").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).removeAttr("checked");
                ChangeSemesterItem2($(this)[0]);
                //$(this).click();
            }
        });
    }
    ValidateForm();
}

function ChangeSemeseter3All() {
    var chckAll = $("#CheckAllSemeseter3").is(":checked");
    if (chckAll) {
        $(".Semester3").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).attr("checked", "checked");
                ChangeSemesterItem3($(this)[0]);
            }
        });
    } else {
        $(".Semester3").each(function () {
            var isdisabled = $(this).is(":disabled")
            if (!isdisabled) {
                $(this).removeAttr("checked");
                ChangeSemesterItem3($(this)[0]);
            }
        });
    }
    ValidateForm();
}

function updateCheckAll(sem) {
    if (sem == 1) {
        var countItem1 = 0;
        $(".Semester1").each(function () {
            var checkdisabled = $(this).is(":disabled")
            if (!checkdisabled) {
                countItem1++;
            }
        });

        var countEach = 0;
        $(".Semester1").each(function () {
            var itemChecked = $(this).is(":checked");
            var _checkdisabled = $(this).is(":disabled")
            if (itemChecked && !_checkdisabled) {
                countEach++;
            }
        });

        if (countItem1 == countEach && countItem1 > 0) {
            $("#CheckAllSemeseter1").attr("checked", "checked");
            
        } else {
            $("#CheckAllSemeseter1").removeAttr("checked");
        }
    }
    else if (sem == 2) {
        var countItem1 = 0;
        $(".Semester2").each(function () {
            var checkdisabled = $(this).is(":disabled")
            if (!checkdisabled) {
                countItem1++;
            }
        });

        var countEach = 0;
        $(".Semester2").each(function () {
            var itemChecked = $(this).is(":checked");
            var _checkdisabled = $(this).is(":disabled")
            if (itemChecked && !_checkdisabled) {
                countEach++;
            }
        });

        if (countItem1 == countEach && countItem1 > 0) {
            $("#CheckAllSemeseter2").attr("checked", "checked");
            
        } else {
            $("#CheckAllSemeseter2").removeAttr("checked");
        }
    }
    else {
        var countItem1 = 0;
        $(".Semester3").each(function () {
            var checkdisabled = $(this).is(":disabled")
            if (!checkdisabled) {
                countItem1++;
            }
        });

        var countEach = 0;
        $(".Semester3").each(function () {
            var itemChecked = $(this).is(":checked");
            var _checkdisabled = $(this).is(":disabled")
            if (itemChecked && !_checkdisabled) {
                countEach++;
            }
        });

        if (countItem1 == countEach && countItem1 > 0) {
            $("#CheckAllSemeseter3").attr("checked", "checked");

        } else {
            $("#CheckAllSemeseter3").removeAttr("checked");
        }
    }
}
function ChangeSemesterItem1(cb) {
    if ($(cb).closest("tr").find(".Semester1").is(":checked") && $(cb).closest("tr").find(".Semester2").is(":checked")) {
        $(cb).closest("tr").find(".Semester1").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester2").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester1").prop("checked", false);
        $(cb).closest("tr").find(".Semester2").prop("checked", false);
        $(cb).closest("tr").find(".Semester3").prop("checked", true);
        updateCheckAll(1);
        updateCheckAll(2);
        updateCheckAll(3);
    }
    else {
        updateCheckAll(1);
    }

    ValidateForm();
}

function ChangeSemesterItem2(cb) {
    if ($(cb).closest("tr").find(".Semester1").is(":checked") && $(cb).closest("tr").find(".Semester2").is(":checked")) {
        $(cb).closest("tr").find(".Semester1").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester2").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester1").prop("checked", false);
        $(cb).closest("tr").find(".Semester2").prop("checked", false);
        $(cb).closest("tr").find(".Semester3").prop("checked", true);
        updateCheckAll(1);
        updateCheckAll(2);
        updateCheckAll(3);
    }
    else {
        updateCheckAll(2);
    }
    
    ValidateForm();
}

function ChangeSemesterItem3(cb) {
    //dem so chckbox hien
    updateCheckAll(3);

    //Kiem tra neu check ca nam thi bo check ky 1 va ky 2
    if ($(cb).is(":checked")) {
        $(cb).closest("tr").find(".Semester1").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester1").prop("checked", false);
        $(cb).closest("tr").find(".Semester2").attr("disabled", "disabled");
        $(cb).closest("tr").find(".Semester2").prop("checked", false);
    }
    else {
        if (currentSemester == FirstSemester) {
            $(cb).closest("tr").find(".Semester1").removeAttr("disabled");
        }
        $(cb).closest("tr").find(".Semester2").removeAttr("disabled");
    }
    updateCheckAll(1);
    updateCheckAll(2);
    ValidateForm();
}

function ShowListServicePackage() {
    var position = $("#LocationShowService").offset();
    $("#ListMenuContextServicePackage").css({ left: position.left, display: "block", top: position.top - 130, position: "fixed", width: "100px", "z-index": "1000" });
}

function HideListServicePackage() {
    $(".ListMenuContextServicePackage").attr("style", "display:none;");
}

function ChangeServicePackage(serviceID) {
    $(".ServicesPackage option[value='" + serviceID + "']").attr("selected", "selected");
    HideListServicePackage();
}

//////////////////  CONTEXT ON RELATIONSHIP COLUMN  //////////////////////////////////////////
function ShowListRelatives() {
    var position = $("#LocationShowRelation").offset();
    $("#ListRelatives").css({ left: position.left, display: "block", top: position.top - 120, position: "fixed", width: "100px", "z-index": "1000" });
}

function HideListRelatives() {
    $(".ListRelatives").attr("style", "display:none;");
}

function ChangePersonType(val) {
    // Set combobox Quan Hệ + Load số điện thoại
    $("select[name^='Relationship-']").each(function () {
        var pupilID = $(this).attr("name").split("-")[1];
        var currentPhone = $.trim($("input[name='PhoneReceiver-" + pupilID + "']").val());
        if (currentPhone.length == 0)
        {
            $(this).val(val);
            var phone = $("select[name='Relationship-" + pupilID + "'] option[value='" + val + "']").attr("phone");
            $("input[name='PhoneReceiver-" + pupilID + "']").val(phone)
            if (phone.length > 0) {
                $("input[name='Register_" + pupilID + "']").attr("checked", "checked");
            }
            else {
                $("input[name='Register_" + pupilID + "']").removeAttr("checked");
            }
        } 
    });

    // Ẩn context
    $("#ListRelatives").css("display", "none");
}

function LoadRelationshipPhone(val) {
    //Load số điện thoại
    var pupilID = $(val).attr("name").split("-")[1];
    var phone = $("select[name='Relationship-" + pupilID + "'] option[value='" + $(val).val() + "']").attr("phone");
    $("input[name='PhoneReceiver-" + pupilID + "']").val(phone);
    if (phone.length > 0) {
        $("input[name='Register_" + pupilID + "']").attr("checked", "checked");
    }
    else {
        $("input[name='Register_" + pupilID + "']").removeAttr("checked");
    }
}

/////////////////////////////  END CONTEXT ON RELATIONSHIP COLUMN ////////////////////////////////////////////
function GetEWallet()
{
    $.ajax({
        url: urlGetEwallet,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data.Type == "success") {
                $("#TotalMoney").html(smas.FormatMoney(data.Balance));
                $(".ContentCalculator").find("#Balance").val(smas.FormatMoney(data.Balance));
                $(".CountDescriptiorViettel").html(0);
                $("#PayMoney").html(0);

                // Thông tin Màn hình search Nợ cước
                $("#DeferredPaymentPopup #TotalMoney").html(smas.FormatMoney(data.Balance));
                $("#DeferredPaymentPopup #Balance").val(smas.FormatMoney(data.Balance));
                $("#DeferredPaymentPopup #CountParent").html(0);
                $("#DeferredPaymentPopup .CountDescriptiorViettel").html(0);
                $("#DeferredPaymentPopup #PayMoney").html(0);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

function TrimSpace(obj)
{
    var val = $(obj).val();
    $(obj).val($.trim(val));
}

function LoadComplete()
{
    var countS1Enabled = 0;
    $(".Semester1").each(function () {
        if ($(this).is(":disabled") == false)
        {
            countS1Enabled++;
        }
    });

    var countS2Enabled = 0;
    $(".Semester2").each(function () {
        if ($(this).is(":disabled") == false) {
            countS2Enabled++;
        }
    });

    var Price = 0;
    var totalPrice = 0;
    var countVT = 0;
    var countOther = 0;
    var countcheckboxRegis = 0;
    var countRegis = 0;
    var balance = $.trim($("#Balance").val()).replaceAll(".", "");
    var counteachS1 = 0;
    var counteachS2 = 0;
    $(".CountDescriptiorViettel").html(0);

    $(".chekbox").each(function () {
        Price = 0;
        countVT = 0;
        countOther = 0;
        countcheckboxRegis++;
        var pupilID = $(this).closest("tr").find(".pupilID").val();
        var pupilName = $(this).closest("tr").find(".pupilName").val();
        var phone = $.trim($(this).closest("tr").find(".PhoneReceiver").val());
        var servicePackageID = $(this).closest("tr").find(".ServicesPackage").val();
        var statusPupil = $(this).closest("tr").find("StatusPupil-" + pupilID).val();
        statusPupil = parseInt(statusPupil);
        var checkSemester1Disabled = $(this).closest("tr").find(".Semester1").is(":disabled");
        var checkSemester2Disabled = $(this).closest("tr").find(".Semester2").is(":disabled");
        servicePackageID = parseInt(servicePackageID);

        if (!checkSemester1Disabled) {
            if (currentSemester == FirstSemester) {
                $(this).closest("tr").find(".Semester1").attr("checked", "checked");
                counteachS1++;
            }
        }

        if (!checkSemester2Disabled) {
            if (currentSemester == SecondSemester) {
                $(this).closest("tr").find(".Semester2").attr("checked", "checked");
                counteachS2++;
            }
        }

        var checkSemester1 = $(this).closest("tr").find(".Semester1").is(":checked");
        var checkSemester2 = $(this).closest("tr").find(".Semester2").is(":checked");
        //neu la so viettel                       
        if (smas.CheckPhoneNumber(phone) && servicePackageID > 0 && (checkSemester1 || checkSemester2)) {

            $(this).attr("checked", "checked");
            countRegis++;
            $(this).closest("tr").find(".ClassMessage").html("&nbsp;");
            if (smas.CheckPhoneVT(phone)) {
                //lay gia goi cuoc theo sdt
                Price = $.trim($("#SVPVT-" + servicePackageID).html());
                Price = Price.replaceAll(".", "");
                Price = parseInt(Price);
                if (checkSemester1 && checkSemester2) {
                    totalPrice += Price * 2;
                } else if (checkSemester1 || checkSemester2) {
                    totalPrice += Price;
                }

                if ((checkSemester1 || checkSemester2) && servicePackageID > 0) {
                    //lay gia tri                       
                    countVT = $.trim($("#Viettel-" + servicePackageID).html());
                    if (countVT == "" || countVT == "0") {
                        countVT = 0;
                    }
                    countVT = parseInt(countVT);
                    // Cong them gia tri
                    $("#Viettel-" + servicePackageID).html(countVT + 1);
                }
            }
            else {
                //lay gia goi cuoc theo sdt
                Price = $.trim($("#SVPOther-" + servicePackageID).html());
                Price = Price.replaceAll(".", "");
                Price = parseInt(Price);
                if (checkSemester1 && checkSemester2) {
                    totalPrice += Price * 2;
                } else if (checkSemester1 || checkSemester2) {
                    totalPrice += Price;
                }

                if ((checkSemester1 || checkSemester2) && servicePackageID > 0) {
                    //lay gia tri

                    countOther = $.trim($("#Other-" + servicePackageID).html());
                    if (countOther == "" || countOther == "0") {
                        countOther = 0;
                    }
                    countOther = parseInt(countOther);
                    // Cong them gia tri
                    $("#Other-" + servicePackageID).html(countOther + 1);
                }
            }
        }
    });
    // cac thong tin chung
    $("#CountParent").html(countRegis);
    $("#PayMoney").html(smas.FormatMoney(totalPrice));
    $("#TotalMoney").html(smas.FormatMoney(balance));

    if (countcheckboxRegis > 0 && countcheckboxRegis == countRegis) {
        $("#RegisAll").attr("checked", "checked");
    } else {
        $("#RegisAll").removeAttr("checked");
    }

    if (countS1Enabled = counteachS1 && counteachS1 > 0)
    {
        $("#CheckAllSemeseter1").attr("checked", "checked");
    }

    if (countS2Enabled = counteachS2 && counteachS2 > 0) {
        $("#CheckAllSemeseter2").attr("checked", "checked");
    }
}

function CreateAccountSparent() {
    $.ajax({
        url: urlCreateSparent,
        data: { __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data.Type == "success") {
              // khong hien thong bao
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

/////////////////    REDIRECT TO CHECKOUT UNPAID CONTRACT   ////////////////////////
function RedirectToCheckout() {
    smas.openDialog("DeferredPaymentPopup", urlRedirectToCheckout, {});
    // Bind to close popup (close in Frame)
    $('#DeferredPaymentPopup div.t-header a.t-link').unbind('click').click(function () {
        onCloseDeferredPaymentPopup();
    });
}
/////////////////    END - REDIRECT TO CHECKOUT UNPAID CONTRACT   ////////////////////////

function onCloseDeferredPaymentPopup() {
    smas.closeDialog("DeferredPaymentPopup");
    Search();
}


/////////////   CANCEL LIST SUBSCRIBER /////////////////////////////
function CheckValidCancel() {
    var count = 0;
    var lstContractID = [];
    var lstPhone = [];
    var lstServicePackageID = [];
    var lstContractDetailID = [];
    $("input[type='checkbox'][name^='CancelRow_']").each(function () {
        if ($(this).is(":checked")) {
            count++;
            var arr = $(this).val().split('-');
            lstContractID.push(arr[0]);
            lstPhone.push(arr[1]);
            lstServicePackageID.push(arr[2]);
            lstContractDetailID.push(arr[3]);
        }
    });

    if (count == 0) {
        smas.alert({ Type: 'error', Message: 'Thầy cô phải chọn danh sách các thuê bao cần hủy' });
        return false;
    } else {
        smas.confirm(messageDeleteContractDetail, function () {
            $.ajax({
                url: urlCancelSubscriber,
                data: { lstContractID: lstContractID, lstPhone: lstPhone, lstServicePackageID: lstServicePackageID, lstContractDetailID: lstContractDetailID, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val() },
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                traditional: true,
                success: function (msg) {
                    if (msg.message != "success") {
                        Search();
                        smas.alert({ Type: 'success', Message: msg.message });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    smas.alert(XMLHttpRequest.responseText);
                }
            });
        });
    }
}


function CheckCancelAll(val) {
    if ($(val).is(":checked")) {
        $('#btnCancelSub').removeAttr("disabled");
        $("input[type='checkbox'][name^='CancelRow_']").each(function () {
            $(this).attr("checked", true);
        });
    } else {
        $('#btnCancelSub').attr("disabled", "disabled");
        $("input[type='checkbox'][name^='CancelRow_']").each(function () {
            $(this).attr("checked", false);
        });
    }
}

function CheckCancelSubscriber(item) {
    if ($(item).is(":checked")) {
        $('#btnCancelSub').removeAttr("disabled");
        var checkAll = true;
        $(item).closest('table').find("input[type='checkbox'][name^='CancelRow_']").each(function () {
            if (!$(this).is(":checked")) {
                checkAll = false;
            }
        });

        if (checkAll) {
            $("#CancelAll").attr("checked", true);
        }
    } else {
        $("#CancelAll").attr("checked", false);
    }
}

function AllowNumbersOnly(e) {
    var code = (e.which) ? e.which : e.keyCode;
    if (code > 31 && (code < 48 || code > 57)) {
        e.preventDefault();
    }
}