﻿function CheckToLoad(li, contactGroupId) {
    $(li).closest("ul").find("a").removeClass("Active");
    $(li).find("a").addClass("Active");

    var data = {
        id: contactGroupId
    };

    ajaxPostUpdateDiv(dtl_url, data, "ContactGroupDetailSection", null);
};
function ajaxPostUpdateDiv(url, data, divID, callBack) {
    $.ajax({
        url: url,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: data,
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        complete: function () {
            smas.hideProcessingDialog();
        },
        success: function (data) {
            $("#" + divID).html(data);
            if (callBack != null && callBack != '') {
                callBack();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            if (failCallBack != null) {
                failCallBack(XMLHttpRequest.responseText);
            }
        }
    });
};
function serializeObject(formId) {
    _x = $("#" + formId).serializeArray();
    _o = {}
    $.each(_x, function (i, field) {
        _o[field.name] = field.value
    });
    return _o;
};

function onSaveSuccess(msg) {

    if (msg.Type == "success") {
        smas.alert(msg.Message);
        //cap nhat lai ten
        var $activeCg = $(".SidebarSection").find("a.Active");
        var name = $("#frmDetail").find("#inputContactGroupName").val();
        $activeCg.text(name)

        if(msg.IsCreate)
        {
            $activeCg.closest("li").unbind("click").attr("onclick", "CheckToLoad(this," + msg.ContactGroupId + ")");
            $activeCg.attr("id", "NameGroup-" + msg.ContactGroupId).css("word-wrap", "break-word;");
            $activeCg.closest("ul").append("<li class='EndItem' onclick='CheckToLoad(this, 0)'><a href='javascript:'><span style='font-size: 11pt;'>+</span> Thêm mới</a></li>");
            $activeCg.closest("li").click();
        }
    }
    else {
        smas.alert({ Type: 'error', Message: msg.Message });
    }
};
function onSaveFail(ajaxContext) {
    smas.alert(ajaxContext.responseText);
};

function onAssignClick(contactGroupId) {
    var strAssignedTeacherID = $("#additionSection #AssignTeachersID").val();
    smas.openDialog("assignDialog", assign_url, { contactGroupId: contactGroupId, strAssignedTeacherID: strAssignedTeacherID });
};

function onDeleteGroup(contactGroupId) {
    var form = $('#frmDetail');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: delete_url,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { contactGroupId: contactGroupId, __RequestVerificationToken: token },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        complete: function () {
            smas.hideProcessingDialog();
        },
        success: function (data) {
            if (data.Type == 'success')
            {
                smas.alert(data.Message);
                window.location = index_url;
            }
            else
            {
                smas.alert({ Type: "error", Message: data.Message });
            }
            
           
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
           
        }
    })
};

function onSaveAssign() {
    //Lay cac can bo duoc chon
    var strArrCheckedId = "";
    $("#GridAssign").find("tr:visible").find(".checkedIds:checked").each(function (index) {
        var id = $(this).val();
        strArrCheckedId = strArrCheckedId + id + ",";

    });

    var strArrUnCheckedId = "";
    $("#GridAssign").find("tr:visible").find(".checkedIds:not(:checked)").each(function (index) {
        var id = $(this).val();
        strArrUnCheckedId = strArrUnCheckedId + id + ",";

    });

    var strAssigndId = $("#additionSection").find("#AssignTeachersID").val();
    var data = {
        strAssignedTeacherId: strAssigndId,
        strCheckedId: strArrCheckedId,
        strUncheckedId: strArrUnCheckedId
    }

    $.ajax({
        url: assign_save_url,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: data,
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        complete: function () {
            smas.hideProcessingDialog();
        },
        success: function (data) {
            if (data.Type == "success") {
                $("#additionSection #AssignedTeacherName").text(data.StrName);
                $("#additionSection #AssignTeachersID").val(data.StrId);
                smas.closeDialog("assignDialog");
            };
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
    
};
function onCloseAssign() {

    smas.closeDialog("assignDialog");
};
function onWorkGroupTypeChange(cb) {
    var workGroupType = $(cb).val();
    controlUtils.loadComboBox(
                     $("#WorkTypeID"),
                    ajaxLoadWorkType_url,
                                { workGroupType: workGroupType },
                                '[Tất cả]', null);
};

function TopSearch() {
    $("#SearchPanel").find("input:text").each(function () {
        $(this).val($.trim($(this).val()));

    });

    var SchoolFacultyID = $("#SearchPanel").find("#SchoolFacultyID").val();
    var WorkGroupTypeID = $("#SearchPanel").find("#WorkGroupTypeID").val();
    var WorkTypeID = $("#SearchPanel").find("#WorkTypeID").val();
    var ComunityID = $("#SearchPanel").find("#ComunityID").val();
    var EmployeeFullName = $("#SearchPanel").find("#EmployeeFullName").val();
    var EmployeeCode = $("#SearchPanel").find("#EmployeeCode").val();
    var Genre = $("#SearchPanel").find("#Genre").val();
    var PhoneNetwork = $("#SearchPanel").find("#PhoneNetwork").val();
    var StrInGroupId = '';
    $("#botListResult").find(".checkedIds").each(function () {
        var id = $(this).val();
        StrInGroupId = StrInGroupId + id + ",";

    });
    var data = {
        SchoolFacultyID: SchoolFacultyID,
        WorkGroupTypeID: WorkGroupTypeID,
        WorkTypeID: WorkTypeID,
        ComunityID: ComunityID,
        EmployeeFullName: EmployeeFullName,
        EmployeeCode: EmployeeCode,
        Genre: Genre,
        PhoneNetwork: PhoneNetwork,
        StrInGroupId: StrInGroupId
    }

    ajaxPostUpdateDiv(search_url, data, "topListResult", null);
};

function onCheckAllClick(cb) {
    var $grid = $(cb).closest(".GeneralTable");
    $grid.find(".t-grid-content").find("tr:visible").find(".checkedIds").each(function () {
        if ($(cb).is(":checked")) {
            $(this).attr('checked', 'checked');
        }
        else {
            $(this).removeAttr('checked');
        }
    });
    if ($grid.find(".t-grid-content").find(".checkedIds:checked").length > 0) {
        $(cb).closest(".detail-wraper").find(".AddOrCancel").removeAttr("disabled");
        $(cb).closest(".detail-wraper").find(".AddOrCancel").removeClass("t-state-disabled");

    }
    else {
        $(cb).closest(".detail-wraper").find(".AddOrCancel").attr("disabled", "disabled");
        $(cb).closest(".detail-wraper").find(".AddOrCancel").addClass("t-state-disabled");

    }
};

function onCheckboxClick(cb) {
    var $grid = $(cb).closest(".GeneralTable");
    if ($grid.find(".t-grid-content").find("tr:visible").find(".checkedIds:not(:checked)").length > 0) {
        $grid.find(".t-grid-header").find("input:checkbox").removeAttr('checked');
    }
    else {
        $grid.find(".t-grid-header").find("input:checkbox").attr('checked', 'checked');
    }

    if ($grid.find(".t-grid-content").find(".checkedIds:checked").length > 0) {
        $(cb).closest(".detail-wraper").find(".AddOrCancel").removeAttr("disabled");
        $(cb).closest(".detail-wraper").find(".AddOrCancel").removeClass("t-state-disabled");

    }
    else {
        $(cb).closest(".detail-wraper").find(".AddOrCancel").attr("disabled", "disabled");
        $(cb).closest(".detail-wraper").find(".AddOrCancel").addClass("t-state-disabled");

    }
};

function onAssignSearch() {
    $("#frmAssignSearch").find("input:submit").click();
};
function lockClick(btn, isLock) {
    var $wraper = $(btn);
    var isCurrentLock = $wraper.find("#IsLock").val();
    //Mo khoa
    if (isCurrentLock == 'True') {
        $wraper.removeClass("On");
        $wraper.addClass("Off");
        $wraper.find("#IsLock").val('False');
    }
        //Khoa
    else {
        $wraper.removeClass("Off");
        $wraper.addClass("On");
        $wraper.find("#IsLock").val('True');
    }
};
function DisableButton(btn) {
    $(btn).attr("disabled", "disabled");
};


function PrepareSave() {
    //trim name
    $("#frmDetail").find("#inputContactGroupName").val($.trim($("#frmDetail").find("#inputContactGroupName").val()));
};
function PrepareAssignSearch() {
    $("#frmAssignSearch").find("input:text").val($.trim($("#frmAssignSearch").find("input:text").val()));
};
function searchGrid(e) {
    $("#grid").data("kendoGrid").dataSource.filter({ field: "ShipName", operator: "contains", value: $("#ShipNameSearch").val() });
};

//SERACH TEACHER - ONKEYUP EVENT INPUT
function searchTeacherReceiver(thisObject) {
    //check maxlength <= 100
    value = $(thisObject).val();
    if (value.length > 100) {
        value = value.substr(0, 100);
        $(thisObject).val(value);
    }
    //khi dang typing thi khong thu hien tim kiem       
    if (isTyping) {
        clearTimeout(isTyping);
    }
    isTyping = setTimeout(function () {
        //chi tim kiem voi truong hop khac rong
        if ($.trim($(thisObject).val()) != '') {
            //cat dau, cat khoang trang        
            value = stringUtils.StripVNSign($.trim(value)).toLowerCase();
            var receiverName;
            var receiverCode;
            //search doi voi ten giao vien, duoc cat dau va remove khoang trang
            $('#GridAssign').find(".t-grid-content").find('table').find("tr").each(function () {
                receiverCode = $.trim(stringUtils.StripVNSign($(this).find('.SearchItem1').html()).toLowerCase());
                receiverName = $.trim(stringUtils.StripVNSign($(this).find('.SearchItem2').html()).toLowerCase());
                
                if (receiverName.indexOf(value) < 0 && receiverCode.indexOf(value) < 0 ) { // neu khong tim thay
                    $(this).css('display', 'none');
                }
                else {// tim thay
                    $(this).css('display', '');
                }
            });
        }
        else {
            //neu la rong thi show all
            $('#GridAssign').find(".t-grid-content").find('table').find("tr").css('display', '');
        }

        if ($('#GridAssign').find(".t-grid-content").find('table').find("tr:visible").length == 0)
        {
            $("#btnSaveAssign").hide();
        }
        else
        {
            $("#btnSaveAssign").show();
        }
        //checkbox all
        if ($('#GridAssign').find(".t-grid-content").find('table').find("tr:visible").length > 0 &&
            $('#GridAssign').find(".t-grid-content").find('table').find("tr:visible").length
            == $('#GridAssign').find(".t-grid-content").find('table').find("tr:visible").find(".checkedIds:checked").length) {
            $('#GridAssign').find("#chekAll").attr("checked", "checked");
        }
        else {
            $('#GridAssign').find("#chekAll").removeAttr('checked');
        }
       
    }, 400);
};

function preventEnterSubmit(eventObject) {
    if (eventObject.keyCode == 13) {
        //fix on IE
        if (isIE78) {
            eventObject.returnValue = false;
        }
        else {
            eventObject.preventDefault();
        }
    }
}