﻿

function openRegistPopup() {
    smas.openDialog("RegistPopup", urlRegist);
    return;
};

function openExtendDialog(id) {
    smas.openDialog("ExtendPopup", urlExtend, { id: id });
    return;
};

function openUpdateDialog(id) {
    smas.openDialog("UpdatePopup", urlUpdate, { id: id });
    return;
};

function openCancelDialog(id) {
    smas.openDialog("CancelPopup", urlCancel, { id: id });
    return;
};

function cancelRegist(id) {

    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: urlCancel,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { __RequestVerificationToken: token, id: id },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        complete: function () {
            smas.hideProcessingDialog();
        },
        success: function (data) {
            if (data.Type == "success") {
                smas.alert(data.Message);
                smas.closeDialog('CancelPopup');
                ajaxPostUpdateDiv(urlSearch, "dataGrid");
            }
            else {
                smas.alert({ Type: 'error', Message: data.Message });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
};

function saveSuccess(msg) {

    if (msg.Type == "success") {
        smas.alert(msg.Message);
        smas.closeDialog("RegistPopup");
        smas.closeDialog("ExtendPopup");
        smas.closeDialog("UpdatePopup");
        ajaxPostUpdateDiv(urlSearch, "dataGrid");
    }
    else {
        smas.alert({ Type: 'error', Message: msg.Message });
    }
};

function saveFail(ajaxContext) {
    smas.alert(ajaxContext.responseText);
};

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

function onBrandnameChange(txt) {
    var brandname = $(txt).val();
    $(".frmSave").find("#ContractNumber").val(contractNumberPrefix + brandname);
    $(".frmSave").find("#hidContractNumber").val(contractNumberPrefix + brandname);
};

function ajaxPostUpdateDiv(url, divID) {
    $.ajax({
        url: url,
        xhrFields: {
            withCredentials: true
        },
        type: 'post',
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        complete: function () {
            smas.hideProcessingDialog();
        },
        success: function (data) {
            $("#" + divID).html(data);
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
};

function onCheckChange(item) {
    var $tbl = $(item).closest("table");
    var $tr = $(item).closest("tr");
    if ($(item).is(':checked')) {
        $tr.find('.Duration').removeAttr("disabled");
    }
    else {
        $tr.find('.Duration').attr("disabled", "disabled");
    }
    UpdateRegistFee(item);

    if ($tbl.find("input:checkbox:checked").length == 0) {
        $("#BtnRegist").attr("disabled", "disabled");
    }
    else {
        $("#BtnRegist").removeAttr("disabled");
    }
}

function onDurationChange(item) {
    var $tbl = $(item).closest("table");
    var value = $(item).val();
    $tbl.find('.Duration').val(value);
    UpdateRegistFee(item);
};

function UpdateRegistFee(item) {
    var $tbl = $(item).closest("table");
    var totalRegistFee = 0;
    var totalMaintainFee = 0;
    var totalExtendFee = 0;
    $tbl.find('tr').each(function () {
        if ($(this).find('input:checkbox').is(':checked')) {
            var registFee = 0;
            var maintainFee = 0;
            var extendFee = 0;
            var duration = parseInt($(this).find(".Duration").val());

            if ($(this).find(".RegistFee") != null && $(this).find(".RegistFee") != 'undefined') {
                registFee = parseInt($(this).find(".RegistFee").val());
            }

            if ($(this).find(".MaintainFee") != null && $(this).find(".MaintainFee") != 'undefined') {
                maintainFee = parseInt($(this).find(".MaintainFee").val());
            }

            if ($(this).find(".ExtendFee") != null && $(this).find(".ExtendFee") != 'undefined') {
                extendFee = parseInt($(this).find(".ExtendFee").val());
            }

            totalRegistFee += registFee;
            totalMaintainFee += maintainFee * duration;
            totalExtendFee += extendFee * duration;
        }
    });

    var totalFee = totalRegistFee + totalMaintainFee;

    $(item).closest(".frmSave").find(".registFeeLabel").text(numberWithCommas(totalRegistFee) + " VNĐ");
    $(item).closest(".frmSave").find(".maintainFeeLabel").text(numberWithCommas(totalMaintainFee) + " VNĐ");
    $(item).closest(".frmSave").find(".extendFeeLabel").text(numberWithCommas(totalExtendFee) + " VNĐ");
    $(item).closest(".frmSave").find(".totalFeeLabel").text(numberWithCommas(totalFee) + " VNĐ");

};

