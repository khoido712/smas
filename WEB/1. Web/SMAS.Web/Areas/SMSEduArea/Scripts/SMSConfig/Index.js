﻿var ValueChanged = false;

function LoadEditor2(smsType) {
    LoadSMSConfig(smsType, loadSMSConfigUrl);
}

function LoadEditor(smsType) {
    if (ValueChanged && typeof (PageValueChanged) == "function") {
        PageValueChanged(function () {
            LoadEditor2(smsType);
        });
    }
    else {
        LoadEditor2(smsType);
    }
}
function LoadSMSConfig(smsType, url) {
    var typeID = $(smsType).attr("typeID");
    var itms = document.getElementsByName("itemSMSType");
    for (var i = 0; i < itms.length; i++) {
        $(itms[i]).attr("class", "");
    }
    $(smsType).attr("class", "Active");
    $.ajax({
        url: url,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { TypeID: typeID },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            $("#div_sms_editor").html(msg);
            beginFormSave(sendTimeErrorText);
            showUnloadPopup;
            bindOnOffEvent(checkBindOnOffEvent);
            checkOnOffAutoSave();
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
};

function LoadCustomType(type) {
    var id = $(type).attr("typeid");

    var itms = document.getElementsByName("itemSMSType");
    for (var i = 0; i < itms.length; i++) {
        $(itms[i]).attr("class", "");
    }
    $(type).attr("class", "Active");

    $.ajax({
        url: urlEditCustomType,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: {id : id},
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            $("#div_sms_editor").html(msg);
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });

}
function CraeteNewSMSType(type) {

    var itms = document.getElementsByName("itemSMSType");
    for (var i = 0; i < itms.length; i++) {
        $(itms[i]).attr("class", "");
    }
    $(type).attr("class", "Active");

    $.ajax({
        url: urlCreate,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: null,
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            $("#div_sms_editor").html(msg);
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });
};

function DeleteType(id) {
    var form = $('#frmCreateNewType');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    $.ajax({
        url: urlDelete,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { id: id, __RequestVerificationToken: token },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            smas.alert(msg);
            smas.hideProcessingDialog();
            if (msg.Type == "success") {
                window.location = urlIndex;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });
};

function ReloadContentEx() {
    var periodType = $("#frmCreateNewType #PeriodType").val();
    var isForVNEN = $("#frmCreateNewType #ForVNEN").val();
    var template = $("#frmCreateNewType #Template").val();
    $.ajax({
        url: urlReloadContentEx,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { periodType: periodType, isForVNEN: isForVNEN, template:template },
        beforeSend: function () {
            //smas.showProcessingDialog();
        },
        success: function (data) {
            
            //smas.hideProcessingDialog();
            $("#frmCreateNewType #ExContent").html(data.Content);
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            //smas.hideProcessingDialog();
        }
    });
}

var ValueChanged = false;
var continueHandler = null;
//Mask input
function setMarkInPutEditor() {
    $("#SendTime").mask("99:99");
    BindUnload();
}

//Bind unload page for check save
function BindUnload() {
    $('a').click(function () {
        var url = $(this).attr('href');
        if (!(url && url != "javascript:")) return;
        if (ValueChanged && typeof (PageValueChanged) == "function") {
            PageValueChanged(function () {
                window.location = url;
            });
            return false;
        }
        return true;
    });
}
//Chap nhan ko save
function okNotSave() {
    smas.closeDialog("UnloadSaveConfirm");
    ValueChanged = false;
    if (continueHandler) continueHandler();
}
function okSave() {
    $("#frmMessage input[type='submit']").click();
    smas.closeDialog("UnloadSaveConfirm");
    if (continueHandler) continueHandler();
}
//Change page without save data
function PageValueChanged(handler) {
    showUnloadPopup();
    continueHandler = handler;
}
//Data changed flag
function ChangeSave() {
    ValueChanged = true;
}
////Ajax event successed
function saveSuccess(msg) {

    ValueChanged = false;
    if (msg.Type == "success") {
        smas.alert(msg);
    } else {
        smas.alert({ Type: "error", Message: msg.Message });
    }
}
////Ajax event fail
function saveFail(ajaxContext) {
    smas.alert(ajaxContext.responseText);
}
////Kiem tra nhap thoi gian hop le
function TimeChange(t) {
    var time = document.getElementById("hidTimeTmp");
    var validTime = $(t).val().match(/^(0?[1-9]|1[0-9]|0[0]|2[0123])(:[0-5]\d)$/);
    if (!validTime) {
        $(t).val(time.value);
    } else {
        time.value = $(t).val();
    }
    ChangeSave();
}
//Load OnOff Item
function checkOnOf(id, onofId) {
    var hidden = document.getElementById(id);
    var itm = $("#" + onofId);
    if (hidden) {
        if (hidden.value == 1) {
            $('.off', itm).hide();
            $('.on', itm).show();
            $(this).addClass('On').removeClass('Off').attr('title', 'Click here to ON');
        }
        else {
            $('.on', itm).hide();
            $('.off', itm).show();
            $(itm).addClass('Off').removeClass('On').attr('title', 'Click here to OFF');
        }
        VisibleItem();
    }
}

////An/Hien control khi chon On/Off
function VisibleItem() {
    var show = true;
    if ($("#lockOnOff").hasClass('On')) show = false;
    if ($("#autoSendOnOff").hasClass('Off')) show = false;

    if ($("#dateBefore").length>0) {
        $("#dateBefore").attr("disabled", "disabled");
        if (show) $("#dateBefore").removeAttr("disabled");
    }
    if ($("#SendDate").length>0) {
        $("#SendDate").attr("disabled", "disabled");
        if (show) $("#SendDate").removeAttr("disabled");
        var tdp = $('#SendDate').data("tDatePicker");
        if (tdp) {
            tdp.disable();
            if (show) tdp.enable();
        }
    }

    if ($("#SendTime").length>0) {
        $("#SendTime").attr("disabled", "disabled");
        if (show) $("#SendTime").removeAttr("disabled");
        var tlCtrl = $('#SendTime').data("tTimePicker");
        if (tlCtrl) {
            tlCtrl.disable();
            if (show) tlCtrl.enable();
        }
    }
    //Unbind Autosend
    if ($("#lockOnOff").hasClass('On')) {
        $("#autoSendOnOff").unbind();
    }
    else {
        bindOnOffEvent();
    }
}

////Bind su kien khi chon on/off
function bindOnOffEvent(disaleDD) {
    //Rebind Event
    $('.OnOff').unbind();
    $('.OnOff').click(function () {
        if (disaleDD == 1) return;
        ChangeSave();
        var hidden = document.getElementById($(this).attr('hiddenID'));
        var id = $(this).attr("id");
        if ($(this).hasClass('On')) {
            $('.on', this).hide();
            $('.off', this).show();
            $(this).addClass('Off').removeClass('On').attr('title', 'Click here to OFF');
            if (hidden) hidden.value = 0;
        }
        else {
            $('.off', this).hide();
            $('.on', this).show();
            $(this).addClass('On').removeClass('Off').attr('title', 'Click here to ON');
            if (hidden) hidden.value = 1;
        }
        //Neu da khoa thi khong cho phep gui tu dong
        if (id == "lockOnOff") {
            if ($("#autoSendOnOff").hasClass('On')) {
                $("#autoSendOnOff").click();
            }
        }
        VisibleItem();
    });
}
////Show popup
function showUnloadPopup() {
    var TypeName = $('a[name="itemSMSType"].Active').text();
    smas.openDialog("UnloadSaveConfirm", "", {});
    $('#div-SMS-content').html(jQuery.validator.format(saveWarning, TypeName));
}

////Beginning of ajax event
function beginFormSave(msg) {
    if ($("#SendTime").length>0) {
        if (msg != "") {
            msg = ErrorTimeSend;
        }
        var time = $("#SendTime").val();
        if (time != null && time != undefined) {
            if (!time.match(/^(0?[1-9]|0[0]|1[0-9]|2[0123])(:[0-5]\d)$/)) {
                smas.alert({ "Type": "Error", "Message": msg });
                return false;
            }
        }
    }
    smas.showProcessingDialog();
}


///////////////////////////////////// SEND UNICODE MESSAGE  \\\\\\\\\\\\\\\\\
function checkAllowSignMsg(chk) {
    var isCheck = $(chk).is(':checked');
    if (isCheck) {
        $('#ContentInfoMsg').text('(67 ký tự/SMS. Chỉ gửi được tin nhắn có dấu cho các mạng Viettel, Vina, Mobi.)');
    } else {
        $('#ContentInfoMsg').text('');
    }
}


// check on/off auto save
function checkOnOffAutoSave() {
    bindOnOffEvent();
    checkOnOf('hidOnOff', 'lockOnOff');
    checkOnOf('hidAutoSend', 'autoSendOnOff');
    var time = document.getElementById("hidTimeTmp");
    if (time) {
        time.value = $("#SendTime").val();
    }
};


$(function () {
    $('#SMSTypeList').find('a').eq(0).click();
    // To disable f5
    $(document).bind("keydown", function (e) {
        if (e.which == 116) {
            if (ValueChanged) {
                e.preventDefault();
                PageValueChanged();
            }
        }
    });
});
