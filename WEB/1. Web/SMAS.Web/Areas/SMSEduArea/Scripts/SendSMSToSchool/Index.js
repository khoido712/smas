﻿//arr searchObject dung luu lai lan search { OrderID, Key, CheckAll, CheckAllNotLoad, HasCheckAll, Total, TotalLoad } _ dung xu ly khi gui tin nhan va dem so nguoi nhan tin nhan
var arrSearchObject = [];
var defaultSearchObject = null; // voi Key =''
var currentSearchObject = null; // voi Key dang duoc tim kiem
var isTyping; //check dk tim kiem, neu dang nhap ky tu thi khong chay ham search
var educationGrade = 0; //cap hoc    
var orderIDSearch = 0; //oderid to search
var orderIDloadRecord = 0; //orderid to load record -> dung de resize height khi load new record
var objLoadContentScrollSearch; //object load content when scoll or search
var msgPreFixErrorSMS = '';
//prefixName = stringUtils.HtmlDecode(prefixName);

//LOAD DETAIL EDUCATION GRADE - ONCLICK LI EVENT
function LoadDetailLevelToSchool(thisObject, selectGrade) {
    //css style
    $(thisObject).closest('ul').find('a').removeClass('Active');
    $(thisObject).addClass('Active');
    //reset search object
    arrSearchObject = [];
    //reset isTyping
    if (isTyping) {
        clearTimeout(isTyping);
    }

    // reset default object voi Key =''
    defaultSearchObject = null;
    // reset current object voi Key dang duoc tim kiem
    currentSearchObject = null;
    //cap hoc
    educationGrade = selectGrade;
    //reset orderID search
    orderIDSearch = 0;
    //reset orderid load record
    orderIDloadRecord = 0;
    //reset object load content when scroll or search
    if (objLoadContentScrollSearch) {
        objLoadContentScrollSearch.abort();
    }

    $('.BtnCenterSection').find('#btSendSMS').attr('disabled', 'disabled');
    //thu nho lai danh sach nguoi nhan
    $('#ListTeacherReceiverSMS').slideUp();
    $('#colapseFunListTeacher').removeClass("Sclose");
    $('#colapseFunListTeacher').addClass("Eclose");
    //reset list receiver
    $('#ListTeacherReceiverSMS').html('');
    $('.GroupPerson').find('.ReceiverName').html('');
    // load detailLevel
    $.ajax({
        url: url_LoadDetailLevelToSchool,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { educationGrade: educationGrade },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (data) {
            // clear textbox
            $('#txtContentToSend').val(prefixName);
            $('#LengSMSToTeacher').html($.trim(prefixName).length + '/' + 1);
            // disable button Gửi
            $("#btSendSMS").attr("disabled", true);
            // set num of receiver      
            $('.NumOfHeadMasterReceiver').html(data.numOfReceiverTotal);
            console.log(data);
            $('.TitleTab1SectionBtm').find('.TittleDetailLevel').html($(thisObject).html() + ' - ' + data.numOfReceiverTotal);
            // set defaultObject
            defaultSearchObject = { OrderID: orderIDSearch, Key: '', CheckAll: true, CheckAllNotLoad: true, Total: data.numOfReceiverTotal, TotalLoad: 0 };
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
        }
    });
}

//LOAD CONTENT WHEN SCROLL/SEARCH - ONKEYUP/ONSCROLLTOBOTTOM EVENT - INPUT/SCROLL
function loadContentScrollSearch(url, dataObject) {
    //scroll
    //tang orderid load new record
    orderIDloadRecord++;
    var classNameLoadRecord = 'ReceiverPopup-' + orderIDloadRecord;
    // neu co ton tai searchObject thi kiem tra trang thai check box theo searchObject
    var objSearchObject = null;
    objLoadContentScrollSearch = $.ajax({
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        url: url,
        data: dataObject,
        beforeSend: function () {
            // show loading icon
            $('.OverflowReceiverClass').find('.IconLoadingReceiverScroll').show();
        },
        success: function (data) {
            if (data.lstReceiver.length > 0) {
                //fill content
                var objReceiver;
                var htmlContent = [];
                var lengthArrSearchObject = arrSearchObject.length;
                var keySearch = '';
                var existObject = false;
                for (var i = 0, size = data.lstReceiver.length; i < size; i++) {
                    objReceiver = data.lstReceiver[i];
                    // kiem tra neu ton tai roi thi khong gan them vao
                    existObject = $('.ItemReceiverID:[id="' + objReceiver.SchoolID + '"]').length > 0;
                    if (existObject) {
                        continue;
                    }

                    objReceiver.Enabled = defaultSearchObject.CheckAllNotLoad;
                    if (lengthArrSearchObject > 0) {
                        keySearch = $.trim(objReceiver.SchoolName).toLowerCase();
                        // objReceiver thoa dk search -> kiem tra CheckAllNotLoad
                        for (var j = 0; j < lengthArrSearchObject; j++) {
                            objSearchObject = arrSearchObject[j];
                            if (objSearchObject.Key != '' && keySearch.indexOf(objSearchObject.Key) > -1) {
                                if (objSearchObject.CheckAllNotLoad) {
                                    objReceiver.Enabled = true;
                                }
                                else {
                                    objReceiver.Enabled = false;
                                }
                            }
                        }
                    }

                    htmlContent.push('<div class="ReceiverPopup ');
                    htmlContent.push(classNameLoadRecord);
                    htmlContent.push('"');
                    htmlContent.push('onclick="SelectReceiverToSend(this)"');
                    htmlContent.push('onmouseover="onMouseOverReceiver(this)" onmouseout="onMouseOutReceiver(this)">');
                    htmlContent.push('<label style="display:none;" class="ItemReceiverID" id="');
                    htmlContent.push(objReceiver.SchoolID);
                    htmlContent.push('">')
                    htmlContent.push(objReceiver.SchoolID);
                    htmlContent.push('</label>');
                    if (objReceiver.Enabled) {
                        htmlContent.push('<div class="IconCheckReceiver IconStyle IconEnable"></div>');
                    }
                    else {
                        htmlContent.push('<div class="IconCheckReceiver IconStyle IconDisable"></div>');
                    }

                    htmlContent.push('<label class="LabelStyle ItemReceiver1">');
                    htmlContent.push(objReceiver.SchoolName);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiver2">');
                    htmlContent.push(objReceiver.DistrictName);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiver2">');
                    htmlContent.push(objReceiver.HeadMasterName);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiver2">');
                    htmlContent.push(objReceiver.HeadMasterPhone);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiver2" style="text-align:center;">');
                    htmlContent.push(objReceiver.TotalInternalSMS);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiver4" style="text-align:center;">');
                    htmlContent.push(objReceiver.TotalExternalSMS);
                    htmlContent.push('</label>');
                    htmlContent.push('<label class="LabelStyle ItemReceiverEnd" style="text-align:center;">');
                    htmlContent.push(objReceiver.TotalSMSPromotion);
                    htmlContent.push('</label>');
                    htmlContent.push('</div><div class="Clear"></div>');
                }

                $('.OverflowReceiverClass').find('.IconLoadingReceiverScroll').before(htmlContent.join(''));
            }

            // neu la search thi kiem tra key + check trang thai
            // neu la scroll thi update
            objSearchObject = null;
            // luu searchObject, kiem tra neu ton tai Key roi thi update lai trang thai ReceiverIsCheck
            orderIDSearch++;
            if (dataObject.searchContent != '') {
                for (var i = arrSearchObject.length - 1; i >= 0; i--) {
                    if (dataObject.searchContent != '' && arrSearchObject[i].Key == dataObject.searchContent) {
                        objSearchObject = arrSearchObject[i];
                        break;
                    }
                }

                if (objSearchObject == null) {
                    objSearchObject = { OrderID: orderIDSearch, Key: dataObject.searchContent, CheckAll: false, CheckAllNotLoad: true, Total: data.totalReceiver, TotalLoad: $('.OverflowReceiverClass').find('.ReceiverPopup:visible').length };
                    arrSearchObject.push(objSearchObject);
                }
                else {
                    objSearchObject.TotalLoad = $('.OverflowReceiverClass').find('.ReceiverPopup:visible').length;
                }

                // set lai currentSearchObject
                currentSearchObject = objSearchObject;
            }
            else {
                defaultSearchObject.TotalLoad = $('.OverflowReceiverClass').find('.ReceiverPopup:visible').length;
                // set lai currentSearchObject
                currentSearchObject = defaultSearchObject;
            }

            // validate check all
            validateCheckAllWhenLoadRecord();

            // auto resize new record
            autoResizeHeightNewRecord('.' + classNameLoadRecord);

            // hide loading icon
            $('.OverflowReceiverClass').find('.IconLoadingReceiverScroll').hide();
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrow) {
            smas.alert(XMLHttpRequest.responseText);
            $('.OverflowReceiverClass').find('.IconLoadingReceiverScroll').hide();
        }
    });
}

// AUTO RESIZE HEIGHT WHEN NEW RECORD IS LOAD
function autoResizeHeightNewRecord(className) {
    var heightResize = 0;
    var heightTemp = 0;
    className = className || '.ReceiverPopup';
    $('.OverflowReceiverClass').find(className).each(function () {
        heightResize = $(this).find('.ItemReceiver1').height();
        heightTemp = $(this).find('.ItemReceiver2').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }

        heightTemp = $(this).find('.ItemReceiver4').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }

        heightTemp = $(this).find('.ItemReceiverEnd').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }

        if (heightResize > 15) {
            $(this).find('.IconStyle').css('height', heightResize + 'px');
            $(this).find('.LabelStyle').css('height', heightResize + 'px');
        }
    });
}

// SET SCROLL/ONCUT/ONPASTE EVENT
function setEventReceiver() {
    // ONCUT/ONPASTE
    $('#txtKeySearch').on('cut paste', function () {
        setTimeout(function () {
            searchListReceiver($('#txtKeySearch'));
        }, 200);
    });
}

//LOAD LIST TEACHER RECEIVER - ONCLICK DIV EVENT
function loadListReceiver() {
    if ($('#ListTeacherReceiverSMS').html() != '') {
        //colapse/expanse
        if ($('#colapseFunListTeacher').hasClass('Sclose')) {
            $('#ListTeacherReceiverSMS').slideUp();
            $('#colapseFunListTeacher').removeClass("Sclose");
            $('#colapseFunListTeacher').addClass("Eclose");
        }
        else {
            $("#ListTeacherReceiverSMS").slideDown();
            $('#colapseFunListTeacher').removeClass("Eclose");
            $('#colapseFunListTeacher').addClass("Sclose");
        }

        smas.hideProcessingDialog();
    }
    else {
        //load du lieu
        AjaxLoadGrid();
    }
}

function AjaxLoadGrid()
{
    $.ajax({
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        url: url_LoadListReceiver,
        data: { educationGrade: educationGrade },
        beforeSend: function () {
            $('.IconLoadingReceiver').show();
        },
        success: function (data) {
            $('#ListTeacherReceiverSMS').show();
            $('#ListTeacherReceiverSMS').html(data);
            $('#colapseFunListTeacher').removeClass("Eclose");
            $('#colapseFunListTeacher').addClass("Sclose");
            $('.IconLoadingReceiver').hide();
            // set receiver event
            setEventReceiver();
            // auto resize height
            autoResizeHeightNewRecord();
            // luu defaultSearchObject
            defaultSearchObject.TotalLoad = $('.ItemReceiverID').length;
            // luu currentSearchObject
            currentSearchObject = defaultSearchObject;
            smas.hideProcessingDialog();
        },
        error: function (xmlHttpRequest, status, throwError) {
            $('.IconLoadingReceiver').hide();
            smas.alert(xmlHttpRequest.responseText);
        }
    });
}
// CHECK IS AVAIBLED SEARCH TEACHER, PREVENT ERROR WHEN USE ASYNC REQUEST - ONKEYUP EVENT INPUT SEARCH
function searchListReceiverCheck(thisObject) {
    //check maxlength <= 200
    value = $(thisObject).val();
    if (value.length > 200) {
        value = value.substr(0, 200);
        $(thisObject).val(value);
    }
    //khi dang typing thi khong thu hien tim kiem       
    if (isTyping) {
        clearTimeout(isTyping);
    }
    isTyping = setTimeout(function () {
        //chi tim kiem voi truong hop khac rong
        if ($.trim($(thisObject).val()) != '') {
            //cat dau, cat khoang trang        
            value = stringUtils.StripVNSign($.trim(value)).toLowerCase();
            var receiverName;
            var receiverCode;
            var receiverGenreName;
            //search doi voi ten giao vien, duoc cat dau va remove khoang trang
            $('#GridSchool').find(".t-grid-content").find('tr').each(function () {
                receiverName = $.trim(stringUtils.StripVNSign($(this).find('td:nth-child(2)').html()).toLowerCase());
                if (receiverName.indexOf(value) < 0) { // neu khong tim thay
                    $(this).css('display', 'none');
                }
                else {// tim thay
                    $(this).css('display', '');
                }
            });
        }
        else {
            //neu la rong thi show all
            $('#GridSchool').find('tr').css('display', '');
        }
        //chi ket qua tim kiem chi hien thi nhung giao vien duoc chon thi tick chon button check all
        // Congnv fix bug: Bug #20379 [Redmine]
        var disableCount = $('#GridSchool').find('tr:visible').find('.csCheckedIds:not(checked)').length;
        var enableCount = $('#GridSchool').find('tr:visible').find('.csCheckedIds:checked').length;
        if (disableCount <= 0 && enableCount <= 0) {
            $('#GridSchool #csCheckAll').attr('checked', false);
        }
        else if (disableCount <= 0) {
            $('#GridSchool #csCheckAll').attr('checked', true);
        }
        else {
            $('#GridSchool #csCheckAll').attr('checked', false);
        }

        //set lai du lieu nguoi nhan
        setReceiverName();
    }, 400);
}


// SERACH TEACHER - ONKEYUP EVENT INPUT
// note: luong xu ly, khi tim kiem theo tu khoa chi tim o server + client
function searchListReceiver(thisObject) {
    //check maxlength <= 200
    var value = $(thisObject).val();
    if (value.length > 200) {
        value = value.substr(0, 200);
        $(thisObject).val(value);
    }

    //khi dang typing thi khong thu hien tim kiem
    if (isTyping) {
        clearTimeout(isTyping);
        if (objLoadContentScrollSearch) {
            objLoadContentScrollSearch.abort();
        }
    }

    isTyping = setTimeout(function () {
        // show process prevent async action
        smas.showProcessingDialog();
        value = $.trim(value.toLowerCase());
        // load du lieu
        var receiverName = '';
        var cReceiver = $('.OverflowReceiverClass').find('.ReceiverPopup').length;
        if (cReceiver > 0) {
            $('.OverflowReceiverClass').find('.ReceiverPopup').each(function () {
                receiverName = $.trim($(this).find('.ItemReceiver1').html().toLowerCase());
                if (receiverName.indexOf(value) > -1) { // tim thay                
                    $(this).css('display', '');
                }
                else {// neu khong tim thay                
                    $(this).css('display', 'none');
                }
            }).promise().done(function () {
                // neu la defaultSearch thi set lai currentSearchObject
                if (value == '') {
                    defaultSearchObject.TotalLoad = $('.OverflowReceiverClass').find('.ReceiverPopup:visible').length;
                    // set lai currentSearchObject
                    currentSearchObject = defaultSearchObject;
                    // hide process prevent async action/ resize iframe
                    smas.hideProcessingDialog();
                }
                else {
                    var numReceiver = $('.OverflowReceiverClass').find('.ReceiverPopup:visible').length;
                    var page = Math.floor(numReceiver / pageSize) + 1;
                    loadContentScrollSearch(url_loadContentScrollSearch, { educationGrade: educationGrade, searchContent: value, currentPage: 1 });
                }
            });
        } else {
            // hide process prevent async action/ resize iframe
            smas.hideProcessingDialog();
        }
    }, 1000);
}

//VALIDATE CHECKALL WHEN LOAD NEW RECORD WHEN SEARCH - ONKEYDOWN INPUT EVENT
function validateCheckAllWhenLoadRecord() {
    //neu ket qua tim kiem chi hien thi nhung giao vien duoc chon thi tick chon button check all
    if ($('.OverflowReceiverClass').find('.ReceiverPopup:visible').find('.IconStyle').length > 0) {
        if ($('.OverflowReceiverClass').find('.ReceiverPopup:visible').find('.IconDisable').length <= 0) {
            $('.IconCheckAllReceiver').removeClass('IconDisable');
            $('.IconCheckAllReceiver').addClass('IconEnable');
        }
        else {
            $('.IconCheckAllReceiver').removeClass('IconEnable');
            $('.IconCheckAllReceiver').addClass('IconDisable');
        }
    }
    else {
        $('.IconCheckAllReceiver').removeClass('IconEnable');
        $('.IconCheckAllReceiver').addClass('IconDisable');
    }
}

//SELECT TEACHER RECEIVER ID - ONCLICK ROW EVENT
function SelectReceiverToSend(thisObject) {
    //kiem tra bat check box
    if ($(thisObject).find('.IconEnable').length > 0) {
        // uncheck
        $(thisObject).find('.IconCheckReceiver').removeClass('IconEnable');
        $(thisObject).find('.IconCheckReceiver').addClass('IconDisable');
    }
    else {
        // check
        $(thisObject).find('.IconCheckReceiver').removeClass('IconDisable');
        $(thisObject).find('.IconCheckReceiver').addClass('IconEnable');
    }

    //kiem tra button checkall
    var totalCheckBox = $('.TeacherReceiverContentStyle').find('.OverflowReceiverClass').find('.IconCheckReceiver:visible').length;
    var totalCheckBoxIsCheck = $('.TeacherReceiverContentStyle').find('.OverflowReceiverClass').find('.IconEnable:visible').length;
    if (totalCheckBox == totalCheckBoxIsCheck) {//check button check all
        if (currentSearchObject.Total == currentSearchObject.TotalLoad) {
            $('.IconCheckAllReceiver').removeClass('IconDisable');
            $('.IconCheckAllReceiver').addClass('IconEnable');
        }
        else {
            if (currentSearchObject.CheckAllNotLoad) {
                $('.IconCheckAllReceiver').removeClass('IconDisable');
                $('.IconCheckAllReceiver').addClass('IconEnable');
            }
        }
    }
    else {
        $('.IconCheckAllReceiver').removeClass('IconEnable');
        $('.IconCheckAllReceiver').addClass('IconDisable');
    }

    //set num of receiver
    setNumOfReceiver();
}

//SELECT ALL TEACHER TO SEND SMS - ONCLICK DIV EVENT
function CheckAllReceiverToSend(thisObject) {
    if ($('.IconCheckAllReceiver').hasClass('IconEnable')) {
        // neu la uncheck
        $('.IconCheckAllReceiver').removeClass('IconEnable');
        $('.IconCheckAllReceiver').addClass('IconDisable');
        $('.ReceiverPopup:visible').find('.IconCheckReceiver').removeClass('IconEnable');
        $('.ReceiverPopup:visible').find('.IconCheckReceiver').addClass('IconDisable');
        // neu dang currentSearch la defaultSearch thi set searchObject CheckAll = false vs CheckAllNotLoad = false vs hasCheckAll = false
        // nguoc lai tim searchObject tung ung va set lai order va CheckAll = false vs CheckAllNotLoad = false vs HasCheckAll = true
        var objSearchObject;
        if (currentSearchObject.Key == defaultSearchObject.Key) {
            defaultSearchObject.CheckAll = false;
            defaultSearchObject.CheckAllNotLoad = false;
            for (var i = arrSearchObject.length - 1; i >= 0; i--) {
                objSearchObject = arrSearchObject[i];
                objSearchObject.CheckAll = false;
                objSearchObject.CheckAllNotLoad = false;
                objSearchObject.HasCheckAll = false;
            }
        }
        else {
            defaultSearchObject.CheckAll = false;
            for (var i = arrSearchObject.length - 1; i >= 0; i--) {
                objSearchObject = arrSearchObject[i];
                if (objSearchObject.Key == currentSearchObject.Key) {
                    //set lai orderID trong truong hop search 1 key 2 lan
                    if (objSearchObject.OrderID < orderIDSearch) {
                        objSearchObject.OrderID = orderIDSearch;
                        //order lai list searchObject;
                        arrSearchObject = arrSearchObject.sort(function (a, b) { return a.OrderID - b.OrderID; });
                    }

                    objSearchObject.CheckAll = false;
                    objSearchObject.CheckAllNotLoad = false;
                    objSearchObject.HasCheckAll = true;
                    currentSearchObject = objSearchObject;
                    break;
                }
            }
        }
    }
    else {
        // neu la check
        $('.IconCheckAllReceiver').removeClass('IconDisable');
        $('.IconCheckAllReceiver').addClass('IconEnable');
        $('.ReceiverPopup:visible').find('.IconCheckReceiver').removeClass('IconDisable');
        $('.ReceiverPopup:visible').find('.IconCheckReceiver').addClass('IconEnable');
        // neu dang currentSearch la defaultSearch thi set searchObject CheckAll = true vs CheckAllNotLoad = true vs HasCheckAll = false
        // nguoc lai tim searchObject tung ung va set lai order va CheckAll = true vs CheckAllNotLoad = true vs HasCheckAll = true
        if (currentSearchObject.Key == defaultSearchObject.Key) {
            defaultSearchObject.CheckAll = true;
            defaultSearchObject.CheckAllNotLoad = true;
            for (var i = arrSearchObject.length - 1; i >= 0; i--) {
                objSearchObject = arrSearchObject[i];
                objSearchObject.CheckAll = true;
                objSearchObject.CheckAllNotLoad = true;
                objSearchObject.HasCheckAll = false;
            }
        }
        else {
            // kiem tra checkAll khac defaultSearchObject
            var numReceiverIsCheck = $('.OverflowReceiverClass').find('.IconEnable').length;
            var numOfReceiverIsLoad = $('.OverflowReceiverClass').find('.IconStyle').length;
            if (numReceiverIsCheck == numOfReceiverIsLoad && numOfReceiverIsLoad > 0) {
                defaultSearchObject.CheckAll = true;
            }

            for (var i = arrSearchObject.length - 1; i >= 0; i--) {
                objSearchObject = arrSearchObject[i];
                if (objSearchObject.Key == currentSearchObject.Key) {
                    // set lai orderID trong truong hop search 1 key 2 lan
                    if (objSearchObject.OrderID < orderIDSearch) {
                        objSearchObject.OrderID = orderIDSearch;
                        // order lai list searchObject;
                        arrSearchObject = arrSearchObject.sort(function (a, b) { return a.OrderID - b.OrderID; });
                    }

                    objSearchObject.CheckAll = true;
                    objSearchObject.CheckAllNotLoad = true;
                    objSearchObject.HasCheckAll = true;
                    currentSearchObject = objSearchObject;
                    break;
                }
            }
        }
    }

    // set num of receiver
    setNumOfReceiver();
}

// SET NUM OF RECEIVER - ONCHANGE CHECKBOX EVENT
function setNumOfReceiver() {
    //set num of receiver
    var numReceiverIsCheck = $('#GridSchool').find('.t-grid-content').find(".csCheckedIds:checked").length;
    
    $('.NumOfHeadMasterReceiver').html(numReceiverIsCheck);
    
}

//ENBALE BUTTON SEND - TXTAREA EVENT ONCHANGE
function enableButtonSend() {
    //neu khong thay doi noi dung thi disable button send
    // Edit AnhVD9 20150629
    var BalanceOfEWallet = $.trim($('.BalanceOfEWallet').html());
    var NumOfPromotionInternalSMS = $.trim($('.NumOfPromotionInternalSMS').html());
    if (BalanceOfEWallet != "" && NumOfPromotionInternalSMS != "") {
        // parse
        BalanceOfEWallet = parseInt(BalanceOfEWallet);
        NumOfPromotionInternalSMS = parseInt(NumOfPromotionInternalSMS);

        if (BalanceOfEWallet < 0 && NumOfPromotionInternalSMS < 0) {
            $('.BtnCenterSection').find('#btSendSMS').attr('disabled', 'disabled');
        } else {
            var strContent = $.trim($('#txtContentToSend').val().toUpperCase());
            if (strContent == $.trim(prefixName.toUpperCase()) || strContent == $.trim(brandNamePrefix.toUpperCase())) {
                $('.BtnCenterSection').find('#btSendSMS').attr('disabled', 'disabled');
            }
            else {
                $('.BtnCenterSection').find('#btSendSMS').removeAttr('disabled');
            }
        }
    } else {
        var strContent = $.trim($('#txtContentToSend').val().toUpperCase());
        if (strContent == $.trim(prefixName.toUpperCase()) || strContent == $.trim(brandNamePrefix.toUpperCase())) {
            $('.BtnCenterSection').find('#btSendSMS').attr('disabled', 'disabled');
        }
        else {
            $('.BtnCenterSection').find('#btSendSMS').removeAttr('disabled');
        }
    }
}

//VALIDATE BEFORE SEND - ONCLICK BUTTON EVENT
function validateBeforeSend() {
    if ($('.NumOfHeadMasterReceiver').html() < 1) {
        smas.alert({ Type: 'error', Message: lblValidateBeforeSend });
    }
    else {
        smas.openDialog('ConfirmToSendSMS', '', {});
    }
}

//CONTINUE TO SEND SMS - ONCLICK BUTTON OK EVENT
function continueSendSMS() {
    cancelSendSMS();
    sendSMSToReceiver();
}

//CANCEL TO SEND SMS - ONCLICK BUTTON CANCEL EVENT
function cancelSendSMS() {
    smas.closeDialog('ConfirmToSendSMS');
}

//SEND SMS - ONCLICK INPUT EVENT
function sendSMSToReceiver() {
    var arrIsCheckReceiverID = ',';
    var idCheck = '';
    $('#GridSchool').find('.t-grid-content').find(".csCheckedIds:checked").each(function () {
        var idCheck = $(this).val();
        
        arrIsCheckReceiverID += idCheck;
        arrIsCheckReceiverID += ',';
    })

    $.ajax({
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        url: url_sendSMSToReceiver,
        data: {
            EducationGrade: educationGrade, ArrIsCheckReceiverID: arrIsCheckReceiverID, Content: $.trim($('#txtContentToSend').val()), 
            AllowSignMessage: $('#AllowSignMessage').is(':checked'), __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (data) {
            smas.alert(data);
            if (data.Type != "error") {
                $('#txtContentToSend').val(prefixName);
                validateUtils.CountSMS('txtContentToSend', 'LengSMSToTeacher', $('#AllowSignMessage').is(':checked'));

                // display after send successful
                $('.GroupPerson .BalanceOfEWallet').html(data.BalanceOfEWallet);
                $('.GroupPerson .NumOfPromotionInternalSMS').html(data.NumOfPromotionInternalSMS);

                $('#btSendSMS').attr('disabled', 'disabled');
                AjaxLoadGrid();
            }
        },
        error: function (xmlHttpRequest, status, errorThrow) {
            smas.alert(xmlHttpRequest.responseText);
        }
    });
}

//CHANGE CHECKBOX WHEN OVER - ONMOUSEROVER DIV EVENT
function onMouseOverReceiver(thisObject) {
    $(thisObject).find('.IconStyle').addClass('is-ready-enable');
}

//CHANGE CHECKBOX WHEN OUT - ONMOUSEOUT DIV EVENT
function onMouseOutReceiver(thisObject) {
    $(thisObject).find('.IconStyle').removeClass('is-ready-enable');
}

//PREVENT ENTER SUBMIT - ONKEYDOWN INPUT EVENT
function preventEnterSubmit(eventObject) {
    if (eventObject.keyCode == 13) {
        //fix on IE
        if (isIE78) {
            eventObject.returnValue = false;
        }
        else {
            eventObject.preventDefault();
        }
    }
}

///////////////////////////////////// SEND UNICODE MESSAGE  \\\\\\\\\\\\\\\\\
function checkAllowSignMsg(chk) {
    var isCheck = $(chk).is(':checked');
    if (isCheck) {
        $('#ContentInfoMsg').text('(67 ký tự/SMS. Chỉ gửi được tin nhắn có dấu cho các mạng Viettel, Vina, Mobi.)');
    } else {
        $('#ContentInfoMsg').text('');
    }
    validateUtils.CountSMS('txtContentToSend', 'LengSMSToTeacher', isCheck);
}


function checkAll(cb) {
    $(cb).parents('#GridSchool').find(':checkbox').attr('checked', cb.checked);
    setNumOfReceiver();
};

function checkItem(cb) {

    if ($("#GridSchool").find(".csCheckedIds:not(:checked)").length == 0) {
        $('#GridSchool #csCheckAll').attr('checked', true);
    }
    else {
        $('#GridSchool #csCheckAll').attr('checked', false);
    }

    setNumOfReceiver();
};

//VALIDATE CONTENT WHEN PASTE - ONPASTE EVENT TEXTAREA
$('#txtContentToSend').on('cut paste', function () {
    //vi su kien chay truoc khi text dang paste vao nen phai settimeout
    setTimeout(function () {
        validateUtils.SetPrefixSMS($('#txtContentToSend'));
        validateUtils.CountSMS('txtContentToSend', 'LengSMSToTeacher', $('#AllowSignMessage').is(':checked'));
        enableButtonSend();
    }, 200);
});

/*_ContentDetail*/
$(function () {
    // click load default 
    $('.ListLevelSelector').find('ul').find('ul').find('li').first().find('a').click();
});