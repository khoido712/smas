﻿

function SendSMSComment(classID, subjectID, semesterID, dayOfMonthID) {
    var fromDate = $('#FromDateSMSComment').val();
    var toDate = $('#ToDateSMSComment').val();
    if (fromDate == '') {
        smas.alert({ Type: 'error', Message: 'Từ ngày không được để trống' });
        return false;
    }
    if (toDate == '') {
        smas.alert({ Type: 'error', Message: 'Đến ngày không được để trống' });
        return false;
    }
    if (fromDate > toDate) {
        smas.alert({ Type: 'error', Message: 'Từ ngày phải nhỏ hơn Đến ngày' });
        return false;
    }
    fromDate = dateUtils.ConvertDateUS(fromDate);
    toDate = dateUtils.ConvertDateUS(toDate);
    var isNoDate = $('#chkNDate').is(':checked') ? 1 : 0;
    var isNoSubject = $('#chkNSubjectID').is(':checked') ? 1 : 0;
    var allowSignMessage = $('#chkallowSignMessage').is(':checked') ? 1 : 0;
    var token = $('[name=__RequestVerificationToken]').val();

    $.ajax({
        url: urlSendSMSComment,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { ClassID: classID, SubjectID: subjectID, SemesterID: semesterID, DayOfMonthID: dayOfMonthID, FromDate: fromDate, ToDate: toDate, NoDate: isNoDate, NoSubject: isNoSubject, AllowSignMessage: allowSignMessage, __RequestVerificationToken: token },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            smas.alert({ Type: msg.Type, Message: msg.Message });
            smas.hideProcessingDialog();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        }
    });
}

function ExportFile(classID, subjectID, semesterID, dayOfMonthID)
{
    var fromDate = $('#FromDateSMSComment').val();
    var toDate = $('#ToDateSMSComment').val();
    if (fromDate == '') {
        smas.alert({ Type: 'error', Message: 'Từ ngày không được để trống' });
        return false;
    }
    if (toDate == '') {
        smas.alert({ Type: 'error', Message: 'Đến ngày không được để trống' });
        return false;
    }
    if (fromDate > toDate) {
        smas.alert({ Type: 'error', Message: 'Từ ngày phải nhỏ hơn Đến ngày' });
        return false;
    }
    fromDate = dateUtils.ConvertDateUS(fromDate);
    toDate = dateUtils.ConvertDateUS(toDate);
    var isNoDate = $('#chkNDate').is(':checked') ? 1 : 0;
    var isNoSubject = $('#chkNSubjectID').is(':checked') ? 1 : 0;
    var allowSignMessage = $('#chkallowSignMessage').is(':checked') ? 1 : 0;
    var url = urlExportFile;
    url += "?ClassID=" + classID + "&SubjectID=" + subjectID + "&SemesterID=" + semesterID + "&DayOfMonthID=" + dayOfMonthID
        + "&FromDate=" + fromDate + "&ToDate=" + toDate + "&NoDate=" + isNoDate + "&NoSubject=" + isNoSubject + "&AllowSignMessage=" + allowSignMessage;
    window.location = url;
}