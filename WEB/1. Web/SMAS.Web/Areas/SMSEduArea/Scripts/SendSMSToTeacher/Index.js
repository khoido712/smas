﻿/*auto load default group (group all) */
//$(document).ready(function () {  
//        //Fire the first element
//        var item = document.getElementsByName("itemContactGroup");
//        if (item) {
//            if (typeof item[0].onclick == "function") {
//                $(item[0]).click();
//            }
//        }

//    });

preFixSendSMS = stringUtils.HtmlDecode(preFixSendSMS);
/* index page*/
function LoadSMSEditor(item) {
    isShowTipSearch = true;
    var groupID = $(item).attr("ContactGroupID");
    var itms = document.getElementsByName("itemContactGroup");
    for (var i = 0; i < itms.length; i++) {
        $(itms[i]).attr("class", "");
    }

    $(item).attr("class", "Active");
    $.ajax({
        url: url_LoadSMSEditor,
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        data: { ContactGroupID: groupID },
        beforeSend: function () {
            smas.showProcessingDialog();
        },
        success: function (msg) {
            $("#div_sms_editor").html(msg);
            smas.hideProcessingDialog();
            setTimeout(function () {
                $('#btSendSMS').attr("disabled", "disabled");
                $('#btSendSMS').addClass('t-state-disabled');

            }, 500);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            smas.alert(XMLHttpRequest.responseText);
            smas.hideProcessingDialog();
        },
        complete: function () {
            setTimeout(function () {
                $('#colapseFuncSMS').click();
            }, 500);
        }
    });


}

function ColapseFuncSMS(thisObject) {
    thisObject = thisObject || $('#colapseFuncSMSDefault');
    if ($(thisObject).hasClass("Sclose")) {
        $(thisObject).removeClass("Sclose");
        $(thisObject).addClass("Eclose");
        if (_isDefaultGroup) {
            $('.ReceiverName').slideUp();
        }
        else {
            $(".ReceiverGroup").css("height", "50");
            //them ... neu list lon hon 2 dong
            groupPersonTemp = $('.ContentListSection .ReceiverGroup p').html();
            if (groupPersonTemp != null) {
                if (groupPersonTemp.length > 180) {
                    $('.ContentListSection .ReceiverGroup p').html(stringUtils.ToShortSMSContent(groupPersonTemp, 180));
                }
            }
        }
    }
    else {
        // _isDefaultGroup = $('#isDefaultGroup').val();
        $(thisObject).removeClass("Eclose");
        $(thisObject).addClass("Sclose");
        if (_isDefaultGroup) {
            $('.ReceiverName').slideDown();
        }
        else {
            $(".ReceiverGroup").css("height", "100%");
            $('.ContentListSection .ReceiverGroup p').html(groupPersonTemp);
        }
    }
    smas.hideProcessingDialog();
}
//LOAD LIST TEACHER RECEIVER - ONCLICK DIV EVENT
function LoadListReceiver(contactGroupID) {
    if ($('#ListTeacherReceiverSMS').html() != '') {
        //colapse/expanse
        if ($('#colapseFunListTeacher').hasClass('Sclose')) {
            $('#ListTeacherReceiverSMS').slideUp();
            $('#colapseFunListTeacher').removeClass("Sclose");
            $('#colapseFunListTeacher').addClass("Eclose");
        }
        else {
            $("#ListTeacherReceiverSMS").slideDown();
            $('#colapseFunListTeacher').removeClass("Eclose");
            $('#colapseFunListTeacher').addClass("Sclose");
        }
        smas.hideProcessingDialog();
    }
    else {
        //load du lieu
        AjaxLoadGrid(contactGroupID);
    }
}



//SERACH TEACHER - ONKEYUP EVENT INPUT
function searchTeacherReceiver(thisObject) {
    //check maxlength <= 200
    value = $(thisObject).val();
    if (value.length > 200) {
        value = value.substr(0, 200);
        $(thisObject).val(value);
    }
    //khi dang typing thi khong thu hien tim kiem       
    if (isTyping) {
        clearTimeout(isTyping);
    }
    isTyping = setTimeout(function () {
        //chi tim kiem voi truong hop khac rong
        if ($.trim($(thisObject).val()) != '') {
            //cat dau, cat khoang trang        
            value = stringUtils.StripVNSign($.trim(value)).toLowerCase();
            var receiverName;
            var receiverCode;
            var receiverGenreName;
            //search doi voi ten giao vien, duoc cat dau va remove khoang trang
            $('#GridTeacher').find(".t-grid-content").find('tr').each(function () {
                receiverName = $.trim(stringUtils.StripVNSign($(this).find('td:nth-child(2)').html()).toLowerCase());
                receiverCode = $.trim(stringUtils.StripVNSign($(this).find('td:nth-child(3)').html()).toLowerCase());
                receiverGenreName = $.trim(stringUtils.StripVNSign($(this).find('td:nth-child(4)').html()).toLowerCase());
                if (receiverName.indexOf(value) < 0 && receiverCode.indexOf(value) < 0 && receiverGenreName.indexOf(value) < 0) { // neu khong tim thay
                    $(this).css('display', 'none');
                }
                else {// tim thay
                    $(this).css('display', '');
                }
            });
        }
        else {
            //neu la rong thi show all
            $('#GridTeacher').find('tr').css('display', '');
        }
        //chi ket qua tim kiem chi hien thi nhung giao vien duoc chon thi tick chon button check all
        // Congnv fix bug: Bug #20379 [Redmine]
        var disableCount = $('#GridTeacher').find('tr:visible').find('.csCheckedIds:not(checked)').length;
        var enableCount = $('#GridTeacher').find('tr:visible').find('.csCheckedIds:checked').length;
        if (disableCount <= 0 && enableCount <= 0) {
            $('#GridTeacher #csCheckAll').attr('checked', false);
        }
        else if (disableCount <= 0) {
            $('#GridTeacher #csCheckAll').attr('checked', true);
        }
        else {
            $('#GridTeacher #csCheckAll').attr('checked', false);
        }

        //set lai du lieu nguoi nhan
        setReceiverName();
    }, 400);
};



///////////////////////////////////// SEND UNICODE MESSAGE  \\\\\\\\\\\\\\\\\
function checkAllowSignMsg(chk) {
    var isCheck = $(chk).is(':checked');
    if (isCheck) {
        $('#ContentInfoMsg').text('(67 ký tự/SMS. Chỉ gửi được tin nhắn có dấu cho các mạng Viettel, Vina, Mobi.)');
    } else {
        $('#ContentInfoMsg').text('');
    }
    validateUtils.CountSMS('SMSContent', 'LengSMSToTeacher', isCheck);
}

//SET RECEIVER NAME - ONCLICK IMG EVENT
function setReceiverName() {
    var strReceiverName = [];
    var strReceiverID = [];
    var numReceiver = 0;
    $('#GridTeacher').find('.csCheckedIds:checked').each(function () {
        strReceiverName.push($(this).closest('tr').find('td:nth-child(2)').html() + ', ');
        strReceiverID.push($(this).val() + ',');
        numReceiver += 1;
    });
    //set num of receiver
    $('.NumOfTeacherReceiver').html(numReceiver);
    $('#LstTeacherReceiverID').val(strReceiverID.join(''));
    NUMBER_OF_TEACHER = $('#HNumberOdTeacher').val();
    if (NUMBER_OF_TEACHER != "") {
        NUMBER_OF_TEACHER = parseInt(NUMBER_OF_TEACHER);
    }
    if (strReceiverName.length == NUMBER_OF_TEACHER) {
        $('#CheckAllReceiver').val(true);
    }
    else {
        $('#CheckAllReceiver').val(false);
    }
    if (_isDefaultGroup && strReceiverName.length == NUMBER_OF_TEACHER) { // Neu tat ca giao vien duoc chon thi hien thi ten nhom thay vi ten tat ca giao vien
        strReceiverName = ContactGroup_Label_DefaultGroup
        $('.ReceiverName').html(strReceiverName);
    }
    else {
        strReceiverName = strReceiverName.join('');
        strReceiverName = strReceiverName.substr(0, strReceiverName.length - 2);
        $('.ReceiverName').html(strReceiverName);
    }
    //set receiver name
    smas.hideProcessingDialog();
}
//CHANGE CHECKBOX WHEN OVER - ONMOUSEROVER DIV EVENT
function onMouseOverReceiver(thisObject) {
    $(thisObject).find('.IconStyle').addClass('is-ready-enable');
}

//CHANGE CHECKBOX WHEN OUT - ONMOUSEOUT DIV EVENT
function onMouseOutReceiver(thisObject) {
    $(thisObject).find('.IconStyle').removeClass('is-ready-enable');
}

//PREVENT ENTER SUBMIT - ONKEYDOWN INPUT EVENT
function preventEnterSubmit(eventObject) {
    if (eventObject.keyCode == 13) {
        //fix on IE
        if (isIE78) {
            eventObject.returnValue = false;
        }
        else {
            eventObject.preventDefault();
        }
    }
}

/*_SMSEditor*/
//Onready page methods
//$(document).ready(function () {
//    $.validator.unobtrusive.parse($('#frmMessage'));
//    $("#btSendSMS").attr("disabled", "disabled");
//    $('#SMSContent').val(preFixSendSMS);
//});
//Neu so luong giao vien qua lon thi thuc hien thong bao confirm gui
function CheckTeacher(e) {
    /* Hien thi popup canh bao khi gui tin nhan den hang loat giao vien cung luc --> đang OFF
    if(NUMBER_OF_TEACHER > 100){
        smas.openDialog("largeOfTeacherConfirm", "", {});
        return false;
    }
    */

    var strReceiverID = [];
    $('#GridTeacher').find('.csCheckedIds:checked').each(function () {
        strReceiverID.push($(this).val() + ',');
    });
    //set num of receiver
    $('#LstTeacherReceiverID').val(strReceiverID.join(''));
    return true;
}
function SendToTeacherWithoutSpecialChar() {
    $("#btHidSendSMS").click();
}
function loadWaiting() {
    smas.showProcessingDialog();
}

//Thong bao confirm OK
function okContinue() {
    onCancel();
    setTimeout(function () {
        $("#btsendSilient").click();
    }, 500);
}
//Thong bao Cancel
function onCancel(text) {
    if (text) {
        smas.closeDialog(text);
    }
    else {
        smas.closeDialog("largeOfTeacherConfirm");
    }
}

// redirect
function redirectPayment() {
    iFrameUtils.postSpecialCode({ code: 4, value: '/SMSEduArea/SMSEdu/Payment?topup=1' });
}

//Check Enable/Disable send Button
function enalbeButtonSend() {
    //neu khong thay doi noi dung thi disable button send
    preFixSendSMS = $.trim(prefixName);
    if ($.trim($('#SMSContent').val().toUpperCase()) == preFixSendSMS.toUpperCase()
        || $.trim($('#SMSContent').val().toUpperCase()) == $.trim(brandNamePrefix.toUpperCase())) { //prefix nam o layout
        $('#btSendSMS').attr('disabled', 'disabled');
        $('#btSendSMS').addClass('t-state-disabled');
    }
    else {
        $('#btSendSMS').removeAttr('disabled');
        $('#btSendSMS').removeClass('t-state-disabled');

    }
}

function SaveSuccess(msg) {
    smas.closeDialog("ShowErrorConfirm");
    if (msg.Type == "ContentError") {
        smas.openDialog("ShowErrorConfirm", urlShowErrorConfirm, { contentError: msg.Message });
    }
    else if (msg.Type == 'success') {
        smas.alert(msg);
        preFixSendSMS = stringUtils.HtmlDecode(preFix);
        //reset SMS content
        // $('#SMSContent').val(preFixSendSMS);
        //reset sms counter
        // $('#LengSMSToTeacher').html(preFixSendSMS.length);
        $('#CountSMSToTeacher').html(1);
        // Xóa nội dụng SMS vừa mới gửi.
        $('#SMSContent').val($.trim(preFixSendSMS));

        validateUtils.CountSMS('SMSContent', 'LengSMSToTeacher', $('#AllowSignMessage').is(':checked'));
        // Disable button gửi
        $('#btSendSMS').attr('disabled', 'disabled');
        $('#btSendSMS').addClass('t-state-disabled');

        if (msg.Balance) {
            $('.BalanceOfEWallet').html(msg.Balance);
            $('.NumOfPromotionInternalSMS').html(msg.NumOfPromotionInternalSMS);
        }
        var contactGroupID = $('#hdfContactGroupID').val();
        AjaxLoadGrid(contactGroupID);
        setTimeout(function () {
            setReceiverName();
        }, 800);
    }
    else {
        if (msg.OutOfBalance) {
            smas.openDialog('OutOfBalanceMsg', '', {});
        }
        else {
            smas.alert(msg);
        }
    }
}


function SaveFail(ajaxContext) {
    smas.alert(ajaxContext.responseText);
}

//VALIDATE CONTENT WHEN PASTE - ONPASTE EVENT TEXTAREA
$('#SMSContent').on('cut paste', function () {
    //vi su kien chay truoc khi text dang paste vao nen phai settimeout
    setTimeout(function () {
        validateUtils.CountSMS('SMSContent', 'LengSMSToTeacher', $('#AllowSignMessage').is(':checked'));
        //validateUtils.validationSMS('SMSContent', 'Msg', 'LengSMSToTeacher', 'CountSMSToTeacher');
        if (allowSend == 'true') {
            enalbeButtonSend();
        }
    }, 200);
});

function AjaxLoadGrid(contactGroupID)
{
    //load du lieu
    $.ajax({
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        url: url_LoadListReceiver,
        data: { contactGroupID: contactGroupID },
        beforeSend: function () {
            $('.IconLoadingReceiver').show();
        },
        success: function (data) {
            $('#ListTeacherReceiverSMS').show();
            $('#ListTeacherReceiverSMS').html(data);
            $('#colapseFunListTeacher').removeClass("Eclose");
            $('#colapseFunListTeacher').addClass("Sclose");
            $('.IconLoadingReceiver').hide();
            smas.hideProcessingDialog();
        },
        error: function (xmlHttpRequest, status, throwError) {
            $('.IconLoadingReceiver').hide();
            smas.alert(xmlHttpRequest.responseText);
        }
    });
}

function checkAll(cb) {
    $(cb).parents('#GridTeacher').find(':checkbox').attr('checked', cb.checked);
    setReceiverName();
};

function checkItem(cb) {

    if ($("#GridTeacher").find(".csCheckedIds:not(:checked)").length == 0) {
        $('#GridTeacher #csCheckAll').attr('checked', true);
    }
    else {
        $('#GridTeacher #csCheckAll').attr('checked', false);
    }

    setReceiverName();
};

/*_ListTeacherReceiver*/
$(document).ready(function () {
    //AUTO RESIZE HEIGHT
    var heightResize = 0;
    var heightTemp = 0;
    $('.OverflowReceiverClass').find('.ReceiverPopup').each(function () {
        heightResize = $(this).find('#lblTeacherName').height();
        heightTemp = $(this).find('#lblEmployeeCode').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }
        heightTemp = $(this).find('#lblFaculty').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }
        heightTemp = $(this).find('.ItemReceiverEnd').height();
        if (heightTemp > heightResize) {
            heightResize = heightTemp;
        }
        if (heightResize > 15) {
            $(this).find('.IconStyle').css('height', heightResize + 'px');
            $(this).find('.LabelStyle').css('height', heightResize + 'px');
        }
    });
});

$(function () {
    var itms = document.getElementsByName("itemContactGroup");
    $(itms[0]).attr("class", "Active");
    $('#btSendSMS').attr("disabled", "disabled");
    $('#btSendSMS').addClass('t-state-disabled');

    if (isAdmin == 'False') {
        if (itms && itms.length > 0) {
            if (typeof itms[0].onclick == "function") {
                $(itms[0]).click();
            }
        }
    }
});
