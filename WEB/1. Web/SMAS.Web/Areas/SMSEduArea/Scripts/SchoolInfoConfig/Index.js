﻿function CheckCheckBox() {
    var allMark = allMark_SchoolConfig;
    var capacityI = capacityI_SchoolConfig;
    var capacityII = capacityII_SchoolConfig;
    var showLock = showLock_SchoolConfig;
    var showMobile = showMobile_SchoolConfig;
    var registerSLL = registerSLL_SchoolConfig;
    var allowSp2Online = allowSp2Online_SchoolConfig;
    //var paymentType = paymentType_SchoolConfig;
    //var disabled_PaymentType = disabled_PaymentType_SchoolConfig;

    var allowSendToHeadTeacher = allowSendSMSToHeadTeacher_SchoolConfig;
    var allowSendUnicodeMessage = allowSendUnicodeMessage_SchoolConfig;

    if (allMark == 'true') {
        $("#SearchAllMark").attr("checked", "checked");
        $("#lblAllMark").html(label_ViewAllMark_SchoolConfig);
        $("#SearchLock").attr("checked", "checked");
        $("#SearchLock").attr("disabled", "disabled");

    } else {
        $("#SearchAllMark").remove("checked", "checked");
        $("#lblAllMark").html(label_Search_LockMark_SchoolConfig);

        $("SearchLock").removeAttr("disabled");
    }
    if (capacityI == "true") {
        $("#SearchHKI").attr("checked", "checked");
        $("#lblSHKI").html(label_ViewHKI_SchoolConfig);
    }
    else {
        $("#SearchHKI").remove("checked", "checked");
        $("#lblSHKI").html(label_Search_LockMark_HKI_SchoolConfig);
    }
    if (capacityII == "true") {
        $("#SearchHKII").attr("checked", "checked");
        $("#lblSHKII").html(label_ViewHKII_SchoolConfig);
    } else {
        $("#SearchHKII").remove("checked", "checked");
        $("#lblSHKII").html(label_Search_LockMark_KHII_CN_SchoolConfig);
    }

    if (showLock == "true" || allMark == 'true') {
        $("#SearchLock").attr("checked", "checked");
        $("#lblShowLock").html("PHHS được xem các con điểm đã khóa (đã chốt)");
    } else {
        $("#SearchLock").remove("checked", "checked");
        $("#lblShowLock").html("");
    }

    if (showMobile == "true") {
        $("#ShowTeacherMobile").attr("checked", "checked");
        $("#lblShowMobile").html("PHHS sẽ được xem Số điện thoại của Giáo viên");
    } else {
        $("#ShowTeacherMobile").remove("checked", "checked");
        $("#lblShowMobile").html("");
    }

    if (registerSLL == "true") {
        $("#RegisterSLL").attr("checked", "checked");
        $("#AllowSp2Online").removeAttr("disabled");
    } else {
        $("#RegisterSLL").remove("checked", "checked");
        $('#AllowSp2Online').prop('checked', false);
        $("#AllowSp2Online").remove("checked", "checked");
        $("#AllowSp2Online").attr("disabled", "disabled");        
    }
    if (allowSp2Online == "true") {
        $("#AllowSp2Online").attr("checked", "checked");
    } else {
        $("#AllowSp2Online").remove("checked", "checked");
    }

    // AnhVD9 20150819 - Tạm thời chưa dùng
    //if (paymentType > 0) {
    //    $("input[type='radio'][name='PaymentType']").each(function () {
    //        if ($(this).val() == paymentType) {
    //            $(this).prop('checked', true);
    //        }
    //    });

    //    if (disabled_PaymentType == "true") {
    //        $("input[type='radio'][name='PaymentType']").each(function () {
    //            $(this).attr("disabled", "disabled");
    //        });
    //        $('.msgCancelContract').html(label_Message_CancelContract);
    //    } else {
    //        $("input[type='radio'][name='PaymentType']").each(function () {
    //            $(this).removeAttr("disabled");
    //        });
    //        $('.msgCancelContract').html('');
    //    }
    //}

    if (allowSendToHeadTeacher == "true") {
        $("#AllowSendSMSToHeadTeacher").attr("checked", "checked");
    } else {
        $("#AllowSendSMSToHeadTeacher").remove("checked", "checked");
    }

    if (allowSendUnicodeMessage == "true") {
        $("#AllowSendUnicodeMessage").attr("checked", "checked");
    } else {
        $("#AllowSendUnicodeMessage").remove("checked", "checked");
    }
    
}
function ChangeCheckBox() {
    var allMark = $("#SearchAllMark").is(':checked');
    var searchHKI = $("#SearchHKI").is(':checked');
    var searchHKII = $("#SearchHKII").is(':checked');
    var showLock = $("#SearchLock").is(':checked');
    var showMobile = $("#ShowTeacherMobile").is(':checked');
    var chckHeadTeacher = $("#ChckHeadTeacher").is(':checked');
    var chckSubTeacher = $("#ChckSubTeacher").is(':checked');
    var chkRegisterSLL = $("#RegisterSLL").is(':checked');
    var chkAllowSp2Online = $("#AllowSp2Online").is(':checked');

    var allowSendToHeadTeacher = $("#AllowSendSMSToHeadTeacher").is(':checked');
    var allowSendUnicodeMessage = $("#AllowSendUnicodeMessage").is(':checked');

    if (allMark == true) {
        $("#SearchAllMark").attr("checked", "checked");
        $("#lblAllMark").html(label_ViewAllMark_SchoolConfig);
        $("#SearchLock").attr("checked", "checked");
        $("#SearchLock").attr("disabled", "disabled");
    } else {
        $("#SearchAllMark").remove("checked", "checked");
        $("#lblAllMark").html(label_Search_LockMark_SchoolConfig);
        $("#SearchLock").removeAttr("disabled");
    }
    if (searchHKI == true) {
        $("#SearchHKI").attr("checked", "checked");
        $("#lblSHKI").html(label_ViewHKI_SchoolConfig);
    }
    else {
        $("#SearchHKI").remove("checked", "checked");
        $("#lblSHKI").html(label_Search_LockMark_HKI_SchoolConfig);
    }
    if (searchHKII == true) {
        $("#SearchHKII").attr("checked", "checked");
        $("#lblSHKII").html(label_ViewHKII_SchoolConfig);
    } else {
        $("#SearchHKII").remove("checked", "checked");
        $("#lblSHKII").html(label_Search_LockMark_KHII_CN_SchoolConfig);
    }

    if (showLock == true || allMark == true) {
        $("#SearchLock").attr("checked", "checked");
        $("#lblShowLock").html("PHHS được xem các con điểm đã khóa (đã chốt)");
    } else {
        $("#SearchLock").remove("checked", "checked");
        $("#lblShowLock").html("");
    }

    if (showMobile == true) {
        $("#ShowTeacherMobile").attr("checked", "checked");
        $("#lblShowMobile").html("PHHS sẽ được xem Số điện thoại của Giáo viên");
    } else {
        $("#ShowTeacherMobile").remove("checked", "checked");
        $("#lblShowMobile").html("");
    }

    if (chkRegisterSLL == true) {
        $("#RegisterSLL").attr("checked", "checked");
        $("#AllowSp2Online").removeAttr("disabled");
    } else {
        $("#RegisterSLL").remove("checked", "checked");
        setTimeout(function () { $('#AllowSp2Online').removeAttr('checked'); },200);
        $("#AllowSp2Online").remove("checked", "checked");
        $("#AllowSp2Online").attr("disabled", "disabled");
    }
    if (chkAllowSp2Online == true) {
        $("#AllowSp2Online").attr("checked", "checked");
    } else {
        $("#AllowSp2Online").remove("checked", "checked");
    }

    if (allowSendToHeadTeacher == true) {
        $("#AllowSendSMSToHeadTeacher").attr("checked", "checked");
    } else {
        $("#AllowSendSMSToHeadTeacher").remove("checked", "checked");
    }

    if (allowSendUnicodeMessage == "true") {
        $("#AllowSendUnicodeMessage").attr("checked", "checked");
    } else {
        $("#AllowSendUnicodeMessage").remove("checked", "checked");
    }
}
function SaveConfig() {
    smas.showProcessingDialog();
    var allMark = $("#SearchAllMark").is(':checked');
    var searchHKI = $("#SearchHKI").is(':checked');
    var searchHKII = $("#SearchHKII").is(':checked');
    var searchLock = $("#SearchLock").is(':checked');
    var showMobile = $("#ShowTeacherMobile").is(':checked');
    var registerSLL = $("#RegisterSLL").is(":checked");
    var allowSp2Online = $("#AllowSp2Online").is(":checked");
    var allowSendToHeadTeacher = $("#AllowSendSMSToHeadTeacher").is(':checked');
    var allowSendUnicodeMessage = $("#AllowSendUnicodeMessage").is(':checked');
    var prefix = $("#Prefix").val();
    var nameDisplay = $("#NameDisplay").val();
    // AnhVD9 20150819 - Tạm thời chưa dùng - Mặc định là Trả trước
    //var paymentType = $("#PaymentType:checked").val();
    var paymentType = 1;
    $.ajax({
        url: "/SMSEduArea/SchoolInfoConfig/InsertOrUpdateSearchAllMark",
        // chua dung tocken data: { 'AllMark': AllMark, 'SearchHKI': SearchHKI, 'SearchHKII': SearchHKII, @Html.ViettelAntiForgeryTokenForAjaxPost() },//chua
        data: {
            'AllMark': allMark, 'SearchHKI': searchHKI, 'SearchHKII': searchHKII, 'SearchLock': searchLock, 'ShowTeacherMobile': showMobile, 'RegisterSLL': registerSLL, 'AllowSp2Online': allowSp2Online,
            'AllowSendSMSToHeadTeacher': allowSendToHeadTeacher, 'AllowSendUnicodeMessage': allowSendUnicodeMessage,
            'PaymentType': paymentType, Prefix:prefix, NameDisplay:nameDisplay, __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
        },
        type: 'post',
        xhrFields: {
            withCredentials: true
        },
        datatype: 'json',
        success: function (data) {
            smas.hideProcessingDialog();
            smas.alert(data);
        }
    });
}

$(function () {
    if (notIsAdmin_SchoolConfig == notIs_Admin_SchoolRole_SchoolConfig) {
        $('#BtnSave').remove();
        CheckCheckBox();
    }
    else {
        CheckCheckBox();
        if (disabled_SchoolConfig == "Disabled") {
            $('#BtnSave').remove();
        }
    }
});