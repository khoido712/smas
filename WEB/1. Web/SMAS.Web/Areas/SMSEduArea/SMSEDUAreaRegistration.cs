﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SMSEduArea
{
    public class SMSEDUAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SMSEDUArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SMSEDU_default",
                "SMSEDUArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
