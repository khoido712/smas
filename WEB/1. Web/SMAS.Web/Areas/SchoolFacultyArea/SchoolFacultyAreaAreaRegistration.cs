﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolFacultyArea
{
    public class SchoolFacultyAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolFacultyArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolFacultyArea_default",
                "SchoolFacultyArea/{controller}/{action}/{ID}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}