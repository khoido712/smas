﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SchoolFacultyArea.Models
{
    public class SchoolFacultyObject
    {
        [ScaffoldColumn(false)]
        public int SchoolFacultyID { get; set; }

        [ResourceDisplayName("SchoolFaculty_Control_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FacultyName { get; set; }
                
        [ResourceDisplayName("SchoolFaculty_Column_Description")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? CreateDateTime { get; set; }

        public string EmployeeAplied { get; set; }
    }
}