﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolFacultyArea.Models
{
    public class SchoolFacultyViewModel
    {
        public DateTime? CreatedDate { get; set; }
        public string Description { get; set; }
        public string FacultyName { get; set; }
        public string HeadMasterID { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int SchoolFacultyID { get; set; }
        public int SchoolID { get; set; }

        public string EmployeeApplied { get; set; }
    }
}