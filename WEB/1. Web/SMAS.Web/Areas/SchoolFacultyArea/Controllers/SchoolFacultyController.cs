﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SchoolFacultyArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using System;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.SchoolFacultyArea.Controllers
{
    public class SchoolFacultyController : BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public SchoolFacultyController(
            ISchoolFacultyBusiness schoolfacultyBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.SchoolFacultyBusiness = schoolfacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        #region index
        public ViewResult Index()
        {
            SetViewDataPermission("SchoolFaculty", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetDataView();
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("SchoolFaculty", globalInfo.UserAccountID, globalInfo.IsAdmin);

            Session[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_CREATE] = this.ListEmployee();
            return View();
        }
        #endregion index

        private List<string> ListEmployee()
        {
            List<EmployeeBO> lst = GetEmployee();

            List<string> myList = new List<string>();
            string fullName = string.Empty;
            string name = string.Empty;
            string facName = string.Empty;
            foreach (var item in lst)
            {
                name = item.FullName.Replace("-", "");
                facName = item.SchoolFacultyName.Replace("-", "");
                fullName = string.Format("{0} - {1} - {2}", name, item.EmployeeCode, facName);             
                myList.Add(fullName);
                name = string.Empty;
                facName = string.Empty;
            }
            return myList;
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetListEmployee()
        {
            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_CREATE] = ListEmployee();
            return Json(ListEmployee());
        }

        private List<EmployeeBO> GetEmployee()
        {
            List<Employee> lst = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                && x.EmploymentStatus == 1
                && x.IsActive == true).ToList();
            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.All
                .Where(x => x.SchoolID == _globalInfo.SchoolID.Value && x.IsActive == true).ToList();

            return (from lstE in lst
                    join lstSF in lstSchoolFaculty
                    on lstE.SchoolFacultyID equals lstSF.SchoolFacultyID
                    select new EmployeeBO
                    {
                        EmployeeID = lstE.EmployeeID,
                        EmployeeCode = lstE.EmployeeCode,
                        FullName = lstE.FullName,
                        SchoolFacultyName = lstSF.FacultyName
                    }).ToList();
        }

        #region SaveSchoolFaculty
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSchoolFaculty(FormCollection frm)
        {
            string arrEmployee = !string.IsNullOrEmpty(frm["PupilNameForCoE"]) ? frm["PupilNameForCoE"] : string.Empty;
            string facultyName = !string.IsNullOrEmpty(frm["FacultyName"]) ? frm["FacultyName"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["Description"]) ? frm["Description"] : string.Empty;

            if (!string.IsNullOrEmpty(facultyName))
            {
                List<string> lstEmployeeRequest = new List<string>();
                if (!string.IsNullOrEmpty(arrEmployee))
                {
                    lstEmployeeRequest = arrEmployee.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                }

                List<int> lstEmployeeAppled = new List<int>();
                List<EmployeeBO> lstEmployee = GetEmployee();
                foreach (var item in lstEmployeeRequest)
                {
                    foreach (var item2 in lstEmployee)
                    {
                        if (item == item2.EmployeeCode)
                        {
                            lstEmployeeAppled.Add(item2.EmployeeID);
                            break;
                        }
                    }
                }

                if (lstEmployeeAppled.Count >= 0)
                {
                    List<string> lstFacultyName = new List<string>();
                    string arrFacultyName = string.Empty;

                    facultyName = facultyName.Trim();
                    string strPattern = @"[\s]+";
                    Regex rgx = new Regex(strPattern);
                    string Ouput = rgx.Replace(facultyName, " ");
                    if (Ouput.Contains(","))
                    {
                        Ouput = Ouput.Replace(", ", ",");
                        lstFacultyName = Ouput.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim().ToString()).ToList();
                    }
                    else
                    {               
                        lstFacultyName.Add(Ouput);
                    }

                    Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                    List<SchoolFaculty> lst = this.SchoolFacultyBusiness.Search(SearchInfo).ToList();
                    SchoolFaculty objTest = null;

                   // SMAS.Web.Utils.Utils.StripVNSignAndSpace("Tổ A", true).ToUpper();
                    // danh sach to bo mon
                    List<string> lstFacultyNameApplied = new List<string>();
                    foreach (var item in lstFacultyName)
                    {
                        string fName = item.Trim();
                        objTest = lst.Where(x => x.FacultyName.ToUpper() == fName.ToUpper()).FirstOrDefault();
                        if (objTest != null)
                        {
                            string message = string.Format("Tổ bộ môn {0} đã tồn tại", fName);
                            return Json(new JsonMessage(message, "error"));
                        }
                        lstFacultyNameApplied.Add(item);

                        if (lstFacultyNameApplied.Where(p => p.ToUpper() == item.ToUpper()).Count() > 1)
                        {
                            string message = "Không thể thêm mới 2 tổ bộ môn trùng nhau";
                            return Json(new JsonMessage(message, "error"));
                        }
                    }

                    // danh sach can bo quan ly
                    string employeeAppled = string.Empty;
                    for (int i = 0; i < lstEmployeeAppled.Count(); i++)
                    {
                        if (i < lstEmployeeAppled.Count - 1)
                        {
                            employeeAppled += lstEmployeeAppled[i] + ", ";
                        }
                        else
                        {
                            employeeAppled += lstEmployeeAppled[i];
                        }
                    }

                    List<SchoolFaculty> lstSchoolFaculty = new List<SchoolFaculty>();
                    SchoolFaculty objSF = null;
                    foreach (var item in lstFacultyNameApplied)
                    {
                        string fName = item.Trim();
                        objSF = new SchoolFaculty()
                        {
                            SchoolID = _globalInfo.SchoolID.Value,
                            FacultyName = fName,
                            Description = description,
                            CreatedDate = DateTime.Now,
                            IsActive = true,
                            ModifiedDate = DateTime.Now,
                            HeadMasterID = employeeAppled
                        };
                        lstSchoolFaculty.Add(objSF);
                    }
                    this.SchoolFacultyBusiness.InsertSchoolFaculty(lstSchoolFaculty);
                    return Json(new JsonMessage(Res.Get("Thêm mới thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
        }
        #endregion

        [ValidateAntiForgeryToken]
        public PartialViewResult GetEmployeeBinding(string arrEmployee)
        {
            arrEmployee = arrEmployee.Replace(" ", "");
            List<string> lstEmployeeId = arrEmployee.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
            List<EmployeeBO> lstResult = GetEmployee().Where(x => !lstEmployeeId.Contains(x.EmployeeCode)).ToList();

            List<string> myList = new List<string>();
            string fullName = string.Empty;
            string name = string.Empty;
            string facName = string.Empty;
            foreach (var item in lstResult)
            {
                name = item.FullName.Replace("-", "");
                facName = item.SchoolFacultyName.Replace("-", "");
                fullName = string.Format("{0} - {1} - {2}", name, item.EmployeeCode, facName);
                myList.Add(fullName);
                name = string.Empty;
                facName = string.Empty;
            }

            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_CREATE] = myList;
            return PartialView("_ViewComboBoxMulti");
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult PartialComboBox(string arrEmployee)
        {
            arrEmployee = arrEmployee.Replace(" ", "");
            List<string> lstEmployeeId = arrEmployee.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
            List<EmployeeBO> lstResult = GetEmployee().Where(x => !lstEmployeeId.Contains(x.EmployeeCode)).ToList();

            List<string> myList = new List<string>();
            string fullName = string.Empty;
            string name = string.Empty;
            string facName = string.Empty;
            foreach (var item in lstResult)
            {
                name = item.FullName.Replace("-", "");
                facName = item.SchoolFacultyName.Replace("-", "");
                fullName = string.Format("{0} - {1} - {2}", name, item.EmployeeCode, facName);
                myList.Add(fullName);
                name = string.Empty;
                facName = string.Empty;
            }

            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_EDIT] = myList;
            return PartialView("_ViewComboBoxMultiEdit");
        }

        #region old fuc
        //
        // GET: /SchoolFaculty/Create
        public ActionResult Create()
        {
            SetDataView();
            return View();
        }

        //
        // POST: /SchoolFaculty/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        public JsonResult Create(SchoolFacultyObject sfo)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            if (GetMenupermission("SchoolFaculty", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //kiem tra Tỗ bộ môn rỗng
            SMAS.Business.Common.Utils.ValidateRequire(sfo.FacultyName,
                  globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                  || globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN
                  ? "Lable_Faculty_MN" : "SchoolFaculty_Control_FacultyName");

            // SchoolFacultyObject sfo = new SchoolFacultyObject();
            //Utils.Utils.BindTo(sfo, schoolfaculty);
            // TryUpdateModel(sfo);
            SchoolFaculty schoolfaculty = null;
            List<string> lstFacultyName = sfo.FacultyName.Split(',').ToList();
            string flagCheckFacultyName = lstFacultyName[0].Trim();
            string tempFacultyName = string.Empty;

            for (int i = 0; i < lstFacultyName.Count; i++)
            {
                tempFacultyName = lstFacultyName[i].Trim();
                schoolfaculty = new SchoolFaculty();
                schoolfaculty.FacultyName = tempFacultyName;
                schoolfaculty.Description = sfo.Description;
                int? SchoolID = globalInfo.SchoolID;
                schoolfaculty.SchoolID = SchoolID.Value;

                if (i != 0 && tempFacultyName.ToUpper().Equals(flagCheckFacultyName.ToUpper()))
                {
                    throw new BusinessException("Không thể thêm mới 2 tổ bộ môn trùng nhau");
                }
                else if (tempFacultyName != "")
                {
                    this.SchoolFacultyBusiness.Insert(schoolfaculty);
                }

            }

            this.SchoolFacultyBusiness.Save();

            SetDataView();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        //
        // GET: /SchoolFaculty/Edit/5
        public ActionResult GetEdit(int id)
        {
            CheckPermissionForAction(id, "SchoolFaculty");
            SchoolFaculty schoolfaculty = this.SchoolFacultyBusiness.Find(id);
            SchoolFacultyObject sfo = new SchoolFacultyObject();
            Utils.Utils.BindTo(schoolfaculty, sfo);
            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_EDIT] = ListEmployee();

            return View("Edit", sfo);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetInfoEdit(int id)
        {
            SchoolFaculty schoolfaculty = this.SchoolFacultyBusiness.Find(id);
            string facultyName = schoolfaculty.FacultyName;
            string description = schoolfaculty.Description;
            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_EDIT] = ListEmployee();

            string arrEmployee = string.Empty;
            if (!string.IsNullOrEmpty(schoolfaculty.HeadMasterID))
                arrEmployee = schoolfaculty.HeadMasterID;

            List<int> lstEmployeeId = new List<int>();
            if (!string.IsNullOrEmpty(arrEmployee))
            {
                arrEmployee = arrEmployee.Replace(" ", "");
                lstEmployeeId = arrEmployee.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            }

            List<EmployeeBO> lstDB = GetEmployee();
            EmployeeBO objDB = null;
            List<EmployeeBO> lstEmployeeBO = new List<EmployeeBO>();
            foreach (var item in lstEmployeeId)
            {
                objDB = lstDB.Where(x => x.EmployeeID == item).FirstOrDefault();
                if (objDB != null)
                {
                    lstEmployeeBO.Add(objDB);
                    objDB = null;
                }
            }

            return Json(new { facultyName = facultyName, description = description, lstEmployeeBO = lstEmployeeBO });
        }

        #region EditSchoolFaculty
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult EditSchoolFaculty(FormCollection frm)
        {
            string id = frm["SchoolFacultyID"];
            int schoolFacultyId = int.Parse(id);
            string arrEmployee = !string.IsNullOrEmpty(frm["arrEmployee_Edit"]) ? frm["arrEmployee_Edit"] : string.Empty;
            string facultyName = !string.IsNullOrEmpty(frm["FacultyName"]) ? frm["FacultyName"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["Description"]) ? frm["Description"] : string.Empty;

            if (!string.IsNullOrEmpty(facultyName) && schoolFacultyId > 0)
            {
                // danh sach ma code nhan vien
                List<string> lstEmployeeRequest = new List<string>();
                if (!string.IsNullOrEmpty(arrEmployee))
                {
                    lstEmployeeRequest = arrEmployee.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                }

                List<int> lstEmployeeAppled = new List<int>();
                List<EmployeeBO> lstEmployee = GetEmployee();
                foreach (var item in lstEmployeeRequest)
                {
                    foreach (var item2 in lstEmployee)
                    {
                        if (item == item2.EmployeeCode)
                        {
                            lstEmployeeAppled.Add(item2.EmployeeID);
                            break;
                        }
                    }
                }

                if (lstEmployeeAppled.Count >= 0)
                {
                    Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                    List<SchoolFaculty> lstDB = this.SchoolFacultyBusiness.Search(SearchInfo)
                        .Where(x => x.FacultyName.ToUpper() == facultyName.ToUpper() && x.SchoolFacultyID != schoolFacultyId)
                        .ToList();

                    if (lstDB.Count > 0)
                    {
                        string message = string.Format("Tổ bộ môn {0} đã tồn tại", facultyName);
                            return Json(new JsonMessage(message, "error"));
                        }
                    // danh sach can bo quan ly
                    string employeeAppled = string.Empty;
                    for (int i = 0; i < lstEmployeeAppled.Count(); i++)
                    {
                        if (i < lstEmployeeAppled.Count - 1)
                        {
                            employeeAppled += lstEmployeeAppled[i] + ", ";
                        }
                        else
                        {
                            employeeAppled += lstEmployeeAppled[i];
                        }
                    }

                    facultyName = facultyName.Trim();
                    string strPattern = @"[\s]+";
                    Regex rgx = new Regex(strPattern);
                    string Ouput = rgx.Replace(facultyName, " ");

                    SchoolFaculty objUpdate = new SchoolFaculty()
                    {
                        SchoolFacultyID = schoolFacultyId,
                        FacultyName = Ouput,
                        Description = description,
                        ModifiedDate = DateTime.Now,
                        HeadMasterID = employeeAppled
                    };

                    this.SchoolFacultyBusiness.UpdateSchoolFaculty(objUpdate, SearchInfo);
                    return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        //
        // POST: /SchoolFaculty/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        //public JsonResult Edit(int SchoolFacultyID)
        //{
        //    GlobalInfo GlobalInfo = new GlobalInfo();
        //    if (GetMenupermission("SchoolFaculty", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
        //    {
        //        throw new BusinessException("Validate_Permission_Teacher");
        //    }
        //    CheckPermissionForAction(SchoolFacultyID, "SchoolFaculty");
        //    SchoolFacultyObject sfo = new SchoolFacultyObject();
        //    TryUpdateModel(sfo);
        //    SchoolFaculty schoolfaculty = SchoolFacultyBusiness.Find(SchoolFacultyID);
        //    Utils.Utils.BindTo(sfo, schoolfaculty);
        //    GlobalInfo globalInfo = new GlobalInfo();

        //    int? SchoolID = globalInfo.SchoolID;
        //    schoolfaculty.SchoolID = SchoolID.Value;

        //    SMAS.Business.Common.Utils.ValidateRequire(schoolfaculty.FacultyName,
        //        globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
        //        || globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN
        //        ? "Lable_Faculty_MN" : "SchoolFaculty_Control_FacultyName");
        //    try
        //    {
        //        this.SchoolFacultyBusiness.Update(schoolfaculty);
        //        this.SchoolFacultyBusiness.Save();
        //    }
        //    catch (BusinessException ex)
        //    {
        //        // Thay the "To bo mon" thanh "To nhom" doi voi cap mam non
        //        if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
        //            || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
        //        {
        //            var message = ex.Params.FirstOrDefault(p => p == "SchoolFaculty_Control_FacultyName");
        //            if (message != null)
        //            {
        //                ex.Params.Remove(message);
        //                ex.Params.Add("Lable_Faculty_MN");
        //            }
        //        }
        //        throw (ex);
        //    }

        //    Paginate<SchoolFacultyObject> paging = this._Search("");
        //    ViewData[SchoolFacultyConstant.LIST_SCHOOLFACULTY] = paging;
        //    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        //}

        //
        // GET: /SchoolFaculty/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SchoolFaculty", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), "error"));
            }
            GlobalInfo global = new GlobalInfo();
            SchoolFaculty schoolFaculty = this.SchoolFacultyBusiness.Find(id);
            if (schoolFaculty.SchoolID != global.SchoolID.Value)
            {
                return Json(new JsonMessage(Res.Get("SchoolFaculty_Label_School_Err"), "error"));             
            }

            var lstEmployee = schoolFaculty.Employees.Where(e => e.IsActive).ToList();
            if (lstEmployee.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("Tồn tại giáo viên thuộc tổ bộ môn. Hãy xóa giáo viên trong tổ bộ môn trước."), "error"));         
            }
            CheckPermissionForAction(id, "SchoolFaculty");          
            this.SchoolFacultyBusiness.Delete(id, global.SchoolID.Value);
            this.SchoolFacultyBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));         
        }

        /// <summary>
        /// search
        /// </summary>

        public PartialViewResult Search(string SchoolFacultyName, int page = 1, string orderBy = "")
        {
            Paginate<SchoolFacultyObject> paging = this._Search(SchoolFacultyName, page, orderBy);
            ViewData[SchoolFacultyConstant.LIST_SCHOOLFACULTY] = paging;

            return PartialView("_GridSchoolFaculty");
        }

        private Paginate<SchoolFacultyObject> _Search(string SchoolFacultyName, int page = 1, string orderBy = "")
        {
            SetViewDataPermission("SchoolFaculty", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            // Dieu kien truy van du lieu
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FacultyName"] = SchoolFacultyName;

            //SearchInfo["SchoolID"] = Session["SchoolID"];
            GlobalInfo globalInfo = new GlobalInfo();

            int? SchoolID = globalInfo.SchoolID;
            SearchInfo["SchoolID"] = SchoolID.Value;
            IQueryable<SchoolFaculty> lst = this.SchoolFacultyBusiness.Search(SearchInfo);

            if (lst != null)
            {
                // Sap xep
                string sort = "";
                string order = "";
                if (orderBy != "")
                {
                    sort = orderBy.Split('-')[0];
                    order = orderBy.Split('-')[1];
                }

                if (sort == "FacultyName" || sort == "")
                {
                    if (order == "desc")
                    {
                        lst = lst.OrderByDescending(o => o.FacultyName);
                    }
                    else
                    {
                        lst = lst.OrderBy(o => o.FacultyName);
                    }
                }

                else if (sort == "Description")
                {
                    if (order == "desc")
                    {
                        lst = lst.OrderByDescending(o => o.Description);
                    }
                    else
                    {
                        lst = lst.OrderBy(o => o.Description);
                    }
                }
            }

            List<EmployeeBO> lstEmployee = GetEmployee();

            List<SchoolFaculty> lstData = lst.ToList();
            List<SchoolFacultyViewModel> lstSearch = new List<SchoolFacultyViewModel>();
            SchoolFacultyViewModel objSearch = null;
            string employeeAplied = string.Empty;

            lstSearch = lst.Where(x => x.SchoolID == _globalInfo.SchoolID.Value)
                  .Select((x) => new SchoolFacultyViewModel
                  {
                      SchoolFacultyID = x.SchoolFacultyID,
                      HeadMasterID = x.HeadMasterID,
                      FacultyName = x.FacultyName,
                      Description = x.Description
                  }).ToList();

            List<SchoolFacultyViewModel> lstResult = new List<SchoolFacultyViewModel>();
            foreach (var item in lstSearch)
            {
                foreach (var item2 in lstEmployee)
                {
                    if (!string.IsNullOrEmpty(item.HeadMasterID))
                    {
                        string employeeId = item2.EmployeeID.ToString();
                        if (item.HeadMasterID.Contains(employeeId))
                        {
                            employeeAplied += item2.FullName + ", ";
                        }
                    }

                }

                if (!string.IsNullOrEmpty(employeeAplied))
                {
                    employeeAplied = employeeAplied.Substring(0, employeeAplied.Length - 2);
                }

                objSearch = new SchoolFacultyViewModel()
                {
                    FacultyName = item.FacultyName,
                    Description = item.Description,
                    SchoolFacultyID = item.SchoolFacultyID,
                    EmployeeApplied = employeeAplied,
                    CreatedDate = item.CreatedDate
                };
                lstResult.Add(objSearch);
                employeeAplied = string.Empty;
            }

            var queryableList = lstResult.AsQueryable();

            // Convert du lieu sang doi tuong
            IQueryable<SchoolFacultyObject> res = queryableList.Select(o => new SchoolFacultyObject
            {
                FacultyName = o.FacultyName,
                Description = o.Description,
                SchoolFacultyID = o.SchoolFacultyID,
                CreateDateTime = o.CreatedDate,
                EmployeeAplied = o.EmployeeApplied
            });

            // Thuc hien phan trang tung phan
            Paginate<SchoolFacultyObject> Paging = new Paginate<SchoolFacultyObject>(res);
            Paging.page = page;
            Paging.paginate();
            //Paging.size = SchoolFacultyConstant.PAGE_SIZE;
            Paging.total = lst.Count();
            return Paging;
        }
        #endregion old fuc

        #region grid binding
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(string SchoolFacultyName, int page = 1, string orderBy = "")
        {
            Paginate<SchoolFacultyObject> paging = this._Search(SchoolFacultyName, page, orderBy);
            GridModel<SchoolFacultyObject> gm = new GridModel<SchoolFacultyObject>(paging.List);
            gm.Total = paging.total;
            return View(gm);
        }
        #endregion grid binding

        #region private func
        private void SetDataView()
        {
            //SchoolID, AcademicYearID lấy từ Session
            GlobalInfo global = new GlobalInfo();

            Paginate<SchoolFacultyObject> paging = this._Search("");
            ViewData[SchoolFacultyConstant.LIST_SCHOOLFACULTY] = paging;

            ViewData[SchoolFacultyConstant.LIST_EMPLOYEE_COMBO_CREATE] = ListEmployee();
        }
        #endregion private func
    }
}
