﻿namespace SMAS.Web.Areas.SchoolFacultyArea
{
    public class SchoolFacultyConstant
    {
        public const string LIST_SCHOOLFACULTY = "ListSchoolFaculty";
        public const int PAGE_SIZE = 20;
        public const string TOTAL = "total";
        public const string PAGE_NUMBER = "position of page";
        public const string LIST_RESULT = "listResult";
        public const string FORBACK = "FORBACK";
        public const string LIST_EMPLOYEE_COMBO_CREATE = "LIST_EMPLOYEE_COMBO_CREATE";
        public const string LIST_EMPLOYEE_COMBO_EDIT = "LIST_EMPLOYEE_COMBO_EDIT";
    }
}