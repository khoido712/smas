﻿namespace SMAS.Web.Areas.HealthTestArea
{
    public class HealthTestConstants
    {
        public const string COMMON_VIEW_MODEL = "common_view_model";
        public const string LIST_DENTALTEST = "list_DentalTest";
        public const string LIST_OVERALLTEST = "list_overalltest";
        public const string LIST_ENTTESTVIEWMODEL = "list_enttestviewmodel";
        public const string LIST_PHYSICALTEST = "LIST_PHYSICALTEST";
        public const string LIST_SPINETEST = "LIST_SPINETEST";
        public const string LIST_EYETEST = "LIST_EYETEST";
        public const string LISTHEALTHPERIOD = "LISTHEALTHPERIOD";
        public const string LIS_PHYSICALCLASSIFICATION = "LIST_PHYSICALCLASSIFICATION";
        public const string LIST_EVALUATION_HEALTH = "list_evaluation_health";
        public const string LIST_HEALTH_STATE = "list_health_state";
        public const string LIST_MONITORINGBOOK = "LIST_MONITORINGBOOK";
        public const string MONITORINGDATE = "MonitoringDate";
        public const string MONITORINGBOOKID = "MONITORINGBOOKID";
        public const string ENABLE_MONITORINGDATE = "Enable_MonitoringDate";
        public const string UNITNAME = "UnitName";
        public const string ENABLE_UNITNAME = "Enable_UnitName";
        public const string DENTALTESTID = "DentalTestID";
        public const string PUPILID = "PupilID";
        public const string CLASSID = "ClassID";
        public const string HEALTHPERIODACTIVE = "HealthPeriodActive";
        public const string EYETEST = "EYETEST";
        //public const string Min = "Min";
        //public const string Max = "Max";
        public const string TABDEFAULT = "tabdefault";
        public const string GridPhysicalPage = "GridPhysicalPage";
        public const string APPLYLEVEL = "applylevel";
        public const string TYPE = "type";
        public const string TOTAL_PUPIL = "TOTAL_PUPIL";
        public const string PUPIL = "PUPIL";
        public const string LIST_PUPIL = "LIST_PUPIL";
        public const string PUPILIDPREVIOUS = "PUPILIDPREVIOUS";
        public const string PUPILIDNEXT = "PUPILIDNEXT";


        // Growth chart
        public const string LIST_CLASS = "lst class by permission";
        public const string LIST_PUPIL_CHART = "lst pupil of class";
        public const string LIST_CHART = "lst monitoring book";
        public const string LIST_CHART_CLASS = "lst monitoring book of class";
        public const string CLASSID_SELECT = "classID select";
        public const string FULL_NAME = "nam of pupil";
        public const string BIRTH_DAY = "birthday of pupil";
        public const string GENRE = "genre of pupil";
        public const string CLASS_NAME = "class of pupil";
        public const string CLASS_ID = "classid of pupil";
        public const string HISTORY_YOUR_SELF = "history yourseft";
        public const string PUPIL_ID = "id of pupil";
        public const string LIST_MONTH = "lst 12 month";
        public const string LIST_EDUCATION_LEVEL = "List of levelid";
        public const string FIRST_LEVEL_SELECTED = "First of level";

        // So ngay cho phep sua xoa
        public const int NUMBER_OF_DAY_ALLOW_EDIT = 2;
    }
}