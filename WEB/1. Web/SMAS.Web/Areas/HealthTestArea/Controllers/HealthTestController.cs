﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Ionic.Zip;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.DrawingChart;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.HealthTestArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Controllers;
using System.Globalization;
using SMAS.VTUtils.Utils;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.HealthTestArea.Controllers
{
    [SkipCheckRole]
    public class HealthTestController : BaseController
    {
        private IAcademicYearBusiness AcademicYearBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IMonitoringBookBusiness MonitoringBookBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IENTTestBusiness ENTTestBusiness;
        private IPhysicalTestBusiness PhysicalTestBusiness;
        private IDentalTestBusiness DentalTestBusiness;
        private IHealthPeriodBusiness HealthPeriodBusiness;
        private IPupilOfClassBusiness PupilOfClassBusiness;
        private ISpineTestBusiness SpineTestBusiness;
        private IEyeTestBusiness EyeTestBusiness;
        private IOverallTestBusiness OverallTestBusiness;
        private IClassificationLevelHealthBusiness ClassificationLevelHealthBusiness;
        private IClassificationCriteriaBusiness ClassificationCriteriaBusiness;
        private IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness;
        private IProvinceBusiness ProvinceBusiness;

        //public static string fileNameZip = "";
        public HealthTestController(
            IAcademicYearBusiness academicYearBusiness,
            IPupilProfileBusiness pupilProfileBusiness, 
            IMonitoringBookBusiness monitoringBookBusiness, 
            IClassProfileBusiness classProfileBusiness, 
            IENTTestBusiness ENTTestBusiness,
            IDentalTestBusiness dentalTestBusiness, 
            IPhysicalTestBusiness physicalTestBusiness,
            IHealthPeriodBusiness healthPeriodBusiness, 
            IPupilOfClassBusiness pupilOfClassBusiness, 
            ISpineTestBusiness SpineTestBusiness,
            IEyeTestBusiness eyeTestBusiness,
            IOverallTestBusiness overallTestBusiness,
            IClassificationLevelHealthBusiness classificationLevelHealthBusiness,
            IClassificationCriteriaBusiness classificationCriteriaBusiness,
            IClassificationCriteriaDetailBusiness classificationCriteriaDetailBusiness,
            IProvinceBusiness ProvinceBusiness)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
            this.AcademicYearBusiness = academicYearBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ENTTestBusiness = ENTTestBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.DentalTestBusiness = dentalTestBusiness;
            this.HealthPeriodBusiness = healthPeriodBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.SpineTestBusiness = SpineTestBusiness;
            this.EyeTestBusiness = eyeTestBusiness;
            this.OverallTestBusiness = overallTestBusiness;
            this.ClassificationLevelHealthBusiness = classificationLevelHealthBusiness;
            this.ClassificationCriteriaBusiness = classificationCriteriaBusiness;
            this.ClassificationCriteriaDetailBusiness = classificationCriteriaDetailBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
        }

        public ActionResult Index(int id, int classID, int? tab, int? GridPhysical_page)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(id);
            int page;
            if (!GridPhysical_page.HasValue && int.TryParse(Request.QueryString["GridPhysical-page"], out page))
            {
                GridPhysical_page = page;
            };
            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            if (!tab.HasValue)
                tab = 0;
            if (pupil != null)
            {
                string historyYourSelf = pupil.MonitoringBooks.SelectMany(u => u.OverallTests).OrderByDescending(u => u.ModifiedDate).Select(u => u.HistoryYourSelf).FirstOrDefault();

                ViewData[HealthTestConstants.COMMON_VIEW_MODEL] = new CommonViewModel
                {
                    PupilID = pupil.PupilProfileID,
                    BirthDate = pupil.BirthDate,
                    ClassID = classProfile.ClassProfileID,
                    ClassName = classProfile.DisplayName,
                    FullName = pupil.FullName,
                    Genre = pupil.Genre,
                    HistoryYourSelf = historyYourSelf
                };
            }
            ViewData[HealthTestConstants.TABDEFAULT] = tab;
            ViewData[HealthTestConstants.PUPILID] = id;
            ViewData[HealthTestConstants.CLASSID] = classID;
            ViewData[HealthTestConstants.GridPhysicalPage] = GridPhysical_page;
            GetViewData();
            GetPermission();
            return View();
        }

        public ActionResult LoadPhysicalTest(int id, int classID, int? GridPhysicalPage)
        {
            Session["PupilID"] = id;
            Session["ClassID"] = classID;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = id;
            SearchInfo["ClassID"] = classID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            int a = new GlobalInfo().AppliedLevel.Value;
            IQueryable<PhysicalTestBO> query = this.PhysicalTestBusiness.SearchByPupil(new GlobalInfo().AcademicYearID.Value, new GlobalInfo().SchoolID.Value, id, classID);
            IQueryable<PhysicalTestViewModel> lst = query
                .Select(o => new PhysicalTestViewModel
                {
                    PhysicalTestID = o.PhysicalTestID ?? 0,
                    MonitoringBookID = o.MonitoringBookID,
                    MonitoringDate = o.Date.HasValue ? o.Date.Value : DateTime.Now,
                    Height = o.Height ?? 0m,
                    Weight = o.Weight ?? 0m,
                    Breast = o.Breast ?? 0m,
                    PhysicalClassification = o.PhysicalClassification ?? 0,
                    Nutrition = o.Nutrition ?? 0,
                    CreateDate = o.CreateDate
                });
            List<PhysicalTestViewModel> lstPhysical = lst.OrderByDescending(o => o.CreateDate).ToList();
            foreach (var item in lstPhysical)
            {
                if (item.PhysicalClassification != 0)
                {
                    if (item.PhysicalClassification.HasValue)
                        item.PhysicalClassificationName = CommonConvert.PhysicalClassification(item.PhysicalClassification.Value);
                }
                if (item.Nutrition != 0)
                {
                    item.NutritionName = CommonConvert.Nutrition(item.Nutrition);
                }
                if (item.CreateDate.HasValue)
                {
                    DateTime CreateDate = item.CreateDate.Value.AddDays(2);
                    if (CreateDate.Date >= DateTime.Now.Date)
                    {
                        item.DisplayLink = 1;
                    }
                    else
                        item.DisplayLink = 0;
                }
                else
                {
                    item.DisplayLink = 0;
                }
            }
            ViewData[HealthTestConstants.LIST_PHYSICALTEST] = lstPhysical;
            PupilProfile pupil = PupilProfileBusiness.Find(id);
            // Tim cac gia tri muc phan loai
            //bool genre = pupil.Genre == 1;
            //decimal min, max;
            ////int month = (int)Math.Ceiling((double)(DateTime.Now - pupil.BirthDate).TotalDays / 30d);
            //int month = (DefaultMonitoringDate.Year - selectedPupilProfile.BirthDate.Year) * 12 + DefaultMonitoringDate.Month - selectedPupilProfile.BirthDate.Month + (DefaultMonitoringDate.Day >= selectedPupilProfile.BirthDate.Day ? 0 : -1);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = min;
            //ViewData[HealthTestConstants.Max] = max;
            ViewData[HealthTestConstants.APPLYLEVEL] = new GlobalInfo().AppliedLevel.Value;
            ViewData[HealthTestConstants.PUPILID] = id;
            ViewData[HealthTestConstants.CLASSID] = classID;
            ViewData[HealthTestConstants.GridPhysicalPage] = GridPhysicalPage;
            GetPermission();
            return PartialView("_gridPhysicalTest");
        }


        [ValidateAntiForgeryToken]
        public ActionResult LoadEyeTest(int id, int classID)
        {
            Session["PupilID"] = id;
            Session["ClassID"] = classID;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = id;
            SearchInfo["ClassID"] = classID;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            IQueryable<EyeTestBO> query = this.EyeTestBusiness.SearchByPupil(new GlobalInfo().AcademicYearID.Value, new GlobalInfo().SchoolID.Value, id, classID);
            IQueryable<EyeTestViewModel> lst = query
                .Select(o => new EyeTestViewModel
                {
                    EyeTestID = o.EyeTestID,
                    MonitoringBookID = o.MonitoringBookID,
                    MonitoringDate = o.Date,
                    REye = o.REye.Value,
                    LEye = o.LEye,
                    RMyopic = o.RMyopic,
                    LMyopic = o.LMyopic,
                    RFarsightedness = o.RFarsightedness,
                    LFarsightedness = o.LFarsightedness,
                    RAstigmatism = o.RAstigmatism,
                    LAstigmatism = o.LAstigmatism,
                    Other = o.Other,
                    IsTreatment = (o.IsTreatment.HasValue ? o.IsTreatment.Value : false),
                    IsMyopic = (o.IsMyopic.HasValue ? o.IsMyopic.Value : false),
                    IsFarsightedness = (o.IsFarsightedness.HasValue ? o.IsFarsightedness.Value : false),
                    IsAstigmatism = (o.IsAstigmatism.HasValue ? o.IsAstigmatism.Value : false),
                    CreateDate = o.CreateDate
                });
            List<EyeTestViewModel> lstEyeTest = lst.OrderByDescending(o => o.CreateDate).ToList();
            foreach (var item in lstEyeTest)
            {
                if (item.CreateDate.HasValue)
                {
                    DateTime CreateDate = item.CreateDate.Value.AddDays(2);
                    if (CreateDate.Date >= DateTime.Now.Date)
                    {
                        item.DisplayLink = 1;
                    }
                    else
                        item.DisplayLink = 0;
                }
                else
                {
                    item.DisplayLink = 0;
                }
            }
            ViewBag.PupilID = id;
            ViewData[HealthTestConstants.LIST_EYETEST] = lstEyeTest;
            GetPermission();
            return PartialView("_gridEyeTest");
        }


        [ValidateAntiForgeryToken]
        public ActionResult LoadENTTest(int id, int classID)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(id);
            IQueryable<ENTTestBO> lstENTTest = ENTTestBusiness.SearchByPupil(new GlobalInfo().AcademicYearID.Value, new GlobalInfo().SchoolID.Value, id, classID);
            lstENTTest = lstENTTest.OrderByDescending(p => p.CreatedDate);
            ViewData[HealthTestConstants.LIST_ENTTESTVIEWMODEL] = lstENTTest;
            ViewBag.PupilID = id;
            GetPermission();
            return PartialView("_gridENTTest");
        }


        [ValidateAntiForgeryToken]
        public ActionResult LoadSpineTest(int id, int classID)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(id);
            IQueryable<SpineTestBO> lstSpineTest = SpineTestBusiness.SearchByPupil(new GlobalInfo().AcademicYearID.Value, new GlobalInfo().SchoolID.Value, id, classID);
            lstSpineTest = lstSpineTest.OrderByDescending(p => p.CreatedDate);
            ViewData[HealthTestConstants.LIST_SPINETEST] = lstSpineTest;
            ViewBag.PupilID = id;
            GetPermission();
            return PartialView("_gridSpineTest");
        }


        [ValidateAntiForgeryToken]
        public ActionResult LoadDentalTest(int id, int classID)
        {
            GlobalInfo global = new GlobalInfo();
            /*
             Hệ thống thực hiện việc load ra grid bằng cách gọi hàm: DentalTestBusiness.SearchByPupil(AcademicYearID, SchoolID, PupilID, ClassID)
             Hiển thị link xóa, sửa: Khi DateTime.Now – lstDentalTest[i].CreatDate <= 2
             */
            IQueryable<DentalTestBO> dentalTests = DentalTestBusiness.SearchByPupil(global.AcademicYearID.Value, global.SchoolID.Value, id, classID);
            List<DentalTestBO> lstDentalTestBO = new List<DentalTestBO>();
            if (dentalTests != null)
            {
                foreach (var item in dentalTests)
                {
                    DentalTestBO dentalTestBO = new DentalTestBO();
                    dentalTestBO = item;
                    if (item.CreatedDate.HasValue)
                    {
                        TimeSpan span = DateTime.Now.Subtract(item.CreatedDate.Value);
                        if (span.Days <= 2)
                            dentalTestBO.EnableAction = true;
                        else
                        {
                            dentalTestBO.EnableAction = false;
                        }
                    }
                    else
                    {
                        dentalTestBO.EnableAction = false;
                    }
                    lstDentalTestBO.Add(dentalTestBO);
                }
            }
            lstDentalTestBO = lstDentalTestBO.OrderByDescending(p => p.CreatedDate).ToList();
            ViewData[HealthTestConstants.LIST_DENTALTEST] = lstDentalTestBO;
            ViewBag.PupilID = id;
            GetPermission();
            return PartialView("_gridDentalTest");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult GetPupil(int ClassID, int PupilID, bool? IsFirst)
        {
            List<PupilViewModel> lstPupil;

            // if is first - get list pupil
            // else using session get list from session

            if (IsFirst.HasValue && IsFirst.Value)
            {
                lstPupil = GetListPupil(ClassID);
            }
            else
            {
                lstPupil = (List<PupilViewModel>)Session[HealthTestConstants.LIST_PUPIL];
            }

            int SumOfPupil = lstPupil.Count;

            ViewData[HealthTestConstants.LIST_PUPIL] = lstPupil;
            ViewData[HealthTestConstants.TOTAL_PUPIL] = SumOfPupil;
            PupilViewModel pupil = new PupilViewModel();

            pupil = lstPupil.Where(x => x.PupilID == PupilID).FirstOrDefault();
            ViewData[HealthTestConstants.PUPIL] = pupil;

            int order = pupil.STT;
            int orderPrevious = order - 1;
            int orderNext = order + 1;
            int pupilIDPrevious = 0;
            int pupilIDNext = 0;
            if (orderPrevious > 0)
            {
                pupilIDPrevious = lstPupil.Where(x => x.STT == orderPrevious).FirstOrDefault().PupilID;
            }

            if (orderNext <= SumOfPupil)
            {
                pupilIDNext = lstPupil.Where(x => x.STT == orderNext).FirstOrDefault().PupilID;
            }
            ViewData[HealthTestConstants.PUPILIDPREVIOUS] = pupilIDPrevious;
            ViewData[HealthTestConstants.PUPILIDNEXT] = pupilIDNext;
            return PartialView("_SlidePupil");
        }

        public List<PupilViewModel> GetListPupil(int? ClassID)
        {
            GlobalInfo global = new GlobalInfo();
            //Danh sach hoc sinh tra ve
            List<PupilViewModel> lstPupilRet = new List<PupilViewModel>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                int schoolID = global.SchoolID.HasValue ? global.SchoolID.Value : 0;
                ClassProfile obj = ClassProfileBusiness.Find(ClassID);
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["CurrentClassID"] = ClassID;
                SearchInfo["CurrentSchoolID"] = global.SchoolID;
                SearchInfo["CurrentAcademicYearID"] = global.AcademicYearID;

                // lay ve danh sach hoc sinh theo cap, lop, truong, nam
                IQueryable<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(schoolID, SearchInfo);
                lstPupil = lstPupil.OrderBy(x => x.FullName);
                // end 
                DateTime dateTimeNow = DateTime.Now;
                if (lstPupil != null && lstPupil.Count() > 0)
                {
                    List<PupilProfileBO> lstPPBO = lstPupil.ToList();
                    int Count = lstPPBO.Count;
                    int index = 1;
                    PupilProfileBO ppBO;
                    for (int i = 0; i < Count; i++)
                    {
                        ppBO = lstPPBO[i];
                        lstPupilRet.Add(new PupilViewModel()
                        {
                            STT = index++,
                            PupilID = ppBO.PupilProfileID,
                            ClassID = ClassID.Value,
                            PupilName = ppBO.FullName,
                            Status = ppBO.ProfileStatus,
                            ImageData = ppBO.Image,
                            Genre = ppBO.Genre,
                            Month = ((dateTimeNow.Year - ppBO.BirthDate.Year) * 12) + dateTimeNow.Month - ppBO.BirthDate.Month
                        });
                    }
                }
                Session[HealthTestConstants.LIST_PUPIL] = lstPupilRet;
            }
            return lstPupilRet;
        }

        
        public ActionResult DetailHealthTest(int monitoringBookID, int? type)
        {
            MonitoringBook monitoringBook = MonitoringBookBusiness.Find(monitoringBookID);
            ViewData[HealthTestConstants.TYPE] = type;
            HealthTestDetailModel model = null;
            if (monitoringBook != null)
            {
                model = new HealthTestDetailModel();
                model.PupilID = monitoringBook.PupilID;
                model.ClassID = monitoringBook.ClassID;
                model.BirthDate = monitoringBook.PupilProfile.BirthDate;
                model.FullName = monitoringBook.PupilProfile.FullName;
                model.ClassName = monitoringBook.ClassProfile.DisplayName;
                model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(monitoringBook.PupilProfile.Genre.ToString())).FirstOrDefault().value;
                //model.MonitoringBookID = monitoringBook.MonitoringBookID;
                model.MonitoringBook = monitoringBook;
                model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
                model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
            }
            return View(model);
        }

        
        public ActionResult PrintDetailHealthTest(int monitoringBookID)
        {
            MonitoringBook monitoringBook = MonitoringBookBusiness.Find(monitoringBookID);
            HealthTestDetailModel model = null;
            string schoolName = new GlobalInfo().SchoolName;
            if (schoolName.Contains("Trường"))
            {
                schoolName = schoolName.Replace("Trường", "");
            }

            if (monitoringBook != null)
            {
                model = new HealthTestDetailModel();
                model.PupilID = monitoringBook.PupilID;
                model.ClassID = monitoringBook.ClassID;
                model.BirthDate = monitoringBook.PupilProfile.BirthDate;
                model.FullName = monitoringBook.PupilProfile.FullName;
                model.SchoolName = schoolName;
                model.Address = monitoringBook.PupilProfile.PermanentResidentalAddress;
                model.ClassName = monitoringBook.ClassProfile.DisplayName;
                model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(monitoringBook.PupilProfile.Genre.ToString())).FirstOrDefault().value;
                //model.MonitoringBookID = monitoringBook.MonitoringBookID;
                model.MonitoringBook = monitoringBook;
                model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
                model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
                model.ProvinceName = ProvinceBusiness.Find(_globalInfo.ProvinceID.Value).ProvinceName;
            }
            return View(model);
        }


        public ActionResult EditHealthTest(int type, int monitoringBookID, bool? isFirst)
        {
            MonitoringBook monitoringBook = MonitoringBookBusiness.Find(monitoringBookID);
            ViewData[HealthTestConstants.TYPE] = type;
            ViewData[HealthTestConstants.PUPILID] = monitoringBook.PupilID;
            ViewData[HealthTestConstants.CLASSID] = monitoringBook.ClassID;
            ViewData[HealthTestConstants.MONITORINGDATE] = monitoringBook.MonitoringDate;
            ViewData[HealthTestConstants.MONITORINGBOOKID] = monitoringBookID;
            ViewData[HealthTestConstants.UNITNAME] = monitoringBook.UnitName;
            List<HealthPeriod> ListHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[HealthTestConstants.LISTHEALTHPERIOD] = new SelectList(ListHealthPeriod, "HealthPeriodID", "Resolution", monitoringBook.HealthPeriodID);
            if (isFirst == false)
            {
                GetPupil(monitoringBook.ClassID, monitoringBook.PupilID, false);
            }
            else
            {
                GetPupil(monitoringBook.ClassID, monitoringBook.PupilID, true);
            }
            PupilProfile pupil = PupilProfileBusiness.Find(monitoringBook.PupilID);
            
            // Tim gia tri min, max cua muc phan loai
            //bool genre = pupil.Genre == 1;
            //decimal min, max;
            //int month = this.caculateNumberOfMonthBetweenTowDate(pupil.BirthDate, monitoringBook.MonitoringDate);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = 0;
            //ViewData[HealthTestConstants.Max] = 0;

            HealthTestDetailModel model = null;
            if (monitoringBook != null)
            {
                model = new HealthTestDetailModel();
                model.PupilID = monitoringBook.PupilID;
                model.ClassID = monitoringBook.ClassID;
                model.BirthDate = monitoringBook.PupilProfile.BirthDate;
                model.FullName = monitoringBook.PupilProfile.FullName;
                model.ClassName = monitoringBook.ClassProfile.DisplayName;
                model.GenreName = CommonList.GenreAndSelect().Where(u => u.key.Equals(monitoringBook.PupilProfile.Genre.ToString())).FirstOrDefault().value;
                model.DentalTest = monitoringBook.DentalTests.FirstOrDefault();
                model.ENTTest = monitoringBook.ENTTests.FirstOrDefault();
                model.EyeTest = monitoringBook.EyeTests.FirstOrDefault();
                model.OverallTest = monitoringBook.OverallTests.FirstOrDefault();
                model.PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
                model.SpineTest = monitoringBook.SpineTests.FirstOrDefault();
            }


            PhysicalTestViewModel physical = new PhysicalTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.PhysicalTest, physical);
            }

            ViewData[HealthTestConstants.LIS_PHYSICALCLASSIFICATION] = new SelectList(CommonList.PhysicalClassification(), "key", "value", physical.PhysicalClassification);

            EyeTestViewModel eye = new EyeTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.EyeTest, eye);
            }

            ENTTestViewModel ent = new ENTTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.ENTTest, ent);
            }

            SpineTestViewModel spine = new SpineTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.SpineTest, spine);
            }

            DentalTestViewModel dental = new DentalTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.DentalTest, dental);
            }

            OverallTestViewModel overall = new OverallTestViewModel();
            if (model.PhysicalTest != null)
            {
                Utils.Utils.BindTo(model.OverallTest, overall);
            }
            overall.DescriptionOverallForeign = model.OverallTest.DescriptionForeign;
            overall.DescriptionOverallInternal = model.OverallTest.DescriptionOverallInternall;
            ViewData[HealthTestConstants.LIST_EVALUATION_HEALTH] = new SelectList(CommonList.EvaluationHealth(), "key", "value", overall.EvaluationHealth);

            var customView = new CustomView
            {
                physicalTest = physical,
                eyeTest = eye,
                eNTTest = ent,
                spineTest = spine,
                dentalTest = dental,
                overallTest = overall,
                overallTestDetail = model
            };
            return View(customView);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadHealthPeriod(int pupilID)
        {
            GlobalInfo glo = new GlobalInfo();
            List<HealthPeriod> listHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();
            List<MonitoringBook> listMonitoringBook = MonitoringBookBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "PupilID", pupilID }, { "AcademicYearID", glo.AcademicYearID.Value }, { "MonitoringType", SystemParamsInFile.TYPE_PERIOD } }).ToList();

            List<SelectListItem> listSelectPriod = new List<SelectListItem>();

            if (listMonitoringBook.Count == 0)
            {
                return Json(listHealthPeriod.Select(u => new SelectListItem { Text = u.Resolution, Value = u.HealthPeriodID.ToString(), Selected = false }).ToList());
            }
            else
            {
                return Json(listHealthPeriod.Select(u => new SelectListItem { Text = u.Resolution, Value = u.HealthPeriodID.ToString(), Selected = u.HealthPeriodID == SystemParamsInFile.HEALTH_PERIOD_TWO }).ToList());
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadDateVsUnit(int pupilID, int classID, int healthPeriodID)
        {
            GlobalInfo glo = new GlobalInfo();
            List<MonitoringBook> lstMonitoringBookA = MonitoringBookBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "PupilID", pupilID }, { "AcademicYearID", glo.AcademicYearID.Value }, { "MonitoringType", SystemParamsInFile.TYPE_PERIOD }, { "HealthPeriodID", healthPeriodID } }).ToList();
            List<MonitoringBook> lstMonitoringBookB = MonitoringBookBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ClassID", classID }, { "AcademicYearID", glo.AcademicYearID.Value }, { "MonitoringType", SystemParamsInFile.TYPE_PERIOD }, { "HealthPeriodID", healthPeriodID } }).ToList();
            List<MonitoringBook> lstMonitoringBookC = MonitoringBookBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", glo.AcademicYearID.Value }, { "MonitoringType", SystemParamsInFile.TYPE_PERIOD }, { "HealthPeriodID", healthPeriodID } }).ToList();

            MonitoringBook mbA = lstMonitoringBookA.FirstOrDefault();
            MonitoringBook mbB = lstMonitoringBookB.Where(u => u.MonitoringDate == lstMonitoringBookB.Max(v => v.MonitoringDate)).FirstOrDefault();
            MonitoringBook mbC = lstMonitoringBookC.Where(u => u.MonitoringDate == lstMonitoringBookC.Max(v => v.MonitoringDate)).FirstOrDefault();

            OverallTest overalltestTemp = null;
            object Doctor = null;

            //Lấy thông tin bác sĩ theo học sinh hoặc theo lớp hoặc theo trường
            if (mbA != null && mbA.OverallTests != null)
            {
                overalltestTemp = mbA.OverallTests.Where(p => p.IsActive == true).OrderByDescending(o => o.OverallTestID).FirstOrDefault();
            }
            if (overalltestTemp != null)
            {
                Doctor = overalltestTemp.Doctor;
            }
            else
            {
                if (mbB != null && mbB.OverallTests != null)
                {
                    overalltestTemp = mbB.OverallTests.Where(p => p.IsActive == true).OrderByDescending(o => o.OverallTestID).FirstOrDefault();
                }
                if (overalltestTemp != null)
                {
                    Doctor = overalltestTemp.Doctor;
                }
                else
                {
                    if (mbC != null && mbC.OverallTests != null)
                    {
                        overalltestTemp = mbC.OverallTests.Where(p => p.IsActive == true).OrderByDescending(o => o.OverallTestID).FirstOrDefault();
                    }
                    if (overalltestTemp != null)
                    {
                        Doctor = overalltestTemp.Doctor;
                    }
                }
            }


            if (mbA != null)
            {
                return Json(new { Existed = true,
                                  MonitoringDate = mbA.MonitoringDate.HasValue ? mbA.MonitoringDate.Value.ToString("dd/MM/yyyy") : DateTime.Now.ToString("dd/MM/yyyy"), 
                                    UnitName = mbA.UnitName, 
                                    Doctor = (Doctor == null ? "" : Doctor) });
            }
            else
            {
                if (mbB != null)
                {

                    return Json(new { Existed = false,
                                      MonitoringDate = mbB.MonitoringDate.HasValue ? mbB.MonitoringDate.Value.ToString("dd/MM/yyyy") : DateTime.Now.ToString("dd/MM/yyyy"), 
                                      UnitName = mbB.UnitName, Doctor = (Doctor == null ? "" : Doctor) });
                }
                else
                    if (mbC != null)
                    {
                        return Json(new { Existed = false,
                                          MonitoringDate = mbC.MonitoringDate.HasValue ? mbC.MonitoringDate.Value.ToString("dd/MM/yyyy") : DateTime.Now.ToString("dd/MM/yyyy"),
                                          UnitName = mbC.UnitName,
                                          Doctor = (Doctor == null ? "" : Doctor)
                        });
                    }
                    else
                    {
                        return Json(new { Existed = false, MonitoringDate = DateTime.Now.ToString("dd/MM/yyyy"), UnitName = "", Doctor = "" });
                    }
            }


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetLastestHistoryYourself(int pupilID)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(pupilID);
            if (pupil != null)
            {
                OverallTest ot = pupil.MonitoringBooks.SelectMany(u => u.OverallTests).Where(u => u.IsActive == true).OrderByDescending(u => u.CreatedDate).FirstOrDefault();
                if (ot != null)
                {
                    return Json(ot.HistoryYourSelf);
                }
            }
            return Json("");
        }

        [HttpGet]
        public ActionResult CreateHealthTest(int id, int classID, int? type, bool? isFirst)
        {
            ViewData[HealthTestConstants.TYPE] = type;
            ViewData[HealthTestConstants.PUPILID] = id;
            ViewData[HealthTestConstants.CLASSID] = classID;

            if (isFirst == false)
            {
                GetPupil(classID, id, false);
            }
            else
            {
                GetPupil(classID, id, true);
            }
            PupilProfile pupil = PupilProfileBusiness.Find(id);


            

            bool isActive = false;
            DateTime? maxDateClass = null;
            DateTime? maxDateSchool = null;
            int schoolID = new GlobalInfo().SchoolID.Value;
            string unitName = "";
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            dictionary["PupilID"] = id;
            dictionary["ClassID"] = classID;
            dictionary["SchoolID"] = schoolID;
            MonitoringBookBusiness.GetCreateInfo(dictionary, ref isActive, ref maxDateClass, ref maxDateSchool, ref unitName);
            DateTime monitoringDate = DateTime.Now;
            if (maxDateClass != null)
            {
                monitoringDate = Convert.ToDateTime(maxDateClass);
            }
            else if (maxDateSchool != null && maxDateClass == null)
            {
                monitoringDate = Convert.ToDateTime(maxDateSchool);
            }

            // Tim gia tri min, max cua muc phan loai
            //bool genre = pupil.Genre == 1;
            //decimal min, max;
            //int month = this.caculateNumberOfMonthBetweenTowDate(pupil.BirthDate, monitoringDate);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = min;
            //ViewData[HealthTestConstants.Max] = max;

            ViewData[HealthTestConstants.MONITORINGDATE] = monitoringDate;
            ViewData[HealthTestConstants.UNITNAME] = unitName;
            List<HealthPeriod> ListHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();

            if (isActive == false)
            {
                ViewData[HealthTestConstants.LISTHEALTHPERIOD] = new SelectList(ListHealthPeriod, "HealthPeriodID", "Resolution");
            }
            else
            {
                ViewData[HealthTestConstants.LISTHEALTHPERIOD] = new SelectList(ListHealthPeriod, "HealthPeriodID", "Resolution", 2);
            }

            ViewData[HealthTestConstants.LIS_PHYSICALCLASSIFICATION] = new SelectList(CommonList.PhysicalClassification(), "key", "value");
            ViewData[HealthTestConstants.LIST_EVALUATION_HEALTH] = new SelectList(CommonList.EvaluationHealth(), "key", "value");
            return View("_creatHealthTest");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateHealthTest(FormCollection frm, PhysicalTestViewModel physicalTest, EyeTestViewModel eyeTest, ENTTestViewModel entTest, SpineTestViewModel spineTest, DentalTestViewModel dentalTest, OverallTestViewModel overallTest)
        {

            int ClassID = Convert.ToInt32(frm["classID"]);
            int PupilID = Convert.ToInt32(frm["pupilID"]);
            int HealthPeriodID = Convert.ToInt32(frm["cboHealthPeriod"]);
            bool IsDeaf = Convert.ToBoolean(frm["IsDeaf"]);
            bool isSick = Convert.ToBoolean(frm["IsSick"]);
            bool IsHeartDiseases = Convert.ToBoolean(frm["IsHeartDiseases"]);
            bool IsBirthDefect = Convert.ToBoolean(frm["IsBirthDefect"]);
            bool IsDredging = Convert.ToBoolean(frm["IsDredging"]);
            bool overallInternal = Convert.ToBoolean(frm["OverallInternal"]);
            bool overallForeign = Convert.ToBoolean(frm["OverallForeign"]);
            bool skinDiseases = Convert.ToBoolean(frm["SkinDiseases"]);

            string OtherEyeTest = frm["OtherEyeTest"].ToString();
            string OtherSpineTest = frm["OtherSpineTest"].ToString();
            string OtherDentalTest = frm["OtherDentalTest"].ToString();

            string unit_Name = frm["unit_Name"].ToString();

            GlobalInfo global = new GlobalInfo();

            PhysicalTest PhysicalTest = new PhysicalTest();
            EyeTest EyeTest = new EyeTest();
            ENTTest ENTTest = new ENTTest();
            SpineTest SpineTest = new SpineTest();
            DentalTest DentalTest = new DentalTest();
            OverallTest OverallTest = new OverallTest();

            //update Physical Test
            PhysicalTest.Breast = physicalTest.Breast;
            PhysicalTest.Weight = physicalTest.Weight;
            PhysicalTest.Height = physicalTest.Height;
            PhysicalTest.PhysicalClassification = physicalTest.PhysicalClassification;
            PhysicalTest.Nutrition = physicalTest.Nutrition;

            //Update Eye Test
            EyeTest.IsAstigmatism = eyeTest.IsAstigmatism;
            EyeTest.IsFarsightedness = eyeTest.IsFarsightedness;
            EyeTest.IsMyopic = eyeTest.IsMyopic;
            EyeTest.IsTreatment = eyeTest.IsTreatment;
            EyeTest.LAstigmatism = eyeTest.LAstigmatism;
            EyeTest.LEye = eyeTest.LEye != null ? (int)eyeTest.LEye : 0;
            EyeTest.LFarsightedness = eyeTest.LFarsightedness;
            EyeTest.LMyopic = eyeTest.LMyopic;
            EyeTest.Other = OtherEyeTest;
            EyeTest.RAstigmatism = eyeTest.RAstigmatism;
            EyeTest.REye = eyeTest.REye != null ? (int)eyeTest.REye : 0;
            EyeTest.RFarsightedness = eyeTest.RFarsightedness;
            EyeTest.RMyopic = eyeTest.RMyopic;

            //update Ent Test
            ENTTest.Description = entTest.Description;
            ENTTest.IsDeaf = IsDeaf;
            ENTTest.IsSick = isSick;
            ENTTest.LCapacityHearing = entTest.LCapacityHearing;
            ENTTest.RCapacityHearing = entTest.RCapacityHearing;

            //Update Spine Test
            SpineTest.ExaminationItalic = spineTest.ExaminationItalic;
            SpineTest.ExaminationStraight = spineTest.ExaminationStraight;
            SpineTest.IsDeformityOfTheSpine = spineTest.IsDeformityOfTheSpine;
            SpineTest.Other = OtherSpineTest;

            //Update DentalTest
            DentalTest.DescriptionDentalFilling = dentalTest.DescriptionDentalFilling;
            DentalTest.DescriptionPreventiveDentalFilling = dentalTest.DescriptionPreventiveDentalFilling;
            DentalTest.DescriptionTeethExtracted = dentalTest.DescriptionTeethExtracted;
            DentalTest.IsScraptTeeth = dentalTest.IsScraptTeeth;
            DentalTest.IsTreatment = dentalTest.IsTreatment;
            DentalTest.NumDentalFilling = dentalTest.NumDentalFilling != null ? dentalTest.NumDentalFilling.Value : 0;
            DentalTest.NumDentalFillingFour = dentalTest.NumDentalFillingFour;
            DentalTest.NumDentalFillingOne = dentalTest.NumDentalFillingOne;
            DentalTest.NumDentalFillingThree = dentalTest.NumDentalFillingThree;
            DentalTest.NumDentalFillingTwo = dentalTest.NumDentalFillingTwo;
            DentalTest.NumPreventiveDentalFilling = dentalTest.NumPreventiveDentalFilling;
            DentalTest.NumTeethExtracted = dentalTest.NumTeethExtracted;
            DentalTest.Other = OtherDentalTest;

            //Update Overall Test 
            OverallTest.DescriptionForeign = overallTest.DescriptionOverallForeign;
            OverallTest.DescriptionOverallInternall = overallTest.DescriptionOverallInternal;
            OverallTest.DescriptionSkinDiseases = overallTest.DescriptionSkinDiseases;
            OverallTest.Doctor = overallTest.Doctor;
            OverallTest.EvaluationHealth = overallTest.EvaluationHealth;
            OverallTest.HistoryYourSelf = overallTest.HistoryYourSelf;
            OverallTest.IsBirthDefect = IsBirthDefect;
            OverallTest.IsDredging = IsDredging;
            OverallTest.IsHeartDiseases = IsHeartDiseases;
            OverallTest.Other = overallTest.Other;
            OverallTest.OverallForeign = overallTest.OverallForeign;
            OverallTest.OverallInternal = overallTest.OverallInternal;
            OverallTest.RequireOfDoctor = overallTest.RequireOfDoctor;
            OverallTest.SkinDiseases = overallTest.SkinDiseases;

            PhysicalTest.CreatedDate
                = EyeTest.CreatedDate
                = ENTTest.CreatedDate
                = SpineTest.CreatedDate
                = DentalTest.CreatedDate
                = OverallTest.CreatedDate
                = DateTime.Now;

            PhysicalTest.IsActive
                = SpineTest.IsActive
                = DentalTest.IsActive
                = OverallTest.IsActive
                = ENTTest.IsActive
                = true;
            EyeTest.IsActive = true;

            PhysicalTest.Date = Convert.ToDateTime(frm["MonitoringDate"]);
            ENTTest.IsSick = isSick;
            OverallTest.OverallInternal = overallInternal == true ? 1 : 0;
            OverallTest.OverallForeign = overallForeign == true ? 1 : 0;
            OverallTest.SkinDiseases = skinDiseases == true ? 1 : 0;

            EyeTest.Other = OtherEyeTest;
            SpineTest.Other = OtherSpineTest;
            DentalTest.Other = OtherDentalTest;

            MonitoringBook MonitoringBook = new MonitoringBook();
            MonitoringBook.AcademicYearID = global.AcademicYearID.Value;
            MonitoringBook.SchoolID = global.SchoolID.Value;
            MonitoringBook.ClassID = ClassID;
            MonitoringBook.PupilID = PupilID;
            if (frm["MonitoringDate"] != "")
            {
                MonitoringBook.MonitoringDate = Convert.ToDateTime(frm["MonitoringDate"]);
            }
            MonitoringBook.UnitName = unit_Name;
            MonitoringBook.CreateDate = DateTime.Now;
            if ((global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE || global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN) && Convert.ToBoolean(frm["chkTrack"]) == true)
            {
                MonitoringBook.MonitoringType = SystemParamsInFile.TYPE_MONTH;
                MonitoringBook.HealthPeriodID = null;
            }
            else
            {
                MonitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                MonitoringBook.HealthPeriodID = HealthPeriodID;
            }
            MonitoringBook.EducationLevelID = ClassProfileBusiness.Find(ClassID).EducationLevelID;
            TryUpdateModel(SpineTest);

            MonitoringBookBusiness.InsertHealthTest(MonitoringBook, PhysicalTest, EyeTest, ENTTest, SpineTest, DentalTest, OverallTest);
            MonitoringBookBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditHealthTest(FormCollection frm, CustomView customView, PhysicalTestViewModel physicalTest, EyeTestViewModel eyeTest, ENTTestViewModel entTest, SpineTestViewModel spineTest, DentalTestViewModel dentalTest, OverallTestViewModel overallTest)
        {
            int ClassID = Convert.ToInt32(frm["classID"]);
            int PupilID = Convert.ToInt32(frm["pupilID"]);
            int HealthPeriodID = Convert.ToInt32(frm["cboHealthPeriod"]);
            bool IsDeaf = Convert.ToBoolean(frm["IsDeaf"]);
            bool isSick = Convert.ToBoolean(frm["IsSick"]);
            bool IsHeartDiseases = Convert.ToBoolean(frm["IsHeartDiseases"]);
            bool IsBirthDefect = Convert.ToBoolean(frm["IsBirthDefect"]);
            bool IsDredging = Convert.ToBoolean(frm["IsDredging"]);
            bool overallInternal = Convert.ToBoolean(frm["OverallInternal"]);
            bool overallForeign = Convert.ToBoolean(frm["OverallForeign"]);
            bool skinDiseases = Convert.ToBoolean(frm["SkinDiseases"]);
            int monitoringBookID = Convert.ToInt32(frm["monitoringBookID"]);
            string OtherEyeTest = frm["OtherEyeTest"].ToString();
            string OtherSpineTest = frm["OtherSpineTest"].ToString();
            string OtherDentalTest = frm["OtherDentalTest"].ToString();

            string unit_Name = frm["unit_Name"].ToString();
            MonitoringBook monitoringBook = MonitoringBookBusiness.Find(monitoringBookID);
            GlobalInfo global = new GlobalInfo();

            PhysicalTest PhysicalTest = new PhysicalTest();
            EyeTest EyeTest = new EyeTest();
            ENTTest ENTTest = new ENTTest();
            SpineTest SpineTest = new SpineTest();
            DentalTest DentalTest = new DentalTest();
            OverallTest OverallTest = new OverallTest();

            PhysicalTest = monitoringBook.PhysicalTests.FirstOrDefault();
            EyeTest = monitoringBook.EyeTests.FirstOrDefault();
            ENTTest = monitoringBook.ENTTests.FirstOrDefault();
            SpineTest = monitoringBook.SpineTests.FirstOrDefault();
            DentalTest = monitoringBook.DentalTests.FirstOrDefault();
            OverallTest = monitoringBook.OverallTests.FirstOrDefault();

            //update Physical Test
            PhysicalTest.Breast = physicalTest.Breast;
            PhysicalTest.Weight = physicalTest.Weight;
            PhysicalTest.Height = physicalTest.Height;
            PhysicalTest.PhysicalClassification = physicalTest.PhysicalClassification;
            PhysicalTest.Nutrition = physicalTest.Nutrition;

            //Update Eye Test
            EyeTest.IsAstigmatism = eyeTest.IsAstigmatism;
            EyeTest.IsFarsightedness = eyeTest.IsFarsightedness;
            EyeTest.IsMyopic = eyeTest.IsMyopic;
            EyeTest.IsTreatment = eyeTest.IsTreatment;
            EyeTest.LAstigmatism = eyeTest.LAstigmatism;
            EyeTest.LEye = eyeTest.LEye != null ? (int)eyeTest.LEye : 0;
            EyeTest.LFarsightedness = eyeTest.LFarsightedness;
            EyeTest.LMyopic = eyeTest.LMyopic;
            EyeTest.Other = OtherEyeTest;
            EyeTest.RAstigmatism = eyeTest.RAstigmatism;
            EyeTest.REye = eyeTest.REye != null ? (int)eyeTest.REye : 0;
            EyeTest.RFarsightedness = eyeTest.RFarsightedness;
            EyeTest.RMyopic = eyeTest.RMyopic;

            //update Ent Test
            ENTTest.Description = entTest.Description;
            ENTTest.IsDeaf = IsDeaf;
            ENTTest.IsSick = isSick;
            ENTTest.LCapacityHearing = entTest.LCapacityHearing;
            ENTTest.RCapacityHearing = entTest.RCapacityHearing;

            //Update Spine Test
            SpineTest.ExaminationItalic = spineTest.ExaminationItalic;
            SpineTest.ExaminationStraight = spineTest.ExaminationStraight;
            SpineTest.IsDeformityOfTheSpine = spineTest.IsDeformityOfTheSpine;
            SpineTest.Other = OtherSpineTest;

            //Update DentalTest
            DentalTest.DescriptionDentalFilling = dentalTest.DescriptionDentalFilling;
            DentalTest.DescriptionPreventiveDentalFilling = dentalTest.DescriptionPreventiveDentalFilling;
            DentalTest.DescriptionTeethExtracted = dentalTest.DescriptionTeethExtracted;
            DentalTest.IsScraptTeeth = dentalTest.IsScraptTeeth;
            DentalTest.IsTreatment = dentalTest.IsTreatment;
            DentalTest.NumDentalFilling = dentalTest.NumDentalFilling != null ? dentalTest.NumDentalFilling.Value : 0;
            DentalTest.NumDentalFillingFour = dentalTest.NumDentalFillingFour;
            DentalTest.NumDentalFillingOne = dentalTest.NumDentalFillingOne;
            DentalTest.NumDentalFillingThree = dentalTest.NumDentalFillingThree;
            DentalTest.NumDentalFillingTwo = dentalTest.NumDentalFillingTwo;
            DentalTest.NumPreventiveDentalFilling = dentalTest.NumPreventiveDentalFilling;
            DentalTest.NumTeethExtracted = dentalTest.NumTeethExtracted;
            DentalTest.Other = OtherDentalTest;

            //Update Overall Test 
            OverallTest.DescriptionForeign = overallTest.DescriptionOverallForeign;
            OverallTest.DescriptionOverallInternall = overallTest.DescriptionOverallInternal;
            OverallTest.DescriptionSkinDiseases = overallTest.DescriptionSkinDiseases;
            OverallTest.Doctor = overallTest.Doctor;
            OverallTest.EvaluationHealth = overallTest.EvaluationHealth;
            OverallTest.HistoryYourSelf = overallTest.HistoryYourSelf;
            OverallTest.IsBirthDefect = IsBirthDefect;
            OverallTest.IsDredging = IsDredging;
            OverallTest.IsHeartDiseases = IsHeartDiseases;
            OverallTest.Other = overallTest.Other;
            OverallTest.OverallForeign = overallTest.OverallForeign;
            OverallTest.OverallInternal = overallTest.OverallInternal;
            OverallTest.RequireOfDoctor = overallTest.RequireOfDoctor;
            OverallTest.SkinDiseases = overallTest.SkinDiseases;

            PhysicalTest.ModifiedDate
                = EyeTest.ModifiedDate
                = ENTTest.ModifiedDate
                = SpineTest.ModifiedDate
                = DentalTest.ModifiedDate
                = OverallTest.ModifiedDate
                = DateTime.Now;

            ENTTest.IsSick = isSick;
            OverallTest.OverallInternal = overallInternal == true ? 1 : 0;
            OverallTest.OverallForeign = overallForeign == true ? 1 : 0;
            OverallTest.SkinDiseases = skinDiseases == true ? 1 : 0;

            EyeTest.Other = OtherEyeTest;
            SpineTest.Other = OtherSpineTest;
            DentalTest.Other = OtherDentalTest;



            if (frm["MonitoringDate"] != "")
            {
                monitoringBook.MonitoringDate = Convert.ToDateTime(frm["MonitoringDate"]);
                PhysicalTest.Date = Convert.ToDateTime(frm["MonitoringDate"]);
            }
            monitoringBook.UnitName = unit_Name;


            MonitoringBookBusiness.Update(monitoringBook);

            PhysicalTestBusiness.Update(PhysicalTest);

            EyeTestBusiness.Update(EyeTest);
            ENTTestBusiness.Update(ENTTest);
            SpineTestBusiness.Update(SpineTest);
            DentalTestBusiness.Update(DentalTest);
            OverallTestBusiness.Update(OverallTest);

            MonitoringBookBusiness.Save();
            PhysicalTestBusiness.Save();
            EyeTestBusiness.Save();
            ENTTestBusiness.Save();
            SpineTestBusiness.Save();
            DentalTestBusiness.Save();
            OverallTestBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteHealthTest(int monitoringBookID)
        {
            MonitoringBook monitoringBook = MonitoringBookBusiness.Find(monitoringBookID);
            if (monitoringBook.PhysicalTests.FirstOrDefault() != null)
            {
                this.PhysicalTestBusiness.Delete(monitoringBook.PhysicalTests.FirstOrDefault().PhysicalTestID);
                this.PhysicalTestBusiness.Save();
            }
            if (monitoringBook.EyeTests.FirstOrDefault() != null)
            {
                this.EyeTestBusiness.Delete(monitoringBook.EyeTests.FirstOrDefault().EyeTestID);
                this.EyeTestBusiness.Save();
            }
            if (monitoringBook.ENTTests.FirstOrDefault() != null)
            {
                this.ENTTestBusiness.Delete(monitoringBook.ENTTests.FirstOrDefault().ENTTestID);
                this.ENTTestBusiness.Save();
            }
            if (monitoringBook.SpineTests.FirstOrDefault() != null)
            {
                this.SpineTestBusiness.Delete(monitoringBook.SpineTests.FirstOrDefault().SpineTestID);
                this.SpineTestBusiness.Save();
            }
            if (monitoringBook.DentalTests.FirstOrDefault() != null)
            {
                this.DentalTestBusiness.Delete(monitoringBook.DentalTests.FirstOrDefault().DentalTestID);
                this.DentalTestBusiness.Save();
            }
            if (monitoringBook.OverallTests.FirstOrDefault() != null)
            {
                this.OverallTestBusiness.Delete(monitoringBook.OverallTests.FirstOrDefault().OverallTestID);
                this.OverallTestBusiness.Save();
            }

            this.MonitoringBookBusiness.Delete(monitoringBookID);
            this.MonitoringBookBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Physical test

        /// <summary>
        /// Lay dot kham mac dinh
        /// </summary>
        /// <param name="pupilID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="typeTest"></param>
        /// <param name="DefaultHealthPeriodID"></param>
        /// <returns></returns>
        private HealthPeriod getDefaultHealthPeriod(int pupilID, int academicYearID, string typeTest)
        {
            List<HealthPeriod> ListHealthPeriod = this.HealthPeriodBusiness.All.Where(o => o.IsActive).OrderBy(p => p.HealthPeriodID).ToList();
            // Tim xem hoc sinh da duoc nhap thong tin dot kham nao trong nam hoc hien tai chua
            var monitoringBooksOfPupil = this.MonitoringBookBusiness.All
                .Where(p => p.PupilID == pupilID && p.AcademicYearID == academicYearID && p.HealthPeriod != null)
                .OrderBy(p => p.HealthPeriodID)
                .ToList();
            HealthPeriod DefaultHealthPeriod;
            if (monitoringBooksOfPupil.Count() == 0) // Neu chua duoc nhap thi chon dot dau tien lam dot mac dinh
            {
                DefaultHealthPeriod = ListHealthPeriod[0];
            }
            else
            {
                if ((typeTest == "PhysicalTest" && monitoringBooksOfPupil.Last().PhysicalTests.Count(p=>p.IsActive) == 0)
                    || (typeTest == "EyeTest" && monitoringBooksOfPupil.Last().EyeTests.Count(p => (!p.IsActive.HasValue || p.IsActive == true)) == 0)
                    || (typeTest == "ENTTest" && monitoringBooksOfPupil.Last().ENTTests.Count(p => p.IsActive) == 0)
                    || (typeTest == "SpineTest" && monitoringBooksOfPupil.Last().SpineTests.Count(p => p.IsActive) == 0)
                    || (typeTest == "DentalTest" && monitoringBooksOfPupil.Last().DentalTests.Count(p => p.IsActive) == 0)
                    || (typeTest == "OverallTest" && monitoringBooksOfPupil.Last().OverallTests.Count(p => p.IsActive) == 0))
                {
                    DefaultHealthPeriod = monitoringBooksOfPupil.Last().HealthPeriod;
                }
                else
                {
                    int temp = monitoringBooksOfPupil.Last().HealthPeriodID.Value;
                    DefaultHealthPeriod = ListHealthPeriod.Where(p => p.HealthPeriodID > temp).OrderBy(p => p.HealthPeriodID).FirstOrDefault();
                    if (DefaultHealthPeriod == null)
                    {
                        DefaultHealthPeriod = monitoringBooksOfPupil.Last().HealthPeriod;
                    }
                }
            }
            return DefaultHealthPeriod;
        }

        /// <summary>
        /// Lay ngay kham va ten don vi kham mac dinh trong dot kham
        /// </summary>
        /// <param name="pupilID"></param>
        /// <param name="HealthPeriodID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="DefaultMonitoringDate"></param>
        /// <param name="EnableToEditMonitoringDate"></param>
        /// <param name="DefaultUnitName"></param>
        /// <param name="EnableToEditUnitName"></param>
        private void getDefaultMonitoringDateAndDefaultUnitName(
            int pupilID, int HealthPeriodID, int academicYearID,
            out DateTime DefaultMonitoringDate, out bool EnableToEditMonitoringDate, out string DefaultUnitName, out bool EnableToEditUnitName)
        {
            // Tim xem hoc sinh da duoc nhap thong tin dot kham nao trong nam hoc hien tai chua
            var monitoringBooksOfPupil = this.MonitoringBookBusiness.All
                .Where(p => p.PupilID == pupilID && p.AcademicYearID == academicYearID && p.HealthPeriod != null)
                .OrderBy(p => p.HealthPeriodID)
                .ToList();
            // Lay ngay va ten don vi kham mac dinh
            var monitoringBookOfPupilInDefaultHealthPeriod = monitoringBooksOfPupil.FirstOrDefault(m => m.HealthPeriodID == HealthPeriodID);
            if (monitoringBookOfPupilInDefaultHealthPeriod != null)
            {
                DefaultMonitoringDate = monitoringBookOfPupilInDefaultHealthPeriod.MonitoringDate.HasValue ? monitoringBookOfPupilInDefaultHealthPeriod.MonitoringDate.Value : DateTime.Now;
                EnableToEditMonitoringDate = false;
                DefaultUnitName = monitoringBookOfPupilInDefaultHealthPeriod.UnitName;
                EnableToEditUnitName = false;
            }
            else
            {
                EnableToEditMonitoringDate = true;
                EnableToEditUnitName = true;
                // Chon ngay kham va don vi kham mac dinh dua tren ban cung lop
                var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == pupilID && pc.AcademicYearID == academicYearID);
                if (classOfPupil == null) throw new SMASException("Khong tim thay lop hoc cua hoc sinh. Ma hoc sinh khong hop le");
                var monitoringBooksOfClass = this.MonitoringBookBusiness.All
                    .Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                                && p.HealthPeriodID == HealthPeriodID
                                && p.ClassID == classOfPupil.ClassID)
                    .OrderByDescending(p => p.MonitoringDate);
                if (monitoringBooksOfClass.Count() > 0)
                {
                    var lastMonitoringBookOfClass = monitoringBooksOfClass.First();
                    DefaultMonitoringDate = lastMonitoringBookOfClass.MonitoringDate.HasValue ? lastMonitoringBookOfClass.MonitoringDate.Value : DateTime.Now;
                    DefaultUnitName = lastMonitoringBookOfClass.UnitName;
                }
                else
                {
                    // Chon ngay kham va don vi kham mac dinh dua tren ban cung truong
                    var monitoringBooksOfSchool = this.MonitoringBookBusiness.All
                            .Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                                        && p.HealthPeriodID == HealthPeriodID
                                        && p.SchoolID == classOfPupil.SchoolID)
                            .OrderByDescending(p => p.MonitoringDate);
                    if (monitoringBooksOfSchool.Count() > 0)
                    {
                        // Neu da co mot hoc sinh trong truong duoc kham, chon ra so kham cua hoc sinh duoc kham cuoi cung
                        var monitoringBookOfSchoolLast = monitoringBooksOfSchool.First();
                        DefaultMonitoringDate = monitoringBookOfSchoolLast.MonitoringDate.HasValue ? monitoringBookOfSchoolLast.MonitoringDate.Value : DateTime.Now;
                        DefaultUnitName = monitoringBookOfSchoolLast.UnitName;
                    }
                    else
                    {
                        // Neu ca truong deu chua duoc kham trong dot nay thi chon mac dinh la ngay hien tai
                        DefaultMonitoringDate = DateTime.Now;
                        DefaultUnitName = "";
                    }
                }
            }
        }

        /// <summary>
        /// Lay Ngay kham va Don vi kham mac dinh.
        /// </summary>
        /// <param name="ppid">Ma hoc sinh.</param>
        /// <param name="hpid">Ma dot kham.</param>
        /// <param name="ayid">Ma nam hoc.</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult getDefaultMonitoringDateAndDefaultUnitName(int ppid, int hpid, int? ayid)
        {
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int pupilProfileID = ppid;
            int healthPeriodID = hpid;
            int academicYearID = ayid ?? _globalInfo.AcademicYearID ?? 0;
            this.getDefaultMonitoringDateAndDefaultUnitName(pupilProfileID, healthPeriodID, academicYearID
                , out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            return Json(new 
            { 
                DefaultMonitoringDate = DefaultMonitoringDate.ToShortDateString(),
                EnableToEditMonitoringDate = EnableToEditMonitoringDate, 
                DefaultUnitName = DefaultUnitName,
                EnableToEditUnitName = EnableToEditUnitName
            }, JsonRequestBehavior.AllowGet);
        }

        private string getDefaultDoctorName(int pupilID, int HealthPeriodID, int academicYearID)
        {
            string defaultDoctorName;
            // Chon ngay kham va don vi kham mac dinh dua tren ban cung lop
            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == pupilID && pc.AcademicYearID == academicYearID);
            if (classOfPupil == null) throw new SMASException("Khong tim thay lop hoc cua hoc sinh. Ma hoc sinh khong hop le");
            var monitoringBooksOfClass = this.MonitoringBookBusiness.All
                .Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                            && p.HealthPeriodID == HealthPeriodID
                            && p.ClassID == classOfPupil.ClassID
                            && p.OverallTests.Count > 0)
                .OrderByDescending(p => p.MonitoringDate);
            if (monitoringBooksOfClass.Count() > 0)
            {
                var lastMonitoringBookOfClass = monitoringBooksOfClass.First();
                defaultDoctorName = lastMonitoringBookOfClass.OverallTests.First().Doctor;
            }
            else
            {
                // Chon ngay kham va don vi kham mac dinh dua tren ban cung truong
                var monitoringBooksOfSchool = this.MonitoringBookBusiness.All
                        .Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                                    && p.HealthPeriodID == HealthPeriodID
                                    && p.SchoolID == classOfPupil.SchoolID
                                    && p.OverallTests.Count > 0)
                        .OrderByDescending(p => p.MonitoringDate);
                if (monitoringBooksOfSchool.Count() > 0)
                {
                    // Neu da co mot hoc sinh trong truong duoc kham, chon ra so kham cua hoc sinh duoc kham cuoi cung
                    var monitoringBookOfSchoolLast = monitoringBooksOfSchool.First();
                    defaultDoctorName = monitoringBookOfSchoolLast.OverallTests.First().Doctor;
                }
                else
                {
                    // Neu ca truong deu chua duoc kham trong dot nay thi chon mac dinh la ngay hien tai
                    defaultDoctorName = "";
                }
            }
            return defaultDoctorName;
        }

        /// <summary>
        /// Lay gia tri mac dinh cua truong "Tien su ban than" trong kham tong quat
        /// </summary>
        /// <param name="pupilProfileID"></param>
        /// <returns></returns>
        private string getDefaultHistoryYourSelf(int pupilProfileID)
        {
            var query = this.OverallTestBusiness.All.Where(o => o.MonitoringBook.PupilID == pupilProfileID && o.IsActive).OrderByDescending(o => o.OverallTestID).FirstOrDefault();
            if (query == null) return "";
            else return query.HistoryYourSelf;
        }


        /// <summary>
        /// Lay phieu kham suc khoe loai "theo dot" cua hoc sinh.
        /// </summary>
        /// <returns></returns>
        private List<HealthPeriod> getHealthPeriods()
        {
            var result = this.HealthPeriodBusiness.All.Where(o => o.IsActive).OrderBy(p => p.HealthPeriodID).ToList();
            return result;
        }
        /// <summary>
        /// Lay monitoringBook cua hoc sinh trong mot dot
        /// </summary>
        /// <param name="pupilID"></param>
        /// <param name="healthPeriodID"></param>
        /// <param name="academicYearID"></param>
        /// <returns></returns>
        private MonitoringBook getMonitoringBookOfPupil(int pupilID, int healthPeriodID, int academicYearID)
        {
            var result = this.MonitoringBookBusiness.All
                .FirstOrDefault(mb => mb.PupilID == pupilID && mb.AcademicYearID == academicYearID && mb.HealthPeriodID == healthPeriodID);
            return result;
        }
        
        /// <summary>
        /// Lay phieu kham suc khoe loai "theo thang" cua hoc sinh.
        /// </summary>
        /// <param name="pupilID">Ma hoc sinh</param>
        /// <param name="month">Thang can lay</param>
        /// <param name="year">Nam can lay</param>
        /// <returns></returns>
        private MonitoringBook getMonitoringBookOfPupil(int pupilID, int month, int year, int academicYearID)
        {
            var result = this.MonitoringBookBusiness.All
                .FirstOrDefault(p => p.PupilID == pupilID && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                    && p.MonitoringType == SystemParamsInFile.TYPE_MONTH && !p.HealthPeriodID.HasValue
                                    && p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Month == month : true 
                                    &&  p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Year == year : true);
            return result;
        }

        /// <summary>
        /// Lay cac nguong danh gia tinh trang suc khoe
        /// </summary>
        /// <param name="genre">Gioi tinh hoc sinh</param>
        /// <param name="month">So thang tuoi cua hoc sinh</param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        private void getThresholdNutritionIndex(bool genre, int month, out decimal min, out decimal max)
        {
            string typeConfigName;
            int typeConfig;
            int genreID = genre ? 1 : 0;
            decimal defaultMin, defaultMax;
            if (month <= 60)
            {
                typeConfigName = "CÂN NẶNG";
                typeConfig = 5; // ID cua tieu chi can nang
                defaultMin = 2m;
                defaultMax = 9m;
            }
            else
            {
                typeConfigName = "BMI";
                typeConfig = 7; // ID cua tieu chi BMI
                defaultMin = 18m;
                defaultMax = 24.9m;
            }
            //add search info	
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["GenreID"] = genreID;
            SearchInfo["TypeConfigID"] = typeConfig;
            IQueryable<ClassificationCriteriaDetail> queryListClassificationCriteriaDetail = this.ClassificationCriteriaDetailBusiness.GetListClassificationDetail(SearchInfo);

            // Lay gia tri nguong nho nhat
            var queryByMonth = queryListClassificationCriteriaDetail.Where(p => p.Month == month)
                                .Select(p => new { p.IndexValue, p.Month, p.ClassificationCriteria.EffectDate, p.ClassificationLevelHealth.Level })
                .ToList();
            var minValue = queryByMonth.Where(p=>p.Level == -3).OrderByDescending(o=>o.EffectDate).FirstOrDefault();
            if (minValue == null || !minValue.IndexValue.HasValue)
            {
                min = defaultMin;
            }
            else
            {
                min = minValue.IndexValue.Value;
            }
            var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (maxValue == null || !maxValue.IndexValue.HasValue)
            {
                max = defaultMax;
            }
            else
            {
                max = maxValue.IndexValue.Value;
            }
        }

        /// <summary>
        /// Lay cac nguong danh gia tinh trang suc khoe
        /// </summary>
        /// <param name="genre">Gioi tinh hoc sinh</param>
        /// <param name="month">So thang tuoi cua hoc sinh</param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        private void getThresholdNutritionIndex(int typeConfigID, int genreID, int month, out decimal min, out decimal max, decimal defaultForMin, decimal defaultForMax)
        {
            //add search info
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["GenreID"] = genreID;
            SearchInfo["TypeConfigID"] = typeConfigID;
            IQueryable<ClassificationCriteriaDetail> queryListClassificationCriteriaDetail = this.ClassificationCriteriaDetailBusiness.GetListClassificationDetail(SearchInfo);

            // Lay gia tri nguong nho nhat
            var queryByMonth = queryListClassificationCriteriaDetail
                .Where(p => p.Month == month)
                .Select(p => new { p.IndexValue, p.Month, p.ClassificationCriteria.EffectDate, p.ClassificationLevelHealth.Level })
                .ToList();
            var minValue = queryByMonth.Where(p => p.Level == -3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (minValue == null || !minValue.IndexValue.HasValue)
            {
                min = defaultForMin;
            }
            else
            {
                min = minValue.IndexValue.Value;
            }
            var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (maxValue == null || !maxValue.IndexValue.HasValue)
            {
                max = defaultForMax;
            }
            else
            {
                max = maxValue.IndexValue.Value;
            }
        }

        class EvaluationResult
        {
            public decimal BMI;
            public int Nutrition;
            public string NutritionName;
        }

        /// <summary>
        /// Tinh so thang giua 2 ngay.
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private int caculateNumberOfMonthBetweenTowDate(DateTime fromDate, DateTime toDate)
        {
            int result = (toDate.Year - fromDate.Year) * 12 + toDate.Month - fromDate.Month + (toDate.Day >= fromDate.Day ? 0 : -1);
            return result;
        }

        private EvaluationResult DanhGiaPhanLoaiDinhDuong(int month, int genreID, decimal Height, decimal Weight)
        {
            // Chi so BMI
            decimal BMI = Math.Round(Weight / ((Height * 0.01m) * (Height * 0.01m)), 2);

            decimal defaultForMin, defaultForMax;
            decimal minThreshold, maxThreshold;
            decimal index;
            int typeConfigID;
            if (month <= 60)
            {
                //typeConfigName = "CÂN NẶNG";
                typeConfigID = 5; // ID cua tieu chi can nang
                defaultForMin = 2m;
                defaultForMax = 9m;
                index = Weight;
            }
            else
            {
                //typeConfigName = "BMI";
                typeConfigID = 7; // ID cua tieu chi BMI
                defaultForMin = 18m;
                defaultForMax = 24.9m;
                index = BMI;
            }
            // Lay cac gia tri nguong
            this.getThresholdNutritionIndex(typeConfigID, genreID, month, out minThreshold, out maxThreshold, defaultForMin, defaultForMax);
            // Xet phan loai dua tren nguong da lay
            int Nutrition;
            string NutritionName;
            if (index < minThreshold)
            {
                Nutrition = SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION;
                NutritionName = "Suy dinh dưỡng";
            }
            else if (index <= maxThreshold)
            {
                Nutrition = SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT;
                NutritionName = "Bình Thường";
            }
            else
            {
                Nutrition = SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT;
                NutritionName = "Béo phì";
            }
            return new EvaluationResult { BMI = BMI, Nutrition = Nutrition, NutritionName = NutritionName };
        }

        public ActionResult DanhGiaPhanLoaiDinhDuong(int PupilID, DateTime MonitoringDate, decimal Height, decimal Weight)
        {
            // Hoc sinh duoc lua chon
            var selectedPupilProfile = this.PupilProfileBusiness.Find(PupilID);
            // So thang tuoi
            int month = this.caculateNumberOfMonthBetweenTowDate(selectedPupilProfile.BirthDate, MonitoringDate);
            // Gioi tinh
            int genreID = selectedPupilProfile.Genre;
            var result = this.DanhGiaPhanLoaiDinhDuong(month, genreID, Height, Weight);
            return Json(new { BMI = result.BMI, Nutrition = result.Nutrition, NutritionName = result.NutritionName }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreatePhysicalTest(int id)
        {
            int pupilProfileID = id;
            PupilProfile selectedPupilProfile = this.PupilProfileBusiness.Find(pupilProfileID);
            if (selectedPupilProfile == null) throw new BusinessException("Học sinh không tồn tại");
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            HealthPeriod DefaultHealthPeriod = this.getDefaultHealthPeriod(pupilProfileID, _globalInfo.AcademicYearID.Value, "PhysicalTest");
            int DefaultHealthPeriodID = DefaultHealthPeriod.HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(pupilProfileID, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value
                , out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            ViewBag.PupilID = pupilProfileID;
            ViewBag.DefaultHealthPeriodID = DefaultHealthPeriodID;
            ViewBag.DefaultMonitoringDate = DefaultMonitoringDate;
            ViewBag.EnableToEditMonitoringDate = EnableToEditMonitoringDate;
            ViewBag.DefaultUnitName = DefaultUnitName;
            ViewBag.EnableToEditUnitName = EnableToEditUnitName;
            ViewBag.HealthPeriods = this.getHealthPeriods();
            // Tim gia tri min, max cua muc phan loai
            //bool genre = selectedPupilProfile.Genre == 1;
            //decimal min,max;
            //int month = this.caculateNumberOfMonthBetweenTowDate(selectedPupilProfile.BirthDate,DefaultMonitoringDate);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = min;
            //ViewData[HealthTestConstants.Max] = max;
            //ViewBag.Month = month;
            ViewBag.Mode = "create";
            return PartialView("__PhysicalTest");
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreatePhysicalTest(PhysicalTestViewModel physicalTestCreating)
        {
            ViewBag.Mode = "create";
            return SavePhysicalTest(physicalTestCreating);
        }


        [HttpGet]
        public ActionResult EditPhysicalTest(int id)
        {
            PhysicalTest physicalTest = PhysicalTestBusiness.Find(id);
            if (physicalTest == null) throw new BusinessException("Lỗi dữ liệu");
            PupilProfile selectedPupilProfile = physicalTest.MonitoringBook.PupilProfile;
            if (selectedPupilProfile == null) throw new BusinessException("Học sinh không tồn tại");

            decimal? BMI = null;
            if ((physicalTest.Height.HasValue && physicalTest.Weight.HasValue))
            {
                BMI = Math.Round(physicalTest.Weight.Value / ((physicalTest.Height.Value * 0.01m) * (physicalTest.Height.Value * 0.01m)), 2);
            }

            PhysicalTestViewModel model = new PhysicalTestViewModel()
            {
                PhysicalTestID = physicalTest.PhysicalTestID,
                HealthPeriodID = physicalTest.MonitoringBook.HealthPeriodID ?? 0,
                MonitoringDate = physicalTest.MonitoringBook.MonitoringDate.HasValue ?  physicalTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                UnitName = physicalTest.MonitoringBook.UnitName,
                PupilID = physicalTest.MonitoringBook.PupilID,
                MonitoringBookID = physicalTest.MonitoringBookID,
                Weight = physicalTest.Weight,
                Height = physicalTest.Height,
                Breast = physicalTest.Breast,
                PhysicalClassification = physicalTest.PhysicalClassification,
                Nutrition = physicalTest.Nutrition ?? 0,
                BMIIndex = BMI,
                chkTrack = !physicalTest.MonitoringBook.HealthPeriodID.HasValue,
                CreateDate = physicalTest.CreatedDate
            };
            List<HealthPeriod> ListHealthPeriod = this.HealthPeriodBusiness.All.Where(o => o.IsActive).OrderBy(p => p.HealthPeriodID).ToList();
            ViewBag.HealthPeriods = ListHealthPeriod;
            // Tim gia tri min, max cua muc phan loai
            //bool genre = selectedPupilProfile.Genre == 1;
            //decimal min, max;
            //int month = this.caculateNumberOfMonthBetweenTowDate(selectedPupilProfile.BirthDate, model.MonitoringDate);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = min;
            //ViewData[HealthTestConstants.Max] = max;
            //ViewBag.Month = month;
            ViewBag.Mode = "update";
            return PartialView("__PhysicalTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdatePhysicalTest(PhysicalTestViewModel physicalTestUpdating)
        {
            ViewBag.Mode = "update";
            return SavePhysicalTest(physicalTestUpdating);
        }

        private ActionResult SavePhysicalTest(PhysicalTestViewModel physicalTestSaving)
        {
            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == physicalTestSaving.PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) throw new BusinessException("Lỗi dữ liệu");
            PupilProfile selectedPupilProfile = classOfPupil.PupilProfile;
            if (selectedPupilProfile == null) throw new BusinessException("Học sinh không tồn tại");
            // Kiểm tra ngày khám phai truoc ngay hien tai
            string MonitoringDateName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.MonitoringDate);
            
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (physicalTestSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
            }
            if (physicalTestSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName],out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");                      
                    }
                }
            }
            // Kiểm tra ngày khám phai trong khoang thoi gian nam hoc
            if (ModelState.IsValidField(MonitoringDateName))
            {
                var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                if (physicalTestSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || physicalTestSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                {
                    ModelState.AddModelError(MonitoringDateName
                        , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}"
                        , ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                }
            }
            // Kiem tra vong nguc
            string BreastName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.Breast);
            // xoa voi cap mam non
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ModelState[BreastName].Errors.Clear();
            }
            // Kiem tra da chon phan loai the luc chua
            string PhysicalClassificationName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.PhysicalClassification);
            if (ModelState.IsValidField(PhysicalClassificationName))
            {
                if (physicalTestSaving.PhysicalClassification < 0 || physicalTestSaving.PhysicalClassification > 5)
                {
                    ModelState.AddModelError(PhysicalClassificationName, "Chưa chọn phân loại thể lực");
                }
            }

            // Xoa loi doi voi truong BMIIndex vi truong nay ko bat buoc
            string BMIIndexName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.BMIIndex);
            ModelState[BMIIndexName].Errors.Clear();
            // Xoa loi doi voi truong Nutrition vi truong nay ko bat buoc
            string NutritionName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.Nutrition);
            ModelState[NutritionName].Errors.Clear();

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                if (physicalTestSaving.chkTrack) // neu nguoi dung muon tao moi phieu kham suc khoe hang thang
                {
                    monitoringBook = this.getMonitoringBookOfPupil(physicalTestSaving.PupilID, physicalTestSaving.MonitoringDate.Month, physicalTestSaving.MonitoringDate.Year, _globalInfo.AcademicYearID.Value);
                }
                else
                {
                    monitoringBook = this.getMonitoringBookOfPupil(physicalTestSaving.PupilID, physicalTestSaving.HealthPeriodID, _globalInfo.AcademicYearID.Value);
                }
                if (monitoringBook == null) // Tao moi
                {
                    monitoringBook = new MonitoringBook()
                    {
                        AcademicYearID = _globalInfo.AcademicYearID.Value,
                        SchoolID = _globalInfo.SchoolID.Value,
                        ClassID = classOfPupil.ClassID,
                        PupilID = classOfPupil.PupilID,
                        EducationLevelID = classOfPupil.ClassProfile.EducationLevelID,
                        MonitoringType = physicalTestSaving.chkTrack ? SystemParamsInFile.TYPE_MONTH : SystemParamsInFile.TYPE_PERIOD,
                        HealthPeriodID = physicalTestSaving.chkTrack ? null : (int?)physicalTestSaving.HealthPeriodID,
                        MonitoringDate = physicalTestSaving.MonitoringDate,
                        UnitName = physicalTestSaving.UnitName,
                        CreateDate = DateTime.Now
                    };
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else // Cap nhat
                {
                    if (monitoringBook.PhysicalTests.Count(p => p.PhysicalTestID != physicalTestSaving.PhysicalTestID) > 0)
                    {
                        if (monitoringBook.MonitoringType == SystemParamsInFile.TYPE_MONTH)
                        {
                            // Thong bao loi hoc sinh da co ket qua kham suc khoe trong thang nay
                            string MonitoringDateFieldName = StaticReflection.GetMemberName<PhysicalTestViewModel>(o => o.MonitoringDate);
                            ModelState.AddModelError(MonitoringDateFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} tháng {2}.", classOfPupil.PupilProfile.FullName, "sức khỏe", physicalTestSaving.MonitoringDate.Month));
                        }
                        else if (monitoringBook.MonitoringType == SystemParamsInFile.TYPE_PERIOD)
                        {
                            // Thong bao loi hoc sinh da co ket qua kham suc khoe trong dot nay
                            string HealthPeriodIDFieldName = StaticReflection.GetMemberName<PhysicalTestViewModel>(o => o.HealthPeriodID);
                            ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "sức khỏe", physicalTestSaving.HealthPeriodID));
                        }
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = physicalTestSaving.MonitoringDate;
                        monitoringBook.UnitName = physicalTestSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                PhysicalTest physicalTest;
                if (!physicalTestSaving.PhysicalTestID.HasValue || physicalTestSaving.PhysicalTestID <= 0)
                {
                    physicalTest = new PhysicalTest();
                    physicalTest.CreatedDate = DateTime.Now;
                }
                else
                {
                    physicalTest = this.PhysicalTestBusiness.Find(physicalTestSaving.PhysicalTestID);
                    if (physicalTest == null) throw new BusinessException("Lỗi dữ liệu");
                }
                physicalTest.MonitoringBook = monitoringBook;
                physicalTest.Height = physicalTestSaving.Height;
                physicalTest.Weight = physicalTestSaving.Weight;
                physicalTest.Breast = physicalTestSaving.Breast;
                physicalTest.PhysicalClassification = physicalTestSaving.PhysicalClassification;
                physicalTest.Date = physicalTestSaving.MonitoringDate;
                // Tinh toan lai phan loai beo phi hay binh thuong hay suy dinh duong
                int numberOfMonth = this.caculateNumberOfMonthBetweenTowDate(selectedPupilProfile.BirthDate, physicalTestSaving.MonitoringDate);
                int genreOfPupil = selectedPupilProfile.Genre;
                if (physicalTestSaving.Height.HasValue && physicalTestSaving.Weight.HasValue)
                {
                    EvaluationResult evaluationResult = this.DanhGiaPhanLoaiDinhDuong(numberOfMonth, genreOfPupil, physicalTestSaving.Height.Value, physicalTestSaving.Weight.Value);
                    physicalTest.Nutrition = evaluationResult.Nutrition;
                }
               
                physicalTest.ModifiedDate = DateTime.Now;
                physicalTest.IsActive = true;
                if (physicalTestSaving.PhysicalTestID.HasValue && physicalTestSaving.PhysicalTestID > 0)
                {
                    this.PhysicalTestBusiness.Update(physicalTest);
                }
                else
                {
                    this.PhysicalTestBusiness.Insert(physicalTest);
                }
                this.PhysicalTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            // Tim gia tri min, max cua muc phan loai
            //bool genre = selectedPupilProfile.Genre == 1;
            //decimal min, max;
            //int month = this.caculateNumberOfMonthBetweenTowDate(selectedPupilProfile.BirthDate, physicalTestSaving.MonitoringDate);
            //this.getThresholdNutritionIndex(genre, month, out min, out max);
            //ViewData[HealthTestConstants.Min] = min;
            //ViewData[HealthTestConstants.Max] = max;
            //ViewBag.Month = month;
            return PartialView("__PhysicalTest", physicalTestSaving);
        }

        [ValidateAntiForgeryToken]
        
        public JsonResult DeletePhysicalTest(int id)
        {
            var deleted = this.PhysicalTestBusiness.All.FirstOrDefault(p=>p.PhysicalTestID == id && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.PhysicalTestBusiness.DeleteAll(new List<PhysicalTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.PhysicalTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.PhysicalTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion Physical test

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Eye test

        [HttpGet]
        public ActionResult CreateEyeTest(int id)
        {
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int DefaultHealthPeriodID = this.getDefaultHealthPeriod(id, _globalInfo.AcademicYearID.Value, "EyeTest").HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(id, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value, out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            ViewBag.PupilID = id;
            ViewBag.DefaultHealthPeriodID = DefaultHealthPeriodID;
            ViewBag.DefaultMonitoringDate = DefaultMonitoringDate;
            ViewBag.EnableToEditMonitoringDate = EnableToEditMonitoringDate;
            ViewBag.DefaultUnitName = DefaultUnitName;
            ViewBag.EnableToEditUnitName = EnableToEditUnitName;
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "create";
            return PartialView("__EyeTest");
        }
        
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateEyeTest(int PupilID,EyeTestViewModel eyeTestCreating)
        {
            ViewBag.Mode = "create";
            return SaveEyeTest(PupilID,eyeTestCreating);
        }

        [HttpGet]
        public ActionResult EditEyeTest(int id)
        {
            EyeTest eyeTest = this.EyeTestBusiness.Find(id);
            EyeTestViewModel model = new EyeTestViewModel()
            {
                EyeTestID = eyeTest.EyeTestID,
                PupilID = eyeTest.MonitoringBook.PupilID,
                MonitoringBookID = eyeTest.MonitoringBookID,
                HealthPeriodID = eyeTest.MonitoringBook.HealthPeriodID ?? 0,
                MonitoringDate = eyeTest.MonitoringBook.MonitoringDate.HasValue ? eyeTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                EnableToEditMonitoringDate = true,
                UnitName = eyeTest.MonitoringBook.UnitName,
                EnableToEditUnitName = true,
                IsAstigmatism = eyeTest.IsAstigmatism ?? false,
                IsFarsightedness = eyeTest.IsFarsightedness ?? false,
                IsMyopic = eyeTest.IsMyopic ?? false,
                IsTreatment = eyeTest.IsTreatment,
                LAstigmatism = eyeTest.LAstigmatism,
                LEye = eyeTest.LEye,
                REye = eyeTest.REye,
                LFarsightedness = eyeTest.LFarsightedness,
                LMyopic = eyeTest.LMyopic,
                RAstigmatism = eyeTest.RAstigmatism,
                RFarsightedness = eyeTest.RFarsightedness,
                RMyopic = eyeTest.RMyopic,
                Other = eyeTest.Other,
                CreateDate = eyeTest.CreatedDate,
            };
            List<HealthPeriod> ListHealthPeriod = this.HealthPeriodBusiness.All.Where(o => o.IsActive).OrderBy(p => p.HealthPeriodID).ToList();
            ViewBag.HealthPeriods = ListHealthPeriod;
            ViewBag.Mode = "update";
            return PartialView("__EyeTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdateEyeTest(int PupilID, EyeTestViewModel eyeTestUpdating)
        {
            ViewBag.Mode = "update";
            return SaveEyeTest(PupilID, eyeTestUpdating);
        }

         [HttpPost]
         [ValidateAntiForgeryToken]
        private ActionResult SaveEyeTest(int PupilID, EyeTestViewModel eyeTestSaving)
        {
            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) throw new SMASException("Invalid PupilID");
            // Kiểm tra ngày khám
            string MonitoringDateName = StaticReflection.GetMemberName<PhysicalTestViewModel>(p => p.MonitoringDate);
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (eyeTestSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
                else
                {
                    var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    if (eyeTestSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || eyeTestSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                    {
                        ModelState.AddModelError(MonitoringDateName
                            , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}", ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                    }
                }
            }

            if (eyeTestSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName], out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");
                    }
                }
            }

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                // Kiem tra xem hoc sinh da kham suc khoe trong dot nay chua, neu chua co thi tao ra
                monitoringBook = this.getMonitoringBookOfPupil(PupilID, eyeTestSaving.HealthPeriodID, _globalInfo.AcademicYearID.Value);
                if (monitoringBook == null)
                {
                    monitoringBook = new MonitoringBook();
                    monitoringBook.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    monitoringBook.SchoolID = _globalInfo.SchoolID.Value;
                    monitoringBook.ClassID = classOfPupil.ClassID;
                    monitoringBook.PupilID = PupilID;
                    monitoringBook.EducationLevelID = classOfPupil.ClassProfile.EducationLevelID;
                    monitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                    monitoringBook.HealthPeriodID = eyeTestSaving.HealthPeriodID;
                    monitoringBook.MonitoringDate = eyeTestSaving.MonitoringDate;
                    monitoringBook.UnitName = eyeTestSaving.UnitName;
                    monitoringBook.CreateDate = DateTime.Now;
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else
                {
                    if (monitoringBook.EyeTests.Where(p => p.EyeTestID != eyeTestSaving.EyeTestID).Count() > 0)
                    {
                        string HealthPeriodIDFieldName = StaticReflection.GetMemberName<EyeTestViewModel>(o => o.HealthPeriodID);
                        ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "mắt", eyeTestSaving.HealthPeriodID));
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = eyeTestSaving.MonitoringDate;
                        monitoringBook.UnitName = eyeTestSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }
               
            }
            if (ModelState.IsValid)
            {
                EyeTest eyeTest;
                if ((!eyeTestSaving.EyeTestID.HasValue || eyeTestSaving.EyeTestID <= 0))
                {

                    eyeTest = new EyeTest();
                    eyeTest.CreatedDate = DateTime.Now;
                }
                else
                {
                    eyeTest = this.EyeTestBusiness.Find(eyeTestSaving.EyeTestID);
                    if (eyeTest == null) throw new BusinessException("Lỗi dữ liệu");
                }
                eyeTest.MonitoringBook = monitoringBook;
                eyeTest.IsAstigmatism = eyeTestSaving.IsAstigmatism;
                eyeTest.IsFarsightedness = eyeTestSaving.IsFarsightedness;
                eyeTest.IsMyopic = eyeTestSaving.IsMyopic;
                eyeTest.IsTreatment = eyeTestSaving.IsTreatment;
                eyeTest.LAstigmatism = eyeTestSaving.LAstigmatism;
                eyeTest.LEye = eyeTestSaving.LEye ?? 0;
                eyeTest.LFarsightedness = eyeTestSaving.LFarsightedness;
                eyeTest.LMyopic = eyeTestSaving.LMyopic;
                eyeTest.Other = eyeTestSaving.Other;
                eyeTest.RAstigmatism = eyeTestSaving.RAstigmatism;
                eyeTest.REye = eyeTestSaving.REye ?? 0;
                eyeTest.RFarsightedness = eyeTestSaving.RFarsightedness;
                eyeTest.RMyopic = eyeTestSaving.RMyopic;
                eyeTest.ModifiedDate = DateTime.Now;
                eyeTest.IsActive = true;
                if (eyeTestSaving.EyeTestID.HasValue && eyeTestSaving.EyeTestID > 0)
                {
                    this.EyeTestBusiness.Update(eyeTest);
                }
                else
                {
                    this.EyeTestBusiness.Insert(eyeTest);
                }
                this.EyeTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám mắt thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            return PartialView("__EyeTest", eyeTestSaving);
        }

        [ValidateAntiForgeryToken]
        
        public JsonResult DeleteEyeTest(int id)
        {
            var deleted = this.EyeTestBusiness.All.FirstOrDefault(p => p.EyeTestID == id && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.EyeTestBusiness.DeleteAll(new List<EyeTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.EyeTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.ENTTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion Eye test

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region ENT test

        [HttpGet]
        public ActionResult CreateENTTest(int id)
        {
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int DefaultHealthPeriodID = this.getDefaultHealthPeriod(id, _globalInfo.AcademicYearID.Value, "ENTTest").HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(id, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value, out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            ViewBag.PupilID = id;
            ViewBag.DefaultHealthPeriodID = DefaultHealthPeriodID;
            ViewBag.DefaultMonitoringDate = DefaultMonitoringDate;
            ViewBag.EnableToEditMonitoringDate = EnableToEditMonitoringDate;
            ViewBag.DefaultUnitName = DefaultUnitName;
            ViewBag.EnableToEditUnitName = EnableToEditUnitName;
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "create";
            return PartialView("__ENTTest");
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateENTTest(ENTTestViewModel entTestCreating)
        {
            ViewBag.Mode = "create";
            return SaveENTTest(entTestCreating);
        }

        [HttpGet]
        public ActionResult EditENTTest(int id)
        {
            ENTTest entTest = this.ENTTestBusiness.Find(id);
            if (entTest == null) throw new BusinessException("Lỗi dữ liệu");
            ENTTestViewModel model = new ENTTestViewModel()
            {
                ENTTestID = entTest.ENTTestID,
                PupilID = entTest.MonitoringBook.PupilID,
                MonitoringBookID = entTest.MonitoringBookID,
                HealthPeriodID = entTest.MonitoringBook.HealthPeriodID ?? 0,
                MonitoringDate = entTest.MonitoringBook.MonitoringDate.HasValue ? entTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                UnitName = entTest.MonitoringBook.UnitName,
                IsDeaf = entTest.IsDeaf,
                LCapacityHearing = entTest.LCapacityHearing,
                RCapacityHearing = entTest.RCapacityHearing,
                IsSick = entTest.IsSick,
                Description = entTest.Description,
            };
            List<HealthPeriod> ListHealthPeriod = this.getHealthPeriods();
            ViewBag.HealthPeriods = ListHealthPeriod;
            ViewBag.Mode = "update";
            return PartialView("__ENTTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdateENTTest(ENTTestViewModel entTestUpdating)
        {
            ViewBag.Mode = "update";
            return SaveENTTest(entTestUpdating);
        }

        [HttpPost]        
        [ValidateAntiForgeryToken]
        private ActionResult SaveENTTest(ENTTestViewModel entTestSaving)
        {
            // Kiểm tra ngày khám
            string MonitoringDateName = StaticReflection.GetMemberName<ENTTestViewModel>(p => p.MonitoringDate);
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (entTestSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
                else
                {
                    var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    if (entTestSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || entTestSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                    {
                        ModelState.AddModelError(MonitoringDateName
                            , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}", ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                    }
                }
            }
            if (entTestSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName], out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");
                    }
                }
            }
            string DescriptionFieldName = StaticReflection.GetMemberName<ENTTestViewModel>(p => p.Description);
            if (ModelState.IsValidField(DescriptionFieldName))
            {
                if (entTestSaving.IsSick.Value) // Neu bi benh thi bat buoc nhap mo ta benh
                {
                    if (string.IsNullOrWhiteSpace(entTestSaving.Description))
                    {
                        ModelState.AddModelError(DescriptionFieldName, "Bệnh tai mũi họng không được để trống.");
                    }
                    else
                    {
                        entTestSaving.Description = entTestSaving.Description.Trim();
                    }
                }
            }
            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == entTestSaving.PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) { throw new BusinessException("Lỗi dữ liệu"); }

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                // Kiem tra xem hoc sinh da kham suc khoe trong dot nay chua, neu chua co thi tao ra
                monitoringBook = this.getMonitoringBookOfPupil(entTestSaving.PupilID, entTestSaving.HealthPeriodID, _globalInfo.AcademicYearID.Value);
                if (monitoringBook == null)
                {
                    monitoringBook = new MonitoringBook();
                    monitoringBook.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    monitoringBook.SchoolID = _globalInfo.SchoolID.Value;
                    monitoringBook.ClassID = classOfPupil.ClassID;
                    monitoringBook.PupilID = classOfPupil.PupilID;
                    monitoringBook.EducationLevelID = classOfPupil.ClassProfile.EducationLevelID;
                    monitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                    monitoringBook.HealthPeriodID = entTestSaving.HealthPeriodID;
                    monitoringBook.MonitoringDate = entTestSaving.MonitoringDate;
                    monitoringBook.UnitName = entTestSaving.UnitName;
                    monitoringBook.CreateDate = DateTime.Now;
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else
                {                    
                    if (monitoringBook.ENTTests.Count(p => p.ENTTestID != entTestSaving.ENTTestID) > 0)
                    {
                        string HealthPeriodIDFieldName = StaticReflection.GetMemberName<ENTTestViewModel>(o => o.HealthPeriodID);
                        ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "tai mũi họng", entTestSaving.HealthPeriodID));
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = entTestSaving.MonitoringDate;
                        monitoringBook.UnitName = entTestSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                ENTTest entTestNew;
                if ((!entTestSaving.ENTTestID.HasValue || entTestSaving.ENTTestID <= 0))
                {
                    entTestNew = new ENTTest();
                    entTestNew.CreatedDate = DateTime.Now;
                }
                else
                {
                    entTestNew = this.ENTTestBusiness.Find(entTestSaving.ENTTestID);
                    if (entTestNew == null) throw new BusinessException("Lỗi dữ liệu");
                }
                entTestNew.MonitoringBook = monitoringBook;
                entTestNew.IsDeaf = entTestSaving.IsDeaf;
                entTestNew.LCapacityHearing = entTestSaving.LCapacityHearing;
                entTestNew.RCapacityHearing = entTestSaving.RCapacityHearing;
                entTestNew.IsSick = entTestSaving.IsSick;
                entTestNew.Description = entTestSaving.Description;
                entTestNew.ModifiedDate = DateTime.Now;
                entTestNew.IsActive = true;
                if (entTestSaving.ENTTestID.HasValue && entTestSaving.ENTTestID > 0)
                {
                    this.ENTTestBusiness.Update(entTestNew);
                }
                else
                {
                    this.ENTTestBusiness.Insert(entTestNew);
                }
                this.ENTTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            return PartialView("__ENTTest", entTestSaving);
        }

        [ValidateAntiForgeryToken]
        
        public JsonResult DeleteENTTest(int ENTTestID)
        {
            var deleted = this.ENTTestBusiness.All.FirstOrDefault(p => p.ENTTestID == ENTTestID && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.ENTTestBusiness.DeleteAll(new List<ENTTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.ENTTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.ENTTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion ENT test

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Spine test

        [HttpGet]
        public ActionResult CreateSpineTest(int id)
        {
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int DefaultHealthPeriodID = this.getDefaultHealthPeriod(id, _globalInfo.AcademicYearID.Value, "SpineTest").HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(id, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value, out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            SpineTestViewModel defaultModel = new SpineTestViewModel
            {
                PupilID = id,
                HealthPeriodID = DefaultHealthPeriodID,
                MonitoringDate = DefaultMonitoringDate,
                EnableToEditMonitoringDate = EnableToEditMonitoringDate,
                UnitName = DefaultUnitName,
                EnableToEditUnitName = EnableToEditUnitName
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "create";
            return PartialView("__SpineTest", defaultModel);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateSpineTest(SpineTestViewModel spineTestCreating)
        {
            ViewBag.Mode = "create";
            return SaveSpineTest(spineTestCreating);
        }

        [HttpGet]
        public ActionResult EditSpineTest(int id)
        {
            SpineTest spineTest = this.SpineTestBusiness.Find(id);
            if (spineTest == null) throw new BusinessException("Lỗi dữ liệu");
            SpineTestViewModel model = new SpineTestViewModel()
            {
                SpineTestID = spineTest.SpineTestID,
                PupilID = spineTest.MonitoringBook.PupilID,
                MonitoringBookID = spineTest.MonitoringBookID,
                HealthPeriodID = spineTest.MonitoringBook.HealthPeriodID ?? 0,
                MonitoringDate = spineTest.MonitoringBook.MonitoringDate.HasValue ? spineTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                UnitName = spineTest.MonitoringBook.UnitName,
                ExaminationItalic = spineTest.ExaminationItalic,
                ExaminationStraight = spineTest.ExaminationStraight,
                IsDeformityOfTheSpine = spineTest.IsDeformityOfTheSpine,
                Other = spineTest.Other
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "update";
            return PartialView("__SpineTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdateSpineTest(SpineTestViewModel spineTestUpdating)
        {
            ViewBag.Mode = "update";
            return SaveSpineTest(spineTestUpdating);
        }

        private ActionResult SaveSpineTest(SpineTestViewModel spineTestSaving)
        {
            // Kiểm tra ngày khám
            string MonitoringDateName = StaticReflection.GetMemberName<SpineTestViewModel>(p => p.MonitoringDate);
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (spineTestSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
                else
                {
                    var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    if (spineTestSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || spineTestSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                    {
                        ModelState.AddModelError(MonitoringDateName
                            , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}", ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                    }
                }
            }
            if (spineTestSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName], out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");
                    }
                }
            }
            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == spineTestSaving.PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) throw new SMASException("Khong tim thay lop hoc cua hoc sinh. Ma hoc sinh khong hop le");

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                // Kiem tra xem hoc sinh da kham suc khoe trong dot nay chua, neu chua co thi tao ra
                monitoringBook = this.getMonitoringBookOfPupil(classOfPupil.PupilID, spineTestSaving.HealthPeriodID, _globalInfo.AcademicYearID.Value);
                if (monitoringBook == null)
                {
                    monitoringBook = new MonitoringBook();
                    monitoringBook.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    monitoringBook.SchoolID = _globalInfo.SchoolID.Value;
                    monitoringBook.ClassID = classOfPupil.ClassID;
                    monitoringBook.PupilID = classOfPupil.PupilID;
                    monitoringBook.EducationLevelID = classOfPupil.ClassProfile.EducationLevelID;
                    monitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                    monitoringBook.HealthPeriodID = spineTestSaving.HealthPeriodID;
                    monitoringBook.MonitoringDate = spineTestSaving.MonitoringDate;
                    monitoringBook.UnitName = spineTestSaving.UnitName;
                    monitoringBook.CreateDate = DateTime.Now;
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else
                {                    
                    if (monitoringBook.SpineTests.Count(p => p.SpineTestID != spineTestSaving.SpineTestID) > 0)
                    {
                        // Thong bao loi hoc sinh da co ket qua kham suc khoe trong dot nay
                        string HealthPeriodIDFieldName = StaticReflection.GetMemberName<SpineTestViewModel>(o => o.HealthPeriodID);
                        ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "cột sống", spineTestSaving.HealthPeriodID));
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = spineTestSaving.MonitoringDate;
                        monitoringBook.UnitName = spineTestSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }

            }
            if (ModelState.IsValid)
            {
                SpineTest spineTestNew;
                if (!spineTestSaving.SpineTestID.HasValue || spineTestSaving.SpineTestID <= 0)
                {
                    spineTestNew = new SpineTest();
                    spineTestNew.CreatedDate = DateTime.Now;
                }
                else
                {
                    spineTestNew = this.SpineTestBusiness.Find(spineTestSaving.SpineTestID);
                    if (spineTestNew == null) throw new BusinessException("Lỗi dữ liệu");
                }
                spineTestNew.MonitoringBook = monitoringBook;
                spineTestNew.ExaminationItalic = spineTestSaving.ExaminationItalic;
                spineTestNew.ExaminationStraight = spineTestSaving.ExaminationStraight;
                spineTestNew.IsDeformityOfTheSpine = spineTestSaving.IsDeformityOfTheSpine;
                spineTestNew.Other = spineTestSaving.Other;
                spineTestNew.ModifiedDate = DateTime.Now;
                spineTestNew.IsActive = true;
                if (spineTestSaving.SpineTestID.HasValue && spineTestSaving.SpineTestID > 0)
                {
                    this.SpineTestBusiness.Update(spineTestNew);
                }
                else
                {
                    this.SpineTestBusiness.Insert(spineTestNew);
                }
                this.SpineTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            return PartialView("__SpineTest", spineTestSaving);
        }


        [ValidateAntiForgeryToken]
        
        public JsonResult DeleteSpineTest(int SpineTestID)
        {
            var deleted = this.SpineTestBusiness.All.FirstOrDefault(p => p.SpineTestID == SpineTestID && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.SpineTestBusiness.DeleteAll(new List<SpineTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.SpineTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.SpineTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion Spine test

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Dental test

        [HttpGet]
        public ActionResult CreateDentalTest(int id)
        {
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int DefaultHealthPeriodID = this.getDefaultHealthPeriod(id, _globalInfo.AcademicYearID.Value, "DentalTest").HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(id, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value, out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            DentalTestViewModel defaultModel = new DentalTestViewModel
            {
                PupilID = id,
                HealthPeriodID = DefaultHealthPeriodID,
                MonitoringDate = DefaultMonitoringDate,
                EnableToEditMonitoringDate = EnableToEditMonitoringDate,
                UnitName = DefaultUnitName,
                EnableToEditUnitName = EnableToEditUnitName,
                NumDentalFilling = 0
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "create";
            return PartialView("__DentalTest", defaultModel);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateDentalTest(DentalTestViewModel dentalTestModelCreating)
        {
            ViewBag.Mode = "create";
            return SaveDentalTest(dentalTestModelCreating);
        }

        [HttpGet]
        public ActionResult EditDentalTest(int id)
        {
            DentalTest dentalTest = this.DentalTestBusiness.Find(id);
            if (dentalTest == null) throw new BusinessException("Lỗi dữ liệu");
            DentalTestViewModel model = new DentalTestViewModel()
            {
                DentalTestID = dentalTest.DentalTestID,
                PupilID = dentalTest.MonitoringBook.PupilID,
                MonitoringBookID = dentalTest.MonitoringBookID,
                HealthPeriodID = dentalTest.MonitoringBook.HealthPeriodID ?? 0,
                MonitoringDate = dentalTest.MonitoringBook.MonitoringDate.HasValue ? dentalTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                EnableToEditMonitoringDate = true,
                UnitName = dentalTest.MonitoringBook.UnitName,
                EnableToEditUnitName = true,
                DescriptionDentalFilling = dentalTest.DescriptionDentalFilling,
                DescriptionPreventiveDentalFilling = dentalTest.DescriptionPreventiveDentalFilling,
                DescriptionTeethExtracted = dentalTest.DescriptionTeethExtracted,
                IsScraptTeeth = dentalTest.IsScraptTeeth,
                IsTreatment = dentalTest.IsTreatment,
                NumDentalFilling = dentalTest.NumDentalFilling,
                NumDentalFillingFour = dentalTest.NumDentalFillingFour,
                NumDentalFillingOne = dentalTest.NumDentalFillingOne,
                NumDentalFillingThree = dentalTest.NumDentalFillingThree,
                NumDentalFillingTwo = dentalTest.NumDentalFillingTwo,
                NumPreventiveDentalFilling = dentalTest.NumPreventiveDentalFilling,
                NumTeethExtracted = dentalTest.NumTeethExtracted,
                Other = dentalTest.Other
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "update";
            return PartialView("__DentalTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdateDentalTest(DentalTestViewModel dentalTestModelUpdating)
        {
            ViewBag.Mode = "update";
            return SaveDentalTest(dentalTestModelUpdating);
        }
      
        private ActionResult SaveDentalTest(DentalTestViewModel dentalTestModelSaving)
        {
            // Kiểm tra ngày khám
            string MonitoringDateName = StaticReflection.GetMemberName<DentalTestViewModel>(p => p.MonitoringDate);
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (dentalTestModelSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
                else
                {
                    var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    if (dentalTestModelSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || dentalTestModelSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                    {
                        ModelState.AddModelError(MonitoringDateName
                            , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}", ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                    }
                }
            }
            if (dentalTestModelSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName], out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");
                    }
                }
            }

            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == dentalTestModelSaving.PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) throw new SMASException("Lỗi dữ liệu");

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                // Kiem tra xem hoc sinh da kham suc khoe trong dot nay chua, neu chua co thi tao ra  
                monitoringBook = this.getMonitoringBookOfPupil(classOfPupil.PupilID, dentalTestModelSaving.HealthPeriodID, _globalInfo.AcademicYearID.Value);
                if (monitoringBook == null)
                {
                    monitoringBook = new MonitoringBook();
                    monitoringBook.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    monitoringBook.SchoolID = _globalInfo.SchoolID.Value;
                    monitoringBook.ClassID = classOfPupil.ClassID;
                    monitoringBook.PupilID = classOfPupil.PupilID;
                    monitoringBook.EducationLevelID = classOfPupil.ClassProfile.EducationLevelID;
                    monitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                    monitoringBook.HealthPeriodID = dentalTestModelSaving.HealthPeriodID;
                    monitoringBook.MonitoringDate = dentalTestModelSaving.MonitoringDate;
                    monitoringBook.UnitName = dentalTestModelSaving.UnitName;
                    monitoringBook.CreateDate = DateTime.Now;
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else
                {
                    if (monitoringBook.DentalTests.Count(p => p.DentalTestID != dentalTestModelSaving.DentalTestID) > 0)
                    {
                        // Thong bao loi hoc sinh da co ket qua kham suc khoe trong dot nay
                        string HealthPeriodIDFieldName = StaticReflection.GetMemberName<DentalTestViewModel>(o => o.HealthPeriodID);
                        ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "răng", dentalTestModelSaving.HealthPeriodID));
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = dentalTestModelSaving.MonitoringDate;
                        monitoringBook.UnitName = dentalTestModelSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                DentalTest dentalTestSaving;
                if (!dentalTestModelSaving.DentalTestID.HasValue || dentalTestModelSaving.DentalTestID <= 0)
                {
                    dentalTestSaving = new DentalTest();
                    dentalTestSaving.CreatedDate = DateTime.Now;
                }
                else
                {
                    dentalTestSaving = this.DentalTestBusiness.Find(dentalTestModelSaving.DentalTestID);
                    if (dentalTestSaving == null) throw new BusinessException("Lỗi dữ liệu");
                }
                // Gan du lieu
                dentalTestSaving.MonitoringBook = monitoringBook;
                dentalTestSaving.DescriptionDentalFilling = dentalTestModelSaving.DescriptionDentalFilling;
                dentalTestSaving.DescriptionPreventiveDentalFilling = dentalTestModelSaving.DescriptionPreventiveDentalFilling;
                dentalTestSaving.DescriptionTeethExtracted = dentalTestModelSaving.DescriptionTeethExtracted;
                dentalTestSaving.IsScraptTeeth = dentalTestModelSaving.IsScraptTeeth;
                dentalTestSaving.IsTreatment = dentalTestModelSaving.IsTreatment;
                dentalTestSaving.NumDentalFilling = dentalTestModelSaving.NumDentalFilling ?? 0;
                dentalTestSaving.NumDentalFillingFour = dentalTestModelSaving.NumDentalFillingFour;
                dentalTestSaving.NumDentalFillingOne = dentalTestModelSaving.NumDentalFillingOne;
                dentalTestSaving.NumDentalFillingThree = dentalTestModelSaving.NumDentalFillingThree;
                dentalTestSaving.NumDentalFillingTwo = dentalTestModelSaving.NumDentalFillingTwo;
                dentalTestSaving.NumPreventiveDentalFilling = dentalTestModelSaving.NumPreventiveDentalFilling;
                dentalTestSaving.NumTeethExtracted = dentalTestModelSaving.NumTeethExtracted;
                dentalTestSaving.Other = dentalTestModelSaving.Other;
                dentalTestSaving.ModifiedDate = DateTime.Now;
                dentalTestSaving.IsActive = true;
                // Luu du lieu
                if (dentalTestModelSaving.DentalTestID.HasValue && dentalTestModelSaving.DentalTestID > 0)
                {
                    this.DentalTestBusiness.Update(dentalTestSaving);
                }
                else
                {
                    this.DentalTestBusiness.Insert(dentalTestSaving);
                }
                this.DentalTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám răng thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            return PartialView("__DentalTest", dentalTestModelSaving);
        }


        [ValidateAntiForgeryToken]
        
        public JsonResult DeleteDentalTest(int id)
        {
            var deleted = this.DentalTestBusiness.All.FirstOrDefault(p => p.DentalTestID == id && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.DentalTestBusiness.DeleteAll(new List<DentalTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.DentalTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.DentalTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion Dental test


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Overall test

        [ValidateAntiForgeryToken]
        public ActionResult LoadOverallTest(int id, int classID)
        {
            //Hiển thị link xóa, sửa: Khi DateTime.Now – lstOverallTest[i].CreatDate <= 2
            GlobalInfo glo = GlobalInfo.getInstance();
            List<OverallTestBO> listOverallTest = OverallTestBusiness.SearchByPupil(glo.AcademicYearID.Value, glo.SchoolID.Value, id, classID).ToList();
            listOverallTest.ForEach(u => { u.EnableDeleteOrEdit = u.CreatedDate.HasValue && u.CreatedDate.Value.AddDays(2) >= DateTime.Now; });
            listOverallTest = listOverallTest.OrderByDescending(p => p.CreatedDate).ToList();
            ViewData[HealthTestConstants.LIST_OVERALLTEST] = listOverallTest;
            ViewBag.PupilProfileID = id;
            GetPermission();
            return PartialView("_gridOverallTest");
        }

        /// <summary>
        /// Tao ban ghi Kham tong quat cho hoc sinh co ma la id
        /// </summary>
        /// <param name="id">ID cua hoc sinh (pupilProfileID)</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateOverallTest(int id)
        {
            int pupilProfileID = id;
            DateTime DefaultMonitoringDate;
            bool EnableToEditMonitoringDate;
            string DefaultUnitName;
            bool EnableToEditUnitName;
            int DefaultHealthPeriodID = this.getDefaultHealthPeriod(pupilProfileID, _globalInfo.AcademicYearID.Value, "OverallTest").HealthPeriodID;
            this.getDefaultMonitoringDateAndDefaultUnitName(pupilProfileID, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value, out DefaultMonitoringDate, out EnableToEditMonitoringDate, out DefaultUnitName, out EnableToEditUnitName);
            string DefaultDoctorName = this.getDefaultDoctorName(pupilProfileID, DefaultHealthPeriodID, _globalInfo.AcademicYearID.Value);
            string DefaultHistoryYourSelf = this.getDefaultHistoryYourSelf(pupilProfileID);
            OverallTestViewModel defaultModel = new OverallTestViewModel
            {
                PupilID = pupilProfileID,
                HealthPeriodID = DefaultHealthPeriodID,
                MonitoringDate = DefaultMonitoringDate,
                EnableToEditMonitoringDate = EnableToEditMonitoringDate,
                UnitName = DefaultUnitName,
                EnableToEditUnitName = EnableToEditUnitName,
                Doctor = DefaultDoctorName,
                HistoryYourSelf = DefaultHistoryYourSelf
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "create";
            return PartialView("__OverallTest", defaultModel);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateOverallTest(OverallTestViewModel overallTestModelCreating)
        {
            ViewBag.Mode = "create";
            return SaveOverallTest(overallTestModelCreating);
        }

        [HttpGet]
        public ActionResult EditOverallTest(int id)
        {
            OverallTest dentalTest = this.OverallTestBusiness.Find(id);
            if (dentalTest == null) throw new BusinessException("Lỗi dữ liệu");
            OverallTestViewModel model = new OverallTestViewModel()
            {
                OverallTestID = dentalTest.OverallTestID,
                PupilID = dentalTest.MonitoringBook.PupilID,
                MonitoringBookID = dentalTest.MonitoringBookID,
                HealthPeriodID = dentalTest.MonitoringBook.HealthPeriodID ?? 0,
                
                MonitoringDate = dentalTest.MonitoringBook.MonitoringDate.HasValue ? dentalTest.MonitoringBook.MonitoringDate.Value : DateTime.Now,
                EnableToEditMonitoringDate = true,

                UnitName = dentalTest.MonitoringBook.UnitName,
                EnableToEditUnitName = true,

                HistoryYourSelf = dentalTest.HistoryYourSelf, // Tien su ban than

                OverallInternal = dentalTest.OverallInternal, // Noi tong quat
                DescriptionOverallInternal = dentalTest.DescriptionOverallInternall, // Mo ta noi tong quat

                OverallForeign = dentalTest.OverallForeign, // Ngoai tong quat
                DescriptionOverallForeign = dentalTest.DescriptionForeign, // Mo ta ngoai tong quat

                SkinDiseases = dentalTest.SkinDiseases, // Benh ngoai da
                DescriptionSkinDiseases = dentalTest.DescriptionSkinDiseases, // Mo ta ngoai tong quat

                IsHeartDiseases = dentalTest.IsHeartDiseases, // Tim bam sinh
                IsBirthDefect = dentalTest.IsBirthDefect, // Di tat bam sinh

                Doctor = dentalTest.Doctor,
                EvaluationHealth = dentalTest.EvaluationHealth,

                IsDredging = dentalTest.IsDredging,

                Other = dentalTest.Other,

                RequireOfDoctor = dentalTest.RequireOfDoctor,
            };
            ViewBag.HealthPeriods = this.getHealthPeriods();
            ViewBag.Mode = "update";
            return PartialView("__OverallTest", model);
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        
        public ActionResult UpdateOverallTest(OverallTestViewModel overallTestModelUpdating)
        {
            ViewBag.Mode = "update";
            return SaveOverallTest(overallTestModelUpdating);
        }

        private ActionResult SaveOverallTest(OverallTestViewModel overallTestModelSaving)
        {
            // Kiểm tra ngày khám
            string MonitoringDateName = StaticReflection.GetMemberName<OverallTestViewModel>(p => p.MonitoringDate);
            if (ModelState.IsValidField(MonitoringDateName))
            {
                if (overallTestModelSaving.MonitoringDate.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(MonitoringDateName, "Ngày khám phải trước ngày hiện tại");
                }
                else
                {
                    var ay = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    if (overallTestModelSaving.MonitoringDate.Date < ay.FirstSemesterStartDate.Value.Date || overallTestModelSaving.MonitoringDate.Date > ay.SecondSemesterEndDate.Value.Date)
                    {
                        ModelState.AddModelError(MonitoringDateName
                            , string.Format("Ngày khám phải trong khoảng thời gian năm học {0}–{1}", ay.FirstSemesterStartDate.Value.ToShortDateString(), ay.SecondSemesterEndDate.Value.ToShortDateString()));
                    }
                }
            }
            if (overallTestModelSaving.MonitoringDate.ToString().Trim().Length > 0)
            {
                DateTime temp;
                if (!DateTime.TryParse(this.ControllerContext.HttpContext.Request.Form[MonitoringDateName], out temp))
                {
                    if (ModelState[MonitoringDateName] != null && ModelState[MonitoringDateName].Errors.Count > 0)
                    {
                        ModelState[MonitoringDateName].Errors.Clear();
                        ModelState.AddModelError(MonitoringDateName, "Ngày khám không hợp lệ.");
                    }
                }
            }
            // Kiem tra Noi tong quat
            string DescriptionOverallInternalFiledName = StaticReflection.GetMemberName<OverallTestViewModel>(p => p.DescriptionOverallInternal);
            if (ModelState.IsValidField(DescriptionOverallInternalFiledName))
            {
                if (overallTestModelSaving.OverallInternal == 1 && string.IsNullOrWhiteSpace(overallTestModelSaving.DescriptionOverallInternal))
                {
                    ModelState.AddModelError(DescriptionOverallInternalFiledName, "Chưa nhập mô tả bệnh nội tổng quát");
                }
            }
            // Kiem tra Ngoai tong quat
            string DescriptionOverallForeignFiledName = StaticReflection.GetMemberName<OverallTestViewModel>(p => p.DescriptionOverallForeign);
            if (ModelState.IsValidField(DescriptionOverallForeignFiledName))
            {
                if (overallTestModelSaving.OverallForeign == 1 && string.IsNullOrWhiteSpace(overallTestModelSaving.DescriptionOverallForeign))
                {
                    ModelState.AddModelError(DescriptionOverallForeignFiledName, "Chưa nhập mô tả bệnh ngoại tổng quát");
                }
            }
            // Kiem tra Benh ngoai da
            string DescriptionSkinDiseasesFiledName = StaticReflection.GetMemberName<OverallTestViewModel>(p => p.DescriptionSkinDiseases);
            if (ModelState.IsValidField(DescriptionSkinDiseasesFiledName))
            {
                if (overallTestModelSaving.SkinDiseases == 1 && string.IsNullOrWhiteSpace(overallTestModelSaving.DescriptionSkinDiseases))
                {
                    ModelState.AddModelError(DescriptionSkinDiseasesFiledName, "Chưa nhập mô tả bệnh ngoài da");
                }
            }

            var classOfPupil = this.PupilOfClassBusiness.All.FirstOrDefault(pc => pc.PupilID == overallTestModelSaving.PupilID && pc.AcademicYearID == _globalInfo.AcademicYearID);
            if (classOfPupil == null) throw new BusinessException("Lỗi dữ liệu");

            MonitoringBook monitoringBook = null;
            if (ModelState.IsValid)
            {
                // Kiem tra xem hoc sinh da co So kham suc khoe chua. Neu chua co thi tao ra
                monitoringBook = this.MonitoringBookBusiness.All.FirstOrDefault(mb => mb.PupilID == overallTestModelSaving.PupilID && mb.HealthPeriodID == overallTestModelSaving.HealthPeriodID && mb.AcademicYearID == _globalInfo.AcademicYearID.Value);
                if (monitoringBook == null)
                {
                    monitoringBook = new MonitoringBook();
                    monitoringBook.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    monitoringBook.SchoolID = _globalInfo.SchoolID.Value;
                    monitoringBook.ClassID = classOfPupil.ClassID;
                    monitoringBook.PupilID = classOfPupil.PupilID;
                    monitoringBook.EducationLevelID = classOfPupil.ClassProfile.EducationLevelID;
                    monitoringBook.HealthPeriodID = overallTestModelSaving.HealthPeriodID;
                    monitoringBook.MonitoringType = SystemParamsInFile.TYPE_PERIOD;
                    monitoringBook.MonitoringDate = overallTestModelSaving.MonitoringDate;
                    monitoringBook.UnitName = overallTestModelSaving.UnitName;
                    monitoringBook.CreateDate = DateTime.Now;
                    this.MonitoringBookBusiness.Insert(monitoringBook);
                }
                else
                {
                    if (monitoringBook.OverallTests.Count(p => p.OverallTestID != overallTestModelSaving.OverallTestID) > 0)
                    {
                        // Thong bao loi hoc sinh da co ket qua kham suc khoe trong dot nay
                        string HealthPeriodIDFieldName = StaticReflection.GetMemberName<OverallTestViewModel>(o => o.HealthPeriodID);
                        ModelState.AddModelError(HealthPeriodIDFieldName, string.Format("Học sinh {0} đã có kết quả khám {1} đợt {2}.", classOfPupil.PupilProfile.FullName, "tổng quát", overallTestModelSaving.HealthPeriodID));
                    }
                    else
                    {
                        monitoringBook.MonitoringDate = overallTestModelSaving.MonitoringDate;
                        monitoringBook.UnitName = overallTestModelSaving.UnitName;
                        this.MonitoringBookBusiness.Update(monitoringBook);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                OverallTest overallTestSaving;
                // Tim du lieu
                if (!overallTestModelSaving.OverallTestID.HasValue || overallTestModelSaving.OverallTestID <= 0)
                {
                    overallTestSaving = new OverallTest();
                    overallTestSaving.CreatedDate = DateTime.Now;
                }
                else
                {
                    overallTestSaving = this.OverallTestBusiness.Find(overallTestModelSaving.OverallTestID);
                    if (overallTestSaving == null) throw new BusinessException("Lỗi dữ liệu");
                }
                // gan du lieu
                overallTestSaving.MonitoringBook = monitoringBook;
                overallTestSaving.DescriptionForeign = overallTestModelSaving.DescriptionOverallForeign;
                overallTestSaving.DescriptionOverallInternall = overallTestModelSaving.DescriptionOverallInternal;
                overallTestSaving.DescriptionSkinDiseases = overallTestModelSaving.DescriptionSkinDiseases;
                overallTestSaving.Doctor = overallTestModelSaving.Doctor;
                overallTestSaving.EvaluationHealth = overallTestModelSaving.EvaluationHealth;
                overallTestSaving.HistoryYourSelf = overallTestModelSaving.HistoryYourSelf;
                overallTestSaving.IsBirthDefect = overallTestModelSaving.IsBirthDefect;
                overallTestSaving.IsDredging = overallTestModelSaving.IsDredging;
                overallTestSaving.IsHeartDiseases = overallTestModelSaving.IsHeartDiseases;
                overallTestSaving.Other = overallTestModelSaving.Other;
                overallTestSaving.OverallForeign = overallTestModelSaving.OverallForeign;
                overallTestSaving.OverallInternal = overallTestModelSaving.OverallInternal;
                overallTestSaving.RequireOfDoctor = overallTestModelSaving.RequireOfDoctor;
                overallTestSaving.SkinDiseases = overallTestModelSaving.SkinDiseases;                
                overallTestSaving.ModifiedDate = DateTime.Now;
                overallTestSaving.IsActive = true;
                // Luu du lieu
                if (overallTestModelSaving.OverallTestID.HasValue && overallTestModelSaving.OverallTestID > 0)
                {
                    this.OverallTestBusiness.Update(overallTestSaving);
                }
                else
                {
                    this.OverallTestBusiness.Insert(overallTestSaving);

                }
                this.OverallTestBusiness.Save();
                ViewBag.Message = "Lưu kết quả khám tổng quát thành công";
            }
            ViewBag.HealthPeriods = this.getHealthPeriods();
            return PartialView("__OverallTest", overallTestModelSaving);
        }

        
        [ValidateAntiForgeryToken]
        public ActionResult DeleteOverallTest(int id)
        {
            var deleted = this.OverallTestBusiness.All.FirstOrDefault(p => p.OverallTestID == id && p.MonitoringBook.SchoolID == _globalInfo.SchoolID);
            if (deleted != null)
            {
                var monitoringBook = deleted.MonitoringBook;
                this.OverallTestBusiness.DeleteAll(new List<OverallTest>() { deleted }); // Xoa ban ghi kham vat ly
                monitoringBook.OverallTests.Remove(deleted);
                if (monitoringBook.PhysicalTests.Count == 0 && monitoringBook.ENTTests.Count == 0 && monitoringBook.DentalTests.Count == 0
                && monitoringBook.SpineTests.Count == 0 && monitoringBook.OverallTests.Count == 0 && monitoringBook.EyeTests.Count == 0)
                {
                    this.MonitoringBookBusiness.DeleteAll(new List<MonitoringBook>() { monitoringBook }); // Xoa ban ghi monitoring book
                }
                this.OverallTestBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage("Lỗi dữ liệu"));
            }
        }

        #endregion Overall test
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void GetViewData()
        {
            //cboHealthPeriod:
            List<HealthPeriod> ListHealthPeriod = this.getHealthPeriods();
            ViewData[HealthTestConstants.LISTHEALTHPERIOD] = new SelectList(ListHealthPeriod, "HealthPeriodID", "Resolution");
            ViewData[HealthTestConstants.LIST_EVALUATION_HEALTH] = new SelectList(CommonList.EvaluationHealth(), "key", "value");
            ViewData[HealthTestConstants.LIST_HEALTH_STATE] = CommonList.HealthState().Select(u => new ViettelCheckboxList { Label = u.value, Value = u.key, disabled = false, cchecked = false }).ToList();
            ViewData[HealthTestConstants.LIS_PHYSICALCLASSIFICATION] = new SelectList(CommonList.PhysicalClassification(), "key", "value");
            ViewData[HealthTestConstants.LIST_MONITORINGBOOK] = null;
            ViewData[HealthTestConstants.EYETEST] = new EyeTestViewModel();
        }

        private void GetGeneralData(int id, int classID)
        {
            GlobalInfo global = new GlobalInfo();
            /*
             * cboHealthPeriod: CommonList.HealthPeriod. Nếu trong năm học học sinh chưa khám sức khỏe thì mặc định là [Đợt 1].
             * Nếu học sinh đã khám trong năm học thì giá trị mặc định là [Đợt 2].
             * Kiểm tra bằng cách gọi hàm MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
             * với Dictionary[“AcademicYearID”] =UserInfo.AcademicYearID, Dictionary[“PupilID”] = PupilID truyền lên, Dictionary[“MonitoringType”] = TYPE_PERIOD (2)
             */
            List<HealthPeriod> ListHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["AcademicYearID"] = global.AcademicYearID;
            dictionary["PupilID"] = id;
            dictionary["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
            List<MonitoringBook> listMonitoringBook = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dictionary).ToList();
            List<SelectListItem> selectlistHealthPeriod = new List<SelectListItem>();
            int healthPeriodActive = SystemParamsInFile.HEALTH_PERIOD_ONE;
            if (listMonitoringBook != null && listMonitoringBook.Count > 0)
            {
                //Nếu có bản ghi thì active đợt 2 và disable ngày khám và đơn vị khám
                healthPeriodActive = SystemParamsInFile.HEALTH_PERIOD_TWO;
                ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = false;
                ViewData[HealthTestConstants.ENABLE_UNITNAME] = false;
                ViewData[HealthTestConstants.MONITORINGDATE] = DateTime.Now;
                ViewData[HealthTestConstants.UNITNAME] = "";

                //gán giá trị cho ngày khám và đơn vị khám
                MonitoringBook mtb = listMonitoringBook.Where(o => o.HealthPeriodID == healthPeriodActive).FirstOrDefault();
                if (mtb != null)
                {
                    ViewData[HealthTestConstants.MONITORINGDATE] = mtb.MonitoringDate;
                    ViewData[HealthTestConstants.UNITNAME] = mtb.UnitName;
                }
                else
                {
                    goto Nextstep;
                }
            }
            else
            {
                //Nếu không có bản ghi thì enable ngày khám và đơn vị khám
                healthPeriodActive = SystemParamsInFile.HEALTH_PERIOD_ONE;
                ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = true;
                ViewData[HealthTestConstants.ENABLE_UNITNAME] = true;
                goto Nextstep;
            }
        Nextstep:
            {

                //gán giá trị cho ngày khám và đơn vị khám
                /*
                 Ta có: MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“PupilID”] = PupilID truyền lên,
                 * Dictionary[“MonitoringType”] = TYPE_PERIOD (2), Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được MonitoringBook.Date_A = MonitoringBook.MonitoringDate. UnitName_A = MonitoringBook.UnitName
                 */
                IDictionary<string, object> dic_pupilid = new Dictionary<string, object>();
                dic_pupilid["AcademicYearID"] = global.AcademicYearID;
                dic_pupilid["PupilID"] = id;
                dic_pupilid["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_pupilid["HealthPeriodID"] = healthPeriodActive;
                MonitoringBook monitoringBookByPupil = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_pupilid).ToList().FirstOrDefault();
                DateTime? Date_A = null;
                if (monitoringBookByPupil == null)
                {
                    Date_A = monitoringBookByPupil.MonitoringDate;
                }
                //DateTime? Date_A = monitoringBookByPupil != null ? monitoringBookByPupil.MonitoringDate : null;
                string UnitName_A = monitoringBookByPupil != null ? monitoringBookByPupil.UnitName : null;
                /*
                 Ta có:  MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“ClassID”] = ClassID truyền lên,
                 * Dictionary[“MonitoringType”] = TYPE_PERIOD (2), Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được lstMonitoringBook. Date_B = Giá trị max trong lstMonitoringBook.
                 * UnitName_B = UnitName lấy trong lstMonitoringBook theo Date_B
                 */
                IDictionary<string, object> dic_classid = new Dictionary<string, object>();
                dic_classid["AcademicYearID"] = global.AcademicYearID;
                dic_classid["ClassID"] = classID;
                dic_classid["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_classid["HealthPeriodID"] = healthPeriodActive;
                DateTime? Date_B = null;
                string UnitName_B = string.Empty;
                List<MonitoringBook> lstMonitoringBookByClass = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_classid).ToList();
                if (lstMonitoringBookByClass != null && lstMonitoringBookByClass.Count > 0)
                {
                    Date_B = lstMonitoringBookByClass.Select(o => o.MonitoringDate).Max();
                    UnitName_B = lstMonitoringBookByClass.Where(o => o.MonitoringDate == Date_B).FirstOrDefault().UnitName;
                }
                /*
                 * Ta có:  MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“MonitoringType”] = TYPE_PERIOD (2),
                 * Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được lstMonitoringBook. Date_C = Giá trị max trong lstMonitoringBook.
                 * UnitName_C = UnitName lấy trong lstMonitoringBook theo Date_C
                 */
                IDictionary<string, object> dic_school = new Dictionary<string, object>();
                dic_school["AcademicYearID"] = global.AcademicYearID;
                dic_school["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_school["HealthPeriodID"] = healthPeriodActive;
                List<MonitoringBook> lstMonitoringBookBySchool = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_school).ToList();
                DateTime? Date_C = null;
                string UnitName_C = string.Empty;
                if (lstMonitoringBookBySchool != null && lstMonitoringBookBySchool.Count > 0)
                {
                    Date_C = lstMonitoringBookBySchool.Select(o => o.MonitoringDate).Max();
                    UnitName_C = lstMonitoringBookBySchool.Where(o => o.MonitoringDate == Date_C).FirstOrDefault().UnitName;
                }
                /*
                Nếu A <> null: dtpMonitoringDate = A và dtpMonitoringDate: Disable.
                Nếu A = null và B <> null: dtpMonitoringDate = B và dtpMonitoringDate: Enable.
                Nếu A= null, B= null, C <> null: dtpMonitoringDate = C và dtpMonitoringDate: Enable
                Nếu A = B = C = null: dtpMonitoringDate = DateTime.Now.
                 */
                if (Date_A.HasValue)
                {
                    ViewData[HealthTestConstants.MONITORINGDATE] = Date_A.Value;
                    ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = false;
                    ViewData[HealthTestConstants.UNITNAME] = UnitName_A;
                    ViewData[HealthTestConstants.ENABLE_UNITNAME] = false;
                }
                if (!Date_A.HasValue && Date_B.HasValue)
                {
                    ViewData[HealthTestConstants.MONITORINGDATE] = Date_B.Value;
                    ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = true;
                    ViewData[HealthTestConstants.UNITNAME] = UnitName_B;
                    ViewData[HealthTestConstants.ENABLE_UNITNAME] = true;
                }
                if (!Date_A.HasValue && !Date_B.HasValue && Date_C.HasValue)
                {
                    ViewData[HealthTestConstants.MONITORINGDATE] = Date_C.Value;
                    ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = true;
                    ViewData[HealthTestConstants.UNITNAME] = UnitName_C;
                    ViewData[HealthTestConstants.ENABLE_UNITNAME] = true;
                }
                if (!Date_A.HasValue && !Date_B.HasValue && !Date_C.HasValue)
                {
                    ViewData[HealthTestConstants.MONITORINGDATE] = DateTime.Now;
                    ViewData[HealthTestConstants.ENABLE_MONITORINGDATE] = true;
                }
            }
            foreach (var healthPeriod in ListHealthPeriod)
            {
                if (healthPeriod.HealthPeriodID == healthPeriodActive)
                {
                    selectlistHealthPeriod.Add(new SelectListItem { Text = healthPeriod.Resolution, Value = healthPeriod.HealthPeriodID.ToString(), Selected = true });
                }
                else
                {
                    selectlistHealthPeriod.Add(new SelectListItem { Text = healthPeriod.Resolution, Value = healthPeriod.HealthPeriodID.ToString(), Selected = false });
                }
            }
            ViewData[HealthTestConstants.LISTHEALTHPERIOD] = selectlistHealthPeriod;
            ViewData[HealthTestConstants.HEALTHPERIODACTIVE] = healthPeriodActive;
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetDataToHealthPeriod(int classid, int id, int HealthPeriodID)
        {
            GlobalInfo global = new GlobalInfo();
            /*
             * cboHealthPeriod: CommonList.HealthPeriod. Nếu trong năm học học sinh chưa khám sức khỏe thì mặc định là [Đợt 1].
             * Nếu học sinh đã khám trong năm học thì giá trị mặc định là [Đợt 2].
             * Kiểm tra bằng cách gọi hàm MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
             * với Dictionary[“AcademicYearID”] =UserInfo.AcademicYearID, Dictionary[“PupilID”] = PupilID truyền lên, Dictionary[“MonitoringType”] = TYPE_PERIOD (2)
             */
            bool ENABLE_MONITORINGDATE = true;
            bool ENABLE_UNITNAME = true;
            DateTime MONITORINGDATE = DateTime.Now;
            String UNITNAME = "";

            List<HealthPeriod> ListHealthPeriod = HealthPeriodBusiness.All.Where(o => o.IsActive == true).ToList();
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["AcademicYearID"] = global.AcademicYearID;
            dictionary["PupilID"] = id;
            dictionary["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
            dictionary["HealthPeriodID"] = HealthPeriodID;
            List<MonitoringBook> listMonitoringBook = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dictionary).ToList();
            List<SelectListItem> selectlistHealthPeriod = new List<SelectListItem>();
            if (listMonitoringBook != null && listMonitoringBook.Count > 0)
            {
                //Nếu có bản ghi thì active đợt 2 và disable ngày khám và đơn vị khám
                ENABLE_MONITORINGDATE = false;
                ENABLE_UNITNAME = false;

                //gán giá trị cho ngày khám và đơn vị khám
                MonitoringBook mtb = listMonitoringBook.FirstOrDefault();
                if (mtb != null)
                {
                    MONITORINGDATE = mtb.MonitoringDate.HasValue ?  mtb.MonitoringDate.Value : DateTime.Now;
                    UNITNAME = mtb.UnitName;
                }
            }
            else
            {
                //Nếu không có bản ghi thì enable ngày khám và đơn vị khám
                ENABLE_MONITORINGDATE = true;
                ENABLE_UNITNAME = true;

                //gán giá trị cho ngày khám và đơn vị khám

                /*
                 Ta có: MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“PupilID”] = PupilID truyền lên,
                 * Dictionary[“MonitoringType”] = TYPE_PERIOD (2), Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được MonitoringBook.Date_A = MonitoringBook.MonitoringDate. UnitName_A = MonitoringBook.UnitName
                 */
                IDictionary<string, object> dic_pupilid = new Dictionary<string, object>();
                dic_pupilid["AcademicYearID"] = global.AcademicYearID;
                dic_pupilid["PupilID"] = id;
                dic_pupilid["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_pupilid["HealthPeriodID"] = HealthPeriodID;
                MonitoringBook monitoringBookByPupil = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_pupilid).ToList().FirstOrDefault();
                DateTime? Date_A = null;
                if (monitoringBookByPupil != null)
                {
                    Date_A = monitoringBookByPupil.MonitoringDate;
                }
                string UnitName_A = monitoringBookByPupil != null ? monitoringBookByPupil.UnitName : null;
                /*
                 Ta có:  MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“ClassID”] = ClassID truyền lên,
                 * Dictionary[“MonitoringType”] = TYPE_PERIOD (2), Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được lstMonitoringBook. Date_B = Giá trị max trong lstMonitoringBook.
                 * UnitName_B = UnitName lấy trong lstMonitoringBook theo Date_B
                 */
                IDictionary<string, object> dic_classid = new Dictionary<string, object>();
                dic_classid["AcademicYearID"] = global.AcademicYearID;
                dic_classid["ClassID"] = classid;
                dic_classid["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_classid["HealthPeriodID"] = HealthPeriodID;
                DateTime? Date_B = null;
                string UnitName_B = string.Empty;
                List<MonitoringBook> lstMonitoringBookByClass = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_classid).ToList();
                if (lstMonitoringBookByClass != null && lstMonitoringBookByClass.Count > 0)
                {
                    Date_B = lstMonitoringBookByClass.Select(o => o.MonitoringDate).Max();
                    UnitName_B = lstMonitoringBookByClass.Where(o => o.MonitoringDate == Date_B).FirstOrDefault().UnitName;
                }
                /*
                 * Ta có:  MonitoringBookBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                 * với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID, Dictionary[“MonitoringType”] = TYPE_PERIOD (2),
                 * Dictionary[“HealthPeriodID”] = x (Tùy thuộc vào cboHealthPeriod).
                 * Kết quả thu được lstMonitoringBook. Date_C = Giá trị max trong lstMonitoringBook.
                 * UnitName_C = UnitName lấy trong lstMonitoringBook theo Date_C
                 */
                IDictionary<string, object> dic_school = new Dictionary<string, object>();
                dic_school["AcademicYearID"] = global.AcademicYearID;
                dic_school["MonitoringType"] = SystemParamsInFile.TYPE_PERIOD;
                dic_school["HealthPeriodID"] = HealthPeriodID;
                List<MonitoringBook> lstMonitoringBookBySchool = MonitoringBookBusiness.SearchBySchool(global.SchoolID.Value, dic_school).ToList();
                DateTime? Date_C = null;
                string UnitName_C = string.Empty;
                if (lstMonitoringBookBySchool != null && lstMonitoringBookBySchool.Count > 0)
                {
                    Date_C = lstMonitoringBookBySchool.Select(o => o.MonitoringDate).Max();
                    UnitName_C = lstMonitoringBookBySchool.Where(o => o.MonitoringDate == Date_C).FirstOrDefault().UnitName;
                }
                /*
                Nếu A <> null: dtpMonitoringDate = A và dtpMonitoringDate: Disable.
                Nếu A = null và B <> null: dtpMonitoringDate = B và dtpMonitoringDate: Enable.
                Nếu A= null, B= null, C <> null: dtpMonitoringDate = C và dtpMonitoringDate: Enable
                Nếu A = B = C = null: dtpMonitoringDate = DateTime.Now.
                 */
                if (Date_A.HasValue)
                {
                    MONITORINGDATE = Date_A.Value;
                    ENABLE_MONITORINGDATE = false;
                    UNITNAME = UnitName_A;
                    ENABLE_UNITNAME = false;
                }
                if (!Date_A.HasValue && Date_B.HasValue)
                {
                    MONITORINGDATE = Date_B.Value;
                    ENABLE_MONITORINGDATE = true;
                    UNITNAME = UnitName_B;
                    ENABLE_UNITNAME = true;
                }
                if (!Date_A.HasValue && !Date_B.HasValue && Date_C.HasValue)
                {
                    MONITORINGDATE = Date_C.Value;
                    ENABLE_MONITORINGDATE = true;
                    UNITNAME = UnitName_C;
                    ENABLE_UNITNAME = true;
                }
                if (!Date_A.HasValue && !Date_B.HasValue && !Date_C.HasValue)
                {
                    MONITORINGDATE = DateTime.Now;
                    ENABLE_MONITORINGDATE = true;
                }
            }
            return Json(new { MONITORINGDATE = MONITORINGDATE.ToString("dd/MM/yyyy"), ENABLE_MONITORINGDATE = ENABLE_MONITORINGDATE, UNITNAME = UNITNAME, ENABLE_UNITNAME = ENABLE_UNITNAME });
        }

        #region CHART


        [ValidateAntiForgeryToken]
        public ActionResult ViewChart(int id, int classID)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(id);
            ClassProfile classProfile = ClassProfileBusiness.Find(classID);

            if (pupil != null)
            {
                string historyYourSelf = pupil.MonitoringBooks.SelectMany(u => u.OverallTests).OrderByDescending(u => u.ModifiedDate).Select(u => u.HistoryYourSelf).FirstOrDefault();

                ViewData[HealthTestConstants.COMMON_VIEW_MODEL] = new CommonViewModel
                {
                    PupilID = pupil.PupilProfileID,
                    BirthDate = pupil.BirthDate,
                    ClassID = classProfile.ClassProfileID,
                    ClassName = classProfile.DisplayName,
                    FullName = pupil.FullName,
                    Genre = pupil.Genre,
                    HistoryYourSelf = historyYourSelf
                };
            }

            ViewData[HealthTestConstants.PUPILID] = id;
            ViewData[HealthTestConstants.CLASSID] = classID;

            return View();
        }

        /// <summary>
        ///  Vẽ biểu đồ cân nặng
        /// <author>DungVA</author>
        /// <date>29/01/2013</date>
        /// </summary>
        /// <returns>ảnh biểu đồ cân nặng</returns>

        [ValidateAntiForgeryToken]
        public ActionResult ShowChart1(int? PupilID, int? ClassID, string title)
        {
            MemoryStream st1 = this.outStream(PupilID, ClassID, title);
            Response.ContentType = "image/png";
            st1.WriteTo(Response.OutputStream);
            return null;
        }

        /// <summary>
        ///  Vẽ biểu đồ chiều cao
        /// <author>DungVA</author>
        /// <date>29/01/2013</date>
        /// </summary>
        /// <returns>ảnh biểu đồ chiều cao</returns>

        [ValidateAntiForgeryToken]
        public ActionResult ShowChart2(int? PupilID, int? ClassID, string title)
        {
            MemoryStream st1 = this.outStream(PupilID, ClassID, title);
            Response.ContentType = "image/png";
            st1.WriteTo(Response.OutputStream);
            return null;
        }

        private MemoryStream outStream(int? PupilID, int? ClassID)
        {
            List<ViettelDataChart> hehe = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe1 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe2 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe3 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe4 = new List<ViettelDataChart>();
            int valueTest = 0;
            for (int i = 0; i < 60; i++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + i;
                vdc.Value = valueTest + 1 + i;
                hehe.Add(vdc);
            }
            int valueTest1 = 2;
            for (int j = 0; j < 60; j++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + j;
                vdc.Value = valueTest1 + 1 + j;
                hehe1.Add(vdc);
            }
            int valueTest2 = 4;
            for (int k = 0; k < 60; k++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + k;
                vdc.Value = valueTest2 + 1 + k;
                hehe2.Add(vdc);
            }
            int valueTest3 = 6;
            for (int l = 0; l < 60; l++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + l;
                vdc.Value = valueTest3 + 1 + l;
                hehe3.Add(vdc);
            }
            int valueTest4 = 0;
            for (int l = 0; l < 60; l++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + l;
                vdc.Value = valueTest4 + 1 + l;
                hehe4.Add(vdc);
            }
            MemoryStream st1 = VTChartHelpers.drawStockChart(hehe, hehe1, hehe2, hehe3, hehe4);

            //Response.ContentType = "image/png";
            //st1.WriteTo(Response.OutputStream);
            //file2 = st1;
            return st1;
        }
        private MemoryStream outStream(int? PupilID, int? ClassID, string title)
        {
            List<ViettelDataChart> hehe = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe1 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe2 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe3 = new List<ViettelDataChart>();
            List<ViettelDataChart> hehe4 = new List<ViettelDataChart>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["Title"] = title;
            int valueTest = 0;
            for (int i = 0; i < 60; i++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + i;
                vdc.Value = valueTest + 1 + i;
                hehe.Add(vdc);
            }
            int valueTest1 = 2;
            for (int j = 0; j < 60; j++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + j;
                vdc.Value = valueTest1 + 1 + j;
                hehe1.Add(vdc);
            }
            int valueTest2 = 4;
            for (int k = 0; k < 60; k++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + k;
                vdc.Value = valueTest2 + 1 + k;
                hehe2.Add(vdc);
            }
            int valueTest3 = 6;
            for (int l = 0; l < 60; l++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + l;
                vdc.Value = valueTest3 + 1 + l;
                hehe3.Add(vdc);
            }
            int valueTest4 = 0;
            for (int l = 0; l < 60; l++)
            {
                ViettelDataChart vdc = new ViettelDataChart();
                vdc.Label = "Parameter" + l;
                vdc.Value = valueTest4 + 1 + l;
                hehe4.Add(vdc);
            }
            MemoryStream st1 = VTChartHelpers.drawStockChart(hehe, hehe1, hehe2, hehe3, hehe4, dic);

            //Response.ContentType = "image/png";
            //st1.WriteTo(Response.OutputStream);
            //file2 = st1;
            return st1;
        }

        /// <summary>
        ///  Xuất ảnh
        /// <author>DungVA</author>
        /// <date>29/01/2013</date>
        /// </summary>
        /// <param name="id">PupilID</param>
        /// <param name="valueCheck">checkbox value</param>
        /// <param name="classId">ClassID</param>
        /// <param name="type">Type of chart</param>
        /// <returns>File download cho người dùng</returns>
        public FileResult SaveImage(int id, bool? valueCheck, int? classId, int type = 1)
        {
            // nếu checkbox = true thì lấy toàn bộ biểu đồ của học sinh trong lớp
            // nếu checkbox = false thì lấy biểu đồ của học sinh được chọn
            if (valueCheck == false)
            {
                string FullName = PupilProfileBusiness.Find(id).FullName;
                if (type == 1)
                {
                    //Stream s1 = new MemoryStream(file1.ToArray());
                    Stream s1 = new MemoryStream(outStream(id, classId).ToArray());
                        FileStreamResult result1 = new FileStreamResult(s1, "application/octet-stream");

                        result1.FileDownloadName = FullName + "_BieuDoCanNang.bmp";
                        return result1;
                    }
                else
                {
                    //Stream s2 = new MemoryStream(file2.ToArray());
                    Stream s2 = new MemoryStream(outStream(id, classId).ToArray());
                        FileStreamResult result2 = new FileStreamResult(s2, "application/octet-stream");

                        result2.FileDownloadName = FullName + "_BieuDoChieuCao.bmp";
                        return result2;
                    }
                }
            else
            {
                GlobalInfo global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ClassID",classId},
                    {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
                };

                // lấy ra danh sách PupilID của lớp hiện tại
                List<PupilOfClass> lstPupilID = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();

                // Với mỗi học sinh sẽ có 2 biểu đồ là cân nặng và chiều cao, tất cả các biểu đồ cho vào cùng 1 thư mục
                for (int i = 0; i < lstPupilID.Count; i++)
                {
                    string FullName = lstPupilID[i].PupilProfile.FullName;

                    //MemoryStream ms1 = drawingChart();
                    //MemoryStream ms2 = drawingChart();
                    MemoryStream ms1 = outStream(id, classId);
                    MemoryStream ms2 = outStream(id, classId);
                    string fileName1 = FullName + "_BieuDoCanNang.bmp";
                    string fileName2 = FullName + "_BieuDoChieuCao.bmp";
                    var physicalPath1 = Path.Combine(Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH), fileName1);
                    Bitmap image1 = new Bitmap(ms1);
                    image1.Save(physicalPath1);
                    var physicalPath2 = Path.Combine(Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH), fileName2);
                    Bitmap image2 = new Bitmap(ms2);
                    image2.Save(physicalPath2);
                    ms1.Close();
                    ms2.Close();
                }

                // tạo tên file zip cho folder chứa biểu đồ
                string ClassName = ClassProfileBusiness.Find(classId).DisplayName;
                string ZipFolderName = ClassName + "_FolderZip";
                ZipFolder(ZipFolderName, Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH));

                // cho người dùng download file zip về

                MemoryStream ms = new MemoryStream();
                string ZipPathOnServer = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FILEPATH + ZipFolderName);

                // chuyển file zip sang kiểu stream
                FileStream file = new FileStream(ZipPathOnServer, FileMode.Open, FileAccess.Read);
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
                file.Close();
                Stream s1 = new MemoryStream(ms.ToArray());
                FileStreamResult result1 = new FileStreamResult(s1, "application/octet-stream");

                // đặt tên file download
                result1.FileDownloadName = ZipFolderName + ".zip";
                //fileNameZip = ZipFolderName;
                Session["fileNameZip"] = ZipFolderName;
                ms.Close();
                return result1;
            }
        }

        /// <summary>
        ///  Nén file
        /// <author>DungVA</author>
        /// <date>29/01/2013</date>
        /// </summary>
        /// <param name="ZipFileName">ZipFileName</param>
        /// <param name="ZipPath">ZipPath</param>
        /// <returns></returns>
        private void ZipFolder(string ZipFileName, string ZipPath)
        {
            ZipFile zip = new ZipFile();

            //ZipFileName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + ZipFileName;
            ZipFileName = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FILEPATH) + ZipFileName;
            zip.AddDirectory(ZipPath);
            zip.Save(ZipFileName);
        }

        /// <summary>
        ///  Xóa file
        /// <author>DungVA</author>
        /// <date>29/01/2013</date>
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult emptyFolder()
        {
            try
            {
                /*Xóa toàn bộ file trong folder*/
                string folderPath = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(folderPath);

                //dir.GetFiles().ToList().ForEach(f => f.Delete());
                List<FileInfo> lst = dir.GetFiles().ToList();
                for (int i = 0; i < lst.Count; i++)
                {
                    lst[i].Delete();
                }
                /*********************************/
                /*Xóa file zip đã tạo ra sau khi download*/
                FileInfo fi = new FileInfo(Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FILEPATH) + Session["fileNameZip"]);
                fi.Delete();
                Session["fileNameZip"] = "";
                /*********************************/
            }
            catch (Exception)
            {
                throw new BusinessException("HealthTest_Label_ErrorEmptyFolder");
            }

            return Json(new JsonMessage(""));
        }

        #endregion CHART

        private void GetPermission()
        {
            string controllerName = "MonitoringBookYear"; //this.ControllerContext.RouteData.Values["controller"].ToString();
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
        }
    }


}
