﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Web.Utils;
using System.Text.RegularExpressions;
using System.Drawing;
using Ionic.Zip;
using System.Diagnostics;
using System.Web.Script.Serialization;

namespace SMAS.Web.Areas.HealthTestArea.Controllers
{
    /// <summary>
    /// Growth chart controller
    /// </summary>
    /// <auth>HaiVT</auth>
    /// <date>24/04/2013</date>
    public class GrowthChartController : BaseController
    {
        private IMonitoringBookBusiness MonitoringBookBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IPhysicalTestBusiness PhysicalTestBusiness;

        private const int numMonthOfBirthDay = 60;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="monitoringBookBusiness"></param>
        /// <param name="pupilProfileBusiness"></param>
        /// <param name="classProfileBusiness"></param>
        public GrowthChartController
            (
                IMonitoringBookBusiness monitoringBookBusiness,
                IPupilProfileBusiness pupilProfileBusiness,
                IClassProfileBusiness classProfileBusiness,
                IPhysicalTestBusiness physicalTestBusiness
            )
        {
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
        }

        #region ve bieu do
        /// <summary>
        /// view growthChart by pupilID
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>24/04/0213</date>
        /// <param name="monitoringID"></param>
        /// <param name="pupilID"></param>
        /// <param name="classID"></param>
        /// <returns></returns>
        public ActionResult Index(int pupilID, int classID)
        {
            PupilProfile objPupilProfile = PupilProfileBusiness.Find(pupilID);
            ViewData[HealthTestConstants.PUPIL_ID] = pupilID;
            if (objPupilProfile != null && objPupilProfile.CurrentSchoolID == _globalInfo.SchoolID)
            {
                //fullname
                ViewData[HealthTestConstants.FULL_NAME] = objPupilProfile.FullName;
                //birthday
                ViewData[HealthTestConstants.BIRTH_DAY] = objPupilProfile.BirthDate;
                ViewData["AddressChildren"] = objPupilProfile.PermanentResidentalAddress;
                //kiem tra so kham de lay ra tieu su benh ban than gan nhat
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["pupilID"] = pupilID;
                dic["birthday"] = objPupilProfile.BirthDate;
                List<MonthlyGrowthBO> lstMonthlyGrowthBO = MonitoringBookBusiness.GetAllMonitoringBookHistoryYourSelfByPupilID(dic).ToList();
                ViewData[HealthTestConstants.HISTORY_YOUR_SELF] = string.Empty;
                if (lstMonthlyGrowthBO != null && lstMonthlyGrowthBO.Count > 0)
                {
                    for (int i = lstMonthlyGrowthBO.Count - 1; i > 0; i--)
                    {
                        if (!string.IsNullOrEmpty(lstMonthlyGrowthBO[i].HistoryYourSelf))
                        {
                            ViewData[HealthTestConstants.HISTORY_YOUR_SELF] = lstMonthlyGrowthBO[i].HistoryYourSelf;
                            break;
                        }
                    }
                }

                IQueryable<PhysicalTestBO> query = this.PhysicalTestBusiness.SearchByPupil(0, GlobalInfo.getInstance().SchoolID.Value, pupilID, classID);// AccademicYearId = 0 : lấy hết tất cả các thông tin khám sức khỏe của tất cả các năm
                List<PhysicalTestBO> listPhysicalTestBO = query.ToList();
                List<MonthlyGrowthBO> lstMonthlyGrowthBOTemp = new List<MonthlyGrowthBO>();
                MonthlyGrowthBO temp = null;
                for (int i = 0; i < listPhysicalTestBO.Count; i++)
                {
                    temp = new MonthlyGrowthBO();
                    temp.BirthDate = objPupilProfile.BirthDate;
                    temp.FullName = objPupilProfile.FullName;
                    temp.Genre = objPupilProfile.Genre;
                    temp.Height = listPhysicalTestBO[i].Height;
                    temp.Weight = listPhysicalTestBO[i].Weight;
                    temp.Month = (int)Math.Floor(listPhysicalTestBO[i].Date.Value.Subtract(objPupilProfile.BirthDate).TotalDays / 30);
                    temp.PupilID = pupilID;
                    lstMonthlyGrowthBOTemp.Add(temp);
                }

                switch (objPupilProfile.Genre)
                {
                    case GlobalConstants.GENRE_FEMALE:
                        {
                            ViewData[HealthTestConstants.GENRE] = GlobalConstants.GENRE_FEMALE_TITLE;
                            break;
                        }
                    case GlobalConstants.GENRE_MALE:
                        {
                            ViewData[HealthTestConstants.GENRE] = GlobalConstants.GENRE_MALE_TITLE;
                            break;
                        }
                    default:
                        break;
                }
                // class name
                ViewData[HealthTestConstants.CLASS_NAME] = ClassProfileBusiness.Find(classID).DisplayName;
                
                #region tao du lieu ve chart
                //children chi co 60 thang tuoi
                List<MonthlyGrowthBO> lstMonthlyGrowthBOChart = new List<MonthlyGrowthBO>();
                MonthlyGrowthBO objMonthlyGrowthBO = null;
                for (int i = 0; i <= numMonthOfBirthDay; i++)
                {
                    objMonthlyGrowthBO = new MonthlyGrowthBO();
                    for (int j = lstMonthlyGrowthBOTemp.Count - 1; j >= 0; j--)
                    {
                        if (lstMonthlyGrowthBOTemp[j].Month == i)
                        {
                            objMonthlyGrowthBO.Weight = lstMonthlyGrowthBOTemp[j].Weight;
                            objMonthlyGrowthBO.Height = lstMonthlyGrowthBOTemp[j].Height;
                        }
                    }
                    objMonthlyGrowthBO.Month = i;
                    lstMonthlyGrowthBOChart.Add(objMonthlyGrowthBO);
                }
                ViewData[HealthTestConstants.CLASS_ID] = objPupilProfile.CurrentClassID;
                ViewData[HealthTestConstants.LIST_CHART] = lstMonthlyGrowthBOChart;
                ViewData[HealthTestConstants.CLASSID_SELECT] = classID;
                #endregion
            }
            else
            {
                ViewData[HealthTestConstants.HISTORY_YOUR_SELF] = string.Empty;
                //fullname
                ViewData[HealthTestConstants.FULL_NAME] = string.Empty;
                //birthday
                ViewData[HealthTestConstants.BIRTH_DAY] = string.Empty;
                //ClassID
                ViewData[HealthTestConstants.CLASS_ID] = string.Empty;
                //classNaem
                ViewData[HealthTestConstants.CLASS_NAME] = string.Empty;
                ViewData[HealthTestConstants.LIST_CHART] = new List<MonthlyGrowthBO>();
            }
            return View();
        }
        #endregion

        #region Draw all class
        /// <summary>
        /// Load du lieu SVG data de xuat hinh cho toan lop
        /// </summary>
        /// <param name="classID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadSvgData(int classId, int pupilID)
        {
            var cls = ClassProfileBusiness.Find(classId);
            if (cls != null)
            {
                GetMonthlyGrowthOfClass(classId, pupilID);
            }
            return PartialView("_AllClass");
        }

        /// <summary>
        /// Tao file hinh tu CSVG data from client
        /// </summary>
        /// <param name="lstSvg"></param>
        /// <param name="classId"></param>
        /// <param name="lstPP"></param>
        /// <param name="lstMapW"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateSVG(List<string> lstSvg, int? classId, List<int> lstPP, List<bool> lstMapW, bool compress = false)
        {
            lstSvg = new JavaScriptSerializer().Deserialize<List<string>>(Request["lstSvg"]);
            lstPP = new JavaScriptSerializer().Deserialize<List<int>>(Request["lstPP"]);
            lstMapW = new JavaScriptSerializer().Deserialize<List<bool>>(Request["lstMapW"]);
            ClassProfile cls = ClassProfileBusiness.Find(classId);
            if (cls != null)
            {
                string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(pathFolder);
                if (!dir.Exists) dir.Create();
                string classPath = Path.Combine(pathFolder, classId.ToString());
                dir = new DirectoryInfo(classPath);
                if (!dir.Exists) dir.Create();
                string ZipFolderName = RemoveSpecialCharacters(cls.DisplayName) + "_BieuDoTangTruong";
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = classId;
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                int counter = 0;
                for (int i = 0; i < lstPP.Count; i++)
                {
                    PupilProfile pp = PupilProfileBusiness.Find(lstPP[i]);
                    string FullName = pp.FullName;
                    string fileName = RemoveSpecialCharacters(FullName) + "_BieuDoCanNang.png";
                    if (lstMapW[i] == false) fileName = RemoveSpecialCharacters(FullName) + "_BieuDoChieuCao.png";
                    fileName = DateTime.Now.ToString("ddMMyyyy_HHmmss") + "_" + fileName;
                    string svg = string.Empty;
                    if (compress)
                    {
                        svg = (lstSvg.Count > i) ? Utils.DeJSend.GetData(Utils.DeJSend.DecodeFrom64(lstSvg[i])) : string.Empty;
                    }
                    else
                    {
                        svg = (lstSvg.Count > i) ? lstSvg[i] : string.Empty;
                    }
                    var physicalPath = Path.Combine(classPath, fileName);
                    var file = createFile(svg, fileName, classId.Value, physicalPath, lstMapW[i], pp);
                    if (file) counter++;
                }
                if (counter > 0)
                {
                    ZipFolder(ZipFolderName, classPath, classPath);
                    return Json(new JsonMessage(string.Empty));
                }
            }
            return Json(new JsonMessage("Xuất file không thành công.", "error"));
        }

        /// <summary>
        /// Save file to client
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        public FileResult DowloadZipped(int classId)
        {
            ClassProfile cls = ClassProfileBusiness.Find(classId);
            if (cls != null)
            {
                //Check path
                string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(pathFolder);
                if (!dir.Exists) dir.Create();
                string classPath = Path.Combine(pathFolder, classId.ToString());
                dir = new DirectoryInfo(classPath);
                if (!dir.Exists) dir.Create();
                string ZipFolderName = RemoveSpecialCharacters(cls.DisplayName) + "_BieuDoTangTruong";
                //End of Check path
                MemoryStream ms = new MemoryStream();
                string ZipPathOnServer = Path.Combine(classPath, ZipFolderName);
                FileInfo fileInf = new FileInfo(ZipPathOnServer);
                if (fileInf.Exists)
                {
                    // chuyển file zip sang kiểu stream
                    FileStream file = new FileStream(ZipPathOnServer, FileMode.Open, FileAccess.Read);
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);
                    file.Close();
                    Stream s1 = new MemoryStream(ms.ToArray());
                    FileStreamResult result1 = new FileStreamResult(s1, "application/octet-stream");
                    // đặt tên file download
                    result1.FileDownloadName = ZipFolderName + ".zip";
                    ms.Close();
                    //Xoa file duoc luu
                    emptyFolder(classId);
                    return result1;
                }
            }
            return null;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// SVG Image converter application
        /// </summary>
        private const string INKSCAPE_PATH = @"~/Components/inkscape/inkscape.exe";
        /// <summary>
        /// SVG Background Image
        /// </summary>
        private const string IMG_BG_HEIGHT_BOY = "~/Content/images/bieudochieucaobetrai.jpg";
        /// <summary>
        /// background bieu do can nang nam
        /// </summary>
        private const string IMG_BG_WEIGHT_BOY = "~/Content/images/bieudocannangbetrai.jpg";
        /// <summary>
        /// background bieu do chieu cao nu
        /// </summary>
        private const string IMG_BG_HEIGHT_GIRL = "~/Content/images/bieudochieucaobegai.jpg";
        /// <summary>
        /// background bieu do can nang nu
        /// </summary>
        private const string IMG_BG_WEIGHT_GIRL = "~/Content/images/bieudocannangbegai.jpg";
        /// <summary>
        /// pixel margin left cua bieu do chieu cao
        /// </summary>
        private const int HEIGHT_IMG_BG_LEFT_PADDING = 46;
        /// <summary>
        /// pixel margin top cua bieu do chieu cao
        /// </summary>
        private const int HEIGHT_IMG_BG_TOP_PADDING = 29;
        /// <summary>
        /// pixel margin trai cua bieu do can nang
        /// </summary>
        private const int WEIGHT_IMG_BG_LEFT_PADDING = 46;
        /// <summary>
        /// pixel margin tren cua hinh bieu do can nang
        /// </summary>
        private const int WEIGHT_IMG_BG_TOP_PADDING = 16;
        /// <summary>
        /// MAP WIDTH
        /// </summary>
        private const string IMAGE_CHART_WITH = "849px";
        /// <summary>
        /// MAP HEIGHT
        /// </summary>
        private const string IMAGE_CHART_HEIGHT = "498px";

        /// <summary>
        /// Tao file hinh
        /// </summary>
        /// <param name="svg"></param>
        /// <param name="fileName"></param>
        /// <param name="classId"></param>
        /// <param name="outImage"></param>
        /// <param name="isMapWeigth"></param>
        /// <param name="gender"></param>
        /// <returns></returns>
        private bool createFile(string svg, string fileName, int classId, string outImage, bool isMapWeigth, PupilProfile pupilObj)
        {
            try
            {
                string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(pathFolder);
                if (!dir.Exists) dir.Create();
                string classPath = Path.Combine(pathFolder, classId.ToString() + "_SVG");
                dir = new DirectoryInfo(classPath);
                if (!dir.Exists) dir.Create();
                string file = Path.Combine(classPath, fileName);
                if (svg != string.Empty)
                {
                    var svgText = HttpUtility.UrlDecode(svg);
                    //Save to file
                    var svgFile = file + ".svg";
                    System.IO.File.WriteAllText(svgFile, svgText);
                    //Creaet file
                    //Su dung svg render engine de tao hinh tu file SVG
                    //Mot so format/shape khong fill dung
                    Svg.SvgDocument doc = Svg.SvgDocument.Open(svgFile);
                    if (doc != null)
                    {
                        doc.Draw().Save(outImage);
                    }

                    if (System.IO.File.Exists(outImage))
                    {
                        MakeBackground(pupilObj.Genre, isMapWeigth, outImage, pupilObj);
                    }
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Tao background
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="mapWeight"></param>
        /// <param name="mapFile"></param>
        private void MakeBackground(int gender, bool mapWeight, string mapFile, PupilProfile pp)
        {
            int padding_left = HEIGHT_IMG_BG_LEFT_PADDING;
            int padding_top = HEIGHT_IMG_BG_TOP_PADDING;
            string fileBackground = string.Empty;
            if (gender == GlobalConstants.GENRE_MALE)
            {
                //Nam
                fileBackground = (mapWeight) ? IMG_BG_WEIGHT_BOY : IMG_BG_HEIGHT_BOY;
            }
            else
            {
                fileBackground = (mapWeight) ? IMG_BG_WEIGHT_GIRL : IMG_BG_HEIGHT_GIRL;
            }
            if (mapWeight)
            {
                //Thong so khi tao background voi dieu do can nang
                padding_left = WEIGHT_IMG_BG_LEFT_PADDING;
                padding_top = WEIGHT_IMG_BG_TOP_PADDING;
            }
            fileBackground = Server.MapPath(fileBackground);
            if (System.IO.File.Exists(fileBackground))
            {
                Stream bgStream = System.IO.File.OpenRead(fileBackground);
                Stream mapStream = System.IO.File.OpenRead(mapFile);
                Image mapImg = Image.FromStream(mapStream);
                Image saveImg = Image.FromStream(bgStream);
                //Create background
                Graphics bkImg = Graphics.FromImage(saveImg);
                if (bkImg != null)
                {
                    bkImg.DrawImage(mapImg, padding_left, padding_top, mapImg.Width, mapImg.Height);
                    int nameWidth = 300;
                    int nameHeight = 60;
                    int marginRight = 44;
                    int marginBottom = 56;
                    bkImg.FillRectangle(System.Drawing.Brushes.White, saveImg.Width - marginRight - nameWidth, saveImg.Height - marginBottom - nameHeight - 35, nameWidth, nameHeight + 35);

                    PointF namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 10 - 35);
                    bkImg.DrawString("Họ tên: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                    namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 10 - 35);
                    bkImg.DrawString(pp.FullName, new Font(new FontFamily("Arial"), 12F, FontStyle.Bold), Brushes.Black, namePoint);

                    namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 35 - 35);
                    bkImg.DrawString("Địa chỉ: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                    namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 65, saveImg.Height - marginBottom - nameHeight + 35 - 35);
                    bkImg.DrawString(pp.PermanentResidentalAddress, new Font(new FontFamily("Arial"), 12F, FontStyle.Bold), Brushes.Black, namePoint);

                    namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 20, saveImg.Height - marginBottom - nameHeight + 60 - 35);
                    bkImg.DrawString("Ngày sinh: ", new Font(new FontFamily("Arial"), 12F), new SolidBrush(Color.FromArgb(33, 94, 162)), namePoint);
                    namePoint = new PointF(saveImg.Width - marginRight - nameWidth + 80, saveImg.Height - marginBottom - nameHeight + 60 - 35);
                    bkImg.DrawString(pp.BirthDate.ToString("dd/MM/yyyy"), new Font(new FontFamily("Arial"), 12F, FontStyle.Bold), Brushes.Black, namePoint);
                }
                Stream tmpStream = new System.IO.MemoryStream();
                saveImg.Save(tmpStream, System.Drawing.Imaging.ImageFormat.Png);
                mapStream.Close();
                bgStream.Close();
                mapImg = Image.FromStream(tmpStream);
                mapImg.Save(mapFile);
                //release object
                tmpStream.Close();
                mapImg.Dispose();
                saveImg.Dispose();
                mapStream.Dispose();
                bgStream.Dispose();
                tmpStream.Dispose();
            }
        }

        /// <summary>
        /// Tao folder zipped
        /// </summary>
        /// <param name="ZipFileName"></param>
        /// <param name="ZippedPath"></param>
        /// <param name="savePath"></param>
        private void ZipFolder(string ZipFileName, string ZippedPath, string savePath)
        {
            ZipFile zip = new ZipFile();
            ZipFileName = Path.Combine(savePath, ZipFileName);
            zip.AddDirectory(ZippedPath);
            zip.Save(ZipFileName);
        }

        /// <summary>
        /// Remove Vietnamese
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string RemoveSpecialCharacters(string str)
        {
            str = CommonFunctions.RemoveSign(str);
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "_", RegexOptions.Compiled);
        }

        /// <summary>
        /// Load data for class
        /// </summary>
        /// <param name="classID"></param>
        private void GetMonthlyGrowthOfClass(int classID, int pupilID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["CurrentClassID"] = classID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            if (pupilID != 0)
            {
                dic["pupilID"] = pupilID;
                dic["PupilProfileID"] = pupilID;
            }

            List<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            #region lay du lieu can nang chieu cao cua tre
            // AccademicYearId = 0 : lấy hết tất cả các thông tin khám sức khỏe của tất cả các năm            
            IQueryable<PhysicalTestBO> query = null;
            if (pupilID != 0)
            {
                query = this.PhysicalTestBusiness.SearchByPupil(0, GlobalInfo.getInstance().SchoolID.Value, pupilID, classID);
            }
            else
            {
                query = this.PhysicalTestBusiness.SearchByPupil(0, GlobalInfo.getInstance().SchoolID.Value, 0, classID);// PupilFileID = 0 : lay tat ca hoc sinh trong lop
            }
            List<PhysicalTestBO> listPhysicalTestBO = query.ToList();
            List<MonthlyGrowthBO> lstMonthlyGrowthBOTemp = new List<MonthlyGrowthBO>();
            MonthlyGrowthBO temp = null;
            for (int i = 0, size = listPhysicalTestBO.Count; i < size; i++)
            {
                temp = new MonthlyGrowthBO();
                temp.BirthDate = listPhysicalTestBO[i].PupilProFile.BirthDate;
                temp.FullName = listPhysicalTestBO[i].PupilProFile.FullName;
                temp.Genre = listPhysicalTestBO[i].PupilProFile.Genre;
                temp.Height = listPhysicalTestBO[i].Height;
                temp.Weight = listPhysicalTestBO[i].Weight;
                temp.Month = (int)Math.Floor(listPhysicalTestBO[i].Date.Value.Subtract(listPhysicalTestBO[i].PupilProFile.BirthDate).TotalDays / 30);
                temp.PupilID = listPhysicalTestBO[i].PupilProFileID;
                lstMonthlyGrowthBOTemp.Add(temp);
            }
            #endregion

            #region tao du lieu ve chart
            //children chi co 60 thang tuoi
            List<MonthlyGrowthBO> lstMonthlyGrowthBOChart = new List<MonthlyGrowthBO>();
            MonthlyGrowthBO objMonthlyGrowthBO = null;
            List<int> lstPupilFileID = lstMonthlyGrowthBOTemp.Select(d => d.PupilID).Distinct().ToList();
            int pID = -1;
            List<MonthlyGrowthBO> lstTmp = null;
            MonthlyGrowthBO objPP = null;
            for (int j = 0, size = lstPupilFileID.Count; j < size; j++)
            {
                pID = lstPupilFileID[j];
                lstTmp = lstMonthlyGrowthBOTemp.Where(a => a.PupilID == pID).ToList();
                objPP = lstTmp.FirstOrDefault();
                if (objPP == null) continue;
                for (int i = 0; i <= numMonthOfBirthDay; i++)
                {
                    objMonthlyGrowthBO = lstTmp.Where(a => a.Month == i).FirstOrDefault();
                    if (objMonthlyGrowthBO == null)
                    {
                        objMonthlyGrowthBO = new MonthlyGrowthBO();
                    }
                    objMonthlyGrowthBO.Genre = objPP.Genre;
                    objMonthlyGrowthBO.Status = objPP.Status;
                    objMonthlyGrowthBO.PupilID = objPP.PupilID;
                    lstMonthlyGrowthBOChart.Add(objMonthlyGrowthBO);
                }
            }
            ViewData[HealthTestConstants.LIST_CHART_CLASS] = lstMonthlyGrowthBOChart;
            ViewData[HealthTestConstants.LIST_PUPIL] = lstPupil;
            #endregion

        }

        private JsonResult emptyFolder(int classID)
        {
            try
            {
                ClassProfile cls = ClassProfileBusiness.Find(classID);
                if (cls != null)
                {
                    string pathFolder = Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FOLDERPATH);
                    string classPath = Path.Combine(pathFolder, classID.ToString());
                    DirectoryInfo dir = new DirectoryInfo(classPath);
                    if (dir.Exists)
                    {
                        dir.Delete(true);
                    }
                    classPath = classPath + "_SVG";
                    dir = new DirectoryInfo(classPath);
                    if (dir.Exists)
                    {
                        dir.Delete(true);
                    }
                }
            }
            catch (Exception)
            {

            }
            return Json(new JsonMessage(""));
        }
        #endregion
    }
}
