using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Utils;
using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class SpineTestViewModel
    {
        public SpineTestViewModel()
        {
            this.SpineTestID = null;
            this.MonitoringBookID = null;
            this.MonitoringDate = DateTime.Now;
            this.EnableToEditMonitoringDate = true;
            this.UnitName = null;
            this.EnableToEditUnitName = true;
            this.Other = null;
        }
        public SpineTestViewModel(int PupilID)
        {
            this.PupilID = PupilID;
        }

        [HiddenInput(DisplayValue = false)]
        public int PupilID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SpineTestID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int HealthPeriodID { get; set; }

        [ResourceDisplayName("DentalTest_Label_MonitoringBookDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime MonitoringDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditMonitoringDate { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_UnitName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string UnitName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditUnitName { get; set; }

        [ResourceDisplayName("SpineTest_Label_IsDeformityOfTheSpine")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsDeformityOfTheSpine { get; set; }

        [ResourceDisplayName("SpineTest_Label_ExaminationStraight")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ExaminationStraight { get; set; }

        [ResourceDisplayName("SpineTest_Label_ExaminationItalic")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string ExaminationItalic { get; set; }


        [ResourceDisplayName("SpineTest_Label_Other")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Other { get; set; }

        //[ResDisplayName("SpineTest_Label_CreatedDate")]
        //public Nullable<System.DateTime> CreatedDate { get; set; }

        //[ResDisplayName("SpineTest_Label_IsActive")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //public bool IsActive { get; set; }

        //[ResDisplayName("SpineTest_Label_ModifiedDate")]
        //public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
