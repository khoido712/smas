﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class ENTTestViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int PupilID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ENTTestID { get; set; }

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        public int HealthPeriodID { get; set; }

        [ResourceDisplayName("DentalTest_Label_MonitoringBookDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime MonitoringDate { get; set; }

        [ResourceDisplayName("HealthTest_Label_UnitName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string UnitName { get; set; }

        [ResourceDisplayName("ENTTest_Label_RCapacityHearing")]
        [Range(0, 1000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int? RCapacityHearing { get; set; }

        [ResourceDisplayName("ENTTest_Label_LCapacityHearing")]
        [Range(0, 1000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int? LCapacityHearing { get; set; }

        [ResourceDisplayName("ENTTest_Label_Deaf")]
        public bool? IsDeaf { get; set; }

        [ResourceDisplayName("ENTTest_Label_Sick")]
        public bool? IsSick { get; set; }

        [ResourceDisplayName("ENTTest_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }        
        
    }
}