using System;
using System.Linq;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class CommonViewModel
    {
        public int PupilID { get; set; }

        public string FullName { get; set; }

        public DateTime BirthDate { get; set; }

        public int Genre { get; set; }

        public string GenreName
        {
            get
            {
                ComboObject c = CommonList.GenreAndSelect().Where(u => u.key == Genre.ToString()).SingleOrDefault();
                return c == null ? string.Empty : c.value;
            }
        }

        public int ClassID { get; set; }

        public string ClassName { get; set; }

        public string HistoryYourSelf { get; set; }
    }
}
