﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class PhysicalTestViewModel
    {
        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int PupilID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PhysicalTestID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int HealthPeriodID { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Date")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime MonitoringDate { get; set; }

        //[HiddenInput(DisplayValue = false)]
        //public bool EnableToEditMonitoringDate { get; set; }

        [ResourceDisplayName("HealthTest_Label_UnitName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100)]
        public string UnitName { get; set; }

        //[HiddenInput(DisplayValue = false)]
        //public bool EnableToEditUnitName { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Height")]
        [AdditionalMetadata("Height",0)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public decimal? Height { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Weight")]
        [AdditionalMetadata("Weight", 0)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public decimal? Weight { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Breast")]
        [AdditionalMetadata("Breast", 0)]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public decimal? Breast { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_PhysicalClassification")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? PhysicalClassification { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_PhysicalClassification")]
        public string PhysicalClassificationName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Nutrition { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Nutrition")]
        public string NutritionName 
        {
            get
            {
                return this.Nutrition == SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION ? "Suy dinh dưỡng"
                       : this.Nutrition == SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT ? "Béo phì"
                       : this.Nutrition == SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT ? "Bình thường" : "";
            }
            set
            {
            }
        }

        public int DisplayLink { get; set; }

        public DateTime? CreateDate { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_BMIIndex")]
        public decimal? BMIIndex { get; set; }

        private bool __chkTrack = false;
        [ResourceDisplayName("PhysicalTest_Label_Track")]
        public bool chkTrack 
        {
            get
            {
                return __chkTrack;
            }
            set
            {
                __chkTrack = value;
            }
        }
    }
}