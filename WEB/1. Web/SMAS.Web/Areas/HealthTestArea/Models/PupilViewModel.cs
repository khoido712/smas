﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class PupilViewModel
    {
        public int STT { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        public string PupilName { get; set; }
        public int Status { get; set; }
        public int Genre { get; set; }
        public byte[] ImageData { get; set; }
        public int Month { get; set; }
        public string byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn != null)
            {
                return "data:image/png;base64," + @System.Convert.ToBase64String(byteArrayIn);
            }
            else
            {
                return "";
            }
        }

    }
}