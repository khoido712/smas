﻿using System;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class HealthTestDetailModel
    {
        public string FullName { get; set; }

        public DateTime BirthDate { get; set; }

        public string GenreName { get; set; }

        public string ClassName { get; set; }

        public string SchoolName { get; set; }

        public string Address { get; set; }

        public string HistoryYourself { get; set; }

        public int PupilID { get; set; }

        public int ClassID { get; set; }

        public string ProvinceName { get; set; }

        //public int MonitoringBookID { get; set; }

        public MonitoringBook MonitoringBook { get; set; }

        public PhysicalTest PhysicalTest { get; set; }

        public EyeTest EyeTest { get; set; }

        public ENTTest ENTTest { get; set; }

        public SpineTest SpineTest { get; set; }

        public DentalTest DentalTest { get; set; }

        public OverallTest OverallTest { get; set; }
    }
}