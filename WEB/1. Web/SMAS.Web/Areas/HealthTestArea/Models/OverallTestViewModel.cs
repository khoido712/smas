﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class OverallTestViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int PupilID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? OverallTestID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }        

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        public int? HealthPeriodID { get; set; }

        [ResourceDisplayName("DentalTest_Label_MonitoringBookDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime MonitoringDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditMonitoringDate { get; set; }

        [ResourceDisplayName("MonitoringBook_Label_UnitName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string UnitName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditUnitName { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string HistoryYourSelf { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int OverallInternal { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string DescriptionOverallInternal { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int OverallForeign { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string DescriptionOverallForeign { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SkinDiseases { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string DescriptionSkinDiseases { get; set; }


        public bool IsHeartDiseases { get; set; }

        public bool IsBirthDefect { get; set; }
        
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Other { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EvaluationHealth { get; set; }

        public string EvaluationHealthName { get; set; }
        [StringLength(1000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string RequireOfDoctor { get; set; }

        [ResourceDisplayName("OverallTest_Label_Doctor")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Doctor { get; set; }

        public bool IsDredging { get; set; }
    }
}