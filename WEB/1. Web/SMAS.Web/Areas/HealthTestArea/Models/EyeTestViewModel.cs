﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class EyeTestViewModel
    {

        [HiddenInput(DisplayValue = false)]
        public int PupilID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? EyeTestID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? MonitoringBookID { get; set; }

        [ResourceDisplayName("DentalTest_Label_HealthTestPeriod")]
        public int HealthPeriodID { get; set; }

        [ResourceDisplayName("HealthTest_Label_UnitName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string UnitName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditUnitName { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public DateTime MonitoringDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool EnableToEditMonitoringDate { get; set; }

        //[ResDisplayName("PhysicalTest_Label_Date")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //public DateTime? MonitoringDate1 { get; set; }

        [ResourceDisplayName("HealthTest_Label_REye")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? REye { get; set; }

        [ResourceDisplayName("HealthTest_Label_LEye")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int? LEye { get; set; }

        [ResourceDisplayName("HealthTest_Label_REye")]
        public Nullable<decimal> RMyopic { get; set; }

        [ResourceDisplayName("HealthTest_Label_LEye")]
        public Nullable<decimal> LMyopic { get; set; }

        [ResourceDisplayName("HealthTest_Label_REye")]
        public Nullable<decimal> RFarsightedness { get; set; }

        [ResourceDisplayName("HealthTest_Label_LEye")]
        public Nullable<decimal> LFarsightedness { get; set; }

        [ResourceDisplayName("HealthTest_Label_REye")]
        public Nullable<decimal> RAstigmatism { get; set; }

        [ResourceDisplayName("HealthTest_Label_LEye")]
        public Nullable<decimal> LAstigmatism { get; set; }

        [ResourceDisplayName("PhysicalTest_Label_Other")]
        public string Other { get; set; }

        public bool IsTreatment { get; set; }

        public bool IsMyopic { get; set; }

        public bool IsFarsightedness { get; set; }

        public bool IsAstigmatism { get; set; }

        public DateTime? CreateDate { get; set; }

        public int DisplayLink { get; set; }
        
    }
}