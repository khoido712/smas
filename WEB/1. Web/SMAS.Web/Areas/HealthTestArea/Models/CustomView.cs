﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HealthTestArea.Models
{
    public class CustomView
    {

        public PupilViewModel pupilView { get; set; }

        public PhysicalTestViewModel physicalTest { get; set; }

        public EyeTestViewModel eyeTest { get; set; }

        public ENTTestViewModel eNTTest { get; set; }

        public SpineTestViewModel spineTest { get; set; }

        public DentalTestViewModel dentalTest { get; set; }

        public OverallTestViewModel overallTest { get; set; }

        public HealthTestDetailModel overallTestDetail { get; set; }
    }
}