﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestArea
{
    public class HealthTestAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HealthTestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HealthTestArea_default",
                "HealthTestArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}