﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DisabledTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.DisabledTypeArea.Controllers
{
    public class DisabledTypeController : BaseController
    {        
        private readonly IDisabledTypeBusiness DisabledTypeBusiness;
		
		public DisabledTypeController (IDisabledTypeBusiness disabledtypeBusiness)
		{
			this.DisabledTypeBusiness = disabledtypeBusiness;
		}
		
		//
        // GET: /DisabledType/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SetViewDataPermission("DisabledType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            //Get view data here
            SearchInfo["IsActive"] = true;

            IEnumerable<DisabledTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[DisabledTypeConstants.LIST_DISABLEDTYPE] = lst;
            return View();
        }

		//
        // GET: /DisabledType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            IEnumerable<DisabledTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[DisabledTypeConstants.LIST_DISABLEDTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            DisabledType disabledtype = new DisabledType();
            disabledtype.IsActive = true;
            TryUpdateModel(disabledtype); 

            Utils.Utils.TrimObject(disabledtype);

            this.DisabledTypeBusiness.Insert(disabledtype);
            this.DisabledTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DisabledTypeID)
        {
            DisabledType disabledtype = this.DisabledTypeBusiness.Find(DisabledTypeID);
            TryUpdateModel(disabledtype);
            Utils.Utils.TrimObject(disabledtype);
            this.DisabledTypeBusiness.Update(disabledtype);
            this.DisabledTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            DisabledTypeBusiness.Delete(id);
            this.DisabledTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DisabledTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<DisabledType> query = this.DisabledTypeBusiness.Search(SearchInfo);
            IQueryable<DisabledTypeViewModel> lst = query.Select(o => new DisabledTypeViewModel {               
						DisabledTypeID = o.DisabledTypeID,								
						Resolution = o.Resolution,								
						Description = o.Description							
					
            });

            return lst.ToList();
        }        
    }
}





