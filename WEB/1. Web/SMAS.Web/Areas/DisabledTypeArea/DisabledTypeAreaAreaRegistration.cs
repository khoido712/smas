﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DisabledTypeArea
{
    public class DisabledTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DisabledTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DisabledTypeArea_default",
                "DisabledTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
