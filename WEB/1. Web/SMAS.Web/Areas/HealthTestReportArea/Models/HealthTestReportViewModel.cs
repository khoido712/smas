﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.HealthTestReportArea.Models
{
    public class HealthTestReportViewModel 
    {


        [ResourceDisplayName("HealthTestReport_Label_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportConstants.LIST_Year)]
        public Nullable<int> Year { get; set; }


        [ResourceDisplayName("HealthTestReport_Label_Period")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportConstants.LIST_Period)]
        public Nullable<int> HealthPeriod { get; set; }

        [ResourceDisplayName("HealthTestReport_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", HealthTestReportConstants.LIST_APPLYLEVEL)]
        public Nullable<int> AppliedLevel { get; set; }

    }
}
