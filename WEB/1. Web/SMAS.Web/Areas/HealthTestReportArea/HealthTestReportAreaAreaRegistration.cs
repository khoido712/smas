﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HealthTestReportArea
{
    public class HealthTestReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HealthTestReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HealthTestReportArea_default",
                "HealthTestReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
