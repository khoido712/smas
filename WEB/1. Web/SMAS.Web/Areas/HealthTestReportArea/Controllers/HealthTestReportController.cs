﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Web.Areas.HealthTestReportArea.Models;
using System.Configuration;

namespace SMAS.Web.Areas.HealthTestReportArea.Controllers
{
    public class HealthTestReportController : BaseController
    {

          private readonly IHealthTestReportBusiness HealthTestReportBusiness;
          private readonly IProcessedReportBusiness ProcessedReportBusiness;
          private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
          private readonly IAcademicYearBusiness AcademicYearBusiness;
          private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;

          public HealthTestReportController(
                                                 IHealthTestReportBusiness HealthTestReportBusiness,
                                                 IAcademicYearBusiness AcademicYearBusiness,
                                                 IProcessedReportBusiness processedreportBusiness,
                                                 IReportDefinitionBusiness reportdefinitionBusiness,
                                                 IAcademicYearOfProvinceBusiness academicYearOfProvinceBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.HealthTestReportBusiness = HealthTestReportBusiness;
            this.ProcessedReportBusiness = processedreportBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
            this.AcademicYearOfProvinceBusiness = academicYearOfProvinceBusiness;
        }


          private void SetViewData()
          {
              GlobalInfo glo = new GlobalInfo();
              Dictionary<string,object> Dic = new Dictionary<string, object>();
              // load combobox Year
              List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept_Pro(glo.SupervisingDeptID.Value);
              List<AcademicYearOfProvince> lstAcademicYearOfProvince = AcademicYearOfProvinceBusiness.SearchByProvince(glo.ProvinceID.Value,Dic).ToList();

              string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
              int curYear = AcademicYearOfProvinceBusiness.GetCurrentYear(glo.ProvinceID.GetValueOrDefault(), stringFirstStartDate);
              List<ComboObject> lstYear = new List<ComboObject>();
              foreach (var item in listAcademicYear)
              {
                  lstYear.Add(new ComboObject(item + "", item + " - "+ (item + 1)));
              }
              ViewData[HealthTestReportConstants.LIST_Year] = new SelectList(lstYear, "key", "value", curYear);
              //load combobox Period
              List<ComboObject> lstPeriod = new List<ComboObject>();
              lstPeriod.Add(new ComboObject("1", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_1));
              lstPeriod.Add(new ComboObject("2", SMAS.Web.Constants.GlobalConstants.PERIOD_EXAMINATION_2));
              ViewData[HealthTestReportConstants.LIST_Period] = new SelectList(lstPeriod, "key", "value",1);

              // load combobox appliedlevel
              List<ComboObject> lstAppliedLevel = new List<ComboObject>();
              if (glo.IsSubSuperVisingDeptRole)
              {
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY + "", "Cấp 1"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY + "", "Cấp 2"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE + "", "Nhà trẻ"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN + "", "Mẫu giáo"));
              }
              else if (glo.IsSuperVisingDeptRole)
              {
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY + "", "Cấp 1"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY + "", "Cấp 2"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY + "", "Cấp 3"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE + "", "Nhà trẻ"));
                  lstAppliedLevel.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN + "", "Mẫu giáo"));
              }
              ViewData[HealthTestReportConstants.LIST_APPLYLEVEL] = new SelectList(lstAppliedLevel, "key", "value");

          }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReport(HealthTestReportViewModel form)
        {

            ExcelHealthTestReportPGDBO ExcelHealthTestReportPGDBO = new ExcelHealthTestReportPGDBO();

            ExcelHealthTestReportPGDBO.Year = form.Year.Value;
            ExcelHealthTestReportPGDBO.DistrictID = new GlobalInfo().DistrictID.Value;
            ExcelHealthTestReportPGDBO.AppliedLevel = form.AppliedLevel == null ? 0 : form.AppliedLevel.Value;
            ExcelHealthTestReportPGDBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportPGDBO.SupervisingDeptID = new GlobalInfo().SupervisingDeptID.Value;


            string reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_PGD;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = HealthTestReportBusiness.GetHealthTestReportPGD(ExcelHealthTestReportPGDBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = HealthTestReportBusiness.CreateHealthTestReportPGD(ExcelHealthTestReportPGDBO);
                processedReport = HealthTestReportBusiness.InsertHealthTestReportPGD(ExcelHealthTestReportPGDBO, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));


        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(HealthTestReportViewModel form)
        {
            ExcelHealthTestReportPGDBO ExcelHealthTestReportPGDBO = new ExcelHealthTestReportPGDBO();
            GlobalInfo global = new GlobalInfo();

            ExcelHealthTestReportPGDBO.Year = form.Year.Value;
            //ExcelHealthTestReportPGDBO.DistrictID = global.DistrictID.Value;
            ExcelHealthTestReportPGDBO.AppliedLevel = form.AppliedLevel == null ? 0 : form.AppliedLevel.Value;
            ExcelHealthTestReportPGDBO.HealthPeriod = form.HealthPeriod.Value;
            ExcelHealthTestReportPGDBO.SupervisingDeptID = global.SupervisingDeptID.Value;

            Stream excel = HealthTestReportBusiness.CreateHealthTestReportPGD(ExcelHealthTestReportPGDBO);
            ProcessedReport processedReport = HealthTestReportBusiness.InsertHealthTestReportPGD(ExcelHealthTestReportPGDBO, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

    }
}
