﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportResultEvaluationArea.Models;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Controllers
{
    public class SummaryResultEvaluationController : BaseController
    {
        #region properties

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;

        public SummaryResultEvaluationController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IDeclareEvaluationIndexBusiness declareEvaluationIndexBusiness,
            IDeclareEvaluationGroupBusiness declareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness evaluationDevelopmentGroupBusiness,
            IEvaluationDevelopmentBusiness evaluationDevelopmentBusiness,
            IGrowthEvaluationBusiness growthEvaluationBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.DeclareEvaluationGroupBusiness = declareEvaluationGroupBusiness;
            this.DeclareEvaluationIndexBusiness = declareEvaluationIndexBusiness;
            this.EvaluationDevelopmentGroupBusiness = evaluationDevelopmentGroupBusiness;
            this.EvaluationDevelopmentBusiness = evaluationDevelopmentBusiness;
            this.GrowthEvaluationBusiness = growthEvaluationBusiness;
        }

        #endregion


        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lstEducation.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }

            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            int? educationLevelID = lstEducation.First().EducationLevelID;

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", educationLevelID);

            //var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(u => u.DisplayName).ToList();
            }

            ViewData["classgroup"] = new SelectList(lstClass.Where(x => x.IsCombinedClass == false), "ClassProfileID", "DisplayName");
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (eduId != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", eduId);

            List<SelectListItem> lstClassItem = new List<SelectListItem>();
            List<ClassProfile> listClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(u => u.DisplayName).ToList();
            }

            if (eduId == SystemParamsInFile.Combine_Class_ID)
            {
                lstClassItem = listClass.Where(x => x.IsCombinedClass == true)
                                .Select(u => new SelectListItem { Value = u.CombinedClassCode.ToUpper(), Text = u.CombinedClassCode, Selected = false })
                                .Distinct()
                                .ToList();

                lstClassItem = lstClassItem.GroupBy(p => new { p.Value }).Select(g => g.First()).ToList();
            }
            else
            {
                lstClassItem = listClass.Where(x => x.IsCombinedClass == false)
                           .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                           .ToList();
            }

            return Json(lstClassItem);
        }

        public Stream ExportExcel(int? educationLevelID, string classID)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.SummaryResultEvaluation + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet newSheet = null;

            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                // Chọn nhóm lớp không ghép
                int classIDMain = Int32.Parse(classID.ToString());
                lstClassProfile = lstClassProfile
                                    .Where(x => x.IsCombinedClass != true &&
                                                x.EducationLevelID == educationLevelID).ToList(); //
                if (classIDMain != 0)
                {
                    lstClassProfile = lstClassProfile.Where(x => x.ClassProfileID == classIDMain).ToList();
                }
            }
            else
                // Chọn nhóm lớp ghép 
                lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == true).ToList();

            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            sheet.SetCellValue("A2", school.SupervisingDept.SupervisingDeptName.ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string Title = "TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ ";
            sheet.SetCellValue("A5", Title);

            #region lay list can thiet

            // tong hox sinh co trong nhom lop 
            #region
            var SumPupilInClassGroup = (List<PupilProfile>)(from a in PupilProfileBusiness.All
                                                            join b in PupilOfClassBusiness.All on a.PupilProfileID equals b.PupilID
                                                            where a.CurrentSchoolID == _globalInfo.SchoolID && a.CurrentAcademicYearID == _globalInfo.AcademicYearID &&
                                                                  a.IsActive == true &&
                                                                  b.AcademicYearID == _globalInfo.AcademicYearID && b.SchoolID == _globalInfo.SchoolID && b.Status == 1
                                                            select a).ToList();
            #endregion

            List<int> lstEducaionLevelID = lstClassProfile.Select(x => x.EducationLevelID).Distinct().ToList();

            IQueryable<EvaluationDevelopment> queryEvaluationDevelopment = EvaluationDevelopmentBusiness.All.Where(x => lstEducaionLevelID.Contains(x.EducationLevelID)
                                                                                                                    && x.IsActive);
            IQueryable<EvaluationDevelopmentGroup> queryEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.All
                                                                                        .Where(x => lstEducaionLevelID.Contains(x.EducationLevelID)
                                                                                                && x.IsActive);

            IQueryable<DeclareEvaluationGroup> queryDeclareEvalutionGroup = DeclareEvaluationGroupBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                                            && x.SchoolID == _globalInfo.SchoolID
                                                                                                            && lstEducaionLevelID.Contains(x.EducationLevelID));

            IQueryable<DeclareEvaluationIndex> queryDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                                            && x.SchoolID == _globalInfo.SchoolID
                                                                                                            && lstEducaionLevelID.Contains(x.EducationLevelID));

            List<int> lstDeclareEvaluationIndexID = new List<int>();
            var temptListDEI = queryDeclareEvaluationIndex.ToList();
            var temptListDEG = queryDeclareEvalutionGroup.ToList();

            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                for (int j = 0; j < temptListDEG.Count(); j++)
                {
                    var lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                    && x.ClassID == lstClassProfile[i].ClassProfileID
                                                                    && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID)
                                                                    .Select(x => x.DeclareEvaluationIndexID).ToList();
                    if (lstDEI_ID.Count() == 0)
                    {
                        lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                   && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID
                                                                   && !x.ClassID.HasValue && x.ClassID == null)
                                                                  .Select(x => x.DeclareEvaluationIndexID).ToList();
                    }
                    if (lstDEI_ID.Count() > 0)
                    {
                        lstDeclareEvaluationIndexID.AddRange(lstDEI_ID);
                    }
                }
            }
            queryDeclareEvaluationIndex = queryDeclareEvaluationIndex.Where(x => lstDeclareEvaluationIndexID.Contains(x.DeclareEvaluationIndexID));

            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();
            List<int> lstEvaluationGroupID = temptListDEG.Select(p => p.EvaluationGroupID).Distinct().ToList();
            List<int> lstEvaluationindexID = temptListDEI.Select(p => p.EvaluationDevelopmentID).Distinct().ToList();

            #region // tong hoc sinh co trong da duoc danh gia
            var Sumpupil = (from c in GrowthEvaluationBusiness.All
                            join d in PupilProfileBusiness.All on c.PupilID equals d.PupilProfileID
                            join e in PupilOfClassBusiness.All on d.PupilProfileID equals e.PupilID
                            where
                                c.SchoolID == _globalInfo.SchoolID && c.AcademicYearID == _globalInfo.AcademicYearID &&
                                d.IsActive == true &&
                                e.Status == 1
                                && lstClassID.Contains(c.ClassID)
                                && lstEvaluationGroupID.Contains(c.DeclareEvaluationGroupID)
                                && lstEvaluationindexID.Contains(c.DeclareEvaluationIndexID)
                            select new ListBillEvaluation
                            {
                                academicID = d.CurrentAcademicYearID,
                                schoolID = d.CurrentSchoolID,
                                classID = c.ClassID,
                                CombineClassCode = d.ClassProfile.CombinedClassCode,
                                classIDGrow = d.CurrentClassID,
                                evaluationDevelopmentGroupID = c.DeclareEvaluationGroupID,
                                evaluationDevelopmentID = c.DeclareEvaluationIndexID,
                                pupilID = d.PupilProfileID,
                                evaluationType = c.EvaluationType,
                            }).ToList();
            #endregion

            #region // danh sách lĩnh vực
            List<ListModel> lstEDGandED = (from deg in queryDeclareEvalutionGroup
                                           join eg in queryEvaluationDevelopmentGroup on deg.EvaluationGroupID equals eg.EvaluationDevelopmentGroupID
                                           join dei in queryDeclareEvaluationIndex on deg.EvaluationGroupID equals dei.EvaluationDevelopmentGroID
                                           join ed in queryEvaluationDevelopment on dei.EvaluationDevelopmentID equals ed.EvaluationDevelopmentID
                                           where eg.IsActive
                                           && ed.IsActive
                                           orderby eg.OrderID, ed.OrderID
                                           select new ListModel
                                           {
                                               ClassID = dei.ClassID,
                                               educationLevelID = deg.EducationLevelID,
                                               EvaluationDevelopmentGroupID = eg.EvaluationDevelopmentGroupID,
                                               EvaluationDevelopmentID = ed.EvaluationDevelopmentID,
                                               EvaluationDevelopmentGroupName = eg.EvaluationDevelopmentGroupName,
                                               EvaluationDevelopmentName = ed.EvaluationDevelopmentName,
                                               EvaluationDevelopmentCode = ed.EvaluationDevelopmentCode,
                                               OrderEvaluationGroup = eg.OrderID,
                                               OrderID = ed.OrderID
                                           }).OrderBy(x => x.OrderEvaluationGroup).Distinct().ToList();

            // danh sach ID linh vuc 
            List<ListModel> lstEvaluationDevGroup = new List<ListModel>();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstEvaluationDevGroup = (from lst in lstEDGandED
                                         group lst by lst.EvaluationDevelopmentGroupID into result
                                         select new ListModel()
                                         {
                                             OrderEvaluationGroup = result.FirstOrDefault().OrderEvaluationGroup,
                                             EvaluationDevelopmentGroupName = result.FirstOrDefault().EvaluationDevelopmentGroupName,
                                             EvaluationDevelopmentGroupID = result.Key
                                         }).OrderBy(x => x.OrderEvaluationGroup).ToList();
            }
            #endregion
            #endregion

            IDictionary<string, object> dicData = new Dictionary<string, object>();
            dicData.Add("SumPupilInClassGroup", SumPupilInClassGroup);
            dicData.Add("Sumpupil", Sumpupil);
            dicData.Add("lstEDGandED", lstEDGandED);
            dicData.Add("lstEvaluationDevGroup", lstEvaluationDevGroup);
            //dicData.Add("lstEDGName", lstEDGName);
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicData.Add("educationLevelID", educationLevelID);

            // xuat nhieu sheet
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                //khi chọn lớp không ghép
                for (int i = 0; i < lstClassProfile.Count(); i++)
                {
                    newSheet = oBook.CopySheetToLast(sheet);
                    this.SetValueToFile(newSheet, dicData, lstClassProfile[i].ClassProfileID, lstClassProfile[i].DisplayName, objAca);
                }
            }
            else
            {
                if (classID != "0")
                    lstClassProfile = lstClassProfile.Where(x => x.CombinedClassCode.ToUpper() == classID).ToList();

                lstClassProfile = lstClassProfile.OrderBy(x => x.CombinedClassCode).ToList();
                //khi chọn lớp ghép
                for (int i = 0; i < lstClassProfile.Count(); i++)
                {
                    int classProfileID = lstClassProfile[i].ClassProfileID;
                    int educationLevel = lstClassProfile[i].EducationLevelID;
                    var lstEDGandEDTempt = lstEDGandED.Where(x => x.educationLevelID == educationLevel && x.ClassID == classProfileID).ToList();
                    if (lstEDGandEDTempt.Count() == 0)
                    {
                        lstEDGandEDTempt = lstEDGandED.Where(x => x.educationLevelID == educationLevel && !x.ClassID.HasValue).ToList();
                    }

                    lstEvaluationDevGroup = (from lst in lstEDGandEDTempt
                                             group lst by lst.EvaluationDevelopmentGroupID into result
                                             select new ListModel()
                                             {
                                                 OrderEvaluationGroup = result.FirstOrDefault().OrderEvaluationGroup,
                                                 EvaluationDevelopmentGroupName = result.FirstOrDefault().EvaluationDevelopmentGroupName,
                                                 EvaluationDevelopmentGroupID = result.Key
                                             }).OrderBy(x => x.OrderEvaluationGroup).ToList();

                    newSheet = oBook.CopySheetToLast(sheet);
                    this.SetValueToFileCombineClass(newSheet,
                        dicData,
                        lstClassProfile[i].ClassProfileID,
                        lstClassProfile[i].DisplayName,
                        lstClassProfile[i].EducationLevelID,
                        lstClassProfile[i].CombinedClassCode,
                        objAca,
                        lstEvaluationDevGroup);
                }
            }
            sheet.Delete();

            return oBook.ToStream();
        }

        private void SetValueToFile(IVTWorksheet sheet,
            IDictionary<string, object> dicData,
            int classId,
            string className,
            AcademicYear objAca)
        {
            #region fill data
            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            string TitleYear = "Lớp " + className.Trim() + " Năm học " + objAca.DisplayTitle;
            sheet.SetCellValue("A6", TitleYear);
            int EducationLevelID = (int)dicData["educationLevelID"];
            sheet.GetRange(5, 1, 5, EducationLevelID == 18 ? 9 : 7).Merge();
            sheet.GetRange(6, 1, 6, EducationLevelID == 18 ? 9 : 7).Merge();
            sheet.Name = className;

            List<PupilProfile> SumPupilInClassGroup = (List<PupilProfile>)dicData["SumPupilInClassGroup"];
            List<ListBillEvaluation> Sumpupil = (List<ListBillEvaluation>)dicData["Sumpupil"];
            // List<int> lstEDGId = (List<int>)dicData["lstEDGId"];
            //List<string> lstEDGName = (List<string>)dicData["lstEDGName"];
            List<ListModel> lstEDGandED = (List<ListModel>)dicData["lstEDGandED"];
            List<ListModel> lstEvaluationDevGroup = (List<ListModel>)dicData["lstEvaluationDevGroup"];

            //lay danh sach lop hoc theo khoi
            SumPupilInClassGroup = SumPupilInClassGroup.Where(x => x.CurrentClassID == classId).ToList();
            List<ListBillEvaluation> tmp = Sumpupil.Where(x => x.classID == classId && x.classIDGrow == classId &&
                                            x.schoolID == _globalInfo.SchoolID && x.academicID == _globalInfo.AcademicYearID).ToList();

            //List<ListBillEvaluation> lstGrow = (from a in lstEDGandED
            //                                    join b in Sumpupil on a.EvaluationDevelopmentID equals b.evaluationDevelopmentID
            //                                    where a.ClassID == b.classID
            //                                    select b).ToList();
            List<int> SumPupilInClassGrouptemp = SumPupilInClassGroup.Select(x => x.PupilProfileID).ToList();
            List<int> Sumpupiltemp = tmp.Select(x => x.pupilID).ToList();
            sheet.SetCellValue("C8", SumPupilInClassGrouptemp.Distinct().Count());

            int startRow = 11;
            List<ClassProfile> lstCPtmp = new List<ClassProfile>();
            string formular = string.Empty;
            int stt = 0;
            int countEvaluation = 0;

            //kiem tra co hien ± hay khong
            if (EducationLevelID == 18)
            {
                sheet.SetCellValue("H9", "±");
                sheet.GetRange("H9", "I9").Merge();
                sheet.SetCellValue("H10", "SL");
                sheet.SetCellValue("I10", "%");
                sheet.GetRange("H9", "I10").SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            for (int i = 0; i < lstEvaluationDevGroup.Count; i++)
            {
                stt = stt + 1;
                sheet.SetCellValue(startRow, 1, stt);
                sheet.SetCellValue(startRow, 2, lstEvaluationDevGroup[i].EvaluationDevelopmentGroupName);

                List<ListModel> lstEDtemp = new List<ListModel>();
                lstEDtemp = lstEDGandED.Where(x => x.EvaluationDevelopmentGroupID == lstEvaluationDevGroup[i].EvaluationDevelopmentGroupID && x.ClassID == classId).OrderBy(x => x.OrderID).ToList();
                countEvaluation = lstEDtemp.Count();

                sheet.GetRange(startRow, 1, countEvaluation == 0 ? countEvaluation + startRow : countEvaluation + startRow - 1, 1).Merge();
                sheet.GetRange(startRow, 2, countEvaluation == 0 ? countEvaluation + startRow : countEvaluation + startRow - 1, 2).Merge();

                for (int j = 0; j < lstEDtemp.Count(); j++)
                {
                    int count1 = 0;
                    int count2 = 0;
                    int count3 = 0;

                    sheet.SetCellValue(startRow, 3, lstEDtemp[j].EvaluationDevelopmentCode);
                    sheet.GetRange(startRow, 3, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    List<ListBillEvaluation> lstGrowTemp = new List<ListBillEvaluation>();
                    lstGrowTemp = tmp.Where(x => x.evaluationDevelopmentGroupID == lstEvaluationDevGroup[i].EvaluationDevelopmentGroupID
                        && x.evaluationDevelopmentID == lstEDtemp[j].EvaluationDevelopmentID && x.classID == classId).ToList();

                    for (int k = 0; k < lstGrowTemp.Count(); k++)
                    {
                        if (lstGrowTemp[k].evaluationType == 1)
                        {
                            count1 = count1 + 1;
                        }
                        else if (lstGrowTemp[k].evaluationType == 2)
                        {
                            count2 = count2 + 1;
                        }
                        else if (lstGrowTemp[k].evaluationType == 3)
                        {
                            count3 = count3 + 1;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    sheet.SetCellValue(startRow, 4, count1);

                    formular = "=IF(C8=0,0,ROUND(D" + startRow + "*100/C8,2))";
                    sheet.SetCellValue(startRow, 5, formular);

                    sheet.SetCellValue(startRow, 6, count2);
                    formular = "=IF(C8=0,0,ROUND(F" + startRow + "*100/C8,2))";
                    sheet.SetCellValue(startRow, 7, formular);

                    if (EducationLevelID == 18)
                    {
                        sheet.SetCellValue(startRow, 8, count3);
                        formular = "=IF(C8=0,0,ROUND(H" + startRow + "*100/C8,2))";
                        sheet.SetCellValue(startRow, 9, formular);
                    }
                    sheet.SetRowHeight(startRow, 16.5);
                    sheet.GetRange(startRow, 4, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;
                }

                if (lstEDtemp.Count == 0)
                    startRow++;

                sheet.GetRange(11, 2, startRow - 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(11, 1, startRow - 1, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(11, 4, startRow - 1, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 5, startRow - 1, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 6, startRow - 1, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 7, startRow - 1, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 8, startRow - 1, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 9, startRow - 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 2, startRow - 1, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 12, false, false);
            }
            sheet.GetRange(11, 3, startRow - 1, EducationLevelID == 18 ? 9 : 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            sheet.FitAllColumnsOnOnePage = true;

            #endregion
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className);
            sheet.SetFontName("Times New Roman", 12);
        }

        private void SetValueToFileCombineClass(IVTWorksheet sheet,
            IDictionary<string, object> dicData,
            int classId,
            string className,
            int EducationLevelID,
            string CombineClassCode,
            AcademicYear objAca,
            List<ListModel> lstEvaluationDevGroup)
        {
            #region fill data
            className = className.Replace("Lớp", "");
            className = className.Replace("lớp", "");
            string TitleYear = "Lớp " + className.Trim() + "(" + CombineClassCode + ")" + " Năm học " + objAca.DisplayTitle;
            sheet.SetCellValue("A6", TitleYear);
            sheet.GetRange(5, 1, 5, EducationLevelID == 18 ? 9 : 7).Merge();
            sheet.GetRange(6, 1, 6, EducationLevelID == 18 ? 9 : 7).Merge();
            sheet.Name = className;

            List<PupilProfile> SumPupilInClassGroup = (List<PupilProfile>)dicData["SumPupilInClassGroup"];
            List<ListBillEvaluation> Sumpupil = (List<ListBillEvaluation>)dicData["Sumpupil"];
            List<ListModel> lstEDGandED = (List<ListModel>)dicData["lstEDGandED"];
            //List<EvaluationDevelopment> lstGrow1 = (List<EvaluationDevelopment>)dicData["lstGrow"];

            //lay danh sach lop hoc theo khoi
            SumPupilInClassGroup = SumPupilInClassGroup.Where(x => x.CurrentClassID == classId).ToList();
            Sumpupil = Sumpupil.Where(x => x.classID == classId && x.classIDGrow == classId &&
                                            x.schoolID == _globalInfo.SchoolID && x.academicID == _globalInfo.AcademicYearID).ToList();

            //List<ListBillEvaluation> lstGrow = (from a in lstEDGandED
            //                                    join b in Sumpupil on a.EvaluationDevelopmentID equals b.evaluationDevelopmentID
            //                                    select b).ToList();
            List<int> SumPupilInClassGrouptemp = SumPupilInClassGroup.Select(x => x.PupilProfileID).ToList();
            List<int> Sumpupiltemp = Sumpupil.Select(x => x.pupilID).ToList();
            sheet.SetCellValue("C8", SumPupilInClassGrouptemp.Distinct().Count());

            int startRow = 11;
            List<ClassProfile> lstCPtmp = new List<ClassProfile>();
            string formular = string.Empty;
            int stt = 0;
            int countEvaluation = 0;
            //kiem tra co hien ± hay khong
            if (EducationLevelID == 18)
            {
                sheet.SetCellValue("H9", "±");
                sheet.GetRange("H9", "I9").Merge();
                sheet.SetCellValue("H10", "SL");
                sheet.SetCellValue("I10", "%");
                sheet.GetRange("H9", "I10").SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            for (int i = 0; i < lstEvaluationDevGroup.Count; i++)
            {
                stt = stt + 1;
                sheet.SetCellValue(startRow, 1, stt);
                sheet.SetCellValue(startRow, 2, lstEvaluationDevGroup[i].EvaluationDevelopmentGroupName);

                List<ListModel> lstEDtemp = new List<ListModel>();
                lstEDtemp = lstEDGandED.Where(x => x.EvaluationDevelopmentGroupID == lstEvaluationDevGroup[i].EvaluationDevelopmentGroupID && x.ClassID == classId)
                                        .OrderBy(x => x.OrderID).ToList();
                countEvaluation = lstEDtemp.Count();

                sheet.GetRange(startRow, 1, countEvaluation == 0 ? countEvaluation + startRow : countEvaluation + startRow - 1, 1).Merge();
                sheet.GetRange(startRow, 2, countEvaluation == 0 ? countEvaluation + startRow : countEvaluation + startRow - 1, 2).Merge();

                for (int j = 0; j < lstEDtemp.Count(); j++)
                {
                    int count1 = 0;
                    int count2 = 0;
                    int count3 = 0;

                    sheet.SetCellValue(startRow, 3, lstEDtemp[j].EvaluationDevelopmentCode);
                    sheet.GetRange(startRow, 3, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    List<ListBillEvaluation> lstGrowTemp = new List<ListBillEvaluation>();
                    lstGrowTemp = Sumpupil.Where(x => x.evaluationDevelopmentGroupID == lstEvaluationDevGroup[i].EvaluationDevelopmentGroupID
                                    && x.evaluationDevelopmentID == lstEDtemp[j].EvaluationDevelopmentID && x.classID == classId).ToList();

                    for (int k = 0; k < lstGrowTemp.Count(); k++)
                    {
                        if (lstGrowTemp[k].evaluationType == 1)
                        {
                            count1 = count1 + 1;
                        }
                        else if (lstGrowTemp[k].evaluationType == 2)
                        {
                            count2 = count2 + 1;
                        }
                        else if (lstGrowTemp[k].evaluationType == 3)
                        {
                            count3 = count3 + 1;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    sheet.SetCellValue(startRow, 4, count1);

                    formular = "=IF(C8=0,0,ROUND(D" + startRow + "*100/C8,2))";
                    sheet.SetCellValue(startRow, 5, formular);

                    sheet.SetCellValue(startRow, 6, count2);
                    formular = "=IF(C8=0,0,ROUND(F" + startRow + "*100/C8,2))";
                    sheet.SetCellValue(startRow, 7, formular);

                    if (EducationLevelID == 18)
                    {
                        sheet.SetCellValue(startRow, 8, count3);
                        formular = "=IF(C8=0,0,ROUND(H" + startRow + "*100/C8,2))";
                        sheet.SetCellValue(startRow, 9, formular);
                    }
                    sheet.SetRowHeight(startRow, 16.5);
                    sheet.GetRange(startRow, 4, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;
                }

                if (lstEDtemp.Count == 0)
                    startRow++;

                sheet.GetRange(11, 2, startRow - 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(11, 1, startRow - 1, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(11, 4, startRow - 1, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 5, startRow - 1, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 6, startRow - 1, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 7, startRow - 1, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 8, startRow - 1, 8).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 9, startRow - 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(11, 2, startRow - 1, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 12, false, false);
            }
            sheet.GetRange(11, 3, startRow - 1, EducationLevelID == 18 ? 9 : 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            sheet.FitAllColumnsOnOnePage = true;

            #endregion
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className + "(" + CombineClassCode + ")");
            sheet.SetFontName("Times New Roman", 12);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNumberOfClassReport_03(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.SummaryResultEvaluation);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                {
                                                    {"AcademicYearID",_globalInfo.AcademicYearID},
                                                    {"SchoolID",_globalInfo.SchoolID},
                                                    {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                    {"EducationLevelID",educationLevelID}                                                    
                                                };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dic.Add("ClassID", classID);
            if (reportDef.IsPreprocessed == true)
            {
                // viet them ham
                processedReport = PupilOfClassBusiness.GetProcessReportSummaryEvaluation(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(educationLevelID, classID);
                processedReport = PupilOfClassBusiness.InsertProcessReportSummaryEvaluation(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicInsert.Add("ClassID", classID);

            Stream excel = this.ExportExcel(educationLevelID, classID);
            processedReport = PupilOfClassBusiness.InsertProcessReportSummaryEvaluation(dicInsert, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.SummaryResultEvaluation,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.SummaryResultEvaluation,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

    }
}
