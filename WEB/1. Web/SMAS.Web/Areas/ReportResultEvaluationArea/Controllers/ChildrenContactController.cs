﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportResultEvaluationArea.Models;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Controllers
{
    public class ChildrenContactController : BaseController
    {

        #region properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IPhysicalExaminationBusiness PhysicalExaminationBusiness;

        public ChildrenContactController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IDeclareEvaluationIndexBusiness declareEvaluationIndexBusiness,
            IDeclareEvaluationGroupBusiness declareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness evaluationDevelopmentGroupBusiness,
            IEvaluationDevelopmentBusiness evaluationDevelopmentBusiness,
            IGrowthEvaluationBusiness growthEvaluationBusiness,
            IPhysicalTestBusiness physicalTestBusiness,
            IMonitoringBookBusiness monitoringBookBusiness,
            IPupilAbsenceBusiness pupilAbsenceBusiness,
            IGoodChildrenTicketBusiness goodChildrenTicketBusiness,
            IClassAssigmentBusiness classAssigmentBusiness,
            IPhysicalExaminationBusiness physicalExaminationBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.DeclareEvaluationGroupBusiness = declareEvaluationGroupBusiness;
            this.DeclareEvaluationIndexBusiness = declareEvaluationIndexBusiness;
            this.EvaluationDevelopmentGroupBusiness = evaluationDevelopmentGroupBusiness;
            this.EvaluationDevelopmentBusiness = evaluationDevelopmentBusiness;
            this.GrowthEvaluationBusiness = growthEvaluationBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.GoodChildrenTicketBusiness = goodChildrenTicketBusiness;
            this.ClassAssigmentBusiness = classAssigmentBusiness;
            this.PhysicalExaminationBusiness = physicalExaminationBusiness;
        }

        #endregion

        //
        // GET: /ReportResultEvaluationArea/ChildrenContact/

        private List<KeyValuePair<int, int>> getListMonth()
        {
            return PhysicalExaminationBusiness.GetListMonthByAcademiId(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
        }

        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels.ToList();
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            if (checkCombineClass)
            {
                lstEducation.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            int? educationLevelID = lstEducation.First().EducationLevelID;

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", educationLevelID);

            List<ClassProfile> lstClass = new List<ClassProfile>();
            //var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(c => c.OrderNumber ?? 0).ThenBy(c => c.DisplayName).ToList();
            }


            ViewData["classgroup"] = new SelectList(lstClass.Where(x => x.IsCombinedClass == false).ToList(), "ClassProfileID", "DisplayName");

            var listMonth = getListMonth();
            var montCurrent = DateTime.Now.Month;
            ViewData["listMonth"] = listMonth.Select(s => new SelectListItem { Value = s.Value.ToString() + s.Key.ToString(), Text = "Tháng " + s.Key.ToString(), Selected = (s.Key == montCurrent) ? true : false }).ToList();

            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            if (eduId != SystemParamsInFile.Combine_Class_ID)
                dicClass.Add("EducationLevelID", eduId);

            List<SelectListItem> listClassItem = new List<SelectListItem>();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(u => u.DisplayName).ToList();
            }

            if (eduId == SystemParamsInFile.Combine_Class_ID)
            {
                listClassItem = lstClass.Where(x => x.IsCombinedClass == true)
                                        .Select(u => new SelectListItem { Value = u.CombinedClassCode.ToUpper(), Text = u.CombinedClassCode, Selected = false })
                                        .Distinct().ToList();

                listClassItem = listClassItem.GroupBy(p => new { p.Value }).Select(g => g.First()).ToList();
            }
            else
            {
                listClassItem = lstClass.Where(x => x.IsCombinedClass == false)
                                       .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                       .ToList();
            }

            return Json(listClassItem);
        }

        int startRow = 2;
        public Stream ExportExcel(int? month, int? educationLevelID, string classID)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.ChildrenContact + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet newSheetInfo = oBook.GetSheet(2);
            IVTWorksheet sheetNumberWeek4 = oBook.GetSheet(3);
            IVTWorksheet sheetNumberWeek5 = oBook.GetSheet(4);
            IVTWorksheet newSheet = null;
            IVTWorksheet sheetInfo = null;

            IVTRange rangNumberWeek4 = sheetNumberWeek4.GetRange("A2", "S10");
            IVTRange rangNumberWeek5 = sheetNumberWeek5.GetRange("A2", "S10");

            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            #region danh sách cần thiết
            // load danh sách trẻ và lớp
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                //combobox không chọn tất cả
                int classIDMain = Int32.Parse(classID.ToString());
                lstClassProfile = lstClassProfile
                                    .Where(x => x.IsCombinedClass != true &&
                                                x.EducationLevelID == educationLevelID).ToList();
                if (classIDMain != 0)
                {
                    lstClassProfile = lstClassProfile.Where(x => x.ClassProfileID == classIDMain).ToList();
                }
            }
            else
            {
                lstClassProfile = lstClassProfile
                                    .Where(x => x.IsCombinedClass == true).ToList();

                if (classID != "0")
                {
                    lstClassProfile = lstClassProfile.Where(x => x.CombinedClassCode.ToUpper() == classID).ToList();
                }
            }
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();
            List<string> lstClassName = lstClassProfile.Select(p => p.DisplayName).Distinct().ToList();

            List<PupilProfileBO> lstPupilBO = this.PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(a => a.ProfileStatus == 1).OrderBy(a => a.OrderInClass).ToList();
            lstPupilBO = lstPupilBO.Where(x => lstClassID.Contains(x.CurrentClassID)).ToList();

            // danh sách lĩnh vực được đánh giá
            #region
            var lstEDG = (List<EvaluationDevelopmentGroup>)(from a in EvaluationDevelopmentGroupBusiness.All.Where(a => a.AppliedLevel == _globalInfo.AppliedLevel &&
                                                            a.IsActive == true)
                                                            select a).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                lstEDG = lstEDG.Where(x => x.EducationLevelID == educationLevelID).ToList();

            List<ListModel> lstDEG = (from DEG in DeclareEvaluationGroupBusiness.All.ToList()
                                      join EDG in lstEDG on DEG.EvaluationGroupID equals EDG.EvaluationDevelopmentGroupID
                                      where DEG.AcademicYearID == _globalInfo.AcademicYearID &&
                                            DEG.SchoolID == _globalInfo.SchoolID &&
                                          //DEG.EducationLevelID == educationLevelID &&
                                            EDG.AppliedLevel == _globalInfo.AppliedLevel &&
                                            EDG.IsActive == true //&&
                                      //EDG.EducationLevelID == educationLevelID
                                      select new ListModel
                                      {
                                          educationLevelID = DEG.EducationLevelID,
                                          educationLevelIDEDG = EDG.EducationLevelID,
                                          EvaluationDevelopmentGroupID = DEG.EvaluationGroupID,
                                          EvaluationDevelopmentGroupName = EDG.EvaluationDevelopmentGroupName,
                                          ProvinceID = EDG.ProvinceID
                                      }).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                lstDEG = lstDEG.Where(x => x.educationLevelID == educationLevelID && x.educationLevelIDEDG == educationLevelID).ToList();

            #endregion

            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }

            int yearMain = Int32.Parse(month.ToString().Substring(0, 4));
            int monthMain = Int32.Parse(month.ToString().Substring(4));

            //danh sách chỉ số
            #region

            var lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID
                                                                                    && x.AcademicYearID == _globalInfo.AcademicYearID);

            var lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All.Where(g => g.AcademicYearID == _globalInfo.AcademicYearID
                                                                                            && g.SchoolID == _globalInfo.SchoolID);

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.EducationLevelID == educationLevelID);

                /* var lstDeclareEvaluationIndexForClass = lstDeclareEvaluationIndex.Where(x => x.ClassID.HasValue && x.EducationLevelID == educationLevelID);
                 if (lstDeclareEvaluationIndexForClass.Count() > 0)
                 {
                     lstDeclareEvaluationIndex = lstDeclareEvaluationIndexForClass;
                 }*/
            }
            else
            {
                List<int> lstEducationLevel = lstClassProfile.Select(x => x.EducationLevelID).ToList();
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => lstEducationLevel.Contains(x.EducationLevelID));
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstEducationLevel.Contains(x.EducationLevelID));
            }

            List<int> lstDeclareEvaluationIndexID = new List<int>();
            var temptListDEI = lstDeclareEvaluationIndex.ToList();
            var temptListDEG = lstDeclareEvaluationGroup.ToList();
            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                for (int j = 0; j < temptListDEG.Count(); j++)
                {
                    var lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                    && x.ClassID == lstClassProfile[i].ClassProfileID
                                                                    && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID)
                                                                    .Select(x => x.DeclareEvaluationIndexID).ToList();
                    if (lstDEI_ID.Count() == 0)
                    {
                        lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                   && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID
                                                                   && !x.ClassID.HasValue && x.ClassID == null)
                                                                  .Select(x => x.DeclareEvaluationIndexID).ToList();
                    }
                    if (lstDEI_ID.Count() > 0)
                    {
                        lstDeclareEvaluationIndexID.AddRange(lstDEI_ID);
                    }
                }
            }
            lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstDeclareEvaluationIndexID.Contains(x.DeclareEvaluationIndexID));

            var lstDEI = (from c in GrowthEvaluationBusiness.All
                          join d in PupilProfileBusiness.All on c.PupilID equals d.PupilProfileID
                          join e in PupilOfClassBusiness.All on d.PupilProfileID equals e.PupilID
                          join f in lstDeclareEvaluationGroup on c.DeclareEvaluationGroupID equals f.EvaluationGroupID
                          join g in lstDeclareEvaluationIndex on c.DeclareEvaluationIndexID equals g.EvaluationDevelopmentID
                          join h in EvaluationDevelopmentBusiness.All on g.EvaluationDevelopmentID equals h.EvaluationDevelopmentID
                          where
                              c.SchoolID == _globalInfo.SchoolID && c.AcademicYearID == _globalInfo.AcademicYearID
                              && ((c.CreateDate.Month == monthMain || c.ModifiedDate.Value.Month == monthMain) &&
                              c.CreateDate.Year == yearMain) &&
                              d.CurrentSchoolID == _globalInfo.SchoolID && d.CurrentAcademicYearID == _globalInfo.AcademicYearID &&
                              d.IsActive == true &&
                              e.AcademicYearID == _globalInfo.AcademicYearID && e.SchoolID == _globalInfo.SchoolID && e.Status == 1 &&
                              //f.EducationLevelID == educationLevelID &&
                              //g.EducationLevelID == educationLevelID &&
                              //h.EducationLevelID == educationLevelID && 
                              h.IsActive == true
                          select new ListBillEvaluation
                          {
                              educationID = h.EducationLevelID,
                              educationIDDEG = f.EducationLevelID,
                              educationIDDEI = g.EducationLevelID,
                              academicID = d.CurrentAcademicYearID,
                              schoolID = d.CurrentSchoolID,
                              classID = c.ClassID,
                              classIDGrow = d.CurrentClassID,
                              evaluationDevelopmentGroupID = c.DeclareEvaluationGroupID,
                              evaluationDevelopmentID = c.DeclareEvaluationIndexID,
                              evaluationDevelopmentName = h.EvaluationDevelopmentName,
                              pupilID = d.PupilProfileID,
                              evaluationType = c.EvaluationType,
                              ClassID2 = g.ClassID,
                              DeclareEvaluationIndexID = g.DeclareEvaluationIndexID
                          }).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                lstDEI = lstDEI.Where(x => x.educationID == educationLevelID &&
                                           x.educationIDDEI == educationLevelID &&
                                           x.educationIDDEG == educationLevelID).ToList();
            #endregion

            string monthForHealth = monthMain.ToString().Length == 1 ? "0" + monthMain.ToString() : monthMain.ToString();
            string yearPlus = yearMain.ToString() + monthForHealth;
            int yearForHealth = Int32.Parse(yearPlus);

            //danh sách sức khỏe
            List<InfoEvaluationHealthy> lstHealthOfPupil = (from pt in PhysicalExaminationBusiness.All
                                                            where pt.MonthID == yearForHealth
                                                            select new InfoEvaluationHealthy
                                                            {
                                                                PupilID = pt.PupilID,
                                                                Height = pt.Height.HasValue ? pt.Height.Value : 0,
                                                                Weight = pt.Weight.HasValue ? pt.Weight.Value : 0,
                                                                AcademicYear = pt.AcademicYearID,
                                                                SchoolID = pt.SchoolID,
                                                                ClassID = pt.ClassID,
                                                                MonitoringDate = pt.CreateDate,
                                                                HeightStatus = pt.PhysicalHeightStatus,
                                                                WeightStatus = pt.PhysicalWeightStatus
                                                            }).ToList();
            //danh sách nghỉ học
            List<PupilAbsence> lstPupilAbenceEdu = PupilAbsenceBusiness.All
               .Where(o => o.AbsentDate.Month == monthMain &&
                    o.AbsentDate.Year == yearMain &&
                    o.SchoolID == _globalInfo.SchoolID
               ).ToList();

            //danh sách bé ngoan
            List<GoodChildrenTicket> lstGoodTicket = GoodChildrenTicketBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID &&
                                                                                             x.SchoolID == _globalInfo.SchoolID &&
                                                                                             x.Month.Month == monthMain && x.Month.Year == yearMain).ToList();

            //danh sách giáo viên chủ nhiệm
            Dictionary<string, object> dicTeacher = new Dictionary<string, object>();
            dicTeacher.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicTeacher.Add("SchoolID", _globalInfo.SchoolID);
            IQueryable<ClassProfile> lst = ClassProfileBusiness.Search(dicTeacher);

            #endregion

            //xuất nhiều sheet
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("month", month);
            dic.Add("lstPupil", lstPupilBO);
            dic.Add("lstHealth", lstHealthOfPupil);
            dic.Add("lstGoodTicket", lstGoodTicket);
            dic.Add("lstPupilAbenceEdu", lstPupilAbenceEdu);

            int sttTTC = 8;
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                dic.Add("lstlstDEG", lstDEG);
                dic.Add("lstDEI", lstDEI);
                #region xuất excel khi không chọn lớp ghép
                for (int i = 0; i < lstClassID.Count(); i++)
                {
                    sttTTC++;
                    int ClassId = lstClassID[i];
                    newSheet = oBook.CopySheetToLast(sheet);

                    var checkByClass = lstGoodTicket.Where(x => x.ClassID == ClassId && x.WeekInMonth != 6
                                                                && (x.IsGood == true || !string.IsNullOrEmpty(x.Note))).ToList();

                    int maxNumberWeek = checkByClass.Count() > 0 ? checkByClass.Max(x => x.WeekInMonth) : 4;
                    if (maxNumberWeek < 4)
                        maxNumberWeek = 4;

                    this.SetValueToFile(newSheet, rangNumberWeek4, rangNumberWeek5, dic, ClassId, lstClassName[i], sttTTC, maxNumberWeek);
                    startRow = 2;
                }
                #endregion
            }
            else
            {
                #region xuất excel khi chọn lớp ghép
                List<ListModel> lstDEGTemp = new List<ListModel>();
                List<ListBillEvaluation> lstDEITemp = new List<ListBillEvaluation>();
                lstClassProfile = lstClassProfile.OrderBy(x => x.CombinedClassCode).ToList();

                List<int> lstEducationID = lstClassProfile.Select(x => x.EducationLevelID).ToList();
                var lstDeclareEvaluationGroup_Tempt = lstDeclareEvaluationGroup.Where(x => lstEducationID.Contains(x.EducationLevelID)).ToList();
                var lstDeclareEvaluationIndex_Tempt = lstDeclareEvaluationIndex.Where(x => lstEducationID.Contains(x.EducationLevelID)).ToList();

                for (int i = 0; i < lstClassProfile.Count(); i++)
                {
                    lstDeclareEvaluationIndexID = new List<int>();
                    sttTTC++;
                    int educationCombineClass = lstClassProfile[i].EducationLevelID;

                    lstDEGTemp = lstDEG.Where(x => x.educationLevelID == educationCombineClass && x.educationLevelIDEDG == educationCombineClass).ToList();
                    lstDEITemp = lstDEI.Where(x => x.educationID == educationCombineClass &&
                                               x.educationIDDEI == educationCombineClass &&
                                               x.educationIDDEG == educationCombineClass).ToList();

                    var lstDeclareEvaluationIndexForClass = lstDeclareEvaluationIndex_Tempt.Where(x => x.ClassID.HasValue &&
                                                                                x.EducationLevelID == educationCombineClass).ToList();
                    if (lstDeclareEvaluationIndexForClass.Count() > 0)
                    {
                        lstDeclareEvaluationIndexID = lstDeclareEvaluationIndexForClass.Select(x => x.DeclareEvaluationIndexID).ToList();
                        lstDEITemp = lstDEITemp.Where(x => lstDeclareEvaluationIndexID.Contains(x.DeclareEvaluationIndexID)).ToList();
                    }

                    var lstDEGTempForProvince = lstDEGTemp.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
                    if (lstDEGTempForProvince.Count() > 0)
                    {
                        lstDEGTemp = lstDEGTempForProvince;
                    }

                    newSheet = oBook.CopySheetToLast(sheet);

                    var checkByClass = lstGoodTicket.Where(x => x.ClassID == lstClassProfile[i].ClassProfileID && x.WeekInMonth != 6
                                                                && (x.IsGood == true || !string.IsNullOrEmpty(x.Note))).ToList();
                    int maxNumberWeek = checkByClass.Count() > 0 ? checkByClass.Max(x => x.WeekInMonth) : 4;
                    if (maxNumberWeek < 4)
                        maxNumberWeek = 4;

                    this.SetValueToFileCombineClass(newSheet,
                        rangNumberWeek4, rangNumberWeek5,
                        dic,
                        lstClassProfile[i].ClassProfileID,
                        lstClassProfile[i].DisplayName,
                        lstClassProfile[i].CombinedClassCode,
                        lstDEGTemp,
                        lstDEITemp,
                        sttTTC,
                        maxNumberWeek);
                    startRow = 2;
                }
                #endregion
            }

            #region dữ liệu của sheet thông tin chung
            sheetInfo = oBook.CopySheetToLast(newSheetInfo);
            int startRowInfo = 3;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            string strDate = objSP.Province.ProvinceName + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;

            int startRowFirst = 3;
            sheetInfo.SetColumnWidth(1, 15.57);
            sheetInfo.SetColumnWidth(2, 43.29);
            sheetInfo.SetCellValue("A" + startRowInfo, "Tên trường");
            sheetInfo.SetCellValue("B" + startRowInfo, _globalInfo.SchoolName);

            startRowInfo++;
            string monthName = string.Empty;
            if (month.HasValue)
            {
                monthName = month.ToString().Substring(4, month.Value.ToString().Length - 4);
            }

            sheetInfo.SetCellValue("A" + startRowInfo, "Tiêu đề 1");
            sheetInfo.SetCellValue("B" + startRowInfo, "PHIẾU LIÊN LẠC THÁNG " + monthName + " - NĂM HỌC " + objAca.DisplayTitle);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Hiệu trưởng");
            sheetInfo.SetCellValue("B" + startRowInfo, "Hiệu trưởng");
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Tên Hiệu trưởng");
            sheetInfo.SetCellValue("B" + startRowInfo, SchoolProfileBusiness.Find(_globalInfo.SchoolID).HeadMasterName);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Tiêu đề 2");
            sheetInfo.SetCellValue("B" + startRowInfo, strDate);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "GVCN");
            sheetInfo.SetCellValue("B" + startRowInfo, "Giáo viên chủ nhiệm");
            startRowInfo++;

            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                List<ClassProfile> employName = lst.ToList().Where(x => x.ClassProfileID == lstClassProfile[i].ClassProfileID
                                                                        && x.EducationLevelID == lstClassProfile[i].EducationLevelID).ToList();
                sheetInfo.SetCellValue("A" + startRowInfo, "Tên GVCN");
                sheetInfo.SetCellValue("B" + startRowInfo, employName.Count() == 0 ? "" : employName[0].Employee == null ? "" : employName[0].Employee.FullName);
                startRowInfo++;
            }

            sheetInfo.GetRange(startRowFirst, 2, startRowInfo - 1, 2).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
            sheetInfo.GetRange(startRowFirst, 1, startRowInfo - 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetInfo.Name = "Thong tin chung";
            sheetInfo.SetFontName("Calibri", 11);
            #endregion

            newSheetInfo.Delete();
            sheet.Delete();
            sheetNumberWeek4.Delete();
            sheetNumberWeek5.Delete();

            return oBook.ToStream();
        }

        private void SetValueToFile(IVTWorksheet sheet, IVTRange rangNumberWeek4, IVTRange rangNumberWeek5,
            IDictionary<string, object> dic, int ClassId, string className, int sttTTC, int numberWeek)
        {
            List<PupilProfileBO> lstPupil = (List<PupilProfileBO>)dic["lstPupil"];
            List<InfoEvaluationHealthy> lstHealthOfPupil = (List<InfoEvaluationHealthy>)dic["lstHealth"];
            List<ListModel> lstDEG = (List<ListModel>)dic["lstlstDEG"];
            List<ListBillEvaluation> lstDEITemp = (List<ListBillEvaluation>)dic["lstDEI"];
            List<GoodChildrenTicket> lstGoodTicketTemp = (List<GoodChildrenTicket>)dic["lstGoodTicket"];
            List<PupilAbsence> lstPupilAbsenceTemp = (List<PupilAbsence>)dic["lstPupilAbenceEdu"];

            int boderlast = 0;
            int boderfirst = 0;

            for (int i = 0; i < lstPupil.Count(); i++)
            {
                if (lstPupil[i].CurrentClassID == ClassId)
                {
                    #region dinh dang I
                    List<ListBillEvaluation> lstDEI = new List<ListBillEvaluation>();
                    List<GoodChildrenTicket> lstGoodTicket = new List<GoodChildrenTicket>();
                    List<PupilAbsence> lstPupilAbsence = new List<PupilAbsence>();
                    lstDEI = lstDEITemp.Where(x => x.classID == ClassId && x.classIDGrow == ClassId && x.pupilID == lstPupil[i].PupilProfileID).ToList();
                    lstGoodTicket = lstGoodTicketTemp.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).OrderBy(x => x.WeekInMonth).ToList();
                    lstPupilAbsence = lstPupilAbsenceTemp.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).ToList();
                    SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

                    #region thong tin tre
                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!B3");
                    sheet.SetCellValue("G" + startRow, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
                    sheet.GetRange(startRow, 1, startRow, 6).WrapText();
                    sheet.SetRowHeight(startRow, 21);
                    sheet.SetRowHeight(startRow + 1, 21.75);
                    sheet.GetRange(startRow, 7, startRow, 19).Merge();
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                    sheet.GetRange(startRow, 7, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("G" + startRow, "Độc lập - Tự do - Hạnh phúc");
                    sheet.GetRange(startRow - 1, 1, startRow, 6).Merge();
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.GetRange(startRow, 7, startRow, 19).Merge();
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                    sheet.GetRange(startRow, 7, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, true);
                    startRow++;

                    AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!B4");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 63.75);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Họ và tên: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetCellValue("C" + startRow, lstPupil[i].FullName);
                    sheet.GetRange(startRow, 3, startRow, 13).Merge();
                    sheet.GetRange(startRow, 3, startRow, 13).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 13).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 13).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("N" + startRow, "Lớp: ");
                    sheet.GetRange(startRow, 14, startRow, 16).Merge();
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.SetCellValue("Q" + startRow, className);
                    sheet.GetRange(startRow, 17, startRow, 19).Merge();
                    sheet.GetRange(startRow, 17, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 17, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 17, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Số ngày nghỉ: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetCellValue("C" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Count() : 0);
                    sheet.GetRange(startRow, 3, startRow, 6).Merge();
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 6).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("G" + startRow, "Có phép: ");
                    sheet.GetRange(startRow, 7, startRow, 9).Merge();
                    sheet.GetRange(startRow, 7, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 7, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 9).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("J" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Where(x => x.IsAccepted == true).Count() : 0);
                    sheet.GetRange(startRow, 10, startRow, 11).Merge();
                    sheet.GetRange(startRow, 10, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 10, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 10, startRow, 11).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("N" + startRow, "Không phép: ");
                    sheet.GetRange(startRow, 14, startRow, 16).Merge();
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 14, startRow, 16).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("Q" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Where(x => x.IsAccepted == false).Count() : 0);
                    sheet.GetRange(startRow, 17, startRow, 17).Merge();
                    sheet.GetRange(startRow, 17, startRow, 17).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 17, startRow, 17).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 17, startRow, 17).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Chiều cao: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetRowHeight(startRow + 1, 6);
                    List<InfoEvaluationHealthy> lstHeightAndWeight = lstHealthOfPupil.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).ToList();
                    string weightstatus = "";
                    string heightstatus = "";
                    if (lstHealthOfPupil.Count() != 0)
                    {
                        if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 1)
                        {
                            heightstatus = "Cao hơn so với tuổi";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 2)
                        {
                            heightstatus = "Bình thường";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 3)
                        {
                            heightstatus = "Thấp còi độ 1";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 4)
                        {
                            heightstatus = "Thấp còi độ 2";
                        }
                    }
                    sheet.SetCellValue("C" + startRow, lstHeightAndWeight.Count() == 0 ? "" : lstHeightAndWeight.FirstOrDefault().Height.ToString() + " cm - " + heightstatus);
                    sheet.GetRange(startRow, 3, startRow, 9).Merge();
                    sheet.GetRange(startRow, 3, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 9).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("J" + startRow, "Cân nặng");
                    sheet.GetRange(startRow, 10, startRow, 13).Merge();
                    sheet.GetRange(startRow, 10, startRow, 13).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 10, startRow, 13).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 10, startRow, 13).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);

                    if (lstHealthOfPupil.Count() != 0)
                    {
                        if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 1)
                        {
                            weightstatus = "Cân nặng hơn so với tuổi";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 2)
                        {
                            weightstatus = "Bình thường";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 3)
                        {
                            weightstatus = "Suy dinh dưỡng nhẹ";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 4)
                        {
                            weightstatus = "Suy dinh dưỡng nặng";
                        }
                    }

                    sheet.SetCellValue("N" + startRow, lstHeightAndWeight.Count() == 0 ? "" : lstHeightAndWeight.FirstOrDefault().Weight.ToString() + " kg - " + weightstatus);
                    sheet.GetRange(startRow, 14, startRow, 18).Merge();
                    sheet.GetRange(startRow, 14, startRow, 18).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 18).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 14, startRow, 18).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow = startRow + 2;

                    sheet.SetCellValue("A" + startRow, "I. Bé ngoan:");
                    sheet.GetRange(startRow, 1, startRow, 11).Merge();
                    sheet.GetRange(startRow, 1, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 20);
                    sheet.SetRowHeight(startRow + 1, 6);
                    startRow = startRow + 2;
                    #endregion

                    #region Bé ngoan

                    if (numberWeek == 4)
                    {
                        sheet.CopyPasteSameSize(rangNumberWeek4, startRow, 1);

                    }
                    else
                    {
                        sheet.CopyPasteSameSize(rangNumberWeek5, startRow, 1);

                    }
                    startRow += 8;
                    sheet = RenderAreaByNumberWeek(sheet, lstGoodTicket, numberWeek, startRow);
                    startRow += 1;

                    /*sheet.SetCellValue("A" + startRow, "Tuần 1 ");
                    sheet.SetCellValue("K" + startRow, "Tuần 2 "); //11,14
                    startRow = startRow + 2;
                    sheet.GetRange(startRow - 2, 1, startRow + 5, 19).SetBorder(VTBorderStyle.Double, VTBorderWeight.Thick, VTBorderIndex.All);
                    sheet.GetRange(startRow - 2, 1, startRow + 1, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                    sheet.GetRange(startRow - 2, 11, startRow + 1, 19).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                    sheet.GetRange(startRow + 2, 1, startRow + 5, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                    sheet.GetRange(startRow + 2, 11, startRow + 5, 19).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

                    sheet.GetRange(startRow - 2, 1, startRow, 3).Merge();
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 16, false, false);

                    sheet.SetRowHeight(startRow - 2, 65);
                    sheet.SetRowHeight(startRow - 1, 90);
                    sheet.SetRowHeight(startRow, 90);

                    sheet.GetRange(startRow - 2, 1, startRow, 3).WrapText();
                    sheet.GetRange(startRow - 2, 4, startRow, 10).Merge();
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetFontStyle(true, System.Drawing.Color.LightGray, false, 20, false, false);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).Merge();
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetFontStyle(true, System.Drawing.Color.Black, false, 16, false, false);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).WrapText();
                    sheet.GetRange(startRow - 2, 15, startRow, 19).Merge();
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetFontStyle(true, System.Drawing.Color.LightGray, false, 20, false, false);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).WrapText();
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Nhận xét: ");
                    sheet.GetRange(startRow, 1, startRow, 10).Merge();
                    sheet.GetRange(startRow, 1, startRow, 10).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 10).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 55);

                    sheet.SetCellValue("K" + startRow, "Nhận xét: ");
                    sheet.GetRange(startRow, 11, startRow, 19).Merge();
                    sheet.GetRange(startRow, 11, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 11, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 11, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Tuần 3 ");
                    sheet.SetCellValue("K" + startRow, "Tuần 4 "); //11,14
                    startRow = startRow + 2;

                    sheet.GetRange(startRow - 2, 1, startRow, 3).Merge();
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 16, false, false);
                    sheet.GetRange(startRow - 2, 1, startRow, 3).WrapText();
                    sheet.SetRowHeight(startRow - 2, 65);
                    sheet.SetRowHeight(startRow - 1, 90);
                    sheet.SetRowHeight(startRow, 90);
                    sheet.GetRange(startRow - 2, 4, startRow, 10).Merge();
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 4, startRow, 10).SetFontStyle(true, System.Drawing.Color.LightGray, false, 20, false, false);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).Merge();
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).SetFontStyle(true, System.Drawing.Color.Black, false, 16, false, false);
                    sheet.GetRange(startRow - 2, 11, startRow, 14).WrapText();
                    sheet.GetRange(startRow - 2, 15, startRow, 19).Merge();
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).SetFontStyle(true, System.Drawing.Color.LightGray, false, 20, false, false);
                    sheet.GetRange(startRow - 2, 15, startRow, 19).WrapText();
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Nhận xét: ");
                    sheet.GetRange(startRow, 1, startRow, 10).Merge();
                    sheet.GetRange(startRow, 1, startRow, 10).Merge();
                    sheet.GetRange(startRow, 1, startRow, 10).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 10).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 55);
                    sheet.SetCellValue("K" + startRow, "Nhận xét: ");
                    sheet.GetRange(startRow, 11, startRow, 19).Merge();
                    sheet.GetRange(startRow, 11, startRow, 19).Merge();
                    sheet.GetRange(startRow, 11, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 11, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 11, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    if (lstGoodTicket.Count() != 0)
                    {
                        if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Count() != 0)
                        {
                            sheet.SetCellValue("A" + (startRow - 8), "Tuần 1           Bé Ngoan");
                            sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                            sheet.SetCellValue("D" + (startRow - 8), "Phiếu bé ngoan");

                            sheet.GetRange(startRow - 8, 4, startRow - 8, 10).SetOrientation(45);
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Count() != 0)
                        {
                            sheet.SetCellValue("K" + (startRow - 8), "Tuần 2            Bé Ngoan");
                            sheet.SetCellValue("K" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                            sheet.SetCellValue("O" + (startRow - 8), "Phiếu bé ngoan");
                            sheet.GetRange(startRow - 8, 15, startRow - 8, 19).SetOrientation(45);
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Count() != 0)
                        {
                            sheet.SetCellValue("A" + (startRow - 4), "Tuần 3             Bé Ngoan");
                            sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                            sheet.SetCellValue("D" + (startRow - 4), "Phiếu bé ngoan");
                            sheet.GetRange(startRow - 4, 4, startRow - 4, 10).SetOrientation(45);
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Count() != 0)
                        {
                            sheet.SetCellValue("K" + (startRow - 4), "Tuần 4             Bé Ngoan");
                            sheet.SetCellValue("K" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                            sheet.SetCellValue("O" + (startRow - 4), "Phiếu bé ngoan");
                            sheet.GetRange(startRow - 4, 15, startRow - 4, 19).SetOrientation(45);
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Count() != 0)
                        {
                            sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Count() != 0)
                        {
                            sheet.SetCellValue("K" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Count() != 0)
                        {
                            sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                        }
                        if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Count() != 0)
                        {
                            sheet.SetCellValue("K" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                        }
                    }

                    sheet.SetCellValue("A" + startRow, "Kết quả cuối tháng: ");
                    sheet.GetRange(startRow, 1, startRow, 5).Merge();
                    sheet.SetRowHeight(startRow, 39);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 14, false, false);

                    sheet.SetCellValue("F" + startRow, lstGoodTicket.Count >= 4 ? "Đạt danh hiệu cháu ngoan bác Hồ" : "");
                    sheet.GetRange(startRow, 6, startRow, 19).Merge();
                    sheet.GetRange(startRow, 6, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 6, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 6, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 14, false, false);
                    startRow++;*/
                    #endregion

                    sheet.SetCellValue("A" + startRow, "II. Đánh giá sự phát triển ");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 24.75);
                    startRow++;

                    boderfirst = startRow;
                    sheet.SetCellValue("A" + startRow, "Lĩnh vực ");
                    sheet.GetRange(startRow, 1, startRow, 4).Merge();
                    sheet.SetCellValue("E" + startRow, "Đạt ");
                    sheet.GetRange(startRow, 5, startRow, 7).Merge();
                    sheet.SetCellValue("H" + startRow, "Chưa Đạt");
                    sheet.GetRange(startRow, 8, startRow, 9).Merge();
                    sheet.SetCellValue("J" + startRow, "Các chỉ số cần cố gắng");
                    sheet.GetRange(startRow, 10, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 24.75);
                    startRow++;
                    #endregion;

                    #region Lĩnh vực đánh giá đến hết
                    for (int j = 0; j < lstDEG.Count(); j++)
                    {
                        sheet.SetCellValue("A" + startRow, lstDEG[j].EvaluationDevelopmentGroupName);
                        sheet.GetRange(startRow, 1, startRow, 4).Merge();
                        sheet.GetRange(startRow, 1, startRow, 4).WrapText();
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                        sheet.SetRowHeight(startRow, 50);
                        int count1 = 0;
                        int count2 = 0;
                        string evaluationTypeName2 = "";
                        List<string> lstEvaluationName = new List<string>();
                        for (int k = 0; k < lstDEI.Count(); k++)
                        {
                            if (lstDEI[k].evaluationDevelopmentGroupID == lstDEG[j].EvaluationDevelopmentGroupID)
                            {
                                if (lstDEI[k].evaluationType == 1)
                                    count1++;
                                else if (lstDEI[k].evaluationType == 2)
                                {
                                    count2++;
                                    evaluationTypeName2 = evaluationTypeName2 + lstDEI[k].evaluationDevelopmentName;
                                    lstEvaluationName.Add(lstDEI[k].evaluationDevelopmentName);
                                }
                            }
                        }
                        lstEvaluationName = lstEvaluationName.Distinct().ToList();
                        evaluationTypeName2 = string.Join(". ", lstEvaluationName.ToArray());

                        sheet.SetCellValue("E" + startRow, count1);
                        sheet.GetRange(startRow, 5, startRow, 7).Merge();
                        sheet.GetRange(startRow, 5, startRow, 7).WrapText();
                        sheet.SetCellValue("H" + startRow, count2);
                        sheet.GetRange(startRow, 8, startRow, 9).Merge();
                        sheet.GetRange(startRow, 8, startRow, 9).WrapText();
                        sheet.SetCellValue("J" + startRow, evaluationTypeName2);
                        sheet.GetRange(startRow, 10, startRow, 19).Merge();
                        sheet.GetRange(startRow, 10, startRow, 19).WrapText();
                        sheet.GetRange(startRow, 10, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                        sheet.GetRange(startRow, 10, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                        sheet.GetRange(startRow, 10, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                        startRow++;
                    }
                    boderlast = startRow - 1;
                    sheet.GetRange(boderfirst, 1, boderlast, 19).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                    sheet.SetCellValue("A" + startRow, "Ý kiến trao đổi với giáo viên:");
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, true);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "………………………………………………………………………………………………………………………………………………………………………………………………………………………………………");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow = startRow + 3;

                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$7");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!$B$5");
                    sheet.GetRange(startRow, 1, startRow, 5).Merge();
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$8");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow = startRow + 6;

                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!$B$6");
                    sheet.GetRange(startRow, 1, startRow, 5).Merge();
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$" + sttTTC + "");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);


                    /*if (numberWeek == 4)
                    {
                        sheet.SetColumnWidth(1, 11.86);
                        sheet.SetColumnWidth(2, 3.86);
                        sheet.SetColumnWidth(3, 2.29);
                        sheet.SetColumnWidth(4, 1.57);
                        sheet.SetColumnWidth(5, 6);
                        sheet.SetColumnWidth(6, 4.29);
                        sheet.SetColumnWidth(7, 1.29);
                        sheet.SetColumnWidth(8, 5);
                        sheet.SetColumnWidth(9, 7);
                        sheet.SetColumnWidth(10, 3.14);
                        sheet.SetColumnWidth(11, 5.29);
                        sheet.SetColumnWidth(12, 5.29);
                        sheet.SetColumnWidth(13, 1.29);
                        sheet.SetColumnWidth(14, 4.43);
                        sheet.SetColumnWidth(15, 5.29);
                        sheet.SetColumnWidth(16, 5);
                        sheet.SetColumnWidth(17, 3.29);
                        sheet.SetColumnWidth(18, 9.14);
                        sheet.SetColumnWidth(19, 9.71);
                    }
                    else
                    {
                        sheet.SetColumnWidth('A', 9);
                        sheet.SetColumnWidth('H', 9);
                        sheet.SetColumnWidth('O', 4.57);
                        sheet.SetColumnWidth('P', 4.57);

                        sheet.SetColumnWidth(1, 11.86);
                        sheet.SetColumnWidth(2, 3.86);
                        sheet.SetColumnWidth(3, 2.29);
                        sheet.SetColumnWidth(4, 1.57);
                        sheet.SetColumnWidth(5, 6);
                        sheet.SetColumnWidth(6, 4.29);
                        sheet.SetColumnWidth(7, 1.29);
                        sheet.SetColumnWidth(8, 5);
                        sheet.SetColumnWidth(9, 7);
                        sheet.SetColumnWidth(10, 3.14);
                        sheet.SetColumnWidth(11, 5.29);
                        sheet.SetColumnWidth(12, 5.29);
                        sheet.SetColumnWidth(13, 1.29);
                        sheet.SetColumnWidth(14, 4.43);
                        sheet.SetColumnWidth(15, 5.29);
                        sheet.SetColumnWidth(16, 5);
                        sheet.SetColumnWidth(17, 3.29);
                        sheet.SetColumnWidth(18, 9.14);
                        sheet.SetColumnWidth(19, 9.71);
                    }*/
                    startRow++;
                    sheet.SetBreakPage(startRow);
                    startRow++;
                    #endregion
                }
            }

            sheet.FitAllColumnsOnOnePage = true;
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className);
            sheet.SetFontName("Times New Roman", 0);
        }

        private IVTWorksheet RenderAreaByNumberWeek(IVTWorksheet sheet, List<GoodChildrenTicket> lstGoodTicket, int numberWeek, int startRow)
        {
            if (numberWeek == 4)
            {
                #region // Tháng có 4 tuần
                if (lstGoodTicket.Count() != 0)
                {
                    if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Count() != 0)
                    {
                        sheet.SetCellValue("A" + (startRow - 8), "Tuần 1           Bé Ngoan");
                        sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                        sheet.SetCellValue("D" + (startRow - 8), "Phiếu bé ngoan");

                        sheet.GetRange(startRow - 8, 4, startRow - 8, 10).SetOrientation(45);
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Count() != 0)
                    {
                        sheet.SetCellValue("K" + (startRow - 8), "Tuần 2            Bé Ngoan");
                        sheet.SetCellValue("K" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                        sheet.SetCellValue("O" + (startRow - 8), "Phiếu bé ngoan");
                        sheet.GetRange(startRow - 8, 15, startRow - 8, 19).SetOrientation(45);
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Count() != 0)
                    {
                        sheet.SetCellValue("A" + (startRow - 4), "Tuần 3             Bé Ngoan");
                        sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                        sheet.SetCellValue("D" + (startRow - 4), "Phiếu bé ngoan");
                        sheet.GetRange(startRow - 4, 4, startRow - 4, 10).SetOrientation(45);
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Count() != 0)
                    {
                        sheet.SetCellValue("K" + (startRow - 4), "Tuần 4             Bé Ngoan");
                        sheet.SetCellValue("K" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                        sheet.SetCellValue("O" + (startRow - 4), "Phiếu bé ngoan");
                        sheet.GetRange(startRow - 4, 15, startRow - 4, 19).SetOrientation(45);
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Count() != 0)
                    {
                        sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Count() != 0)
                    {
                        sheet.SetCellValue("K" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Count() != 0)
                    {
                        sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                    }
                    if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Count() != 0)
                    {
                        sheet.SetCellValue("K" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                    }
                }
                sheet.SetCellValue("F" + startRow, lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 6).FirstOrDefault() != null ? "Đạt danh hiệu cháu ngoan bác Hồ" : "");
                #endregion

                return sheet;
            }

            #region // Tháng 5 tuan
            if (lstGoodTicket.Count() != 0)
            {
                if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Count() != 0)
                {
                    sheet.SetCellValue("A" + (startRow - 8), "Tuần 1           Bé Ngoan");
                    sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                    sheet.GetRange((startRow - 5), 1, (startRow - 5), 7).WrapText();
                    sheet.SetCellValue("B" + (startRow - 8), "Phiếu bé ngoan");
                    sheet.GetRange(startRow - 8, 2, startRow - 8, 7).SetOrientation(45);
                    sheet.GetRange(startRow - 8, 2, startRow - 8, 7).SetFontStyle(true, System.Drawing.Color.Gray, false, 20, false, false);
                }
                if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Count() != 0)
                {
                    sheet.SetCellValue("H" + (startRow - 8), "Tuần 2            Bé Ngoan");
                    sheet.SetCellValue("H" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                    sheet.GetRange((startRow - 5), 8, (startRow - 5), 14).WrapText();
                    sheet.SetCellValue("I" + (startRow - 8), "Phiếu bé ngoan");
                    sheet.GetRange(startRow - 8, 9, startRow - 8, 14).SetOrientation(45);
                    sheet.GetRange(startRow - 8, 9, startRow - 8, 14).SetFontStyle(true, System.Drawing.Color.Gray, false, 20, false, false);
                }
                if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Count() != 0)
                {
                    sheet.SetCellValue("O" + (startRow - 8), "Tuần 3            Bé Ngoan");
                    sheet.SetCellValue("O" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                    sheet.GetRange((startRow - 5), 15, (startRow - 5), 19).WrapText();
                    sheet.SetCellValue("Q" + (startRow - 8), "Phiếu bé ngoan");
                    sheet.GetRange(startRow - 8, 17, startRow - 8, 19).SetOrientation(45);
                    sheet.GetRange(startRow - 8, 17, startRow - 8, 19).SetFontStyle(true, System.Drawing.Color.Gray, false, 20, false, false);
                }

                if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Count() != 0)
                {
                    sheet.SetCellValue("A" + (startRow - 4), "Tuần 4             Bé Ngoan");
                    sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                    sheet.GetRange((startRow - 1), 1, (startRow - 1), 7).WrapText();
                    sheet.SetCellValue("B" + (startRow - 4), "Phiếu bé ngoan");
                    sheet.GetRange(startRow - 4, 2, startRow - 4, 7).SetOrientation(45);
                    sheet.GetRange(startRow - 4, 2, startRow - 4, 7).SetFontStyle(true, System.Drawing.Color.Gray, false, 20, false, false);
                }
                if (lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 5).Count() != 0)
                {
                    sheet.SetCellValue("H" + (startRow - 4), "Tuần 5             Bé Ngoan");
                    sheet.SetCellValue("H" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 5).Select(x => x.Note).FirstOrDefault());
                    sheet.GetRange((startRow - 1), 8, (startRow - 1), 14).WrapText();
                    sheet.SetCellValue("I" + (startRow - 4), "Phiếu bé ngoan");
                    sheet.GetRange(startRow - 4, 9, startRow - 4, 14).SetOrientation(45);
                    sheet.GetRange(startRow - 4, 9, startRow - 4, 14).SetFontStyle(true, System.Drawing.Color.Gray, false, 20, false, false);
                }

                if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Count() != 0)
                {
                    sheet.SetCellValue("A" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 1).Select(x => x.Note).FirstOrDefault());
                }
                if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Count() != 0)
                {
                    sheet.SetCellValue("H" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 2).Select(x => x.Note).FirstOrDefault());
                }
                if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Count() != 0)
                {
                    sheet.SetCellValue("O" + (startRow - 5), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 3).Select(x => x.Note).FirstOrDefault());
                }
                if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Count() != 0)
                {
                    sheet.SetCellValue("A" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 4).Select(x => x.Note).FirstOrDefault());
                }
                if (lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 5).Count() != 0)
                {
                    sheet.SetCellValue("H" + (startRow - 1), "Nhận xét: " + lstGoodTicket.Where(x => x.IsGood == false && x.WeekInMonth == 5).Select(x => x.Note).FirstOrDefault());
                }
            }
            sheet.SetCellValue("F" + startRow, lstGoodTicket.Where(x => x.IsGood == true && x.WeekInMonth == 6).FirstOrDefault() != null ? "Đạt danh hiệu cháu ngoan bác Hồ" : "");
            sheet.SetColumnWidth('A', 9);
            sheet.SetColumnWidth('H', 9);
            sheet.SetColumnWidth('O', 4.57);
            sheet.SetColumnWidth('P', 4.57);
            #endregion

            return sheet;
        }

        private void SetValueToFileCombineClass(IVTWorksheet sheet,
            IVTRange rangNumberWeek4, IVTRange rangNumberWeek5,
            IDictionary<string, object> dic,
            int ClassId,
            string className,
            string CombineClassCode,
            List<ListModel> lstDEG,
            List<ListBillEvaluation> lstDEITemp,
            int sttTTC, int numberWeek)
        {
            List<PupilProfileBO> lstPupil = (List<PupilProfileBO>)dic["lstPupil"];
            List<InfoEvaluationHealthy> lstHealthOfPupil = (List<InfoEvaluationHealthy>)dic["lstHealth"];
            List<GoodChildrenTicket> lstGoodTicketTemp = (List<GoodChildrenTicket>)dic["lstGoodTicket"];
            List<PupilAbsence> lstPupilAbsenceTemp = (List<PupilAbsence>)dic["lstPupilAbenceEdu"];

            int boderlast = 0;
            int boderfirst = 0;

            for (int i = 0; i < lstPupil.Count(); i++)
            {
                if (lstPupil[i].CurrentClassID == ClassId)
                {
                    #region dinh dang I
                    List<ListBillEvaluation> lstDEI = new List<ListBillEvaluation>();
                    List<GoodChildrenTicket> lstGoodTicket = new List<GoodChildrenTicket>();
                    List<PupilAbsence> lstPupilAbsence = new List<PupilAbsence>();
                    lstDEI = lstDEITemp.Where(x => x.classID == ClassId && x.classIDGrow == ClassId && x.pupilID == lstPupil[i].PupilProfileID).ToList();
                    lstGoodTicket = lstGoodTicketTemp.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).OrderBy(x => x.WeekInMonth).ToList();
                    lstPupilAbsence = lstPupilAbsenceTemp.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).ToList();
                    SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

                    #region tam
                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!B3");
                    sheet.SetCellValue("G" + startRow, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
                    sheet.GetRange(startRow, 1, startRow, 6).WrapText();
                    sheet.SetRowHeight(startRow, 21);
                    sheet.SetRowHeight(startRow + 1, 21.75);
                    sheet.GetRange(startRow, 7, startRow, 19).Merge();
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                    sheet.GetRange(startRow, 7, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("G" + startRow, "Độc lập - Tự do - Hạnh phúc");
                    sheet.GetRange(startRow - 1, 1, startRow, 6).Merge();
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow - 1, 1, startRow, 6).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.GetRange(startRow, 7, startRow, 19).Merge();
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                    sheet.GetRange(startRow, 7, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, true);
                    startRow++;

                    AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!B4");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 63.75);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Họ và tên: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetCellValue("C" + startRow, lstPupil[i].FullName);
                    sheet.GetRange(startRow, 3, startRow, 13).Merge();
                    sheet.GetRange(startRow, 3, startRow, 13).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 13).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 13).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("N" + startRow, "Lớp: ");
                    sheet.GetRange(startRow, 14, startRow, 16).Merge();
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.SetCellValue("Q" + startRow, className + "(" + CombineClassCode + ")");
                    sheet.GetRange(startRow, 17, startRow, 19).Merge();
                    sheet.GetRange(startRow, 17, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 17, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 17, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Số ngày nghỉ: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetCellValue("C" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Count() : 0);
                    sheet.GetRange(startRow, 3, startRow, 6).Merge();
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 6).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("G" + startRow, "Có phép: ");
                    sheet.GetRange(startRow, 7, startRow, 9).Merge();
                    sheet.GetRange(startRow, 7, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 7, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 7, startRow, 9).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("J" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Where(x => x.IsAccepted == true).Count() : 0);
                    sheet.GetRange(startRow, 10, startRow, 11).Merge();
                    sheet.GetRange(startRow, 10, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 10, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 10, startRow, 11).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("N" + startRow, "Không phép: ");
                    sheet.GetRange(startRow, 14, startRow, 16).Merge();
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 16).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 14, startRow, 16).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("Q" + startRow, lstPupilAbsence.Count() != 0 ? lstPupilAbsence.Where(x => x.IsAccepted == false).Count() : 0);
                    sheet.GetRange(startRow, 17, startRow, 17).Merge();
                    sheet.GetRange(startRow, 17, startRow, 17).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 17, startRow, 17).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 17, startRow, 17).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "Chiều cao: ");
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 2).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 25);
                    sheet.SetRowHeight(startRow + 1, 6);
                    List<InfoEvaluationHealthy> lstHeightAndWeight = lstHealthOfPupil.Where(x => x.ClassID == ClassId && x.PupilID == lstPupil[i].PupilProfileID).ToList();
                    string weightstatus = "";
                    string heightstatus = "";
                    if (lstHealthOfPupil.Count() != 0)
                    {
                        if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 1)
                        {
                            heightstatus = "Cao hơn so với tuổi";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 2)
                        {
                            heightstatus = "Bình thường";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 3)
                        {
                            heightstatus = "Thấp còi độ 1";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().HeightStatus == 4)
                        {
                            heightstatus = "Thấp còi độ 2";
                        }
                    }
                    sheet.SetCellValue("C" + startRow, lstHeightAndWeight.Count() == 0 ? "" : lstHeightAndWeight.FirstOrDefault().Height.ToString() + " cm - " + heightstatus);
                    sheet.GetRange(startRow, 3, startRow, 9).Merge();
                    sheet.GetRange(startRow, 3, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 3, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 9).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("J" + startRow, "Cân nặng");
                    sheet.GetRange(startRow, 10, startRow, 13).Merge();
                    sheet.GetRange(startRow, 10, startRow, 13).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 10, startRow, 13).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 10, startRow, 13).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);

                    if (lstHealthOfPupil.Count() != 0)
                    {
                        if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 1)
                        {
                            weightstatus = "Cân nặng hơn so với tuổi";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 2)
                        {
                            weightstatus = "Bình thường";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 3)
                        {
                            weightstatus = "Suy dinh dưỡng nhẹ";
                        }
                        else if (lstHealthOfPupil.FirstOrDefault().WeightStatus == 4)
                        {
                            weightstatus = "Suy dinh dưỡng nặng";
                        }
                    }

                    sheet.SetCellValue("N" + startRow, lstHeightAndWeight.Count() == 0 ? "" : lstHeightAndWeight.FirstOrDefault().Weight.ToString() + " kg - " + weightstatus);
                    sheet.GetRange(startRow, 14, startRow, 18).Merge();
                    sheet.GetRange(startRow, 14, startRow, 18).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 14, startRow, 18).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 14, startRow, 18).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow = startRow + 2;

                    sheet.SetCellValue("A" + startRow, "I. Bé ngoan:");
                    sheet.GetRange(startRow, 1, startRow, 11).Merge();
                    sheet.GetRange(startRow, 1, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 20);
                    sheet.SetRowHeight(startRow + 1, 6);
                    startRow = startRow + 2;
                    #endregion

                    if (numberWeek == 4)
                    {
                        sheet.CopyPasteSameSize(rangNumberWeek4, startRow, 1);

                    }
                    else
                    {
                        sheet.CopyPasteSameSize(rangNumberWeek5, startRow, 1);

                    }
                    startRow += 8;
                    sheet = RenderAreaByNumberWeek(sheet, lstGoodTicket, numberWeek, startRow);
                    startRow += 1;

                    sheet.SetCellValue("A" + startRow, "II. Đánh giá sự phát triển ");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 24.75);
                    startRow++;

                    boderfirst = startRow;
                    sheet.SetCellValue("A" + startRow, "Lĩnh vực ");
                    sheet.GetRange(startRow, 1, startRow, 4).Merge();
                    sheet.SetCellValue("E" + startRow, "Đạt ");
                    sheet.GetRange(startRow, 5, startRow, 7).Merge();
                    sheet.SetCellValue("H" + startRow, "Chưa Đạt");
                    sheet.GetRange(startRow, 8, startRow, 9).Merge();
                    sheet.SetCellValue("J" + startRow, "Các chỉ số cần cố gắng");
                    sheet.GetRange(startRow, 10, startRow, 19).Merge();
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetRowHeight(startRow, 24.75);
                    startRow++;
                    #endregion;

                    #region Lĩnh vực đánh giá đến hết
                    for (int j = 0; j < lstDEG.Count(); j++)
                    {
                        sheet.SetCellValue("A" + startRow, lstDEG[j].EvaluationDevelopmentGroupName);
                        sheet.GetRange(startRow, 1, startRow, 4).Merge();
                        sheet.GetRange(startRow, 1, startRow, 4).WrapText();
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                        sheet.SetRowHeight(startRow, 50);
                        int count1 = 0;
                        int count2 = 0;
                        string evaluationTypeName2 = "";
                        for (int k = 0; k < lstDEI.Count(); k++)
                        {
                            if (lstDEI[k].evaluationDevelopmentGroupID == lstDEG[j].EvaluationDevelopmentGroupID)
                            {
                                if (lstDEI[k].evaluationType == 1)
                                    count1++;
                                else if (lstDEI[k].evaluationType == 2)
                                {
                                    count2++;
                                    evaluationTypeName2 = evaluationTypeName2 + lstDEI[k].evaluationDevelopmentName;
                                }
                            }
                        }
                        sheet.SetCellValue("E" + startRow, count1);
                        sheet.GetRange(startRow, 5, startRow, 7).Merge();
                        sheet.GetRange(startRow, 5, startRow, 7).WrapText();
                        sheet.SetCellValue("H" + startRow, count2);
                        sheet.GetRange(startRow, 8, startRow, 9).Merge();
                        sheet.GetRange(startRow, 8, startRow, 9).WrapText();
                        sheet.SetCellValue("J" + startRow, evaluationTypeName2);
                        sheet.GetRange(startRow, 10, startRow, 19).Merge();
                        sheet.GetRange(startRow, 10, startRow, 19).WrapText();
                        sheet.GetRange(startRow, 10, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                        sheet.GetRange(startRow, 10, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                        sheet.GetRange(startRow, 10, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                        startRow++;
                    }
                    boderlast = startRow - 1;
                    sheet.GetRange(boderfirst, 1, boderlast, 19).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                    sheet.SetCellValue("A" + startRow, "Ý kiến trao đổi với giáo viên:");
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, true);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "……………………………………………………………………………………………………………………………………………………………………………………………………………………………………….");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "………………………………………………………………………………………………………………………………………………………………………………………………………………………………………");
                    sheet.GetRange(startRow, 1, startRow, 19).Merge();
                    startRow = startRow + 3;

                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$7");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(false, System.Drawing.Color.Black, false, 13, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!$B$5");
                    sheet.GetRange(startRow, 1, startRow, 5).Merge();
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$8");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    startRow = startRow + 6;

                    sheet.SetCellValue("A" + startRow, "='Thong tin chung'!$B$6");
                    sheet.GetRange(startRow, 1, startRow, 5).Merge();
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                    sheet.SetCellValue("L" + startRow, "='Thong tin chung'!$B$" + sttTTC + "");
                    sheet.GetRange(startRow, 12, startRow, 19).Merge();
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 12, startRow, 19).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);

                    startRow++;
                    sheet.SetBreakPage(startRow);
                    startRow++;
                    #endregion
                }

            }

            sheet.FitAllColumnsOnOnePage = true;
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className + "(" + CombineClassCode + ")");
            sheet.SetFontName("Times New Roman", 0);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNumberOfClassReport_03(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            int month = !string.IsNullOrEmpty(frm["Month"]) ? int.Parse(frm["Month"]) : 0;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.ChildrenContact);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                {
                                                    {"AcademicYearID",_globalInfo.AcademicYearID},
                                                    {"SchoolID",_globalInfo.SchoolID},
                                                    {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                    {"EducationLevelID",educationLevelID}
                                                };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dic.Add("ClassID", classID);

            if (reportDef.IsPreprocessed == true)
            {
                // viet them ham
                processedReport = PupilOfClassBusiness.GetProcessReportChildrenContact(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(month, educationLevelID, classID);
                processedReport = PupilOfClassBusiness.InsertProcessChildrenContact(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : "0";
            int month = !string.IsNullOrEmpty(frm["Month"]) ? int.Parse(frm["Month"]) : 0;

            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID}
            };

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicInsert.Add("ClassID", classID);

            Stream excel = this.ExportExcel(month, educationLevelID, classID);
            processedReport = PupilOfClassBusiness.InsertProcessChildrenContact(dicInsert, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.ChildrenContact,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.ChildrenContact,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }


}
