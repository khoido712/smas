﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportResultEvaluationArea.Models;
using System.Globalization;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Controllers
{
    public class ReportEvaluationController : BaseController
    {
        //
        // GET: /ReportResultEvaluationArea/ReportEvaluation/

        #region properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        public ReportEvaluationController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IDeclareEvaluationIndexBusiness declareEvaluationIndexBusiness,
            IDeclareEvaluationGroupBusiness declareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness evaluationDevelopmentGroupBusiness,
            IEvaluationDevelopmentBusiness evaluationDevelopmentBusiness,
            IGrowthEvaluationBusiness growthEvaluationBusiness,
            IClassAssigmentBusiness classAssigmentBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.DeclareEvaluationGroupBusiness = declareEvaluationGroupBusiness;
            this.DeclareEvaluationIndexBusiness = declareEvaluationIndexBusiness;
            this.EvaluationDevelopmentGroupBusiness = evaluationDevelopmentGroupBusiness;
            this.EvaluationDevelopmentBusiness = evaluationDevelopmentBusiness;
            this.GrowthEvaluationBusiness = growthEvaluationBusiness;
            this.ClassAssigmentBusiness = classAssigmentBusiness;
        }
        #endregion
        //
        // GET: /ReportMenuChildrenArea/ChildrenOfClassReport/
        //
        public ActionResult Index()
        {
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(_globalInfo.SchoolID.Value,
                                                                                _globalInfo.AcademicYearID.Value,
                                                                                 _globalInfo.AppliedLevel.Value);
            var lstEducation = _globalInfo.EducationLevels.ToList();
            if (checkCombineClass)
            {
                lstEducation.Add(new EducationLevel { Resolution = SystemParamsInFile.Combine_Class_Name, EducationLevelID = SystemParamsInFile.Combine_Class_ID });
            }

            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            int? educationLevelID = lstEducation.First().EducationLevelID;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", educationLevelID);

            //var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
            //                                        .Where(x=>x.IsCombinedClass == false)
            //                                        .OrderBy(u => u.DisplayName).ToList();

            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(u => u.DisplayName).ToList();
            }
            lstClass = lstClass.Where(x => !x.IsCombinedClass).ToList();

            ViewData["classgroup"] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            int classIndex = lstClass.Count > 0 ? lstClass.FirstOrDefault().ClassProfileID : 0;
            var lstpupil = (List<PupilProfile>)(from a in PupilProfileBusiness.All
                                                join b in PupilOfClassBusiness.All on a.PupilProfileID equals b.PupilID
                                                where a.CurrentAcademicYearID == _globalInfo.AcademicYearID &&
                                                      a.CurrentSchoolID == _globalInfo.SchoolID &&
                                                      a.IsActive == true &&
                                                      b.SchoolID == _globalInfo.SchoolID &&
                                                      b.Status == 1 &&
                                                      b.AcademicYearID == _globalInfo.AcademicYearID &&
                                                      b.ClassID == classIndex
                                                orderby b.OrderInClass
                                                select a).ToList();
            ViewData["Children"] = new SelectList(lstpupil, "PupilProfileID", "FullName");
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dicClass)
                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            }
            else
            {
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                                                    .OrderBy(u => u.DisplayName).ToList();
            }

            if (eduId == SystemParamsInFile.Combine_Class_ID)
            {
                var _lstClass = lstClass.Where(x => x.IsCombinedClass == true).Select(x => new { CombinedClassCode = x.CombinedClassCode }).Distinct().OrderBy(x => x.CombinedClassCode);
                var lstCombineClass = _lstClass.Select(u => new SelectListItem { Value = u.CombinedClassCode.ToString(), Text = u.CombinedClassCode, Selected = false }).ToList();
                return Json(lstCombineClass);
            }

            var lstNotCombineClass = lstClass.Where(x => x.IsCombinedClass == false).Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();
            return Json(lstNotCombineClass);
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadPuppil(int eduId, string classIdFirst)
        {
            if (string.IsNullOrEmpty(classIdFirst) || eduId <= 0)
                return Json(new List<SelectListItem>());

            IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All
                                                    .Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                                            && x.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                            && x.Status == 1);

            if (eduId != SystemParamsInFile.Combine_Class_ID)
            {
                int classID = Int32.Parse(classIdFirst);
                lstPupilOfClass = lstPupilOfClass.Where(x => x.ClassID == classID);
            }
            else
            {
                List<ClassProfile> lstClassProfile = ClassProfileBusiness.All
                                            .Where(x => x.IsCombinedClass == true
                                            && x.CombinedClassCode == classIdFirst
                                            && x.IsActive == true
                                            && x.SchoolID == _globalInfo.SchoolID.Value
                                            && x.AcademicYearID == _globalInfo.AcademicYearID.Value).ToList<ClassProfile>();

                List<int> lstClassID = lstClassProfile.Select(x => x.ClassProfileID).Distinct().ToList();
                lstPupilOfClass = lstPupilOfClass.Where(x => lstClassID.Contains(x.ClassID));
            }

            var lstpupil = (from a in PupilProfileBusiness.All
                            join b in lstPupilOfClass on a.PupilProfileID equals b.PupilID
                            where a.CurrentAcademicYearID == _globalInfo.AcademicYearID &&
                                  a.CurrentSchoolID == _globalInfo.SchoolID &&
                                  a.IsActive == true
                            orderby b.OrderInClass
                            select a).ToList();

            var result = lstpupil.Select(u => new SelectListItem { Value = u.PupilProfileID.ToString(), Text = u.FullName, Selected = false }).ToList();
            return Json(result);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNumberOfClassReport_03(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : string.Empty;
            int ChildrenID = !string.IsNullOrEmpty(frm["ChildrenID"]) ? int.Parse(frm["ChildrenID"]) : 0;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.ReportEvaluation);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID},
                                                        //{"ClassID",classID}
                                                    };
            if (educationLevelID.Value != SystemParamsInFile.Combine_Class_ID)
                dic.Add("ClassID", !string.IsNullOrEmpty(classID) ? Int32.Parse(classID) : 0);
            else
                dic.Add("ClassID", 0);

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReportEvaluation(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(educationLevelID, classID, ChildrenID);
                processedReport = PupilOfClassBusiness.InsertProcessReportEvaluation(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            string classID = !string.IsNullOrEmpty(frm["ClassID"]) ? frm["ClassID"] : string.Empty;
            int ChildrenID = !string.IsNullOrEmpty(frm["ChildrenID"]) ? int.Parse(frm["ChildrenID"]) : 0;

            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                //{"ClassID",classID},
                {"ChildrenId",ChildrenID}
            };
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
                dicInsert.Add("ClassID", !string.IsNullOrEmpty(classID) ? Int32.Parse(classID) : 0);
            else
                dicInsert.Add("ClassID", 0);

            Stream excel = this.ExportExcel(educationLevelID, classID, ChildrenID);
            processedReport = PupilOfClassBusiness.InsertProcessReportEvaluation(dicInsert, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> {
                SystemParamsInFile.ReportEvaluation
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> {
                SystemParamsInFile.ReportEvaluation
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(int? educationLevelID, string classID, int? ChildrenID)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.ReportEvaluation + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int schoolID = _globalInfo.SchoolID.Value;
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            int provinceId = school.ProvinceID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                 {"AcademicYearID",_globalInfo.AcademicYearID},
                 {"SchoolID",_globalInfo.SchoolID},//
                 {"AppliedLevel",_globalInfo.AppliedLevel},
                 {"EducationLevelID",educationLevelID},
                 {"CurrentClassID",classID},
                 {"PupilProfileID",ChildrenID},
                 {"StrCurrentClassID", string.IsNullOrEmpty(classID) ? "ValueIsNullOrEmpty" : classID}
            };

            #region lay danh sach can thiet
            // lay danh sach tat ca hoc sinh theo educationId
            List<PupilProfileBO> lstPupilBO = this.PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(a => a.ProfileStatus == 1)
                                                                        .OrderBy(a => a.OrderInClass).ToList();
            List<ClassProfile> lstCPBO = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                                    .OrderBy(p => p.EducationLevelID)
                                                                    .ThenBy(u => u.OrderNumber)
                                                                    .ThenBy(u => u.DisplayName).ToList();

            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstCPBO = lstCPBO.Where(x => x.IsCombinedClass == false).ToList();
            }


            List<string> lstCombineClassID = educationLevelID == SystemParamsInFile.Combine_Class_ID ? lstCPBO.Select(p => p.CombinedClassCode).Distinct().ToList() : new List<string>();
            if (lstCombineClassID.Count > 0)
            {
                if (!string.IsNullOrEmpty(classID))
                {
                    lstCPBO = lstCPBO.Where(x => x.CombinedClassCode.ToUpper() == classID.ToUpper()).ToList();
                    lstCombineClassID = lstCombineClassID.Where(x => x.ToUpper() == classID.ToUpper()).ToList();
                }

            }
            List<int> lstClassID = lstCPBO.Select(p => p.ClassProfileID).Distinct().ToList();
            List<PupilOfClassBO> lstPupilOfClassBO = this.ListPupilOfClass(lstClassID);
            List<int> lstEdu = lstPupilOfClassBO.Select(x => x.EducationLevelID).Distinct().ToList();

            IQueryable<ClassAssigment> lstClassAssigment = ClassAssigmentBusiness.All.Where(x => x.IsHeadTeacher == true || x.IsTeacher == true
                                                                                            && x.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                                            && x.SchoolID == _globalInfo.SchoolID.Value);
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolID == _globalInfo.SchoolID
                                                                                   && x.AcademicYearID == _globalInfo.AcademicYearID).ToList();
            int _classId = 0;
            if (educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                if (!string.IsNullOrEmpty(classID))
                {
                    _classId = Int32.Parse(classID);
                    lstClassAssigment = lstClassAssigment.Where(x => x.ClassID == _classId);
                    lstClassProfile = lstClassProfile.Where(x => x.ClassProfileID == _classId).ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(classID))
                {
                    lstClassAssigment = lstClassAssigment.Where(x => x.ClassProfile.CombinedClassCode.ToUpper() == classID.ToUpper());
                    lstClassProfile = lstClassProfile.Where(x => x.IsCombinedClass == true && x.CombinedClassCode.ToUpper() == classID.ToUpper()).ToList();
                }
            }
            lstClassProfile = lstClassProfile.OrderBy(x => x.OrderNumber).ToList();

            List<Employee> iqEmployee = (from e in EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value && x.IsActive == true)
                                         join aca in lstClassAssigment on e.EmployeeID equals aca.TeacherID
                                         select e).ToList();

            dic.Add("ProvinceID", _globalInfo.ProvinceID.Value);
            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
                dic.Add("LstEducationLevel", lstEdu);

            // danh sách chỉ số theo lĩnh vực đã được khai báo
            lstClassID = lstClassProfile.Select(x => x.ClassProfileID).ToList();
            dic.Add("lstClassID", lstClassID);
            var lstEVGandEVI = EvaluationDevelopmentBusiness.GetListEvaluationDevelopment(dic);

            Dictionary<string, object> dicDEG = new Dictionary<string, object>();
            dicDEG["SchoolID"] = schoolID;
            dicDEG["AcademicYearID"] = academicYearID;

            IQueryable<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.GetListByAppliedLevelAndProvince(appliedLevel, provinceId);
            IQueryable<EvaluationDevelopment> lstEvaluationDevelopment = EvaluationDevelopmentBusiness.GetListByAppliedLevelAndProvince(appliedLevel, provinceId);
            IQueryable<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.GetListBySchoolAndAcademicYear(dicDEG);
            IQueryable<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.GetListBySchoolAndAcademicYear(schoolID, academicYearID);

            if (educationLevelID != 0 && educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => x.EducationLevelID == educationLevelID);
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => x.EducationLevelID == educationLevelID);
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => x.EducationLevelID == educationLevelID);
            }
            else if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                lstEvaluationDevelopmentGroup = lstEvaluationDevelopmentGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstEvaluationDevelopment = lstEvaluationDevelopment.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstDeclareEvaluationGroup = lstDeclareEvaluationGroup.Where(x => lstEdu.Contains(x.EducationLevelID));
                lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstEdu.Contains(x.EducationLevelID));
            }

            List<int> lstDeclareEvaluationIndexID = new List<int>();
            var lstDeclareEvaluationIndexTempt = lstDeclareEvaluationIndex.ToList();
            var temptListDEI = lstDeclareEvaluationIndex.ToList();
            var temptListDEG = lstDeclareEvaluationGroup.ToList();

            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                for (int j = 0; j < temptListDEG.Count(); j++)
                {
                    var lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                    && x.ClassID == lstClassProfile[i].ClassProfileID
                                                                    && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID)
                                                                    .Select(x => x.DeclareEvaluationIndexID).ToList();
                    if (lstDEI_ID.Count() == 0)
                    {
                        lstDEI_ID = temptListDEI.Where(x => x.EducationLevelID == lstClassProfile[i].EducationLevelID
                                                                   && x.EvaluationDevelopmentGroID == temptListDEG[j].EvaluationGroupID
                                                                   && !x.ClassID.HasValue && x.ClassID == null)
                                                                  .Select(x => x.DeclareEvaluationIndexID).ToList();
                    }
                    if (lstDEI_ID.Count() > 0)
                    {
                        lstDeclareEvaluationIndexID.AddRange(lstDEI_ID);
                    }
                }
            }
            lstDeclareEvaluationIndex = lstDeclareEvaluationIndex.Where(x => lstDeclareEvaluationIndexID.Contains(x.DeclareEvaluationIndexID));

            IDictionary<string, object> dicGrow = new Dictionary<string, object>();
            dicGrow.Add("SchoolID", schoolID);
            dicGrow.Add("AcademicYearID", academicYearID);
            dicGrow.Add("lstClassID", lstClassID);
            dicGrow.Add("PupilID", ChildrenID);
            IQueryable<GrowthEvaluation> lstGrowthEvaluation = GrowthEvaluationBusiness.Search(dicGrow);

            var lstGrowtemp = (from a in lstPupilBO
                               join c in lstGrowthEvaluation on a.PupilProfileID equals c.PupilID
                               join d in lstEvaluationDevelopmentGroup on c.DeclareEvaluationGroupID equals d.EvaluationDevelopmentGroupID
                               join e in lstEvaluationDevelopment on c.DeclareEvaluationIndexID equals e.EvaluationDevelopmentID
                               join f in lstDeclareEvaluationGroup on c.DeclareEvaluationGroupID equals f.EvaluationGroupID
                               join h in lstDeclareEvaluationIndex on c.DeclareEvaluationIndexID equals h.EvaluationDevelopmentID
                               where lstClassID.Contains(h.ClassID ?? 0)
                               orderby a.PupilProfileID, d.OrderID, e.OrderID
                               select new ListBillEvaluation
                               {
                                   schoolID = c.SchoolID,
                                   schoolName = a.SchoolName,
                                   academicID = c.AcademicYearID,
                                   educationID = f.EducationLevelID,
                                   classID = c.ClassID,
                                   className = a.ClassName,
                                   pupilID = a.PupilProfileID,
                                   pupilName = a.FullName,
                                   evaluationDevelopmentGroupID = d.EvaluationDevelopmentGroupID,
                                   evaluationDevelopmentGroupName = d.EvaluationDevelopmentGroupName,
                                   evaluationDevelopmentID = h.EvaluationDevelopmentID,
                                   evaluationDevelopmentName = e.EvaluationDevelopmentName,
                                   evaluationType = c.EvaluationType,
                                   birthdate = a.BirthDate
                               }).ToList();

            // danh sách lĩnh vực được khai báo
            var _lstEvaluationGroup = (from lstEDG in lstEvaluationDevelopmentGroup
                                       join lstDEG in lstDeclareEvaluationGroup
                                       on lstEDG.EvaluationDevelopmentGroupID equals lstDEG.EvaluationGroupID
                                       select lstEDG).ToList();
            #endregion

            int startRowTemp = 12;
            string formular = string.Empty;
            int startRow = 2;
            #region fill data
            List<ListBillEvaluation> lstGrow = new List<ListBillEvaluation>();
            List<ClassProfile> lstcheckCombineAndClassID = new List<ClassProfile>();
            List<int> lstClassIdInCombined = new List<int>();
            List<EvaluationDevelopmentGroup> lstEvaluationGroup = new List<EvaluationDevelopmentGroup>();
            int educationLevelInCombineClass = 0;
            string Title = "PHIẾU ĐÁNH GIÁ SỰ PHÁT TRIỂN CỦA TRẺ";
            // neu la lop ghep
            string className = string.Empty;
            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                #region
                List<PupilOfClassBO> lstPupilInCombinedClass = new List<PupilOfClassBO>();
                IVTWorksheet sheetFill = null;
                foreach (var item in lstCPBO)
                {
                    lstPupilInCombinedClass = lstPupilOfClassBO.Where(x => x.ClassID == item.ClassProfileID).ToList();
                    if (ChildrenID.HasValue && ChildrenID.Value != 0)
                        lstPupilInCombinedClass = lstPupilInCombinedClass.Where(x => x.PupilID == ChildrenID).ToList();
                    if (lstPupilInCombinedClass.Count > 0)
                    {
                        className = item.DisplayName.Replace("Lớp", "");
                        className = className.Replace("lớp", "");
                        sheetFill = oBook.CopySheetToLast(sheet);
                        startRow = 2;
                        startRowTemp = 12;
                        foreach (var itemPupil in lstPupilInCombinedClass)
                        {
                            educationLevelInCombineClass = itemPupil.EducationLevelID;
                            // lĩnh vực đánh giá theo từng education học sinh
                            lstEvaluationGroup = _lstEvaluationGroup.Where(x => x.EducationLevelID == itemPupil.EducationLevelID).ToList();
                            // thông tin đánh giá của từng học sinh
                            lstGrow = lstGrowtemp.Where(x => x.pupilID == itemPupil.PupilID && x.schoolID == _globalInfo.SchoolID &&
                                                        x.academicID == _globalInfo.AcademicYearID
                                                        && x.classIDGrow == itemPupil.ClassID).ToList();

                            #region report excel
                            int stt = 0;
                            sheetFill.SetCellValue("A" + startRow, school.SupervisingDept.SupervisingDeptName.ToUpper());
                            sheetFill.GetRange(startRow, 1, startRow, 3).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetFontStyle(false, System.Drawing.Color.Black, false, 12, false, false);
                            startRow++;

                            sheetFill.SetCellValue("A" + startRow, _globalInfo.SchoolName.ToUpper());
                            sheetFill.GetRange(startRow, 1, startRow, 3).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                            sheetFill.GetRange(startRow, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, true);
                            startRow = startRow + 2;

                            sheetFill.SetCellValue("A" + startRow, Title);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                            startRow++;
                            sheetFill.SetCellValue("A" + startRow, "Năm học " + objAca.DisplayTitle);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                            sheetFill.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                            startRow = startRow + 2;

                            sheetFill.SetCellValue("A" + startRow, "Họ và tên: " + itemPupil.PupilFullName);
                            sheetFill.GetRange(startRow, 1, startRow, 2).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                            sheetFill.SetCellValue("C" + startRow, (itemPupil.Birthday != null ? ("Ngày sinh: " + itemPupil.Birthday.ToString("dd/MM/yyyy")) : "Ngày sinh: "));
                            sheetFill.GetRange(startRow, 3, startRow, 4).Merge();
                            sheetFill.GetRange(startRow, 3, startRow, 4).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                            startRow++;
                            sheetFill.SetCellValue("A" + startRow, string.Format("Lớp: {0} ({1})", className, classID));

                            sheetFill.GetRange(startRow, 1, startRow, 2).Merge();
                            sheetFill.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);

                            if (iqEmployee.Count() != 0)
                                sheetFill.SetCellValue("C" + startRow, iqEmployee[0] == null ? "Giáo viên" : "Giáo viên: " + iqEmployee[0].FullName); //objEmployee[0].FullName
                            else
                                sheetFill.SetCellValue("C" + startRow, "Giáo viên: ");
                            sheetFill.GetRange(startRow, 3, startRow, 6).Merge();
                            sheetFill.GetRange(startRow, 3, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            sheetFill.GetRange(startRow, 3, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRow = startRow + 3;

                            sheetFill.SetCellValue(startRow - 1, 1, "Chỉ số");
                            sheetFill.SetCellValue(startRow - 1, 2, "Nội dung chỉ số");
                            sheetFill.SetCellValue(startRow - 1, 7, "Đạt");
                            sheetFill.SetCellValue(startRow - 1, 8, "Chưa đạt");
                            sheetFill.SetRowHeight(startRow - 1, 20);
                            if ((educationLevelInCombineClass != 0 && educationLevelInCombineClass == 18))
                            {
                                sheetFill.SetCellValue(startRow - 1, 9, "±");
                            }
                            sheetFill.GetRange(startRow - 1, 1, startRow - 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheetFill.GetRange(startRow - 1, 1, startRow - 1, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            sheetFill.GetRange(startRow - 1, 1, startRow - 1, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                            sheetFill.GetRange(startRow - 1, 2, startRow - 1, 6).Merge();

                            for (int i = 0; i < lstEvaluationGroup.Count(); i++)
                            {
                                sheetFill.SetCellValue(startRow, 1, lstEvaluationGroup[i].EvaluationDevelopmentGroupName);
                                sheetFill.SetRowHeight(startRow, 18);

                                if (educationLevelInCombineClass == 18)
                                    sheetFill.GetRange(startRow, 1, startRow, 9).Merge();
                                else
                                    sheetFill.GetRange(startRow, 1, startRow, 8).Merge();

                                sheetFill.GetRange(startRow, 1, startRow, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                                sheetFill.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                                sheetFill.GetRange(startRow, 1, startRow, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                                startRow++;

                                for (int j = 0; j < lstEVGandEVI.Count(); j++)
                                {
                                    if (lstEVGandEVI[j].EvaluationDevelopmentGroupID == lstEvaluationGroup[i].EvaluationDevelopmentGroupID)
                                    {
                                        stt++;
                                        sheetFill.SetCellValue(startRow, 1, stt);
                                        sheetFill.GetRange(startRow, 1, startRow, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                                        sheetFill.SetCellValue(startRow, 2, lstEVGandEVI[j].EvaluationDevelopmentName);
                                        sheetFill.GetRange(startRow, 2, startRow, 6).WrapText();
                                        sheetFill.GetRange(startRow, 2, startRow, 6).Merge();
                                        sheetFill.SetRowHeight(startRow - 1, 16.5);
                                        sheetFill.GetRange(startRow, 2, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);

                                        for (int h = 0; h < lstGrow.Count(); h++)
                                        {
                                            if (lstGrow[h].evaluationDevelopmentID == lstEVGandEVI[j].EvaluationDevelopmentID)
                                            {
                                                if (lstGrow[h].evaluationType == 1)
                                                {
                                                    sheetFill.SetCellValue(startRow, 7, '+');
                                                }
                                                else if (lstGrow[h].evaluationType == 2)
                                                {
                                                    sheetFill.SetCellValue(startRow, 8, '-');
                                                }
                                                else if (lstGrow[h].evaluationType == 3)
                                                {
                                                    sheetFill.SetCellValue(startRow, 9, '±');
                                                }
                                            }
                                        }
                                        sheetFill.GetRange(startRow, 7, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                                        startRow++;
                                    }
                                }
                            }

                            sheetFill.SetCellValue(startRow, 1, "Tổng số");
                            sheetFill.GetRange(startRow, 1, startRow, 6).Merge();
                            sheetFill.SetCellValue(startRow, 7, "=COUNTIF(G" + startRowTemp + ":G" + (startRow - 1) + ",\"+\")");
                            sheetFill.SetCellValue(startRow, 8, "=COUNTIF(H" + startRowTemp + ":H" + (startRow - 1) + ",\"-\")");
                            sheetFill.SetRowHeight(startRow, 16.5);
                            if ((educationLevelInCombineClass != 0 && educationLevelInCombineClass == 18))
                            {
                                sheetFill.SetCellValue(startRow, 9, "=COUNTIF(I" + startRowTemp + ":I" + (startRow - 1) + ",\"±\")");
                            }
                            sheetFill.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                            sheetFill.SetCellValue(startRow + 1, 1, "Tỷ lệ %");
                            sheetFill.SetRowHeight(startRow + 1, 15.75);
                            sheetFill.GetRange(startRow + 1, 1, startRow + 1, 6).Merge();

                            if (lstEVGandEVI.Count() != 0)
                            {
                                int rowPercent = startRow - 1;
                                for (int i = rowPercent; i > 0; i--)
                                {
                                    if (sheetFill.GetCellValue(rowPercent, 2) == null)
                                    {
                                        rowPercent--;
                                    }
                                    else
                                    {
                                        formular = "=ROUND(G" + startRow + "*100/A" + rowPercent + ",2)";
                                        sheetFill.SetFormulaValue(startRow + 1, 7, formular);
                                        formular = "=ROUND(H" + startRow + "*100/A" + rowPercent + ",2)";
                                        sheetFill.SetFormulaValue(startRow + 1, 8, formular);
                                        if ((educationLevelInCombineClass != 0 && educationLevelInCombineClass == 18))
                                        {
                                            formular = "=ROUND(I" + startRow + "*100/A" + rowPercent + ",2)";
                                            sheetFill.SetFormulaValue(startRow + 1, 9, formular);
                                        }
                                        break;
                                    }

                                }

                            }
                            else
                            {
                                sheetFill.SetFormulaValue(startRow + 1, 7, "0");
                                sheetFill.SetFormulaValue(startRow + 1, 8, "0");
                                if ((educationLevelInCombineClass != 0 && educationLevelInCombineClass == 18))
                                {
                                    sheetFill.SetFormulaValue(startRow + 1, 9, "0");
                                }

                            }
                            sheetFill.GetRange(startRow + 1, 1, startRow + 1, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                            sheetFill.GetRange(startRowTemp, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignCenter);

                            if (educationLevelInCombineClass == 18)
                                sheetFill.GetRange(startRowTemp - 1, 1, startRow + 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                            else
                                sheetFill.GetRange(startRowTemp - 1, 1, startRow + 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                            sheetFill.GetRange(startRowTemp, 7, startRow + 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheetFill.GetRange(startRow, 1, startRow + 1, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);

                            sheetFill.SetBreakPage(startRow + 5);
                            #endregion
                            startRow = startRow + 5;
                            startRowTemp = startRow + 10;
                            educationLevelInCombineClass = 0;
                        }
                        sheetFill.SetFontName("Times New Roman", 12);
                        sheetFill.Name = Utils.Utils.StripVNSignAndSpace(string.Format("{0} ({1})", item.DisplayName, classID));
                    }
                }
                sheet.Delete();
                #endregion
            }
            else
            {
                #region
                _classId = 0;
                if (!string.IsNullOrEmpty(classID))
                {
                    _classId = Int32.Parse(classID);
                }
                List<int> lstChildrenID = lstPupilBO.Select(x => x.PupilProfileID).Distinct().ToList();
                for (int p = 0; p < lstChildrenID.Count; p++)
                {
                    #region report excel
                    int stt = 0;
                    int itemChildrenID = lstChildrenID[p];

                    lstGrow = lstGrowtemp.Where(x => x.pupilID == itemChildrenID && x.schoolID == _globalInfo.SchoolID &&
                                              x.classID == _classId && x.academicID == _globalInfo.AcademicYearID).ToList();

                    sheet.SetCellValue("A" + startRow, school.SupervisingDept.SupervisingDeptName.ToUpper());
                    sheet.GetRange(startRow, 1, startRow, 3).Merge();
                    sheet.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, 3).SetFontStyle(false, System.Drawing.Color.Black, false, 12, false, false);
                    startRow++;

                    sheet.SetCellValue("A" + startRow, _globalInfo.SchoolName.ToUpper());
                    sheet.GetRange(startRow, 1, startRow, 3).Merge();
                    sheet.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow, 1, startRow, 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, true);
                    startRow = startRow + 2;

                    sheet.SetCellValue("A" + startRow, Title);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).Merge();
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    startRow++;
                    sheet.SetCellValue("A" + startRow, "Năm học " + objAca.DisplayTitle);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).Merge();
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    startRow = startRow + 2;

                    sheet.SetCellValue("A" + startRow, "Họ và tên: " + lstPupilBO[p].FullName);
                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.SetCellValue("C" + startRow, lstPupilBO[p].BirthDate == null ? "Ngày sinh: " : "Ngày sinh: " + lstPupilBO[p].BirthDate.ToString("dd/MM/yyyy"));
                    sheet.GetRange(startRow, 3, startRow, 4).Merge();
                    sheet.GetRange(startRow, 3, startRow, 4).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                    startRow++;

                    lstEvaluationGroup = _lstEvaluationGroup.ToList();
                    lstPupilBO[p].ClassName = lstPupilBO[p].ClassName.Replace("Lớp", "");
                    lstPupilBO[p].ClassName = lstPupilBO[p].ClassName.Replace("lớp", "");
                    sheet.SetCellValue("A" + startRow, "Lớp: " + lstPupilBO[p].ClassName.Trim());

                    sheet.GetRange(startRow, 1, startRow, 2).Merge();
                    sheet.GetRange(startRow, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignLeft);

                    if (iqEmployee.Count() != 0)
                        sheet.SetCellValue("C" + startRow, iqEmployee[0] == null ? "Giáo viên" : "Giáo viên: " + iqEmployee[0].FullName); //objEmployee[0].FullName
                    else
                        sheet.SetCellValue("C" + startRow, "Giáo viên: ");
                    sheet.GetRange(startRow, 3, startRow, 6).Merge();
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow, 3, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);
                    startRow = startRow + 3;

                    sheet.SetCellValue(startRow - 1, 1, "Chỉ số");
                    sheet.SetCellValue(startRow - 1, 2, "Nội dung chỉ số");
                    sheet.SetCellValue(startRow - 1, 7, "Đạt");
                    sheet.SetCellValue(startRow - 1, 8, "Chưa đạt");
                    sheet.SetRowHeight(startRow - 1, 20);
                    if (educationLevelID == 18)
                    {
                        sheet.SetCellValue(startRow - 1, 9, "±");
                    }

                    sheet.GetRange(startRow - 1, 1, startRow - 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow - 1, 1, startRow - 1, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheet.GetRange(startRow - 1, 1, startRow - 1, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    sheet.GetRange(startRow - 1, 2, startRow - 1, 6).Merge();

                    for (int i = 0; i < lstEvaluationGroup.Count(); i++)
                    {
                        sheet.SetCellValue(startRow, 1, lstEvaluationGroup[i].EvaluationDevelopmentGroupName);
                        sheet.SetRowHeight(startRow, 18);

                        sheet.GetRange(startRow, 1, startRow, educationLevelID != 18 ? 8 : 9).Merge();
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                        sheet.GetRange(startRow, 1, startRow, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                        startRow++;

                        for (int j = 0; j < lstEVGandEVI.Count(); j++)
                        {
                            if (lstEVGandEVI[j].EvaluationDevelopmentGroupID == lstEvaluationGroup[i].EvaluationDevelopmentGroupID)
                            {
                                stt++;
                                sheet.SetCellValue(startRow, 1, stt);
                                sheet.GetRange(startRow, 1, startRow, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                                sheet.SetCellValue(startRow, 2, lstEVGandEVI[j].EvaluationDevelopmentName);
                                sheet.GetRange(startRow, 2, startRow, 6).WrapText();
                                sheet.GetRange(startRow, 2, startRow, 6).Merge();
                                sheet.SetRowHeight(startRow - 1, 16.5);
                                sheet.GetRange(startRow, 2, startRow, 6).SetHAlign(VTHAlign.xlHAlignLeft);

                                for (int h = 0; h < lstGrow.Count(); h++)
                                {
                                    if (lstGrow[h].evaluationDevelopmentID == lstEVGandEVI[j].EvaluationDevelopmentID)
                                    {
                                        if (lstGrow[h].evaluationType == 1)
                                        {
                                            sheet.SetCellValue(startRow, 7, '+');
                                        }
                                        else if (lstGrow[h].evaluationType == 2)
                                        {
                                            sheet.SetCellValue(startRow, 8, '-');
                                        }
                                        else if (lstGrow[h].evaluationType == 3)
                                        {
                                            sheet.SetCellValue(startRow, 9, '±');
                                        }
                                    }
                                }
                                sheet.GetRange(startRow, 7, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                                startRow++;
                            }
                        }
                    }

                    sheet.SetCellValue(startRow, 1, "Tổng số");
                    sheet.GetRange(startRow, 1, startRow, 6).Merge();
                    sheet.SetCellValue(startRow, 7, "=COUNTIF(G" + startRowTemp + ":G" + (startRow - 1) + ",\"+\")");
                    sheet.SetCellValue(startRow, 8, "=COUNTIF(H" + startRowTemp + ":H" + (startRow - 1) + ",\"-\")");
                    sheet.SetRowHeight(startRow, 16.5);
                    if (educationLevelID == 18)
                    {
                        sheet.SetCellValue(startRow, 9, "=COUNTIF(I" + startRowTemp + ":I" + (startRow - 1) + ",\"±\")");
                    }
                    sheet.GetRange(startRow, 1, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                    sheet.SetCellValue(startRow + 1, 1, "Tỷ lệ %");
                    sheet.SetRowHeight(startRow + 1, 15.75);
                    sheet.GetRange(startRow + 1, 1, startRow + 1, 6).Merge();

                    if (lstEVGandEVI.Count() != 0)
                    {
                        int rowPercent = startRow - 1;
                        var k = sheet.GetCellValue(rowPercent, 2);
                        for (int i = rowPercent; i > 0; i--)
                        {
                            if (sheet.GetCellValue(rowPercent, 2) == null)
                            {
                                rowPercent--;
                            }
                            else
                            {
                                formular = "=ROUND(G" + startRow + "*100/A" + rowPercent + ",2)";
                                sheet.SetFormulaValue(startRow + 1, 7, formular);
                                formular = "=ROUND(H" + startRow + "*100/A" + rowPercent + ",2)";
                                sheet.SetFormulaValue(startRow + 1, 8, formular);
                                if (educationLevelID == 18)
                                {
                                    formular = "=ROUND(I" + startRow + "*100/A" + rowPercent + ",2)";
                                    sheet.SetFormulaValue(startRow + 1, 9, formular);
                                }
                                break;
                            }

                        }

                    }
                    else
                    {
                        sheet.SetFormulaValue(startRow + 1, 7, "0");
                        sheet.SetFormulaValue(startRow + 1, 8, "0");
                        if (educationLevelID == 18)
                        {
                            sheet.SetFormulaValue(startRow + 1, 9, "0");
                        }

                    }
                    sheet.GetRange(startRow + 1, 1, startRow + 1, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                    sheet.GetRange(startRowTemp, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignCenter);

                    sheet.GetRange(startRowTemp - 1, 1, startRow + 1, educationLevelID != 18 ? 8 : 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    sheet.GetRange(startRowTemp, 7, startRow + 1, 9).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(startRow, 1, startRow + 1, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);

                    sheet.SetBreakPage(startRow + 5);
                    #endregion
                    startRow = startRow + 5;
                    startRowTemp = startRow + 10;
                    #endregion
                }

                sheet.SetFontName("Times New Roman", 12);
                sheet.Name = Utils.Utils.StripVNSignAndSpace(educationLevelID != SystemParamsInFile.Combine_Class_ID ? lstPupilBO.Count > 0 ? lstPupilBO[0].ClassName : "Sheet1" : classID);
                #endregion
            }

            return oBook.ToStream();
        }

        // danh sach hoc sinh trong lop
        private List<PupilOfClassBO> ListPupilOfClass(List<int> lstClassID)
        {
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                 where poc.AcademicYearID == _globalInfo.AcademicYearID.Value
                                 && poc.SchoolID == _globalInfo.SchoolID
                                 && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                 && lstClassID.Contains(poc.ClassID)
                                 && pf.IsActive == true
                                 select new PupilOfClassBO
                                 {
                                     PupilID = poc.PupilID,
                                     ClassID = poc.ClassID,
                                     PupilFullName = poc.PupilProfile.FullName,
                                     PupilCode = poc.PupilProfile.PupilCode,
                                     Birthday = poc.PupilProfile.BirthDate,
                                     Genre = poc.PupilProfile.Genre,
                                     EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                                     OrderInClass = poc.OrderInClass,
                                     Name = pf.Name,
                                     Combine_Class_Code = poc.ClassProfile.CombinedClassCode,
                                     EducationLevelID = poc.ClassProfile.EducationLevelID
                                 }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();
            return lstPupilOfClassBO;
        }

    }
}
