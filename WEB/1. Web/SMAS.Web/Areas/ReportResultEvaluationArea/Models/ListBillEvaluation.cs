﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Models
{
    public class ListBillEvaluation
    {
        public int schoolID { get; set; }
        public string schoolName { get; set; }
        public int academicID { get; set; }

        public int educationID { get; set; }
        public int educationIDDEG { get; set; }
        public int educationIDDEI { get; set; }
        public int classID { get; set; }
        public string CombineClassCode { get; set; }
        public int classIDGrow { get; set; }
        public string className { get; set; }
        public int pupilID { get; set; }
        public string pupilName { get; set; }
        public int evaluationDevelopmentGroupID { get; set; }
        public string evaluationDevelopmentGroupName { get; set; }
        public int evaluationDevelopmentID { get; set; }
        public string evaluationDevelopmentName { get; set; }
        public int? evaluationType { get; set; }
        public string teacherName { get; set; }
        public DateTime birthdate { get; set; }

        // Ma lop trong bang DeclareEvaluationIndex
        public int? ClassID2 { get; set; }
        public int DeclareEvaluationIndexID { get; set; }
    }
}