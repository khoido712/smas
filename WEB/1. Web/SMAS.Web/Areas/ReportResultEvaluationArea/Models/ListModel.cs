﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Models
{
    public class ListModel
    {
        public int EvaluationDevelopmentID { set; get; }
        public int EvaluationDevelopmentGroupID { set; get; }
        public string EvaluationDevelopmentGroupName { set; get; }
        public string EvaluationDevelopmentName { set; get; }
        public string EvaluationDevelopmentCode { set; get; }
        public int educationLevelID { set; get; }
        public int educationLevelIDEDG { get; set; }
        public int? ProvinceID { get; set; }
        public int? ClassID { get; set; }
        public int? OrderEvaluationGroup { get; set; }
        public int? OrderID { get; set; }
    }
}
