﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Models
{
    public class InfoEvaluationHealthy
    {
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int AcademicYear { get; set; }
        public int PupilID { get; set; }
        public string FullName { get; set; }

        /// <summary>
        /// Trang thai hoc sinh (dang hoc, chuyen truong...)
        /// </summary>
        public int ProfileStatus { get; set; }

        /// <summary>
        /// chieu cao
        /// </summary>
        public decimal Height { get; set; }
        public int? HeightStatus { get; set; }
        /// <summary>
        /// can nang
        /// </summary>
        public decimal Weight { get; set; }
        public int? WeightStatus { get; set; }

        /// <summary>
        /// trinh trang suc khoe
        /// </summary>
        public int EvaluationHealth { get; set; }

        public DateTime? MonitoringDate { get; set; }

        /// <summary>
        /// so ngay nghi
        /// </summary>
        public int NumberAbsence { get; set; }

        /// <summary>
        /// so ngay nghi co phep
        /// </summary>
        public int HasAccepted { get; set; }

        /// <summary>
        /// so ngay ngi khong phep
        /// </summary>
        public int NoAccepted { get; set; }

        /// <summary>
        /// danh sach nhan xet theo thang cua hoc sinh
        /// </summary>
        /// 
      
 
    }
}