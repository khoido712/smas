﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportResultEvaluationArea.Models
{
    public class EmployModel
    {
        public int classID { get; set; }
        public string FullName { get; set; }
        public int EmployID { get; set; }
    }
}