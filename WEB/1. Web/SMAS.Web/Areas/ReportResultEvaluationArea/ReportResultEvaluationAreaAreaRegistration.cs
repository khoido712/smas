﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportResultEvaluationArea
{
    public class ReportResultEvaluationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportResultEvaluationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportResultEvaluationArea_default",
                "ReportResultEvaluationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
