/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Areas.FaultCriteriaArea.Models;
using System.Threading;
using System.Globalization;

namespace SMAS.Web.Areas.FaultCriteriaArea.Controllers
{
    // TODO : HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
    public class FaultCriteriaController : BaseController
    {
        private readonly IFaultCriteriaBusiness FaultCriteriaBusiness;
        private readonly IFaultGroupBusiness FaultGroupBusiness;

        public FaultCriteriaController(IFaultCriteriaBusiness faultcriteriaBusiness, IFaultGroupBusiness faultGroupBusiness)
        {
            this.FaultCriteriaBusiness = faultcriteriaBusiness;
            this.FaultGroupBusiness = faultGroupBusiness;
        }

        //
        // GET: /FaultCriteria/
        public ActionResult Index()
        {
            SetViewDataPermission("FaultCriteria", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here
            IEnumerable<FaultCriteriaViewModel> lst = this._Search(SearchInfo);
            ViewData[FaultCriteriaConstants.LIST_FAULTCRITERIA] = lst;
            GetViewData();
            return View();
        }

        //
        // GET: /FaultCriteria/Search
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolusion;
            SearchInfo["FaultGroupID"] = frm.GroupID;

            IEnumerable<FaultCriteriaViewModel> lst = this._Search(SearchInfo);
            ViewData[FaultCriteriaConstants.LIST_FAULTCRITERIA] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(FormCollection form)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            FaultCriteria faultcriteria = new FaultCriteria();
            TryUpdateModel(faultcriteria);
            //faultcriteria.PenalizedMark = Convert.ToDecimal(form["PenalizedMark"], CultureInfo.CreateSpecificCulture("en-US"));
            Utils.Utils.TrimObject(faultcriteria);
            faultcriteria.SchoolID = new GlobalInfo().SchoolID.GetValueOrDefault();
            this.FaultCriteriaBusiness.Insert(faultcriteria);
            this.FaultCriteriaBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int FaultCriteriaID, FormCollection form)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            FaultCriteria faultcriteria = this.FaultCriteriaBusiness.Find(FaultCriteriaID);
            TryUpdateModel(faultcriteria);
          //  faultcriteria.PenalizedMark = Convert.ToDecimal(form["PenalizedMark"], CultureInfo.CreateSpecificCulture("en-US"));
            Utils.Utils.TrimObject(faultcriteria);
            faultcriteria.SchoolID = new GlobalInfo().SchoolID.GetValueOrDefault();
            this.FaultCriteriaBusiness.Update(faultcriteria);
            this.FaultCriteriaBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultCriteria", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.FaultCriteriaBusiness.Delete(new GlobalInfo().SchoolID.GetValueOrDefault(), id);
            this.FaultCriteriaBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<FaultCriteriaViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("FaultCriteria", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<FaultCriteria> query = this.FaultCriteriaBusiness.SearchBySchool(
                new GlobalInfo().SchoolID.GetValueOrDefault(), SearchInfo);
            IQueryable<FaultCriteriaViewModel> lst = query.Select(o => new FaultCriteriaViewModel
            {
                FaultCriteriaID = o.FaultCriteriaID,
                Resolution = o.Resolution,
                GroupID = o.GroupID,
                GroupResolution = o.FaultGroup.Resolution,
                PenalizedMark = o.PenalizedMark,
                Absence = o.Absence,
                Description = o.Description
            });
            List<FaultCriteriaViewModel> listFaultCriteria = lst.ToList();
            listFaultCriteria = listFaultCriteria.OrderBy(o => o.GroupResolution).ThenBy(o => o.Resolution).ToList();
            return listFaultCriteria;
        }

        private void GetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<FaultGroup> lstGroup = this.FaultGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.GetValueOrDefault(),
                SearchInfo).ToList();
            lstGroup = lstGroup.OrderBy(o => o.Resolution).ToList();
            ViewData[FaultCriteriaConstants.LIST_FAULTGROUP] = lstGroup != null ? new SelectList(lstGroup, "FaultGroupID", "Resolution") : null;

            List<ComboObject> lstAbsence = new List<ComboObject>();
            lstAbsence.Add(new ComboObject(Res.Get("FaultCriteria_HasPermission"), "P"));
            lstAbsence.Add(new ComboObject(Res.Get("FaultCriteria_NoPermission"), "K"));

            SelectList sltAbsence = new SelectList(lstAbsence, "value", "key");
            ViewData[FaultCriteriaConstants.LIST_FAULTCRITERIA_ABSENCE] = sltAbsence;
        }
    }
}





