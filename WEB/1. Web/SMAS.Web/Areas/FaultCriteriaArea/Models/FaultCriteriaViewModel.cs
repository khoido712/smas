/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.FaultCriteriaArea.Models
{
    public class FaultCriteriaViewModel
    {
        [ScaffoldColumn(false)]
		public int FaultCriteriaID { get; set; }

        [ResourceDisplayName("FaultCriteria_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public string Resolution { get; set; }

        [ResourceDisplayName("FaultCriteria_Label_GroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", FaultCriteriaConstants.LIST_FAULTGROUP)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> GroupID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("FaultCriteria_Label_GroupID")]
        public string GroupResolution { get; set; }

        [ResourceDisplayName("FaultCriteria_Label_PenalizedMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [AdditionalMetadata("class", "decimal")]
        //[RegularExpression(@"^[0-9,]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public Nullable<System.Decimal> PenalizedMark { get; set; }

        [ResourceDisplayName("FaultCriteria_Label_Absence")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", FaultCriteriaConstants.LIST_FAULTCRITERIA_ABSENCE)]
        public string Absence { get; set; }

        [ResourceDisplayName("FaultCriteria_Label_Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public string Description { get; set; }	
    }
}


