﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FaultCriteriaArea
{
    public class FaultCriteriaAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FaultCriteriaArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FaultCriteriaArea_default",
                "FaultCriteriaArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
