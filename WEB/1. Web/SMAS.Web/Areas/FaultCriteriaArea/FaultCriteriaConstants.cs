/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.FaultCriteriaArea
{
    public class FaultCriteriaConstants
    {
        public const string LIST_FAULTCRITERIA = "listFaultCriteria";

        public const string LIST_FAULTGROUP = "listFaultGroup";

        public const string LIST_FAULTCRITERIA_ABSENCE = "listFaultCriteriaAbsence";
    }
}