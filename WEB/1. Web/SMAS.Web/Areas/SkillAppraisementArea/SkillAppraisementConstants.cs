﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SkillAppraisementArea
{
    public class SkillAppraisementConstants
    {
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string LIST_PUPIL = "LIST_PUPILS";
        public const string SKILL_DETAIL = "SKILL_DETAIL";
        public const string LIST_SKILL = "LIST_SKILL";
        public const string LIST_SKILL_COMMENT = "LIST_SKILL_COMMENT";
        public const string LIST_PUPIL_RANKING = "LIST_PUPIL_RANKING";
        public const string CLASS_NAME = "CLASS_NAME";
        public const string CLASS_ID = "CLASS_ID";
        public const string SUM_OF_SKILL = "SUM_OF_SKILL";
    }
}