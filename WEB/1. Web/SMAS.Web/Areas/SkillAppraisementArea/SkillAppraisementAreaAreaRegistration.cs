﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SkillAppraisementArea
{
    public class SkillAppraisementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SkillAppraisementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SkillAppraisementArea_default",
                "SkillAppraisementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
