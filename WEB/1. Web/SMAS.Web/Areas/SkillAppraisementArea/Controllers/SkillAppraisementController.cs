﻿/*
    author Anhvd
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.SkillAppraisementArea.Models;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.SkillAppraisementArea.Controllers
{
    public class SkillAppraisementController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        private readonly ISkillOfPupilBusiness SkillOfPupilBusiness;
        private readonly ISkillBusiness SkillBusiness;
        private readonly ISkillTitleBusiness SkillTitleBusiness;
        private readonly ISkillTitleClassBusiness SkillTitleClassBusiness;
        private readonly ISkillOfClassBusiness SkillOfClassBusiness;

        public SkillAppraisementController(
            IAcademicYearBusiness academicYearBusiness,
            IClassProfileBusiness classprofilebusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            ISkillOfPupilBusiness SkillOfPupilBusiness,
            ISkillBusiness SkillBusiness,
            ISkillTitleBusiness SkillTitleBusiness,
            ISkillTitleClassBusiness SkillTitleClassBusiness,
            ISkillOfClassBusiness SkillOfClassBusiness)
        {
            this.AcademicYearBusiness = academicYearBusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SkillOfPupilBusiness = SkillOfPupilBusiness;
            this.SkillBusiness = SkillBusiness;
            this.SkillTitleBusiness = SkillTitleBusiness;
            this.SkillOfClassBusiness = SkillOfClassBusiness;
            this.SkillTitleClassBusiness = SkillTitleClassBusiness;
        }
        //
        // GET: /SkillAppraisementArea/SkillAppraisement/

        public ActionResult Index(int? ClassID)
        {
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            ClassSearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            SelectListItem item;
            // Model cho combo Khoi
            List<SelectListItem> lstLevelItem = new List<SelectListItem>();
            IQueryable<EducationLevel> IqLevel = lstClass.Select(o => o.EducationLevel).Distinct();
            foreach (var level in IqLevel)
            {
                item = new SelectListItem();
                item.Text = level.Resolution;
                item.Value = level.EducationLevelID.ToString();
                lstLevelItem.Add(item);
            }
            ViewData[SkillAppraisementConstants.LIST_EDUCATION_LEVEL] = lstLevelItem;
            // Model cho combo Lop

            List<SelectListItem> lstClassItem = new List<SelectListItem>();
            lstClass = lstClass.Where(o => o.EducationLevelID == IqLevel.FirstOrDefault().EducationLevelID).OrderBy(o => o.DisplayName);

            foreach (var cls in lstClass)
            {
                item = new SelectListItem();
                item.Text = cls.DisplayName;
                item.Value = cls.ClassProfileID.ToString();
                if (cls.ClassProfileID == ClassID) item.Selected = true;
                lstClassItem.Add(item);
            }
            ViewData[SkillAppraisementConstants.LIST_CLASS] = lstClassItem;

            // Dieu kien tim kiem 
            ClassSearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            ClassSearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            ClassSearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            ClassSearchInfo["IsActive"] = true;

            // Model cho combo Chu de
            int EducationLevelID = IqLevel.FirstOrDefault().EducationLevelID;
            List<SelectListItem> lstSubjectItem = new List<SelectListItem>();
            if (!ClassID.HasValue)
            {
                ClassID = lstClass.FirstOrDefault().ClassProfileID;
                if (ClassID.HasValue && ClassID > 0)
                {
                    ClassSearchInfo["ClassID"] = ClassID;
                    List<int?> LstSkillTitleID = SkillTitleClassBusiness.Search(ClassSearchInfo).Select(o => o.SkillTitleID).Distinct().ToList();
                    IQueryable<SkillTitleBO> IqSkillTitle = SkillTitleBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo);
                    bool IsFirst = true;
                    foreach (var skillTitle in IqSkillTitle)
                    {
                        if (LstSkillTitleID.Contains(skillTitle.SkillTitleID))
                        {
                            item = new SelectListItem();
                            item.Text = skillTitle.SkillTitle;
                            item.Value = skillTitle.SkillTitleID.ToString();
                            if (IsFirst)
                            {
                                item.Selected = true;
                                IsFirst = false;
                            }
                            lstSubjectItem.Add(item);
                        }
                    }
                }
            }
            ViewData[SkillAppraisementConstants.LIST_SUBJECT] = lstSubjectItem;

            // danh sach chi tiet
            List<PupilViewModel> lstPupil = new List<PupilViewModel>();
            if (lstSubjectItem.Count > 0)
            {
                lstPupil = GetListPupil(ClassID);
                int FirstSkillTitleID = Int32.Parse(lstSubjectItem.First().Value);
                List<SkillViewModel> lstSkill = GetListSkill(FirstSkillTitleID);
                if (lstSkill.Count > 0)
                {
                    ViewData[SkillAppraisementConstants.SKILL_DETAIL] = lstSkill.First();
                    ViewData[SkillAppraisementConstants.SUM_OF_SKILL] = lstSkill.Count;
                    Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["ClassID"] = ClassID;
                    SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                    SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                    IQueryable<SkillOfPupil> IqSkillOfPupil = SkillOfPupilBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo);
                    SkillOfPupil skillOfPupil = null;
                    int PupilID = 0;
                    int FirstSkillID = lstSkill.First().SkillID;
                    int CountPupil = lstPupil.Count;
                    for (int i = 0; i < CountPupil; i++)
                    {
                        PupilID = lstPupil[i].PupilID;
                        skillOfPupil = IqSkillOfPupil.Where(o => o.PupilID == PupilID && o.SkillOfClassID == FirstSkillID).FirstOrDefault();
                        if (skillOfPupil != null)
                        {
                            lstPupil[i].CommentOfTeacher = skillOfPupil.CommentOfTeacher;
                            lstPupil[i].CommentOfParent = skillOfPupil.CommentOfParent;
                            lstPupil[i].IsPass = skillOfPupil.IsPass;
                            lstPupil[i].Rank = skillOfPupil.Rank.HasValue ? skillOfPupil.Rank.ToString() : string.Empty;
                            lstPupil[i].Prize = skillOfPupil.Prize;
                        }
                    }
                }
            }

            ViewData[SkillAppraisementConstants.LIST_PUPIL] = lstPupil;
            return View();
        }

        public List<SkillViewModel> GetListSkill(int? SkillTitleID)
        {
            //Danh sach ky nang tra ve
            List<SkillViewModel> LstKillModel = new List<SkillViewModel>();
            if (SkillTitleID.HasValue && SkillTitleID.Value > 0)
            {
                int schoolID = _globalInfo.SchoolID.HasValue ? _globalInfo.SchoolID.Value : 0;
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SkillTitleID"] = SkillTitleID;
                IQueryable<SkillOfClass> IqSkillOfClass = SkillOfClassBusiness.Search(SearchInfo).OrderBy(o => o.FromDate);
                IQueryable<Skill> IqSkill = SkillBusiness.Search(SearchInfo).OrderBy(o => o.FromDate);

                if (IqSkillOfClass == null || IqSkillOfClass.Count() <= 0)
                {
                    // su dung ds Skill
                    foreach (var item in IqSkill)
                    {
                        LstKillModel.Add(new SkillViewModel()
                        {
                            Period = item.Period,
                            Description = item.Description,
                            SkillID = item.SkillID,
                            SkillTitleID = item.SkillTitleID,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate
                        });
                    }
                }
                else // su dung ds SkillOfClass
                {
                    foreach (var item in IqSkillOfClass)
                    {
                        LstKillModel.Add(new SkillViewModel()
                        {
                            Period = item.Period,
                            Description = item.Description,
                            SkillID = item.SkillOfClassID,
                            SkillTitleID = item.SkillTitleID.Value,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate
                        });
                    }
                }
                Session[SkillAppraisementConstants.LIST_SKILL] = LstKillModel;
            }
            return LstKillModel;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SelectSkill(int STT, int? ClassID, int? SkillTitleID)
        {
            List<SkillViewModel> LstSKillModel = new List<SkillViewModel>();
            SkillViewModel Skill = new SkillViewModel();
            if (SkillTitleID.HasValue)
            {
                if (Session[SkillAppraisementConstants.LIST_SKILL] == null)
                {
                    // lay danh sach
                    LstSKillModel = this.GetListSkill(SkillTitleID);
                }
                else
                {
                    LstSKillModel = (List<SkillViewModel>)Session[SkillAppraisementConstants.LIST_SKILL];
                }
                if (STT < 0 || STT > LstSKillModel.Count - 1) return PartialView("_SkillInfo");
                Skill = LstSKillModel[STT];
            }
            ViewData[SkillAppraisementConstants.SUM_OF_SKILL] = LstSKillModel.Count;
            ViewData[SkillAppraisementConstants.SKILL_DETAIL] = Skill;
            return PartialView("_SkillInfo");
        }

        public List<PupilViewModel> GetListPupil(int? ClassID)
        {
            //Danh sach hoc sinh tra ve
            List<PupilViewModel> lstPupilRet = new List<PupilViewModel>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                int schoolID = _globalInfo.SchoolID.HasValue ? _globalInfo.SchoolID.Value : 0;
                ClassProfile obj = ClassProfileBusiness.Find(ClassID);
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["CurrentClassID"] = ClassID;
                SearchInfo["CurrentSchoolID"] = _globalInfo.SchoolID;
                SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;

                // lay ve danh sach hoc sinh theo cap, lop, truong, nam
                IQueryable<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(schoolID, SearchInfo);

                if (lstPupil != null && lstPupil.Count() > 0)
                {
                    List<PupilProfileBO> lstPPBO = lstPupil.ToList();
                    int Count = lstPPBO.Count;
                    int index = 1;
                    PupilProfileBO ppBO;
                    for (int i = 0; i < Count; i++)
                    {
                        ppBO = lstPPBO[i];
                        lstPupilRet.Add(new PupilViewModel()
                        {
                            STT = index++,
                            PupilID = ppBO.PupilProfileID,
                            ClassID = ClassID.Value,
                            PupilName = ppBO.FullName,
                            Status = ppBO.ProfileStatus,
                            ImageData = ppBO.Image
                        });
                    }
                }
            }
            Session[SkillAppraisementConstants.LIST_PUPIL] = lstPupilRet;
            return lstPupilRet;
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult ViewDetail(int? ClassID, int? SkillID)
        {
            List<PupilViewModel> lstPupil = new List<PupilViewModel>();
            if (SkillID.HasValue)
            {
                if (Session[SkillAppraisementConstants.LIST_PUPIL] == null)
                {
                    lstPupil = GetListPupil(ClassID);
                }
                else
                {
                    lstPupil = (List<PupilViewModel>)Session[SkillAppraisementConstants.LIST_PUPIL];
                }

                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["ClassID"] = ClassID;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                IQueryable<SkillOfPupil> IqSkillOfPupil = SkillOfPupilBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo);
                SkillOfPupil skillOfPupil = null;
                int PupilID = 0;
                int CountPupil = lstPupil.Count;
                for (int i = 0; i < CountPupil; i++)
                {
                    PupilID = lstPupil[i].PupilID;
                    skillOfPupil = IqSkillOfPupil.Where(o => o.PupilID == PupilID && o.SkillOfClassID == SkillID).FirstOrDefault();
                    if (skillOfPupil != null)
                    {
                        lstPupil[i].CommentOfTeacher = skillOfPupil.CommentOfTeacher;
                        lstPupil[i].CommentOfParent = skillOfPupil.CommentOfParent;
                        lstPupil[i].IsPass = skillOfPupil.IsPass;
                        lstPupil[i].Rank = (skillOfPupil.Rank.HasValue && skillOfPupil.Rank.Value > 0) ? skillOfPupil.Rank.ToString() : string.Empty;
                        lstPupil[i].Prize = skillOfPupil.Prize;
                    }
                    else 
                    {
                        lstPupil[i].CommentOfTeacher = string.Empty;
                        lstPupil[i].CommentOfParent = string.Empty;
                        lstPupil[i].IsPass = null;
                        lstPupil[i].Rank = string.Empty;
                        lstPupil[i].Prize = string.Empty;
                    }
                }
            }

            ViewData[SkillAppraisementConstants.LIST_PUPIL] = lstPupil;
            return PartialView("_Detail");
        }


        [ValidateAntiForgeryToken]
        public JsonResult ViewComment(int? ClassID, int? SkillID)
        {
            List<PupilViewModel> lstPupil = new List<PupilViewModel>();
            if (Session[SkillAppraisementConstants.LIST_PUPIL] == null)
            {
                lstPupil = GetListPupil(ClassID);
            }
            else
            {
                lstPupil = (List<PupilViewModel>)Session[SkillAppraisementConstants.LIST_PUPIL];
            }

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            IQueryable<SkillOfPupil> IqSkillOfPupil = SkillOfPupilBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo);
            SkillOfPupil skillOfPupil = null;
            List<string> LSTCommentOfTeachers = new List<string>();
            List<string> LSTCommentOfParents = new List<string>();
            int PupilID = 0;
            int CountPupil = lstPupil.Count;
            for (int i = 0; i < CountPupil; i++)
            {
                PupilID = lstPupil[i].PupilID;
                skillOfPupil = IqSkillOfPupil.Where(o => o.PupilID == PupilID && o.SkillOfClassID == SkillID).FirstOrDefault();
                if (skillOfPupil != null)
                {
                    lstPupil[i].CommentOfTeacher = skillOfPupil.CommentOfTeacher;
                    lstPupil[i].CommentOfParent = skillOfPupil.CommentOfParent;
                    lstPupil[i].IsPass = skillOfPupil.IsPass;
                    lstPupil[i].Rank = skillOfPupil.Rank.HasValue ? skillOfPupil.Rank.ToString() : string.Empty;
                    lstPupil[i].Prize = skillOfPupil.Prize;
                }
                else
                {
                    lstPupil[i].CommentOfTeacher = string.Empty;
                    lstPupil[i].CommentOfParent = string.Empty;
                    lstPupil[i].IsPass = null;
                    lstPupil[i].Rank = string.Empty;
                    lstPupil[i].Prize = string.Empty;
                }
            }
            ViewData[SkillAppraisementConstants.LIST_PUPIL] = lstPupil;
            return Json(new
            {
                LSTCommentTeachers = new List<String>(),
                LSTCommentParents = "DS",
            });
        }

        #region load combobox
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID", EducationLevelID},
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value}
                };

            if (!_globalInfo.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            return lsCP;
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private IQueryable<SkillTitleBO> getSubjectFromClassFrofile(int EducationLevelID, int ClassID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID", EducationLevelID},
                    {"AppliedLevel", _globalInfo.AppliedLevel},
                    {"ClassID", ClassID},
                    {"AcademicYearID", _globalInfo.AcademicYearID},
                    {"SchoolID", _globalInfo.SchoolID},
                    {"IsActive", true}
                };

            IQueryable<SkillTitleBO> lsSkillTitle = SkillTitleBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            return lsSkillTitle;
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? EducationLevelID, int? ClassID)
        {
            if (EducationLevelID.HasValue && ClassID.HasValue)
            {
                IQueryable<SkillTitleBO> lstSkillTitle = getSubjectFromClassFrofile(EducationLevelID.Value, ClassID.Value);
                if (lstSkillTitle.Count() != 0)
                {
                    return Json(new SelectList(lstSkillTitle.ToList(), "SkillTitleID", "SkillTitle"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }
        #endregion

        #region Lưu đánh giá

        [ValidateAntiForgeryToken]
        public JsonResult SaveData(DataViewModel data)
        {
            List<int> LSTPupilID = data.LSTPupilID;
            List<string> LSTCommentOfTeacher = data.LSTCommentOfTeacher;
            List<int?> LSTRank = data.LSTRank;
            List<bool?> LSTIsPass = data.LSTIsPass;
            List<string> LSTPrizes = data.LSTPrizes;
            int? SkillTitleID = data.SkillTitleID;
            int? SkillID = data.SkillID;
            int? ClassID = data.ClassID;
            int? EducationLevelID = data.EducationLevelID;
            if (!ClassID.HasValue) return Json(new
            {
                Type = "error",
                Message = Res.Get("Common_Label_Save"),
            });
            // kiem tra du lieu 
            if (LSTPupilID == null || LSTCommentOfTeacher == null ||
                LSTRank == null || LSTIsPass == null || LSTPrizes == null)
            {
                return Json(new
                {
                    Type = "error",
                    Message = Res.Get("WeeklyMenu_Label_ErrorData")
                });
            }

            if (LSTPupilID.Count != LSTCommentOfTeacher.Count || LSTPupilID.Count != LSTRank.Count ||
                LSTPupilID.Count != LSTIsPass.Count || LSTPupilID.Count != LSTPrizes.Count)
            {
                return Json(new
                {
                    Type = "error",
                    Message = Res.Get("WeeklyMenu_Label_ErrorData")
                });
            }

            // kiem tra giao vien co quyen thao tac voi lop

            bool isInRole = false;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                isInRole = true;
            }
            else
            {
                bool IsOverSeeing = ClassProfileBusiness.CheckRoleOfClass(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value, ClassID.Value, Business.Business.RoleOfClass.Overseeing);
                if (IsOverSeeing)
                {
                    isInRole = true;
                }
                else
                {
                    bool IsHeadTeacher = ClassProfileBusiness.CheckRoleOfClass(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value, ClassID.Value, Business.Business.RoleOfClass.HeadTeacher);
                    if (IsHeadTeacher)
                    {
                        isInRole = true;
                    }
                }
            }
            if (isInRole) // co quyen thao tac
            {
                // lay danh sach lop tuong ung
                int schoolID = _globalInfo.SchoolID.HasValue ? _globalInfo.SchoolID.Value : 0;
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["CurrentClassID"] = ClassID;
                SearchInfo["CurrentSchoolID"] = _globalInfo.SchoolID;
                SearchInfo["CurrentAcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["EducationLevelID"] = EducationLevelID;

                // kiem tra id da co hay chua?
                SearchInfo["SkillID"] = SkillID;
                SearchInfo["SkillTitleID"] = SkillTitleID;
                IQueryable<SkillOfClass> IqSkillOfClass = SkillOfClassBusiness.Search(SearchInfo);
                SkillOfClass skillOfClass = null; // su dung de luu tru
                if (IqSkillOfClass.Count() == 0) // neu chua ton tai
                {
                    // them vao table SkillOfClass
                    List<SkillOfClass> LstSkillOfClassInsert = new List<SkillOfClass>();
                    SearchInfo["SkillID"] = null;
                    IQueryable<Skill> IqSkill = SkillBusiness.Search(SearchInfo).OrderBy(o => o.FromDate);
                    foreach (var item in IqSkill)
                    {
                        LstSkillOfClassInsert.Add(new SkillOfClass()
                        {
                            SkillTitleID = SkillTitleID,
                            Description = item.Description,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate,
                            Period = item.Period,
                            CreatedDate = item.CreatedDate
                        });
                    }
                    SkillOfClassBusiness.Insert(LstSkillOfClassInsert); // tunning
                    SkillOfClassBusiness.Save();
                    // lay object SkillOfClass tuong ung
                    Skill obj = SkillBusiness.Find(SkillID);
                    skillOfClass = SkillOfClassBusiness.Search(new Dictionary<string, object>()
                            {
                                {"SkillTitleID", SkillTitleID},
                                {"Description", obj.Description},
                                {"FromDate", obj.FromDate},
                                {"ToDate", obj.ToDate},
                            }).FirstOrDefault();
                }
                else
                {
                    skillOfClass = IqSkillOfClass.FirstOrDefault();
                }
                Dictionary<string, object> dic = new Dictionary<string, object>()
                            {
                                {"SchoolID", _globalInfo.SchoolID},
                                {"ClassID", ClassID},
                                {"AcademicYearID", _globalInfo.AcademicYearID},
                                {"EducationLevelID", EducationLevelID},
                                {"SkillOfClassID", skillOfClass.SkillOfClassID},
                            };
                // lay ve danh sach hoc sinh theo cap, lop, truong, nam
                List<int> lstPupilID = PupilProfileBusiness.SearchBySchool(schoolID, SearchInfo).Select(o => o.PupilProfileID).ToList(); // ds su dung so sanh
                int Count = LSTPupilID.Count;
                // khoi tao bien 
                int pupilID = 0;
                DateTime CurrentDate = DateTime.Now;
                string comment = string.Empty, prize = string.Empty;
                SkillOfPupil skillOfPupil = null;
                try
                {
                    for (int i = 0; i < Count; i++)
                    {
                        pupilID = LSTPupilID[i];
                        if (lstPupilID.Contains(pupilID))
                        {
                            dic["PupilID"] = pupilID;
                            comment = LSTCommentOfTeacher[i].Trim();
                            if (string.IsNullOrEmpty(comment)) continue;
                            int? rank;
                            rank = LSTRank[i];
                            bool? Passed = null;
                            if (LSTIsPass[i].HasValue)
                                Passed = LSTIsPass[i].Value;
                            prize = LSTPrizes[i].Trim();
                            // tien hanh luu
                            skillOfPupil = SkillOfPupilBusiness.SearchBySchool(schoolID, dic).FirstOrDefault();
                            /* neu chua co*/
                            if (skillOfPupil == null)
                            {
                                skillOfPupil = new SkillOfPupil();
                                skillOfPupil.PupilID = pupilID;
                                skillOfPupil.ClassID = ClassID;
                                skillOfPupil.SchoolID = _globalInfo.SchoolID;
                                skillOfPupil.AcademicYearID = _globalInfo.AcademicYearID;
                                skillOfPupil.CreatedDate = CurrentDate;
                                skillOfPupil.CommentOfTeacher = comment;
                                skillOfPupil.IsPass = Passed;
                                skillOfPupil.Rank = rank;
                                skillOfPupil.Prize = prize;
                                skillOfPupil.IsSMS = false;
                                skillOfPupil.SkillOfClassID = skillOfClass.SkillOfClassID;
                                SkillOfPupilBusiness.Insert(skillOfPupil);
                            }
                            else /* neu da ton tai cap nhat */
                            {
                                skillOfPupil.CommentOfTeacher = comment;
                                skillOfPupil.IsPass = Passed;
                                skillOfPupil.Rank = rank;
                                skillOfPupil.Prize = prize;
                                skillOfPupil.IsSMS = false;
                                skillOfPupil.ModifiedDate = CurrentDate;
                                SkillOfPupilBusiness.Update(skillOfPupil);
                            }
                        }
                    } // end for
                    // commit
                    SkillOfPupilBusiness.Save();
                }
                catch (Exception)
                {
                    return Json(new
                    {
                        Type = "Error",
                        Message = Res.Get("WeeklyMenu_Label_ErrorData"),
                    });
                }

                return Json(new
                {
                    Type = "Success",
                    Message = Res.Get("Common_Label_Save"),
                });
            }
            else // ko co quyen thao tac
            {
                return Json(new
                {
                    Type = "Error",
                    Message = Res.Get("Validate_UserAccountID")
                });
            }

        }
        #endregion
    }
}
