﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SkillAppraisementArea.Models
{
    public class DataViewModel
    {
        public List<int> LSTPupilID { get; set; }
        public List<string> LSTCommentOfTeacher { get; set; }
        public List<string> LSTCommentOfParent { get; set; }
        public List<int?> LSTRank { get; set; }
        public List<bool?> LSTIsPass { get; set; }
        public List<string> LSTPrizes { get; set; }
        public int? SkillTitleID { get; set; }
        public int? SkillID { get; set; }
        public int? ClassID { get; set; }
        public int? EducationLevelID { get; set; }

        public DataViewModel()
        {
            LSTPupilID = new List<int>();
            LSTCommentOfTeacher = new List<string>();
            LSTCommentOfParent = new List<string>();
            LSTRank = new List<int?>();
            LSTIsPass = new List<bool?>();
            LSTPrizes = new List<string>();
            ClassID = 0;
        }
    }
}