﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SkillAppraisementArea.Models
{
    public class PupilViewModel
    {
        public int STT { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        public string CommentOfTeacher { get; set; }
        public string CommentOfParent { get; set; }
        public string PupilName { get; set; }
        public bool? IsPass { get; set; }
        public string Rank { get; set; }
        public string Prize { get; set; }
        public int Status { get; set; }

        public byte[] ImageData { get; set; }

        public string byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn != null)
            {
                return "data:image/png;base64," + @System.Convert.ToBase64String(byteArrayIn);
            }
            else
            {
                return "";
            }
        }
    }
}