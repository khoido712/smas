﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SkillAppraisementArea.Models
{
    public class SkillViewModel
    {
        public int STT { get; set; }
        public int SkillID { get; set; }
        public int SkillTitleID { get; set; }
        public string Period { get; set; }
        public string Description { get; set; }
        public System.DateTime? FromDate { get; set; }
        public System.DateTime? ToDate { get; set; }
        public System.DateTime? CreatedDate { get; set; }
        public System.DateTime? ModifiedDate { get; set; }
    }
}