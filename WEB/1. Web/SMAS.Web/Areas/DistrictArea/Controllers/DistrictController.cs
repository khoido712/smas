﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.DistrictArea.Models;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.DistrictArea.Models;
using SMAS.Business.Common;
// Author: AuNH
// Created at: 27/08/2012
// Danh mục tỉnh thành
namespace SMAS.Web.Areas.DistrictArea.Controllers
{
    public class DistrictController : BaseController
    {
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;

        public DistrictController(IDistrictBusiness DistrictBusiness, IProvinceBusiness ProvinceBusiness)
        {
            this.DistrictBusiness = DistrictBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
        }

        //
        // GET: /DistrictArea/District/

        public ActionResult Index()
        {
            SetViewDataPermission("District", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;

            GetProvinceList();

            IEnumerable<DistrictViewModel> lst = this._Search(SearchInfo);
            ViewData[DistrictConstants.LIST_DISTRICT] = lst;
            return View();
        }

        //
        // GET: /DistrictArea/District/Search

        
        [SkipCheckRole]
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["DistrictName"] = frm.DistrictName;
            SearchInfo["DistrictCode"] = frm.DistrictCode;
            SearchInfo["ProvinceID"] = frm.ProvinceID;

            IEnumerable<DistrictViewModel> lst = this._Search(SearchInfo);
            ViewData[DistrictConstants.LIST_DISTRICT] = lst;

            GetProvinceList();

            return PartialView("_List");
        }


        /// <summary>
        /// Thêm mới quận huyện
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("District", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            District district = new District();
            TryUpdateModel(district);
            district.IsActive = true;

            Utils.Utils.TrimObject(district);

            this.DistrictBusiness.Insert(district);
            this.DistrictBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Cập nhật Quận/Huyện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DistrictID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("District", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            District district = this.DistrictBusiness.Find(DistrictID);
            TryUpdateModel(district);

            Utils.Utils.TrimObject(district);
            this.DistrictBusiness.Update(district);
            this.DistrictBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Xóa Quận/Huyện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("District", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.DistrictBusiness.Delete(id);
            this.DistrictBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        

        /// <summary>
        /// Tìm kiếm Quận/Huyện
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DistrictViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("District", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<District> query = this.DistrictBusiness.Search(SearchInfo);
            IQueryable<DistrictViewModel> lst = query.Select(o => new DistrictViewModel { 
                DistrictID = o.DistrictID,
                ProvinceID = o.ProvinceID,
                ProvinceName = o.Province.ProvinceName,
                DistrictCode = o.DistrictCode,
                DistrictName = o.DistrictName,
                ShortName = o.ShortName,
                Description = o.Description
            });

            return lst.OrderBy(o=>o.DistrictName).ToList();
        }

        /// <summary>
        /// Lấy về danh sách các tỉnh thành và gán vào ViewData
        /// </summary>
        private void GetProvinceList()
        {
            IDictionary<string, object> ProvinceSearchInfo = new Dictionary<string, object>();
            ProvinceSearchInfo["IsActive"] = true;

            ViewData[SMAS.Web.Constants.GlobalConstants.LIST_PROVINCE] = this.ProvinceBusiness.Search(ProvinceSearchInfo).OrderBy(o=>o.ProvinceName).ToList();
        }
    }
}
