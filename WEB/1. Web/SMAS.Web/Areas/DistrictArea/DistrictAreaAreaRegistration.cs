﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DistrictArea
{
    public class DistrictAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DistrictArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DistrictArea_default",
                "DistrictArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
