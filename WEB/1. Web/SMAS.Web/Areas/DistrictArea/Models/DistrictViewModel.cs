using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DistrictArea.Models
{
    public class DistrictViewModel
    {
        [ScaffoldColumn(false)]
        public int DistrictID { get; set; }

        [ResourceDisplayName("District_Label_Province")]
        [UIHint("CboProvince")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> ProvinceID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("District_Label_Province")]
        public string ProvinceName { get; set; }

        [ResourceDisplayName("District_Label_Code")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DistrictCode { get; set; }

        [ResourceDisplayName("District_Label_Name")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("District_Label_ShortName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ShortName { get; set; }

        [ResourceDisplayName("District_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
