using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DistrictArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("District_Label_Name")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("District_Label_Code")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DistrictCode { get; set; }

        [ResourceDisplayName("District_Label_Province")]
        [UIHint("CboProvince")]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> ProvinceID { get; set; }
    }
}
