﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.EducationQualityStatisticArea.Controllers
{
    public class EducationQualityStatisticController : BaseController
    {
        #region properties
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IEducationQualityStatisticBusiness EducationQualityStatisticBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        #endregion

        #region Constructor
        public EducationQualityStatisticController(IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IEducationQualityStatisticBusiness EducationQualityStatisticBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.EducationQualityStatisticBusiness = EducationQualityStatisticBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamMarkByClassReport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

       
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(int semester)
        {
            
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["Semester"] = semester;

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_GIAO_DUC;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = EducationQualityStatisticBusiness.GetEducationQualityStatisticReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = EducationQualityStatisticBusiness.CreateEducationQualityStatisticReport(dic);
                processedReport = EducationQualityStatisticBusiness.InsertEducationQualityStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(int semester)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["Semester"] = semester;

            Stream excel = EducationQualityStatisticBusiness.CreateEducationQualityStatisticReport(dic);
            ProcessedReport processedReport = EducationQualityStatisticBusiness.InsertEducationQualityStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            List<Object> listSemester = new List<object>() { new { key = 1, value = Res.Get("Common_Label_Combo_FirstSemester") },
                new { key = 2, value = Res.Get("Common_Label_Combo_SecondSemester") } };

            AcademicYear ay=AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime firstSemesterStartDate = ay.FirstSemesterStartDate.GetValueOrDefault();
            DateTime firstSemesterEndDate = ay.FirstSemesterEndDate.GetValueOrDefault();

            int curSemester = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            if (DateTime.Compare(DateTime.Now, firstSemesterStartDate)>=0 && DateTime.Compare(DateTime.Now, firstSemesterEndDate)<=0)
            {
                curSemester = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            ViewData[EducationQualityStatisticConstants.CBO_SEMESTER] = new SelectList(listSemester, "key", "value",curSemester);

            if (ay != null)
            {
                ViewData[EducationQualityStatisticConstants.ACADEMICYEAR] = ay.DisplayTitle;
            }
            else
            {
                ViewData[EducationQualityStatisticConstants.ACADEMICYEAR] = String.Empty;
            }

        }

        #endregion
    }
}