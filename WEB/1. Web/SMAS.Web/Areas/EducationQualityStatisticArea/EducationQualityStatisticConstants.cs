﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EducationQualityStatisticArea
{
    public class EducationQualityStatisticConstants
    {
        public const string CBO_SEMESTER = "cbo_semester";
        public const string ACADEMICYEAR = "Academic_Year";
    }
}