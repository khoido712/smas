﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EducationQualityStatisticArea
{
    public class EducationQualityStatisticAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EducationQualityStatisticArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EducationQualityStatisticArea_default",
                "EducationQualityStatisticArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
