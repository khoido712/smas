/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SubjectCatArea.Models
{
    public class SubjectCatViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 SubjectCatID { get; set; }

        //Tên môn học
        [ScaffoldColumn(false)]
        public System.String SubjectName { get; set; }

        //Kí hiệu
        [ResourceDisplayName("SubjectCat_Label_Abbreviation")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Abbreviation { get; set; }

        //Môn ngoại ngữ hay không
        [ResourceDisplayName("SubjectCat_Column_IsForeignLanguage")]
        public System.Boolean chkIsForeignLanguage { get; set; }

        //Kiểm tra giữa kì
        [ResourceDisplayName("SubjectCat_Column_MiddleSemesterTest")]
        public System.Boolean chkMiddleSemesterTest { get; set; }

        //Môn kết hợp nhận xét và đánh giá
        [ResourceDisplayName("SubjectCat_Column_IsCommentingMarkAndComment")]
        public System.Boolean chkIsCommentingMarkAndComment { get; set; }

        //Diem thi giua ki gom doc va viet
         [ResourceDisplayName("SubjectCat_Column_ReadAndWriteMiddleTest")]
        public bool ReadAndWriteMiddleTest { get; set; }

        //Cho phep sua kieu mon
        public bool IsEditIsCommentting { get; set; }

        //Môn có thể miễn giảm
        [ResourceDisplayName("SubjectCat_Column_IsExemptible")]
        public System.Boolean chkIsExemptible { get; set; }

        //Có thực hành hay không
        [ResourceDisplayName("SubjectCat_Column_HasPractice")]
        public System.Boolean chkHasPractice { get; set; }

        //Mô tả
        [ResourceDisplayName("SubjectCat_Column_Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Description { get; set; }

        //Loại môn
        [ScaffoldColumn(false)]
        [ResourceDisplayName("SubjectCat_Column_IsCommenting")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int IsCommenting { get; set; }

        //Cấp học áp dụng
        [ScaffoldColumn(false)]
        [ResourceDisplayName("SubjectCat_Column_AppliedLevel")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.Int32> AppliedLevel { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Primary")]
        public bool chkPrimary { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Secondary")]
        public bool chkSecondary { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Tertiary")]
        public bool chkTertiary { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Childcare")]
        public bool chkChildcare { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Children")]
        public bool chkChildren { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Primary")]
        public bool chkPrimaryEdit { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Secondary")]
        public bool chkSecondaryEdit { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Tertiary")]
        public bool chkTertiaryEdit { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Childcare")]
        public bool chkChildcareEdit { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Children")]
        public bool chkChildrenEdit { get; set; }

        //Tên loại môn
        [ScaffoldColumn(false)]
        [ResourceDisplayName("SubjectCat_Column_IsCommentingName")]
        public string IsCommentingName { get; set; }

        //Là môn nghề hay không
       // [ScaffoldColumn(false)]
        [ResourceDisplayName("SubjectCat_Column_IsApprenticeshipSubject")]
        public System.Boolean IsApprenticeshipSubject { get; set; }

        [ScaffoldColumn(false)]
        public System.Boolean IsCoreSubject { get; set; }

        //Tên hiển thị
        [ResourceDisplayName("SubjectCat_Label_SubjectName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String DisplayName { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ScaffoldColumn(false)]
        public System.Boolean IsActive { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        public bool isPrimaryEnable { get; set; }
        public bool isSecondaryEnable { get; set; }
        public bool isTertiaryEnable { get; set; }
        public bool isChildrenEnable { get; set; }
        public bool isChildcareEnable { get; set; }

        [ResourceDisplayName("SubjectCat_Label_ApprenticeshipGroup")]
        public System.Int32? ApprenticeshipGroupID { get; set; }
        [ResourceDisplayName("SubjectCat_Label_OrderInSubject")]
        public int? OrderInSubject { get; set; }
    }
}


