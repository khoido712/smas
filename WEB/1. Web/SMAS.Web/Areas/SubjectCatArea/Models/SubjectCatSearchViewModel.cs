/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SubjectCatArea.Models
{
    public class SubjectCatSearchViewModel
    {
        [ResourceDisplayName("SubjectCat_Label_AppliedLevelInSearch")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey",SubjectCatConstants.LIST_APPLIEDLEVEL)]
        [AdditionalMetadata("Placeholder","All")]
        public int? AppliedLevel { get; set; }

        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SubjectCatConstants.LIST_ISCOMMENTING)]
        [AdditionalMetadata("Placeholder", "All")]
        [ResourceDisplayName("SubjectCat_Label_IsCommenting")]
        public string IsCommenting { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Abbreviation")]
        public string Abbreviation { get; set; }

        [ResourceDisplayName("SubjectCat_Label_DisplayName")]
        public string DisplayName { get; set; }

        
    }
}
