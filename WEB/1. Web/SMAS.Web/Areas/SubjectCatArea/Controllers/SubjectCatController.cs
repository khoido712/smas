﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SubjectCatArea.Models;

using SMAS.VTUtils.HtmlHelpers;
using System.Transactions;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.SubjectCatArea.Controllers
{
    public class SubjectCatController : BaseController
    {
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness;
        private readonly IApprenticeshipGroupBusiness ApprenticeshipGroupBusiness;
        private const int COUNT_PAGE = 1; //Bien phan trang

        public SubjectCatController(ISubjectCatBusiness subjectcatBusiness, IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness,
            IApprenticeshipGroupBusiness apprenticeshipGroupBusiness)
        {
            this.SubjectCatBusiness = subjectcatBusiness;
            this.ApprenticeshipSubjectBusiness = ApprenticeshipSubjectBusiness;
            this.ApprenticeshipGroupBusiness = apprenticeshipGroupBusiness;
        }

        public ActionResult Index()
        {
            //Update 1/2/2013: bỏ checkbox Môn có cả lý thuyết và thực hành (trường chkHasPractice trong Model)
            SetViewData();
            return View();
        }

        private void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;

            IQueryable<SubjectCat> lst = this.SubjectCatBusiness.Search(SearchInfo);
            ViewData[SubjectCatConstants.LIST_SUBJECTCAT] = lst;
            lst = lst.OrderBy(o => o.SubjectName);
            IQueryable<SubjectCatViewModel> list = lst.Select(o => new SubjectCatViewModel
            {
                SubjectCatID = o.SubjectCatID,
                SubjectName = o.SubjectName,
                Abbreviation = o.Abbreviation,
                DisplayName = o.DisplayName,
                chkMiddleSemesterTest = o.MiddleSemesterTest,
                chkIsExemptible = o.IsExemptible,
                IsCommenting = o.IsCommenting,
                //  chkHasPractice = o.HasPractice,
                chkIsForeignLanguage = o.IsForeignLanguage,
                IsCoreSubject = o.IsCoreSubject,
                IsApprenticeshipSubject = o.IsApprenticeshipSubject,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                AppliedLevel = o.AppliedLevel
            });
            List<SubjectCatViewModel> newlist = list.ToList();
            foreach (var item in newlist)
            {

                int isCommenting = item.IsCommenting;
                if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Mark");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Comment");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_MarkAndComment");
                }
            }
            Paginate<SubjectCatViewModel> paging = new Paginate<SubjectCatViewModel>(newlist.AsQueryable());
            paging.page = COUNT_PAGE;
            paging.paginate();
            ViewData[SubjectCatConstants.LIST_SUBJECTCAT] = paging;

            BindAppliedLevelCheckbox();
            List<ComboObject> listAppliedLevel = new List<ComboObject>();
            listAppliedLevel.Add(new ComboObject("1", Res.Get("SubjectCat_AppliedLevel_Level1")));
            listAppliedLevel.Add(new ComboObject("2", Res.Get("SubjectCat_AppliedLevel_Level2")));
            listAppliedLevel.Add(new ComboObject("3", Res.Get("SubjectCat_AppliedLevel_Level3")));
            ViewData[SubjectCatConstants.LIST_APPLIEDLEVEL] = new SelectList(listAppliedLevel, "key", "value");

            List<ComboObject> listIsCommenting = new List<ComboObject>();
            listIsCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK.ToString(), Res.Get("SubjectCat_IsCommenting_Mark")));
            listIsCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT.ToString(), Res.Get("SubjectCat_IsCommenting_Comment")));
            //listIsCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT.ToString(), Res.Get("SubjectCat_IsCommenting_MarkAndComment")));
            ViewData[SubjectCatConstants.LIST_ISCOMMENTING] = new SelectList(listIsCommenting, "key", "value");

            List<ComboObject> listTwoCommenting = new List<ComboObject>();
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK.ToString(), Res.Get("SubjectCat_IsCommenting_Mark")));
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT.ToString(), Res.Get("SubjectCat_IsCommenting_Comment")));
            ViewData[SubjectCatConstants.LIST_TWOCOMMENTING] = new SelectList(listTwoCommenting, "key", "value");

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ApprenticeshipGroup> lstApprenticeshipGroup = ApprenticeshipGroupBusiness.Search(dic).ToList();
            ViewData[SubjectCatConstants.LIST_APPRENTICESHIPGROUPID] = new SelectList(lstApprenticeshipGroup, "ApprenticeshipGroupID", "GroupName");
            ViewData[SubjectCatConstants.ENABLEPADING] = true;
            ViewData[SubjectCatConstants.LIST_SUBJECTCATID] = "";
        }

        public void BindAppliedLevelCheckbox()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            //List<int> ls = GlobalInfo.AppliedLevels;
            List<int> ls = new List<int> { 1, 2, 3, 4, 5 };
            List<ViettelCheckboxList> lscheckbox = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelchk = new ViettelCheckboxList();
            if (ls != null)
            {
                for (int i = 0; i < ls.Count; i++)
                {
                    if (ls[i] == 1)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_AppliedLevelPrimary");
                        viettelchk.cchecked = true;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 2)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_AppliedLevelSecondary");
                        viettelchk.cchecked = true;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 3)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_AppliedLevelTertiary");
                        viettelchk.cchecked = true;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 4)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_AppliedLevelChildren");
                        viettelchk.cchecked = true;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 5)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_AppliedLevelChildcare");
                        viettelchk.cchecked = true;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }

                }

            }
            ViewData[SubjectCatConstants.LIST_APPLIEDLEVELCHECKBOX] = lscheckbox;
        }
            
        #region search
        
        public PartialViewResult Search(SubjectCatSearchViewModel SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            ViewData[SubjectCatConstants.ENABLEPADING] = true;
            if (SearchForm.AppliedLevel.HasValue && SearchForm.AppliedLevel.Value > 0)
            {
                ViewData[SubjectCatConstants.ENABLEPADING] = false;
                List<SubjectCatViewModel> listSubjectCat = this._Search(SearchForm, "");
                ViewData[SubjectCatConstants.LIST_SUBJECTCAT] = listSubjectCat;
                string lstid = "";
                foreach (var item in listSubjectCat)
                {
                    lstid += item.SubjectCatID + ",";
                }
                ViewData[SubjectCatConstants.LIST_SUBJECTCATID] = lstid;
            }
            else
            {
                Paginate<SubjectCatViewModel> paging = this._Search(SearchForm, COUNT_PAGE, "");
                ViewData[SubjectCatConstants.LIST_SUBJECTCAT] = paging;
            }
            return PartialView("_List");

        }
        #endregion

        #region create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        /// 
        public PartialViewResult Create()
        {
            SubjectCatViewModel frm = new SubjectCatViewModel();
            frm.isChildcareEnable = true;
            frm.isChildrenEnable = true;
            frm.isPrimaryEnable = true;
            frm.isSecondaryEnable = true;
            frm.isTertiaryEnable = true;
            GlobalInfo GlobalInfo = new GlobalInfo();
            //List<int> ls = GlobalInfo.AppliedLevels;
            List<int> ls = new List<int> { 1, 2, 3, 4, 5 };
            List<ViettelCheckboxList> lscheckbox = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelchk = new ViettelCheckboxList();
            if (ls != null)
            {
                for (int i = 0; i < ls.Count; i++)
                {
                    if (ls[i] == 1)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_Primary");
                        viettelchk.cchecked = false;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 2)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_Secondary");
                        viettelchk.cchecked = false;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    else if (ls[i] == 3)
                    {
                        viettelchk = new ViettelCheckboxList();
                        viettelchk.Label = Res.Get("SubjectCat_Label_Tertiary");
                        viettelchk.cchecked = false;
                        viettelchk.disabled = false;
                        viettelchk.Value = ls[i];
                        lscheckbox.Add(viettelchk);
                    }
                    //else if (ls[i] == 4)
                    //{
                    //    viettelchk = new ViettelCheckboxList();
                    //    viettelchk.Label = Res.Get("SubjectCat_Label_Children");
                    //    viettelchk.cchecked = false;
                    //    viettelchk.disabled = false;
                    //    viettelchk.Value = ls[i];
                    //    lscheckbox.Add(viettelchk);
                    //}
                    //else if (ls[i] == 5)
                    //{
                    //    viettelchk = new ViettelCheckboxList();
                    //    viettelchk.Label = Res.Get("SubjectCat_Label_Childcare");
                    //    viettelchk.cchecked = false;
                    //    viettelchk.disabled = false;
                    //    viettelchk.Value = ls[i];
                    //    lscheckbox.Add(viettelchk);
                    //}

                }

            }
            List<ComboObject> listTwoCommenting = new List<ComboObject>();
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK.ToString(), Res.Get("SubjectCat_IsCommenting_Mark")));
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT.ToString(), Res.Get("SubjectCat_IsCommenting_Comment")));
            ViewData[SubjectCatConstants.LIST_TWOCOMMENTING] = new SelectList(listTwoCommenting, "key", "value");
            ViewData[SubjectCatConstants.LIST_APPLIEDLEVELCHECKBOX] = lscheckbox;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ApprenticeshipGroup> lstApprenticeshipGroup = ApprenticeshipGroupBusiness.Search(dic).ToList();
            ViewData[SubjectCatConstants.LIST_APPRENTICESHIPGROUPID] = new SelectList(lstApprenticeshipGroup, "ApprenticeshipGroupID", "GroupName");
            return PartialView("_Create", frm);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SubjectCatViewModel frm)
        {
            if (!frm.chkPrimary && !frm.chkSecondary && !frm.chkTertiary)
            {
                throw new SMAS.Business.Common.BusinessException("EducationGrade_Require");
            }
            string subjectNameSecondary = ""; //Tên môn học cấp 2 - Chỉ dùng đến khi checkbox môn nghề được chọn
            string subjectNameTertiary = ""; //Tên môn học cấp 3 - Chỉ dùng đến khi checkbox môn nghề được chọn
            bool checkApprenshipSubjectSecondary = false; //Biến kiểm tra môn nghề cấp 2
            bool checkApprenshipSubjectTertiary = false;//Biến kiểm tra môn nghề cấp 3
            //bool success = false;
            int count = 0; //Bien dem so luong mon nghe dc them vao, chi co the co gia tri la 0, 1,  2 
            //+(0 : ko tick checkbox mon nghe - 1: tick checkbox mon nghe va chon cap 2 HOAC cap 3 -  2: tick checkbox mon nghe va chon ca cap 2 VA cap 3
            //SubjectCat subjectcat = new SubjectCat();
            if (ModelState.IsValid)
            {

                List<SubjectCat> ls = new List<SubjectCat>();
                if (frm.chkPrimary)
                {
                    SubjectCat subjectcat = new SubjectCat();
                    subjectcat.Abbreviation = frm.Abbreviation;
                    subjectcat.Description = frm.Description;
                    subjectcat.IsExemptible = frm.chkIsExemptible;
                    //Nếu tick chọn miễn giảm thì mới xét giá trị của môn có thực hành và lý thuyết hay ko
                    if (frm.chkIsExemptible)
                    {
                        // subjectcat.HasPractice = frm.chkHasPractice;
                    }
                    //Nếu ko tick môn miễn giảm thì ko có môn thực hành và lý thuyết
                    else
                    {
                        subjectcat.HasPractice = false;
                    }
                    subjectcat.SubjectName = frm.DisplayName;
                    subjectcat.DisplayName = frm.DisplayName;
                    subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
                    subjectcat.CreatedDate = DateTime.Now;
                    if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
                    }
                    else if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    subjectcat.AppliedLevel = GlobalConstants.SUBJECTCAT_PRIMARY;
                    subjectcat.IsEditIsCommentting = frm.IsEditIsCommentting;
                    subjectcat.MiddleSemesterTest = frm.chkMiddleSemesterTest;
                    subjectcat.ReadAndWriteMiddleTest = frm.ReadAndWriteMiddleTest;
                    subjectcat.OrderInSubject = SubjectCatBusiness.GetMaxOrderIsSubjectByGrade(subjectcat.AppliedLevel.Value);
                    //Cap 1 thi ko co mon nghe
                    subjectcat.IsApprenticeshipSubject = false;
                    //fixed bug 0172544 - DungVA
                    subjectcat.IsCoreSubject = frm.IsCoreSubject;
                    subjectcat.IsActive = true;

                    ls.Add(subjectcat);
                    //this.SubjectCatBusiness.Insert(subjectcat);
                    //this.SubjectCatBusiness.Save();
                    //success = true;
                }
                if (frm.chkSecondary)
                {
                    SubjectCat subjectcat = new SubjectCat();
                    subjectcat.Abbreviation = frm.Abbreviation;
                    subjectcat.Description = frm.Description;
                    subjectcat.IsExemptible = frm.chkIsExemptible;
                    if (frm.chkIsExemptible)
                    {
                        //subjectcat.HasPractice = frm.chkHasPractice;
                    }
                    else
                    {
                        subjectcat.HasPractice = false;
                    }
                    subjectcat.SubjectName = frm.DisplayName;
                    subjectcat.DisplayName = frm.DisplayName;
                    subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
                    subjectcat.CreatedDate = DateTime.Now;
                    if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
                    }
                    else if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    subjectcat.IsEditIsCommentting = frm.IsEditIsCommentting;
                    //added by namdv3 13-03-2013
                    subjectcat.AppliedLevel = GlobalConstants.SUBJECTCAT_SECONDARY;
                    subjectcat.ApprenticeshipGroupID = frm.ApprenticeshipGroupID;
                    subjectcat.OrderInSubject = SubjectCatBusiness.GetMaxOrderIsSubjectByGrade(subjectcat.AppliedLevel.Value);
                    //Cap 2 ko co kiem tra giua ki va ko co diem thi giua ki gom doc va viet
                    subjectcat.ReadAndWriteMiddleTest = false;
                    subjectcat.MiddleSemesterTest = false;

                    subjectcat.IsApprenticeshipSubject = frm.IsApprenticeshipSubject;
                    if (frm.IsApprenticeshipSubject)
                    {
                        checkApprenshipSubjectSecondary = true;
                        count++;
                        subjectNameSecondary = frm.SubjectName;
                    }
                    //fixed bug 0172544 - DungVA
                    subjectcat.IsCoreSubject = frm.IsCoreSubject;
                    subjectcat.IsActive = true;

                    ls.Add(subjectcat);
                    //this.SubjectCatBusiness.Insert(subjectcat);
                    //this.SubjectCatBusiness.Save();
                    //success = true;
                }
                if (frm.chkTertiary)
                {
                    SubjectCat subjectcat = new SubjectCat();
                    subjectcat.Abbreviation = frm.Abbreviation;
                    subjectcat.Description = frm.Description;
                    subjectcat.IsExemptible = frm.chkIsExemptible;
                    if (frm.chkIsExemptible)
                    {
                        // subjectcat.HasPractice = frm.chkHasPractice;
                    }
                    else
                    {
                        subjectcat.HasPractice = false;
                    }
                    subjectcat.SubjectName = frm.DisplayName;
                    subjectcat.DisplayName = frm.DisplayName;
                    subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
                    subjectcat.CreatedDate = DateTime.Now;
                    if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
                    }
                    else if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
                    }
                    else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                    {
                        subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    }
                    subjectcat.IsEditIsCommentting = frm.IsEditIsCommentting;
                    //Cap 3 ko co kiem tra giua ki va ko co diem thi giua ki bao gom doc va viet
                    subjectcat.MiddleSemesterTest = false;
                    subjectcat.ReadAndWriteMiddleTest = false;
                    subjectcat.AppliedLevel = GlobalConstants.SUBJECTCAT_TERTIARY;
                    subjectcat.IsApprenticeshipSubject = frm.IsApprenticeshipSubject;
                    //added by namdv3 13-03-2013
                    subjectcat.ApprenticeshipGroupID = frm.ApprenticeshipGroupID;
                    subjectcat.OrderInSubject = SubjectCatBusiness.GetMaxOrderIsSubjectByGrade(subjectcat.AppliedLevel.Value);

                    if (frm.IsApprenticeshipSubject)
                    {
                        checkApprenshipSubjectTertiary = true;
                        count++;
                        subjectNameTertiary = frm.DisplayName;
                    }
                    //fixed bug 0172544 - DungVA
                    subjectcat.IsCoreSubject = frm.IsCoreSubject;
                    subjectcat.IsActive = true;

                    ls.Add(subjectcat);
                    //this.SubjectCatBusiness.Insert(subjectcat);
                    //this.SubjectCatBusiness.Save();
                    //success = true;
                }
                #region Mau giao va Nha Tre
                //if (frm.chkChildcare)
                //{
                //    SubjectCat subjectcat = new SubjectCat();
                //    subjectcat.Abbreviation = frm.Abbreviation;
                //    subjectcat.Description = frm.Description;
                //    subjectcat.HasPractice = frm.chkHasPractice;
                //    subjectcat.SubjectName = frm.SubjectName;
                //    subjectcat.DisplayName = frm.SubjectName;
                //    subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
                //    subjectcat.MiddleSemesterTest = frm.chkMiddleSemesterTest;
                //    subjectcat.IsExemptible = frm.chkIsExemptible;
                //    subjectcat.CreatedDate = DateTime.Now;
                //    if (frm.chkIsCommentingMarkAndComment) subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
                //    subjectcat.IsActive = true;
                //    subjectcat.IsApprenticeshipSubject = frm.IsApprenticeshipSubject;
                //    subjectcat.AppliedLevel = GlobalConstants.SUBJECTCAT_CHILDCARE;
                //    ls.Add(subjectcat);
                //    //this.SubjectCatBusiness.Insert(subjectcat);
                //    //this.SubjectCatBusiness.Save();
                //    //success = true;
                //    subjectName = subjectcat.SubjectName;
                //}
                //if (frm.chkChildren)
                //{
                //    SubjectCat subjectcat = new SubjectCat();
                //    subjectcat.Abbreviation = frm.Abbreviation;
                //    subjectcat.Description = frm.Description;
                //    subjectcat.HasPractice = frm.chkHasPractice;
                //    subjectcat.SubjectName = frm.SubjectName;
                //    subjectcat.DisplayName = frm.SubjectName;
                //    subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
                //    subjectcat.MiddleSemesterTest = frm.chkMiddleSemesterTest;
                //    subjectcat.IsExemptible = frm.chkIsExemptible;
                //    subjectcat.CreatedDate = DateTime.Now;
                //    if (frm.chkIsCommentingMarkAndComment) subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
                //    subjectcat.IsActive = true;
                //    subjectcat.IsApprenticeshipSubject = frm.IsApprenticeshipSubject;
                //    subjectcat.AppliedLevel = GlobalConstants.SUBJECTCAT_CHILDREN;
                //    ls.Add(subjectcat);
                //    //this.SubjectCatBusiness.Insert(subjectcat);
                //    //this.SubjectCatBusiness.Save();
                //    //success = true;
                //    subjectName = subjectcat.SubjectName;
                //}
                #endregion

                //Nếu tick chọn môn nghề và tick cả cấp 2 và cấp 3 thì chỉ insert môn nghề cấp 3 => cho môn nghề cấp 2 là false
                //if (count == 2)
                //{
                //    for (int i = 0; i < ls.Count; i++)
                //    {
                //        if (ls[i].AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                //        {
                //            ls[i].IsApprenticeshipSubject = false;
                //        }
                //    }
                //}
                //Danh sach mon hoc ls them vao chi co the bao gom 1 phan tu, 2 phan tu hoac 3 phan tu
                //+ các phần tử trong danh sách theo thứ tự từ cấp thấp đến cấp cao hơn
                this.SubjectCatBusiness.Insert(ls);
                this.SubjectCatBusiness.Save();

                //Nếu checkbox môn nghề được tick
                //Do không cho insert 2 môn nghề trùng tên khác cấp nên nếu người dùng tick chọn môn nghề và chọn cả cấp 2 và cấp 3 thì chỉ insert vào bảng môn nghề theo SubjectID của môn cấp 3
                if (count > 0)
                {
                    List<ApprenticeshipSubject> lsApprenticeshipSubject = new List<ApprenticeshipSubject>();
                    List<SubjectCat> lsSubjectCat = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY).OrderByDescending(o => o.SubjectCatID).ToList();
                    //+Neu nguoi dung tick chon ca cap 2 va cap 3 va tick checkbox mon nghe thi phai them ca 2 mon do vao bang mon nghe, tuy nhiên
                    //do ko cho insert 2 môn nghề trùng tên khác cấp nên ta chỉ insert 1 môn nghề vào 

                    if (count == 2)
                    {
                        //Lay ra list mon hoc theo thu tu SubjectCatID tu cao xuong thap thi se lay duoc ID cua 2 mon hoc vua them vao
                        int subjectCatLargestID = lsSubjectCat[0].SubjectCatID; //ID của môn học cấp 3 vừa thêm vào
                        int subjectCatBelowLargestID = lsSubjectCat[1].SubjectCatID; //ID của môn học cấp 2 vừa thêm vào
                        subjectNameSecondary = lsSubjectCat[1].DisplayName;
                        subjectNameTertiary = lsSubjectCat[0].DisplayName;
                        ApprenticeshipSubject ApprenticeshipSubject = new ApprenticeshipSubject();
                        //Thêm mới môn nghề cấp 2
                        ApprenticeshipSubject.SubjectID = subjectCatBelowLargestID;
                        ApprenticeshipSubject.SubjectName = subjectNameSecondary;
                        ApprenticeshipSubject.IsActive = true;
                        lsApprenticeshipSubject.Add(ApprenticeshipSubject);
                        ApprenticeshipSubject = new ApprenticeshipSubject();
                        //Thêm mới môn nghề cấp 3
                        ApprenticeshipSubject.SubjectID = subjectCatLargestID;
                        ApprenticeshipSubject.SubjectName = subjectNameTertiary;
                        ApprenticeshipSubject.IsActive = true;
                        lsApprenticeshipSubject.Add(ApprenticeshipSubject);
                    }
                    //Nếu người dùng tick chọn cấp 2 HOẶC cấp 3  và tick checkbox môn nghề
                    else if (count == 1)
                    {
                        int subjectCatLargestID = lsSubjectCat[0].SubjectCatID; //ID lớn nhất của bảng SubjectCat chính là ID của môn vừa thêm vào
                        ApprenticeshipSubject ApprenticeshipSubject = new ApprenticeshipSubject();
                        ApprenticeshipSubject.SubjectID = subjectCatLargestID;
                        if (checkApprenshipSubjectSecondary)
                        {
                            ApprenticeshipSubject.SubjectName = lsSubjectCat[0].DisplayName;
                        }
                        else if (checkApprenshipSubjectTertiary)
                        {
                            ApprenticeshipSubject.SubjectName = lsSubjectCat[0].DisplayName;
                        }
                        ApprenticeshipSubject.IsActive = true;
                        lsApprenticeshipSubject.Add(ApprenticeshipSubject);
                    }
                    for (int i = 0; i < lsApprenticeshipSubject.Count; i++)
                    {
                        ApprenticeshipSubjectBusiness.Insert(lsApprenticeshipSubject[i]);
                        ApprenticeshipSubjectBusiness.Save();
                    }
                }
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                //}
            }
            //}

            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList));

        }
        #endregion

        #region edit
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public PartialViewResult Edit(int id)
        {
            SubjectCat sc = this.SubjectCatBusiness.Find(id);
            SubjectCatViewModel frm = new SubjectCatViewModel();
            frm.SubjectCatID = sc.SubjectCatID;
            frm.SubjectName = sc.SubjectName;
            frm.DisplayName = sc.DisplayName;
            frm.Abbreviation = sc.Abbreviation;
            frm.AppliedLevel = sc.AppliedLevel;
            frm.isChildcareEnable = false;
            frm.isChildrenEnable = false;
            frm.isPrimaryEnable = false;
            frm.isSecondaryEnable = false;
            frm.isTertiaryEnable = false;
            if (frm.AppliedLevel != null)
            {
                if ((frm.AppliedLevel == GlobalConstants.SUBJECTCAT_CHILDREN))
                {
                    frm.chkChildren = true;
                    ViewData[SubjectCatConstants.CHECKCBB] = "5";
                }
                else if ((frm.AppliedLevel == GlobalConstants.SUBJECTCAT_CHILDCARE))
                {
                    frm.chkChildcare = true;
                    ViewData[SubjectCatConstants.CHECKCBB] = "4";
                }
                else if ((frm.AppliedLevel == GlobalConstants.SUBJECTCAT_TERTIARY))
                {
                    frm.chkTertiary = true;
                    ViewData[SubjectCatConstants.CHECKCBB] = "3";
                }
                else if ((frm.AppliedLevel == GlobalConstants.SUBJECTCAT_SECONDARY))
                {
                    frm.chkSecondary = true;
                    ViewData[SubjectCatConstants.CHECKCBB] = "2";
                }
                else if ((frm.AppliedLevel == GlobalConstants.SUBJECTCAT_PRIMARY))
                {
                    frm.chkPrimary = true;
                    ViewData[SubjectCatConstants.CHECKCBB] = "1";
                }
            }
            //else
            //{
            //    frm.chkChildren = true;
            //    frm.chkChildcare = true;
            //    frm.chkPrimary = true;
            //    frm.chkSecondary = true;
            //    frm.chkTertiary = true;
            //}
            //added by namdv3 13-03-2013
            frm.ApprenticeshipGroupID = sc.ApprenticeshipGroupID;
            frm.OrderInSubject = (int?)sc.OrderInSubject;
            frm.IsCoreSubject = sc.IsCoreSubject;
            frm.chkIsExemptible = sc.IsExemptible;
            frm.chkIsForeignLanguage = sc.IsForeignLanguage;
            frm.chkMiddleSemesterTest = sc.MiddleSemesterTest;
            //frm.chkHasPractice = sc.HasPractice;
            frm.Description = sc.Description;
            //Nếu là môn vừa nhận xét vừa đánh giá thì tick checkbox nhận xét  đánh giá và cho combobox kiểu môn là chấm điểm
            if (sc.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT)
            {
                frm.chkIsCommentingMarkAndComment = true;
                frm.IsCommenting = GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
            }
            else
            {
                frm.IsCommenting = sc.IsCommenting;
                frm.chkIsCommentingMarkAndComment = false;
            }
            frm.IsApprenticeshipSubject = sc.IsApprenticeshipSubject;
            frm.IsEditIsCommentting = sc.IsEditIsCommentting;
            frm.ReadAndWriteMiddleTest = sc.ReadAndWriteMiddleTest;
            List<ComboObject> listTwoCommenting = new List<ComboObject>();
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK.ToString(), Res.Get("SubjectCat_IsCommenting_Mark")));
            listTwoCommenting.Add(new ComboObject(SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT.ToString(), Res.Get("SubjectCat_IsCommenting_Comment")));
            ViewData[SubjectCatConstants.LIST_TWOCOMMENTING] = new SelectList(listTwoCommenting, "key", "value", frm.IsCommenting);
            //namdv3
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<ApprenticeshipGroup> lstApprenticeshipGroup = ApprenticeshipGroupBusiness.Search(dic).ToList();
            ViewData[SubjectCatConstants.LIST_APPRENTICESHIPGROUPID] = new SelectList(lstApprenticeshipGroup, "ApprenticeshipGroupID", "GroupName");
            return PartialView("_Edit", frm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(SubjectCatViewModel frm)
        {
            //if (!frm.chkPrimary && !frm.chkSecondary && !frm.chkTertiary)
            //{
            //    throw new SMAS.Business.Common.BusinessException("EducationGrade_Require");
            //}
            SubjectCat subjectcat = this.SubjectCatBusiness.Find(frm.SubjectCatID);
            subjectcat.Description = frm.Description;
            subjectcat.Abbreviation = frm.Abbreviation;
            subjectcat.SubjectName = frm.DisplayName;
            subjectcat.DisplayName = frm.DisplayName;
            subjectcat.IsExemptible = frm.chkIsExemptible;
            if (frm.chkIsExemptible)
            {
                // subjectcat.HasPractice = frm.chkHasPractice;
            }
            else
            {
                subjectcat.HasPractice = false;
            }
            subjectcat.IsForeignLanguage = frm.chkIsForeignLanguage;
            subjectcat.ModifiedDate = DateTime.Now;
            frm.AppliedLevel = subjectcat.AppliedLevel;
            subjectcat.IsEditIsCommentting = frm.IsEditIsCommentting;
            //added by namdv3 13-03-2013
            if (frm.IsApprenticeshipSubject)
            {
                subjectcat.ApprenticeshipGroupID = frm.ApprenticeshipGroupID;
                //subjectcat.IsApprenticeshipSubject = true;
            }
            subjectcat.IsCoreSubject = frm.IsCoreSubject;
            //KIEM TRA GIUA KY VA DOC VIET
            if (subjectcat.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                subjectcat.MiddleSemesterTest = frm.chkMiddleSemesterTest;
                subjectcat.ReadAndWriteMiddleTest = frm.ReadAndWriteMiddleTest;
            }
            else
            {
                subjectcat.MiddleSemesterTest = false;
                subjectcat.ReadAndWriteMiddleTest = false;
            }
            //KIEM TRA LOAI MON
            if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
            {
                subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT;
            }
            else if (frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
            {
                subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
            }
            else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
            {
                subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
            }
            else if (!frm.chkIsCommentingMarkAndComment && frm.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
            {
                subjectcat.IsCommenting = (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
            }
            //KIEM TRA MON NGHE
            if (subjectcat.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || subjectcat.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                //Nếu ban đầu ko phải là môn nghề và sau đó đc tick chọn môn nghề thì thêm mới môn đó vào bảng môn nghề
                if (!subjectcat.IsApprenticeshipSubject && frm.IsApprenticeshipSubject)
                {
                    ApprenticeshipSubject ApprenticeshipSubject = new ApprenticeshipSubject();
                    ApprenticeshipSubject.SubjectID = subjectcat.SubjectCatID;
                    ApprenticeshipSubject.SubjectName = subjectcat.SubjectName;
                    ApprenticeshipSubject.IsActive = true;
                    ApprenticeshipSubjectBusiness.Insert(ApprenticeshipSubject);
                    ApprenticeshipSubjectBusiness.Save();
                }
                //Ban đầu là môn nghề và sau đó bỏ chọn tick môn nghề thì phải xóa môn đó ra khỏi bảng môn nghề
                else if (subjectcat.IsApprenticeshipSubject && !frm.IsApprenticeshipSubject)
                {
                    int subjectID = subjectcat.SubjectCatID;
                    string name = subjectcat.SubjectName;
                    ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipSubjectBusiness.All.Where(o => o.SubjectID == subjectID && o.SubjectName == name && o.IsActive).FirstOrDefault();
                    if (ApprenticeshipSubject != null)
                    {
                        ApprenticeshipSubjectBusiness.Delete(ApprenticeshipSubject.ApprenticeshipSubjectID);
                        ApprenticeshipSubjectBusiness.Save();
                    }
                }

                subjectcat.IsApprenticeshipSubject = frm.IsApprenticeshipSubject;
            }
            else if (subjectcat.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                subjectcat.IsApprenticeshipSubject = false;
            }
            using (TransactionScope scopeEdit = new TransactionScope())
            {
                this.SubjectCatBusiness.Update(subjectcat);
                this.SubjectCatBusiness.Save();
                scopeEdit.Complete();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }

        #endregion

        #region delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            SubjectCat subjectcat = this.SubjectCatBusiness.Find(id);
            //int subjectID = subjectcat.SubjectCatID;
            //string name = subjectcat.SubjectName;
            //ApprenticeshipSubject ApprenticeshipSubject = ApprenticeshipSubjectBusiness.All.Where(o => o.SubjectID == subjectID && o.SubjectName == name && o.IsActive == true).FirstOrDefault();
            //if (ApprenticeshipSubject != null)
            //{
            //    ApprenticeshipSubjectBusiness.Delete(ApprenticeshipSubject.ApprenticeshipSubjectID);
            //    ApprenticeshipSubjectBusiness.Save();
            //}
            //this.SubjectCatBusiness.Delete(id);
            subjectcat.IsApprenticeshipSubject = false;
            subjectcat.IsActive = false;
            //this.SubjectCatBusiness.Update(subjectcat);
            this.SubjectCatBusiness.Delete(id);
            this.SubjectCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        private List<SubjectCatViewModel> _Search(SubjectCatSearchViewModel SearchForm, string orderBy = "")
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = SearchForm.AppliedLevel;
            SearchInfo["DisplayName"] = SearchForm.DisplayName;
            SearchInfo["SubjectName"] = SearchForm.DisplayName;
            SearchInfo["Abbreviation"] = SearchForm.Abbreviation;
            SearchInfo["IsCommenting"] = SearchForm.IsCommenting;
            SearchInfo["IsActive"] = true;
            // Thuc hien truy van du lieu
            IQueryable<SubjectCat> lst = this.SubjectCatBusiness.Search(SearchInfo);

            // Sap xep
            string sort = "";
            string order = "";
            if (orderBy != "")
            {
                sort = orderBy.Split('-')[0];
                order = orderBy.Split('-')[1];
            }

            if (sort == "SubjectName" || sort == "")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SubjectName);
                }
                else
                {
                    lst = lst.OrderBy(o => o.SubjectName);
                }
            }

            else if (sort == "Abbreviation")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.Abbreviation);
                }
                else
                {
                    lst = lst.OrderBy(o => o.Abbreviation);
                }
            }

            else if (sort == "AppliedLevel")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.AppliedLevel);
                }
                else
                {
                    lst = lst.OrderBy(o => o.AppliedLevel);
                }
            }

            IQueryable<SubjectCatViewModel> res = lst.Select(o => new SubjectCatViewModel
            {
                SubjectCatID = o.SubjectCatID,
                SubjectName = o.SubjectName,
                Abbreviation = o.Abbreviation,
                DisplayName = o.DisplayName,
                chkMiddleSemesterTest = o.MiddleSemesterTest,
                chkIsExemptible = o.IsExemptible,
                IsCommenting = o.IsCommenting,
                //  chkHasPractice = o.HasPractice,
                chkIsForeignLanguage = o.IsForeignLanguage,
                IsCoreSubject = o.IsCoreSubject,
                OrderInSubject = o.OrderInSubject,
                IsApprenticeshipSubject = o.IsApprenticeshipSubject,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                AppliedLevel = o.AppliedLevel,
                chkIsCommentingMarkAndComment = o.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT ? true : false,
                ReadAndWriteMiddleTest = o.ReadAndWriteMiddleTest

            });
            List<SubjectCatViewModel> list = res.ToList();
            foreach (var item in list)
            {

                int isCommenting = item.IsCommenting;
                if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Mark");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Comment");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_MarkAndComment");
                }
            }
            return list.OrderBy(o=>o.DisplayName).ToList();
        }

        #region paginate
        private Paginate<SubjectCatViewModel> _Search(SubjectCatSearchViewModel SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = SearchForm.AppliedLevel;
            SearchInfo["SubjectName"] = SearchForm.DisplayName;
            SearchInfo["Abbreviation"] = SearchForm.Abbreviation;
            SearchInfo["IsCommenting"] = SearchForm.IsCommenting;
            SearchInfo["IsActive"] = true;
            // Thuc hien truy van du lieu
            IQueryable<SubjectCat> lst = this.SubjectCatBusiness.Search(SearchInfo);

            // Sap xep
            string sort = "";
            string order = "";
            if (orderBy != "")
            {
                sort = orderBy.Split('-')[0];
                order = orderBy.Split('-')[1];
            }

            if (sort == "SubjectName" || sort == "")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SubjectName);
                }
                else
                {
                    lst = lst.OrderBy(o => o.SubjectName);
                }
            }

            else if (sort == "Abbreviation")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.Abbreviation);
                }
                else
                {
                    lst = lst.OrderBy(o => o.Abbreviation);
                }
            }

            else if (sort == "AppliedLevel")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.AppliedLevel);
                }
                else
                {
                    lst = lst.OrderBy(o => o.AppliedLevel);
                }
            }

            IQueryable<SubjectCatViewModel> res = lst.Select(o => new SubjectCatViewModel
            {
                SubjectCatID = o.SubjectCatID,
                SubjectName = o.SubjectName,
                Abbreviation = o.Abbreviation,
                DisplayName = o.DisplayName,
                chkMiddleSemesterTest = o.MiddleSemesterTest,
                chkIsExemptible = o.IsExemptible,
                IsCommenting = o.IsCommenting,
                //  chkHasPractice = o.HasPractice,
                chkIsForeignLanguage = o.IsForeignLanguage,
                IsCoreSubject = o.IsCoreSubject,
                OrderInSubject = o.OrderInSubject,
                IsApprenticeshipSubject = o.IsApprenticeshipSubject,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                AppliedLevel = o.AppliedLevel,
                chkIsCommentingMarkAndComment = o.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT ? true : false,
                ReadAndWriteMiddleTest = o.ReadAndWriteMiddleTest

            });
            List<SubjectCatViewModel> list = res.ToList();
            foreach (var item in list)
            {

                int isCommenting = item.IsCommenting;
                if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Mark");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Comment");
                }
                else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT)
                {
                    item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_MarkAndComment");
                }
            }
            res = list.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).AsQueryable();
            // Thuc hien phan trang tung phan
            Paginate<SubjectCatViewModel> Paging = new Paginate<SubjectCatViewModel>(res);
            Paging.page = page;
            Paging.paginate();

            return Paging;
        }

        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(SubjectCatSearchViewModel SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            Paginate<SubjectCatViewModel> paging = this._Search(SearchForm, page, orderBy);
            GridModel<SubjectCatViewModel> gm = new GridModel<SubjectCatViewModel>(paging.List);
            gm.Total = paging.total;
            return View(gm);
        }
        #endregion

        //public PartialViewResult Search(SubjectCatSearchViewModel SearchForm)
        //{
        //    Utils.Utils.TrimObject(SearchForm);

        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    SearchInfo["AppliedLevel"] = SearchForm.AppliedLevel;
        //    SearchInfo["SubjectName"] = SearchForm.SubjectName;
        //    SearchInfo["Abbreviation"] = SearchForm.Abbreviation;
        //    SearchInfo["IsCommenting"] = SearchForm.IsCommenting;
        //    SearchInfo["IsActive"] = true;

        //    IEnumerable<SubjectCatViewModel> lst = this._Search(SearchInfo);
        //    ViewData[SubjectCatConstants.LIST_SUBJECTCAT] = lst;

        //    //Get view data here

        //    return PartialView("_List");
        //}

        //
        // GET: /SubjectCat/Search
        //private IEnumerable<SubjectCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        //{
        //    IQueryable<SubjectCat> query = this.SubjectCatBusiness.Search(SearchInfo);
        //    IQueryable<SubjectCatViewModel> lst = query.Select(o => new SubjectCatViewModel
        //    {
        //        SubjectCatID = o.SubjectCatID,
        //        SubjectName = o.SubjectName,
        //        Abbreviation = o.Abbreviation,
        //        DisplayName = o.DisplayName,
        //        chkMiddleSemesterTest = o.MiddleSemesterTest,
        //        chkIsExemptible = o.IsExemptible,
        //        IsCommenting = o.IsCommenting,
        //        chkHasPractice = o.HasPractice,
        //        chkIsForeignLanguage = o.IsForeignLanguage,
        //        IsCoreSubject = o.IsCoreSubject,
        //        IsApprenticeshipSubject = o.IsApprenticeshipSubject,
        //        Description = o.Description,
        //        CreatedDate = o.CreatedDate,
        //        IsActive = o.IsActive,
        //        ModifiedDate = o.ModifiedDate,
        //        AppliedLevel = o.AppliedLevel


        //    });
        //    List<SubjectCatViewModel> list = lst.ToList();
        //    foreach (var item in list)
        //    {

        //        int isCommenting = item.IsCommenting;
        //        if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
        //        {
        //            item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Mark");
        //        }
        //        else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
        //        {
        //            item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_Comment");
        //        }
        //        else if (isCommenting == (int)SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK_AND_COMMENT)
        //        {
        //            item.IsCommentingName = Res.Get("SubjectCat_IsCommenting_MarkAndComment");
        //        }
        //    }
        //    return list;
        //}
        //#endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateOrderInSubject()
        {
            var listID = Request["listvalue"];
            var listValue = listID.Split(',');
            if (listValue.Count() > 0)
            {
                Dictionary<int, int?> dicData = new Dictionary<int, int?>();
                foreach (var item in listValue)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var value = item.Split('_');
                        int subjectCatID = int.Parse(value[0]);
                        int? orderInSubject = (int?)null;
                        if (!string.IsNullOrEmpty(value[1]))
                        {
                            try
                            {
                                orderInSubject = int.Parse(value[1]);
                            }
                            catch
                            {
                                return Json(new JsonMessage(string.Format("{0}.{1}", Res.Get("SubjectCat_Validation_OrderInSubject_IsNumber"), value[0]), "Error"));
                            }
                        }
                        SubjectCat sc = SubjectCatBusiness.Find(subjectCatID);
                        if (sc != null && sc.OrderInSubject != orderInSubject)
                        {
                            dicData.Add(subjectCatID, orderInSubject);
                        }
                    }
                }
                SubjectCatBusiness.UpdateOrderInSubject(dicData);
                SubjectCatBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
    }
}





