/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
namespace SMAS.Web.Areas.SubjectCatArea
{
    public class SubjectCatConstants
    {
        public const string LIST_SUBJECTCAT = "listSubjectCat";
        public const string LIST_ISCOMMENTING = "listIsCommenting";
        public const string LIST_APPLIEDLEVEL = "listAppliedLevel";
        public const string LIST_APPLIEDLEVELCHECKBOX = "listAppliedLevel";
        public const string CHECKCBB = "checkCBB";
        public const string LIST_TWOCOMMENTING = "LIST_ISCOMMENTING";
        public const string LIST_APPRENTICESHIPGROUPID = "LIST_APPRENTICESHIPGROUPID";
        public const string ENABLEPADING = "EnablePading";
        public const string LIST_SUBJECTCATID = "LIST_SUBJECTCATID";
        
    }
}