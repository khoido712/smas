﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SubjectCatArea
{
    public class SubjectCatAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SubjectCatArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SubjectCatArea_default",
                "SubjectCatArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
