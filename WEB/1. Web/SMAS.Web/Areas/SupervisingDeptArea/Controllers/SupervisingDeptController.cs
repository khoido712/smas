﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.SupervisingDeptArea.Models;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using System.Text.RegularExpressions;

/**
 * 
 * Nguoi thuc hien: Aunh
 * Ngay tao: 02/08/2012
 * Controller thuc hien cac xu ly trong chu nang quan ly don vi
 * 
 **/
namespace SMAS.Web.Areas.SupervisingDeptArea.Controllers
{
    public class SupervisingDeptController : BaseController
    {
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private GlobalInfo GlobalInfo = new GlobalInfo();
        public const int MinLengthPassword = 8;
        public const int MaxLengthPassword = 128;

        /**
         * 
         * Ham khoi tao controller. Khi can su dung Business nao thi phai khai bao
         * Business do trong danh sach tham so cua ham khoi tao
         * 
         **/
        public SupervisingDeptController(ISupervisingDeptBusiness supervisingdeptBusiness,
                                         IUserAccountBusiness UserAccountBusiness,
                                         IProvinceBusiness ProvinceBusiness,
                                         IDistrictBusiness DistrictBusiness, ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.SupervisingDeptBusiness = supervisingdeptBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }


        /**
         * Hien thi trang tim kiem 
         **/
        public ViewResult Index()
        {
            ViewData[SupervisingDeptConstants.ISSP] = false;
            SupervisingDeptTreeViewModel DeptTree = CommonFunctions.TreeSupervisingDeptManagedByUser(SupervisingDeptBusiness);
            List<SupervisingDeptTreeViewModel> ListDeptNode = new List<SupervisingDeptTreeViewModel>();
            if (DeptTree != null)
            {
                ListDeptNode.Add(DeptTree);
            }
            ViewData[SupervisingDeptConstants.LT_SPV_DEPT_TREE] = ListDeptNode;
            Paginate<SupervisingDeptViewModel> paging = new Paginate<SupervisingDeptViewModel>(null);
            paging.List = new List<SupervisingDeptViewModel>();
            paging.page = 1;
            paging.paginate();
            ViewData[SupervisingDeptConstants.LT_SPV_DEPT] = paging;
            List<SelectListItem> lsSMS = new List<SelectListItem>();
            lsSMS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
            lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
            lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
            ViewData["SMS"] = new SelectList(lsSMS, "Value", "Text");


            List<SelectListItem> lsSMAS = new List<SelectListItem>();
            lsSMAS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "false" });
            lsSMAS.Add(new SelectListItem { Text = Res.Get("Active"), Value = "true" });
            ViewData["SMAS"] = new SelectList(lsSMAS, "Value", "Text");

            SetViewDataPermission("SupervisingDept", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }


        public PartialViewResult TreeViewPartial()
        {
            SupervisingDeptTreeViewModel DeptTree = CommonFunctions.TreeSupervisingDeptManagedByUser(SupervisingDeptBusiness);
            List<SupervisingDeptTreeViewModel> ListDeptNode = new List<SupervisingDeptTreeViewModel>();
            if (DeptTree != null)
            {
                ListDeptNode.Add(DeptTree);
                ViewData[SupervisingDeptConstants.LT_SPV_DEPT_TREE] = ListDeptNode;
            }

            ViewData[SupervisingDeptConstants.LT_SPV_DEPT_TREE] = ListDeptNode;
            return PartialView("_TreeViewPartial");
        }

        /**
         * Chuan bi du lieu cho thao tác thêm mới/sửa
         **/
        private void PrepareData()
        {

            ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = new List<SupervisingDept>();
            ViewData[SupervisingDeptConstants.LST_UNIT_TYPE] = SupervisingDeptConstants.ListUnitType(GlobalInfo.IsSuperRole);
            ViewData[SupervisingDeptConstants.LST_PROVINCE] = CommonFunctions.UserListProvince(ProvinceBusiness);
            ViewData[SupervisingDeptConstants.LST_DISTRICT] = new List<District>();
            //Account đăng nhập là account Admin
            //- Tính chất đơn vị: Disable radio Phòng ban trực thuộc. radio đơn vị quản lý được chọn
            //- Cấp quản lý: 
            //1: Cấp Sở
            //2: Cấp phòng
            //- Thuộc (cboSupervisingDept): Nếu cấp quản lý là cấp Sở: thì chỉ load ra giá trị “Bộ giáo dục và đào tạo”. Nếu cấp quản lý là cấp Phòng. Dùng hàm SupervisingDeptBusiness.Search() với các tham số TraversalPath = SupervisingDept.TraversalPath và thêm tham số HierachyLevel = EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE (3), ProvinceID = cboProvince.Value
            //- cboProvince: Gọi hàm ProvinceBusiness.Search() với các tham số mặc định để load tất cả dữ liệu. Thêm giá trị mặc định [Lựa chọn]
            //- cboDistrict: Gọi hàm DistrictBusiness.Search() với các tham số mặc định bổ sung thêm ProvinceID để lấy các Quận huyện trong tỉnh/thành đã chọn. Thêm giá trị mặc định [Lựa chọn]
            //- Kích hoạt SMAS: 
            //- Kích hoạt SMS:
            if (GlobalInfo.IsSystemAdmin == true)
            {

                ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = SupervisingDeptBusiness.All.Where(O => O.ParentID == null).OrderBy(o => o.SupervisingDeptName).ToList();
                List<SelectListItem> lsSMS = new List<SelectListItem>();
                lsSMS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
                ViewData["SMS"] = new SelectList(lsSMS, "Value", "Text");


                List<SelectListItem> lsSMAS = new List<SelectListItem>();
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "false" });
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Active"), Value = "true" });
                ViewData["SMAS"] = new SelectList(lsSMAS, "Value", "Text");
            }

            //Account đăng nhập là account cấp sở
            //Thuộc (cboSupervisingDept): Nếu chọn radio Đơn vị quản lý thì chỉ load Sở 
            //mà người dung thuộc. Nếu chọn radio Phòng ban trực thuộc. 
            //Dùng hàm SupervisingDeptBusiness.Search() với 
            //ProvinceID = UserInfo.ProvinceID, TraversalPath = SupervisingDept.TraversalPath 
            //và thêm tham số HierachyLevel = 4. Bổ sung them đơn vị Sở,
            //- cboProvince: Lấy ra tỉnh ứng với UserInfo.ProvinceID
            //- cboDistrict: Gọi hàm DistrictBusiness.Search() với các tham số mặc định bổ sung 
            //thêm ProvinceID để lấy các Quận huyện trong tỉnh/thành đã chọn. Thêm giá trị mặc định [Lựa chọn]
            if (GlobalInfo.IsSuperVisingDeptRole)
            {
                //ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = SupervisingDeptBusiness.All.Where(O => O.ParentID == null).OrderBy(o => o.SupervisingDeptName).ToList();
                List<SelectListItem> lsSMS = new List<SelectListItem>();
                lsSMS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
                lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
                ViewData["SMS"] = new SelectList(lsSMS, "Value", "Text");


                List<SelectListItem> lsSMAS = new List<SelectListItem>();
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "false" });
                lsSMAS.Add(new SelectListItem { Text = Res.Get("Active"), Value = "true" });
                ViewData["SMAS"] = new SelectList(lsSMAS, "Value", "Text");

                SupervisingDept svd = SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID.Value);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = GlobalInfo.ProvinceID;
                dic["TraversalPath"] = svd.TraversalPath + svd.SupervisingDeptID.ToString();

                List<SupervisingDept> lstSupervisingDept = new List<SupervisingDept>();
                IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.Search(dic).Where(o => o.HierachyLevel == 4).OrderBy(o => o.SupervisingDeptName);
                if (lsSuper.Count() > 0)
                {
                    lstSupervisingDept = lsSuper.OrderBy(o => o.SupervisingDeptName).ToList();
                }
                List<SupervisingDept> ListSuperVising = new List<SupervisingDept>();
                IQueryable<SupervisingDept> lsParentSupervising = SupervisingDeptBusiness.All
                .Where(o => (o.HierachyLevel == 4 || o.HierachyLevel == 5) && o.ParentID == GlobalInfo.SupervisingDeptID).OrderBy(o => o.SupervisingDeptName);
                if (lsParentSupervising.Count() > 0)
                {
                    List<SupervisingDept> lstParentSuper = lsParentSupervising.ToList();
                    if (lstParentSuper.Select(o => o.HierachyLevel).Distinct().Count() > 1)
                    {
                        foreach (SupervisingDept sdp in lstParentSuper)
                        {
                            sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                            ListSuperVising.Add(sdp);
                        }
                    }

                }
                ListSuperVising.Insert(0, svd);

                ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = ListSuperVising;
                IQueryable<District> lsDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == GlobalInfo.ProvinceID && o.IsActive == true).OrderBy(o => o.DistrictName);
                if (lsDistrict.Count() > 0)
                {
                    ViewData[SupervisingDeptConstants.LST_DISTRICT] = lsDistrict.ToList();
                }
            }




        }

        public PartialViewResult Create()
        {
            PrepareData();
            return PartialView("_Create");
        }

        public PartialViewResult PrepareCreateSub()
        {
            ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = new List<SupervisingDept>();

            SupervisingDept svd = SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ProvinceID"] = GlobalInfo.ProvinceID;
            dic["TraversalPath"] = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            //dic["HierachyLevel"] = 4;
            List<SupervisingDept> lstSupervisingDept = SupervisingDeptBusiness.Search(dic).ToList();
            lstSupervisingDept.Insert(0, svd);


            ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = lstSupervisingDept.OrderBy(o => o.SupervisingDeptName);
            IQueryable<District> lsDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == GlobalInfo.ProvinceID && o.IsActive == true).OrderBy(o => o.DistrictName);
            if (lsDistrict.Count() > 0)
            {
                ViewData[SupervisingDeptConstants.LST_DISTRICT] = lsDistrict.ToList();
            }
            return PartialView("_CreateSub");
        }

        /**
         * Thuc hien viec tao moi don vi
         **/
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SupervisingDeptViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDept", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SupervisingDept obj = new SupervisingDept();
            int RoleID = 0;
            SupervisingDept parent = this.SupervisingDeptBusiness.Find(frm.ParentID);
            obj.TraversalPath = parent.TraversalPath == null ?
                                "\\" + parent.SupervisingDeptID + "\\" :
                                parent.TraversalPath + parent.SupervisingDeptID + "\\";

            obj.IsActive = true;
            if (GlobalInfo.IsSystemAdmin)
            {
                if (frm.UnitType == 1)
                {
                    obj.HierachyLevel = 3;
                    RoleID = SMAS.Web.Constants.GlobalConstants.ROLE_SO;
                }
                if (frm.UnitType == 2)
                {
                    obj.HierachyLevel = 5;
                    RoleID = SMAS.Web.Constants.GlobalConstants.ROLE_PHONG;
                }
            }
            if (GlobalInfo.IsSuperVisingDeptRole)
            {
                if (frm.rdoSubSuper == 1)
                {
                    obj.HierachyLevel = 4;
                }
                if (frm.rdoSubSuper == 2)
                {
                    obj.HierachyLevel = 5;
                    RoleID = SMAS.Web.Constants.GlobalConstants.ROLE_PHONG;
                }
            }
            if (GlobalInfo.IsSubSuperVisingDeptRole)
            {
                obj.HierachyLevel = 6;
            }
            if (frm.Telephone != null)
            {
                obj.Telephone = frm.Telephone.ToString();
            }
            obj.SupervisingDeptCode = frm.SupervisingDeptCode;
            obj.SupervisingDeptName = frm.SupervisingDeptName.Replace("...........","");
            obj.SupervisingDeptID = frm.SupervisingDeptID;

            //if (GlobalInfo.IsSystemAdmin == false)
            //{
            //    obj.ProvinceID = GlobalInfo.ProvinceID;
            //}
            obj.ProvinceID = frm.ProvinceID != 0 ? frm.ProvinceID : int.Parse(Request["ProvinceID2"]);
            obj.ParentID = frm.ParentID;
            obj.DistrictID = frm.DistrictID != 0 ? frm.DistrictID : int.Parse(Request["DistrictID2"]);

            obj.Address = frm.Address;
            obj.IsActiveSMAS = frm.IsActiveSMAS;
            obj.SMSActiveType = frm.SMSActiveType;
            if (frm.rdoSubSuper == 2)
            {
                //cap mat khau cho account
                //Kiểm tra độ dài password
                if (frm.Password.Length < 8)
                {
                    return Json(new { @Message = "Mật khẩu phải lớn hơn 8 ký tự.", Type = "error" });
                }
                else if (!frm.Password.Equals(frm.ConfirmChangePassword))
                {
                    return Json(new { @Message = "Xác nhận mật khẩu mới không hợp lệ.", Type = "error" });
                }
                else if (!SMAS.Business.Common.Utils.CheckPasswordInValid(frm.Password))
                {
                    // return Res.Get("Mật khẩu mới phải có chữ in hoa, chữ thường và ký tự đặt biệt."); //kí tự đặt biệt không chưa dấu ' và / \
                    return Json(new { @Message = "Mật khẩu mới không được nhỏ hơn 8 ký tự, phải bao gồm số, chữ thường, chữ in hoa và ký tự đặc biệt.", Type = "error" });
                }
                else if (frm.Password != frm.ConfirmChangePassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFail"), "error"));
                }
                else if (!CheckPassRequire(frm.Password))
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFormatFail"), "error"));

                }
                else if (frm.Password.Length < MinLengthPassword || frm.Password.Length > MaxLengthPassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_MaxlengthPassword"), "error"));
                }
            }
            this.SupervisingDeptBusiness.InsertSupervisingDept(obj, frm.UserName, GlobalInfo.UserAccountID, RoleID, frm.ActiveAccount,frm.Password);
            this.SupervisingDeptBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult CreateSub(SupervisingDeptViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDept", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SupervisingDept obj = new SupervisingDept();
            SupervisingDept super = this.SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID.Value);
            SupervisingDept parent = this.SupervisingDeptBusiness.Find(frm.ParentID);
            obj.TraversalPath = parent.TraversalPath == null ?
                                "\\" + parent.SupervisingDeptID + "\\" :
                                parent.TraversalPath + parent.SupervisingDeptID + "\\";
            obj.IsActive = true;
            obj.HierachyLevel = 6;
            obj.SupervisingDeptCode = frm.SupervisingDeptCode;
            obj.SupervisingDeptName = frm.SupervisingDeptName;
            obj.ProvinceID = GlobalInfo.ProvinceID;
            obj.ParentID = frm.ParentID;
            obj.DistrictID = super.DistrictID;
            this.SupervisingDeptBusiness.Insert(obj);
            this.SupervisingDeptBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }

        public const int HierachyLevel4 = 4;
        public const int HierachyLevel5 = 5;

        private SupervisingDeptViewModel PrepareEdit(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            PrepareData();
            SupervisingDept supervisingdept = new SupervisingDept();
            SupervisingDept supervisingdept2 = SupervisingDeptBusiness.Find(id);
            Utils.Utils.BindTo(supervisingdept2, supervisingdept);
            supervisingdept.SupervisingDeptName = supervisingdept.SupervisingDeptName.Replace("...........", "");
            
            //Day vao session gia tri id sua phuc vu cho load lai combo thuoc loai bo phan tu chinh no
            Session["SupervisingDeptID"] = id;


            // Danh sách tỉnh thành
            IEnumerable<District> lst = CommonFunctions.UserListDistrict(supervisingdept.ProvinceID, DistrictBusiness).OrderBy(o => o.DistrictName);
            ViewData[SupervisingDeptConstants.LST_DISTRICT] = lst;

            // Danh sách đơn vị cha
            int ProvinceOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE");
            int DistrictOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE");
            int Ministry = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_MINISTRY");

            //Hungnd 24/04/2013 fix when login by superadmin

            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<SupervisingDept> lstSupervisingDept = new List<SupervisingDept>();
            SupervisingDept parentSupervisingDept = new SupervisingDept(); // Add parent node
            dic["ProvinceID"] = GlobalInfo.ProvinceID;
            if (!GlobalInfo.IsSystemAdmin)
            {
                parentSupervisingDept = SupervisingDeptBusiness.GetSupervisingDeptOfUser(GlobalInfo.UserAccountID);

            }
            else
            {
                parentSupervisingDept = SupervisingDeptBusiness.All.Where(a => a.ParentID == 0 || a.ParentID == null).FirstOrDefault();
            }

            dic["TraversalPath"] = parentSupervisingDept.TraversalPath + parentSupervisingDept.SupervisingDeptID.ToString();
            IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.Search(dic);
            lsSuper = lsSuper.Where(o => o.SupervisingDeptID != id && o.HierachyLevel == 5);

            if (lsSuper.Count() > 0)
            {
                lstSupervisingDept = lsSuper.ToList();
            }
            lstSupervisingDept.Insert(0, parentSupervisingDept);
            ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = lstSupervisingDept.OrderBy(o => o.SupervisingDeptName);

            //xem danh sach cac con cua donvi NAMTA
            dic = new Dictionary<string, object>();
            dic["TraversalPath"] = supervisingdept.TraversalPath + supervisingdept.SupervisingDeptID.ToString();
            IQueryable<SupervisingDept> lsSuperChild = SupervisingDeptBusiness.Search(dic);

            dic = new Dictionary<string, object>();
            dic["SupervisingDeptID"] = supervisingdept.SupervisingDeptID;
            IQueryable<SchoolProfile> lsSchoolProfile = SchoolProfileBusiness.Search(dic);



            IDictionary<string, object> search = new Dictionary<string, object>();
            dic["ParentID"] = id;

            IQueryable<SupervisingDept> listSupersingDeptForParent = SupervisingDeptBusiness.Search(search);

            //end fix when login by...

            SupervisingDeptViewModel frm = new SupervisingDeptViewModel();
            Utils.Utils.BindTo(supervisingdept, frm);
            frm.ProvinceID = supervisingdept.ProvinceID != null ? supervisingdept.ProvinceID.Value : (int)0;
            frm.DistrictID = supervisingdept.DistrictID != null ? supervisingdept.DistrictID.Value : (int)0;
            frm.UnitType = CommonFunctions.GetUnitTypeFromSupervisingDept(supervisingdept);
            frm.ActiveAccount = supervisingdept.UserAccount != null ? (supervisingdept.UserAccount.aspnet_Users != null && supervisingdept.UserAccount.aspnet_Users.aspnet_Membership.IsApproved == 1) ? true : false : false;
            frm.UserName = supervisingdept.UserAccount != null && supervisingdept.UserAccount.aspnet_Users != null ? supervisingdept.UserAccount.aspnet_Users.UserName : "";
            frm.IsActiveSMAS = supervisingdept.IsActiveSMAS;
            if (supervisingdept.HierachyLevel == HierachyLevel4)
            {
                frm.rdoSubSuper = 1;
            }
            if (supervisingdept.HierachyLevel == HierachyLevel5 || supervisingdept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                frm.rdoSubSuper = 2;
            }
            if ((lsSuperChild != null && lsSuperChild.Count() > 0) || (lsSchoolProfile != null && lsSchoolProfile.Count() > 0))
            {
                frm.VisUnitType = true;
            }
            else
            {
                frm.VisUnitType = false;
            }

            return frm;
        }
        [SkipCheckRole]
        public PartialViewResult Edit(int id)
        {
            return PartialView("_Edit", PrepareEdit(id));
        }

        public PartialViewResult EditSub(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SupervisingDept supervisingdept = this.SupervisingDeptBusiness.Find(id);
            SupervisingDept superLogin = this.SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID);
            string TraversalPath = superLogin.TraversalPath + superLogin.SupervisingDeptID.ToString() + "\\";
            List<SupervisingDept> ListParent = new List<SupervisingDept>();
            if (supervisingdept.HierachyLevel == 3)
            {
                SupervisingDept super = SupervisingDeptBusiness.All.Where(o => o.IsActive == true && o.ParentID == null).FirstOrDefault();
            }
            // Nếu đơn vị đang sửa là cấp phòng ban trực thuộc thì list đơn vị cha là Phòng, sở, bộ
            if (supervisingdept.HierachyLevel == 4)
            {
                IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.All.Where(o => o.IsActive == true && o.HierachyLevel == 3 && o.TraversalPath.Contains(TraversalPath));
                if (lsSuper.Count() > 0)
                {
                    ListParent = lsSuper.ToList();
                }
            }
            if (supervisingdept.HierachyLevel == 6)
            {
                IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.All.Where(o => o.IsActive == true && o.SupervisingDeptID != id && o.TraversalPath.Contains(TraversalPath));
                if (lsSuper.Count() > 0)
                {
                    ListParent = lsSuper.ToList();
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ParentID"] = id;
            IQueryable<SupervisingDept> lsSuperChild = SupervisingDeptBusiness.Search(dic);



            ListParent.Insert(0, superLogin);
            // Đưa dữ liệu vào ViewData
            ViewData[SupervisingDeptConstants.LST_PARENT_DEPT] = ListParent;
            // Danh sách tỉnh thành
            IEnumerable<District> lst = CommonFunctions.UserListDistrict(supervisingdept.ProvinceID, DistrictBusiness);
            ViewData[SupervisingDeptConstants.LST_DISTRICT] = lst.OrderBy(o => o.DistrictName);
            SupervisingDeptViewModel frm = new SupervisingDeptViewModel();
            Utils.Utils.BindTo(supervisingdept, frm);

            if (lsSuperChild != null && lsSuperChild.Count() > 0)
            {
                frm.VisUnitType = true;
            }
            else
            {
                frm.VisUnitType = false;
            }

            return PartialView("_EditSub", frm);
        }

        /**
         * Thuc hien sua du lieu
         **/
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Edit(SupervisingDeptViewModel frm)
        {
            SupervisingDept obj = this.SupervisingDeptBusiness.Find(frm.SupervisingDeptID);
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDept", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(frm.SupervisingDeptID, "SupervisingDept", 5);
            SupervisingDept parent = this.SupervisingDeptBusiness.Find(frm.ParentID);
            if (parent != null)
            {
                obj.TraversalPath = parent.TraversalPath == null ?
                                    "\\" + parent.SupervisingDeptID + "\\" :
                                    parent.TraversalPath + parent.SupervisingDeptID + "\\";
                obj.ParentID = frm.ParentID;
            }


            obj.IsActive = true;
            obj.SupervisingDeptID = frm.SupervisingDeptID;
            obj.SupervisingDeptName = frm.SupervisingDeptName.Replace("...........", "");
            obj.SupervisingDeptCode = frm.SupervisingDeptCode;
            obj.Telephone = frm.Telephone;
            obj.Address = frm.Address;
            if (frm.DistrictID != 0)
            {
                obj.DistrictID = frm.DistrictID;
            }
            if (frm.ProvinceID != 0)
            {
                obj.ProvinceID = frm.ProvinceID;
            }

            obj.IsActiveSMAS = Request["IsActiveSMAS"] != null ? bool.Parse(Request["IsActiveSMAS"]) : false;
            obj.SMSActiveType = frm.SMSActiveType;

            //•	Nếu là Admin: Chỉ thêm được Sở HierachyLevel = 3
            //•	Nếu là Account Sở: Nếu chọn Phòng ban trực thuộc HierachyLevel = 4, nếu chọn Đơn vị quản lý: HierachyLevel = 5
            //•	Nếu là Account Phòng: HierachyLevel = 6
            if (GlobalInfo.IsSystemAdmin)
            {
                if (frm.UnitType == 1)
                {
                    obj.HierachyLevel = 3;
                }
                if (frm.UnitType == 2)
                {
                    obj.HierachyLevel = 5;
                }
            }
            if (GlobalInfo.IsSuperVisingDeptRole)
            {

                if (frm.rdoSubSuper == 1)
                {
                    if (obj.HierachyLevel == 5)
                    {
                        SupervisingDeptBusiness.DeleteSub(frm.SupervisingDeptID);
                    }
                    obj.HierachyLevel = 4;
                }
                if (frm.rdoSubSuper == 2)
                {
                    if (obj.HierachyLevel == 4)
                    {
                        SupervisingDeptBusiness.DeleteSub(frm.SupervisingDeptID);
                    }
                    obj.HierachyLevel = 5;
                }
            }
            if (GlobalInfo.IsSubSuperVisingDeptRole)
            {
                obj.HierachyLevel = 6;
            }
            int RoleID = 0;
            if (frm.rdoSubSuper == 1)
            {
                this.SupervisingDeptBusiness.Update(obj);
            }
            else if (frm.rdoSubSuper == 2)
            {
                if (frm.GetPassword)
                {
                    //cap mat khau cho account
                    //Kiểm tra độ dài password
                    if (frm.Password.Length < 8)
                    {
                        return Json(new JsonMessage("Mật khẩu phải lớn hơn 8 ký tự.","error" ));
                    }
                    else if (!frm.Password.Equals(frm.ConfirmChangePassword))
                    {
                        return Json(new JsonMessage("Xác nhận mật khẩu mới không hợp lệ.", "error"));
                    }
                    else if (!SMAS.Business.Common.Utils.CheckPasswordInValid(frm.Password))
                    {
                        // return Res.Get("Mật khẩu mới phải có chữ in hoa, chữ thường và ký tự đặt biệt."); //kí tự đặt biệt không chưa dấu ' và / \
                        return Json(new JsonMessage("Mật khẩu mới không được nhỏ hơn 8 ký tự, phải bao gồm số, chữ thường, chữ in hoa và ký tự đặc biệt.", "error"));
                    }
                    else if (frm.Password != frm.ConfirmChangePassword)
                    {
                        return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFail"), "error"));
                    }
                    else if (!CheckPassRequire(frm.Password))
                    {
                        return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFormatFail"), "error"));

                    }
                    else if (frm.Password.Length < MinLengthPassword || frm.Password.Length > MaxLengthPassword)
                    {
                        return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_MaxlengthPassword"), "error"));
                    }
                    UserAccountBusiness.ResetPassword(obj.AdminID.Value, frm.Password);
                }
                if (parent == null)
                {
                    this.SupervisingDeptBusiness.UpdateSupervisingDept(obj, frm.ActiveAccount, frm.GetPassword);
                }
                else
                {
                    if (frm.UnitType != 0)
                    {
                        if (frm.UnitType == SupervisingDeptConstants.UNIT_TYPE_DEPT)
                        {
                            RoleID = Web.Constants.GlobalConstants.ROLE_SO;
                        }
                        else if (frm.UnitType == SupervisingDeptConstants.UNIT_TYPE_SUBDEPT)
                        {
                            RoleID = Web.Constants.GlobalConstants.ROLE_PHONG;
                        }
                    }

                    this.SupervisingDeptBusiness.UpdateSupervisingDept(obj, frm.ActiveAccount, frm.GetPassword, RoleID);
                }
            }

            this.SupervisingDeptBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult EditSub(SupervisingDeptViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDept", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(frm.SupervisingDeptID, "SupervisingDept", 5);
            SupervisingDept obj = this.SupervisingDeptBusiness.Find(frm.SupervisingDeptID);
            if (frm.ParentID != 0)
            {
                SupervisingDept parent = this.SupervisingDeptBusiness.Find(frm.ParentID);
                obj.TraversalPath = parent.TraversalPath == null ?
                                    "\\" + parent.SupervisingDeptID + "\\" :
                                    parent.TraversalPath + parent.SupervisingDeptID + "\\";
                obj.ParentID = frm.ParentID;

            }
            obj.IsActive = true;
            obj.HierachyLevel = 6;
            obj.SupervisingDeptCode = frm.SupervisingDeptCode;
            obj.SupervisingDeptName = frm.SupervisingDeptName;
            this.SupervisingDeptBusiness.Update(obj);
            this.SupervisingDeptBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

        }


        /**
         * Thuc hien xoa du lieu
         **/
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDept", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(id, "SupervisingDept", 5);
            this.SupervisingDeptBusiness.Delete(id, GlobalInfo.UserAccountID);
            this.SupervisingDeptBusiness.Save();
            UserAccountBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /**
         * Thuc hien load du lieu Quan/Huyen khi chon Tinh/Thanh Pho
         **/
        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingDistrict(int? provinceId, int typeDistrict = 0)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);

                if (typeDistrict != 0)
                {
                    lst = lst.Where(o => o.DistrictID == GlobalInfo.DistrictID);
                }
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingParentDept(int? UnitType, int? ProvinceID)
        {
            // Danh sách đơn vị cha
            List<SupervisingDept> ListParent = GetListParent(UnitType, ProvinceID);

            return Json(new SelectList(ListParent.OrderBy(o => o.SupervisingDeptName), "SupervisingDeptID", "SupervisingDeptName"));
        }



        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSupervisingDept(int? check, int? UnitType, int? ProvinceID)
        {
            List<SupervisingDept> lstSupervisingDept = new List<SupervisingDept>();
            if (check == 2)
            {
                if (GlobalInfo.IsSuperVisingDeptRole)
                {
                    SupervisingDept svd = SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID.Value);
                    //IDictionary<string, object> dic = new Dictionary<string, object>();
                    //dic["ProvinceID"] = GlobalInfo.ProvinceID;
                    //if (svd != null)
                    //{
                    //    dic["TraversalPath"] = svd.TraversalPath;
                    //}
                    //else
                    //{
                    //    throw new BusinessException("Validate_Noy_SupervisingDept");
                    //}
                    //dic["HierachyLevel"] = 4;

                    //IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.Search(dic);
                    //if (lsSuper.Count() > 0)
                    //{
                    //    lstSupervisingDept = lsSuper.ToList();
                    //}
                    lstSupervisingDept.Add(svd);
                }
                if (GlobalInfo.IsSystemAdmin)
                {
                    if (UnitType == 1)
                    {
                        SupervisingDept svd = SupervisingDeptBusiness.All.Where(o => o.ParentID == null).SingleOrDefault();
                        if (svd != null)
                        {
                            lstSupervisingDept.Add(svd);
                        }
                    }
                    if (UnitType == 2)
                    {
                        int SupervisingDeptID = 0;
                        if (Session["SupervisingDeptID"] != null)
                        {
                            SupervisingDeptID = (int)Session["SupervisingDeptID"];
                        }

                        IDictionary<string, object> dic = new Dictionary<string, object>();
                        dic["ProvinceID"] = ProvinceID;
                        dic["HierachyLevel"] = SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE;
                        IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.Search(dic).Where(o => o.SupervisingDeptID != SupervisingDeptID);
                        if (lsSuper.Count() > 0)
                        {
                            List<SupervisingDept> lst = lsSuper.ToList();

                            foreach (SupervisingDept sdp in lst)
                            {
                                if (sdp.HierachyLevel == 4)
                                {
                                    sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                                }
                                lstSupervisingDept.Add(sdp);
                            }
                        }
                    }
                }

            }
            if (check == 1)
            {
                if (GlobalInfo.IsSuperVisingDeptRole)
                {
                    int SupervisingDeptID = 0;
                    if (Session["SupervisingDeptID"] != null)
                    {
                        SupervisingDeptID = (int)Session["SupervisingDeptID"];
                    }
                    SupervisingDept svd = SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID.Value);
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    //dic["ProvinceID"] = ProvinceID;
                    if (svd != null)
                    {
                        dic["TraversalPath"] = svd.TraversalPath + svd.SupervisingDeptID + "\\";
                    }
                    else
                    {
                        throw new BusinessException("Validate_Noy_SupervisingDept");
                    }
                    dic["HierachyLevel"] = 4;

                    IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.Search(dic).Where(o => o.SupervisingDeptID != SupervisingDeptID);
                    if (lsSuper.Count() > 0)
                    {
                        List<SupervisingDept> lst = lsSuper.OrderBy(o => o.SupervisingDeptName).ToList();

                        foreach (SupervisingDept sdp in lst)
                        {
                            if (sdp.HierachyLevel == 4)
                            {
                                sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                                lstSupervisingDept.Add(sdp);
                            }
                        }
                    }
                    lstSupervisingDept.Insert(0, svd);
                }
            }
            return Json(new SelectList(lstSupervisingDept, "SupervisingDeptID", "SupervisingDeptName"));
        }
        /**
         * Tim kiem don vi
         **/
        [SkipCheckRole]
        public PartialViewResult Search(string SupervisingDeptCode, string SupervisingDeptName, int? ParentDeptId, int? HierachyLevel, int page = 1, string orderBy = "", int? TypeFolder = 0)
        {
            if (ParentDeptId == null)
            {
                throw new Business.Common.BusinessException(Res.Get("Common_Validate_ErrorPermision"));
            }
            this.CheckPermissionForAction(ParentDeptId, "SupervisingDept", 5);
            //SetViewDataPermission("SupervisingDept", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Paginate<SupervisingDeptViewModel> paging = this._Search(SupervisingDeptCode, SupervisingDeptName, ParentDeptId, page, orderBy, HierachyLevel, TypeFolder);
            ViewData[SupervisingDeptConstants.LT_SPV_DEPT] = paging;
            SetViewDataPermission("SupervisingDept", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return PartialView("_List");

        }

        [SkipCheckRole]
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(string SupervisingDeptCode, string SupervisingDeptName, int? ParentDeptId, int page = 1, string orderBy = "", int? HierachyLevel = 0, int? TypeFolder = 0)
        {
            SetViewDataPermission("SupervisingDept", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Paginate<SupervisingDeptViewModel> paging = this._Search(SupervisingDeptCode, SupervisingDeptName, ParentDeptId, page, orderBy, HierachyLevel, TypeFolder);
            GridModel<SupervisingDeptViewModel> gm = new GridModel<SupervisingDeptViewModel>(paging.List);
            gm.Total = paging.total;
            return View(gm);
        }

        private Paginate<SupervisingDeptViewModel> _Search(string SupervisingDeptCode, string SupervisingDeptName, int? ParentDeptId, int page = 1, string orderBy = "", int? HierachyLevel = 0, int? TypeFolder = 0)
        {
            Paginate<SupervisingDeptViewModel> Paging = null;

            // Dieu kien truy van du lieu
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SupervisingDeptCode"] = SupervisingDeptCode;
            SearchInfo["SupervisingDeptName"] = SupervisingDeptName;
            if ((HierachyLevel == 4 || HierachyLevel == 5) && TypeFolder == 1)
            {
                SearchInfo["HierachyLevel"] = HierachyLevel;
            }
            SearchInfo["ParentID"] = ParentDeptId;
            SearchInfo["IsActive"] = true;
            SupervisingDept ParentDept = SupervisingDeptBusiness.Find(ParentDeptId);
            if (ParentDept != null)
            {
                SearchInfo["TraversalPath"] = ParentDept.ParentID == null ? ParentDept.TraversalPath : ParentDept.TraversalPath + ParentDept.SupervisingDeptID + "\\";
            }

            // Thuc hien truy van du lieu
            IQueryable<SupervisingDept> lst = this.SupervisingDeptBusiness.Search(SearchInfo);
            var lstN = lst.Where(o => o.UserAccount == null);
            // Sap xep
            string sort = "";
            string order = "";
            if (orderBy != "")
            {
                sort = orderBy.Split('-')[0];
                order = orderBy.Split('-')[1];
            }

            if (sort == "SupervisingDeptName" || sort == "")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SupervisingDeptName);
                }
                else
                {
                    lst = lst.OrderBy(o => o.SupervisingDeptName);
                }
            }

            else if (sort == "SupervisingDeptCode")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SupervisingDeptCode);
                }
                else
                {
                    lst = lst.OrderBy(o => o.SupervisingDeptCode);
                }
            }
            List<SupervisingDept> lstSup = lst.ToList();
            // Convert du lieu sang doi tuong SupervisingDeptForm
            List<SupervisingDeptViewModel> res = lstSup.Select(o => new SupervisingDeptViewModel
            {
                rdoSubSuper = o.HierachyLevel,
                SupervisingDeptID = o.SupervisingDeptID,
                SupervisingDeptName = o.SupervisingDeptName, // Utils.Utils.AutoInsertSpace(o.SupervisingDeptName, 10, 2),
                SupervisingDeptCode = o.SupervisingDeptCode,
                //UserName = o.UserAccount != null ? (o.UserAccount.aspnet_Users != null ? (Utils.Utils.AutoInsertSpace(o.UserAccount.aspnet_Users.UserName, 12, 1)) : "") : "",
                UserName = o.UserAccount != null ? (o.UserAccount.aspnet_Users != null ? o.UserAccount.aspnet_Users.UserName : "") : "",
                IsLoggedIn = o.UserAccount != null ? (o.UserAccount.FirstLoginDate != null ? true : false) : false,
                IsActiveSMAS = o.IsActiveSMAS,
                SMSActiveType = o.SMSActiveType,
                Password = "********",
                strIsActiveSMAS = Utils.Utils.ConvertIsActiveSMAS(o.IsActiveSMAS),
                strSMSActiveType = Utils.Utils.ConvertIsActiveSMS(o.SMSActiveType)
            }).ToList();
            var lsRes = res.ToList();

            // Thuc hien phan trang tung phan
            Paging = new Paginate<SupervisingDeptViewModel>(lsRes.AsQueryable());
            Paging.page = page;
            Paging.paginate();
            ViewData[SupervisingDeptConstants.LT_SPV_DEPT] = Paging;
            return Paging;
        }

        private List<SupervisingDept> GetListParent(int? UnitType, int? ProvinceID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Danh sách đơn vị cha
            List<SupervisingDept> ListParent = new List<SupervisingDept>();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int ProvinceOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE");
            int DistrictOffice = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE");
            int Ministry = SystemParams.GetInt("EDUCATION_HIERACHY_LEVEL_MINISTRY");
            List<int> ListHierachyLevel = null;
            // Nếu đơn vị đang sửa là cấp Phòng/Sở thì chỉ List ra các đơn vị cha cấp Phòng và Bộ
            if (UnitType == SupervisingDeptConstants.UNIT_TYPE_DEPT)
            {
                ListHierachyLevel = new List<int> { ProvinceOffice };
            }
            // Nếu đơn vị đang sửa là cấp phòng ban trực thuộc thì list đơn vị cha là Phòng, sở, bộ
            else
            {
                ListHierachyLevel = new List<int> { ProvinceOffice, DistrictOffice };
            }

            SearchInfo["ListHierachyLevel"] = ListHierachyLevel;
            SearchInfo["ProvinceID"] = ProvinceID;

            // Nếu account đang đăng nhập là SuperAdmin thì list hết các đơn vị
            if (GlobalInfo.IsSuperRole)
            {
                ListParent = this.SupervisingDeptBusiness
                                 .Search(SearchInfo)
                                 .OrderBy(o => o.TraversalPath)
                                 .ToList();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ListHierachyLevel"] = new List<int> { Ministry };
                SupervisingDept rootdept = this.SupervisingDeptBusiness.Search(dic).FirstOrDefault();
                if (rootdept != null)
                {
                    ListParent.Insert(0, rootdept);
                    ListParent.Insert(0, null);

                }
            }
            // Nếu là Cấp Sở thì list ra nó và cấp phòng con của nỏ
            else if (GlobalInfo.IsProvinceLevel)
            {
                SearchInfo["TraversalPath"] = "\\" + GlobalInfo.SupervisingDeptID + "\\";
                ListParent = this.SupervisingDeptBusiness
                                 .Search(SearchInfo)
                                 .OrderBy(o => o.TraversalPath)
                                 .ToList();
                ListParent.Insert(0, null);
                SupervisingDept svd = this.SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID);
                ListParent.Insert(0, svd);

            }
            // Nếu là cấp phòng thì chỉ list ra nó
            else if (GlobalInfo.IsDistrictLevel)
            {
                ListParent.Insert(0, this.SupervisingDeptBusiness.Find(GlobalInfo.SupervisingDeptID));
            }

            return ListParent;
        }
        public bool CheckPassRequire(string Password)
        {
            bool character = false;
            bool number = false;

            //kiem tra co kytu khong
            foreach (char ch in Password)
            {
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122))
                {
                    character = true;
                }
                if (ch >= 48 && ch <= 64)
                {
                    number = true;
                }
            }
            if (character && number)
                return true;
            return false;
        }
    }
}
