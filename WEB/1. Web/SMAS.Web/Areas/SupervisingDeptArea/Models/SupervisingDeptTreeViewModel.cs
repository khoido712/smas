﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SupervisingDeptArea.Models
{
    public class SupervisingDeptTreeViewModel
    {
        public int SupervisingDeptID { get; set; }
        public string SupervisingDeptName { get; set; }
        public string TraversalPath { get; set; }
        public int HierachyLevel { get; set; }
        public int? ParentID { get; set; }
        public List<SupervisingDeptTreeViewModel> ListChildren { get; set; }

        public SupervisingDeptTreeViewModel()
        {
        }

        public SupervisingDeptTreeViewModel(int SupervisingDeptID,
                                            string SupervisingDeptName,
                                            int HierachyLevel,
                                            string TraversalPath,
                                            List<SupervisingDeptTreeViewModel> ListChildren,int? parentID=null)
        {
            this.SupervisingDeptID = SupervisingDeptID;
            this.SupervisingDeptName = SupervisingDeptName;
            this.TraversalPath = TraversalPath;
            this.HierachyLevel = HierachyLevel;
            this.ListChildren = ListChildren;
            this.ParentID = parentID;
        }
    }
}