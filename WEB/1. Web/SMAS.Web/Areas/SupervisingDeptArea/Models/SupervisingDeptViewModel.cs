﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.Web.Security;

namespace SMAS.Web.Areas.SupervisingDeptArea.Models
{
    public class SupervisingDeptViewModel
    {
        [ScaffoldColumn(false)]
        public int SupervisingDeptID { get; set; }

        public int rdoSubSuper { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_Province")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ProvinceID { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_District")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int DistrictID { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_UnitType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int UnitType { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_CreateSupervisingDept")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ParentID { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_SupervisingDeptName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SupervisingDeptName { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_SupervisingDeptCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SupervisingDeptCode { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_Telephone")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string Telephone { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_Address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Address { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_Username")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^([0-9]|[a-z]|[A-Z]|[A-Z]|[_.])*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_CharSpecial")]
        public string UserName { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_ActiveAccount")]
        public bool ActiveAccount { get; set; }

        [ResourceDisplayName("SupervisingDept_Label_GetPassword")]
        public bool GetPassword { get; set; }

        [ScaffoldColumn(false)]
        public bool IsLoggedIn { get; set; }

        //[ScaffoldColumn(false)]
        //[ResourceDisplayName("Password")]
        //public string Password
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(this.UserName))
        //        {
        //            return null;
        //        }
        //        if (this.IsLoggedIn)
        //        {
        //            return "******";
        //        }
        //        var user = Membership.GetUser(this.UserName);
        //        if (user == null)
        //        {
        //            return "******";
        //        }
        //        if (user.IsLockedOut)
        //        {
        //            return "******";
        //        }
        //        string pass = user.GetPassword();
        //        return pass;
        //    }
        //}
        [ResourceDisplayName("Password")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Password { get; set; }
        [ResourceDisplayName("ConfirmPassword")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ConfirmChangePassword { get; set; }
        public bool? IsActiveSMAS { get; set; }
        public int? SMSActiveType { get; set; }
        public int? DistrictID2 { get; set; }
        public bool? VisUnitType { get; set; }
        public string strIsActiveSMAS { get; set; }
        public string strSMSActiveType { get; set; }
    }
}
