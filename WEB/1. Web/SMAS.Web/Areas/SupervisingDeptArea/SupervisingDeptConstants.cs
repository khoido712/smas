﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.SupervisingDeptArea
{
    public class SupervisingDeptConstants
    {
        public const string LT_SPV_DEPT = "ListSupervisingDept";
        public const string LT_SPV_DEPT_TREE = "ListSupervisingDeptTree";
        public const string LST_PROVINCE = "ListProvince";
        public const string LST_DISTRICT = "ListDistrict";
        public const string LST_UNIT_TYPE = "ListUnitType";
        public const string LST_PARENT_DEPT = "ListParentDept";
        public const int UNIT_TYPE_DEPT = 1;
        public const int UNIT_TYPE_SUBDEPT = 2;
        public const string ISSP = "ISSP";
        public const string DISABLED_DISTRIC = "DISABLED_DISTRIC";
        public static SelectList ListUnitType(bool IsSuperRole)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            if (IsSuperRole)
            {
                lst.Add(new SelectListItem { Text = Res.Get("SupervisingDept_UnitType_Dept"), Value = UNIT_TYPE_DEPT.ToString() });
            }
            lst.Add(new SelectListItem { Text = Res.Get("SupervisingDept_UnitType_SubDept"), Value = UNIT_TYPE_SUBDEPT.ToString() });

            return new SelectList(lst, "Value", "Text");
        }
    }
}