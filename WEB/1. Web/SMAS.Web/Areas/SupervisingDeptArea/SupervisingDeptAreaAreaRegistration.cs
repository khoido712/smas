﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SupervisingDeptArea
{
    public class SupervisingDeptAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SupervisingDeptArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SupervisingDeptArea_default",
                "SupervisingDeptArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
