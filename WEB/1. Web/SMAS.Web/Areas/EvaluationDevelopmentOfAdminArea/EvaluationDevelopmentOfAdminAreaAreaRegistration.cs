﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea
{
    public class EvaluationDevelopmentOfAdminAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EvaluationDevelopmentOfAdminArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EvaluationDevelopmentOfAdminArea_default",
                "EvaluationDevelopmentOfAdminArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
