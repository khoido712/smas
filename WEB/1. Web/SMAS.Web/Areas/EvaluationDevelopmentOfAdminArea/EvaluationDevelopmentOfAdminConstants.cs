﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea
{
    public class EvaluationDevelopmentOfAdminConstants
    {
        public const string LISTPROVINCE = "LISTPROVINCE";//all
        public const string LIST_EDUCATIONLEVEL = "ListEducation";
        public const string LIST_EVALUATION_DEVELOPMENT_GROUP = "ListEvaluationDevelopmentGroup";
        public const string LIST_EVALUATION_DEVELOPMENT_GROUP_CREATE = "ListEvaluationDevelopmentGroupCreate";
        public const string LIST_STATUS = "ListStatus";
        public const string TOTAL = "total";
        public const string PAGE_NUMBER = "position of page";
        public const string LIST_RESULT = "listResult";
        public const string LIST_EDUCATION_CREATE = "ListEducationCreate";
        public const string LIST_EDUCATION_EDIT = "ListEducationEdit";
        public const string LIST_PROVINCE_CREATE = "ListProvinceCreate";    
        public const string LIST_PROVINCE_EDIT = "ListProvinceEdit";
        public const string SHOW_ORDER = "ShowOrder";
        public const string CHECK_CHOOSE_PROVINCE = "CHECK_CHOOSE_PROVINCE";
        public const string IS_LOCK = "IS_LOCK";

        public const string LIST_PROVINCE_FORM = "LIST_PROVINCE_FORM";
        public const string LIST_EDUCATION_FORM = "LIST_EDUCATION_FORM";
        public const string LIST_EVALUATION_DEV_GROUP_FORM = "LIST_EVALUATION_DEV_GROUP_FORM";

        public const string LIST_PROVINCE_APPLY = "LIST_PROVINCE_APPLY";
        public const string LIST_EDUCATION_FAPPLY = "LIST_EDUCATION_FAPPLY";
        public const string LIST_EVALUATION_DEV_GROUP_APPLY = "LIST_EVALUATION_DEV_GROUP_APPLY";

        public const string LIST_EVALUATION_SYSTEM = "LIST_EVALUATION_SYSTEM";
        public const string LIST_EVALUATION_APPLY = "LIST_EVALUATION_APPLY";
        public const string PHISICAL_PATH = "PHISICAL_PATH";
        public const string LIST_ERROR_IMPORT = "LIST_ERROR_IMPORT";
    }
}