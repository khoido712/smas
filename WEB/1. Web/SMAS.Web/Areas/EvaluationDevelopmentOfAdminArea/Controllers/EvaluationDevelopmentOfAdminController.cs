﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;


namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Controllers
{
    public class EvaluationDevelopmentOfAdminController : BaseController
    {
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;

        public EvaluationDevelopmentOfAdminController(
            IProvinceBusiness ProvinceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness,
            IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness)
        {
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
            this.EvaluationDevelopmentBusiness = EvaluationDevelopmentBusiness;
        }

        //
        // GET: /EvaluationDevelopmentOfAdminArea/EvaluationDevelopmentOfAdmin/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public PartialViewResult Search(int? eduId,
           int? provinceId, int? statusId,
           string evaluaCode, string evaluaName, int? evaluaDevGroId, bool showOrder)
        {
            ViewData[EvaluationDevelopmentOfAdminConstants.CHECK_CHOOSE_PROVINCE] = (provinceId.HasValue && provinceId > 0) ? true : false;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //if (eduId > 0)
            SearchInfo["EducationLevelID"] = eduId.HasValue == true ? eduId.Value : 0;
            //if (provinceId > 0)

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            }
            else
            {
                SearchInfo["ProvinceID"] = provinceId.HasValue == true ? provinceId.Value : 0;
            }

            SearchInfo["StatusID"] = statusId.HasValue == true ? statusId.Value : 0;
            SearchInfo["EvaluationDevelopmentGroupID"] = evaluaDevGroId.HasValue == true ? evaluaDevGroId.Value : 0;
            SearchInfo["EvaluationDevelopmentCode"] = evaluaCode != string.Empty ? evaluaCode : string.Empty;
            SearchInfo["EvaluationDevelopmentName"] = evaluaName != string.Empty ? evaluaName : string.Empty;

            //SearchInfo["showOrder"] = showOrder;
            // Page 1
            List<EvaluationDevelopmentOfAdminViewModel> lst = this._Search(SearchInfo).ToList();
            Session[EvaluationDevelopmentOfAdminConstants.LIST_RESULT] = lst;

            if (showOrder)
            {
                ViewData[EvaluationDevelopmentOfAdminConstants.LIST_RESULT] = lst;
            }
            else
            {
                List<EvaluationDevelopmentOfAdminViewModel> lstPage1 = lst.Skip(0).Take(15).ToList();
                ViewData[EvaluationDevelopmentOfAdminConstants.LIST_RESULT] = lstPage1;
            }

            ViewData[EvaluationDevelopmentOfAdminConstants.TOTAL] = lst.Count;
            ViewData[EvaluationDevelopmentOfAdminConstants.PAGE_NUMBER] = 1;
            ViewData[EvaluationDevelopmentOfAdminConstants.SHOW_ORDER] = showOrder;
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EvaluationDevelopmentOfAdminViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            //bool shwOrder = false;
            //shwOrder = (bool)SearchInfo["showOrder"];
            //this.SetViewDataPermission("EvaluationDevelopmentGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            int provinceID = 0;
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            provinceID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "ProvinceID");

            List<EvaluationDevelopment> lstDB = EvaluationDevelopmentBusiness.Search(SearchInfo).ToList();
            List<EducationLevel> lstEducation = ListEducationLevel();

            List<EvaluationDevelopmentOfAdminViewModel> lstResult = new List<EvaluationDevelopmentOfAdminViewModel>();
            List<Province> lstProvince = ListProvince();
            List<EvaluationDevelopmentGroup> lstEvaluationDevGro = ListEvaluationDevelopmentGroup();

            ViewData[EvaluationDevelopmentOfAdminConstants.IS_LOCK] = false;
            if (provinceID != 0)
            {
                var checkLock = lstEvaluationDevGro.Where(x => x.ProvinceID == provinceID).FirstOrDefault();
                if (checkLock != null)
                {
                    ViewData[EvaluationDevelopmentOfAdminConstants.IS_LOCK] = checkLock.IsLocked.HasValue ? (checkLock.IsLocked.Value == true ? false : true) : false;
                }
            }

            var lstSearch = (from lst in lstDB
                             join lstEdu in lstEducation
                             on lst.EducationLevelID equals lstEdu.EducationLevelID
                             join lstEDG in lstEvaluationDevGro
                             on lst.EvaluationDevelopmentGroupID equals lstEDG.EvaluationDevelopmentGroupID
                             select new EvaluationDevelopmentOfAdminViewModel()
                             {
                                 EvaluationID = lst.EvaluationDevelopmentID,
                                 EducationID = lst.EducationLevelID,
                                 EducationName = lstEdu.Resolution,
                                 EvaluationName = lst.EvaluationDevelopmentName,
                                 EvaluationGroupName = lstEDG.EvaluationDevelopmentGroupName,
                                 EvaluationCode = lst.EvaluationDevelopmentCode,
                                 StatusName = lst.IsActive == true ? "Đang hoạt động" : "Tạm ngưng",
                                 StatusID = lst.IsActive == true ? 1 : 0,
                                 EvaluationGroupID = lst.EvaluationDevelopmentGroupID,
                                 OrderBy = lst.OrderID,
                                 ProvinceID = lst.ProvinceID != null && lst.ProvinceID != -1 ? lst.ProvinceID : SystemParamsInFile.COUNTRY,
                                 ProvinceName = lst.ProvinceID != null && lst.ProvinceID != -1 ? lstProvince.Where(x => x.ProvinceID == lst.ProvinceID).FirstOrDefault().ProvinceName : SystemParamsInFile.COUNTRY_NAME,
                                 Description = lst.Description,
                             }).ToList();

            var lstNoCountry = lstSearch.Where(x => x.ProvinceID != SystemParamsInFile.COUNTRY)
                                        .OrderBy(x => x.ProvinceName).ThenBy(x => x.EducationID)
                                        .ThenBy(x => x.EvaluationGroupName).ToList();

            var lstIsCountry = lstSearch.Where(x => x.ProvinceID == SystemParamsInFile.COUNTRY).ToList();
            if (lstIsCountry.Count > 0)
            {
                lstIsCountry = lstIsCountry.OrderBy(x => x.EducationID).ThenBy(x => x.EvaluationGroupName).ToList();
                lstResult = lstIsCountry;
            }

            foreach (var item in lstNoCountry)
            {
                lstResult.Add(item);
            }

            return lstResult;
        }

        /// <summary>
        /// Tìm kiếm có phân trang
        /// </summary>
        /// 
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(GridCommand command)
        {
            int Page = 1;
            if (command != null)
                Page = command.Page;

            IEnumerable<EvaluationDevelopmentOfAdminViewModel> lst;
            if (Session[EvaluationDevelopmentOfAdminConstants.LIST_RESULT] != null)
            {
                lst = (IEnumerable<EvaluationDevelopmentOfAdminViewModel>)Session[EvaluationDevelopmentOfAdminConstants.LIST_RESULT];
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                lst = this._Search(SearchInfo);
            }

            List<EvaluationDevelopmentOfAdminViewModel> lstAtCurrentPage = lst.Skip((Page - 1) * 15).Take(15).ToList();
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_RESULT] = lstAtCurrentPage;
            GridModel<EvaluationDevelopmentOfAdminViewModel> gm = new GridModel<EvaluationDevelopmentOfAdminViewModel>(lstAtCurrentPage);
            gm.Total = lst.Count();
            return View(gm);
        }


        public PartialViewResult GetInfoCreate()
        {
            #region //
            int provinceID = 0;
            List<Province> lsProvinceDB = ListProvince();
            List<ProvinceViewModel> lstProvinceVM = new List<ProvinceViewModel>();
            lstProvinceVM.Add(new ProvinceViewModel { ProvinceID = SystemParamsInFile.COUNTRY, ProvinceName = SystemParamsInFile.COUNTRY_NAME, OrderBy = 1 });
            for (int i = 0; i < lsProvinceDB.Count(); i++)
            {
                lstProvinceVM.Add(new ProvinceViewModel { ProvinceID = lsProvinceDB[i].ProvinceID, ProvinceName = lsProvinceDB[i].ProvinceName, OrderBy = (i + 2) });
            }
            lstProvinceVM = lstProvinceVM.OrderBy(x => x.OrderBy).ToList();

            List<ProvinceViewModel> lstProvinceForm = new List<ProvinceViewModel>();
            List<ProvinceViewModel> lstProvinceApply = new List<ProvinceViewModel>();
            if (_globalInfo.IsSuperVisingDeptRole) // Sở
            {
                provinceID = _globalInfo.ProvinceID.Value;
                lstProvinceForm = lstProvinceVM.Where(x => x.ProvinceID != _globalInfo.ProvinceID.Value).ToList();
                lstProvinceApply = lstProvinceVM.Where(x => x.ProvinceID == _globalInfo.ProvinceID.Value).ToList();
            }
            else // Admin
            {
                lstProvinceApply = lstProvinceVM.Where(x => x.ProvinceID != SystemParamsInFile.COUNTRY).ToList();
                provinceID = lstProvinceApply.Count() > 0 ? lstProvinceApply.FirstOrDefault().ProvinceID : 0;
                lstProvinceForm = lstProvinceVM.Where(x => x.ProvinceID != provinceID).ToList();
            }

            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_PROVINCE_FORM] = new SelectList(lstProvinceForm, "ProvinceID", "ProvinceName");
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_PROVINCE_APPLY] = new SelectList(lstProvinceApply, "ProvinceID", "ProvinceName");


            List<EducationLevel> lstEdu = ListEducationLevel();
            int educationLevelID = lstEdu.Count() > 0 ? lstEdu.FirstOrDefault().EducationLevelID : 0;
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EDUCATION_FORM] = new SelectList(lstEdu, "EducationLevelID", "Resolution", educationLevelID);

            List<EvaluationDevelopmentGroup> lstEvaluationGroupBD = ListEvaluationDevelopmentGroup().Where(x => x.EducationLevelID == educationLevelID && x.IsActive).ToList();

            List<EvaluationDevelopmentGroup> lstEvaluationGroupFrom = lstEvaluationGroupBD.Where(x => (x.ProvinceID.HasValue == false || x.ProvinceID == SystemParamsInFile.COUNTRY)).ToList();
            List<EvaluationDevelopmentGroup> lstEvaluationGroupApply = lstEvaluationGroupBD.Where(x => x.ProvinceID == provinceID).ToList();

            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_DEV_GROUP_FORM] =
                new SelectList(lstEvaluationGroupFrom, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName", educationLevelID);
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_DEV_GROUP_APPLY] =
                new SelectList(lstEvaluationGroupApply, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName", educationLevelID);
            #endregion

            return PartialView("_Create");
        }

        public PartialViewResult GetInfoEdit(int evaluationId)
        {
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EDUCATION_EDIT] = new SelectList(ListEducationLevel(), "EducationLevelID", "Resolution");
            //ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_DEVELOPMENT_GROUP_CREATE] = new SelectList(ListEvaluationDevelopmentGroup(), "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");
            this.GetProvince(EvaluationDevelopmentOfAdminConstants.LIST_PROVINCE_EDIT);
            return PartialView("_Edit");
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetInfoEvaluationEdit(int evaluationId)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            EvaluationDevelopmentOfAdminViewModel objVM = this._Search(SearchInfo).Where(x => x.EvaluationID == evaluationId).FirstOrDefault();

            int provinceId = 0;
            string evalName = string.Empty;
            string evalCode = string.Empty;
            string description = string.Empty;
            int educationId = 0;
            int status = 0;
            int evaluationDevGroId = 0;

            if (objVM != null)
            {
                evalName = objVM.EvaluationName;
                evalCode = objVM.EvaluationCode;
                description = objVM.Description;
                educationId = objVM.EducationID;
                provinceId = objVM.ProvinceID.Value;
                evaluationDevGroId = objVM.EvaluationGroupID;
                status = objVM.StatusID;

                List<EvaluationDevelopmentGroup> lst = ListEvaluationDevelopmentGroup().Where(x => x.ProvinceID == provinceId && x.EducationLevelID == educationId).ToList();
                var lstEvaluationDG = lst.Select(x => new SelectListItem { Value = x.EvaluationDevelopmentGroupID.ToString(), Text = x.EvaluationDevelopmentGroupName, Selected = false }).ToList();

                return Json(new
                {
                    evalName = evalName,
                    evalCode = evalCode,
                    description = description,
                    educationId = educationId,
                    provinceId = provinceId,
                    status = status,
                    evaluationDevGroId = evaluationDevGroId,
                    lstEvaluationDG = lstEvaluationDG
                });
            }

            return Json(null);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetInfoEvaluationDevGro(int provinceId, int educationId)
        {
            if (educationId <= 0 || provinceId < -1)
                return Json(new List<SelectListItem>());
            if (_globalInfo.IsSuperVisingDeptRole)
            {
               // if (_globalInfo.ProvinceID.Value != provinceId)
               //     return Json(new List<SelectListItem>());
            }

            List<EvaluationDevelopmentGroup> lst = lst = ListEvaluationDevelopmentGroup().Where(x => x.EducationLevelID == educationId).ToList();
            if (provinceId == SystemParamsInFile.COUNTRY)
            {
                lst = lst.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == provinceId).ToList();
            }
            else
            {
                lst = lst.Where(x => x.ProvinceID == provinceId).ToList();
            }

            var lstResult = lst.Select(x => new SelectListItem { Value = x.EvaluationDevelopmentGroupID.ToString(), Text = x.EvaluationDevelopmentGroupName, Selected = false }).ToList();

            return Json(lstResult);
        }

        [HttpPost]
        public PartialViewResult AjaxLoadEvaluationSystem(int provinceId, int educationId, int? evaluationGroupID,
             int provinceIdApply, int educationIdApply, int? evaluationGroupIDApply, string arrayEvaluationDev)
        {
            if ((educationId <= 0 || evaluationGroupID < 0 || provinceId < -1)
                || (provinceIdApply <= 0 || educationIdApply < 0 || evaluationGroupIDApply < -1))
                return PartialView("_GridEvaluationSystem");

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (_globalInfo.ProvinceID.Value != provinceIdApply)
                    return PartialView("_GridEvaluationSystem");
            }

            if (provinceId == provinceIdApply)
            {
                return PartialView("_GridEvaluationSystem");
            }

            if (!evaluationGroupID.HasValue)
            { evaluationGroupID = 0; }

            if (!evaluationGroupIDApply.HasValue)
            { evaluationGroupIDApply = 0; }

            List<int> lstEvaluationDevIDApplied = new List<int>(); // Danh sách evaluation đã applied
            if (!string.IsNullOrEmpty(arrayEvaluationDev))
            {
                lstEvaluationDevIDApplied = arrayEvaluationDev.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).ToList();
            List<EvaluationDevelopment> lstEvaluationForm = lstEvaluationDB.Where(x => x.EducationLevelID == educationId && x.EvaluationDevelopmentGroupID == evaluationGroupID).ToList();
            List<EvaluationDevelopment> lstEvaluationApply = lstEvaluationDB.Where(x => x.EducationLevelID == educationIdApply && x.EvaluationDevelopmentGroupID == evaluationGroupIDApply).ToList();

            lstEvaluationForm = lstEvaluationForm.OrderBy(x => x.OrderID).ToList();


            List<EvaluationDevelopmentFormViewModel> lstResult = new List<EvaluationDevelopmentFormViewModel>();
            EvaluationDevelopmentFormViewModel objResult = null;
            string evaluationName = string.Empty;
            for (int i = 0; i < lstEvaluationForm.Count(); i++)
            {
                objResult = new EvaluationDevelopmentFormViewModel();
                objResult.EvaluationName = lstEvaluationForm[i].EvaluationDevelopmentName;
                objResult.EvaluationDevelopmentID = lstEvaluationForm[i].EvaluationDevelopmentID;
                objResult.EvaluationCode = lstEvaluationForm[i].EvaluationDevelopmentCode;
                objResult.OrderID = lstEvaluationForm[i].OrderID;

                evaluationName = lstEvaluationForm[i].EvaluationDevelopmentName.ToUpper().Trim();
                var checkName = lstEvaluationApply.Where(x => x.EvaluationDevelopmentName.ToUpper().Trim() == evaluationName).FirstOrDefault();
                objResult.IsEnabled = checkName != null ? false : true;

                if (lstEvaluationDevIDApplied.Contains(lstEvaluationForm[i].EvaluationDevelopmentID))
                {
                    objResult.IsEnabled = false;
                }

                lstResult.Add(objResult);
            }

            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_SYSTEM] = lstResult;
            return PartialView("_GridEvaluationSystem");
        }

        [HttpPost]
        public PartialViewResult AjaxLoadEvaluationApply(int provinceId, int educationId, int? evaluationGroupID)
        {
            if ((educationId <= 0 || evaluationGroupID < 0 || provinceId < -1))
                return PartialView("_GridEvaluationApply");

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (_globalInfo.ProvinceID.Value != provinceId)
                    return PartialView("_GridEvaluationApply");
            }

            if (!evaluationGroupID.HasValue)
            { evaluationGroupID = 0; }


            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).ToList();
            List<EvaluationDevelopment> lstEvaluationApply = lstEvaluationDB.Where(x => x.EducationLevelID == educationId && x.EvaluationDevelopmentGroupID == evaluationGroupID).ToList();

            lstEvaluationApply = lstEvaluationApply.OrderBy(x => x.OrderID).ToList();

            List<EvaluationDevelopmentFormViewModel> lstResult = new List<EvaluationDevelopmentFormViewModel>();
            EvaluationDevelopmentFormViewModel objResult = null;
            string evaluationName = string.Empty;
            for (int i = 0; i < lstEvaluationApply.Count(); i++)
            {
                objResult = new EvaluationDevelopmentFormViewModel();
                objResult.EvaluationName = lstEvaluationApply[i].EvaluationDevelopmentName;
                objResult.EvaluationDevelopmentID = lstEvaluationApply[i].EvaluationDevelopmentID;
                objResult.EvaluationCode = lstEvaluationApply[i].EvaluationDevelopmentCode;
                objResult.OrderID = lstEvaluationApply[i].OrderID;
                lstResult.Add(objResult);
            }

            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_APPLY] = lstResult;
            return PartialView("_GridEvaluationApply");
        }

        [HttpPost]
        public PartialViewResult AjaxLoadMoveRigth(List<EvaluationDevelopmentFormViewModel> lstEvaluationDevelopmentVM,
            string ArrSystemID, int? EvaluationGroupID, int EducationLevelID)
        {
            List<int> lstEvaluationDevelopmentID = (lstEvaluationDevelopmentVM != null && lstEvaluationDevelopmentVM.Count > 0)
                                                   ? lstEvaluationDevelopmentVM.Select(p => p.EvaluationDevelopmentID).ToList() : new List<int>();

            List<int> lstEvaluationDevSystemID = ArrSystemID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).ToList();

            List<EvaluationDevelopment> lstEvaluationForm = new List<EvaluationDevelopment>();
            if (lstEvaluationDevSystemID.Count() > 0)
            {
                lstEvaluationForm = lstEvaluationDB.Where(x => lstEvaluationDevSystemID.Contains(x.EvaluationDevelopmentID)).ToList();
            }

            List<EvaluationDevelopment> lstEvaluationApply = new List<EvaluationDevelopment>();
            if (lstEvaluationDevelopmentID.Count() > 0)
            {
                lstEvaluationApply = lstEvaluationDB.Where(x => lstEvaluationDevelopmentID.Contains(x.EvaluationDevelopmentID)).ToList();
                //&& !lstEvaluationDevSystemID.Contains(x.EvaluationDevelopmentID)
            }

            List<EvaluationDevelopmentFormViewModel> lstResult = new List<EvaluationDevelopmentFormViewModel>();
            List<EvaluationDevelopmentFormViewModel> lstResultTemp = new List<EvaluationDevelopmentFormViewModel>();
            EvaluationDevelopmentFormViewModel objResult = null;
            string evaluationName = string.Empty;
            for (int i = 0; i < lstEvaluationApply.Count(); i++)
            {
                objResult = new EvaluationDevelopmentFormViewModel();
                objResult.EvaluationName = lstEvaluationApply[i].EvaluationDevelopmentName;
                objResult.EvaluationDevelopmentID = lstEvaluationApply[i].EvaluationDevelopmentID;
                objResult.EvaluationCode = lstEvaluationApply[i].EvaluationDevelopmentCode;
                objResult.OrderID = lstEvaluationApply[i].OrderID;
                lstResult.Add(objResult);
                lstResultTemp.Add(objResult);
            }           

            int maxcode = lstEvaluationApply.Count();
            for (int i = 0; i < lstEvaluationForm.Count(); i++)
            {
                var checkExists = lstResultTemp.Where(x => x.EvaluationName == lstEvaluationForm[i].EvaluationDevelopmentName && x.EvaluationCode == lstEvaluationForm[i].EvaluationDevelopmentCode).FirstOrDefault();
                if (checkExists == null)
                {
                    maxcode = maxcode + 1;
                    objResult = new EvaluationDevelopmentFormViewModel();
                    objResult.EvaluationName = lstEvaluationForm[i].EvaluationDevelopmentName;
                    objResult.EvaluationDevelopmentID = lstEvaluationForm[i].EvaluationDevelopmentID;
                    objResult.EvaluationCode = lstEvaluationForm[i].EvaluationDevelopmentCode; 
                    objResult.OrderID = lstEvaluationForm[i].OrderID;
                    lstResult.Add(objResult);
                }
            }

            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_APPLY] = lstResult;
            return PartialView("_GridEvaluationApply");
        }

        public int FindResultCode(List<string> lstResultCode)
        {
            int result = 0;
            bool TF;
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            foreach (var item in lstResultCode)
            {
                int index = item.ToLower().LastIndexOfAny(alphabet);
                if (index == item.Length - 1)
                {
                    continue;
                }
                string maxCode = item.Substring(index + 1).Trim();
                if (!string.IsNullOrEmpty(maxCode) && maxCode.Length > 0)
                {
                    int resultTemp;
                    TF = Int32.TryParse(maxCode, out resultTemp);
                    if (TF && resultTemp > result)
                        result = resultTemp;
                }
            }
            return result;

        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadProvinceApply(int provinceId)
        {
            if (provinceId < -1)
                return Json(new List<SelectListItem>());

            List<Province> lsProvinceDB = ListProvince();
            List<ProvinceViewModel> lstProvinceVM = new List<ProvinceViewModel>();
            lstProvinceVM.Add(new ProvinceViewModel { ProvinceID = SystemParamsInFile.COUNTRY, ProvinceName = SystemParamsInFile.COUNTRY_NAME, OrderBy = 1 });
            for (int i = 0; i < lsProvinceDB.Count(); i++)
            {
                lstProvinceVM.Add(new ProvinceViewModel { ProvinceID = lsProvinceDB[i].ProvinceID, ProvinceName = lsProvinceDB[i].ProvinceName, OrderBy = (i + 2) });
            }
            lstProvinceVM = lstProvinceVM.Where(x => x.ProvinceID != provinceId).OrderBy(x => x.OrderBy).ToList();
            var lstResult = lstProvinceVM.Select(x => new SelectListItem { Value = x.ProvinceID.ToString(), Text = x.ProvinceName, Selected = true }).ToList();

            return Json(lstResult);
        }


        #region // Xuat Excel
        public FileResult ExportExcel(int ProvinceID)
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                ProvinceID = _globalInfo.ProvinceID.Value;
            }

            string templatePath = string.Empty;
            string fileName = string.Empty;

            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.ChiSoDanhGiaTre + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.CopySheetToLast(firstSheet);
            IVTWorksheet sheet = null;
            IVTRange rang = secondSheet.GetRange("A2", "D7");

            thirdSheet.SetCellValue("A2", "STT");
            thirdSheet.SetColumnWidth(1, 3.75);
            thirdSheet.GetRange(2, 1, 2, 1).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
            thirdSheet.GetRange(2, 1, 2, 1).SetHAlign(VTHAlign.xlHAlignCenter);

            #region // Get Data
            string suppervisingDeptName = _globalInfo.SuperVisingDeptName.ToUpper();
            List<EducationLevel> lstEducation = ListEducationLevel();
            List<int> lstEducationLevelID = lstEducation.Select(x => x.EducationLevelID).ToList();

            IQueryable<EvaluationDevelopmentGroup> lstEvaluationGroupDB = EvaluationDevelopmentGroupBusiness.All.Where(x => x.IsActive
                                                                                                                    && lstEducationLevelID.Contains(x.EducationLevelID));
            if (ProvinceID == SystemParamsInFile.COUNTRY)
            {
                lstEvaluationGroupDB = lstEvaluationGroupDB.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == ProvinceID);
            }
            else
            {
                lstEvaluationGroupDB = lstEvaluationGroupDB.Where(x => x.ProvinceID == ProvinceID);
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            IQueryable<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).Where(x => x.IsActive && lstEducationLevelID.Contains(x.EducationLevelID));
            if (ProvinceID == SystemParamsInFile.COUNTRY)
            {
                lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == ProvinceID);
            }
            else
            {
                lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID == ProvinceID);
            }
            List<EvaluationDevelopmentGroup> lstEvaluationDevGroup = lstEvaluationGroupDB.ToList();
            List<EvaluationDevelopmentExcelViewModel> lstResult = (from edg in lstEvaluationGroupDB
                                                                   join ed in lstEvaluationDB
                                                                    on edg.EvaluationDevelopmentGroupID equals ed.EvaluationDevelopmentGroupID into ps
                                                                   from p in ps.DefaultIfEmpty()
                                                                   select new EvaluationDevelopmentExcelViewModel()
                                                                   {
                                                                       EvaluationGroupName = edg.EvaluationDevelopmentGroupName,
                                                                       EvaluationName = p.EvaluationDevelopmentName,
                                                                       EvaluationCode = p.EvaluationDevelopmentCode,
                                                                       OrderEvaluationGroup = edg.OrderID,
                                                                       OrderEvaluation = p.OrderID,
                                                                       EducationLevelID = edg.EducationLevelID
                                                                   }).ToList();

            lstResult = lstResult.OrderBy(x => x.OrderEvaluationGroup).ThenBy(x => x.OrderEvaluation).ToList();
            #endregion

            #region // fill Sheet
            string educationNameTrim = string.Empty;
            EducationLevel objEdu = null;
            List<EvaluationDevelopmentExcelViewModel> lstResultByEdu = new List<EvaluationDevelopmentExcelViewModel>();
            EvaluationDevelopmentExcelViewModel objResultByEdu = null;
            int startRow = 8;
            int startRow2 = 3;
            int maxIndex = 0;
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation = new VTDataValidation();
            List<string> lstEvaluationGroupName = new List<string>();

            for (int i = 0; i < lstEducation.Count(); i++)
            {
                objEdu = lstEducation[i];
                lstResultByEdu = lstResult.Where(x => x.EducationLevelID == objEdu.EducationLevelID).ToList();
                lstEvaluationGroupName = lstResultByEdu.Select(x => x.EvaluationGroupName).Distinct().ToList();
                var lstEvaluationCode = lstResultByEdu.Where(x => !string.IsNullOrEmpty(x.EvaluationCode)).ToList();
                educationNameTrim = objEdu.Resolution;

                sheet = oBook.CopySheetToBeforeLast(firstSheet);
                sheet.CopyPasteSameSize(rang, 2, 1);
                sheet.SetCellValue("A2", suppervisingDeptName);
                sheet.SetCellValue("A5", ("NHÓM LỚP " + educationNameTrim.ToUpper()));

                if (lstEvaluationCode.Count() > 0)
                {
                    for (int j = 0; j < lstResultByEdu.Count(); j++)
                    {
                        objResultByEdu = lstResultByEdu[j];
                        sheet.SetCellValue(startRow, 1, (j + 1));
                        sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);

                        sheet.SetCellValue(startRow, 2, objResultByEdu.EvaluationGroupName);
                        sheet.GetRange(startRow, 2, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                        sheet.SetCellValue(startRow, 3, objResultByEdu.EvaluationCode);
                        sheet.GetRange(startRow, 3, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);

                        sheet.SetCellValue(startRow, 4, objResultByEdu.EvaluationName);
                        sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignLeft);
                        sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                        startRow++;
                    }
                }

                if (lstEvaluationCode.Count() > 0 && lstResultByEdu.Count() > 0)
                {
                    sheet.GetRange(8, 1, (startRow - 1), 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                    if (lstEvaluationGroupName.Count() > 0)
                    {
                        objValidation = new VTDataValidation
                        {
                            Contrains = lstEvaluationGroupName.ToArray(),
                            FromColumn = 2,
                            FromRow = 8,
                            SheetIndex = (i + 1),
                            ToColumn = 2,
                            ToRow = 8 + lstResultByEdu.Count() - 1,
                            Type = VTValidationType.LIST
                        };
                        lstValidation.Add(objValidation);
                    }
                }
                else
                {
                    sheet.GetRange(8, 1, 27, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    if (lstEvaluationGroupName.Count() > 0)
                    {
                        objValidation = new VTDataValidation
                        {
                            Contrains = lstEvaluationGroupName.ToArray(),
                            FromColumn = 2,
                            FromRow = 8,
                            SheetIndex = (i + 1),
                            ToColumn = 2,
                            ToRow = 27,
                            Type = VTValidationType.LIST
                        };
                        lstValidation.Add(objValidation);
                    }

                    for (int n = 0; n < 20; n++)
                    {
                        sheet.SetCellValue((8 + n), 1, (n + 1));
                        sheet.GetRange((8 + n), 1, (8 + n), 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    }
                }

                startRow = 8;
                educationNameTrim = Utils.Utils.StripVNSign(objEdu.Resolution);
                sheet.Name = educationNameTrim;
                sheet.SetFontName("Times New Roman", 12);
                // Sheet Linh vuc
                if (maxIndex < lstEvaluationGroupName.Count())
                {
                    maxIndex = lstEvaluationGroupName.Count();
                }

                thirdSheet.SetColumnWidth((i + 2), 25);
                thirdSheet.GetRange(2, (i + 2), 2, (i + 2)).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                thirdSheet.GetRange(2, (i + 2), 2, (i + 2)).SetHAlign(VTHAlign.xlHAlignCenter);
                thirdSheet.SetCellValue((startRow2 - 1), (i + 2), objEdu.Resolution);

                for (int e = 0; e < lstEvaluationGroupName.Count(); e++)
                {
                    thirdSheet.SetCellValue(startRow2, (i + 2), lstEvaluationGroupName[e]);
                    startRow2++;
                }
                startRow2 = 3;
            }

            startRow2 = 3;
            if (maxIndex != 0)
            {
                for (int i = 0; i < maxIndex; i++)
                {
                    thirdSheet.SetCellValue(startRow2, 1, (i + 1));
                    thirdSheet.GetRange(startRow2, 1, startRow2, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    startRow2++;
                }

                thirdSheet.GetRange(2, 1, (maxIndex + 2), (lstEducation.Count() + 1)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            thirdSheet.Name = "Linh vuc";
            thirdSheet.SetFontName("Times New Roman", 12);
            #endregion

            firstSheet.Delete();
            secondSheet.Delete();
            Stream excel = oBook.ToStreamValidationData(lstValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "ChiSoDanhGiaTre.xls";
            return result;
        }
        #endregion

        #region // Nhap Excel
        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImport(IEnumerable<HttpPostedFileBase> attachments, int provinceID)
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (provinceID != _globalInfo.ProvinceID)
                    return Json(new JsonMessage(Res.Get("Tỉnh/Thành không hợp lệ!"), "error"));
            }

            if (provinceID == 0)
                return Json(new JsonMessage(Res.Get("Tỉnh/Thành không hợp lệ!"), "error"));

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<string> listSheetNameExcel = new List<string>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                // danh sach nhom lop
                List<EducationLevel> lstEducation = ListEducationLevel();
                List<string> lstEducationName = lstEducation.Select(x => Utils.Utils.StripVNSign(x.Resolution)).ToList();
                string sheetInfo = string.Empty;
                // duyệt kiểm tra từng sheet
                string sheetName = string.Empty;
                StringBuilder strBuilder = new StringBuilder();
                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        sheetInfo = sheet.Name;
                        sheetInfo = Utils.Utils.StripVNSign(sheetInfo);
                        if (!lstEducationName.Contains(sheetInfo) || !sheetInfo.Equals("Linh vuc"))
                        {
                            listSheetNameExcel.Add(sheet.Name);
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("File dữ liệu không hợp lệ"), "error"));
                        }
                    }
                }

                if (listSheetNameExcel.Count() > 0)
                {
                    Session[EvaluationDevelopmentOfAdminConstants.PHISICAL_PATH] = physicalPath;
                    return Json(new { TypeImport = 1 });
                }

                return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }

        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int provinceID)
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (provinceID != _globalInfo.ProvinceID)
                    return Json(new JsonMessage(Res.Get("Tỉnh/Thành không hợp lệ!"), "error"));
            }
            if (provinceID == 0)
                return Json(new JsonMessage(Res.Get("Tỉnh/Thành không hợp lệ!"), "error"));


            if (Session[EvaluationDevelopmentOfAdminConstants.PHISICAL_PATH] == null)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[EvaluationDevelopmentOfAdminConstants.PHISICAL_PATH];

            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = messageSuccess = "Cập nhật từ Excel thành công";
            string Error = string.Empty;

            List<EvaluationDevelopment> lstDataFromExcel = new List<EvaluationDevelopment>();
            List<ErrorViewModel> lstError = new List<ErrorViewModel>();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(SearchInfo).Where(x=>x.IsActive).ToList();

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
            }
            else
            {
                if (provinceID == SystemParamsInFile.COUNTRY)
                {
                    lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == SystemParamsInFile.COUNTRY).ToList();
                }
                else
                {
                    lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID == provinceID).ToList();
                }
            }

            this.GetDataToFileExcel(obook, lstEvaluationDB, provinceID, ref lstDataFromExcel, ref lstError, ref Error);

            if (!string.IsNullOrEmpty(Error))
            {
                return Json(new JsonMessage(Error, "error"));
            }

            if (lstError.Count > 0)
            {
                ViewData[EvaluationDevelopmentOfAdminConstants.LIST_ERROR_IMPORT] = lstError;
                return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "CreateViewError"));      
            }
            else
            {
                if (lstDataFromExcel.Count() > 0)
                {
                    EvaluationDevelopmentBusiness.InsertOrUpdateMultiFromExcel(lstDataFromExcel, lstEvaluationDB);
                    return Json(new JsonMessage(messageSuccess, "success"));
                }
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }

        private void GetDataToFileExcel(IVTWorkbook oBook, 
            List<EvaluationDevelopment> lstEvaluationDB, 
            int provinceID,
            ref List<EvaluationDevelopment> lstDataFromExcel,
            ref List<ErrorViewModel> lstError,
            ref string Error)
        {
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;

            IVTWorksheet sheet = null;
            List<IVTWorksheet> lstSheets = oBook.GetSheets();

            #region Validate du lieu
            int countSheet = lstSheets.Count();
            for (int i = 0; i < countSheet; i++)
            {
                sheet = lstSheets[i];

                string Title = "";
                var TitleObj = sheet.GetCellValue("A5");

                if (TitleObj != null)
                    Title = TitleObj.ToString();

                if (string.IsNullOrEmpty(Title))
                {
                    Error = "File excel không đúng định dạng";
                }

                if (!Title.Contains(sheetNameDefault.ToUpper()))
                {
                    if (string.IsNullOrEmpty(Error))
                    {
                        Error = "Nhóm lớp " + sheetNameDefault + " không hợp lệ";
                    }
                }

                if (!string.IsNullOrEmpty(Error))
                {
                    return;
                }
            }
            #endregion

            #region // Get data from Excel
            List<EducationLevel> lstEducation = ListEducationLevel();
            List<EvaluationDevelopmentGroup> lstEvaluationGroupDB = ListEvaluationDevelopmentGroup().Where(x => x.IsActive).ToList();

            if (provinceID == SystemParamsInFile.COUNTRY)
            {
                lstEvaluationGroupDB = lstEvaluationGroupDB.Where(x => (x.ProvinceID.HasValue == false || x.ProvinceID == SystemParamsInFile.COUNTRY)).ToList();
            }
            else
            {
                lstEvaluationGroupDB = lstEvaluationGroupDB.Where(x => (x.ProvinceID == provinceID)).ToList();
            }


            ErrorViewModel objError = null;
            bool isError = false;
            List<IndexEvaluationCodeViewModel> lstIndexEvaluationCodeVM = new List<IndexEvaluationCodeViewModel>();
            int startRow = 8;
            int indexError = 0;
            int acb = sheet.Worksheet.UsedRange.Row + sheet.Worksheet.UsedRange.Rows.Count - 1;

            string evaluationCode = string.Empty;
            string evaluationGroupName = string.Empty;
            string evaluationName = string.Empty;

            EvaluationDevelopment objEvaluation = null;

            for (int i = 0; i < countSheet; i++)
            {
                sheet = lstSheets[i];

                if (sheet.Name.ToUpper().Equals("LINH VUC"))
                    continue;

                var objEdu = lstEducation.Where(x => sheet.Name == Utils.Utils.StripVNSign(x.Resolution)).FirstOrDefault();

                if (objEdu == null)
                    continue;

                #region //duyệt nội dung chỉ số
                while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null && sheet.GetCellValue(startRow, 4) != null)
                {
                    #region // Validate
                    evaluationGroupName = sheet.GetCellValue(startRow, 2).ToString().Trim();
                    evaluationCode = sheet.GetCellValue(startRow, 3).ToString().Trim();
                    evaluationName = sheet.GetCellValue(startRow, 4).ToString().Trim();

                    var checkEvaluationGroup = lstEvaluationGroupDB.Where(x => x.EvaluationDevelopmentGroupName.ToUpper() == evaluationGroupName.ToUpper()).FirstOrDefault();
                    if (checkEvaluationGroup == null)
                    {
                        objError = new ErrorViewModel();
                        objError.EvaluationCode = evaluationCode;
                        objError.EvaluationName = evaluationName;
                        objError.EvaluationNameGroup = evaluationGroupName;
                        objError.Description = "Tên lĩnh vực không tồn tại.";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);

                        indexError++;

                        startRow++;
                        continue;
                    }

                    lstIndexEvaluationCodeVM.Add(new IndexEvaluationCodeViewModel()
                    {
                        EvaluationGroupName = evaluationGroupName.ToUpper(),
                        EvaluationCode = evaluationCode.ToUpper(),
                        EvaluationName = evaluationName.ToUpper(),
                        EducationLevel = objEdu.EducationLevelID
                    });

                    var checkValidate = lstIndexEvaluationCodeVM.Where(x => x.EvaluationGroupName == evaluationGroupName.ToUpper()
                                                                        && x.EducationLevel == objEdu.EducationLevelID).ToList();
                    if (checkValidate.Count() > 1)
                    {
                        var checkCode = checkValidate.Where(x => x.EvaluationCode == evaluationCode.ToUpper()).ToList();
                        if (checkCode.Count() > 1)
                        {
                            objError = new ErrorViewModel();
                            objError.EvaluationCode = evaluationCode;
                            objError.EvaluationName = evaluationName;
                            objError.EvaluationNameGroup = evaluationGroupName;
                            objError.Description = "Các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì mã chỉ số không được trùng nhau.";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);

                            indexError++;
                            isError = true;
                        }

                        var checkName = checkValidate.Where(x => x.EvaluationName == evaluationName.ToUpper()).ToList();
                        if (checkName.Count() > 1)
                        {
                            objError = new ErrorViewModel();
                            objError.EvaluationCode = evaluationCode;
                            objError.EvaluationName = evaluationName;
                            objError.EvaluationNameGroup = evaluationGroupName;
                            objError.Description = "Các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.";
                            objError.SheetName = sheet.Name;
                            lstError.Add(objError);

                            indexError++;
                            isError = true;
                        }
                    }

                    if (evaluationCode.Length > 20)
                    {
                        objError = new ErrorViewModel();
                        objError.EvaluationCode = evaluationCode;
                        objError.EvaluationName = evaluationName;
                        objError.EvaluationNameGroup = evaluationGroupName;
                        objError.Description = "Mã chỉ số không được phép quá 20 ký tự.";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);

                        indexError++;
                        isError = true;
                    }

                    if (evaluationName.Length > 200)
                    {
                        objError = new ErrorViewModel();
                        objError.EvaluationCode = evaluationCode;
                        objError.EvaluationName = evaluationName;
                        objError.EvaluationNameGroup = evaluationGroupName;
                        objError.Description = "Nội dung chỉ số không được phép quá 200 ký tự.";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);

                        indexError++;
                        isError = true;
                    }

                    if (isError)
                    {
                        startRow++;
                        isError = false;
                        continue;
                    }
                    #endregion

                    var objEvaluationGroup = lstEvaluationGroupDB.Where(x => x.EvaluationDevelopmentGroupName.ToUpper() == evaluationGroupName.ToUpper()
                                                                        && x.EducationLevelID == objEdu.EducationLevelID).FirstOrDefault();

                    objEvaluation = new EvaluationDevelopment();
                    objEvaluation.EvaluationDevelopmentName = evaluationName;
                    objEvaluation.EvaluationDevelopmentCode = evaluationCode.ToUpper();
                    objEvaluation.Description = evaluationName;
                    objEvaluation.EducationLevelID = objEdu.EducationLevelID;
                    objEvaluation.IsActive = true;
                    objEvaluation.IsRequired = true;
                    objEvaluation.EvaluationDevelopmentGroupID = objEvaluationGroup.EvaluationDevelopmentGroupID;
                    objEvaluation.ModifiedDate = DateTime.Now;
                    objEvaluation.OrderID = null;
                    objEvaluation.ProvinceID = provinceID;
                    lstDataFromExcel.Add(objEvaluation);

                    startRow++;
                }
                startRow = 8;
                #endregion
            }
            #endregion
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        private bool SetIsNullOrEmpty(string evalName, string evalCode)
        {
            if (!string.IsNullOrEmpty(evalName) && !string.IsNullOrEmpty(evalCode))
                return true;
            return false;
        }

        #region SaveEvaluation
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEvaluation(FormCollection frm)
        {
            int provinceId = !string.IsNullOrEmpty(frm["provinceId"]) ? int.Parse(frm["provinceId"]) : 0;
            int educationId = !string.IsNullOrEmpty(frm["EducationLevelID_Create"]) ? int.Parse(frm["EducationLevelID_Create"]) : 0;
            int evaluationDevGroId = !string.IsNullOrEmpty(frm["EvaluationDevelopmentGroupID_Create"]) ? int.Parse(frm["EvaluationDevelopmentGroupID_Create"]) : 0;
            string evaluationName = !string.IsNullOrEmpty(frm["evaluationName"]) ? frm["evaluationName"] : string.Empty;
            string evaluationCode = !string.IsNullOrEmpty(frm["evaluationCode"]) ? frm["evaluationCode"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["description"]) ? frm["description"] : string.Empty;

            if (provinceId != 0 && educationId != 0 && evaluationDevGroId != 0 && this.SetIsNullOrEmpty(evaluationName, evaluationCode))
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    if (_globalInfo.ProvinceID.Value != provinceId)
                    {
                        return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
                    }
                }

                string strPattern = @"[\s]+";
                Regex rgx = new Regex(strPattern);
                string evalName = rgx.Replace(evaluationName, " ");
                string evalCode = rgx.Replace(evaluationCode, " ");
                evalCode = evalCode.ToUpper();

                EducationLevel objEdu = ListEducationLevel().Where(x => x.EducationLevelID == educationId).FirstOrDefault();
                if (objEdu == null)
                    return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));

                EvaluationDevelopmentGroup objEDG = ListEvaluationDevelopmentGroup().Where(x => x.ProvinceID == provinceId
                                                                                    && x.EducationLevelID == educationId
                                                                                    && x.EvaluationDevelopmentGroupID == evaluationDevGroId).FirstOrDefault();
                if (objEDG == null)
                    return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = educationId;
                List<EvaluationDevelopment> lstDB = EvaluationDevelopmentBusiness.Search(SearchInfo).ToList();
                if (lstDB.Count > 0)
                {
                    #region // Validate TH
                    var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                                        && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)).FirstOrDefault();
                    if (testCode != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }

                    var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                                        && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)).FirstOrDefault();
                    if (testName != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }
                    #endregion
                }

                EvaluationDevelopment objInput = null;
                var orderDB = lstDB.Where(x => x.ProvinceID == provinceId).OrderByDescending(x => x.OrderID).FirstOrDefault();
                objInput = new EvaluationDevelopment()
                {
                    EvaluationDevelopmentCode = evalCode,
                    EvaluationDevelopmentName = evalName,
                    Description = description,
                    IsRequired = true,
                    InputType = 1,
                    EducationLevelID = educationId,
                    EvaluationDevelopmentGroupID = evaluationDevGroId,
                    CreatedDate = DateTime.Now,
                    IsActive = true,
                    OrderID = orderDB != null ? (orderDB.OrderID.Value + 1) : 1,
                    ProvinceID = provinceId
                };

                this.EvaluationDevelopmentBusiness.InsertEval(objInput);
                return Json(new JsonMessage(Res.Get("Thêm mới thành công"), "success"));
            }
            return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
        }
        #endregion

        #region EditEvaluation
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult EditEvaluation(FormCollection frm)
        {
            int evalID = !string.IsNullOrEmpty(frm["evalIdEdit"]) ? int.Parse(frm["evalIdEdit"]) : 0;
            int provinceId = !string.IsNullOrEmpty(frm["provinceIdEdit"]) ? int.Parse(frm["provinceIdEdit"]) : 0;
            int provinceIdOld = !string.IsNullOrEmpty(frm["provinceIdOld"]) ? int.Parse(frm["provinceIdOld"]) : 0;
            int educationId = !string.IsNullOrEmpty(frm["EducationLevelID_Edit"]) ? int.Parse(frm["EducationLevelID_Edit"]) : 0;
            int educationIdOld = !string.IsNullOrEmpty(frm["educationIdOld"]) ? int.Parse(frm["educationIdOld"]) : 0;
            int evaluationDevGroId = !string.IsNullOrEmpty(frm["EvaluationDevelopmentGroupID_Edit"]) ? int.Parse(frm["EvaluationDevelopmentGroupID_Edit"]) : 0;
            int evaluationDevGroIdOld = !string.IsNullOrEmpty(frm["evaluationDevGroIdOld"]) ? int.Parse(frm["evaluationDevGroIdOld"]) : 0;
            string evaluationName = !string.IsNullOrEmpty(frm["evaluationNameEdit"]) ? frm["evaluationNameEdit"] : string.Empty;
            string evaluationCode = !string.IsNullOrEmpty(frm["evaluationCodeEdit"]) ? frm["evaluationCodeEdit"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["descriptionEdit"]) ? frm["descriptionEdit"] : string.Empty;

            if (provinceId != 0 && educationId != 0 && evaluationDevGroId != 0 && evalID != 0 && evaluationDevGroIdOld != 0 &&
                provinceIdOld != 0 && educationIdOld != 0 && this.SetIsNullOrEmpty(evaluationName, evaluationCode))
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    if (_globalInfo.ProvinceID.Value != provinceId && _globalInfo.ProvinceID.Value != provinceIdOld)
                    {
                        return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
                    }
                }

                string strPattern = @"[\s]+";
                Regex rgx = new Regex(strPattern);
                evaluationName = evaluationName.Trim();
                string evalName = rgx.Replace(evaluationName, " ");
                evaluationCode = evaluationCode.Trim();
                string evalCode = rgx.Replace(evaluationCode, " ");
                evalCode = evalCode.ToUpper();

                var lstEducationDB = ListEducationLevel();
                var objEdu = lstEducationDB.Where(x => x.EducationLevelID == educationId).FirstOrDefault();
                if (objEdu == null)
                    return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));

                var lstEvaluationDevGroDB = ListEvaluationDevelopmentGroup();
                var objEDG = lstEvaluationDevGroDB.Where(x => x.ProvinceID == provinceId && x.EducationLevelID == educationId
                                                        && x.EvaluationDevelopmentGroupID == evaluationDevGroId).FirstOrDefault();
                if (objEDG == null)
                    return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                //SearchInfo["EducationLevelID"] = educationId;
                List<EvaluationDevelopment> lstDB = EvaluationDevelopmentBusiness.Search(SearchInfo).ToList();
                if (lstDB.Count > 0)
                {
                    #region // Validate TH1 - provinceId == provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId == educationIdOld
                    if (provinceId == provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId == educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                        && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)
                        && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH2 - provinceId == provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId != educationIdOld
                    if (provinceId == provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId != educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                       && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID != educationId)
                       && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID != educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH3 - provinceId == provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId != educationIdOld
                    if (provinceId == provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId != educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                       && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID != educationId)
                       && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID != educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH4 - provinceId != provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId != educationIdOld
                    if (provinceId != provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId != educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                       && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID != educationId)
                       && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID != educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH5 - provinceId != provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId == educationIdOld
                    if (provinceId != provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId == educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                           && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID == educationId)
                           && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                            && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID == educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH6 - provinceId != provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId == educationIdOld
                    if (provinceId != provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId == educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                       && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)
                       && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID == educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH7 - provinceId != provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId != educationIdOld
                    if (provinceId != provinceIdOld && evaluationDevGroId == evaluationDevGroIdOld && educationId != educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                           && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID != educationId)
                           && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                              && (x.ProvinceID != provinceId && x.EvaluationDevelopmentGroupID == evaluationDevGroId && x.EducationLevelID != educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion

                    #region // Validate TH8 - provinceId == provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId == educationIdOld
                    if (provinceId == provinceIdOld && evaluationDevGroId != evaluationDevGroIdOld && educationId == educationIdOld)
                    {
                        var testCode = lstDB.Where(x => x.EvaluationDevelopmentCode.ToUpper() == evalCode.ToUpper()
                           && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID == educationId)
                           && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testCode != null)
                        {
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }

                        var testName = lstDB.Where(x => x.EvaluationDevelopmentName.ToUpper() == evalName.ToUpper()
                             && (x.ProvinceID == provinceId && x.EvaluationDevelopmentGroupID != evaluationDevGroId && x.EducationLevelID == educationId)
                             && x.EvaluationDevelopmentID != evalID).FirstOrDefault();
                        if (testName != null)
                        {
                            //string message = string.Format("Chỉ số {0} đã tồn tại.", evalName);
                            string message = string.Format("Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.");
                            return Json(new JsonMessage(message, "error"));
                        }
                    }
                    #endregion
                }

                bool _isActive = true;
                if ("on".Equals(frm["statusEdit"]))
                {
                    _isActive = false;
                }

                EvaluationDevelopment objInput = new EvaluationDevelopment()
                {
                    EvaluationDevelopmentID = evalID,
                    EvaluationDevelopmentCode = evalCode,
                    EvaluationDevelopmentName = evalName,
                    Description = description,
                    EducationLevelID = educationId,
                    EvaluationDevelopmentGroupID = evaluationDevGroId,
                    IsActive = _isActive,
                    ProvinceID = provinceId,
                    ModifiedDate = DateTime.Now
                };
                this.EvaluationDevelopmentBusiness.UpdateEval(objInput, lstDB);
                return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        #region EditOrderBy
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult EditOrderBy(FormCollection frm)
        {
            int provinceSTT = !string.IsNullOrEmpty(frm["provinceSTT"]) ? int.Parse(frm["provinceSTT"]) : 0;
            int educationSTT = !string.IsNullOrEmpty(frm["educationSTT"]) ? int.Parse(frm["educationSTT"]) : 0;
            int evaluationDevGroSTT = !string.IsNullOrEmpty(frm["evaluationDevGroIdSTT"]) ? int.Parse(frm["evaluationDevGroIdSTT"]) : 0;
            int statusSTT = !string.IsNullOrEmpty(frm["statusSTT"]) ? int.Parse(frm["statusSTT"]) : 0;
            string codeSTT = !string.IsNullOrEmpty(frm["codeSTT"]) ? frm["codeSTT"] : string.Empty;
            string nameSTT = !string.IsNullOrEmpty(frm["nameSTT"]) ? frm["nameSTT"] : string.Empty;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = educationSTT;
            SearchInfo["ProvinceID"] = provinceSTT;
            SearchInfo["StatusID"] = statusSTT;
            SearchInfo["EvaluationDevelopmentGroupID"] = evaluationDevGroSTT;
            SearchInfo["EvaluationDevelopmentCode"] = codeSTT;
            SearchInfo["EvaluationDevelopmentName"] = nameSTT;

            List<EvaluationDevelopment> lstDB = EvaluationDevelopmentBusiness.Search(SearchInfo).ToList();
            List<EvaluationDevelopment> lstUpdate = new List<EvaluationDevelopment>();
            EvaluationDevelopment objUpdate = null;
            int valOrder = 0;
            foreach (var item in lstDB)
            {
                valOrder = !string.IsNullOrEmpty(frm["orderBy_" + item.EvaluationDevelopmentID]) ? int.Parse(frm["orderBy_" + item.EvaluationDevelopmentID]) : 0;
                if (valOrder != 0)
                {
                    objUpdate = new EvaluationDevelopment()
                    {
                        EvaluationDevelopmentID = item.EvaluationDevelopmentID,
                        OrderID = valOrder,
                        ModifiedDate = DateTime.Now
                    };
                    lstUpdate.Add(objUpdate);
                }
            }

            if (lstUpdate.Count > 0)
            {
                this.EvaluationDevelopmentBusiness.UpdateOrderID(lstUpdate, lstDB);
                return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            if (id > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                EvaluationDevelopment objBD = EvaluationDevelopmentBusiness.Search(SearchInfo).Where(x => x.EvaluationDevelopmentID == id).FirstOrDefault();

                if (objBD != null)
                {
                    DeclareEvaluationIndex objTest = DeclareEvaluationIndexBusiness.All.Where(x => x.EvaluationDevelopmentID == objBD.EvaluationDevelopmentID).FirstOrDefault();
                    if (objTest != null)
                    {
                        return Json(new JsonMessage(Res.Get("Xóa không thành công. Chỉ số đánh giá đã được khai báo"), "error"));
                    }

                    this.EvaluationDevelopmentBusiness.DeleteEval(id);
                    return Json(new JsonMessage(Res.Get("Xóa thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveMultiEvaluationDevelopment(List<EvaluationDevelopmentFormViewModel> lstEvaluationDevelopmentVM,
            int provinceID, int educationID, int evaluationGroupID)
        {
            string messenger = string.Empty;
            if (evaluationGroupID <= 0)
            {
                messenger = "Lưu không thành công. Lĩnh vực không hợp lệ!";
                return Json(new JsonMessage(Res.Get(messenger), "error"));
            }
            if (provinceID == 0)
            {
                messenger = "Lưu không thành công. Tỉnh/Thành không hợp lệ!";
                return Json(new JsonMessage(Res.Get(messenger), "error"));
            }

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (_globalInfo.ProvinceID.Value != provinceID)
                {
                    messenger = "Lưu không thành công. Tỉnh/Thành không hợp lệ!";
                    return Json(new JsonMessage(Res.Get(messenger), "error"));
                }
            }

            var lstEvaluationDevelopmentVMTempt = lstEvaluationDevelopmentVM;

            lstEvaluationDevelopmentVM = lstEvaluationDevelopmentVM.Where(p => p.isChecked.HasValue && p.isChecked.Value).ToList();
            if (lstEvaluationDevelopmentVM == null || lstEvaluationDevelopmentVM.Count() == 0)
            {
                messenger = "Lưu không thành công. Thầy/cô vui lòng chọn ít nhất một chỉ số!";
                return Json(new JsonMessage(Res.Get(messenger), "error"));
            }

            List<EvaluationDevelopment> lstInput = new List<EvaluationDevelopment>();
            EvaluationDevelopment objInput = null;
            EvaluationDevelopmentFormViewModel objVM = null;
            for (int i = 0; i < lstEvaluationDevelopmentVM.Count(); i++)
            {
                objVM = lstEvaluationDevelopmentVM[i];

                if (string.IsNullOrEmpty(objVM.EvaluationCode))
                {
                    messenger = "Mã chỉ số không được để trống!";
                    return Json(new JsonMessage(messenger, "error"));
                }

                if (string.IsNullOrEmpty(objVM.EvaluationName))
                {
                    messenger = "Nội dung chỉ số không được để trống!";
                    return Json(new JsonMessage(messenger, "error"));
                }

                objInput = new EvaluationDevelopment();
                objInput.EvaluationDevelopmentID = objVM.EvaluationDevelopmentID;
                objInput.EvaluationDevelopmentName = objVM.EvaluationName.Trim();
                objInput.EvaluationDevelopmentCode = objVM.EvaluationCode.Trim().ToUpper();
                objInput.Description = objVM.EvaluationName.Trim();
                objInput.EducationLevelID = educationID;
                objInput.IsActive = true;
                objInput.IsRequired = true;
                objInput.EvaluationDevelopmentGroupID = evaluationGroupID;
                objInput.ModifiedDate = DateTime.Now;
                objInput.OrderID = objVM.OrderID;
                objInput.ProvinceID = provinceID;
                lstInput.Add(objInput);
            }

            List<string> lstEvaluationCode = lstEvaluationDevelopmentVM.Select(x => x.EvaluationCode.Trim().ToUpper()).Distinct().ToList();
            List<string> lstEvaluationName = lstEvaluationDevelopmentVM.Select(x => x.EvaluationName.Trim().ToUpper()).Distinct().ToList();

            if (lstEvaluationCode.Count() < lstEvaluationDevelopmentVM.Count())
            {
                messenger = "Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì ký hiệu không được trùng nhau.";
                return Json(new JsonMessage(messenger, "error"));
            }

            if (lstEvaluationName.Count() < lstEvaluationDevelopmentVM.Count())
            {
                messenger = "Trong một Tỉnh/Thành các chỉ số cùng nhóm lớp, cùng lĩnh vực đánh giá thì tên chỉ số không được trùng nhau.";
                return Json(new JsonMessage(messenger, "error"));
            }
         
            List<int> lstEvaluationDevID = new List<int>();
            lstEvaluationDevID = lstEvaluationDevelopmentVM.Where(x => x.EvaluationDevelopmentID != 0).Select(x => x.EvaluationDevelopmentID).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("lstEvaluationDevelopmentID", lstEvaluationDevID);
            dic.Add("EducationLevelID", educationID);
            dic.Add("EvaluationDevelopmentGroupID", evaluationGroupID);
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).ToList();

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();       
            }
            else
            {
                if (provinceID == SystemParamsInFile.COUNTRY)
                {
                    lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == SystemParamsInFile.COUNTRY).ToList(); 
                }
                else
                {
                    lstEvaluationDB = lstEvaluationDB.Where(x => x.ProvinceID == provinceID).ToList(); 
                }
                       
            }

            List<EvaluationDevelopmentFormViewModel> lstUnCheckEvaluationDev = lstEvaluationDevelopmentVMTempt.Where(x => !x.isChecked.HasValue
                                                                                                                       || x.isChecked.Value == false).ToList();
            List<int> lstUncheckEvaluationDevID = lstUnCheckEvaluationDev.Select(x => x.EvaluationDevelopmentID).ToList();
            if (lstUncheckEvaluationDevID.Count() > 0)
            {
                List<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All.Where(x => lstUncheckEvaluationDevID.Contains(x.EvaluationDevelopmentID)
                                                                                                               && x.EvaluationDevelopmentGroID == evaluationGroupID
                                                                                                               && x.EducationLevelID == educationID
                                                                                                               && x.ClassID.HasValue == false).ToList();

                if (lstDeclareEvaluationIndex.Count() > 0)
                {
                    messenger = "Lưu không thành công. Các chỉ số {0} đã được khai báo!";

                    List<string> lstUncheckEvaluationCode = lstDeclareEvaluationIndex.Join(lstEvaluationDB,
                                                                                    x => x.EvaluationDevelopmentID,
                                                                                    y => y.EvaluationDevelopmentID,
                                                                                    (x, y) => new { X = x, Y = y })
                                                                                    .Select(o => o.Y.EvaluationDevelopmentCode).Distinct().ToList();
                    string strCode = "";
                    for (int i = 0; i < lstUncheckEvaluationCode.Count(); i++)
                    {
                        if (i < (lstUncheckEvaluationCode.Count() - 1))
                        {
                            strCode += lstUncheckEvaluationCode[i] + ", ";
                        }
                        if (i == (lstUncheckEvaluationCode.Count() - 1))
                        {
                            strCode += lstUncheckEvaluationCode[i];
                        }
                    }
                    messenger = string.Format(messenger, strCode);
                    return Json(new JsonMessage(Res.Get(messenger), "error"));
                }
            }
            EvaluationDevelopmentBusiness.InsertOrUpdateMulti(lstInput, lstEvaluationDB, lstEvaluationDevID, lstUncheckEvaluationDevID);
            return Json(new JsonMessage("Lưu thành công", "success"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult DeleteMultiEvaluationDevelopment(string ArrayEvaluationDevID, int provinceID, int educationID, int evaluationGroupID)
        {
            List<int> lstEvaluationDevID = ArrayEvaluationDevID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            string messenger = string.Empty;

            if (lstEvaluationDevID.Count() == 0)
            {
                messenger = "Xóa thành công";
                return Json(new JsonMessage(Res.Get(messenger), "success"));
            }
          
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                if (_globalInfo.ProvinceID.Value != provinceID)
                {
                    messenger = "Xóa không thành công. Tỉnh/Thành không hợp lệ!";
                    return Json(new JsonMessage(Res.Get(messenger), "error"));
                }
            }

            if (evaluationGroupID <= 0)
            {
                messenger = "Xóa không thành công. Lĩnh vực không hợp lệ!";
                return Json(new JsonMessage(Res.Get(messenger), "error"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("lstEvaluationDevelopmentID", lstEvaluationDevID);
            dic.Add("EducationLevelID", educationID);
            dic.Add("EvaluationDevelopmentGroupID", evaluationGroupID);
            List<EvaluationDevelopment> lstEvaluationDB = EvaluationDevelopmentBusiness.Search(dic).ToList();
            lstEvaluationDevID = lstEvaluationDB.Select(x => x.EvaluationDevelopmentID).ToList();

            List<DeclareEvaluationIndex> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.All.Where(x => lstEvaluationDevID.Contains(x.EvaluationDevelopmentID)
                                                                                                                && x.EvaluationDevelopmentGroID == evaluationGroupID).ToList();

            if (lstDeclareEvaluationIndex.Count() > 0)
            {
                messenger = "Xóa không thành công. Các chỉ số {0} đã được khai báo!";

                List<string> lstEvaluationCode = lstDeclareEvaluationIndex.Join(lstEvaluationDB,
                                                                                x => x.EvaluationDevelopmentID,
                                                                                y => y.EvaluationDevelopmentID,
                                                                                (x, y) => new { X = x, Y = y })
                                                                                .Select(o => o.Y.EvaluationDevelopmentCode).Distinct().ToList();
                string strCode = "";
                for (int i = 0; i < lstEvaluationCode.Count(); i++)
                {
                    if (i < (lstEvaluationCode.Count() - 1))
                    {
                        strCode += lstEvaluationCode[i] + ", ";
                    }
                    if (i == (lstEvaluationCode.Count() - 1))
                    {
                        strCode += lstEvaluationCode[i];
                    }
                }
                messenger = string.Format(messenger, strCode);
                return Json(new JsonMessage(Res.Get(messenger), "error"));
            }
            messenger = "Xóa thành công";
            EvaluationDevelopmentBusiness.DeleteAll(lstEvaluationDB);
            EvaluationDevelopmentBusiness.Save();
            return Json(new JsonMessage(messenger, "success"));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CheckLock(int provinceID, int valCheck)
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                provinceID = _globalInfo.ProvinceID.Value;
            }

            bool IsLocked = (valCheck == 0 ? false : true);
            EvaluationDevelopmentGroupBusiness.UpdateLock(provinceID, IsLocked);

            if (!IsLocked) // Lock
            {
                return Json(new JsonMessage(Res.Get("Nhà trường không được khai báo thêm chỉ số"), "success"));
            }
            else // UnLock
            {
                return Json(new JsonMessage(Res.Get("Nhà trường được khai báo thêm chỉ số"), "success"));
            }
        }

        #region // Get Data
        private void SetViewData()
        {
            this.GetProvince(EvaluationDevelopmentOfAdminConstants.LISTPROVINCE);
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel(), "EducationLevelID", "Resolution");
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_EVALUATION_DEVELOPMENT_GROUP] = new SelectList(ListEvaluationDevelopmentGroup(), "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");

            List<StatusViewModel> lstStatus = new List<StatusViewModel>();
            lstStatus.Add(new StatusViewModel { StatusID = 1, StatusName = "Đang hoạt động" });
            lstStatus.Add(new StatusViewModel { StatusID = 2, StatusName = "Tạm ngưng" });
            ViewData[EvaluationDevelopmentOfAdminConstants.LIST_STATUS] = new SelectList(lstStatus, "StatusID", "StatusName");
        }

        private void GetProvince(string strConstant)
        {
            List<Province> lsProvince = ListProvince();
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                int province = _globalInfo.ProvinceID.Value;
                lsProvince = lsProvince.Where(x => x.ProvinceID == province).ToList();

                ViewData[strConstant] = new SelectList(lsProvince, "ProvinceID", "ProvinceName", province);
            } // admin
            else
            {
                List<ProvinceViewModel> lstProvinceNew = new List<ProvinceViewModel>();
                lstProvinceNew.Add(new ProvinceViewModel { ProvinceID = SystemParamsInFile.COUNTRY, ProvinceName = SystemParamsInFile.COUNTRY_NAME, OrderBy = 1 });
                for (int i = 0; i < lsProvince.Count(); i++)
                {
                    lstProvinceNew.Add(new ProvinceViewModel { ProvinceID = lsProvince[i].ProvinceID, ProvinceName = lsProvince[i].ProvinceName, OrderBy = (i + 2) });
                }
                lstProvinceNew = lstProvinceNew.OrderBy(x => x.OrderBy).ToList();
                ViewData[strConstant] = new SelectList(lstProvinceNew, "ProvinceID", "ProvinceName");
            }
        }

        private List<Province> ListProvince()
        {
            return ProvinceBusiness.Search(new Dictionary<string, object>() { })
               .OrderBy(o => o.ProvinceName).ToList();
        }

        private List<EducationLevel> ListEducationLevel()
        {
            return EducationLevelBusiness.All.Where(x => (x.Grade == 4 || x.Grade == 5) && x.IsActive == true)
                                            .OrderBy(x => x.EducationLevelID).ToList();
        }

        private List<EvaluationDevelopmentGroup> ListEvaluationDevelopmentGroup()
        {
            return EvaluationDevelopmentGroupBusiness.All.ToList();
        }
        #endregion
    }
}
