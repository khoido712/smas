﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models
{
    public class EvaluationDevelopmentExcelViewModel
    {
        public string EvaluationGroupName { get; set; }
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public int? OrderEvaluationGroup { get; set; }
        public int? OrderEvaluation { get; set; }
        public int EducationLevelID { get; set; }
    }
}