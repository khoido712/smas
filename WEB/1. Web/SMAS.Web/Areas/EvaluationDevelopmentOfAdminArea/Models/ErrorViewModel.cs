﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models
{
    public class ErrorViewModel
    {
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public string EvaluationNameGroup { get; set; }
        public string SheetName { get; set; }
        public string Description { get; set; }
    }

    public class IndexEvaluationCodeViewModel
    {
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public string EvaluationGroupName { get; set; }
        public int EducationLevel { get; set; }
    }
}