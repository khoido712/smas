﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models
{
    public class EvaluationDevelopmentOfAdminViewModel
    {
        public int? ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int EvaluationID { get; set; }
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public int EducationID { get; set; }
        public string EducationName { get; set; }
        public string Description { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public int? OrderBy { get; set; }
        public int EvaluationGroupID { get; set; }
        public string EvaluationGroupName { get; set; }

        public bool ShowOrderBy { get; set; }
    }
}