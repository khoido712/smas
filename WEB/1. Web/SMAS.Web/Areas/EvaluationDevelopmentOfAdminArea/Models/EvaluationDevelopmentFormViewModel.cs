﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models
{
    public class EvaluationDevelopmentFormViewModel
    {
        public int EvaluationDevelopmentID { get; set; }
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public bool IsEnabled { get; set; }
        public int? OrderID { get; set; }
        public bool? isChecked { get; set; }
    }
}