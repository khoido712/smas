﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentOfAdminArea.Models
{
    public class ProvinceViewModel
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int OrderBy { get; set; }
    }
}