﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DivisiveConfiguarationArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.DivisiveConfiguarationArea.Controllers
{
    public class DivisiveConfiguarationController : BaseController
    {
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IAssignSubjectConfigBusiness AssignSubjectConfigBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IDistributeProgramBusiness DistributeProgramBusiness;

        public DivisiveConfiguarationController(
            ISchoolProfileBusiness SchoolProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            IAssignSubjectConfigBusiness AssignSubjectConfigBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IDistributeProgramBusiness DistributeProgramBusiness)
        {
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.AssignSubjectConfigBusiness = AssignSubjectConfigBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistributeProgramBusiness = DistributeProgramBusiness;
        }

        //
        // GET: /DivisiveConfiguarationArea/DivisiveConfiguaration/

        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[DivisiveConfiguarationContants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = ListSubject();
            Session[DivisiveConfiguarationContants.LIST_SUBJECT] = lstSubject;
            ViewData[DivisiveConfiguarationContants.LIST_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "DisplayName");

            return View();
        }

        private List<SubjectCatBO> ListSubject()
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = (from sc in SchoolSubjectBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                  && x.AcademicYearID == _globalInfo.AcademicYearID.Value)
                          join s in SubjectCatBusiness.All.OrderBy(x => x.OrderInSubject)
                                on sc.SubjectID equals s.SubjectCatID
                              where s.IsActive == true
                              && s.AppliedLevel == _globalInfo.AppliedLevel.Value
                              select new SubjectCatBO()
                              {
                                  SubjectCatID = s.SubjectCatID,
                                  DisplayName = s.DisplayName,
                                  OrderInSubject = s.OrderInSubject
                              }).Distinct().ToList();
            lstSubject = lstSubject.OrderBy(x => x.OrderInSubject).ToList();
            return lstSubject;
        }

        private List<SubjectCatBO> ListSubjectWhenSelectEdu(int eduId)
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = (from sc in SchoolSubjectBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                  && x.AcademicYearID == _globalInfo.AcademicYearID.Value)
                          join s in SubjectCatBusiness.All.OrderBy(x=>x.OrderInSubject)
                            on sc.SubjectID equals s.SubjectCatID
                          where s.IsActive == true
                          && s.AppliedLevel == _globalInfo.AppliedLevel.Value
                          && sc.EducationLevelID == eduId
                          select new SubjectCatBO()
                          {
                              SubjectCatID = s.SubjectCatID,
                              DisplayName = s.DisplayName,
                              OrderInSubject = s.OrderInSubject,
                              EducationLevel = sc.EducationLevelID.Value
                          }).Distinct().ToList();
            lstSubject = lstSubject.OrderBy(x => x.OrderInSubject).ToList();
            return lstSubject;
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadSubject(int eduId)
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();

            if (eduId <= 0)
            {
                lstSubject = ListSubject();
                return Json(lstSubject);
            } 
                     
            lstSubject = ListSubjectWhenSelectEdu(eduId);
            return Json(lstSubject);
        }

        public PartialViewResult GetInfoSubjectAndEducation()
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = ListSubject();
            ViewData["lstSubjectCre"] = new SelectList(lstSubject, "SubjectCatID", "DisplayName");

            List<EducationLevelBO> lstEdu = new List<EducationLevelBO>();
            lstEdu = (from e in _globalInfo.EducationLevels
                      select new EducationLevelBO
                      {
                        EducationLevelID = e.EducationLevelID,
                        DisplayName = e.Resolution
                     }).ToList();

            ViewData["lstEducationCre"] = lstEdu;          

            return PartialView("_CreateDivisive");
        }

        public PartialViewResult GetInfoDivisiveConfig(int assignSubjectId)
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = ListSubject();
            ViewData["lstSubjectEdit"] = new SelectList(lstSubject, "SubjectCatID", "DisplayName");

            List<EducationLevelBO> lstEdu = new List<EducationLevelBO>();
            lstEdu = (from e in _globalInfo.EducationLevels
                      select new EducationLevelBO
                      {
                          EducationLevelID = e.EducationLevelID,
                          DisplayName = e.Resolution
                      }).ToList();

            ViewData["lstEducationEdit"] = lstEdu;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["AssignSubjectConfigID"] = assignSubjectId;

            List<DivisiveConfiguarationViewModel> lst = this._Search(SearchInfo).ToList();

            string divisiveSubject = string.Empty;
            List<int> lstEducationId = new List<int>(); ;
            foreach (var item in lst)
            {
                divisiveSubject = item.DivisiveSubject;
                lstEducationId.Add(item.EducationLevelID);
            }

            ViewData["lstEducationId"] = lstEducationId;
            return PartialView("_Edit");
        }

        public JsonResult GetInfo(int assignSubjectId)
        {           
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
            SearchInfo["AssignSubjectConfigID"] = assignSubjectId;

            DivisiveConfiguarationViewModel lst = this._Search(SearchInfo).FirstOrDefault();
            string divisiveSubject = string.Empty;
            int subjectId = 0;
            List<int> lstEducationId = new List<int>();
            if (lst != null)
            {
                subjectId = lst.SubjectID;
                divisiveSubject = lst.DivisiveSubject;
                string educationApplied = lst.EducationApplied.Replace(" ", "");
                List<int> lstEducationID = educationApplied.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                foreach (var item in lstEducationID)
                {
                    lstEducationId.Add(item);
                }
            }

            List<SubjectCatBO> lstEducationIdBD = ListSubject2(subjectId);

            return Json(new
            {
                lstEducationId = lstEducationId,
                divisiveSubject = divisiveSubject, 
                subjectId = subjectId,
                lstEducationIdBD = lstEducationIdBD
            });
        }


        private List<SubjectCatBO> ListSubject2(int subjectId)
        {
            List<SubjectCatBO> lstSubject = new List<SubjectCatBO>();
            lstSubject = (from sc in SchoolSubjectBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                  && x.AcademicYearID == _globalInfo.AcademicYearID.Value)
                          join s in SubjectCatBusiness.All
                            on sc.SubjectID equals s.SubjectCatID
                          where s.IsActive == true
                          && s.AppliedLevel == _globalInfo.AppliedLevel.Value
                          && s.SubjectCatID == subjectId
                          && sc.SubjectID == subjectId
                          select new SubjectCatBO()
                          {
                              EducationLevel = sc.EducationLevelID.Value
                          }).Distinct().ToList();

            return lstSubject;
        }

        public JsonResult GetEducationIdBySubjectID(int subjectId)
        {
            List<SubjectCatBO> lstEduInSubject = ListSubject2(subjectId);
            return Json(new { lstEduInSubject = lstEduInSubject });
        }

        public PartialViewResult Search(int? eduId, int? subjectId)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            
            //add search info          
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;

            if (eduId > 0)
                SearchInfo["EducationLevelID"] = eduId.Value;
            if(subjectId > 0)
                SearchInfo["SubjectID"] = subjectId.Value;

            // Page 1
            List<DivisiveConfiguarationViewModel> lst = this._Search(SearchInfo).ToList();
            Session[DivisiveConfiguarationContants.LIST_RESULT] = lst;
            List<DivisiveConfiguarationViewModel> lstPage1 = lst.Skip(0).Take(Config.PageSize).ToList();
            ViewData[DivisiveConfiguarationContants.TOTAL] = lst.Count;
            ViewData[DivisiveConfiguarationContants.PAGE_NUMBER] = 1;
            ViewData[DivisiveConfiguarationContants.LIST_RESULT] = lstPage1;
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DivisiveConfiguarationViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            this.SetViewDataPermission("DivisiveConfiguaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            List<AssignSubjectConfig> lstDB = this.AssignSubjectConfigBusiness.GetList(SearchInfo);

            List<SubjectCatBO> lstSubject = ListSubject();

            List<DivisiveConfiguarationViewModel> lstResult = new List<DivisiveConfiguarationViewModel>();
            DivisiveConfiguarationViewModel objResult = null;
            List<DivisiveConfiguarationViewModel> _lstVM = new List<DivisiveConfiguarationViewModel>();
            List<ListEducationLevel> lstEdu = new List<ListEducationLevel>();
            //ListEducationLevel objEdu = null;
            string educationName = string.Empty;
            foreach (var item in lstSubject)
            {
                _lstVM = lstDB.Where(x => x.SubjectID == item.SubjectCatID
                    && x.SchoolID == _globalInfo.SchoolID.Value
                    && x.AcademicYearID == _globalInfo.AcademicYearID)
                    .Select((x) => new DivisiveConfiguarationViewModel
                    {
                        SubjectID = x.SubjectID,
                        DivisiveSubject = x.AssignSubjectName,
                        EducationApplied = x.EducationApplied,
                        DivisiveConfiguarationID = x.AssignSubjectConfigID,
                    }).ToList();

                if (_lstVM != null && _lstVM.Count > 0)
                {
                    foreach (var itemVM in _lstVM)
                    {
                        objResult = new DivisiveConfiguarationViewModel();
                        objResult.SubjectID = item.SubjectCatID;
                        objResult.SubjectName = item.DisplayName;
                        objResult.DivisiveSubject = itemVM.DivisiveSubject;
                        objResult.EducationApplied = itemVM.EducationApplied;
                        objResult.DivisiveConfiguarationID = itemVM.DivisiveConfiguarationID;

                        objResult.perView = (bool)ViewData[SystemParamsInFile.PERMISSION_VIEW];
                        objResult.perCreate = (bool)ViewData[SystemParamsInFile.PERMISSION_CREATE];
                        objResult.perUpdate = (bool)ViewData[SystemParamsInFile.PERMISSION_UPDATE];
                        objResult.perDelete = (bool)ViewData[SystemParamsInFile.PERMISSION_DELETE];
                        lstResult.Add(objResult);
                    }
                }  
            }         
            return lstResult;
        }

        #region SaveDivisiveSubjectConfig
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveDivisiveSubjectConfig(FormCollection frm)
        {
            int subjectId = !string.IsNullOrEmpty(frm["SubjectID_Create"]) ? int.Parse(frm["SubjectID_Create"]) : 0;
            string arrEducationID = !string.IsNullOrEmpty(frm["ArrEducationIDSave"]) ? frm["ArrEducationIDSave"]: string.Empty;
            string arrAssignSubjectName = !string.IsNullOrEmpty(frm["assignSubjectName"]) ? frm["assignSubjectName"] : string.Empty;

            arrAssignSubjectName = arrAssignSubjectName.Trim();
            string strPattern = @"[\s]+";
            Regex rgx = new Regex(strPattern);
            string arrOuput = rgx.Replace(arrAssignSubjectName, " ");

            if (!string.IsNullOrEmpty(arrEducationID)
                && !string.IsNullOrEmpty(arrOuput)
                && subjectId != 0)
            {           

                List<string> arrAssignSub = new List<string>();
                arrOuput = arrOuput.Replace(", ", ",");
                arrAssignSub = arrOuput.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();

                List<string> arrAssignSubApplied = new List<string>();
                foreach (var item in arrAssignSub)
                {
                    arrAssignSubApplied.Add(item);
                    if (arrAssignSubApplied.Where(p => p.ToUpper() == item.ToUpper()).Count() > 1)
                    {
                        string message = "Không thể thêm mới 2 phân môn trùng nhau";
                        return Json(new JsonMessage(message, "error"));
                    }
                }

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();      
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                SearchInfo["SubjectID"] = subjectId;
                List<AssignSubjectConfig> lstDB = this.AssignSubjectConfigBusiness.GetList(SearchInfo);
                List<AssignSubjectConfig> lstDBTest = new List<AssignSubjectConfig>();

                foreach (var item in arrAssignSubApplied)
                {
                    lstDBTest = lstDB.Where(x => x.AssignSubjectName.ToUpper() == item.ToUpper()).ToList();
                    if (lstDBTest.Count > 0)
                {
                        string message = string.Format("Phân môn {0} đã tồn tại", item);
                    return Json(new JsonMessage(message, "error"));
                }
                }

                List<AssignSubjectConfigBO> lstInput = new List<AssignSubjectConfigBO>();
               
                List<int> lstEducationID = arrEducationID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<int> lstEducationApplied = new List<int>();
                foreach (var item in lstEducationID)
                { 
                    foreach(EducationLevel item2 in new GlobalInfo().EducationLevels)
                    {
                        if (item == item2.EducationLevelID)
                        {
                            lstEducationApplied.Add(item);
                            break;
                        }
                    }
                }

                if ((lstEducationApplied != null && lstEducationApplied.Count > 0) && arrAssignSubApplied.Count > 0)
                {
                    lstEducationApplied = lstEducationApplied.OrderBy(x => x).ToList();
                    string EducationApplied = string.Empty;
                    for (int i = 0; i < lstEducationApplied.Count(); i++)
                    {
                        if (i < lstEducationApplied.Count - 1)
                        {
                            EducationApplied += lstEducationApplied[i] + ", ";
                        }
                        else
                        {
                            EducationApplied += lstEducationApplied[i];                           
                        }
                    }

                    List<AssignSubjectConfig> lstInsert = new List<AssignSubjectConfig>();
                    AssignSubjectConfig objInput = null;
                    foreach (var item in arrAssignSubApplied)
                    {
                        objInput = new AssignSubjectConfig()
                    {
                            AssignSubjectName = item,
                            SubjectID = subjectId,
                            EducationApplied = EducationApplied,
                            AcademicYearID = _globalInfo.AcademicYearID.Value,
                            SchoolID = _globalInfo.SchoolID.Value
                     };
                        lstInsert.Add(objInput);
                    }

                    this.AssignSubjectConfigBusiness.InsertAssignSubjectConfig(lstInsert);
                    return Json(new JsonMessage(Res.Get("Thêm mới thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));               
            }
            return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));  
        }
        #endregion

        #region UpdateDivisiveSubjectConfig
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateDivisiveSubjectConfig(FormCollection frm)
        {
            int subjectId = !string.IsNullOrEmpty(frm["SubjectID_Edit"]) ? int.Parse(frm["SubjectID_Edit"]) : 0;
            int subjectId_Old = !string.IsNullOrEmpty(frm["subjectId_Old"]) ? int.Parse(frm["subjectId_Old"]) : 0;
            string arrEducationID = !string.IsNullOrEmpty(frm["ArrEducationIdUpdate"]) ? frm["ArrEducationIdUpdate"] : string.Empty;
            string AssignSubjectName = !string.IsNullOrEmpty(frm["assignSubjectName_Update"]) ? frm["assignSubjectName_Update"] : string.Empty;
            int assignSubjectId = !string.IsNullOrEmpty(frm["asignSubjectId_Update"]) ? int.Parse(frm["asignSubjectId_Update"]) : 0;

            AssignSubjectName = AssignSubjectName.Trim();
            string strPattern = @"[\s]+";
            Regex rgx = new Regex(strPattern);
            string Ouput = rgx.Replace(AssignSubjectName, " ");

            if (!string.IsNullOrEmpty(arrEducationID)
                && !string.IsNullOrEmpty(Ouput)
                && subjectId != 0 && assignSubjectId != 0 && subjectId_Old != 0)
            {          
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                //SearchInfo["AssignSubjectName"] = Ouput;
                List<AssignSubjectConfig> lstDB = this.AssignSubjectConfigBusiness.GetList(SearchInfo)
                    .Where(x => x.AssignSubjectConfigID != assignSubjectId && x.AssignSubjectName.ToUpper() == Ouput.ToUpper()).ToList();
                if (lstDB.Count > 0)
                {
                    string message = string.Format("Phân môn {0} đã tồn tại", Ouput);
                     return Json(new JsonMessage(message, "error"));                                  
                }

                List<AssignSubjectConfigBO> lstInput = new List<AssignSubjectConfigBO>();
                List<int> lstEducationID = arrEducationID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<int> lstEducationApplied = new List<int>();
                foreach (var item in lstEducationID)
                {
                    foreach (EducationLevel item2 in new GlobalInfo().EducationLevels)
                    {
                        if (item == item2.EducationLevelID)
                        {
                            lstEducationApplied.Add(item);
                            break;
                        }
                    }
                }

                if (lstEducationApplied != null && lstEducationApplied.Count > 0)
                {
                    lstEducationApplied = lstEducationApplied.OrderBy(x => x).ToList();
                    string EducationApplied = string.Empty;
                    for (int i = 0; i < lstEducationApplied.Count(); i++)
                    {
                        if (i < lstEducationApplied.Count - 1)
                        {
                            EducationApplied += lstEducationApplied[i] + ", ";
                        }
                        else
                        {
                            EducationApplied += lstEducationApplied[i];
                        }
                    }

                    AssignSubjectConfigBO objInput = new AssignSubjectConfigBO()
                    {
                        AssignSubjectConfigID = assignSubjectId,
                        AssignSubjectName = Ouput,
                        SubjectID = subjectId,
                        EducationApplied = EducationApplied,
                        AcademicYearID = _globalInfo.AcademicYearID.Value,
                        SchoolID = _globalInfo.SchoolID.Value
                    };

                    this.AssignSubjectConfigBusiness.UpdateAssignSubjectConfig(objInput, SearchInfo);
                    return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int assignSubjectId)
        {
            if (assignSubjectId > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                AssignSubjectConfig objBD = this.AssignSubjectConfigBusiness.GetList(SearchInfo)
                    .Where(x => x.AssignSubjectConfigID == assignSubjectId).FirstOrDefault();

                if(objBD != null)
                {
                    DistributeProgram objDP = DistributeProgramBusiness.All
                        .Where(x => x.SchoolID == _globalInfo.SchoolID
                            && x.SubjectID == objBD.SubjectID && x.AcademicYearID == _globalInfo.AcademicYearID
                            && x.AssignSubjectID == objBD.AssignSubjectConfigID).FirstOrDefault();

                    if (objDP != null)
                    {
                        return Json(new JsonMessage(Res.Get("Phân môn đã được sử dụng"), "error"));
                    }

                    this.AssignSubjectConfigBusiness.Delete(objBD.AssignSubjectConfigID);
                    this.AssignSubjectConfigBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Xóa thành công"), "success"));
                }

                return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
             
            }
            return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
        }

        /// <summary>
        /// Tìm kiếm có phân trang
        /// </summary>
        /// 
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(GridCommand command)
        {
            int Page = 1;
            if (command != null) Page = command.Page;

            string Member = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].Member : "";
            string SortDirection = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].SortDirection.ToString() : "Descending";
            IEnumerable<DivisiveConfiguarationViewModel> lst;
            if (Session[DivisiveConfiguarationContants.LIST_RESULT] != null)
            {
                lst = (IEnumerable<DivisiveConfiguarationViewModel>)Session[DivisiveConfiguarationContants.LIST_RESULT];
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();         
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID.Value;
                lst = this._Search(SearchInfo);
            }

            /*if (command.SortDescriptors.Count > 0 && command.SortDescriptors != null)
            {
                lst = this.SortGrid(lst, Member, SortDirection);
            }*/

            List<DivisiveConfiguarationViewModel> lstAtCurrentPage = lst.Skip((Page - 1) * Config.PageSize).Take(Config.PageSize).ToList();
            ViewData[DivisiveConfiguarationContants.LIST_RESULT] = lstAtCurrentPage;
            GridModel<DivisiveConfiguarationViewModel> gm = new GridModel<DivisiveConfiguarationViewModel>(lstAtCurrentPage);
            gm.Total = lst.Count();
            return View(gm);
        }

    }
}
