﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DivisiveConfiguarationArea
{
    public class DivisiveConfiguarationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DivisiveConfiguarationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DivisiveConfiguarationArea_default",
                "DivisiveConfiguarationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
