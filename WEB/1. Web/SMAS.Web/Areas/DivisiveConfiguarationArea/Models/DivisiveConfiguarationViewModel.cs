﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DivisiveConfiguarationArea.Models
{
    public class DivisiveConfiguarationViewModel
    {
        public int DivisiveConfiguarationID { get; set; }
        public string DivisiveSubject { get; set; }
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public string EducationApply { get; set; }
        public string EducationApplied { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }

        public List<ListEducationLevel> ListEducationID { get; set; }

        public bool perView { get; set; }
        public bool perCreate { get; set; }
        public bool perUpdate { get; set; }
        public bool perDelete { get; set; }
    }

    public class ListEducationLevel
    {
        public int EducationLevel { get; set; }
    }
}