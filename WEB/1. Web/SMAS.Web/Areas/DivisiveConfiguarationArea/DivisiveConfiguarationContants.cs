﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DivisiveConfiguarationArea
{
    public class DivisiveConfiguarationContants
    {
        public const string LIST_EDUCATIONLEVEL = "listEducation";
        public const string LIST_SUBJECT = "listSubject";
        public const string TOTAL = "total";
        public const string PAGE_NUMBER = "position of page";
        public const string LIST_RESULT = "listResult";
        public const string FORBACK = "FORBACK";
    }
}