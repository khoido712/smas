﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DeactiveSMSTeacherContractArea
{
    public class DeactiveSMSTeacherContractAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeactiveSMSTeacherContractArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeactiveSMSTeacherContractArea_default",
                "DeactiveSMSTeacherContractArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
