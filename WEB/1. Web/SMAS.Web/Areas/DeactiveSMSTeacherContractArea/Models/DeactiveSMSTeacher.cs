﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeactiveSMSTeacherContractArea.Models
{
    public class DeactiveSMSTeacher
    {
        public string NumberAccount { get; set; }
        public string Cause { get; set; }
        public bool Status { get; set; }
        public string Error { get; set; }
            

    }
}