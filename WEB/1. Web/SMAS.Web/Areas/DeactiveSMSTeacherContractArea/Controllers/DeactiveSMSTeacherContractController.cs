﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.MarkRecordArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.MarkRecordArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.DeactiveSMSTeacherContractArea.Models;
namespace SMAS.Web.Areas.DeactiveSMSTeacherContractArea.Controllers
{
    public class DeactiveSMSTeacherContractController : BaseController
    {
        //
        // GET: /DeactiveSMSTeacherContractArea/DeactiveSMSTeacherContract/

        public ActionResult Index()
        {
            return View();
        }
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        public DeactiveSMSTeacherContractController(IReportDefinitionBusiness ReportDefinitionBusiness)
        {
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
        }


        public FileResult ExportFile()
        {
            string reportCode = "";
            reportCode = SystemParamsInFile.SMS_DSTHUEBAOCHANCATKHONGTHANHCONG;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = reportDef.OutputNamePattern + "." + reportDef.OutputFormat;
            result.FileDownloadName = ReportName;

            return result;
        }

        public int DEACTIVED_ERROR = -61;
        public int WAIT_ACTIVED_ERROR = -62;
        public int COMIT_ERROR = -1;


        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments)
        {
            string error = "";
            GlobalInfo global = new GlobalInfo();
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;


                string Error = "";
                List<DeactiveSMSTeacher> listExcel = LoadDataFileExcel(fileName, out Error);
                int cntRecordTrue = 0;
                List<DeactiveSMSTeacher> listRecordErrod = new List<DeactiveSMSTeacher>();
                if (Error.Trim().Length == 0)
                {

                    //lay danh sach va goi service add cac tai khoan
                    SMASEdu.BCCSService service = new SMASEdu.BCCSService();
                    string authen = "";
                    SMASEdu.SMSTeacherContractObj SMSTeacherContractObj = new SMASEdu.SMSTeacherContractObj();

                    SMASEdu.SMSTeacherContractResult SMSTeacherContractResult = service.GetSMSTeacherContract(authen, SMSTeacherContractObj);

                    SMASEdu.SMSTeacherContractObj[] listResult = SMSTeacherContractResult.Result;


                    foreach (var TeacherSMS in listExcel)
                    {
                        DeactiveSMSTeacher DeactiveSMSTeacher = new DeactiveSMSTeacher();
                        var listResultTeacher =  listResult.Where(o=>o.SchoolCode==TeacherSMS.NumberAccount|| o.PackageCode==TeacherSMS.NumberAccount);
                        if (listResultTeacher!=null && listResultTeacher.Count()>0){
                            SMSTeacherContractObj = listResultTeacher.FirstOrDefault();
                            SMASEdu.MessageResult ressult = service.UpdateSMSTeacherContract(authen,SMSTeacherContractObj,SystemParamsInFile.DEACTIVE_SMSTEACHER_CONTRACT);
                            if (ressult.Result=="1")
                            {
                                cntRecordTrue++;
                            }else
                            {
                                

                                if (ressult.Result == DEACTIVED_ERROR.ToString())
                                {
                                    DeactiveSMSTeacher.NumberAccount = TeacherSMS.NumberAccount;
                                    DeactiveSMSTeacher.Error = "Thuê bao đã bị chặn cắt ngày "+ ressult.ResultDescription;
                                }
                                else if (ressult.Result == COMIT_ERROR.ToString())
                                {
                                    DeactiveSMSTeacher.NumberAccount = TeacherSMS.NumberAccount;
                                    DeactiveSMSTeacher.Error = "Lỗi trong quá trình cập nhật dữ liệu " + ressult.ResultDescription;
                                }
                                else if (ressult.Result == WAIT_ACTIVED_ERROR.ToString())
                                {
                                    DeactiveSMSTeacher.NumberAccount = TeacherSMS.NumberAccount;
                                    DeactiveSMSTeacher.Error = "Thuê bao chưa được kích hoạt " + ressult.ResultDescription;
                                }
                            }

                        }else
                        {
                            DeactiveSMSTeacher.NumberAccount = TeacherSMS.NumberAccount;
                            DeactiveSMSTeacher.Error="Thuê bao không tồn tại trong hệ thống";
                        }
                        listRecordErrod.Add(DeactiveSMSTeacher);
                    }



                    //SMSTeacherContractObj.ContractCode = 
                    //service.UpdateSMSTeacherContract(authen,

                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
                else if (Error.Trim().Length > 0)
                {
                    return Json(new { success = Error });
                }
            }

            error = "";

            return Json(new { success = error });
        }

        public int rowStart = 5;
        public int colNumber = 2;
        public int colCause = 3;
        //fix cung dong chay vi van cho dong chong trong file excel
        public int numberRowLoad = 100;
        /// <summary>
        /// LoadDataFileExcel
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        /// <param name="Error">The error.</param>
        /// <returns>
        /// List{DeactiveSMSTeacher}
        /// </returns>
        ///<author>NamTA</author>
        ///<date>3/20/2013</date>
        public List<DeactiveSMSTeacher> LoadDataFileExcel(string FileName, out string Error)
        {
            List<DeactiveSMSTeacher> listDeactiveSMSTeacher = new List<DeactiveSMSTeacher>();
            Error = "";
            IVTWorkbook oBook = VTExport.OpenWorkbook(FileName);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);

            //KIEM TRA FILE EXCEL CO DUNG DINH DANG KHONG

            string nameReport = (string)sheet.GetCellValue("A2");

            if (nameReport != null && nameReport.Equals("DANH SÁCH THUÊ BAO CHẶN CẮT TIN NHẮN SMS TEACHER"))
            {
                for (int row = rowStart; row <= numberRowLoad; row++)
                {
                    DeactiveSMSTeacher DeactiveSMSTeacher = new DeactiveSMSTeacher();
                    string valueNumber = (string)sheet.GetCellValue(rowStart, colNumber);
                    if (valueNumber == null || valueNumber == "")
                        continue;
                    else
                    {
                        DeactiveSMSTeacher.NumberAccount = valueNumber;
                        DeactiveSMSTeacher.Cause = (string)sheet.GetCellValue(rowStart, colCause);
                        DeactiveSMSTeacher.Status = true;
                    }
                    listDeactiveSMSTeacher.Add(DeactiveSMSTeacher);
                }
                if (listDeactiveSMSTeacher.Count == 0)
                {
                    Error = "Trong file excel chưa có thuê bao để chặn cắt";
                }
            }
            else
            {
                Error = "Trong file excel không chưa thuê bao chặn cắt";
            }


            return listDeactiveSMSTeacher;
        }




    }
}
