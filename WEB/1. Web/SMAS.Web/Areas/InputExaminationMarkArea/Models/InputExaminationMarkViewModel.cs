﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.InputExaminationMarkArea.Models
{
    public class InputExaminationMarkViewModel
    {
        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InputExaminationMarkConstants.LIST_EXAMINATION)]
        public int ExaminationID { get; set; }

        [ResourceDisplayName("EducationLevel_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InputExaminationMarkConstants.LIST_EDUCATION_LEVEL)]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InputExaminationMarkConstants.LIST_EXAMINATION_SUBJECT)]
        public int ExaminationSubjectID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_BagTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", InputExaminationMarkConstants.LIST_DETACHABLE_HEAD_BAG)]
        public int DetachableHeadBagID { get; set; }
    }
}