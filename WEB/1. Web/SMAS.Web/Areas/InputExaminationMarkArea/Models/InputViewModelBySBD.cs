﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.InputExaminationMarkArea.Models
{
    public class InputViewModelBySBD
    {
       
        public string ErrorReason { get; set; }
        
        public string NamedListCode { get; set; }
        public int? NamedListNumber { get; set; }
        public string ExamMark { get; set; }
        
    }
}