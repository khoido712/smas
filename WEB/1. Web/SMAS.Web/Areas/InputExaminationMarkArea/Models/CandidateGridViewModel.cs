using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.InputExaminationMarkArea.Models
{
    public class CandidateGridViewModel
    {
        public int CandidateID { get; set; }
        public DateTime? BirthDate { get; set; }
        public int PupilID { get; set; }
        public decimal? Mark { get; set; }
        public string Judgement { get; set; }
        public string ExamMark { get; set; }
        public string RealMark { get; set; }
        public int? PenaltyMarkPercent { get; set; }
        public string DetachableHeadNumber { get; set; }
        public int DetachableHeadBagID { get; set; }
        public string ErrorReason { get; set; }
        public string Description { get; set; }
        public string PupilName { get; set; }
        public string PupilCode { get; set; }
        public string NamedListCode { get; set; }
        public int? RoomID { get; set; }
        public bool IsCheck { get; set; }
        public bool IsDisable { get; set; }
        public int? IsCommenting { get; set; }
        public int? NameListNumber { get; set; }
        public string RoomName { get; set; }
        public string DisplayMark { get; set; }
        public string Name { get; set; }
    }
}
