﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Web.Areas.InputExaminationMarkArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Controllers;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.InputExaminationMarkArea.Controllers
{
    public class InputExaminationMarkController : BaseController
    {
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IDetachableHeadBagBusiness DetachableHeadBagBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        public InputExaminationMarkController(IExaminationBusiness examinationBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
                                IExaminationSubjectBusiness examinationSubjectBusiness,
                                ICandidateBusiness candidateBusiness,
                                IDetachableHeadBagBusiness detachableHeadBagBusiness,
                                IExaminationRoomBusiness examinationRoomBusiness)
        {
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.CandidateBusiness = candidateBusiness;
            this.DetachableHeadBagBusiness = detachableHeadBagBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
        }


        public ActionResult Index()
        {
            GetViewData();

            return View();
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGridInput(int examinationID, int educationLevelID, int? examinationSubjectID, int? detachableHeadBagID)
        {
            Examination exam = ExaminationBusiness.Find(examinationID);
            GlobalInfo glo = new GlobalInfo();

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationID"] = examinationID;
            dic["EducationLevelID"] = educationLevelID;
            if (examinationSubjectID != null)
            {
                dic["ExaminationSubjectID"] = examinationSubjectID;

                ViewData[InputExaminationMarkConstants.INT_MARK_TYPE] = ExaminationSubjectBusiness.GetSubjectMarkType(examinationSubjectID.Value, glo.AppliedLevel);
            }
            ViewData[InputExaminationMarkConstants.IS_IMPORT_PANEL] = false;
            ViewData[InputExaminationMarkConstants.INT_EXAMINATION_SUBJECT_ID] = examinationSubjectID;
            if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
            {
                if (detachableHeadBagID.HasValue && detachableHeadBagID.Value > 0)
                {
                    dic.Add("DetachableHeadBagID", detachableHeadBagID);
                    List<CandidateGridViewModel> lstCandiateMark = CandidateBusiness.SearchMark(glo.SchoolID.Value, (int)glo.AppliedLevel.Value, dic)
                                                                            .Where(u => u.DetachableHeadBagID > 0)
                                                                            .Select(u => new CandidateGridViewModel
                                                                                        {
                                                                                            BirthDate = u.BirthDate,
                                                                                            CandidateID = u.CandidateID,
                                                                                            Description = u.Description,
                                                                                            DetachableHeadBagID = u.DetachableHeadBagID,
                                                                                            DetachableHeadNumber = u.DetachableHeadNumber,
                                                                                            ErrorReason = u.ErrorReason,
                                                                                            ExamMark = u.ExamMark,
                                                                                            IsCheck = u.IsCheck,
                                                                                            IsCommenting = u.IsCommenting,
                                                                                            IsDisable = u.IsDisable,
                                                                                            Judgement = u.Judgement,
                                                                                            Mark = u.Mark,
                                                                                            NamedListCode = u.NamedListCode,
                                                                                            PenaltyMarkPercent = u.PenaltyMarkPercent,
                                                                                            PupilCode = u.PupilCode,
                                                                                            PupilID = u.PupilID,
                                                                                            PupilName = u.PupilName,
                                                                                            RealMark = u.RealMark,
                                                                                            RoomID = u.RoomID,
                                                                                            RoomName = u.RoomName
                                                                                        })
                                                                            .ToList();
                    // Sap xep danh sach thi sinh theo so phach
                    int maxLength = lstCandiateMark.Count == 0 ? 0 : lstCandiateMark.Max(p => p.DetachableHeadNumber.Length); // Tim do dai so phach lon nhat
                    lstCandiateMark = lstCandiateMark.OrderBy(p => new string('0',maxLength - p.DetachableHeadNumber.Length) + p.DetachableHeadNumber).ToList(); // Sap xep theo so phach duoc mo rong voi 0

                    //for (int i = 0, size = lstCandiateMark.Count; i < size; i++)
                    //    lstCandiateMark[i].Description.Replace("$MINUS$", Res.Get("Common_Label_Minus")).Replace("$BECAUSE$", Res.Get("Common_Label_Because"));
                    ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = lstCandiateMark;
                }
                return PartialView("_GridDetachableHead");
            }
            else
            {
                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                {
                    List<CandidateGridViewModel> lstCandiateMark = CandidateBusiness.SearchMark(glo.SchoolID.Value, (int)glo.AppliedLevel.Value, dic)
                        //.OrderBy(u => u.NamedListCode)
                                                                            .Select(u => new CandidateGridViewModel
                                                                                        {
                                                                                            BirthDate = u.BirthDate,
                                                                                            CandidateID = u.CandidateID,
                                                                                            Description = u.Description,
                                                                                            DetachableHeadBagID = u.DetachableHeadBagID,
                                                                                            DetachableHeadNumber = u.DetachableHeadNumber,
                                                                                            ErrorReason = u.ErrorReason,
                                                                                            ExamMark = u.ExamMark,
                                                                                            IsCheck = u.IsCheck,
                                                                                            IsCommenting = u.IsCommenting,
                                                                                            IsDisable = u.IsDisable,
                                                                                            Judgement = u.Judgement,
                                                                                            Mark = u.Mark,
                                                                                            NamedListCode = u.NamedListCode,
                                                                                            PenaltyMarkPercent = u.PenaltyMarkPercent,
                                                                                            PupilCode = u.PupilCode,
                                                                                            PupilID = u.PupilID,
                                                                                            PupilName = u.PupilName,
                                                                                            RealMark = u.RealMark,
                                                                                            RoomID = u.RoomID,
                                                                                            RoomName = u.RoomName,
                                                                                            NameListNumber = u.NamedListNumber
                                                                                        })
                                                                            .ToList();
                    //for (int i = 0, size = lstCandiateMark.Count; i < size; i++)
                    //    lstCandiateMark[i].Description.Replace("$MINUS$", Res.Get("Common_Label_Minus")).Replace("$BECAUSE$", Res.Get("Common_Label_Because"));
                    ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = lstCandiateMark.OrderBy(u => u.NameListNumber).ToList();
                    return PartialView("_GridOrderNumber");
                }
                else
                {
                    List<CandidateGridViewModel> lstCandiateMark = CandidateBusiness.SearchMark(glo.SchoolID.Value, (int)glo.AppliedLevel.Value, dic)
                                                                            .OrderBy(u => u.PupilCode) // Sap xep theo ma  - AnhVD 20140910
                                                                            .Select(u => new CandidateGridViewModel
                                                                                        {
                                                                                            BirthDate = u.BirthDate,
                                                                                            CandidateID = u.CandidateID,
                                                                                            Description = u.Description,
                                                                                            DetachableHeadBagID = u.DetachableHeadBagID,
                                                                                            DetachableHeadNumber = u.DetachableHeadNumber,
                                                                                            ErrorReason = u.ErrorReason,
                                                                                            ExamMark = u.ExamMark,
                                                                                            IsCheck = u.IsCheck,
                                                                                            IsCommenting = u.IsCommenting,
                                                                                            IsDisable = u.IsDisable,
                                                                                            Judgement = u.Judgement,
                                                                                            Mark = u.Mark,
                                                                                            NamedListCode = u.NamedListCode,
                                                                                            PenaltyMarkPercent = u.PenaltyMarkPercent,
                                                                                            PupilCode = u.PupilCode,
                                                                                            PupilID = u.PupilID,
                                                                                            PupilName = u.PupilName,
                                                                                            RealMark = u.RealMark,
                                                                                            RoomID = u.RoomID,
                                                                                            RoomName = u.RoomName,
                                                                                            Name = u.Name
                                                                                        })
                                                                            .ToList();
                    //for (int i = 0, size = lstCandiateMark.Count; i < size; i++)
                    //    lstCandiateMark[i].Description.Replace("$MINUS$", Res.Get("Common_Label_Minus")).Replace("$BECAUSE$", Res.Get("Common_Label_Because"));
                    ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = lstCandiateMark.OrderBy(u => u.PupilCode).ThenBy(u => u.Name).ThenBy(u => u.PupilName).ToList();
                    return PartialView("_GridInputPupilCode");
                }
            }
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGridImport(int? examinationSubjectID, int? detachableHeadBagID)
        {
            GlobalInfo glo = new GlobalInfo();
            ExaminationSubject examSubject = new ExaminationSubject();
            ViewData[InputExaminationMarkConstants.IS_IMPORT_PANEL] = true;
            if (examinationSubjectID != null)
            {
                examSubject = ExaminationSubjectBusiness.Find(examinationSubjectID);

                ViewData[InputExaminationMarkConstants.INT_MARK_TYPE] = ExaminationSubjectBusiness.GetSubjectMarkType(examinationSubjectID.Value, glo.AppliedLevel);

                ViewData[InputExaminationMarkConstants.INT_EXAMINATION_SUBJECT_ID] = examinationSubjectID;

                if (examSubject.Examination.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
                {
                    List<CandidateGridViewModel> lstCandiateMark = Session[InputExaminationMarkConstants.INPUT_FOR_DETACHABLE_HEAD] != null ? (List<CandidateGridViewModel>)Session[InputExaminationMarkConstants.INPUT_FOR_DETACHABLE_HEAD] : new List<CandidateGridViewModel>();

                    if (detachableHeadBagID.HasValue && detachableHeadBagID.Value > 0)
                    {
                        lstCandiateMark = lstCandiateMark.Where(u => u.DetachableHeadBagID == detachableHeadBagID.Value).ToList();
                        // Sap xep danh sach thi sinh theo so phach
                        int maxLength = lstCandiateMark.Count == 0 ? 0 : lstCandiateMark.Max(p => p.DetachableHeadNumber.Length); // Tim do dai so phach lon nhat
                        lstCandiateMark = lstCandiateMark.OrderBy(p => new string('0', maxLength - p.DetachableHeadNumber.Length) + p.DetachableHeadNumber).ToList(); // Sap xep theo so phach duoc mo rong voi 0
                    }
                    ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = lstCandiateMark;
                    return PartialView("_GridDetachableHead");
                }
                else
                {
                    if (examSubject.Examination.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                    {
                        ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] != null ? (List<CandidateGridViewModel>)Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] : new List<CandidateGridViewModel>();
                        return PartialView("_GridOrderNumber");
                    }
                    else
                    {
                        ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = Session[InputExaminationMarkConstants.INPUT_FOR_PUPILCODE] != null ? (List<CandidateGridViewModel>)Session[InputExaminationMarkConstants.INPUT_FOR_PUPILCODE] : new List<CandidateGridViewModel>();
                        return PartialView("_GridInputPupilCode");
                    }
                }
            }
            else
            {
                ViewData[InputExaminationMarkConstants.LIST_GRID_DATA] = Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] != null ? (List<InputViewModelBySBD>)Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] : new List<InputViewModelBySBD>();
                return PartialView("_GridAll");
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadDetachableHeadBag(int? examinationSubjectID)
        {
            if (examinationSubjectID != null)
            {
                ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(examinationSubjectID.Value);
                if (examSubject != null && examSubject.Examination.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
                {
                    GlobalInfo glo = new GlobalInfo();
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["ExaminationSubjectID"] = examinationSubjectID;
                    List<DetachableHeadBag> list = DetachableHeadBagBusiness.SearchBySchool(glo.SchoolID.Value, dic).ToList();
                    List<ComboObject> listCombo = list.Select(u => new ComboObject(u.DetachableHeadBagID.ToString(), u.BagTitle)).ToList();
                    return Json(new { enabled = true, data = listCombo });
                }
                else
                {
                    return Json(new { enabled = false, data = true });
                }
            }
            else
            {
                return Json(new { enabled = false, data = true });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            var lstEdu = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct()
                .OrderBy(o => o.EducationLevelID).ToList();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadExamSubject(int? educationLevelID, int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);

            if (educationLevelID.HasValue && educationLevelID.Value > 0)
                dicExaminationSubject.Add("EducationLevelID", educationLevelID);

            if (examinationID.HasValue && examinationID.Value > 0)
                dicExaminationSubject.Add("ExaminationID", examinationID);

            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            return Json(lstExamSubject.Select(u => new ComboObject(u.ExaminationSubjectID.ToString(), u.SubjectCat.DisplayName)).ToList());
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult UpdateMark(int hddExaminationSubjectID, int[] Candidates, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            Candidates = Candidates != null ? Candidates.Where(u => u > 0).ToArray() : new int[] { };

            int markType = ExaminationSubjectBusiness.GetSubjectMarkType(hddExaminationSubjectID, glo.AppliedLevel);
            List<Candidate> listCandidate = CandidateBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ExaminationSubjectID",hddExaminationSubjectID}
            }).ToList();
            if (Candidates.Length > 0)
            {
                List<Candidate> listCandidateUpdate = new List<Candidate>();
                if (!string.IsNullOrEmpty(form.Get("save")))
                {
                    foreach (int candidateID in Candidates)
                    {
                        string examMark = form.Get("ExamMark_" + candidateID);
                        examMark = examMark != null ? examMark.ToUpper() : null;
                        UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, markType, examMark);
                        Candidate ca = listCandidate.FirstOrDefault(o => o.CandidateID == candidateID);
                        if (ca != null)
                        {
                            // Cap nhat diem
                            if (markType == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                            {

                                if (examMark != "" && examMark != null)
                                {
                                    ca.Mark = decimal.Parse(examMark);
                                }
                                else
                                {
                                    ca.Mark = null;
                                }
                            }
                            else
                            {
                                ca.Judgement = examMark;
                            }
                            listCandidateUpdate.Add(ca);
                        }
                    }
                    if (listCandidateUpdate.Count > 0)
                    {
                        CandidateBusiness.UpdateList(listCandidateUpdate);
                        CandidateBusiness.Save();
                    }
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }

                // Lay lai list
                listCandidateUpdate.Clear();
                if (!string.IsNullOrEmpty(form.Get("delete")))
                {
                    foreach (int candidateID in Candidates)
                    {
                        Candidate ca = listCandidate.FirstOrDefault(o => o.CandidateID == candidateID);
                        if (ca != null)
                        {
                            ca.Mark = null;
                            ca.Judgement = "";
                            listCandidateUpdate.Add(ca);
                        }
                    }
                    if (listCandidateUpdate.Count > 0)
                    {
                        CandidateBusiness.UpdateList(listCandidateUpdate);
                        CandidateBusiness.Save();
                    }
                    return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
                }
                return Json(new JsonMessage(Res.Get("Common_Validate_NoRecordSelected"), JsonMessage.ERROR));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NoRecordSelected"), JsonMessage.ERROR));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult CheckDetachableHeadBag(int examinationID)
        {
            Examination exam = ExaminationBusiness.Find(examinationID);
            return Json(exam != null && exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD);
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<string, object> dicExamination = new Dictionary<string, object>();
            dicExamination.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExamination.Add("AppliedLevel", glo.AppliedLevel.Value);
            dicExamination.Add("CurrentStage", SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED.ToString());
            List<Examination> lstExam = ExaminationBusiness.SearchBySchool(glo.SchoolID.Value, dicExamination).OrderByDescending(o => o.ToDate).ToList();
            ViewData[InputExaminationMarkConstants.LIST_EXAMINATION] = new SelectList(lstExam, "ExaminationID", "Title");
            ViewData[InputExaminationMarkConstants.LIST_EDUCATION_LEVEL] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public ActionResult Import(InputExaminationMarkViewModel model, FormCollection form)
        {
            #region Kiem tra file hop le
            HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

            if (file == null)
            {
                return Json(new { success = false, message = Res.Get("InputExaminationMark_Label_NoFileSelected") });
            }

            if (!(file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx")))
            {
                return Json(new { success = false, message = Res.Get("InputExaminationMark_Label_FileIsNotExcel") });
            }

            if (file.ContentLength > 1024000)
            {
                return Json(new { success = false, message = Res.Get("InputExaminationMark_Label_MaxSizeExceeded") });
            }
            #endregion

            //Nhap diem tat ca cac mon
            if (model.ExaminationSubjectID == 0)
            {
                GlobalInfo glo = new GlobalInfo();
                //Thong tin ky thi
                Examination exam = ExaminationBusiness.Find(model.ExaminationID);
                //ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(model.ExaminationSubjectID);
                Dictionary<string, object> dic = new Dictionary<string, object>();

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                List<IVTWorksheet> sheets = book.GetSheets();

                IDictionary<string, object> dicRoom = new Dictionary<string, object>();
                dicRoom["ExaminationID"] = model.ExaminationID;
                dicRoom["EducationLevelID"] = model.EducationLevelID;
                dicRoom["AcademicYearID"] = glo.AcademicYearID.Value;
                List<ExaminationRoom> listRoom = ExaminationRoomBusiness.SearchBySchool(glo.SchoolID.Value, dicRoom).ToList();
                //List<DetachableHeadBag> listHeadBag = DetachableHeadBagBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ExaminationSubjectID", model.ExaminationSubjectID } }).ToList();

                #region kiem tra du lieu trong file hop le


                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER || exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE)
                {
                    var sheetNotExist = sheets.Where(u => !listRoom.Any(v => CommonFunctions.RemoveSign(v.RoomTitle).ToLower().Trim().Equals(CommonFunctions.RemoveSign(u.Name).Trim().ToLower())));
                    if (sheetNotExist.Count() > 0)
                    {
                        string message = string.Format(Res.Get("InputExaminationMark_Label_ExaminationNotHasRoom"), string.Join(",", sheetNotExist.Select(u => u.Name)));
                        return Json(new { success = false, message = Res.Get(message) });
                    }
                }



                #endregion

                List<Candidate> listInfo = CandidateBusiness.SearchBySchool(glo.SchoolID.Value
                                                                , new Dictionary<string, object> { 
                                                                {"ExaminationID", model.ExaminationID},
                                                                {"EducationLevelID", model.EducationLevelID},
                                                                {"AppliedLevel", glo.AppliedLevel.Value}
                                                            }).ToList();

                List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicRoom).Distinct().OrderBy(o => o.SubjectCat.OrderInSubject).ToList();

                List<InputViewModelBySBD> storedData = new List<InputViewModelBySBD>();
                #region nhap diem theo so bao danh tat ca cac mon
                //Với mỗi row trong 1 Sheet trong file Excel khởi tạo 1 đối tượng CandidateMark

                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                {
                    IDictionary<string, object> dicSub = new Dictionary<string, object>();
                    dicSub["SchoolID"] = glo.SchoolID;
                    dicSub["AcademicYearID"] = glo.AcademicYearID;
                    dicSub["AppliedLevel"] = glo.AppliedLevel;
                    dicSub["EducationLevelID"] = model.EducationLevelID;
                    List<Candidate> lstCan = new List<Candidate>();
                    Candidate ca = new Candidate();
                    List<string> lstDuplicateSBDExcel = new List<string>();
                    //Danh sach SBD thi sinh tham gia ky thi
                    List<string> listPupilExam = listInfo.Select(o => o.NamedListCode).ToList();
                    //tao dictionary de luu diem tu file excel
                    IDictionary<string, object> dicMark = new Dictionary<string, object>();
                    //Lay danh sach cac so bao danh bi trung
                    List<string> lstSBDExcel = new List<string>();
                    //Lay danh sach thi sinh ko ton tai trong DB
                    List<string> lstSBDNull = new List<string>();
                    foreach (IVTWorksheet sheet in sheets)
                    {

                        int rowIndex = 11;
                        bool hasValue = true;
                        while (hasValue)
                        {
                            hasValue = GetCellValue(sheet, "A" + (rowIndex + 1).ToString()) != string.Empty;
                            string NamedListCode = GetCellValue(sheet, "B" + rowIndex);

                            if (!listPupilExam.Contains(NamedListCode))
                            {
                                lstSBDNull.Add(NamedListCode);
                            }
                            if (lstSBDExcel.Contains(NamedListCode))
                            {
                                lstDuplicateSBDExcel.Add(NamedListCode);

                            }
                            else
                            {
                                for (int i = 0; i < lstExamSubject.Count; i++)
                                {
                                    string Subject = GetCellValueIndex(sheet, 10, i + 3).ToUpper();
                                    string mark = GetCellValueIndex(sheet, rowIndex, i + 3).ToUpper();
                                    dicMark[NamedListCode + "_" + Subject] = mark;
                                }
                                lstSBDExcel.Add(NamedListCode);
                            }
                            rowIndex++;
                        }
                    }
                    //Danh sach mon hoc cho truong
                    List<SchoolSubject> listss = SchoolSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicSub).Distinct().ToList();
                    //Duyet theo tung mon thi
                    for (int i = 0; i < lstExamSubject.Count; i++)
                    {
                        //Lấy ra kiểu môn và tên môn
                        int Iscommenting = listss.Where(o => o.SubjectID == lstExamSubject[i].SubjectID).FirstOrDefault().IsCommenting.Value;
                        string SubjectName = lstExamSubject[i].SubjectCat.SubjectName;

                        foreach (IVTWorksheet sheet in sheets)
                        {
                            List<string> listNameListCode = new List<string>();
                            //Hàng có dữ liệu đầu tiên
                            int rowIndex = 11;

                            InputViewModelBySBD cm = new InputViewModelBySBD();
                            List<ExaminationRoom> lstExamRoom = listRoom.Where(o => o.ExaminationSubjectID == lstExamSubject[i].ExaminationSubjectID).ToList();

                            bool hasValue = true;

                            while (hasValue)
                            {
                                //Kiểm tra sheet có dữ liệu hay ko
                                hasValue = GetCellValue(sheet, "A" + (rowIndex + 1).ToString()) != string.Empty;

                                foreach (ExaminationRoom er in lstExamRoom)
                                {
                                    if (hasValue)
                                    {
                                        if (CommonFunctions.RemoveSign(er.RoomTitle).ToLower().Equals(CommonFunctions.RemoveSign(sheet.Name).ToLower().Trim()))
                                        {
                                            //Danh sách thí sinh trong phòng thi (DB)
                                            List<Candidate> listcmInList = listInfo.Where(u => u.RoomID == er.ExaminationRoomID).OrderBy(o => o.NamedListNumber).ToList();
                                            //Danh sach so bao danh trong phong thi (DB)
                                            List<string> listSBD = listcmInList.Select(o => o.NamedListCode).ToList();
                                            // List<string> listSBDExcel = new List<string>();

                                            foreach (Candidate cmInList in listcmInList)
                                            {
                                                //Tạo biến để theo dõi dữ liệu và hiển thị lỗi
                                                cm = new InputViewModelBySBD();
                                                cm.NamedListCode = cmInList.NamedListCode;
                                                cm.NamedListNumber = cmInList.NamedListNumber;
                                                //Lay mon thi trong file excel
                                                string Subject = GetCellValueIndex(sheet, 10, i + 3).ToUpper();
                                                //Mon thi khong giong voi mon thi trong DB --
                                                if (SubjectName.ToUpper() != Subject.ToUpper())
                                                {
                                                    List<object> Params = new List<object>();
                                                    Params.Add(Subject);
                                                    throw new BusinessException("Validate_Duplicate_Subject_Import", Params);
                                                }
                                                //Lay ra so bao danh hien tai kiem tra xem ton tai hay ko
                                                if (cmInList.NamedListCode != "" && !lstSBDExcel.Contains(cmInList.NamedListCode) && !lstDuplicateSBDExcel.Contains(cmInList.NamedListCode))
                                                {

                                                    cm.ErrorReason = String.Format(Res.Get("InputExaminationMark_Label_NamedListCodeNotExist_inDB"), cmInList.NamedListCode);
                                                    listNameListCode.Add(cmInList.NamedListCode);
                                                    storedData.Add(cm);
                                                    rowIndex++;
                                                    continue;
                                                }

                                                //Lay diem tu file excel
                                                string mark = "";
                                                if (dicMark.ContainsKey(cmInList.NamedListCode + "_" + Subject))
                                                {
                                                    mark = SMAS.Business.Common.Utils.GetString(dicMark, cmInList.NamedListCode + "_" + Subject);
                                                }
                                                else
                                                {
                                                    cm.ErrorReason = String.Format(Res.Get("InputExaminationMark_Label_NamedListCodeNotExist_inDB"), cmInList.NamedListCode);
                                                    listNameListCode.Add(cmInList.NamedListCode);
                                                    storedData.Add(cm);
                                                    rowIndex++;
                                                    continue;
                                                }

                                                cm.ExamMark = string.Empty;

                                                //Hoc sinh vang thi
                                                if (cmInList.HasReason.HasValue)
                                                {

                                                    cm.ErrorReason = String.Format(Res.Get("InputExaminationMark_Label_PupilIsAbsence"), SubjectName);
                                                    listNameListCode.Add(cm.NamedListCode);
                                                    storedData.Add(cm);
                                                    rowIndex++;
                                                    continue;
                                                }


                                                //Kiem tra diem
                                                try
                                                {
                                                    UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, Iscommenting, mark);
                                                    cm.ExamMark = mark;
                                                }
                                                catch
                                                {
                                                    cm.ErrorReason = String.Format(Res.Get("ImportSemesterTestMark_Label_MarkInvalid"), SubjectName);
                                                    cm.NamedListCode = cmInList.NamedListCode;
                                                    listNameListCode.Add(cm.NamedListCode);
                                                    storedData.Add(cm);
                                                    rowIndex++;
                                                    continue;
                                                }

                                                if (cm.ErrorReason == null || cm.ErrorReason.Trim() == "")
                                                {
                                                    if (!lstDuplicateSBDExcel.Contains(cmInList.NamedListCode))
                                                    {

                                                        if (Iscommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                                                        {
                                                            if (cm.ExamMark != "" && cm.ExamMark != null)
                                                            {

                                                                cmInList.Mark = decimal.Parse(cm.ExamMark);
                                                            }
                                                            else
                                                            {
                                                                cmInList.Mark = null;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cmInList.Judgement = cm.ExamMark;
                                                        }
                                                    }

                                                }
                                                //Check trung SBD
                                                if (lstDuplicateSBDExcel.Contains(cm.NamedListCode))
                                                {

                                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_TooManyNamedListCode"), cm.NamedListCode);
                                                    listNameListCode.Add(cm.NamedListCode);
                                                    storedData.Add(cm);
                                                    rowIndex++;
                                                    continue;
                                                }
                                                //Ko trung thi add vao list so bao danh
                                                listNameListCode.Add(cm.NamedListCode);
                                                //do du lieu
                                                if (!lstDuplicateSBDExcel.Contains(cmInList.NamedListCode))
                                                {
                                                    lstCan.Add(cmInList);
                                                }
                                                storedData.Add(cm);
                                                rowIndex++;
                                            }
                                        }
                                    }



                                }
                                rowIndex++;
                            }

                        }


                    }
                    //Hien thi loi
                    List<InputViewModelBySBD> lststoredDataError = storedData.Where(o => o.ErrorReason != null && o.ErrorReason.Trim() != "").OrderBy(o => o.NamedListNumber).ToList();
                    List<InputViewModelBySBD> lststoredDatashow = new List<InputViewModelBySBD>();
                    List<string> lstSBD = new List<string>();
                    //Hiển thị thí sinh lỗi điểm,trùng số báo danh,vắng thi
                    InputViewModelBySBD ivsb = new InputViewModelBySBD();
                    int k = 0;
                    foreach (InputViewModelBySBD ivb in lststoredDataError)
                    {

                        if (lstSBD.Contains(ivb.NamedListCode))
                        {
                            if (!ivsb.ErrorReason.Contains(ivb.ErrorReason))
                            {
                                ivsb.ErrorReason = ivsb.ErrorReason + " - " + ivb.ErrorReason;
                            }
                            lststoredDatashow[k - 1] = ivsb;
                        }
                        else
                        {
                            ivsb = ivb;
                            lstSBD.Add(ivb.NamedListCode);
                            lststoredDatashow.Add(ivsb);
                            k++;
                        }
                    }


                    //Hiển thị thí sinh có trong file Excel nhưng ko có trong CSDL
                    InputViewModelBySBD inputview = new InputViewModelBySBD();
                    foreach (string sbdnull in lstSBDNull)
                    {
                        inputview = new InputViewModelBySBD();
                        inputview.NamedListCode = sbdnull;
                        inputview.NamedListNumber = 0;
                        inputview.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_NamedListCodeNotExist"), sbdnull);
                        inputview.ExamMark = "";
                        lststoredDatashow.Add(inputview);
                    }

                    Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] = lststoredDatashow;

                    //Vao diem thi
                    CandidateBusiness.UpdateList(lstCan);
                    CandidateBusiness.Save();


                    //Thay đổi trạng thái kì thi - đợi bổ sung nghiệp vụ
                    //exam.CurrentStage = SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED;
                    //ExaminationBusiness.Update(exam);
                    //ExaminationBusiness.Save();
                }
                #endregion
            }
            else
            {
                GlobalInfo glo = new GlobalInfo();
                Examination exam = ExaminationBusiness.Find(model.ExaminationID);
                ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(model.ExaminationSubjectID);
                Dictionary<string, object> dic = new Dictionary<string, object>();

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                List<IVTWorksheet> sheets = book.GetSheets();


                List<ExaminationRoom> listRoom = ExaminationRoomBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ExaminationSubjectID", model.ExaminationSubjectID } }).ToList();
                List<DetachableHeadBag> listHeadBag = DetachableHeadBagBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "ExaminationSubjectID", model.ExaminationSubjectID } }).ToList();

                #region kiem tra du lieu trong file hop le
                if (!GetCellValue(book.GetSheet(1), "A7").ToLower().Contains(examSubject.SubjectCat.DisplayName.ToLower().Trim()))
                {
                    return Json(new { success = false, message = string.Format(Res.Get("InputExaminationMark_Label_FileNotForSubject"), examSubject.SubjectCat.DisplayName) });
                }

                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER || exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE)
                {
                    var sheetNotExist = sheets.Where(u => !listRoom.Any(v => CommonFunctions.RemoveSign(v.RoomTitle).ToLower().Trim().Equals(CommonFunctions.RemoveSign(u.Name).Trim().ToLower())));
                    if (sheetNotExist.Count() > 0)
                    {
                        string message = string.Format(Res.Get("InputExaminationMark_Label_ExaminationNotHasRoom"), string.Join(",", sheetNotExist.Select(u => u.Name)));
                        return Json(new { success = false, message = Res.Get(message) });
                    }
                }

                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
                {
                    var sheetNotExist = sheets.Where(u => !listHeadBag.Any(v => CommonFunctions.RemoveSign(v.BagTitle).ToLower().Trim().Equals(CommonFunctions.RemoveSign(u.Name).Trim().ToLower())));
                    if (sheetNotExist.Count() > 0)
                    {
                        string message = string.Format(Res.Get("InputExaminationMark_Label_ExaminationNotHasHeadBag"), string.Join(",", sheetNotExist.Select(u => u.Name)));
                        return Json(new { success = false, message = Res.Get(message) });
                    }
                }
                #endregion

                List<CandidateMark> listInfo = CandidateBusiness.SearchMark(glo.SchoolID.Value
                                                                , (int)glo.AppliedLevel.Value
                                                                , new Dictionary<string, object> { 
                                                                {"ExaminationID", model.ExaminationID},
                                                                {"EducationLevelID", model.EducationLevelID},
                                                                {"ExaminationSubjectID", model.ExaminationSubjectID}
                                                            }).ToList();

                List<CandidateGridViewModel> storedData = new List<CandidateGridViewModel>();

                #region nhap diem theo so bao danh
                //Với mỗi row trong 1 Sheet trong file Excel khởi tạo 1 đối tượng CandidateMark
                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
                {
                    //- Nếu Examination(cboExamination .Value).MarkImportType = MARK_IMPORT_TYPE_ORDER_NUMBER
                    //+ Gán NamedListCode = giá trị ở cột SBD
                    //+ Gán RoomID = Sheet tương ứng
                    //+ Từ NamedListCode và RoomID, select trong ListInfo để lấy thông tin CandidateID, PenaltyMarkPercent. 
                    //> Nếu không có thì IsDisable = TRUE, ErrorReason = “Số báo danh không tồn tại trên hệ thống”
                    //> Nếu có
                    //Ø	Gán các thông tin từ bản ghi tìm kiếm được vào đối tượng CandidateMark; IsDisable = FALSE
                    //Ø	Nếu UtilsBusiness.CheckValidateMark(UserInfo.AppliedLevel, CandidateMark.IsCommenting, [giá trị ở cột Điểm thi]) 
                    //   không ném ra lỗi  thi gán CandidateMark.ExamMark = [giá trị ở cột Điểm thi] , IsCheck = TRUE 
                    //   co ném ra lỗi thi gán CandidateMark.ExamMark = “” , IsCheck = FALSE,  ErrorReason = [Lỗi ném ra]
                    //+ Nếu có NamedListCode đã có trong file Excel trước đó thì IsDisable = TRUE, ErrorReason = “Trong file exel có nhiều hơn 1 số báo danh x”
                    //  List<InputViewModelBySBD> storedDataBySBD = new List<InputViewModelBySBD>();
                    List<string> listNameListCode = new List<string>();
                    foreach (IVTWorksheet sheet in sheets)
                    {
                        bool hasValue = true;
                        int rowIndex = 10;
                        while (hasValue)
                        {
                            hasValue = GetCellValue(sheet, "A" + rowIndex) != string.Empty;
                            if (hasValue)
                            {
                                CandidateGridViewModel cm = new CandidateGridViewModel();

                                string mark = GetCellValue(sheet, "C" + rowIndex).ToUpper();

                                cm.NamedListCode = GetCellValue(sheet, "B" + rowIndex);
                                cm.RoomID = listRoom.Where(u => CommonFunctions.RemoveSign(u.RoomTitle).ToLower().Equals(CommonFunctions.RemoveSign(sheet.Name).ToLower().Trim())).FirstOrDefault().ExaminationRoomID;
                                cm.IsDisable = true;
                                cm.IsCheck = false;
                                CandidateMark cmInList = listInfo.FirstOrDefault(u => u.RoomID == cm.RoomID && u.NamedListCode == cm.NamedListCode);

                                if (cmInList == null)
                                {
                                    cm.ErrorReason = Res.Get("InputExaminationMark_Label_NamedListCodeNotExist");
                                    listNameListCode.Add(cm.NamedListCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.BirthDate = cmInList.BirthDate;
                                cm.CandidateID = cmInList.CandidateID;
                                cm.Description = cmInList.Description;
                                cm.DetachableHeadBagID = cmInList.DetachableHeadBagID;
                                cm.DetachableHeadNumber = cmInList.DetachableHeadNumber;
                                cm.IsCommenting = cmInList.IsCommenting;
                                cm.Judgement = cmInList.Judgement;
                                cm.Mark = cmInList.Mark;
                                cm.NamedListCode = cmInList.NamedListCode;
                                cm.PenaltyMarkPercent = cmInList.PenaltyMarkPercent;
                                cm.PupilCode = cmInList.PupilCode;
                                cm.PupilID = cmInList.PupilID;
                                cm.PupilName = cmInList.PupilName;
                                cm.RoomName = cmInList.RoomName;
                                cm.DisplayMark = mark;
                                cm.ExamMark = string.Empty;

                                if (cmInList.HasReason.HasValue) //Hoc sinh vang thi
                                {
                                    cm.ErrorReason = Res.Get("ErrorMsg_InputExaminationMark_PupilIsAbsence");
                                    listNameListCode.Add(cm.NamedListCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                if (string.IsNullOrEmpty(mark)) //Hoc sinh vang thi
                                {
                                    cm.ErrorReason = "";
                                    listNameListCode.Add(cm.NamedListCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                try
                                {
                                    UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, (int)cmInList.IsCommenting.Value, mark);
                                    cm.ExamMark = mark;

                                    if (cmInList.IsCommenting.HasValue && cmInList.IsCommenting.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && cmInList.PenaltyMarkPercent.HasValue)
                                        cm.RealMark = glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        ? (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("#")
                                                        : (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("0.#");
                                    else cm.RealMark = mark;
                                }
                                catch
                                {
                                    cm.ErrorReason = Res.Get("ErrorMsg_ImportTestMark_MarkInvalid");
                                    listNameListCode.Add(cm.NamedListCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                if (listNameListCode.Contains(cm.NamedListCode))
                                {
                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_TooManyNamedListCode"), cm.NamedListCode);
                                    listNameListCode.Add(cm.NamedListCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.IsDisable = false;
                                cm.IsCheck = true;
                                listNameListCode.Add(cm.NamedListCode);
                                storedData.Add(cm);
                            }
                            rowIndex++;
                        }

                        Session[InputExaminationMarkConstants.INPUT_FOR_ORDER_NUMBER] = storedData;
                    }
                }
                #endregion

                #region nhap diem theo ma hoc sinh
                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE)
                {
                    //- Nếu Examination(cboExamination .Value).MarkImportType = MARK_IMPORT_TYPE_PUPILCODE
                    //+ Gán PupilName  = giá trị ở cột Họ và tên
                    //+ Gán PupilCode  = giá trị ở cột Mã học sinh
                    //+ Gán RoomID = Sheet tương ứng
                    //+ Từ PupilName , PupilCode và RoomID, select trong ListInfo để lấy thông tin CandidateID, PenaltyMarkPercent. 
                    //> Nếu không có thì IsDisable = TRUE, ErrorReason = “Mã học sinh không tồn tại trên hệ thống hoặc tên học sinh không khớp mã học sinh.”
                    //> Nếu có
                    //Ø	Gán các thông tin từ bản ghi tìm kiếm được vào đối tượng CandidateMark; IsDisable = FALSE
                    //Ø	Nếu UtilsBusiness.CheckValidateMark(UserInfo.AppliedLevel, CandidateMark.IsCommenting, [giá trị ở cột Điểm thi]) không ném ra lỗi thi gán CandidateMark.ExamMark = [giá trị ở cột Điểm thi] , IsCheck = TRUE 
                    //Ø	Nếu UtilsBusiness.CheckValidateMark(UserInfo.AppliedLevel, CandidateMark.IsCommenting, [giá trị ở cột Điểm thi]) ném ra lỗi thi gán CandidateMark.ExamMark = “” , IsCheck = FALSE,  ErrorReason = [Lỗi ném ra]
                    //+ Nếu có PupilCode đã có trong file Excel trước đó thì IsDisable = TRUE, ErrorReason = “Trong file excel có nhiều hơn 1 mã học sinh x”
                    List<string> listPupilCode = new List<string>();
                    foreach (IVTWorksheet sheet in sheets)
                    {
                        bool hasValue = true;
                        int rowIndex = 10;
                        while (hasValue)
                        {
                            hasValue = GetCellValue(sheet, "A" + rowIndex) != string.Empty;
                            if (hasValue)
                            {
                                CandidateGridViewModel cm = new CandidateGridViewModel();
                                cm.PupilCode = GetCellValue(sheet, "B" + rowIndex);
                                cm.PupilName = GetCellValue(sheet, "C" + rowIndex);
                                cm.RoomID = listRoom.Where(u => CommonFunctions.RemoveSign(u.RoomTitle).ToLower().Equals(CommonFunctions.RemoveSign(sheet.Name).ToLower().Trim())).FirstOrDefault().ExaminationRoomID;
                                cm.IsDisable = true;
                                cm.IsCheck = false;

                                string mark = GetCellValue(sheet, "D" + rowIndex).ToUpper();

                                CandidateMark cmInList = listInfo.Where(u => u.RoomID == cm.RoomID && u.PupilCode == cm.PupilCode).FirstOrDefault();
                                if (cmInList == null)
                                {
                                    cm.IsDisable = true;
                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_PupilCodeNotExist"), cm.PupilCode, sheet.Name.Trim());
                                    listPupilCode.Add(cm.PupilCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.BirthDate = cmInList.BirthDate;
                                cm.CandidateID = cmInList.CandidateID;
                                cm.Description = cmInList.Description;
                                cm.DetachableHeadBagID = cmInList.DetachableHeadBagID;
                                cm.DetachableHeadNumber = cmInList.DetachableHeadNumber;
                                cm.IsCommenting = cmInList.IsCommenting;
                                cm.Judgement = cmInList.Judgement;
                                cm.Mark = cmInList.Mark;
                                cm.NamedListCode = cmInList.NamedListCode;
                                cm.PenaltyMarkPercent = cmInList.PenaltyMarkPercent;
                                cm.PupilCode = cmInList.PupilCode;
                                cm.PupilID = cmInList.PupilID;
                                cm.PupilName = cmInList.PupilName;
                                cm.RoomName = cmInList.RoomName;
                                cm.DisplayMark = mark;
                                cm.ExamMark = string.Empty;

                                if (cmInList.HasReason.HasValue) //Hoc sinh vang thi
                                {
                                    cm.ErrorReason = Res.Get("ErrorMsg_InputExaminationMark_PupilIsAbsence");
                                    listPupilCode.Add(cm.PupilCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                try
                                {
                                    UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, (int)cmInList.IsCommenting.Value, mark);
                                    cm.ExamMark = mark;
                                    if (cmInList.IsCommenting.HasValue && cmInList.IsCommenting.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && cmInList.PenaltyMarkPercent.HasValue)
                                        cm.RealMark = glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        ? (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("#")
                                                        : (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("0.#");
                                    else cm.RealMark = mark;
                                }
                                catch (Exception ex)
                                {
                                    cm.ExamMark = string.Empty;
                                    cm.ErrorReason = Res.Get("ErrorMsg_ImportTestMark_MarkInvalid");
                                    listPupilCode.Add(cm.PupilCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                if (listPupilCode.Contains(cm.PupilCode))
                                {
                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_TooManyPupilCode"), cm.PupilName);
                                    listPupilCode.Add(cm.PupilCode);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.IsCheck = true;
                                cm.IsDisable = false;
                                listPupilCode.Add(cm.PupilCode);
                                storedData.Add(cm);
                            }
                            rowIndex++;
                        }

                        Session[InputExaminationMarkConstants.INPUT_FOR_PUPILCODE] = storedData;
                    }
                }
                #endregion

                #region nhap diem theo so phach
                if (exam.MarkImportType == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
                {
                    //- Nếu Examination(cboExamination .Value).MarkImportType = MARK_IMPORT_TYPE_DETACHABLE_HEAD
                    //+ Gán DetachableHeadNumber  = giá trị ở cột Số phách
                    //+ Gán DetachableHeadBagID = Sheet tương ứng
                    //+ Từ DetachableHeadNumber  và DetachableHeadBagID, select trong ListInfo để lấy thông tin CandidateID, PenaltyMarkPercent. 
                    //> Nếu không có thì IsDisable = TRUE, ErrorReason = “Không tồn tại số phách x trong kỳ thi.”
                    //> Nếu có
                    //Ø	Gán các thông tin từ bản ghi tìm kiếm được vào đối tượng CandidateMark; IsDisable = FALSE
                    //Ø	Nếu UtilsBusiness.CheckValidateMark(UserInfo.AppliedLevel, CandidateMark.IsCommenting, [giá trị ở cột Điểm thi]) không ném ra lỗi thi gán CandidateMark.ExamMark = [giá trị ở cột Điểm thi] , IsCheck = TRUE 
                    //Ø	Nếu Utils.CheckValidateMark(UserInfo.AppliedLevel, CandidateMark.IsCommenting, [giá trị ở cột Điểm thi]) ném ra lỗi thi gán CandidateMark.ExamMark = “” , IsCheck = FALSE,  ErrorReason = [Lỗi ném ra]
                    //+ Nếu có DetachableHeadNumber  đã có trong file Excel trước đó thì IsDisable = TRUE, ErrorReason = “Trong file excel có nhiều hơn 1 số phách x”
                    List<string> listDetachNumber = new List<string>();
                    foreach (IVTWorksheet sheet in sheets)
                    {
                        bool hasValue = true;
                        int rowIndex = 10;
                        while (hasValue)
                        {
                            hasValue = GetCellValue(sheet, "A" + rowIndex) != string.Empty;
                            if (hasValue)
                            {
                                CandidateGridViewModel cm = new CandidateGridViewModel();

                                cm.DetachableHeadNumber = GetCellValue(sheet, "B" + rowIndex);
                                cm.DetachableHeadBagID = listHeadBag.Where(u => CommonFunctions.RemoveSign(u.BagTitle).ToLower().Equals(CommonFunctions.RemoveSign(sheet.Name).ToLower().Trim())).FirstOrDefault().DetachableHeadBagID;
                                cm.IsDisable = true;
                                cm.IsCheck = false;

                                string mark = GetCellValue(sheet, "C" + rowIndex).ToUpper();

                                CandidateMark cmInList = listInfo.Where(u => u.DetachableHeadBagID == cm.DetachableHeadBagID && u.DetachableHeadNumber != null && u.DetachableHeadNumber.Trim() == cm.DetachableHeadNumber).FirstOrDefault();
                                if (cmInList == null)
                                {
                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_DetachableHeadNumberNotExist"), cm.DetachableHeadNumber);
                                    listDetachNumber.Add(cm.DetachableHeadNumber);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.BirthDate = cmInList.BirthDate;
                                cm.CandidateID = cmInList.CandidateID;
                                cm.Description = cmInList.Description;
                                cm.DetachableHeadBagID = cmInList.DetachableHeadBagID;
                                cm.DetachableHeadNumber = cmInList.DetachableHeadNumber;
                                cm.IsCommenting = cmInList.IsCommenting;
                                cm.Judgement = cmInList.Judgement;
                                cm.Mark = cmInList.Mark;
                                cm.NamedListCode = cmInList.NamedListCode;
                                cm.PenaltyMarkPercent = cmInList.PenaltyMarkPercent;
                                cm.PupilCode = cmInList.PupilCode;
                                cm.PupilID = cmInList.PupilID;
                                cm.PupilName = cmInList.PupilName;
                                cm.RoomName = cmInList.RoomName;
                                cm.DisplayMark = mark;
                                cm.ExamMark = string.Empty;

                                try
                                {
                                    UtilsBusiness.CheckValidateMark(glo.AppliedLevel.Value, (int)cmInList.IsCommenting.Value, mark);
                                    cm.ExamMark = mark;
                                    if (cmInList.IsCommenting.HasValue && cmInList.IsCommenting.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && cmInList.PenaltyMarkPercent.HasValue)
                                        cm.RealMark = glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        ? (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("#")
                                                        : (decimal.Parse(mark) * (1 - (decimal)cmInList.PenaltyMarkPercent.Value / 100)).ToString("0.#");
                                    else cm.RealMark = mark;
                                }
                                catch
                                {
                                    cm.ErrorReason = Res.Get("ErrorMsg_ImportTestMark_MarkInvalid");
                                    listDetachNumber.Add(cm.DetachableHeadNumber);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                if (cmInList.HasReason.HasValue) //Hoc sinh vang thi
                                {
                                    cm.ErrorReason = Res.Get("ErrorMsg_InputExaminationMark_PupilIsAbsence");
                                    listDetachNumber.Add(cm.DetachableHeadNumber);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                if (listDetachNumber.Contains(cm.DetachableHeadNumber))
                                {
                                    cm.ErrorReason = string.Format(Res.Get("InputExaminationMark_Label_TooManyDetachableHeadNumber"), cm.DetachableHeadNumber);
                                    listDetachNumber.Add(cm.DetachableHeadNumber);
                                    storedData.Add(cm);
                                    rowIndex++;
                                    continue;
                                }

                                cm.IsCheck = true;
                                cm.IsDisable = false;
                                listDetachNumber.Add(cm.DetachableHeadNumber);
                                storedData.Add(cm);
                            }
                            rowIndex++;
                        }
                        Session[InputExaminationMarkConstants.INPUT_FOR_DETACHABLE_HEAD] = storedData;
                    }
                }
                #endregion
            }
            return Json(new { success = true, message = Res.Get("InputExaminationMark_Label_ImportSuccess") });
        }

        private string GetCellValue(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
        private string GetCellValueIndex(IVTWorksheet sheet, int Row, int Col)
        {
            object o = sheet.GetRange(Row, Col, Row, Col).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
        //Bo sung 12/12
        public JsonResult checkExam(int? idExaminationID)
        {

            Examination Examination = ExaminationBusiness.Find(idExaminationID);
            string TypeMark = "0";
            if (Examination != null)
            {
                TypeMark = Examination.MarkImportType.ToString();
            }
            return Json(TypeMark, JsonRequestBehavior.AllowGet);

        }
    }
}
