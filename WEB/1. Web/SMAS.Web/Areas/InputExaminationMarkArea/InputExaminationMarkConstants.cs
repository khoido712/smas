﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.InputExaminationMarkArea
{
    public class InputExaminationMarkConstants
    {
        public const string LIST_EXAMINATION = "listExamination";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "listExaminationSubject";
        public const string LIST_EXAMVIOLATION_TYPE = "listExamViolationType";
        public const string LIST_CANDIDATE = "listCandidate";
        public const string LIST_DETACHABLE_HEAD_BAG = "ListDetachableHeadBag";

        public const string LIST_GRID_DATA = "ListGridData";

        public const string INT_MARK_TYPE = "IntMarkType";
        public const string INT_EXAMINATION_SUBJECT_ID = "int_examination_subject_id";
        public const string IS_IMPORT_PANEL = "is_import_panel";

        public const string INPUT_FOR_ORDER_NUMBER = "input_for_order_number";
        public const string INPUT_FOR_PUPILCODE = "input_for_pupilcode";
        public const string INPUT_FOR_DETACHABLE_HEAD = "input_for_detachable_head";
    }
}