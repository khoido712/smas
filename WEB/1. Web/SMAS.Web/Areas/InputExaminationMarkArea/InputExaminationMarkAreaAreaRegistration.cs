﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.InputExaminationMarkArea
{
    public class InputExaminationMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "InputExaminationMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "InputExaminationMarkArea_default",
                "InputExaminationMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
