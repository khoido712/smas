/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Resources;
using SMAS.Models.Models;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SchoolAccountArea.Models
{
    public class SchoolAccountViewModel
    {
        public int UserAccountID;
        public int? EmployeeID;
        public int? FacultyID;

        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("Employee_Label_LoginName")]
        public String UserName { get; set; }

        [ResourceDisplayName("LogOn_Password")]
        public String Pass { get; set; }

        [ResourceDisplayName("Salary_Label_EmployeeName")]
        public String FullName { get; set; }

        [ResourceDisplayName("Employee_Column_StaffPosition")]
        public String Position { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String PhoneNo { get; set; }
        [ResourceDisplayName("SchoolProfile_Label_Status")]
        public string StatusName { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<System.DateTime> FirstLoginDate { get; set; }

        [ScaffoldColumn(false)]
        public List<Employee> ListEmployee { get; set; }

        [ScaffoldColumn(false)]
        public IQueryable<GroupCat> ListGroupOfRole { get; set; }

        [ScaffoldColumn(false)]
        public List<SelectListItem> ListRoleOfUser { get; set; }

        [ScaffoldColumn(false)]
        public int? Order { get; set; }
    }
}


