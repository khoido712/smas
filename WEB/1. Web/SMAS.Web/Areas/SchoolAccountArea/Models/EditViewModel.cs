﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SMAS.Models.Models;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolAccountArea.Models
{
    public class EditViewModel
    {
        public int UserAccountID;
        public int RoleID;


        [ResourceDisplayName("Employee_Label_LoginName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public String UserName { get; set; }

        [ResourceDisplayName("Salary_Label_EmployeeName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public String FullName { get; set; }

        [ResourceDisplayName("Employee_Column_StaffPosition")]
        public String Position { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public String BirthDate { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String PhoneNo { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String Email { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String PermanentResidentalAddress { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String Genre { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String HomeTown { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        public String SchoolFaculty { get; set; }

        [ScaffoldColumn(false)]
        public IEnumerable<GroupCat> ListGroupOfRole { get; set; }

        [ScaffoldColumn(false)]
        public bool IsActive { get; set; }

        [ScaffoldColumn(false)]
        public int EmployeeID { get; set; }
         

        [ScaffoldColumn(false)]
        public IEnumerable<GroupCat> ListGroupOfSupervisingDeptAccount { get; set; }

        [ScaffoldColumn(false)]
        public List<SelectListItem> ListEmployee { get; set; }


    }
}