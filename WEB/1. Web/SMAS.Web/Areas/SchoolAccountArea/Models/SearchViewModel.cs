/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.UserAccountArea.Models
{
    public class SearchViewModel
    {
           [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public String Username { get; set; }
           [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public String FullName { get; set; }
        public Nullable<bool> Genre { get; set; }
        public Nullable<int> SchoolFacutyID { get; set; }
        public Nullable<int> GroupCatID { get; set; }
        public Nullable<int> Status { get; set; }
    }
}