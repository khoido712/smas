﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolAccountArea
{
    public class SchoolAccountAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolAccountArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolAccountArea_default",
                "SchoolAccountArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
