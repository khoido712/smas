﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.UserAccountArea.Models;
using SMAS.Web.Areas.UserAccountArea;
using SMAS.Web.Areas;
using SMAS.Web.Areas.SchoolAccountArea;
using SMAS.Web.Areas.SchoolAccountArea.Models;
using System.Web.Security;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.SchoolAccountArea.Controllers
{
    public class SchoolAccountController : BaseController
    {
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IUserGroupBusiness UserGroupBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IMenuBusiness MenuBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        public SchoolAccountController(IUserAccountBusiness useraccountBusiness, ISchoolFacultyBusiness schoolFacultyBusiness, IGroupCatBusiness groupCatBusiness, IUserGroupBusiness userGroupBusiness, IEmployeeBusiness employeeBusiness, ISchoolProfileBusiness schoolProfileBusiness, ISupervisingDeptBusiness supervisingDeptBusiness, IMenuBusiness MenuBusiness, IGroupMenuBusiness GroupMenuBusiness)
        {
            this.UserAccountBusiness = useraccountBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.GroupCatBusiness = groupCatBusiness;
            this.UserGroupBusiness = userGroupBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.MenuBusiness = MenuBusiness;
            this.GroupMenuBusiness = GroupMenuBusiness;
        }

        public ActionResult Index()
        {
            SetViewDataPermission("SchoolAccount", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            List<ComboObject> listSex = new List<ComboObject>();
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
            ViewData[SchoolAccountConstants.LIST_LISTSEX] = new SelectList(listSex, "key", "value");

            List<ComboObject> listStatus = new List<ComboObject>();
            listStatus.Add(new ComboObject("1", SchoolAccountConstants.NEWACTIVE));
            ViewData[SchoolAccountConstants.LIST_STATUS] = new SelectList(listStatus, "key", "value");

            GlobalInfo globalInfo = new GlobalInfo();
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 52;

            IDictionary<string, object> Search = new Dictionary<string, object>();
            Search["isActive"] = true;

            IQueryable<SchoolFaculty> listSchoolfaculty = SchoolFacultyBusiness.SearchBySchool(SchoolID, Search).OrderBy(o => o.FacultyName);

            if (listSchoolfaculty != null)
            {
                ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(listSchoolfaculty.ToList(), "SchoolFacultyID", "FacultyName");
            }
            else ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(new string[] { });



            int RoleId = globalInfo.RoleID != 0 ? globalInfo.RoleID : 0;
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
            IQueryable<GroupCat> listGroupcat = GroupCatBusiness.GetGroupByRoleAndAdminID(RoleId, schoolProfile.AdminID.Value);
            if (listGroupcat != null)
            {
                ViewData[SchoolAccountConstants.LIST_LISTGROUPCAT] = new SelectList(listGroupcat.OrderBy(o => o.GroupName).ToList(), "GroupCatID", "GroupName");
            }
            else ViewData[SchoolAccountConstants.LIST_LISTGROUPCAT] = new SelectList(new string[] { });

            Menu menu = MenuBusiness.All.Where(o => o.URL.Contains("/SchoolAccountArea/SchoolAccount")).FirstOrDefault();
            int menuID = menu.MenuID;
            List<int> lstGroupID = UserGroupBusiness.All.Where(ug => ug.UserID == globalInfo.UserAccountID).Select(gm => gm.GroupID).ToList();
            //Kiểm tra trong bảng GroupMenu quyền cao nhất
            int? permission = 0;
            List<int?> lstPermission = new List<int?>();
            foreach (var groupID in lstGroupID)
            {
                permission = GroupMenuBusiness.All.Where(gm => gm.GroupID == groupID && gm.MenuID == menuID).Select(gm => gm.Permission).Max();
                lstPermission.Add(permission);
            }
            if (lstPermission != null && lstPermission.Count() > 0)
            {
                permission = lstPermission.Max();
                ViewData[SchoolAccountConstants.PERMISSION] = permission.ToString();
            }
            else
            {
                ViewData[SchoolAccountConstants.PERMISSION] = "0";
            }
            //ViewData[SchoolAccountConstants.PERMISSION] = permission.ToString();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //SearchInfo["RoleID"] = new GlobalInfo().RoleID;
            //SearchInfo["UserAccountID"] = new GlobalInfo().UserAccountID;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            //Get view data here
            if (Session["IDEdit"] != null)
            {
                IEnumerable<SchoolAccountViewModel> lst = this._Search(SearchInfo, false, (int)Session["IDEdit"]);
                ViewData[SchoolAccountConstants.LIST_USERACCOUNT] = lst;
            }
            else
            {
                IEnumerable<SchoolAccountViewModel> lst = this._Search(SearchInfo, false);
                ViewData[SchoolAccountConstants.LIST_USERACCOUNT] = lst;
            }
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            SetViewDataPermission("SchoolAccount", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //SearchInfo["RoleID"] = new GlobalInfo().RoleID;
            //SearchInfo["UserAccountID"] = new GlobalInfo().UserAccountID;
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["UserName"] = frm.Username;
            SearchInfo["Genre"] = frm.Genre;
            SearchInfo["SchoolFacutyID"] = frm.SchoolFacutyID;
            SearchInfo["GroupCatID"] = frm.GroupCatID;
            SearchInfo["Status"] = frm.Status;
            SearchInfo["isNewAccount"] = frm.Status == 1 ? true : false;
            //add search info
            //
            GlobalInfo globalInfo = new GlobalInfo();
            Menu menu = MenuBusiness.All.Where(o => o.URL.Contains("/SchoolAccountArea/SchoolAccount")).FirstOrDefault();
            int menuID = menu.MenuID;
            List<int> lstGroupID = UserGroupBusiness.All.Where(ug => ug.UserID == globalInfo.UserAccountID).Select(gm => gm.GroupID).ToList();
            //Kiểm tra trong bảng GroupMenu quyền cao nhất
            int? permission = 0;
            List<int?> lstPermission = new List<int?>();
            foreach (var groupID in lstGroupID)
            {
                permission = GroupMenuBusiness.All.Where(gm => gm.GroupID == groupID && gm.MenuID == menuID).Select(gm => gm.Permission).Max();
                lstPermission.Add(permission);
            }
            if (lstPermission != null && lstPermission.Count() > 0)
            {
                permission = lstPermission.Max();
                ViewData[SchoolAccountConstants.PERMISSION] = permission.ToString();
            }
            else
            {
                ViewData[SchoolAccountConstants.PERMISSION] = "0";
            }

            IEnumerable<SchoolAccountViewModel> lst = this._Search(SearchInfo, false);
            ViewData[SchoolAccountConstants.LIST_USERACCOUNT] = lst;

            //Get view data here

            return PartialView("_List");
        }

        public ActionResult Create()
        {
            SchoolAccountViewModel model = new SchoolAccountViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID "] = _globalInfo.SchoolID;
            SearchInfo["CurrentEmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;

            IEnumerable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, SearchInfo);
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            //(Lấy lên danh sách giáo viên đã được cấp Account)
            var lstEmployeeWithAccount = UserAccountBusiness.SearchUser(SearchInfo).ToList();

            if (lstEmployee != null && lstEmployee.Count() > 0)
            {
                if (lstEmployeeWithAccount == null || lstEmployeeWithAccount.Count() == 0)
                {
                    model.ListEmployee = lstEmployee.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                                                   .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName)).ToList();
                }
                else if (lstEmployeeWithAccount != null && lstEmployeeWithAccount.Count() > 0)
                {
                    List<Employee> listemployee = new List<Employee>();
                    List<int> userbydeptid = lstEmployeeWithAccount.Where(u => u.EmployeeID.HasValue && u.IsActive == true).Select(u => u.EmployeeID.Value).ToList();
                    foreach (var employee in lstEmployee)
                    {
                        if (!userbydeptid.Contains(employee.EmployeeID))
                            listemployee.Add(employee);
                    }
                    model.ListEmployee = listemployee.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.FullName)).ToList();
                }
                else
                {
                    model.ListEmployee = new List<SMAS.Models.Models.Employee>();
                }

                //Lấy lên danh sách nhóm người dùng của phòng ban và fill vào từng Dropdownlist with checkbox trên Gridview                
                var listrole = GroupCatBusiness.GetGroupByRoleAndAdminID(_globalInfo.RoleID, schoolProfile.AdminID.Value).OrderBy(o => o.GroupName).ToList();
                model.ListRoleOfUser = new List<SelectListItem>();
                foreach (var role in listrole)
                {
                    model.ListRoleOfUser.Add(new SelectListItem { Text = role.GroupName, Value = role.GroupCatID.ToString(), Selected = false });
                }

                /* 
                 * Hệ thống gọi hàm SchoolFaculty.SearchBySchool(UserInfo.SchoolID) đê lấy lên danh sách tổ bộ môn của trường 
                 * và fill vào cboSchoolFacuty (Mặc định là [tất cả])
                 */
                var supervisingDeptOfUsers = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).OrderBy(o => o.FacultyName).ToList();

                ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(supervisingDeptOfUsers, "SchoolFacultyID", "FacultyName");
                ViewData[SchoolAccountConstants.LIST_GROUPOFROLE] = model.ListRoleOfUser;
                ViewData[SchoolAccountConstants.LISTSCHOOLACCOUNTEMPLOYEE] = model.ListEmployee;
                ViewData[SchoolAccountConstants.COUNTSCHOOLACCOUNTEMPLOYEE] = model.ListEmployee.Count;
                ViewData[SchoolAccountConstants.shortName] = schoolProfile.ShortName;
                ViewData[SchoolAccountConstants.ENABLED_NOTE] = model.ListRoleOfUser.Count > 0 ? false : true;
            }
            else
            {
                ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(new string[] { });
                ViewData[SchoolAccountConstants.LIST_GROUPOFROLE] = new SelectList(new string[] { });
                ViewData[SchoolAccountConstants.LISTSCHOOLACCOUNTEMPLOYEE] = new List<Employee>();
                ViewData[SchoolAccountConstants.COUNTSCHOOLACCOUNTEMPLOYEE] = 0;
                ViewData[SchoolAccountConstants.shortName] = schoolProfile.ShortName;
            }
            IDictionary<string, object> Search = new Dictionary<string, object>();
            Search["isActive"] = true;
            IQueryable<SchoolFaculty> listSchoolfaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Search).OrderBy(o => o.FacultyName);

            if (listSchoolfaculty != null)
            {
                ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(listSchoolfaculty.ToList(), "SchoolFacultyID", "FacultyName");
            }
            else ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(new string[] { });
            return View();
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        [ValidateAntiForgeryToken]
        public JsonResult CreateUser(SchoolAccountViewModel model, FormCollection col)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            var listchkemployee = col["rptGroupOfSchoolAccount"];

            if (!string.IsNullOrWhiteSpace(listchkemployee))
            {
                string[] employeeids = listchkemployee.Split(new Char[] { ',' });
                string username = "";
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
                //int userid = schoolProfile.AdminID.Value;
                int userid = globalInfo.UserAccountID;
                int roleid = globalInfo.RoleID;

                bool isadmin = false;
                int? useraccountid = 0;
                string listgrp;
                string[] groupids;

                //ten tien to + them ten tai khoan thay doi ngay 7/3/2013 hungnd+namta+tungnd
                string shortNameSchool = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value).ShortName + "_";

                Dictionary<int, string> dicUserName = new Dictionary<int, string>();
                Dictionary<int, string> dicRole = new Dictionary<int, string>();
                List<int> Employees = new List<int>();
                int Employeeid = 0;
                foreach (var employeeid in employeeids)
                {
                    if (!string.IsNullOrWhiteSpace(employeeid))
                    {
                        Employeeid = int.Parse(employeeid);
                        username = col["UserName_" + employeeid];
                        listgrp = col["Role_" + employeeid];
                        if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(listgrp))
                        {
                            throw new BusinessException("SchoolAccount_Label_TeacherNotEnoughInformation");
                        }
                        username = shortNameSchool + username;
                        if (username.Length <= 8)
                        {
                            throw new BusinessException("SchoolAccount_Label_UserNameLessThan8");
                        }
                        Employees.Add(Employeeid);
                        dicUserName.Add(Employeeid, username);
                        dicRole.Add(Employeeid, listgrp);
                    }
                }

                var checkIsDuplicateUserName = IsDuplicateUserName(Employees, dicUserName, dicRole);
                JsonMessage jsonMessage = (JsonMessage)checkIsDuplicateUserName.Data;
                MembershipCreateStatus status;

                List<Employee> lstEmployee = EmployeeBusiness.All.Where(e=>e.SchoolID==schoolProfile.SchoolProfileID && Employees.Contains(e.EmployeeID) && e.IsActive).ToList();
                if (jsonMessage != null && jsonMessage.Type == JsonMessage.SUCCESS)
                {
                    for (int i = employeeids.Count() - 1; i >= 0; i--)
                    {
                        List<int> listgroupid = new List<int>();
                        var employeeid = employeeids[i];
                        username = shortNameSchool + col["UserName_" + employeeid];
                        listgrp = col["Role_" + employeeid];
                        useraccountid = UserAccountBusiness.CreateUser(username, userid, isadmin, Int32.Parse(employeeid), out status);
                        UserAccountBusiness.Save();
                        if (useraccountid.HasValue && useraccountid.Value > 0)
                        {
                            groupids = listgrp.Split(new Char[] { ',' });
                            foreach (var groupid in groupids)
                            {
                                if (Int32.Parse(groupid) > 0)
                                    listgroupid.Add(Int32.Parse(groupid));
                            }
                            UserGroupBusiness.AssignGroupsForUser(userid, listgroupid, roleid, useraccountid.Value);
                            UserGroupBusiness.Save();
                        }

                    }
                    Session["IDEdit"] = useraccountid;
                    return Json(new JsonMessage(string.Format("{0}_{1}_{2}", Res.Get("SupervisingDept_Create_Done"), useraccountid, "0")));
                }
                else
                    return checkIsDuplicateUserName;
            }
            return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_NoCheck"), "Error"));
        }

        private JsonResult IsDuplicateUserName(List<int> listEmployee, Dictionary<int, string> dicUserName, Dictionary<int, string> dicRole)
        {
            Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9\-_]*$");
            Regex firstCharPattern = new Regex(@"[a-zA-Z]$");
            string listgrp;
            string username = string.Empty;
            UserAccount userAccount = new UserAccount();

            //dungnt77 them check login nam trung
            var checkDupLoginName = dicUserName.GroupBy(x => x.Value).Where(x => x.Count() > 1);
            if (checkDupLoginName.Count() > 0)
                return Json(new JsonMessage(string.Format("{0}", Res.Get("SupervisingDept_Validation_Error_DuplicateUserName")), "ErrorUserName"));

            foreach (var item in listEmployee)
            {
                username = dicUserName[item];
                listgrp = dicRole[item];
                if (!string.IsNullOrWhiteSpace(username))
                {
                    if (!firstCharPattern.IsMatch(username.Substring(0, 1)))
                        return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_FirstCharUserName"), item), "ErrorUserName"));

                    if (!objAlphaPattern.IsMatch(username))
                        return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_IsMatchUsername"), "ErrorUserName"));

                    userAccount = UserAccountBusiness.All.Where(o => o.EmployeeID == item && o.IsActive).FirstOrDefault();
                    if (userAccount != null)
                    {
                        return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_DuplicateEmployee"), item), "ErrorUserName"));
                    }

                    if (listgrp != null && listgrp.Count() > 0)
                    {
                        userAccount = UserAccountBusiness.All.Where(o => o.aspnet_Users.UserName == username && o.IsActive).FirstOrDefault();
                        if (userAccount != null)
                            return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_DuplicateUserName"), item), "ErrorUserName"));
                    }
                    else
                        return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error"), item), "Error"));
                }
                else
                    return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_UserName"), item), "ErrorUserName"));
            }
            return Json(new JsonMessage("", JsonMessage.SUCCESS));
        }


        public ActionResult GetListEmployeeWithAccount(int? id)
        {
            SchoolAccountViewModel model = new SchoolAccountViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int schoolid = new GlobalInfo().SchoolID.HasValue ? new GlobalInfo().SchoolID.Value : 0;
            SearchInfo["SchoolID"] = schoolid;
            SearchInfo["FacultyID"] = id.HasValue ? id.Value : 0;
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            SearchInfo["CurrentEmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            //Employee.SearchTeacher(UserInfo.SchoolID) – lstEmployee (Lấy lên danh sách giáo viên trong trường)
            var lstEmployee = EmployeeBusiness.SearchTeacher(schoolid, SearchInfo);

            //UserAccount.SearchUser(Employee.SchoolID = UserInfo.SchoolID) – lstEmployeeWithAccount
            //(Lấy lên danh sách giáo viên đã được cấp Account)
            var lstEmployeeWithAccount = UserAccountBusiness.SearchUser(SearchInfo).ToList();
            if (lstEmployee != null && lstEmployee.Count() > 0)
            {
                if (lstEmployeeWithAccount == null || lstEmployeeWithAccount.Count() == 0)
                {
                    model.ListEmployee = lstEmployee.ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.FullName)).ToList();
                }
                else if (lstEmployeeWithAccount != null && lstEmployeeWithAccount.Count() > 0)
                {
                    List<Employee> listemployee = new List<Employee>();
                    List<int> userbydeptid = lstEmployeeWithAccount.Where(u => u.EmployeeID.HasValue && u.IsActive == true).Select(u => u.EmployeeID.Value).ToList();
                    foreach (var employee in lstEmployee)
                    {
                        if (!userbydeptid.Contains(employee.EmployeeID))
                            listemployee.Add(employee);
                    }
                    model.ListEmployee = listemployee.ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.FullName)).ToList();
                }
                else
                {
                    model.ListEmployee = null;
                }
                //Lấy lên danh sách nhóm người dùng của phòng ban và fill vào từng Dropdownlist with checkbox trên Gridview
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolid);
                var listrole = GroupCatBusiness.GetGroupByRoleAndAdminID(new GlobalInfo().RoleID, schoolProfile.AdminID.Value).ToList();
                model.ListRoleOfUser = new List<SelectListItem>();
                foreach (var role in listrole)
                {
                    model.ListRoleOfUser.Add(new SelectListItem { Text = role.GroupName, Value = role.GroupCatID.ToString(), Selected = false });
                }
                /* 
                 * Hệ thống gọi hàm SchoolFaculty.SearchBySchool(UserInfo.SchoolID) đê lấy lên danh sách tổ bộ môn của trường 
                 * và fill vào cboSchoolFacuty (Mặc định là [tất cả])
                 */
                var supervisingDeptOfUsers = SchoolFacultyBusiness.SearchBySchool(schoolid, SearchInfo).ToList();

                ViewData[SchoolAccountConstants.LIST_LISTSCHOOLFACULTY] = new SelectList(supervisingDeptOfUsers, "SchoolFacultyID", "FacultyName");
                ViewData[SchoolAccountConstants.LIST_GROUPOFROLE] = model.ListRoleOfUser;
                ViewData[SchoolAccountConstants.LISTSCHOOLACCOUNTEMPLOYEE] = model.ListEmployee;
                ViewData[SchoolAccountConstants.COUNTSCHOOLACCOUNTEMPLOYEE] = model.ListEmployee.Count;
                ViewData[SchoolAccountConstants.ENABLED_NOTE] = model.ListRoleOfUser.Count > 0 ? false : true;
            }
            return PartialView("_ListEmployeeWithAccount");
        }

        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                throw new BusinessException("Common_Error_InternalError");
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = new GlobalInfo().UserAccountID;



            EditViewModel model = new EditViewModel();
            Dictionary<string, object> Search = new Dictionary<string, object>();
            Search["UserAccountID"] = id;

            UserAccount user = this.UserAccountBusiness.SearchUser(Search).ToList().FirstOrDefault();
            CheckPermissionForAction(user.EmployeeID, "Employee", 0, dic);
            if (user != null)
            {
                model.UserName = user.aspnet_Users.UserName;
                model.IsActive = user.aspnet_Users.aspnet_Membership.IsApproved == 1 ? true : false;
                if (user.Employee != null)
                {

                    model.UserName = user.aspnet_Users.UserName;
                    model.FullName = user.Employee.FullName + "-" + user.Employee.EmployeeCode;
                    model.UserAccountID = user.UserAccountID;
                    model.EmployeeID = user.EmployeeID.Value;
                    model.RoleID = user.UserRoles.FirstOrDefault() != null ? user.UserRoles.FirstOrDefault().RoleID : 0;
                    /*if (user.Employee.StaffPosition != null)
                        model.Position = user.Employee.StaffPosition.Resolution;*/
                    //01/10/2014. Lay chuc danh can bo
                    model.Position = user.Employee.Description;
                    if (user.Employee.Genre == false)
                        model.Genre = Res.Get("Common_Label_Female");
                    else model.Genre = Res.Get("Common_Label_Male");
                    if (user.Employee.BirthDate != null)
                        model.BirthDate = String.Format("{0:dd/MM/yyyy}", user.Employee.BirthDate);
                    if (user.Employee.Mobile != null)
                        model.PhoneNo = user.Employee.Mobile;
                    if (user.Employee.Email != null)
                        model.Email = user.Employee.Email;
                    if (user.Employee.HomeTown != null)
                        model.HomeTown = user.Employee.HomeTown;
                    if (user.Employee.PermanentResidentalAddress != null)
                        model.PermanentResidentalAddress = user.Employee.PermanentResidentalAddress;

                    model.SchoolFaculty = user.Employee.SchoolFaculty != null ? user.Employee.SchoolFaculty.FacultyName : string.Empty;
                }
            }


            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);
            IEnumerable<GroupCat> ListGroupOfRole = this.GroupCatBusiness.GetGroupByRoleAndAdminID(new GlobalInfo().RoleID, schoolProfile.AdminID.Value).OrderBy(o => o.GroupName).ToList();
            IEnumerable<GroupCat> ListGroupOfUser = this.UserGroupBusiness.GetGroupsOfUser(model.UserAccountID).OrderBy(o => o.GroupName);

            ViewData[SchoolAccountConstants.LIST_GROUPOFROLE] = ListGroupOfRole;
            ViewData[SchoolAccountConstants.LIST_GROUPOFUSER] = ListGroupOfUser;


            IDictionary<string, object> SearchEmp = new Dictionary<string, object>();
            SearchEmp["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            int SchoolID = new GlobalInfo().SchoolID != null ? new GlobalInfo().SchoolID.Value : 0;

            IEnumerable<Employee> lstEmployee = this.EmployeeBusiness.SearchTeacher(SchoolID, SearchEmp).ToList();

            if (lstEmployee != null)
            {
                int i = lstEmployee.Count();

                Dictionary<string, object> SearchUser = new Dictionary<string, object>();
                SearchUser["SchoolID"] = SchoolID;
                IQueryable<UserAccount> ListUser = this.UserAccountBusiness.SearchUser(SearchUser);

                IEnumerable<UserAccount> lstEmployeeWithAccount = ListUser.ToList();

                //var lstEmployeeAccount = from u in lstEmployee
                //        from v in lstEmployeeWithAccount
                //        where u.EmployeeID != v.EmployeeID.Value
                //        select u;

                //var lstEmployeeAccount = lstEmployee.Where(u => lstEmployeeWithAccount.Count(v => v.EmployeeID.Value != u.EmployeeID) > 0);
                List<SelectListItem> listEmpl = new List<SelectListItem>();
                List<int> listEmployID = lstEmployeeWithAccount.Where(o => o.EmployeeID.HasValue && o.IsActive == true).Select(o => o.EmployeeID.Value).ToList();
                foreach (var item in lstEmployee)
                {
                    if (!listEmployID.Contains(item.EmployeeID))
                    {
                        SelectListItem _mList = new SelectListItem();
                        _mList = new SelectListItem() { Text = item.FullName.Trim() + "-" + item.EmployeeCode, Value = item.EmployeeID.ToString() };
                        listEmpl.Add(_mList);
                    }
                }


                model.ListEmployee = listEmpl;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Edit(int? UserAccountID, EditViewModel model, FormCollection col, List<int> rptGroupOfDept)
        {
            if (!UserAccountID.HasValue)
            {
                throw new BusinessException("Common_Error_InternalError");
            }
            //check attt
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = new GlobalInfo().UserAccountID;



            GlobalInfo global = new GlobalInfo();
            int id = UserAccountID.Value;
            UserAccount userAccount = UserAccountBusiness.Find(id);
            CheckPermissionForAction(userAccount.EmployeeID, "Employee", 0, dic);
            bool isresetpass = string.IsNullOrWhiteSpace(col["chkResetPass"]) ? false : col["chkResetPass"].Trim() == "1" ? true : false;
            bool isactive = string.IsNullOrWhiteSpace(col["chkIsActive"]) ? false : col["chkIsActive"].Trim() == "1" ? true : false;
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(global.SchoolID.Value);
            int userid = schoolProfile.AdminID.Value;

            string userName = col["UserName"];
            string employid = col["EmployeeID"];
            string employeeName = Request.Form["EmployeeName"];

            GetEmployeeInfo(employeeName);
            // Lay thong tin user cu
            string oldUserName = userAccount.aspnet_Users.UserName;

            string shortName = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value).ShortName + "_";
            if (!oldUserName.Trim().ToLower().Equals(userName.Trim().ToLower()))
            {
                if (!userName.ToLower().Contains(shortName.ToLower()))
                {
                    return Json(new JsonMessage(Res.Get("SchoolAccount_Edit_ValidateUserName"), "error"));
                }
                Regex firstCharPattern = new Regex(@"[a-zA-Z]$");
                if (!firstCharPattern.IsMatch(userName.Substring(0, 1)))
                {
                    throw new BusinessException(string.Format("{0}", Res.Get("SupervisingDept_Validation_Error_FirstCharUserName")));
                }
                Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9\-_.]*$");
                bool IsMatchUsername = objAlphaPattern.IsMatch(userName);
                if (!IsMatchUsername)
                {
                    throw new BusinessException(Res.Get("SupervisingDept_Validation_Error_IsMatchUsername"));
                }
                if (userName.Length <= 8)
                {
                    throw new BusinessException("SchoolAccount_Label_UserNameLessThan8");
                }
            }
            //Xoa du lieu map cua group va user
            List<UserGroup> listUserGroup = UserGroupBusiness.GetUserGroupsByUser(id, global.RoleID).ToList();
            UserGroupBusiness.DeleteAll(listUserGroup);

            //insert du lieu map cua group va user
            if (employid != "0")
            {
                UserAccountBusiness.UpdateUser(userAccount.UserAccountID, userName, isresetpass, Int32.Parse(employid), isactive);
                UserAccountBusiness.Save();
            }
            else return Json(new JsonMessage(Res.Get("SchoolAccount_Error")));
            var usergroups = col["rptGroupOfDept"];
            if (usergroups != null)
                if (usergroups.Length > 0)
                {
                    string[] groupids = usergroups.Split(new Char[] { ',' });
                    List<int> listGrp = new List<int>();
                    foreach (var groupid in groupids)
                    {
                        if (Int32.Parse(groupid) > 0)
                        {
                            listGrp.Add(Int32.Parse(groupid));
                        }
                    }
                    UserGroupBusiness.AssignGroupsForUser(userid, listGrp, new GlobalInfo().RoleID, id);
                }
            UserGroupBusiness.Save();
            Session["IDEdit"] = id;

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetEmployeeInfo(string employ)
        {
            //string EmployeeName = "";
            ////if (employ != null)
            ////    for (int i = 0; i < employ.Length; i++)
            ////    {
            ////        if (employ[i] == '-' && (employ.Length - i) > 1)
            ////        {
            ////            EmployeeCode = employ.Substring(i + 1, employ.Length - i - 1);
            ////            break;
            ////        }
            ////    }
            //EmployeeName = employ;

            //int schoolID = new GlobalInfo().SchoolID.Value;

            //EditViewModel editViewModel = new EditViewModel();
            ////if (EmployeeCode != "")
            //{
            //    IDictionary<string, object> Search = new Dictionary<string, object>();
            //    Search["FullName"] = EmployeeName;
            //    IEnumerable<Employee> employees = EmployeeBusiness.SearchTeacher(schoolID, Search);
            //    if (employees != null)
            //    {
            //        Employee employee = new Employee();
            //        if (employees.Count() > 0)
            //        {
            //            employee = employees.FirstOrDefault();
            //            editViewModel.EmployeeID = employee.EmployeeID;
            //            editViewModel.FullName = employee.FullName;
            //            editViewModel.Position = employee.StaffPosition != null ? employee.StaffPosition.Resolution : string.Empty;
            //            editViewModel.Genre = employee.Genre == true ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
            //            editViewModel.BirthDate = String.Format("{0:dd/MM/yyyy}", employee.BirthDate);
            //            editViewModel.PhoneNo = employee.Mobile;
            //            editViewModel.Email = employee.Email;
            //            editViewModel.HomeTown = employee.HomeTown;
            //            editViewModel.PermanentResidentalAddress = employee.PermanentResidentalAddress;
            //            editViewModel.SchoolFaculty = employee.SchoolFaculty != null ? employee.SchoolFaculty.FacultyName : string.Empty;
            //        }
            //        else
            //        {
            //            throw new BusinessException("Không tồn tại cán bộ có tên " + employ);
            //        }



            //    }

            //}
            //return Json(editViewModel);
            string EmployeeCode = "";
            if (employ != null)
                for (int i = 0; i < employ.Length; i++)
                {
                    if (employ[i] == '-' && (employ.Length - i) > 1)
                    {
                        EmployeeCode = employ.Substring(i + 1, employ.Length - i - 1);
                        break;
                    }
                }

            int schoolID = new GlobalInfo().SchoolID.Value;

            EditViewModel editViewModel = new EditViewModel();
            if (EmployeeCode != "")
            {
                IDictionary<string, object> Search = new Dictionary<string, object>();
                Search["EmployeeCode"] = EmployeeCode;
                IEnumerable<Employee> employees = EmployeeBusiness.SearchTeacher(schoolID, Search);
                if (employees != null)
                {
                    Employee employee = new Employee();
                    if (employees.Count() > 0)
                        employee = employees.FirstOrDefault();
                    if (employee != null)
                    {
                        editViewModel.EmployeeID = employee.EmployeeID;
                        editViewModel.FullName = employee.FullName;
                        editViewModel.Position = employee.Description; //employee.StaffPosition != null ? employee.StaffPosition.Resolution : string.Empty;
                        editViewModel.Genre = employee.Genre == true ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
                        editViewModel.BirthDate = employee.BirthDate != null ? String.Format("{0:dd/MM/yyyy}", employee.BirthDate) : string.Empty;

                        editViewModel.PhoneNo = employee.Mobile;
                        editViewModel.Email = employee.Email;
                        editViewModel.HomeTown = employee.HomeTown;
                        editViewModel.PermanentResidentalAddress = employee.PermanentResidentalAddress;
                        editViewModel.SchoolFaculty = employee.SchoolFaculty != null ? employee.SchoolFaculty.FacultyName : string.Empty;
                    }
                    else
                    {
                        throw new BusinessException("Không tồn tại cán bộ có mã " + EmployeeCode);
                    }
                }
            }
            else
            {
                throw new BusinessException("Không tồn tại cán bộ có tên " + employ + " chưa có tài khoản");
            }
            return Json(editViewModel);
        }

        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = _globalInfo.UserAccountID;

            UserAccount user = this.UserAccountBusiness.Find(id);
            CheckPermissionForAction(user.EmployeeID, "Employee", 0, dic);
            if (id == _globalInfo.UserAccountID)
            {
                throw new BusinessException("Bạn không được xóa tài khoản đang đăng nhập.");
            }
            if (UserAccountBusiness.DeleteAccount(id))
            {
                //xoa du lieu trong bang UserGroup
                List<UserGroup> listUserGroup = UserGroupBusiness.GetUserGroupsByUser(id, _globalInfo.RoleID).ToList();
                UserGroupBusiness.DeleteAll(listUserGroup);
                UserGroupBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else return Json(new JsonMessage(Res.Get("Common_Label_DeleteFail")));
        }

        private IEnumerable<SchoolAccountViewModel> _Search(IDictionary<string, object> SearchInfo, bool isExportExcel, int idFirst = 0)
        {
            //IQueryable<UserAccount> query = this.UserAccountBusiness.SearchUser(SearchInfo as Dictionary<string, object>).Where(o => o.Employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).OrderByDescending(o => o.CreatedDate).ThenBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName);
            IQueryable<UserAccount> query = this.UserAccountBusiness.SearchUser(SearchInfo as Dictionary<string, object>);//.Where(o => o.Employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).OrderByDescending(o => o.CreatedDate).ThenBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName);
            List<UserAccount> lstTemp = query.Where(o => o.Employee != null).ToList();
            int GroupCatID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "GroupCatID");
            if (GroupCatID != 0)
            {
                lstTemp = lstTemp.Where(o => o.UserGroups.Select(m => m.GroupID).Contains(GroupCatID)).ToList();
            }
            var query2 = lstTemp.Where(o => o.Employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).OrderByDescending(o => o.CreatedDate).ThenBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName);

            //from e in lstTemp
            //    join em in EmployeeBusiness.All on e.EmployeeID equals em.EmployeeID
            //    where em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
            //    orderby e.CreatedDate descending, em.Name, em.FullName
            //    select e;

            MembershipUser membership;
            List<SchoolAccountViewModel> lst = query2.Select(o => new SchoolAccountViewModel
            {
                UserAccountID = o.UserAccountID,
                UserName = o.aspnet_Users != null ? o.aspnet_Users.UserName : null,

                Pass = o.aspnet_Users != null ? o.aspnet_Users.aspnet_Membership.Password : "",

                FullName = o.Employee.FullName,

                Position = o.Employee.Description,//o.Employee.StaffPosition != null ? o.Employee.StaffPosition.Resolution : "",

                BirthDate = o.Employee.BirthDate,

                PhoneNo = o.Employee.Mobile,
                FacultyName = o.Employee.SchoolFaculty.FacultyName,
                FirstLoginDate = o.FirstLoginDate,
                EmployeeID = o.EmployeeID,
                FacultyID = o.Employee != null ? o.Employee.SchoolFacultyID : null,
                StatusName = o.FirstLoginDate.HasValue ? "" : "Mới tạo"
            }).ToList();
            List<SchoolAccountViewModel> listSchool = lst;
            if (lst != null)
            {
                foreach (var item in listSchool)
                {
                    if (item.UserAccountID == idFirst)
                    {
                        item.Order = 1;
                    }

                    if (item.FirstLoginDate == null && item.UserName != null)
                    {
                        membership = Membership.GetUser(item.UserName);

                        if (membership == null)
                        {
                            item.Pass = "******";
                            continue;
                        }
                        if (membership.IsLockedOut)
                        {
                            item.Pass = "******";
                        }
                        else
                        {
                            if (isExportExcel)
                            {
                                item.Pass = SMAS.Business.Common.Utils.GenPassRandom(SystemParamsInFile.MAX_PASS_LENTGTH);
                                //if (membership.GetPassword().Length < 7)
                                //    item.Pass = membership.GetPassword().Substring(0, 7);
                                //else
                                //{
                                //    item.Pass = membership.GetPassword();
                                //}
                            }
                            else
                            {
                                item.Pass = "******";
                            }
                        }
                    }
                    else
                    {
                        item.Pass = "******";
                    }
                }
            }
            if (idFirst != 0)
            {

            }

            return listSchool.OrderByDescending(o => o.Order);
        }

        public FileResult ExportExcel(string Username, string FullName, string Genre, int SchoolFacutyID, int GroupCatID, string strUserAccountID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            List<SchoolFaculty> lstFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).ToList();
            SearchInfo["UserName"] = Username;
            SearchInfo["FullName"] = FullName;
            if (!string.IsNullOrEmpty(Genre))
            {
                SearchInfo["Genre"] = "true".Equals(Genre) ? true : false;
            }

            SearchInfo["SchoolFacutyID"] = SchoolFacutyID;
            SearchInfo["GroupCatID"] = GroupCatID;
            List<int> lstUserAccountID = strUserAccountID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            SearchInfo["lstUserAccountID"] = lstUserAccountID;

            //// Thuc hien truy van du lieu
            List<SchoolAccountViewModel> lst = this._Search(SearchInfo, true).ToList();
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HT" + "/" + "DanhSachTaiKhoanGV.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "G6"), 1, 1);
            IDictionary<int, string> lstUserAccount = new Dictionary<int, string>();
            if (lst.Count() > 0)
            {
                //List<SchoolAccountViewModel> lstSchool = lst.OrderBy(o => o.FullName).ToList();
                int StartRow = 6;
                int StartCol = 1;
                for (int i = 0; i < lst.Count; i++)
                {
                    SchoolAccountViewModel sp = lst[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, sp.FullName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, sp.BirthDate.HasValue ? sp.BirthDate.Value.ToString("dd/MM/yyyy") : null);
                    if (!string.IsNullOrEmpty(sp.StatusName))
                    {
                        lstUserAccount[sp.UserAccountID] = sp.Pass;
                    }
                    if (sp.FacultyID.HasValue)
                    {
                        SchoolFaculty sf = lstFaculty.Where(o => o.SchoolFacultyID == sp.FacultyID).FirstOrDefault();
                        if (sf != null)
                        {
                            Sheet.SetCellValue(StartRow + i, StartCol + 3, sf.FacultyName);
                        }
                        else
                        {
                            Sheet.SetCellValue(StartRow + i, StartCol + 3, "");
                        }
                    }
                    else
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 3, "");
                    }

                    Sheet.SetCellValue(StartRow + i, StartCol + 4, sp.UserName);
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, sp.Pass);

                    Sheet.CopyPaste(Sheet.GetRange("A6", "G6"), StartRow + i, 1, true);

                }
                Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                dicVarable["SchoolName"] = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value).SchoolName;
                dicVarable["SchoolFaculty"] = ((GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                                                || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN))
                                                ? Res.Get("Lable_Faculty_MN") : Res.Get("SchoolAccount_Label_SchoolFacultyName");
                Sheet.FillVariableValue(dicVarable);

            }
            else
            {
                Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                dicVarable["SchoolName"] = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value).SchoolName;
                dicVarable["SchoolFaculty"] = ((GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                                                || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN))
                                                ? Res.Get("Lable_Faculty_MN") : Res.Get("SchoolAccount_Label_SchoolFacultyName");
                Sheet.FillVariableValue(dicVarable);
            }
            UserAccountBusiness.ChangePasswordByListUseraccount(lstUserAccount);

            oBook.GetSheet(1).Delete();
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string ReportName = "DanhSachBanGiaoTaiKhoan.xls";
            result.FileDownloadName = ReportName;
            return result;
        }

        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(string strUserAccountID)
        {
            List<int> lstUserAccountID = strUserAccountID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            UserAccountBusiness.ResetPasswordByListUserAccount(lstUserAccountID);
            return Json(new JsonMessage("ResetPassword Success", "success"));
        }
    }
}





