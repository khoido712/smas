﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SchoolAccountArea
{
    public class SchoolAccountConstants
    {
        public const string LIST_USERACCOUNT = "listUserAccount";
        public const string LIST_LISTSEX = "LISTSEX";
        public const string LIST_LISTSCHOOLFACULTY = "LISTSCHOOLFACULTY";
        public const string LIST_LISTGROUPCAT = "LISTSCHOOLGROUPCAT";
        public const string LIST_STATUS = "LISTSTATUS";
        public const string LIST_GROUPOFROLE = "LIST_GROUPOFROLE";
        public const string LIST_GROUPOFUSER = "LIST_GROUPOFUSER";
        public const string LISTSCHOOLACCOUNTEMPLOYEE = "LIST_SCHOOLACCOUNTEMPLOYEE";
        public const string ALL = "[Tất cả]";
        public const string NEWACTIVE = "Mới tạo";
        public const string MSG = "Chưa chọn cán bộ";
        public const string COUNTSCHOOLACCOUNTEMPLOYEE = "COUNTSCHOOLACCOUNTEMPLOYEE";
        public const string PERMISSION = "Permission";
        public const string shortName = "shortName";
        public const string ENABLED_NOTE = "EnabledNote";
        
    }
}