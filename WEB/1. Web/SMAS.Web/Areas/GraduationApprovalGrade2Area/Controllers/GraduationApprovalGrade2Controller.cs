﻿using Newtonsoft.Json;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.GraduationApprovalGrade2Area.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area.Controllers
{
    public class GraduationApprovalGrade2Controller : BaseController
    {
        //
        // GET: /GraduationApprovalGrade2Area/GraduationApprovalGrade2/

        private readonly IGraduationApprovalBusiness GraduationApprovalBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilGraduationBusiness PupilGraduationBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        public GraduationApprovalGrade2Controller(IGraduationApprovalBusiness GraduationApprovalBusiness,
            IProvinceBusiness ProvinceBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilOfSchoolBusiness PupilOfSchoolBusiness, IClassProfileBusiness ClassProfileBusiness,
            IPupilProfileBusiness PupilProfileBusiness, IPupilGraduationBusiness PupilGraduationBusiness,
            IUserAccountBusiness UserAccountBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness
            )
        {
            this.GraduationApprovalBusiness = GraduationApprovalBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilGraduationBusiness = PupilGraduationBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        public ActionResult Index()
        {
            SetData();
            return View();
        }

        public void SetData()
        {
            int currentYear = DateTime.Now.Year;
            currentYear = currentYear - 1;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }
            GlobalInfo global = new GlobalInfo();
            List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
            if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
            {
                currentYear = lsYear.FirstOrDefault();
            }
            List<ComboObject> lscbYear = new List<ComboObject>();
            if (lsYear != null && lsYear.Count > 0)
            {
                foreach (var year in lsYear)
                {
                    string value = year.ToString() + " - " + (year + 1).ToString();
                    ComboObject cb = new ComboObject(year.ToString(), value);
                    lscbYear.Add(cb);
                }
            }
            ViewData[GraduationApprovalGrade2Constants.ACADEMIC] = new SelectList(lscbYear, "key", "value", currentYear);

            int academic = SMAS.Business.Common.Utils.GetInt(lscbYear.Where(x => x.key == currentYear.ToString()).Select(x => x.key).FirstOrDefault().ToString());
            var lstSPBO = GetSchool(academic);
            ViewData[GraduationApprovalGrade2Constants.SCHOOL] = new SelectList(lstSPBO, "SchoolProfileID", "SchoolName");

            ViewData[GraduationApprovalGrade2Constants.STATUS] = new SelectList(CommonList.StatusGraduationAprroval(), "key", "value");
        }

        public PartialViewResult Search(SearchModel frm, GridCommand command)
        {
            Utils.Utils.TrimObject(frm);
            int schoolID = 0;
            if (frm.SchoolID != null && frm.SchoolID != 0)
            {
                schoolID = frm.SchoolID.Value;
            }
            int year = 0;
            if (frm.YearID != null && frm.YearID != 0)
            {
                year = frm.YearID.Value;
            }
            int status = 0;
            if (frm.StatusID != null && frm.StatusID != 0)
            {
                status = frm.StatusID.Value;
            }

            //F_GET_CLASS_NOT_SUMMED_UP();
            int count;
            var model = GetDataForSearch(schoolID, year, status, 10, 1, out count);
            ViewData["Total"] = count;
            return PartialView("_List", model);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchModel frm, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(frm);

            int schoolID = 0;
            if (frm.SchoolID != null && frm.SchoolID != 0)
            {
                schoolID = frm.SchoolID.Value;
            }
            int year = 0;
            if (frm.YearID != null && frm.YearID != 0)
            {
                year = frm.YearID.Value;
            }
            int status = 0;
            if (frm.StatusID != null && frm.StatusID != 0)
            {
                status = frm.StatusID.Value;
            }

            int count;
            var model = GetDataForSearch(schoolID, year, status, pageSize, currentPage, out count);

            int totalRecord = count;
            ViewData["Total"] = totalRecord;
            return View(new GridModel<GraduationApprovalViewModel>
            {
                Total = totalRecord,
                Data = model
            });
        }

        public JsonResult Approval(string arr, bool isCancel)
        {
            try
            {
                List<GraduationApprovalViewModel> lstData = JsonConvert.DeserializeObject<List<GraduationApprovalViewModel>>(arr).ToList();
                List<GraduationApproval> lstSave = new List<GraduationApproval>();
                GraduationApproval objSave = null;
                foreach (var item in lstData)
                {
                    objSave = new GraduationApproval();
                    objSave.SchoolID = item.schoolID;
                    objSave.AcademicYearID = item.academicYearID;
                    objSave.Year = item.year;
                    objSave.UserApprove = _globalInfo.UserAccountID;
                    objSave.Note = item.note;
                    lstSave.Add(objSave);
                }
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = lstSave.FirstOrDefault().Year;
                dic["IsCancel"] = isCancel;
                GraduationApprovalBusiness.SaveBySubSuperVising(lstSave, dic);
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            catch
            {
                return Json(new JsonMessage(Res.Get("Save_Failed"), "error"));
            }
        }

        public FileResult ExportExcel(int year, int schoolID, int status)
        {
            string fileName = "";
            Stream excel = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["Year"] = year;
            dic["Status"] = status;

            excel = ReportExcel(dic, out fileName);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        public Stream ReportExcel(Dictionary<string, object> dic, out string fileName)
        {

            int schoolID = Business.Common.Utils.GetInt(dic, "SchoolID");
            int year = Business.Common.Utils.GetInt(dic, "Year");
            int status = Business.Common.Utils.GetInt(dic, "Status");
            string reportCode = "REPORT_GRADUATION_APPROVAL";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            string outputName = reportDef.OutputNamePattern;
            fileName = ReportUtils.RemoveSpecialCharacters(outputName) + "." + reportDef.OutputFormat;
            UserAccount Sub = UserAccountBusiness.Find(_globalInfo.UserAccountID);
            string SubSuperVising = "";
            if (Sub != null) 
            {
                SubSuperVising = Sub.aspnet_Users.UserName;
            }
            IVTWorksheet regularSheet4 = oBook.GetSheet(4);
            IVTWorksheet regularSheet1 = oBook.GetSheet(1);
            IVTWorksheet regularSheet2 = oBook.GetSheet(2);
            IVTWorksheet regularSheet3 = oBook.GetSheet(3);
            IVTWorksheet regularSheet5 = oBook.GetSheet(5);
            IVTWorksheet regularSheet6 = oBook.GetSheet(6);
            IVTWorksheet sheet = oBook.GetSheet(7);
            IVTRange rang1 = sheet.GetRange("A2", "T4");

            regularSheet4.SetCellValue(2, 1, SubSuperVising.ToUpper());
            var lstData = GetDataForExportExcel(schoolID, year, status).ToList();
            int startRow = 9;
            int stt = 1;
            foreach (var item in lstData)
            {
                sheet.SetCellValue(startRow, 1, stt);
                sheet.SetCellValue(startRow, 2, item.schoolName);
                sheet.SetCellValue(startRow, 3, item.expectedPupilSum);
                sheet.SetCellValue(startRow, 4, item.expectedFemalePupilSum);
                sheet.SetCellValue(startRow, 5, "=IF(D" + startRow + "=0,0,ROUND(D" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 6, item.expectedRegionPupilSum);
                sheet.SetCellValue(startRow, 7, "=IF(F" + startRow + "=0,0,ROUND(F" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 8, 0);
                sheet.SetCellValue(startRow, 9, 0);
                sheet.SetCellValue(startRow, 10, item.achievementPupilSum);
                sheet.SetCellValue(startRow, 11, "=IF(J" + startRow + "=0,0,ROUND(J" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 12, item.achievementFemalePupilSum);
                sheet.SetCellValue(startRow, 13, "=IF(L" + startRow + "=0,0,ROUND(L" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 14, item.achievementRegionPupilSum);
                sheet.SetCellValue(startRow, 15, "=IF(N" + startRow + "=0,0,ROUND(N" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 16, item.goodGraduation);
                sheet.SetCellValue(startRow, 17, "=IF(P" + startRow + "=0,0,ROUND(P" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 18, item.normalGraduation);
                sheet.SetCellValue(startRow, 19, "=IF(R" + startRow + "=0,0,ROUND(R" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 20, "");
                regularSheet4.SetRowHeight(startRow, 25);               
                startRow++;
                stt++;
            }
            sheet.SetCellValue(startRow, 3, "=SUM(C" + 9 + ":C" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + 9 + ":D" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 5, "=IF(D" + startRow + "=0,0,ROUND(D" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 6, "=SUM(F" + 9 + ":F" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 7, "=IF(F" + startRow + "=0,0,ROUND(F" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 8, 0);
            sheet.SetCellValue(startRow, 9, 0);
            sheet.SetCellValue(startRow, 10, "=SUM(J" + 9 + ":J" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 11, "=IF(J" + startRow + "=0,0,ROUND(J" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 12, "=SUM(L" + 9 + ":L" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 13, "=IF(L" + startRow + "=0,0,ROUND(L" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 14, "=SUM(N" + 9 + ":N" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 15, "=IF(N" + startRow + "=0,0,ROUND(N" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 16, "=SUM(P" + 9 + ":P" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 17, "=IF(P" + startRow + "=0,0,ROUND(P" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetCellValue(startRow, 18, "=SUM(R" + 9 + ":R" + (startRow - 1) + ")");
            sheet.SetCellValue(startRow, 19, "=IF(R" + startRow + "=0,0,ROUND(R" + startRow + "*100/C" + startRow + ",2))");
            sheet.SetRowHeight(startRow, 25);
            sheet.GetRange(startRow, 1, startRow, 2).Merge();
            sheet.SetCellValue(startRow, 1, "Tổng số");
            sheet.GetRange(startRow, 1, startRow , 1).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
            regularSheet4.SetRowHeight(startRow, 25);  
            startRow++;

            sheet.GetRange(9, 1, startRow - 1, 20).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(9, 20, startRow - 1, 20).WrapText();
            sheet.GetRange(9, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(9, 1, startRow - 1, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(9, 3, startRow - 1, 20).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(9, 3, startRow - 1, 20).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.SetFontName("Times New Roman", 10);

            IVTRange rang2 = sheet.GetRange("A9", "T" + startRow + 1);

            regularSheet4.CopyPaste(rang2, 9, 1);
            regularSheet4.CopyPaste(rang1, startRow + 1, 1);            
            //sheet.CopyPaste(rang2, startRow + 1, 15);
            regularSheet1.Delete();
            regularSheet2.Delete();
            regularSheet3.Delete();
            regularSheet5.Delete();
            regularSheet6.Delete();
            sheet.Delete();    
            return oBook.ToStream();
        }

        public List<GraduationApprovalViewModel> GetDataForSearch(int schoolID, int year, int status, int pageSize, int currentPage, out int count)
        {
            count = 0;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", _globalInfo.ProvinceID },
                    { "DistrictID", _globalInfo.DistrictID}
                };
          
            IQueryable<GraduationApprovalViewModel> queryGA = (from sp in SchoolProfileBusiness.Search(SearchInfo).Where(x => (x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS) && x.IsActive == true )
                                                    join ac in AcademicYearBusiness.All.Where(x=>x.Year == year && x.IsActive == true) on sp.SchoolProfileID equals ac.SchoolID
                                                    join ga in GraduationApprovalBusiness.All.Where(x=>x.Year == year)
                                                            on new { p1 = ac.SchoolID, p2 = ac.AcademicYearID, p3 = ac.Year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
                                                    from ga in ap.DefaultIfEmpty()
                                                             select new GraduationApprovalViewModel
                                                    {
                                                        schoolID = ac.SchoolID,
                                                        academicYearID = ac.AcademicYearID,
                                                        year = ac.Year,
                                                        schoolName = sp.SchoolName,
                                                        expectedPupilSum = 0,
                                                        achievementPupilSum = 0,
                                                        status = ga.Status,
                                                        userApprovalID = ga.UserApprove,
                                                        date = ga.ModifiedDate,
                                                        note = ga.Note,
                                                    }).Distinct().OrderBy(o => o.schoolName.ToLower());
            if (status != 0) 
            {                
                switch (status)
                {
                    case 1: queryGA = queryGA.Where(X => X.status == null);
                        count = queryGA.Count();
                        queryGA = queryGA.Skip((currentPage - 1) * pageSize).Take(pageSize);
                        break;
                    case 2: queryGA = queryGA.Where(x => x.status == 2);
                        count = queryGA.Count();
                        queryGA = queryGA.Skip((currentPage - 1) * pageSize).Take(pageSize);
                        break;
                    case 3: queryGA = queryGA.Where(x => x.status == 1);
                        count = queryGA.Count();
                        queryGA = queryGA.Skip((currentPage - 1) * pageSize).Take(pageSize);
                        break;
                    case 4: queryGA = queryGA.Where(x => x.status == 3);
                        count = queryGA.Count();
                        queryGA = queryGA.Skip((currentPage - 1) * pageSize).Take(pageSize);
                        break;
                }
            }
            else 
            {
                count = queryGA.Count();
                queryGA = queryGA.Skip((currentPage - 1) * pageSize).Take(pageSize);
            }

            if (schoolID != 0)
            {
                queryGA = queryGA.Where(x => x.schoolID == schoolID);
                count = queryGA.Count();
            }

            var lstGA = queryGA.ToList();
            List<int> lstSchoolID = lstGA.Select(x => x.schoolID).Distinct().ToList();

            IQueryable<GraduationApprovalViewModel> model = (from ac in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.Year == year && lstSchoolID.Contains(x.SchoolID))
                                                             join pos in PupilOfSchoolBusiness.All on ac.SchoolID equals pos.SchoolID
                                                             join poc in PupilOfClassBusiness.All.Where(x => (x.Status == 1 || x.Status == 2) && x.ClassProfile.IsActive == true && x.PupilProfile.IsActive == true && x.ClassProfile.EducationLevelID == 9 )
                                                                      on new { pos.PupilID, pos.SchoolID, ac.AcademicYearID } equals new { poc.PupilID, poc.SchoolID, poc.AcademicYearID }
                                                             join pg in PupilGraduationBusiness.All.Where(x => x.Status == 1)
                                                                    on new { p1 = ac.SchoolID, p2 = ac.AcademicYearID, p3 = poc.PupilID, p4 = poc.ClassID } equals new { p1 = pg.SchoolID, p2 = pg.AcademicYearID, p3 = pg.PupilID, p4 = pg.ClassID } into ps
                                                             from pg in ps.DefaultIfEmpty()                                                                     
                                                             group new { pg, poc, ac } by ac.SchoolID into result
                                                             select new GraduationApprovalViewModel
                                                             {
                                                                 schoolID = result.Key,
                                                                 academicYearID = result.FirstOrDefault().ac.AcademicYearID,
                                                                 year = result.FirstOrDefault().ac.Year,
                                                                 schoolName = result.FirstOrDefault().ac.SchoolProfile.SchoolName,
                                                                 expectedPupilSum = result.Count(),
                                                                 achievementPupilSum = result.Where(x => x.pg.SchoolID != null).Count(),
                                                             }).OrderBy(o => o.schoolName.ToLower());

            var lstModel = model.ToList();
            List<int?> lstUserID = lstGA.Where(x => x.userApprovalID != null).Select(x => x.userApprovalID).ToList();
            List<UserAccount> lstSupervisingDept = UserAccountBusiness.All.Where(x => lstUserID.Contains(x.UserAccountID) && x.IsActive == true).ToList();
            GraduationApprovalViewModel objGA = null;
            foreach (var item in lstGA) 
            {
                objGA = lstModel.Where(x => x.schoolID == item.schoolID).FirstOrDefault();
                var objSubSuperVising = lstSupervisingDept.Where(x => x.UserAccountID == item.userApprovalID).Select(x => x.aspnet_Users.UserName).FirstOrDefault();
                if (objGA != null) 
                {
                    item.expectedPupilSum = objGA.expectedPupilSum;
                    item.achievementPupilSum = objGA.achievementPupilSum;                                    
                }
                item.graduationApproStatus = item.status == 1 ? "Chờ xét duyệt" : (item.status == 2 ? "Đã xét duyệt" : (item.status == 3 ? "Hủy xét duyệt" : "Chưa gửi xét duyệt"));
                if (objSubSuperVising != null && item.date != null && (item.status == 2 || item.status == 3))
                {
                    //item.graduationApproStatus = item.graduationApproStatus + " </br> bởi " + objSubSuperVising + " lúc " + item.date.Value.Hour + ":" + item.date.Value.Minute + "  " + item.date.Value.ToShortDateString();
                    item.graduationApproStatus = item.graduationApproStatus + " </br> bởi " + objSubSuperVising + " lúc " + (item.date.Value.Hour.ToString().Length == 1 ? "0" + item.date.Value.Hour : item.date.Value.Hour.ToString()) +
                        ":" + (item.date.Value.Minute.ToString().Length == 1 ? "0" + item.date.Value.Minute : item.date.Value.Minute.ToString() ) + "  " + item.date.Value.ToShortDateString();
                }   
                item.note = item.note == null ? "" : item.note;
            }
          
            return lstGA;
        }

        public List<GraduationApprovalViewModel> GetDataForExportExcel(int schoolID, int year, int status)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", _globalInfo.ProvinceID },
                    { "DistrictID", _globalInfo.DistrictID}
                };

            IQueryable<GraduationApprovalViewModel> queryGA = (from sp in SchoolProfileBusiness.Search(SearchInfo).Where(x => (x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS) && x.IsActive == true)
                                                               join ac in AcademicYearBusiness.All.Where(x => x.Year == year && x.IsActive == true) on sp.SchoolProfileID equals ac.SchoolID
                                                               join ga in GraduationApprovalBusiness.All.Where(x => x.Year == year)
                                                                       on new { p1 = ac.SchoolID, p2 = ac.AcademicYearID, p3 = ac.Year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
                                                               from ga in ap.DefaultIfEmpty()
                                                               select new GraduationApprovalViewModel
                                                               {
                                                                   schoolID = ac.SchoolID,
                                                                   academicYearID = ac.AcademicYearID,
                                                                   year = ac.Year,
                                                                   schoolName = sp.SchoolName,
                                                                   expectedPupilSum = 0,
                                                                   expectedFemalePupilSum = 0,
                                                                   expectedRegionPupilSum = 0,
                                                                   achievementPupilSum = 0,
                                                                   achievementFemalePupilSum = 0,
                                                                   achievementRegionPupilSum = 0,
                                                                   goodGraduation = 0,
                                                                   normalGraduation = 0,
                                                                   status = ga.Status,
                                                                   userApprovalID = ga.UserApprove,
                                                                   date = ga.ModifiedDate,
                                                                   note = ga.Note,
                                                               }).Distinct().OrderBy(o => o.schoolName.ToLower());
            if (status != 0)
            {
                switch (status)
                {
                    case 1: queryGA = queryGA.Where(x => x.status == null);                     
                        break;
                    case 2: queryGA = queryGA.Where(x => x.status == 2);                       
                        break;
                    case 3: queryGA = queryGA.Where(x => x.status == 1);                   
                        break;
                    case 4: queryGA = queryGA.Where(x => x.status == 3);                      
                        break;
                }
            }

            if (schoolID != 0)
            {
                queryGA = queryGA.Where(x => x.schoolID == schoolID);
            }

            var lstGA = queryGA.ToList();
            List<int> lstSchoolID = lstGA.Select(x => x.schoolID).Distinct().ToList();

            IQueryable<GraduationApprovalViewModel> model = (from ac in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.Year == year && lstSchoolID.Contains(x.SchoolID))
                                                             join pos in PupilOfSchoolBusiness.All on ac.SchoolID equals pos.SchoolID
                                                             join poc in PupilOfClassBusiness.All.Where(x => (x.Status == 1 || x.Status == 2) && x.ClassProfile.IsActive == true && x.PupilProfile.IsActive == true && x.ClassProfile.EducationLevelID == 9)
                                                                      on new { pos.PupilID, pos.SchoolID, ac.AcademicYearID } equals new { poc.PupilID, poc.SchoolID, poc.AcademicYearID }
                                                             join pg in PupilGraduationBusiness.All.Where(x => x.Status == 1)
                                                                    on new { p1 = ac.SchoolID, p2 = ac.AcademicYearID, p3 = poc.PupilID, p4 = poc.ClassID } equals new { p1 = pg.SchoolID, p2 = pg.AcademicYearID, p3 = pg.PupilID, p4 = pg.ClassID } into ps
                                                             from pg in ps.DefaultIfEmpty()
                                                             group new { pg, poc, ac } by ac.SchoolID into result
                                                             select new GraduationApprovalViewModel
                                                             {
                                                                 schoolID = result.Key,
                                                                 academicYearID = result.FirstOrDefault().ac.AcademicYearID,
                                                                 year = result.FirstOrDefault().ac.Year,
                                                                 schoolName = result.FirstOrDefault().ac.SchoolProfile.SchoolName,
                                                                 expectedPupilSum = result.Count(),
                                                                 expectedFemalePupilSum = result.Where(x => x.poc.PupilProfile.Genre == 0).Count(),
                                                                 expectedRegionPupilSum = result.Where(x => x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79).Count(),
                                                                 achievementPupilSum = result.Where(x => x.pg.SchoolID != null).Count(),
                                                                 achievementFemalePupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.PupilProfile.Genre == 0).Count(),
                                                                 achievementRegionPupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79).Count(),
                                                                 goodGraduation = result.Where(x => x.pg.SchoolID != null && x.pg.GraduationGrade == 1).Count(),
                                                                 normalGraduation = result.Where(x => x.pg.SchoolID != null && x.pg.GraduationGrade == 2).Count(),
                                                             }).OrderBy(o => o.schoolName.ToLower());

            var lstModel = model.ToList();           
            GraduationApprovalViewModel objGA = null;
            foreach (var item in lstGA)
            {
                objGA = lstModel.Where(x => x.schoolID == item.schoolID).FirstOrDefault();             
                if (objGA != null)
                {
                    item.expectedPupilSum = objGA.expectedPupilSum;
                    item.expectedFemalePupilSum = objGA.expectedFemalePupilSum;
                    item.expectedRegionPupilSum = objGA.expectedRegionPupilSum;
                    item.achievementPupilSum = objGA.achievementPupilSum;
                    item.achievementFemalePupilSum = objGA.achievementFemalePupilSum;
                    item.achievementRegionPupilSum = objGA.achievementRegionPupilSum;
                    item.goodGraduation = objGA.goodGraduation;
                    item.normalGraduation = objGA.normalGraduation;
                }
            }

            return lstGA;
        }

        //public IQueryable<GraduationApprovalViewModel> GetDataForExportExcel(int schoolID, int year, int status)
        //{
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
        //            { "ProvinceID", _globalInfo.ProvinceID },
        //            { "DistrictID", _globalInfo.DistrictID}
        //        };
        //    IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY ||
        //                                                                            x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY ||
        //                                                                            x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY ||
        //                                                                            x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL ||
        //                                                                            x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS);

        //    if (schoolID != 0)
        //    {
        //        lstSP = lstSP.Where(x => x.SchoolProfileID == schoolID);
        //    }

        //    IQueryable<GraduationApprovalViewModel> model = (from sp in lstSP.Where(x => x.IsActive == true)
        //                                                     join ac in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.Year == year) on sp.SchoolProfileID equals ac.SchoolID
        //                                                     join pos in PupilOfSchoolBusiness.All on sp.SchoolProfileID equals pos.SchoolID
        //                                                     join poc in PupilOfClassBusiness.All.Where(x => (x.Status == 1 || x.Status == 2) && x.ClassProfile.IsActive == true && x.PupilProfile.IsActive == true)
        //                                                              on new { pos.PupilID, pos.SchoolID, ac.AcademicYearID } equals new { poc.PupilID, poc.SchoolID, poc.AcademicYearID }
        //                                                     join pg in PupilGraduationBusiness.All.Where(x => x.Status == 1)
        //                                                            on new { p1 = sp.SchoolProfileID, p2 = ac.AcademicYearID, p3 = poc.PupilID, p4 = poc.ClassID } equals new { p1 = pg.SchoolID, p2 = pg.AcademicYearID, p3 = pg.PupilID, p4 = pg.ClassID } into ps
        //                                                     from pg in ps.DefaultIfEmpty()
        //                                                     //join ga in GraduationApprovalBusiness.All
        //                                                     //       on new { p1 = sp.SchoolProfileID, p2 = ac.AcademicYearID, p3 = ac.Year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
        //                                                     //from ga in ap.DefaultIfEmpty()
        //                                                     group new { pg, poc, ac } by sp.SchoolProfileID into result
        //                                                     select new GraduationApprovalViewModel
        //                                                     {
        //                                                         schoolID = result.Key,
        //                                                         academicYearID = result.FirstOrDefault().ac.AcademicYearID,
        //                                                         year = result.FirstOrDefault().ac.Year,
        //                                                         schoolName = result.FirstOrDefault().ac.SchoolProfile.SchoolName,
        //                                                         expectedPupilSum = result.Where(x => x.poc.ClassProfile.EducationLevelID == 9).Count(),
        //                                                         expectedFemalePupilSum = result.Where(x => x.poc.PupilProfile.Genre == 0 && x.poc.ClassProfile.EducationLevelID == 9).Count(),
        //                                                         expectedRegionPupilSum = result.Where(x => x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79 && x.poc.ClassProfile.EducationLevelID == 9).Count(),
        //                                                         achievementPupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9).Count(),
        //                                                         achievementFemalePupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.poc.PupilProfile.Genre == 0).Count(),
        //                                                         achievementRegionPupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79).Count(),
        //                                                         goodGraduation = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.pg.GraduationGrade == 1).Count(),
        //                                                         normalGraduation = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.pg.GraduationGrade == 2).Count(),
        //                                                     }).OrderBy(o => o.schoolName.ToLower());

        //    model = (from sp in model
        //             join ga in GraduationApprovalBusiness.All
        //                 on new { p1 = sp.schoolID, p2 = sp.academicYearID, p3 = sp.year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
        //             from ga in ap.DefaultIfEmpty()
        //             select new
        //             {
        //                 sp = sp,
        //                 ga = ga,
        //             }).ToList().Select(o => new GraduationApprovalViewModel
        //             {
        //                 schoolID = o.sp.schoolID,
        //                 academicYearID = o.sp.academicYearID,
        //                 schoolName = o.sp.schoolName,
        //                 expectedPupilSum = o.sp.expectedPupilSum,
        //                 expectedFemalePupilSum = o.sp.expectedFemalePupilSum,
        //                 expectedRegionPupilSum = o.sp.expectedRegionPupilSum,
        //                 achievementPupilSum = o.sp.achievementPupilSum,
        //                 achievementFemalePupilSum = o.sp.achievementFemalePupilSum,
        //                 achievementRegionPupilSum = o.sp.achievementRegionPupilSum,
        //                 goodGraduation = o.sp.goodGraduation,
        //                 normalGraduation = o.sp.normalGraduation,
        //                 status = o.ga != null ? o.ga.Status : -1,
        //                 userApprovalID = o.ga != null ? o.ga.UserApprove : -1,
        //                 date = o.ga != null ? o.ga.ModifiedDate : new DateTime(),
        //                 graduationApprovalID = o.ga != null ? o.ga.GraduationApprovalID : -1,
        //                 year = o.ga != null ? o.ga.Year : -1,
        //             }).AsQueryable();

        //    if (status != 0)
        //    {
        //        switch (status)
        //        {
        //            case 1: model = model.Where(x => x.status == -1);
        //                break;
        //            case 2: model = model.Where(x => x.status == 2);
        //                break;
        //            case 3: model = model.Where(x => x.status == 1);
        //                break;
        //            case 4: model = model.Where(x => x.status == 3);
        //                break;
        //        }
        //    }

        //    return model;
        //}

        public ActionResult LoadSchool(string year)
        {
            int yearInt = 0;
            if (Int32.TryParse(year, out yearInt))
            {
                yearInt = Int32.Parse(year);
            }
            var model = GetSchool(yearInt);
            return Json(model);
        }

        public List<SchoolProfileBO> GetSchool(int year)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", _globalInfo.ProvinceID } ,
                    { "DistrictID", _globalInfo.DistrictID}
                };
            IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS);

            IQueryable<SchoolProfileBO> lstSPBO = (from sp in lstSP
                                                   join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                   where sp.IsActive == true && ac.IsActive == true &&
                                                       sp.IsActive == true && ac.Year == year &&
                                                       sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                   select new SchoolProfileBO
                                                   {
                                                       SchoolProfileID = sp.SchoolProfileID,
                                                       SchoolCode = sp.SchoolCode,
                                                       SchoolName = sp.SchoolName,
                                                       AcademicYearID = ac.AcademicYearID,
                                                       EducationGrade = sp.EducationGrade
                                                   }).OrderBy(o => o.SchoolName.ToLower());

            return lstSPBO.ToList();
        }

        public FileStreamResult ExportApproveGraduationProfile(int SchoolID, int AcademicYearID, int Year, string PriorityCheck = "")
        {
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);

            List<PupilGraduationBO> listApprovePupil = SearchData(SchoolID, AcademicYearID, Year, true);
            List<PupilGraduationBO> listApprovePupilPriority = new List<PupilGraduationBO>();
            listApprovePupilPriority = listApprovePupil.Where(x => !string.IsNullOrEmpty(x.PriorityReason)).ToList();
            listApprovePupil = listApprovePupil.Where(x => !listApprovePupilPriority.Select(y => y.PupilID).Contains(x.PupilID)).ToList();
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_GRADUATION_APPROVAL);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            string reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            Stream stream = GraduationApprovalBusiness.ExportApproveGraduationProfile(oBook, listApprovePupil, listApprovePupilPriority, objSP, acaYear, dic);
            FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
            result.FileDownloadName = reportDefinition.TemplateName;
            return result;
        }

        private List<PupilGraduationBO> SearchData(int SchoolID, int AcademicYearID, int Year, bool check)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();           
            dic["AcademicYearID"] = AcademicYearID;
            dic["Check"] = "Check";
            dic.Add("EducationLevelID", SystemParamsInFile.EDUCATION_LEVEL_NINTH); //Lop 9
            List<PupilGraduationBO> lstApproveGrd = PupilGraduationBusiness.GetApprovePupil(SchoolID, AcademicYearID, 2, dic);
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(SchoolID);
            bool checkGDTX = schoolProfile.TrainingTypeID.HasValue ? schoolProfile.TrainingType.Resolution == "GDTX" : false;
            foreach (var pg in lstApproveGrd)
            {
                // Trường hợp thỏa mãn các điều kiện thì được hiển thị ô lựa chọn, set giá trị disabled checkbox = false
                pg.chkDisabled = false;
                pg.Notification = pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT || pg.GraduationStatus == SystemParamsInFile.GRADUATION_STATUS_GRADUATED ? string.Empty : pg.Description;
                // End set disabled
                if (check)
                {
                    if (PupilProfileBusiness.IsNotConductRankingPupil(checkGDTX, 2, pg.PupilLearningType, pg.BirthDate)) //Hoc sinh khong phai xet hanh kiem
                    {
                        if (!pg.CapacityLevelID.HasValue)
                        {
                            pg.Notification = Res.Get("PupilGraduation_Error_CapacityNotRanked") + ", ";
                            pg.chkDisabled = true;
                        }
                    }
                    else //Hoc sinh phai xet hanh kiem
                    {
                        if (!pg.ConductLevelID.HasValue && !pg.CapacityLevelID.HasValue)
                        {
                            pg.Notification = Res.Get("PupilGraduation_Error_ConductVsCapacityNotRanked") + ", ";
                            pg.chkDisabled = true;
                        }
                        else
                        {
                            if (!pg.ConductLevelID.HasValue)
                            {
                                pg.Notification = Res.Get("PupilGraduation_Error_ConductNotRanked") + ", ";
                                pg.chkDisabled = true;
                            }

                            if (!pg.CapacityLevelID.HasValue)
                            {
                                pg.Notification = Res.Get("PupilGraduation_Error_CapacityNotRanked") + ", ";
                                pg.chkDisabled = true;
                            }
                        }
                    }
                }
                pg.StatusString = pg.GraduationStatus.HasValue ? CommonList.GraduationStatus().Where(u => u.key.Equals(pg.GraduationStatus.Value.ToString())).SingleOrDefault().value : string.Empty;
            }
            if (lstApproveGrd.Count > 0 && lstApproveGrd != null)
            {             
                lstApproveGrd = lstApproveGrd.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            }

            return lstApproveGrd;
        }

        public void F_GET_CLASS_NOT_SUMMED_UP()
        {
        //    var f_SCHOOL_IDParameter = f_SCHOOL_ID.HasValue ?
        //        new OracleParameter("F_SCHOOL_ID", f_SCHOOL_ID) :
        //        new OracleParameter("F_SCHOOL_ID", typeof(int));

        //    var f_ACADEMIC_YEAR_IDParameter = f_ACADEMIC_YEAR_ID.HasValue ?
        //        new OracleParameter("F_ACADEMIC_YEAR_ID", f_ACADEMIC_YEAR_ID) :
        //        new OracleParameter("F_ACADEMIC_YEAR_ID", typeof(int));

        //    var f_SEMESTERParameter = f_SEMESTER.HasValue ?
        //        new OracleParameter("F_SEMESTER", f_SEMESTER) :
        //        new OracleParameter("F_SEMESTER", typeof(int));

        //    var f_APPLIED_LEVELParameter = f_APPLIED_LEVEL.HasValue ?
        //        new OracleParameter("F_APPLIED_LEVEL", f_APPLIED_LEVEL) :
        //        new OracleParameter("F_APPLIED_LEVEL", typeof(int));

            object[] Parameters = {};
            string commandString = @"SELECT * FROM table(SMAS3.GET_GRADUATION_APPROAL())";

            var result = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TestGetFunction>(commandString, Parameters).ToList();
        }

    }
}
