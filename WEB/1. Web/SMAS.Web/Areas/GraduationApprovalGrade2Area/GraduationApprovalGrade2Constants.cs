﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area
{
    public class GraduationApprovalGrade2Constants
    {
        public const string ACADEMIC = "ACADEMIC";

        public const string SCHOOL = "SCHOOL";

        public const string STATUS = "STATUS";
    }
}