﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area
{
    public class GraduationApprovalGrade2AreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GraduationApprovalGrade2Area";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GraduationApprovalGrade2Area_default",
                "GraduationApprovalGrade2Area/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
