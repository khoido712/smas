﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area.Models
{
    public class GraduationApprovalViewModel
    {
        public int graduationApprovalID { get; set; }
        public int schoolID { get; set; }
        [ResourceDisplayName("Trường")]
        public string schoolName { get; set; }
        public int academicYearID { get; set; }
        [ResourceDisplayName("Tổng HS dự kiến")]
        public int expectedPupilSum { get; set; }
        public int expectedFemalePupilSum { get; set; }
        public int expectedRegionPupilSum { get; set; }
        public int expectedFreePupilSum { get; set; }
        [ResourceDisplayName("Tổng HS đỗ tốt nghiệp")]
        public int achievementPupilSum { get; set; }
        public int achievementFemalePupilSum { get; set; }
        public int achievementRegionPupilSum { get; set; }
        public int goodGraduation { get; set; }
        public int normalGraduation { get; set; }
        [ResourceDisplayName("Ghi chú")]
        public string note { get; set; }
        [ResourceDisplayName("Trạng thái")]
        public string graduationApproStatus { get; set; }
        public int year { get; set; }
        public int? status { get; set; }
        public int? userApprovalID { get; set; }
        public Nullable<DateTime> date { get; set; }
        public int? classID { get; set; }
    }
}