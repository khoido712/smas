﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area.Models
{
    public class SearchModel
    {
        public int? YearID { get; set; }
        public int? SchoolID { get; set; }
        public int? StatusID { get; set; }
    }
}