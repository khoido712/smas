﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GraduationApprovalGrade2Area.Models
{
    public class TestGetFunction
    {
        public int School_ID { get; set; }
        public int Academic_Year_ID { get; set; }
        public int Year { get; set; }
        public string School_Name { get; set; }
        public int Expected_Pupil_Sum { get; set; }
        public int Expected_Female_Pupil_Sum { get; set; }
        public int Expected_Region_Pupil_Sum { get; set; }
        public int Achievement_Pupil_Sum { get; set; }
        public int Achievement_Female_Pupil_Sum { get; set; }
        public int Achievement_Region_Pupil_Sum { get; set; }
    }
}