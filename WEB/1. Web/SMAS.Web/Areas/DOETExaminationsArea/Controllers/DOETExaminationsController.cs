﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DOETExaminationsArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExaminationsArea.Controllers
{
    public class DOETExaminationsController:BaseController
    {
         #region Properties
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        private int UnitID;
        #endregion

        #region Constructor
        public DOETExaminationsController(IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness, IDOETExaminationsBusiness DOETExaminationsBusiness,
            IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamGroupBusiness DOETExamGroupBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.DOETExaminationsBusiness = DOETExaminationsBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamGroupBusiness = DOETExamGroupBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;

            this.UnitID = GetUnitID();
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {
            
            SetViewData();

            //Search du lieu
            int Year = GetCurYear();
            List<ExaminationsViewModel> lstModel = _Search(Year);
            ViewData[DOETExaminationsConstants.LIST_EXAMINATIONS] = lstModel;
            ViewData[DOETExaminationsConstants.IS_CUR_YEAR] = true;
            
            return View();
        }

        //
        // GET: /ExamGroup/Search
        
        public PartialViewResult Search(int year)
        {
            List<ExaminationsViewModel> lstModel = _Search(year);
            int curYear = GetCurYear();
            ViewData[DOETExaminationsConstants.IS_CUR_YEAR] = curYear == year;
            return PartialView("_List", lstModel);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(ExaminationsFormModel model)
        {
            if (ModelState.IsValid)
            {
                //Kiem tra thoi gian thi
                DateTime nowDate=DateTime.Now.Date;
                if (model.FromDate != null && DateTime.Compare(model.FromDate.Value, nowDate) < 0)
                {
                  
                    throw new BusinessException("DOETExaminations_Validate_Date_Less_Than_Now");
                    
                }

                if (model.ToDate != null && DateTime.Compare(model.ToDate.Value, nowDate) < 0)
                {
                   
                    throw new BusinessException("DOETExaminations_Validate_Date_Less_Than_Now");
                    
                }

                if (model.FromDate != null && model.ToDate != null && DateTime.Compare(model.FromDate.Value,model.ToDate.Value)>0)
                {
                    throw new BusinessException("DOETExaminations_Validate_Date_Logic");
                }

                if (DateTime.Compare(model.Deadline, nowDate) < 0)
                {
                    throw new BusinessException("DOETExaminations_Validate_Deadline_Less_Than_Now");
                }

                int curYear=GetCurYear();
                //Kiem tra ten ky thi da ton tai
                if (DOETExaminationsBusiness.GetExaminationsOfDept(UnitID, curYear)
                    .Where(o => o.ExaminationsName.ToLower() == model.ExaminationsName.ToLower()).Count() > 0)
                {
                    throw new BusinessException("DOETExaminations_Validate_Name_Exist");
                }

                DOETExaminations exam = new DOETExaminations();
                exam.ExaminationsID = DOETExaminationsBusiness.GetNextSeq<int>("DOET_EXAMINATIONS_SEQ");
                exam.AppliedType = model.AppliedType;
                exam.CreateTime = DateTime.Now;
                exam.DeadLine = model.Deadline;
                exam.ExamCouncil = model.ExamCouncil;
                exam.ExaminationsName = model.ExaminationsName;
                exam.FromDate = model.FromDate;
                exam.Note = model.Note;
                exam.ToDate = model.ToDate;
                exam.UnitID = UnitID;
                exam.Year = curYear;

                DOETExaminationsBusiness.Insert(exam);
                DOETExaminationsBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }
        public PartialViewResult Create()
        {
            SetViewData();
          
            ExaminationsFormModel model = new ExaminationsFormModel();
          
            return PartialView("_Create", model);
        }
        public PartialViewResult Edit(int examinationsId)
        {
            SetViewData();
            DOETExaminations exam = DOETExaminationsBusiness.Find(examinationsId);
            ExaminationsFormModel model = new ExaminationsFormModel();
            if (exam != null)
            {
                model.AppliedType = exam.AppliedType;
                model.Deadline = exam.DeadLine;
                model.ExamCouncil = exam.ExamCouncil;
                model.ExaminationsID = exam.ExaminationsID;
                model.ExaminationsName = exam.ExaminationsName;
                model.FromDate = exam.FromDate;
                model.Note = exam.Note;
                model.ToDate = exam.ToDate;
                model.Year = exam.Year;
            }

            return PartialView("_Edit", model);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(ExaminationsFormModel model)
        {
            if (ModelState.IsValid)
            {
                //Kiem tra thoi gian thi
                DateTime nowDate = DateTime.Now.Date;
                if (model.FromDate != null && DateTime.Compare(model.FromDate.Value, nowDate) < 0)
                {

                    throw new BusinessException("DOETExaminations_Validate_Date_Less_Than_Now");

                }

                if (model.ToDate != null && DateTime.Compare(model.ToDate.Value, nowDate) < 0)
                {

                    throw new BusinessException("DOETExaminations_Validate_Date_Less_Than_Now");

                }

                if (model.FromDate != null && model.ToDate != null && DateTime.Compare(model.FromDate.Value, model.ToDate.Value) > 0)
                {
                    throw new BusinessException("DOETExaminations_Validate_Date_Logic");
                }

                if (DateTime.Compare(model.Deadline, nowDate) < 0)
                {
                    throw new BusinessException("DOETExaminations_Validate_Deadline_Less_Than_Now");
                }

                DOETExaminations exam = DOETExaminationsBusiness.Find(model.ExaminationsID);
                exam.UpdateTime = DateTime.Now;
                exam.DeadLine = model.Deadline;
                exam.ExamCouncil = model.ExamCouncil;
                exam.AppliedType = model.AppliedType;
                exam.ExaminationsName = model.ExaminationsName;
                exam.FromDate = model.FromDate;
                exam.Note = model.Note;
                exam.ToDate = model.ToDate;
                exam.UnitID = UnitID;

                DOETExaminationsBusiness.Update(exam);
                DOETExaminationsBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));

            
        }
        #endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            DOETExaminations exam = this.DOETExaminationsBusiness.Find(id);
            
            //Kiem tra ky thi da co thi sinh chua
            IQueryable<DOETExamPupil> lstExamPupil = DOETExamPupilBusiness.All.Where(o => o.ExaminationsID == id);
            if (lstExamPupil.Count() > 0)
            {
                throw new BusinessException("DOETExaminations_Not_Delete");
            }
            
            //Lay ra cac nhom thi cua ky thi
            List<DOETExamGroup> lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == id).ToList();
            List<int> lstExamGroupID = lstExamGroup.Select(o => o.ExamGroupID).ToList();

            //Lay ra cac mon thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = id;
            dic["ListGroupID"] = lstExamGroupID;
            List<DOETExamSubject> lstExamSubject = DOETExamSubjectBusiness.Search(dic).ToList();

            //Xoa mon thi
            for (int i = 0; i < lstExamSubject.Count; i++)
            {
                DOETExamSubjectBusiness.Delete(lstExamSubject[i].ExamSubjectID);
            }
            DOETExamSubjectBusiness.Save();

            //Xoa nhom thi
            for (int i = 0; i < lstExamGroup.Count; i++)
            {
                DOETExamGroupBusiness.Delete(lstExamGroup[i].ExamGroupID);
            }
            DOETExamGroupBusiness.Save();

            //xoa ky thi
            this.DOETExaminationsBusiness.Delete(id);

            this.DOETExaminationsBusiness.Save();
           
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #region Private methods
        
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            var lstYear=new List<ComboObject>();
            //Lay nam hoc hien tai
            ComboObject curYear = GetCurYearObject();
            lstYear.Add(curYear);

            //Lay ra cac nam hoc co ky thi cua so
            List<int> lstExamYear = DOETExaminationsBusiness.GetExaminationsOfDept(UnitID).OrderByDescending(o=>o.CreateTime).Select(o=>o.Year).Distinct().ToList();
            for (int i = 0; i < lstExamYear.Count; i++)
            {
                int year = lstExamYear[i];
                if (year != Int32.Parse(curYear.key))
                {
                    lstYear.Add(GetYearObject(year));
                }
            }

            ViewData[DOETExaminationsConstants.CBO_EXAM_YEAR] = new SelectList(lstYear, "key", "value");


        }

        private List<ExaminationsViewModel> _Search(int Year)
        {
            List<ExaminationsViewModel> lstExam = (from de in DOETExaminationsBusiness.GetExaminationsOfDept(UnitID, Year)
                                                   join dep in DOETExamPupilBusiness.All on de.ExaminationsID equals dep.ExaminationsID into g
                                                   from x in g.DefaultIfEmpty()
                                                   select new ExaminationsViewModel
                                                  {
                                                      ExaminationsID = de.ExaminationsID,
                                                      ExaminationsName = de.ExaminationsName,
                                                      FromDate = de.FromDate,
                                                      ToDate = de.ToDate,
                                                      Deadline = de.DeadLine,
                                                      ExamCouncil = de.ExamCouncil,
                                                      CreateDate = de.CreateTime,
                                                      EnableToDelete = x == null ? true : false
                                                  }).Distinct().OrderByDescending(o => o.CreateDate).ToList();

            return lstExam;
        }

        private int GetCurYear()
        {
            int year;
            DateTime dateNow = DateTime.Now.Date;
            DateTime midDate = new DateTime(dateNow.Year, 7, 1);
            if (DateTime.Compare(dateNow, midDate) < 0)
            {
                year = dateNow.Year - 1;
            }
            else
            {
                year = dateNow.Year;
            }
            return year;
        }
        private ComboObject GetCurYearObject()
        {
            int year = GetCurYear();

            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        private ComboObject GetYearObject(int year)
        {
            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        //ham lay UnitID, neu la account thuoc phong thi lay ID phong, neu khong thi lay ID so
        private int GetUnitID()
        {
            int unitId = 0;
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
                //Neu la accont phong ban thuoc phong
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDept subSupervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    unitId = subSupervisingDept.SupervisingDeptID;

                }
                //Neu la account phong
                else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    unitId = supervisingDeptID;
                }
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);

                //Neu la phong ban thuoc so
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    unitId = supervisingDept.SupervisingDeptID;
                }
                //Neu la account so
                else if (dept != null && dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                {
                    unitId = supervisingDeptID;
                }
            }

            return unitId;
        }
        #endregion
    }
}