﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExaminationsArea
{
    public class DOETExaminationsConstants
    {
        public const string CBO_EXAM_YEAR = "CBO_EXAM_YEAR";
        public const string LIST_EXAMINATIONS = "LIST_EXAMINATIONS";
        public const string IS_CUR_YEAR = "IS_CUR_YEAR";
        public const int APPLIED_TYPE_SCHOOL = 0;
        public const int APPLIED_TYPE_SUB_SUPERVISORY = 1;
    }
}