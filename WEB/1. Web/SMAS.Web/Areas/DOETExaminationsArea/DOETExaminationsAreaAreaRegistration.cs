﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExaminationsArea
{
    public class DOETExaminationsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExaminationsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExaminationsArea_default",
                "DOETExaminationsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
