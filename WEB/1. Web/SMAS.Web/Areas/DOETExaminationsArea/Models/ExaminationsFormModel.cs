﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExaminationsArea.Models
{
    public class ExaminationsFormModel
    {
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExaminationsConstants.CBO_EXAM_YEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [ResourceDisplayName("Common_Label_AcademicYear")]
        public int? Year { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ExaminationsID { get; set; }

        [ResourceDisplayName("DOETExaminations_Examinations")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "DOETExaminations_Validate_ExaminationsName")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExaminationsName { get; set; }

        [ResourceDisplayName("DOETExaminations_FromDate2")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("DOETExaminations_ToDate2")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public DateTime? ToDate { get; set; }

        [ResourceDisplayName("DOETExaminations_Deadline")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "DOETExaminations_Validate_Deadline")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public DateTime Deadline { get; set; }

        [ResourceDisplayName("DOETExaminations_Exam_Council")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamCouncil { get; set; }

        [ResourceDisplayName("Common_Label_Note")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Note { get; set; }

        public int AppliedType { get; set; }

    }
}