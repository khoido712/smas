﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExaminationsArea.Models
{
    public class ExaminationsViewModel
    {
        
        public int ExaminationsID { get; set; }
        [ResourceDisplayName("DOETExaminations_Examinations")]
        public string ExaminationsName { get; set; }

        [ResourceDisplayName("DOETExaminations_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("DOETExaminations_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        [ResourceDisplayName("DOETExaminations_Deadline")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Deadline { get; set; }

        [ResourceDisplayName("DOETExaminations_Exam_Council")]
        public string ExamCouncil { get; set; }

        public bool EnableToDelete { get; set; }
        public DateTime CreateDate { get; set; }
    }
}