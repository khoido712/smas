﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PolicyRegimeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.PolicyRegimeArea.Controllers
{
    public class PolicyRegimeController : BaseController
    {        
        private readonly IPolicyRegimeBusiness PolicyRegimeBusiness;
		
		public PolicyRegimeController (IPolicyRegimeBusiness policyregimeBusiness)
		{
			this.PolicyRegimeBusiness = policyregimeBusiness;
		}
		
		//
        // GET: /PolicyRegime/

        public ActionResult Index()
        {
            SetViewDataPermission("PolicyRegime", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<PolicyRegimeViewModel> lst = this._Search(SearchInfo);
            ViewData[PolicyRegimeConstants.LIST_POLICYREGIME] = lst;
            return View();
        }

		//
        // GET: /PolicyRegime/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            IEnumerable<PolicyRegimeViewModel> lst = this._Search(SearchInfo);
            ViewData[PolicyRegimeConstants.LIST_POLICYREGIME] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            PolicyRegime policyregime = new PolicyRegime();
            policyregime.IsActive = true;
            TryUpdateModel(policyregime); 
            Utils.Utils.TrimObject(policyregime);

            this.PolicyRegimeBusiness.Insert(policyregime);
            this.PolicyRegimeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PolicyRegimeID)
        {
            PolicyRegime policyregime = this.PolicyRegimeBusiness.Find(PolicyRegimeID);
            TryUpdateModel(policyregime);
            Utils.Utils.TrimObject(policyregime);
            this.PolicyRegimeBusiness.Update(policyregime);
            this.PolicyRegimeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PolicyRegimeBusiness.Delete(id);
            this.PolicyRegimeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PolicyRegimeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PolicyRegime> query = this.PolicyRegimeBusiness.Search(SearchInfo);
            IQueryable<PolicyRegimeViewModel> lst = query.Select(o => new PolicyRegimeViewModel {               
						PolicyRegimeID = o.PolicyRegimeID,								
						Resolution = o.Resolution,								
						Description = o.Description								
										
					
				
            });

            return lst.ToList();
        }        
    }
}





