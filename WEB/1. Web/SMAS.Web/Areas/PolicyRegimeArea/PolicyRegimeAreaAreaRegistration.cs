﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PolicyRegimeArea
{
    public class PolicyRegimeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PolicyRegimeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PolicyRegimeArea_default",
                "PolicyRegimeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
