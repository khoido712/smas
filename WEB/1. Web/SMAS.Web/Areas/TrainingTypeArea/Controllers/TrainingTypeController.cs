﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.TrainingTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.TrainingTypeArea.Controllers
{
    public class TrainingTypeController : BaseController
    {        
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
		
		public TrainingTypeController (ITrainingTypeBusiness trainingtypeBusiness)
		{
			this.TrainingTypeBusiness = trainingtypeBusiness;
		}
		
		//
        // GET: /TrainingType/

        public ActionResult Index()
        {
            SetViewDataPermission("Ethnic", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<TrainingTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[TrainingTypeConstants.LIST_TRAININGTYPE] = lst;
            return View();
        }

		//
        // GET: /TrainingType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            IEnumerable<TrainingTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[TrainingTypeConstants.LIST_TRAININGTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            TrainingType trainingtype = new TrainingType();
            TryUpdateModel(trainingtype); 
            Utils.Utils.TrimObject(trainingtype);

            this.TrainingTypeBusiness.Insert(trainingtype);
            this.TrainingTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TrainingTypeID)
        {
            TrainingType trainingtype = this.TrainingTypeBusiness.Find(TrainingTypeID);
            TryUpdateModel(trainingtype);
            Utils.Utils.TrimObject(trainingtype);
            this.TrainingTypeBusiness.Update(trainingtype);
            this.TrainingTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.TrainingTypeBusiness.Delete(id);
            this.TrainingTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TrainingTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<TrainingType> query = this.TrainingTypeBusiness.Search(SearchInfo);
            IQueryable<TrainingTypeViewModel> lst = query.Select(o => new TrainingTypeViewModel {               
						TrainingTypeID = o.TrainingTypeID,								
						Resolution = o.Resolution,								
												
					
				
            });

            return lst.ToList();
        }        
    }
}





