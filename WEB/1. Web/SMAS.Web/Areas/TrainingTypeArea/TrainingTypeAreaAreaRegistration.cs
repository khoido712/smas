﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TrainingTypeArea
{
    public class TrainingTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TrainingTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TrainingTypeArea_default",
                "TrainingTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
