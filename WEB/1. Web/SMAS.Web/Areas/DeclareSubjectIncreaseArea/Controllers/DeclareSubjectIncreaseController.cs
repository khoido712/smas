﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.DeclareSubjectIncreaseArea.Models;

namespace SMAS.Web.Areas.DeclareSubjectIncreaseArea.Controllers
{
    public class DeclareSubjectIncreaseController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public DeclareSubjectIncreaseController(IClassProfileBusiness ClassProfileBusiness
                                                , IClassSubjectBusiness ClassSubjectBusiness
                                                , ISubjectCatBusiness SubjectCatBusiness
            , IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        //
        // GET: /DeclareSubjectIncreaseArea/DeclareSubjectIncrease/

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            SetViewDataPermission("DeclareSubjectIncrease", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin, Global.RoleID);
            return View();
        }

        private void SetViewData()
        {
            //Đổ dữ liệu vào EducationLevelcombobox
            IEnumerable<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels;
            ViewData[DeclareSubjectIncreaseConstant.LS_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            //Đổ dữ liệu vào ClassCombobox
            List<ClassProfile> lsClassID = GetClassFromEducationLevel(lsEducationLevel.Select(p => p.EducationLevelID).FirstOrDefault());
            ViewData[DeclareSubjectIncreaseConstant.LS_CLASSID] = new SelectList(lsClassID, "ClassProfileID", "DisplayName");

            //Search 
            Search(lsEducationLevel.Select(p => p.EducationLevelID).FirstOrDefault(), lsClassID.Select(p => p.ClassProfileID).FirstOrDefault());

            // Xet nam hoc hien tai
            ViewData[DeclareSubjectIncreaseConstant.CHECK_ACADEMIC_YEAR] = CheckAcademicYear();
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {

            List<ClassProfile> lsCP = GetClassFromEducationLevel(educationLevelID);
            if (lsCP.Count() != 0)
            {
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }

        }

        private List<ClassProfile> GetClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };

            List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic).OrderBy(x => x.OrderNumber).ThenBy(x => x.DisplayName).ToList();
            return lsCP;
        }

        public PartialViewResult Search(int EducationLevelID, int ClassID)
        {
            //Lấy danh sách các môn học của lớp chỉ gồm các môn tính điểm 
            List<GirdViewModel> lstGird = GetListSubjectByClass(EducationLevelID, ClassID);


            ViewData[DeclareSubjectIncreaseConstant.LS_GIRD] = lstGird;

            //lấy ra danh sach cac mon hoc duoc tang cuong 
            List<int> lsSubjectIncrease = lstGird.Select(p => p.SubjectIDIncrease.GetValueOrDefault()).Distinct().ToList();
            lsSubjectIncrease = lsSubjectIncrease.Where(p => p != 0).ToList();
            ViewData[DeclareSubjectIncreaseConstant.LS_SUBJECT_INCREASE] = lsSubjectIncrease;

            //Lấy danh sách môn học khai báo cho lớp 
            List<int> lstAppliedType = new List<int>() { DeclareSubjectIncreaseConstant.APPLIED_TYPE_MARK, DeclareSubjectIncreaseConstant.APPLIED_TYPE_OCCUPATION, DeclareSubjectIncreaseConstant.APPLIED_TYPE_TO_ADD_PRIORRITY };
            IDictionary<string, object> dicSearch = new Dictionary<string, object>(){
                {"ClassID", ClassID},
                {"EducationLevelID", EducationLevelID},               
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},    
                {"lstAppliedType",lstAppliedType},
                {"IsVNEN",true}
            };
            List<SubjectCat> lstSC = (from cs in ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch)
                                      join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                      where sc.IsActive == true && cs.IsCommenting == DeclareSubjectIncreaseConstant.IS_COMMENTING
                                      select sc).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();

            ViewData[DeclareSubjectIncreaseConstant.LS_SUBJECT] = lstSC;

            return PartialView("_List");
        }

        private List<GirdViewModel> GetListSubjectByClass(int EducationLevelID, int ClassID)
        {
            List<int> lstAppliedType = new List<int>() { DeclareSubjectIncreaseConstant.APPLIED_TYPE_MARK, DeclareSubjectIncreaseConstant.APPLIED_TYPE_OCCUPATION, DeclareSubjectIncreaseConstant.APPLIED_TYPE_TO_ADD_PRIORRITY };
            IDictionary<string, object> dicSearch = new Dictionary<string, object>(){
                {"ClassID", ClassID},
                {"EducationLevelID", EducationLevelID},
                {"IsCommenting", DeclareSubjectIncreaseConstant.IS_COMMENTING},
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"lstAppliedType",lstAppliedType},
                {"IsVNEN",true}
            };

            List<GirdViewModel> lstGird = (from cs in ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch)
                                           join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                           where sc.IsActive == true
                                          
                                           select new GirdViewModel()
                                           {
                                               ClassSubjectID = cs.ClassSubjectID,
                                               SubjectID = cs.SubjectID,
                                               SubjectName = sc.SubjectName,
                                               Abbreviation = sc.Abbreviation,
                                               AppliedType = cs.AppliedType,
                                               IsCommenting = cs.IsCommenting,
                                               SubjectIDIncrease = cs.SubjectIDIncrease,
                                               OrderInSubject = sc.OrderInSubject
                                           }).OrderBy(p => p.OrderInSubject).ToList();

            if (lstGird != null && lstGird.Count > 0)
            {
                GirdViewModel gridObj = null;

                // AppliedTypeName = (cs.AppliedType != null && cs.AppliedType.Value == 0) ? "Môn bắt buộc" : cs.AppliedType.Value == 1 ? "Môn tự chọn" : string.Empty,
                //IsCommentingName = (cs.IsCommenting != null && cs.IsCommenting == DeclareSubjectIncreaseConstant.IS_COMMENTING) ? "Tính điểm" : string.Empty,
                for (int i = 0; i < lstGird.Count; i++)
                {
                    gridObj = lstGird[i];
                    lstGird[i].AppliedTypeName = (gridObj.AppliedType != null && gridObj.AppliedType == 0) ? "Môn bắt buộc" : gridObj.AppliedType.Value == 1 ? "Môn tự chọn" : string.Empty;
                    lstGird[i].IsCommentingName = (gridObj.IsCommenting != null && gridObj.IsCommenting == DeclareSubjectIncreaseConstant.IS_COMMENTING) ? "Tính điểm" : string.Empty;
                }
            }

            return lstGird;
        }

        
        [ValidateAntiForgeryToken]
        public JsonResult SaveDeclareSubject(int? cboEducationLevel, int? cboClass, int[] checkChangeSubjectIncrease, int[] ChoiceSubjectIncrea,
                                              int[] SubjectIDHidden, int? ButtonClickType)
        {
            string strCheckAcademicYear = CheckAcademicYear();
            if (!strCheckAcademicYear.Equals(string.Empty))
            {
                return Json(new JsonMessage(strCheckAcademicYear, "error"));
            }

            if (GetMenupermission("DeclareSubjectIncrease", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!cboEducationLevel.HasValue) // Kiem tra xem chon khoi chua
            {
                throw new BusinessException("TransferData_Label_EducationLevelError");
            }
            if (!cboClass.HasValue) // kiem tra xem chon lop chua 
            {
                throw new BusinessException("SchoolMovement_Validate_Class");
            }
            int semesterId = _globalInfo.Semester.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            #region Get data
            IDictionary<string, object> dicSearch = new Dictionary<string, object>(){
                {"EducationLevelID",cboEducationLevel.Value},
                {"AppliedLevel", _globalInfo.AppliedLevel},
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"ClassID", cboClass.Value},
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"IsActive", true}
            };
            List<SubjectCat> lsSC = SubjectCatBusiness.Search(dicSearch).ToList();

            int countSubjectIDHidden = SubjectIDHidden.Count();
            int countcheckChangeSubjectIncrease = checkChangeSubjectIncrease != null ? checkChangeSubjectIncrease.Count() : 0;
            IDictionary<int, int> dicSubjcetChoice = new Dictionary<int, int>();
            for (int i = 0; i < countSubjectIDHidden; i++)
            {
                dicSubjcetChoice.Add(SubjectIDHidden[i], ChoiceSubjectIncrea[i]);
            }

            //lấy danh sách lớp của một khối
            List<int> lsClassOfEducationLevelID = this.GetClassFromEducationLevel(cboEducationLevel.Value).Select(p => p.ClassProfileID).ToList();
            List<int> lstAppliedType = new List<int>() { DeclareSubjectIncreaseConstant.APPLIED_TYPE_MARK, DeclareSubjectIncreaseConstant.APPLIED_TYPE_OCCUPATION, DeclareSubjectIncreaseConstant.APPLIED_TYPE_TO_ADD_PRIORRITY };
            IDictionary<string, object> dicSearchByEducationLevel = new Dictionary<string, object>(){
                {"EducationLevelID",cboEducationLevel.Value},
                {"AppliedLevel", _globalInfo.AppliedLevel},
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"IsActive", true},
                {"lstAppliedType",lstAppliedType}
            };

            //lay danh sach cac khoi cua truong
            List<int> lstEducationLevel = _globalInfo.EducationLevels.Select(p => p.EducationLevelID).ToList();
            #endregion

            //-	Nếu đã check chọn môn tăng cường nhưng chưa chọn môn được tăng cường,
            //thông báo “Thầy cô chưa chọn môn được tăng cường của môn [Tên các môn học tăng cường].”
            string msgErr = string.Empty;
            string nameSubject = string.Empty;
            for (int j = 0; j < countcheckChangeSubjectIncrease; j++)
            {
                if (dicSubjcetChoice.ContainsKey(checkChangeSubjectIncrease[j]) == true)
                {
                    if (dicSubjcetChoice[checkChangeSubjectIncrease[j]] == 0)// giá trị mặc định lưa chọn
                    {
                        nameSubject = lsSC.Where(p => p.SubjectCatID == checkChangeSubjectIncrease[j]).Select(p => p.SubjectName).FirstOrDefault();
                        msgErr += nameSubject + ", ";
                    }
                }
            }
            msgErr = msgErr != string.Empty ? "Thầy cô chưa chọn môn được tăng cường của môn " + msgErr.Remove(msgErr.Length - 2) : string.Empty;
            if (msgErr != string.Empty)
            {
                throw new BusinessException(msgErr);
            }

            //-	Nếu một môn học được khai báo là môn học được tăng cường bởi  nhiều hơn 1 môn học khác, 
            //thông báo “Chỉ được khai báo 1 môn là môn tăng cường cho môn [Tên môn học]”.
            var countSubjectChoice = ChoiceSubjectIncrea.Where(p => p != 0).GroupBy(p => p).Select(p => new { id = p.Key, count = p.Count() });
            int[] KeySubject = null;
            foreach (var item in countSubjectChoice)
            {
                KeySubject = dicSubjcetChoice.Where(p => p.Key != 0 && p.Value == item.id).Select(p => p.Key).ToArray(); //dem cac mon dc check dang ky cung 1 mon tang cuong
                KeySubject = checkChangeSubjectIncrease != null ? KeySubject.Where(p => checkChangeSubjectIncrease.Contains(p)).ToArray() : null; // dem so mon hoc dc check
                if (item.count > 1 && KeySubject != null && KeySubject.Count() > 1)
                {
                    nameSubject = lsSC.Where(p => p.SubjectCatID == item.id).Select(p => p.SubjectName).FirstOrDefault();
                    throw new BusinessException("Chỉ được khai báo 1 môn là môn tăng cường cho môn " + nameSubject);
                }
            }

            if (checkChangeSubjectIncrease != null)
            {
                List<int> lstKey = dicSubjcetChoice.Select(p => p.Key).ToList();
                List<int> lstValue = dicSubjcetChoice.Select(p => p.Value).ToList();
                for (int i = 0; i < dicSubjcetChoice.Count; i++)
                {
                    if (!checkChangeSubjectIncrease.Contains(lstKey[i]))
                        continue;
                    int demsolanxuathienKey = lstKey.Where(p => p == lstValue[i]).Count();
                    int demsolanxuathienValue = lstValue.Where(p => p == lstValue[i]).Count();
                    int sum = demsolanxuathienKey + demsolanxuathienValue;
                    if (sum > 1 && checkChangeSubjectIncrease.Contains(lstValue[i]))
                    {
                        nameSubject = lsSC.Where(p => p.SubjectCatID == lstValue[i]).Select(p => p.SubjectName).FirstOrDefault();
                        throw new BusinessException("Môn " + nameSubject + " đã được khai báo là môn tăng cường");
                    }
                }
            }

            //loc ra các môn học tăng cường và các môn được tăng cường
            IDictionary<int, int> dicSubjectSaveIncrease = new Dictionary<int, int>();//dic luu cac mon dc them mon hoc tang cuong            
            bool checkSave;
            var filterDicValueSubjectChoice = dicSubjcetChoice.Where(p => p.Value > 0);
            foreach (var obj in filterDicValueSubjectChoice)
            {
                checkSave = checkChangeSubjectIncrease != null ? checkChangeSubjectIncrease.Where(p => p == obj.Key).Count() > 0 : false;
                if (checkSave)
                {
                    dicSubjectSaveIncrease.Add(obj.Key, obj.Value);
                }
                else
                {
                    dicSubjectSaveIncrease.Add(obj.Key, 0);
                }
            }

            if (ButtonClickType.Value == 1)//lưu môn học cho lớp
            {
                ClassSubjectBusiness.UpdateSubjectIncreaseByClass(academicYearId, schoolId, semesterId, dicSearch, dicSubjectSaveIncrease);
                return Json(new JsonMessage(Res.Get("Lưu khai báo môn tăng cường thành công")));
            }
            else if (ButtonClickType.Value == 2) //ap dung cho toan khoi
            {
                ClassSubjectBusiness.UpdateSubjectIncreaseByEducationLevel(academicYearId, schoolId, semesterId, lsClassOfEducationLevelID, cboClass.Value,
                                                                                dicSearchByEducationLevel, dicSubjectSaveIncrease);
                return Json(new JsonMessage(Res.Get("Lưu khai báo môn tăng cường cho khối thành công")));
            }
            else //ap dung cho toan truong khong phân biệt cấp
            {
                dicSearchByEducationLevel = new Dictionary<string, object>(){
                          {"AcademicYearID", academicYearId},
                          {"SchoolID", schoolId},
                          {"IsActive", true},
                          {"lstAppliedType",lstAppliedType}
                };
                ClassSubjectBusiness.UpdateSubjectIncreaseBySchool(academicYearId, schoolId, semesterId, lstEducationLevel, cboClass.Value, dicSubjectSaveIncrease, dicSearchByEducationLevel);
                return Json(new JsonMessage(Res.Get("Lưu khai báo môn tăng cường cho trường thành công")));
            }
        }

        public JsonResult CheckSunjectIncrease(int EducationLevelID, int ClassID, int subjectIncreaseID)
        {
            if (subjectIncreaseID == 0)
            {
                throw new BusinessException(Res.Get("Vui lòng chọn môn tăng cường"));
            }

            //lấy danh sách môn học theo khoi + lop
            IDictionary<string, object> dicSearchSC = new Dictionary<string, object>(){
                {"EducationLevelID",EducationLevelID},
                {"AppliedLevel", _globalInfo.AppliedLevel},
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"ClassID", ClassID},
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"IsActive", true}
            };
            List<SubjectCat> lsSC = SubjectCatBusiness.Search(dicSearchSC).ToList();

            //lấy danh sách môn tính điểm của lớp
            IDictionary<string, object> dicSearch = new Dictionary<string, object>(){
                {"ClassID", ClassID},
                {"EducationLevelID", EducationLevelID},
                {"IsCommenting", DeclareSubjectIncreaseConstant.IS_COMMENTING},
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},              
            };
            List<ClassSubject> lstCS = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).ToList();
            ClassSubject objCS = null;
            string error = string.Empty;
            objCS = lstCS.Where(p => p.SubjectIDIncrease == subjectIncreaseID).FirstOrDefault();
            if (objCS != null)//môn học này đã được khai báo là môn tăng cường của môn học khác 
            {
                error = string.Format("Môn {0} đã được khai báo là môn tăng cường của môn {1}. Thầy cô vui lòng kiểm tra lại.",
                    lsSC.Where(p => p.SubjectCatID == subjectIncreaseID).Select(p => p.SubjectName).FirstOrDefault(), objCS.SubjectCat.SubjectName);
            }
            return Json(new JsonMessage(error));
        }

        public string CheckAcademicYear()
        {
            string message = string.Empty;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            DateTime currentDate = DateTime.Now;
            if (currentDate <= aca.FirstSemesterStartDate || (currentDate >= aca.FirstSemesterEndDate && currentDate <= aca.SecondSemesterStartDate) || currentDate >= aca.SecondSemesterEndDate)
            {
                message = "Thời gian hiện tại không thuộc năm học";
            }
            return message;
        }
    }
}
