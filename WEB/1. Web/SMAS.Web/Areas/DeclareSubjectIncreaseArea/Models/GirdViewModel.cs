﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareSubjectIncreaseArea.Models
{
    public class GirdViewModel
    {
        public long ClassSubjectID { get; set; }

        public int SubjectID { get; set; }

        public string SubjectName { get; set; }

        public string Abbreviation { get; set; }

        public int? AppliedType { get; set; }

        public string AppliedTypeName { get; set; }

        public int? IsCommenting { get; set; }

        public string IsCommentingName { get; set; }

        public int? SubjectIDIncrease { get; set; }

        public int? OrderInSubject { get; set; }

    }
}