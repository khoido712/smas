﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareSubjectIncreaseArea.Models
{
    public class SearchViewModel
    {
        [DisplayName("Khối: ")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DeclareSubjectIncreaseConstant.LS_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "OncbEducationLevelChange(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cboEducationLevel { get; set; }

        [DisplayName("Lớp: ")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DeclareSubjectIncreaseConstant.LS_CLASSID)]
        [AdditionalMetadata("OnChange", "OncbClassChange(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cboClass { get; set; }
    }
}