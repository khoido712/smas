﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareSubjectIncreaseArea
{
    public class DeclareSubjectIncreaseConstant
    {
        public const string LS_EDUCATIONLEVEL = "LS_EDUCATIONLEVEL";
        public const string LS_CLASSID = "LS_CLASSID";
        public const string LS_GIRD = "LS_GIRD";
        public const string LS_SUBJECT = "LS_SUBJECT";
        public const string LS_SUBJECT_INCREASE = "LS_SUBJECT_INCREASE";
        public const string CHECK_ACADEMIC_YEAR = "CHECK_ACADEMIC_YEAR";

        /// <summary>
        /// KIỂU MÔN: TÍNH ĐIỂM
        /// </summary>
        public const int IS_COMMENTING = 0;

        public const int APPLIED_TYPE_TO_ADD_PRIORRITY = 2;
        public const int APPLIED_TYPE_MARK = 3;
        public const int APPLIED_TYPE_OCCUPATION = 4;
    }
}