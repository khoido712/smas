﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareSubjectIncreaseArea
{
    public class DeclareSubjectIncreaseAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeclareSubjectIncreaseArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeclareSubjectIncreaseArea_default",
                "DeclareSubjectIncreaseArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
