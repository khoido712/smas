﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ClassMovementArea.Models
{
    public class ClassMovementObject 
    {
        [Required]
        public int PupilOfClassID
        {
            get;
            set;
        }

        [Required]
        public int PupilProfileID
        {
            get;
            set;
        }


        [Required]
        public int ClassID
        {
            get;
            set;
        }

        public string DisplayName
        {
            get;
            set;
        }

         [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode
        {
            get;
            set;
        }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName
        {
            get;
            set;
        }

 

        public int? SchoolID
        {
            get;
            set;
        }


        public int? AcademicYearID
        {
            get;
            set;
        }

    }
}
