/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassMovementArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassMovement_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassMovementConstants.LST_EDUCATIONLEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this, this.options[this.selectedIndex].value);")]
        public Nullable<int> EducationLevelID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassMovementConstants.LS_CBNEWCLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> ToClassID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }
        
    }
}