/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ClassMovementArea.Models
{
    public class ClassMovementViewModel
    {
        public System.Int32 ClassMovementID { get; set; }
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FromClass")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? FromClassID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_ToClass")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? ToClassID { get; set; }

        [ResourceDisplayName("ClassMovement_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ClassMovement_Label_FromClassName")]
        public string FromClassName { get; set; }

        [ResourceDisplayName("ClassMovement_Label_ToClassName")]
        public string ToClassName { get; set; }
        public System.Nullable<System.Int32> EducationLevelID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Int32 AcademicYearID { get; set; }
        public int? OrderInClass { get; set; }
        public string PupilName { get; set; }

        [ResourceDisplayName("ClassMovement_Label_MovedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime? MovedDate { get; set; }

        [ResourceDisplayName("ClassMovement_Label_Reason")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Reason { get; set; }

    }
}


