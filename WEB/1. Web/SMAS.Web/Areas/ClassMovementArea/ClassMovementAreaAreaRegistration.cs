﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassMovementArea
{
    public class ClassMovementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassMovementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassMovementArea_default",
                "ClassMovementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
