﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ClassMovementArea.Models;

using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.ClassMovementArea.Controllers
{

    public class ClassMovementController : BaseController
    {
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private List<PupilOfClass> listOldPupil = new List<SMAS.Models.Models.PupilOfClass>();
        private List<PupilOfClass> listTransferPupil = new List<SMAS.Models.Models.PupilOfClass>();
        private List<int> listClassID = new List<int>();

        public ClassMovementController(IClassMovementBusiness classmovementBusiness, IPupilProfileBusiness pupilProfileBusiness, IPupilOfClassBusiness pupilOfClassBusiness, IAcademicYearBusiness academicYearBusiness, IClassProfileBusiness classProfileBusiness, IEducationLevelBusiness educationLevelBusiness)
        {
            this.ClassMovementBusiness = classmovementBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
        }

        public ViewResult Index()
        {
            GlobalInfo gl = new GlobalInfo();
            ViewData[ClassMovementConstants.TeacherPermission] = true;
            //Nếu ko phải năm học hiện tại hoặc account đăng nhập ko có quyền giáo viên chủ nhiệm của lớp chuyển đến (đã làm ở hàm ChangeNewClassGrid)  thì disable nút lưu và nút hủy
            if (!gl.IsCurrentYear) ViewData[ClassMovementConstants.TeacherPermission] = false;
            List<ClassMovementViewModel> lst = new List<ClassMovementViewModel>();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //IEnumerable<ClassMovementViewModel> lst = this._Search(SearchInfo);
            ViewData[ClassMovementConstants.LST_CLASSMOVEMENT] = lst;
            SetViewData();
            // Xoa thong tin session cu
            Session.Remove("OldClassID");
            Session.Remove("NewClassID");
            Session.Remove("listOldPupil");
            Session.Remove("listTransferPupil");
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            GlobalInfo global = new GlobalInfo();
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["ToClassID"] = frm.ToClassID;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            //Dictionary[“ListClassID”] = UserInfo.ListClassByHeadTeacher, rồi gọi hàm ClassMovementBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary<string, object>) để lấy kết quả tìm kiếm.
            //listClassID = global.ListClassByHeadTeacher;
            SearchInfo["ListClassID"] = listClassID;
            IEnumerable<ClassMovementViewModel> lst = this._Search(SearchInfo);
            ViewData[ClassMovementConstants.LST_CLASSMOVEMENT] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(int rptSemester)
        {
            ClassMovementViewModel formMap = new ClassMovementViewModel();
            TryUpdateModel(formMap);
            Utils.Utils.TrimObject(formMap);
            GlobalInfo globalInfo = new GlobalInfo();
            List<ClassMovement> ListClassMovement = new List<ClassMovement>();
            List<PupilOfClass> listTransfer = Session["listTransferPupil"] as List<PupilOfClass>;
            if (!formMap.EducationLevelID.HasValue)
            {
                throw new BusinessException(Res.Get("ClassMovement_Validate_EducationLevel"));
            }
            else
                if (!formMap.FromClassID.HasValue)
                {
                    throw new BusinessException(Res.Get("ClassMovement_Validate_FromClass"));
                }
                else if (!formMap.ToClassID.HasValue)
                {
                    throw new BusinessException(Res.Get("ClassMovement_Validate_ToClass"));
                }
                else if (formMap.MovedDate == null)
                {
                    throw new BusinessException(Res.Get("ClassMovement_Validate_MovedDate"));
                }
            if (listTransfer != null && listTransfer.Count > 0)
            {
                foreach (PupilOfClass PupilOfClass in listTransfer)
                {

                    ClassMovement ClassMovement = new ClassMovement();
                    ClassMovement.AcademicYearID = globalInfo.AcademicYearID.Value;
                    ClassMovement.SchoolID = globalInfo.SchoolID.Value;
                    ClassMovement.EducationLevelID = formMap.EducationLevelID.Value;
                    ClassMovement.MovedDate = formMap.MovedDate.Value + DateTime.Now.TimeOfDay;
                    ClassMovement.FromClassID = formMap.FromClassID.Value;
                    ClassMovement.ToClassID = formMap.ToClassID.Value;
                    ClassMovement.Reason = formMap.Reason;
                    ClassMovement.PupilID = PupilOfClass.PupilID;
                    ClassMovement.Semester = rptSemester;
                    ListClassMovement.Add(ClassMovement);
                }
                this.ClassMovementBusiness.InsertClassMovement(globalInfo.UserAccountID, ListClassMovement, true);
                this.ClassMovementBusiness.Save();
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ClassMovement_Label_AddNewMessageError")));
            }
            return Json(new JsonMessage(Res.Get("ClassMovement_Label_AddNewMessage")));
        }

        #region SetViewData
        void SetViewData()
        {

            #region LoadSemester
            //Load RadioButton
            AcademicYear ay = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value);
            bool rbchecked = false;
            List<int> lsSemester = new List<int> { 1, 2 };
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio1 = new ViettelCheckboxList();
            ViettelCheckboxList viettelradio2 = new ViettelCheckboxList();
            if (lsSemester != null)
            {
                for (int i = 0; i < lsSemester.Count; i++)
                {
                    if (lsSemester[i] == 1)
                    {
                        viettelradio1 = new ViettelCheckboxList();
                        viettelradio1.Label = Res.Get("Common_Label_FirstSemester");
                        viettelradio1.Value = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        if ((ay.FirstSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.FirstSemesterEndDate))
                        {
                            viettelradio1.cchecked = true;
                            rbchecked = true;
                        }

                        lsradio.Add(viettelradio1);
                    }
                    else if (lsSemester[i] == 2)
                    {
                        viettelradio2 = new ViettelCheckboxList();
                        viettelradio2.Label = Res.Get("Common_Label_SecondSemester");
                        viettelradio2.Value = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        if ((ay.SecondSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.SecondSemesterEndDate))
                        {
                            viettelradio2.cchecked = true;
                            rbchecked = true;
                        }
                        lsradio.Add(viettelradio2);
                    }
                    if (!rbchecked)
                    {
                        viettelradio1.cchecked = true;
                    }
                }
                ViewData[ClassMovementConstants.LST_SEMESTER] = lsradio;
            }
            #endregion

            #region LoadEducationLevel
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[ClassMovementConstants.LST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[ClassMovementConstants.LST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }
            #endregion

            List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
            //IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
            // Thuc hien phan trang tung phan
            //Paginate<ClassMovementObject> paging2 = new Paginate<ClassMovementObject>(res2);
            //paging2.page = 1;
            //paging2.paginate();
            ViewData[ClassMovementConstants.LS_LISTNEWCLASS] = newls2;
            ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = newls2;
            ViewData[ClassMovementConstants.LS_CBNEWCLASS] = new SelectList(new string[] { });
            ViewData[ClassMovementConstants.LS_CBOLDCLASS] = new SelectList(new string[] { });
        }
        #endregion

        #region fill dropdown
        /// <summary>
        /// Load danh sách lớp cũ theo khối học được chọn
        /// </summary>
        /// <param name="EducationLevelId"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingOldClass(int? EducationLevelId)
        {

            return _GetClass(EducationLevelId);
        }

        /// <summary>
        /// Lấy danh sách lớp mới theo khối học đc chọn, loại bỏ lớp cũ đang chọn ra khỏi danh sách
        /// </summary>
        /// <param name="EducationLevelId"></param>
        /// <param name="OldClassID"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingNewClass(int? EducationLevelId, int? OldClassID)
        {

            return _GetNewClass(EducationLevelId, OldClassID);
        }

        /// <summary>
        /// Thực hiện lấy ra danh sách lớp cũ
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private JsonResult _GetClass(int? ID)
        {
            if (ID.HasValue)
            {
                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = new GlobalInfo();
                int? AcademicYearID = globalInfo.AcademicYearID;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", AcademicYearID.Value);
                dic.Add("EducationLevelID", ID);
                if (!globalInfo.IsAdminSchoolRole)
                {
                    dic.Add("EmployeeID", globalInfo.EmployeeID);
                    dic.Add("Type", 1);
                }
                IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).OrderBy(o => o.DisplayName);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }
        }

        /// <summary>
        /// Thực hiện lấy ra danh sách lớp mới
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="OldClassID"></param>
        /// <returns></returns>
        private JsonResult _GetNewClass(int? ID, int? OldClassID)
        {
            if (ID.HasValue)
            {

                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = new GlobalInfo();
                int? AcademicYearID = globalInfo.AcademicYearID;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", AcademicYearID.Value);
                dic.Add("EducationLevelID", ID);
                IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Where(o => o.ClassProfileID != OldClassID).OrderBy(o => o.DisplayName);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }

        }

        #endregion

        #region Change Class Grid
        /// <summary>
        /// Hiển thị danh sách học sinh đang học trong lớp cũ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeOldClassGrid(int? id)
        {
            if (Session["OldClassID"] != null) Session["OldClassID"] = null;
            Session["OldClassID"] = id;
            if (!id.HasValue)
            {
                List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
                //IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
                //// Thuc hien phan trang tung phan
                //Paginate<ClassMovementObject> paging2 = new Paginate<ClassMovementObject>(res2);
                //paging2.page = 1;
                //paging2.paginate();
                ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = newls2;
            }
            else
            {
                //ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = new SelectList(GetOldClassGridData(id.Value), "PupilProfileID", "FullName");
                ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = GetOldClassGridData(id.Value);
            }
            return PartialView("OldClassGrid");

        }

        public List<ClassMovementObject> GetOldClassGridData(int? id)
        {
            if (id.HasValue)
            {
                GlobalInfo globalInfo = new GlobalInfo();
                int? SchoolID = globalInfo.SchoolID;
                int? AcademicYearID = globalInfo.AcademicYearID;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", AcademicYearID.Value);
                dic["ClassID"] = id;
                IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).OrderBy(o => o.OrderInClass).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);
                if (lsPupilOfClass != null)
                {
                    List<ClassMovementObject> lsClassMovementObject = new List<ClassMovementObject>();
                    List<PupilOfClass> ListPupilOfClass = lsPupilOfClass.ToList();
                    foreach (PupilOfClass item in ListPupilOfClass)
                    {
                        ClassMovementObject newItem = new ClassMovementObject();
                        newItem.PupilOfClassID = item.PupilOfClassID;
                        newItem.PupilProfileID = item.PupilID;
                        newItem.ClassID = item.ClassID;
                        newItem.SchoolID = item.SchoolID;
                        newItem.AcademicYearID = item.AcademicYearID;
                        newItem.FullName = item.PupilProfile.FullName;
                        newItem.PupilCode = item.PupilProfile.PupilCode;
                        lsClassMovementObject.Add(newItem);
                    }
                    //return lsClassMovementObject;
                    // Thuc hien phan trang tung phan
                    //Luu danh sach hoc sinh dang hoc o lop cu
                    listOldPupil = lsPupilOfClass.ToList();
                    Session["listOldPupil"] = listOldPupil;
                    //Paginate<ClassMovementObject> paging = new Paginate<ClassMovementObject>(lsClassMovementObject.AsQueryable());
                    //paging.page = 1;
                    //paging.paginate();
                    return lsClassMovementObject;
                }
                else
                {
                    //
                    List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
                    //IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
                    ////return newls2;
                    //// Thuc hien phan trang tung phan
                    //Paginate<ClassMovementObject> paging2 = new Paginate<ClassMovementObject>(res2);
                    //paging2.page = 1;
                    //paging2.paginate();
                    return newls2;
                }
            }
            else
            {
                List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
                return newls2;
            }
        }

        /// <summary>
        /// Hàm thực hiện khi thay đổi lớp mới
        /// </summary>
        /// <param name="id">ip lớp mới</param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeNewClassGrid(int? id)
        {
            if (id.HasValue)
            {
                if (!new GlobalInfo().HasHeadTeacherPermission(id.Value)) ViewData[ClassMovementConstants.TeacherPermission] = false;
            }
            if (Session["NewClassID"] != null) Session["NewClassID"] = null;
            Session["NewClassID"] = id;
            Session.Remove("listTransferPupil");
            //if (!id.HasValue)
            //{
            List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
            //IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
            //// Thuc hien phan trang tung phan
            //Paginate<ClassMovementObject> paging2 = new Paginate<ClassMovementObject>(res2);
            //paging2.page = 1;
            //paging2.paginate();
            ViewData[ClassMovementConstants.LS_LISTNEWCLASS] = newls2;
            //}
            //else
            //{
            //    ViewData[ClassMovementConstants.LS_LISTNEWCLASS] = GetNewClassGridData(id.Value);
            //}
            return PartialView("NewClassGrid");
        }

        /// <summary>
        /// Load lại list khi học sinh được chuyển
        /// </summary>
        /// <param name="id">id lớp mới</param>
        /// <returns></returns>
        //public Paginate<ClassMovementObject> GetNewClassGridData(int id, int[] checkedOldPupil)
        //{
        //    GlobalInfo globalInfo = new GlobalInfo();
        //    List<ClassMovementObject> lstTranferred = new List<ClassMovementObject>();
        //    int? SchoolID = globalInfo.SchoolID;
        //    int? AcademicYearID = globalInfo.AcademicYearID;
        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    dic.Add("AcademicYearID", AcademicYearID.Value);
        //    dic.Add("SchoolID", SchoolID.Value);
        //    dic["ClassID"] = id;
        //    IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic);

        //    if (lsPupilOfClass != null)
        //    {
        //        //Chỉ hiển thị học sinh có Status = 1
        //        lsPupilOfClass = lsPupilOfClass.Where(em => (em.Status == 1));
        //        List<ClassMovementObject> lsTransferDataOptionObject = new List<ClassMovementObject>();
        //        foreach (PupilOfClass item in lsPupilOfClass.ToList())
        //        {
        //            ClassMovementObject newItem = new ClassMovementObject();
        //            newItem.PupilProfileID = item.PupilID;
        //            newItem.ClassID = item.ClassID;
        //            newItem.SchoolID = item.SchoolID;
        //            newItem.AcademicYearID = item.AcademicYearID;

        //            //pupil code
        //            PupilProfile pupilPro = PupilProfileBusiness.All.Where(em => (em.PupilProfileID == item.PupilID)).FirstOrDefault();
        //            newItem.PupilCode = pupilPro.PupilCode;
        //            newItem.FullName = pupilPro.FullName;

        //            lsTransferDataOptionObject.Add(newItem);

        //        }

        //        // Thuc hien phan trang tung phan
        //        Paginate<ClassMovementObject> paging = new Paginate<ClassMovementObject>(lsTransferDataOptionObject.AsQueryable());
        //        paging.page = 1;
        //        paging.paginate();
        //        return paging;
        //    }
        //    else
        //    {
        //        //
        //        List<ClassMovementObject> newls2 = new List<TransferDataOptionObject>();
        //        IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
        //        // Thuc hien phan trang tung phan
        //        Paginate<ClassMovementObject> paging2 = new Paginate<TransferDataOptionObject>(res2);
        //        paging2.page = 1;
        //        paging2.paginate();
        //        return paging2;
        //    }

        //}
        #endregion

        #region button transfer click >>
        /// <summary>
        /// Load lai hoc sinh lop cu khi nhan nut >>
        /// </summary>
        /// <param name="checkedOldPupil"></param>
        /// <returns></returns>
        private List<ClassMovementObject> ReloadOldClassGrid(int[] checkedOldPupil, List<PupilOfClass> list, int type)
        {
            checkedOldPupil = checkedOldPupil ?? new int[] { };
            listOldPupil = list;
            // listOldPupil = (List<PupilOfClass>)Session["listOldPupil"];
            IDictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;
            int OldClassID = (int)Session["OldClassID"];
            int listOldPupilCount = listOldPupil.Count;
            if (listOldPupilCount > 0)
            {
                for (int i = listOldPupilCount - 1; i >= 0; i--)
                {
                    for (int j = 0; j < checkedOldPupil.Length; j++)
                    {
                        if (listOldPupil[i].PupilOfClassID == checkedOldPupil[j])
                        {
                            listOldPupil.RemoveAt(i);
                            break;
                        }
                    }
                }
            }
            if (type == ClassMovementConstants.New2Old)
            {
                Session["listTransferPupil"] = listOldPupil;
            }
            else if (type == ClassMovementConstants.Old2New)
            {
                Session["listOldPupil"] = listOldPupil;
            }
            List<ClassMovementObject> lsClassMovementObject = new List<ClassMovementObject>();
            if (listOldPupil.Count > 0)
            {
                foreach (PupilOfClass item in listOldPupil)
                {
                    ClassMovementObject newItem = new ClassMovementObject();
                    newItem.PupilOfClassID = item.PupilOfClassID;
                    newItem.PupilProfileID = item.PupilID;
                    newItem.ClassID = item.ClassID;
                    newItem.SchoolID = item.SchoolID;
                    newItem.AcademicYearID = item.AcademicYearID;
                    PupilProfile pupilPro = PupilProfileBusiness.All.Where(em => (em.PupilProfileID == item.PupilID)).FirstOrDefault();
                    newItem.FullName = pupilPro.FullName;
                    newItem.PupilCode = pupilPro.PupilCode;
                    lsClassMovementObject.Add(newItem);
                }
                //Session["listOldPupil"] = null;
                //Paginate<ClassMovementObject> paging = new Paginate<ClassMovementObject>(lsClassMovementObject.AsQueryable());
                //paging.page = 1;
                //paging.paginate();
                return lsClassMovementObject;
            }
            List<ClassMovementObject> newls2 = new List<ClassMovementObject>();
            //IQueryable<ClassMovementObject> res2 = newls2.AsQueryable();
            //// Thuc hien phan trang tung phan
            //Paginate<ClassMovementObject> paging2 = new Paginate<ClassMovementObject>(res2);
            //paging2.page = 1;
            //paging2.paginate();
            return newls2;
        }

        /// <summary>
        /// Load lai hoc sinh lop moi sau khi an nut >>
        /// </summary>
        /// <param name="checkedOldPupil"></param>
        /// <returns></returns>
        private List<ClassMovementObject> ReloadNewClassGrid(int[] checkedOldPupil, List<PupilOfClass> list, int type)
        {
            checkedOldPupil = checkedOldPupil ?? new int[] { };
            listTransferPupil = list;
            GlobalInfo globalInfo = new GlobalInfo();
            int? SchoolID = globalInfo.SchoolID;
            int? AcademicYearID = globalInfo.AcademicYearID;
            int OldClassID = (int)Session["OldClassID"];

            foreach (int oldPupil in checkedOldPupil)
            {
                PupilOfClass thisPupil = PupilOfClassBusiness.Find(oldPupil);
                listTransferPupil.Add(thisPupil);
            }
            if (type == ClassMovementConstants.New2Old)
            {
                Session["listOldPupil"] = listTransferPupil;
            }
            else if (type == ClassMovementConstants.Old2New)
            {
                Session["listTransferPupil"] = listTransferPupil;
            }
            List<ClassMovementObject> lsClassMovementObject = new List<ClassMovementObject>();

            foreach (PupilOfClass item in listTransferPupil)
            {
                ClassMovementObject newItem = new ClassMovementObject();
                newItem.PupilOfClassID = item.PupilOfClassID;
                newItem.PupilProfileID = item.PupilID;
                newItem.ClassID = item.ClassID;
                newItem.SchoolID = item.SchoolID;
                newItem.AcademicYearID = item.AcademicYearID;
                newItem.FullName = item.PupilProfile.FullName;
                newItem.PupilCode = item.PupilProfile.PupilCode;
                lsClassMovementObject.Add(newItem);
            }
            //Paginate<ClassMovementObject> paging = new Paginate<ClassMovementObject>(lsClassMovementObject.AsQueryable());
            //paging.page = 1;
            //paging.paginate();
            return lsClassMovementObject;
        }


        [ValidateAntiForgeryToken]
        public ActionResult TransferDataForward(int[] checkedOldPupil)
        {
            checkedOldPupil = checkedOldPupil ?? new int[] { };

            if (checkedOldPupil.Count() > 0)
            {
                List<PupilOfClass> list = (List<PupilOfClass>)Session["listOldPupil"];
                List<PupilOfClass> listTransfer = new List<SMAS.Models.Models.PupilOfClass>();
                if (Session["listTransferPupil"] != null)
                {
                    listTransfer = (List<PupilOfClass>)Session["listTransferPupil"];
                }
                ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = ReloadOldClassGrid(checkedOldPupil, list, ClassMovementConstants.Old2New);
                ViewData[ClassMovementConstants.LS_LISTNEWCLASS] = ReloadNewClassGrid(checkedOldPupil, listTransfer, ClassMovementConstants.Old2New);

            }
            return View("ClassMovementGrid");

        }



        [ValidateAntiForgeryToken]
        public ActionResult TransferDataBackward(int[] checkedNewPupil)
        {
            checkedNewPupil = checkedNewPupil ?? new int[] { };

            if (checkedNewPupil.Count() > 0)
            {
                List<PupilOfClass> list = (List<PupilOfClass>)Session["listOldPupil"];
                List<PupilOfClass> listTransfer = new List<SMAS.Models.Models.PupilOfClass>();
                if (Session["listTransferPupil"] != null)
                {
                    listTransfer = (List<PupilOfClass>)Session["listTransferPupil"];
                }
                ViewData[ClassMovementConstants.LS_LISTOLDCLASS] = ReloadNewClassGrid(checkedNewPupil, list, ClassMovementConstants.New2Old);
                ViewData[ClassMovementConstants.LS_LISTNEWCLASS] = ReloadOldClassGrid(checkedNewPupil, listTransfer, ClassMovementConstants.New2Old);

            }
            return View("ClassMovementGrid");
        }
        #endregion

        //============Form Search=============/

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingToClass(int? educationLevelId)
        {
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (educationLevelId != null)
            {
                GlobalInfo global = new GlobalInfo();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = educationLevelId;
                dic["AcademicYearID"] = global.AcademicYearID;
                // Đối với UserInfo.IsSchoolAdmin() = False thêm điều kiện:
                //+ Dictionnary[“EmployeeID”] = UserInfo.EmployeeID   
                //+ Dictionary[“Type”] = 1. Kết quả thu được lstClass
                if (!global.IsAdminSchoolRole)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER;
                }

                lst = this.ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
                foreach (ClassProfile item in lst)
                {
                    listClassID.Add(item.ClassProfileID);
                }
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ClassMovementViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ClassMovement> query = this.ClassMovementBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).OrderBy(o => o.PupilProfile.FullName);
            IQueryable<ClassMovementViewModel> lst = query.Select(o => new ClassMovementViewModel
            {
                ClassMovementID = o.ClassMovementID,
                PupilID = o.PupilID,
                FromClassID = o.FromClassID,
                ToClassID = o.ToClassID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                MovedDate = o.MovedDate,
                Reason = o.Reason,
                FullName = o.PupilProfile.FullName,
                PupilCode = o.PupilProfile.PupilCode,
                FromClassName = o.ClassProfile.DisplayName,
                ToClassName = o.ClassProfile1.DisplayName,
                PupilName = o.PupilProfile.Name
            });
            int ToClassID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "ToClassID");
            List<ClassMovementViewModel> list;
            // Khong chon lop thi xep theo ten
            if (ToClassID == 0)
            {
                list = lst.OrderBy(o => o.PupilName).ThenBy(o => o.FullName).ToList();
            }
            else
            {
                // Co thong tin lop thi se lay thu tu trong lop de sap xep

                list = lst.ToList();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ToClassID;
                dic["check"] = "true";
                List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic)
                    .ToList()
                    .Where(o => list.Select(x => x.PupilID).Contains(o.PupilID))
                    .OrderBy(o => o.OrderInClass)
                    .ToList();
                foreach (PupilOfClass poc in listPupilOfClass)
                {
                    int index = list.FindIndex(o => o.PupilID == poc.PupilID);

                    if (index >= 0)
                    {
                        ClassMovementViewModel item = list[index];
                        item.OrderInClass = poc.OrderInClass;
                    }
                }
                list = list.OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilName).ThenBy(o => o.FullName).ToList();
            }

            return list;
        }
    }
}





