/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ClassMovementArea
{
    public class ClassMovementConstants
    {
        public const string LST_CLASSMOVEMENT = "listClassMovement";
        public const string LST_SEMESTER = "listSemester";
        public const string LST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LS_CBOLDCLASS = "listFromClass";
        public const string LS_CBNEWCLASS = "listToClass";
        public const string LS_LISTOLDCLASS = "listOldClass";
        public const string LS_LISTNEWCLASS = "listPupilMovemented";
        public const string TeacherPermission = "PERMISSION";
        public const string LS_PUPILTRANSFER = "ListPupilTransfer"; //List hoc sinh duoc chon de chuyen sang lop moi
        public const int New2Old = 1;
        public const int Old2New = 2;
    }
}