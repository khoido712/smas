﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.RewardCommentFinalArea;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.RewardCommentFinalArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Filter;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.RewardCommentFinalArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class RewardCommentFinalController : BaseController
    {
        //
        // GET: /RewardCommentFinalArea/RewardCommentFinal/
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IRewardCommentFinalBusiness RewardCommentFinalBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IRewardFinalBusiness RewardFinalBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        public RewardCommentFinalController(IClassProfileBusiness classProfileBusiness, IPupilOfClassBusiness pupilOfClassBusiness, IRewardCommentFinalBusiness rewardCommentFinalBusiness,
                                            IRewardFinalBusiness rewardFinalBusiness, ISummedEndingEvaluationBusiness summedEndingEvaluationBusiness, IEvaluationCriteriaBusiness evaluationCriteriaBusiness,
                                            IAcademicYearBusiness academicYearBusiness, ISummedEvaluationBusiness summedEvaluationBusiness, IClassSubjectBusiness classSubjectBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.RewardCommentFinalBusiness = rewardCommentFinalBusiness;
            this.RewardFinalBusiness = rewardFinalBusiness;
            this.SummedEndingEvaluationBusiness = summedEndingEvaluationBusiness;
            this.EvaluationCriteriaBusiness = evaluationCriteriaBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SummedEvaluationBusiness = summedEvaluationBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
        }

        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return PartialView();
        }

        public PartialViewResult AjaxLoadGrid(int ClassID,int SemesterID)
        {
            ViewData[RewardCommentFinalConstant.LIST_REWARDCOMMENT_FINAL] = this.GetDataViewModel(ClassID, SemesterID);
            SetVisbileButton(SemesterID, ClassID);
            return PartialView("_GridRewardCommentFinal");
        }

        public PartialViewResult AjaxLoadComment(int PupilID,int ClassID,int SemesterID)
        {
            //Load danh sach mon hoc
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = SemesterID;
            iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting
            }).ToList().OrderBy(o => o.OrderInSubject).ToList();
            ViewData[RewardCommentFinalConstant.LIST_SUBJECT] = lstSubject;


            //lay thong tin nhan xet theo mon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"PupilID",PupilID},
                {"Year",objAcademicYear.Year},
                {"EvaluationId",GlobalConstants.TAB_EDUCATION}//Chiendd: 29/12/2015, bo sung tieu chi lay theo danh gia mon hoc de khong lay nham sang phan pham chat
            };
            List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel> lstSubjectView = RewardCommentFinalBusiness.GetCommentByPupil(dic);

            //lay nhan xet nang luc pham chat
            List<SummedEvaluationBO> lstSummedEvaluation = RewardCommentFinalBusiness.GetCapacityOrQuality(dic);

            //lay danh gia nang luc pham chat cua hoc sinh
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dic).Where(p => p.SemesterID == SemesterID).ToList();

            SummedEvaluationBO objSummedEvaluationBO = new SummedEvaluationBO();

            //lấy các mặt tiêu chí của lớp
            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();
            EvaluationCriteria objEvaluationCriteria = null;
            List<EvaluationCriteriaModel> lstEvaluationCriteriaModel = new List<EvaluationCriteriaModel>();
            EvaluationCriteriaModel objEvaluationCriteriaModel = null;

            for (int i = 0; i < lstEvaluationCriteria.Count; i++)
            {
                objEvaluationCriteria = lstEvaluationCriteria[i];
                objEvaluationCriteriaModel = new EvaluationCriteriaModel();
                objSummedEvaluationBO = lstSummedEvaluation.Where(p => p.EvaluationCriteriaID == objEvaluationCriteria.EvaluationCriteriaID).FirstOrDefault();//lay nhan xet nang luc pham chat theo tung tieu chi
                objEvaluationCriteriaModel.EvaluationCriteriaID = objEvaluationCriteria.EvaluationCriteriaID;
                objEvaluationCriteriaModel.EvaluationCriteriaName = objEvaluationCriteria.CriteriaName;
                objEvaluationCriteriaModel.EvaluationID = objEvaluationCriteria.TypeID;
                if (objSummedEvaluationBO != null)
                {
                    objEvaluationCriteriaModel.EndingComment = objSummedEvaluationBO.EndingComment;//nhan xet
                    
                }
                objEvaluationCriteriaModel.EndingEvaluation = lstSummedEndingEvaluation.Where(p=>p.EvaluationID == objEvaluationCriteria.TypeID).Select(p=>p.EndingEvaluation).FirstOrDefault();//danh gia
                lstEvaluationCriteriaModel.Add(objEvaluationCriteriaModel);
            }

            ViewData[RewardCommentFinalConstant.LIST_EVALUATION_CRITERIA_MODEL] = lstEvaluationCriteriaModel;

            RewardCommentFinalViewModel objRewardComment = new RewardCommentFinalViewModel();
            objRewardComment.lstSubject = lstSubjectView;

            ViewData[RewardCommentFinalConstant.REWARDCOMMENT_FINAL] = objRewardComment; 
            return PartialView("_PupilComment");
        }

        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        [ActionAudit]
        public JsonResult InsertOrUpdateRewardCommentFinal(FormCollection frm)
        {

            return InsertOrUpdate(frm);
        }

        [ValidateAntiForgeryToken]
        [ActionAudit]
        [ValidateInput(false)]
        public JsonResult ImportError(FormCollection frm)
        {
            return InsertOrUpdate(frm);
        }

        [ValidateAntiForgeryToken]
        private JsonResult InsertOrUpdate(FormCollection frm)
        {
            try
            {
                int ClassID = frm["ClassID"] != null ? int.Parse(frm["ClassID"]) : 0;
                int SemesterID = frm["SemesterID"] != null ? int.Parse(frm["SemesterID"]) : 0;
                string ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;

                //Check quyen nguoi dung
                bool isAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
                bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                if (!(isAdmin || isGVCN))
                {
                    return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ErrorMesage_Permision") + ClassName), "error");
                }
                // Lay danh sach hoc sinh
                List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                    .OrderBy(u => u.OrderInClass)
                                                                    .ThenBy(u => u.Name)
                                                                    .ThenBy(u => u.PupilFullName)
                                                                    .ToList();
                PupilOfClassBO objPupilOfClassBO = null;
                List<RewardCommentFinal> lstRewardCommentFinal = new List<RewardCommentFinal>();
                RewardCommentFinal objRewardCommentFinal = null;


                for (int i = 0; i < listPupil.Count; i++)
                {
                    objPupilOfClassBO = listPupil[i];
                    objRewardCommentFinal = new RewardCommentFinal();
                    if (!"on".Equals(frm["chk_" + objPupilOfClassBO.PupilID]))
                    {
                        continue;
                    }

                    objRewardCommentFinal.RewardCommentFinalID = RewardCommentFinalBusiness.GetNextSeq<int>();
                    objRewardCommentFinal.SchoolID = _globalInfo.SchoolID.Value;
                    objRewardCommentFinal.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objRewardCommentFinal.SemesterID = SemesterID;
                    objRewardCommentFinal.PupilID = objPupilOfClassBO.PupilID;
                    objRewardCommentFinal.ClassID = objPupilOfClassBO.ClassID;
                    objRewardCommentFinal.LastDigitSchoolID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value, RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL);
                    objRewardCommentFinal.CreateTime = DateTime.Now;
                    objRewardCommentFinal.OutstandingAchievement = frm["OutStanding_" + objPupilOfClassBO.PupilID].Trim();
                    objRewardCommentFinal.RewardID = frm["RewardID_" + objPupilOfClassBO.PupilID];
                    lstRewardCommentFinal.Add(objRewardCommentFinal);
                }
                if (lstRewardCommentFinal != null && lstRewardCommentFinal.Count > 0)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"ClassID",ClassID},
                        {"SemesterID",SemesterID},
                        {"ModPartion",RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL}
                    };
                    RewardCommentFinalBusiness.InsertOrUpdate(dic, lstRewardCommentFinal);
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ValidateEmpty"), "error"));
                }

                //ghi log nguoi dung
                this.SetLog(ClassID, ClassName, SemesterID, SMAS.Business.Common.GlobalConstants.ACTION_ADD);

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdate","null", ex);
                return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ErrorMesage"), "error"));
            }
        }

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult DeleteRewardComment(string ArrayID,int ClassID,int SemesterID)
        {
            try
            {
                string className = ClassProfileBusiness.Find(ClassID).DisplayName;
                //Check quyen nguoi dung
                bool isAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
                bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                if (!(isAdmin || isGVCN))
                {
                    return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ErrorMesage_Permision") + className), "error");
                }

                string[] arrayID = ArrayID.Split(new string[] {","},StringSplitOptions.RemoveEmptyEntries);
                List<string> strArray = arrayID.ToList();
                List<int> IDList = strArray.Select(int.Parse).ToList(); ;

                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"ClassID",ClassID},
                    {"SemesterID",SemesterID},
                    {"ModPartion",RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL}
                };
                List<RewardCommentFinal> lstRewardComment = RewardCommentFinalBusiness.GetListRewardCommentFinal(dic);
                RewardCommentFinal objRewardComment = null;
                List<RewardCommentFinal> lstDelete = new List<RewardCommentFinal>();
                int rewardCommentFinalID = 0;

                for (int i = 0; i < IDList.Count; i++)
                {
                    rewardCommentFinalID = IDList[i];
                    objRewardComment = lstRewardComment.Where(p => p.RewardCommentFinalID == rewardCommentFinalID).FirstOrDefault();
                    if (objRewardComment != null)
                    {
                        lstDelete.Add(objRewardComment);
                    }
                }
                if (lstDelete != null && lstDelete.Count > 0)
                {
                    RewardCommentFinalBusiness.DeleteAll(lstDelete);
                    RewardCommentFinalBusiness.Save();
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ValidateEmpty"), "error"));
                }
                //luu log nguoi dung
                this.SetLog(ClassID, className, SemesterID, SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                return Json(new JsonMessage(Res.Get("success_del"), "success"));
            }
            catch (Exception ex)
            {
                string paraList = string.Format("ArrayID={0}, ClassID={1},SemesterID={2}", ArrayID, ClassID, SemesterID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteRewardComment", paraList,ex);
                return Json(new JsonMessage(Res.Get("RewardCommentFinal_Label_ErrorMesage"),"error"));
            }
        }

        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int ClassID, int SemesterID)
        {
            if (attachments == null || attachments.Count() <= 0)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            //The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 1024)
                {
                    return Json(new JsonMessage(Res.Get("JudgeRecord_Validate_FileMaxSize"), "error"));
                }
                string className = ClassProfileBusiness.Find(ClassID).DisplayName;

                #region check permision
                bool isSchoolAdmin = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
                bool isGVBM = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                if (!(isSchoolAdmin || isGVBM))
                {
                    return Json(new JsonMessage("Thầy/cô không có quyền Import chức năng này","error"));
                }
                #endregion

                //luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);

                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = null;
                try
                {
                    sheet = oBook.GetSheet(1);
                }
                catch
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                string errMessage = this.ErrValidateFileExcel(sheet, SemesterID, ClassID);
                if (!string.IsNullOrEmpty(errMessage))
                {
                    return Json(new JsonMessage(errMessage,"error"));
                }

                List<RewardCommentFinalViewModel> excelDataList = this.GetDataToFileImport(sheet, ClassID, SemesterID, className);
                List<RewardCommentFinalViewModel> lstErr = excelDataList.Where(p => p.Pass == false).ToList();
                if ( lstErr != null && lstErr.Count > 0)
                {
                    ViewData[RewardCommentFinalConstant.LIST_REWARDCOMMENT_FINAL] = excelDataList;
                    return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "Importerror")); 
                }

                if (excelDataList != null && excelDataList.Count > 0)
                {
                    List<RewardCommentFinal> lstRewardCommentFinal = new List<RewardCommentFinal>();
                    RewardCommentFinal objRewardCommentFinal = null;
                    RewardCommentFinalViewModel objRewardView = null;
                    for (int i = 0; i < excelDataList.Count; i++)
                    {
                        objRewardCommentFinal = new RewardCommentFinal();
                        objRewardView = excelDataList[i];

                        objRewardCommentFinal.RewardCommentFinalID = RewardCommentFinalBusiness.GetNextSeq<int>();
                        objRewardCommentFinal.SchoolID = _globalInfo.SchoolID.Value;
                        objRewardCommentFinal.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objRewardCommentFinal.SemesterID = SemesterID;
                        objRewardCommentFinal.PupilID = objRewardView.PupilID;
                        objRewardCommentFinal.ClassID = ClassID;
                        objRewardCommentFinal.LastDigitSchoolID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value,RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL);
                        objRewardCommentFinal.CreateTime = DateTime.Now;
                        objRewardCommentFinal.OutstandingAchievement = objRewardView.OutstandingAchievement;
                        objRewardCommentFinal.RewardID = objRewardView.RewardID;
                        lstRewardCommentFinal.Add(objRewardCommentFinal);
                    }
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"ClassID",ClassID},
                        {"SemesterID",SemesterID},
                        {"ModPartion",RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL}
                    };
                    RewardCommentFinalBusiness.InsertOrUpdate(dic, lstRewardCommentFinal);
                    //luu log nguoi dung
                    this.SetLog(ClassID, className, SemesterID, SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);

                    return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "success"));
                }

            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "success"));
        }

        public FileResult ExportExcel(int ClassID,int SemesterID,string ClassName)
        {
            string templatePath = string.Empty;
            string OutPutName = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "BangNhanXetCuoiHocKi_LopK1_1.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheetReward = oBook.GetSheet(2);
            List<RewardCommentFinalViewModel> lstRewardCommentFinal = this.GetDataViewModel(ClassID, SemesterID);
            OutPutName = SetCellValueExcel(sheet,sheetReward,lstRewardCommentFinal, ClassID, SemesterID,ClassName);
            sheet.ProtectSheet();
            sheetReward.ProtectSheet();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = OutPutName;
            return result;
        }

        private string SetCellValueExcel(IVTWorksheet sheet,IVTWorksheet sheetReward,List<RewardCommentFinalViewModel> lstRewardCommentFinal,int ClassID,int SemesterID,string ClassName)
        {
            #region fill tieu de
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            sheet.SetCellValue("A2",_globalInfo.SchoolName);
            string Title = string.Format("BẢNG ĐÁNH GIÁ NHẬN XÉT VÀ KHEN THƯỞNG CUỐI HỌC KỲ {0} - {1}", (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "1" : "2"), ClassName.ToUpper());
            sheet.SetCellValue("A3", Title);
            sheet.SetCellValue("A4", "NĂM HỌC " + objAcademicYear.DisplayTitle);
            #endregion
            #region fill du lieu
            int startRow = 7;
            int startColumn = 5;
            RewardCommentFinalViewModel objRewardViewModel = null;
            StringBuilder strRate = new StringBuilder();
          
            for (int i = 0; i < lstRewardCommentFinal.Count; i++)
            {
                strRate = new StringBuilder();
                objRewardViewModel = lstRewardCommentFinal[i];
                sheet.SetCellValue(startRow, 1, i + 1);//STT
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 2, objRewardViewModel.PupilCode);//Mã HS
                sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 3, objRewardViewModel.FullName);//Họ tên
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                sheet.SetCellValue(startRow, 4, objRewardViewModel.BirthDay.ToString(RewardCommentFinalConstant.FORMAT_DATE));//Ngày sinh
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                //đánh giá
                sheet.SetCellValue(startRow, startColumn,objRewardViewModel.Comment);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                startColumn++;

                //các thành tích nổi bật
                sheet.SetCellValue(startRow, startColumn, objRewardViewModel.OutstandingAchievement);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                startColumn++;

                //hình thức khen thưởng
                sheet.SetCellValue(startRow, startColumn, objRewardViewModel.RewardMode);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, startColumn, startRow, startColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                startColumn = 5;
                if ((i + 1) % 5 == 0)
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }
                else
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + 2).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                if ((i + 1) == lstRewardCommentFinal.Count())
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }

                //học sinh khác đang học tô đỏ ghạch ngang
                if (objRewardViewModel.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, startColumn + 2).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                    sheet.GetRange(startRow, startColumn, startRow, startColumn + 2).IsLock = true;
                }

                startRow++;
            }
            //dong ghi chu
            sheet.SetCellValue(8 + lstRewardCommentFinal.Count,1,"Ghi chú: Hình thức khen thưởng xem trong sheet HinhThucKT. Tên hình thức khen thưởng phân cách bởi dấu ;");
            sheet.GetRange(8 + lstRewardCommentFinal.Count, 1, 8 + lstRewardCommentFinal.Count, startColumn + 2).MergeLeft();
            #endregion
            #region fill sheet HTKT
            startRow = 4;
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID).ToList().OrderBy(p=>p.RewardMode).ToList();
            int k = 0;
            foreach (RewardFinal item in lstRewardFinal)
            {
                k++;
                sheetReward.SetCellValue(startRow, 1, k);//STT
                sheetReward.GetRange(startRow, 1, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                sheetReward.SetCellValue(startRow, 2, item.RewardMode);
                sheetReward.GetRange(startRow, 1, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                startRow++;
            }
            #endregion
            return string.Format("BangNhanXetCuoiHocKy{0}_{1}.xls",SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "1" : "2",Utils.Utils.StripVNSign(ClassName.Replace(" ","").ToLower()));
        }

        private List<RewardCommentFinalViewModel> GetDataViewModel(int classID,int semesterID)
        {
            //Load danh sach mon hoc
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = classID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = semesterID;
            iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting
            }).ToList().OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
            ClassSubjectBO objClassSubjectBO = null;

            List<RewardCommentFinalViewModel> lstRewardModel = new List<RewardCommentFinalViewModel>();
            RewardCommentFinalViewModel objViewModel = null;
            // Lay danh sach hoc sinh
            List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilInClass(classID)
                                                                .OrderBy(u => u.OrderInClass)
                                                                .ThenBy(u => u.Name)
                                                                .ThenBy(u => u.PupilFullName)
                                                                .ToList();

            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID).ToList().OrderBy(p=>p.RewardMode).ToList();

            ViewData[RewardCommentFinalConstant.LIST_REWARD_FINAL] = lstRewardFinal;
            RewardFinal objRewardFinal = null;
            IDictionary<int, string> DicRewardFinal = new Dictionary<int, string>();
            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                objRewardFinal = lstRewardFinal[i];
                DicRewardFinal.Add(objRewardFinal.RewardFinalID, objRewardFinal.RewardMode);
            }
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"SemesterID",semesterID},
                {"ModPartion",RewardCommentFinalConstant.PARTION_REWARD_COMMMENT_FINAL},
                {"Year",objAcademicYear.Year}
            };
            List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel> lstSubjectView = RewardCommentFinalBusiness.GetCommentByPupil(dic);//lấy thông tin nhận xét theo môn
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dic).Where(p=>p.SemesterID == semesterID).ToList();//thông tin nhận xét năng lực phẩm chất

            List<RewardCommentFinal> lstRewardComment = RewardCommentFinalBusiness.GetListRewardCommentFinal(dic);
            RewardCommentFinal objRewardCommentFinal = null;
            PupilOfClassBO objPupil = null;
            string RewardMode = string.Empty;

            List<string> lstStr = new List<string>();
            List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel> lstSubjectViewByPupil = new List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel>();
            SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel objSubjectView = new Business.Business.RewardCommentFinalBusiness.SubjectViewModel();
            string comment = string.Empty;
            string capaciy = string.Empty;
            string quality = string.Empty;
            int countSubject = 0;
            int countComment = 0;
            for (int i = 0; i < listPupil.Count; i++)
            {
                RewardMode = string.Empty;
                objPupil = listPupil[i];
                lstStr = new List<string>();
                comment = string.Empty;
                objRewardCommentFinal = lstRewardComment.Where(p => p.PupilID == objPupil.PupilID).FirstOrDefault(); 
                objViewModel = new RewardCommentFinalViewModel();
                objViewModel.RewardCommentFinalID = objRewardCommentFinal != null ? objRewardCommentFinal.RewardCommentFinalID : 0;
                objViewModel.PupilID = objPupil.PupilID;
                objViewModel.Status = objPupil.Status;
                objViewModel.FullName = objPupil.PupilFullName;
                objViewModel.PupilCode = objPupil.PupilCode;
                objViewModel.BirthDay = objPupil.Birthday;
                countSubject = 0;
                countComment = 0;

                lstSubjectViewByPupil = lstSubjectView.Where(p => p.PupilID == objPupil.PupilID && p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
                for (int j = 0; j < lstSubject.Count; j++)
                {
                    objClassSubjectBO = lstSubject[j];
                    objSubjectView = lstSubjectViewByPupil.Where(s => s.SubjectID == objClassSubjectBO.SubjectID).FirstOrDefault();
                    comment += objClassSubjectBO.DisplayName + ": ";
                    if (objSubjectView != null)
                    {
                        if (string.IsNullOrEmpty(objSubjectView.EndingComment))
                        {
                            countSubject++;
                        }

                        if (!string.IsNullOrEmpty(objSubjectView.EndingEvaluation))
                        {
                            comment += objSubjectView.EndingEvaluation + (objSubjectView.PeriodicEndingMark > 0 ? ", " : "");
                        }
                        else
                        {
                            countSubject++;
                        }
                        if (objSubjectView.PeriodicEndingMark > 0 )
                        {
                            comment += "KTCK " + objSubjectView.PeriodicEndingMark;
                        }
                        else if(objClassSubjectBO.IsCommenting == 0)
                        {
                            countSubject++;
                        }
                    }
                    else
                    {
                        countSubject++;
                    }
                    comment += "; ";
                }
                capaciy = lstSummedEndingEvaluation.Where(p => p.PupilID == objPupil.PupilID && p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                quality = lstSummedEndingEvaluation.Where(p => p.PupilID == objPupil.PupilID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                if (string.IsNullOrEmpty(capaciy))
                {
                    countComment++;
                }
                comment += "\nNăng lực: " + capaciy;

                if (string.IsNullOrEmpty(quality))
                {
                    countComment++;
                    
                }
                comment += "\nPhẩm chất: " + quality;
                objViewModel.IsCommentSubject = countSubject > 0 ? false : true;
                objViewModel.IsCommentEvaluation = countComment > 0 ? false : true;
                objViewModel.Comment = comment;
                if (objRewardCommentFinal != null)
                {
                    objViewModel.OutstandingAchievement = objRewardCommentFinal.OutstandingAchievement;
                    if (!string.IsNullOrEmpty(objRewardCommentFinal.RewardID))
                    {
                        lstStr = objRewardCommentFinal.RewardID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();    
                    }

                    for (int j = 0; j < lstStr.Count; j++)
                    {
                        RewardMode = RewardMode + DicRewardFinal[int.Parse(lstStr[j])] + "; ";
                    }

                    objViewModel.RewardID = objRewardCommentFinal.RewardID;
                    objViewModel.RewardMode = RewardMode;
                }
                lstRewardModel.Add(objViewModel);
            }
            return lstRewardModel;
        }

        private string ErrValidateFileExcel(IVTWorksheet sheet, int semesterID, int classID)
        {
            string ErrMessage = "";
            string Title = "";
            string semesterName = "";
            string className = "";
            string academicYearName = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).DisplayTitle.Replace("-", "").Replace(" ", "");
            string TitleYear = "";
            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                semesterName = "HOCKY1";
            }
            else if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                semesterName = "HOCKY2";
            }

            className = Utils.Utils.StripVNSign(ClassProfileBusiness.Find(classID).DisplayName.ToUpper().Replace(" ", ""));
            if (sheet.GetCellValue(3, 1) != null)
            {
                Title = Utils.Utils.StripVNSign(sheet.GetCellValue(3, 1).ToString().Replace(" ", "")).ToUpper();
            }

            if (sheet.GetCellValue(4, 1) != null)
            {
                TitleYear = Utils.Utils.StripVNSign(sheet.GetCellValue(4, 1).ToString().Replace("-", "").Replace(" ", ""));
            }

            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(TitleYear) || !Title.Contains("BANGDANHGIANHANXETVAKHENTHUONGCUOIHOCKY"))
            {
                ErrMessage = Res.Get("Reward_Validate_FileImport_Specimen");
            }
            else if (!Title.Contains(semesterName))
            {
                ErrMessage = Res.Get("Reward_Validate_SemesterName");
            }
            else if (!Title.Contains(className))
            {
                ErrMessage = Res.Get("Reward_Validate_ClassName");
            }
            else if (!TitleYear.Contains(academicYearName))
            {
                ErrMessage = Res.Get("Reward_Validate_AcademicYear");
            }

            return ErrMessage;
        }

        private List<RewardCommentFinalViewModel> GetDataToFileImport(IVTWorksheet sheet, int ClassID, int SemesterID, string ClassName)
        {
            Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9\-_.,;:\r\n\na)(]*$");
            bool IsMatch = true;
            List<RewardCommentFinalViewModel> lstRewardCommentFinal = new List<RewardCommentFinalViewModel>();
            RewardCommentFinalViewModel objRewardModel = null;
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID).ToList().OrderBy(p=>p.RewardMode).ToList();
            RewardFinal objRewardFinal = null;
            string RewardID = string.Empty;
            IDictionary<string, int> dicRewardFinal = new Dictionary<string, int>();
            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                objRewardFinal = lstRewardFinal[i];
                dicRewardFinal.Add(objRewardFinal.RewardMode, objRewardFinal.RewardFinalID);
            }
            //danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClass = PupilOfClassBusiness.GetPupilInClass(ClassID).ToList().OrderBy(c => c.OrderInClass).ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.Name + " " + c.PupilFullName)).ToList();
            List<string> lstPupilCode = new List<string>();
            List<string> lstStrArr = new List<string>();
            bool checkDuplicate = false;
            bool pass = true;
            int startRow = 7;
            while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
            {
                string pupilcode = (string)sheet.GetCellValue(startRow, 2);
                RewardID = string.Empty;
                objRewardModel = new RewardCommentFinalViewModel();
                lstStrArr = new List<string>();
                if (lstPupilCode.Count() == 0)
                {
                    // add vao list ma hoc sinh:
                    lstPupilCode.Add(pupilcode);
                }
                else
                {
                    if (lstPupilCode.Contains(pupilcode))
                    {
                        checkDuplicate = true;
                    }
                    else
                    {
                        checkDuplicate = false;
                        lstPupilCode.Add(pupilcode);
                    }
                }
                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    pass = false;
                    objRewardModel.MessageError = "-" + Res.Get("Reward_Validate_Duplicate_PupilCode") + "\r\n";
                }

                var poc = lstPupilOfClass.SingleOrDefault(u => u.PupilCode == pupilcode);
                
                if (poc == null)
                {
                    objRewardModel.MessageError = "- " + Res.Get("RewardComment_Label_Not_PupilCode") + "\r\n";
                    pass = false;
                }
                else if(!checkDuplicate)
                {
                    objRewardModel.PupilID = poc.PupilID;
                    objRewardModel.Status = poc.Status;
                    objRewardModel.BirthDay = poc.Birthday;
                }            

                objRewardModel.PupilCode = pupilcode;
                string pupilname = (string)sheet.GetCellValue(startRow, 3);
                if (poc != null && !poc.PupilFullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                {
                    objRewardModel.MessageError = "- " + Res.Get("MarkRecord_Label_NameError") + "\r\n";
                    pass = false;
                }

                objRewardModel.FullName = pupilname;

                if (poc != null && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    if (sheet.GetCellValue(startRow, 6) != null)
                    {
                        IsMatch = objAlphaPattern.IsMatch(Utils.Utils.StripVNSignAndSpace(sheet.GetCellValue(startRow, 6).ToString()));
                        if (sheet.GetCellValue(startRow, 6).ToString().Length > RewardCommentFinalConstant.MAX_LENGHT)
                        {
                            objRewardModel.MessageError = "- " + Res.Get("Reward_Validate_MaxLengh") + "\r\n";
                            pass = false;
                        }
                        else if (!IsMatch)
                        {
                            objRewardModel.MessageError = "- " + Res.Get("Reward_Validate_HTML") + "\r\n";
                            pass = false;
                        }
                        objRewardModel.OutstandingAchievement = sheet.GetCellValue(startRow, 6).ToString();
                    }
                    if (sheet.GetCellValue(startRow, 7) != null)
                    {
                        lstStrArr = sheet.GetCellValue(startRow, 7).ToString().Trim().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList().OrderBy(p=>p.Trim()).ToList();
                        objRewardModel.RewardMode = sheet.GetCellValue(startRow, 7).ToString();
                    }

                    string strRewardMode = string.Empty;
                    for (int j = 0; j < lstStrArr.Count; j++)
                    {
                        strRewardMode = lstStrArr[j].Trim();
                        if (lstRewardFinal.Where(p => p.RewardMode.Equals(strRewardMode)).Count() > 0)
                        {
                            RewardID = RewardID + (dicRewardFinal[strRewardMode] + ",");
                        }
                        else
                        {
                            objRewardModel.MessageError = "- " + Res.Get("Reward_Validate_RewardMode_NotExit") + "\r\n";
                            pass = false;
                        }

                    }
                }

                objRewardModel.RewardID = RewardID;
                
                objRewardModel.Pass = pass;
                lstRewardCommentFinal.Add(objRewardModel);
                pass = true;             

                startRow++;
            }

            return lstRewardCommentFinal;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private void SetViewData(int? ClassID)
        {
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[RewardCommentFinalConstant.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }
            ViewData[RewardCommentFinalConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[RewardCommentFinalConstant.LIST_CLASS] = listClass;
                if (listClass.Count() == 0)
                {
                    ViewData[RewardCommentFinalConstant.CHECK_CLASS_NULL] = true;
                }
                else
                {
                    var listCP = listClass.ToList();

                    if (listCP != null && listCP.Count() > 0)
                    {
                        //Tính ra lớp cần được chọn đầu tiên
                        cp = listCP.First();
                        if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                        {
                            cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                        }
                        else
                        {
                            // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                            {
                                // Ưu tiên trước đối với giáo viên bộ môn
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                                {
                                    cp = listSubjectTeacher.First();
                                }
                                else
                                {
                                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                    List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                    if (listHead != null && listHead.Count > 0)
                                    {
                                        cp = listHead.First();
                                    }
                                }
                            }
                        }
                    }
                    ViewData[RewardCommentFinalConstant.CLASS_PROFILE] = cp;
                }
            }
        }

        private void SetLog(int classID,string className, int semesterID, string action)
        {

            string desCription = "";
            if (action == SMAS.Business.Common.GlobalConstants.ACTION_ADD || action == SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)
            {
                desCription = String.Format("Lưu nhận xét và khen thưởng cuối kỳ : {0}, lớp : {1}", semesterID,className);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_DELETE)
            {
                desCription = String.Format("Xóa nhận xét và khen thưởng cuối kỳ : {0}, lớp : {1}", semesterID, className);
            }
            else if (action == SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)
            {
                desCription = String.Format("Import nhận xét và khen thưởng cuối kỳ : {0}, lớp : {1}", semesterID, className);
            }
            string parameter = String.Format("Class_id:{0}, Semester:{1}",classID,semesterID);
            SetViewDataActionAudit(String.Empty, String.Empty, classID.ToString(), desCription, parameter, "Nhận xét khen thưởng cuối kỳ", action, desCription);
        }

        private void SetVisbileButton(int semester, int classID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVCN))
            {
                ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                if (dateTimeNow < objAca.FirstSemesterStartDate.Value || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate) 
                    || dateTimeNow > objAca.SecondSemesterEndDate)//ngoài thời gian năm học
                {
                    ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = false;
                }
                else
                {
                    ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = true;
                }
            }
            else if(isGVCN)//chỉ được thao tác trong học kỳ hiện tại
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = true;
                    }
                }
                else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[RewardCommentFinalConstant.DISABLED_BUTTON] = true;
                    }
                }
            }

                
        }
    }
}
