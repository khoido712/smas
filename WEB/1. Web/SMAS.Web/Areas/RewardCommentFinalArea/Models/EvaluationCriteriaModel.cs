﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RewardCommentFinalArea.Models
{
    public class EvaluationCriteriaModel
    {
        public int EvaluationCriteriaID { get; set; }
        public string EvaluationCriteriaName { get; set; }
        public int EvaluationID { get; set; }
        public string EndingComment { get; set; }//Nhan xet cuoi ky
        public string EndingEvaluation { get; set; }//Danh gia cuoi ky
    }
}