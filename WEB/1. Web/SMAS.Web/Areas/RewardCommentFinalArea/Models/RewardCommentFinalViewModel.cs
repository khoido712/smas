﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RewardCommentFinalArea.Models
{
    public class RewardCommentFinalViewModel
    {
        public int RewardCommentFinalID { get; set; }
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDay { get; set; }
        public int Status { get; set; }
        public string OutstandingAchievement { get; set; }
        public string RewardID { get; set; }
        public string RewardMode { get; set; }
        public string Capacity { get; set; }
        public string Quality { get; set; }
        public string MessageError { get; set; }
        public bool Pass { get; set; }
        public string Comment { get; set; }//Đánh giá
        public bool IsCommentSubject { get; set; }
        public bool IsCommentEvaluation { get; set; }
        public List<SMAS.Business.Business.RewardCommentFinalBusiness.SubjectViewModel> lstSubject { get; set; }
    }
}