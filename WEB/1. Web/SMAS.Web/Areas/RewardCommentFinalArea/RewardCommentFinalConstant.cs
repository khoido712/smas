﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.RewardCommentFinalArea
{
    public class RewardCommentFinalConstant
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_PROFILE = "clasProfile";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_REWARDCOMMENT_FINAL = "ListRewardCommentFinal";
        public const string REWARDCOMMENT_FINAL = "RewardCommentFinal";
        public const string SEMESTER_ID = "SemesterID";
        public const string DISABLED_BUTTON = "DisabledButton";
        public const string LIST_REWARD_FINAL = "ListRewardFinal";
        public const string LIST_EVALUATION_CRITERIA_MODEL = "EvaluationCriteriaModel";
        public const int PARTION_REWARD_COMMMENT_FINAL = 20;
        public const string CHECK_CLASS_NULL = "CheckClassNull";
        public const string CHECK_ENABLED = "checkEnabled";
        public const string FORMAT_DATE = "dd/MM/yyyy";
        public const int MAX_LENGHT = 500;
        public const string COMPLETE = "Hoàn thành";
        public const string NOT_COMPLETE = "Chưa hoàn thành";

    }
}