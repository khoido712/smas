﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RewardCommentFinalArea
{
    public class RewardCommentFinalAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RewardCommentFinalArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RewardCommentFinalArea_default",
                "RewardCommentFinalArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
