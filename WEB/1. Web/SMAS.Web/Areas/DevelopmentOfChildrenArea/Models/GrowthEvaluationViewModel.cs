﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class GrowthEvaluationViewModel
    {
        public int EvaluationDevelopmentID { get; set; }
        public int SchoolID { get; set; }
        public int AcademicYearID { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int PupilID { get; set; }
        public int? EvaluationType { get; set; }
        public int DeclareEvaluationGroupID { get; set; }
        public int DeclareEvaluationIndexID { get; set; }
        public int? LogID { get; set; }
        public int LastDigitSchoolID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string PupilName { get; set; }
        public int Status { get; set; }
        public string EvaluationTypeStr { get; set; }
        public string Name { get; set; }
        public List<EvaluationDevelopmentModel> lstEDVM { get; set; }
        public List<StringViewModel> ListEvaluationType { get; set; }

        public string Error { get; set; }
    }

    public class EvaluationDevelopmentModel
    {
        public int DeclareEvaluationIndexID { get; set; }
        public string EvaluationIndexName { get; set; }
        public string EvaluationIndexVal { get; set; }
        public int? LogID { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class EvaluationDevIndexViewModel
    {
        // mã linh vưc
        public int EvaluationDevGroID { get; set; }
        // mã chỉ sổ
        public int EvaluationDevIndexID { get; set; }
        public string EvaluationDevIndexCode { get; set; }
        public bool isNull { get; set; }
    }


    public class StringViewModel
    {       
        public int PupilID { get; set; }     
        public string EvaluationType { get; set; }
    }

    public class CellIndexViewModel
    {
        public int EvaluationGroupID { get; set; }
        public int StartColumnExcel { get; set; }
        public int EndColumnExcel { get; set; }
    }
}