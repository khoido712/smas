/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class DevelopmentOfChildrenViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 DevelopmentOfChildrenID { get; set; }
        [ScaffoldColumn(false)]						
		public System.Int32 Semester { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 Year { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 PupilID { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 EvaluationDevelopmentID { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 ClassID { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 SchoolID { get; set; }
        [ScaffoldColumn(false)]
		public System.Int32 AcademicYearID { get; set; }
        [ResourceDisplayName("PupilPraise_Label_FullName")]
        public string PupilName { get; set; }
        [ResourceDisplayName("DevelopmentOfChildren_Label_DOB")]
        public string DisplayDOB { get; set; }
        public DateTime DOB { get; set; }
        public string[] Selected { get; set; }
        public List<bool> listChecked { get; set; }
        public int Status { get; set; }
    }
}


