﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class GrowthEvaluationGradeVM
    {
        public string GradeResolution { get; set; }
        public int TeacherGradeID { get; set; }
        public int Order { get; set; }
    }
}