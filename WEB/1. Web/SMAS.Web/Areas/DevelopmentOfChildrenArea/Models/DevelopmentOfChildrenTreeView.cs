﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class DevelopmentOfChildrenTreeView //: Controller
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int Permission { get; set; }
        public List<DevelopmentOfChildrenTreeView> ListChildren { get; set; }
        public int OrderInLevel { get; set; }
    }
}
