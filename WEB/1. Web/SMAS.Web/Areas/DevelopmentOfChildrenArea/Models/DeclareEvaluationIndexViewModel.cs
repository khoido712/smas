﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class DeclareEvaluationIndexViewModel
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int EvaluationGroupID { get; set; }
    }
}