/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Models
{
    public class SearchViewModel
    {
        public int? ClassID { get; set; }

        public int Semester { get; set; }

        public int EvaluationDevelopmentGroupID { get; set; }
    }
}
