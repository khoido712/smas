﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DevelopmentOfChildrenArea
{
    public class DevelopmentOfChildrenAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DevelopmentOfChildrenArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DevelopmentOfChildrenArea_default",
                "DevelopmentOfChildrenArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
