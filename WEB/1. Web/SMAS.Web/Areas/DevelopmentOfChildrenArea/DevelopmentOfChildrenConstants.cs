/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DevelopmentOfChildrenArea
{
    public class DevelopmentOfChildrenConstants
    {
        public const string CLASS_NULL = "classnull";
        public const string LIST_DEVELOPMENTOFCHILDREN = "listDevelopmentOfChildren";
        public const string LT_SPV_DEPT_TREE = "listTree";
        public const string LIST_CLASS = "listClass";
        public const string LIST_EVALUTION_OF_CLASS = "listEva";
        public const string LIST_EVALUTION = "listEva";
        public const string LST_SEMESTER = "litSem";
        public const string CLASS_ID = "classID";
        public const string semester = "semester";
        public const string classid = "classid";
        public const string evagroupid = "evagrpupid";
        public const string DECLARE_EVALUATION_GROUP_NULL = "declareEvaluationGroup_null";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string LAST_UPDATE_PERIOD_MARK = "LAST_UPDATE_PERIOD_MARK";
        public const string LIST_EVALUATION_INDEX = "lstEvaluationIndex";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_DECLARE_EVALUATION_GRO = "ListDeclareEvaluationGro";
        public const string ClassID = "ClassID";
        public const string GVCN_TEACHER = "GVCNTeacher";
        public const string PHISICAL_PATH = "PhisicalPath";
        public const string LIST_ERROR_IMPORT = "ListErrorImport";
        public const string CELL_CHECK_TYPE_FILE_IMPORT = "CC3";
    }
}