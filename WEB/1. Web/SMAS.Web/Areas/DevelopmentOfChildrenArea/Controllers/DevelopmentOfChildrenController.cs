﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DevelopmentOfChildrenArea.Models;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.RatedCommentPupilArea;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.RatedCommentPupilArea.Models;
using System.Text;
using System.IO;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Web.Areas.DevelopmentOfChildrenArea.Controllers
{
    public class DevelopmentOfChildrenController : BaseController
    {
        #region Create Constructor
        private readonly IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        private GlobalInfo globalInfo = GlobalInfo.getInstance();
        private int ID_OF_EDUCATIONLEVEL = -2;
        public DevelopmentOfChildrenController(IClassProfileBusiness ClassProfileBusiness
            , IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness
            , IDevelopmentOfChildrenBusiness developmentofchildrenBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness
            , IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness
            , IGrowthEvaluationBusiness GrowthEvaluationBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.DevelopmentOfChildrenBusiness = developmentofchildrenBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.EvaluationDevelopmentBusiness = EvaluationDevelopmentBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
            this.GrowthEvaluationBusiness = GrowthEvaluationBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }
        #endregion

        public ActionResult Index(int? ClassID)
        {
            this.SetViewData(ClassID);
            return View();
        }

        # region SetViewData
        private void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();

            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[DevelopmentOfChildrenConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();

                ViewData[DevelopmentOfChildrenConstants.LIST_CLASS] = listClass;
                var listCP = listClass.ToList();

                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                }

                ViewData["ClassProfile"] = cp;
            }

            //lay list linh vuc
            List<DeclareEvaluationGroup> lstDEG = new List<DeclareEvaluationGroup>();
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue && cp.ClassProfileID > 0)
                {
                    int AcademicYearID = _globalInfo.AcademicYearID.Value;
                    int SchoolID = _globalInfo.SchoolID.Value;
                    int AppliedLevel = _globalInfo.AppliedLevel.Value;
                    int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(cp.ClassProfileID);

                    lstDEG = DeclareEvaluationGroupBusiness.GetListEvaluationByEvaluationTeacher(
                                                            AcademicYearID,
                                                            SchoolID,
                                                            AppliedLevel,
                                                            EducationLevelID)
                                                            .OrderBy(x => x.EvaluationGroupID).ToList();
                }
            }
            else
            {
                ViewData[DevelopmentOfChildrenConstants.CLASS_NULL] = true;
            }

            //ViewData[RatedCommentPupilConstants.SubjectID] = lstDEG.Select(x => x.DeclareEvaluationGroupID).FirstOrDefault();
        }
        #endregion

        #region Get Evaluation Panel
        // lấy danh sách lĩnh vực đánh giá
        [HttpPost]
        public PartialViewResult GetEvaluationPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            ViewData[DevelopmentOfChildrenConstants.ClassID] = classid.Value;

            //GVCN, PT, admin = true
            bool isGVCN_PT = UtilsBusiness.HasChargeTeacherPemission(_globalInfo.UserAccountID, classid.Value);
            Session[DevelopmentOfChildrenConstants.GVCN_TEACHER] = isGVCN_PT;

            int SchoolID = _globalInfo.SchoolID.Value;
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO = new List<DeclareEvaluationGroupBO>();
            int EducationLevelID = 0;
            if (classid.HasValue)
            {
                EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classid.Value);

                lstDeclareEvaluationGroBO = this.ListDeclareEvaluationGroBO(EducationLevelID);

                Session["ListSubject"] = lstDeclareEvaluationGroBO;
                ViewData[DevelopmentOfChildrenConstants.LIST_DECLARE_EVALUATION_GRO] = lstDeclareEvaluationGroBO;
                int evaluationGroupID = lstDeclareEvaluationGroBO.Count() > 0 ? lstDeclareEvaluationGroBO.FirstOrDefault().EvaluationGroupID : 0;
                ViewData["ValueEvaluationGroupID"] = evaluationGroupID;
            }

            ViewData["IsConfig"] = IsLockEvaluation(EducationLevelID);

            return PartialView("_ViewSubject");
        }

        public PartialViewResult IsConfigEvaluation(int ClassID, int? evaluationGroupID)
        {
            if (!evaluationGroupID.HasValue)
            {
                ViewData["IsConfig"] = true;
                ViewData["ValueEvaluationGroupID"] = 0;
            }

            int educationLevelId = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            ViewData["IsConfig"] = IsLockEvaluation(educationLevelId);
            ViewData["ValueEvaluationGroupID"] = evaluationGroupID;
            ViewData["ClassID"] = ClassID;
            return PartialView("_LinkConfigEvaluation");
        }

        private bool IsLockEvaluation(int EducationLevelID)
        {
            DeclareEvaluationGroup objDEG = DeclareEvaluationGroupBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        && x.SchoolID == _globalInfo.SchoolID
                                                                                        && x.EducationLevelID == EducationLevelID).FirstOrDefault();
            if (objDEG != null)
            {
                if (!objDEG.IsLocked.HasValue)
                    return true;
                if (objDEG.IsLocked.Value)
                    return true;
                else
                    return false;
            }

            return true;
        }

        // danh sach linh vuc
        private List<DeclareEvaluationGroupBO> ListDeclareEvaluationGroBO(int EducationLevelID)
        {
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO = new List<DeclareEvaluationGroupBO>();
            List<EvaluationDevelopmentGroup> lstEvaluationDevelopmentGro = new List<EvaluationDevelopmentGroup>();
            List<DeclareEvaluationGroup> lstDeclareEvaluationGroup = new List<DeclareEvaluationGroup>();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevel", EducationLevelID);
            dic.Add("ProvinceID", _globalInfo.ProvinceID.Value);

            lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
           
            lstEvaluationDevelopmentGro = EvaluationDevelopmentGroupBusiness.All
                                            .Where(x => x.IsActive == true && x.AppliedLevel == _globalInfo.AppliedLevel.Value)
                                            .Where(x=>x.EducationLevelID == EducationLevelID)
                                            .OrderBy(x => x.EvaluationDevelopmentGroupName)
                                            .ToList();
            var temptEDG = lstEvaluationDevelopmentGro.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
            if (temptEDG.Count() > 0)
            {
                lstEvaluationDevelopmentGro = temptEDG;
            }

            lstDeclareEvaluationGroBO = (from lstDEG in lstDeclareEvaluationGroup
                                         join lstEDG in lstEvaluationDevelopmentGro
                                        on lstDEG.EvaluationGroupID equals lstEDG.EvaluationDevelopmentGroupID
                                         select new DeclareEvaluationGroupBO
                                         {
                                             DeclareEvaluationGroupID = lstDEG.DeclareEvaluationGroupID,
                                             EvaluationDevelopmentGroupName = lstEDG.EvaluationDevelopmentGroupName,
                                             CurrentSchoolID = lstDEG.SchoolID,
                                             CurrentAcademicYearID = lstDEG.AcademicYearID,
                                             EvaluationGroupID = lstDEG.EvaluationGroupID,
                                             OrderID = lstEDG.OrderID,
                                             IsLocked = lstDEG.IsLocked,
                                         }).OrderBy(o => o.OrderID)
                                          .ThenBy(o => o.EvaluationDevelopmentGroupName)
                                          .ToList();
            return lstDeclareEvaluationGroBO;
        }
        #endregion

        #region AjaxLoadGrid
        public PartialViewResult AjaxLoadGrid(int ClassID, int EvaluationDevGroID)
        {
            // danh sach cac danh gia
            List<GrowthEvaluationBO> lstGrowthEvaluation = new List<GrowthEvaluationBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID", ClassID},
                {"EvaluationDevGroID", EvaluationDevGroID},
            };
            lstGrowthEvaluation = this.ListGrowthEvaluationBOAjaxLoadGrid(dic);
            int educationLevelId = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            List<DeclareEvaluationGroup> lstDeclareEvaluationGroup = DeclareEvaluationGroupBusiness.All
                                                                                .Where(x => x.SchoolID == _globalInfo.SchoolID.Value
                                                                                    && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                    && x.EducationLevelID == educationLevelId
                                                                                    && x.EvaluationGroupID == EvaluationDevGroID).ToList();

            var lstGrowthEval = (from lstGE in lstGrowthEvaluation
                                 join lstDEG in lstDeclareEvaluationGroup
                                 on lstGE.DeclareEvaluationGroupID equals lstDEG.EvaluationGroupID
                                 select lstGE).ToList();

            List<GrowthEvaluationViewModel> lstResult = this.GetListViewData(ClassID, EvaluationDevGroID, lstGrowthEval);

            #region nguoi cap nhat sau cung
            List<int> lstEmployeeID = new List<int>();
            GrowthEvaluationBO lastUpdateBO = new GrowthEvaluationBO();
            Employee lastUser = null;
            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            List<int> lstLastUpdatePupilID = new List<int>();
            string lastUpdateTime = string.Empty;
            List<Employee> lstEmployeeChange = new List<Employee>();
            lstEmployeeID = lstGrowthEvaluation.Where(p => p.LogID.Value > 0).Select(p => p.LogID.Value).Distinct().ToList();
            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
            }
            lastUpdateBO = lstGrowthEvaluation.Where(x => x.EvaluationType >= 0).OrderByDescending(x => x.ModifiedDate).FirstOrDefault();

            if (lastUpdateBO != null)
            {
                string lastUserName = String.Empty;

                if (lastUpdateBO.LogID == 0)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateBO.LogID).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }

                if (lastUpdateBO.ModifiedDate.HasValue)
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateBO.ModifiedDate.Value.Hour.ToString(),
                             lastUpdateBO.ModifiedDate.Value.Minute.ToString("D2"),
                             lastUpdateBO.ModifiedDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                }
                else
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateBO.CreateDate.Hour.ToString(),
                             lastUpdateBO.CreateDate.Minute.ToString("D2"),
                             lastUpdateBO.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                }

                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);

                lastUpdateTime = lastUpdateBO.ModifiedDate != null ? lastUpdateBO.ModifiedDate.Value.ToString("dd/MM/yyyy hh:mm") : lastUpdateBO.CreateDate.ToString("dd/MM/yyyy hh:mm");

                lstLastUpdatePupilID = lstResult.Where(p => (p.ModifiedDate.HasValue ? p.ModifiedDate.Value.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime : p.CreateDate.ToString("dd/MM/yyyy hh:mm") == lastUpdateTime)
                                        && (p.EvaluationType >= 0))
                                        .Select(p => p.PupilID).Distinct().ToList();
            }
            #endregion

            ViewData[DevelopmentOfChildrenConstants.LAST_UPDATE_PERIOD_MARK] = lstLastUpdatePupilID;
            ViewData[DevelopmentOfChildrenConstants.LAST_UPDATE_STRING] = strLastUpdate;
            ViewData[DevelopmentOfChildrenConstants.LIST_DEVELOPMENTOFCHILDREN] = lstResult;
            return PartialView("_GridGrowthEvaluation");
        }

        private List<GrowthEvaluationBO> ListGrowthEvaluationBOAjaxLoadGrid(IDictionary<string, object> dic)
        {
            List<GrowthEvaluationBO> lstGrowthEvaluation = new List<GrowthEvaluationBO>();
            lstGrowthEvaluation = (from lst in GrowthEvaluationBusiness.Search(dic)
                                   select new GrowthEvaluationBO
                                   {
                                       GrowthEvaluationID = lst.GrowthEvaluationID,
                                       PupilID = lst.PupilID,
                                       DeclareEvaluationGroupID = lst.DeclareEvaluationGroupID,
                                       DeclareEvaluationIndexID = lst.DeclareEvaluationIndexID,
                                       EvaluationType = lst.EvaluationType,
                                       LogID = lst.LogID,
                                       CreateDate = lst.CreateDate,
                                       ModifiedDate = lst.ModifiedDate,
                                   }).ToList();
            return lstGrowthEvaluation;
        }
        #endregion

        #region noi dung chi tiet cua linh vuc
        public PartialViewResult AjaxLoadDialog(int ClassID, int EvaluationDevGroID)
        {
            // linh vuc
            int SchoolID = _globalInfo.SchoolID.Value;
            List<DeclareEvaluationGroupBO> lsClassSubject = new List<DeclareEvaluationGroupBO>();
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO = new List<DeclareEvaluationGroupBO>();
            lstDeclareEvaluationGroBO = this.ListDeclareEvaluationGroBO(EducationLevelID);
            List<int> lstEvaluationDevGroupID = lstDeclareEvaluationGroBO.Select(x => x.EvaluationGroupID).Distinct().ToList();
            ViewData[DevelopmentOfChildrenConstants.LIST_DECLARE_EVALUATION_GRO] = lstDeclareEvaluationGroBO;

            // content         
            List<DeclareEvaluationIndexBO> lstDeclareEvalIndex = new List<DeclareEvaluationIndexBO>();

            IDictionary<string, object> dicDEI = new Dictionary<string, object>();
            dicDEI.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicDEI.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicDEI.Add("EducationLevel", EducationLevelID);
            dicDEI.Add("DeclareEvaluationGroupID", EvaluationDevGroID);
            dicDEI.Add("ProvinceID", _globalInfo.ProvinceID);
            dicDEI.Add("ListEvaluationGroupID", lstEvaluationDevGroupID);
            lstDeclareEvalIndex = DeclareEvaluationIndexBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicDEI).ToList();

            var tempt = lstDeclareEvalIndex.Where(x => x.ClassID.HasValue && x.ClassID == ClassID).ToList();
            if (tempt.Count() > 0)
            {
                lstDeclareEvalIndex = tempt;
            }
            else
            {
                tempt = lstDeclareEvalIndex.Where(x => !x.ClassID.HasValue && x.ClassID == null).ToList();
                lstDeclareEvalIndex = tempt;
            }

            var lstDEI = (from lst in lstDeclareEvalIndex
                         group lst by lst.EvaluationDevCode into a
                         select new DeclareEvaluationIndexBO
                         {
                             EvaluationDevCode = a.Key,
                             DescriptionIndex = a.FirstOrDefault().DescriptionIndex
                         }).ToList();

            ViewData[DevelopmentOfChildrenConstants.LIST_EVALUATION_INDEX] = lstDEI;   

            return PartialView("_GridViewDetail");
        }
        #endregion

        #region GetListViewData
        private List<GrowthEvaluationViewModel> GetListViewData(int ClassID, int DeclareEvaluationGroupID, List<GrowthEvaluationBO> lstGrowthEvaluation)
        {
            //lay danh sach hoc sinh cua lop
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .OrderBy(c => c.OrderInClass)
                                                                .ToList();
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            //lấy danh sách các chỉ số đánh giá 
            List<DeclareEvaluationIndexBO> lstDeclareEvalIndex = new List<DeclareEvaluationIndexBO>();
            DeclareEvaluationIndexBO objDeclareEvaluationIndex = null;
            // lấy danh sach sự đánh giá phát triển
            List<GrowthEvaluationBO> listGrowthEvaluationBO = new List<GrowthEvaluationBO>();

            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevel", EducationLevelID);
            dic.Add("DeclareEvaluationGroupID", DeclareEvaluationGroupID);
            dic.Add("ProvinceID", _globalInfo.ProvinceID);

            lstDeclareEvalIndex = DeclareEvaluationIndexBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            var lstDeclareEvaluationByClass = lstDeclareEvalIndex.Where(x => x.ClassID == ClassID).ToList();
            if (lstDeclareEvaluationByClass.Count() > 0)
            {
                lstDeclareEvalIndex = lstDeclareEvaluationByClass;
            }
            else
            {
                lstDeclareEvalIndex = lstDeclareEvalIndex.Where(x => !x.ClassID.HasValue && x.ClassID == null).ToList();
            }

            ViewBag.isNotEvaluationIndex = false;
            if (lstDeclareEvalIndex.Count > 0)
                ViewBag.isNotEvaluationIndex = true;

            // nhom lop 5-6 tuoi
            ViewBag.isEducationLevel = false;
            if (EducationLevelID == 18)
                ViewBag.isEducationLevel = true;

            ViewData[DevelopmentOfChildrenConstants.LIST_EVALUATION_INDEX] = lstDeclareEvalIndex;
            List<GrowthEvaluationViewModel> lstResultVM = new List<GrowthEvaluationViewModel>();
            GrowthEvaluationViewModel objGRResult = null;
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            GrowthEvaluationBO item = null;
            List<GrowthEvaluationBO> itemData = new List<GrowthEvaluationBO>();
            GrowthEvaluationBO objGEBO = null;
            PupilOfClassBO objPOC = null;
            int PupilID = 0;
            List<GrowthEvaluationBO> lstGrowthEvaluationBO = new List<GrowthEvaluationBO>();
            List<StringViewModel> listEva = new List<StringViewModel>();
            List<EvaluationDevelopmentModel> lstEDVM = new List<EvaluationDevelopmentModel>();
            EvaluationDevelopmentModel objEDVM = null;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                objGRResult = new GrowthEvaluationViewModel();
                PupilID = objPOC.PupilID;
                objGRResult.PupilID = PupilID;
                objGRResult.ClassID = objPOC.ClassID;
                objGRResult.PupilName = objPOC.PupilFullName;
                objGRResult.Status = objPOC.Status;
                lstEDVM = new List<EvaluationDevelopmentModel>();

                objGEBO = lstGrowthEvaluation.Where(x => x.PupilID == PupilID).FirstOrDefault();

                for (int j = 0; j < lstDeclareEvalIndex.Count; j++)
                {
                    objDeclareEvaluationIndex = lstDeclareEvalIndex[j];
                    objEDVM = new EvaluationDevelopmentModel();
                    objEDVM.DeclareEvaluationIndexID = objDeclareEvaluationIndex.EvaluationDevIndexID;
                    objEDVM.EvaluationIndexName = objDeclareEvaluationIndex.Name;
                    item = lstGrowthEvaluation.Where(p => p.PupilID == PupilID && p.DeclareEvaluationIndexID == objDeclareEvaluationIndex.EvaluationDevIndexID).FirstOrDefault();
                    if (item != null)
                    {
                        objEDVM.EvaluationIndexVal = item.EvaluationType == 1 ? "+" : item.EvaluationType == 2 ? "-" : item.EvaluationType == 3 ? "±" : "";
                        objEDVM.LogID = item.LogID;
                        objEDVM.ModifiedDate = item.ModifiedDate;
                    }
                    lstEDVM.Add(objEDVM);
                }

                objGRResult.lstEDVM = lstEDVM;
                lstResultVM.Add(objGRResult);
            }

            return lstResultVM.ToList();
        }
        #endregion

        #region SaveComentsPupil
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveComentsPupil(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            int DeclareEvaluationGroupID = !string.IsNullOrEmpty(frm["EvaluationDevGroID"]) ? int.Parse(frm["EvaluationDevGroID"]) : 0;
            //lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(ClassID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();
            PupilOfClassBO objPOC = null;

            List<GrowthEvaluationBO> lstGrowthEvaluationBO = new List<GrowthEvaluationBO>();
            GrowthEvaluationBO objGrowEvaluationBO = null;

            string UserNameComment = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeName : Res.Get("Message_Label_GroupUserSchoolAdmin");
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            int PupilID = 0;

            //danh sach chi so
            DeclareEvaluationIndexBO objDEI = null;         
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            IDictionary<string, object> dicDEI = new Dictionary<string, object>();
            dicDEI.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicDEI.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicDEI.Add("EducationLevel", EducationLevelID);
            dicDEI.Add("DeclareEvaluationGroupID", DeclareEvaluationGroupID);
            dicDEI.Add("ProvinceID", _globalInfo.ProvinceID);
            List<DeclareEvaluationIndexBO> lstDEI = DeclareEvaluationIndexBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicDEI).ToList();

            var lstDeclareEvaluationByClass = lstDEI.Where(x => x.ClassID == ClassID).ToList();
            if (lstDeclareEvaluationByClass.Count() > 0)
            {
                lstDEI = lstDeclareEvaluationByClass;
            }
            else
            {
                lstDEI = lstDEI.Where(x => !x.ClassID.HasValue && x.ClassID == null).ToList();
            }

            List<int> lstPupilID = new List<int>();
            int index = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + PupilID]) || objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    index = index + lstDEI.Count;
                    continue;
                }
                lstPupilID.Add(PupilID);

                for (int j = 0; j < lstDEI.Count; j++)
                {
                    objDEI = lstDEI[j];
                    objGrowEvaluationBO = new GrowthEvaluationBO();
                    objGrowEvaluationBO.SchoolID = _globalInfo.SchoolID.Value;
                    objGrowEvaluationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objGrowEvaluationBO.ClassID = ClassID;
                    objGrowEvaluationBO.PupilID = PupilID;
                    objGrowEvaluationBO.DeclareEvaluationGroupID = objDEI.EvaluationDevGroupID;
                    objGrowEvaluationBO.DeclareEvaluationIndexID = objDEI.EvaluationDevIndexID;
                    objGrowEvaluationBO.LogID = LogChangeID;
                    objGrowEvaluationBO.LastDigitSchoolID = _globalInfo.SchoolID.Value % 100;

                    string nameFrm = "txtEvaluationIndex_" + PupilID + "_" + objDEI.DeclareEvaluationIndexID;
                    string frmValue = frm[nameFrm];
                    objGrowEvaluationBO.EvaluationType = frmValue == "+" ? 1 : frmValue == "-" ? 2 : frmValue == "±" ? 3 : 0;

                    lstGrowthEvaluationBO.Add(objGrowEvaluationBO);
                }
            }

            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"ClassID",ClassID},
                    {"DeclareEvaluationGroupID", DeclareEvaluationGroupID},
                    {"lstPupilID", lstPupilID}
                };
            GrowthEvaluationBusiness.InsertOrUpdateGrowthEvaluation(lstGrowthEvaluationBO, dicInsert);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteCommentPupil
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCommentPupil(int ClassID, int? DeclareEvaluationGroupID, string arrPupilID)
        {
            // int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"DeclareEvaluationGroupID",DeclareEvaluationGroupID},
                {"lstPupilID",lstPupilID},
                //{"LogChangeID",LogChangeID}
            };
            GrowthEvaluationBusiness.DeleteCommentPupil(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        #region xuat excel
        public FileResult ExportGrowthEvaluationOfChildren(int ClassID, int DeclareEvaluationGroupID, int EducationLevelID, int TypeExport)
        {
            string templatePath = string.Empty;
            string fileName = string.Empty;

            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.DANHGIASUPHATTRIENTRE_MAM1_THECHAT + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);

            string suppervisingDeptName = "";
            if (_globalInfo.EmployeeID > 0)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                suppervisingDeptName = objSP.SupervisingDept.SupervisingDeptName.ToUpper();
            }
            else
            {
                suppervisingDeptName = _globalInfo.SuperVisingDeptName.ToUpper();
            }

            firstSheet.SetCellValue("A2", suppervisingDeptName);
            firstSheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            List<VTDataValidation> lstDataValidation = new List<VTDataValidation>();

            //danh sach chi so
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SchoolID = _globalInfo.SchoolID.Value;
            int AppliedLevel = _globalInfo.AppliedLevel.Value;

            #region TypeExport 1,2
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            string className = SMAS.Web.Utils.Utils.StripVNSignAndSpace(objCP.DisplayName);

            //lay danh sach hoc sinh cua 1 lop
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ToList();
            //lay danh sach danh gia cua hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",ClassID}                  
            };

            // Danh sach linh vuc
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroupBO = new List<DeclareEvaluationGroupBO>();
            lstDeclareEvaluationGroupBO = this.ListDeclareEvaluationGroBO(EducationLevelID);
            List<int> lstEvaluationDevGroupID = lstDeclareEvaluationGroupBO.Select(x => x.EvaluationGroupID).ToList();
            //Danh sach chi so 
            IDictionary<string, object> dicDEI = new Dictionary<string, object>();
            dicDEI.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicDEI.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicDEI.Add("EducationLevel", EducationLevelID);
            dicDEI.Add("DeclareEvaluationGroupID", 0);
            dicDEI.Add("ProvinceID", _globalInfo.ProvinceID);
            dicDEI.Add("ListEvaluationGroupID", lstEvaluationDevGroupID);
            List<DeclareEvaluationIndexBO> lstDeclareEvaluationIndex = DeclareEvaluationIndexBusiness.SearchBySchool(SchoolID, dicDEI).ToList();

            // danh sach danh gia
            List<GrowthEvaluationBO> lstGrowthEvaluationBO = new List<GrowthEvaluationBO>();
            lstGrowthEvaluationBO = (from lst in GrowthEvaluationBusiness.Search(dic)
                                     select new GrowthEvaluationBO
                                     {
                                         PupilID = lst.PupilID,
                                         DeclareEvaluationGroupID = lst.DeclareEvaluationGroupID,
                                         DeclareEvaluationIndexID = lst.DeclareEvaluationIndexID,
                                         EvaluationType = lst.EvaluationType,
                                     }).ToList();

            List<DeclareEvaluationIndexBO> lstDEI_Tempt = new List<DeclareEvaluationIndexBO>();

            for (int i = 0; i < lstDeclareEvaluationGroupBO.Count(); i++)
            {
                var tempt = lstDeclareEvaluationIndex.Where(x => x.EvaluationDevGroupID == lstDeclareEvaluationGroupBO[i].EvaluationGroupID).ToList();

                var lstDeclareEvaluationByClass = tempt.Where(x => x.ClassID == ClassID).ToList();
                if (lstDeclareEvaluationByClass.Count() > 0)
                {
                    lstDEI_Tempt.AddRange(lstDeclareEvaluationByClass);
                }
                else
                {
                    tempt = tempt.Where(x => !x.ClassID.HasValue).ToList();
                    lstDEI_Tempt.AddRange(tempt);
                }
            }

            lstDeclareEvaluationIndex = lstDEI_Tempt;
            AcademicYear objAY = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            this.SetDataToFile1(firstSheet, lstPOC,
                   lstGrowthEvaluationBO, lstDeclareEvaluationIndex,
                   objAY.DisplayTitle, lstDeclareEvaluationGroupBO,
                   objCP.DisplayName, 1, lstDataValidation, EducationLevelID);

            this.FillDataEvaluation2(secondSheet, ClassID, lstDeclareEvaluationGroupBO, lstDeclareEvaluationIndex);
            fileName = string.Format("Danhgiasuphattrien_{0}_Tatcalinhvuc.xls", className);

            firstSheet.SetCellValue(1000, 1, TypeExport);
            firstSheet.SetRowHeight(1000, 0);

            firstSheet.ProtectSheet();
            secondSheet.ProtectSheet();
            #endregion

            Stream excel = oBook.ToStreamValidationData(lstDataValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        #endregion

        #region FillDataEvaluation
        private void FillDataEvaluation2(
            IVTWorksheet secondSheet, int ClassID,
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO,
             List<DeclareEvaluationIndexBO> lstDeclareEvaluationIndexBO)
        {
            DeclareEvaluationGroupBO objDEG_BO = null;
            List<DeclareEvaluationIndexBO> lstNewIndexBO = new List<DeclareEvaluationIndexBO>();
            DeclareEvaluationIndexBO objNewIndex = null;

            int startRow = 3;
            int countIndex = 1;
            int startIndex = 4;
            for (int i = 0; i < lstDeclareEvaluationGroBO.Count; i++)
            {
                objDEG_BO = lstDeclareEvaluationGroBO[i];
                lstNewIndexBO = lstDeclareEvaluationIndexBO
                                          .Where(x => x.DeclareEvaluationGroupID == objDEG_BO.DeclareEvaluationGroupID)
                                          .OrderBy(x => x.OrderID).ToList();

                secondSheet.GetRange(startRow, 1, startRow, 3).Merge();
                secondSheet.SetCellValue(startRow, 1, objDEG_BO.EvaluationDevelopmentGroupName.ToUpper());
                secondSheet.GetRange(startRow, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                for (int j = 0; j < lstNewIndexBO.Count; j++)
                {
                    objNewIndex = lstNewIndexBO[j];

                    secondSheet.SetCellValue(startIndex, 1, countIndex);
                    secondSheet.GetRange(startIndex, 1, startIndex, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    secondSheet.SetCellValue(startIndex, 2, objNewIndex.EvaluationDevCode);
                    secondSheet.SetCellValue(startIndex, 3, objNewIndex.DescriptionIndex);

                    countIndex++;
                    startIndex++;
                }
                secondSheet.GetRange(startRow, 1, (startRow + lstNewIndexBO.Count), 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

                startRow = startRow + lstNewIndexBO.Count + 1;
                startIndex++;
                countIndex = 1;
            }
            secondSheet.SetFontName("Times New Roman", 0);
        }
        private void FillDataEvaluation(IVTWorksheet secondSheet, int ClassID, int EvaluationDevGroID, string EvaluationDevName)
        {
            int SchoolID = _globalInfo.SchoolID.Value;
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            //  chi so
            List<DeclareEvaluationIndexBO> lstDeclareEvalIndex = new List<DeclareEvaluationIndexBO>();
  
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevel", EducationLevelID);
            dic.Add("DeclareEvaluationGroupID", EvaluationDevGroID);
            dic.Add("ProvinceID", _globalInfo.ProvinceID);

            lstDeclareEvalIndex = DeclareEvaluationIndexBusiness.SearchBySchool(SchoolID, dic).ToList();

            secondSheet.GetRange(3, 1, 3, 3).Merge();
            secondSheet.SetCellValue(3, 1, EvaluationDevName.ToUpper());
            secondSheet.GetRange(3, 1, 3, 1).SetHAlign(VTHAlign.xlHAlignCenter);
            secondSheet.GetRange(3, 1, 3, 1).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

            int startRow = 4;
            int index = 1;
            DeclareEvaluationIndexBO objDEI = null;
            for (int i = 0; i < lstDeclareEvalIndex.Count(); i++)
            {
                objDEI = lstDeclareEvalIndex[i];
                secondSheet.SetCellValue(startRow, 1, index);
                secondSheet.SetCellValue(startRow, 2, objDEI.EvaluationDevCode);
                secondSheet.SetCellValue(startRow, 3, objDEI.DescriptionIndex);
                index++;
                startRow++;
            }
            secondSheet.GetRange(4, 1, startRow, 2).SetHAlign(VTHAlign.xlHAlignCenter);

            if (lstDeclareEvalIndex.Count > 0)
            {
                secondSheet.GetRange(3, 1, 3 + lstDeclareEvalIndex.Count, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            secondSheet.SetFontName("Times New Roman", 0);
        }
        #endregion

        #region SetDataToFile1
        private void SetDataToFile1(
            IVTWorksheet sheet,
            List<PupilOfClassBO> lstPOC,
            List<GrowthEvaluationBO> lstGrowthEvaluation,
            List<DeclareEvaluationIndexBO> lstDeclareEvaluationIndex,
            string YearTitle,
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroupBO,
            string ClassName,
            int sheetNumber,
            List<VTDataValidation> lstValidation, int EducationLevelID)
        {
            //fill thong tin chung

            string strTitle = string.Empty;
            VTDataValidation objValidation = new VTDataValidation();
            GrowthEvaluationBO objGE = null;
            string[] cd = new string[] { };

            if (ClassName.ToUpper().Contains("LỚP"))
            {
                strTitle = "BẢNG ĐÁNH GIÁ SỰ PHÁT TRIỂN {0} - {1}";
            }
            else
            {
                strTitle = "BẢNG ĐÁNH GIÁ SỰ PHÁT TRIỂN {0} - LỚP {1}";
            }

            sheet.SetCellValue("A2", string.Format(strTitle, "", ClassName.ToUpper()));
            strTitle = "Năm học " + YearTitle;
            sheet.SetCellValue("A3", strTitle);

            if (EducationLevelID == 18)
            {
                cd = new string[] { "+", "-", "±" };
            }
            else
            {
                cd = new string[] { "+", "-" };
            }

            sheet = RenderHeader(sheet, lstDeclareEvaluationGroupBO, lstDeclareEvaluationIndex, EducationLevelID);

            int startSumary = (lstDeclareEvaluationIndex.Count() + 4);
            string alphaEnd = UtilsBusiness.GetExcelColumnName((startSumary - 1));

            int startRow = 7;
            int startColumn = 4;
            PupilOfClassBO objPOC = null;
            // fill thông tin hoc sinh
            DeclareEvaluationIndexBO objDEI = null;

            string keyWord = string.Empty;
            int index = 1;

            if (lstDeclareEvaluationIndex.Count > 0)
            {
                //combobox
                objValidation = new VTDataValidation
                {
                    Contrains = cd,
                    FromColumn = 4,
                    FromRow = 7,
                    SheetIndex = sheetNumber,
                    ToColumn = 4 + lstDeclareEvaluationIndex.Count - 1,
                    ToRow = 7 + lstPOC.Count - 1,
                    Type = VTValidationType.LIST
                };
                lstValidation.Add(objValidation);

                for (int i = 0; i < lstPOC.Count; i++)
                {
                    objPOC = lstPOC[i];
                    // thong tin hoc sinh
                    sheet.SetCellValue(startRow, 1, index);
                    sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                    sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                    objGE = lstGrowthEvaluation.Where(p => p.PupilID == objPOC.PupilID).FirstOrDefault();
                    if (objGE == null)
                    {
                        for (int h = 0; h < lstDeclareEvaluationIndex.Count; h++)
                        {
                            sheet.SetCellValue(startRow, 4 + h, "");
                        }

                        if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, 2, startRow, 3 + lstDeclareEvaluationIndex.Count()).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                            sheet.GetRange(startRow, 1, startRow, 3 + lstDeclareEvaluationIndex.Count()).IsLock = true;
                        }
                        else
                        {
                            sheet.GetRange(startRow, 4, startRow, 3 + lstDeclareEvaluationIndex.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.GetRange(startRow, 1, startRow, 3 + lstDeclareEvaluationIndex.Count()).IsLock = false;
                        }
                        index++;
                        startRow++;
                        continue;
                    }
                    else
                        objGE = null;

                    for (int j = 0; j < lstDeclareEvaluationIndex.Count; j++) // danh sach chi so
                    {
                        objDEI = lstDeclareEvaluationIndex[j];
                        objGE = lstGrowthEvaluation.Where(p => p.PupilID == objPOC.PupilID && p.DeclareEvaluationIndexID == objDEI.EvaluationDevIndexID).FirstOrDefault();

                        if (objGE != null)
                        {
                            keyWord = objGE.EvaluationType == 1 ? "+" : objGE.EvaluationType == 2 ? "-" : objGE.EvaluationType == 3 ? "±" : "";
                        }
                        else
                            keyWord = "";

                        sheet.SetCellValue(startRow, startColumn, keyWord);
                        sheet.GetRange(startRow, startColumn, startRow, startColumn).SetHAlign(VTHAlign.xlHAlignCenter);

                        if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                        {
                            sheet.GetRange(startRow, 1, startRow, 3 + lstDeclareEvaluationIndex.Count()).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).IsLock = true;
                        }

                        else
                            sheet.GetRange(startRow, startColumn, startRow, startColumn).IsLock = false;

                        startColumn++;
                    }

                    sheet = RenderFollowColumn(sheet, EducationLevelID, startRow, startSumary, alphaEnd);

                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 1, startRow, (3 + lstDeclareEvaluationIndex.Count + (EducationLevelID < 18 ? 4 : 6))).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        sheet.GetRange(startRow, 1, startRow, (3 + lstDeclareEvaluationIndex.Count + (EducationLevelID < 18 ? 4 : 6))).IsLock = true;
                    }

                    index++;
                    startColumn = 4;
                    startRow++;
                }

                if (lstPOC.Count > 0)
                {
                    sheet.GetRange(7, 1, 7 + lstPOC.Count - 1 + (EducationLevelID < 18 ? 4 : 6), (3 + lstDeclareEvaluationIndex.Count + (EducationLevelID < 18 ? 4 : 6))).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                else
                {
                    startRow++;
                }
                sheet.GetRange(startRow, startSumary, startRow + (EducationLevelID < 18 ? 3 : 5), startSumary + (EducationLevelID < 18 ? 3 : 5)).Merge();

                sheet = RenderFollowRow(sheet, lstDeclareEvaluationIndex, EducationLevelID, startRow);
            }

            if (lstDeclareEvaluationIndex.Count == 0)
            {
                for (int i = 0; i < lstPOC.Count; i++)
                {
                    objPOC = lstPOC[i];
                    sheet.SetCellValue(startRow, 1, index);
                    sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                    sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);

                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 2, startRow, 3).SetFontStyle(false, System.Drawing.Color.Red, false, 11, true, false);
                        // sheet.GetRange(startRow, 1, startRow, 3).IsLock = true;
                    }
                    //else
                    //{
                    //    sheet.GetRange(startRow, 4, startRow, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                    //    // sheet.GetRange(startRow, 1, startRow, 3).IsLock = false;
                    //}
                    index++;
                    startRow++;
                }
                sheet.GetRange(7, 1, 7 + lstPOC.Count - 1, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            sheet.SetFontName("Times New Roman", 12);
        }

        #region // RenderHeader
        private IVTWorksheet RenderHeader(IVTWorksheet sheet,
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroupBO,
            List<DeclareEvaluationIndexBO> lstDeclareEvaluationIndex,
            int EducationLevelID)
        {
            int g = 4;
            int startCol = 4;
            for (int n = 0; n < lstDeclareEvaluationGroupBO.Count(); n++)
            {
                var tempt = lstDeclareEvaluationIndex.Where(x => x.EvaluationDevGroupID == lstDeclareEvaluationGroupBO[n].EvaluationGroupID).ToList();

                for (int i = 0; i < tempt.Count; i++)
                {
                    sheet.SetCellValue(6, startCol, tempt[i].EvaluationDevCode);
                    sheet.GetRange(6, startCol, 6, startCol).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(6, startCol, 6, startCol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    sheet.GetRange(6, startCol, 6, startCol).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    sheet.SetColumnWidth(startCol, 5);

                    sheet.SetCellValue(1000, startCol, lstDeclareEvaluationGroupBO[n].EvaluationGroupID); // Dung de check import
                    startCol++;
                }

                if (tempt.Count() > 0)
                {
                    sheet.SetCellValue(5, g, lstDeclareEvaluationGroupBO[n].EvaluationDevelopmentGroupName);
                    sheet.GetRange(5, g, 5, (g + tempt.Count() - 1)).Merge();
                    sheet.GetRange(5, g, 5, (g + tempt.Count() - 1)).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheet.GetRange(5, g, 5, (g + tempt.Count() - 1)).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    sheet.GetRange(5, g, 5, (g + tempt.Count() - 1)).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                    g += tempt.Count();
                }
            }

            sheet.SetCellValue(5, g, "Tổng kết");
            sheet.GetRange(5, g, 5, (EducationLevelID < 18 ? (g + 3) : (g + 5))).Merge();
            sheet.GetRange(5, g, 5, (EducationLevelID < 18 ? (g + 3) : (g + 5))).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(5, g, 5, (EducationLevelID < 18 ? (g + 3) : (g + 5))).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(5, g, 5, (EducationLevelID < 18 ? (g + 3) : (g + 5))).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

            sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 4, "Tổng (+)");
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 4, 6, lstDeclareEvaluationIndex.Count() + 4).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 4, 6, lstDeclareEvaluationIndex.Count() + 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 4, 6, lstDeclareEvaluationIndex.Count() + 4).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 4, 8);

            sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 5, "% (+)");
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 5, 6, lstDeclareEvaluationIndex.Count() + 5).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 5, 6, lstDeclareEvaluationIndex.Count() + 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 5, 6, lstDeclareEvaluationIndex.Count() + 5).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 5, 8);

            sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 6, "Tổng (-)");
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 6, 6, lstDeclareEvaluationIndex.Count() + 6).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 6, 6, lstDeclareEvaluationIndex.Count() + 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 6, 6, lstDeclareEvaluationIndex.Count() + 6).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 6, 8);

            sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 7, "% (-)");
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 7, 6, lstDeclareEvaluationIndex.Count() + 7).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 7, 6, lstDeclareEvaluationIndex.Count() + 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 7, 6, lstDeclareEvaluationIndex.Count() + 7).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 7, 8);

            if (EducationLevelID == 18)
            {
                sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 8, "Tổng (±)");
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 8, 6, lstDeclareEvaluationIndex.Count() + 8).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 8, 6, lstDeclareEvaluationIndex.Count() + 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 8, 6, lstDeclareEvaluationIndex.Count() + 8).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 8, 8);

                sheet.SetCellValue(6, lstDeclareEvaluationIndex.Count() + 9, "% (±)");
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 9, 6, lstDeclareEvaluationIndex.Count() + 9).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 9, 6, lstDeclareEvaluationIndex.Count() + 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(6, lstDeclareEvaluationIndex.Count() + 9, 6, lstDeclareEvaluationIndex.Count() + 9).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.SetColumnWidth(lstDeclareEvaluationIndex.Count() + 9, 8);
            }
            return sheet;
        }
        #endregion

        #region // render follow column
        private IVTWorksheet RenderFollowColumn(IVTWorksheet sheet, int EducationLevelID, int startRow, int startSumary, string alphaEnd)
        {
            // Tong (+)
            string str1 = "=COUNTIF(D{0}:{1}{2}," + "{3}+{3}" + ")";
            str1 = string.Format(str1, startRow, alphaEnd, startRow, '"');
            sheet.SetCellValue(startRow, startSumary, str1);

            // % (+) =IF(COUNTA(D7:AL7)>0,ROUND(AM7/COUNTA(D7:AL7)*100,2),0)
            str1 = "=IF(COUNTA(D{0}:{1}{2})>0,ROUND({3}{4}/COUNTA(D{5}:{6}{7})*100,2),0)";
            str1 = string.Format(str1, startRow, alphaEnd, startRow, UtilsBusiness.GetExcelColumnName(startSumary), startRow, startRow, alphaEnd, startRow);
            sheet.SetCellValue(startRow, startSumary + 1, str1);

            // Tong (-)
            str1 = "=COUNTIF(D{0}:{1}{2}," + "{3}-{3}" + ")";
            str1 = string.Format(str1, startRow, alphaEnd, startRow, '"');
            sheet.SetCellValue(startRow, startSumary + 2, str1);

            // % (-) =IF(COUNTA(D7:AL7)>0,ROUND(AM7/COUNTA(D7:AL7)*100,2),0)
            str1 = "=IF(COUNTA(D{0}:{1}{2})>0,ROUND({3}{4}/COUNTA(D{5}:{6}{7})*100,2),0)";
            str1 = string.Format(str1, startRow, alphaEnd, startRow, UtilsBusiness.GetExcelColumnName(startSumary + 2), startRow, startRow, alphaEnd, startRow);
            sheet.SetCellValue(startRow, startSumary + 3, str1);

            if (EducationLevelID == 18)
            {
                // Tong (±)
                str1 = "=COUNTIF(D{0}:{1}{2}," + "{3}±{3}" + ")";
                str1 = string.Format(str1, startRow, alphaEnd, startRow, '"');
                sheet.SetCellValue(startRow, startSumary + 4, str1);

                // % (±) =IF(COUNTA(D7:AL7)>0,ROUND(AM7/COUNTA(D7:AL7)*100,2),0)
                str1 = "=IF(COUNTA(D{0}:{1}{2})>0,ROUND({3}{4}/COUNTA(D{5}:{6}{7})*100,2),0)";
                str1 = string.Format(str1, startRow, alphaEnd, startRow, UtilsBusiness.GetExcelColumnName(startSumary + 4), startRow, startRow, alphaEnd, startRow);
                sheet.SetCellValue(startRow, startSumary + 5, str1);
            }
            return sheet;
        }
        #endregion

        #region RenderFollowRow
        private IVTWorksheet RenderFollowRow(IVTWorksheet sheet, List<DeclareEvaluationIndexBO> lstDeclareEvaluationIndex,
            int EducationLevelID, int startRow)
        {
            sheet.SetCellValue(startRow, 1, "Tổng (+)");
            sheet.GetRange(startRow, 1, startRow, 3).Merge();
            sheet.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignRight);
            sheet.GetRange(startRow, 1, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(startRow, 1, startRow, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);

            sheet.SetCellValue(startRow + 1, 1, "% (+)");
            sheet.GetRange(startRow + 1, 1, startRow + 1, 3).Merge();
            sheet.GetRange(startRow + 1, 1, startRow + 1, 3).SetHAlign(VTHAlign.xlHAlignRight);
            sheet.GetRange(startRow + 1, 1, startRow + 1, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(startRow + 1, 1, startRow + 1, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);

            sheet.SetCellValue(startRow + 2, 1, "Tổng (-)");
            sheet.GetRange(startRow + 2, 1, startRow + 2, 3).Merge();
            sheet.GetRange(startRow + 2, 1, startRow + 2, 3).SetHAlign(VTHAlign.xlHAlignRight);
            sheet.GetRange(startRow + 2, 1, startRow + 2, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(startRow + 2, 1, startRow + 2, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);

            sheet.SetCellValue(startRow + 3, 1, "% (-)");
            sheet.GetRange(startRow + 3, 1, startRow + 3, 3).Merge();
            sheet.GetRange(startRow + 3, 1, startRow + 3, 3).SetHAlign(VTHAlign.xlHAlignRight);
            sheet.GetRange(startRow + 3, 1, startRow + 3, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(startRow + 3, 1, startRow + 3, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);

            if (EducationLevelID == 18)
            {
                sheet.SetCellValue(startRow + 4, 1, "Tổng (±)");
                sheet.GetRange(startRow + 4, 1, startRow + 4, 3).Merge();
                sheet.GetRange(startRow + 4, 1, startRow + 4, 3).SetHAlign(VTHAlign.xlHAlignRight);
                sheet.GetRange(startRow + 4, 1, startRow + 4, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(startRow + 4, 1, startRow + 4, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);

                sheet.SetCellValue(startRow + 5, 1, "% (±)");
                sheet.GetRange(startRow + 5, 1, startRow + 5, 3).Merge();
                sheet.GetRange(startRow + 5, 1, startRow + 5, 3).SetHAlign(VTHAlign.xlHAlignRight);
                sheet.GetRange(startRow + 5, 1, startRow + 5, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheet.GetRange(startRow + 5, 1, startRow + 5, 3).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);
            }

            for (int m = 4; m < (lstDeclareEvaluationIndex.Count() + 4); m++)
            {
                string alpha = UtilsBusiness.GetExcelColumnName(m);

                string str1 = "=COUNTIF({0}7:{1}{2}," + "{3}+{3}" + ")";
                str1 = string.Format(str1, alpha, alpha, (startRow - 1), '"');
                // Tong (+)
                sheet.SetCellValue(startRow, m, str1);
                // % (+)
                str1 = "=IF(COUNTA({0}7:{1}{2})>0,ROUND({3}{4}/COUNTA({5}7:{6}{7})*100,2),0)";
                str1 = string.Format(str1, alpha, alpha, (startRow - 1), alpha, (startRow), alpha, alpha, (startRow - 1));
                sheet.SetCellValue(startRow + 1, m, str1);

                // Tong (-)
                str1 = "=COUNTIF({0}7:{1}{2}," + "{3}-{3}" + ")";
                str1 = string.Format(str1, alpha, alpha, (startRow - 1), '"');
                sheet.SetCellValue(startRow + 2, m, str1);
                // % (-)
                str1 = "=IF(COUNTA({0}7:{1}{2})>0,ROUND({3}{4}/COUNTA({5}7:{6}{7})*100,2),0)";
                str1 = string.Format(str1, alpha, alpha, (startRow - 1), alpha, (startRow + 2), alpha, alpha, (startRow - 1));
                sheet.SetCellValue(startRow + 3, m, str1);

                if (EducationLevelID == 18)
                {
                    // Tong (±)
                    str1 = "=COUNTIF({0}7:{1}{2}," + "{3}±{3}" + ")";
                    str1 = string.Format(str1, alpha, alpha, (startRow - 1), '"');
                    sheet.SetCellValue(startRow + 4, m, str1);
                    // % (±)
                    str1 = "=IF(COUNTA({0}7:{1}{2})>0,ROUND({3}{4}/COUNTA({5}7:{6}{7})*100,2),0)";
                    str1 = string.Format(str1, alpha, alpha, (startRow - 1), alpha, (startRow + 4), alpha, alpha, (startRow - 1));
                    sheet.SetCellValue(startRow + 5, m, str1);
                }
            }

            return sheet;
        }
        #endregion
        #endregion

        #region GetListDeclareEvaluation
        private List<GrowthEvaluationBO> GetListDeclareEvaluation(int ClassID)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            List<GrowthEvaluationBO> lsGE = new List<GrowthEvaluationBO>();

            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID", ClassID},
                };

            lsGE = (from ge in GrowthEvaluationBusiness.Search(dic).ToList()
                    join deg in DeclareEvaluationGroupBusiness.All
                        on ge.DeclareEvaluationGroupID equals deg.DeclareEvaluationGroupID
                    join edg in EvaluationDevelopmentGroupBusiness.All
                        on deg.EvaluationGroupID equals edg.EvaluationDevelopmentGroupID
                    where edg.IsActive = true
                    select new GrowthEvaluationBO()
                    {
                        Name = edg.EvaluationDevelopmentGroupName,
                        DeclareEvaluationGroupID = ge.DeclareEvaluationGroupID
                    }).OrderBy(o => o.DeclareEvaluationGroupID).ThenBy(o => o.Name).Distinct().ToList();
            return lsGE;
        }
        #endregion


        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImportSubject(IEnumerable<HttpPostedFileBase> attachments, int ClassID, int EvaluationDevGroID)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                List<string> listSheetNameExcel = new List<string>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage(Res.Get("Common_Max_Size_FileExcel"), "error"));
                }

                string sheetInfo = string.Empty;
                // duyệt kiểm tra từng sheet
                string sheetName = string.Empty;
                StringBuilder strBuilder = new StringBuilder();
                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        sheetInfo = sheet.Name;
                        sheetInfo = SMAS.Web.Utils.Utils.StripVNSignAndSpace(sheetInfo, true).ToUpper();
                        if (!sheetInfo.Equals("THONGTINCHISO"))
                        {
                            strValue = (sheet.GetCellValue(1000, 1) != null ? sheet.GetCellValue(1000, 1).ToString() : string.Empty);

                            if (!String.IsNullOrEmpty(strValue))
                            {

                                if (strValue.Equals("1"))
                                {
                                    listTypeReportExcel.Add(1);
                                }
                            }
                            else
                            {
                                return Json(new JsonMessage(Res.Get("File dữ liệu không hợp lệ"), "error"));
                            }
                        }
                    }
                }

                if (listTypeReportExcel != null && listTypeReportExcel.Count > 0)
                {
                    int valueImport = listTypeReportExcel.FirstOrDefault();
                    Session[DevelopmentOfChildrenConstants.PHISICAL_PATH] = physicalPath;

                    return Json(new { TypeImport = valueImport });
                }

                return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }


        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int typeImport, int ClassID)
        {
            if (Session[DevelopmentOfChildrenConstants.PHISICAL_PATH] == null)
                return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            string physicalPath = (string)Session[DevelopmentOfChildrenConstants.PHISICAL_PATH];
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            List<GrowthEvaluationBO> lstGrowthEvaluationBO = new List<GrowthEvaluationBO>();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            List<int> lstPupilID = new List<int>();

            IVTWorkbook obook = VTExport.OpenWorkbook(physicalPath);
            string messageSuccess = string.Empty;
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            string Error = string.Empty;
            List<int> lstError = new List<int>();

            // danh sach linh vuc
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO = new List<DeclareEvaluationGroupBO>();
            lstDeclareEvaluationGroBO = this.ListDeclareEvaluationGroBO(EducationLevelID);

            // danh sách id các lĩnh vực
            //List<int> lstEvaluationGroAll = ArrEvaluationGroID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<int> lstEvaluationGroID = new List<int>();

            if (typeImport == 1)
            {
                if (lstDeclareEvaluationGroBO.Count >= 1)
                {
                    List<string> lstEvaluationName = lstDeclareEvaluationGroBO.OrderBy(p => p.OrderID)
                                                                                .Select(p => p.EvaluationDevelopmentGroupName)
                                                                                .Distinct().ToList();

                    messageSuccess = "Nhập dữ liệu từ excel thành công cho lĩnh vực {0}";
                    string str = string.Join(", ", lstEvaluationName.ToArray());
                    messageSuccess = string.Format(messageSuccess, str);
                }

                //lay danh sach hoc sinh theo lop
                lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ToList();

                lstGrowthEvaluationBO = this.GetDataToFileExcel(obook, ref lstGrowthEvaluationBO, lstPOC,
                lstDeclareEvaluationGroBO, ClassID, EducationLevelID,
                ref lstPupilID, ref Error, ref lstError);

                if (!string.IsNullOrEmpty(Error))
                {
                    return Json(new JsonMessage(Error, "error"));
                }

                if (lstGrowthEvaluationBO.Count > 0)
                {
                    if (lstError != null && lstError.Count() > 0)
                    {
                        List<ListError> LstErrorMessage = new List<ListError>();
                        ListError objError = null;
                        foreach (var item in lstGrowthEvaluationBO)
                        {
                            if (item.ListErrorMessage != null)
                            {
                                foreach (var item2 in item.ListErrorMessage)
                                {
                                    objError = new ListError();
                                    objError.PupilCode = item2.PupilCode;
                                    objError.PupilName = item2.PupilName;
                                    objError.Description = item2.Description;
                                    objError.SheetName = item2.SheetName;
                                    LstErrorMessage.Add(objError);
                                }
                                ViewData[DevelopmentOfChildrenConstants.LIST_ERROR_IMPORT] = LstErrorMessage;
                                return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "CreateViewError"));
                            }

                        }
                    }

                    Dictionary<string, object> dicInsert = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID.Value},
                        {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                        {"ClassID",ClassID},
                        {"lstPupilID", lstPupilID}
                    };
                    GrowthEvaluationBusiness.InsertOrUpdateGrowthEvaluation(lstGrowthEvaluationBO, dicInsert);
                    return Json(new JsonMessage(messageSuccess, "success"));
                }
                else
                { // file excel không hợp lệ
                    return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
                }
            }

            return Json(new JsonMessage(Res.Get("Common_FileExcel_Error"), "error"));
        }

        private List<GrowthEvaluationBO> GetDataToFileExcel(
            IVTWorkbook oBook,
            ref List<GrowthEvaluationBO> lstGrowthEvaluationBO,
            List<PupilOfClassBO> lstPOC,
            List<DeclareEvaluationGroupBO> lstDeclareEvaluationGroBO,
            int ClassID,
            int EducationLevelID,
            ref List<int> lstPupilID,
            ref string Error,
            ref List<int> lstError)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;

            // thông tin lop
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            DeclareEvaluationGroupBO objEvaluaGroBO = null;

            IVTWorksheet sheetData = oBook.GetSheet(1);
            #region Validate du lieu
            #region Kiem tra du lieu chon dau vao
            string ClassName = classProfile.DisplayName;
            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
            string AcademicYearNameFromExcel = (string)sheetData.GetCellValue("A3");
            string Title = "";
            var TitleObj = sheetData.GetCellValue("A2");

            if (TitleObj != null)
                Title = TitleObj.ToString();

            if (Title == null || AcademicYearNameFromExcel == null)
            {
                Error = "File excel không đúng định dạng";
            }

            if (!Title.Contains(ClassName.ToUpper()))
            {
                if (string.IsNullOrEmpty(Error))
                {
                    Error = "File excel không phải của lớp " + ClassName;
                }
            }

            if (!AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper())
                && !AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()))
            {
                if (string.IsNullOrEmpty(Error))
                {
                    Error = "Năm học không phải năm học hiện tại hoặc định dạng không hợp lệ";
                }
            }

            if (!string.IsNullOrEmpty(Error))
            {
                return lstGrowthEvaluationBO;
            }
            #endregion

            List<int> lstEvaluationGroID = lstDeclareEvaluationGroBO.Select(x => x.EvaluationGroupID).Distinct().ToList();

            // danh sách chỉ số của tất cả các lĩnh vực thuộc 1 nhóm lớp
            List<DeclareEvaluationIndexBO> lstNewIndexBO = new List<DeclareEvaluationIndexBO>();
            IDictionary<string, object> dicDEI = new Dictionary<string, object>();
            dicDEI.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicDEI.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicDEI.Add("EducationLevel", EducationLevelID);
            dicDEI.Add("DeclareEvaluationGroupID", 0);
            dicDEI.Add("ProvinceID", _globalInfo.ProvinceID);

            List<DeclareEvaluationIndexBO> lstDEI = DeclareEvaluationIndexBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicDEI).ToList();

            lstDEI = lstDEI.Where(x => lstEvaluationGroID.Contains(x.EvaluationDevGroupID)).ToList();

            List<DeclareEvaluationIndexBO> lstDEI_Tempt = new List<DeclareEvaluationIndexBO>();

            for (int i = 0; i < lstEvaluationGroID.Count(); i++)
            {
                var tempt = lstDEI.Where(x => x.EvaluationDevGroupID == lstEvaluationGroID[i]).ToList();
                var lstDeclareEvaluationByClass = tempt.Where(x => x.ClassID == ClassID).ToList();
                if (lstDeclareEvaluationByClass.Count() > 0)
                {
                    lstDEI_Tempt.AddRange(lstDeclareEvaluationByClass);
                }
                else
                {
                    tempt = tempt.Where(x => !x.ClassID.HasValue).ToList();
                    lstDEI_Tempt.AddRange(tempt);
                }
            }
            lstDEI = lstDEI_Tempt;

            //lay du lieu tu file
            #region lay du lieu tu file
            int startCol = 4;
            List<CellIndexViewModel> lstCellIndexViewModelTempt = new List<CellIndexViewModel>();

            while (sheetData.GetCellValue(1000, startCol) != null)
            {
                int valuaCell = Int32.Parse(sheetData.GetCellValue(1000, startCol).ToString());

                if (lstEvaluationGroID.Contains(valuaCell))
                {
                    CellIndexViewModel objCell = new CellIndexViewModel();
                    objCell.EvaluationGroupID = valuaCell;
                    objCell.StartColumnExcel = startCol;
                    objCell.EndColumnExcel = startCol;
                    lstCellIndexViewModelTempt.Add(objCell);
                }
                startCol++;
            }

            if (lstCellIndexViewModelTempt.Count() == 0)
            {
                Error = "File excel không hợp lệ.";
                return lstGrowthEvaluationBO;
            }

            List<CellIndexViewModel> lstCellIndexViewModel = new List<CellIndexViewModel>();
            for (int i = 0; i < lstEvaluationGroID.Count; i++)
            {
                objEvaluaGroBO = lstDeclareEvaluationGroBO.Where(x => x.EvaluationGroupID == lstEvaluationGroID[i]).FirstOrDefault();

                lstNewIndexBO = lstDEI.Where(x => x.DeclareEvaluationGroupID == objEvaluaGroBO.DeclareEvaluationGroupID)
                                          .OrderBy(x => x.OrderID).ToList();

                if (lstNewIndexBO.Count == 0)
                {
                    Error = "Lĩnh vực " + objEvaluaGroBO.EvaluationDevelopmentGroupName + " chưa được khai báo chỉ số";
                    return lstGrowthEvaluationBO;
                }

                CellIndexViewModel objCell = new CellIndexViewModel();
                objCell.EvaluationGroupID = objEvaluaGroBO.EvaluationGroupID;
                objCell.StartColumnExcel = lstCellIndexViewModelTempt.Where(x => x.EvaluationGroupID == objEvaluaGroBO.EvaluationGroupID).Min(x => x.StartColumnExcel);
                objCell.EndColumnExcel = lstCellIndexViewModelTempt.Where(x => x.EvaluationGroupID == objEvaluaGroBO.EvaluationGroupID).Max(x => x.EndColumnExcel);
                lstCellIndexViewModel.Add(objCell);
            }

            if (lstCellIndexViewModel.Count() > 0)
            {
                lstGrowthEvaluationBO = this.GetData(sheetData, lstCellIndexViewModel, lstDEI,
                    ref lstGrowthEvaluationBO, lstPOC, ClassID, classProfile.DisplayName,
                    EducationLevelID, ref lstPupilID, ref Error, ref lstError);
            }
            else
            {
                Error = "File excel không hợp lệ.";
            }
            #endregion

            return lstGrowthEvaluationBO;
        }

        private List<GrowthEvaluationBO> GetData(
            IVTWorksheet sheet,
            List<CellIndexViewModel> lstCellIndexViewModel,
            List<DeclareEvaluationIndexBO> lstDEI,
            ref List<GrowthEvaluationBO> lstGrowthEvaluationBO,
            List<PupilOfClassBO> lstPOC,
            int ClassID,
            string ClassName,
            int EducationLevelID,
            ref List<int> lstPupilID,
            ref string Error,
            ref List<int> lstErrorIndex)
        {
            int logChangeID = _globalInfo.IsAdminSchoolRole ? 0 : _globalInfo.EmployeeID.Value;
            int LastDigitSchoolID = _globalInfo.SchoolID.Value % 100;
            PupilOfClassBO objPOC = null;
            string pupilCode = string.Empty;
            int pupilID = 0;
            int startRow = 7;
            int indexError = 0;
            GrowthEvaluationBO objGrowEvaluationBO = null;
            List<int> lstNewPupilID = new List<int>();
            List<ListError> lstError = new List<ListError>();
            ListError objError = null;
            string lstEvaluaIndexNotFound = string.Empty;
            int starCol = 0;
            int endCol = 0;
            string codeIndex = "";
            for (int i = 0; i < lstCellIndexViewModel.Count(); i++)
            {
                var objCellVM = lstCellIndexViewModel[i];

                starCol = objCellVM.StartColumnExcel;
                endCol = objCellVM.EndColumnExcel;

                for (int j = starCol; j < endCol; j++)
                {
                    if (sheet.GetCellValue(6, starCol) != null)
                    {
                        codeIndex = sheet.GetCellValue(6, starCol).ToString().Trim();
                        var checkObjDEI = lstDEI.Where(x => x.EvaluationDevGroupID == objCellVM.EvaluationGroupID && x.EvaluationDevCode == codeIndex).FirstOrDefault();

                        if (checkObjDEI != null)
                            continue;

                        lstEvaluaIndexNotFound += codeIndex + ", ";
                    }
                    else
                    {
                        Error = "Chỉ số không được để trống";
                        return lstGrowthEvaluationBO;
                    }
                }
            }

            if (!string.IsNullOrEmpty(lstEvaluaIndexNotFound))
            {
                lstEvaluaIndexNotFound = lstEvaluaIndexNotFound.Substring(0, lstEvaluaIndexNotFound.Length - 2);
                Error = "Chỉ số " + lstEvaluaIndexNotFound + " không tồn tại";
                return lstGrowthEvaluationBO;
            }

            //duyệt nội dung đánh giá
            string valuaCellType = string.Empty;
            while (sheet.GetCellValue(startRow, 2) != null && sheet.GetCellValue(startRow, 3) != null)
            {
                #region // check ma hoc sinh
                pupilCode = sheet.GetCellValue(startRow, 2).ToString();
                objPOC = lstPOC.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                if (objPOC == null)
                {
                    objError = new ListError();
                    objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                    objError.PupilCode = pupilCode;
                    objError.Description = "Mã trẻ không tồn tại";
                    objError.SheetName = sheet.Name;
                    lstError.Add(objError);

                    objGrowEvaluationBO = new GrowthEvaluationBO();
                    objGrowEvaluationBO.ListErrorMessage = lstError;
                    lstGrowthEvaluationBO.Add(objGrowEvaluationBO);
                    indexError++;

                    startRow++;
                    continue;
                }
                else
                {
                    pupilID = objPOC.PupilID;
                    lstNewPupilID.Add(pupilID);
                    if (lstNewPupilID.Where(p => p == pupilID).Count() > 1)
                    {
                        objError = new ListError();
                        objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                        objError.PupilCode = pupilCode;
                        objError.Description = "Mã trẻ bị trùng lập";
                        objError.SheetName = sheet.Name;
                        lstError.Add(objError);

                        objGrowEvaluationBO = new GrowthEvaluationBO();
                        objGrowEvaluationBO.ListErrorMessage = lstError;
                        lstGrowthEvaluationBO.Add(objGrowEvaluationBO);
                        indexError++;

                    }
                    if (objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        startRow++;
                        continue;
                    }
                }
                #endregion

                #region // Get data
                for (int i = 0; i < lstCellIndexViewModel.Count(); i++)
                {
                    var objCellVM = lstCellIndexViewModel[i];
                    starCol = objCellVM.StartColumnExcel;
                    endCol = objCellVM.EndColumnExcel;

                    for (int j = starCol; j <= endCol; j++)
                    {
                        codeIndex = sheet.GetCellValue(6, j).ToString().Trim();
                        var checkObjDEI = lstDEI.Where(x => x.EvaluationDevGroupID == objCellVM.EvaluationGroupID
                                                            && x.EvaluationDevCode == codeIndex).FirstOrDefault();

                        objGrowEvaluationBO = new GrowthEvaluationBO();
                        objGrowEvaluationBO.PupilID = pupilID;
                        objGrowEvaluationBO.PupilCode = pupilCode;
                        objGrowEvaluationBO.LogID = logChangeID;
                        objGrowEvaluationBO.SchoolID = _globalInfo.SchoolID.Value;
                        objGrowEvaluationBO.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objGrowEvaluationBO.ClassID = ClassID;
                        objGrowEvaluationBO.DeclareEvaluationGroupID = checkObjDEI.EvaluationDevGroupID;
                        objGrowEvaluationBO.DeclareEvaluationIndexID = checkObjDEI.EvaluationDevelopmentID;
                        objGrowEvaluationBO.LastDigitSchoolID = LastDigitSchoolID;

                        if (sheet.GetCellValue(startRow, j) != null)
                        {
                            valuaCellType = sheet.GetCellValue(startRow, j).ToString();

                            if (!(valuaCellType.Equals("+") || valuaCellType.Equals("-") || valuaCellType.Equals("±")))
                            {
                                objError = new ListError();
                                objError.PupilName = sheet.GetCellValue(startRow, 3).ToString();
                                objError.PupilCode = pupilCode;
                                objError.Description = "Dữ liệu đánh giá không hợp lệ";
                                objError.SheetName = sheet.Name;
                                lstError.Add(objError);

                                objGrowEvaluationBO = new GrowthEvaluationBO();
                                objGrowEvaluationBO.ListErrorMessage = lstError;
                                lstGrowthEvaluationBO.Add(objGrowEvaluationBO);
                                indexError++;
                                continue;
                            }
                        }
                        else
                            valuaCellType = "";

                        objGrowEvaluationBO.EvaluationType = valuaCellType == "+" ? 1 : valuaCellType == "-" ? 2 : valuaCellType == "±" ? 3 : 0;
                        lstGrowthEvaluationBO.Add(objGrowEvaluationBO);
                    }
                }
                #endregion

                startRow++;
            }

            if (indexError != 0)
            {
                lstErrorIndex.Add(indexError);
            }

            if (lstPupilID == null || lstPupilID.Count == 0)
            {
                lstPupilID = lstNewPupilID;
            }

            return lstGrowthEvaluationBO;
        }
            #endregion

    }

}
