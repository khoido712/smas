﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
//using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilLeavingOffArea.Models;

using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;
using System.Transactions;
using System.Globalization;

namespace SMAS.Web.Areas.PupilLeavingOffArea.Controllers
{
    public class PupilLeavingOffController : BaseController
    {
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ILeavingReasonBusiness LeavingReasonBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PupilLeavingOffController(IPupilLeavingOffBusiness pupilleavingoffBusiness, IEducationLevelBusiness educationLevelBusiness,
            ILeavingReasonBusiness leavingReasonBusiness, IClassProfileBusiness classProfileBusiness, IPupilProfileBusiness pupilProfileBusiness
            , IPupilOfClassBusiness pupilOfClassBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.PupilLeavingOffBusiness = pupilleavingoffBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.LeavingReasonBusiness = leavingReasonBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        //
        // GET: /PupilLeavingOff/

        public ActionResult Index()
        {
            if (_globalInfo.HasHeadTeacherPermission(0) == false)
            {
                throw new BusinessException("Common_Validate_User");
            }
            SetViewData();
            return View();
        }
        #region setViewData
        private void SetViewData()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = _globalInfo.AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox lớp = rỗng
            ViewData[PupilLeavingOffConstants.LIST_Class] = new SelectList(new string[] { });
            //Đưa dữ liệu ra combobox học sinh = rỗng
            ViewData[PupilLeavingOffConstants.LIST_Pupil] = new SelectList(new string[] { });
            //Đưa dữ liệu ra combobox học kỳ
            //List<ComboObject> ListSemester = new List<ComboObject>();
            //ListSemester.Add(new ComboObject("1", Res.Get("Semester_Of_Year_First")));
            //ListSemester.Add(new ComboObject("2", Res.Get("Semester_Of_Year_Second")));
            //ViewData[PupilLeavingOffConstants.LIST_Semester] = new SelectList(ListSemester, "key", "value");
            ViewData[PupilLeavingOffConstants.LIST_Semester] = CommonFunctions.GetListSemester();

            #region LoadSemester
            //Load RadioButton
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool rbchecked = false;
            List<int> lsSemester = new List<int> { 1, 2 };
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio1 = new ViettelCheckboxList();
            ViettelCheckboxList viettelradio2 = new ViettelCheckboxList();
            if (lsSemester != null)
            {
                for (int i = 0; i < lsSemester.Count; i++)
                {
                    if (lsSemester[i] == 1)
                    {
                        viettelradio1 = new ViettelCheckboxList();
                        viettelradio1.Label = Res.Get("Common_Label_FirstSemester");
                        viettelradio1.Value = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        if ((ay.FirstSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.FirstSemesterEndDate))
                        {
                            viettelradio1.cchecked = true;
                            rbchecked = true;
                        }

                        lsradio.Add(viettelradio1);
                    }
                    else if (lsSemester[i] == 2)
                    {
                        viettelradio2 = new ViettelCheckboxList();
                        viettelradio2.Label = Res.Get("Common_Label_SecondSemester");
                        viettelradio2.Value = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        if ((ay.SecondSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.SecondSemesterEndDate))
                        {
                            viettelradio2.cchecked = true;
                            rbchecked = true;
                        }
                        lsradio.Add(viettelradio2);
                    }
                    if (!rbchecked)
                    {
                        viettelradio1.cchecked = true;
                    }
                }
                ViewData[PupilLeavingOffConstants.LST_SEMESTER] = lsradio;
            }
            #endregion

            //Đưa dữ liệu ra combobox lý do nghỉ học
            IDictionary<string, object> LeavingReasonInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            List<LeavingReason> LstLeavingReason = LeavingReasonBusiness.Search(LeavingReasonInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_LeavingReason] = new SelectList(LstLeavingReason, "LeavingReasonID", "Resolution");

            //Đưa ra danh sách học sinh thôi học
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["ListClassID"] = null;
            foreach (var item in lsradio)
            {
                if (item.cchecked == true)
                {
                    SearchInfo["Semester"] = item.Value;
                }
            }
            IEnumerable<PupilLeavingOffViewModel> lst = this._Search(SearchInfo);
            ViewData[PupilLeavingOffConstants.LIST_PUPILLEAVINGOFF] = lst;
            if (_globalInfo.IsCurrentYear == false)
            {
                ViewData[PupilLeavingOffConstants.Disable_AddNew] = true;
            }
            else
                if (_globalInfo.HasHeadTeacherPermission(0) == false)
                {
                    ViewData[PupilLeavingOffConstants.Disable_AddNew] = true;
                }
                else
                {
                    ViewData[PupilLeavingOffConstants.Disable_AddNew] = false;
                }

        }
        #endregion
        // GET: /PupilLeavingOff/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Semester"] = frm.rptSemester;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["ListClassID"] = null;
            //add search info
            //
            IEnumerable<PupilLeavingOffViewModel> lst = this._Search(SearchInfo);
            ViewData[PupilLeavingOffConstants.LIST_PUPILLEAVINGOFF] = lst;
            GlobalInfo global = _globalInfo;
            if (global.IsCurrentYear == false)
            {
                ViewData[PupilLeavingOffConstants.Disable_AddNew] = true;
            }
            else
                if (global.HasHeadTeacherPermission(0) == false)
                {
                    ViewData[PupilLeavingOffConstants.Disable_AddNew] = true;
                }
                else
                {
                    ViewData[PupilLeavingOffConstants.Disable_AddNew] = false;
                }
            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(PupilLeavingOffViewModel frm)
        {
            if (frm.EducationLevelID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_EducationLevelRepuired"), "error"));
            }
            if (frm.ClassID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_ClassRepuired"), "error"));
            }
            if (frm.PupilID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilPraise_Label_PupilRepuired"), "error"));
            }
            if (frm.Semester == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_SemesterRepuired"), "error"));
            }
            if (!frm.LeavingDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_LeavingDateRepuired"), "error"));
            }
            if (frm.LeavingReasonID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_LeavingReasonRepuired"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                PupilLeavingOff PupilLeavingOff = new PupilLeavingOff();
                //if (ModelState.IsValid)
                //{
                    PupilLeavingOff.PupilID = frm.PupilID;
                    PupilLeavingOff.ClassID = frm.ClassID;
                    PupilLeavingOff.EducationLevelID = frm.EducationLevelID;
                    PupilLeavingOff.SchoolID = _globalInfo.SchoolID.Value;
                    PupilLeavingOff.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    PupilLeavingOff.LeavingReasonID = frm.LeavingReasonID;
                    PupilLeavingOff.Semester = frm.Semester;
                    PupilLeavingOff.LeavingDate = frm.LeavingDate.Value.Date;
                    PupilLeavingOff.Description = frm.Description;
                    Utils.Utils.TrimObject(PupilLeavingOff);
                    this.PupilLeavingOffBusiness.InsertPupilLeavingOff(_globalInfo.UserAccountID, PupilLeavingOff);
                    this.PupilLeavingOffBusiness.Save();
                    scope.Complete();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                //}
                //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
                //return Json(new JsonMessage(jsonErrList, "error"));
            }
        }
        public PartialViewResult _Create()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = _globalInfo.AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox lớp = rỗng
            ViewData[PupilLeavingOffConstants.LIST_Class] = new SelectList(new string[] { });
            //Đưa dữ liệu ra combobox học sinh = rỗng
            ViewData[PupilLeavingOffConstants.LIST_Pupil] = new SelectList(new string[] { });
            //Đưa dữ liệu ra combobox học kỳ
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject("1", Res.Get("Semester_Of_Year_First")));
            ListSemester.Add(new ComboObject("2", Res.Get("Semester_Of_Year_Second")));
            ViewData[PupilLeavingOffConstants.LIST_Semester] = new SelectList(ListSemester, "key", "value");

            //Đưa dữ liệu ra combobox lý do nghỉ học
            IDictionary<string, object> LeavingReasonInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            List<LeavingReason> LstLeavingReason = LeavingReasonBusiness.Search(LeavingReasonInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_LeavingReason] = new SelectList(LstLeavingReason, "LeavingReasonID", "Resolution");

            return PartialView("_Create");
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PupilLeavingOffID,PupilLeavingOffEditModel frm)
        {         
            if (frm.Semester == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_SemesterRepuired"), "error"));
            }
            if (!frm.LeavingDate1.HasValue)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_LeavingDateRepuired"), "error"));
            }
          
            if (frm.LeavingReasonID == 0)
            {
                return Json(new JsonMessage(Res.Get("PupilLeavingOff_Label_LeavingReasonRepuired"), "error"));
            }
            using (TransactionScope scope = new TransactionScope())
            {
                PupilLeavingOff PupilLeavingOff = PupilLeavingOffBusiness.Find(PupilLeavingOffID);
                TryUpdateModel(PupilLeavingOff);
                string s = Request["LeavingDate1"];
             
                DateTime dt;
                if (!DateTime.TryParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                {
                    return Json(new JsonMessage(Res.Get("Common_Validate_LeavingDate"), "error"));
                }

                    PupilLeavingOff.LeavingDate = frm.LeavingDate1.Value;
                    Utils.Utils.TrimObject(PupilLeavingOff);
                    this.PupilLeavingOffBusiness.UpdatePupilLeavingOff(_globalInfo.UserAccountID, PupilLeavingOff);
                    this.PupilLeavingOffBusiness.Save();
                    scope.Complete();
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }
        public PartialViewResult _Edit(int id)
        {
            return PartialView("_Edit", PrepareEdit(id));
        }
        private PupilLeavingOffEditModel PrepareEdit(int id)
        {
            PupilLeavingOff PupilLeavingOff = this.PupilLeavingOffBusiness.Find(id);

            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = _globalInfo.AppliedLevel;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");

            // Dua ra combobox Class
            IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
            ClassSearchInfo["EducationLevelID"] = PupilLeavingOff.EducationLevelID;
            ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<ClassProfile> ListClassProfile = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_Class] = new SelectList(ListClassProfile, "ClassProfileID", "DisplayName");

            // Dua ra combobox Pupil
            IDictionary<string, object> PupilSearchInfo = new Dictionary<string, object>();
            PupilSearchInfo["ClassID"] = PupilLeavingOff.ClassID;
            PupilSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, PupilSearchInfo).ToList();
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            foreach (var pop in ListPupilOfClass)
            {
                PupilProfile PupilProfile = new PupilProfile();
                PupilProfile.PupilProfileID = pop.PupilID;
                PupilProfile.FullName = pop.PupilProfile.FullName;
                LstPupilProfile.Add(PupilProfile);
            }
            ViewData[PupilLeavingOffConstants.LIST_Pupil] = new SelectList(LstPupilProfile, "PupilProfileID", "FullName");

            //Đưa dữ liệu ra combobox học kỳ
            List<ComboObject> ListSemester = new List<ComboObject>();
            ListSemester.Add(new ComboObject("1", Res.Get("Semester_Of_Year_First")));
            ListSemester.Add(new ComboObject("2", Res.Get("Semester_Of_Year_Second")));
            ViewData[PupilLeavingOffConstants.LIST_Semester] = new SelectList(ListSemester, "key", "value");

            //Đưa dữ liệu ra combobox lý do nghỉ học
            IDictionary<string, object> LeavingReasonInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            List<LeavingReason> LstLeavingReason = LeavingReasonBusiness.Search(LeavingReasonInfo).ToList();
            ViewData[PupilLeavingOffConstants.LIST_LeavingReason] = new SelectList(LstLeavingReason, "LeavingReasonID", "Resolution");

            PupilLeavingOffEditModel frm = new PupilLeavingOffEditModel();
            frm.LeavingDate1 = PupilLeavingOff.LeavingDate;
            Utils.Utils.BindTo(PupilLeavingOff, frm);
            return frm;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id,int semester)
        {
            PupilLeavingOff objPLO = PupilLeavingOffBusiness.Find(id);
            PupilProfile objPF = PupilProfileBusiness.Find(objPLO.PupilID);
            this.PupilLeavingOffBusiness.DeletePupilLeavingOff(_globalInfo.UserAccountID, id, _globalInfo.SchoolID.Value,semester);
            this.PupilLeavingOffBusiness.Save();
            //luu log
            ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.ObjectID] = id;
            ViewData[CommonKey.AuditActionKey.Description] = "Cập nhật hồ sơ";
            ViewData[CommonKey.AuditActionKey.Parameter] = id;
            ViewData[CommonKey.AuditActionKey.userAction] = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE;
            ViewData[CommonKey.AuditActionKey.userFunction] = "Hồ sơ học sinh";
            ViewData[CommonKey.AuditActionKey.userDescription] = "Hủy thôi học cho HS " + objPF.FullName + ", mã " + objPF.PupilCode;
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PupilLeavingOffViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int Grade = (int)_globalInfo.AppliedLevel.Value;
            IQueryable<PupilLeavingOff> query = this.PupilLeavingOffBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(o => o.EducationLevel.Grade == Grade);
            List<PupilLeavingOffViewModel> lst = query.Select(o => new PupilLeavingOffViewModel
            {
                PupilLeavingOffID = o.PupilLeavingOffID,
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                LeavingReasonID = o.LeavingReasonID,
                Semester = o.Semester,
                LeavingDate = o.LeavingDate,
                Description = o.Description,
                PupilCode = o.PupilProfile.PupilCode,
                Name = o.PupilProfile.Name,
                FullName = o.PupilProfile.FullName,
                DisplayName = o.ClassProfile.DisplayName,
                DisplayTile = o.AcademicYear.DisplayTitle,
                LeavingReason = o.LeavingReason.Resolution
            }).ToList();     
            foreach (var item in lst)
	        {
		    PupilOfClass poc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value,new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID}
                ,{"PupilID",item.PupilID}
                ,{"ClassID",item.ClassID}
            }).FirstOrDefault();
                item.OrderInClass = poc!=null?poc.OrderInClass:null;
	        }
            lst = lst.OrderBy(o => o.Name).ThenBy(o => o.FullName).ThenBy(o=>o.OrderInClass).ToList();
            return lst;
        }

        #region Load Combobox


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? educationLevelID)
        {
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (educationLevelID.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = educationLevelID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                if (_globalInfo.IsSchoolRole == false)
                {
                    dic["EmployeeID"] = _globalInfo.EmployeeID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                }
                lst = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o=>o.DisplayName);
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPupil(int? classid)
        {
            List<PupilOfClass> lst = new List<PupilOfClass>();
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            if (classid.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = classid;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["Status"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                //if (_globalInfo.IsSchoolRole == false)
                //{ 
                //    dic["EmployeeID"] = _globalInfo.EmployeeID;
                //    dic["Type"] = 1;
                //}                
                lst = this.PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(o => o.PupilProfile.IsActive == true).ToList();
                List<PupilOfClass> lst1 = lst.Where(o => o.OrderInClass == null).OrderBy(u => u.PupilProfile.Name).ThenBy(v => v.PupilProfile.FullName).ToList();
                List<PupilOfClass> lst2 = lst.Where(o => o.OrderInClass != null).OrderBy(u => u.OrderInClass).ToList();
                lst1.AddRange(lst2);
               
                foreach (var pop in lst1)
                {
                    PupilProfile PupilProfile = new PupilProfile();
                    PupilProfile.PupilProfileID = pop.PupilID;
                    PupilProfile.FullName = pop.PupilProfile.FullName;                   
                    LstPupilProfile.Add(PupilProfile);
                }
                
            }
            return Json(new SelectList(LstPupilProfile, "PupilProfileID", "FullName"));
        }
        #endregion
    }
}





