using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.PupilLeavingOffArea.Models
{
    public class PupilLeavingOffEditModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PupilLeavingOffID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_EducationLevel)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassOfEducationLevel(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Class)]
        [AdditionalMetadata("OnChange", "AjaxLoadPupilOfClass(this, this.options[this.selectedIndex].value);")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Pupil)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Semester)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 Semester { get; set; }

        //[ResDisplayName("PupilLeavingOff_Label_LeavingDate")]
        //[UIHint("DateTimePicker")]     
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]      
        //public DateTime? LeavingDate { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingDate")]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = "Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? LeavingDate1 { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingReason")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_LeavingReason)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 LeavingReasonID { get; set; }

        //[ResDisplayName("PupilLeavingOff_Label_Description")]
        //[StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //[DataType(DataType.MultilineText)]
        //public System.String Description { get; set; }



        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_School")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_AcademicYear")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 AcademicYearID { get; set; }
    }
}
