/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.PupilLeavingOffArea.Models
{
    public class PupilLeavingOffViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 PupilLeavingOffID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_EducationLevel)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassOfEducationLevel(this, this.options[this.selectedIndex].value);")]        
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Class")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Class)]
        [AdditionalMetadata("OnChange", "AjaxLoadPupilOfClass(this, this.options[this.selectedIndex].value);")]        
        public System.Int32 ClassID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Pupil")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Pupil)]        
        public System.Int32 PupilID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_Semester)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 Semester { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = "Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [UIHint("DateTimePicker")]  
        public DateTime? LeavingDate { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_LeavingReason")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilLeavingOffConstants.LIST_LeavingReason)]       
        public System.Int32 LeavingReasonID { get; set; }

        [ResourceDisplayName("PupilLeavingOff_Label_Description")]
        [StringLength(256, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }



        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_School")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 SchoolID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_AcademicYear")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 AcademicYearID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_Fullname")]
        public System.String FullName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_Name")]
        public System.String Name { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_PupilCode")]
        public System.String PupilCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_DisplayName")]
        public System.String DisplayName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_DisplayTile")]
        public System.String DisplayTile { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("PupilLeavingOff_Label_LeavingReason")]
        public System.String LeavingReason { get; set; }


        [ScaffoldColumn(false)]
        public Nullable<int> OrderInClass { get; set; }
    }
}


