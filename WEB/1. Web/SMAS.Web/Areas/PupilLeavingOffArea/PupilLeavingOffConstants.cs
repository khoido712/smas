/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilLeavingOffArea
{
    public class PupilLeavingOffConstants
    {
        public const string LIST_PUPILLEAVINGOFF = "listPupilLeavingOff";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_Class = "LIST_Class";
        public const string LIST_Pupil = "LIST_Pupil";
        public const string LIST_Semester = "LIST_Semester";
        public const string LIST_LeavingReason = "LIST_LeavingReason";
        public const string LST_SEMESTER = "LST_SEMESTER";
        public const string Disable_AddNew = "Disable_AddNew";         
        

    }
}