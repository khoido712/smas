﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilLeavingOffArea
{
    public class PupilLeavingOffAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilLeavingOffArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilLeavingOffArea_default",
                "PupilLeavingOffArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
