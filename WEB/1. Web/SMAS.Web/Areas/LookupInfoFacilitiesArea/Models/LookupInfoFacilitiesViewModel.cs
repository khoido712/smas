﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.LookupInfoFacilitiesArea.Models
{
    public class LookupInfoFacilitiesViewModel
    {
        #region Fields on Grid 
        public int SchoolID { get; set; }
        [ResourceDisplayName("LookupInfo_Label_School")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_District")]
        public string DistrictName { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_RoomHBM")]
        public int NumOfDeptClassroom { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_PublicHouse")]
        public int NumOfPublicHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolBoardHouse")]
        public int NumOfSchoolBoardHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_BoardingHouse")]
        public int NumOfBoardingHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_Library")]
        public string LibraryStatus { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_OnewayKitchen")]
        public string KitchenStatus { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolGate")]
        public string SchoolGateStatus { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolFence")]
        public string SchoolFenceStatus { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_TheWater")]
        public string WaterSourceStatus { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_Restroom")]
        public int StandardRestroom { get; set; }
        public int NotStandardRestroom { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_ShortlivedRoom")]
        public int NumOfShortlivedRoom { get; set; }
        [ResourceDisplayName("LookupIInfo_Facilities_ThreeShiftsRoom")]
        public int NumOfThreeShiftsRoom { get; set; }
        #endregion

        #region Fields to Export excel
        public int AcademicYear { get; set; }
        public int DataSemester { get; set; }
        public int Province { get; set; }
        public int? District { get; set; }
        public int? EducationGrade { get; set; }
        public int? School { get; set; }
        public int? DeptClassroom { get; set; }
        public int? PublicHouse { get; set; }
        public int? SchoolBoardHouse { get; set; }
        public int? BoardingHouse { get; set; }
        public int? Library { get; set; }
        public int? Kitchen { get; set; }
        public int? SchoolGate { get; set; }
        public int? SchoolFence { get; set; }
        public int? WaterSource { get; set; }
        public int? Restroom { get; set; }
        public int? ShortlivedRoom { get; set; }
        public int? ThreeShiftsRoom { get; set; }
        #endregion
    }
}