using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.LookupInfoFacilitiesArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_ACADEMICYEAR)]
        [AdditionalMetadata("Placeholder", "null")]
        public int AcademicYear { get; set; }


        [ResourceDisplayName("LookupIInfo_Facilities_DataSemester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_DATA_SEMESTER)]
        [AdditionalMetadata("Placeholder", "null")]
        public int DataSemester { get; set; }


        [ResourceDisplayName("LookupIInfo_Facilities_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSchool(this)")]
        public int? District { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_EDUCATION_GRADE)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSchool(this)")]
        public int? EducationGrade { get; set; }

        [ScaffoldColumn(false)]
        public string SchoolName { get; set; }

        [ResourceDisplayName("Employee_Label_School")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_SCHOOL)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? School { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_RoomHBM")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_DEPT_CLASSROOM)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? DeptClassroom { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_PublicHouse")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_PUBLIC_HOUSE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? PublicHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolBoardHouse")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_BOARDSCHOOL_HOUSE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? SchoolBoardHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_BoardingHouse")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_BOARDING_HOUSE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? BoardingHouse { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_Library")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_LIBRARY)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Library { get; set; }


        [ResourceDisplayName("LookupIInfo_Facilities_Kitchen")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_KITCHEN)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Kitchen { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolGate")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_SCHOOL_GATE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? SchoolGate { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_SchoolFence")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_SCHOOL_FENCE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? SchoolFence { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_TheWater")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_WATER_SOURCE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? WaterSource { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_Restroom")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_RESTROOM)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? Restroom { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_ShortlivedRoom")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_SHORTLIVED_ROOM)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ShortlivedRoom { get; set; }

        [ResourceDisplayName("LookupIInfo_Facilities_ThreeShiftsRoom")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookupInfoFacilitiesConstants.LIST_3SHIFT_ROOM)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ThreeShiftsRoom { get; set; }
      
    }
}
