﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.Web.Areas.LookupInfoFacilitiesArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Web.Areas.LookupInfoFacilitiesArea.Controllers
{

    public class LookupInfoFacilitiesController : BaseController
    {
        #region Khởi tạo biến
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IIndicatorBusiness IndicatorBusiness;
        private readonly IIndicatorDataBusiness IndicatorDataBusiness;
        public LookupInfoFacilitiesController(IDistrictBusiness DistrictBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IIndicatorBusiness IndicatorBusiness,
            IIndicatorDataBusiness IndicatorDataBusiness
            )
        {
            this.DistrictBusiness = DistrictBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.IndicatorBusiness = IndicatorBusiness;
            this.IndicatorDataBusiness = IndicatorDataBusiness;
        }
        #endregion
        //
        // GET: /LookupInfoFacilitiesArea/LookupInfoFacilities/
        #region Trang Index
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        #endregion

        #region Load dữ liệu đầu vào
        /// <summary>
        /// Lấy thông tin chung
        /// </summary>
        public void SetViewData()
        {
            // Năm học
            List<int> lstYear = AcademicYearBusiness.GetListYearForSupervisingDept(_globalInfo.SupervisingDeptID.Value);
            List<ComboObject> lstCbYear = new List<ComboObject>();
            if (lstYear != null && lstYear.Count > 0)
            {
                foreach (var year in lstYear)
                {
                    string value = year.ToString() + "-" + (year + 1).ToString();
                    ComboObject cb = new ComboObject(year.ToString(), value);
                    lstCbYear.Add(cb);
                }
            }
            ViewData[LookupInfoFacilitiesConstants.LIST_ACADEMICYEAR] = new SelectList(lstCbYear, "key", "value");
            // Kỳ lấy số liệu
            List<ComboObject> lstSemData = new List<ComboObject>();
            lstSemData.Add(new ComboObject("1", "Đầu năm"));
            lstSemData.Add(new ComboObject("2", "Giữa năm"));
            lstSemData.Add(new ComboObject("3", "Cuối năm"));
            ViewData[LookupInfoFacilitiesConstants.LIST_DATA_SEMESTER] = new SelectList(lstSemData, "key", "value");
            // Huyện
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID", _globalInfo.ProvinceID}
                };
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);
                ViewData[LookupInfoFacilitiesConstants.LIST_DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
                // Cấp trường
                ViewData[LookupInfoFacilitiesConstants.LIST_EDUCATION_GRADE] = new SelectList(CommonList.AppliedLevelForProvince(), "key", "value");
                // Danh sách trường
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                ViewData[LookupInfoFacilitiesConstants.LIST_SCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                District d = DistrictBusiness.Find(_globalInfo.DistrictID.Value);
                List<District> lst = new List<District>();
                lst.Add(d);
                ViewData[LookupInfoFacilitiesConstants.LIST_DISTRICT] = new SelectList(lst, "DistrictID", "DistrictName");
                // Cấp trường
                ViewData[LookupInfoFacilitiesConstants.LIST_EDUCATION_GRADE] = new SelectList(CommonList.AppliedLevelForDistrict(), "key", "value");
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", _globalInfo.SupervisingDeptID}
                };
                // Danh sách trường
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                ViewData[LookupInfoFacilitiesConstants.LIST_SCHOOL] = new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName");
            }
            
            // Trường
            // Phòng học bộ môn
            ViewData[LookupInfoFacilitiesConstants.LIST_DEPT_CLASSROOM] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Nhà công vụ
            ViewData[LookupInfoFacilitiesConstants.LIST_PUBLIC_HOUSE] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Nhà BGH
            ViewData[LookupInfoFacilitiesConstants.LIST_BOARDSCHOOL_HOUSE] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Nhà nội trú cho HS
            ViewData[LookupInfoFacilitiesConstants.LIST_BOARDING_HOUSE] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Thư viện
            List<ComboObject> lstLibraryStatus = new List<ComboObject>();
            lstLibraryStatus.Add(new ComboObject("1", "Không đạt"));
            lstLibraryStatus.Add(new ComboObject("2", "Đạt chuẩn"));
            lstLibraryStatus.Add(new ComboObject("3", "Tiên tiến"));
            lstLibraryStatus.Add(new ComboObject("4", "Xuất sắc"));
            ViewData[LookupInfoFacilitiesConstants.LIST_LIBRARY] = new SelectList(lstLibraryStatus, "key", "value");
            // Bếp ăn
            ViewData[LookupInfoFacilitiesConstants.LIST_KITCHEN] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Cổng trường
            ViewData[LookupInfoFacilitiesConstants.LIST_SCHOOL_GATE] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Hàng rào
            List<ComboObject> lstFenceStatus = new List<ComboObject>();
            lstFenceStatus.Add(new ComboObject("1", "Xây"));
            lstFenceStatus.Add(new ComboObject("2", "Kẽm lưới"));
            lstFenceStatus.Add(new ComboObject("3", "Cây xanh"));
            ViewData[LookupInfoFacilitiesConstants.LIST_SCHOOL_FENCE] = new SelectList(lstFenceStatus, "key", "value");
            // Nguồn nước
            List<ComboObject> lstWaterSource = new List<ComboObject>();
            lstWaterSource.Add(new ComboObject("1", "Nước máy"));
            lstWaterSource.Add(new ComboObject("2", "Giếng khoan/đào"));
            lstWaterSource.Add(new ComboObject("3", "Sông/Suối"));
            lstWaterSource.Add(new ComboObject("4", "Nước mưa"));
            lstWaterSource.Add(new ComboObject("5", "Ao/hồ"));
            ViewData[LookupInfoFacilitiesConstants.LIST_WATER_SOURCE] = new SelectList(lstWaterSource, "key", "value");
            // Nhà vệ sinh
            List<ComboObject> lstWCStatus = new List<ComboObject>();
            lstWCStatus.Add(new ComboObject("0", "Không có"));
            lstWCStatus.Add(new ComboObject("1", "Đạt chuẩn"));
            lstWCStatus.Add(new ComboObject("2", "Chưa đạt chuẩn"));
            ViewData[LookupInfoFacilitiesConstants.LIST_RESTROOM] = new SelectList(lstWCStatus, "key", "value");
            // Phòng học nhờ
            ViewData[LookupInfoFacilitiesConstants.LIST_SHORTLIVED_ROOM] = new SelectList(CommonList.StatusYesNo(), "key", "value");
            // Phòng học 3 ca
            ViewData[LookupInfoFacilitiesConstants.LIST_3SHIFT_ROOM] = new SelectList(CommonList.StatusYesNo(), "key", "value");
        }
        #endregion

        #region Load Combo Box
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSchool(int? EducationGradeID, int? DistrictID)
        {
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID", _globalInfo.ProvinceID},
                    {"DistrictID", DistrictID},
                    {"AppliedLevel", EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() > 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
                int SupervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", SupervisingDeptID},
                    {"AppliedLevel", EducationGradeID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(dic).OrderBy(o => o.SchoolName);
                if (lstSP.Count() > 0)
                {
                    return Json(new SelectList(lstSP.ToList(), "SchoolProfileID", "SchoolName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
        }
        #endregion

        #region Search information facilities
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            var dataSearch = this.SearchData(frm);
            #region Lấy dữ liệu cộng dồn từ CSVC MN
            if (dataSearch != null && dataSearch.Count > 0 && (frm.EducationGrade == null || frm.EducationGrade == 0 || frm.EducationGrade > 3)) 
            {
                var isInteger = false;
                var iresult = 0;
                var listIndicatorData = this.GetIndicatorDataPrimaryLevel(frm);
                if (listIndicatorData.Any()) 
                {
                    foreach (var objResult in dataSearch)
                    {
                        var listIndicatorBySchool = listIndicatorData.Where(x => x.SchoolID == objResult.SchoolID).ToList();
                        var objPublicHouse = listIndicatorBySchool.Where(x => x.CellAddress == "B55").FirstOrDefault();
                        var objKitchen1Way = listIndicatorBySchool.Where(x => x.CellAddress == "B113").FirstOrDefault();
                        var objSchoolGate = listIndicatorBySchool.Where(x => x.CellAddress == "B114").FirstOrDefault();
                        var objFency = listIndicatorBySchool.Where(x => x.CellAddress == "B118").FirstOrDefault();
                        var objWaterSource = listIndicatorBySchool.Where(x => x.CellAddress == "B110").FirstOrDefault();
                        var listStandardRestRoom = listIndicatorBySchool.Where(x => x.CellAddress == "E105" || x.CellAddress == "F105" || x.CellAddress == "G105").ToList();
                        var listNotStandardRestRoom = listIndicatorBySchool.Where(x => x.CellAddress == "E106" || x.CellAddress == "F106" || x.CellAddress == "G106").ToList();
                        var objSortedRoom = listIndicatorBySchool.Where(x => x.CellAddress == "F67").FirstOrDefault();
                        var objRoom3Shift = listIndicatorBySchool.Where(x => x.CellAddress == "F68").FirstOrDefault();
                        if (objPublicHouse != null && (frm.PublicHouse == null || frm.PublicHouse == 1))
                        {
                            isInteger = Int32.TryParse(objPublicHouse.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfPublicHouse += iresult;
                            }
                        }
                        if (objKitchen1Way != null && (frm.Kitchen == null || frm.Kitchen == 1))
                        {
                            isInteger = Int32.TryParse(objKitchen1Way.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.KitchenStatus = "Có";
                            }
                        }
                        if (objSchoolGate != null && (frm.SchoolGate == null || frm.SchoolGate == 1))
                        {
                            isInteger = Int32.TryParse(objSchoolGate.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.SchoolGateStatus = "Có";
                            }
                        }
                        if (objFency != null && !string.IsNullOrEmpty(objFency.CellValue))
                        {
                            if ((frm.SchoolFence == null || frm.SchoolFence == -1) || frm.SchoolFence == int.Parse(objFency.CellValue)) 
                            {
                                var FenceStatus = objFency.CellValue == "1" ? "Xây" : (objFency.CellValue == "2" ? "Kẽm lưới" : (objFency.CellValue == "3" ? "Cây xanh" : ""));
                                if (!string.IsNullOrEmpty(FenceStatus))
                                {
                                    objResult.SchoolFenceStatus = !string.IsNullOrEmpty(objResult.SchoolFenceStatus) ? objResult.SchoolFenceStatus + ", " + FenceStatus : FenceStatus;
                                }
                            }
                        }
                        if (objWaterSource != null && !string.IsNullOrEmpty(objWaterSource.CellValue))
                        {
                            if ((frm.WaterSource == null || frm.WaterSource == -1) || frm.WaterSource == int.Parse(objWaterSource.CellValue))
                            {
                                var WaterSource = objWaterSource.CellValue == "1" ? "Nước máy" : (objWaterSource.CellValue == "2" ? "Giếng khoan/đào" : (objWaterSource.CellValue == "3" ? "Sông/suối" : (objWaterSource.CellValue == "4" ? "Nước mưa" : (objWaterSource.CellValue == "5" ? "Ao/hồ" : ""))));
                                if (!string.IsNullOrEmpty(WaterSource))
                                {
                                    objResult.WaterSourceStatus = !string.IsNullOrEmpty(objResult.WaterSourceStatus) ? objResult.WaterSourceStatus + ", " + WaterSource : WaterSource;
                                }
                            }
                            
                        }
                        if (listStandardRestRoom != null && listStandardRestRoom.Count > 0 && (frm.Restroom == null || frm.Restroom == 1)) 
                        {
                            var totalStandardRestRoom = 0;
                            foreach (var room in listStandardRestRoom)
                            {
                                isInteger = Int32.TryParse(room.CellValue, out iresult);
                                if (isInteger && iresult > 0)
                                {
                                    totalStandardRestRoom += iresult;
                                }
                            }
                            objResult.StandardRestroom = objResult.StandardRestroom + totalStandardRestRoom;
                        }
                        if (listNotStandardRestRoom != null && listNotStandardRestRoom.Count > 0 && (frm.Restroom == null || frm.Restroom == 2))
                        {
                            var totalNotStandardRestRoom = 0;
                            foreach (var room in listNotStandardRestRoom)
                            {
                                isInteger = Int32.TryParse(room.CellValue, out iresult);
                                if (isInteger && iresult > 0)
                                {
                                    totalNotStandardRestRoom += iresult;
                                }
                            }
                            objResult.NotStandardRestroom = objResult.NotStandardRestroom + totalNotStandardRestRoom;
                        }
                        if (objSortedRoom != null && (frm.ShortlivedRoom == null || frm.ShortlivedRoom == 1))
                        {
                            isInteger = Int32.TryParse(objSortedRoom.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfShortlivedRoom = objResult.NumOfShortlivedRoom != null ? objResult.NumOfShortlivedRoom + iresult : iresult;
                            }
                        }
                        if (objRoom3Shift != null && (frm.ThreeShiftsRoom == null || frm.ThreeShiftsRoom == 1))
                        {
                            isInteger = Int32.TryParse(objRoom3Shift.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfThreeShiftsRoom = objResult.NumOfThreeShiftsRoom != null ? objResult.NumOfThreeShiftsRoom + iresult : iresult;
                            }
                        }
                    }               
                }
            }
            #endregion
            ViewData[LookupInfoFacilitiesConstants.LIST_LOOKUP_FACILITIES] = dataSearch;
            LookupInfoFacilitiesViewModel model = new LookupInfoFacilitiesViewModel();
            Utils.Utils.BindTo(frm, model);
            return PartialView("_List", model);
        }

        private List<LookupInfoFacilitiesViewModel> SearchData(SearchViewModel frm)
        {
            int year = frm.AcademicYear;
            int periodID = frm.DataSemester;
            int provinceID = _globalInfo.ProvinceID.Value;
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["Year"] = year;
            search["DataSemester"] = periodID;
            search["ProvinceID"] = provinceID;
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                search["DistrictID"] = _globalInfo.DistrictID;
                search["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            }
            else
            {
                search["DistrictID"] = frm.District;
            }
                
            search["AppliedLevel"] = frm.EducationGrade;
            search["SchoolID"] = frm.School;

            search["DeptClassRoom"] = frm.DeptClassroom.HasValue ? frm.DeptClassroom.Value : -1;
            search["PublicHouse"] = frm.PublicHouse.HasValue ? frm.PublicHouse.Value : -1;
            search["SchoolBoardHouse"] = frm.SchoolBoardHouse.HasValue ? frm.SchoolBoardHouse.Value : -1;
            search["BoardingHouse"] = frm.BoardingHouse.HasValue ? frm.BoardingHouse.Value : -1;
            search["Library"] = frm.Library;
            search["Kitchen"] = frm.Kitchen.HasValue ? frm.Kitchen.Value : -1;
            search["SchoolGate"] = frm.SchoolGate.HasValue ? frm.SchoolGate.Value : -1;
            search["SchoolFence"] = frm.SchoolFence;
            search["WaterSource"] = frm.WaterSource;
            search["RestRoom"] = frm.Restroom.HasValue ? frm.Restroom.Value : -1;
            search["ShortlivedRoom"] = frm.ShortlivedRoom.HasValue ? frm.ShortlivedRoom.Value : -1;
            search["ThreeShiftsRoom"] = frm.ThreeShiftsRoom.HasValue ? frm.ThreeShiftsRoom.Value : -1;

            return IndicatorDataBusiness.LookupFacilitiesData(search).Select(o => new LookupInfoFacilitiesViewModel
                    {
                        AcademicYear = year,
                        DataSemester = periodID,
                        SchoolID = o.SchoolID,
                        SchoolName = o.SchoolName,
                        DistrictName = o.DistrictName,
                        NumOfDeptClassroom = o.NumOfDeptClassroom,
                        NumOfPublicHouse = o.NumOfPublicHouse,
                        NumOfSchoolBoardHouse = o.NumOfSchoolBoardHouse,
                        NumOfBoardingHouse = o.NumOfBoardingHouse,
                        LibraryStatus = o.LibraryStatus == 1 ? "Không đạt" : (o.LibraryStatus == 2 ? "Đạt chuẩn" : (o.LibraryStatus == 3 ? "Tiên tiến" : (o.LibraryStatus == 4 ? "Xuất sắc" : ""))),
                        KitchenStatus = o.OnewayKitchenStatus == 1 ? "Có" : "Không",
                        SchoolGateStatus = o.SchoolGateStatus == 1 ? "Có" : "Không",
                        SchoolFenceStatus = o.SchoolFence == 1 ? "Xây" : (o.SchoolFence == 2 ? "Kẽm lưới" : (o.SchoolFence == 3 ? "Cây xanh" :"")),
                        WaterSourceStatus = o.WaterSource == 1 ? "Nước máy" : (o.WaterSource == 2 ? "Giếng khoan/đào" : (o.WaterSource == 3 ? "Sông/suối" : (o.WaterSource == 4 ? "Nước mưa" :  (o.WaterSource == 5 ? "Ao/hồ" : "")))),
                        StandardRestroom = o.NumOfStandardWC,
                        NotStandardRestroom = o.NumOfNotStandardWC,
                        NumOfShortlivedRoom = o.NumOfShortlivedRoom,
                        NumOfThreeShiftsRoom = o.NumOfThreeShiftsRoom,
                        EducationGrade = frm.EducationGrade
                    }).ToList();
        }
        #endregion

        #region Report
        /// <summary>
        /// Xuất thông tin CSVC của từng trường
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="periodId"></param>
        /// <returns></returns>
        public FileResult ExportFacilitiesSchool(int SchoolID, int Year, int PeriodID, int? AppliedLevel)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            // Thông tin Trường/Kỳ số liệu
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            sheet.SetCellValue("A2", sp.SchoolName.ToUpper());
            sheet.SetCellValue("A4", string.Format("Kỳ {0} - Năm học {1}-{2}", CommonConvert.GetNameByPeriodId(PeriodID), Year, Year + 1));
            
            List<PhysicalFacilitiesBO> listResult = new List<PhysicalFacilitiesBO>();
            if (PeriodID > 0)
            {
                listResult = IndicatorDataBusiness.GetListIndicatorData(_globalInfo.ProvinceID.Value, PeriodID, SchoolID, Year);
            }

            int iresult = 0;
            bool isInteger = true;
            for (int i = 0; i < listResult.Count; i++)
            {
                if (!string.IsNullOrEmpty(listResult[i].CellValue))
                {
                    isInteger = Int32.TryParse(listResult[i].CellValue, out iresult);
                    if (isInteger) sheet.SetCellValue(listResult[i].CellAddress, iresult);
                    else sheet.SetCellValue(listResult[i].CellAddress, listResult[i].CellValue);
                }
            }
            ////Get data Facilities Primary
            //if (AppliedLevel != null && (AppliedLevel == 0 || AppliedLevel > 3))
            //{
                var oBookPrimary = this.GetDataForFacilitiesPrimarySheet(Year, PeriodID, 4, SchoolID);
                if (oBookPrimary != null)
                {
                    IVTWorksheet PrimarySheet = oBookPrimary.GetSheet(1);
                    var copySheetData = oBook.CopySheetToLast(PrimarySheet, "A1");
                    copySheetData.Name = "CSVC_MN";
                    copySheetData.CopySheet(PrimarySheet);
                    //if (AppliedLevel > 3)
                    //{
                    //    string fileNamePri = string.Format("Truong_BCCSVC_MN_{0}.xls", Utils.Utils.StripVNSignAndSpace(CommonConvert.GetNameByPeriodId(PeriodID)));
                    //    Stream excelPri = oBookPrimary.ToStream();
                    //    FileStreamResult resultPri = new FileStreamResult(excelPri, "application/octet-stream");
                    //    resultPri.FileDownloadName = fileNamePri;
                    //    return resultPri;
                    //}
                }
            //}
            
            string fileName = string.Format("Truong_BCCSVC_{0}.xls", Utils.Utils.StripVNSignAndSpace(CommonConvert.GetNameByPeriodId(PeriodID)));
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        /// <summary>
        /// Get Data For Facilities Primary Sheet
        /// </summary>
        /// <param name="PeriodId"></param>
        /// <param name="LevelApplied"></param>
        /// <returns></returns>
        public IVTWorkbook GetDataForFacilitiesPrimarySheet(int Year,int PeriodId, int LevelApplied, int SchoolID) 
        {
            var periodname = CommonConvert.GetNameByPeriodId(PeriodId);
            var templateName = SystemParamsInFile.TEMPLATE_NAME_PHYCICAL_FACILITIES_PRIMARY;
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, SystemParamsInFile.FOLDER_TRUONG, templateName);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            List<PhysicalFacilitiesBO> listResult = new List<PhysicalFacilitiesBO>();
            if (PeriodId > 0)
            {
                listResult = IndicatorDataBusiness.GetListIndicatorData(_globalInfo.ProvinceID.Value, PeriodId, SchoolID, Year, LevelApplied);
            }
            //if (listResult == null || listResult.Count == 0)
            //    return null;
            #region Fill thong tin chung
            //Tieu de bao cao
            string title = string.Format(Res.Get("Lbl_Title_Report_Physical_Facilites"), periodname, Year + "-" + (Year + 1));            
            sheet.SetCellValue("A4", title);
            //Ten truong
            sheet.SetCellValue("A2", _globalInfo.SchoolName.ToUpper());
            #endregion

            int iresult = 0;
            bool isInteger = true;
            for (int i = 0; i < listResult.Count; i++)
            {
                if (!string.IsNullOrEmpty(listResult[i].CellValue))
                {
                    isInteger = Int32.TryParse(listResult[i].CellValue, out iresult);
                    if (isInteger)
                    {
                        sheet.SetCellValue(listResult[i].CellAddress, Int32.Parse(listResult[i].CellValue));
                    }
                    else
                    {
                        sheet.SetCellValue(listResult[i].CellAddress, null);
                    }
                }
                else
                {
                    sheet.SetCellValue(listResult[i].CellAddress, null);
                }
            }
            return oBook;
        
        }
        /// <summary>
        /// Xuất thống kê CSVC toàn trường của đơn vị Phòng/Sở đang quản lý
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public FileResult ExportFile(LookupInfoFacilitiesViewModel frm)
        {
            SearchViewModel search = new SearchViewModel();
            Utils.Utils.BindTo(frm, search);
            List<LookupInfoFacilitiesViewModel> LstResult = this.SearchData(search);
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", "PS_BCSolieuCSVC.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            // Phòng/Sở
            SupervisingDept sp = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
            sheet.SetCellValue("A2", sp.SupervisingDeptName.ToUpper());
            sheet.SetCellValue("A6", string.Format("Kỳ số liệu {0} - Năm học {1}-{2}", CommonConvert.GetNameByPeriodId(frm.DataSemester), frm.AcademicYear, frm.AcademicYear + 1));
            int startRow = 11;
            var sourceRange = sheet.GetRange(10, 1, 10, 13);
            LookupInfoFacilitiesViewModel item;
            if (LstResult != null && LstResult.Count > 0 && (search.EducationGrade == null || search.EducationGrade == 0 || search.EducationGrade > 3)) 
            {
                var isInteger = false;
                var iresult = 0;
                var listIndicatorData = this.GetIndicatorDataPrimaryLevel(search);
                
                if (listIndicatorData.Any()) 
                {
                    foreach (var objResult in LstResult)
                    {
                        var listIndicatorBySchool = listIndicatorData.Where(x => x.SchoolID == objResult.SchoolID).ToList();
                        var objPublicHouse = listIndicatorBySchool.Where(x => x.CellAddress == "B55").FirstOrDefault();
                        var objKitchen1Way = listIndicatorBySchool.Where(x => x.CellAddress == "B113").FirstOrDefault();
                        var objSchoolGate = listIndicatorBySchool.Where(x => x.CellAddress == "B114").FirstOrDefault();
                        var objFency = listIndicatorBySchool.Where(x => x.CellAddress == "B118").FirstOrDefault();
                        var objWaterSource = listIndicatorBySchool.Where(x => x.CellAddress == "B110").FirstOrDefault();
                        var listStandardRestRoom = listIndicatorBySchool.Where(x => x.CellAddress == "E105" || x.CellAddress == "F105" || x.CellAddress == "G105").ToList();
                        var listNotStandardRestRoom = listIndicatorBySchool.Where(x => x.CellAddress == "E106" || x.CellAddress == "F106" || x.CellAddress == "G106").ToList();
                        var objSortedRoom = listIndicatorBySchool.Where(x => x.CellAddress == "F67").FirstOrDefault();
                        var objRoom3Shift = listIndicatorBySchool.Where(x => x.CellAddress == "F68").FirstOrDefault();
                        if (objPublicHouse != null && (search.PublicHouse == null || search.PublicHouse == 1))
                        {
                            isInteger = Int32.TryParse(objPublicHouse.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfPublicHouse += iresult;
                            }
                        }
                        if (objKitchen1Way != null && (search.Kitchen == null || search.Kitchen == 1))
                        {
                            isInteger = Int32.TryParse(objKitchen1Way.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.KitchenStatus = "Có";
                            }
                        }
                        if (objSchoolGate != null && (search.SchoolGate == null || search.SchoolGate == 1))
                        {
                            isInteger = Int32.TryParse(objSchoolGate.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.SchoolGateStatus = "Có";
                            }
                        }
                        if (objFency != null && !string.IsNullOrEmpty(objFency.CellValue))
                        {
                            if ((search.SchoolFence == null || search.SchoolFence == -1) || search.SchoolFence == int.Parse(objFency.CellValue)) 
                            {
                                var FenceStatus = objFency.CellValue == "1" ? "Xây" : (objFency.CellValue == "2" ? "Kẽm lưới" : (objFency.CellValue == "3" ? "Cây xanh" : ""));
                                if (!string.IsNullOrEmpty(FenceStatus))
                                {
                                    objResult.SchoolFenceStatus = !string.IsNullOrEmpty(objResult.SchoolFenceStatus) ? objResult.SchoolFenceStatus + ", " + FenceStatus : FenceStatus;
                                }
                            }
                        }
                        if (objWaterSource != null && !string.IsNullOrEmpty(objWaterSource.CellValue))
                        {
                            if ((search.WaterSource == null || search.WaterSource == -1) || search.WaterSource == int.Parse(objWaterSource.CellValue))
                            {
                                var WaterSource = objWaterSource.CellValue == "1" ? "Nước máy" : (objWaterSource.CellValue == "2" ? "Giếng khoan/đào" : (objWaterSource.CellValue == "3" ? "Sông/suối" : (objWaterSource.CellValue == "4" ? "Nước mưa" : (objWaterSource.CellValue == "5" ? "Ao/hồ" : ""))));
                                if (!string.IsNullOrEmpty(WaterSource))
                                {
                                    objResult.WaterSourceStatus = !string.IsNullOrEmpty(objResult.WaterSourceStatus) ? objResult.WaterSourceStatus + ", " + WaterSource : WaterSource;
                                }
                            }
                            
                        }
                        if (listStandardRestRoom != null && listStandardRestRoom.Count > 0 && (search.Restroom == null || search.Restroom == 1)) 
                        {
                            var totalStandardRestRoom = 0;
                            foreach (var room in listStandardRestRoom)
                            {
                                isInteger = Int32.TryParse(room.CellValue, out iresult);
                                if (isInteger && iresult > 0)
                                {
                                    totalStandardRestRoom += iresult;
                                }
                            }
                            objResult.StandardRestroom = objResult.StandardRestroom + totalStandardRestRoom;
                        }
                        if (listNotStandardRestRoom != null && listNotStandardRestRoom.Count > 0 && (search.Restroom == null || search.Restroom == 2))
                        {
                            var totalNotStandardRestRoom = 0;
                            foreach (var room in listNotStandardRestRoom)
                            {
                                isInteger = Int32.TryParse(room.CellValue, out iresult);
                                if (isInteger && iresult > 0)
                                {
                                    totalNotStandardRestRoom += iresult;
                                }
                            }
                            objResult.NotStandardRestroom = objResult.NotStandardRestroom + totalNotStandardRestRoom;
                        }
                        if (objSortedRoom != null && (frm.ShortlivedRoom == null || frm.ShortlivedRoom == 1))
                        {
                            isInteger = Int32.TryParse(objSortedRoom.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfShortlivedRoom = objResult.NumOfShortlivedRoom != null ? objResult.NumOfShortlivedRoom + iresult : iresult;
                            }
                        }
                        if (objRoom3Shift != null && (frm.ThreeShiftsRoom == null || frm.ThreeShiftsRoom == 1))
                        {
                            isInteger = Int32.TryParse(objRoom3Shift.CellValue, out iresult);
                            if (isInteger && iresult > 0)
                            {
                                objResult.NumOfThreeShiftsRoom = objResult.NumOfThreeShiftsRoom != null ? objResult.NumOfThreeShiftsRoom + iresult : iresult;
                            }
                        }
                    }               
                }
            }
            for (int i = 0, Count = LstResult.Count; i < Count; i++)
            {
                sheet.CopyAndInsertARow(sourceRange, startRow);
                item = LstResult[i];
                sheet.SetCellValue("A" + startRow, i + 1);
                sheet.SetCellValue("B" + startRow, item.SchoolName);
                sheet.SetCellValue("C" + startRow, item.DistrictName);
                sheet.SetCellValue("D" + startRow, item.NumOfDeptClassroom);
                sheet.SetCellValue("E" + startRow, item.NumOfPublicHouse);
                sheet.SetCellValue("F" + startRow, item.NumOfSchoolBoardHouse);
                sheet.SetCellValue("G" + startRow, item.NumOfBoardingHouse);

                sheet.SetCellValue("H" + startRow, item.LibraryStatus);
                sheet.SetCellValue("I" + startRow, item.KitchenStatus);
                sheet.SetCellValue("J" + startRow, item.SchoolGateStatus);
                sheet.SetCellValue("K" + startRow, item.SchoolFenceStatus);
                sheet.SetCellValue("L" + startRow, item.WaterSourceStatus);
                sheet.SetCellValue("M" + startRow, item.StandardRestroom);
                sheet.SetCellValue("N" + startRow, item.NotStandardRestroom);
                sheet.SetCellValue("O" + startRow, item.NumOfShortlivedRoom);
                sheet.SetCellValue("P" + startRow, item.NumOfThreeShiftsRoom);
                startRow++;
            }

            // xoá dòng 10
            sheet.DeleteRow(10);

            string fileName = string.Format("PS_BCSolieuCSVC_{0}.xls",  Utils.Utils.StripVNSignAndSpace(CommonConvert.GetNameByPeriodId(frm.DataSemester)));
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        public List<PhysicalFacilitiesBO> GetIndicatorDataPrimaryLevel(SearchViewModel frm)
        {
            int year = frm.AcademicYear;
            int periodID = frm.DataSemester;
            int provinceID = _globalInfo.ProvinceID.Value;
            
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["Year"] = year;
            search["DataSemester"] = periodID;
            search["ProvinceID"] = provinceID;
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                search["DistrictID"] = _globalInfo.DistrictID;
                search["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;
            }
            else
            {
                search["DistrictID"] = frm.District;
            }
            search["Period"] = periodID;
            var listIdiacatorData = IndicatorDataBusiness.SynthesizeDataPrimaryLevelforSupervisingDept(search);
            return listIdiacatorData;
        }
        #endregion
    }
}
