﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookupInfoFacilitiesArea
{
    public class LookupInfoFacilitiesConstants
    {
        public const int PAGE_SIZE = 15;

        public const string LIST_ACADEMICYEAR = "list academic year";
        public const string LIST_DATA_SEMESTER = "list data semester in year";
        public const string LIST_DISTRICT = "list district in province";
        public const string LIST_EDUCATION_GRADE = "list education grade";
        public const string LIST_SCHOOL = "list school with district and education grade";
        public const string LIST_DEPT_CLASSROOM = "list department classroom";
        public const string LIST_PUBLIC_HOUSE = "list public house";
        public const string LIST_BOARDSCHOOL_HOUSE = "list board school house";
        public const string LIST_BOARDING_HOUSE = "list boarding house for pupil";
        public const string LIST_LIBRARY = "list library";
        public const string LIST_KITCHEN = "list one way kitchen in school";
        public const string LIST_SCHOOL_GATE = "list all school gate";

        public const string LIST_SCHOOL_FENCE = "list type school fence";
        
        public const string LIST_WATER_SOURCE = "list water source using in school";
        public const string LIST_RESTROOM = "list restroom in school";

        public const string LIST_SHORTLIVED_ROOM = "list short lived room";
        public const string LIST_3SHIFT_ROOM = "list three shifts room";

        public const string LIST_LOOKUP_FACILITIES = "list record when lookup facilities";
        
        public const string PAGING = "PAGING";
    }
}