﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LookupInfoFacilitiesArea
{
    public class LookupInfoFacilitiesAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LookupInfoFacilitiesArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LookupInfoFacilitiesArea_default",
                "LookupInfoFacilitiesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
