﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PeriodDeclarationArea.Models
{
    public class PeriodDeclarationViewModel
    {
        public int PeriodDeclarationID { get; set; }

        /// <summary>
        /// ten dot
        /// </summary>
        public string Resolution { get; set; }

        /// <summary>
        /// tu ngay
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// den ngay
        /// </summary>
        public DateTime? ToDate { get; set; }

        public int? InterviewMark { get; set; }

        public int? StartIndexInterviewMark { get; set; }

        public int? WritingMark { get; set; }

        public int? StartIndexWritingMark { get; set; }

        public int? TwiceCoeffiecientMark { get; set; }

        public int? StartIndexTwiceCoeMark { get; set; }

        public bool? IsLock { get; set; }

        public bool? ContantSemesterMark { get; set; }

        public int? Semester { get; set; }

        public int AcademicYearID { get; set; }

        public int? SchoolId { get; set; }

        public string StrInterviewMark { get; set; }

        public string StrWritingMark { get; set; }

        public string StrTwiceMark { get; set; }

        public bool isCheckMark { get; set; }

        public  bool IsEditMark { get; set; }

    }
}