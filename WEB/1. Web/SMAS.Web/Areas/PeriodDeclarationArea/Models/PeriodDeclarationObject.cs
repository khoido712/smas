using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PeriodDeclarationArea.Models
{
    public class PeriodDeclarationObject
    {
        private const string ValidateDateTime = "Dữ liệu nhập vào không đúng định dạng ngày tháng";

        [ScaffoldColumn(false)]
        public int Index
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        public int PeriodDeclarationID
        {
            get;
            set;
        }

        //[UIHint("Semester"), Required]
        //[ResDisplayName("PeriodDeclaration_Label_Semester")]
        //public int? Semester
        //{
        //    get;
        //    set;
        //}

        [ResourceDisplayName("Tên đợt/Thời gian")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        //[UIHint("DateTimePicker")]
        //[StringLength(20, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Resolution
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
       // [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = ValidateDateTime)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        public DateTime? FromDate
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = ValidateDateTime)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        public DateTime? EndDate
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_InterviewMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotPositiveInteger")]
        [AdditionalMetadata("class", "integer")]
        public int? InterviewMark
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_WritingMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotPositiveInteger")]
        [AdditionalMetadata("class", "integer")]
        public int? WritingMark
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_TwiceCoefficientWritingMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotPositiveInteger")]
        [AdditionalMetadata("class", "integer")]
        public int? TwiceCoeffiecientMark
        {
            get;
            set;
        }

        public int? SchoolID
        {
            get;
            set;
        }

        public int AcademicYearID
        {
            get;
            set;
        }

        public int OrderNumber
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_Discontinued")]
        public bool IsLock
        {
            get;
            set;
        }

        [ResourceDisplayName("PeriodDeclaration_Label_ContaintSemesterMark")]
        public bool ContaintSemesterMark { get; set; }
    }
}
