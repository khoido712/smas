﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.PeriodDeclarationArea.Models
{
    public class CreateOrEditModel
    {
        public int PeriodDeclarationID { get; set; }
        public string PeriodDeclarationName { get; set; }

        [ResourceDisplayName("PeriodDeclaration_Label_FromDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataConstraint(DataConstraintAttribute.LESS, "EndDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime FromDate { get; set; }

        [ResourceDisplayName("PeriodDeclaration_Label_ToDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime EndDate { get; set; }
        public bool? chkLock { get; set; }
        public bool? chkKTHK { get; set; }
        public string strInterviewMark { get; set; }
        public string strWritingMark { get; set; }
        public string strTwiceMark { get; set; }
        public int SemesterID { get; set; }
        public int? InterviewMark { get; set; }
        public int? WritingMark { get; set; }
        public int? TwiceMark { get; set; }
        public  bool IsEditMark { get; set; }

      

    }
}