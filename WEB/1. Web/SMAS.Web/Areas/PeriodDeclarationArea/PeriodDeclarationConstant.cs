﻿namespace SMAS.Web.Areas.PeriodDeclarationArea
{
    public class PeriodDeclarationConstant
    {
        public const string LS_PERIODDECLARATION = "ListPeriodDeclaration";
        public const string LS_SEMESTER = "ListSemester";
        public const string LS_PERIODDECLARATIONHK2 = "LS_PERIODDECLARATIONHK2";
        public const string LIST_SEMESTER = "LIST_SEMESTER";

        public const string INTERVIEW_MARK = "InterviewMark";
        public const string WRITING_MARK = "WritingMark";
        public const string TWICE_COEFFIECIENT_MARK = "TwiceCoeffiecientMark";

        public const string STR_INTERVIEW_MARK_ALL = "strInterviewMarkAll";
        public const string STR_WRITING_MARK_ALL = "strWritingMarkAll";
        public const string STR_TWICE_MARK_ALL = "strTwiceMarkAll";
        public const string MESSAGE_NOTE = "Message_Note";

        public const string CHK_KTHK = "chk_kthk";

        public const string ACADEMIC_YEAR = "AcademicYear";


    }
}