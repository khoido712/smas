﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.PeriodDeclarationArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.PeriodDeclarationArea.Controllers
{
    public class PeriodDeclarationController : BaseController
    {
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IThreadMarkBusiness ThreadMarkBusiness;
        public PeriodDeclarationController(IPeriodDeclarationBusiness perioddeclarationBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , ISchoolSubjectBusiness SchoolSubjectBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , ILockedMarkDetailBusiness LockedMarkDetailBusiness
            , ISemeterDeclarationBusiness SemeterDeclarationBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISummedUpRecordBusiness SummedUpRecordBusiness
            , IJudgeRecordBusiness JudgeRecordBusiness
            , IMarkTypeBusiness MarkTypeBusiness
            , IVPupilRankingBusiness vPupilRankingBusiness
            , ISummedUpRecordClassBusiness summedUpRecordClassBusiness
            , IEducationLevelBusiness educationLevelBusiness
            , IThreadMarkBusiness threadMarkBusiness)
        {
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.PeriodDeclarationBusiness = perioddeclarationBusiness;
            this.LockedMarkDetailBusiness = LockedMarkDetailBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.VPupilRankingBusiness = vPupilRankingBusiness;
            this.SummedUpRecordClassBusiness = summedUpRecordClassBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ThreadMarkBusiness = threadMarkBusiness;
        }

        //
        // GET: /PeriodDeclaration/

        #region trang chu

        public ViewResult Index()
        {
            SetViewDataPermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            SetViewData();
            return View();
        }

        #endregion

        #region chuyển kiểu từ model sang view model

        public IQueryable<PeriodDeclarationObject> ConvertToObject(IQueryable<PeriodDeclaration> listConvert)
        {
            if (listConvert != null)
            {
                // Convert du lieu sang doi tuong
                IQueryable<PeriodDeclarationObject> res = listConvert.Select(o => new PeriodDeclarationObject
                {
                    InterviewMark = o.InterviewMark,
                    PeriodDeclarationID = o.PeriodDeclarationID,
                    EndDate = o.EndDate,
                    FromDate = o.FromDate,
                    Resolution = o.Resolution,
                    TwiceCoeffiecientMark = o.TwiceCoeffiecientMark,
                    WritingMark = o.WritingMark,
                    SchoolID = o.SchoolID,
                    AcademicYearID = o.AcademicYearID,
                    IsLock = o.IsLock.HasValue ? o.IsLock.Value : false
                    ,
                    ContaintSemesterMark = o.ContaintSemesterMark.HasValue ? o.ContaintSemesterMark.Value : false,
                });
                return res;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Nếu 1 đợt đã được khai báo cột điểm HK, đã nhập điểm và tổng kết thì không cho phép thêm đợt mới nữa
        private void CheckValidForInsert(int Semester)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            AcademicYear academicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID);
            //lay ra dot co diem hk,thuong moi ky chi co 1 dot,va la dot cuoi cung
            IQueryable<PeriodDeclaration> lsPDTestHKCheck = PeriodDeclarationBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",globalInfo.AcademicYearID.Value},
                    {"Semester",Semester}
                }).Where(o => o.ContaintSemesterMark == true);
            if (lsPDTestHKCheck.Count() > 0)        //khi co dot khai bao diem hk thi moi xet
            {
                int PeriodID = lsPDTestHKCheck.FirstOrDefault().PeriodDeclarationID;
                //diem nhan xet
                var lsJudge = JudgeRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"AcademicYearID",globalInfo.AcademicYearID}
                            ,{"Semester",Semester}
                            ,{"PeriodID",PeriodID}
                            ,{"Title","HK"}
                            ,{"Year", academicYear.Year}
                        });

                //mon tinh diem
                var lsMark = MarkRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"AcademicYearID",globalInfo.AcademicYearID}
                            ,{"Semester",Semester}
                            ,{"PeriodID",PeriodID}
                            ,{"Title","HK"}
                             ,{"Year", academicYear.Year}
                        });

                //diem tong ket
                var lsSummedUp = SummedUpRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"AcademicYearID",globalInfo.AcademicYearID}
                            ,{"Semester",Semester}
                            ,{"PeriodID",PeriodID}
                        });

                //nếu đã nhập điểm và tổng kết
                if (lsSummedUp.Count() > 0 && (lsJudge.Count() > 0 || lsMark.Count() > 0))
                {
                    throw new BusinessException("PeriodDeclaration_Label_CanNotInsertPeriod");
                }
            }
        }
        #endregion

        #region dot hk1
        #region _SelectAjaxEditing - Load du lieu cho grid hk1

        [GridAction]
        public ActionResult _SelectAjaxEditing()
        {
            SetViewData();

            IQueryable<PeriodDeclaration> listPeriod = (IQueryable<PeriodDeclaration>)ViewData[PeriodDeclarationConstant.LS_PERIODDECLARATION];

            // Convert du lieu sang doi tuong SupervisingDeptForm
            IQueryable<PeriodDeclarationObject> res = ConvertToObject(listPeriod);
            return View(new GridModel(res.ToList()));
        }

        #endregion _SelectAjaxEditing - Load du lieu cho grid hk1

        #region _SaveAjaxEditing - Update dot hk1

        [ValidateAntiForgeryToken]

        public JsonResult _EditAjaxEditing(CreateOrEditModel objModel)
        {
            int id = objModel.PeriodDeclarationID;
            if (id != 0)
            {
                if (GetMenupermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
            }
            else
            {
                if (GetMenupermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
            }

            CheckPermissionForAction(id, "PeriodDeclaration");
            if (!_globalInfo.IsCurrentYear)
            {
                throw new BusinessException("ClassAssigment_Error_IsCurrentYear");
            }
            //

            int? SchoolID = _globalInfo.SchoolID;
            int? AcademicYearID = _globalInfo.AcademicYearID;
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["AcademicYearID"] = AcademicYearID;
            dicSearch["Semester"] = objModel.SemesterID;
            //du lieu hop le thi convert data va insert
            PeriodDeclaration periodModel = PeriodDeclarationBusiness.Find(id);
            bool isPreLock = objModel.chkLock.HasValue && objModel.chkLock.Value;
            bool islockDB = periodModel.IsLock.HasValue && periodModel.IsLock.Value;
            bool isKTHK = objModel.chkKTHK.HasValue && objModel.chkKTHK.Value;
            bool isKTHKDB = periodModel.ContaintSemesterMark.HasValue && periodModel.ContaintSemesterMark.Value;

            //Kiem tra so con diem co bi thay doi hay khong
            bool checkChange = false;
            if ((!String.IsNullOrEmpty(periodModel.StrInterviewMark) && !periodModel.StrInterviewMark.Equals(objModel.strInterviewMark)) ||
                (!String.IsNullOrEmpty(periodModel.StrWritingMark) && !periodModel.StrWritingMark.Equals(objModel.strWritingMark)) ||
                (!String.IsNullOrEmpty(periodModel.StrTwiceCoeffiecientMark) && !periodModel.StrTwiceCoeffiecientMark.Equals(objModel.strTwiceMark)))
            {
                checkChange = true;
            }
            if (periodModel == null)
            {
                throw new BusinessException("Common_Validate_NotAvailable", new List<object>() { "PeriodDeclaration_Label_PeriodDeclarationID" });
            }
            //chi co 1 dot la duoc khai bao diem hoc ky
            if (objModel.chkKTHK.HasValue && objModel.chkKTHK.Value)
            {
                var temp = PeriodDeclarationBusiness.SearchBySchool(SchoolID.Value, dicSearch).ToList();
                var temp1 = temp.Where(o => o.PeriodDeclarationID != id).Where(o => o.ContaintSemesterMark.Value == true);
                if (temp1.Count() > 0)
                {
                    throw new BusinessException("PeriodDeclaration_Label_PeriodContaintSemesterMarkHK1");
                }
                //lay ra dot co ngay lon nhat
                var temp2 = temp.Where(o => o.PeriodDeclarationID != id).Max(o => o.EndDate);

                if (temp2 != null && DateTime.Compare(objModel.EndDate, temp2.Value) < 0)
                {
                    throw new BusinessException("PeriodDeclaration_Label_PeriodContaintSemesterMarkErrorDate");
                }
            }
            List<PeriodDeclaration> lsPD = PeriodDeclarationBusiness.SearchBySchool(SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_FIRST}
                }).Where(o => DateTime.Compare(o.FromDate.Value, objModel.FromDate) < 0).ToList();
            Utils.Utils.BindTo(objModel, periodModel);
            periodModel.Resolution = objModel.PeriodDeclarationName;
            periodModel.InterviewMark = objModel.InterviewMark;
            periodModel.WritingMark = objModel.WritingMark;
            periodModel.TwiceCoeffiecientMark = objModel.TwiceMark;
            periodModel.IsLock = isPreLock;
            periodModel.ContaintSemesterMark = objModel.chkKTHK.HasValue && objModel.chkKTHK.Value ? true : false;
            periodModel.Semester = objModel.SemesterID;
            periodModel.StrInterviewMark = objModel.strInterviewMark;
            periodModel.StrWritingMark = objModel.strWritingMark;
            periodModel.StrTwiceCoeffiecientMark = objModel.strTwiceMark;
            periodModel.EndDate = SMAS.Web.Utils.Utils.FixDateTime(objModel.EndDate);
            //08/01/2017: Bo sung cot cho phep giao vien nhap diem khi dot da ket thuc
            periodModel.IsEditMark = objModel.IsEditMark;
            this.PeriodDeclarationBusiness.Update(periodModel, checkChange);
            this.PeriodDeclarationBusiness.Save();

            //Bo sung nghiep vu 19/12/2013
            // Neu co su thay doi trang thai khoa moi thuc hien
            if (isPreLock != islockDB || checkChange)
            {
                LockMarkSemester(objModel.SemesterID, periodModel, checkChange, isPreLock);
            }

            if (isKTHK != isKTHKDB && checkChange)//insert ThreadMark de tinh lai TBM
            {
                this.InsertThreadMark(periodModel);
            }

            return Json(new JsonMessage("Cập nhật thành công", "success"));
        }

        #endregion _SaveAjaxEditing

        #region _DeleteAjaxEditing - xoa dot hk1
        [ValidateAntiForgeryToken]

        public JsonResult _DeleteAjaxEditing(int id)
        {
            if (GetMenupermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id, "PeriodDeclaration");

            if (!_globalInfo.IsCurrentYear)
            {
                throw new BusinessException("ClassAssigment_Error_IsCurrentYear");
            }

            //tim trong PeriodDeclairation xem co ClasssID tuong ung voi ClasssID cua object nay ko
            PeriodDeclaration PeriodDeclarationTest = PeriodDeclarationBusiness.Find(id);
            if (PeriodDeclarationTest != null)
            {
                PeriodDeclarationBusiness.Delete(PeriodDeclarationTest.PeriodDeclarationID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value);
                PeriodDeclarationBusiness.Save();
            }
            else
            {
                List<object> Params = new List<object>();
                Params.Add("PeriodDeclaration_Label_PeriodDeclarationID");
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
            return Json(new JsonMessage("Xóa thành công"));
        }

        #endregion _DeleteAjaxEditing

        #region _InsertAjaxEditing - them moi dot hk1

        [ValidateAntiForgeryToken]

        public JsonResult _InsertAjaxEditing(CreateOrEditModel objModel)
        {
            if (GetMenupermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }



            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSearch["Semester"] = objModel.SemesterID;
            IQueryable<PeriodDeclaration> lsPDTestHKCheck = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"Semester",objModel.SemesterID}
                }).Where(o => o.ContaintSemesterMark == true);
            if (lsPDTestHKCheck.Count() > 0)
            {
                throw new BusinessException("PeriodDeclaration_Label_HKCheck");
            }

            //	Nếu 1 đợt đã được khai báo cột điểm HK, đã nhập điểm và tổng kết thì không cho phép thêm đợt mới nữa
            CheckValidForInsert(objModel.SemesterID);


            //tao object moi
            PeriodDeclarationObject periodObject = new PeriodDeclarationObject();
            ////truyen du lieu tren form vao PeriodDeclarationObject
            //if (TryUpdateModel(objModel))
            //{
            bool isLockMark = objModel.chkLock.HasValue && objModel.chkLock.Value ? true : false;
            int? SchoolID = _globalInfo.SchoolID;
            int? AcademicYearID = _globalInfo.AcademicYearID;
            periodObject.SchoolID = SchoolID.Value;
            periodObject.AcademicYearID = AcademicYearID.Value;
            periodObject.FromDate = objModel.FromDate.Date;
            periodObject.EndDate = SMAS.Web.Utils.Utils.FixDateTime(objModel.EndDate);
            
            //du lieu hop le thi convert data va insert
            PeriodDeclaration periodModel = new PeriodDeclaration();
            Utils.Utils.BindTo(periodObject, periodModel);

            //chi co 1 dot la duoc khai bao diem hoc ky
            if (objModel.chkKTHK.HasValue && objModel.chkKTHK.Value)
            {
                var temp = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                        {
                            {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                            {"Semester",objModel.SemesterID}
                        })
                    ;
                var temp1 = temp.Where(o => o.PeriodDeclarationID != periodModel.PeriodDeclarationID).Where(o => o.ContaintSemesterMark.Value == true);
                if (temp1.Count() > 0)
                {
                    if (objModel.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        throw new BusinessException("PeriodDeclaration_Label_PeriodContaintSemesterMarkHK1");
                    }
                    else
                    {
                        throw new BusinessException("PeriodDeclaration_Label_PeriodContaintSemesterMarkHK2");
                    }

                }

                //lay ra dot co ngay lon nhat
                var temp2 = temp.Max(o => o.EndDate);
                if (temp2.HasValue && periodObject.EndDate.HasValue)
                {
                    if (DateTime.Compare(periodObject.EndDate.Value, temp2.Value) < 0)
                    {
                        throw new BusinessException("PeriodDeclaration_Label_PeriodContaintSemesterMarkErrorDate");
                    }
                }
            }

            //List<PeriodDeclaration> lsPD = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
            //    {
            //        {"AcademicYearID",_globalInfo.AcademicYearID.Value},
            //        {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_FIRST}
            //    }).Where(o => DateTime.Compare(o.FromDate.Value, periodObject.FromDate.Value) < 0).ToList();

            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID.Value);
            periodModel.Year = ay.Year;


            //periodModel.StartIndexOfInterviewMark = ((lsPD.Sum(o => o.InterviewMark).HasValue ? lsPD.Sum(o => o.InterviewMark).Value : 0) + 1);
            //periodModel.StartIndexOfWritingMark = ((lsPD.Sum(o => o.WritingMark).HasValue ? lsPD.Sum(o => o.WritingMark).Value : 0) + 1);
            //periodModel.StartIndexOfTwiceCoeffiecientMark = ((lsPD.Sum(o => o.TwiceCoeffiecientMark).HasValue ? lsPD.Sum(o => o.TwiceCoeffiecientMark).Value : 0) + 1);
            periodModel.StrInterviewMark = objModel.strInterviewMark;
            periodModel.StrWritingMark = objModel.strWritingMark;
            periodModel.StrTwiceCoeffiecientMark = objModel.strTwiceMark;
            periodModel.IsLock = isLockMark;
            periodModel.ContaintSemesterMark = objModel.chkKTHK.HasValue && objModel.chkKTHK.Value ? true : false;
            periodModel.Semester = objModel.SemesterID;
            periodModel.InterviewMark = objModel.InterviewMark;
            periodModel.WritingMark = objModel.WritingMark;
            periodModel.TwiceCoeffiecientMark = objModel.TwiceMark;
            periodModel.Resolution = objModel.PeriodDeclarationName;
            periodModel.IsEditMark = objModel.IsEditMark;
            this.PeriodDeclarationBusiness.Insert(periodModel);
            this.PeriodDeclarationBusiness.Save();


            //Bo sung nghiep vu 19/12/2013
            if (objModel.chkLock.HasValue)
            {
                LockMarkSemester(objModel.SemesterID, periodModel, true, isLockMark);
            }
            return Json(new JsonMessage("Thêm mới thành công", "success"));
        }

        #endregion _InsertAjaxEditing
        #endregion

        #region SetViewData - set du lieu ban dau

        private void SetViewData()
        {
            //Load semester
            bool semester1 = _globalInfo.Semester.Value == 1 ? true : false;
            bool semester2 = _globalInfo.Semester.Value == 2 ? true : false;

            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterFirst"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterSecond"), 2, semester2, false));
            ViewData[PeriodDeclarationConstant.LIST_SEMESTER] = listSemester;

            List<SemeterDeclaration> lstSD = SemeterDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                                    && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                                    && p.Semester == _globalInfo.Semester).ToList();
            //SemeterDeclaration objSD = lstSD.FirstOrDefault();
            SemeterDeclaration objSD = null;
            if (lstSD != null && lstSD.Count > 0)
            {
                objSD = lstSD.OrderByDescending(p => p.AppliedDate).FirstOrDefault();
            }
            List<PeriodDeclarationViewModel> lstPeriodDeclaration = PeriodDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                            && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                            && p.Semester == _globalInfo.Semester).Select(o => new PeriodDeclarationViewModel
                                                                            {
                                                                                PeriodDeclarationID = o.PeriodDeclarationID,
                                                                                Resolution = o.Resolution,
                                                                                FromDate = o.FromDate,
                                                                                ToDate = o.EndDate,
                                                                                SchoolId = o.SchoolID,
                                                                                AcademicYearID = o.AcademicYearID,
                                                                                InterviewMark = o.InterviewMark,
                                                                                StrInterviewMark = o.StrInterviewMark,
                                                                                StartIndexInterviewMark = o.StartIndexOfInterviewMark,
                                                                                WritingMark = o.WritingMark,
                                                                                StrWritingMark = o.StrWritingMark,
                                                                                StartIndexWritingMark = o.StartIndexOfWritingMark,
                                                                                TwiceCoeffiecientMark = o.TwiceCoeffiecientMark,
                                                                                StrTwiceMark = o.StrTwiceCoeffiecientMark,
                                                                                StartIndexTwiceCoeMark = o.StartIndexOfTwiceCoeffiecientMark,
                                                                                ContantSemesterMark = o.ContaintSemesterMark.HasValue ? o.ContaintSemesterMark.Value : false,
                                                                                IsLock = o.IsLock.HasValue ? o.IsLock.Value : false,
                                                                                Semester = o.Semester,
                                                                                IsEditMark = o.IsEditMark ?? false
                                                                            }).ToList();
            if (objSD != null)
            {
                ViewData[PeriodDeclarationConstant.INTERVIEW_MARK] = objSD.InterviewMark;
                ViewData[PeriodDeclarationConstant.WRITING_MARK] = objSD.WritingMark;
                ViewData[PeriodDeclarationConstant.TWICE_COEFFIECIENT_MARK] = objSD.TwiceCoeffiecientMark;
            }
            ViewData[PeriodDeclarationConstant.LS_PERIODDECLARATION] = lstPeriodDeclaration;

        }

        #endregion SetViewData


        #region Bổ sung nghiệp vụ khóa đợt 12/17/2013
        /// <summary>
        /// Khoa dot
        /// QuangLM1
        /// </summary>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="isChangeIndex">Neu co thay doi so con diem cua dot thi phai thu hien mo khoa va khoa doi voi cac dot sau trong ky</param>
        private void LockMarkSemester(int Semester, PeriodDeclaration Period, bool isChangeIndex, bool ischkLock)
        {


            if (Period != null)
            {
                List<MarkType> lstMarkTypeDB = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", _globalInfo.AppliedLevel } }).ToList();
                string strInterviewMark = Period.StrInterviewMark;
                string strWritingMark = Period.StrWritingMark;
                string strTwiceMark = Period.StrTwiceCoeffiecientMark;
                List<int> lstMarkType = new List<int>();
                List<int> lstMarkIndex = new List<int>();
                List<string> arrM = new List<string>();
                List<string> arrP = new List<string>();
                List<string> arrV = new List<string>();
                MarkType objMTM = null;
                MarkType objMTP = null;
                MarkType objMTV = null;
                MarkType objMTHK = null;
                if (!string.IsNullOrEmpty(strInterviewMark))
                {
                    objMTM = lstMarkTypeDB.Where(p => "M".Equals(p.Title)).FirstOrDefault();
                    if (objMTM != null)
                    {
                        lstMarkType.Add(objMTM.MarkTypeID);
                    }
                    arrM = strInterviewMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    int indexM = 0;
                    for (int i = 0; i < arrM.Count; i++)
                    {
                        indexM = Int32.Parse(arrM[i].Replace("M", ""));
                        lstMarkIndex.Add(indexM);
                    }
                }

                if (!string.IsNullOrEmpty(strWritingMark))
                {
                    objMTP = lstMarkTypeDB.Where(p => "P".Equals(p.Title)).FirstOrDefault();
                    if (objMTP != null)
                    {
                        lstMarkType.Add(objMTP.MarkTypeID);
                    }
                    arrP = strWritingMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    int indexP = 0;
                    for (int i = 0; i < arrP.Count; i++)
                    {
                        indexP = Int32.Parse(arrP[i].Replace("P", ""));
                        lstMarkIndex.Add(indexP);
                    }
                }

                if (!string.IsNullOrEmpty(strTwiceMark))
                {
                    objMTV = lstMarkTypeDB.Where(p => "V".Equals(p.Title)).FirstOrDefault();
                    if (objMTV != null)
                    {
                        lstMarkType.Add(objMTV.MarkTypeID);
                    }
                    arrV = strTwiceMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    int indexV = 0;
                    for (int i = 0; i < arrV.Count; i++)
                    {
                        indexV = Int32.Parse(arrV[i].Replace("V", ""));
                        lstMarkIndex.Add(indexV);
                    }
                }

                if (Period.ContaintSemesterMark.HasValue && Period.ContaintSemesterMark.Value)
                {
                    objMTHK = lstMarkTypeDB.Where(p => "HK".Equals(p.Title)).FirstOrDefault();
                    if (objMTHK != null)
                    {
                        lstMarkType.Add(objMTHK.MarkTypeID);
                    }
                }

                int partitionId = UtilsBusiness.GetPartionId(Period.SchoolID.Value);

                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SemesterID",Semester}
                };
                List<LockedMarkDetail> lstLMD = new List<LockedMarkDetail>();
                lstLMD = LockedMarkDetailBusiness.SearchLockedMarkDetail(dic).Where(p => lstMarkType.Contains(p.MarkTypeID.Value) && lstMarkIndex.Contains(p.MarkIndex)).ToList();

                //remove truoc 
                LockedMarkDetailBusiness.DeleteAll(lstLMD);
                PeriodDeclarationBusiness.Save();
                //Insert khoa diem moi

                if (ischkLock)
                {
                    List<ClassSubjectBO> lstClassSubjectBO = new List<ClassSubjectBO>();
                    lstClassSubjectBO = (from cs in ClassSubjectBusiness.All
                                         join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                         join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                         where cp.SchoolID == _globalInfo.SchoolID
                                         && cp.AcademicYearID == _globalInfo.AcademicYearID
                                         && cs.Last2digitNumberSchool == partitionId
                                         && (ed.Grade == 2 || ed.Grade == 3)
                                         && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                         select new ClassSubjectBO
                                         {
                                             ClassID = cp.ClassProfileID,
                                             SubjectID = cs.SubjectID,
                                         }).ToList();

                    List<LockedMarkDetail> lstResult = new List<LockedMarkDetail>();
                    LockedMarkDetail objLMD = null;
                    ClassSubjectBO objCSBO = null;
                    for (int i = 0; i < lstClassSubjectBO.Count; i++)
                    {
                        objCSBO = lstClassSubjectBO[i];
                        for (int j = 0; j < arrM.Count; j++)
                        {
                            objLMD = new LockedMarkDetail();
                            objLMD.SchoolID = Period.SchoolID;
                            objLMD.AcademicYearID = Period.AcademicYearID;
                            objLMD.SubjectID = objCSBO.SubjectID;
                            objLMD.Semester = Semester;
                            objLMD.ClassID = objCSBO.ClassID;
                            objLMD.MarkTypeID = objMTM.MarkTypeID;
                            objLMD.MarkIndex = Int32.Parse(arrM[j].Replace("M", ""));
                            objLMD.Last2digitNumberSchool = partitionId;
                            lstResult.Add(objLMD);
                        }

                        for (int j = 0; j < arrP.Count; j++)
                        {
                            objLMD = new LockedMarkDetail();
                            objLMD.SchoolID = Period.SchoolID;
                            objLMD.AcademicYearID = Period.AcademicYearID;
                            objLMD.SubjectID = objCSBO.SubjectID;
                            objLMD.Semester = Semester;
                            objLMD.ClassID = objCSBO.ClassID;
                            objLMD.MarkTypeID = objMTP.MarkTypeID;
                            objLMD.MarkIndex = Int32.Parse(arrP[j].Replace("P", ""));
                            objLMD.Last2digitNumberSchool = partitionId;
                            lstResult.Add(objLMD);
                        }

                        for (int j = 0; j < arrV.Count; j++)
                        {
                            objLMD = new LockedMarkDetail();
                            objLMD.SchoolID = Period.SchoolID;
                            objLMD.AcademicYearID = Period.AcademicYearID;
                            objLMD.SubjectID = objCSBO.SubjectID;
                            objLMD.Semester = Semester;
                            objLMD.ClassID = objCSBO.ClassID;
                            objLMD.MarkTypeID = objMTV.MarkTypeID;
                            objLMD.MarkIndex = Int32.Parse(arrV[j].Replace("V", ""));
                            objLMD.Last2digitNumberSchool = partitionId;
                            lstResult.Add(objLMD);
                        }

                        if (Period.ContaintSemesterMark.HasValue && Period.ContaintSemesterMark.Value)
                        {
                            objMTHK = lstMarkTypeDB.Where(p => "HK".Equals(p.Title)).FirstOrDefault();
                            objLMD = new LockedMarkDetail();
                            objLMD.SchoolID = Period.SchoolID;
                            objLMD.AcademicYearID = Period.AcademicYearID;
                            objLMD.SubjectID = objCSBO.SubjectID;
                            objLMD.Semester = Semester;
                            objLMD.ClassID = objCSBO.ClassID;
                            objLMD.MarkTypeID = objMTHK.MarkTypeID;
                            objLMD.MarkIndex = 1;
                            objLMD.Last2digitNumberSchool = partitionId;
                            lstResult.Add(objLMD);
                        }
                    }

                    LockedMarkDetailBusiness.InsertLockedMarkDetail(lstResult);
                }
                //PeriodDeclarationBusiness.LockPeriod(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Semester, Period.PeriodDeclarationID);
                //// Neu co thay doi so con diem
                //if (isChangeIndex)
                //{
                //    // Lay cac dot sau no
                //    List<PeriodDeclaration> listPeriodNext = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                //        new Dictionary<string, object>() { { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "Semester", Semester } })
                //        .Where(o => o.FromDate > Period.FromDate).ToList();
                //    foreach (PeriodDeclaration periodNext in listPeriodNext)
                //    {
                //        PeriodDeclarationBusiness.LockPeriod(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, Semester, periodNext.PeriodDeclarationID);
                //    }
                //}
            }
        }
        #endregion

        #region Phát triển giai đoạn 10 _anhnph_ 30/06/2015

        [HttpPost]
        public PartialViewResult AjaxLoadGirdBySemester()
        {
            SetViewDataPermission("PeriodDeclaration", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            var r = Request;
            int Semester = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
            List<SemeterDeclaration> lstSD = SemeterDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                                    && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                                    && p.Semester == Semester).ToList();
            //SemeterDeclaration objSD = lstSD.FirstOrDefault();
            SemeterDeclaration objSD = null;
            if (lstSD != null && lstSD.Count > 0)
            {
                objSD = lstSD.OrderByDescending(p => p.AppliedDate).FirstOrDefault();
            }
            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                            && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                            && p.Semester == Semester).OrderBy(p => p.FromDate).ToList();

            //Lay thong tin de kiem tra xoa neu dot co diem thi thong bao confirm truoc khi xoa
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"SemesterID",Semester}
            };
            int schoolID = _globalInfo.SchoolID.Value;
            List<PeriodDeclarationViewModel> lstModel = new List<PeriodDeclarationViewModel>();
            PeriodDeclarationViewModel objModel = null;
            PeriodDeclaration objPeriod = null;
            string strViewAllMark = string.Empty;
            for (int i = 0; i < lstPeriod.Count; i++)
            {
                objPeriod = lstPeriod[i];
                objModel = new PeriodDeclarationViewModel();
                strViewAllMark = objPeriod.StrInterviewMark + objPeriod.StrWritingMark + objPeriod.StrTwiceCoeffiecientMark;
                objModel.PeriodDeclarationID = objPeriod.PeriodDeclarationID;
                objModel.Resolution = objPeriod.Resolution;
                objModel.FromDate = objPeriod.FromDate;
                objModel.ToDate = objPeriod.EndDate;
                objModel.SchoolId = objPeriod.SchoolID;
                objModel.AcademicYearID = objPeriod.AcademicYearID;
                objModel.InterviewMark = objPeriod.InterviewMark;
                objModel.StrInterviewMark = objPeriod.StrInterviewMark;
                objModel.StartIndexInterviewMark = objPeriod.StartIndexOfInterviewMark;
                objModel.WritingMark = objPeriod.WritingMark;
                objModel.StrWritingMark = objPeriod.StrWritingMark;
                objModel.StartIndexWritingMark = objPeriod.StartIndexOfWritingMark;
                objModel.TwiceCoeffiecientMark = objPeriod.TwiceCoeffiecientMark;
                objModel.StrTwiceMark = objPeriod.StrTwiceCoeffiecientMark;
                objModel.StartIndexTwiceCoeMark = objPeriod.StartIndexOfTwiceCoeffiecientMark;
                objModel.ContantSemesterMark = objPeriod.ContaintSemesterMark.HasValue ? objPeriod.ContaintSemesterMark.Value : false;
                objModel.IsLock = objPeriod.IsLock.HasValue ? objPeriod.IsLock.Value : false;
                objModel.Semester = objPeriod.Semester;
                lstModel.Add(objModel);
            }

            if (objSD != null)
            {
                ViewData[PeriodDeclarationConstant.INTERVIEW_MARK] = objSD.InterviewMark;
                ViewData[PeriodDeclarationConstant.WRITING_MARK] = objSD.WritingMark;
                ViewData[PeriodDeclarationConstant.TWICE_COEFFIECIENT_MARK] = objSD.TwiceCoeffiecientMark;
            }
            ViewData[PeriodDeclarationConstant.LS_PERIODDECLARATION] = lstModel;
            return PartialView("_List");
        }

        public PartialViewResult _EditPeriod(int PeriodDeclarationID, int SemesterID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"Semester",SemesterID}
            };
            PeriodDeclaration objPeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodDeclarationID);
            List<PeriodDeclaration> lstPeriodDeclaration = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.PeriodDeclarationID != PeriodDeclarationID).ToList();
            CreateOrEditModel objModel = new CreateOrEditModel();
            List<SemeterDeclaration> lstSD = SemeterDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                                    && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                                    && p.Semester == SemesterID).ToList();
            ViewData[PeriodDeclarationConstant.CHK_KTHK] = lstPeriodDeclaration.Where(p => p.ContaintSemesterMark.HasValue && p.ContaintSemesterMark.Value).Count() > 0;
            SemeterDeclaration objSD = null;
            if (lstSD != null && lstSD.Count > 0)
            {
                objSD = lstSD.OrderByDescending(p => p.AppliedDate).FirstOrDefault();
            }
            if (objSD != null)
            {
                ViewData[PeriodDeclarationConstant.INTERVIEW_MARK] = objSD.InterviewMark;
                ViewData[PeriodDeclarationConstant.WRITING_MARK] = objSD.WritingMark;
                ViewData[PeriodDeclarationConstant.TWICE_COEFFIECIENT_MARK] = objSD.TwiceCoeffiecientMark;
            }

            if (objPeriodDeclaration != null)
            {
                objModel.PeriodDeclarationID = objPeriodDeclaration.PeriodDeclarationID;
                objModel.FromDate = objPeriodDeclaration.FromDate.Value;
                objModel.EndDate = objPeriodDeclaration.EndDate.Value;
                objModel.PeriodDeclarationName = objPeriodDeclaration.Resolution;
                objModel.chkLock = objPeriodDeclaration.IsLock;
                objModel.chkKTHK = objPeriodDeclaration.ContaintSemesterMark;
                objModel.strInterviewMark = objPeriodDeclaration.StrInterviewMark;
                objModel.strWritingMark = objPeriodDeclaration.StrWritingMark;
                objModel.strTwiceMark = objPeriodDeclaration.StrTwiceCoeffiecientMark;
                objModel.IsEditMark = objPeriodDeclaration.IsEditMark ?? false;
            }

            //gan viewdata
            PeriodDeclaration objDB = null;
            string strInterviewMarkAll = "";
            string strWritingMarkAll = "";
            string strTwiceMarkAll = "";
            for (int i = 0; i < lstPeriodDeclaration.Count; i++)
            {
                objDB = lstPeriodDeclaration[i];
                strInterviewMarkAll += objDB.StrInterviewMark;
                strWritingMarkAll += objDB.StrWritingMark;
                strTwiceMarkAll += objDB.StrTwiceCoeffiecientMark;

            }
            ViewData[PeriodDeclarationConstant.STR_INTERVIEW_MARK_ALL] = strInterviewMarkAll;
            ViewData[PeriodDeclarationConstant.STR_WRITING_MARK_ALL] = strWritingMarkAll;
            ViewData[PeriodDeclarationConstant.STR_TWICE_MARK_ALL] = strTwiceMarkAll;
            ViewData[PeriodDeclarationConstant.ACADEMIC_YEAR] = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            return PartialView("_EditPeriod", objModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        public PartialViewResult _CreatePeriod(int SemesterID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"Semester",SemesterID}
            };
            List<PeriodDeclaration> lstPeriodDeclaration = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            List<SemeterDeclaration> lstSD = SemeterDeclarationBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID.Value
                                                                                   && p.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                                                   && p.Semester == SemesterID).ToList();
            //SemeterDeclaration objSD = lstSD.FirstOrDefault();
            SemeterDeclaration objSD = null;
            if (lstSD != null && lstSD.Count > 0)
            {
                objSD = lstSD.OrderByDescending(p => p.AppliedDate).FirstOrDefault();
            }
            if (objSD != null)
            {
                ViewData[PeriodDeclarationConstant.INTERVIEW_MARK] = objSD.InterviewMark;
                ViewData[PeriodDeclarationConstant.WRITING_MARK] = objSD.WritingMark;
                ViewData[PeriodDeclarationConstant.TWICE_COEFFIECIENT_MARK] = objSD.TwiceCoeffiecientMark;
            }
            else
            {
                ViewData[PeriodDeclarationConstant.MESSAGE_NOTE] = true;
            }
            //gan viewdata
            PeriodDeclaration objDB = null;
            string strInterviewMarkAll = "";
            string strWritingMarkAll = "";
            string strTwiceMarkAll = "";
            for (int i = 0; i < lstPeriodDeclaration.Count; i++)
            {
                objDB = lstPeriodDeclaration[i];
                strInterviewMarkAll += objDB.StrInterviewMark;
                strWritingMarkAll += objDB.StrWritingMark;
                strTwiceMarkAll += objDB.StrTwiceCoeffiecientMark;

            }
            ViewData[PeriodDeclarationConstant.STR_INTERVIEW_MARK_ALL] = strInterviewMarkAll;
            ViewData[PeriodDeclarationConstant.STR_WRITING_MARK_ALL] = strWritingMarkAll;
            ViewData[PeriodDeclarationConstant.STR_TWICE_MARK_ALL] = strTwiceMarkAll;
            ViewData[PeriodDeclarationConstant.ACADEMIC_YEAR] = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            return PartialView("_CreatePeriod");
        }
        #endregion Phát triển giai đoạn 10 _anhnph_ 30/06/2015

        private void InsertThreadMark(PeriodDeclaration objPeriod)
        {
            int SchoolID = objPeriod.SchoolID.Value;
            int AcademicYearID = objPeriod.AcademicYearID;
            List<ThreadMark> lstThreadMark = new List<ThreadMark>();
            ThreadMark objThreadMark = null;
            //lay danh sach lop toan truong
            IDictionary<string, object> dicClass = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
            List<int> lstClassID = ClassProfileBusiness.SearchBySchool(SchoolID, dicClass).Select(p => p.ClassProfileID).ToList();
            int ClassID = 0;
            int partitionid = UtilsBusiness.GetPartionId(SchoolID);
            for (int i = 0; i < lstClassID.Count; i++)
            {
                ClassID = lstClassID[i];
                objThreadMark = new ThreadMark();
                objThreadMark.PupilID = 0;
                objThreadMark.SchoolID = SchoolID;
                objThreadMark.AcademicYearID = AcademicYearID;
                objThreadMark.ClassID = ClassID;
                objThreadMark.Semester = (short)objPeriod.Semester.Value;
                objThreadMark.SubjectID = 0;
                objThreadMark.PeriodID = objPeriod.PeriodDeclarationID;
                objThreadMark.LastDigitSchoolID = partitionid;
                objThreadMark.Status = 1;
                objThreadMark.NumberScan = 0;
                objThreadMark.Type = GlobalConstants.SUMMED_UP_MARK_AUTO_TYPE;
                lstThreadMark.Add(objThreadMark);
            }
            ThreadMarkBusiness.BulkInsertListThreadMark(lstThreadMark);
        }
    }
}
