﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PeriodDeclarationArea
{
    public class PeriodDeclarationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PeriodDeclarationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PeriodDeclarationArea_default",
                "PeriodDeclarationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}