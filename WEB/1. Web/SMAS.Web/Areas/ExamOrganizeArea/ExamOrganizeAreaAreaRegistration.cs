﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamOrganizeArea
{
    public class ExamOrganizeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamOrganizeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamOrganizeArea_default",
                "ExamOrganizeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
