﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.Business;
using SMAS.Web.Areas.ClassEmulationArea.Models;

using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ClassEmulationArea.Controllers
{
    public class ClassEmulationController : BaseController
    {
        private readonly IClassEmulationBusiness ClassEmulationBusiness;
        private readonly IEmulationCriteriaBusiness EmulationCriteriaBusiness;
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IClassEmulationDetailBusiness ClassEmulationDetailBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public ClassEmulationController(IClassEmulationBusiness classemulationBusiness,
            IEmulationCriteriaBusiness emulationcriteriabusiness,
            IPupilFaultBusiness pupilfaultbusiness,
            IClassEmulationDetailBusiness classemulationdetailbusiness,
            IClassProfileBusiness classprofilebusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IAcademicYearBusiness academicYearBusiness)
        {
            this.ClassEmulationBusiness = classemulationBusiness;
            this.EmulationCriteriaBusiness = emulationcriteriabusiness;
            this.PupilFaultBusiness = pupilfaultbusiness;
            this.ClassEmulationDetailBusiness = classemulationdetailbusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        //
        // GET: /ClassEmulation/

        public ActionResult Index()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<ClassEmulationViewModel> lst = this._Search(SearchInfo);
            ViewData[ClassEmulationConstants.LIST_CLASSEMULATION] = lst;
            List<EducationLevel> lstEducation = new GlobalInfo().EducationLevels;
            List<ComboObject> lstOrderType = CommonList.OrderType();
            ViewData[ClassEmulationConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            ViewData[ClassEmulationConstants.LIST_ORDERTTYPE] = new SelectList(lstOrderType, "Key", "Value");
            ViewData[ClassEmulationConstants.DATE] = DateTime.Now;
            DateTime Start = DateTime.Now.StartOfWeek();
            ViewData[ClassEmulationConstants.FROMDATE] = Start.ToShortDateString();
            ViewData[ClassEmulationConstants.TODATE] = Start.EndOfWeek().ToShortDateString();
            ViewData["Flag"] = false;
            ViewData["ErrorDate"] = false;
            AcademicYear ay = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            ViewData["AcademicYear"] = ay;
            return View();
        }

        //
        // GET: /ClassEmulation/Search

        public string GetStartEndOfDate()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";
            string date = Request["date"];
            AcademicYear academicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            DateTime firstSemesterStartDate = academicYear.FirstSemesterStartDate.Value;
            DateTime secondSemesterEndDate = academicYear.SecondSemesterEndDate.Value;
            int orderttypeid = !string.IsNullOrWhiteSpace(Request["orderttypeid"]) ? Int32.Parse(Request["orderttypeid"]) : SystemParamsInFile.ORDER_TYPE_WEEK;
            if (!string.IsNullOrWhiteSpace(date))
            {
                DateTime day = DateTime.Parse(date);
                //if ((orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK && day.EndOfWeek() >= firstSemesterStartDate && day.StartOfWeek() <= secondSemesterEndDate) || (orderttypeid == SystemParamsInFile.ORDER_TYPE_MONTH && day.EndOfMonth() >= firstSemesterStartDate && day.StartOfMonth() <= secondSemesterEndDate))
                //{
                if (day.StartOfWeek() > DateTime.Now.Date)
                {
                    if (orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK)
                    {
                        day = day.AddDays(-7);
                    }
                    else
                    {
                        day = day.AddMonths(-1);
                    }
                }
                var start = orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK ? day.StartOfWeek() : day.StartOfMonth();
                var end = orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK ? start.EndOfWeek() : start.EndOfMonth();
                return string.Format("{0},{1}", start.ToShortDateString(), end.ToShortDateString());
                //}
            }
            return string.Empty;
        }
        [HttpPost]
        public ActionResult Index(FormCollection col)
        {
            AcademicYear ay = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            ViewData["AcademicYear"] = ay;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";
            GlobalInfo global = new GlobalInfo();
            int educationlevelid = !string.IsNullOrWhiteSpace(col["EducationLevelID"]) ? Int32.Parse(col["EducationLevelID"]) : 0;
            int orderttypeid = !string.IsNullOrWhiteSpace(col["OrdertTypeID"]) ? Int32.Parse(col["OrdertTypeID"]) : SystemParamsInFile.ORDER_TYPE_WEEK;
            DateTime date = DateTime.Now;
            ViewData["ErrorDate"] = false;
            if (!string.IsNullOrWhiteSpace(col["dtpDate"]))
            {
                try
                {
                    date = DateTime.Parse(col["dtpDate"]);
                }
                catch (Exception ex)
                {
                    ViewData["ErrorDate"] = true;
                    ViewData["CurrentDate"] = col["dtpDate"];
                    //return View("Index", new ClassEmulationViewModel());
                }
            }
            DateTime _start = orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK ? date.StartOfWeek() : date.StartOfMonth();
            DateTime _end = orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK ? _start.EndOfWeek() : _start.EndOfMonth();
            DateTime start = new DateTime(_start.Year, _start.Month, _start.Day, 0, 0, 0);
            DateTime end = new DateTime(_end.Year, _end.Month, _end.Day, 0, 0, 0);

            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //IEnumerable<ClassEmulationViewModel> lst = this._Search(SearchInfo);
            //ViewData[ClassEmulationConstants.LIST_CLASSEMULATION] = lst;
            ViewData[ClassEmulationConstants.ORDERTTYPEID] = orderttypeid;
            ClassEmulationViewModel model = new ClassEmulationViewModel();
            //xep loai thi du theo tuan
            if (orderttypeid == SystemParamsInFile.ORDER_TYPE_WEEK)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID;
                dic["EducationLevelID"] = educationlevelid;
                dic["FromViolatedDate"] = start;
                dic["ToViolatedDate"] = end;
                dic["FromDate"] = start;
                dic["EndDate"] = end;
                //+ ClassProfileBussiness. SearchBySchool(UserInfo.SchoolID, Dictionary) với
                //Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                var lstClassProfile = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic)
                    .Select(o => new ClassProfileTemp
            {
                ID = o.ClassProfileID,
                Name = o.DisplayName,
                ClassOrderNumber = o.OrderNumber
            });
                List<ClassProfileTemp> List_ClassProfileTemp = new List<ClassProfileTemp>();
                if (lstClassProfile != null && lstClassProfile.Count() > 0)
                {
                    foreach (var classprofile in lstClassProfile)
                    {
                        ClassProfileTemp classProfileTemp = new ClassProfileTemp();
                        classProfileTemp = classprofile;
                        classProfileTemp.TotalPenalizedMark = PupilFaultBusiness.SumTotalPenalizedMarkByClass(classprofile.ID, start, end);
                        List_ClassProfileTemp.Add(classProfileTemp);
                    }
                }
                //+ PupilFaultBussiness. SearchBySchool(UserInfo.SchoolID, Dictionary) 
                //    với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //    Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                //    Dictionary[“FromViolatedDate”] = dtpFromDate.Value
                //    Dictionary[“ToViolatedDate”] = dtpToDate.Value : thông tin lỗi vi phạm
                //var lstpPupilFaultInfo = PupilFaultBusiness.SearchBySchool(global.SchoolID.Value, dic);

                // + EmulationCriteriaBussiness.SearchBySchool(UserInfo.SchoolID) : danh sách loại điểm thi đua
                IQueryable<EmulationCriteria> lstEmulationCriteria = EmulationCriteriaBusiness.SearchBySchool(global.SchoolID.Value);

                //+ ClassEmulationDetailBussiness.SearchBySchool(UserInfo.SchoolID, Dictionary) với
                //    Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //    Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                //    Dictionary[“FromDate”] = dtpFromDate.Value
                //    Dictionary[“EndDate”] = dtpToDate.Value : thông tin điểm chi tiết đối với loại điểm thi đua
                IQueryable<ClassEmulationDetail> lstClassEmulationDetail = ClassEmulationDetailBusiness.SearchBySchool(global.SchoolID.Value, dic);
                IQueryable<ClassEmulation> lstClassEmulation = ClassEmulationBusiness.Search(dic);
                ViewData["Flag"] = true;
                //model.ListPupilFault = lstpPupilFaultInfo.ToList();
                model.ListEmulationCriteria = lstEmulationCriteria.ToList();
                model.ListClassEmulationDetail = lstClassEmulationDetail != null ? lstClassEmulationDetail.ToList() : null;
                model.ListClassEmulation = lstClassEmulation != null ? lstClassEmulation.ToList() : null;
                if (model.ListClassEmulation != null && model.ListClassEmulation.Count > 0)
                {
                    List<ClassProfileTemp> lstClassProfileTemp = new List<ClassProfileTemp>();
                    foreach (var item in List_ClassProfileTemp)
                    {
                        foreach (var item1 in model.ListClassEmulation)
                        {
                            if (item1.ClassID == item.ID)
                            {
                                item.Rank = item1.Rank;
                                break;
                            }
                        }
                        lstClassProfileTemp.Add(item);
                    }
                    //model.ListByWeek = lstClassProfileTemp.OrderBy(o => o.Name).OrderBy(o => o.Rank).ToList();
                    model.ListByWeek = lstClassProfileTemp.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.Name).ToList();
                }
                else
                    model.ListByWeek = List_ClassProfileTemp.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.Name).ToList();

            }
            //xep loai thi dua theo thang
            else if (orderttypeid == SystemParamsInFile.ORDER_TYPE_MONTH)
            {
                //* Với trường hợp cboOrdertType.Value = ORDER_TYPE_MONTH: tương ứng plResult2
                //- btnSave ẩn đi
                //Xác định thời gian đầu tuần của từng tuần trong tháng rồi gọi:
                //ClassEmulationBussiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                //với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Dictionary[“FromDate”] =  ngày vừa xác định
                //Để lấy thông tin từng tuần của tháng.
                //Tổng điểm bằng tổng các điểm của tuần
                // Xếp hạng: thứ tự xếp hạng theo tổng điểm. Nếu cùng điểm thì cùng hạng, ví dụ 2 lớp cùng hạng 5 thì lớp kế tiếp sẽ là hạng 7.
                List<ClassProfileTempMonth> lstClassProfileTempMonth = new List<ClassProfileTempMonth>();
                List<StartEndDate> lstWeek = GetListWeekByDate(date);
                List<ClassEmulation> lstClassEmulation = new List<ClassEmulation>();
                for (var i = 0; i < lstWeek.Count; i++)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["SchoolID"] = global.SchoolID;
                    dic["AcademicYearID"] = global.AcademicYearID;
                    dic["EducationLevelID"] = educationlevelid;
                    dic["FromDate"] = new DateTime(lstWeek[i].FromDate.Year, lstWeek[i].FromDate.Month, lstWeek[i].FromDate.Day, 0, 0, 0);
                    dic["EndDate"] = new DateTime(lstWeek[i].ToDate.Year, lstWeek[i].ToDate.Month, lstWeek[i].ToDate.Day, 0, 0, 0);
                    ViewData["Flag"] = true;
                    IQueryable<ClassEmulation> lstEB = ClassEmulationBusiness.Search(dic);
                    lstClassEmulation = lstEB != null ? ClassEmulationBusiness.Search(dic).ToList() : null;
                    if (lstClassEmulation != null && lstClassEmulation.Count > 0)
                    {
                        foreach (var ClassEmulation in lstClassEmulation)
                        {
                            if (lstClassProfileTempMonth.Any(o => o.ID == ClassEmulation.ClassID))
                            {
                                ClassProfileTempMonth ClassProfileTempMonth = lstClassProfileTempMonth.Where(o => o.ID == ClassEmulation.ClassID).FirstOrDefault();
                                if (i == 0)
                                {
                                    ClassProfileTempMonth.TotalMark1 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark1;
                                }
                                else if (i == 1)
                                {
                                    ClassProfileTempMonth.TotalMark2 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark2;
                                }
                                else if (i == 2)
                                {
                                    ClassProfileTempMonth.TotalMark3 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark3;
                                }
                                else if (i == 3)
                                {
                                    ClassProfileTempMonth.TotalMark4 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark4;
                                }
                                else if (i == 4)
                                {
                                    ClassProfileTempMonth.TotalMark5 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark5;
                                }
                                lstClassProfileTempMonth.Remove(lstClassProfileTempMonth.Where(o => o.ID == ClassEmulation.ClassID).FirstOrDefault());
                                lstClassProfileTempMonth.Add(ClassProfileTempMonth);
                            }
                            else
                            {
                                ClassProfileTempMonth ClassProfileTempMonth = new ClassProfileTempMonth();
                                ClassProfileTempMonth.ID = ClassEmulation.ClassID;
                                ClassProfileTempMonth.Name = ClassEmulation.ClassProfile.DisplayName;
                                ClassProfileTempMonth.ClassOrderNumber = ClassEmulation.ClassProfile.OrderNumber;
                                ClassProfileTempMonth.TotalMark = 0;
                                if (i == 0)
                                {
                                    ClassProfileTempMonth.TotalMark1 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark1;
                                }
                                else if (i == 1)
                                {
                                    ClassProfileTempMonth.TotalMark2 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark2;
                                }
                                else if (i == 2)
                                {
                                    ClassProfileTempMonth.TotalMark3 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark3;
                                }
                                else if (i == 3)
                                {
                                    ClassProfileTempMonth.TotalMark4 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark4;
                                }
                                else if (i == 4)
                                {
                                    ClassProfileTempMonth.TotalMark5 = ClassEmulation.TotalMark;
                                    ClassProfileTempMonth.TotalMark += ClassProfileTempMonth.TotalMark5;
                                }
                                lstClassProfileTempMonth.Add(ClassProfileTempMonth);
                            }
                        }
                    }
                }
                //model.ListClassProfileTempMonth = lstClassProfileTempMonth.OrderBy(o => o.Name).OrderByDescending(o => o.TotalMark).ToList();
                model.ListClassProfileTempMonth = lstClassProfileTempMonth.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.Name).ToList();
                ViewData["WeekNumber"] = lstWeek.Count;
            }
            List<EducationLevel> lstEducation = new GlobalInfo().EducationLevels;
            List<ComboObject> lstOrderType = CommonList.OrderType();
            ViewData[ClassEmulationConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            ViewData[ClassEmulationConstants.LIST_ORDERTTYPE] = new SelectList(lstOrderType, "Key", "Value");
            ViewData[ClassEmulationConstants.DATE] = date;
            DateTime Start = date.StartOfWeek();
            ViewData[ClassEmulationConstants.FROMDATE] = _start.ToShortDateString();
            ViewData[ClassEmulationConstants.TODATE] = _end.ToShortDateString();
            ViewData[ClassEmulationConstants.EDUCATIONLEVELID] = educationlevelid;
            ViewData[ClassEmulationConstants.ORDERTTYPEID] = orderttypeid;
            return View("Index", model);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateByWeek(FormCollection col)
        {
            try
            {
                //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator = "/";
                #region Get Param
                int educationlevelid = !string.IsNullOrWhiteSpace(col["educationlelvelid"]) ? Int32.Parse(col["educationlelvelid"]) : 0;
                int orderttypeid = !string.IsNullOrWhiteSpace(col["orderttypeid"]) ? Int32.Parse(col["orderttypeid"]) : SystemParamsInFile.ORDER_TYPE_WEEK;
                DateTime date = !string.IsNullOrWhiteSpace(col["date"]) ? DateTime.Parse(col["date"]) : DateTime.Now;
                DateTime _start = !string.IsNullOrWhiteSpace(col["fromdate"]) ? DateTime.Parse(col["fromdate"]) : DateTime.Now;
                DateTime _end = !string.IsNullOrWhiteSpace(col["todate"]) ? DateTime.Parse(col["todate"]) : DateTime.Now;
                DateTime start = new DateTime(_start.Year, _start.Month, _start.Day, 0, 0, 0);
                DateTime end = new DateTime(_end.Year, _end.Month, _end.Day, 0, 0, 0);
                GlobalInfo global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID;
                dic["EducationLevelID"] = educationlevelid;
                dic["FromViolatedDate"] = start;
                dic["ToViolatedDate"] = end;
                #endregion

                #region lấy list học sinh và các thông tin liên quan
                //+ ClassProfileBussiness. SearchBySchool(UserInfo.SchoolID, Dictionary) với
                //Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                var lstClassProfile = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic)
                    .Select(o => new ClassProfileTemp
                    {
                        ID = o.ClassProfileID,
                        Name = o.DisplayName
                    }).OrderBy(o => o.Name).ToList();
                //+ PupilFaultBussiness. SearchBySchool(UserInfo.SchoolID, Dictionary) 
                //    với Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //    Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                //    Dictionary[“FromViolatedDate”] = dtpFromDate.Value
                //    Dictionary[“ToViolatedDate”] = dtpToDate.Value : thông tin lỗi vi phạm
                List<PupilFault> lstpPupilFaultInfo = PupilFaultBusiness.SearchBySchool(global.SchoolID.Value, dic).ToList();

                // + EmulationCriteriaBussiness.SearchBySchool(UserInfo.SchoolID) : danh sách loại điểm thi đua
                List<EmulationCriteria> lstEmulationCriteria = EmulationCriteriaBusiness.SearchBySchool(global.SchoolID.Value).ToList();

                //+ ClassEmulationDetailBussiness.SearchBySchool(UserInfo.SchoolID, Dictionary) với
                //    Dictionary[“AcademicYearID”] = UserInfo.AcademicYearID
                //    Dictionary[“EducationLevelID”] = cboEducationLevel.Value
                //    Dictionary[“FromDate”] = dtpFromDate.Value
                //    Dictionary[“EndDate”] = dtpToDate.Value : thông tin điểm chi tiết đối với loại điểm thi đua
                IQueryable<ClassEmulationDetail> lstClassEmulationDetail = ClassEmulationDetailBusiness.SearchBySchool(global.SchoolID.Value, dic);
                #endregion


                //- Kiểm tra các rằng buộc 
                //+ Điểm ở các cột loại điểm thi đua phải là kiểu số >= 0
                //- Map dữ liệu thành: Dictionary<int, Dictionnay> 
                // data với cặp key value là ClassID – Dictionnay<int, decimal>(key – value là EmulationCriteriaID – điểm ở cột tương ứng).
                // - Gọi hàm ClassEmulationBussiness.InsertOrUpdate(dtpDate.Value, data)
                //- Hiện thị lại thông tin đã có sắp xếp theo dữ liệu mới cập nhật

                Dictionary<int, Dictionary<int, decimal>> dictionary = new Dictionary<int, Dictionary<int, decimal>>();
                int ClassID;
                decimal? TotalPenalizedMark = 0;
                string MarkFormat = "Mark_{0}_{1}";
                string Col_Mark;

                foreach (var classprofile in lstClassProfile)
                {
                    ClassID = classprofile.ID;
                    if (lstpPupilFaultInfo != null && lstpPupilFaultInfo.Count > 0
                        && lstpPupilFaultInfo.Where(p => p.ClassID == classprofile.ID).FirstOrDefault() != null)
                    {
                        TotalPenalizedMark = lstpPupilFaultInfo.Where(p => p.ClassID == classprofile.ID).FirstOrDefault().TotalPenalizedMark;
                    }

                    int EmulationCriteriaID;
                    decimal Mark;
                    string idcolumn;
                    Dictionary<int, decimal> dictionary2 = new Dictionary<int, decimal>();
                    foreach (var emulationcriteria in lstEmulationCriteria)
                    {
                        EmulationCriteriaID = emulationcriteria.EmulationCriteriaId;
                        idcolumn = string.Format(MarkFormat, ClassID.ToString(), emulationcriteria.EmulationCriteriaId.ToString());
                        Col_Mark = col[idcolumn];
                        try
                        {
                            Mark = !string.IsNullOrWhiteSpace(Col_Mark) ? decimal.Parse(Col_Mark) : -1;
                        }
                        catch
                        {
                            return Json(new JsonMessage(string.Format("{0}.{1}", Res.Get("ClassEmulation_Err_NumberRequire"), idcolumn), "emptyerror"));
                        }
                        if (Mark < 0)
                        {
                            return Json(new JsonMessage(string.Format("{0}.{1}", Res.Get("Common_Label_Validation_Empty"), idcolumn), "emptyerror"));
                        }
                        dictionary2.Add(EmulationCriteriaID, Mark);
                    }
                    dictionary.Add(ClassID, dictionary2);
                    //- Gọi hàm ClassEmulationBussiness.InsertOrUpdate(dtpDate.Value, data)
                    ClassEmulationBusiness.InsertOrUpdate(global.AcademicYearID.Value, educationlevelid, date, dictionary);
                    ClassEmulationBusiness.Save();
                    ClassEmulationDetailBusiness.Save();
                    dictionary.Clear();
                }
                //- Hiện thị lại thông tin đã có sắp xếp theo dữ liệu mới cập nhật

                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            catch (Exception ex)
            {
                return Json(new JsonMessage(Res.Get(ex.Message), "ERROR"));
            }
        }

        [HttpPost]
        public FileResult DownloadFile(FormCollection col)
        {
            DateTime date = !string.IsNullOrWhiteSpace(col["date"]) ? DateTime.Parse(col["date"]) : DateTime.Now;
            GlobalInfo global = new GlobalInfo();
            int EducationLevelID = col["educationlelvelid"].Equals("") ? 0 : int.Parse(col["educationlelvelid"]);

            string filename = "";
            Stream excel = ClassEmulationBusiness.ExportClassEmulationByWeek(global.SchoolID.Value, global.AcademicYearID.Value, date, EducationLevelID, out filename);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = filename;
            result.FileDownloadName = ReportName;

            return result;
        }

        [HttpPost]
        public FileResult DownloadFileByMonth(FormCollection col)
        {
            DateTime date = !string.IsNullOrWhiteSpace(col["date"]) ? DateTime.Parse(col["date"]) : DateTime.Now;
            GlobalInfo global = new GlobalInfo();
            int EducationLevelID = col["educationlelvelid"].Equals("") ? 0 : int.Parse(col["educationlelvelid"]);

            string filename = "";
            Stream excel = ClassEmulationBusiness.ExportClassEmulationByMonth(global.SchoolID.Value, global.AcademicYearID.Value, EducationLevelID, date, out filename);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = filename;
            result.FileDownloadName = ReportName;

            return result;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int ClassEmulationID)
        {
            ClassEmulation classemulation = this.ClassEmulationBusiness.Find(ClassEmulationID);
            TryUpdateModel(classemulation);
            Utils.Utils.TrimObject(classemulation);
            this.ClassEmulationBusiness.Update(classemulation);
            this.ClassEmulationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ClassEmulationBusiness.Delete(id);
            this.ClassEmulationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ClassEmulationViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ClassEmulation> query = this.ClassEmulationBusiness.Search(SearchInfo);
            if (query != null)
            {
                IQueryable<ClassEmulationViewModel> lst = query.Select(o => new ClassEmulationViewModel
                {
                    ClassEmulationID = o.ClassEmulationID,
                    SchoolID = o.SchoolID,
                    ClassID = o.ClassID,
                    ClassOrderNumber = o.ClassProfile.OrderNumber,
                    ClassName = o.ClassProfile.DisplayName,
                    AcademicYearID = o.AcademicYearID,
                    Year = o.Year,
                    Month = o.Month,
                    FromDate = o.FromDate,
                    ToDate = o.ToDate,
                    TotalEmulationMark = o.TotalEmulationMark,
                    TotalPenalizedMark = o.TotalPenalizedMark,
                    TotalMark = o.TotalMark,
                    Rank = o.Rank
                });

                return lst.OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();
            }
            else return null;
        }

        private List<StartEndDate> GetListWeekByDate(DateTime date)
        {
            List<StartEndDate> lstStartEndDate = new List<StartEndDate>();
            DateTime startofweek = date.StartOfWeek();
            StartEndDate StartEndDateCurrent = new StartEndDate();
            StartEndDateCurrent.FromDate = startofweek;
            StartEndDateCurrent.ToDate = startofweek.AddDays(6);
            if (StartEndDateCurrent.FromDate.Month == date.Month)
            {
                lstStartEndDate.Add(StartEndDateCurrent);
            }
            for (var i = 1; i <= 5; i++)
            {

                if (startofweek.AddDays(-(i * 7)).Month == date.Month)
                {
                    StartEndDate StartEndDate = new StartEndDate();
                    StartEndDate.FromDate = startofweek.AddDays(-(i * 7));
                    StartEndDate.ToDate = StartEndDate.FromDate.AddDays(6);
                    lstStartEndDate.Add(StartEndDate);
                }
                if (startofweek.AddDays((i * 7)).Month == date.Month)
                {
                    StartEndDate StartEndDate1 = new StartEndDate();
                    StartEndDate1.FromDate = startofweek.AddDays((i * 7));
                    StartEndDate1.ToDate = StartEndDate1.FromDate.AddDays(6);
                    lstStartEndDate.Add(StartEndDate1);
                }
            }

            var results = from dateString in lstStartEndDate
                          orderby (Convert.ToDateTime(dateString.FromDate))
                          select dateString;
            return results.ToList();
        }
    }
}





