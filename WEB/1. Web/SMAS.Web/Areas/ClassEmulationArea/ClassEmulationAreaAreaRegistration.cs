﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassEmulationArea
{
    public class ClassEmulationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassEmulationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassEmulationArea_default",
                "ClassEmulationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
