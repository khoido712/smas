/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ClassEmulationArea
{
    public class ClassEmulationConstants
    {
        public const string LIST_CLASSEMULATION = "listClassEmulation";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_ORDERTTYPE = "listOrdertType";
        public const string ORDERTTYPEID = "OrdertTypeID";
        public const string EDUCATIONLEVELID = "EducationLevelID";
        public const string DATE = "ClassEmulationDate";
        public const string FROMDATE = "ClassEmulationFromDate";
        public const string TODATE = "ClassEmulationToDate";
    }
}