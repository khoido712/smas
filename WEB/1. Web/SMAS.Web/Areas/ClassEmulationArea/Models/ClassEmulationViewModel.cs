/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
namespace SMAS.Web.Areas.ClassEmulationArea.Models
{
    public class ClassEmulationViewModel
    {
        public System.Int32 ClassEmulationID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Int32 ClassID { get; set; }
        public System.Nullable<System.Int32> ClassOrderNumber { get; set; }
        public string ClassName { get; set; }
        public System.Nullable<System.Int32> AcademicYearID { get; set; }
        public System.Nullable<System.Int32> Year { get; set; }
        public System.Nullable<System.Int32> Month { get; set; }
        public System.Nullable<System.DateTime> FromDate { get; set; }
        public System.Nullable<System.DateTime> ToDate { get; set; }
        public System.Nullable<System.Decimal> TotalEmulationMark { get; set; }
        public System.Nullable<System.Decimal> TotalPenalizedMark { get; set; }
        public System.Nullable<System.Decimal> TotalMark { get; set; }
        public System.Nullable<System.Int32> Rank { get; set; }
        public List<ClassProfileTemp> ListByWeek { get; set; }
        public List<ClassProfile> ListByMonth { get; set; }
        public List<PupilFault> ListPupilFault { get; set; }
        public List<ClassEmulationDetail> ListClassEmulationDetail { get; set; }
        public List<ClassEmulation> ListClassEmulation { get; set; }
        public List<EmulationCriteria> ListEmulationCriteria { get; set; }
        public List<ClassProfileTempMonth> ListClassProfileTempMonth { get; set; }
    }

    public class ClassProfileTemp
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? ClassOrderNumber { get; set; }
        public int? Rank { get; set; }
        public decimal? TotalMark { get; set; }
        public decimal? TotalPenalizedMark { get; set; }
    }
    public class ClassProfileTempMonth
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? Rank { get; set; }
        public int? ClassOrderNumber { get; set; }
        public decimal? TotalMark { get; set; }
        public decimal? TotalMark1 { get; set; }
        public decimal? TotalMark2 { get; set; }
        public decimal? TotalMark3 { get; set; }
        public decimal? TotalMark4 { get; set; }
        public decimal? TotalMark5 { get; set; }
    }
    public class StartEndDate
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}


