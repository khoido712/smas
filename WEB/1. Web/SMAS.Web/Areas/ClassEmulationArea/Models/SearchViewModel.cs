/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassEmulationArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassEmulation_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassEmulationConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ClassEmulation_Label_OrdertType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassEmulationConstants.LIST_ORDERTTYPE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? OrdertTypeID { get; set; }

        [ResourceDisplayName("ClassEmulation_Label_Date")]
        public DateTime? Date { get; set; }
        [ResourceDisplayName("ClassEmulation_Label_FromDate")]
        public DateTime? FromDate { get; set; }
        [ResourceDisplayName("ClassEmulation_Label_ToDate")]
        public DateTime? ToDate { get; set; }
    }
}