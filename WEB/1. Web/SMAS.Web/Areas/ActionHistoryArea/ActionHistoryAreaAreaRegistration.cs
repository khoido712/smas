﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ActionHistoryArea
{
    public class ActionHistoryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ActionHistoryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ActionHistoryArea_default",
                "ActionHistoryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
