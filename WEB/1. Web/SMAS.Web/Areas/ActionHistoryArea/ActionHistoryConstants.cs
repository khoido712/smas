﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ActionHistoryArea
{
    public class ActionHistoryConstants
    {
        public const string LIST_ACTIONAUDIT = "listActionAudit";
        public const int PAGE_SIZE = 20;
        public const string TOTAL = "total";
        public const string CBO_ACTION = "cbo_action";
    }
}