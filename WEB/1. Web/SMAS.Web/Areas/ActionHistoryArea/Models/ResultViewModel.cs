﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ActionHistoryArea.Models
{
    public class ResultViewModel
    {
        public int ActionAuditID { get; set; }
        public string ActionDate { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string FunctionName { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string IP { get; set; }
    }
}