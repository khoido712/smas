﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ActionHistoryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Từ ngày")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Từ ngày không được để trống")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [ResourceDisplayName("Đến ngày")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage="Đến ngày không được để trống")]
        public DateTime ToDate { get; set; }

        [ResourceDisplayName("Tên đăng nhập")]
        public string UserName { get; set; }

        [ResourceDisplayName("Hành động")]
        public string ActionName { get; set; }

    }
}