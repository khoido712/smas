﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ActionHistoryArea.Models;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ActionHistoryArea;
using SMAS.Business.Business;

namespace SMAS.Web.Areas.ActionHistoryArea.Controllers
{
    public class ActionHistoryController : BaseController
    {
        int Total = 0;
        private readonly IActionAuditBusiness ActionAuditBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        public ActionHistoryController(IActionAuditBusiness actionAuditBusiness, IAcademicYearBusiness academicYearBusiness, IAcademicYearOfProvinceBusiness academicYearOfProvinceBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.ActionAuditBusiness = actionAuditBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.AcademicYearOfProvinceBusiness = academicYearOfProvinceBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        public ActionResult Index()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Xem", Value = SMAS.Business.Common.GlobalConstants.ACTION_VIEW });
            list.Add(new SelectListItem { Text = "Thêm", Value = SMAS.Business.Common.GlobalConstants.ACTION_ADD });
            list.Add(new SelectListItem { Text = "Sửa", Value = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE });
            list.Add(new SelectListItem { Text = "Xóa", Value = SMAS.Business.Common.GlobalConstants.ACTION_DELETE });
            list.Add(new SelectListItem { Text = "Import", Value = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT });
            list.Add(new SelectListItem { Text = "Xuất excel", Value = SMAS.Business.Common.GlobalConstants.ACTION_EXPORT });            
            ViewData[ActionHistoryConstants.CBO_ACTION] = new SelectList(list, "Value", "Text");

            return View();
        }

        public PartialViewResult Search(SearchViewModel ObjSearch)
        {
            int SchoolID = _globalInfo.SchoolID != null ? _globalInfo.SchoolID.Value : 0;
            int SuperVisingDeptID = (_globalInfo.SupervisingDeptID != null ? _globalInfo.SupervisingDeptID.Value : 0);
            int PageNum = 1;
            List<ActionHistoryObject> ActionHistoryList = null;
            List<ResultViewModel> list = null;
            if (SchoolID > 0)
            {
                int AdminID = ActionAuditBusiness.GetAdminID(SchoolID);
                ActionHistoryList = ActionAuditBusiness.ListActionBySchool(SchoolID, AdminID, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.UserName, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = (string.IsNullOrEmpty(o.Name)) ? SMAS.Business.Common.GlobalConstants.MANAGE_SCHOOL : o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP

                }).ToList();
                ViewData[ActionHistoryConstants.LIST_ACTIONAUDIT] = list;
                ViewData[ActionHistoryConstants.TOTAL] = Total;
            }
            else if (SuperVisingDeptID > 0)
            {
                SupervisingDept supervising = SupervisingDeptBusiness.Find(SuperVisingDeptID);
                int Admin_SuperVisingDept = ActionAuditBusiness.GetAdminIDBySuperVisingDept(_globalInfo.SupervisingDeptID.Value);
                ActionHistoryList = ActionAuditBusiness.ListActionBySuperVisingDeptID(SuperVisingDeptID, Admin_SuperVisingDept, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.UserName, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = (string.IsNullOrEmpty(o.Name)) ? (supervising.HierachyLevel == 3 ? SMAS.Business.Common.GlobalConstants.MANAGE_SO : SMAS.Business.Common.GlobalConstants.MANAGE_PHONG) : o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP

                }).ToList();
                ViewData[ActionHistoryConstants.LIST_ACTIONAUDIT] = list;
                ViewData[ActionHistoryConstants.TOTAL] = Total;
            }
            else
            {
                int Admin_System = _globalInfo.UserAccountID;
                ActionHistoryList = ActionAuditBusiness.ListActionByAdmin(Admin_System, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP
                }).ToList();
                ViewData[ActionHistoryConstants.LIST_ACTIONAUDIT] = list;
                ViewData[ActionHistoryConstants.TOTAL] = Total;
            }
            return PartialView("_List");
        }

        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(GridCommand command, SearchViewModel ObjSearch)
        {
            int SchoolID = _globalInfo.SchoolID != null ? _globalInfo.SchoolID.Value : 0;
            int SuperVisingDeptID = (_globalInfo.SupervisingDeptID != null ? _globalInfo.SupervisingDeptID.Value : 0);
            List<ResultViewModel> list = null;
            int PageNum = command.Page;
            List<ActionHistoryObject> ActionHistoryList = new List<ActionHistoryObject>();

            if (SchoolID > 0)
            {
                int AdminID = ActionAuditBusiness.GetAdminID(SchoolID);
                ActionHistoryList = ActionAuditBusiness.ListActionBySchool(SchoolID, AdminID, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.UserName, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = (string.IsNullOrEmpty(o.Name)) ? SMAS.Business.Common.GlobalConstants.MANAGE_SCHOOL : o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP

                }).ToList();
            }
            else if (SuperVisingDeptID > 0)
            {
                SupervisingDept supervising = SupervisingDeptBusiness.Find(SuperVisingDeptID);
                int Admin_SuperVisingDept = ActionAuditBusiness.GetAdminIDBySuperVisingDept(_globalInfo.SupervisingDeptID.Value);
                ActionHistoryList = ActionAuditBusiness.ListActionBySuperVisingDeptID(SuperVisingDeptID, Admin_SuperVisingDept, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.UserName, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = (string.IsNullOrEmpty(o.Name)) ? (supervising.HierachyLevel == 3 ? SMAS.Business.Common.GlobalConstants.MANAGE_SO : SMAS.Business.Common.GlobalConstants.MANAGE_PHONG) : o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP

                }).ToList();
            }
            else
            {
                int Admin_System = _globalInfo.UserAccountID;
                ActionHistoryList = ActionAuditBusiness.ListActionByAdmin(Admin_System, ObjSearch.FromDate, ObjSearch.ToDate, ObjSearch.ActionName, PageNum, ActionHistoryConstants.PAGE_SIZE, ref Total);
                list = ActionHistoryList.Select(o => new ResultViewModel
                {
                    ActionAuditID = o.ActionAuditID,
                    ActionDate = o.ActionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    UserName = o.UserName,
                    Name = o.Name,
                    FunctionName = o.FunctionName,
                    Action = o.Action,
                    Description = o.Description,
                    IP = o.IP

                }).ToList();
            }

            return View(new GridModel<ResultViewModel>()
            {
                Data = list,
                Total = Total
            });
        }

        public FileResult ExportExcel()
        {
            DateTime Fromdate = new DateTime();
            DateTime Todate = new DateTime();
            string Username = "-1";
            string ActionName = "-1";
            if (Request["fromdate"] != "")
            {
                Fromdate = DateTime.Parse(Request["fromdate"]);
            }
            if (Request["todate"] != "")
            {
                Todate = DateTime.Parse(Request["todate"]);
            }
            if (Request["username"] != "")
            {
                Username = Request["username"];
            }
            if (Request["actionname"] != "")
            {
                ActionName = Request["actionname"];
            }

            int SchoolID = _globalInfo.SchoolID != null ? _globalInfo.SchoolID.Value : 0;
            int SuperVisingDeptID = (_globalInfo.SupervisingDeptID != null ? _globalInfo.SupervisingDeptID.Value : 0);
            List<ActionHistoryObject> ActionHistoryList = null;
            //string temp = "";
            if (SchoolID > 0)
            {
                //int EducationGrade = ActionAuditBusiness.EducationGrade(SchoolID);
                //if (EducationGrade == 1)
                //{
                //    temp = "TH";
                //}
                //else if (EducationGrade == 2)
                //{
                //    temp = "THCS";
                //}
                //else if (EducationGrade == 4)
                //{
                //    temp = "THPT";
                //}
                //else if (EducationGrade == 3)
                //{
                //    temp = "TPCS";
                //}
                //else if (EducationGrade == 6)
                //{
                //    temp = "PTTH";
                //}
                //else if (EducationGrade == 7)
                //{
                //    temp = "PT";
                //}
                //else
                //{
                //    temp = "";
                //}
                int AdminID = ActionAuditBusiness.GetAdminID(SchoolID);
                ActionHistoryList = ActionAuditBusiness.ListActionBySchoolExport(SchoolID, AdminID, Fromdate, Todate, Username, ActionName);
            }
            else if (SuperVisingDeptID > 0)
            {
                //if (_globalInfo.IsSuperVisingDeptRole)
                //{
                //    temp = "Sở";
                //}
                //else
                //{
                //    temp = "Phòng";
                //}
                int Admin_SuperVisingDept = ActionAuditBusiness.GetAdminIDBySuperVisingDept(_globalInfo.SupervisingDeptID.Value);
                ActionHistoryList = ActionAuditBusiness.ListActionBySuperVisingDeptIDExport(SuperVisingDeptID, Admin_SuperVisingDept, Fromdate, Todate, Username, ActionName);
            }
            else
            {
                //temp = "Admin";
                int Admin_System = _globalInfo.UserAccountID;
                ActionHistoryList = ActionAuditBusiness.ListActionByAdminExport(Admin_System, Fromdate, Todate, ActionName);
            }



            String pathFile = AppDomain.CurrentDomain.BaseDirectory + Guid.NewGuid().ToString() + ".xls";
            string TEMPLATE_NAME = "THCS_LichSuSuDung_04062014.xls";
            //string ExportTemplateName = temp + "_LichSuSuDung_" + Todate.ToString("dd/MM/yyyy") + ".xls";
            string ExportTemplateName = "LichSuSuDung_" + Todate.ToString("dd/MM/yyyy") + ".xls";
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, TEMPLATE_NAME);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int startRow = 6;
            IVTWorksheet sheet = oBook.GetSheet(1);
            sheet.SetCellValue("A3", "thời gian: " + Fromdate.ToString("dd/MM/yyyy") + "-" + Todate.ToString("dd/MM/yyyy"));

            if (ActionHistoryList.Count > 0)
            {
                int StartCol = 1;
                for (int i = 0; i < ActionHistoryList.Count; i++)
                {
                    sheet.SetCellValue(startRow + i, StartCol, i + 1);
                    sheet.SetCellValue(startRow + i, StartCol + 1, ActionHistoryList[i].ActionDate.ToString("dd/MM/yyyy HH:mm:ss"));
                    sheet.SetCellValue(startRow + i, StartCol + 2, ActionHistoryList[i].UserName);
                    if (!String.IsNullOrEmpty(ActionHistoryList[i].Name))
                    {
                        sheet.SetCellValue(startRow + i, StartCol + 3, ActionHistoryList[i].Name);
                    }
                    else
                    {
                        if (SchoolID > 0)
                        {
                            sheet.SetCellValue(startRow + i, StartCol + 3, SMAS.Business.Common.GlobalConstants.MANAGE_SCHOOL);
                        }
                        else if (SuperVisingDeptID > 0)
                        {
                            SupervisingDept supervising = SupervisingDeptBusiness.Find(SuperVisingDeptID);
                            sheet.SetCellValue(startRow + i, StartCol + 3, (supervising.HierachyLevel == 3 ? SMAS.Business.Common.GlobalConstants.MANAGE_SO : SMAS.Business.Common.GlobalConstants.MANAGE_PHONG));
                        }
                        else 
                        {
                            sheet.SetCellValue(startRow + i, StartCol + 3, "Admin");
                        }
                    }
                    sheet.SetCellValue(startRow + i, StartCol + 4, ActionHistoryList[i].FunctionName);
                    sheet.SetCellValue(startRow + i, StartCol + 5, ActionHistoryList[i].Action);
                    sheet.SetCellValue(startRow + i, StartCol + 6, ActionHistoryList[i].Description);
                    sheet.SetCellValue(startRow + i, StartCol + 7, ActionHistoryList[i].IP);
                    sheet.CopyPaste(sheet.GetRange("A" + (startRow + 1).ToString(), "H" + (startRow + 1).ToString()), startRow + i, 1, true);

                    if ((startRow + i) % 5 == 1)
                    {
                        sheet.GetRange((startRow + i) - 1, 1, (startRow + i) - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }
                }
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ExportTemplateName;
            return result;
        }
    }
}
