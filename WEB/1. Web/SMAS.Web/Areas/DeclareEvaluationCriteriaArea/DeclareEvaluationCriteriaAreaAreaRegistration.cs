﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareEvaluationCriteriaArea
{
    public class DeclareEvaluationCriteriaAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeclareEvaluationCriteriaArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeclareEvaluationCriteriaArea_default",
                "DeclareEvaluationCriteriaArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
