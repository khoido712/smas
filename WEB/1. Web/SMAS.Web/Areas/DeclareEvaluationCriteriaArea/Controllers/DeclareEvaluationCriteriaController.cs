﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DeclareEvaluationCriteriaArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareEvaluationCriteriaArea.Controllers
{
    public class DeclareEvaluationCriteriaController : BaseController
    {
        #region properties
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        #endregion

        #region Constructor
        public DeclareEvaluationCriteriaController(
            IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness,
            IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness,
            IGrowthEvaluationBusiness GrowthEvaluationBusiness
            )
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
            this.EvaluationDevelopmentBusiness = EvaluationDevelopmentBusiness;
            this.GrowthEvaluationBusiness = GrowthEvaluationBusiness;
        }
        #endregion

        //
        // GET: /DeclareEvaluationCriteriaArea/DeclareEvaluationCriteria/

        #region // viewIndex 
        public ActionResult Index(int? ClassID, int? EvaluationGroup)
        {
            SetData(ClassID, EvaluationGroup);
            return View();
        }
        #endregion

        #region // LoadClass
        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int educationId)
        {
            if (educationId <= 0)
                return Json(new List<SelectListItem>());

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", educationId);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
        
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
            }
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(x => x.DisplayName).ToList();

            var lstResult = lstClass.Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();
            return Json(lstResult);
        }
        #endregion

        #region // LoadEvaluationGroup
        [ValidateAntiForgeryToken]
        public JsonResult LoadEvaluationGroup(int educationId)
        {
            if (educationId <= 0)
                return Json(new List<SelectListItem>());

            List<EvaluationDevelopmentGroup> lstEvaluationGroup = GetEvaluationDevelopmentGroup(educationId);
            var lstResult = lstEvaluationGroup.Select(u => new SelectListItem { Value = u.EvaluationDevelopmentGroupID.ToString(), Text = u.EvaluationDevelopmentGroupName, Selected = false }).ToList();
            return Json(lstResult);
        }
        #endregion

        #region // LoadGrid
        public PartialViewResult AjaxLoadGrid(int educationId, int? classId, int? evaluationGroupID)
        {
            List<DeclareEvaluationCriteriaViewModel> lstResult = GetDeclareEvaluationCriteriaVM(educationId, classId, evaluationGroupID);
            return PartialView("_List", lstResult);
        }
        #endregion

        #region // Get data multi class
        public PartialViewResult AjaxLoadFormMultiClass(int educationId)
        {
            List<EducationLevel> lstEducation = ListEducationLevel().Where(x => x.EducationLevelID == educationId).ToList();
            List<ClassProfile> lstClass = new List<ClassProfile>();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (lstEducation.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["EducationLevelID"] = educationId;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                lstClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic).Where(x => x.IsActive.HasValue && x.IsActive.Value == true)
                                                                            .OrderBy(x=>x.OrderNumber)
                                                                            .ThenBy(x => x.DisplayName).ToList();
            }

            List<ClassViewModel> lstClassVM = new List<ClassViewModel>();
            for (int i = 0; i < lstClass.Count(); i++)
            {
                lstClassVM.Add(new ClassViewModel()
                {
                    ClassID = lstClass[i].ClassProfileID,
                    ClassName = lstClass[i].DisplayName,
                    EducationLevelID = lstClass[i].EducationLevelID,
                    OrderID = lstClass[i].OrderNumber
                });
            }
            lstClassVM = lstClassVM.OrderBy(x => x.OrderID).ThenBy(x => x.ClassName).ToList();

            ViewData[DeclareEvaluationCriteriaConstant.LIST_CLASS_FORM_CREATE] = lstClassVM;
            ViewData[DeclareEvaluationCriteriaConstant.LIST_EDUCATION_LEVEL_FORM_CREATE] = lstEducation;
            return PartialView("_FormMultiClass");
        }
        #endregion

        #region // Apply
        [ValidateAntiForgeryToken]
        public JsonResult AppliedEvaluationForClass(
            int educationLevelID, string arrayClassID, string arrayDeclareEvaluationIndexIDApply, string arrayDeclareEvaluationIndexIDDelete)
        {
            string messenger = string.Empty;
            if (!_globalInfo.IsCurrentYear)
            {
                messenger = "Thầy/cô không được khai báo chỉ số cho lớp của năm học cũ.";
                return Json(new JsonMessage(messenger, "error"));
            }

            bool isAdmin = false;
            
            if (string.IsNullOrEmpty(arrayClassID))
            {
                messenger = "Thầy/cô vui lòng chọn lớp học.";
                return Json(new JsonMessage(messenger, "error"));
            }

            DeclareEvaluationGroup objDeclareGroup = DeclareEvaluationGroupBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                                && x.SchoolID == _globalInfo.SchoolID
                                                                                                && x.EducationLevelID == educationLevelID).FirstOrDefault();
            // true: locked, false: unlocked
            bool? isLocked = objDeclareGroup != null ? objDeclareGroup.IsLocked : true;
            if (!isLocked.HasValue)
            {
                isLocked = true;
            }

            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
            {
                isAdmin = false;
            }
            else
            {
                isAdmin = true;
            }

            if (!isLocked.Value || isAdmin)
            {
                List<int> lstClassID = arrayClassID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                // danh sach cac chi so duoc ap dung
                List<int> lstDeclareEducatioIndexIDApply = arrayDeclareEvaluationIndexIDApply.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                                                                .Select(p => int.Parse(p)).Distinct().ToList();
                // danh sach cac chi so se xoa
                List<int> lstDeclareEducatioIndexIDDelete = arrayDeclareEvaluationIndexIDDelete.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                                                               .Select(p => int.Parse(p)).Distinct().ToList();

                #region // GetData
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    if (lstClassID.Count() > 1)
                    {
                        messenger = "Thầy/cô không được khai báo chỉ số cho nhiều lớp.";
                        return Json(new JsonMessage(messenger, "error"));
                    }
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                ClassProfileBusiness.SetAutoDetectChangesEnabled(false);
                List<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .Where(x => lstClassID.Contains(x.ClassProfileID))
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();
                ClassProfileBusiness.SetAutoDetectChangesEnabled(true);
                if (listClass.Count() > 0)
                {
                    DeclareEvaluationIndexBusiness.SetAutoDetectChangesEnabled(false);
                    DeclareEvaluationGroupBusiness.SetAutoDetectChangesEnabled(false);
                    EvaluationDevelopmentGroupBusiness.SetAutoDetectChangesEnabled(false);
                    EvaluationDevelopmentBusiness.SetAutoDetectChangesEnabled(false);
                    GrowthEvaluationBusiness.SetAutoDetectChangesEnabled(false);

                    IQueryable<EvaluationDevelopment> queryED = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true
                                                                                                    && x.EducationLevelID == educationLevelID);
                    var checkQueryEDInProvince = queryED.Where(x => x.ProvinceID == _globalInfo.ProvinceID).FirstOrDefault();
                    if (checkQueryEDInProvince != null)
                    {
                        queryED = queryED.Where(x => x.ProvinceID == _globalInfo.ProvinceID);
                    }

                    IQueryable<DeclareEvaluationIndex> queryDEI = DeclareEvaluationIndexBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID
                                                                                    && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                    && x.EducationLevelID == educationLevelID);

                    List<DeclareEvaluationIndex> lstDEI_Delete = new List<DeclareEvaluationIndex>();

                    lstClassID = listClass.Select(x => x.ClassProfileID).ToList();
                    if (lstDeclareEducatioIndexIDDelete.Count() > 0)
                    {
                        #region // Kiểm tra xem các chỉ số khai báo đã được sử dụng hay chưa? thì mới có quyền xóa
                        var queryDEI_Delete = queryDEI.Where(x => lstDeclareEducatioIndexIDDelete.Contains(x.DeclareEvaluationIndexID)).ToList();
                        var temptDelete = queryDEI_Delete.Where(x=>x.ClassID.HasValue && lstClassID.Contains(x.ClassID.Value)).ToList();
                        List<int> lstEvaluationDevelopmentID_Delete = queryDEI_Delete.Select(x => x.EvaluationDevelopmentID).ToList();

                        var temptDeleteOther = queryDEI.Where(x => x.ClassID.HasValue && lstClassID.Contains(x.ClassID.Value)
                                                                && lstEvaluationDevelopmentID_Delete.Contains(x.EvaluationDevelopmentID)
                                                                && !lstDeclareEducatioIndexIDDelete.Contains(x.DeclareEvaluationIndexID)).ToList();

                        if (temptDeleteOther.Count() > 0)
                        {
                            temptDelete.AddRange(temptDeleteOther);
                        }

                        List<GrowthEvaluation> lstGrowthEvaluation = GrowthEvaluationBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID
                                                                                                    && x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                                    && x.LastDigitSchoolID == (_globalInfo.SchoolID % 100)
                                                                                                    && lstClassID.Contains(x.ClassID)
                                                                                                    && lstEvaluationDevelopmentID_Delete.Contains(x.DeclareEvaluationIndexID))
                                                                                                    .ToList();
                        if (lstGrowthEvaluation.Count() > 0)
                        {
                            // danh sach ma chi so dang duoc su dung
                            List<int> lstEvaluationDevelopmentIDUsing = lstGrowthEvaluation.Select(o => o.DeclareEvaluationIndexID).ToList();
                            var lstEvaluationApplied = queryED.Where(x => lstEvaluationDevelopmentIDUsing.Contains(x.EvaluationDevelopmentID)).ToList();
                            List<string> lstCode = lstEvaluationApplied.Select(x => x.EvaluationDevelopmentCode).Distinct().ToList();
                            messenger = "Chỉ số " + string.Join(", ", lstCode.ToArray()) + " đã được sử dụng.";
                            return Json(new JsonMessage(messenger, "error"));
                        }

                        DeclareEvaluationIndexBusiness.DeleteAll(temptDelete);
                        DeclareEvaluationIndexBusiness.Save();
                        #endregion
                    }

                    IQueryable<DeclareEvaluationIndex> queryDEI_Apply = queryDEI.Where(x => lstDeclareEducatioIndexIDApply.Contains(x.DeclareEvaluationIndexID));

                    IQueryable<DeclareEvaluationGroup> queryDEG = DeclareEvaluationGroupBusiness.All.Where(x => educationLevelID == x.EducationLevelID
                                                                                                            && x.SchoolID == _globalInfo.SchoolID
                                                                                                            && x.AcademicYearID == _globalInfo.AcademicYearID);

                    IQueryable<EvaluationDevelopmentGroup> queryEDG = EvaluationDevelopmentGroupBusiness.All.Where(x => x.IsActive == true
                                                                                                                    && x.AppliedLevel == _globalInfo.AppliedLevel
                                                                                                                    && x.EducationLevelID == educationLevelID);

                    var checkqueryEDGInProvince = queryEDG.Where(x => x.ProvinceID == _globalInfo.ProvinceID).FirstOrDefault();
                    if (checkqueryEDGInProvince != null)
                    {
                        queryEDG = queryEDG.Where(x => x.ProvinceID == _globalInfo.ProvinceID);
                    }

                    var lstDeclareEvaluationIndex = (from a in queryDEG
                                                     join b in queryEDG on a.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                                                     join c in queryED on b.EvaluationDevelopmentGroupID equals c.EvaluationDevelopmentGroupID
                                                     join d in queryDEI_Apply on c.EvaluationDevelopmentID equals d.EvaluationDevelopmentID
                                                     select d).ToList();

                    if (lstDeclareEvaluationIndex.Count() > 0 && lstDeclareEducatioIndexIDApply.Count() > 0)
                    {
                        List<DeclareEvaluationIndexBO> lstInsert = new List<DeclareEvaluationIndexBO>();
                        // Áp dụng chỉ số cho lớp                     
                        for (int i = 0; i < listClass.Count(); i++)
                        {
                            for (int j = 0; j < lstDeclareEvaluationIndex.Count(); j++)
                            {
                                var objIndex = lstDeclareEvaluationIndex[j];

                                if (objIndex.ClassID.HasValue)
                                {
                                    if (objIndex.ClassID == listClass[i].ClassProfileID) // Bỏ qua chỉ số đã được áp dụng cho lớp
                                    {
                                        continue;
                                    }
                                }

                                DeclareEvaluationIndexBO objNew = new DeclareEvaluationIndexBO();
                                objNew.ClassID = listClass[i].ClassProfileID;
                                objNew.CreateDate = DateTime.Now;
                                objNew.SchoolID = _globalInfo.SchoolID.Value;
                                objNew.AcademicYearID = _globalInfo.AcademicYearID.Value;
                                objNew.EducationID = lstDeclareEvaluationIndex[j].EducationLevelID;
                                objNew.EvaluationDevGroupID = lstDeclareEvaluationIndex[j].EvaluationDevelopmentGroID;
                                objNew.EvaluationDevIndexID = lstDeclareEvaluationIndex[j].EvaluationDevelopmentID;
                                objNew.Description = lstDeclareEvaluationIndex[j].Note;

                                lstInsert.Add(objNew);
                            }
                        }
                        IDictionary<string, object> dicInsert = new Dictionary<string, object>();
                        dicInsert.Add("EducationLevelID", educationLevelID);
                        dicInsert.Add("SchoolID", _globalInfo.SchoolID.Value);
                        dicInsert.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                        DeclareEvaluationIndexBusiness.InsertOrUpdate(lstInsert, dicInsert);     
                    }

                    DeclareEvaluationIndexBusiness.SetAutoDetectChangesEnabled(true);
                    DeclareEvaluationGroupBusiness.SetAutoDetectChangesEnabled(true);
                    EvaluationDevelopmentGroupBusiness.SetAutoDetectChangesEnabled(true);
                    EvaluationDevelopmentBusiness.SetAutoDetectChangesEnabled(true);
                    GrowthEvaluationBusiness.SetAutoDetectChangesEnabled(true);

                    messenger = "Thầy/cô khai báo chỉ số cho lớp thành công";
                    return Json(new JsonMessage(messenger, "success"));
                }
                else
                {
                    messenger = "Lớp áp dụng không tồn tại";
                    return Json(new JsonMessage(messenger, "error"));
                }
            }
                #endregion

            messenger = "Thầy/cô không được cho phép khai báo chỉ số cho lớp.";
            return Json(new JsonMessage(messenger, "error"));
        }
        #endregion

        #region // Get data

        private List<DeclareEvaluationCriteriaViewModel> GetDeclareEvaluationCriteriaVM(int educationId, int? classId, int? evaluationGroupID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationId;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            IQueryable<DeclareEvaluationGroup> lstDEG = DeclareEvaluationGroupBusiness.SearchByClass(educationId, dic);
            IQueryable<DeclareEvaluationIndex> lstDEI = DeclareEvaluationIndexBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID.Value &&
                                                                        x.SchoolID == _globalInfo.SchoolID.Value && x.EducationLevelID == educationId);
            IQueryable<EvaluationDevelopmentGroup> lstEDG = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == _globalInfo.AppliedLevel &&
                                                                                                         x.EducationLevelID == educationId);
            IQueryable<EvaluationDevelopment> lstED = EvaluationDevelopmentBusiness.All.Where(x => x.IsActive == true && x.EducationLevelID == educationId);

            if (evaluationGroupID.HasValue && evaluationGroupID.Value > 0)
            {
                lstEDG = lstEDG.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
                lstED = lstED.Where(x => x.EvaluationDevelopmentGroupID == evaluationGroupID);
            }

            var lstResult = (from a in lstDEG
                             join b in lstEDG on a.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                             join c in lstED on b.EvaluationDevelopmentGroupID equals c.EvaluationDevelopmentGroupID
                             join dei in lstDEI on c.EvaluationDevelopmentID equals dei.EvaluationDevelopmentID
                             select new { a = a, b = b, c = c, dei = dei }).ToList()
                            .Select(x => new DeclareEvaluationCriteriaViewModel()
                            {
                                EvaluationCode = x.c.EvaluationDevelopmentCode,
                                EvaluationName = x.c.EvaluationDevelopmentName,
                                EvaluationGroupName = x.b.EvaluationDevelopmentGroupName,
                                EvaluationGroupID = x.b.EvaluationDevelopmentGroupID,
                                EvaluationDevelopmentID = x.c.EvaluationDevelopmentID,
                                DeclareEducatioIndexID = x.dei.DeclareEvaluationIndexID,
                                OrderEvaluationGroup = x.b.OrderID,
                                OrderEvaluation = x.c.OrderID,
                                EducationLevelID = x.dei.EducationLevelID,
                                ClassID = x.dei.ClassID,
                                IsCheck = x.dei.ClassID.HasValue ? true : false
                            }).OrderBy(p => p.OrderEvaluationGroup).ThenBy(p => p.OrderEvaluation).ToList();

            if (classId.HasValue)
            {
                lstResult = lstResult.Where(x => x.ClassID == classId || !x.ClassID.HasValue).ToList();

                var lstDeclareEvaluationForClass = lstResult.Where(x => x.ClassID.HasValue).ToList();
                for (int i = 0; i < lstDeclareEvaluationForClass.Count(); i++)
                {
                    var tempt = lstResult.Where(x => !x.ClassID.HasValue && (x.EvaluationCode == lstDeclareEvaluationForClass[i].EvaluationCode)
                                                && x.EvaluationName == lstDeclareEvaluationForClass[i].EvaluationName).ToList();

                    if (tempt.Count() > 0)
                    {
                        for(int j = 0; j < tempt.Count(); j++)
                        {
                            lstResult.Remove(tempt[j]);
                        }  
                    }
                }
            }
            else
            {
                lstResult = lstResult.Where(x => !x.ClassID.HasValue || x.ClassID == null).ToList();
            }

            return lstResult;
        }

        private void SetData(int? ClassID, int? EvaluationGroup)
        {
            bool IsHeaderTeaching = false;
            ClassProfile objClass = null;
            List<EducationLevel> lstEducation = ListEducationLevel();
            List<int> lstEducationLevelID = lstEducation.Select(x => x.EducationLevelID).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            int? defaultEducationLevelID = null;
            if (lstEducation.Count > 0)
            {
                defaultEducationLevelID = lstEducation.FirstOrDefault().EducationLevelID;
                bool isAdmin = true;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    IsHeaderTeaching = true;
                    isAdmin = false;
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();
                if (ClassID.HasValue)
                {
                    objClass = listClass.Where(x => x.ClassProfileID == ClassID).FirstOrDefault();
                }

                if (!isAdmin)
                {
                    var lstClass = listClass.ToList();
                    lstEducationLevelID = lstClass.Count() > 0 ? lstClass.Select(x => x.EducationLevelID).ToList() : new List<int>();
                    lstEducation = lstEducation.Where(x => lstEducationLevelID.Contains(x.EducationLevelID)).ToList();
                }

                if (objClass != null)
                {
                    defaultEducationLevelID = objClass.EducationLevelID;
                }
                else
                {
                    defaultEducationLevelID = lstEducation.Count() > 0 ? lstEducation.FirstOrDefault().EducationLevelID : 0;
                }
                listClass = listClass.Where(x => x.EducationLevelID == defaultEducationLevelID);

                ViewData[DeclareEvaluationCriteriaConstant.LIST_CLASS] = new SelectList(listClass, "ClassProfileID", "DisplayName", (objClass != null ? objClass.ClassProfileID : 0));
            }

            ViewData[DeclareEvaluationCriteriaConstant.LIST_EDUCATION_LEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution", (objClass != null ? objClass.EducationLevelID : 0));

            List<EvaluationDevelopmentGroup> lstEvaluationGroup = new List<EvaluationDevelopmentGroup>();
            if (defaultEducationLevelID != null)
            {
                lstEvaluationGroup = GetEvaluationDevelopmentGroup(defaultEducationLevelID.Value);
            }
            else
            {
                lstEvaluationGroup = new List<EvaluationDevelopmentGroup>();
            }

            ViewData[DeclareEvaluationCriteriaConstant.LIST_EVALUATION_GROUP] = new SelectList(lstEvaluationGroup, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName", (EvaluationGroup.HasValue && EvaluationGroup.Value > 0) ? EvaluationGroup.Value : 0);

            ViewData[DeclareEvaluationCriteriaConstant.IS_HEADER_TEACHING] = IsHeaderTeaching;
        }

        private List<EvaluationDevelopmentGroup> GetEvaluationDevelopmentGroup(int educationLevel)
        {
            List<EvaluationDevelopmentGroup> lstEvaluationGroup = new List<EvaluationDevelopmentGroup>();

            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["SchoolID"] = _globalInfo.SchoolID;
            dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicSearch["EducationLevelID"] = educationLevel;
            dicSearch["AppliedLevel"] = _globalInfo.AppliedLevel;
            lstEvaluationGroup = DeclareEvaluationIndexBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dicSearch).OrderBy(p => p.OrderID.HasValue ? p.OrderID : 0).ToList();

            lstEvaluationGroup = (from oik in lstEvaluationGroup
                                  join b in DeclareEvaluationGroupBusiness.All on oik.EvaluationDevelopmentGroupID equals b.EvaluationGroupID
                                  where b.AcademicYearID == _globalInfo.AcademicYearID && b.SchoolID == _globalInfo.SchoolID && b.EducationLevelID == educationLevel
                                  && oik.IsActive
                                  select oik).ToList();

            return lstEvaluationGroup;
        }

        private List<EducationLevel> ListEducationLevel()
        {
            return EducationLevelBusiness.All.Where(x => (x.Grade == 4 || x.Grade == 5) && x.IsActive == true)
                                            .Where(x => x.Grade == _globalInfo.AppliedLevel)
                                            .OrderBy(x => x.EducationLevelID).ToList();
        }
        #endregion
    }
}
