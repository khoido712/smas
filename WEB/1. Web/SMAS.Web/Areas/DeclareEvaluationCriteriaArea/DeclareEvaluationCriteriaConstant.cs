﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationCriteriaArea
{
    public class DeclareEvaluationCriteriaConstant
    {
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_EVALUATION_GROUP = "LIST_EVALUATION_GROUP";
        public const string LIST_RESULT = "LIST_RESULT";

        public const string LIST_EDUCATION_LEVEL_FORM_CREATE = "LIST_EDUCATION_LEVEL_FORM_CREATE";
        public const string LIST_CLASS_FORM_CREATE = "LIST_CLASS_FORM_CREATE";

        public const string IS_HEADER_TEACHING = "IS_HEADER_TEACHING";
    }
}