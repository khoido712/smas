﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationCriteriaArea.Models
{
    public class DeclareEvaluationCriteriaViewModel
    {
        public string EvaluationCode { get; set; }
        public string EvaluationName { get; set; }
        public string EvaluationGroupName { get; set; }
        public int? OrderEvaluationGroup { get; set; }
        public int? OrderEvaluation { get; set; }
        public int EvaluationDevelopmentID { get; set; }
        public int EvaluationGroupID { get; set; }
        public bool IsCheck { get; set; }
        public int? ClassID { get; set; }
        public int DeclareEducatioGroupID { get; set; }
        public int DeclareEducatioIndexID { get; set; }
        public int EducationLevelID { get; set; }
    }

    public class ClassViewModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int? OrderID { get; set; }
        public int EducationLevelID { get; set; }
    }

}