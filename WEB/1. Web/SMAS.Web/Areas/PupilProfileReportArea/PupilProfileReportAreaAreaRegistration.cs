﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea
{
    public class PupilProfileReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilProfileReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilProfileReportArea_default",
                "PupilProfileReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
