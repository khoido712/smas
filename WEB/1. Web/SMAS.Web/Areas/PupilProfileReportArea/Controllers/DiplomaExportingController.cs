﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.PupilProfileReportArea.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.Business.Common;
using System.Web.Script.Serialization;
using System.Configuration;
using Rotativa.Options;
using System.Globalization;
using Rotativa;
using System.Text;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class DiplomaExportingController : BaseController
    {
        #region Declare variables
        private readonly IDiplomaTemplateBusiness DiplomaTemplateBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPraiseTypeBusiness PraiseTypeBusiness;
        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly IEvaluationRewardBusiness EvaluationRewardBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilEmulationBusiness PupilEmulationBusiness;
        private readonly IUpdateRewardBusiness UpdateRewardBusiness;
        private readonly IPupilPraiseBusiness PupilPraiseBusiness;
        private readonly IDiplomaImageBusiness DiplomaImageBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IRewardFinalBusiness RewardFinalBusiness;
        #endregion

        #region Constructor
        public DiplomaExportingController(IDiplomaTemplateBusiness DiplomaTemplateBusiness, IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, IPraiseTypeBusiness PraiseTypeBusiness, ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness,
            IEvaluationRewardBusiness EvaluationRewardBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IPupilProfileBusiness PupilProfileBusiness,
            IPupilEmulationBusiness PupilEmulationBusiness, IUpdateRewardBusiness UpdateRewardBusiness, IPupilPraiseBusiness PupilPraiseBusiness,
            ITemplateBusiness TemplateBusiness, IDiplomaImageBusiness DiplomaImageBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IRewardFinalBusiness RewardFinalBusiness)
        {
            this.DiplomaTemplateBusiness = DiplomaTemplateBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PraiseTypeBusiness = PraiseTypeBusiness;
            this.SummedEndingEvaluationBusiness = SummedEndingEvaluationBusiness;
            this.EvaluationRewardBusiness = EvaluationRewardBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilEmulationBusiness = PupilEmulationBusiness;
            this.UpdateRewardBusiness = UpdateRewardBusiness;
            this.PupilPraiseBusiness = PupilPraiseBusiness;
            this.DiplomaImageBusiness = DiplomaImageBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.RewardFinalBusiness = RewardFinalBusiness;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        [HttpPost]
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);


            IEnumerable<ListViewModel> iq = _Search(form.EducationLevelID, form.ClassID, form.Semester.Value, form.Type, form.Appellation);
            int totalRecord = iq.Count();
            ViewData[PupilProfileReportConstants.TOTAL] = totalRecord;

            List<ListViewModel> listResult;
            if (form.ClassID.HasValue && form.ClassID.Value != 0)
            {
                listResult = iq.ToList();
                ViewData[PupilProfileReportConstants.ENABLE_PAGING] = false;
            }
            else
            {
                listResult = iq.Take(PupilProfileReportConstants.PageSize).ToList();
                ViewData[PupilProfileReportConstants.ENABLE_PAGING] = true;
            }
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);


            IEnumerable<ListViewModel> iq = _Search(form.EducationLevelID, form.ClassID, form.Semester.Value, form.Type, form.Appellation);
            int totalRecord = iq.Count();
            ViewData[PupilProfileReportConstants.TOTAL] = totalRecord;

            List<ListViewModel> listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            return View(new GridModel<ListViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveTemplate(int? templateId, string templateName, string html)
        {
            templateName = templateName.Trim();
            if (string.IsNullOrEmpty(templateName))
            {
                throw new BusinessException("Tên mẫu giấy khen là bắt buộc");
            }

            if (templateName.Length > 100)
            {
                throw new BusinessException(Res.Get("Common_Validate_MaxLength"));
            }

            DiplomaTemplate template = DiplomaTemplateBusiness.Find(templateId);
            if (template != null)
            {
                if (DiplomaTemplateBusiness.All.Where(o => o.IsSystemTemplate == false && o.DiplomaTemplateID != templateId && o.SchoolID == _globalInfo.SchoolID && o.TemplateName == templateName && o.Type == 1).Count() > 0)
                {
                    throw new BusinessException("Tên mẫu giấy khen không được trùng với tên mẫu giấy khen đã có");
                }

                template.TemplateName = templateName;
                template.HtmlTemplate = html;
                template.ModifiedDate = DateTime.Now;
                DiplomaTemplateBusiness.Update(template);
            }
            else
            {
                if (DiplomaTemplateBusiness.All.Where(o => o.IsSystemTemplate == false && o.SchoolID == _globalInfo.SchoolID && o.TemplateName == templateName && o.Type == 1).Count() > 0)
                {
                    throw new BusinessException("Tên mẫu giấy khen không được trùng với tên mẫu giấy khen đã có");
                }

                template = new DiplomaTemplate
                {
                    CreateDate = DateTime.Now,
                    HtmlTemplate = html,
                    IsSystemTemplate = false,
                    ModifiedDate = null,
                    SchoolID = _globalInfo.SchoolID.Value,
                    TemplateName = templateName,
                    Type = 1
                };

                DiplomaTemplateBusiness.Insert(template);

            }

            DiplomaTemplateBusiness.Save();

            return Json(new JsonMessage { Type = SMAS.Web.Constants.GlobalConstants.TYPE_SUCCESS, Message = "Lưu mẫu giấy khen thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteTemplate(int? templateId)
        {
            DiplomaTemplateBusiness.Delete(templateId);

            DiplomaTemplateBusiness.Save();

            return Json(new JsonMessage { Type = SMAS.Web.Constants.GlobalConstants.TYPE_SUCCESS, Message = "Xóa mẫu giấy khen thành công" });
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            try
            {
                int maxSize = 1 * 1024 * 1024;
                if (file.ContentLength > maxSize)
                {
                    throw new BusinessException("Dung lượng file không được quá 1 MB");
                }

                string[] arr = file.FileName.Split('.');
                string extension = arr[arr.Length - 1];
                if (!"PNG".Equals(extension.ToUpper()) && !"JPG".Equals(extension.ToUpper()) && !"JPEG".Equals(extension.ToUpper()))
                {
                    throw new BusinessException(string.Format("Hệ thống không hỗ trợ up file có định dạng {0}.", extension));
                }

                byte[] _Buffer = null;

                using (BinaryReader _BinaryReader = new BinaryReader(file.InputStream))
                {
                    _Buffer = _BinaryReader.ReadBytes(file.ContentLength);
                }

                DiplomaImage image = new DiplomaImage();

                image.DiplomaImageID = DiplomaImageBusiness.GetNextSeq<int>("DIPLOMA_IMAGE_SEQ");
                image.DiplomaTemplateID = 0;
                image.SchoolID = _globalInfo.SchoolID.Value;
                image.ToByte = _Buffer;

                image = DiplomaImageBusiness.Insert(image);
                DiplomaImageBusiness.Save();



                string url = Url.Action("ShowImage", new { id = image.DiplomaImageID });

                return Json(new { URL = url });
            }
            catch (Exception e)
            {
                if (e is BusinessException)
                {
                    throw e;
                }
                else
                {
                    throw new BusinessException("Upload ảnh không thành công");
                }
            }
        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ShowImage(int? id)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/default.png");
            DiplomaImage image = DiplomaImageBusiness.Find(id);
            byte[] imageData;
            if (image != null)
            {
                if (image.ToByte != null)
                {
                    imageData = image.ToByte;
                    return File(imageData, "image/jpg");
                }
                else
                {
                    imageData = FileToByteArray(defaultImagePath);
                    return File(imageData, "image/jpg");
                }
            }

            //neu ko co tra ve anh default
            imageData = FileToByteArray(defaultImagePath);
            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");

        }

        public JsonResult AjaxLoadClass(int? paraEducationLevelID)
        {
            List<ClassProfile> listClass = new List<ClassProfile>();

            if (paraEducationLevelID.HasValue)
            {
                IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
                dicToGetClass["EducationLevelID"] = paraEducationLevelID;
                dicToGetClass["AcademicYearID"] = _globalInfo.AcademicYearID;
                listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetClass).ToList();
            }

            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadDiplomaTemplateID(int? paraEducationLevelID)
        {
            List<DiplomaTemplate> lst = GetListTemplate();

            return Json(new SelectList(lst, "DiplomaTemplateID", "TemplateName"));
        }

        public PartialViewResult Design(int? id)
        {
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();
            lstDiplomaTemplate.Insert(0, new DiplomaTemplate { TemplateName = "[Thêm mới]", DiplomaTemplateID = 0 });
            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName", id);
            ViewData[PupilProfileReportConstants.SCHOOL_PROFILE] = sp;

            return PartialView("_Design");

        }

        public PartialViewResult CopyDialog()
        {
            //Lay danh sach mau giay khen
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();

            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName");

            return PartialView("_Copy");
        }

        public PartialViewResult AddBgDialog()
        {

            return PartialView("_AddBackground");
        }

        [HttpPost]
        public JsonResult LoadTemplate(int id)
        {
            //Lay danh sach mau giay khen
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(id);
            string html = string.Empty;
            if (template != null)
            {
                html = template.HtmlTemplate;
            }

            return Json(new { HTML = html, Name = template.TemplateName, IsSystem = template.IsSystemTemplate });
        }

        [HttpPost]
        public JsonResult CopyFrom(int CopyFromID)
        {
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(CopyFromID);
            string html = string.Empty;
            if (template != null)
            {
                html = template.HtmlTemplate;
            }

            return Json(new { HTML = html });
        }


        public JsonResult GetBackground(int TemplateType, HttpPostedFileBase file)
        {
            try
            {
                string defaultImagePath = Server.MapPath("~/Content/images/phoigiaykhen/");
                byte[] _Buffer = null;

                if (TemplateType == 1)
                {
                    defaultImagePath = defaultImagePath + "bg-1.png";
                }
                else if (TemplateType == 2)
                {
                    defaultImagePath = defaultImagePath + "bg-2.png";
                }
                else if (TemplateType == 3)
                {
                    defaultImagePath = defaultImagePath + "bg-3.png";
                }
                else if (TemplateType == 4)
                {
                    if (file == null)
                    {
                        throw new BusinessException("Thầy cô chưa chọn file ảnh");
                    }

                    int maxSize = 5 * 1024 * 1024;
                    if (file.ContentLength > maxSize)
                    {
                        throw new BusinessException("Dung lượng file không được quá 5 MB");
                    }

                    string[] arr = file.FileName.Split('.');
                    string extension = arr[arr.Length - 1];
                    if (!"PNG".Equals(extension.ToUpper()) && !"JPG".Equals(extension.ToUpper()) && !"JPEG".Equals(extension.ToUpper()))
                    {
                        throw new BusinessException(string.Format("Hệ thống không hỗ trợ up file có định dạng {0}.", extension));
                    }
                }

                if (TemplateType == 4)
                {
                    using (BinaryReader _BinaryReader = new BinaryReader(file.InputStream))
                    {
                        _Buffer = _BinaryReader.ReadBytes(file.ContentLength);
                    }
                }
                else
                {
                    _Buffer = FileToByteArray(defaultImagePath);
                }

                DiplomaImage image = new DiplomaImage();

                image.DiplomaImageID = DiplomaImageBusiness.GetNextSeq<int>("DIPLOMA_IMAGE_SEQ");
                image.DiplomaTemplateID = 0;
                image.SchoolID = _globalInfo.SchoolID.Value;
                image.ToByte = _Buffer;

                image = DiplomaImageBusiness.Insert(image);
                DiplomaImageBusiness.Save();


                string url = Url.Action("ShowImage", new { id = image.DiplomaImageID });

                return Json(new { URL = url });

            }
            catch (Exception e)
            {
                if (e is BusinessException)
                {
                    throw e;
                }
                else
                {
                    throw new BusinessException("Upload ảnh không thành công");
                }
            }
        }

        [HttpPost]
        public JsonResult SaveExport(List<ListViewModel> lstModel, bool exportAll, SearchViewModel form)
        {
            if (form.DiplomaTemplateID == null)
            {
                throw new BusinessException("Thầy/cô chưa chọn mẫu giấy khen");
            }
            if (exportAll)
            {
                lstModel = _Search(form.EducationLevelID, form.ClassID, form.Semester.Value, form.Type, form.Appellation).ToList();

            }

            if (lstModel == null || (lstModel != null && lstModel.Count == 0))
            {
                throw new BusinessException("Thầy/cô chưa chọn học sinh để in giấy khen");
            }

            Session["ListToExport"] = lstModel;

            return Json(new JsonMessage { Type = "success" });
        }

        [HttpGet]
        public ActionResult ExportPDF(int templateId, bool exportAll, bool withBackground, SearchViewModel form)
        {
            List<ListViewModel> lstModel;
            if (exportAll)
            {
                lstModel = _Search(form.EducationLevelID, form.ClassID, form.Semester.Value, form.Type, form.Appellation).ToList();

            }
            else
            {
                lstModel = (List<ListViewModel>)Session["ListToExport"];
            }

            if (lstModel == null || (lstModel != null && lstModel.Count == 0))
            {
                throw new BusinessException("Thầy/cô chưa chọn học sinh để in giấy khen");
            }
            //get template
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(templateId);

            string htmlTemplate = UnescapeHTML(template.HtmlTemplate);

            //replace image path
            htmlTemplate = htmlTemplate.Replace("../../PupilProfileReportArea", "/PupilProfileReportArea");
            string localPath = ConfigurationManager.AppSettings["urlLocalHost"];
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/DiplomaExporting/ShowImage", localPath + "/PupilProfileReportArea/DiplomaExporting/ShowImage");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < lstModel.Count; i++)
            {
                string eachHtml = htmlTemplate;
                ListViewModel model = lstModel[i];
                model.ClassName = model.ClassName.Replace("LỚP", string.Empty);
                model.ClassName = model.ClassName.Replace("Lớp", string.Empty);
                model.ClassName = model.ClassName.Replace("lớp", string.Empty);
                model.ClassName = model.ClassName.Trim();

                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);

                string semester;
                if (_globalInfo.Semester == 1)
                {
                    semester = "Học kỳ I";
                }
                else
                {
                    semester = "Học kỳ II";
                }

                AcademicYear academic = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                string YearName = string.Format("{0} - {1}", academic.Year, academic.Year + 1);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_NAME, model.PupilName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_CLASS_NAME, model.ClassName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_CONTENT, model.Content);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_NAME, sp.HeadMasterName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SCHOOL_NAME, sp.SchoolName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SEMESTER, semester);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_ACADEMIC_YEAR, YearName);


                sb.Append(eachHtml);

                if (i < lstModel.Count - 1)
                {
                    sb.Append("<p class=\"breakhere\"></p>");
                }

            }

            Margins margins = new Margins(0, 0, 0, 0);

            string customSwitches = "--zoom 1.754";

            Session.Remove("ListToExport");

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["HtmlContent"] = sb.ToString();
            dic["WithBackground"] = withBackground;
            return new ViewAsPdf("_Export", new { })
            {
                FileName = "Giay_Khen.pdf",
                PageOrientation = Rotativa.Options.Orientation.Landscape,
                PageSize = Rotativa.Options.Size.A4,
                PageMargins = margins,
                CustomSwitches = customSwitches,
                Model = dic
            }
            ;
            //return View("_Export", (object)sb.ToString());
        }
        #endregion

        #region Methods
        private void SetViewData()
        {
            //Lay danh sach mau giay khen
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();

            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName");

            //Lay danh sach khoi
            ViewData[PupilProfileReportConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            ViewData[PupilProfileReportConstants.CBO_CLASS] = new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName");

            //Danh sach hoc ky
            List<ComboObject> lstSemester = new List<ComboObject>()
            {
                new ComboObject{key = "1", value = "Học kỳ I"},
                new ComboObject{key = "2", value= "Học kỳ II"},
                new ComboObject{key = "3", value = "Cả năm"}
            };

            ViewData[PupilProfileReportConstants.CBO_SEMESTER] = new SelectList(lstSemester, "key", "value", _globalInfo.Semester.GetValueOrDefault());

            //Hinh thuc
            List<ComboObject> lstType = new List<ComboObject>();
            if (_globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SMAS.Business.Common.GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                lstType.Add(new ComboObject { key = "-1", value = "Khen thưởng cuối kỳ" });
                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                if (sp.IsNewSchoolModel.HasValue && sp.IsNewSchoolModel.Value == true)
                {
                    lstType.Add(new ComboObject { key = "-2", value = "Khen thưởng cuối kỳ (VNEN)" });
                }
            }
            else
            {
                lstType.Add(new ComboObject { key = "-1", value = "Khen thưởng cuối năm" });
                lstType.Add(new ComboObject { key = "-2", value = "Khen thưởng đột xuất" });
                lstType.Add(new ComboObject { key = "-3", value = "Học sinh có thành tích vượt trội" });
            }

            //lay ra cac danh muc khen thuong
            List<PraiseType> lstPraiseType = PraiseTypeBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } }).ToList();

            lstType.AddRange(lstPraiseType.Select(o => new ComboObject { key = o.PraiseTypeID.ToString(), value = o.Resolution }).ToList());

            ViewData[PupilProfileReportConstants.CBO_TYPE] = new SelectList(lstType, "key", "value");

            //Danh hieu
            List<ComboObject> lstAppellation = new List<ComboObject>()
            {
                new ComboObject{key = "Học sinh giỏi", value= "Học sinh giỏi"},
                new ComboObject{key = "Học sinh tiên tiến", value="Học sinh tiên tiến"}
            };
            ViewData[PupilProfileReportConstants.CBO_APPELLATION] = new SelectList(lstAppellation, "key", "value");
        }

        private IEnumerable<ListViewModel> _Search(int? educationLevel, int? classId, int semester, int? type, string appellation)
        {
            List<ListViewModel> result;
            IDictionary<string, object> dic;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevel;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["ClassID"] = classId;
            dic["Status"] = new List<int>{SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING,
                                        SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED};
            if (classId.HasValue)
            {
                dic["Check"] = "Check";
            }

            IQueryable<PupilOfClass> iqPoc = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);

            //khen thuong trong ho so hoc sinh
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevel;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["ClassID"] = classId;
            if (classId.HasValue)
            {
                dic["Check"] = "Check";
            }

            result = (from poc in iqPoc
                      join ppr in PupilPraiseBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic) on new { poc.ClassID, poc.PupilID } equals new { ppr.ClassID, ppr.PupilID }
                      where semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_ALL
                      || (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST && ppr.PraiseDate >= aca.FirstSemesterStartDate && ppr.PraiseDate <= aca.FirstSemesterEndDate)
                      || (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND && ppr.PraiseDate >= aca.SecondSemesterStartDate && ppr.PraiseDate <= aca.SecondSemesterEndDate)
                      select new ListViewModel
                      {
                          ClassName = poc.ClassProfile.DisplayName,
                          Content = ppr.Description,
                          PupilCode = poc.PupilProfile.PupilCode,
                          PupilName = poc.PupilProfile.FullName,
                          TypeID = ppr.PraiseTypeID,
                          TypeName = ppr.PraiseType.Resolution,
                          EducationLevel = poc.ClassProfile.EducationLevelID,
                          Name = poc.PupilProfile.Name,
                          ClassOrderNumber = poc.ClassProfile.OrderNumber
                      }).ToList();



            if (_globalInfo.AppliedLevel == 1)
            {
                //Khen thuong cuoi nam
                if (semester == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_ALL
                    || semester == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    dic = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID", _globalInfo.AcademicYearID},
                        {"EducationLevelID", educationLevel},
                        {"ClassID", classId},
                        {"Semester", SMAS.Business.Common.GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                    };
                    var lstSEE = from poc in iqPoc
                                 join s in SummedEndingEvaluationBusiness.Search(dic).Where(o => o.RewardID != null && o.RewardID != 0)
                                    on new { poc.ClassID, poc.PupilID } equals new { s.ClassID, s.PupilID }
                                 select new
                                 {
                                     ClassName = poc.ClassProfile.DisplayName,
                                     PupilCode = poc.PupilProfile.PupilCode,
                                     PupilName = poc.PupilProfile.FullName,
                                     Name = poc.PupilProfile.Name,
                                     EducationLevel = poc.ClassProfile.EducationLevelID,
                                     OrderNumber = poc.ClassProfile.OrderNumber
                                 };

                    result = result.Union(lstSEE.Select(o => new ListViewModel
                        {
                            ClassName = o.ClassName,
                            Content = "Học sinh xuất sắc",
                            PupilCode = o.PupilCode,
                            PupilName = o.PupilName,
                            TypeID = -1,
                            TypeName = "Khen thưởng cuối năm",
                            EducationLevel = o.EducationLevel,
                            Name = o.Name,
                            ClassOrderNumber = o.OrderNumber
                        }).ToList()).ToList();
                }

                //Khen thuong dot xuat, thanh tich vuot troi
                dic = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID},
                        {"ClassID", classId}

                    };
                if (semester == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    dic["SemesterID"] = semester;
                }

                var lstEvaluationReward = from poc in iqPoc
                                          join e in EvaluationRewardBusiness.Search(dic) on new { poc.ClassID, poc.PupilID } equals new { e.ClassID, e.PupilID }
                                          where e.RewardID == 1 || e.RewardID == 2
                                          select new
                                          {
                                              ClassName = poc.ClassProfile.DisplayName,
                                              PupilCode = poc.PupilProfile.PupilCode,
                                              PupilName = poc.PupilProfile.FullName,
                                              Content = e.Content,
                                              RewardID = e.RewardID,
                                              EducationLevel = poc.ClassProfile.EducationLevelID,
                                              Name = poc.PupilProfile.Name,
                                              OrderNumber = poc.ClassProfile.OrderNumber
                                          };

                //khen thuong dot xuat
                result = result.Union(lstEvaluationReward.Where(o => o.RewardID == 1).Select(o => new ListViewModel
                {
                    ClassName = o.ClassName,
                    Content = o.Content,
                    PupilCode = o.PupilCode,
                    PupilName = o.PupilName,
                    TypeID = -2,
                    TypeName = "Khen thưởng đột xuất",
                    EducationLevel = o.EducationLevel,
                    Name = o.Name,
                    ClassOrderNumber = o.OrderNumber
                }).ToList()).ToList();

                //thanh tich vuot troi
                result = result.Union(lstEvaluationReward.Where(o => o.RewardID == 2).Select(o => new ListViewModel
                {
                    ClassName = o.ClassName,
                    Content = o.Content,
                    PupilCode = o.PupilCode,
                    PupilName = o.PupilName,
                    TypeID = -3,
                    TypeName = "Học sinh có thành tích vượt trội",
                    EducationLevel = o.EducationLevel,
                    Name = o.Name,
                    ClassOrderNumber = o.OrderNumber
                }).ToList()).ToList();
            }
            else
            {
                dic = new Dictionary<string, object>() {
                            { "AcademicYearID", _globalInfo.AcademicYearID } ,
                            { "ClassID", classId },
                            { "Semester", semester }
                            };
                var listPupilEmulation = from poc in iqPoc
                                         join p in PupilEmulationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic) on new { poc.ClassID, poc.PupilID } equals new { p.ClassID, p.PupilID }

                                         select new
                           {
                               PupilID = p.PupilID,
                               Content = p.HonourAchivementType.Resolution,
                               ClassName = poc.ClassProfile.DisplayName,
                               PupilCode = poc.PupilProfile.PupilCode,
                               PupilName = poc.PupilProfile.FullName,
                               EducationLevel = poc.ClassProfile.EducationLevelID,
                               Name = poc.PupilProfile.Name,
                               OrderNumber = poc.ClassProfile.OrderNumber
                           };

                result = result.Union(listPupilEmulation.Select(o => new ListViewModel
                {
                    ClassName = o.ClassName,
                    Content = o.Content,
                    PupilCode = o.PupilCode,
                    PupilName = o.PupilName,
                    TypeID = -1,
                    TypeName = "Khen thưởng cuối kỳ",
                    EducationLevel = o.EducationLevel,
                    Name = o.Name,
                    ClassOrderNumber = o.OrderNumber,
                    Appellation = o.Content.Equals("Học sinh giỏi") ? 1 : o.Content.Equals("Học sinh tiên tiến") ? 2 : 0
                }).ToList()).ToList();

                //Khen thuong cuoi ky VNEN
                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                if (sp.IsNewSchoolModel.HasValue && sp.IsNewSchoolModel.Value == true)
                {
                    int partition = _globalInfo.SchoolID.Value % 100;
                    var listRewardComments = (from poc in iqPoc
                                             join ur in UpdateRewardBusiness.All on new { poc.ClassID, poc.PupilID } equals new { ur.ClassID, ur.PupilID }
                                             where ur.SchoolID == _globalInfo.SchoolID.Value && ur.PartitionID == partition
                                                        && ur.AcademicYearID == _globalInfo.AcademicYearID.Value
                                                        && (semester == SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_ALL || (ur.SemesterID == semester))
                                             select new ListViewModel
                                             {
                                                 ClassName = poc.ClassProfile.DisplayName,
                                                 Content = ur.Rewards,
                                                 PupilCode = poc.PupilProfile.PupilCode,
                                                 PupilName = poc.PupilProfile.FullName,
                                                 TypeID = -2,
                                                 TypeName = "Khen thưởng cuối kỳ (VNEN)",
                                                 EducationLevel = poc.ClassProfile.EducationLevelID,
                                                 Name = poc.PupilProfile.Name,
                                                 ClassOrderNumber = poc.ClassProfile.OrderNumber
                                             }).ToList();

                    List<RewardFinal> lstRf = RewardFinalBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value).ToList();

                    listRewardComments.ForEach(o =>
                    {

                        string rewardName = string.Empty;
                        string[] arrId = o.Content.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < arrId.Count(); i++)
                        {
                            int id;
                            if (Int32.TryParse(arrId[i], out id))
                            {
                                RewardFinal rd = lstRf.FirstOrDefault(u => u.RewardFinalID == id);
                                if (rd != null)
                                {
                                    rewardName = rewardName + rd.RewardMode;
                                    if (i < arrId.Count() - 1)
                                    {
                                        rewardName = rewardName + ", ";
                                    }
                                }
                            }
                        }

                        o.Content = rewardName;

                    });

                    result = result.Union(listRewardComments).ToList();
                }
            }

            if (type.HasValue)
            {
                result = result.Where(o => o.TypeID == type).ToList();
            }

            if (!string.IsNullOrEmpty(appellation))
            {
                result = result.Where(o => o.Content == appellation).ToList();
            }

            result.ForEach(o =>
            {
                if (string.IsNullOrEmpty(o.Content))
                {
                    o.Content = o.TypeName;
                }
            });

            return result.OrderBy(o => o.EducationLevel).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.Name).ThenBy(o => o.PupilName);
        }

        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        private List<DiplomaTemplate> GetListTemplate()
        {
            //Lay danh sach mau giay khen
            return DiplomaTemplateBusiness.All.Where(o => o.Type == 1 && (o.IsSystemTemplate || o.SchoolID == _globalInfo.SchoolID))
                                                                                .OrderBy(o => o.IsSystemTemplate).ThenBy(o => o.CreateDate).ToList();
        }

        private string UnescapeHTML(string html)
        {
            return System.Net.WebUtility.HtmlDecode(html);

        }

        #endregion
    }
}