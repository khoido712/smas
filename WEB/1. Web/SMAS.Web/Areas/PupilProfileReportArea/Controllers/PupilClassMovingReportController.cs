﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class PupilClassMovingReportController: BaseController
    {
         private readonly IFlowSituationBusiness  FlowSituationBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public PupilClassMovingReportController(
                       IFlowSituationBusiness flowSituationBusiness,
                       IProcessedReportBusiness processedReportBusiness)
        {
            this.FlowSituationBusiness = flowSituationBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;          
        }
        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            ViewData[PupilProfileReportConstants.LIST_SEMESTER] = CommonList.SemesterAndAll();
            int defaultSemester = (Global.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_ALL : Global.Semester.GetValueOrDefault();
            ViewData[PupilProfileReportConstants.DF_SEMESTER] = defaultSemester;
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetReport(FlowSituationBO flowSituationBO)
        {
            GlobalInfo global = new GlobalInfo();
            flowSituationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            flowSituationBO.SchoolID = global.SchoolID.Value;
            flowSituationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = FlowSituationBusiness.GetReportDefinitionOfClassMovement(flowSituationBO);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                 processedReport = FlowSituationBusiness.ExcelGetClassMovement(flowSituationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = FlowSituationBusiness.ExcelCreateClassMovement(flowSituationBO);
                processedReport = FlowSituationBusiness.ExcelInsertClassMovement(flowSituationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FlowSituationBO flowSituationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            flowSituationBO.AcademicYearID = glo.AcademicYearID.Value;
            flowSituationBO.SchoolID = glo.SchoolID.Value;
            flowSituationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = FlowSituationBusiness.ExcelCreateClassMovement(flowSituationBO);
            ProcessedReport processedReport = FlowSituationBusiness.ExcelInsertClassMovement(flowSituationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_HOC_SINH_CHUYEN_LOP_A4
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}