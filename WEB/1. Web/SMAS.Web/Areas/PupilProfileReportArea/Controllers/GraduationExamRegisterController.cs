﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class GraduationExamRegisterController:BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public GraduationExamRegisterController(IClassProfileBusiness classProfileBusiness, ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IEmployeeBusiness employeeBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness, IPupilProfileBusiness pupilProfileBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }
        public ActionResult Index()
        {
            //Lay danh sach khoi
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution", 12);

            //Lay danh sach lop
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", 12);
            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList() ;
            ViewData[PupilProfileReportConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            return View();
        }
        
        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_GRADUATION_EXAM_REGISTER);
            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID", 12},
                {"ClassID",classID}
            };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetGraduationExamRegisterReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PupilOfClassBusiness.CreateGraduationExamRegisterReport(dic);
                processedReport = PupilOfClassBusiness.InsertGraduationExamRegisterReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            ProcessedReport processedReport = null;
            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID", 12},
                {"ClassID",classID}
            };
            Stream excel = PupilOfClassBusiness.CreateGraduationExamRegisterReport(dic);
            processedReport = PupilOfClassBusiness.InsertGraduationExamRegisterReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        
        #endregion
    }
}