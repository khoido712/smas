﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.PupilProfileReportArea.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.Business.Common;
using System.Web.Script.Serialization;
using System.Configuration;
using Rotativa.Options;
using System.Globalization;
using Rotativa;
using System.Text;
using System.Drawing;
using Ionic.Zip;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    [SkipCheckRole]
    public class PupilCardController:BaseController
    {
        #region Declare variables
        private readonly IDiplomaTemplateBusiness DiplomaTemplateBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IDiplomaImageBusiness DiplomaImageBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        #endregion

        #region Constructor
        public PupilCardController(IDiplomaTemplateBusiness DiplomaTemplateBusiness, IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness, IPraiseTypeBusiness PraiseTypeBusiness, ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness,
            IEvaluationRewardBusiness EvaluationRewardBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IPupilProfileBusiness PupilProfileBusiness,
            IPupilEmulationBusiness PupilEmulationBusiness, IUpdateRewardBusiness UpdateRewardBusiness, IPupilPraiseBusiness PupilPraiseBusiness,
            ITemplateBusiness TemplateBusiness, IDiplomaImageBusiness DiplomaImageBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IRewardFinalBusiness RewardFinalBusiness)
        {
            this.DiplomaTemplateBusiness = DiplomaTemplateBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.DiplomaImageBusiness = DiplomaImageBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        [HttpPost]
        public PartialViewResult Search(SearchViewModel form, GridCommand command)
        {
            Utils.Utils.TrimObject(form);


            IEnumerable<ListViewModel> iq = _Search(form.EducationLevelID, form.ClassID);
            int totalRecord = iq.Count();
            ViewData[PupilProfileReportConstants.TOTAL] = totalRecord;

            List<ListViewModel> listResult;
            if (form.ClassID.HasValue && form.ClassID.Value != 0)
            {
                listResult = iq.ToList();
                ViewData[PupilProfileReportConstants.ENABLE_PAGING] = false;
            }
            else
            {
                listResult = iq.Take(PupilProfileReportConstants.PageSize).ToList();
                ViewData[PupilProfileReportConstants.ENABLE_PAGING] = true;
            }
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);


            IEnumerable<ListViewModel> iq = _Search(form.EducationLevelID, form.ClassID);
            int totalRecord = iq.Count();
            ViewData[PupilProfileReportConstants.TOTAL] = totalRecord;

            List<ListViewModel> listResult = iq.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            return View(new GridModel<ListViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveTemplate(int? templateId, string templateName, string html)
        {
            templateName = templateName.Trim();
            if (string.IsNullOrEmpty(templateName))
            {
                throw new BusinessException("Tên mẫu thẻ học sinh là bắt buộc");
            }

            if (templateName.Length > 100)
            {
                throw new BusinessException(Res.Get("Common_Validate_MaxLength"));
            }

            DiplomaTemplate template = DiplomaTemplateBusiness.Find(templateId);
            if (template != null)
            {
                if (DiplomaTemplateBusiness.All.Where(o => o.IsSystemTemplate == false && o.DiplomaTemplateID != templateId && o.SchoolID == _globalInfo.SchoolID && o.TemplateName == templateName && o.Type == 2).Count() > 0)
                {
                    throw new BusinessException("Tên mẫu thẻ học sinh không được trùng với tên mẫu thẻ học sinh đã có");
                }

                template.TemplateName = templateName;
                template.HtmlTemplate = html;
                template.ModifiedDate = DateTime.Now;
                DiplomaTemplateBusiness.Update(template);
            }
            else
            {
                if (DiplomaTemplateBusiness.All.Where(o => o.IsSystemTemplate == false && o.SchoolID == _globalInfo.SchoolID && o.TemplateName == templateName && o.Type == 2).Count() > 0)
                {
                    throw new BusinessException("Tên mẫu thẻ học sinh không được trùng với tên mẫu thẻ học sinh đã có");
                }

                template = new DiplomaTemplate
                {
                    CreateDate = DateTime.Now,
                    HtmlTemplate = html,
                    IsSystemTemplate = false,
                    ModifiedDate = null,
                    SchoolID = _globalInfo.SchoolID.Value,
                    TemplateName = templateName,
                    Type = 2
                };

                DiplomaTemplateBusiness.Insert(template);

            }

            DiplomaTemplateBusiness.Save();

            return Json(new JsonMessage { Type = SMAS.Web.Constants.GlobalConstants.TYPE_SUCCESS, Message = "Lưu mẫu thẻ học sinh thành công" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteTemplate(int? templateId)
        {
            DiplomaTemplateBusiness.Delete(templateId);

            DiplomaTemplateBusiness.Save();

            return Json(new JsonMessage { Type = SMAS.Web.Constants.GlobalConstants.TYPE_SUCCESS, Message = "Xóa mẫu thẻ học sinh thành công" });
        }

        [HttpPost]
        public JsonResult UploadImage(HttpPostedFileBase file)
        {
            try
            {
                int maxSize = 1 * 1024 * 1024;
                if (file.ContentLength > maxSize)
                {
                    throw new BusinessException("Dung lượng file không được quá 1 MB");
                }

                string[] arr = file.FileName.Split('.');
                string extension = arr[arr.Length - 1];
                if (!"PNG".Equals(extension.ToUpper()) && !"JPG".Equals(extension.ToUpper()) && !"JPEG".Equals(extension.ToUpper()))
                {
                    throw new BusinessException(string.Format("Hệ thống không hỗ trợ up file có định dạng {0}.", extension));
                }

                byte[] _Buffer = null;

                using (BinaryReader _BinaryReader = new BinaryReader(file.InputStream))
                {
                    _Buffer = _BinaryReader.ReadBytes(file.ContentLength);
                }

                DiplomaImage image = new DiplomaImage();

                image.DiplomaImageID = DiplomaImageBusiness.GetNextSeq<int>("DIPLOMA_IMAGE_SEQ");
                image.DiplomaTemplateID = 0;
                image.SchoolID = _globalInfo.SchoolID.Value;
                image.ToByte = _Buffer;

                image = DiplomaImageBusiness.Insert(image);
                DiplomaImageBusiness.Save();



                string url = Url.Action("ShowImage", new { id = image.DiplomaImageID });
             
                return Json(new { URL = url});
            }
            catch (Exception e)
            {
                if (e is BusinessException)
                {
                    throw e;
                }
                else
                {
                    throw new BusinessException("Upload ảnh không thành công");
                }
            }
        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ShowImage(int? id)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/default.png");
            DiplomaImage image = DiplomaImageBusiness.Find(id);
            byte[] imageData;
            if (image != null)
            {
                if (image.ToByte != null)
                {
                    imageData = image.ToByte;
                    return File(imageData, "image/jpg");
                }
                else
                {
                    imageData = FileToByteArray(defaultImagePath);
                    return File(imageData, "image/jpg");
                }
            }

            //neu ko co tra ve anh default
            imageData = FileToByteArray(defaultImagePath);
            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");

        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ShowPupilImage(int? PupilId)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/profile_picture.jpg");

            byte[] imageData = FileToByteArray(defaultImagePath);
            PupilProfile pp = PupilProfileBusiness.Find(PupilId);
            if (pp != null && pp.Image != null)
            {
                imageData = pp.Image;
            }

            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");

        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ShowBarcode(int? PupilId, string PupilCode, string PupilName, string ClassName)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/barcode.png");

            byte[] imageData = FileToByteArray(defaultImagePath);
            PupilProfile pp = PupilProfileBusiness.Find(PupilId);
            if (pp != null)
            {
                string pupilCode =SMAS.Business.Common.Utils.StripVNSign(PupilCode);

                Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                Image image = barcode.Draw(pupilCode, 150);
                imageData = ImageToByteArray(image);
            }

            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");

        }

        [SkipCheckRole]
        [AllowAnonymous]
        public ActionResult ShowQrcode(int? PupilId, string PupilCode, string PupilName, string ClassName)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/qrcode.png");

            byte[] imageData = FileToByteArray(defaultImagePath);
            PupilProfile pp = PupilProfileBusiness.Find(PupilId);
            if (pp != null)
            {
                string content = string.Format("- Ten: {0}\n- Lop: {1}\n- Ma HS: {2}",
                    SMAS.Business.Common.Utils.StripVNSign(PupilName),
                    SMAS.Business.Common.Utils.StripVNSign(ClassName),
                    SMAS.Business.Common.Utils.StripVNSign(PupilCode));

                Zen.Barcode.CodeQrBarcodeDraw qrcode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                Image image = qrcode.Draw(content, 150);
                imageData = ImageToByteArray(image);
            }

            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");

        }

        public JsonResult AjaxLoadClass(int? paraEducationLevelID)
        {
            List<ClassProfile> listClass = new List<ClassProfile>();

            if (paraEducationLevelID.HasValue)
            {
                IDictionary<string, object> dicToGetClass = new Dictionary<string, object>();
                dicToGetClass["EducationLevelID"] = paraEducationLevelID;
                dicToGetClass["AcademicYearID"] = _globalInfo.AcademicYearID;
                listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicToGetClass).ToList();
            }

            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadDiplomaTemplateID(int? paraEducationLevelID)
        {
            List<DiplomaTemplate> lst = GetListTemplate();

            return Json(new SelectList(lst, "DiplomaTemplateID", "TemplateName"));
        }

        public PartialViewResult Design(int? id)
        {
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();
            lstDiplomaTemplate.Insert(0, new DiplomaTemplate { TemplateName = "[Thêm mới]", DiplomaTemplateID = 0 });
            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName", id);
            ViewData[PupilProfileReportConstants.SCHOOL_PROFILE] = sp;

            return PartialView("_Design");
            
        }

        public PartialViewResult CopyDialog()
        {
            //Lay danh sach mau giay khen
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();

            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName");

            return PartialView("_Copy");
        }

        public PartialViewResult AddBgDialog()
        {
           
            return PartialView("_AddBackground");
        }

        [HttpPost]
        public JsonResult LoadTemplate(int id)
        {
            //Lay danh sach mau giay khen
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(id);
            string html = string.Empty;
            if (template != null)
            {
                html = template.HtmlTemplate;
            }

            return Json(new { HTML = html, Name = template.TemplateName, IsSystem = template.IsSystemTemplate });
        }

        [HttpPost]
        public JsonResult CopyFrom(int CopyFromID)
        {
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(CopyFromID);
            string html = string.Empty;
            if (template != null)
            {
                html = template.HtmlTemplate;
            }

            return Json(new { HTML = html });
        }


        public JsonResult GetBackground(int TemplateType, HttpPostedFileBase file)
        {
            try
            {
                string defaultImagePath = Server.MapPath("~/Content/images/");
                byte[] _Buffer = null;

                if (TemplateType == 1)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau1.png";
                }
                else if(TemplateType == 2)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau2.png";
                }
                else if (TemplateType == 3)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau3.png";
                }
                else if (TemplateType == 4)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau4.png";
                }
                else if (TemplateType == 5)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau5.png";
                }
                else if (TemplateType == 6)
                {
                    defaultImagePath = defaultImagePath + "THS_Mau6.png";
                }
                else if (TemplateType == 7)
                {
                    if (file == null)
                    {
                        throw new BusinessException("Thầy cô chưa chọn file ảnh");
                    }

                    int maxSize = 5 * 1024 * 1024;
                    if (file.ContentLength > maxSize)
                    {
                        throw new BusinessException("Dung lượng file không được quá 5 MB");
                    }

                    string[] arr = file.FileName.Split('.');
                    string extension = arr[arr.Length - 1];
                    if (!"PNG".Equals(extension.ToUpper()) && !"JPG".Equals(extension.ToUpper()) && !"JPEG".Equals(extension.ToUpper()))
                    {
                        throw new BusinessException(string.Format("Hệ thống không hỗ trợ up file có định dạng {0}.", extension));
                    }
                }

                if (TemplateType == 7)
                {
                    using (BinaryReader _BinaryReader = new BinaryReader(file.InputStream))
                    {
                        _Buffer = _BinaryReader.ReadBytes(file.ContentLength);
                    }
                }
                else
                {
                    _Buffer = FileToByteArray(defaultImagePath);
                }

                DiplomaImage image = new DiplomaImage();

                image.DiplomaImageID = DiplomaImageBusiness.GetNextSeq<int>("DIPLOMA_IMAGE_SEQ");
                image.DiplomaTemplateID = 0;
                image.SchoolID = _globalInfo.SchoolID.Value;
                image.ToByte = _Buffer;

                image = DiplomaImageBusiness.Insert(image);
                DiplomaImageBusiness.Save();


                string url = Url.Action("ShowImage", new { id = image.DiplomaImageID });
               
                return Json(new { URL = url });

            }
            catch(Exception e)
            {
                if (e is BusinessException)
                {
                    throw e;
                }
                else
                {
                    throw new BusinessException("Upload ảnh không thành công");
                }
            }
        }

        [HttpPost]
        public JsonResult SaveExport(List<int> lstPupilId, bool exportAll, bool frontSide, bool backSide, SearchViewModel form)
        {
            if (!frontSide && !backSide)
            {
                throw new BusinessException("Thầy/cô chưa chọn mặt in");
            }

            if (form.DiplomaTemplateID == null)
            {
                throw new BusinessException("Thầy/cô chưa chọn mẫu thẻ học sinh");
            }

            if (lstPupilId == null)
            {
                lstPupilId = new List<int>();
            }

            List<ListViewModel> lstModel = _Search(form.EducationLevelID, form.ClassID).ToList();
            if (!exportAll)
            {
                lstModel = _Search(form.EducationLevelID, form.ClassID).Where(o => lstPupilId.Contains(o.PupilID)).ToList();
            }

            if (lstModel == null || (lstModel != null && lstModel.Count == 0))
            {
                throw new BusinessException("Thầy/cô chưa chọn học sinh để in thẻ học sinh");
            }

            Session["ListToExport"] = lstModel;

            return Json(new JsonMessage { Type = "success" });
        }

        [HttpGet]
        public ActionResult ExportPDF(int templateId, bool exportAll, bool frontSide, bool backSide, SearchViewModel form)
        {
            List<ListViewModel> lstModel;
            if (exportAll)
            {
                lstModel = _Search(form.EducationLevelID, form.ClassID).ToList();

            }
            else
            {
                lstModel = (List<ListViewModel>)Session["ListToExport"];
            }

            if (lstModel==null || (lstModel!=null && lstModel.Count == 0))
            {
                throw new BusinessException("Thầy/cô chưa chọn học sinh để in thẻ học sinh");
            }

            if (!frontSide && !backSide)
            {
                throw new BusinessException("Thầy/cô chưa chọn mặt in");
            }

            int emptyAddedCount = (lstModel.Count % 3) != 0 ? 3 - (lstModel.Count % 3) : 0;
            for (int i = 0; i < emptyAddedCount; i++)
            {
                lstModel.Add(new ListViewModel());
            }

            //get template
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(templateId);

            string htmlTemplate = UnescapeHTML(template.HtmlTemplate);

            //replace image path
            htmlTemplate = "<div style=\"display:inline-block;padding:15px;width:500px;height:315.303738318px;box-sizing: content-box;\">" + htmlTemplate.Replace("../../PupilProfileReportArea", "/PupilProfileReportArea") + "</div>";
            string localPath = ConfigurationManager.AppSettings["urlLocalHost"];
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/PupilCard/ShowImage", localPath + "/PupilProfileReportArea/PupilCard/ShowImage");
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/PupilCard/ShowPupilImage", localPath + "/PupilProfileReportArea/PupilCard/ShowPupilImage");
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/PupilCard/ShowBarcode", localPath + "/PupilProfileReportArea/PupilCard/ShowBarcode");
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/PupilCard/ShowQrcode", localPath + "/PupilProfileReportArea/PupilCard/ShowQrcode");

            StringBuilder sb = new StringBuilder();
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);

            ViewAsPdf fv = null;
            ViewAsPdf bv = null;
            byte[] frontFile = null;
            byte[] backFile = null;

            if (frontSide)
            {
                fv = this.BuildPdfFile(lstModel, htmlTemplate, 1);
                frontFile = fv.BuildPdf(this.ControllerContext);
            }

            if (backSide)
            {
                this.RevertListToPrintBackSide(lstModel);
                bv = this.BuildPdfFile(lstModel, htmlTemplate, 2);
                backFile = bv.BuildPdf(this.ControllerContext);
            }

            FileStreamResult res = null;
            if (frontSide && backSide)
            {
                using (ZipFile zip = new ZipFile())
                {
                    MemoryStream fstream = new MemoryStream(frontFile);
                    zip.AddEntry("The_hoc_sinh_mat_truoc.pdf", fstream);

                    MemoryStream bstream = new MemoryStream(backFile);
                    zip.AddEntry("The_hoc_sinh_mat_sau.pdf", bstream);

                    MemoryStream output = new MemoryStream();
                    zip.Save(output);
                    output.Seek(0, SeekOrigin.Begin);
                    res = File(output, "application/zip", "The_hoc_sinh.zip");

                    fstream.Dispose();
                    bstream.Dispose();
                }
            }
            else if (frontSide)
            {
                return fv;
            }
            else if (backSide)
            {
                return bv;
            }

            return res;
        }

        [HttpGet]
        public ActionResult ViewPDF(int templateId, bool exportAll, bool withBackground, SearchViewModel form)
        {
            List<ListViewModel> lstModel;
            if (exportAll)
            {
                lstModel = _Search(form.EducationLevelID, form.ClassID).ToList();

            }
            else
            {
                lstModel = (List<ListViewModel>)Session["ListToExport"];
            }

            if (lstModel == null || (lstModel != null && lstModel.Count == 0))
            {
                throw new BusinessException("Thầy/cô chưa chọn học sinh để in thẻ học sinh");
            }
            //get template
            DiplomaTemplate template = DiplomaTemplateBusiness.Find(templateId);

            string htmlTemplate = UnescapeHTML(template.HtmlTemplate);

            //replace image path
            htmlTemplate = "<div style=\"display:inline-block;padding:15px;width:500px;height:315.303738318px;box-sizing: content-box;\">" + htmlTemplate.Replace("../../PupilProfileReportArea", "/PupilProfileReportArea") + "</div>";
            string localPath = ConfigurationManager.AppSettings["urlLocalHost"];
            htmlTemplate = htmlTemplate.Replace("/PupilProfileReportArea/DiplomaExporting/ShowImage", localPath + "/PupilProfileReportArea/DiplomaExporting/ShowImage");

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < lstModel.Count; i++)
            {
                string eachHtml = htmlTemplate;
                ListViewModel model = lstModel[i];
                model.ClassName = model.ClassName.Replace("LỚP", string.Empty);
                model.ClassName = model.ClassName.Replace("Lớp", string.Empty);
                model.ClassName = model.ClassName.Replace("lớp", string.Empty);
                model.ClassName = model.ClassName.Trim();

                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);

                string semester;
                if (_globalInfo.Semester == 1)
                {
                    semester = "Học kỳ I";
                }
                else
                {
                    semester = "Học kỳ II";
                }

                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_NAME, model.PupilName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_CLASS_NAME, model.ClassName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_CONTENT, model.Content);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_NAME, sp.HeadMasterName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SCHOOL_NAME, sp.SchoolName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SEMESTER, semester);


                sb.Append(eachHtml);

                if (i < lstModel.Count - 1 && (i + 1) % 9 == 0 && i > 0)
                {
                    sb.Append("<p class=\"breakhere\"></p>");
                }

            }

            Margins margins = new Margins(20, 15, 20, 15);

            Session.Remove("ListToExport");

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["HtmlContent"] = sb.ToString();
            dic["WithBackground"] = withBackground;

            return View("_Export", dic);
        } 
        #endregion

        #region Methods
        private void SetViewData()
        {
            //Lay danh sach mau giay khen
            List<DiplomaTemplate> lstDiplomaTemplate = GetListTemplate();

            ViewData[PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE] = new SelectList(lstDiplomaTemplate, "DiplomaTemplateID", "TemplateName");

            //Lay danh sach khoi
            ViewData[PupilProfileReportConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            ViewData[PupilProfileReportConstants.CBO_CLASS] = new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName"); 
        }

        private IEnumerable<ListViewModel> _Search(int? educationLevel, int? classId)
        {
            List<ListViewModel> result;
            IDictionary<string, object> dic;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevel;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["ClassID"] = classId;
            dic["Status"] = new List<int>{SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING,
                                        SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED};
            if (classId.HasValue)
            {
                dic["Check"] = "Check";
            }

            result = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => new ListViewModel
                {
                    PupilID = o.PupilID,
                    PupilName = o.PupilProfile.FullName,
                    PupilCode = o.PupilProfile.PupilCode,
                    BirthDate = o.PupilProfile.BirthDate,
                    Genre = o.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE ? "Nam" : "Nữ",
                    ClassName = o.ClassProfile.DisplayName,
                    EducationLevel = o.ClassProfile.EducationLevelID,
                    ClassOrderNumber = o.ClassProfile.OrderNumber,
                    Name = o.PupilProfile.Name,
                    FatherMobile = o.PupilProfile.FatherMobile,
                    FatherName = o.PupilProfile.FatherFullName,
                    //HeadTeacherName = o.ClassProfile.Employee != null ? o.ClassProfile.Employee.FullName : string.Empty,
                    LivePlace = o.PupilProfile.PermanentResidentalAddress,
                    MotherMobile = o.PupilProfile.MotherMobile,
                    MotherName = o.PupilProfile.MotherFullName,
                    TempLivePlace = o.PupilProfile.TempResidentalAddress,
                    OrderInClass  = o.OrderInClass,
                    ClassId = o.ClassID
                }).ToList();


            return result.OrderBy(o => o.EducationLevel).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.PupilName);
        }

        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        private List<DiplomaTemplate> GetListTemplate()
        {
            //Lay danh sach template
            return DiplomaTemplateBusiness.All.Where(o => (o.IsSystemTemplate || o.SchoolID == _globalInfo.SchoolID) && o.Type == 2)
                                                                                .OrderBy(o => o.IsSystemTemplate).ThenBy(o => o.CreateDate).ToList();
        }

        private string UnescapeHTML(string html)
        {
            return System.Net.WebUtility.HtmlDecode(html);
           
        }

        public byte[] ImageToByteArray(Image img)
        {
            using (var ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
        }

        private void RevertListToPrintBackSide(List<ListViewModel> listToRevert)
        {
            for (int i = 0; i < listToRevert.Count; i++)
            {
                if (i > 0 && ((i + 1) % 3 == 0))
                {
                    ListViewModel tmp = listToRevert[i];
                    listToRevert[i] = listToRevert[i - 2];
                    listToRevert[i - 2] = tmp;
                }
            }
        }

        private ViewAsPdf BuildPdfFile(List<ListViewModel> lstModel, string htmlTemplate, int side)
        {
            StringBuilder sb = new StringBuilder();
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string academicYearTitle = aca.Year + " - " + (aca.Year + 1);
            List<int> lstClassId = lstModel.Select(o => o.ClassId).Distinct().ToList();
            List<ClassProfile> lstCp = ClassProfileBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID && lstClassId.Contains(o.ClassProfileID)).ToList();
            for (int i = 0; i < lstModel.Count; i++)
            {
                string eachHtml = htmlTemplate;
                ListViewModel model = lstModel[i];

                if (model.PupilID == 0)
                {
                    eachHtml = eachHtml.Replace("<div style=\"display:inline-block;padding:15px;width:500px;height:315.303738318px;box-sizing: content-box;\">", "<div style=\"visibility:hidden;display:inline-block;padding:15px;width:500px;height:315.303738318px;box-sizing: content-box;\">");
                }
                if (model.ClassName != null)
                {
                    model.ClassName = model.ClassName.Replace("LỚP", string.Empty);
                    model.ClassName = model.ClassName.Replace("Lớp", string.Empty);
                    model.ClassName = model.ClassName.Replace("lớp", string.Empty);
                    model.ClassName = model.ClassName.Trim();
                }

                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_NAME, model.PupilName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_CODE, model.PupilCode);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_BIRTH_DAY, model.StrBirthDate);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_GENRE, model.Genre);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_CLASS_NAME, model.ClassName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SCHOOL_NAME, sp.SchoolName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_ACADEMIC_YEAR, academicYearTitle);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_SUPERVISING_DEPT, sp.SupervisingDept.SupervisingDeptName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_NAME, sp.HeadMasterName);
                //eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_TEACHER_NAME, model.HeadTeacherName);
                ClassProfile cp = lstCp.FirstOrDefault(o => o.ClassProfileID == model.ClassId);
                if (cp != null)
                {
                    eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_TEACHER_NAME, cp.Employee != null ? cp.Employee.FullName : string.Empty);
                }
                else
                {
                    eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_HEAD_TEACHER_NAME, string.Empty);
                }
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_TEMP_LIVE_PLACE, model.TempLivePlace);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_PUPIL_LIVE_PLACE, model.LivePlace);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_FATHER_NAME, model.FatherName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_MOTHER_NAME, model.MotherName);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_FATHER_MOBILE, model.FatherMobile);
                eachHtml = eachHtml.Replace(PupilProfileReportConstants.TEMPLATE_KEY_MOTHER_MOBILE, model.MotherMobile);

                //pupil image
                eachHtml = eachHtml.Replace("/PupilProfileReportArea/PupilCard/ShowPupilImage", "/PupilProfileReportArea/PupilCard/ShowPupilImage?PupilId=" + model.PupilID);

                //bar code
                eachHtml = eachHtml.Replace("/PupilProfileReportArea/PupilCard/ShowBarcode", "/PupilProfileReportArea/PupilCard/ShowBarcode?PupilId=" + model.PupilID + "&PupilCode=" + model.PupilCode + "&PupilName=" + model.PupilName + "&ClassName=" + model.ClassName);

                //Qr code
                eachHtml = eachHtml.Replace("/PupilProfileReportArea/PupilCard/ShowQrcode", "/PupilProfileReportArea/PupilCard/ShowQrcode?PupilId=" + model.PupilID + "&PupilCode=" + model.PupilCode + "&PupilName=" + model.PupilName + "&ClassName=" + model.ClassName);

                sb.Append(eachHtml);

                if (i < lstModel.Count - 1 && (i + 1) % 9 == 0 && i > 0)
                {
                    sb.Append("<p class=\"breakhere\"></p>");
                }

            }

            //Margins margins = new Margins(16, 0, 0, 0);

            string customSwitches = "--print-media-type --page-size A4 --zoom 0.81 --dpi 96 -L 0mm -R 0mm -T 15.48mm -B 0mm";

            Session.Remove("ListToExport");

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["HtmlContent"] = sb.ToString();

            ViewAsPdf v = new ViewAsPdf("_Export", new { })
            {
                FileName = "The_hoc_sinh_" + (side == 1 ? "mat_truoc" : "mat_sau") + ".pdf",
                PageOrientation = Rotativa.Options.Orientation.Landscape,
                CustomSwitches = customSwitches,
                Model = dic
            };

            return v;
            
        }
        #endregion
    }
}