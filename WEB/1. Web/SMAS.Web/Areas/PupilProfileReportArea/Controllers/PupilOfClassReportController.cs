﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class PupilOfClassReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PupilOfClassReportController(IClassProfileBusiness classProfileBusiness, ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IEmployeeBusiness employeeBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness, IPupilProfileBusiness pupilProfileBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }
        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            return View();
        }
        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }
        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetPupilOfClassReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_DANHSACHHOCSINH);
            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID}
            };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(educationLevelID, classID);
                processedReport = PupilOfClassBusiness.InsertProcessReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            ProcessedReport processedReport = null;
            IDictionary<string,object> dicInsert = new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID}
            };
            Stream excel = this.ExportExcel(educationLevelID, classID);
            processedReport = PupilOfClassBusiness.InsertProcessReport(dicInsert, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.HS_DANHSACHHOCSINH,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.HS_DANHSACHHOCSINH,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(int? educationLevelID, int? classID)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", SystemParamsInFile.HS_DANHSACHHOCSINH + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = null;
            //lay danh sach lop hoc theo khoi
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dicClass.Add("EducationLevelID", educationLevelID);
            dicClass.Add("ClassProfileID", classID);
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(p=>p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            IDictionary<string,object> dic = new Dictionary<string,object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"AppliedLevelID",_globalInfo.AppliedLevel}
            };
            IQueryable<Employee> iqEmployee = (from e in EmployeeBusiness.All
                                               join aca in AcademicYearBusiness.All on e.SchoolID equals aca.SchoolID
                                               where e.SchoolID == _globalInfo.SchoolID
                                               //&& e.AppliedLevel == _globalInfo.AppliedLevel
                                               && aca.AcademicYearID == academicYearID
                                               && e.EmployeeType == GlobalConstants.EMPLOYEE_TYPE_TEACHER
                                               select e);
            Employee objEmployee = null;

            ClassProfile objCP = null;

            //lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                                      join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                                      where poc.AcademicYearID == academicYearID
                                                      && poc.SchoolID == _globalInfo.SchoolID
                                                      && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                      && lstClassID.Contains(poc.ClassID)
                                                      && pf.IsActive == true
                                                      select new PupilOfClassBO
                                                      {
                                                          PupilID = poc.PupilID,
                                                          ClassID = poc.ClassID,
                                                          PupilFullName = poc.PupilProfile.FullName,
                                                          PupilCode = poc.PupilProfile.PupilCode,
                                                          Birthday = poc.PupilProfile.BirthDate,
                                                          Genre = poc.PupilProfile.Genre,
                                                          EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                                                          OrderInClass = poc.OrderInClass,
                                                          Name = pf.Name
                                                      }).ToList().OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0).ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            List<int> lstPupilID = lstPupilOfClassBO.Select(p=>p.PupilID).Distinct().ToList();

            //lay danh sach lop hoc nam hoc truoc cua hoc sinh
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            string TitleYearUnder = (objAca.Year - 1) + "-" + (objAca.Year);
            List<PupilOfClassBO> lstPOCUnderClass = new List<PupilOfClassBO>();
            lstPOCUnderClass = (from poc in PupilOfClassBusiness.All
                                join ay in AcademicYearBusiness.All on poc.AcademicYearID equals ay.AcademicYearID
                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                where lstPupilID.Contains(poc.PupilID)
                                && ay.DisplayTitle.Equals(TitleYearUnder)
                                && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                select new PupilOfClassBO
                                {
                                    PupilID = poc.PupilID,
                                    ClassID = poc.ClassID,
                                    ClassName = cp.DisplayName
                                }).ToList();

            string className = string.Empty;
            string HeadTeacherName = "";
            for (int i = 0; i < lstClassProfile.Count; i++)
            {
                sheet = oBook.CopySheetToLast(firstSheet);
                objCP = lstClassProfile[i];
                HeadTeacherName = "";
                lstPOCtmp = lstPupilOfClassBO.Where(p => p.ClassID == objCP.ClassProfileID).ToList();
                
                if (objCP.HeadTeacherID.HasValue)
                {
                    objEmployee = iqEmployee.Where(p => p.EmployeeID == objCP.HeadTeacherID).FirstOrDefault();
                    if (objEmployee != null)
                    {
                        HeadTeacherName = objEmployee.FullName;    
                    }
                    
                }
                this.SetValueToFile(sheet, lstPOCtmp,lstPOCUnderClass, objCP.DisplayName,HeadTeacherName);
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }
        private void SetValueToFile(IVTWorksheet sheet, List<PupilOfClassBO> lstPupilOfClassBO,List<PupilOfClassBO> lstPOCUnderClass,string className,string TeacherName)
        {
            #region fill du lieu
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            //phong so
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
            //truong
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            sheet.SetCellValue("A6","Lớp " + className + ", GVCN: " + TeacherName);
            int startRow = 9;
            string datetime = "";

            datetime = (objSP.District != null ? objSP.District.DistrictName : "") + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
            PupilOfClassBO objPOC = null;
            PupilOfClassBO objPOCUnderClass = null;
            int pupilID = 0;
            for (int i = 0; i < lstPupilOfClassBO.Count; i++)
            {
                objPOC = lstPupilOfClassBO[i];
                pupilID = objPOC.PupilID;
                objPOCUnderClass = lstPOCUnderClass.Where(p => p.PupilID == pupilID).FirstOrDefault();
                //STT
                sheet.SetCellValue(startRow, 1, i + 1);
                sheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                //Ho va ten
                sheet.SetCellValue(startRow, 2, objPOC.PupilFullName);
                //Ma hoc sinh
                sheet.SetCellValue(startRow, 3, objPOC.PupilCode);
                sheet.GetRange(startRow, 3, startRow, 3).SetHAlign(VTHAlign.xlHAlignCenter);
                //Ngay sinh
                sheet.SetCellValue(startRow, 4, objPOC.Birthday.ToString("dd/MM/yyyy"));
                sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                //Gioi tinh
                sheet.SetCellValue(startRow, 5, objPOC.Genre.Value == 0 ? "Nữ" : "Nam");
                sheet.GetRange(startRow, 5, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                //Dan toc
                sheet.SetCellValue(startRow, 6, objPOC.EthnicName);
                sheet.GetRange(startRow, 6, startRow, 6).WrapText();
                //Lop nam hoc cu
                sheet.SetCellValue(startRow, 7, objPOCUnderClass != null ? objPOCUnderClass.ClassName : "");
                sheet.GetRange(startRow, 7, startRow, 7).SetHAlign(VTHAlign.xlHAlignCenter);
                //Ghi chu
                startRow++;
            }
            //kẻ khung
            sheet.GetRange(9, 1, 8 + lstPupilOfClassBO.Count, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(11 + lstPupilOfClassBO.Count, 4, 11 + lstPupilOfClassBO.Count, 8).Merge();
            sheet.SetCellValue(11 + lstPupilOfClassBO.Count, 4, datetime);
            sheet.GetRange(12 + lstPupilOfClassBO.Count, 4, 12 + lstPupilOfClassBO.Count, 8).Merge();
            sheet.SetCellValue("D" + (12 + lstPupilOfClassBO.Count), "HIỆU TRƯỞNG");
            sheet.GetRange(12 + lstPupilOfClassBO.Count, 4, 12 + lstPupilOfClassBO.Count, 8).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            int headTeacherPoint = 16 + lstPupilOfClassBO.Count;
            sheet.SetCellValue(headTeacherPoint, 4, SchoolProfileBusiness.Find(_globalInfo.SchoolID).HeadMasterName);
            sheet.GetRange(headTeacherPoint, 4, headTeacherPoint, 8).Merge();
            sheet.GetRange(headTeacherPoint, 4, headTeacherPoint, 8).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.Name = Utils.Utils.StripVNSignAndSpace(className);
            sheet.SetFontName("Times New Roman",0);
            #endregion
        }
        #endregion
    }
}