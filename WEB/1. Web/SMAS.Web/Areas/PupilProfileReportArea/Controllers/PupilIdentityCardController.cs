﻿using ClosedXML.Excel;
using NativeExcel;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Excel.ExportXML;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class PupilIdentityCardController : BaseController
    {
        private const string TemplateName = "HS_THCS_Thehocsinh.xls";
        private const string ExportTemplateName = "HS_{0}_Thehocsinh.xls";
        private readonly IClassProfileBusiness _classProfileBusiness;
        private readonly IPupilProfileBusiness _pupilProfileBusiness;
        private readonly IAcademicYearBusiness _academicYearBusiness;
        public PupilIdentityCardController(IClassProfileBusiness classProfileBusiness, IPupilProfileBusiness pupilProfileBusiness, IAcademicYearBusiness academicYearBusiness)
        {
            this._classProfileBusiness = classProfileBusiness;
            this._pupilProfileBusiness = pupilProfileBusiness;
            this._academicYearBusiness = academicYearBusiness;
        }

        /// <summary>
        /// Trang chủ chức năng
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> classList = new List<SelectListItem>();
            if (lstEducation.Any())
            {
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("EducationLevelID", lstEducation.FirstOrDefault().EducationLevelID);

                classList = Enumerable.ToList(this._classProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName))
                    .Select(u => new SelectListItem
                    {
                        Value = u.ClassProfileID.ToString(),
                        Text = u.DisplayName,
                        Selected = false
                    }).ToList();
            }
            ViewData[PupilProfileReportConstants.LIST_CLASS] = classList;
            return View();
        }

        /// <summary>
        /// Lấy danh sách lớp khi khối thay đổi
        /// </summary>
        /// <param name="eduId"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
            {
                return Json(new List<SelectListItem>());
            }
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            List<ClassProfile> lstClass = this._classProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            ClassProfile objCP = null;
            List<SelectListItem> lstItem = new List<SelectListItem>();
            SelectListItem objSelectlistItem = null;
            for (int i = 0; i < lstClass.Count; i++)
            {
                objCP = lstClass[i];
                objSelectlistItem = new SelectListItem();
                objSelectlistItem.Value = objCP.ClassProfileID.ToString();
                objSelectlistItem.Text = objCP.DisplayName;
                objSelectlistItem.Selected = false;
                lstItem.Add(objSelectlistItem);
            }
            return Json(lstItem);
        }

        /// <summary>
        /// Xuất ra excel danh sách thẻ học sinh
        /// </summary>
        /// <param name="educationLevelId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public FileResult DownloadReport(int? educationLevelId, int? classId)
        {
            int schoolId = (int)(_globalInfo.SchoolID ?? 0);
            int academicYearId = (int)(_globalInfo.AcademicYearID ?? 0);
            AcademicYear academicYear = _academicYearBusiness.Find(academicYearId);
            int total;
            List<int> educationLevelIds = new List<int>() { educationLevelId ?? 0 };
            List<PupilProfile> list = _pupilProfileBusiness.GetPupilWithPaging(1, Int16.MaxValue, academicYearId, schoolId, educationLevelIds, classId, null, null, null, null, null, out total);
            var dic = new Dictionary<int, List<PupilProfile>>();
            list.GroupBy(o => o.CurrentClassID).ForEach(group => dic.Add(group.Key, group.ToList()));
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", TemplateName);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet currentSheet = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            string super = _globalInfo.SuperVisingDeptName.ToUpper();
            string schoolName = _globalInfo.SchoolName.ToUpper();
            sheet2.SetCellValue("B2", super);
            sheet2.SetCellValue("B3", schoolName);
            List<ClassProfile> lstClass;
            if (classId != null && classId > 0)
            {
                // Chỉ có 1 lớp
                lstClass = new List<ClassProfile>
                {
                    _classProfileBusiness.Find(classId)
                };
            }
            else
            {
                var dicClass = new Dictionary<string, object>()
                {
                    {"AcademicYearID", academicYearId},
                    {"EducationLevelID", educationLevelId}
                };
                // Có nhiều hơn 1 lớp
                lstClass = _classProfileBusiness.SearchBySchool(schoolId, dicClass).OrderBy(u => u.DisplayName).ToList();
            }
            IVTWorksheet sheet = null;
            IVTRange originalRange = currentSheet.GetRange("B2", "N12");
            int count = 0;
            foreach (ClassProfile cp in lstClass)
            {
                // Lớp này có học sinh, in
                if (dic.ContainsKey(cp.ClassProfileID) && dic[cp.ClassProfileID] != null && dic[cp.ClassProfileID].Any())
                {
                    sheet = oBook.CopySheetToBeforeLast(currentSheet);
                    //IXLWorksheet sheet = workbook.Worksheet(key);
                    //IXLRange originalRange = sheet.Range("B2:G12");
                    int iterator = 2;
                    for (var i = 0; i < dic[cp.ClassProfileID].Count; i++)
                    {
                        PupilProfile pupil = dic[cp.ClassProfileID][i];
                        if (i % 2 == 0)
                        {  // bỏ qua 1 phần tử đầu tiên không copy
                            if (i > 0)
                            {
                                sheet.CopyPasteSameSize(originalRange, "B" + iterator);
                                count++;
                            }
                            sheet.SetCellValue(string.Format("B{0}", iterator), "='Thong tin chung'!B2");
                            sheet.SetCellValue(string.Format("B{0}", iterator + 1), "='Thong tin chung'!B3");
                            sheet.SetCellValue(string.Format("E{0}", iterator + 5), pupil.FullName.ToUpper());
                            sheet.SetCellValue(string.Format("E{0}", iterator + 6), string.Format("Mã học sinh: {0}", pupil.PupilCode));
                            sheet.GetRange(iterator + 6, 5, iterator + 6, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                            sheet.SetCellValue(string.Format("F{0}", iterator + 7), "'" + pupil.BirthDate.ToString("dd/MM/yyyy"));
                            sheet.SetCellValue(string.Format("F{0}", iterator + 8), "'" + pupil.ClassProfile.DisplayName);
                            sheet.SetCellValue(string.Format("F{0}", iterator + 9), academicYear.DisplayTitle);
                        }
                        else
                        {
                            sheet.SetCellValue(string.Format("I{0}", iterator), "='Thong tin chung'!B2");
                            sheet.SetCellValue(string.Format("I{0}", iterator + 1), "='Thong tin chung'!B3");
                            sheet.SetCellValue(string.Format("L{0}", iterator + 5), pupil.FullName.ToUpper());
                            sheet.SetCellValue(string.Format("L{0}", iterator + 6), string.Format("Mã học sinh: {0}", pupil.PupilCode));
                            sheet.SetCellValue(string.Format("M{0}", iterator + 7), "'" + pupil.BirthDate.ToString("dd/MM/yyyy"));
                            sheet.SetCellValue(string.Format("M{0}", iterator + 8), "'" + pupil.ClassProfile.DisplayName);
                            sheet.SetCellValue(string.Format("M{0}", iterator + 9), academicYear.DisplayTitle);
                        }

                        // mỗi thẻ chiếm 10 dòng + 2 dòng trắng, 
                        // sau khi copy thì tăng biến đếm của con trỏ trỏ đến row của excel lên
                        if (i >= 1 && i % 2 == 1)
                        {
                            iterator += 10 + 2;
                        }
                        if (count > 0 && count % 5 == 0)
                        {
                            sheet.SetBreakPage(iterator);
                            count = 0;
                        }
                    }
                    sheet.Name = Utils.Utils.StripVNSignAndSpace(cp.DisplayName);
                }
            }
            //Xóa 2 sheet đầu tiên không dùng nữa
            currentSheet.Delete();
            Stream exelResult = oBook.ToStream();
            var midName = _globalInfo.AppliedLevel == 1 ? "TH" : (_globalInfo.AppliedLevel == 2 ? "THCS" : (_globalInfo.AppliedLevel == 3 ? "THPT" : ""));
            FileStreamResult result = new FileStreamResult(exelResult, "application/octet-stream");
            result.FileDownloadName = midName + ".xls";
            return result;
            //workbook.SaveAs(outPut);
            //outPut.Seek(0, SeekOrigin.Begin);
            //var midName = _globalInfo.AppliedLevel == 1 ? "TH" : (_globalInfo.AppliedLevel == 2 ? "THCS" : (_globalInfo.AppliedLevel == 3 ? "THPT" : ""));
            ////return new FileResult(outPut, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format(ExportTemplateName, midName));
            //FileStreamResult result = new FileStreamResult(outPut, "application/octet-stream");
            //result.FileDownloadName = string.Format(ExportTemplateName, midName);
            //return result;
        }
    }
}
