﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class PupilViolateReportController:BaseController
    {
         private readonly IReportSituationOfViolationBusiness ReportSituationOfViolationBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PupilViolateReportController(IReportSituationOfViolationBusiness situationOfViolationBusiness,
                       IAcademicYearBusiness academicYearID,
                       IProcessedReportBusiness processedReportBusiness)
        {
            this.ReportSituationOfViolationBusiness = situationOfViolationBusiness;
            this.AcademicYearBusiness = academicYearID;
             this.ProcessedReportBusiness = processedReportBusiness;          
        }
        public ActionResult Index()
        {
            GlobalInfo Global = new GlobalInfo();
            var lstEducation = Global.EducationLevels;
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            return View();
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo global = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = global.AcademicYearID.GetValueOrDefault();
            reportSituationOfViolationBO.SchoolID = global.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = global.AppliedLevel.Value;
            ReportDefinition reportDefinition = ReportSituationOfViolationBusiness.GetReportDefinitionOfPupilFault(reportSituationOfViolationBO);
            
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDefinition.IsPreprocessed == true)
            {
                processedReport = ReportSituationOfViolationBusiness.ExcelGetPupilFault(reportSituationOfViolationBO);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFault(reportSituationOfViolationBO);
                processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFault(reportSituationOfViolationBO, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            GlobalInfo glo = new GlobalInfo();
            reportSituationOfViolationBO.AcademicYearID = glo.AcademicYearID.Value;
            reportSituationOfViolationBO.SchoolID = glo.SchoolID.Value;
            reportSituationOfViolationBO.AppliedLevel = glo.AppliedLevel.Value;
            Stream excel = ReportSituationOfViolationBusiness.ExcelCreatePupilFault(reportSituationOfViolationBO);
            ProcessedReport processedReport = ReportSituationOfViolationBusiness.ExcelInsertPupilFault(reportSituationOfViolationBO, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value,_globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", GlobalInfo.AcademicYearID},
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

    }
}