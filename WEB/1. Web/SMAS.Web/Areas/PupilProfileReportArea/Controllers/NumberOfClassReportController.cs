﻿using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Web.Utils;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.PupilProfileReportArea.Controllers
{
    public class NumberOfClassReportController : BaseController
    {
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public NumberOfClassReportController(IProcessedReportBusiness processedReportBusiness, IPupilOfClassBusiness pupilOfClassBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness, IClassProfileBusiness classProfileBusiness, IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness, ISchoolProfileBusiness schoolProfileBusiness)
        {
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }
        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[PupilProfileReportConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            return View();
        }
        #region Export Excel
        [ValidateAntiForgeryToken]
        public JsonResult GetNumberOfClassReport(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int reportType = int.Parse(frm["reportType"]);
            if (reportType == 1)
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THONGKESISO);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID}
                                                    };
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = PupilOfClassBusiness.GetProcessReportNumberOfClass(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = this.ExportExcel(educationLevelID);
                    processedReport = PupilOfClassBusiness.InsertProcessReportNumberOfClass(dic, excel);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THONG_KE_DO_TUOI);
                IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID}
                                                    };
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = PupilOfClassBusiness.GetPupilByAgeReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = PupilOfClassBusiness.CreatePupilByAgeReport(dic);
                    processedReport = PupilOfClassBusiness.InsertPupilByAgeReport(dic, excel, _globalInfo.AppliedLevel.Value);
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int reportType = int.Parse(frm["reportType"]);
            ProcessedReport processedReport = null;

            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID}
            };
            if (reportType == 1)
            {
                Stream excel = this.ExportExcel(educationLevelID);
                processedReport = PupilOfClassBusiness.InsertProcessReportNumberOfClass(dicInsert, excel);
                excel.Close();
            }
            else
            {
                Stream excel = PupilOfClassBusiness.CreatePupilByAgeReport(dicInsert);
                processedReport = PupilOfClassBusiness.InsertPupilByAgeReport(dicInsert, excel, _globalInfo.AppliedLevel.Value);
                excel.Close();
                
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.HS_THONGKESISO,
                SystemParamsInFile.HS_THONG_KE_DO_TUOI
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.HS_THONGKESISO,
                SystemParamsInFile.HS_THONG_KE_DO_TUOI
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        public Stream ExportExcel(int? educationLevelID)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", SystemParamsInFile.HS_THONGKESISO + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thong tin chung
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            string Title = "THỐNG KÊ SĨ SỐ HỌC SINH NĂM HỌC " + objAca.DisplayTitle;
            sheet.SetCellValue("A5", Title);
            //lay danh sach lop hoc theo khoi
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dicClass.Add("EducationLevelID", educationLevelID);
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            //lay danh sach hoc sinh theo cap
            var lstPOC = (from poc in PupilOfClassBusiness.All
                          join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                          join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                          where poc.AcademicYearID == _globalInfo.AcademicYearID
                          && poc.SchoolID == _globalInfo.SchoolID
                          && ((_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY && cp.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID && cp.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID6)
                             || (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY && cp.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID5 && cp.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID10)
                             || (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY && cp.EducationLevelID > GlobalConstants.EDUCATION_LEVEL_ID9 && cp.EducationLevelID < GlobalConstants.EDUCATION_LEVEL_ID13))
                          && lstClassID.Contains(poc.ClassID)
                          && ( poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status==GlobalConstants.PUPIL_STATUS_GRADUATED)
                          && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                          && pf.IsActive
                          group poc by new
                          {
                              poc.ClassID,
                              pf.EthnicID,
                              pf.Genre,
                              pf.ClassType,
                              cp.EducationLevelID
                          } into g
                          select new
                          {
                              ClassID = g.Key.ClassID,
                              EthnicID = g.Key.EthnicID,
                              Genre = g.Key.Genre,
                              ClassType = g.Key.ClassType,
                              EducationLevelID = g.Key.EducationLevelID,
                              CountPupil = g.Count()
                          });

            List<int> lstEducationLevelID = lstClassProfile.Select(p=>p.EducationLevelID).Distinct().ToList();
            int EduID = 0;
            int firstRow = 9;
            int startRow = 9;
            List<ClassProfile> lstCPtmp = new List<ClassProfile>();
            ClassProfile objtmp = null;
            string formular = string.Empty;
            IDictionary<int, int> dicTotal = new Dictionary<int, int>();
            for (int i = 0; i < lstEducationLevelID.Count; i++)
            {
                EduID = lstEducationLevelID[i];
                lstCPtmp = lstClassProfile.Where(p => p.EducationLevelID == EduID).ToList();
                sheet.SetCellValue(startRow, 1, "Khối " + EduID);
                //Hien tai dang fill cung theo template neu thay doi template phai sua lai
                //Tong so
                formular = "=SUM(B"+ (startRow + 1) +":B"+ (lstCPtmp.Count + startRow) +")";
                sheet.SetCellValue(startRow, 2, formular);
                //Nu
                formular = "=SUM(C" + (startRow + 1) + ":C" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 3, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(C" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 4, formular);
                //DT
                formular = "=SUM(E" + (startRow + 1) + ":E" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 5, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(E" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 6, formular);
                //Nu DT
                formular = "=SUM(G" + (startRow + 1) + ":G" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 7, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(G" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 8, formular);
                //Noi tru
                formular = "=SUM(I" + (startRow + 1) + ":I" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 9, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(I" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 10, formular);
                //DT
                formular = "=SUM(K" + (startRow + 1) + ":K" + (lstCPtmp.Count + startRow) + ")";
                sheet.SetCellValue(startRow, 11, formular);
                formular = "=IF(B" + startRow + "=0,0,ROUND(K" + startRow + "*100/B" + startRow + ",2))";
                sheet.SetCellValue(startRow, 12, formular);
                sheet.GetRange(startRow, 1, startRow, 12).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                dicTotal[i] = startRow;
                startRow++;
                for (int j = 0; j < lstCPtmp.Count; j++)
                {
                    objtmp = lstCPtmp[j];
                    sheet.SetCellValue(startRow, 1, objtmp.DisplayName);
                    var lstPOCtmp = lstPOC.Where(p => p.ClassID == objtmp.ClassProfileID);
                    //tong so
                    sheet.SetCellValue(startRow, 2, lstPOCtmp.Count() > 0 ? lstPOCtmp.Select(p => p.CountPupil).Sum() : 0);
                    //Nu
                    sheet.SetCellValue(startRow, 3, lstPOCtmp.Where(p => p.Genre == 0).Count() > 0 ? lstPOCtmp.Where(p => p.Genre == 0).Select(p => p.CountPupil).Sum() : 0);
                    formular = "=IF(B" + startRow + "=0,0,ROUND(C" + startRow + "*100/B" + startRow + ",2))";
                    sheet.SetFormulaValue(startRow, 4, formular);
                    //Dan toc
                    sheet.SetCellValue(startRow, 5, lstPOCtmp.Where(p => p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Count() > 0 ? lstPOCtmp.Where(p => p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Select(p => p.CountPupil).Sum() : 0);
                    formular = "=IF(B" + startRow + "=0,0,ROUND(E" + startRow + "*100/B" + startRow + ",2))";
                    sheet.SetFormulaValue(startRow, 6, formular);
                    //Nu dan toc
                    sheet.SetCellValue(startRow, 7, lstPOCtmp.Where(p => p.Genre == 0 && p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Count() > 0 ? lstPOCtmp.Where(p => p.Genre == 0 && p.EthnicID != null && p.EthnicID > SystemParamsInFile.ETHNIC_ID_KINH && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).Select(p => p.CountPupil).Sum() : 0);
                    formular = "=IF(B" + startRow + "=0,0,ROUND(G" + startRow + "*100/B" + startRow + ",2))";
                    sheet.SetFormulaValue(startRow, 8, formular);
                    //Noi tru
                    sheet.SetCellValue(startRow, 9, lstPOCtmp.Where(p => p.ClassType == 1).Count() > 0 ? lstPOCtmp.Where(p => p.ClassType == 1).Select(p => p.CountPupil).Sum() : 0);
                    formular = "=IF(B" + startRow + "=0,0,ROUND(I" + startRow + "*100/B" + startRow + ",2))";
                    sheet.SetFormulaValue(startRow, 10, formular);
                    //Ban tru
                    sheet.SetCellValue(startRow, 11, lstPOCtmp.Where(p => (p.ClassType == 2 || p.ClassType == 3)).Count() > 0 ? lstPOCtmp.Where(p => (p.ClassType == 2 || p.ClassType == 3)).Select(p => p.CountPupil).Sum() : 0);
                    formular = "=IF(B" + startRow + "=0,0,ROUND(K" + startRow + "*100/B" + startRow + ",2))";
                    sheet.SetFormulaValue(startRow, 12, formular);
                    startRow++;
                }
            }
            if (lstEducationLevelID.Count > 0)
            {
                //fill tong cong
                int startRowTotal = firstRow + lstClassProfile.Count + lstEducationLevelID.Count;
                sheet.SetCellValue(startRowTotal, 1, "Tổng cộng");
                string formularTT = "=SUM(B";
                string formularNu = "=SUM(C";
                string formularDT = "=SUM(E";
                string formularNDT = "=SUM(G";
                string formularNT = "=SUM(I";
                string formularBT = "=SUM(K";
                for (int i = 0; i < dicTotal.Count; i++)
                {
                    formularTT += dicTotal[i] + ",B";
                    formularNu += dicTotal[i] + ",C";
                    formularDT += dicTotal[i] + ",E";
                    formularNDT += dicTotal[i] + ",G";
                    formularNT += dicTotal[i] + ",I";
                    formularBT += dicTotal[i] + ",K";
                }
                formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                formularNu = formularNu.Substring(0, formularNu.Length - 2) + ")";
                formularDT = formularDT.Substring(0, formularDT.Length - 2) + ")";
                formularNDT = formularNDT.Substring(0, formularNDT.Length - 2) + ")";
                formularNT = formularNT.Substring(0, formularNT.Length - 2) + ")";
                formularBT = formularBT.Substring(0, formularBT.Length - 2) + ")";
                //Tong
                sheet.SetCellValue(startRowTotal, 2, formularTT);
                //Nu
                sheet.SetCellValue(startRowTotal, 3, formularNu);
                formular = "=IF(B" + startRowTotal + "=0,0,ROUND(C" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                sheet.SetCellValue(startRowTotal, 4, formular);
                //DT
                sheet.SetCellValue(startRowTotal, 5, formularDT);
                formular = "=IF(B" + startRowTotal + "=0,0,ROUND(E" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                sheet.SetCellValue(startRowTotal, 6, formular);
                //Nu DT
                sheet.SetCellValue(startRow, 7, formularNDT);
                formular = "=IF(B" + startRowTotal + "=0,0,ROUND(G" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                sheet.SetCellValue(startRowTotal, 8, formular);
                //Noi tru
                sheet.SetCellValue(startRowTotal, 9, formularNT);
                formular = "=IF(B" + startRowTotal + "=0,0,ROUND(I" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                sheet.SetCellValue(startRowTotal, 10, formular);
                //DT
                sheet.SetCellValue(startRowTotal, 11, formularBT);
                formular = "=IF(B" + startRowTotal + "=0,0,ROUND(K" + startRowTotal + "*100/B" + startRowTotal + ",2))";
                sheet.SetCellValue(startRowTotal, 12, formular);
                sheet.GetRange(startRowTotal, 1, startRowTotal, 12).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            }
            //ke khung
            sheet.GetRange(9, 1, 9 + lstEducationLevelID.Count + lstClassProfile.Count,12).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            //fill chữ ký
            string strDate = (objSP.District != null ? objSP.District.DistrictName : "") + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
            int startRowDate = firstRow + lstClassProfile.Count + lstEducationLevelID.Count + 3;
            sheet.GetRange(startRowDate, 8, startRowDate, 12).Merge();
            sheet.SetCellValue(startRowDate, 8, strDate);
            sheet.GetRange(startRowDate + 1, 8, startRowDate + 1, 12).Merge();
            sheet.SetCellValue(startRowDate + 1, 8, "HIỆU TRƯỞNG");
            sheet.GetRange(startRowDate + 1, 8, startRowDate + 1, 12).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.GetRange(startRowDate + 5, 8, startRowDate + 5, 12).Merge();
            sheet.GetRange(startRowDate + 5, 8, startRowDate + 5, 12).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.SetCellValue(startRowDate + 5, 8, SchoolProfileBusiness.Find(_globalInfo.SchoolID).HeadMasterName);
            return oBook.ToStream();
        }
        #endregion
    }
}