﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilProfileReportArea
{
    public class PupilProfileReportConstants
    {
        #region Danh sach hoc sinh vi pham
        public const string LIST_EDUCATIONLEVEL = "listEducation";
        #endregion

        #region Danh sach hoc sinh chuyen lop
        public const string LIST_SEMESTER = "listSemester";
        public const string DF_SEMESTER = "defaultSemester";
        #endregion
        public const string LIST_CLASS = "listClass";

        #region in giay khen
        public const string LIST_RESULT = "LIST_RESULT";
        public const string CBO_DILOPMA_TEMPLATE = "CBO_DILOPMA_TEMPLATE";
        public const string CBO_EDUCATION_LEVEL = "CBO_EDUCATION_LEVEL";
        public const string CBO_CLASS = "CBO_CLASS";
        public const string CBO_SEMESTER = "CBO_SEMESTER";
        public const string CBO_TYPE = "CBO_TYPE";
        public const string CBO_APPELLATION = "CBO_APPELLATION";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const int PageSize = 20;
        public const string ENABLE_PAGING = "ENABLE_PAGING";
        public const string SCHOOL_PROFILE = "SCHOOL_PROFILE";
        public const string TEMPLATE_KEY_PUPIL_NAME = "[TÊN HỌC SINH]";
        public const string TEMPLATE_KEY_PUPIL_BIRTH_DAY = "[NGÀY SINH HS]";
        public const string TEMPLATE_KEY_PUPIL_GENRE = "[GIỚI TÍNH HS]";
        public const string TEMPLATE_KEY_PUPIL_CODE = "[MÃ HỌC SINH]";
        public const string TEMPLATE_KEY_CLASS_NAME = "[TÊN LỚP]";
        public const string TEMPLATE_KEY_SUPERVISING_DEPT = "[TÊN ĐƠN VỊ QUẢN LÝ]";
        public const string TEMPLATE_KEY_HEAD_TEACHER_NAME = "[TÊN GV CHỦ NHIỆM]";
        public const string TEMPLATE_KEY_PUPIL_TEMP_LIVE_PLACE = "[ĐỊA CHỈ TẠM TRÚ HS]";
        public const string TEMPLATE_KEY_PUPIL_LIVE_PLACE = "[ĐỊA CHỈ THƯỜNG TRÚ HS]";
        public const string TEMPLATE_KEY_FATHER_NAME = "[HỌ TÊN CHA]";
        public const string TEMPLATE_KEY_FATHER_MOBILE = "[SĐT CHA]";
        public const string TEMPLATE_KEY_MOTHER_NAME = "[HỌ TÊN MẸ]";
        public const string TEMPLATE_KEY_MOTHER_MOBILE = "[SĐT MẸ]";

        public const string TEMPLATE_KEY_CONTENT = "[NỘI DUNG KHEN THƯỞNG]";
        public const string TEMPLATE_KEY_SCHOOL_NAME = "[TÊN TRƯỜNG]";
        public const string TEMPLATE_KEY_ACADEMIC_YEAR = "[NĂM HỌC]";
        public const string TEMPLATE_KEY_HEAD_NAME = "[TÊN HIỆU TRƯỞNG]";
        public const string TEMPLATE_KEY_SEMESTER = "[HỌC KỲ]";
        
        #endregion
    }
}