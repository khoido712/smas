﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilProfileReportArea.Models
{
    public class ListViewModel
    {
        public int PupilID { get; set; }

        [ResourceDisplayName("Common_Label_FullName")]
        public string PupilName { get; set; }

        public string Name { get; set; }

        [ResourceDisplayName("Common_Label_PupilCode")]
        public string PupilCode { get; set; }

        
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("Common_Label_BirthDate")]
        public string StrBirthDate
        {
            get
            {
                return BirthDate.ToString("dd/MM/yyyy");
            }
        }

        [ResourceDisplayName("Common_Label_Genre")]
        public string Genre { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        public string ClassName { get; set; }

        public int TypeID { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_Type")]
        public string TypeName { get; set; }

        [ResourceDisplayName("Common_Label_Content")]
        public string Content { get; set; }

        public int EducationLevel { get; set; }

        public int? ClassOrderNumber { get; set; }

        public int Appellation { get; set; }

        public string SchoolName { get; set; }

        public string SupervisingDeptName { get; set; }

        public string HeadTeacherName { get; set; }

        public string TempLivePlace { get; set; }

        public string LivePlace { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string FatherMobile { get; set; }

        public string MotherMobile { get; set; }

        public int? OrderInClass { get; set; }

        public int ClassId { get; set; }

    }
}