﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilProfileReportArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilCard_Label_DiplomaTemplateID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_DILOPMA_TEMPLATE)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? DiplomaTemplateID { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_EducationLevelID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_CLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_SEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? Semester { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_Type")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_TYPE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "onTypeChange()")]
        public int? Type { get; set; }

        [ResourceDisplayName("DiplomaExporting_Label_Appellation")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilProfileReportConstants.CBO_APPELLATION)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public string Appellation { get; set; }
    }
}