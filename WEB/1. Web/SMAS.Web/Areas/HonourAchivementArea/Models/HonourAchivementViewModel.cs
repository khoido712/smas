/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.DAL.Repository;
using SMAS.Business.Business;
namespace SMAS.Web.Areas.HonourAchivementArea.Models
{
    public class HonourAchivementViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 HonourAchivementID { get; set; }
		[ScaffoldColumn(false)]						
		public System.Int32 AcademicYearID { get; set; }
		[ScaffoldColumn(false)]						
		public System.Int32 SchoolID { get; set; }								
									
		[ScaffoldColumn(false)]
        public System.Nullable<System.Int32> FacultyID { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> EmployeeID { get; set; }					
		[ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> AchivedDate { get; set; }



        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName
        {
            get;
            set;
        }
        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivement_Label_HonourAchivementType")]
        public string Resolution { get; set; }


        [ResourceDisplayName("HonourAchivement_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementConstants.LIST_FACULTY)]
        public int SchoolFacultyID { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_HonourAchivementType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementConstants.LIST_HONOUR_ACHIVEMENT_TYPE)]
        public int HonourAchivementTypeID { get; set; }

        //[ResDisplayName("TeacherGrading_Label_TeacherGrade")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[UIHint("Combobox")]
        //[AdditionalMetadata("ViewDataKey", TeacherGradingConstants.LIST_TEACHERGRADE)]
        //public int TeacherGradeID { get; set; }

        [ResourceDisplayName("HonourAchivement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }



    }
}


