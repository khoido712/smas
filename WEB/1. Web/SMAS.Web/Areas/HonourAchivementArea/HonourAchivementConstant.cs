/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.HonourAchivementArea
{
    public class HonourAchivementConstants
    {
        public const string LIST_HONOURACHIVEMENT = "listHonourAchivement";
        public const string LIST_FACULTY = "listFaculty";
        public const string LIST_TEACHEROFFACULTY = "listTeacherOfFaculty";
        public const string ISYEAR = "ISYEAR";
        public const string LIST_HONOUR_ACHIVEMENT_TYPE = "listHonourAchivementType";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}