﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HonourAchivementArea
{
    public class HonourAchivementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HonourAchivementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HonourAchivementArea_default",
                "HonourAchivementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
