/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.HonourAchivementArea.Models;
using SMAS.Business.BusinessExtensions;

namespace SMAS.Web.Areas.HonourAchivementArea.Controllers
{
    public class HonourAchivementController : BaseController
    {        
        private readonly IHonourAchivementBusiness HonourAchivementBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IHonourAchivementTypeBusiness HonourAchivementTypeBusiness;
        public HonourAchivementController(IHonourAchivementTypeBusiness honourAchivementTypeBusiness,ISchoolFacultyBusiness schoolFacultyBusiness, IHonourAchivementBusiness honourachivementBusiness, IEmployeeBusiness employeeBusiness)
		{
			this.HonourAchivementBusiness = honourachivementBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.HonourAchivementTypeBusiness = honourAchivementTypeBusiness;
		}
		
		//
        // GET: /HonourAchivement/

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            this.Search(0);
            return View();
        }

		//
        // GET: /HonourAchivement/Search

        
        public PartialViewResult Search(int? FacultyID)
        {
            if (FacultyID == null)
            {
                FacultyID = 0;
            }
            //Utils.Utils.TrimObject(frm);
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["AcademicYearID"] = Global.AcademicYearID;
            SearchInfo["FacultyID"] = FacultyID;
            SearchInfo["AppliedLevel"] = Global.AppliedLevel;
            IEnumerable<HonourAchivementViewModel> lst = this._Search(Global.SchoolID.Value,SearchInfo);
            ViewData[HonourAchivementConstants.LIST_HONOURACHIVEMENT] = lst;
            ViewData[HonourAchivementConstants.ISYEAR] = true;
            if (!Global.IsCurrentYear)
            {
                ViewData[HonourAchivementConstants.ISYEAR] = false;
            }



            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(HonourAchivementViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HonourAchivement honour = new HonourAchivement();
            
            honour.FacultyID = frm.SchoolFacultyID;
            honour.Description = frm.Description;
            honour.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            honour.SchoolID = GlobalInfo.SchoolID.Value;
            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();
            FacultySearchInfo["FacultyID"] = frm.SchoolFacultyID;
            FacultySearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            honour.HonourAchivementTypeID = (int)frm.HonourAchivementTypeID;
            List<HonourAchivement> lstHonourAchivementFaculty = HonourAchivementBusiness.SearchHonourAchivementForFaculty(GlobalInfo.SchoolID.Value, FacultySearchInfo).ToList();
            if(lstHonourAchivementFaculty.Where(o=>o.HonourAchivementTypeID ==honour.HonourAchivementTypeID).Count() >0 ){
                HonourAchivementType honourrAchivementType = HonourAchivementTypeBusiness.Find(honour.HonourAchivementTypeID);
                string str = Res.Get("HonourAchivement_Label_DuplicateHonourAchivement") + " " + honourrAchivementType.Resolution;
                return Json(new JsonMessage(str, JsonMessage.ERROR));
            }
            this.HonourAchivementBusiness.InsertHonourAchivementForFaculty(honour);
            this.HonourAchivementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int HonourAchivementID)
        {
            this.CheckPermissionForAction(HonourAchivementID, "HonourAchivement");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            HonourAchivement honourachivement = this.HonourAchivementBusiness.Find(HonourAchivementID);
            TryUpdateModel(honourachivement);
            Utils.Utils.TrimObject(honourachivement);

            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();

            FacultySearchInfo["FacultyID"] = honourachivement.FacultyID;
            FacultySearchInfo["AcademicYearID"] = global.AcademicYearID;
            List<HonourAchivement> lstHonourAchivementFaculty = HonourAchivementBusiness.SearchHonourAchivementForFaculty(global.SchoolID.Value, FacultySearchInfo).ToList();
            if (lstHonourAchivementFaculty.Where(o => o.HonourAchivementTypeID == honourachivement.HonourAchivementTypeID && o.HonourAchivementID != honourachivement.HonourAchivementID).Count() > 0)
            {
                HonourAchivementType honourrAchivementType = HonourAchivementTypeBusiness.Find(honourachivement.HonourAchivementTypeID);
                string str = Res.Get("HonourAchivement_Label_DuplicateHonourAchivement") + " " + honourrAchivementType.Resolution;
                return Json(new JsonMessage(str, JsonMessage.ERROR));
            }
            this.HonourAchivementBusiness.UpdateHonourAchivementForFaculty(honourachivement);
            this.HonourAchivementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "HonourAchivement");
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("HonourAchivement", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.HonourAchivementBusiness.DeleteHonourAchivementForFaculty(new GlobalInfo().SchoolID.Value,id);
            this.HonourAchivementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<HonourAchivementViewModel> _Search(int SchoolID,IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[HonourAchivementConstants.HAS_PERMISSION] = GetMenupermission("HonourAchivement",global.UserAccountID, global.IsAdmin);
            IQueryable<HonourAchivement> query = this.HonourAchivementBusiness.SearchHonourAchivementForFaculty(SchoolID,SearchInfo);
            List<HonourAchivementViewModel> lst = query.Select(o => new HonourAchivementViewModel {               
						HonourAchivementID = o.HonourAchivementID,								
						AcademicYearID = o.AcademicYearID,								
						SchoolID = o.SchoolID,								
						HonourAchivementTypeID = o.HonourAchivementTypeID,								
						FacultyID = o.FacultyID,	
						EmployeeID = o.EmployeeID,
						AchivedDate = o.AchivedDate,								
						Description = o.Description,								
                       Resolution = o.HonourAchivementType.Resolution,
                       FacultyName = o.SchoolFaculty.FacultyName
            }).ToList();

            if (lst.Count > 0 && lst != null)
            {
                lst = lst.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.FacultyName)).ToList();
            }

            return lst;
        }

        #region SetViewData
        private void SetViewData()
        {
            
            GlobalInfo GlobalInfo = new GlobalInfo();
            
            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();
            FacultySearchInfo["IsActive"] = true;
            List<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, FacultySearchInfo).OrderBy(o => o.FacultyName).ToList();


            ViewData[HonourAchivementConstants.LIST_FACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            

            //cbb HonourAchivementType
            
            IDictionary<string, object> HonourTypeInfo = new Dictionary<string, object>();
            HonourTypeInfo["IsActive"] = true;
            HonourTypeInfo["Type"] = SystemParamsInFile.HONOUR_ACHIVEMENT_TYPE_FACULTY;
            List<HonourAchivementType> lsHonourType = HonourAchivementTypeBusiness.Search(HonourTypeInfo).OrderBy(o => o.Resolution).ToList();


            ViewData[HonourAchivementConstants.LIST_HONOUR_ACHIVEMENT_TYPE] = new SelectList(lsHonourType, "HonourAchivementTypeID", "Resolution");

        }
        #endregion



    }
}





