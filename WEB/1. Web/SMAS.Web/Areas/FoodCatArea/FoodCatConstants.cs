﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.FoodCatArea
{
    public class FoodCatConstants
    {
        public const string LIST_FOODCAT = "listFoodCat";
        public const string LIST_FOODCAT_CUSTOM = "listFoodCatCustom";
        public const string LIST_TYPEOFFOOD = "listTypeOfFood";
        public const string LIST_FOODGROUP = "listFoodGroup";
        public const string LIST_FOODPACKING = "listFoodPacking";
        public const string LIST_GROUPTYPE = "listGroupType";
        public const string LIST_CalculationUnit = "listCalculationUnit";
        public const string LIST_MINERALCAT = "listMineralCat";

    }
}