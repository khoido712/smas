﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.FoodCatArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
namespace SMAS.Web.Areas.FoodCatArea.Controllers
{
    public class FoodCatController : BaseController
    {
        private readonly IFoodCatBusiness FoodCatBusiness;
        private readonly ITypeOfFoodBusiness TypeOfFoodBusiness;
        private readonly IFoodMineralBusiness FoodMineralBusiness;
        private readonly IFoodGroupBusiness FoodGroupBusiness;
        private readonly IFoodPackingBusiness FoodPackingBusiness;
        private readonly IMinenalCatBusiness MinenalCatBusiness;
        private readonly IDailyMenuFoodBusiness DailyMenuFoodBusiness;
        private readonly IFoodMineralBusiness FoodMineral;
        private readonly IDishDetailBusiness DishDetailBusiness;

        public FoodCatController(IFoodCatBusiness foodcatBusiness,
                                 ITypeOfFoodBusiness typeoffoodBusiness,
                                 IFoodGroupBusiness foodgroupBusiness,
                                 IMinenalCatBusiness minenalcatBusiness,
                                 IFoodPackingBusiness foodpackingBusiness,
                                 IFoodMineralBusiness foodmineralBusiness,
                                 IDailyMenuFoodBusiness dailyMenuFoodBusiness,
                                IDishDetailBusiness dishDetailBusiness,
            IFoodMineralBusiness foodMineralBusiness)
		{
            this.FoodCatBusiness = foodcatBusiness;
            this.FoodGroupBusiness = foodgroupBusiness;
            this.MinenalCatBusiness = minenalcatBusiness;
            this.FoodPackingBusiness = foodpackingBusiness;
            this.TypeOfFoodBusiness = typeoffoodBusiness;
            this.FoodMineralBusiness = foodmineralBusiness;
            this.DailyMenuFoodBusiness = dailyMenuFoodBusiness;
            this.FoodMineral = foodMineralBusiness;
            this.DishDetailBusiness = dishDetailBusiness;
		}
		
		//
        // GET: /FoodCat/

        #region SetViewData
        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;

            IEnumerable<FoodCatViewModel> lst = this._Search(SearchInfo);

            //Kiem tra ton tai food_id tai table Dish-detail va Daily_menu_food
            List<FoodCatViewModel> list = new List<FoodCatViewModel>();
            List<DishDetail> listDishDetail = DishDetailBusiness.All.ToList();
            List<DailyMenuFood> listDailyMenuFood = DailyMenuFoodBusiness.All.ToList();
            foreach (FoodCatViewModel obj in lst)
            {
                var listDishDetailContraint = listDishDetail.Where(o => o.FoodID == obj.FoodID).FirstOrDefault();
                var listDailyMenuFoodContraint = listDailyMenuFood.Where(o => o.FoodID == obj.FoodID).FirstOrDefault();
                if (listDishDetailContraint != null || listDailyMenuFoodContraint!=null)
                {
                    obj.DeleteAble = false;
                }
                else
                {
                    obj.DeleteAble = true;
                }
                list.Add(obj);
            }

            List<CustomViewFootCat> lstCustom = new List<CustomViewFootCat>();
            Dictionary<int, List<FoodMineralCatBO>> lstMineral = new Dictionary<int, List<FoodMineralCatBO>>();
            if (lst != null)
            {
                foreach (var item in list)
                {
                    List<FoodMineralCatBO> lstMineralBO = this._SearchMineral(item.FoodID);
                    CustomViewFootCat cvft = new CustomViewFootCat();
                    cvft.footCatViewModel = item;
                    cvft.footNineralCatBO = lstMineralBO;
                    lstMineral.Add(item.FoodID, lstMineralBO);
                    lstCustom.Add(cvft);
                }
            }

           

            ViewData["listMineral"] = lstMineral;
            ViewData[FoodCatConstants.LIST_FOODCAT_CUSTOM] = lstCustom;
            ViewData[FoodCatConstants.LIST_FOODCAT] = lst;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;

            List<TypeOfFood> lstTypeofFood = this.TypeOfFoodBusiness.Search(dic).ToList();

            ViewData[FoodCatConstants.LIST_TYPEOFFOOD] = new SelectList(lstTypeofFood, "TypeOfFoodID", "TypeOfFoodName");

            List<FoodGroup> lstFoodGroup = this.FoodGroupBusiness.All.Where(o=> o.IsActive==true).ToList();
            ViewData[FoodCatConstants.LIST_FOODGROUP] = new SelectList(lstFoodGroup, "FoodGroupID", "FoodGroupName");


            List<FoodPacking> lstFoodPacking = this.FoodPackingBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[FoodCatConstants.LIST_FOODPACKING] = new SelectList(lstFoodPacking, "FoodPackingID", "FoodPackingName");


            List<ComboObject> listGroupType = new List<ComboObject>();
            listGroupType.Add(new ComboObject("1", "ĐV"));
            listGroupType.Add(new ComboObject("2", "TV"));

            ViewData[FoodCatConstants.LIST_GROUPTYPE] = new SelectList(listGroupType, "key", "value");

            List<ComboObject> listCalculationUnit = new List<ComboObject>();
            listCalculationUnit.Add(new ComboObject("1", "Kg"));
            listCalculationUnit.Add(new ComboObject("2", "g"));

            ViewData[FoodCatConstants.LIST_CalculationUnit] = new SelectList(listCalculationUnit, "key", "value");

            List<MinenalCat> lstMinenalCat = this.MinenalCatBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[FoodCatConstants.LIST_MINERALCAT] = lstMinenalCat.ToList();

            List<MinenalCat> lstmineralcat = this.MinenalCatBusiness.All.Where(o => o.IsActive == true).OrderBy(o=>o.MinenalName).ToList();
            ViewData["lstmineralcat"] = lstmineralcat;

        }

        #endregion


        public ActionResult Index()
        {
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("vi-VN");
            SetViewData();
            return View();
        }

		//
        // GET: /FoodCat/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FoodName"] = frm.FoodName;
            SearchInfo["TypeOfFoodID"] = frm.TypeOfFoodID;
            SearchInfo["IsActive"] = true;

            IEnumerable<FoodCatViewModel> lst = this._Search(SearchInfo);
            //Kiem tra ton tai food_id tai table Dish-detail va Daily_menu_food
            List<FoodCatViewModel> list = new List<FoodCatViewModel>();
            List<DishDetail> listDishDetail = DishDetailBusiness.All.ToList();
            List<DailyMenuFood> listDailyMenuFood = DailyMenuFoodBusiness.All.ToList();
            foreach (FoodCatViewModel obj in lst)
            {
                var listDishDetailContraint = listDishDetail.Where(o => o.FoodID == obj.FoodID).FirstOrDefault();
                var listDailyMenuFoodContraint = listDailyMenuFood.Where(o => o.FoodID == obj.FoodID).FirstOrDefault();
                if (listDishDetailContraint != null || listDailyMenuFoodContraint != null)
                {
                    obj.DeleteAble = false;
                }
                else
                {
                    obj.DeleteAble = true;
                }
                list.Add(obj);
            }

            ViewData[FoodCatConstants.LIST_FOODCAT] = lst;
            List<CustomViewFootCat> lstCustom = new List<CustomViewFootCat>();
            Dictionary<int, List<FoodMineralCatBO>> lstMineral = new Dictionary<int, List<FoodMineralCatBO>>();
            if (lst != null)
            {
                foreach (var item in list)
                {
                    CustomViewFootCat cvft = new CustomViewFootCat();
                    cvft.footCatViewModel = item;
                    List<FoodMineralCatBO> lstMineralBO = this._SearchMineral(item.FoodID);
                    cvft.footNineralCatBO = lstMineralBO;
                    lstMineral.Add(item.FoodID, lstMineralBO);
                    lstCustom.Add(cvft);
                }
            }
            ViewData[FoodCatConstants.LIST_FOODCAT_CUSTOM] = lstCustom;
            ViewData["listMineral"] = lstMineral;

            List<MinenalCat> lstmineralcat = this.MinenalCatBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData["lstmineralcat"] = lstmineralcat;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        
        public JsonResult Create( FormCollection frm)
        {
            int price = Convert.ToInt32(frm.Get("Price"));
            /*TungND12*/
            //int fat = Convert.ToInt32(frm.Get("Price"));
            //int protein = Convert.ToInt32(frm.Get("Price"));
            //int sugar = Convert.ToInt32(frm.Get("Price"));

            /*DungVA*/
            decimal fat = frm.Get("Fat")==""? 0: Convert.ToDecimal(frm.Get("Fat"));
            decimal protein = frm.Get("Protein") == "" ? 0 : Convert.ToDecimal(frm.Get("Protein"));
            decimal sugar = frm.Get("Sugar") == "" ? 0 : Convert.ToDecimal(frm.Get("Sugar"));
            if (price == 0)
            {
                return Json(new JsonMessage(Res.Get("FoodCat_Error_Price"), "error"));
            }

            if ((protein  + fat + sugar ) >= 100)
            {
                return Json(new JsonMessage(Res.Get("FoodCat_Error_PLG"), "error"));
            }

            
            FoodCat foodgroup = new FoodCat();
            foodgroup.FoodName = frm.Get("FoodName");
            foodgroup.ShortName = frm.Get("ShortName");
            foodgroup.TypeOfFoodID = Convert.ToInt32(frm.Get("TypeOfFoodID"));
            foodgroup.GroupType = Convert.ToByte(frm.Get("GroupType"));
            foodgroup.DiscardedRate = frm.Get("DiscardedRate") == ""  ? (int)0 : Convert.ToInt16(frm.Get("DiscardedRate"));
            foodgroup.FoodPackingID = frm.Get("FoodPackingID") != "" ? Convert.ToInt32(frm.Get("FoodPackingID")) : -1;
            if (frm.Get("FoodGroupID") != "")
            {
                foodgroup.FoodGroupID = Convert.ToInt32(frm.Get("FoodGroupID"));
            }
            else
            {
                foodgroup.FoodGroupID = null;
            }
            foodgroup.Fat = fat;
            foodgroup.Protein = protein;
            foodgroup.Sugar = sugar;
            foodgroup.CalculationUnit = Convert.ToByte(frm.Get("CalculationUnit"));
            foodgroup.Price = price;
            foodgroup.CreatedDate = DateTime.Now;
            foodgroup.IsActive = true;

            List<FoodMineral> lstFoodMineral = GetFoodMineral(frm);
            this.FoodCatBusiness.Insert(foodgroup, lstFoodMineral);
            this.FoodCatBusiness.Save();
            this.FoodMineralBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        
        public JsonResult Edit(FormCollection frm)
        {

            int price = Convert.ToInt32(frm.Get("Price"));
            /*TungND12*/
            //int fat = Convert.ToInt32(frm.Get("Price"));
            //int protein = Convert.ToInt32(frm.Get("Price"));
            //int sugar = Convert.ToInt32(frm.Get("Price"));

            /*DungVA*/
            decimal fat = Convert.ToDecimal(frm.Get("Fat"));
            decimal protein = Convert.ToDecimal(frm.Get("Protein"));
            decimal sugar = Convert.ToDecimal(frm.Get("Sugar"));
            if (price == 0)
            {
                return Json(new JsonMessage(Res.Get("FoodCat_Error_Price"), "error"));
            }

            if ((protein + fat + sugar) >= 100)
            {
                return Json(new JsonMessage(Res.Get("FoodCat_Error_PLG"), "error"));
            }

            FoodCat foodgroup = new FoodCat();
            foodgroup.FoodID = Convert.ToInt32(frm.Get("FoodID"));
            foodgroup.FoodName = frm.Get("FoodName");
            foodgroup.ShortName = frm.Get("ShortName");
            foodgroup.TypeOfFoodID = Convert.ToInt32(frm.Get("TypeOfFoodID"));
            foodgroup.GroupType = Convert.ToByte(frm.Get("GroupType"));
            foodgroup.DiscardedRate = frm.Get("DiscardedRate") == "" ? (int)0 : Convert.ToInt16(frm.Get("DiscardedRate"));
            foodgroup.FoodPackingID = frm.Get("FoodPackingID") != "" ? Convert.ToInt32(frm.Get("FoodPackingID")) : -1;
            if (frm.Get("FoodGroupID") != "")
            {
                foodgroup.FoodGroupID = Convert.ToInt32(frm.Get("FoodGroupID"));
            }
            else
            {
                foodgroup.FoodGroupID = null;
            }
            foodgroup.Fat = fat;
            foodgroup.Protein = protein;
            foodgroup.Sugar = sugar;
            foodgroup.CalculationUnit = Convert.ToByte(frm.Get("CalculationUnit"));
            foodgroup.Price = price;
            foodgroup.CreatedDate = DateTime.Now;
            foodgroup.IsActive = true;

            List<FoodMineral> lstFoodMineral = GetFoodMineral(frm);
            this.FoodCatBusiness.Update(foodgroup, lstFoodMineral);
            this.FoodCatBusiness.Save();
            this.FoodMineralBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            int CountDailyMenu = this.DailyMenuFoodBusiness.All.Where(p => p.FoodID == id).Count();
            int CountFootMineral = this.FoodMineralBusiness.All.Where(p => p.FoodID == id).Count();
            if (CountDailyMenu > 0 || CountFootMineral > 0)
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_UsingFood")));
            }
            this.FoodCatBusiness.Delete(id);
            this.FoodCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<FoodCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<FoodCat> query = this.FoodCatBusiness.Search(SearchInfo);
            IQueryable<FoodCatViewModel> lst = query.Select(o => new FoodCatViewModel
            {               
				FoodID = o.FoodID,
                FoodName = o.FoodName,
                FoodPackingID = o.FoodPackingID.Value,
                GroupType = o.GroupType,
                DiscardedRate = o.DiscardedRate,
                Protein = o.Protein,
                Fat = o.Fat,
                Sugar = o.Sugar,
                Calo = o.Calo,
                ShortName = o.ShortName,
                TypeOfFoodID = o.TypeOfFoodID,
                TypeOfFoodName = o.TypeOfFood.TypeOfFoodName,	
                Price = o.Price,
                FoodGroupID = o.FoodGroupID,
                FoodPackingName = o.FoodPacking.FoodPackingName,
                CalculationUnit = o.CalculationUnit,
				
            });

            List<FoodCatViewModel> lstFoodCat = new List<FoodCatViewModel>();
            foreach (var item in lst)
            {
                if (item.GroupType == 1)
                {
                    item.StringGroupType = "ĐV";
                }

                if (item.GroupType == 2)
                {
                    item.StringGroupType = "TV";
                }

              lstFoodCat.Add(item);
            }


            return lstFoodCat;
        }

        /// <summary>
        /// Search Mineral
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>

        private List<FoodMineralCatBO> _SearchMineral(int FoodID)
        {

           
                IDictionary<string, object> SearchInfoMineral = new Dictionary<string, object>();
                SearchInfoMineral["FoodID"] = FoodID;
                IQueryable<FoodMineral> queryFoodMineral = this.FoodMineralBusiness.Search(SearchInfoMineral);
                IQueryable<FoodMineralCatBO> FoodMineral = queryFoodMineral.Select(o => new FoodMineralCatBO
                {
                    MineralID = o.MinenalID,
                    Weight = o.Weight,
                    MinenalName = o.MinenalCat.MinenalName
                });
                List<FoodMineralCatBO> listMineral = new List<FoodMineralCatBO>();
                foreach (var itemMineral in FoodMineral)
                {
                    listMineral.Add(itemMineral);
                }

                
            return listMineral;
         }


        private List<FoodMineral> GetFoodMineral(FormCollection frm)
        {
            List<FoodMineral> lstFoodMineral = new List<FoodMineral>();
            List<MinenalCat> lstMinenalCat = this.MinenalCatBusiness.All.Where(o => o.IsActive == true).ToList();
            //sử dụng for() thay cho foreach() để cải thiện tốc độ
            for (int i = 0,size = lstMinenalCat.Count; i < size; i++)
            {
                var item = lstMinenalCat[i];
                FoodMineral foodmineral = new FoodMineral();
                foodmineral.MinenalID = item.MinenalCatID;
                foodmineral.Weight = frm.Get(item.MinenalName) == "" ? 0 : Convert.ToInt16(frm.Get(item.MinenalName));
                lstFoodMineral.Add(foodmineral);
            }
            //foreach (var item in lstMinenalCat)
            //{
            //    FoodMineral foodmineral = new FoodMineral();
            //    foodmineral.MinenalID = item.MinenalCatID;
            //    foodmineral.Weight = frm.Get(item.MinenalName) == "" ? 0 : Convert.ToInt16(frm.Get(item.MinenalName));
            //    lstFoodMineral.Add(foodmineral);
            //}
            return lstFoodMineral;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult GetFoodMineral(int FoodID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;

            List<TypeOfFood> lstTypeofFood = this.TypeOfFoodBusiness.Search(dic).ToList();

            ViewData[FoodCatConstants.LIST_TYPEOFFOOD] = new SelectList(lstTypeofFood, "TypeOfFoodID", "TypeOfFoodName");

            List<FoodGroup> lstFoodGroup = this.FoodGroupBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[FoodCatConstants.LIST_FOODGROUP] = new SelectList(lstFoodGroup, "FoodGroupID", "FoodGroupName");


            List<FoodPacking> lstFoodPacking = this.FoodPackingBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[FoodCatConstants.LIST_FOODPACKING] = new SelectList(lstFoodPacking, "FoodPackingID", "FoodPackingName");


            List<ComboObject> listGroupType = new List<ComboObject>();
            listGroupType.Add(new ComboObject("1", "ĐV"));
            listGroupType.Add(new ComboObject("2", "TV"));

            ViewData[FoodCatConstants.LIST_GROUPTYPE] = new SelectList(listGroupType, "key", "value");

            List<ComboObject> listCalculationUnit = new List<ComboObject>();
            listCalculationUnit.Add(new ComboObject("1", "Kg"));
            listCalculationUnit.Add(new ComboObject("2", "g"));

            ViewData[FoodCatConstants.LIST_CalculationUnit] = new SelectList(listCalculationUnit, "key", "value");

            List<MinenalCat> lstMinenalCat = this.MinenalCatBusiness.All.Where(o => o.IsActive == true).ToList();
            ViewData[FoodCatConstants.LIST_MINERALCAT] = lstMinenalCat.ToList();



            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["FoodID"] = FoodID;
            FoodCatViewModel foodcat = this._Search(SearchInfo).FirstOrDefault();
            List<FoodMineralCatBO> lstFoodMineral = this._SearchMineral(FoodID);
            ViewData["lstFoodMineral"] = lstFoodMineral;
            ViewData[FoodCatConstants.LIST_FOODCAT] = foodcat;
            return PartialView("_Edit",foodcat);
        }

    }
}





