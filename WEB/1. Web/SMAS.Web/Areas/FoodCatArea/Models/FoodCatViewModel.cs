/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.FoodCatArea.Models
{
    public class FoodCatViewModel 
    {

        public int FoodID { get; set; }

        [ResourceDisplayName("FoodCat_Lable_GroupType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int GroupType { get; set; }

        [ResourceDisplayName("FoodCat_Lable_GroupType")]
        public string StringGroupType { get; set; }

        [ResourceDisplayName("FoodCat_Lable_DiscardedRate")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public Nullable<int> DiscardedRate { get; set; }

        [ResourceDisplayName("FoodCat_Lable_Price")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> Price { get; set; }

        [ResourceDisplayName("FoodCat_Lable_Protein")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessage = "Price must can't have more than 2 decimal places")]
        public decimal? Protein { get; set; }

        [ResourceDisplayName("FoodCat_Lable_Fat")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessage = "Price must can't have more than 2 decimal places")]
        public decimal? Fat { get; set; }

        [ResourceDisplayName("FoodCat_Lable_Sugar")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessage = "Price must can't have more than 2 decimal places")]
        public decimal? Sugar { get; set; }

        [ResourceDisplayName("FoodCat_Lable_Calo")]
        public decimal? Calo { get; set; }

        [ResourceDisplayName("FoodCat_Label_FoodName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FoodName { get; set; }

        [ResourceDisplayName("FoodCat_Lable_ShortName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression("^([a-zA-Z0-9]+)$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Label_SpecialCharacter")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ShortName { get; set; }
        
        
        [ResourceDisplayName("FoodCat_Lable_TypeOfFood")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int TypeOfFoodID { get; set; }

        [ResourceDisplayName("FoodCat_Lable_TypeOfFood")]
        public string TypeOfFoodName { get; set; }

        [ResourceDisplayName("FoodCat_Lable_FoodPacking")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int FoodPackingID { get; set; }

        [ResourceDisplayName("FoodCat_Lable_FoodPacking")]
        public string FoodPackingName { get; set; }

       // [ResDisplayName("FoodCat_Lable_FoodGroup")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> FoodGroupID { get; set; }

         [ResourceDisplayName("FoodCat_Lable_CalculationUnit")]
        public int CalculationUnit { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}
