﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.FoodCatArea.Models
{
    public class SearchViewModel
    {

        [ResourceDisplayName("FoodCat_Label_FoodName")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FoodName { get; set; }

        [ResourceDisplayName("FoodCat_Lable_TypeOfFood")]
        public System.Nullable<int> TypeOfFoodID { get; set; }
    }
}