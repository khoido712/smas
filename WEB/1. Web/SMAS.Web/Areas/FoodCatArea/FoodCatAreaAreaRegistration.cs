﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FoodCatArea
{
    public class FoodCatAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FoodCatArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FoodCatArea_default",
                "FoodCatArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
