﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using System.Data;
using Aspose.Cells;
using SMAS.Tasks;
using SMAS.Business.Business;
using SMAS.Models.Models;

namespace HRM.Business.Library.Domain
{

    public class Task_AutoBackup : JobItem
    {
        protected override void OnExecute()
        {
            using (var context = new SMASEntities())
            {
                var logger = log4net.LogManager.GetLogger("Base");
                AlertMessageBusiness AlertMessageBusiness = new AlertMessageBusiness(logger, context);
                base.OnExecute();

                if (!string.IsNullOrWhiteSpace(JobArgs))
                {
                    try
                    {
                        //Push notification to app when is new alert message
                        AlertMessageBusiness.PushAlertMessageScheduler();
                    }
                    catch (Exception)
                    {
                        //Somethings wrong   
                    }
                }
            }
            
        }
        
    }
}
