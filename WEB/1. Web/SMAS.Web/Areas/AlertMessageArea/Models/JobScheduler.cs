﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SMAS.Tasks
{
    public class JobScheduler
    {
        #region Properties

        private List<JobItem> jobItems;
        private System.Timers.Timer jobTimer;
        private double timerInterval = 60000;
        private object thisLock = new object();
        private object logLock = new object();
        private int TotalTimeByInterval = 24;
        private int countInterval;

        /// <summary>
        /// Thời gian lặp lại việc kiểm tra job.
        /// Giá trị mặc định là 60000 milliseconds.
        /// </summary>
        public double TimerInterval
        {
            get { return timerInterval; }
            set { timerInterval = value; }
        }

        public List<JobItem> JobItems
        {
            get
            {
                if (jobItems == null)
                {
                    jobItems = new List<JobItem>();
                }

                return jobItems;
            }
        }

        private System.Timers.Timer JobTimer
        {
            get
            {
                if (jobTimer == null)
                {
                    jobTimer = new System.Timers.Timer();
                    jobTimer.Elapsed += OnElapsed;
                }

                return jobTimer;
            }
        }

        /// <summary>
        /// Trạng thái của JobScheduler.
        /// </summary>
        public bool IsRunning
        {
            get { return JobTimer.Enabled; }
        }

        #endregion

        #region Methods

        public void Start()
        {
            if (!JobTimer.Enabled)
            {
                JobTimer.Enabled = true;
                JobTimer.Start();
            }
        }

        public void Stop()
        {
            if (JobTimer.Enabled)
            {
                JobTimer.Enabled = false;
                JobTimer.Stop();
                JobTimer.Close();
            }
        }

        private void OnElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //Chạy qua 1 lần mới set lại Interval
                JobTimer.Interval = TimerInterval;

                lock (thisLock)
                {
                    if (JobItems != null && JobItems.Count() > 0)
                    {
                        foreach (var jobItem in JobItems)
                        {
                            try
                            {
                                using (var jobWorker = new BackgroundWorker())
                                {
                                    jobWorker.WorkerReportsProgress = true;
                                    jobWorker.WorkerSupportsCancellation = true;

                                    jobWorker.DoWork += OnExecuteJob;
                                    jobWorker.RunWorkerAsync(jobItem);
                                }
                            }
                            catch (Exception )
                            {
                                //Somethings wrong   
                            }
                        }
                    }
                }
            }
            catch (Exception )
            {
                //Somethings wrong   
            }
        }

        private void OnExecuteJob(object sender, DoWorkEventArgs e)
        {
            var jobItem = (JobItem)e.Argument;
            try
            {
                var startTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0);
                countInterval = (int)DateTime.Now.Subtract(startTime).TotalHours;
               
                if (jobItem != null)
                {
                    if (jobItem.IsFirstStart || countInterval > TotalTimeByInterval - 1 || countInterval == TotalTimeByInterval)
                    {
                        jobItem.Execute();
                    }
                }
                //Set lai tham so k con chay lan dau tien nua
                jobItem.IsFirstStart = false;
            }
            catch (Exception)
            {
                //Somethings wrong   
            }
        }

        private bool IsAvailable(JobItem jobItem, DateTime currentDate)
        {
            bool result = false;

            if (jobItem != null && jobItem.IsActivate && !jobItem.IsRunning)
            {
                //Tạo lại ngày giờ để đảm bảo luôn luôn không tồn tại Milliseconds
                currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day,
                    currentDate.Hour, currentDate.Minute, currentDate.Second);

                jobItem.DateStart = new DateTime(jobItem.DateStart.Year, jobItem.DateStart.Month,
                    jobItem.DateStart.Day, jobItem.DateStart.Hour, jobItem.DateStart.Minute,
                    jobItem.DateStart.Second);

                //Lưu vết ngày giờ đang kiểm tra
                jobItem.CurrentDate = currentDate;

                if (!jobItem.DateExpired.HasValue || jobItem.DateExpired > currentDate)
                {
                    if (!jobItem.NextStart.HasValue)
                    {
                        if (jobItem.LastStart.HasValue)
                        {
                            var lastStart = new DateTime(jobItem.LastStart.Value.Year, jobItem.LastStart.Value.Month, jobItem.LastStart.Value.Day,
                                jobItem.LastStart.Value.Hour, jobItem.LastStart.Value.Minute, jobItem.LastStart.Value.Second);

                            //Tạo lại NextStart từ lần chạy gần nhất (LastStart == CurrentDate của lần chạy trước đó được set trong JobItem Execute)
                            jobItem.NextStart = jobItem.GetNextStart(currentDate, lastStart);
                        }
                        else
                        {
                            jobItem.NextStart = jobItem.GetNextStart(currentDate);
                        }
                    }

                    if (jobItem.NextStart <= currentDate)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
