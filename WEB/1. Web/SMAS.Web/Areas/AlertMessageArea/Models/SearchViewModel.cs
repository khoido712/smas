﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.AlertMessageArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("AlertMessage_Label_Title")]
        public string Title { get; set; }
    }
}