﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SMAS.Tasks
{
    public class JobItem
    {
        public int ID { get; set; }

        /// <summary>
        /// Tên chức năng job.
        /// </summary>
        public string JobName { get; set; }

        public string JobType { get; set; }

        public string JobAction { get; set; }

        /// <summary>
        /// Tham số cho job.
        /// </summary>
        public string JobArgs { get; set; }

        /// <summary>
        /// Chỉ định cách chạy lặp lại cho job.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Chu kỳ ngày, tháng, năm tùy vào Type.
        /// </summary>
        public double? Interval { get; set; }

        /// <summary>
        /// Ngày bắt đầu chạy lần đầu tiên.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Ngày hết hạn thực hiện công việc này.
        /// </summary>
        public DateTime? DateExpired { get; set; }

        /// <summary>
        /// Ngày chạy cuối cùng gần đây nhất.
        /// </summary>
        public DateTime? LastStart { get; set; }

        /// <summary>
        /// Lần chạy tiếp theo là ngày nào
        /// </summary>
        public DateTime? NextStart { get; set; }

        /// <summary>
        /// Lưu thời gian hiện tại lúc gọi hàm IsAvailable
        /// </summary>
        internal DateTime CurrentDate { get; set; }

        /// <summary>
        /// Đang hoạt động.
        /// </summary>
        public bool IsActivate { get; set; }
        public bool IsFirstStart { get; set; }
        public bool IsExcutedByInterval { get; set; }
        public int IntervalCount { get; set; }

        /// <summary>
        /// Job đang chạy.
        /// </summary>
        public bool IsRunning { get; private set; }

        public Action<JobItem> OnExecuteSuccess { set; get; }
        public Action<JobItem> OnExecuteError { set; get; }
        public Func<JobItem, DateTime?> GetLastDateStart { set; get; }

        private object thisLock = new object();

        internal DateTime GetNextStart(DateTime currentdate, DateTime? lastStart = null)
        {
            DateTime result = DateStart;
            CurrentDate = currentdate;

            if (NextStart.HasValue)
            {
                //Tránh trường hợp nextStart = lastStart
                currentdate = currentdate.AddSeconds(1);
            }
            else
            {
                //Gắn currentdate = lastStart để kiểm tra lại lần trước đó có chạy hay chưa
                currentdate = lastStart.HasValue ? lastStart.Value : currentdate;
            }

            if (result < currentdate)
            {
                result = GetDateByMinutes(currentdate, 1 * 24 * 60);
            }
            return result;
        }

        /// <summary>
        /// Tính thời gian chạy kế tiếp dựa vào số phút chu kỳ lặp lại.
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="minutes"></param>
        /// <returns></returns>
        private DateTime GetDateByMinutes(DateTime currentDate, double minutes)
        {
            //Tạo lại ngày giờ để đảm bảo luôn luôn không tồn tại Milliseconds
            DateStart = new DateTime(DateStart.Year, DateStart.Month, DateStart.Day,
                DateStart.Hour, DateStart.Minute, DateStart.Second);

            currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day,
                currentDate.Hour, currentDate.Minute, currentDate.Second);

            //Chu kỳ tối thiểu phải là số dương
            minutes = minutes > 0 ? minutes : 1;

            //Phải lấy số nguyên của TotalMinutes trong hàm Subtract mới đúng
            double totalMinutes = (int)currentDate.Subtract(DateStart).TotalMinutes;
            DateTime result = DateStart.AddMinutes(totalMinutes - totalMinutes % minutes);
            result = result > currentDate ? result : result.AddMinutes(minutes);
            return result;
        }

        /// <summary>
        /// Tính thời gian chạy kế tiếp dựa vào số tháng chu kỳ lặp lại.
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="months"></param>
        /// <returns></returns>
        private DateTime GetDateByMonths(DateTime currentDate, int months)
        {
            //Tạo lại ngày giờ để đảm bảo luôn luôn không tồn tại Milliseconds
            DateStart = new DateTime(DateStart.Year, DateStart.Month, DateStart.Day,
                DateStart.Hour, DateStart.Minute, DateStart.Second);

            currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day,
                currentDate.Hour, currentDate.Minute, currentDate.Second);

            //Chu kỳ tối thiểu phải là số dương
            months = months > 0 ? months : 1;

            int totalMonths = (currentDate.Year - DateStart.Year) * 12;
            totalMonths = totalMonths + currentDate.Month - DateStart.Month;
            DateTime result = DateStart.AddMonths(totalMonths - totalMonths % months);
            result = result > currentDate ? result : result.AddMonths(months);
            return result;
        }
        /// <summary>
        /// Lớp con viết lại hàm này để thực thi job.
        /// </summary>
        protected virtual void OnExecute()
        {

        }

        /// <summary>
        /// Gọi hàm này khi muốn thực thi để tranh chạy nhiều lần.
        /// </summary>
        public void Execute()
        {
            lock (thisLock)
            {
                if (!IsRunning)
                {
                    IsRunning = true;

                    if (GetLastDateStart != null)
                    {
                        var lastDateStart = GetLastDateStart(this);
                        if (lastDateStart.HasValue && DateTime.Compare(lastDateStart.Value, LastStart.Value) > 0)
                        {
                            IsRunning = false;
                            return;
                        }
                    }

                    var lastTime = LastStart;
                    var nextTime = NextStart;
                    LastStart = CurrentDate;

                    //Tính lại thời gian hợp lệ cho lần tới
                    NextStart = GetNextStart(CurrentDate);

                    try
                    {
                        OnExecute();

                        if (OnExecuteSuccess != null)
                            OnExecuteSuccess(this);
                    }
                    catch (Exception)
                    {
                        LastStart = lastTime;
                        NextStart = nextTime;

                        if(OnExecuteError != null)
                            OnExecuteError(this);

                        IsRunning = false;
                        throw;
                    }
                    finally
                    {
                        IsRunning = false;
                    }
                }
            }
        }
    }
}
