﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.AlertMessageArea.Models
{
    public class Schedule_AutoBackupModel
    {
        public int ID { get; set; }
        public double? Interval { get; set; }
        public double? TimeWaiting { get; set; }
        public string JobName { get; set; }
        public string JobArgs { get; set; }
        public string Type { get; set; }
    }
}