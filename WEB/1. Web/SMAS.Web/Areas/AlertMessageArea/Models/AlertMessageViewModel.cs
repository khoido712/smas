﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.AlertMessageArea.Models
{
    public class AlertMessageViewModel
    {
        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_AlertMessageID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int AlertMessageID { get; set; }

        [ResourceDisplayName("AlertMessage_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Title { get; set; }

        [ResourceDisplayName("AlertMessage_Label_FileUrl")]
        public string FileUrl { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_FileUrl")]
        public string FileUrlFull { get; set; }

        [ResourceDisplayName("AlertMessage_Label_PublishedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.DateTime PublishedDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_ContentMessage")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string ContentMessage { get; set; }
        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_IsPublish")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsPublish { get; set; }

        [ResourceDisplayName("AlertMessage_Label_IsPublish")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string IsPublishName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_CreatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.DateTime CreatedDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_ModifiedDate")]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("AlertMessage_Label_IsActive")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public bool IsActive { get; set; }	


        //dungnt77 them vao phuc vu validate tren client
        [ResourceDisplayName("AlertMessage_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string TitleCreate { get; set; } 

          [ResourceDisplayName("AlertMessage_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string TitleEdit { get; set; } 


        [ResourceDisplayName("AlertMessage_Label_PublishedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.DateTime dtpPublishDate { get; set; }

        [ResourceDisplayName("AlertMessage_Label_PublishedDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.DateTime dtpPublishDateEdit { get; set; }


        [ResourceDisplayName("AlertMessage_Label_ContentMessage1")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(4000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string editorEdit { get; set; }


        [ResourceDisplayName("AlertMessage_Label_ContentMessage1")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(4000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string editor { get; set; }

        public short TypeID { get; set; }

        public bool IsGlobal { get; set; }
    }
}