﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections;
using System.Net;
using System.Web;
using SMAS.Tasks;
using SMAS.Web.Areas.AlertMessageArea.Models;
using HRM.Business.Library.Domain;
namespace SMAS.Library.ScheduleTaskAuto
{
    public static class ScheduleTaskExcute
    {
        #region Schedule Task

        static JobScheduler scheduler = null;

        public static void RunTaskScheduler(Guid id)
        {
            if (scheduler != null)
            {
                scheduler.Stop();
                scheduler = null;
            }

            scheduler = new JobScheduler();
        }

        public static void StartTaskSchedulerGlobal()
       {
            try
            {
                scheduler = new JobScheduler();
                scheduler.TimerInterval = 60000 * 60;//60000 = 1 phút
                string status = string.Empty;

                var lstObj = new List<object>();
                lstObj.Add(null);
                lstObj.Add(1);
                lstObj.Add(int.MaxValue - 1);
                List<Schedule_AutoBackupModel> listAutoBackup = new List<Schedule_AutoBackupModel>();
                listAutoBackup.Add(new Schedule_AutoBackupModel()
                {
                    ID = 1,
                    Interval = scheduler.TimerInterval,
                    JobName = "Schedule auto push notification",
                    JobArgs = "Excute"                    
                });

                if (listAutoBackup != null)
                {
                    foreach (var autoBackup in listAutoBackup)
                    {
                        var jobItem = new Task_AutoBackup
                        {
                            ID = autoBackup.ID,
                            Interval = double.Parse(autoBackup.Interval.ToString()),
                            JobName = autoBackup.JobName,
                            JobArgs = autoBackup.JobArgs,
                            IsActivate = true,
                            IsFirstStart = true,
                            IsExcutedByInterval= false
                        };

                        scheduler.JobItems.Add(jobItem);
                    }
                }

                if (scheduler.JobItems.Count > 0)
                {
                    scheduler.Start();
                }
            }
            catch (Exception)
            {
                //Somethings wrong   
            }
            
        }
         
        public static List<JobItem> GetJobItems()
        {
            List<JobItem> result = null;

            if (scheduler != null)
            {
                result = scheduler.JobItems;
            }

            return result;
        }
        #endregion
       
    }
}
