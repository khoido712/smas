﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.AlertMessageArea
{
    public class AlertMessageAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AlertMessageArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AlertMessageArea_default",
                "AlertMessageArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
