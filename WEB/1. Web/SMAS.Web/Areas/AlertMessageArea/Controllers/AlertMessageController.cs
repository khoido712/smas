﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.AlertMessageArea.Models;
using SMAS.Models.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using System.IO;
using System.Globalization;
using SMAS.Business.Common;
using SMAS.Web.Controllers;
using System.Net;
using System.Text;
using Microsoft.Security.Application;

namespace SMAS.Web.Areas.AlertMessageArea.Controllers
{
    public class AlertMessageController : BaseController
    {
        private readonly IAlertMessageBusiness AlertMessageBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public AlertMessageController(IAlertMessageBusiness AlertMessageBusiness, ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.AlertMessageBusiness = AlertMessageBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {
          
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            SearchInfo["IsActive"] = true;

            //tanla4
            //12/11/2014
            //them them truong CreatedUserID khi thuc hien search
            //SearchInfo["CreatedUserID"] = _globalInfo.UserAccountID;

            IEnumerable<AlertMessageViewModel> lst = this._Search(SearchInfo);
            ViewData[AlertMessageConstants.LIST_ALERTMESSAGE] = lst.OrderByDescending(o=>o.PublishedDate);
        }

        //tanla4
        //25/11/2014
        //upload file to ftp server
        //String sourcefilepath = “@absolutepath”; // e.g. “d:/test.docx”
        //String ftpurl = “@ftpurl”; // e.g. ftp://serverip/foldername/foldername
        //String ftpusername = “@ftpusername”; // e.g. username
        //String ftppassword = “@ftppassword”; // e.g. password

        private static void UploadFileToFTP(HttpPostedFileBase file, string source, string ftpurl, string ftpusername, string ftppassword)
        {
            try
            {
                string filename = Path.GetFileName(source);
                string ftpfullpath = ftpurl + filename;
                FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath);
                ftp.Credentials = new NetworkCredential(ftpusername, ftppassword);

                ftp.KeepAlive = true;
                ftp.UseBinary = true;
                ftp.Method = WebRequestMethods.Ftp.UploadFile;
                //FileStream fs = System.IO.File.OpenRead(source);                
                //byte[] buffer = new byte[fs.Length];
                //fs.Read(buffer, 0, buffer.Length);
                //fs.Close();

                //create new Bite Array
                byte[] buffer = new byte[file.ContentLength];
                //Set pointer to the beginning of the stream
                file.InputStream.Position = 0;
                //Read the entire stream
                file.InputStream.Read(buffer, 0, (int)file.ContentLength);

                Stream ftpstream = ftp.GetRequestStream();
                ftpstream.Write(buffer, 0, buffer.Length);
                ftpstream.Close();

                ftp = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //tanla4
        //26/11/2014
        //upload file to http server
        public void UploadFileToHTTP(HttpPostedFileBase file, string source, string httpurl)
        {
            try
            {
               
                //upload with httwebrequest success
                string filename = Path.GetFileName(source);
                string httpfullpath = httpurl + filename;
                HttpWebRequest http = (HttpWebRequest)HttpWebRequest.Create(httpurl);
                http.ContentLength = file.ContentLength;
                http.Method = WebRequestMethods.Http.Put;
                http.KeepAlive = true;
                http.Credentials = new NetworkCredential(Config.UsernameUpload, Config.PasswordUpload);
                //create new Bite Array
                byte[] buffer = new byte[file.ContentLength];
                //Set pointer to the beginning of the stream
                file.InputStream.Position = 0;
                //Read the entire stream
                file.InputStream.Read(buffer, 0, file.ContentLength);


                Stream httpstream = http.GetRequestStream();
                httpstream.Write(buffer, 0, buffer.Length);
                httpstream.Close();

                WebResponse webResponse2 = http.GetResponse();

                webResponse2.Close();
                http = null;
                webResponse2 = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UploadFilesToRemoteUrl(HttpPostedFileBase file, string source, string httpurl)
        {
            try
            {

                // Create a boundry
                string boundary = Guid.NewGuid().ToString();
                string filename = Path.GetFileName(source);
                string httpfullpath = httpurl + filename;
                HttpWebRequest request = HttpWebRequest.Create(httpurl) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
                request.PreAuthenticate = true;

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("--{0}", boundary);
                sb.AppendFormat("\r\n");
                sb.AppendFormat("Content-Disposition: form-data; name=\"form1\"; filename=\"" + filename + "\"");
                sb.AppendFormat("\r\n");
                sb.AppendFormat("Content-Type:  application / octet-stream");
                sb.AppendFormat("\r\n");
                sb.AppendFormat("\r\n");

                /*using (FileStream fs = new FileStream(Config.UsernameUpload, FileMode.Open, FileAccess.Read))
                {
                    byte[] contents = new byte[fs.Length];
                    fs.Read(contents, 0, contents.Length);
                    sb.Append(Encoding.Default.GetString(contents));
                }*/
                byte[] buffer = new byte[file.ContentLength];
                //Read the entire stream
                file.InputStream.Read(buffer, 0, file.ContentLength);
                sb.Append(Encoding.Default.GetString(buffer));

                sb.AppendFormat("\r\n");
                sb.AppendFormat("--{0}", boundary);
                sb.AppendFormat("\r\n");
                sb.AppendFormat("Content-Disposition: form-data; name=\"username\"");
                sb.AppendFormat("\r\n");
                sb.AppendFormat("\r\n");
                sb.AppendFormat(Config.UsernameUpload);
                sb.AppendFormat("\r\n");

                sb.AppendFormat("--{0}", boundary);
                sb.AppendFormat("\r\n");
                sb.AppendFormat("Content-Disposition: form-data; name=\"password\"");
                sb.AppendFormat("\r\n");
                sb.AppendFormat("\r\n");
                sb.AppendFormat(Config.PasswordUpload);
                sb.AppendFormat("\r\n");

                sb.AppendFormat("--{0}", boundary);
                sb.AppendFormat("\r\n");
                sb.AppendFormat("Content-Disposition: form-data; name=\"location\"");
                sb.AppendFormat("\r\n");
                sb.AppendFormat("\r\n");
                sb.AppendFormat(Config.UploadPath);
                sb.AppendFormat("\r\n");

                sb.AppendFormat("--{0}--", boundary);
                byte[] fulldata = Encoding.Default.GetBytes(sb.ToString());

                request.ContentLength = fulldata.Length;
                // Set the response timeout time (300 seconds)
                request.Timeout = 300000;

                //xu ly
                using (Stream sw = request.GetRequestStream())
                {
                    sw.Write(fulldata, 0, fulldata.Length);
                }


                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                string result = string.Empty;

                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    //MessageBox.Show(sr.ReadToEnd());
                    result = sr.ReadLine();
                }

            }
            catch (Exception ex)
            {
                throw ex;                
            }
            
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachmentsCreate)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachmentsCreate == null || attachmentsCreate.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error")); 
                res.ContentType = "text/plain";
                return res;
            }

            // The Name of the Upload component is "attachments"
            var file = attachmentsCreate.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = Config.GetExtentions.ToList();
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                //xu li doi ten file de tranh trung lap
                fileName = fileName + "_" + DateTime.Now.Ticks + extension;

                //UploadFileToHTTP(file, fileName, Config.UrlUpload);
                UploadFilesToRemoteUrl(file, fileName, Config.GetUrl);
                string physicalPath = Config.UrlUpload + fileName;

                Session["FilePath"] = physicalPath;

                JsonResult res1 = Json(new JsonMessage(""));
                res1.ContentType = "text/plain";
                return res1;
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFileEdit(IEnumerable<HttpPostedFileBase> attachmentsEdit)
        {
            GlobalInfo global = new GlobalInfo();

            //if (attachmentsEdit == null || attachmentsEdit.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            HttpPostedFileBase file = null;
            if (attachmentsEdit != null)
            {
                file = attachmentsEdit.FirstOrDefault();
            }

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = Config.GetExtentions.ToList();
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                //xu li doi ten file de tranh trung lap
                fileName = fileName + "_" + DateTime.Now.Ticks + extension;
                //UploadFileToHTTP(file, fileName, Config.UrlUpload);
                UploadFilesToRemoteUrl(file, fileName, Config.GetUrl);
                string physicalPath = Config.UrlUpload + fileName;
                Session["FilePathEdit"] = physicalPath;
                JsonResult res1 = Json(new JsonMessage(""));
                res1.ContentType = "text/plain";
                return res1;
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(FormCollection frm)
        {
            string editor = frm["editor"] != null ? frm["editor"].ToString() : string.Empty;
            bool isPrivate = frm["IsPrivate"] != null ? Convert.ToBoolean(frm["IsPrivate"]) : false;
            bool isSchool = frm["IsSchool"] != null ? Convert.ToBoolean(frm["IsSchool"]) : false;
            bool isSuperVisingDeptSub = frm["IsSuperVisingDeptSub"] != null ? Convert.ToBoolean(frm["IsSuperVisingDeptSub"]) : false;
            string datePublic = frm["dtpPublishDate"] != null ? frm["dtpPublishDate"].ToString() : string.Empty;
           
            //viethd4: fix loi khong xoa session sau khi them
            object filePath = Session["FilePath"];
            Session.Remove("FilePath");

            #region validate

            DateTime num1;
            if (string.IsNullOrEmpty(datePublic))
            {
                throw new BusinessException("AlertMessage_Date_Promulgate_Empty");
            }
            bool res = DateTime.TryParse(datePublic, out num1);
            if (res == false)
            {
                throw new BusinessException("AlertMessage_Date_Promulgate_Not_Format");
            }
            if (_globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
            {
                if (!isPrivate && !isSchool && !isSuperVisingDeptSub)
                {
                    throw new BusinessException("AlertMessage_Choice");
                }
            }

            if (editor.Trim().Length == 0)
            {
                throw new BusinessException("Editor_AlertMessage_Not_Empty");
            }
                       
            #endregion

            AlertMessage alertMessage = new AlertMessage();
            alertMessage.Title = Sanitizer.GetSafeHtmlFragment((String)frm["TitleCreate"]);
            alertMessage.PublishedDate = DateTime.ParseExact(frm["dtpPublishDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            alertMessage.ContentMessage = Sanitizer.GetSafeHtmlFragment(editor);
            if (alertMessage.ContentMessage.Length > 4000)
            {
                throw new BusinessException("Validate_MaxLength_Content");
            }
            alertMessage.IsPublish = frm["chkpublish"]!=null;

            if (_globalInfo.IsSystemAdmin)
            {
                alertMessage.IsGlobal = frm["chkglobal"] != null;
            }
            alertMessage.CreatedDate = DateTime.Now;
            alertMessage.FileUrl = (string)filePath;
            //D:\\SMAS3.0\\1. Web\\SMAS.Web\\Uploads\\AlertMessages\\HDSD_IBM_Rational QT_v1 0.doc
            //alertMessage.ContentMessage = frm["editor"];
            TryUpdateModel(alertMessage);
            Utils.Utils.TrimObject(alertMessage);
           
            if (_globalInfo.SchoolID == null)
            {
                alertMessage.TypeID = this.GetTypeID(isPrivate, isSchool, isSuperVisingDeptSub);
            }
            
            // alertMessage.IsLocal = frm["chklocal"] != null;
            //them truong TypeID khi Insert
            if (_globalInfo.IsSchoolRole)
            {
                alertMessage.UnitID = _globalInfo.SchoolID;
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                alertMessage.UnitID = _globalInfo.SupervisingDeptID;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                alertMessage.UnitID = _globalInfo.SupervisingDeptID;
            }
            else if (_globalInfo.IsAdmin && _globalInfo.IsSuperRole)
            {
                alertMessage.UnitID = null;
            }

            //tanla4
            //12/11/2014
            //them truong CreatedUserID khi Insert
            alertMessage.CreatedUserID = _globalInfo.UserAccountID;
            alertMessage.IsActive = true;
            
            if (alertMessage.IsGlobal.HasValue && alertMessage.IsGlobal.Value == true && alertMessage.IsPublish)
            {
                List<AlertMessage> lstPrePublish = AlertMessageBusiness.All.Where(o => o.IsPublish && o.IsGlobal.HasValue && o.IsGlobal.Value == true).ToList();
                for (int i = 0; i < lstPrePublish.Count; i++)
                {
                    AlertMessage am = lstPrePublish[i];
                    am.IsPublish = false;
                    this.AlertMessageBusiness.BaseUpdate(am);
                }
            }

            this.AlertMessageBusiness.Insert(alertMessage);
            this.AlertMessageBusiness.Save();
            var isPush = false;
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(alertMessage.UnitID != null ? alertMessage.UnitID : 0);
            if (schoolProfile != null && alertMessage.IsPublish && alertMessage.PublishedDate.Day == DateTime.Now.Day && alertMessage.PublishedDate.Month == DateTime.Now.Month && alertMessage.PublishedDate.Year == DateTime.Now.Year)
            {
                isPush = true;
            }
            var objReturn = new {
                Result = new JsonMessage(Res.Get("Common_Label_AddNewMessage")),
                SchoolID = _globalInfo.IsSystemAdmin ? 0 : alertMessage.UnitID,
                AlertMessageID = alertMessage.AlertMessageID,
                IsEditData = false,
                IsPushNotification = isPush
            };
            return Json(objReturn);

            //return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        } 
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Edit(int AlertMessageID, FormCollection frm)
        {
            string editor =  frm["editorEdit"] != null ?  frm["editorEdit"].ToString() : string.Empty;
            bool isPrivate = frm["IsPrivate"] != null ? Convert.ToBoolean(frm["IsPrivate"]) : false;
            bool isSchool = frm["IsSchool"] != null ? Convert.ToBoolean(frm["IsSchool"]) : false;
            bool isSuperVisingDeptSub = frm["IsSuperVisingDeptSub"] != null ? Convert.ToBoolean(frm["IsSuperVisingDeptSub"]) : false;
            string datePublic = frm["dtpPublishDateEdit"] != null ? frm["dtpPublishDateEdit"].ToString() : string.Empty;

            //viethd4: fix loi khong xoa session sau khi luu
            object filePath = Session["FilePathEdit"];
            Session.Remove("FilePathEdit");
            #region validate
            DateTime num1;
            if (string.IsNullOrEmpty(datePublic))
            {
                throw new BusinessException("AlertMessage_Date_Promulgate_Empty");
            }
            bool res = DateTime.TryParse(datePublic, out num1);
            if (res == false)
            {
                throw new BusinessException("AlertMessage_Date_Promulgate_Not_Format");
            }

            if (_globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
            {
                if (!isPrivate && !isSchool && !isSuperVisingDeptSub)
                {
                    throw new BusinessException("AlertMessage_Choice");
                }
            }

            if (editor.Trim().Length == 0)
            {
                throw new BusinessException("Editor_AlertMessage_Not_Empty");
            }
         
            #endregion


            AlertMessage alertMessage = this.AlertMessageBusiness.Find(AlertMessageID);
            alertMessage.Title = Sanitizer.GetSafeHtmlFragment((String)frm["TitleEdit"]);
            alertMessage.PublishedDate = DateTime.ParseExact(frm["dtpPublishDateEdit"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            alertMessage.ContentMessage = Sanitizer.GetSafeHtmlFragment(editor);
            alertMessage.IsPublish = frm["chkpublish1"] != null;
            alertMessage.IsLocal = frm["chklocal1"] != null;

            if (_globalInfo.IsSystemAdmin)
            {
                alertMessage.IsGlobal = frm["chkglobal1"] != null;
            }
            
            if (frm["chkDelFile"] != null)
            {
               // deleteFile(Path.GetFileName(alertMessage.FileUrl));
                alertMessage.FileUrl = null;
            }
            string FilePathEdit = filePath == null ? "" : (string)filePath;
            if (filePath != null)
            {
                alertMessage.FileUrl = FilePathEdit;
            }
            alertMessage.ModifiedDate = DateTime.Now;
            TryUpdateModel(alertMessage);
            Utils.Utils.TrimObject(alertMessage);
           
            ///Get typeID
            if (_globalInfo.SchoolID == null)
            {
                alertMessage.TypeID = this.GetTypeID(isPrivate, isSchool, isSuperVisingDeptSub);
            }

            if (_globalInfo.IsSchoolRole)
            {
                alertMessage.UnitID = _globalInfo.SchoolID;
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                alertMessage.UnitID = _globalInfo.SupervisingDeptID;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                alertMessage.UnitID = _globalInfo.SupervisingDeptID;
            }
            else if (_globalInfo.IsAdmin && _globalInfo.IsSuperRole)
            {
                alertMessage.UnitID = null;
            }

            //tanla4
            //12/11/2014
            //them truong CreatedUserID khi Update
            alertMessage.CreatedUserID = _globalInfo.UserAccountID;

            if (alertMessage.IsGlobal.HasValue && alertMessage.IsGlobal.Value == true && alertMessage.IsPublish)
            {
                List<AlertMessage> lstPrePublish = AlertMessageBusiness.All.Where(o => o.IsPublish && o.IsGlobal.HasValue && o.IsGlobal.Value == true && o.AlertMessageID != alertMessage.AlertMessageID).ToList();
                for (int i = 0; i < lstPrePublish.Count; i++)
                {
                    AlertMessage am = lstPrePublish[i];
                    am.IsPublish = false;
                    this.AlertMessageBusiness.BaseUpdate(am);
                }
            }

            this.AlertMessageBusiness.Update(alertMessage);
            this.AlertMessageBusiness.Save();
            var isPush = false;
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(alertMessage.UnitID != null ? alertMessage.UnitID : 0);
            if (schoolProfile != null && alertMessage.IsPublish && alertMessage.PublishedDate.Day <= DateTime.Now.Day && alertMessage.PublishedDate.Month == DateTime.Now.Month && alertMessage.PublishedDate.Year == DateTime.Now.Year)
            {
                isPush = true;
            }
            var objReturn = new
            {
                Result = new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")),
                SchoolID = _globalInfo.IsSystemAdmin ? 0 : alertMessage.UnitID,
                AlertMessageID = alertMessage.AlertMessageID,
                IsEditData = true,
                IsPushNotification = isPush
            };
            return Json(objReturn);
            //return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
 
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            
            this.AlertMessageBusiness.Delete(id);
            this.AlertMessageBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            //return View();
        }

        public PartialViewResult Search(string txtTitle)
        {
            //Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["IsActive"] = true;

            if (txtTitle != "")
            {
            SearchInfo["Title"] = txtTitle;
            }

            //tanla4
            //12/11/2014
            //them them truong CreatedUserID khi thuc hien search
            //SearchInfo["CreatedUserID"] = _globalInfo.UserAccountID;

            IEnumerable<AlertMessageViewModel> lst = this._Search(SearchInfo);
            ViewData[AlertMessageConstants.LIST_ALERTMESSAGE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        [HttpPost]
        public JsonResult PushNotification(int schoolid, string alertmessageids, bool iseditdata)
        {
            var ListDeviceToken = new List<string>();
            var ListAlertMessageID = new List<int>();
            string res = string.Empty;
            if (!string.IsNullOrEmpty(alertmessageids))
            {
                ListAlertMessageID.AddRange(alertmessageids.Split(',').Select(x => int.Parse(x)).ToList());
            }
            ListDeviceToken = AlertMessageBusiness.GetListDeviceToken(schoolid);
            foreach (var AlertMessageID in ListAlertMessageID)
            {
                var objAlertMessage = AlertMessageBusiness.Find(AlertMessageID);
                ////Call fuction
                if (objAlertMessage != null)
                {
                    if (iseditdata)// Edit notification
                    {
                        res = AlertMessageBusiness.SendNotificationToAPI(ListDeviceToken, objAlertMessage.ContentMessage, objAlertMessage.Title);
                    }
                    else // Create notification
                    {
                        res = AlertMessageBusiness.SendNotificationToAPI(ListDeviceToken, objAlertMessage.ContentMessage, objAlertMessage.Title);
                    }
                }
            }

            return Json(new { result = res });
        }

        private IEnumerable<AlertMessageViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("AlertMessage", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            string isPublishGrid = Res.Get("AlertMessage_Label_IsPublishGrid");
            string isNotPublishGrid = Res.Get("AlertMessage_Label_IsNotPublishGrid");
           
            //tanla4
            //14/11/2014
            //them truong TypeID khi Search
            if (_globalInfo.IsSchoolRole)//truong
            {
               // SearchInfo["TypeID"] = 4;
                SearchInfo["UnitID"] = _globalInfo.SchoolID;
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)//phong
            {
                SearchInfo["UnitID"] = _globalInfo.SupervisingDeptID;
                //SearchInfo["TypeID"] = 3;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)//so
            {
                SearchInfo["UnitID"] = _globalInfo.SupervisingDeptID;
                //SearchInfo["TypeID"] = 2;
            }
            else if (_globalInfo.IsAdmin && _globalInfo.IsSuperRole)//admin
            {
                //SearchInfo["TypeID"] = 1;
                SearchInfo["isAdmin"] = true;
                
            }

            if (_globalInfo.IsSystemAdmin)
            {
                SearchInfo["ExcludeGlobal"] = false;
            }

            IQueryable<AlertMessage> query = this.AlertMessageBusiness.Search(SearchInfo);
            List<AlertMessageViewModel> lst = query.Select(o => new AlertMessageViewModel
            {
                AlertMessageID = o.AlertMessageID,
                Title = o.Title,
                FileUrl = o.FileUrl,
                FileUrlFull = o.FileUrl,
                PublishedDate = o.PublishedDate,
                ContentMessage = o.ContentMessage,
                IsPublish = o.IsPublish,
                CreatedDate = o.CreatedDate,
                ModifiedDate = o.ModifiedDate,
                IsActive = o.IsActive,
                IsPublishName = o.IsPublish ? isPublishGrid : isNotPublishGrid,
                TypeID = o.TypeID,
                IsGlobal = o.IsGlobal.HasValue ? o.IsGlobal.Value : false
            }).ToList();
            foreach (AlertMessageViewModel item in lst)
            {
                item.FileUrl = Path.GetFileName(item.FileUrl);
                item.Title = Sanitizer.GetSafeHtmlFragment(item.Title);
                item.ContentMessage = Sanitizer.GetSafeHtmlFragment(item.ContentMessage);
               // item.ContentMessage = System.Uri.UnescapeDataString(item.ContentMessage);
                //item.ContentMessage = HttpUtility.HtmlDecode(item.ContentMessage);
            }
            ////tanla4
            ////12/11/2014
            ////sua chua - xet quyen cho hanh dong them, sua, xoa
                
            ////switch (GetMenupermission("AlertMessage", _globalInfo.UserAccountID, _globalInfo.IsAdmin))
            ////{
            ////    case 2:
            ////        ViewData[AlertMessageConstants.IS_CREATED] = true;
            ////        break;
            ////    case 3:
            ////        ViewData[AlertMessageConstants.IS_CREATED] = true;
            ////        ViewData[AlertMessageConstants.IS_EDITED] = true;
            ////        break;
            ////    case 4:
            ////        ViewData[AlertMessageConstants.IS_CREATED] = true;
            ////        ViewData[AlertMessageConstants.IS_EDITED] = true;
            ////        ViewData[AlertMessageConstants.IS_DELETED] = true;
            ////        break;
            ////}    
            ////do chi hien thi thong bao cua chinh nguoi tao ra thang bao, nen khong can xet hanh dong them - sua - xoa
            //ViewData[AlertMessageConstants.IS_CREATED] = true;
            //ViewData[AlertMessageConstants.IS_EDITED] = true;
            //ViewData[AlertMessageConstants.IS_DELETED] = true;
            return lst.OrderByDescending(o=>o.PublishedDate).ToList();
        }

        /// <summary>
        ///  Xóa file
        /// <author>DungVA</author>
        /// <date>20/05/2013</date>
        /// </summary>
        /// <returns></returns>
        //[HttpPost]
        public void deleteFile(string fileName)
        {
            try
            {
                /*Xóa toàn bộ file trong folder*/
                string folderPath = Server.MapPath(SMAS.Web.Constants.GlobalConstants.ALERTMESSAGE_FOLDERPATH);
                DirectoryInfo dir = new DirectoryInfo(folderPath);

                //dir.GetFiles().ToList().ForEach(f => f.Delete());
                List<FileInfo> lst = dir.GetFiles().ToList();
                
                for (int i = 0; i < lst.Count; i++)
                {
                    if (lst[i].Name == fileName)
                    {
                        lst[i].Delete();
                    }
                    
                }
                ///*********************************/
                ///*Xóa file zip đã tạo ra sau khi download*/
                //FileInfo fi = new FileInfo(Server.MapPath(SMAS.Web.Constants.GlobalConstants.HEALTHTEST_FILEPATH) + Session["fileNameZip"]);
                //fi.Delete();
                //Session["fileNameZip"] = "";
                ///*********************************/
            }
            catch (Exception)
            {
                throw new BusinessException("HealthTest_Label_ErrorEmptyFolder");
            }

            //dir.GetFiles().ToList().ForEach(f => f.Delete());
            //dir.Delete(true);

            //return Json(new JsonMessage(""));
        }

        private short GetTypeID(bool isPrivate, bool isSchool, bool isSuperVisingDeptSub)
        {
            short typeID = 0;
           // TypeID: 1: Nội bộ ; 
            //2: các trường; 
            //3: các phòng GD; 
            //4: nội bộ & các trường; 
            //5: nội bộ & các phòng GD; 
            //6: các trường & các phòng GD; 
            //7: nội bộ & các trường & các phòng GD
            if (isPrivate && isSchool && isSuperVisingDeptSub) typeID = AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL_SUPERVISINGDEPTSUB;
            else if (isSchool && isSuperVisingDeptSub) typeID = AlertMessageConstants.TYPE_ID_IS_SCHOOL_SUPERVISINGDEPTSUB;
            else if (isPrivate && isSuperVisingDeptSub) typeID = AlertMessageConstants.TYPE_ID_IS_PRIVATE_SUPERVISINGDEPTSUB;
            else if (isPrivate && isSchool) typeID = AlertMessageConstants.TYPE_ID_IS_PRIVATE_SCHOOL;
            else if (isSuperVisingDeptSub) typeID = AlertMessageConstants.TYPE_ID_IS_SUPERVISINGDEPT;
            else if (isSchool) typeID = AlertMessageConstants.TYPE_ID_IS_SCHOOL;
            else if (isPrivate) typeID = AlertMessageConstants.TYPE_ID_IS_PRIVATE;

            return typeID;
        }
    }
}
