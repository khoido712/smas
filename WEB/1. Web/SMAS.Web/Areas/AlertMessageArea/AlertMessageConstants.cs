﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.AlertMessageArea
{
    public class AlertMessageConstants
    {
        public const string LIST_ALERTMESSAGE = "LIST_ALERTMESSAGE";
        public const string IS_CREATED = "IS_CREATED";
        public const string IS_EDITED = "IS_EDITED";
        public const string IS_DELETED = "IS_DELETED";
        public const int TYPE_ADMIN = 1;//admin
        public const int TYPE_SUPERVISINGDEPT = 2;//so
        public const int TYPE_SUBSUPERVISINGDEPT = 3;//phong
        public const int TYPE_SCHOOL = 4;//truong


        public const short TYPE_ID_IS_PRIVATE = 1;
        public const short TYPE_ID_IS_SCHOOL = 2;
        public const short TYPE_ID_IS_SUPERVISINGDEPT = 3;
        public const short TYPE_ID_IS_PRIVATE_SCHOOL = 4;
        public const short TYPE_ID_IS_PRIVATE_SUPERVISINGDEPTSUB = 5;
        public const short TYPE_ID_IS_SCHOOL_SUPERVISINGDEPTSUB = 6;
        public const short TYPE_ID_IS_PRIVATE_SCHOOL_SUPERVISINGDEPTSUB = 7;

    }
}