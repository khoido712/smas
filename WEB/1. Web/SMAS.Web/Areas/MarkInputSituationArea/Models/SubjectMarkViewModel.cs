﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkInputSituationArea.Models
{
    public class SubjectMarkViewModel
    {
        public int markP { get; set; }
        public int markV { get; set; }
        public int markHK { get; set; }
    }
}