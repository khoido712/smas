﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkInputSituationArea.Models
{
    public class ListViewModel
    {
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string TeacherFullName { get; set; }
        public List<ClassMarkModel> lstMarkM { get; set; }
        public List<ClassMarkModel> lstMarkP { get; set; }
        public List<ClassMarkModel> lstMarkV { get; set; }
        public List<ClassMarkModel> lstMarkHK { get; set; }
    }
}