﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkInputSituationArea.Models
{
    public class ClassViewModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
    }
}