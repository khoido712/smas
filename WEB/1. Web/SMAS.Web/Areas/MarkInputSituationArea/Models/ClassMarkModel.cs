﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkInputSituationArea.Models
{
    public class ClassMarkModel
    {
        public int ClassID { get; set; }
        public int InputedNum { get; set; }
        public int MissPupilNum { get; set; }
        public int TotalPupil { get; set; }
        public string strMissDivTotal
        {
            get
            {
                return MissPupilNum + "/" + TotalPupil;
            }
        }

    }
}