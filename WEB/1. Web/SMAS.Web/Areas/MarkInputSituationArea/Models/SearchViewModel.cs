﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkInputSituationArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", MarkInputSituationConstants.CBO_SEMESTER)]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkInputSituation_Education_Level")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MarkInputSituationConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", MarkInputSituationConstants.CBO_CLASS)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("MarkInput_Label_GroupName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", MarkInputSituationConstants.CBO_GROUP)]
        [AdditionalMetadata("OnChange", "onGroupChange()")]
        public int? GroupID { get; set; }

        [ResourceDisplayName("Common_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", MarkInputSituationConstants.CBO_SUBJECT)]
        [AdditionalMetadata("OnChange", "onSubjectChange()")]
        public int? SubjectID { get; set; }

        public int FormType { get; set; }
    }
}