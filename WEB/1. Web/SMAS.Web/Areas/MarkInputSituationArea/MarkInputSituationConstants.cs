﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkInputSituationArea
{
    public class MarkInputSituationConstants
    {
        public const string CBO_EDUCATION_LEVEL = "CBO_EDUCATION_LEVEL";
        public const string CBO_CLASS = "CBO_CLASS";
        public const string CBO_SEMESTER = "CBO_SEMESTER";
        public const string CBO_SUBJECT = "CBO_SUBJECT";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string CBO_GROUP = "CBO_GROUP";
        public const string LIST_DATA = "LIST_DATA";
    }
}