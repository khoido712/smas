﻿using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Utils;
using SMAS.Web.Areas.MarkInputSituationArea.Models;
using SMAS.Business.Common;
using SMAS.Business.Business;
using System.IO;
using SMAS.VTUtils.HtmlHelpers.Control;

namespace SMAS.Web.Areas.MarkInputSituationArea.Controllers
{
    public class MarkInputSituationController:BaseController
    {
        #region properties
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness ;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkInputSituationBusiness MarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        #endregion

        #region Constructor
        public MarkInputSituationController(IClassSubjectBusiness ClassSubjectBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness, IPupilOfClassBusiness PupilOfClassBusiness,
            IMarkInputSituationBusiness MarkInputSituationBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness, IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness)
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkInputSituationBusiness = MarkInputSituationBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
        }
        #endregion

        #region Actions
       
        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        public PartialViewResult Search(SearchViewModel form)
        {
            Utils.Utils.TrimObject(form);

            if (form.FormType == 1)
            {
                #region Code lai phan thong ke tinh hinh nhap diem
                //Lay danh sach lop
                IDictionary<string, object> dic = new Dictionary<string, object>();

                dic.Add("EducationLevelID", form.EducationLevel);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                    .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                List<ClassViewModel> lstClassModel = lstClass.Select(o => new ClassViewModel { ClassID = o.ClassProfileID, ClassName = o.DisplayName }).ToList();
                ViewData[MarkInputSituationConstants.LIST_CLASS] = lstClassModel;

                List<ListViewModel> lstResult = new List<ListViewModel>();

                if (form.EducationLevel != null && form.SubjectID != null)
                {
                    lstResult = _Search(form.Semester.Value, form.EducationLevel.Value, form.SubjectID.HasValue ? form.SubjectID.Value : 0);
                }

                return PartialView("_List", lstResult);
                #endregion
            }
            else
            {
                #region Tim kiem danh sach hoc sinh thieu diem, giu nguyen code nhu chuc nang cu
                ReportMissingMarkSituationBO bo = MarkInputSituationBusiness.GetMissingMark(_globalInfo.SchoolID.GetValueOrDefault(),
               _globalInfo.AcademicYearID.GetValueOrDefault(), form.Semester.Value, form.EducationLevel.Value, form.ClassID.GetValueOrDefault(),form.SubjectID.GetValueOrDefault());
                if (bo.ListPupil == null || bo.ListPupil.Count == 0)
                {
                    ViewData["ListMissingMark"] = Res.Get("common_Validate_NoData");
                }
                else if (bo.ListSubjectName == null || bo.ListSubjectName.Count == 0)
                {
                    ViewData["ListMissingMark"] = Res.Get("common_Validate_NoData");
                }
                else
                {
                    //Tạo table
                    Table table = new Table();
                    int index = 1;

                    //Tạo Header
                    table.Thead.SetAttributes(new[] { "class", "t-grid-header" });
                    List<string> listStaticHeader = new List<string> {
                    Res.Get("Common_Column_Order"),
                    Res.Get("ReportMarkInputSituation_Column_Class"),
                    Res.Get("ReportMarkInputSituation_Column_Name")
                };
                    table.Thead.AddTR(Tr.ParseTH(listStaticHeader, new[] { "class", "t-header", "rowspan", "2" })
                        .AddRangeTH(bo.ListSubjectName, new[] { "class", "t-header", "colspan", "3" }));

                    Tr secondRow = new Tr();
                    foreach (string subject in bo.ListSubjectName)
                    {
                        secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_Writing"), new[] { "class", "t-header" }));
                        secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_TwiceCoeffiecient"), new[] { "class", "t-header" }));
                        secondRow.AddTH(Th.Parse(Res.Get("ReportMarkInputSituation_Row_Final"), new[] { "class", "t-header" }));
                    }
                    table.Thead.AddTR(secondRow);

                    foreach (MissingMarkOfPupilBO pupil in bo.ListPupil)
                    {
                        if (pupil.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        {
                            table.Tbody.AddTR(new Tr()
                                .AddTD(index, new[] { "class", "t-cell-number" })
                                .AddTD(pupil.ClassName)
                                .AddTD(pupil.PupilName)
                                .AddRangeTD(pupil.ListMissingMark, new[] { "class", "t-cell-number" }));
                        }
                        else
                        {
                            table.Tbody.AddTR(new Tr()
                                .AddTD(index, new[] { "class", "t-cell-number" })
                                .AddTD(pupil.ClassName, new[] { "style", " color:red;" })
                                .AddTD(pupil.PupilName, new[] { "style", " color:red;" })
                                .AddRangeTD(pupil.ListMissingMark, new Dictionary<string, string> { { "class", "t-cell-number" }, { "style", "color:red;" } }));
                        }
                        index++;
                    }
                    ViewData["ListMissingMark"] = string.Format("<div class=\"t-widget t-grid\">{0}</div>", table.ToHTML());
                }
                return PartialView("_ListMissingMark");

                #endregion
            }
        }

        [HttpPost]
        public PartialViewResult AjaxSearchByGroupSubject(SearchViewModel form)
        {
            int SchoolID = _globalInfo.SchoolID.Value;
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SemesterID = form.Semester.HasValue ? form.Semester.Value : 0;
            int EducationLevelID = form.EducationLevel.HasValue ? form.EducationLevel.Value : 0;
            ReportMarkInputSituationBO data = ReportMarkInputSituationBusiness.GetInputMark(
                SchoolID, AcademicYearID, SemesterID, EducationLevelID);
            ViewData[MarkInputSituationConstants.LIST_DATA] = data;
            return PartialView("_ListSearchByGroupSubject");
        }

        [HttpPost]
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("EducationLevelID", educationLevelID);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        [HttpPost]
        public JsonResult AjaxLoadSubject(int educationLevelID)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["SchoolID"] = _globalInfo.SchoolID;
            Dictionary["EducationLevelID"] = educationLevelID;
            Dictionary["AcademicYearID"] = _globalInfo.AcademicYearID;
            Dictionary["AppliedLevel"] = _globalInfo.AppliedLevel;

            IEnumerable<SubjectCatBO> lstSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), Dictionary)
                                            .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                                            .Select(o => new
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName
                                            }).ToList().Distinct()
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectCatID,
                                                DisplayName = o.DisplayName
                                            });

            return Json(new SelectList(lstSubject, "SubjectCatID", "DisplayName"), JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = form.Semester;
            dic["EducationLevel"] = form.EducationLevel;
            dic["SubjectID"] = form.SubjectID;

            if (form.FormType == 1)
            {
                ReportDefinition reportDef = null;
                ProcessedReport processedReport = null;
                if (form.GroupID == 0)
                {
                    string reportCode = SystemParamsInFile.REPORT_MARK_INPUT_SITUATION;
                    reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                    Stream excel = MarkInputSituationBusiness.CreateMarkInputSituationReport(dic);
                    processedReport = MarkInputSituationBusiness.InsertMarkInputSituationReport(dic, excel);
                    excel.Close();
                }
                else
                {
                    reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_TKTINHHINHNHAPDIEM_KHOI10_HKI);
                    Stream excel = ReportMarkInputSituationBusiness.CreateReportMarkInputSituation(
                    _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, form.Semester.Value, form.EducationLevel.Value);
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    processedReport = ReportMarkInputSituationBusiness.InsertMarkInputSituationReport(dic, excel);
                    //string SchoolLevel = ReportUtils.ConvertAppliedLevelForReportName(Edu.Grade); ;
                    //string semester = ReportUtils.ConvertSemesterForReportName(SemesterID);
                    //string EducationLevel = ReportUtils.StripVNSign(Edu.Resolution);
                    //string ReportName = "HS_" + SchoolLevel + "_TKTinhHinhNhapDiem_" + EducationLevel + "_" + semester + ".xls";
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
            else
            {
                string reportCode = SystemParamsInFile.REPORT_MISSING_MARK;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                ProcessedReport processedReport = null;

                Stream excel = MarkInputSituationBusiness.CreateReportPupilNotEnoughMark(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value
                    , form.Semester.Value, form.EducationLevel.Value, form.ClassID.GetValueOrDefault(), form.SubjectID.GetValueOrDefault(),_globalInfo.AppliedLevel.Value);

                processedReport = MarkInputSituationBusiness.InsertReportPupilNotEnoughMark(dic, excel);
                excel.Close();

                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Danh sach hoc ky
            ViewData[MarkInputSituationConstants.CBO_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            //Lay danh sach khoi
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;

            int? defaultEducationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                defaultEducationLevelID = lstEducationLevel.First().EducationLevelID;
            }
            ViewData[MarkInputSituationConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            List<ClassProfile> lstClass;
            IDictionary<string, object> dic;
            if (defaultEducationLevelID != null)
            {
                dic = new Dictionary<string, object>();

                dic.Add("EducationLevelID", defaultEducationLevelID);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                    .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                lstClass = new List<ClassProfile>();
            }
            ViewData[MarkInputSituationConstants.CBO_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            //danh sach nhom
            List<SelectListItem> lstGroup = new List<SelectListItem>();
            lstGroup.Add(new SelectListItem { Text = Res.Get("Giáo viên"), Value = "0" ,Selected = true});
            lstGroup.Add(new SelectListItem { Text = Res.Get("Môn"), Value = "1" });
            ViewData[MarkInputSituationConstants.CBO_GROUP] = new SelectList(lstGroup, "Value", "Text");

            //Lay danh sach mon hoc
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["EducationLevelID"] = defaultEducationLevelID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["IsVNEN"] = true;

            IEnumerable<SubjectCatBO> lstSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                             .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                                            .Select(o => new
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName
                                            }).ToList().Distinct()
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectCatID,
                                                DisplayName = o.DisplayName
                                            });

            ViewData[MarkInputSituationConstants.CBO_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "DisplayName");
        }


        private List<ListViewModel> _Search(int semester, int educationLevel, int subjectId)
        {
            List<ListViewModel> lstModel = new List<ListViewModel>();

            IDictionary<string, object> dic = new Dictionary<string, object>();

            //Lay danh sach lop
            dic.Add("EducationLevelID", educationLevel);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            //Lay ra danh sach phan cong giang day
            dic = new Dictionary<string, object>();
            dic["EducationLevel"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["Semester"] = semester;
            dic["IsActive"] = true;
            List<TeachingAssignment> lstTa = TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Lay danh sach mon hoc cho lop
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["EducationLevelID"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;
            dic["IsVNEN"] = true;
            List<ClassSubject> lstCs = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Lấy thông tin học sinh đang học
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevel;
            dic["CheckWithClass"] = "CheckWithClass";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).AddCriteriaSemester(aca, semester).ToList();

            //Lấy thông tin điểm 
            List<MaxMarkNumberBO> listMark = MarkInputSituationBusiness
                .GetMaxInputMarkOfClass(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semester, educationLevel).ToList();

            List<MarkInputSituationBO> lstMarkInputSituation = MarkInputSituationBusiness.GetMarkInputSituation(subjectId, semester, lstClass, lstTa, lstCs, listPupilOfClass, listMark, aca);

            if (lstMarkInputSituation != null)
            {
                for (int i = 0; i < lstMarkInputSituation.Count; i++)
                {
                    MarkInputSituationBO misBO = lstMarkInputSituation[i];
                    ListViewModel model = new ListViewModel();
                    lstModel.Add(model);

                    model.TeacherID = misBO.TeacherID;
                    model.TeacherName = misBO.TeacherName;
                    model.TeacherFullName = misBO.TeacherFullName;
                    model.lstMarkM = misBO.lstMarkM.Select(o => new ClassMarkModel
                        {
                            ClassID = o.ClassID,
                            InputedNum = o.InputedNum,
                            MissPupilNum = o.MissPupilNum,
                            TotalPupil = o.TotalPupil
                        }).ToList();

                    model.lstMarkP = misBO.lstMarkP.Select(o => new ClassMarkModel
                    {
                        ClassID = o.ClassID,
                        InputedNum = o.InputedNum,
                        MissPupilNum = o.MissPupilNum,
                        TotalPupil = o.TotalPupil
                    }).ToList();

                    model.lstMarkV = misBO.lstMarkV.Select(o => new ClassMarkModel
                    {
                        ClassID = o.ClassID,
                        InputedNum = o.InputedNum,
                        MissPupilNum = o.MissPupilNum,
                        TotalPupil = o.TotalPupil
                    }).ToList();

                    model.lstMarkHK = misBO.lstMarkHK.Select(o => new ClassMarkModel
                    {
                        ClassID = o.ClassID,
                        InputedNum = o.InputedNum,
                        MissPupilNum = o.MissPupilNum,
                        TotalPupil = o.TotalPupil
                    }).ToList();

                }
            }

            return lstModel;

        }

        #endregion


    }
}