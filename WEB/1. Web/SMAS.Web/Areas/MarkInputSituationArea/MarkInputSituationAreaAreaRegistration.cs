﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkInputSituationArea
{
    public class MarkInputSituationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkInputSituationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkInputSituationArea_default",
                "MarkInputSituationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
