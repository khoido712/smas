﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea
{
    public class ApprenticeshipTrainingScheduleAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipTrainingScheduleArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipTrainingScheduleArea_default",
                "ApprenticeshipTrainingScheduleArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
