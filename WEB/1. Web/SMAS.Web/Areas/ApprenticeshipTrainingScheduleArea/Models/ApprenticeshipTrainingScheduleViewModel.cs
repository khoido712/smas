/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea.Models
{
    public class ApprenticeshipTrainingScheduleViewModel
    {
		public System.Int32 ApprenticeshipTrainingScheduleID { get; set; }								
        //public System.Int32 AcademicYearID { get; set; }								
        //public System.Nullable<System.Int32> SchoolID { get; set; }								
        //public System.Int32 ClassID { get; set; }								
		public System.Nullable<System.Int32> Semester { get; set; }								
		public System.Int32 Month { get; set; }
        
        [ResourceDisplayName("ApprenticeshipTrainingSchedule_Column_SectionInDay")]
        public System.Nullable<System.Int32> SectionInDay { get; set; }								
		
        public System.String ScheduleDetail { get; set; }
       
        [ResourceDisplayName("ApprenticeshipTrainingSchedule_Column_ClassID")]
        public System.String DisplayName { get; set; }

        public int ApprenticeshipClassID { get; set; }



       
							
	       
    }
}


