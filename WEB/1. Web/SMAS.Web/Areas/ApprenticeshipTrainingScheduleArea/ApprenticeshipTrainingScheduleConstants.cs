/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea
{
    public class ApprenticeshipTrainingScheduleConstants
    {
        public const string LIST_APPRENTICESHIPTRAININGSCHEDULE = "listApprenticeshipTrainingSchedule";
        public const string LIST_SEMESTER = "ListSemester";
        public const string LIST_SECTIONINDAY = "ListSectionInDay";
        public const string LIST_MONTH = "ListMonth";
        public const string IS_YEAR4 = "IS_YEAR4";
        public const string DISABLED_SAVE = "DISABLED_SAVE";
        public const string DEFAULT_SEMESTER = "DefaultSemester";
        public const string DAYS_OF_MONTH = "DAYS_OF_MONTH";
    }
}