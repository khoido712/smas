﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea.Models;
using SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ApprenticeshipTrainingScheduleArea.Controllers
{
    public class ApprenticeshipTrainingScheduleController : BaseController
    {
        private readonly IApprenticeshipTrainingScheduleBusiness ApprenticeshipTrainingScheduleBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public ApprenticeshipTrainingScheduleController(IApprenticeshipTrainingScheduleBusiness apprenticeshiptrainingscheduleBusiness,
                                                         IAcademicYearBusiness academicyearBusiness,
                                                         IApprenticeshipClassBusiness apprenticeshipclassBusiness)
        {
            this.ApprenticeshipTrainingScheduleBusiness = apprenticeshiptrainingscheduleBusiness;
            this.ApprenticeshipClassBusiness = apprenticeshipclassBusiness;
            this.AcademicYearBusiness = academicyearBusiness;
        }

        //
        // GET: /ApprenticeshipTrainingSchedule/


        #region SetViewData
        private void SetViewData()
        {
            LoadSemester();
            //ViewData[ApprenticeshipTrainingScheduleConstants.LIST_MONTH] = new SelectList(new string[] { });
            List<ComboObject> listSectionInDay = new List<ComboObject>();
            listSectionInDay.Add(new ComboObject("Buổi sáng", "001"));
            listSectionInDay.Add(new ComboObject("Buổi chiều", "010"));
            listSectionInDay.Add(new ComboObject("Buổi tối", "100"));
            ViewData[ApprenticeshipTrainingScheduleConstants.LIST_SECTIONINDAY] = listSectionInDay;
        }

        private void LoadSemester()
        {
            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_FIRST.ToString(), Res.Get("Common_Label_FirstSemester")));
            lsSemester.Add(new ComboObject(SMAS.Business.Common.GlobalConstants.SEMESTER_OF_YEAR_SECOND.ToString(), Res.Get("Common_Label_SecondSemester")));

            GlobalInfo global = new GlobalInfo();
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            int semester = acaYear.FirstSemesterEndDate.HasValue && acaYear.FirstSemesterEndDate > DateTime.Now
                                ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;


            ViewData[ApprenticeshipTrainingScheduleConstants.LIST_SEMESTER] = new SelectList(lsSemester, "key", "value", semester.ToString());
            GetMonth(semester);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetMonth(int? Semester)
        {
            AcademicYear academicyear = this.AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            int year = academicyear.Year;
            int nextyear = year + 1;
            int FirstSemesterStartMonth = academicyear.FirstSemesterStartDate.Value.Month;
            int FirstSemesterEndMonth = academicyear.FirstSemesterEndDate.Value.Month;
            int SecondSemesterStartMonth = academicyear.SecondSemesterStartDate.Value.Month;
            int SecondSemesterEndMonth = academicyear.SecondSemesterEndDate.Value.Month;

            List<ComboObject> listMonth = new List<ComboObject>();
            if (Semester == 1)
            {
                if (FirstSemesterEndMonth < FirstSemesterStartMonth)
                {
                    for (int i = FirstSemesterStartMonth; i <= 12; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + year));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + year));
                    }


                    for (int i = 1; i <= FirstSemesterEndMonth; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + nextyear));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + nextyear));
                    }
                }
                else
                {
                    for (int i = FirstSemesterStartMonth; i <= FirstSemesterEndMonth; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + year));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + year));
                    }
                }

                ViewData[ApprenticeshipTrainingScheduleConstants.LIST_MONTH] = new SelectList(listMonth, "key", "value");

            }

            if (Semester == 2)
            {
                if (SecondSemesterEndMonth < SecondSemesterStartMonth)
                {
                    for (int i = SecondSemesterStartMonth; i <= 12; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + nextyear));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + nextyear));
                    }


                    for (int i = 1; i <= SecondSemesterEndMonth; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + nextyear));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + nextyear));
                    }
                }
                else
                {
                    for (int i = SecondSemesterStartMonth; i <= SecondSemesterEndMonth; i++)
                    {
                        if (i < 10)
                        {
                            listMonth.Add(new ComboObject("" + i, "0" + i + "/" + nextyear));
                        }
                        else
                            listMonth.Add(new ComboObject("" + i, i + "/" + nextyear));
                    }
                }

                ViewData[ApprenticeshipTrainingScheduleConstants.LIST_MONTH] = new SelectList(listMonth, "key", "value");

            }

            return Json(new SelectList(listMonth, "key", "value"));

        }
        #endregion

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region LoadGrid

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(int Month, int Semester)
        {
            AcademicYear academicyear = this.AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            int dayOfMonth = DateTime.DaysInMonth(academicyear.Year, Month);

            ViewData[ApprenticeshipTrainingScheduleConstants.DAYS_OF_MONTH] = dayOfMonth;

            List<ComboObject> listSectionInDay = new List<ComboObject>();
            listSectionInDay.Add(new ComboObject("1", "Sáng"));
            listSectionInDay.Add(new ComboObject("2", "Chiều"));
            listSectionInDay.Add(new ComboObject("4", "Tối"));
            ViewData[ApprenticeshipTrainingScheduleConstants.LIST_SECTIONINDAY] = listSectionInDay;

            IDictionary<string, object> SearchInfoTraining = new Dictionary<string, object>();
            SearchInfoTraining["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfoTraining["Semester"] = Semester;
            List<ApprenticeshipClass> lstApprenticeshipClass = this.ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoTraining).ToList();

            IDictionary<string, object> SearchInfoSchedule = new Dictionary<string, object>();
            SearchInfoSchedule["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfoSchedule["Semester"] = Semester;
            SearchInfoSchedule["Month"] = Month;

            List<ApprenticeshipTrainingSchedule> lstApprenticeshipTrainingSchedule = this.ApprenticeshipTrainingScheduleBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoSchedule).ToList();

            List<ApprenticeshipTrainingScheduleViewModel> lstSchedule = new List<ApprenticeshipTrainingScheduleViewModel>();

            foreach (ApprenticeshipClass item in lstApprenticeshipClass)
            {
                ApprenticeshipTrainingScheduleViewModel objSchedule = new ApprenticeshipTrainingScheduleViewModel();
                objSchedule.DisplayName = item.ClassName;
                objSchedule.ApprenticeshipClassID = item.ApprenticeshipClassID;
                objSchedule.Month = (int)Month;
                objSchedule.Semester = (int)Semester;
                objSchedule.ApprenticeshipTrainingScheduleID = 0;
                ApprenticeshipTrainingSchedule listTrainingSchedule = lstApprenticeshipTrainingSchedule.Where(o => o.ApprenticeshipClassID == item.ApprenticeshipClassID).SingleOrDefault();
                if (listTrainingSchedule != null)
                {
                    objSchedule.SectionInDay = listTrainingSchedule.SectionInDay;
                    objSchedule.ScheduleDetail = listTrainingSchedule.ScheduleDetail;
                    objSchedule.ApprenticeshipTrainingScheduleID = listTrainingSchedule.ApprenticeshipTrainingScheduleID;
                }

                lstSchedule.Add(objSchedule);
            }

            lstSchedule = lstSchedule.OrderBy(u => u.DisplayName).ToList();

            Dictionary<int, List<ComboObject>> Dic_listScheduleDetail = new Dictionary<int, List<ComboObject>>();
            
            foreach (var item in lstSchedule)
            {
                List<ComboObject> listScheduleDetail = new List<ComboObject>();
                if (item.ScheduleDetail != null)
                {
                    for (int i = 0; i < item.ScheduleDetail.Length; i++)
                    {
                        if (item.ScheduleDetail[i] == 'K')
                            listScheduleDetail.Add(new ComboObject("C", ""));
                        else
                            listScheduleDetail.Add(new ComboObject("C", "checked"));
                    }
                }
                else
                {
                    for (int i = 0; i < dayOfMonth; i++)
                    {
                        listScheduleDetail.Add(new ComboObject("C", ""));
                    }
                }
                Dic_listScheduleDetail.Add(item.ApprenticeshipClassID, listScheduleDetail);
            }

            ViewData[ApprenticeshipTrainingScheduleConstants.IS_YEAR4] = dayOfMonth < 30; ;
            ViewData["listScheduleDetail"] = Dic_listScheduleDetail;
            ViewData[ApprenticeshipTrainingScheduleConstants.DISABLED_SAVE] = "display:block";

            if (!new GlobalInfo().IsCurrentYear)
            {
                ViewData[ApprenticeshipTrainingScheduleConstants.DISABLED_SAVE] = "display:none";
            }

            return PartialView("_List", lstSchedule);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Insert
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Insert(SearchViewModel model, FormCollection form)
        {
            int SchoolID = new GlobalInfo().SchoolID.Value;
            int AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            int Semester = model.Semester.Value;
            int Month = model.Month;

            IDictionary<string, object> SearchInfoTraining = new Dictionary<string, object>();
            SearchInfoTraining["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfoTraining["Semester"] = Semester;
            List<ApprenticeshipClass> lstApprenticeshipClass = this.ApprenticeshipClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoTraining).ToList();

            IDictionary<string, object> SearchInfoSchedule = new Dictionary<string, object>();
            SearchInfoSchedule["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SearchInfoSchedule["Semester"] = Semester;
            SearchInfoSchedule["Month"] = Month;

            List<ApprenticeshipTrainingSchedule> lstApprenticeshipTrainingSchedule = this.ApprenticeshipTrainingScheduleBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfoSchedule).ToList();

            List<ApprenticeshipTrainingScheduleViewModel> lstSchedule = new List<ApprenticeshipTrainingScheduleViewModel>();

            foreach (ApprenticeshipClass item in lstApprenticeshipClass)
            {
                ApprenticeshipTrainingScheduleViewModel objSchedule = new ApprenticeshipTrainingScheduleViewModel();
                objSchedule.DisplayName = item.ClassName;
                objSchedule.ApprenticeshipClassID = item.ApprenticeshipClassID;
                objSchedule.Month = (int)Month;
                objSchedule.Semester = (int)Semester;
                objSchedule.ApprenticeshipTrainingScheduleID = 0;
                ApprenticeshipTrainingSchedule listTrainingSchedule = lstApprenticeshipTrainingSchedule.Where(o => o.ApprenticeshipClassID == item.ApprenticeshipClassID).FirstOrDefault();
                if (listTrainingSchedule != null)
                {
                    objSchedule.SectionInDay = listTrainingSchedule.SectionInDay;
                    objSchedule.ScheduleDetail = listTrainingSchedule.ScheduleDetail;
                    objSchedule.ApprenticeshipTrainingScheduleID = listTrainingSchedule.ApprenticeshipTrainingScheduleID;
                }

                lstSchedule.Add(objSchedule);
            }

            foreach (var itemInsert in lstSchedule)
            {
                ApprenticeshipTrainingSchedule apprenticeshipTrainingScheduleInsert = new ApprenticeshipTrainingSchedule();
                apprenticeshipTrainingScheduleInsert.ApprenticeshipClassID = itemInsert.ApprenticeshipClassID;
                apprenticeshipTrainingScheduleInsert.Month = model.Month;
                apprenticeshipTrainingScheduleInsert.Semester = model.Semester;
                apprenticeshipTrainingScheduleInsert.SchoolID = SchoolID;
                apprenticeshipTrainingScheduleInsert.AcademicYearID = AcademicYearID;
                apprenticeshipTrainingScheduleInsert.CreatedDate = DateTime.Now;
                apprenticeshipTrainingScheduleInsert.ApprenticeshipTrainingScheduleID = itemInsert.ApprenticeshipTrainingScheduleID;
                apprenticeshipTrainingScheduleInsert.SectionInDay = (int)GetSectionInDay(itemInsert.ApprenticeshipClassID, form);
                apprenticeshipTrainingScheduleInsert.ScheduleDetail = GetSchedule(itemInsert.ApprenticeshipClassID, Month, form);


                this.ApprenticeshipTrainingScheduleBusiness.InsertApprenticeshipTrainingSchedule(apprenticeshipTrainingScheduleInsert);
                this.ApprenticeshipTrainingScheduleBusiness.Save();
            }

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        public static int? GetSectionInDay(int ApprenticeshipClassID, FormCollection form)
        {

            string cboSectionInDay = form.Get("SectionInDay_" + ApprenticeshipClassID);
            if (!String.IsNullOrEmpty(cboSectionInDay))
            {
                int SectionInDay = Int32.Parse(cboSectionInDay);
                return SectionInDay;
            }
            return null;
        }

        public string GetSchedule(int ApprenticeshipClassID, int Month, FormCollection form)
        {
            GlobalInfo glo = new GlobalInfo();

            AcademicYear academicyear = this.AcademicYearBusiness.Find(glo.AcademicYearID.Value);

            int dayOfMonth = DateTime.DaysInMonth(academicyear.Year, Month);
            string ScheduleDetail = "";
            for (int i = 1; i <= 31; i++)
            {
                string ScheduleCheck = form.Get(ApprenticeshipClassID + "_" + i);
                if (ScheduleCheck != null)
                {

                    ScheduleDetail += ScheduleCheck;
                }
                else
                {
                    ScheduleDetail = ScheduleDetail + "K";
                }
            }

            return ScheduleDetail;

        }
        #endregion
    }
}





