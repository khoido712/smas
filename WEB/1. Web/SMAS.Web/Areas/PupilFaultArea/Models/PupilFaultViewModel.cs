/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SMAS.Web.Areas.PupilFaultArea.Models
{
    public class PupilFaultViewModel
    {
        public int PupilFaultID;

        [ResourceDisplayName("PupilFault_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilName")]
        public string PupilName { get; set; }

        [ResourceDisplayName("PupilFault_Label_ViolatedDate")]
        public DateTime ViolatedDate { get; set; }

        [ResourceDisplayName("PupilFault_Label_FaultName")]
        public string FaultName { get; set; }

        [ResourceDisplayName("PupilFault_Label_TotalPenalizedMark")]
        public Decimal? TotalPenalizedMark { get; set; }

        [ResourceDisplayName("PupilFault_Label_TotalPenalizedMark")]
        public string strTotal
        {
            get
            {
                string result = string.Empty;
                if (TotalPenalizedMark.HasValue)
                {
                    if (TotalPenalizedMark == 0 || TotalPenalizedMark == 10)
                    {
                        if (TotalPenalizedMark == 0)
                        {
                            result = "0.0";
                        }
                        else
                        {
                            result = "10";
                        }
                    }
                    else
                    {
                        result = TotalPenalizedMark.Value.ToString("0.#").Replace(",", ".");
                    }
                }
                return result;
            }
        }

        public bool CanEdit { get; set; }

        public bool CanDelete { get; set; }

        [ResourceDisplayName("PupilFault_Label_NumberOfFault")]
        public int NumberOfFault { get; set; }

        public int PupilStatus { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int PupilID { get; set; }

        public string Name { get; set; }
        
        public int? ClassOrderNumber { get; set; }

        public int? OrderOfPupilInClass { get; set; }
    }
}
