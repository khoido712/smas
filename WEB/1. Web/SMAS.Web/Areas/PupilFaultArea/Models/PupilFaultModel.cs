﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.PupilFaultArea.Models;
using SMAS.Web.Areas.PupilFaultArea;
using SMAS.Models.Models;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.PupilFaultArea.Models
{
    /// <summary>
    /// Search Fault Model
    /// </summary>
    public class SearchFaultModel
    {
        [ResourceDisplayName("")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("OnChange", "SelectClass(this)")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$")]
        public DateTime? SearchDate { get; set; }

        public int EducationLevelID { get; set; }
        /// <summary>
        /// ClassID
        /// </summary>
        public int SelectedClassID { get; set; }
        /// <summary>
        /// History ClassID
        /// </summary>
        public int CurrentClassID { get; set; }
        /// <summary>
        /// ListClass
        /// </summary>
        public List<ClassProfile> ListClass { get; set; }
        /// <summary>
        /// Contructor
        /// </summary>
        public SearchFaultModel()
        {
            SearchDate = DateTime.Now;
            SelectedClassID = 0;
            CurrentClassID = 0;
            ListClass = new List<ClassProfile>();
        }
    }
    /// <summary>
    /// Pupil List MOdel
    /// </summary>
    public class ListPupilModel
    {
        /// <summary>
        /// PupilFileID
        /// </summary>
        public int PupilID { get; set; }
        /// <summary>
        /// PupilFullName
        /// </summary>
        public string PupilName { get; set; }
        /// <summary>
        /// Class ID
        /// </summary>
        public int ClassID { get; set; }
        /// <summary>
        /// ClassName
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// Image Data
        /// </summary>
        public byte[] ImageData { get; set; }
        /// <summary>
        /// List's order number
        /// </summary>
        public int OrderNumber { get; set; }
        /// <summary>
        /// Trang thai hoc sinh
        /// </summary>
        public int Status { get; set; } 
    }
    /// <summary>
    /// Fault List Model
    /// </summary>
    public class PupilFaultModel
    {
        public int ? PupilFaultID { get; set; }
        public int FaultCriteriaID { get; set; }
        public string ClassName { get; set; }        
        public string PupilName { get; set; }
        public DateTime ViolatedDate { get; set; }
        public string FaultName { get; set; }
        public Decimal? TotalPenalizedMark { get; set; }
        public Decimal? PenalizedMark { get; set; }
        public bool CanEdit { get; set; }
        public int NumberOfFault { get; set; }
        public int ClassID { get; set; }
        public int PupilID { get; set; }
        public string Note { get; set; }
    }
    /// <summary>
    /// Collection object
    /// </summary>
    public class UpdateFaultModel
    {
        public List<PupilFaultModel> lstFault { get; set; }
        public int classID { get; set; }
        public DateTime? curDate { get; set; }
        public int pupilID { get; set; }
        public UpdateFaultModel()
        {
            lstFault = new List<PupilFaultModel>();
            classID = 0;
            curDate = DateTime.Now;
            pupilID = 0;
        }
    }
}