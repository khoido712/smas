using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilFaultArea.Models
{
    public class FaultGridViewModel
    {
        public int FaultID { get; set; }

        [ResourceDisplayName("PupilFault_Label_FaultName")]
        public string FaultName { get; set; }

        [ResourceDisplayName("PupilFault_Label_NumberOfFault")]
        public int NumberOfFault { get; set; }

        [ResourceDisplayName("PupilFault_Label_PenalizedMark")]
        public decimal? PenalizedMark { get; set; }

        [ResourceDisplayName("PupilFault_Label_TotalPenalizedMark")]
        public decimal? TotalPenalizedMark { get; set; }

        public bool IsFaultCriteriaAbsence { get; set; }
    }
}
