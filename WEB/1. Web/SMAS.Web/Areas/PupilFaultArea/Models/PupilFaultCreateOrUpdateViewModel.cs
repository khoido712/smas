using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilFaultArea.Models
{
    public class PupilFaultCreateOrUpdateViewModel
    {
        [ScaffoldColumn(false)]
        public int PupilFaultID { get; set; }

        [ResourceDisplayName("PupilFault_Label_EducationLevelID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass1(this)")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("PupilFault_Label_EducationLevelID")]
        [ScaffoldColumn(false)]
        [UIHint("Display")]
        public string EducationLevelName { get; set; }

        [ResourceDisplayName("PupilFault_Label_ClassID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_CLASS)]
        [AdditionalMetadata("OnChange", "AjaxLoadPupil(this)")]
        public int ClassID { get; set; }

        [ResourceDisplayName("PupilFault_Label_ClassID")]
        [ScaffoldColumn(false)]
        [UIHint("Display")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_PUPIL)]
        [AdditionalMetadata("OnChange", "AjaxLoadFaultList()")]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilID")]
        [ScaffoldColumn(false)]
        [UIHint("Display")]
        public string PupilName { get; set; }

        [ResourceDisplayName("PupilFault_Label_FaultGroupID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_FAULT_GROUP)]
        [AdditionalMetadata("OnChange", "AjaxLoadFaultList()")]
        public int FaultGroupID { get; set; }

        [ResourceDisplayName("PupilFault_Label_ViolatedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("OnChange", "AjaxLoadFaultList")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = "Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public System.DateTime ViolatedDate { get; set; }
    }
}
