/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.PupilFaultArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilFault_Label_FaultGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_FAULT_GROUP)]
        public int? FaultGroupID { get; set; }

        [ResourceDisplayName("PupilFault_Label_EducationLevelID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public Nullable<int> EducationLevelID { get; set; }

        [ResourceDisplayName("PupilFault_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_CLASS)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilFault_Label_PupilName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }

        [ResourceDisplayName("PupilFault_Label_FromDate")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "ToViolatedDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FromViolatedDate { get; set; }

        [ResourceDisplayName("PupilFault_Label_ToDate")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? ToViolatedDate { get; set; }

        [ResourceDisplayName("PupilFault_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", PupilFaultConstants.LIST_SEMESTER)]
        public int? Semester { get; set; }

        public int SearchBy { get; set; }
    }
}
