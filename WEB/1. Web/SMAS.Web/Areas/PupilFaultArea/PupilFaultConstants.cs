/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.PupilFaultArea
{
    public class PupilFaultConstants
    {
        public const string LIST_PUPILFAULT = "listPupilFault";
        public const string LIST_FAULT_GROUP = "listFaultGroup";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SEMESTER = "listSemester";
        public const int SEARCH_BY_TIME = 1;
        public const int SEARCH_BY_SEMESTER = 2;
        public const string LIST_PUPIL = "listPupil";
        public const string ENABLE_BUTTON = "enable_button";
        public const string CLASS_NAME = "class_name";
    }
}