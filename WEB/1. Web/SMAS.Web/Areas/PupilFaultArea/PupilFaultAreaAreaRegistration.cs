﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilFaultArea
{
    public class PupilFaultAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilFaultArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilFaultArea_default",
                "PupilFaultArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}