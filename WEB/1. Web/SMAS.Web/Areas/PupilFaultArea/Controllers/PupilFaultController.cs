﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.PupilFaultArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using System.Web.Script.Serialization;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.PupilFaultArea.Controllers
{
    public class PupilFaultController : BaseController
    {
        private readonly IPupilFaultBusiness PupilFaultBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IFaultGroupBusiness FaultGroupBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IFaultCriteriaBusiness FaultCriteriaBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ILockTrainingBusiness LockTrainingBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness; 

        public PupilFaultController(IPupilFaultBusiness pupilfaultBusiness,
            IClassProfileBusiness classProfileBusiness,
            IFaultGroupBusiness faultGroupBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IFaultCriteriaBusiness faultCriteriaBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            ILockTrainingBusiness lockTrainingBusiness,
            ISchoolProfileBusiness schoolProfileBusiness)
        {
            this.PupilFaultBusiness = pupilfaultBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.FaultGroupBusiness = faultGroupBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.FaultCriteriaBusiness = faultCriteriaBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.LockTrainingBusiness = lockTrainingBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }

        private void SetViewData(int? classID)
        {
            GlobalInfo global = new GlobalInfo();

            int selectedEducationLevel = 0;
            if (classID.HasValue && classID.Value > 0)
            {
                ClassProfile cp = ClassProfileBusiness.Find(classID);
                if (cp != null)
                {
                    selectedEducationLevel = cp.EducationLevelID;
                }
            }

            // Lấy danh sách nhóm vi phạm
            List<FaultGroup> ListFaultGroup = this.FaultGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.GetValueOrDefault(), new Dictionary<string, object>()).ToList();
            ListFaultGroup = ListFaultGroup.OrderBy(o => o.Resolution).ToList();
            ViewData[PupilFaultConstants.LIST_FAULT_GROUP] = new SelectList(ListFaultGroup, "FaultGroupID", "Resolution");

            // Lấy về danh sách khôi
            ViewData[PupilFaultConstants.LIST_EDUCATION_LEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution", selectedEducationLevel);

            // Danh sách lớp
            List<ClassProfile> lstClass = GetClassByEducationLevel(selectedEducationLevel, global.AppliedLevel.Value).ToList();
            ViewData[PupilFaultConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName", classID);

            // Danh sách học kỳ
            ViewData[PupilFaultConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[PupilFaultConstants.ENABLE_BUTTON] = "true";

            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, 0);
            bool isOverseeing = UtilsBusiness.HasOverseeingTeacherPermission(global.UserAccountID, 0);
            if (!isHeadTeacher && !isOverseeing)
            {
                ViewData[PupilFaultConstants.ENABLE_BUTTON] = "false";
            }
        }

        private IEnumerable<ClassProfile> GetClassByEducationLevel(int? EducationLevelID, int AppliedLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            dic["AppliedLevel"] = AppliedLevelID;
            if (!GlobalInfo.IsAdminSchoolRole && !GlobalInfo.IsViewAll && !GlobalInfo.IsEmployeeManager)
            {
                dic["UserAccountID"] = GlobalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
            }

            return this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).OrderBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
        }

        private IEnumerable<ClassProfile> GetClassByEducationLevelCreate(int EducationLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (!GlobalInfo.IsAdminSchoolRole)
            {
                dic["UserAccountID"] = GlobalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_OVERSEEING;
            }

            return this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
        }

        private IEnumerable<PupilProfile> GetPupilByClass(int? ClassID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IEnumerable<PupilProfile> lst = new List<PupilProfile>();
            if (ClassID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID;
                dic["Status"] = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING };
                lst = this.PupilOfClassBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).Select(o => o.PupilProfile).OrderBy(o => o.FullName)
                    .ToList();
            }
            return lst;
        }

        //
        // GET: /PupilFault/

        public ActionResult Index(int? ClassID)
        {
            SetViewData(ClassID);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            SearchViewModel svm = new SearchViewModel();
            svm.FromViolatedDate = DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)).Date;
            svm.ToViolatedDate = DateTime.Now.Date;

            if (ClassID.HasValue && ClassID.Value > 0)
                svm.ClassID = ClassID.Value;

            this.Search(svm);

            return View();
        }

        //
        // GET: /PupilFault/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            List<int> ListClassID = new List<int>();
 

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = GlobalInfo.AppliedLevel.Value;
            SearchInfo["FaultGroupID"] = frm.FaultGroupID;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.PupilName;
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID.Value;
            SearchInfo["EmployeeID"] = GlobalInfo.EmployeeID;

            //AnhVD 20140814 - Kiem tra lay danh sach phan cong
            //Neu la quan tri truong/BGH/xem thong tin toan truong
            if (GlobalInfo.IsViewAll || GlobalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager)
                {
                    ListClassID = GetClassByEducationLevel(frm.EducationLevelID.HasValue ? frm.EducationLevelID.Value : 0, GlobalInfo.AppliedLevel.Value).Select(s => s.ClassProfileID).ToList();
                }
            else //Danh sach lop giao vien phu trach. Fix bug 14964
                {
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["EducationLevelID"] = frm.EducationLevelID;
                dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;
                dicClass["AppliedLevel"] = GlobalInfo.AppliedLevel.Value;
                dicClass["UserAccountID"] = GlobalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
                //dicClass["TeacherByRoleID"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;

                ListClassID = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName).Select(c => c.ClassProfileID).ToList();
                //ListClassID = ClassProfileBusiness.SearchByTeacherPermission(SearchInfo).Select(o => o.ClassID).ToList();
                }
            
            SearchInfo["ListClassID"] = ListClassID;
            if (frm.SearchBy == PupilFaultConstants.SEARCH_BY_SEMESTER)
            {
                AcademicYear AcademicYear = this.AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
                SearchInfo["FromViolatedDate"] = frm.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? AcademicYear.SecondSemesterStartDate : AcademicYear.FirstSemesterStartDate;
                SearchInfo["ToViolatedDate"] = frm.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? AcademicYear.FirstSemesterEndDate : AcademicYear.SecondSemesterEndDate;
            }
            else
            {
                SearchInfo["FromViolatedDate"] = frm.FromViolatedDate.HasValue ? frm.FromViolatedDate.Value.Date : frm.FromViolatedDate;
                SearchInfo["ToViolatedDate"] = frm.ToViolatedDate.HasValue ? frm.ToViolatedDate.Value.Date : frm.ToViolatedDate;
            }

            IEnumerable<PupilFaultViewModel> lst = this._Search(SearchInfo);
            ViewData[PupilFaultConstants.LIST_PUPILFAULT] = lst;
            return PartialView("_List");
        }

        public PartialViewResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Lấy danh sách nhóm vi phạm
            List<FaultGroup> ListFaultGroup = this.FaultGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.GetValueOrDefault(), new Dictionary<string, object>()).ToList();
            ListFaultGroup = ListFaultGroup.OrderBy(o => o.Resolution).ToList();
            ViewData[PupilFaultConstants.LIST_FAULT_GROUP] = new SelectList(ListFaultGroup, "FaultGroupID", "Resolution");

            // Lấy về danh sách khôi
            ViewData[PupilFaultConstants.LIST_EDUCATION_LEVEL] = new SelectList(GlobalInfo.EducationLevels, "EducationLevelID", "Resolution");

            // Danh sách lớp
            ViewData[PupilFaultConstants.LIST_CLASS] = new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName");

            // Danh sách học sinh
            ViewData[PupilFaultConstants.LIST_PUPIL] = new SelectList(new List<PupilProfile>(), "PupilProfileID", "FullName");

            ViewData[PupilFaultConstants.LIST_PUPILFAULT] = new List<FaultGridViewModel>();

            return PartialView("_Create");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(PupilFaultCreateOrUpdateViewModel frm, Dictionary<int, int> DicNumberOfFault)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<int> ListFaultID = new List<int>();
            List<int> ListNum = new List<int>();
            if (DicNumberOfFault != null)
            {
                foreach (int key in DicNumberOfFault.Keys)
                {
                    if (DicNumberOfFault[key] != 0)
                    {
                        ListFaultID.Add(key);
                        ListNum.Add(DicNumberOfFault[key]);
                    }
                }
            }
            else
            {
                throw new BusinessException("PupilFault_Label_NoFault");
            }
            int count = 0;
            if (ListFaultID.Count == 0)
            {
                foreach (int key in DicNumberOfFault.Keys)
                {
                    if (FaultCriteriaBusiness.Find(key).Absence == "P" || FaultCriteriaBusiness.Find(key).Absence == "K")
                    {
                        count++;
                    }
                }
                if (count == DicNumberOfFault.Count)
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_NoVilolateNumberAbsence"), JsonMessage.ERROR));
                }
                return Json(new JsonMessage(Res.Get("Common_Label_NoVilolateNumber"), JsonMessage.ERROR));
            }
            this.PupilFaultBusiness.InsertPupilFault(
                GlobalInfo.AcademicYearID.Value,
                GlobalInfo.UserAccountID,
                GlobalInfo.SchoolID.Value,
                GlobalInfo.AppliedLevel.Value,
                frm.ClassID,
                frm.PupilID,
                frm.FaultGroupID,
                frm.ViolatedDate,
                ListFaultID,
                ListNum
            );

            this.PupilFaultBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        public PartialViewResult Edit(int PupilFaultID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            PupilFaultCreateOrUpdateViewModel frm = new PupilFaultCreateOrUpdateViewModel();

            PupilFault PupilFault = this.PupilFaultBusiness.Find(PupilFaultID);

            // Lấy danh sách nhóm vi phạm
            List<FaultGroup> ListFaultGroup = this.FaultGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.GetValueOrDefault(), new Dictionary<string, object>()).ToList();
            ListFaultGroup = ListFaultGroup.OrderBy(o => o.Resolution).ToList();
            ViewData[PupilFaultConstants.LIST_FAULT_GROUP] = new SelectList(ListFaultGroup, "FaultGroupID", "Resolution");

            AjaxLoadFaultList(PupilFault.FaultCriteria.GroupID.Value, PupilFault.PupilID, PupilFault.ViolatedDate);

            frm.EducationLevelName = PupilFault.EducationLevel.Resolution;
            frm.ClassName = PupilFault.ClassProfile.DisplayName;
            frm.PupilName = PupilFault.PupilProfile.FullName;
            frm.FaultGroupID = PupilFault.FaultCriteria.GroupID.Value;
            frm.ViolatedDate = PupilFault.ViolatedDate;

            return PartialView("_Edit", frm);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(PupilFaultCreateOrUpdateViewModel frm, Dictionary<int, int> DicNumberOfFault)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            PupilFault PupilFault = this.PupilFaultBusiness.Find(frm.PupilFaultID);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilFault.PupilID;
            dic["FaultGroupID"] = frm.FaultGroupID;
            dic["ViolatedDate"] = PupilFault.ViolatedDate;
            List<PupilFault> ListPupilFault = this.PupilFaultBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();

            foreach (PupilFault pf in ListPupilFault)
            {
                this.PupilFaultBusiness.DeletePupilFault(GlobalInfo.UserAccountID, GlobalInfo.IsAdmin, pf.PupilFaultID, GlobalInfo.SchoolID.Value);
            }

            if (DicNumberOfFault != null)
            {
                List<int> ListFaultID = new List<int>();
                List<int> ListNum = new List<int>();
                foreach (int key in DicNumberOfFault.Keys)
                {
                    if (DicNumberOfFault[key] != 0)
                    {
                        ListFaultID.Add(key);
                        ListNum.Add(DicNumberOfFault[key]);
                    }
                }
                if (ListFaultID.Count == 0)
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_NoVilolateNumber"), JsonMessage.ERROR));
                }
                this.PupilFaultBusiness.InsertPupilFault(GlobalInfo.AcademicYearID.Value, GlobalInfo.UserAccountID, GlobalInfo.SchoolID.Value,
                                                    GlobalInfo.AppliedLevel.Value, PupilFault.ClassID, PupilFault.PupilID, frm.FaultGroupID,
                                                    PupilFault.ViolatedDate, ListFaultID, ListNum);

                this.PupilFaultBusiness.Save();
            }
            else
            {
                throw new BusinessException("PupilFault_Label_NoFault");
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.PupilFaultBusiness.DeleteAllFaultInDay(_globalInfo.UserAccountID, _globalInfo.IsAdmin, id, _globalInfo.SchoolID.Value);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public bool checkOverHead(int classID)
        {
            GlobalInfo global = new GlobalInfo();
            if (!global.IsCurrentYear)
                return false;
            else
            {
                if (UtilsBusiness.HasHeadTeacherPermission(global.UserAccountID, classID) || UtilsBusiness.HasOverseeingTeacherPermission(global.UserAccountID, classID))
                    return true;
                else
                    return false;
            }
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PupilFaultViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[PupilFaultConstants.ENABLE_BUTTON] = "true";
            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(glo.UserAccountID, 0);
            bool isOverseeing = UtilsBusiness.HasOverseeingTeacherPermission(glo.UserAccountID, 0);
            if (!isHeadTeacher && !isOverseeing)
            {
                ViewData[PupilFaultConstants.ENABLE_BUTTON] = "false";
            }
            List<int> lstClassID1 =SMAS.Business.Common.Utils.GetIntList(SearchInfo, "ListClassID");
            //Neu giao vien khong co quyen lop nao thi tra ve danh sach rong
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager && lstClassID1.Count==0)
            {
                return new List<PupilFaultViewModel>();
            }
            var lstFault = (from p in PupilFaultBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).Where(x => x.EducationLevel.Grade == glo.AppliedLevel)
                            join pp in PupilProfileBusiness.All on p.PupilID equals pp.PupilProfileID
                            join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                            join poc in PupilOfClassBusiness.All on cp.ClassProfileID equals poc.ClassID
                            join q in FaultCriteriaBusiness.All.Where(o => o.SchoolID == glo.SchoolID.Value) on p.FaultID equals q.FaultCriteriaID
                            where poc.PupilID == pp.PupilProfileID
                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                            select new
                            {
                                PupilID = p.PupilID,
                                PupilCode = pp.PupilCode,
                                ClassID = p.ClassID,
                                EducationLevelID = cp.EducationLevelID,
                                ProfileStatus = pp.ProfileStatus,
                                FullName = pp.FullName,
                                Name = pp.Name,
                                DisplayName = cp.DisplayName,
                                ClassOrderNumber = cp.OrderNumber,
                                PupilFaultID = p.PupilFaultID,
                                ViolatedDate = p.ViolatedDate,
                                Resolution = q.Resolution,
                                TotalPenalizedMark = p.TotalPenalizedMark,
                                NumberOfFault = p.NumberOfFault,
                                Description = p.Description,
                                OrderOfPupilInClass = poc.OrderInClass,
                                Note = p.Note
                            })
                            .ToList()
                            .GroupBy(u => new { u.EducationLevelID, u.ClassID, u.PupilCode, u.DisplayName, u.ClassOrderNumber, u.OrderOfPupilInClass, u.PupilID, u.FullName, u.Name, u.ProfileStatus, u.ViolatedDate })
                            .Select(u => new PupilFaultViewModel
                               {
                                   ClassName = u.Key.DisplayName,
                                   ClassID = u.Key.ClassID,
                                   FaultName = string.Join(",", u.Select(v => v.Resolution + "(" + v.NumberOfFault + (!string.IsNullOrEmpty(v.Note) ? ": " + v.Note : "" )+ ")")),
                                   NumberOfFault = u.Sum(v => v.NumberOfFault).Value,
                                   PupilCode = u.Key.PupilCode,
                                   PupilID = u.Key.PupilID,
                                   PupilName = u.Key.FullName,
                                   Name = u.Key.Name,
                                   ClassOrderNumber = u.Key.ClassOrderNumber,
                                   PupilStatus = u.Key.ProfileStatus,
                                   TotalPenalizedMark = u.Sum(v => v.TotalPenalizedMark),
                                   ViolatedDate = u.Key.ViolatedDate,
                                   EducationLevelID = u.Key.EducationLevelID,
                                   PupilFaultID = u.Max(v => v.PupilFaultID),
                                   OrderOfPupilInClass = u.Key.OrderOfPupilInClass
                               })
                               .ToList()
                               .OrderBy(o => o.EducationLevelID)
                               .ThenBy(o => o.ClassOrderNumber ?? 0).ThenBy(o => o.ClassName)
                               .ThenBy(o => o.OrderOfPupilInClass ?? 0).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.PupilName))
                               .ThenBy(o => o.ViolatedDate)
                               .ToList();

            List<int> lstClassID = lstFault.Select(u => u.ClassID).Distinct().ToList();
            Dictionary<int, bool> dicCheckOverHead = new Dictionary<int, bool>();
            lstClassID.ForEach(u => dicCheckOverHead.Add(u, checkOverHead(u)));
            // Kiem tra neu tk giao vien va bi khoa thi bao loi
            List<LockTraining> listLockTraining = !glo.IsAdmin ? LockTrainingBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID", glo.AcademicYearID},
                    {"IsViolation", true}
                }).ToList() : new List<LockTraining>();
            foreach (PupilFaultViewModel u in lstFault)
            {
                u.CanDelete = dicCheckOverHead[u.ClassID];
                u.CanEdit = dicCheckOverHead[u.ClassID];
                LockTraining lockTraining = listLockTraining.Where(o => o.ClassID == u.ClassID).FirstOrDefault();
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : DateTime.MinValue;
                    if (fromLockDate <= u.ViolatedDate && lockTraining.ToDate.AddDays(1) > u.ViolatedDate)
                    {
                        // Khong cho sua xoa
                        u.CanDelete = false;
                        u.CanEdit = false;
                    }
                }
            }
            return lstFault.ToList();
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                lst = GetClassByEducationLevel(EducationLevelID.Value, GlobalInfo.AppliedLevel.Value).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass1(int? EducationLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                lst = GetClassByEducationLevelCreate(EducationLevelID.Value).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadPupil(int? ClassID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IEnumerable<PupilProfile> lst = GetPupilByClass(ClassID);
            if (lst == null)
                lst = new List<PupilProfile>();
            return Json(new SelectList(lst, "PupilProfileID", "FullName"));
        }

        /// <summary>
        /// Lấy về danh sách các vi phạm
        /// </summary>
        /// <param name="FaultGroupID"></param>
        /// <param name="PupilID"></param>
        /// <param name="ViolatedDate"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadFaultList(int FaultGroupID, int PupilID, DateTime ViolatedDate)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["FaultGroupID"] = FaultGroupID;
            List<FaultCriteria> ListFaultCriteria = this.FaultCriteriaBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic).ToList();
            ListFaultCriteria = ListFaultCriteria.OrderBy(o => o.FaultGroup.Resolution).ThenBy(o => o.Resolution).ToList();
                             
            dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["ViolatedDate"] = ViolatedDate;
            List<PupilFault> ListPupilFault = this.PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            List<FaultGridViewModel> ListFaultGridViewModel = new List<FaultGridViewModel>();
            foreach (FaultCriteria fc in ListFaultCriteria)
            {
                FaultGridViewModel Fault = new FaultGridViewModel();
                Fault.FaultID = fc.FaultCriteriaID;
                Fault.FaultName = fc.Resolution;
                Fault.PenalizedMark = fc.PenalizedMark;

                Fault.IsFaultCriteriaAbsence = fc.Absence != null;

                foreach (PupilFault pf in ListPupilFault)
                {
                    if (pf.FaultID == fc.FaultCriteriaID)
                    {
                        Fault.NumberOfFault = pf.NumberOfFault == null ? (int)0 : pf.NumberOfFault.Value;
                        break;
                    }
                }
                if (Fault.PenalizedMark.HasValue)
                {
                    Fault.TotalPenalizedMark = Fault.PenalizedMark * Fault.NumberOfFault;
                }
                ListFaultGridViewModel.Add(Fault);
            }

            ViewData[PupilFaultConstants.LIST_PUPILFAULT] = ListFaultGridViewModel;
            return PartialView("_FaultList");
        }



        public ActionResult FaultEditor(int? id, int? PupilID, DateTime? Date)
        {
            int classID = id.HasValue ? id.Value : 0;
            int pupilID = PupilID.HasValue ? PupilID.Value : 0;
            DateTime date = Date.HasValue ? Date.Value : DateTime.Now;

            if (pupilID != 0 && classID == 0)
            {
                classID = PupilProfileBusiness.Find(pupilID).CurrentClassID;
            }
            // Lấy về danh sách khôi
            ViewData[PupilFaultConstants.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            return LoadViewIndex(classID, 0, pupilID, date);
        }
        /// <summary>
        /// Load content
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="curDate"></param>
        /// <param name="curClassID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadContent(int classID, DateTime? curDate, int curClassID, int pupilID)
        {
            curDate = (curDate.HasValue) ? curDate : DateTime.Now;
            if (!_globalInfo.IsRolePrincipal && !_globalInfo.IsAdminSchoolRole)
            {
                RoleOfClass role = CheckRoleOfClass(classID);
                if (role == RoleOfClass.None)
                {
                    ViewData["ErrorAuthenticated"] = 1;
                    return PartialView("_Edit_Content");
                }
            }
            if (curDate > DateTime.Now)
            {
                ViewData["DateTimeError"] = 1;
            }
            else
            {
                LoadData(classID, pupilID, curDate, curClassID, true);
            }
            //Show error message / Redirect to MAIN
            return PartialView("_Edit_Content");
        }
        /// <summary>
        /// Load by pupilFileID
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="curDate"></param>
        /// <param name="curClassID"></param>
        /// <param name="pupilID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadByPupil(int classID, DateTime? curDate, int curClassID, int pupilID)
        {
            curDate = (curDate.HasValue) ? curDate : DateTime.Now;
            LoadData(classID, pupilID, curDate, curClassID, false);
            return PartialView("_Edit_ListFault");
        }

        public PartialViewResult AjaxLoadPupilByClass(int ClassID, string strSearch, int SelectedPupilID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["CurrentClassID"] = ClassID;
            dic["strSearch"] = strSearch;
            var query = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);

            ClassProfile cls = ClassProfileBusiness.Find(ClassID);
            ListPupilModel itmModel = null;
            int stt = 1;
            if (query != null)
            {
                List<ListPupilModel> lstPupil = new List<ListPupilModel>();
                var lstPupilProfiles = query.ToList().OrderBy(p => p.OrderInClass ?? 0).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                foreach (PupilProfileBO item in lstPupilProfiles)
                {
                    itmModel = new ListPupilModel();
                    itmModel.ClassID = ClassID;
                    itmModel.ClassName = cls.DisplayName;
                    itmModel.ImageData = item.Image;
                    itmModel.OrderNumber = stt;
                    itmModel.PupilID = item.PupilProfileID;
                    itmModel.PupilName = item.FullName;
                    itmModel.Status = item.ProfileStatus;
                    lstPupil.Add(itmModel);
                    stt++;
                    if (item.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING && SelectedPupilID == 0)
                    {
                        SelectedPupilID = item.PupilProfileID;
                    }
                }
                ViewData["lstPupil"] = lstPupil;
                ViewData["classSelectedID"] = ClassID;
                ViewData["pupilSelectedID"] = SelectedPupilID;
                ViewData["SearchAjax"] = true;
            }
            return PartialView("_Edit_ListPupil");
        }

        /// <summary>
        /// Save Fault
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="levelID"></param>
        /// <param name="typeID"></param>
        /// <returns></returns>
        [SMAS.Web.Filter.ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveFault(UpdateFaultModel model)
        {
            List<PupilFaultModel> lstFault = new JavaScriptSerializer().Deserialize<List<PupilFaultModel>>(Request["lstFault"]);
            int classID = model.classID;
            DateTime? curDate = model.curDate;
            int pupilID = model.pupilID;
            if (curDate.HasValue == false)
            {
                return Json(new JsonMessage(Res.Get("Fault_Date_Error"), "ERROR"));
            }
            //Check ROLE
            if (!_globalInfo.IsRolePrincipal && !_globalInfo.IsAdminSchoolRole)
            {
                RoleOfClass role = CheckRoleOfClass(classID);
                if (!(role == RoleOfClass.HeadTeacher || role == RoleOfClass.HeadTeacherSub || role == RoleOfClass.Overseeing))
                {
                    return Json(new JsonMessage(Res.Get("Fault_Role_Error"), "ERROR"));
                }
            }
            //Kiem tra thoi gian vi pham phai thuoc thoi gian nam hoc
            if (!AcademicYearBusiness.CheckTimeInYear(_globalInfo.AcademicYearID.Value, curDate.Value))
            {
                return Json(new JsonMessage(Res.Get("Fault_Role_Error"), "ERROR"));
            }
            //Check Pupil<->Class
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["CurrentClassID"] = classID;
            dic["PupilProfileID"] = pupilID;
            PupilProfileBO pupilProfile = null;
            var query = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            if (query != null)
            {
                pupilProfile = query.FirstOrDefault();
                if (pupilProfile.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    return Json(new JsonMessage(Res.Get("Fault_Pupil_Leave_Error"), "ERROR"));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Fault_Pupil_Error"), "ERROR"));
            }
            if (lstFault == null)
            {
                return Json(new JsonMessage(Res.Get("Fault_Pupil_Error"), "ERROR"));
            }
            //Lay danh sach Fault trong ngay
            dic = new Dictionary<string, object>();
            dic["PupilID"] = pupilID;
            dic["ClassID"] = classID;
            dic["ViolatedDate"] = curDate;
            dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID.Value;
            var lstInDay = PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            PupilFault indayItem = null;
            FaultCriteria faultItem = null;
            int coutSave = 0;
            bool isNew = false;
            //Xoa nhung Fault khong co trong danh sach luu trong ngay
            PupilFaultModel itmCheck = null;
            foreach (PupilFault inday in lstInDay)
            {
                itmCheck = lstFault.Where(a => a.PupilFaultID.HasValue && a.PupilFaultID.Value == inday.PupilFaultID).FirstOrDefault();
                if (itmCheck == null)
                {
                    //Xoa neu so lan vi pham = 0
                    PupilFaultBusiness.Delete(inday.PupilFaultID);
                }
            }
            //Save Fault
            List<FaultCriteria> lstFaultItem = FaultCriteriaBusiness.All.Where(a => a.SchoolID == _globalInfo.SchoolID && a.IsActive == true).ToList();
            foreach (PupilFaultModel item in lstFault)
            {
                isNew = true;
                //Update
                faultItem = lstFaultItem.Where(a => a.FaultCriteriaID == item.FaultCriteriaID).FirstOrDefault();
                if (faultItem == null) continue;
                if (item.PupilFaultID.HasValue && item.PupilFaultID > 0)
                {
                    indayItem = lstInDay.Where(a => a.PupilFaultID == item.PupilFaultID).FirstOrDefault();
                    if (indayItem != null)
                    {
                        isNew = false;
                        if (item.NumberOfFault <= 0)
                        {
                            coutSave++;
                            //Xoa neu so lan vi pham = 0
                            PupilFaultBusiness.Delete(item.PupilFaultID.Value);
                        }
                        else
                        {
                            coutSave++;
                            //Cap nhat lai vi pham
                            indayItem.NumberOfFault = (byte)item.NumberOfFault;
                            indayItem.FaultID = (short)item.FaultCriteriaID;
                            indayItem.IsSMS = false;
                            indayItem.TotalPenalizedMark = item.NumberOfFault * ((faultItem.PenalizedMark.HasValue) ? faultItem.PenalizedMark.Value : 0);
                            PupilFaultBusiness.Update(indayItem);
                        }
                    }
                }
                //Add new
                if (isNew)
                {
                    indayItem = new PupilFault();
                    indayItem.PupilID = pupilID;
                    indayItem.ClassID = classID;
                    indayItem.FaultID = faultItem.FaultCriteriaID;
                    indayItem.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    indayItem.EducationLevelID = pupilProfile.EducationLevelID;
                    indayItem.ViolatedDate = curDate.Value;
                    indayItem.NumberOfFault = (byte)item.NumberOfFault;
                    indayItem.TotalPenalizedMark = item.NumberOfFault * ((faultItem.PenalizedMark.HasValue) ? faultItem.PenalizedMark.Value : 0);
                    indayItem.SchoolID = _globalInfo.SchoolID.Value;
                    indayItem.IsSMS = false;
                    PupilFaultBusiness.Insert(indayItem);
                    coutSave++;
                }
            }
            if (coutSave > 0)
            {
                PupilFaultBusiness.Save();
                SetViewDataActionAudit(string.Empty, string.Empty, pupilID.ToString(), pupilProfile.FullName);
            }
            return Json(new JsonMessage(Res.Get("Fault_Edit_Success")));
        }
        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult LoadEditFaultClass(int? EducationLevelID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["EducationLevelID"] = (EducationLevelID.HasValue) ? EducationLevelID.Value : 0;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                //Do nothing more
            }
            else
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = 5; //GVCN + GT
            }
            var lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).OrderBy(a => a.EducationLevelID).OrderBy(a => a.DisplayName).ToList();
            if (lstClass == null) lstClass = new List<ClassProfile>();
            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }
        #region Private Methods
        /// <summary>
        /// Check role of current user by classiD
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private RoleOfClass CheckRoleOfClass(int id)
        {
            if (ClassProfileBusiness.CheckRoleOfClass(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value, id, RoleOfClass.HeadTeacher))
            {
                //QUyen GVCN
                return RoleOfClass.HeadTeacher;
            }
            else if (ClassProfileBusiness.CheckRoleOfClass(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value, id, RoleOfClass.HeadTeacherSub))
            {
                //QUyen GV Day Thay
                return RoleOfClass.HeadTeacherSub;
            }
            else if (ClassProfileBusiness.CheckRoleOfClass(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value, id, RoleOfClass.Overseeing))
            {
                //QUyen Giamthi
                return RoleOfClass.Overseeing;
            }
            return RoleOfClass.None;
        }
        /// <summary>
        /// Load View for Index
        /// </summary>
        /// <param name="role"></param>
        /// <param name="currentClassID"></param>
        /// <returns></returns>
        private ActionResult LoadViewIndex(int currentClassID, int educationLevelID, int pupilID, DateTime date)
        {
            List<ClassProfile> lstClass = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            if (educationLevelID != 0)
            {
                dic["EducationLevelID"] = educationLevelID;
            }
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
            {
                //Do nothing more
            }
            else
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = 5; //GVCN + GT
            }
            lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).OrderBy(a => a.EducationLevelID).OrderBy(a => a.DisplayName).ToList();
            SearchFaultModel mdl = new SearchFaultModel();
            ClassProfile cls = lstClass.Where(a => a.ClassProfileID == currentClassID).FirstOrDefault();
            if (lstClass == null || lstClass.Count == 0)
            {
                //Redirect or show message box to notice user have no permission.
                return RedirectToAction("Index", "PupilFault");
            }
            else if (currentClassID == 0 || cls == null)
            {
                return LoadViewIndex(lstClass[0].ClassProfileID, lstClass[0].EducationLevelID, pupilID, date);
            }
            if (cls != null)
            {
                mdl.EducationLevelID = cls.EducationLevelID;
            }
            mdl.CurrentClassID = currentClassID;
            mdl.SelectedClassID = currentClassID;
            mdl.ListClass = lstClass.Where(a => a.EducationLevelID == mdl.EducationLevelID).ToList();
            LoadData(currentClassID, pupilID, date, currentClassID, true);
            return View("_Edit_Index", mdl);
        }
        /// <summary>
        /// Make data by class and pupilfile
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="pupilFileID"></param>
        /// <param name="faultDate"></param>
        private void LoadData(int classID, int selectedPupilID, DateTime? faultDate, int currentClassID, bool loadPupil)
        {
            if (faultDate == null) faultDate = DateTime.Now;
            faultDate = new DateTime(faultDate.Value.Year, faultDate.Value.Month, faultDate.Value.Day, 0, 0, 0);
            DateTime toDate = new DateTime(faultDate.Value.Year, faultDate.Value.Month, faultDate.Value.Day, 23, 59, 59);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cls = ClassProfileBusiness.Find(classID);
            if (cls == null)
            {
                //Lớp ko hợp lệ.
                return;
            }
            ViewData[PupilFaultConstants.CLASS_NAME] = cls.DisplayName;
            //Load danh sach hoc sinh  
            if (loadPupil)
            {
                dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["CurrentClassID"] = classID;
                var query = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
                ListPupilModel itmModel = null;
                int stt = 1;
                if (query != null)
                {
                    List<ListPupilModel> lstPupil = new List<ListPupilModel>();
                    var lstPupilProfiles = query.ToList().OrderBy(p=>p.OrderInClass??0).ThenBy(p=> SMAS.Business.Common.Utils.SortABC(p.Name+" " + p.FullName)).ToList();
                    foreach (PupilProfileBO item in lstPupilProfiles)
                    {
                        itmModel = new ListPupilModel();
                        itmModel.ClassID = classID;
                        itmModel.ClassName = cls.DisplayName;
                        itmModel.ImageData = item.Image;
                        itmModel.OrderNumber = stt;
                        itmModel.PupilID = item.PupilProfileID;
                        itmModel.PupilName = item.FullName;
                        itmModel.Status = item.ProfileStatus;
                        lstPupil.Add(itmModel);
                        stt++;
                        if (item.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING && selectedPupilID == 0)
                        {
                            selectedPupilID = item.PupilProfileID;
                        }
                    }
                    ViewData["lstPupil"] = lstPupil;
                }
            }
            ViewData["classSelectedID"] = classID;
            ViewData["pupilSelectedID"] = selectedPupilID;
            ViewData["currentClassID"] = currentClassID;
            ViewData["faultDate"] = faultDate;
            // Lay thong tin thoi gian bi khoa diem danh
            LockTraining lockTraining = LockTrainingBusiness.SearchByClass(classID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault();
            ViewData["isLock"] = false;
            if (lockTraining != null)
            {
                DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value).FirstSemesterStartDate.Value;
                DateTime toLockDate = lockTraining.ToDate.AddDays(1);
                ViewData["lockTitle"] = string.Format(Res.Get("LockTraining_Label_ViolationTitle"), fromLockDate.ToString("dd/MM/yyyy"), lockTraining.ToDate.ToString("dd/MM/yyyy"));
                if (!_globalInfo.IsAdmin)
                {
                    if (fromLockDate <= faultDate && toLockDate > faultDate)
                    {
                        ViewData["isLock"] = true;
                    }
                }
            }
            //Load Danh sach Vi Pham cua hoc sinh theo ngay hien tai
            //Neu hoc sinh ko co thi van load thong tin vi pham
            List<PupilFaultModel> lstPupilFaultModel = new List<PupilFaultModel>();
            PupilProfile pf = PupilProfileBusiness.Find(selectedPupilID);
            //Lay tat cac Fault cua truong
            List<FaultCriteria> lstFault = FaultCriteriaBusiness.All.Where(a => a.IsActive == true && a.SchoolID == _globalInfo.SchoolID.Value).ToList();
            IEnumerable<FaultCriteria> lstFaultBelongToGroup = lstFault.Where(p => p.FaultGroup != null); // Danh sach cac loi thuoc mot nhom loi nao do
            IEnumerable<FaultCriteria> lstFaultDoNotBelongToGroup = lstFault.Where(p => p.FaultGroup == null); // Danh sach cac loi khong thuoc nhom loi nao ca
            try
            {
                lstFault = lstFaultBelongToGroup.OrderBy(o => o.FaultGroup.Resolution).ThenBy(o => o.Resolution)
                            .Union(lstFaultDoNotBelongToGroup.OrderBy(o=>o.Resolution))
                            .ToList();
            }
            catch (Exception ex)
            {
                string ParamList = string.Format("classID={0},selectedPupilID={1}, faultDate={2},currentClassID={3},loadPupil={4}", classID, selectedPupilID, faultDate, currentClassID, loadPupil);
                LogExtensions.ErrorExt(logger, DateTime.Now, "LoadData", ParamList, ex);
            }
            //Lay Fault cua hoc sinh trong ngay
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["PupilID"] = selectedPupilID;
            dic["ClassID"] = classID;
            dic["ViolatedDate"] = faultDate;
            dic["CurrentAcademicYearID"] = _globalInfo.AcademicYearID.Value;
            List<PupilFault> queryFault = PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            //Tao list Model
            if (lstFault != null)
            {
                PupilFaultModel newFault = null;
                int? numberOfFault = 0;
                foreach (FaultCriteria item in lstFault)
                {
                    newFault = new PupilFaultModel();
                    newFault.CanEdit = string.IsNullOrWhiteSpace(item.Absence);
                    newFault.ClassID = classID;
                    newFault.ClassName = cls.DisplayName;
                    newFault.FaultName = item.Resolution;
                    numberOfFault = queryFault.Where(a => a.FaultID == item.FaultCriteriaID).Sum(a => a.NumberOfFault);
                    if (numberOfFault.HasValue) newFault.NumberOfFault = numberOfFault.Value;
                    newFault.PupilID = selectedPupilID;
                    newFault.PupilName = (pf != null) ? pf.FullName : string.Empty;
                    newFault.TotalPenalizedMark = queryFault.Where(a => a.FaultID == item.FaultCriteriaID).Sum(a => a.TotalPenalizedMark);
                    newFault.ViolatedDate = faultDate.Value;
                    newFault.FaultCriteriaID = item.FaultCriteriaID;
                    var tmp = queryFault.Where(a => a.FaultID == item.FaultCriteriaID).FirstOrDefault();
                    newFault.PupilFaultID = (tmp != null) ? tmp.PupilFaultID : 0;
                    newFault.PenalizedMark = item.PenalizedMark;
                    newFault.Note = tmp != null ? tmp.Note : "";
                    lstPupilFaultModel.Add(newFault);
                }
            }
            ViewData["ViolatedDate"] = faultDate;
            ViewData["lstPupilFault"] = lstPupilFaultModel;
        }
        #endregion

        //phuongh1
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Save(int PupilID, DateTime Date, string DataList)
        {
            GlobalInfo gi = new GlobalInfo();
            List<PupilFault> listData = new List<PupilFault>();
            string[] arrData = DataList.Split(',');
            // Kiem tra neu tk giao vien va bi khoa thi bao loi
            LockTraining lockTraining = !gi.IsAdmin ? LockTrainingBusiness.SearchByClass(PupilProfileBusiness.Find(PupilID).CurrentClassID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault() : null;
            if (lockTraining != null)
            {
                DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYearBusiness.Find(_globalInfo.AcademicYearID).FirstSemesterStartDate.Value;
                if (fromLockDate <= Date && lockTraining.ToDate.AddDays(1) > Date)
                {
                    string lockTitle = string.Format(Res.Get("LockTraining_Label_ViolationTitle"), fromLockDate.ToString("dd/MM/yyyy"), lockTraining.ToDate.ToString("dd/MM/yyyy"));
                    throw new BusinessException(lockTitle);
                }
            }
            foreach (string data in arrData)
            {
                if (!string.IsNullOrWhiteSpace(data))
                {
                    string[] arrEle = data.Split('_');
                    PupilFault pa = new PupilFault();
                    pa.FaultID = Int32.Parse(arrEle[0]);
                    pa.NumberOfFault = Int32.Parse(arrEle[1]);
                    pa.Note = arrEle[2];
                    listData.Add(pa);
                }
            }
            PupilFaultBusiness.UpdateForPupil(gi.UserAccountID, gi.IsAdmin, gi.SchoolID.GetValueOrDefault(),
                gi.AcademicYearID.GetValueOrDefault(), PupilID, Date, listData);
            return Json(new JsonMessage(Res.Get("PupilFault_Label_Success")));
        }

        
        public FileResult ExportExcel(int? FaultGroupID, int? EducationLevelID, int? ClassID, string PupilCode, string PupilName, string FromDate, string ToDate, int? SemesterID)
        {
            

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            SearchInfo["FaultGroupID"] = FaultGroupID;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["PupilCode"] = PupilCode;
            SearchInfo["FullName"] = PupilName;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            if (SemesterID.HasValue)
            {
                AcademicYear AcademicYear = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                SearchInfo["FromViolatedDate"] = SemesterID.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? AcademicYear.SecondSemesterStartDate : AcademicYear.FirstSemesterStartDate;
                SearchInfo["ToViolatedDate"] = SemesterID.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? AcademicYear.FirstSemesterEndDate : AcademicYear.SecondSemesterEndDate;
            }
            else
            {
                DateTime? fDate = null;
                DateTime? tDate = null;
                try
                {
                    fDate = Convert.ToDateTime(FromDate);
                }
                catch
                {
                }

                try
                {
                    tDate = Convert.ToDateTime(ToDate);
                }
                catch
                {
                }
                SearchInfo["FromViolatedDate"] = fDate;
                SearchInfo["ToViolatedDate"] = tDate;
            }
            #region Do du lieu vao file
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/HS/" + "HS_BaoCaoHSViPham.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thong tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            sheet.SetCellValue("A2", school.SchoolName.ToUpper());
            string date = "Từ ngày " + FromDate + " Đến ngày " + ToDate;
            sheet.SetCellValue("A6", date);
            sheet.SetRowHeight(5, 0);
            //fill thong tin tung re cord

			List<int> lstClassID1 = new List<int>();
            //Neu la quan tri truong/BGH/xem thong tin toan truong
            if (_globalInfo.IsViewAll || _globalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager)
            {
                lstClassID1 = GetClassByEducationLevel(EducationLevelID.HasValue ? EducationLevelID.Value : 0, _globalInfo.AppliedLevel.Value).Select(s => s.ClassProfileID).ToList();
            }
            else //Danh sach lop giao vien phu trach. Fix bug 14964
            {
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass["EducationLevelID"] = EducationLevelID;
                dicClass["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicClass["AppliedLevel"] = _globalInfo.AppliedLevel;
                dicClass["UserAccountID"] = _globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING;
                lstClassID1 = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName).Select(c => c.ClassProfileID).ToList();
                //ListClassID = ClassProfileBusiness.SearchByTeacherPermission(SearchInfo).Select(o => o.ClassID).ToList();
            }
            SearchInfo["ListClassID"] = lstClassID1;
            List<PupilFaultViewModel> lst = new List<PupilFaultViewModel>();

            lst = this._Search(SearchInfo).ToList()
                               .OrderBy(o => o.EducationLevelID)
                               .ThenBy(o => o.ClassOrderNumber ?? 0).ThenBy(o => o.ClassName)
                               .ThenBy(o => o.OrderOfPupilInClass ?? 0).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.PupilName))
                               .ThenBy(o=>o.ViolatedDate)
                               .ToList();


            //List<int> lstClassID1 = SMAS.Business.Common.Utils.GetIntList(SearchInfo, "ListClassID");
            
            PupilFaultViewModel objViewModel = null;
            int startRow = 9;
            for (int i = 0; i < lst.Count; i++)
            {
                objViewModel = lst[i];
                //STT
                sheet.SetCellValue(startRow, 1, i + 1);
                //LOP
                sheet.SetCellValue(startRow, 2, objViewModel.ClassName);
                //Ho Ten
                sheet.SetCellValue(startRow, 3, objViewModel.PupilName);
                //Ngay vi pham
                sheet.SetCellValue(startRow, 4, objViewModel.ViolatedDate.ToString("dd/MM/yyyy"));
                sheet.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                //Thong tin vi pham
                sheet.SetCellValue(startRow, 5, objViewModel.FaultName);
                //Tong diem tru
                sheet.SetCellValue(startRow, 6, objViewModel.TotalPenalizedMark);
                startRow++;
            }
            //ke khung
            sheet.GetRange(9, 1, lst.Count + 8, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.Name = "BaoCaoHSViPham";
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "HS_BaoCaoHSViPham.xls";
            #endregion
            return result;
        }
    }
}
