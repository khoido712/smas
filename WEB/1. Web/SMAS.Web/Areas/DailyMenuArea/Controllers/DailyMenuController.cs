﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author namdv3, email: namdv3@viettel.com.vn
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DailyMenuArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Web.Areas.DailyMenuArea.Controllers
{
    public class DailyMenuController : BaseController
    {
        private readonly IDailyMenuBusiness DailyMenuBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IDailyDishCostBusiness DailyDishCostBusiness;
        private readonly IMealCatBusiness MealCatBusiness;
        private readonly IFoodCatBusiness FoodCatBusiness;
        private readonly IOtherServiceBusiness OtherServiceBusiness;
        private readonly INutritionalNormBusiness NutritionalNormBusiness;
        private readonly IDailyMenuFoodBusiness DailyMenuFoodBusiness;
        private readonly IDishCatBusiness DishCatBusiness;
        private readonly IDishDetailBusiness DishDetailBusiness;
        private readonly IDailyMenuDetailBusiness DailyMenuDetailBusiness;
        private readonly ITypeOfFoodBusiness TypeOfFoodBusiness;
        private readonly IDishInspectionBusiness DishInspectionBusiness;
        private readonly IWeeklyMenuDetailBusiness WeeklyMenuDetailBusiness;
        private readonly IWeeklyMenuBusiness WeeklyMenuBusiness;

        private GlobalInfo global = new GlobalInfo();
        public DailyMenuController(IDailyMenuBusiness dailymenuBusiness,IPupilOfClassBusiness pupilOfClassBusiness, IEatingGroupBusiness eatinggroupbusiness, IDailyDishCostBusiness dailydishcostbusiness,
            IMealCatBusiness mealcatbusiness, IFoodCatBusiness foodcatbusiness, IOtherServiceBusiness otherservicebusiness, INutritionalNormBusiness nutritionalnormbusiness,
            IDailyMenuFoodBusiness dailymenufoodbusiness, IDishCatBusiness dishcatbusiness, IDishDetailBusiness dishdetailbusiness, IDailyMenuDetailBusiness dailymenudetailbusiness,
            ITypeOfFoodBusiness typeOfFoodBusiness, IDishInspectionBusiness DishInspectionBusiness, IWeeklyMenuDetailBusiness WeeklyMenuDetailBusiness, IWeeklyMenuBusiness WeeklyMenuBusiness)
        {
            this.DailyMenuBusiness = dailymenuBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EatingGroupBusiness = eatinggroupbusiness;
            this.DailyDishCostBusiness = dailydishcostbusiness;
            this.MealCatBusiness = mealcatbusiness;
            this.FoodCatBusiness = foodcatbusiness;
            this.OtherServiceBusiness = otherservicebusiness;
            this.NutritionalNormBusiness = nutritionalnormbusiness;
            this.DailyMenuFoodBusiness = dailymenufoodbusiness;
            this.DishCatBusiness = dishcatbusiness;
            this.DishDetailBusiness = dishdetailbusiness;
            this.DailyMenuDetailBusiness = dailymenudetailbusiness;
            this.TypeOfFoodBusiness = typeOfFoodBusiness;
            this.DishInspectionBusiness = DishInspectionBusiness;
            this.WeeklyMenuDetailBusiness = WeeklyMenuDetailBusiness;
            this.WeeklyMenuBusiness = WeeklyMenuBusiness;
        }

        //
        // GET: /DailyMenu/

        private void GetListEatGrp(IDictionary<string, object> dic)
        {
            List<int?> lstEatgrp = new List<int?>();
            List<EatingGroupBO> lst_eatinggrp = EatingGroupBusiness.SearchBySchool(global.SchoolID.Value, dic).Distinct().ToList();
            List<EatingGroupBO> listEatGrp = new List<EatingGroupBO>();
            foreach (var eg in lst_eatinggrp)
            {
                if (!lstEatgrp.Contains(eg.EatingGroupID))
                {
                    lstEatgrp.Add(eg.EatingGroupID);
                    listEatGrp.Add(eg);
                }
            }
            ViewData[DailyMenuConstants.LIST_EATINGGROUP] = new SelectList(listEatGrp, "EatingGroupID", "EatingGroupName");
        }

        public ActionResult Index()
        {
            IDictionary<string, object> EatGrp_dic = new Dictionary<string, object>();
            EatGrp_dic["AcademicYearID"] = global.AcademicYearID;
            GetListEatGrp(EatGrp_dic);
            List<ComboObject> lst_menutype = CommonList.MenuType();
            ViewData[DailyMenuConstants.LIST_MENUTYPE] = new SelectList(lst_menutype, "key", "value");
            SetData(string.Empty, null, null);
            return View();
        }

        //
        // GET: /DailyMenu/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            SetData(frm.DailyMenuCode, frm.EatingGroupID, frm.MenuType);
            return PartialView("_List");
        }

        private void SetData(string DailyMenuCode, int? EatingGroupID, int? MenuType)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["DailyMenuCode"] = DailyMenuCode;
            SearchInfo["EatingGroupID"] = EatingGroupID;
            SearchInfo["MenuType"] = MenuType;

            IEnumerable<DailyMenuViewModel> lst = this._Search(SearchInfo);
            ViewData[DailyMenuConstants.LIST_DAILYMENU] = lst;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="option">1:edit by daily, 2: edit by food, null or empty: detail</param>
        /// <returns></returns>
        

        [ValidateAntiForgeryToken]
        public ActionResult Create(int? id, string type, string option)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = global.AcademicYearID;
            var lst_menutype = CommonList.MenuType();
            GetListEatGrp(dic);
            ViewData[DailyMenuConstants.LIST_MENUTYPE] = new SelectList(lst_menutype, "key", "value");

            IDictionary<string, object> dic_dailydishcost = new Dictionary<string, object>();

            IQueryable<DailyDishCost> dailyDishCost = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>());
            dailyDishCost = dailyDishCost.Where(x => x.EffectDate < DateTime.Now);
            if (dailyDishCost != null && dailyDishCost.Count() > 0)
            {
                DateTime max_effectdate = dailyDishCost.Select(o => o.EffectDate).Max();
                dic_dailydishcost["EffectDate"] = max_effectdate;
            }
            var daylidishcost = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, dic_dailydishcost).FirstOrDefault();
            ViewData[DailyMenuConstants.TXTDAILYCOSTOFCHILDREN] = daylidishcost != null ? daylidishcost.Cost : 0;
            var lst_mealcat = MealCatBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).Select(o => new DailyInfo
            {
                MealID = o.MealCatID,
                DishName = o.MealName
            }).ToList();
            ViewData[DailyMenuConstants.LIST_MEALCAT] = lst_mealcat;
            if (type != DailyMenuConstants.TYPE_DETAIL)
            {
                List<Key4Grid> ListKey4Grid = new List<Key4Grid>();
                for (var i = 1; i <= 3; i++)
                {
                    Key4Grid Key4Grid = new Key4Grid();
                    Key4Grid.ID = i;
                    ListKey4Grid.Add(Key4Grid);
                }
                ViewData[DailyMenuConstants.LIST_KEY_4GRID] = ListKey4Grid;
            }
            if (type == DailyMenuConstants.TYPE_DETAIL)
            {
                DailyMenu dailymenu = DailyMenuBusiness.Find(id.Value);
                ViewData[DailyMenuConstants.DAILYMENU] = dailymenu;
                /*
                 Thực hiện tìm kiếm trong bảng DailyMenuDetailBusiness.Search(Dictionary) với Dictionary[“MealID”] = MealID. Kết quả thu được dailyMenuDetail. 
                 */
                IDictionary<string, object> dic_dmd = new Dictionary<string, object>();
                dic_dmd["DailyMenuID"] = dailymenu.DailyMenuID;
                List<DailyMenuDetail> lstDailyMenuDetail = DailyMenuDetailBusiness.Search(dic_dmd).OrderBy(o => o.DailyMenuDetailID).ToList();
                List<DailyMenuDetailBO> lstDailyMenuDetailBO = new List<DailyMenuDetailBO>();
                List<int> listmax = new List<int>();
                foreach (var i in lst_mealcat)
                {
                    listmax.Add(lstDailyMenuDetail.Where(o => o.MealID == i.MealID).Count());
                }
                var _max = listmax.OrderByDescending(o => o).FirstOrDefault();
                lstDailyMenuDetail.Select(o => o.MealID).Distinct().Count();
                if (lstDailyMenuDetail != null && lstDailyMenuDetail.Count > 0)
                {
                    var i = lst_mealcat.Count;
                    List<int> itemcheck = new List<int>();
                    for (var j = 0; j < _max; j++)
                    {
                        DailyMenuDetailBO DailyMenuDetailBO = new DailyMenuDetailBO();
                        DailyMenuDetailBO.DishNames = new string[i];
                        DailyMenuDetailBO.MealIDs = new int?[i];
                        DailyMenuDetailBO.DishIDS = new int?[i];
                        foreach (var item in lstDailyMenuDetail)
                        {
                            for (var meal = 0; meal < i; meal++)
                            {
                                if (item.MealID == lst_mealcat[meal].MealID)
                                {
                                    if (!itemcheck.Contains(item.DailyMenuDetailID))
                                    {
                                        if (!DailyMenuDetailBO.MealIDs.Contains(item.MealID))
                                        {
                                            DailyMenuDetailBO.MealIDs[meal] = item.MealID;
                                            DailyMenuDetailBO.DailyMenuID = item.DailyMenuID;
                                            DailyMenuDetailBO.DailyMenuDetailID = item.DailyMenuDetailID;
                                            DailyMenuDetailBO.DishNames[meal] = !string.IsNullOrWhiteSpace(item.DishName) ? item.DishName : DishCatBusiness.Find(item.DishID).DishName;
                                            DailyMenuDetailBO.DishIDS[meal] = item.DishID;
                                            itemcheck.Add(item.DailyMenuDetailID);
                                        }
                                    }
                                }
                            }
                        }
                        lstDailyMenuDetailBO.Add(DailyMenuDetailBO);
                    }
                }
                ViewData[DailyMenuConstants.LIST_DAILYMENUDETAIL] = lstDailyMenuDetailBO;
                ViewData[DailyMenuConstants.DAILYMENUID] = dailymenu.DailyMenuID;
                ViewData[DailyMenuConstants.EATINGGROUPID] = dailymenu.EatingGroupID;
                ViewData[DailyMenuConstants.OPTION] = string.IsNullOrWhiteSpace(option) ? "0" : option;
            }
            var lstDishCat = DishCatBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).ToList();
            lstDishCat = lstDishCat.OrderBy(x => x.DishName).ToList();
            ViewData[DailyMenuConstants.LIST_DISHCAT] = new SelectList(lstDishCat, "DishCatID", "DishName");
            ViewData[DailyMenuConstants.ADD_BY] = type;
            var message = string.Empty;
            ViewData["RescourceLabel"] = "DailyMenu_Label_MenuAdd";
            if (string.IsNullOrWhiteSpace(option))
            {
                message = Res.Get("Common_Label_AddNewMessage");
                ViewData["RescourceLabel"] = "DailyMenu_Label_MenuAdd";
            }
            else
            {
                message = Res.Get("Common_Label_UpdateSuccessMessage");
                ViewData["RescourceLabel"] = "DailyMenu_Label_MenuEdit";
            }
            return View();
        }


        
        public ActionResult Edit(int? id, string type, string option)
        {

            CheckPermissionForAction(id, "DailyMenu");
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = global.AcademicYearID;
            var lst_menutype = CommonList.MenuType();
            GetListEatGrp(dic);
            ViewData[DailyMenuConstants.LIST_MENUTYPE] = new SelectList(lst_menutype, "key", "value");

            IDictionary<string, object> dic_dailydishcost = new Dictionary<string, object>();

            IQueryable<DailyDishCost> dailyDishCost = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>());
            dailyDishCost = dailyDishCost.Where(x => x.EffectDate < DateTime.Now);
            if (dailyDishCost != null && dailyDishCost.Count() > 0)
            {
                DateTime max_effectdate = dailyDishCost.Select(o => o.EffectDate).Max();
                dic_dailydishcost["EffectDate"] = max_effectdate;
            }
            var daylidishcost = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, dic_dailydishcost).FirstOrDefault();
            ViewData[DailyMenuConstants.TXTDAILYCOSTOFCHILDREN] = daylidishcost != null ? daylidishcost.Cost : 0;
            var lst_mealcat = MealCatBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).Select(o => new DailyInfo
            {
                MealID = o.MealCatID,
                DishName = o.MealName
            }).ToList();
            ViewData[DailyMenuConstants.LIST_MEALCAT] = lst_mealcat;
            if (type != DailyMenuConstants.TYPE_DETAIL)
            {
                List<Key4Grid> ListKey4Grid = new List<Key4Grid>();
                for (var i = 1; i <= 3; i++)
                {
                    Key4Grid Key4Grid = new Key4Grid();
                    Key4Grid.ID = i;
                    ListKey4Grid.Add(Key4Grid);
                }
                ViewData[DailyMenuConstants.LIST_KEY_4GRID] = ListKey4Grid;
            }
            if (type == DailyMenuConstants.TYPE_DETAIL)
            {
                DailyMenu dailymenu = DailyMenuBusiness.Find(id.Value);
                ViewData[DailyMenuConstants.DAILYMENU] = dailymenu;
                /*
                 Thực hiện tìm kiếm trong bảng DailyMenuDetailBusiness.Search(Dictionary) với Dictionary[“MealID”] = MealID. Kết quả thu được dailyMenuDetail. 
                 */
                IDictionary<string, object> dic_dmd = new Dictionary<string, object>();
                dic_dmd["DailyMenuID"] = dailymenu.DailyMenuID;
                List<DailyMenuDetail> lstDailyMenuDetail = DailyMenuDetailBusiness.Search(dic_dmd).OrderBy(o => o.DailyMenuDetailID).ToList();
                List<DailyMenuDetailBO> lstDailyMenuDetailBO = new List<DailyMenuDetailBO>();
                List<int> listmax = new List<int>();
                foreach (var i in lst_mealcat)
                {
                    listmax.Add(lstDailyMenuDetail.Where(o => o.MealID == i.MealID).Count());
                }
                var _max = listmax.OrderByDescending(o => o).FirstOrDefault();
                lstDailyMenuDetail.Select(o => o.MealID).Distinct().Count();
                if (lstDailyMenuDetail != null && lstDailyMenuDetail.Count > 0)
                {
                    var i = lst_mealcat.Count;
                    List<int> itemcheck = new List<int>();
                    for (var j = 0; j < _max; j++)
                    {
                        DailyMenuDetailBO DailyMenuDetailBO = new DailyMenuDetailBO();
                        DailyMenuDetailBO.DishNames = new string[i];
                        DailyMenuDetailBO.MealIDs = new int?[i];
                        DailyMenuDetailBO.DishIDS = new int?[i];
                        foreach (var item in lstDailyMenuDetail)
                        {
                            for (var meal = 0; meal < i; meal++)
                            {
                                if (item.MealID == lst_mealcat[meal].MealID)
                                {
                                    if (!itemcheck.Contains(item.DailyMenuDetailID))
                                    {
                                        if (!DailyMenuDetailBO.MealIDs.Contains(item.MealID))
                                        {
                                            DailyMenuDetailBO.MealIDs[meal] = item.MealID;
                                            DailyMenuDetailBO.DailyMenuID = item.DailyMenuID;
                                            DailyMenuDetailBO.DailyMenuDetailID = item.DailyMenuDetailID;
                                            DailyMenuDetailBO.DishNames[meal] = !string.IsNullOrWhiteSpace(item.DishName) ? item.DishName : DishCatBusiness.Find(item.DishID).DishName;
                                            DailyMenuDetailBO.DishIDS[meal] = item.DishID;
                                            itemcheck.Add(item.DailyMenuDetailID);
                                        }
                                    }
                                }
                            }
                        }
                        lstDailyMenuDetailBO.Add(DailyMenuDetailBO);
                    }
                }
                ViewData[DailyMenuConstants.LIST_DAILYMENUDETAIL] = lstDailyMenuDetailBO;
                ViewData[DailyMenuConstants.DAILYMENUID] = dailymenu.DailyMenuID;
                ViewData[DailyMenuConstants.EATINGGROUPID] = dailymenu.EatingGroupID;
                ViewData[DailyMenuConstants.OPTION] = string.IsNullOrWhiteSpace(option) ? "0" : option;
            }
            var lstDishCat = DishCatBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).ToList();
            ViewData[DailyMenuConstants.LIST_DISHCAT] = new SelectList(lstDishCat, "DishCatID", "DishName");
            ViewData[DailyMenuConstants.ADD_BY] = type;
            var message = string.Empty;
            ViewData["RescourceLabel"] = "DailyMenu_Label_MenuAdd";
            if (string.IsNullOrWhiteSpace(option))
            {
                message = Res.Get("Common_Label_AddNewMessage");
                ViewData["RescourceLabel"] = "DailyMenu_Label_MenuAdd";
            }
            else
            {
                message = Res.Get("Common_Label_UpdateSuccessMessage");
                ViewData["RescourceLabel"] = "DailyMenu_Label_MenuEdit";
            }
            return View("Create");
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadNumberOfChildren(int? eatingGroupID)
        {
            if (eatingGroupID.HasValue)
            {
                EatingGroup eatinggrp = EatingGroupBusiness.Find(eatingGroupID);
                List<int> lstClassID = new List<int>();
                if (eatinggrp != null)
                {
                    var listClass = eatinggrp.EatingGroupClasses;
                    if (listClass.Count > 0)
                    {
                        foreach (var cl in listClass)
                        {
                            lstClassID.Add(cl.ClassID);
                        }
                    }
                }
                List<PupilOfClass> lstPupil = PupilOfClassBusiness.All.Where(x => lstClassID.Contains(x.ClassID)).Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                int totalPupil = lstPupil.Count;
                return Json(totalPupil);
            }
            else
            {
                return Json(0);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(string type, FormCollection col)
        {
            string TypeMealCat = "1";
            string TypeDishCat = "2";
            var DailyMenuCode = col["txtDailyMenuCode"];
            var NumberOfChildren = col["txtNumberOfChildren"];
            var MenuType = col["cboMenuType"];
            var EatingGroupID = col["cboEatingGroup"];
            var DailyCostOfChildren = col["txtDailyCostOfChildren"];
            var option = col["editoption"];
            var id = !string.IsNullOrWhiteSpace(col["dailymenuid"]) ? int.Parse(col["dailymenuid"]) : 0;

            DailyMenu dailymenu = new DailyMenu();
            if (!string.IsNullOrWhiteSpace(option))
            {
                dailymenu = DailyMenuBusiness.Find(id);
            }
            dailymenu.DailyMenuCode = DailyMenuCode.Trim();
            dailymenu.NumberOfChildren = int.Parse(NumberOfChildren.Trim());
            dailymenu.MenuType = int.Parse(MenuType.Trim());
            dailymenu.GetFromDishCat = type == DailyMenuConstants.ADD_BYDAILY ? false : true;
            dailymenu.EatingGroupID = int.Parse(EatingGroupID ?? "0");
            dailymenu.SchoolID = global.SchoolID.Value;
            dailymenu.AcademicYearID = global.AcademicYearID.Value;
            dailymenu.DailyCostOfChildren = int.Parse(DailyCostOfChildren.Trim());
            dailymenu.IsActive = true;
            var lst_mealcat = MealCatBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).Select(o => new DailyInfo
            {
                MealID = o.MealCatID,
                DishName = o.MealName
            });

            List<DailyMenuDetail> lstDailyMenuDetail = new List<DailyMenuDetail>();

            var lstStt = col["lstSTT"].Split(',');
            var format = "";
            if (type == DailyMenuConstants.ADD_BYDAILY)
            {
                format = "Meal_{0}_{1}";
            }
            else if (type == DailyMenuConstants.ADD_BYFOOD)
            {
                format = "Dish_{0}_{1}";
            }
            else if (type == DailyMenuConstants.TYPE_DETAIL)
            {
                if (option == TypeMealCat)
                {
                    format = "Meal_{0}_{1}";
                }
                else if (option == TypeDishCat)
                {
                    format = "Dish_{0}_{1}";
                }
            }
            var mealcatrow = "";
            foreach (var mealcat in lst_mealcat)
            {
                for (var i = 0; i < lstStt.Length; i++)
                {
                    if (int.Parse(lstStt[i]) > 0)
                    {
                        DailyMenuDetail dailymenudetail = new DailyMenuDetail();
                        mealcatrow = string.Format(format, mealcat.MealID.ToString(), lstStt[i]);
                        if (!string.IsNullOrWhiteSpace(col[mealcatrow]))
                        {
                            if (type == DailyMenuConstants.ADD_BYDAILY)
                            {
                                dailymenudetail.DishName = col[mealcatrow].Trim();
                            }
                            else if (type == DailyMenuConstants.ADD_BYFOOD)
                            {
                                dailymenudetail.DishID = int.Parse(col[mealcatrow]);
                            }
                            else if (type == DailyMenuConstants.TYPE_DETAIL)
                            {
                                if (option == TypeMealCat)
                                {
                                    dailymenudetail.DishName = col[mealcatrow].Trim();
                                }
                                else
                                {
                                    dailymenudetail.DishID = int.Parse(col[mealcatrow]);
                                }
                            }
                            dailymenudetail.MealID = mealcat.MealID;
                            lstDailyMenuDetail.Add(dailymenudetail);
                        }
                    }
                }
            }
            var message = string.Empty;
            if (string.IsNullOrWhiteSpace(option))
            {
                if (_globalInfo.IsCurrentYear)
                {
                    this.DailyMenuBusiness.Insert(dailymenu, lstDailyMenuDetail, _globalInfo.SchoolID, _globalInfo.AcademicYearID);
                    SetViewDataActionAudit("", "", "", "Create");
                }
            }
            else
            {
                if (_globalInfo.IsCurrentYear)
                {
                    this.DailyMenuBusiness.Update(dailymenu, lstDailyMenuDetail, _globalInfo.SchoolID, _globalInfo.AcademicYearID);
                    SetViewDataActionAudit("", "", "", "Edit");
                }
            }
            if (_globalInfo.IsCurrentYear)
            {
                this.DailyMenuBusiness.Save();
                if (string.IsNullOrWhiteSpace(option))
                {
                    message = Res.Get("Common_Label_AddNewMessage");
                }
                else
                {
                    message = Res.Get("Common_Label_UpdateSuccessMessage");
                }
                return Json(new JsonMessage(string.Format("{0};{1}", dailymenu.DailyMenuID.ToString(), message)));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ClassAssigment_Error_IsCurrentYearNoSave")), "erro");
            }
        }


        [ValidateAntiForgeryToken]
        public ActionResult Food(string id, string type, string option, string lstweight)
        {
            string[] lstid = id.Split('_');
            int dailymenuid = int.Parse(lstid[0]);
            int eatinggroupid = int.Parse(lstid[1]);
            DateTime datenow = DateTime.Now;
            if (dailymenuid > 0 && eatinggroupid > 0)
            {
                var listweightadd = new List<decimal>();
                if (lstweight != "empty")
                {
                    if (!string.IsNullOrWhiteSpace(lstweight))
                    {
                        string[] lstw = lstweight.Split(',');
                        foreach (var i in lstw)
                        {
                            if (!string.IsNullOrWhiteSpace(i) && decimal.Parse(i.Replace(".", ",")) >= 0)
                            {
                                listweightadd.Add(decimal.Parse(i.Replace(".", ",")));
                            }
                        }
                    }
                }
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DailyMenuID"] = dailymenuid;
                DailyMenu dailymenu = DailyMenuBusiness.Find(dailymenuid);
                ViewData[DailyMenuConstants.DAILYMENU] = dailymenu;
                var lstDailyMenuFood = DailyMenuFoodBusiness.Search(dic).ToList();
                if (lstDailyMenuFood != null && lstDailyMenuFood.Count > 0)
                {
                    int j = 0;
                    List<DailyMenuFoodBO> lstDailyMenuFoodBO = new List<DailyMenuFoodBO>();
                    foreach (var item in lstDailyMenuFood)
                    {
                        IDictionary<string, object> dic_foodcat = new Dictionary<string, object>();
                        dic_foodcat["FoodID"] = item.FoodID;
                        FoodCat FoodCat = FoodCatBusiness.Search(dic_foodcat).FirstOrDefault();
                        DailyMenuFoodBO dailymenufoodbo = new DailyMenuFoodBO();
                        if (listweightadd != null && listweightadd.Count == lstDailyMenuFood.Count && listweightadd.Count > 0)
                        {
                            item.Weight += listweightadd[j];
                        }
                        dailymenufoodbo.DailyMenuFood = item;
                        dailymenufoodbo.DiscardedRate = FoodCat.DiscardedRate;
                        dailymenufoodbo.NumberOfChildren = dailymenu.NumberOfChildren;
                        lstDailyMenuFoodBO.Add(dailymenufoodbo);
                        j++;
                    }
                    ViewData[DailyMenuConstants.LIST_KEY_4GRID] = lstDailyMenuFoodBO;
                    ViewData["Flag"] = true;
                    IDictionary<string, object> dic_dmd = new Dictionary<string, object>();
                    dic_dmd["DailyMenuID"] = dailymenu.DailyMenuID;
                    if (DailyMenuDetailBusiness.Search(dic_dmd).FirstOrDefault().DishID.HasValue)
                    {
                        var lstRateByMeal = FoodCatBusiness.CaculatorRateByMeal(dailymenuid, global.SchoolID.Value, global.AcademicYearID.Value, eatinggroupid);
                        ViewData[DailyMenuConstants.LSTRATEBYMEAL] = lstRateByMeal;
                    }
                    else
                    {
                        ViewData[DailyMenuConstants.LSTRATEBYMEAL] = new List<RateByMealBO>();
                    }
                }
                else
                {
                    //thuc pham theo thuc an
                    if (type == DailyMenuConstants.ADD_BYFOOD)
                    {
                        var lstDishDetail = DishDetailBusiness.GetListFoodOfDailyMenu(dailymenu.DailyMenuID).ToList();
                        if (lstDishDetail != null && lstDishDetail.Count > 0)
                        {
                            var j = 0;
                            List<DailyMenuFoodBO> lstDailyMenuFoodBO = new List<DailyMenuFoodBO>();
                            var dic_dmf = new Dictionary<string, object>();
                            dic_dmf["DailyMenuID"] = dailymenuid;

                            foreach (var item in lstDishDetail)
                            {
                                var dic_foodcat = new Dictionary<string, object>();
                                dic_foodcat["FoodID"] = item.FoodID;
                                var FoodCat = FoodCatBusiness.Search(dic_foodcat).FirstOrDefault();
                                DailyMenuFoodBO dailymenufoodbo = new DailyMenuFoodBO();
                                if (listweightadd != null && listweightadd.Count == lstDailyMenuFood.Count && listweightadd.Count > 0)
                                    item.WeightPerRation += listweightadd[j];
                                dic_dmf["FoodID"] = item.FoodID;
                                dailymenufoodbo.FoodCat = FoodCat;
                                dailymenufoodbo.WeightPerRation = item.WeightPerRation;
                                dailymenufoodbo.DiscardedRate = FoodCat.DiscardedRate;
                                dailymenufoodbo.NumberOfChildren = dailymenu.NumberOfChildren;
                                lstDailyMenuFoodBO.Add(dailymenufoodbo);
                                j++;
                            }
                            ViewData[DailyMenuConstants.LIST_KEY_4GRID] = lstDailyMenuFoodBO;
                        }
                        else
                        {
                            ViewData[DailyMenuConstants.LIST_KEY_4GRID] = new List<DailyMenuFoodBO>();
                        }
                        /*
                         Gọi FootCatBusiness.CaculatorRateByMeal(DailyMenuID, UserInfo.SchoolID, UserInfo.AcademicYearID, EatingGroupID) => lstRateByMeal
                         */
                        var lstRateByMeal = FoodCatBusiness.CaculatorRateByMeal(dailymenuid, global.SchoolID.Value, global.AcademicYearID.Value, eatinggroupid);
                        ViewData[DailyMenuConstants.LSTRATEBYMEAL] = lstRateByMeal;
                    }
                    //thuc pham theo ngay
                    else if (type == DailyMenuConstants.ADD_BYDAILY)
                    {
                        List<Key4Grid> ListKey4Grid = new List<Key4Grid>();
                        Key4Grid Key4Grid = new Key4Grid();
                        Key4Grid.ID = 1;
                        ListKey4Grid.Add(Key4Grid);
                        ViewData[DailyMenuConstants.LIST_KEY_4GRID] = ListKey4Grid;
                    }
                    ViewData["Flag"] = false;
                }
                List<SelectListItem> selectlistitems = new List<SelectListItem>();
                List<FoodCat> lstFodd = FoodCatBusiness.Search(new Dictionary<string, object>()).OrderBy(fc => fc.FoodName).ToList();
                if (lstFodd != null && lstFodd.Count > 0)
                {
                    var i = 0;
                    foreach (var item in lstFodd)
                    {
                        selectlistitems.Add(new SelectListItem { Text = item.FoodName, Value = item.FoodID.ToString(), Selected = i == 0 ? true : false });
                        if (i == 0)
                        {
                            var dic_foodcat = new Dictionary<string, object>();
                            dic_foodcat["FoodID"] = item.FoodID;
                            var FoodCat = FoodCatBusiness.Search(dic_foodcat).FirstOrDefault();
                            ViewData[DailyMenuConstants.PRICEPERONE] = FoodCat.Price;
                            ViewData[DailyMenuConstants.DISCARDEDRATE] = FoodCat.DiscardedRate;
                        }
                        i++;
                    }
                }
                ViewData[DailyMenuConstants.LIST_FOOD] = selectlistitems;

                var lstFoodCat = FoodCatBusiness.CaculatorContent(new List<int>(), new List<decimal>(), eatinggroupid, global.SchoolID.Value, global.AcademicYearID.Value);
                ViewData[DailyMenuConstants.LIST_FOODCAT] = lstFoodCat;

                var lstOtherService = OtherServiceBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).ToList();
                ViewData[DailyMenuConstants.LIST_OTHERSERVICE] = lstOtherService;

                DailyDishCost ddc = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).Where(o => o.EffectDate <= datenow).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                ViewData[DailyMenuConstants.DAILYDISHCOST] = ddc != null ? ddc.Cost : 0;

                IDictionary<string, object> dic_nn = new Dictionary<string, object>();
                dic_nn["AcademicYearID"] = global.AcademicYearID.Value;
                dic_nn["EatingGroupID"] = eatinggroupid;
                NutritionalNorm nn = NutritionalNormBusiness.SearchBySchool(global.SchoolID.Value, dic_nn).Where(o => o.EffectDate <= datenow).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                if (nn != null)
                {
                    ViewData[DailyMenuConstants.NUTRITIONALNORM] = nn;
                }
                else
                {
                    ViewData[DailyMenuConstants.NUTRITIONALNORM] = null;
                }
                ViewData[DailyMenuConstants.ADD_BY] = type;
                ViewData[DailyMenuConstants.OPTION] = string.IsNullOrWhiteSpace(option) ? "0" : option;
                return View();

            }
            return RedirectToAction("Index");
        }

        public FileResult ExportReport(int? DailyMenuID)
        {
            GlobalInfo global = new GlobalInfo();
            Stream excel = DailyMenuBusiness.CreateDailyMenuReport(global.SchoolID.Value, global.AcademicYearID.Value, DailyMenuID.Value);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "ThucDonNgay.xls";
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
            result.FileDownloadName = ReportName;
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Food(string id, string type, string option, string lstweight, FormCollection col)
        {

            var lstid = id.Split('_');
            var dailymenuid = int.Parse(lstid[0]);
            var eatinggroupid = int.Parse(lstid[1]);

            var lstStt = col["lstSTT"].Split(',');
            var format_food = "Food_{0}";
            var format_kg = "KG_{0}";
            var format_perone = "PricePerOne_{0}";
            List<DailyMenuFood> lstDailyMenuFood = new List<DailyMenuFood>();
            decimal weight = 0;
            for (var i = 1; i < lstStt.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(lstStt[i]) && int.Parse(lstStt[i]) > 0)
                {
                    weight = decimal.Parse(col[string.Format(format_kg, lstStt[i])].Trim());
                    if (weight > 0)
                    {
                        DailyMenuFood dailymenufood = new DailyMenuFood();
                        dailymenufood.Weight = weight;
                        dailymenufood.FoodID = int.Parse(col[string.Format(format_food, lstStt[i])].Trim());
                        dailymenufood.PricePerOnce = int.Parse(col[string.Format(format_perone, lstStt[i])].Trim());
                        dailymenufood.DailyMenuID = dailymenuid;
                        lstDailyMenuFood.Add(dailymenufood);
                    }
                }
            }
            DailyMenuFoodBusiness.Insert(global.SchoolID.Value, dailymenuid, lstDailyMenuFood);
            DailyMenuFoodBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpGet]
        public ActionResult FoodAdditional()
        {
            var r = Request;
            FoodAdditionalViewModel model = new FoodAdditionalViewModel();
            model.DailyMenuID = int.Parse(r["dailymenuid"]);
            model.ProteinAnimal = decimal.Parse(r["txtProteinAnimal"].Trim().Replace(".", ","));
            model.ProteinPlant = decimal.Parse(r["txtProteinPlant"].Trim().Replace(".", ","));
            model.FatAnimal = decimal.Parse(r["txtFatAnimal"].Trim().Replace(".", ","));
            model.FatPlant = decimal.Parse(r["txtFatPlant"].Trim().Replace(".", ","));
            model.Sugar = decimal.Parse(r["txtSugar"].Trim().Replace(".", ","));
            model.ProteinAnimalMin = decimal.Parse(r["txtProteinAnimalMin"].Trim().Replace(".", ","));
            model.ProteinAnimalMax = decimal.Parse(r["txtProteinAnimalMax"].Trim().Replace(".", ","));
            model.ProteinPlantMin = decimal.Parse(r["txtProteinPlantMin"].Trim().Replace(".", ","));
            model.ProteinPlantMax = decimal.Parse(r["txtProteinPlantMax"].Trim().Replace(".", ","));
            model.FatAnimalMin = decimal.Parse(r["txtFatAnimalMin"].Trim().Replace(".", ","));
            model.FatAnimalMax = decimal.Parse(r["txtFatAnimalMax"].Trim().Replace(".", ","));
            model.FatPlantMin = decimal.Parse(r["txtFatPlantMin"].Trim().Replace(".", ","));
            model.FatPlantMax = decimal.Parse(r["txtFatPlantMax"].Trim().Replace(".", ","));
            model.SugarMin = decimal.Parse(r["txtSugarMin"].Trim().Replace(".", ","));
            model.SugarMax = decimal.Parse(r["txtSugarMax"].Trim().Replace(".", ","));
            int numberOfChilden = int.Parse(r["numberOfChilden"]);
            List<SelectListItem> ListcboNutritionalToAdd = new List<SelectListItem>();
            Dictionary<int, string> lstCbo = new Dictionary<int, string>();
            List<int> lstNutritionalToAdd = new List<int>();
            List<int> listNutritionalToAddHasValue = new List<int>();
            if (model.ProteinAnimal < model.ProteinAnimalMin)
            {
                lstCbo.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN, Res.Get("DailyMenu_Label_Nutrition_Component_Animal_Protein"));
                listNutritionalToAddHasValue.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN);
            }
            lstNutritionalToAdd.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN);
            if (model.ProteinPlant < model.ProteinPlantMin)
            {
                lstCbo.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN, Res.Get("DailyMenu_Label_Nutrition_Component_Plant_Protein"));
                listNutritionalToAddHasValue.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN);
            }
            lstNutritionalToAdd.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN);
            if (model.FatAnimal < model.FatAnimalMin)
            {
                lstCbo.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT, Res.Get("DailyMenu_Label_Nutrition_Component_Animal_Fat"));
                listNutritionalToAddHasValue.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT);
            }
            lstNutritionalToAdd.Add(SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT);
            if (model.FatPlant < model.FatPlantMin)
            {
                lstCbo.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT, Res.Get("DailyMenu_Label_Nutrition_Component_Plant_Fat"));
                listNutritionalToAddHasValue.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT);
            }
            lstNutritionalToAdd.Add(SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT);
            if (model.Sugar < model.SugarMin)
            {
                lstCbo.Add(SystemParamsInFile.NUTRITION_COMPONENT_SUGAR, Res.Get("DailyMenu_Label_Nutrition_Component_Sugar"));
                listNutritionalToAddHasValue.Add(SystemParamsInFile.NUTRITION_COMPONENT_SUGAR);
            }
            lstNutritionalToAdd.Add(SystemParamsInFile.NUTRITION_COMPONENT_SUGAR);
            if (lstCbo != null && lstCbo.Count > 0)
            {
                foreach (var item in lstCbo)
                {
                    ListcboNutritionalToAdd.Add(new SelectListItem { Text = item.Value, Value = item.Key.ToString(), Selected = false });
                }
            }
            ViewData[DailyMenuConstants.CBONUTRITIONALTOADD] = ListcboNutritionalToAdd;
            ViewData[DailyMenuConstants.DAILYMENUID] = model.DailyMenuID;
            ViewData[DailyMenuConstants.PROTEINANIMALMIN] = model.ProteinAnimalMin;
            ViewData[DailyMenuConstants.PROTEINPLANTMIN] = model.ProteinPlantMin;
            ViewData[DailyMenuConstants.FATANIMALMIN] = model.FatAnimalMin;
            ViewData[DailyMenuConstants.FATPLANTMIN] = model.FatPlantMin;
            ViewData[DailyMenuConstants.SUGARMIN] = model.SugarMin;
            var lstContentToAdd = "";
            var listntritionadd = "";
            if (lstNutritionalToAdd.Count > 0)
            {
                foreach (var id in lstNutritionalToAdd)
                {
                    listntritionadd += id + ",";
                    if (listNutritionalToAddHasValue.Contains(id))
                    {
                        var lstDailyMenuFood = DailyMenuFoodBusiness.All.Where(o => o.DailyMenuID == model.DailyMenuID).ToList();
                        var dailymenu = DailyMenuBusiness.All.Where(o => o.DailyMenuID == model.DailyMenuID).FirstOrDefault();
                        List<FoodCatBO> lstFoodCatBO = new List<FoodCatBO>();

                        decimal totaladd = 0;
                        if (lstDailyMenuFood != null && lstDailyMenuFood.Count > 0)
                        {
                            foreach (var dailymenufood in lstDailyMenuFood)
                            {
                                FoodCat foodcat = new FoodCat();
                                FoodCatBO foodcatbo = new FoodCatBO();
                                foodcat = FoodCatBusiness.All.Where(o => o.FoodID == dailymenufood.FoodID).FirstOrDefault();
                                foodcatbo.FoodCat = foodcat;
                                if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN)
                                {
                                    if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                                        foodcatbo.PLG = foodcat.Protein;
                                    else foodcatbo.PLG = 0;
                                    foodcatbo.Min = model.ProteinAnimalMin;
                                }
                                if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN)
                                {
                                    if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                                        foodcatbo.PLG = foodcat.Protein;
                                    else foodcatbo.PLG = 0;
                                    foodcatbo.Min = model.ProteinPlantMin;
                                }
                                if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT)
                                {
                                    if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                                        foodcatbo.PLG = foodcat.Fat;
                                    else foodcatbo.PLG = 0;
                                    foodcatbo.Min = model.FatAnimalMin;
                                }
                                if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT)
                                {
                                    if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                                        foodcatbo.PLG = foodcat.Fat;
                                    else foodcatbo.PLG = 0;
                                    foodcatbo.Min = model.FatPlantMin;
                                }
                                if (id == SystemParamsInFile.NUTRITION_COMPONENT_SUGAR)
                                {
                                    foodcatbo.PLG = foodcat.Sugar;
                                    foodcatbo.Min = model.SugarMin;
                                }
                                foodcatbo.Weight = dailymenufood.Weight;
                                lstFoodCatBO.Add(foodcatbo);
                                totaladd += (foodcatbo.Weight * 1000 * foodcatbo.PLG.Value * (1 - decimal.Parse(foodcatbo.FoodCat.DiscardedRate.Value.ToString()) / 100)) / dailymenu.NumberOfChildren;
                            }
                        }
                        lstContentToAdd += (lstFoodCatBO[0].Min - totaladd).ToString() + "_";
                    }
                    else lstContentToAdd += "0_";
                }
            }
            ViewData[DailyMenuConstants.LSTCONTENTTOADD] = lstContentToAdd;
            ViewData[DailyMenuConstants.LSTNUTRITIONALTOADD] = listntritionadd;
            ViewData[DailyMenuConstants.NUMBEROFCHILDREN] = numberOfChilden;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoadListNutritionalToAdd()
        {
            var dailymenuid = int.Parse(Request["dailymenuid"]);
            var id = int.Parse(Request["id"]);
            var proteinanimalmin = decimal.Parse(Request["proteinanimalmin"]);
            var proteinplantmin = decimal.Parse(Request["proteinplantmin"]);
            var fatanimalmin = decimal.Parse(Request["fatanimalmin"]);
            var sugarmin = decimal.Parse(Request["sugarmin"]);
            var fatplantmin = decimal.Parse(Request["fatplantmin"]);
            var lstDailyMenuFood = DailyMenuFoodBusiness.All.Where(o => o.DailyMenuID == dailymenuid).ToList();
            var dailymenu = DailyMenuBusiness.All.Where(o => o.DailyMenuID == dailymenuid).FirstOrDefault();
            List<FoodCatBO> lstFoodCatBO = new List<FoodCatBO>();

            decimal totaladd = 0;
            if (lstDailyMenuFood != null && lstDailyMenuFood.Count > 0)
            {
                foreach (var dailymenufood in lstDailyMenuFood)
                {
                    FoodCat foodcat = new FoodCat();
                    FoodCatBO foodcatbo = new FoodCatBO();
                    foodcat = FoodCatBusiness.All.Where(o => o.FoodID == dailymenufood.FoodID).FirstOrDefault();
                    foodcatbo.FoodCat = foodcat;
                    if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN)
                    {
                        if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                            foodcatbo.PLG = foodcat.Protein;
                        else foodcatbo.PLG = 0;
                        foodcatbo.Min = proteinanimalmin;
                    }
                    if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN)
                    {
                        if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                            foodcatbo.PLG = foodcat.Protein;
                        else foodcatbo.PLG = 0;
                        foodcatbo.Min = proteinplantmin;
                    }
                    if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT)
                    {
                        if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_ANIMAL)
                            foodcatbo.PLG = foodcat.Fat;
                        else foodcatbo.PLG = 0;
                        foodcatbo.Min = fatanimalmin;
                    }
                    if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT)
                    {
                        if (foodcat.GroupType == SystemParamsInFile.FOOD_GROUP_TYPE_PLANT)
                            foodcatbo.PLG = foodcat.Fat;
                        else foodcatbo.PLG = 0;
                        foodcatbo.Min = fatplantmin;
                    }
                    if (id == SystemParamsInFile.NUTRITION_COMPONENT_SUGAR)
                    {
                        foodcatbo.PLG = foodcat.Sugar;
                        foodcatbo.Min = sugarmin;
                    }
                    foodcatbo.Weight = dailymenufood.Weight;
                    lstFoodCatBO.Add(foodcatbo);
                    totaladd += (foodcatbo.Weight * 1000 * foodcatbo.PLG.Value * (1 - decimal.Parse(foodcatbo.FoodCat.DiscardedRate.Value.ToString()) / 100)) / dailymenu.NumberOfChildren;
                }
            }
            ViewData[DailyMenuConstants.LIST_DAILYMENUFOOD] = lstFoodCatBO;
            ViewData[DailyMenuConstants.TITLECOMBO] = "";
            if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_PROTEIN)
                ViewData[DailyMenuConstants.TITLECOMBO] = Res.Get("DailyMenu_Label_Nutrition_Component_Animal_Protein");
            if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_PROTEIN)
                ViewData[DailyMenuConstants.TITLECOMBO] = Res.Get("DailyMenu_Label_Nutrition_Component_Plant_Protein");
            if (id == SystemParamsInFile.NUTRITION_COMPONENT_ANIMAL_FAT)
                ViewData[DailyMenuConstants.TITLECOMBO] = Res.Get("DailyMenu_Label_Nutrition_Component_Animal_Fat");
            if (id == SystemParamsInFile.NUTRITION_COMPONENT_PLANT_FAT)
                ViewData[DailyMenuConstants.TITLECOMBO] = Res.Get("DailyMenu_Label_Nutrition_Component_Plant_Fat");
            if (id == SystemParamsInFile.NUTRITION_COMPONENT_SUGAR)
                ViewData[DailyMenuConstants.TITLECOMBO] = Res.Get("DailyMenu_Label_Nutrition_Component_Sugar");
            ViewData[DailyMenuConstants.TOTALADD] = totaladd;
            ViewData[DailyMenuConstants.NUMBEROFCHILDREN] = dailymenu.NumberOfChildren;

            return View("_ListNutritionalToAdd");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CaculateWeight()
        {
            var lstFoodID = Request["lstFoodID"];
            var lstnutritionaltoadd = Request["lstnutritionaltoadd"];
            var lstcontenttoadd = Request["lstcontenttoadd"];
            int numberOfChilden = int.Parse(Request["numberOfChilden"]);
            List<int> ListFoodID = new List<int>();
            List<int> ListNutritionalToAdd = new List<int>();
            List<decimal> ListContentToAdd = new List<decimal>();
            if (!string.IsNullOrWhiteSpace(lstFoodID))
            {
                var lstid = lstFoodID.Split(',').ToList();
                foreach (var id in lstid)
                {
                    if (!string.IsNullOrWhiteSpace(id) && int.Parse(id) > 0)
                        ListFoodID.Add(int.Parse(id));
                }
            }
            if (!string.IsNullOrWhiteSpace(lstnutritionaltoadd))
            {
                var lstid = lstnutritionaltoadd.Split(',').ToList();
                foreach (var id in lstid)
                {
                    if (!string.IsNullOrWhiteSpace(id) && int.Parse(id) > 0)
                        ListNutritionalToAdd.Add(int.Parse(id));
                }
            }
            if (!string.IsNullOrWhiteSpace(lstcontenttoadd))
            {
                var lstid = lstcontenttoadd.Split('_').ToList();
                foreach (var id in lstid)
                {
                    if (!string.IsNullOrWhiteSpace(id) && float.Parse(id) >= 0)
                        ListContentToAdd.Add(decimal.Parse(id));
                }
            }
            List<decimal> ListWeight = new List<decimal>();
            ListWeight = FoodCatBusiness.CaculatorFoodToAdd(ListFoodID, ListNutritionalToAdd, ListContentToAdd, numberOfChilden);
            if (ListWeight != null && ListWeight.Count > 0)
            {
                return Json(ListWeight);
            }
            else
            {
                return Json(new JsonMessage("empty"));
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult FoodAdditional(int id, FormCollection col)
        {
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult LoadPricePerOne()
        {
            var id = int.Parse(Request["id"]);
            var FoodCat = FoodCatBusiness.All.Where(o => o.FoodID == id).FirstOrDefault();
            return Json(new JsonMessage(string.Format("{0},{1}", FoodCat.Price.ToString(), FoodCat.DiscardedRate.HasValue ? FoodCat.DiscardedRate.Value.ToString() : "0")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult LoadNutritionalBalance()
        {
            var r = Request;
            var lstFood = r["foods"];
            var lstWeight = r["kls"];
            var eatinggroupid = int.Parse(r["eatinggroupid"]);
            #region
            List<int> listFood = new List<int>();
            if (!string.IsNullOrWhiteSpace(lstFood))
            {
                var foods = lstFood.Split('_');
                if (foods.Count() > 0)
                {
                    foreach (var f in foods)
                    {
                        if (!string.IsNullOrWhiteSpace(f) && int.Parse(f) > 0)
                        {
                            listFood.Add(int.Parse(f));
                        }
                    }
                }
            }
            List<decimal> listWeight = new List<decimal>();
            if (!string.IsNullOrWhiteSpace(lstWeight))
            {
                var weights = lstWeight.Split('_');
                if (weights.Count() > 0)
                {
                    foreach (var w in weights)
                    {

                        if (!string.IsNullOrWhiteSpace(w))
                        {
                            decimal new_w = decimal.Parse(w.Replace(".", ","));
                            if (new_w > 0)
                                listWeight.Add(new_w);
                        }
                    }
                }
            }
            #endregion
            var lstFoodCat = FoodCatBusiness.CaculatorContent(listFood, listWeight, eatinggroupid, global.SchoolID.Value, global.AcademicYearID.Value);
            lstFoodCat.ProteinAnimal = Math.Round(lstFoodCat.ProteinAnimal, 1);
            lstFoodCat.ProteinPlant = Math.Round(lstFoodCat.ProteinPlant, 1);
            lstFoodCat.FatAnimal = Math.Round(lstFoodCat.FatAnimal, 1);
            lstFoodCat.FatPlant = Math.Round(lstFoodCat.FatPlant, 1);
            lstFoodCat.Sugar = Math.Round(lstFoodCat.Sugar, 1);
            lstFoodCat.Calo = Math.Round(lstFoodCat.Calo, 1);

            lstFoodCat.ProteinAnimalMin = Math.Round(lstFoodCat.ProteinAnimalMin, 2);
            lstFoodCat.ProteinPlantMin = Math.Round(lstFoodCat.ProteinPlantMin, 2);
            lstFoodCat.FatAnimalMin = Math.Round(lstFoodCat.FatAnimalMin, 2);
            lstFoodCat.FatPlantMin = Math.Round(lstFoodCat.FatPlantMin, 2);
            lstFoodCat.SugarMin = Math.Round(lstFoodCat.SugarMin, 2);
            lstFoodCat.CaloMin = Math.Round(lstFoodCat.CaloMin, 2);

            lstFoodCat.ProteinAnimalMax = Math.Round(lstFoodCat.ProteinAnimalMax, 2);
            lstFoodCat.ProteinPlantMax = Math.Round(lstFoodCat.ProteinPlantMax, 2);
            lstFoodCat.FatAnimalMax = Math.Round(lstFoodCat.FatAnimalMax, 2);
            lstFoodCat.FatPlantMax = Math.Round(lstFoodCat.FatPlantMax, 2);
            lstFoodCat.SugarMax = Math.Round(lstFoodCat.SugarMax, 2);
            lstFoodCat.CaloMax = Math.Round(lstFoodCat.CaloMax, 2);
            List<FoodMineralMenuBO> lstFoodMineral = new List<FoodMineralMenuBO>();
            if (lstFoodCat.lstFoodMineral != null && lstFoodCat.lstFoodMineral.Count > 0)
            {
                foreach (var item in lstFoodCat.lstFoodMineral)
                {
                    FoodMineralMenuBO foodmineral = new FoodMineralMenuBO();
                    foodmineral = item;
                    foodmineral.Value = Math.Round(foodmineral.Value, 2);
                    foodmineral.ValueFrom = Math.Round(foodmineral.ValueFrom, 2);
                    foodmineral.ValueTo = Math.Round(foodmineral.ValueTo, 2);
                    lstFoodMineral.Add(foodmineral);
                }
            }
            lstFoodCat.lstFoodMineral = lstFoodMineral;
            return Json(lstFoodCat);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DailyMenuID)
        {
            //DailyMenu dailymenu = this.DailyMenuBusiness.Find(DailyMenuID);
            //TryUpdateModel(dailymenu);
            //Utils.Utils.TrimObject(dailymenu);
            //this.DailyMenuBusiness.Update(dailymenu);
            //this.DailyMenuBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "DailyMenu");
            if (_globalInfo.IsCurrentYear)
            {
                this.DailyMenuBusiness.Delete(id, global.SchoolID.Value);
                this.DailyMenuBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ClassAssigment_Error_IsCurrentYearNoSave")), "erro");
            }

        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DailyMenuViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int yearNow = _globalInfo.AcademicYearID ?? 0;
            IQueryable<DailyMenu> query = DailyMenuBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(a => a.AcademicYearID == yearNow);
            IQueryable<DailyMenuViewModel> lst = query.Select(o => new DailyMenuViewModel
            {
                DailyMenuID = o.DailyMenuID,
                DailyMenuCode = o.DailyMenuCode,
                NumberOfChildren = o.NumberOfChildren,
                MenuType = o.MenuType,
                GetFromDishCat = o.GetFromDishCat,
                EatingGroupID = o.EatingGroupID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            });
            List<DailyMenuViewModel> ListModel = new List<DailyMenuViewModel>();

            IEnumerable<DishInspection> dishInspection = DishInspectionBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
            IEnumerable<WeeklyMenuDetail> weeklyMenuDetail = WeeklyMenuDetailBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
            IEnumerable<WeeklyMenu> WeeklyMenu = WeeklyMenuBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });


            if (query.Count() > 0)
            {
                foreach (var o in query)
                {
                    DailyMenuViewModel model = new DailyMenuViewModel();
                    model.DailyMenuID = o.DailyMenuID;
                    model.DailyMenuCode = o.DailyMenuCode;
                    model.NumberOfChildren = o.NumberOfChildren;
                    model.MenuType = o.MenuType;
                    if (model.MenuType == 1)
                    {
                        model.MenuTypeName = Res.Get("Common_Label_Menu_Type_General");
                    }
                    else if (model.MenuType == 2)
                    {
                        model.MenuTypeName = Res.Get("Common_Label_Menu_Type_Additional");
                    }
                    model.GetFromDishCat = o.GetFromDishCat;
                    model.EatingGroupID = o.EatingGroupID;
                    model.SchoolID = o.SchoolID;
                    model.AcademicYearID = o.AcademicYearID;
                    model.CreatedDate = o.CreatedDate;
                    model.IsActive = o.IsActive;
                    model.ModifiedDate = o.ModifiedDate;
                    if (o.DailyMenuDetails.FirstOrDefault() != null && o.DailyMenuDetails.FirstOrDefault().DishID.HasValue)
                    {
                        model.EditOption = DailyMenuConstants.OPTION4FOOD;
                    }
                    else if (o.DailyMenuDetails.FirstOrDefault() != null && !o.DailyMenuDetails.FirstOrDefault().DishID.HasValue)
                    {
                        model.EditOption = DailyMenuConstants.OPTION4DAILY;
                    }
                    if (model.EatingGroupID != 0)
                    {
                        model.EatingGroupName = EatingGroupBusiness.Find(model.EatingGroupID).EatingGroupName;
                    }
                    else
                    {
                        model.EatingGroupName = string.Empty;
                    }
                    model.DailyCostOfChildren = o.DailyCostOfChildren;

                    var listDishInspestion = dishInspection.Where(a => a.DailyMenuID == o.DailyMenuID).FirstOrDefault();
                    var listweeklyMenuDetail = (from dt in weeklyMenuDetail
                                                join w in WeeklyMenu on dt.WeeklyMenuID equals w.WeeklyMenuID
                                                where w.IsActive == true
                                                && dt.DailyMenuID == o.DailyMenuID
                                                select dt).FirstOrDefault();


                    if (listDishInspestion != null || listweeklyMenuDetail != null)
                    {
                        model.DeleteAble = false;
                    }
                    else
                    {
                        model.DeleteAble = true;
                    }

                    ListModel.Add(model);
                }
            }
            return ListModel;
        }

        //trangdd
        //19/12/2012
        #region Import thực đơn ngày

        public FileResult DownloadFileTemplate()
        {
            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "MN" + "/" + "Template_Import_TD_Ngay.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            //IVTWorksheet sheet3 = oBook.GetSheet(3);
            IVTRange range = sheet.GetRange("A13", "A16");
            int d = 0;
            foreach (var item in lsMealCat)
            {
                d = d + 1;
                //Copy row style
                sheet.CopyPasteSameSize(range, 13, d);
                sheet.SetCellValue(13, d, item.MealName);
            }
            //Dua du lieu vao sheet 3, 4
            List<TypeOfFood> ListTypeOfFood = TypeOfFoodBusiness.All.Where(o => o.IsActive == true).OrderBy(p => p.TypeOfFoodName).ToList();
            List<FoodCat> ListFoodCat = FoodCatBusiness.All.Where(o => o.IsActive == true).OrderBy(p => p.FoodName).ToList();
            int startRow = 5;
            for (int i = 0; i < ListTypeOfFood.Count(); i++)
            {
                sheet2.SetCellValue(startRow, 8, ListTypeOfFood[i].TypeOfFoodName);
                startRow++;
            }
            startRow = 5;          
            string strformular = "";
            int CountListFootCat = ListFoodCat.Count();
            for (int i = 0; i < CountListFootCat; i++)
            {
                int counti = i + 5;
                string str = "\"\"";
                strformular = "=IF(C"+ counti +"<>\"\",HLOOKUP(C" + counti + ",I5:J" + CountListFootCat + 4 + ",MATCH(C" + counti + ",I5:I" + CountListFootCat + 4 + ", 0))," + str +")";
                sheet2.SetCellValue(startRow, 9, ListFoodCat[i].FoodName);
                sheet2.SetCellValue(startRow, 10, ListFoodCat[i].ShortName);
                sheet2.SetCellValue(startRow, 4, strformular);
                //sheet2.SetCellValue(startRow, 4, strformular.Replace('\'', '\"'));
                startRow++;
            }
            sheet2.HideColumn(10);//ẩn cột kí tự viết tắt

            //Stream excel = oBook.ToStream();
            decimal fromNum = 0;
            decimal toNum = 10;
            Stream excel = oBook.ToStreamNumberValidationData(1,fromNum,toNum,5,1,CountListFootCat,15);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "Mau_Import_Thuc_Don_Ngay.xls";
            result.FileDownloadName = ReportName;
            return result;
        }
        

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                return Json(new JsonMessage(""));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }


        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int cboMenuType, int cboEatingGroup)
        {
            try
            {
                string FilePath = (string)Session["FilePath"];

                IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                //Lấy sheet 
                IVTWorksheet sheet = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                GlobalInfo global = new GlobalInfo();

                DailyMenu DailyMenu = new DailyMenu();
                List<DailyMenuDetail> ListDailyMenuDetail = new List<DailyMenuDetail>();

                DailyMenu.DailyMenuCode = (sheet.GetCellValue(7, 2) == null) ? null : sheet.GetCellValue(7, 2).ToString().Trim();
                if (sheet.GetCellValue(8, 2) != null)
                {
                    DailyMenu.NumberOfChildren = int.Parse(sheet.GetCellValue(8, 2).ToString());
                }
                DailyMenu.MenuType = (int)cboMenuType;
                DailyMenu.GetFromDishCat = false;
                DailyMenu.EatingGroupID = cboEatingGroup;
                DailyMenu.SchoolID = global.SchoolID.Value;
                DailyMenu.AcademicYearID = global.AcademicYearID.Value;

                IDictionary<string, object> dic_dailydishcost = new Dictionary<string, object>();
                DateTime max_effectdate = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()).Where(x => x.EffectDate < DateTime.Now).Select(o => o.EffectDate).Max();
                dic_dailydishcost["EffectDate"] = max_effectdate;
                var daylidishcost = DailyDishCostBusiness.SearchBySchool(global.SchoolID.Value, dic_dailydishcost).FirstOrDefault();
                DailyMenu.DailyCostOfChildren = daylidishcost != null ? daylidishcost.Cost : 0;
                DailyMenu.IsActive = true;

                IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
                IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);
                for (int i = 14; i < 30; i++)
                {
                    if (sheet.GetCellValue(i, 1) == null && sheet.GetCellValue(i, 2) == null && sheet.GetCellValue(i, 3) == null && sheet.GetCellValue(i, 4) == null)
                    {
                        break;
                    }
                    int col = 0;
                    foreach (var item in lsMealCat)
                    {
                        col = col + 1;
                        DailyMenuDetail DailyMenuDetail = new DailyMenuDetail();
                        DailyMenuDetail.DishName = (sheet.GetCellValue(i, col) == null) ? null : sheet.GetCellValue(i, col).ToString().Trim();
                        DailyMenuDetail.MealID = item.MealCatID;
                        ListDailyMenuDetail.Add(DailyMenuDetail);
                    }
                }
                IVTRange range = sheet.GetRange("E3", "E3");
                Boolean PASS = true;
                if (DailyMenuBusiness.CheckExistDailyMenuCode(DailyMenu.DailyMenuCode, DailyMenu.SchoolID.Value, DailyMenu.AcademicYearID) == true)
                {
                    PASS = false;
                }
                if (DailyMenu.NumberOfChildren <= 0)
                {
                    PASS = false;
                }
                if (DailyMenu.DailyMenuCode.Length > 50)
                {
                    PASS = false;
                }
                foreach (var item in lsMealCat)
                {
                    if (ListDailyMenuDetail.Where(o => o.MealID == item.MealCatID && o.DishName != null).Count() == 0)
                    {
                        PASS = false;
                        break;
                    }
                }
                List<DailyMenuFood> lstDailyMenuFood = new List<DailyMenuFood>();
                if (PASS == true)
                {
                    //Kiểm tra dữ liệu sheet 2
                    for (int i = 5; i < 50; i++)
                    {
                        if (sheet2.GetCellValue(i, 3) == null && sheet2.GetCellValue(i, 5) == null)
                        {
                            break;
                        }
                        string FoodName = (sheet2.GetCellValue(i, 3) == null) ? null : sheet2.GetCellValue(i, 3).ToString().Trim();
                        IQueryable<FoodCat> FoodCat = FoodCatBusiness.All.Where(o => o.FoodName == FoodName);
                        if (FoodCat.Count() == 0)
                        {
                            PASS = false;
                            break;
                        }
                        object KL;
                        if (sheet2.GetCellValue(i, 5) == null)
                        {
                            PASS = false;
                            break;
                        }
                        else
                        {
                            KL = sheet2.GetCellValue(i, 5);
                            int Temp;
                            if (int.TryParse(KL.ToString(), out Temp) == false)
                            {
                                PASS = false;
                                break;
                            }
                            else
                            {
                                if (int.Parse(KL.ToString()) <= 0)
                                {
                                    PASS = false;
                                    break;
                                }
                            }
                        }
                        if (PASS == true)
                        {
                            DailyMenuFood DailyMenuFood = new DailyMenuFood();
                            DailyMenuFood.Weight = decimal.Parse(KL.ToString());
                            DailyMenuFood.FoodID = FoodCat.FirstOrDefault().FoodID;
                            DailyMenuFood.PricePerOnce = FoodCat.FirstOrDefault().Price;
                            DailyMenuFood.DailyMenuID = 0;
                            lstDailyMenuFood.Add(DailyMenuFood);
                        }
                    }
                }
                if (PASS == true)
                {
                    //không có lỗi thì hệ thống thực hiện giống chức năng nhập điểm môn tính điểm
                    this.DailyMenuBusiness.Insert(DailyMenu, ListDailyMenuDetail);
                    this.DailyMenuBusiness.Save();
                    int dailymenuid = DailyMenuBusiness.All.Max(o => o.DailyMenuID);
                    DailyMenuFoodBusiness.Insert(global.SchoolID.Value, dailymenuid, lstDailyMenuFood);
                    DailyMenuFoodBusiness.Save();
                    return Json(new { success = 1 });
                }
                else
                {
                    return Json(new { success = 0 });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = "Định dạng file excel không đúng. Bạn tải file mẫu để nhập số liệu." });
            }
        }

        public FileResult ViewFileExcelError()
        {
            string FilePath = (string)Session["FilePath"];

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            GlobalInfo global = new GlobalInfo();

            DailyMenu DailyMenu = new DailyMenu();
            List<DailyMenuDetail> ListDailyMenuDetail = new List<DailyMenuDetail>();

            DailyMenu.DailyMenuCode = (sheet.GetCellValue(7, 2) == null) ? null : sheet.GetCellValue(7, 2).ToString().Trim();
            if (sheet.GetCellValue(8, 2) != null)
            {
                DailyMenu.NumberOfChildren = int.Parse(sheet.GetCellValue(8, 2).ToString());
            }
            DailyMenu.SchoolID = global.SchoolID.Value;
            DailyMenu.AcademicYearID = global.AcademicYearID.Value;

            IDictionary<string, object> MealCatSearchInfo = new Dictionary<string, object>();
            IQueryable<MealCat> lsMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, MealCatSearchInfo);
            for (int i = 14; i < 30; i++)
            {
                if (sheet.GetCellValue(i, 1) == null && sheet.GetCellValue(i, 2) == null && sheet.GetCellValue(i, 3) == null && sheet.GetCellValue(i, 4) == null)
                {
                    break;
                }
                int col = 0;
                foreach (var item in lsMealCat)
                {
                    col = col + 1;
                    DailyMenuDetail DailyMenuDetail = new DailyMenuDetail();
                    DailyMenuDetail.DishName = (sheet.GetCellValue(i, col) == null) ? null : sheet.GetCellValue(i, col).ToString().Trim();
                    DailyMenuDetail.MealID = item.MealCatID;
                    ListDailyMenuDetail.Add(DailyMenuDetail);
                }

            }
            IVTRange range = sheet.GetRange("E4", "E4");
            if (DailyMenuBusiness.CheckExistDailyMenuCode(DailyMenu.DailyMenuCode, DailyMenu.SchoolID.Value, DailyMenu.AcademicYearID) == true)
            {
                sheet.CopyPasteSameSize(range, 7, lsMealCat.Count() + 1);
                sheet.SetCellValue(7, lsMealCat.Count() + 1, "Mã thực đơn đã tồn tại");
            }
            else if (DailyMenu.DailyMenuCode.Length > 50)
            {
                sheet.CopyPasteSameSize(range, 7, lsMealCat.Count() + 1);
                sheet.SetCellValue(7, lsMealCat.Count() + 1, "Mã thực đơn không được lớn hơn 50 kí tự");
            }
            if (DailyMenu.NumberOfChildren <= 0)
            {
                sheet.CopyPasteSameSize(range, 8, lsMealCat.Count() + 1);
                sheet.SetCellValue(8, lsMealCat.Count() + 1, "Số lượng trẻ phải > 0");
            }
            string Error = "";
            foreach (var item in lsMealCat)
            {
                if (ListDailyMenuDetail.Where(o => o.MealID == item.MealCatID && o.DishName != null).Count() == 0)
                {
                    if (Error == "")
                        Error = Error + "Vui lòng chọn món ăn cho " + item.MealName;
                    else
                        Error = Error + ", " + item.MealName;
                }
            }
            sheet.CopyPasteSameSize(range, 14, lsMealCat.Count() + 1);
            sheet.SetCellValue(14, lsMealCat.Count() + 1, Error);
            //Gán lỗi cho sheet 2
            for (int i = 5; i < 50; i++)
            {
                Error = "";
                if (sheet2.GetCellValue(i, 3) == null && sheet2.GetCellValue(i, 5) == null)
                {
                    break;
                }
                string FoodName = (sheet2.GetCellValue(i, 3) == null) ? null : sheet2.GetCellValue(i, 3).ToString().Trim();
                if (FoodName != null)
                {
                    IQueryable<FoodCat> FoodCat = FoodCatBusiness.All.Where(o => o.FoodName == FoodName);
                    if (FoodCat.Count() == 0)
                    {
                        Error = "Thực phẩm " + FoodName + " không tồn tại. ";
                    }
                }
                else
                {
                    Error = "Bạn chưa chọn thực phẩm cho khối lượng đã nhập. ";
                }
                object KL;
                if (sheet2.GetCellValue(i, 5) == null)
                {
                    Error = Error + "Bạn chưa nhập khối lượng cho thực phẩm đã chọn. ";
                }
                else
                {
                    KL = sheet2.GetCellValue(i, 5);
                    int Temp;
                    if (int.TryParse(KL.ToString(), out Temp) == false)
                    {
                        Error = Error + "Khối lượng phải là số dương > 0. ";
                    }
                    else
                    {
                        if (int.Parse(KL.ToString()) <= 0)
                        {
                            Error = Error + "Khối lượng phải là số dương > 0. ";
                        }
                    }
                }
                sheet2.CopyPasteSameSize(range, i, 6);
                sheet2.SetCellValue(i, 6, Error);
            }

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "Import_Thuc_Don_Ngay_Loi.xls";
            result.FileDownloadName = ReportName;
            return result;
        }
        #endregion
    }
}





