/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DailyMenuArea
{
    public class DailyMenuConstants
    {
        public const string LIST_DAILYMENU = "listDailyMenu";
        public const string LIST_EATINGGROUP = "list_eatinggroup";
        public const string LIST_MENUTYPE = "list_menutype";
        public const string TXTDAILYCOSTOFCHILDREN = "txtdailycostofchildren";
        public const string LIST_MEALCAT = "list_mealcat";
        public const string LIST_KEY_4GRID = "list_key_4grid";
        public const string DAILYMENUNAME = "dailymenuname";
        public const string LIST_FOOD = "list_food";
        public const string DAILYMENU = "dailymenu";
        public const string LIST_FOODCAT = "list_foodcat";
        public const string LIST_OTHERSERVICE = "list_otherservice";
        public const string DAILYDISHCOST = "DailyDishCost";
        public const string PRICEPERONE = "PricePerOne";
        public const string DISCARDEDRATE = "DiscardedRate";
        public const string NUTRITIONALNORM = "NutritionalNorm";
        public const string DAILYMENUID = "dailymenuid";
        public const string EATINGGROUPID = "eatinggroupid";
        public const string LIST_DAILYMENUFOOD = "list_dailymenufood";
        public const string CBONUTRITIONALTOADD = "cboNutritionalToAdd";
        public const string TITLECOMBO = "TitleCombod";
        public const string PROTEINANIMALMIN = "ProteinAnimalMin";
        public const string PROTEINPLANTMIN = "ProteinPlantMin";
        public const string FATANIMALMIN = "FatAnimalMin";
        public const string FATPLANTMIN = "FatPlantMin";
        public const string SUGARMIN = "SugarMin";
        public const string TOTALADD = "totaladd";
        public const string NUMBEROFCHILDREN = "NumberOfChildren";
        public const string LSTNUTRITIONALTOADD = "lstNutritionalToAdd";
        public const string LSTCONTENTTOADD = "lstContentToAdd";
        public const string LIST_DISHCAT = "list_dishcat";
        public const string ADD_BYDAILY = "bydaily";
        public const string ADD_BYFOOD = "byfood";
        public const string ADD_BY = "addby";
        public const string TYPE_DETAIL = "detail";
        public const string LSTRATEBYMEAL = "lstRateByMeal";
        public const string LIST_DAILYMENUDETAIL = "List_DailyMenuDetail";
        public const string OPTION = "Option";
        public const string OPTION4DAILY = "1";
        public const string OPTION4FOOD = "2";
    }
    public class Key4Grid
    {
        public int ID { get; set; }
    }
}