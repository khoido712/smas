/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.DailyMenuArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DailyMenu_Label_DailyMenuCode")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string DailyMenuCode { get; set; }

        [ResourceDisplayName("DailyMenu_Label_EatingGroupName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DailyMenuConstants.LIST_EATINGGROUP)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> EatingGroupID { get; set; }

        [ResourceDisplayName("DailyMenu_Label_MenuType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DailyMenuConstants.LIST_MENUTYPE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> MenuType { get; set; }
    }
}