/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.DailyMenuArea.Models
{
    public class DailyMenuViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 DailyMenuID { get; set; }
        [ResourceDisplayName("DailyMenu_Label_DailyMenuCode")]
        public System.String DailyMenuCode { get; set; }

        [ResourceDisplayName("DailyMenu_Label_NumberOfChildren")]
        public System.Int32 NumberOfChildren { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32 MenuType { get; set; }
        

        [ResourceDisplayName("DailyMenu_Label_MenuType")]
        public System.String MenuTypeName { get; set; }
        [ScaffoldColumn(false)]
        public System.Boolean GetFromDishCat { get; set; }
        public System.Int32 EatingGroupID { get; set; }

        [ResourceDisplayName("DailyMenu_Label_EatingGroupName")]
        public string EatingGroupName { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Int32 AcademicYearID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        public string EditOption { get; set; }
        [ResourceDisplayName("DailyMenu_Label_DailyCostOfChildren")]
        public int DailyCostOfChildren { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
    }
}


