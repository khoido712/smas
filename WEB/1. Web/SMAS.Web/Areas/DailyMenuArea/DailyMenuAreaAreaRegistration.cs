﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DailyMenuArea
{
    public class DailyMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DailyMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DailyMenuArea_default",
                "DailyMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                            "DailyMenuArea_Food",
                            "DailyMenuArea/DailyMenu/Food/{id}/{type}/{option}/{lstweight}",
                            new { action = "Food", id = UrlParameter.Optional, type = UrlParameter.Optional, option = UrlParameter.Optional, lstweight = UrlParameter.Optional }
                        );
        }
    }
}
