﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingConductArea
{
    public class PupilRankingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingConductArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingConductArea_default",
                "PupilRankingConductArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
