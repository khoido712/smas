/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PupilRankingConductArea.Models
{
    public class PupilRankingViewModel
    {
        public int PupilID { get; set; }
        public int ClassID { get; set; }
        public int? Semester { get; set; }
        public int EducationLevelID { get; set; }
        public int? ConductLevelID { get; set; }
        public int? Status { get; set; }
        public bool Task11 { get; set; }
        public bool Task12 { get; set; }
        public bool Task21 { get; set; }
        public bool Task22 { get; set; }
        public bool Task31 { get; set; }
        public bool Task32 { get; set; }
        public bool Task41 { get; set; }
        public bool Task42 { get; set; }
        public bool Task51 { get; set; }
        public bool Task52 { get; set; }
    }
}


