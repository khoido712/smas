﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PupilRankingConductArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.PupilRankingConductArea.Controllers
{
    public class PupilRankingController : BaseController
    {        
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IConductEvaluationDetailBusiness ConductEvaluationDetailBusiness;
        private GlobalInfo GlobalInfo = new GlobalInfo();
		public PupilRankingController (IPupilRankingBusiness pupilrankingBusiness,
            IPupilAbsenceBusiness PupilAbsenceBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IConductEvaluationDetailBusiness ConductEvaluationDetailBusiness,
            IClassProfileBusiness ClassProfileBusiness)
		{
			this.PupilRankingBusiness = pupilrankingBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.ConductEvaluationDetailBusiness = ConductEvaluationDetailBusiness;
		}
		
		//
        // GET: /PupilRanking/

        private int GetTaskEvaluation(bool Task1, bool Task2)
        {
            if (Task1 && Task2)
            {
                return PupilRankingConstants.TASK_EVALUATION_11;
            }
            else if (Task1 && !Task2)
            {
                return PupilRankingConstants.TASK_EVALUATION_10;
            }
            else if (!Task1 && Task2)
            {
                return PupilRankingConstants.TASK_EVALUATION_01;
            }
            else
            {
                return PupilRankingConstants.TASK_EVALUATION_00;
            }
        }

        private int ComputeConductLevel(PupilRankingViewModel item)
        {
            int res = 0;
            if (item.Task11)
            {
                res++;
            }
            if (item.Task12)
            {
                res++;
            }
            if (item.Task21)
            {
                res++;
            }
            if (item.Task22)
            {
                res++;
            }
            if (item.Task31)
            {
                res++;
            }
            if (item.Task32)
            {
                res++;
            }
            if (item.Task41)
            {
                res++;
            }
            if (item.Task42)
            {
                res++;
            }
            if (item.Task51)
            {
                res++;
            }
            if (item.Task52)
            {
                res++;
            }

            if (res >= 5)
            {
                return ConductLevelBusiness.PASS;
            }
            else
            {
                return ConductLevelBusiness.NOT_PASS;
            }
        }

        public ActionResult Index()
        {
            ViewData["AcademicYear"] = this.AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);

            // Thông tin học kỳ
            List<ComboObject> lstSemester = CommonList.Semester();
            List<ViettelCheckboxList> lstRadioSemester = new List<ViettelCheckboxList>();
            int semester = GlobalInfo.Semester.Value;
           
            int i = 1;
            foreach (ComboObject item in lstSemester)
            {
                lstRadioSemester.Add(new ViettelCheckboxList(item.value, item.key, i == semester, false));
                i++;
            }

            lstRadioSemester.Add(new ViettelCheckboxList(Res.Get("PupilRanking_Label_Retrain"), PupilRankingConstants.RETRAIN, false, false));
            ViewData[PupilRankingConstants.LIST_SEMESTER] = lstRadioSemester;
            ViewData["Semester"] = semester;

            // Thông tin khối
            i = 0;
            List<EducationLevel> ListEducationLevel = GlobalInfo.EducationLevels;
            List<ViettelCheckboxList> lstEducationLevel = new List<ViettelCheckboxList>();
            foreach (EducationLevel item in ListEducationLevel)
            {
                lstEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, i==0, false));
                i++;
            }
            ViewData[PupilRankingConstants.LIST_EDUCATIONLEVEL] = lstEducationLevel;

            // Thông tin lớp
            List<ClassProfile> ListClass = new List<ClassProfile>();
            if (lstEducationLevel.Count > 0)
            {
                IDictionary<string, object> Dictionary = new Dictionary<string, object>();
                Dictionary["EducationLevelID"] = lstEducationLevel.First().Value;
                Dictionary["AcademicYearID"] = GlobalInfo.AcademicYearID;
                Dictionary["AppliedLevel"] = GlobalInfo.AppliedLevel.Value;
                if (!GlobalInfo.IsAdminSchoolRole && !GlobalInfo.IsViewAll && !GlobalInfo.IsEmployeeManager)
                {
                    Dictionary["UserAccountID"] = GlobalInfo.UserAccountID;
                    Dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                ListClass = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, Dictionary).ToList();
                ListClass = ListClass.OrderBy(o => o.DisplayName).ToList();
            }
            ViewData[PupilRankingConstants.LIST_CLASS] = ListClass;

            ViewData[PupilRankingConstants.LIST_PUPILRANKING] = new List<PupilRankingBO>();
            return View();
        }

		//
        // GET: /PupilRanking/Search

        
        public PartialViewResult Search(int? Semester, int? EducationLevelID, int? ClassID)
        {
            string className = "";
            if(ClassID.HasValue&&ClassID!=0){
                EducationLevelID = ClassProfileBusiness.Find(ClassID.Value).EducationLevelID;
                className = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            AcademicYear aca = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID);
            ViewData["AcademicYear"] = aca;
            ViewData["Semester"] = Semester;

            ViewData[PupilRankingConstants.TITLE_PAGE] = Res.Get("PupilRankingPeriod_List") + " - Học Kỳ " + Semester + " Lớp: " + className;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            SearchInfo["Semester"] = Semester;
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["ClassID"] = ClassID;
            IEnumerable<PupilRankingBO> lst = this._Search(SearchInfo).OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ToList();
            ViewData[PupilRankingConstants.LIST_PUPILRANKING] = lst;

            //Get view data here
            //Hiển thị nút lưu
            DateTime FirstSemesterStartDate = aca.FirstSemesterStartDate.Value;
            DateTime FirstSemesterEndDate = aca.FirstSemesterEndDate.Value;
            DateTime SecondSemesterStartDate = aca.SecondSemesterStartDate.Value;
            DateTime SecondSemesterEndDate = aca.SecondSemesterEndDate.Value;

            ViewData[PupilRankingConstants.ENABLE_SAVE] = "true";
            if (Semester != null || EducationLevelID != null || ClassID != null)
            {
                if (UtilsBusiness.HasHeadTeacherPermission(GlobalInfo.UserAccountID, ClassID.Value) == false)
                    ViewData[PupilRankingConstants.ENABLE_SAVE] = "false";
                else
                {
                    if (GlobalInfo.IsAdminSchoolRole)
                    {
                        if (!GlobalInfo.IsCurrentYear)
                        {
                            ViewData[PupilRankingConstants.ENABLE_SAVE] = "false";
                        }
                        else
                        {
                            ViewData[PupilRankingConstants.ENABLE_SAVE] = "true";
                        }
                    }
                    else
                    {
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (DateTime.Now >= FirstSemesterStartDate && DateTime.Now <= FirstSemesterEndDate)
                            {
                                ViewData[PupilRankingConstants.ENABLE_SAVE] = "true";
                            }
                            else
                                ViewData[PupilRankingConstants.ENABLE_SAVE] = "false";
                        }
                        else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (DateTime.Now >= SecondSemesterStartDate && DateTime.Now <= SecondSemesterEndDate)
                            {
                                ViewData[PupilRankingConstants.ENABLE_SAVE] = "true";
                            }
                            else
                                ViewData[PupilRankingConstants.ENABLE_SAVE] = "false";
                        }
                    }
                }
            }

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Save(List<PupilRankingViewModel> ListRanking, int? ClassID, int? Semester, int? EducationLevelID, FormCollection col)
        {
            // DungVA - Lấy EducationLevelID cho chức năng gộp mới
            if(ClassID.HasValue&&ClassID!=0){
                EducationLevelID = ClassProfileBusiness.Find(ClassID.Value).EducationLevelID;
            }
            // End DungVA
            List<PupilRanking> ListPupilRanking = new List<PupilRanking>();
            List<ConductEvaluationDetail> ListDetail = new List<ConductEvaluationDetail>();
            AcademicYear Year = this.AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
            var listPupilID= col["checkAll"];
            if (listPupilID == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ErrorCheckRowPupil"), "error"));
            }
            string[] pupilids = listPupilID.Split(new Char[] { ',' });

            DateTime startDate;
            DateTime endDate;

            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                startDate = Year.FirstSemesterStartDate.Value;
                endDate = Year.FirstSemesterEndDate.Value;
            }
            else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                startDate = Year.SecondSemesterStartDate.Value;
                endDate = Year.SecondSemesterEndDate.Value;
            }
            else 
            {
                startDate = Year.FirstSemesterStartDate.Value;
                endDate = Year.SecondSemesterEndDate.Value;
            }

            List<PupilAbsence> lstPupilAbsends = PupilAbsenceBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", GlobalInfo.AcademicYearID.Value },{ "FromDate", startDate }, { "EndDate", endDate } }).ToList();

            foreach (var pupilid in pupilids)
            {
                int pid = string.IsNullOrWhiteSpace(pupilid) ? 0 : int.Parse(pupilid);
                if (pid > 0)
                {
                    PupilRankingViewModel item = ListRanking.Where(o => o.PupilID == pid).FirstOrDefault();
                    if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        continue;
                    }
                    PupilRanking PupilRanking = new PupilRanking();
                    PupilRanking.PupilID = item.PupilID;
                    PupilRanking.ClassID = ClassID.Value;
                    PupilRanking.Semester = Semester.Value;
                    PupilRanking.EducationLevelID = EducationLevelID.Value;
                    PupilRanking.ConductLevelID = this.ComputeConductLevel(item);
                    PupilRanking.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                    PupilRanking.SchoolID = GlobalInfo.SchoolID.Value;
                    PupilRanking.CreatedAcademicYear = Year.Year;
                    PupilRanking.Last2digitNumberSchool = GlobalInfo.SchoolID.Value % 100;
                    PupilRanking.TotalAbsentDaysWithPermission = lstPupilAbsends.Count(u => u.PupilID == PupilRanking.PupilID && u.IsAccepted.HasValue && u.IsAccepted.Value);
                    PupilRanking.TotalAbsentDaysWithoutPermission = lstPupilAbsends.Count(u => u.PupilID == PupilRanking.PupilID && (!u.IsAccepted.HasValue || !u.IsAccepted.Value));
                    
                    ListPupilRanking.Add(PupilRanking);

                    // Khoi tao ConductEvaluationDetail
                    ConductEvaluationDetail ConductEvaluationDetail = new ConductEvaluationDetail();
                    ConductEvaluationDetail.PupilID = item.PupilID;
                    ConductEvaluationDetail.ClassID = ClassID.Value;
                    ConductEvaluationDetail.Semester = Semester.Value;
                    ConductEvaluationDetail.EvaluatedDate = DateTime.Now;
                    ConductEvaluationDetail.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                    ConductEvaluationDetail.SchoolID = GlobalInfo.SchoolID.Value;

                    ConductEvaluationDetail.Task1Evaluation = this.GetTaskEvaluation(item.Task11, item.Task12);
                    ConductEvaluationDetail.Task2Evaluation = this.GetTaskEvaluation(item.Task21, item.Task22);
                    ConductEvaluationDetail.Task3Evaluation = this.GetTaskEvaluation(item.Task31, item.Task32);
                    ConductEvaluationDetail.Task4Evaluation = this.GetTaskEvaluation(item.Task41, item.Task42);
                    ConductEvaluationDetail.Task5Evaluation = this.GetTaskEvaluation(item.Task51, item.Task52);

                    ListDetail.Add(ConductEvaluationDetail);
                }
            }
            //Thuc hien cho TH
            this.PupilRankingBusiness.RankingPupilConduct(GlobalInfo.UserAccountID, ListPupilRanking, Semester.Value, false);
            this.ConductEvaluationDetailBusiness.InsertOrUpdateConductEvaluationDetail(GlobalInfo.UserAccountID, ListDetail);
            //this.PupilRankingBusiness.Save();
           this.ConductEvaluationDetailBusiness.Save();
           
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PupilRankingBO> _Search(IDictionary<string, object> SearchInfo)
        {
            int Semester = (int)SMAS.Business.Common.Utils.GetByte(SearchInfo, "Semester");
            IEnumerable<PupilRankingBO> query;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
               query = this.PupilRankingBusiness.GetPupilConduct(SearchInfo);
            }
            else
            {
               query = this.PupilRankingBusiness.GetPupilConductRetest(SearchInfo);
            }

            return query;
        }

        public PartialViewResult AjaxLoadClass(int? EducationLevelID)
        {

            List<ClassProfile> ListClass = new List<ClassProfile>();

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (!GlobalInfo.IsAdminSchoolRole && !GlobalInfo.IsViewAll && !GlobalInfo.IsEmployeeManager)
            {
                Dictionary["EmployeeID"] = GlobalInfo.EmployeeID;
                Dictionary["UserAccountID"] = GlobalInfo.UserAccountID;
                Dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            ListClass = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, Dictionary).ToList();
            ListClass = ListClass.OrderBy(o => o.DisplayName).ToList();
            ViewData[PupilRankingConstants.LIST_CLASS] = ListClass;
            return PartialView("_ListClass");
        }
    }
}





