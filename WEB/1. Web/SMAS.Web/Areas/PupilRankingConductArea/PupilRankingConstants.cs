/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PupilRankingConductArea
{
    public class PupilRankingConstants
    {
        public const string LIST_PUPILRANKING = "listPupilRanking";
        public const string LIST_SEMESTER = "ListSemester";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_EDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const int RETRAIN = 4;
        public const int TASK_EVALUATION_00 = 0;
        public const int TASK_EVALUATION_01 = 1;
        public const int TASK_EVALUATION_10 = 2;
        public const int TASK_EVALUATION_11 = 3;
        public const string ENABLE_SAVE = "enable_save";
        public const string TITLE_PAGE = "TITLE_PAGE";
    }
}
