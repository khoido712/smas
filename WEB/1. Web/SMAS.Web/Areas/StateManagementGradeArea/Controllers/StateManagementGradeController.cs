﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.StateManagementGradeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.StateManagementGradeArea.Controllers
{
    public class StateManagementGradeController : BaseController
    {        
        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
		
		public StateManagementGradeController (IStateManagementGradeBusiness statemanagementgradeBusiness)
		{
			this.StateManagementGradeBusiness = statemanagementgradeBusiness;
		}
		
		//
        // GET: /StateManagementGrade/

        public ActionResult Index()
        {
            SetViewDataPermission("StateManagementGrade", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<StateManagementGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[StateManagementGradeConstants.LIST_STATEMANAGEMENTGRADE] = lst;
            return View();
        }

		//
        // GET: /StateManagementGrade/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<StateManagementGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[StateManagementGradeConstants.LIST_STATEMANAGEMENTGRADE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            StateManagementGrade statemanagementgrade = new StateManagementGrade();
            TryUpdateModel(statemanagementgrade); 
            Utils.Utils.TrimObject(statemanagementgrade);

            this.StateManagementGradeBusiness.Insert(statemanagementgrade);
            this.StateManagementGradeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int StateManagementGradeID)
        {
            StateManagementGrade statemanagementgrade = this.StateManagementGradeBusiness.Find(StateManagementGradeID);
            TryUpdateModel(statemanagementgrade);
            Utils.Utils.TrimObject(statemanagementgrade);
            this.StateManagementGradeBusiness.Update(statemanagementgrade);
            this.StateManagementGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.StateManagementGradeBusiness.Delete(id);
            this.StateManagementGradeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<StateManagementGradeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<StateManagementGrade> query = this.StateManagementGradeBusiness.Search(SearchInfo);
            IQueryable<StateManagementGradeViewModel> lst = query.Select(o => new StateManagementGradeViewModel {               
						StateManagementGradeID = o.StateManagementGradeID,								
						Resolution = o.Resolution,								
						Description = o.Description,								
											
					
				
            });

            return lst.ToList();
        }        
    }
}





