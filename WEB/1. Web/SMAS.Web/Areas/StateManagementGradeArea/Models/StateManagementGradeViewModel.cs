/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.StateManagementGradeArea.Models
{
    public class StateManagementGradeViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 StateManagementGradeID { get; set; }

        [ResourceDisplayName("StateManagementGrade_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]			
		public System.String Resolution { get; set; }

        [ResourceDisplayName("StateManagementGrade_Description")]       
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }														
	       
    }
}


