﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StateManagementGradeArea
{
    public class StateManagementGradeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StateManagementGradeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StateManagementGradeArea_default",
                "StateManagementGradeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
