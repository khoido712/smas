﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Collections.Generic;
using SMAS.Web.Areas.ExamPupilAbsenceArea.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.ExamPupilAbsenceArea.Controllers
{
    public class ExamPupilAbsenceController : BaseController
    {
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public ExamPupilAbsenceController(IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness,
            IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            IExamRoomBusiness ExamRoomBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            IDictionary<string, object> searchInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"SchoolID", _globalInfo.SchoolID.Value },
                {"IsActive", true},
                {"IsAppliedForCandidate", true }
            };

            ViewData[ExamPupilAbsenceConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamPupilAbsenceConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            List<Examinations> listExamInsert = listExam.Where(o => o.MarkInput == false && o.MarkClosing == false).ToList();
            ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExamInsert, "ExaminationsID", "ExaminationsName");

            if (listExam != null && listExam.Count > 0)
            {
                Examinations firstExam = listExam.FirstOrDefault();
                search["ExaminationsID"] = firstExam.ExaminationsID;
                searchInsert["ExaminationsID"] = firstExam.ExaminationsID;

                List<ExamGroup> listGroup = GetExamGroupFromExaminations(firstExam.ExaminationsID);
                if (listGroup.Count > 0)
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");
                    searchInsert["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;

                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName");
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode");

                   // searchInsert["SubjectID"] = listSubject != null && listSubject.Count > 0 ? listSubject.FirstOrDefault().SubjectID : 0;
                   // searchInsert["ExamRoomID"] = listRoom != null && listRoom.Count > 0 ? listRoom.FirstOrDefault().ExamRoomID : 0;
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                }

                List<ExamPupilAbsenceViewModel> listPupilViolate = this._Search(search).ToList();
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE] = listPupilViolate;
                //IEnumerable<ExamPupilAbsenceViewModel> list = this._SearchInsert(search);
                //ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = list;

                if (_globalInfo.IsCurrentYear == true && firstExam.MarkInput.HasValue && firstExam.MarkInput.Value == false && firstExam.MarkClosing.HasValue && firstExam.MarkClosing.Value == false)
                {
                    ViewData[ExamPupilAbsenceConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.BTN_COMMON_ENABLE] = false;
                }
            }
            else
            {
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });

                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE] = new List<ExamPupilAbsenceViewModel>();
               // ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = listPupilViolate;

                ViewData[ExamPupilAbsenceConstants.BTN_COMMON_ENABLE] = false;
            }
        }

        #endregion SetViewData

        #region LoadCombobox

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationsID", ExaminationsID},
            };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue && ExaminationsID > 0)
            {
                List<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamSubjectBO> GetExamSubjectFromExamGroup(long ExamGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "ExamGroupID", ExamGroupID }
            };

            List<ExamSubjectBO> listSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            return listSubject;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamSubject(long? ExamGroupID)
        {
            if (ExamGroupID.HasValue && ExamGroupID > 0)
            {
                List<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExamGroupID.Value);
                return Json(new SelectList(listSubject, "SubjectID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private List<ExamRoom> GetExamRoomFromExamSubject(long ExamGroupID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "ExamGroupID", ExamGroupID },
            };

            List<ExamRoom> listRoom = ExamRoomBusiness.Search(dic).OrderBy(o => o.ExamRoomCode).ToList();
            return listRoom;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamRoom(long? ExamGroupID)
        {
            if (ExamGroupID.HasValue && ExamGroupID > 0)
            {
                List<ExamRoom> listRoom = GetExamRoomFromExamSubject(ExamGroupID.Value);
                return Json(new SelectList(listRoom, "ExamRoomID", "ExamRoomCode"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadCombobox

        #region Search

        
        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamPupilAbsenceConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (_globalInfo.IsCurrentYear == true && exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamPupilAbsenceConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamPupilAbsenceConstants.BTN_COMMON_ENABLE] = false;
            }

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID);
            search.Add("ExaminationsID", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup);
            search.Add("SubjectID", form.ExamSubject);
            search.Add("ExamRoomID", form.ExamRoom);
            search.Add("FullName", form.FullName);

            IEnumerable<ExamPupilAbsenceViewModel> list = this._Search(search);
            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE] = list;

            return PartialView("_List");
        }

        private List<ExamPupilAbsenceViewModel> _Search(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamPupilAbsenceViewModel> listPupilAbsence = ExamPupilAbsenceBusiness.GetListExamPupilAbsence(search)
                .Select(o => new ExamPupilAbsenceViewModel
                {
                    ExamPupilAbsenceID = o.ExamPupilAbsenceID,
                    ExaminationsID = o.ExaminationsID,
                    ExamRoomID = o.ExamRoomID,
                    ExamPupilID = o.ExamPupilID,
                    ExamGroupID = o.ExamGroupID,
                    SubjectID = o.SubjectID,
                    ContentReasonAbsence = o.ContentReasonAbsence,
                    ReasonAbsence = o.ReasonAbsence,
                    PupilCode = o.PupilCode,
                    Name = o.Name,
                    FullName = o.FullName,
                    EthnicCode = o.EthnicCode,
                    SubjectName = o.SubjectName,
                    ExamRoomCode = o.ExamRoomCode,
                    SBD = o.SBD,
                    ClassName = o.ClassName
                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode))).ToList();

            return listPupilAbsence;
        }

        
        public PartialViewResult SearchInsert(long? ExaminationsID, long? ExamGroupID, int? ExamSubjectID, long? ExamRoomID)
        {
            ViewData[ExamPupilAbsenceConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;

            if (ExaminationsID.HasValue && ExamGroupID.HasValue && ExamSubjectID.HasValue && ExamRoomID.HasValue)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                search["SchoolID"] = _globalInfo.SchoolID.Value;
                search["ExaminationsID"] = ExaminationsID.HasValue ? ExaminationsID.Value : 0;
                search["ExamGroupID"] = ExamGroupID.HasValue ? ExamGroupID.Value : 0;
                search["SubjectID"] = ExamSubjectID.HasValue ? ExamSubjectID.Value : 0;
                search["ExamRoomID"] = ExamRoomID.HasValue ? ExamRoomID.Value : 0;

                List<ExamPupilAbsenceViewModel> list = this._SearchInsert(search);
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = list;
            }
            else
            {
                List<ExamPupilAbsenceViewModel> list = new List<ExamPupilAbsenceViewModel>();
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = list;
            }

            return PartialView("_ListCreate");
        }

        private List<ExamPupilAbsenceViewModel> _SearchInsert(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamPupilAbsenceViewModel> listPupilAbsence = ExamPupilAbsenceBusiness.GetListExamPupil(search)
                .Select(o => new ExamPupilAbsenceViewModel
                {
                    ExaminationsID = o.ExaminationsID,
                    ExamPupilID = o.ExamPupilID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    PupilCode = o.PupilCode,
                    FullName = o.FullName,
                    SBD = o.SBD

                }).ToList().OrderBy(o => o.SBD).ToList();

            return listPupilAbsence;
        }

        /*
        public PartialViewResult SearchUpdate(long ExamPupilAbsenceID, long? ExaminationsID, long? ExamGroupID, int? ExamSubjectID, long? ExamRoomID)
        {
            if (ExaminationsID.HasValue && ExamGroupID.HasValue && ExamSubjectID.HasValue && ExamRoomID.HasValue)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                search["SchoolID"] = _globalInfo.SchoolID.Value;
                search["ExaminationsID"] = ExaminationsID.HasValue ? ExaminationsID.Value : 0;
                search["ExamGroupID"] = ExamGroupID.HasValue ? ExamGroupID.Value : 0;
                search["SubjectID"] = ExamSubjectID.HasValue ? ExamSubjectID.Value : 0;
                search["ExamRoomID"] = ExamRoomID.HasValue ? ExamRoomID.Value : 0;

                List<ExamPupilAbsenceViewModel> list = this._Search(search);
                ExamPupilAbsenceViewModel temp = null;
                for (int i = 0; i < list.Count; i++)
                {
                    temp = list[i];
                    if (temp.ExamPupilAbsenceID == ExamPupilAbsenceID)
                    {
                        list[i].IsUpdate = true;
                        break;
                    }
                }

                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_UPDATE] = list;
            }
            else
            {
                List<ExamPupilAbsenceViewModel> list = new List<ExamPupilAbsenceViewModel>();
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_UPDATE] = list;
            }

            return PartialView("_ListUpdate");
        }*/

        #endregion Search

        #region Create

        public PartialViewResult GetDataInsert(long ExaminationsID, long? ExamGroupID, long? SubjectID, long? ExamRoomID)
        {
            SetViewDataPermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);
            search.Add("MarkClosing", 1);
            search.Add("MarkInput", 1);

            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            List<ExamGroup> listGroup = null;
            List<ExamSubjectBO> listSubject = null;
            List<ExamRoom> listRoom = null;

            if (exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
                ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName", ExaminationsID);

                search["ExaminationsID"] = ExaminationsID;
                listGroup = GetExamGroupFromExaminations(ExaminationsID);
                if (ExamGroupID.HasValue && ExamGroupID > 0)
                {
                    search["ExamGroupID"] = ExamGroupID.Value;
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName", ExamGroupID);

                    listSubject = GetExamSubjectFromExamGroup(ExamGroupID.Value);
                    if (ExamGroupID.HasValue && SubjectID.HasValue)
                    {
                        search["SubjectID"] = SubjectID.Value;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName", SubjectID);
                    }
                    else if (listSubject != null && listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    }

                    listRoom = GetExamRoomFromExamSubject(ExamGroupID.Value);
                    if (ExamGroupID.HasValue && ExamRoomID.HasValue)
                    {
                        search["ExamRoomID"] = ExamRoomID.Value;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode", ExamRoomID);
                    }
                    else if (listRoom != null && listRoom.Count > 0)
                    {
                        search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else if (listGroup != null && listGroup.Count > 0)
                {
                    search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");

                    listSubject = GetExamSubjectFromExamGroup(listGroup.FirstOrDefault().ExamGroupID);
                    if (listSubject != null && listSubject.Count > 0)
                    {
                        search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    }

                    listRoom = GetExamRoomFromExamSubject(listGroup.FirstOrDefault().ExamGroupID);
                    if (listRoom != null && listRoom.Count > 0)
                    {
                        search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                }

                if (listGroup != null && listGroup.Count > 0 && listSubject != null && listSubject.Count > 0 && listRoom != null && listRoom.Count > 0)
                {
                    List<ExamPupilAbsenceViewModel> list = this._SearchInsert(search);
                    if (list != null && list.Count > 0)
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = list;
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = new List<ExamPupilAbsenceViewModel>();
                    }
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = new List<ExamPupilAbsenceViewModel>();
                }
            }
            else
            {
                List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();

                if (listExam != null && listExam.Count > 0)
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");

                    Examinations firstExam = listExam.FirstOrDefault();
                    search["ExaminationsID"] = firstExam.ExaminationsID;
                    listGroup = GetExamGroupFromExaminations(firstExam.ExaminationsID);

                    if (listGroup != null && listGroup.Count > 0)
                    {
                        search["ExamGroupID"] = listGroup.FirstOrDefault().ExamGroupID;
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");

                        listSubject = GetExamSubjectFromExamGroup(listGroup.FirstOrDefault().ExamGroupID);
                        if (listSubject != null && listSubject.Count > 0)
                        {
                            search["SubjectID"] = listSubject.FirstOrDefault().SubjectID;
                            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName");
                        }
                        else
                        {
                            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                        }

                        listRoom = GetExamRoomFromExamSubject(listGroup.FirstOrDefault().ExamGroupID);
                        if (listRoom != null && listRoom.Count > 0)
                        {
                            search["ExamRoomID"] = listRoom.FirstOrDefault().ExamRoomID;
                            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode");
                        }
                        else
                        {
                            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                        }
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                    }
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(new string[] { });
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(new string[] { });
                }

                if (listGroup != null && listGroup.Count > 0 && listSubject != null && listSubject.Count > 0 && listRoom != null && listRoom.Count > 0)
                {
                    List<ExamPupilAbsenceViewModel> list = this._SearchInsert(search);
                    if (list != null && list.Count > 0)
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = list;
                    }
                    else
                    {
                        ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = new List<ExamPupilAbsenceViewModel>();
                    }
                }
                else
                {
                    ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_INSERT] = new List<ExamPupilAbsenceViewModel>();
                }
            }

            return PartialView("_Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SearchViewModel model, long[] CheckPupilAbsenceInsert,
            long[] HiddenExamPupilID, string[] ContentReasonAbsence, long[] CheckReasonAbsenceInsert)
        {
            if (GetMenupermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin,_globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            Examinations exam = ExaminationsBusiness.Find(model.ExaminationsID);
            if (exam != null && exam.MarkInput == false && exam.MarkClosing == false)
            {
                IDictionary<string, object> insertInfo = new Dictionary<string, object>()
                {
                    { "ExaminationsID", model.ExaminationsID },
                    { "ExamGroupID", model.ExamGroupID },
                    { "SubjectID", model.ExamSubjectID },
                    { "ExamRoomID", model.ExamRoomID }
                };
                ExamPupilAbsenceBusiness.InsertListExamPupilAbsence(insertInfo, CheckPupilAbsenceInsert, HiddenExamPupilID, ContentReasonAbsence, CheckReasonAbsenceInsert);
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations"),"error"));
            }
        }

        #endregion Create

        #region Update

        public PartialViewResult GetDataUpdate(long ExaminationsID, long ExamGroupID, long SubjectID, long ExamRoomID, long ExamPupilAbsenceID)
        {
            SetViewDataPermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamPupilAbsenceConstants.LIST_EXAMINATIONS_INSERT] = new SelectList(listExam, "ExaminationsID", "ExaminationsName", ExaminationsID);

            search["ExaminationsID"] = ExaminationsID;
            List<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID);

            search["ExamGroupID"] = ExamGroupID;
            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName", ExamGroupID);

            List<ExamSubjectBO> listSubject = GetExamSubjectFromExamGroup(ExamGroupID);
            search["SubjectID"] = SubjectID;
            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_SUBJECT] = new SelectList(listSubject, "SubjectID", "SubjectName", SubjectID);

            List<ExamRoom> listRoom = GetExamRoomFromExamSubject(ExamGroupID);
            search["ExamRoomID"] = ExamRoomID;
            ViewData[ExamPupilAbsenceConstants.LIST_EXAM_ROOM] = new SelectList(listRoom, "ExamRoomID", "ExamRoomCode", ExamRoomID);

            ///Lay hoc sinh vang thi can update
            ExamPupilAbsenceBO examPupilAbsenceObj = ExamPupilAbsenceBusiness.GetExamPupilByID(ExamPupilAbsenceID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            List<ExamPupilAbsenceViewModel> list = new List<ExamPupilAbsenceViewModel>();
            ExamPupilAbsenceViewModel objEdit = new ExamPupilAbsenceViewModel();
            objEdit.SBD = examPupilAbsenceObj.SBD;
            objEdit.ContentReasonAbsence = examPupilAbsenceObj.ContentReasonAbsence;
            objEdit.EthnicCode = examPupilAbsenceObj.EthnicCode;
            objEdit.ExamGroupID = examPupilAbsenceObj.ExamGroupID;
            objEdit.ExaminationsID = examPupilAbsenceObj.ExaminationsID;
            objEdit.ExamPupilAbsenceID = examPupilAbsenceObj.ExamPupilAbsenceID;
            objEdit.ExamPupilID = examPupilAbsenceObj.ExamPupilID;
            objEdit.ExamRoomCode = examPupilAbsenceObj.ExamRoomCode;
            objEdit.ExamRoomID = examPupilAbsenceObj.ExamRoomID;
            objEdit.FullName = examPupilAbsenceObj.FullName;
            objEdit.PupilCode = examPupilAbsenceObj.PupilCode;
            objEdit.ReasonAbsence = examPupilAbsenceObj.ReasonAbsence;
            objEdit.SubjectID = examPupilAbsenceObj.SubjectID;
            list.Add(objEdit);

            /*List<ExamPupilAbsenceViewModel> list = this._Search(search);
            ExamPupilAbsenceViewModel temp = null;
            for (int i = 0; i < list.Count; i++)
            {
                temp = list[i];
                if (temp.ExamPupilAbsenceID == ExamPupilAbsenceID)
                {
                    list[i].IsUpdate = true;
                    break;
                }
            }*/

            if (list != null && list.Count > 0)
            {
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_UPDATE] = list;
            }
            else
            {
                ViewData[ExamPupilAbsenceConstants.LIST_EXAM_PUPIL_ABSENCE_UPDATE] = new List<ExamPupilAbsenceViewModel>();
            }

            return PartialView("_Edit");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Update(long? CheckPupilAbsenceUpdate, string ContentReasonAbsence, int? CheckReasonAbsence, long ExaminationsID)
        {
            if (GetMenupermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (ExaminationsID > 0)
            {
                Examinations examinationsObj = ExaminationsBusiness.Find(ExaminationsID);
                if (examinationsObj != null && examinationsObj.MarkClosing == false && examinationsObj.MarkInput == false && CheckPupilAbsenceUpdate.HasValue)
                {
                    ExamPupilAbsenceBusiness.UpdateExamPupilAbsence(CheckPupilAbsenceUpdate.Value, ContentReasonAbsence, CheckReasonAbsence.HasValue);
                    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations"), "error"));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("ExamBag_Validate_Required_Examinations"), "error"));
            }
        }

        #endregion Update

        #region Delete

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(string stringExamPupilAbsence)
        {
            if (GetMenupermission("ExamPupilAbsence", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            string[] listDelete = stringExamPupilAbsence.Split(',');
            List<long> listExamPupilAbsence = new List<long>();
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                listExamPupilAbsence.Add(long.Parse(listDelete[i]));
            }
            ExamPupilAbsenceBusiness.DeleteExamPupilAbsence(listExamPupilAbsence);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete

        #region xuat excel
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.Examinations;
            dic["ExamGroupID"] = form.ExamGroup;
            dic["SubjectID"] = form.ExamSubject;
            dic["ExamRoomID"] = form.ExamRoom;
            dic["FullName"] = form.FullName;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamPupilAbsenceBusiness.CreateExamPupilAbsenceReport(dic);
                processedReport = ExamPupilAbsenceBusiness.InsertExamPupilAbsenceReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

    
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

    }
}
