﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamPupilAbsenceArea
{
    public class ExamPupilAbsenceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamPupilAbsenceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamPupilAbsenceArea_default",
                "ExamPupilAbsenceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
