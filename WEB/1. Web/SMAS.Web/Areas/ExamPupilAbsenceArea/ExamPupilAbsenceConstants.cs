﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamPupilAbsenceArea
{
    public class ExamPupilAbsenceConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";
        public const string BTN_COMMON_ENABLE = "BtnCommonEnable";

        public const string LIST_EXAM_ROOM = "ListExamRoom";
        public const string LIST_EXAM_GROUP = "ListExamGroup";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAMINATIONS_INSERT = "ListExaminationsInsert";
        public const string LIST_EXAM_SUBJECT = "ListExamSubject";

        public const string LIST_EXAM_PUPIL_ABSENCE = "ListExamPupilAbsence";
        public const string LIST_EXAM_PUPIL_ABSENCE_INSERT = "ListExamPupilAbsenceInsert";
        public const string LIST_EXAM_PUPIL_ABSENCE_UPDATE = "ListExamPupilAbsenceUpdate";
        public const string OBJECT_EDIT= "ObjetEdit";
    }
}