﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookUpSchedulesArea
{
    public class LookUpSchedulesConstants
    {
        /// <summary>
        /// So lop tren moi trang
        /// </summary>
        public const int CLASS_PER_PAGE = 4;
        /// <summary>
        /// Ngay bat dau cua tuan (thứ 2)
        /// </summary>
        public const int DAY_IN_WEEK_START = 2;
        /// <summary>
        /// Ngày kết thúc của tuần (thứ 7)
        /// </summary>
        public const int DAY_IN_WEEK_END = 8;
        /// <summary>
        /// Số tiết trên một buổi
        /// </summary>
        public const int NUMBER_OF_TIME_UNIT_IN_SECTION = 5;

        public const string MON_CHAO_CO = "Chào cờ";
        public const string MON_SINH_HOAT_LOP = "Sinh hoạt lớp";

        public const string TEMPLATE_NAME = "Import_ThoiKhoaBieu.xls";

        public const string CONTENT_SMS = "content_sms";

        public const string TEACHER_NAME = "TeacherName";

        public const string DISTRICT = "DISTRICT";

        public const string ACADEMIC = "ACADEMIC";

        public const string SCHOOL = "SCHOOL";

        public const string TYPEOFREPORT = "TYPEOFREPORT";

        public const string FROMLISTWEEKNAME = "FROMLISTWEEKNAME";

        public const string TOLISTWEEKNAME = "TOLISTWEEKNAME";

        public const string STATUSSCHEDULES = "STATUSSCHEDULES";

        public const string DAYOFWEEK = "DAYOFWEEK";

        public const string EDUCATIONLEVEL = "EDUCATIONLEVEL";

        public const string CLASS = "CLASS";

        public const string SECTION = "SECTION";

        public const string WEEKFORSCHOOL = "WEEKFORSCHOOL";

        public const string LIST_TEACHER = "LIST_TEACHER";

        public const string LIST_PSCHOOL = "LIST_PSCHOOL";

        public const string PLACE_ALL = "[Tất cả]";


        public const string LIST_WEEKNAME = "ListWeekName";
        public const string LIST_SECTION = "ListSection";
        public const string LIST_TEACHER1 = "ListTeacher";
        public const string LIST_TEACHING_SCHEDULE = "ListTeachingSchedule";
        public const string LIST_SECTION_GRID = "ListSectionGrid";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LIST_ASSIGN_SUBJECT = "ListAssignSubject";
        public const string LIST_CLASS_PROFILE = "ListClassProfile";
        public const string LIST_TEACHING_SCHEDULE_ORDER = "ListTeachingScheduleOrder";
        public const string LIST_DATE = "ListDate";
        public const string SECTION_ID = "SectionID";
        public const string SUBJECT_ORDER = "SubjectOrder";
        public const string OBJ_TEACHING_SCHEDULE = "ObjTeachingSchedule";
        public const string IS_TEACHER = "IsTeacher";
        public const string IS_ADMIN = "IsAdmin";
        public const string TEACHER_ID = "TeacherID";
        public const string LOGGED_TEACHER_ID = "LoggedTeacherID";
        public const string IS_RESULT = "IsResult";
        public const string TEMPLATE_FILE = "PhieuBaoGiangTuan.xls";
        public const string TEMPLATE_FILE_BOOK_TS = "LichBaoGiang_NguyenVanA_20102017.xls";
        public const string SCHOOL_WEEK_NAME = "SchoolWeekName";
        public const string IS_ENABLED = "IsEnabled";
        public const string ENABLED_BUTTON = "EnabledButton";
        public const string TS_ID = "TeachingScheduleID";
        public const string REPORT_CODE = "SoLichBaoGiang";

        public const int TEACHING_SCHEDULE_APPROVAL = 1;
        public const int TEACHING_SCHEDULE_REJECT = 2;
        public const string TEACHING_SCHEDULE_PERMISSION_APPROVAL = "permission_approval";
        public const string TEACHING_SCHEDULE_LOG_APPROVAL = "string_log_approval";
        public const string TEACHING_SCHEDULE_SHOW_APPROVAL = "show_button_approval";
        public const string TEACHING_SCHEDULE_APPROVALED = "permission_approvaled";
        public const string APPLIEd_APPROVALED = "APPLIEd_APPROVALED";
    }
}