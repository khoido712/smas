﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.LookUpSchedulesArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ClassProfileArea;
using System.IO;
using SMAS.Business.Business;
using System.Web.Script.Serialization;
using SMAS.Web;
using SMAS.VTUtils.Excel.Export;
using System.Web;
using System.Text;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using Newtonsoft.Json;


namespace SMAS.Web.Areas.LookUpSchedulesArea.Controllers
{
    public class LookUpSchedulesController : BaseController
    {
        //
        // GET: /LookUpSchedulesArea/LookUpSchedules/


        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassPropertyCatBusiness ClassPropertyCatBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IPropertyOfClassBusiness PropertyOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ISendResultBusiness SendResultBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolWeekBusiness SchoolWeekBusiness;
        private readonly ITeachingScheduleBusiness TeachingScheduleBusiness;
        private readonly ITeachingScheduleApprovalBusiness TeachingScheduleApprovalBusiness;
        private readonly IDistributeProgramBusiness DistributeProgramBusiness;
        private readonly IAssignSubjectConfigBusiness AssignSubjectConfigBusiness;

        /// <summary>
        /// Contructor
        /// </summary>
        public LookUpSchedulesController(IClassProfileBusiness classprofileBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassPropertyCatBusiness ClassPropertyCatBusiness, ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness,
            IAcademicYearBusiness AcademicYearBusiness, ISubCommitteeBusiness SubCommitteeBusiness,
            IPropertyOfClassBusiness PropertyOfClassBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness,
            ITeachingAssignmentBusiness teachingAssignmentBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            ISendResultBusiness SendResultBusiness,
            ISubjectCatBusiness SubjectCatBusiness, ICalendarBusiness calendarBusiness, IDistrictBusiness DistrictBusiness,
            ISchoolWeekBusiness SchoolWeekBusiness,
            ITeachingScheduleBusiness TeachingScheduleBusiness,
            ITeachingScheduleApprovalBusiness TeachingScheduleApprovalBusiness,
            IAssignSubjectConfigBusiness AssignSubjectConfigBusiness,
            IDistributeProgramBusiness DistributeProgramBusiness)
        {
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassPropertyCatBusiness = ClassPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.PropertyOfClassBusiness = PropertyOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SendResultBusiness = SendResultBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.SchoolWeekBusiness = SchoolWeekBusiness;
            this.TeachingScheduleBusiness = TeachingScheduleBusiness;
            this.TeachingScheduleApprovalBusiness = TeachingScheduleApprovalBusiness;
            this.DistributeProgramBusiness = DistributeProgramBusiness;
            this.AssignSubjectConfigBusiness = AssignSubjectConfigBusiness;
        }

        public const int startDay = 2, endDay = 8;
        public const int startNumberOfPeriod = 1, endNumberOfPeriod = 5;

        #region private class

        public class Week
        {
            public string WeekName { get; set; }
            public long WeekID { get; set; }

        }

        public class DateName
        {
            public DateTime Date { get; set; }
            public string Name { get; set; }
        }

        #endregion

        public ActionResult Index()
        {
            Session["CheckLevel"] = "0";
            GetViewData();
            return View();
        }

        //Session["CheckLevel"] == "1" : sở, == "2" : phòng, == "3" : trường, == "4" giáo viên thường
        //hằng số LookUpSchedules
        // 1: Đã lên, 2: Chưa lên, 3: Đã phê, 4: Chưa phê, 5: Hủy phê

        private List<WeekDefaultViewModel> AutoGenericWeekDefaul(IDictionary<string, object> dic)
        {
            // lay ngay thu 2 cua tuan bat dau nam hoc
            DateTime? startDayOfAcademic = SMAS.Business.Common.Utils.GetDateTime(dic, "FirstSemesterStartDate");
            int delta = DayOfWeek.Monday - startDayOfAcademic.Value.DayOfWeek;
            DateTime mondaystartDayOfAcademic = startDayOfAcademic.Value.AddDays(delta);

            //Lay ngay CN cua tuan ket thuc nam hoc
            DateTime? endDayOfAcademic = SMAS.Business.Common.Utils.GetDateTime(dic, "SecondSemesterEndDate");
            int delta2 = DayOfWeek.Monday - endDayOfAcademic.Value.DayOfWeek;
            DateTime sundayEndDayOfAcademic = endDayOfAcademic.Value.AddDays(delta2 + 6);

            //Tao tuan tu dong
            TimeSpan diffDay = (sundayEndDayOfAcademic - mondaystartDayOfAcademic);
            List<WeekDefaultViewModel> lstDefault = new List<WeekDefaultViewModel>();
            WeekDefaultViewModel obj = null;

            string fromDate = "";
            string toDate = "";
            for (int i = 0; i <= diffDay.TotalDays; i += 7)
            {
                fromDate = mondaystartDayOfAcademic.AddDays(i).ToString("dd/MM/yyyy");
                toDate = mondaystartDayOfAcademic.AddDays(i + 6).AddHours(23).AddMinutes(59).AddSeconds(55).ToString("dd/MM/yyyy");

                obj = new WeekDefaultViewModel();
                obj.WeekDefaultName = fromDate + " - " + toDate;
                obj.WeekDefaultID = fromDate + "*" + toDate;
                obj.FromDate = fromDate;
                obj.ToDate = toDate;
                obj.StartDate = mondaystartDayOfAcademic.AddDays(i);
                obj.SelectedWeek = false;

                if (DateTime.Now.Date >= mondaystartDayOfAcademic.AddDays(i) && DateTime.Now.Date <= mondaystartDayOfAcademic.AddDays(i).AddDays(6))
                    obj.SelectedWeek = true;

                lstDefault.Add(obj);
                fromDate = "";
                toDate = "";
            }

            return lstDefault;
        }

        public void GetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            int currentYear = DateTime.Now.Year;
            currentYear = currentYear - 1;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }

            // Đăng nhập là sở
            #region
            if (global.IsSuperVisingDeptRole == true)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                ViewData["IsSuperVisingDeptRole"] = 1;
                Session["CheckLevel"] = "1";
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);

                //Combobox Quận/Huyện
                ViewData[LookUpSchedulesConstants.DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");

                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }
                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + " - " + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }

                //Combobox Năm
                ViewData[LookUpSchedulesConstants.ACADEMIC] = new SelectList(lscbYear, "key", "value", currentYear);

                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                };

                // Combobox trường
                int academic = SMAS.Business.Common.Utils.GetInt(lscbYear.Where(x => x.key == currentYear.ToString()).Select(x => x.key).FirstOrDefault().ToString());
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                                x.EducationGrade != 16 &&
                                                                                                x.EducationGrade != 24);

                IQueryable<SchoolProfileBO> lstSPBO = (from sp in lstSP
                                                       join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                       where sp.IsActive == true && ac.IsActive == true &&
                                                           sp.IsActive == true && ac.Year == academic &&
                                                       sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                       select new SchoolProfileBO
                                                       {
                                                           SchoolProfileID = sp.SchoolProfileID,
                                                           SchoolCode = sp.SchoolCode,
                                                           SchoolName = sp.SchoolName,
                                                           AcademicYearID = ac.AcademicYearID,
                                                           EducationGrade = sp.EducationGrade
                                                       }).OrderBy(o => o.SchoolName.ToLower());

                ViewData[LookUpSchedulesConstants.SCHOOL] = new SelectList(lstSPBO.ToList(), "SchoolProfileID", "SchoolName");

                // Combobox từ tuần và đến tuần là rỗng khi vừa load lên, thay đổi khi chọn trường
                List<Week> lstWeek = new List<Week>();
                ViewData[LookUpSchedulesConstants.WEEKFORSCHOOL] = new SelectList(lstWeek, "WeekID", "WeekName");
                ViewData[LookUpSchedulesConstants.FROMLISTWEEKNAME] = new SelectList(lstWeek, "WeekID", "WeekName");
                ViewData[LookUpSchedulesConstants.TOLISTWEEKNAME] = new SelectList(lstWeek, "WeekID", "WeekName");

            }
            #endregion
            // Đăng nhập là phòng
            #region
            else if (global.IsSubSuperVisingDeptRole == true)
            {
                //ViewData[FillingLockConstants.LIST_ACCOUNTTYPE] = false;    
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID", global.ProvinceID},
                    {"DistrictID", global.DistrictID}
                };

                ViewData["IsSuperVisingDeptRole"] = 2;
                Session["CheckLevel"] = "2";
                int? DistrictID = global.DistrictID;
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic);

                // Lấy danh sách quận huyện
                //ViewData[FillingLockConstants.LIST_DEFAULT_PDISTRICT] = DistrictID;
                ViewData[LookUpSchedulesConstants.DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName", DistrictID);

                // Lấy danh sách năm
                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }
                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + " - " + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }
                ViewData[LookUpSchedulesConstants.ACADEMIC] = new SelectList(lscbYear, "key", "value", currentYear);


                // Lấy danh sách trường
                int academic = SMAS.Business.Common.Utils.GetInt(lscbYear.Where(x => x.key == currentYear.ToString()).Select(x => x.key).FirstOrDefault().ToString());
                IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", _globalInfo.ProvinceID } ,
                    { "DistrictID", _globalInfo.DistrictID}
                };
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                                x.EducationGrade != 16 &&
                                                                                                x.EducationGrade != 24);
                lstSP = lstSP.Where(x => (x.EducationGrade == 1 || x.EducationGrade == 2 || x.EducationGrade == 3) && x.SupervisingDeptID == _globalInfo.SupervisingDeptID);

                IQueryable<SchoolProfileBO> lstSPBO = (from sp in lstSP
                                                       join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                       where sp.IsActive == true && ac.IsActive == true &&
                                                           sp.IsActive == true && ac.Year == academic &&
                                                           sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                       select new SchoolProfileBO
                                                       {
                                                           SchoolProfileID = sp.SchoolProfileID,
                                                           SchoolCode = sp.SchoolCode,
                                                           SchoolName = sp.SchoolName,
                                                           AcademicYearID = ac.AcademicYearID,
                                                           EducationGrade = sp.EducationGrade
                                                       }).OrderBy(o => o.SchoolName.ToLower());

                ViewData[LookUpSchedulesConstants.SCHOOL] = new SelectList(lstSPBO.ToList(), "SchoolProfileID", "SchoolName");


                // Combobox từ tuần và đến tuần là rỗng khi vừa load lên, thay đổi khi chọn trường
                List<Week> lstWeek = new List<Week>();
                ViewData[LookUpSchedulesConstants.WEEKFORSCHOOL] = new SelectList(lstWeek, "WeekID", "WeekName");
                ViewData[LookUpSchedulesConstants.FROMLISTWEEKNAME] = new SelectList(lstWeek, "WeekID", "WeekName");
                ViewData[LookUpSchedulesConstants.TOLISTWEEKNAME] = new SelectList(lstWeek, "WeekID", "WeekName");
            }
            #endregion
            // Đăng nhập là trường
            #region
            else
            {
                Session["CheckLevel"] = "3";
                IDictionary<string, object> dicCP = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AppliedLevel",_globalInfo.AppliedLevel}
                };
                List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicCP).ToList();
                if (lstCP.Count > 0)
                {
                    AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                    dicCP["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
                    dicCP["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;

                    List<WeekDefaultViewModel> lstWeek = AutoGenericWeekDefaul(dicCP);
                    ListWeekDefault = AutoGenericWeekDefaul(dicCP);

                    List<WeekDefaultViewModel> ll = lstWeek.Where(x => x.StartDate.Date.AddDays(6) >= DateTime.Now.Date && x.StartDate.Date <= DateTime.Now.Date).ToList();
                    if (ll.Count > 0)
                    {
                        string selectedWeek = ll.FirstOrDefault().WeekDefaultID;
                        ViewData[LookUpSchedulesConstants.WEEKFORSCHOOL] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", selectedWeek);
                        ViewData[LookUpSchedulesConstants.FROMLISTWEEKNAME] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", selectedWeek);
                        ViewData[LookUpSchedulesConstants.TOLISTWEEKNAME] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", selectedWeek);
                    }
                    else
                    {
                        ViewData[LookUpSchedulesConstants.WEEKFORSCHOOL] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", false);
                        ViewData[LookUpSchedulesConstants.FROMLISTWEEKNAME] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", false);
                        ViewData[LookUpSchedulesConstants.TOLISTWEEKNAME] = new SelectList(lstWeek, "WeekDefaultID", "WeekDefaultName", false);
                    }

                }
            }
            #endregion
            // Combobox loại báo cáo
            ViewData[LookUpSchedulesConstants.TYPEOFREPORT] = new SelectList(CommonList.TypeOfReport(), "key", "value");

            // Combobox tình trạng 
            ViewData[LookUpSchedulesConstants.STATUSSCHEDULES] = new SelectList(CommonList.StatusSchedules(), "key", "value");

            // Combobox thứ
            ViewData[LookUpSchedulesConstants.DAYOFWEEK] = new SelectList(CommonList.DayOfWeek(), "key", "value");

            //Combobox khối
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[LookUpSchedulesConstants.EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            //Nếu là trường
            if (_globalInfo.SchoolID != null)
            {
                //Combobox lớp
                IDictionary<string, object> dicClass = new Dictionary<string, object>();
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
                ViewData[LookUpSchedulesConstants.CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

                //Combobox buổi      
                int classID = 0;
                ClassProfile selClass = null;
                if (classID > 0)
                {
                    selClass = ClassProfileBusiness.All.Where(a => a.ClassProfileID == classID && a.IsActive.Value).FirstOrDefault();
                }

                Dictionary<string, object> dicc = new Dictionary<string, object>();
                dicc["SchoolID"] = _globalInfo.SchoolID.Value;
                dicc["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicc["AppliedLevel"] = _globalInfo.AppliedLevel;
                if (selClass != null)
                {
                    dicc["EducationLevelID"] = selClass.EducationLevelID;
                }
                IEnumerable<ClassProfile> lstClasss = ClassProfileBusiness.Search(dicc).OrderBy(a => a.DisplayName).ToList();
                int? maxSection = lstClasss.Max(a => a.Section);
                if (selClass != null)
                {
                    maxSection = lstClasss.Where(a => a.ClassProfileID == selClass.ClassProfileID).Max(a => a.Section);
                }
                var lstSection = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection((maxSection.HasValue) ? maxSection.Value : 0, ref lstSection);
                ViewData[LookUpSchedulesConstants.SECTION] = new SelectList(lstSection.ToList().OrderBy(u => u.Section), "Section", "SectionName");


                //Combobox giáo viên
                List<TeacherComboboxViewModel> lstTeacher = new List<TeacherComboboxViewModel>();
                TeacherComboboxViewModel objTeacher = new TeacherComboboxViewModel();
                objTeacher.EmployeeID = 0;
                objTeacher.DisplayName = LookUpSchedulesConstants.PLACE_ALL;
                lstTeacher.Add(objTeacher);

                List<Employee> lstE = new List<Employee>();
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                {
                    lstE = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value && x.IsActive == true &&
                                                     (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList();
                }
                else
                {
                    List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()).ToList().Where(x => x.HeadMasterID != null).ToList();
                    lstSf = lstSf.Where(x => x.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                    //.Where(o => o.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                    if (lstSf.Count > 0)
                    {
                        lstE = (from e in EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID.Value && x.IsActive == true &&
                            (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList()
                                join sf in lstSf on e.SchoolFacultyID equals sf.SchoolFacultyID
                                select e).ToList();
                    }
                }

                List<TeacherComboboxViewModel> query = (from q in lstE
                                                        select new TeacherComboboxViewModel
                                                        {
                                                            EmployeeID = q.EmployeeID,
                                                            DisplayName = q.FullName + " - " + q.EmployeeCode + " - " + q.SchoolFaculty.FacultyName
                                                        }).ToList();
                lstTeacher.AddRange(query);
                ViewData[LookUpSchedulesConstants.LIST_TEACHER] = lstTeacher;
            }
            else //Nếu là sở hoặc phòng
            {
                //Combobox lớp
                var lstClass = new List<ClassProfileBO>();
                ViewData[LookUpSchedulesConstants.CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

                //Combobox buổi
                var lstSection = new List<CalendarBusiness.ClassSection>();
                ViewData[LookUpSchedulesConstants.SECTION] = new SelectList(lstSection.ToList(), "Section", "SectionName");

                List<TeacherComboboxViewModel> lstTeacher = new List<TeacherComboboxViewModel>();
                TeacherComboboxViewModel objTeacher = new TeacherComboboxViewModel();
                objTeacher.EmployeeID = 0;
                objTeacher.DisplayName = LookUpSchedulesConstants.PLACE_ALL;
                lstTeacher.Add(objTeacher);
                ViewData[LookUpSchedulesConstants.LIST_TEACHER] = lstTeacher;
            }
        }

        [ValidateInput(true)]
        public PartialViewResult Search(SearchModel frm, GridCommand command)
        {

            Utils.Utils.TrimObject(frm);

            int schoolID;
            int academicID;
            List<ForTeacherViewModel> listResult = new List<ForTeacherViewModel>();
            if ((frm.YearVM == null || frm.SchoolVM == null) && (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2"))
            {
                if (frm.TypeReportVM == 1)
                {
                    ViewData["Total"] = 0;
                    return PartialView("_ListToTeacher", listResult);
                }
            }
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                schoolID = frm.SchoolVM.Value;
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == frm.YearVM.Value) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                academicID = academic;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
                academicID = _globalInfo.AcademicYearID.Value;
            }

            #region Theo giáo viên
            if (frm.TypeReportVM == 1)
            {
                string fromWeekIDVM = !string.IsNullOrEmpty(frm.FromWeekIDVM) ? frm.FromWeekIDVM.ToString() : "";
                string toWeekIDVM = !string.IsNullOrEmpty(frm.ToWeekIDVM) ? frm.ToWeekIDVM.ToString() : "";
                int? teacherIDVM = frm.EmployeeID;
                int? statusVM = frm.StatusVM;

                List<int> lstEducationID = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
                List<int> lstClassEducation = ClassProfileBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID && x.IsActive == true && lstEducationID.Contains(x.EducationLevelID)).Select(x => x.ClassProfileID).ToList();

                // danh sách lịch báo giảng
                List<TeachingSchedule> lstTS = TeachingScheduleBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID).ToList();
                if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                {
                    //CheckPermissionButtonApproval(TeacherID, schoolID, academicID);
                    if (lstClassEducation.Count != 0)
                        lstTS = lstTS.Where(x => lstClassEducation.Contains(x.ClassID)).ToList();
                    else
                        lstTS = new List<TeachingSchedule>();
                }

                List<TeachingSchedule> lstTSTemp = new List<TeachingSchedule>();

                // danh sách phê duyệt
                IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    //{"TeacherID",teacherID},
                    //{"FromDate", fromDate},
                    {"AcademicYearID", academicID},
                    //{"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };

                if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                    SearchInfoTeachingScheduleApproval["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;

                if (frm.EmployeeID != 0)
                    SearchInfoTeachingScheduleApproval["TeacherID"] = frm.EmployeeID;

                List<TeachingScheduleApproval> lstApproval = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)
                .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();

                // danh sách giáo viên

                //admid
                List<Employee> lstE = new List<Employee>();
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
                {
                    lstE = EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                        (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList();
                }
                else
                {
                    List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()).ToList().Where(x => x.HeadMasterID != null).ToList();
                    lstSf = lstSf.Where(x => x.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                    //.Where(o => o.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                    if (lstSf.Count > 0)//là cán bộ quản lý bộ môn
                    {
                        lstE = (from e in EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                            (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList()
                                join sf in lstSf on e.SchoolFacultyID equals sf.SchoolFacultyID
                                select e).ToList();
                    }
                    else //là giáo viên thường
                    {
                        Session["CheckLevel"] = "4";
                    }
                }

                if (frm.TypeReportVM == 1 && lstE.Count == 0)
                {
                    ViewData["Total"] = 0;
                    return PartialView("_ListToTeacher", listResult);
                }

                // danh sách tuần của trường
                List<SchoolWeek> lstSW = SchoolWeekBusiness.All.Where(x => x.SchoolID == schoolID && x.AcademicYearID == academicID).ToList();

                // Tìm theo giáo viên
                if (frm.EmployeeID != 0)
                {
                    lstE = lstE.Where(x => x.EmployeeID == frm.EmployeeID).ToList();
                    lstTS = lstTS.Where(x => x.TeacherID == frm.EmployeeID).ToList();
                }

                int numberDate = 0;
                DateTime fromDate1 = DateTime.Now;
                //Xét bao nhiêu tuần
                if (fromWeekIDVM != "" && toWeekIDVM != "")
                {
                    string[] fromDate = fromWeekIDVM.Split('*');
                    string[] toDate = toWeekIDVM.Split('*');

                    fromDate1 = Convert.ToDateTime(fromDate[0]);
                    DateTime fromDate2 = Convert.ToDateTime(toDate[0]);
                    DateTime toDate1 = Convert.ToDateTime(fromDate[1]);

                    TimeSpan difference = fromDate2 - fromDate1;
                    double days = difference.TotalDays / 7;
                    numberDate = Convert.ToInt32(days + 1.0);
                }

                // load View Nếu có 4 giáo viên và 3 tuần thì số record là 4*3

                ForTeacherViewModel objResult = null;
                int countFirst = 0;
                DateTime a = DateTime.Now;
                DateTime b = DateTime.Now;
                for (int i = 0; i < numberDate; i++)
                {
                    if (countFirst == 0)
                    {
                        a = fromDate1;
                        b = fromDate1.AddDays((i + 1) * 6);
                    }
                    foreach (Employee itemID in lstE)
                    {
                        int lessionOfTeacher = 0;
                        //List<long> lstSchoolWeekID = lstSW.Where(x => x.FromDate == a && x.ToDate == b).Select(x => x.SchoolWeekID).ToList();
                        //if (lstSchoolWeekID.Count() != 0)
                        //    lessionOfTeacher = lstTS.Where(x => lstSchoolWeekID.Contains(x.SchoolWeekID) && x.TeacherID == itemID.EmployeeID).Count();

                        lessionOfTeacher = lstTS.Where(x => x.ScheduleDate <= b && x.ScheduleDate >= a && x.TeacherID == itemID.EmployeeID).Count();

                        objResult = new ForTeacherViewModel();
                        objResult.teacherID = itemID.EmployeeID;
                        objResult.teacherName = itemID.FullName;
                        objResult.Week = (a.ToString("dd/MM/yyyy")) + " - " + b.ToString("dd/MM/yyyy");
                        objResult.numberOfLession = lessionOfTeacher;

                        var objApproval = lstApproval.Where(x => x.TeacherID == itemID.EmployeeID && x.FromDate.Date == a.Date).OrderByDescending(x => x.TeachingScheduleApprovalID).FirstOrDefault();

                        string ApprovalStatus = "Hủy phê duyệt";
                        objResult.statusApproval = 5;
                        if (lessionOfTeacher == 0)
                        {
                            objResult.Status = "Chưa lên lịch";
                            objResult.statusApproval = 2;
                        }
                        else if (objApproval == null)
                        {
                            objResult.Status = "Chưa phê duyệt";
                            objResult.statusApproval = 4;
                        }
                        else
                        {
                            if (objApproval.Status == LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL)
                            {
                                ApprovalStatus = "Đã phê duyệt";
                                objResult.statusApproval = 3;
                            }
                            if (objApproval.EmployeeID == 0)
                                objResult.Status = string.Format("{0}  - bởi Quản trị trường lúc {1}", ApprovalStatus, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                            else
                            {
                                var employeeEntity = lstE.Where(x => x.EmployeeID == objApproval.EmployeeID).FirstOrDefault();
                                objResult.Status = string.Format("{0} - {1} lúc {2}", ApprovalStatus, employeeEntity.FullName, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                            }
                        }

                        objResult.orderByDate = a;
                        objResult.isSuperVising = Session["CheckLevel"].ToString();
                        listResult.Add(objResult);
                    }
                    a = b.AddDays(1);
                    b = a.AddDays(6);
                    countFirst++;
                }

                if (frm.StatusVM != null)
                {
                    if (frm.StatusVM == 1)
                        listResult = listResult.Where(x => x.statusApproval == 3 || x.statusApproval == 4 || x.statusApproval == 5).ToList();
                    else if (frm.StatusVM == 2)
                        listResult = listResult.Where(x => x.statusApproval == 2).ToList();
                    else if (frm.StatusVM == 3)
                        listResult = listResult.Where(x => x.statusApproval == 3).ToList();
                    else if (frm.StatusVM == 4)
                        listResult = listResult.Where(x => x.statusApproval == 4).ToList();
                    else if (frm.StatusVM == 5)
                        listResult = listResult.Where(x => x.statusApproval == 5).ToList();
                }

                int totalRecord = listResult.Count();
                ViewData["Total"] = totalRecord;

                listResult = listResult.OrderByDescending(x => x.orderByDate).ThenBy(x => x.teacherName).Take(20).ToList();
                return PartialView("_ListToTeacher", listResult);
            }
            #endregion

            #region Theo lớp học
            else
            {
                List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
                List<TeachingScheduleForStudentViewModel> lstModel = new List<TeachingScheduleForStudentViewModel>();
                List<ClassProfileTempBO> lstClass = new List<ClassProfileTempBO>();

                if ((Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2") && frm.WeekVM == null)
                {
                    return PartialView("_ListToStudent", lstModel);
                }

                int currentPage = 1;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicID;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                dicCalendar = dic;
                //dicCalendar["AppliedDate"] = Convert.ToDateTime(frm.AppliedDate);
                if (frm.EducationLevelVM.HasValue)
                {
                    dic["EducationLevelID"] = frm.EducationLevelVM.Value;
                    dic["EducationLevel"] = frm.EducationLevelVM.Value;
                    dicCalendar["EducationLevelID"] = frm.EducationLevelVM.Value;
                }
                if (frm.ClassVM.HasValue)
                {
                    dic["ClassProfileID"] = frm.ClassVM.Value;
                    dic["ClassID"] = frm.ClassVM.Value;
                    dicCalendar["ClassID"] = frm.ClassVM.Value;
                }
                if (frm.SectionVM.HasValue)
                {
                    dicCalendar["Section"] = frm.SectionVM.Value;
                    dic["Section"] = frm.SectionVM.Value;
                }

                if (frm.CurrentPage.HasValue && frm.CurrentPage.Value > 1)
                {
                    currentPage = frm.CurrentPage.Value;
                }

                if (frm.SectionVM.HasValue)
                {
                    lstSection.Add(new CalendarBusiness.ClassSection(frm.SectionVM.Value));
                }

                //ViewData["lstLevel"] = lstLevel;
                var queryClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12)
                    .Select(o => new ClassProfileTempBO
                    {
                        EducationLevelID = o.EducationLevelID,
                        ClassProfileID = o.ClassProfileID,
                        DisplayName = o.DisplayName,
                        EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty,
                        OrderNumber = o.OrderNumber,
                        Section = o.Section
                    }).ToList();
                int totalClass = queryClass.Count();
                int totalPage = (totalClass + LookUpSchedulesConstants.CLASS_PER_PAGE - 1) / LookUpSchedulesConstants.CLASS_PER_PAGE;
                lstClass = queryClass.Skip((currentPage - 1) * LookUpSchedulesConstants.CLASS_PER_PAGE).Take(LookUpSchedulesConstants.CLASS_PER_PAGE).ToList();


                // danh sách môn học
                List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.ToList();

                // danh sách phân môn
                List<AssignSubjectConfig> lstSubjectConfig = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == schoolID &&
                                                                                                    x.AcademicYearID == academicID).ToList();
                // danh sách nhân giáo viên               
                List<Employee> lstE = EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1).ToList();


                string[] lstDate = frm.WeekVM.Split('*');
                DateTime fromDate = Convert.ToDateTime(lstDate[0]);
                DateTime toDate = Convert.ToDateTime(lstDate[1]);

                // danh sách lịch báo giảng
                IQueryable<TeachingSchedule> lstTS = TeachingScheduleBusiness.All.Where(x => x.AcademicYearID == academicID &&
                                                                                        x.SchoolID == schoolID &&
                                                                                        x.ScheduleDate <= toDate &&
                                                                                        x.ScheduleDate >= fromDate);

                int day = 0;
                if (frm.DayOfWeekVM != null)
                {
                    lstTS = lstTS.Where(x => x.DayOfWeek == frm.DayOfWeekVM);
                    day = frm.DayOfWeekVM.Value;
                }
                if (frm.ClassVM != null)
                    lstTS = lstTS.Where(x => x.ClassID == frm.ClassVM);
                if (frm.SectionVM != null)
                    lstTS = lstTS.Where(x => x.SectionID == frm.SectionVM);


                List<TeachingScheduleForStudentViewModel> lstCalendar = (from cal in lstTS
                                                                         join c in ClassProfileBusiness.All on cal.ClassID equals c.ClassProfileID
                                                                         where c.IsActive.Value
                                                                         select new TeachingScheduleForStudentViewModel
                                                                         {
                                                                             AcademicYearID = cal.AcademicYearID,
                                                                             ClassID = cal.ClassID,
                                                                             DayOfWeek = cal.DayOfWeek,
                                                                             NumberOfPeriod = cal.SubjectOrder,
                                                                             Section = cal.SectionID,
                                                                             SubjectID = cal.SubjectID,
                                                                             AssignSubjectID = cal.AssignSubjectID,
                                                                             TeachingScheduleID = cal.TeachingScheduleID,
                                                                             TeacherID = cal.TeacherID,
                                                                             TeachingScheduleOrder = cal.TeachingScheduleOrder,
                                                                             EducationLevelID = c.EducationLevelID
                                                                         }).ToList();

                // Lay danh sach Distribute Program
                IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicID},
                {"SchoolID",schoolID}
                //{"SchoolWeekID",weekID}
            };
                List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
                DistributeProgram objDP = null;
                lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();

                if (lstClass != null && lstClass.Count > 0)
                {
                    //lstModel = new List<TeachingScheduleForStudentViewModel>();
                    foreach (var cls in lstClass)
                    {
                        dic["ClassID"] = cls.ClassProfileID;
                        dic["SubjectID"] = null;
                        // Lấy danh sách calendar theo classID
                        List<TeachingScheduleForStudentViewModel> clsCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                        if (clsCalendar != null && clsCalendar.Count > 0)
                        {
                            foreach (TeachingScheduleForStudentViewModel cal in clsCalendar)
                            {
                                SubjectCat objSubject = lstSubjectCat.Find(x => x.SubjectCatID == cal.SubjectID);
                                if (cal.AssignSubjectID != null)
                                {
                                    cal.SubjectName = lstSubjectConfig.Find(x => x.AssignSubjectConfigID == cal.AssignSubjectID).AssignSubjectName;
                                    var b = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == cal.SubjectID
                                                            && p.AssignSubjectID == cal.AssignSubjectID
                                                            && p.EducationLevelID == cal.EducationLevelID
                                                            && p.ClassID == cal.ClassID
                                                            && p.DistributeProgramOrder == cal.TeachingScheduleOrder);
                                    if (b != null)
                                        cal.SubjectContent = b.LessonName;
                                    else
                                        cal.SubjectContent = "";
                                }
                                else
                                {
                                    cal.SubjectName = objSubject.DisplayName;
                                    var b = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == cal.SubjectID
                                                            && p.AssignSubjectID.HasValue == false
                                                            && p.ClassID == cal.ClassID
                                                            && p.EducationLevelID == cal.EducationLevelID
                                                            && p.DistributeProgramOrder == cal.TeachingScheduleOrder);
                                    if (b != null)
                                        cal.SubjectContent = b.LessonName;
                                    else
                                        cal.SubjectContent = "";
                                }

                                cal.SubjectColor = objSubject.Color;
                                cal.NameTeacher = lstE.Find(x => x.EmployeeID == cal.TeacherID).FullName;

                                lstModel.Add(cal);
                            }
                        }
                    }

                    if (lstSection.Count == 0)
                    {
                        foreach (var cl in lstClass)
                        {
                            var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                            CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                            var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                            lstSection = lstSection.Union(listSectionOther).ToList();

                            if (lstSection.Count >= 3)
                                break;
                        }
                    }
                    lstSection = (lstSection != null) ? lstSection.OrderBy(a => a.Section).ToList() : new List<CalendarBusiness.ClassSection>();
                }

                //Add view data here
                ViewData["DAY"] = day;
                ViewData["lstSectionSearch"] = lstSection;
                ViewData["lstClassSearch"] = lstClass;
                ViewData["Semester"] = 1;
                ViewData["TotalPage"] = totalPage;
                ViewData["CurrentPage"] = currentPage;
                //Get view data here
                return PartialView("_ListToStudent", lstModel);

                //return PartialView("_ListToStudent", listResult);
            }
            #endregion
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(SearchModel frm, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(frm);

            int schoolID;
            int academicID;
            List<ForTeacherViewModel> listResult = new List<ForTeacherViewModel>();

            if ((frm.YearVM == null || frm.SchoolVM == null) && (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2"))
            {
                if (frm.TypeReportVM == 1)
                {
                    ViewData["Total"] = 0;
                    return PartialView("_ListToTeacher", listResult);
                }
            }

            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                schoolID = frm.SchoolVM.Value;
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == frm.YearVM.Value) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                academicID = academic;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
                academicID = _globalInfo.AcademicYearID.Value;
            }

            string fromWeekIDVM = !string.IsNullOrEmpty(frm.FromWeekIDVM) ? frm.FromWeekIDVM.ToString() : "";
            string toWeekIDVM = !string.IsNullOrEmpty(frm.ToWeekIDVM) ? frm.ToWeekIDVM.ToString() : "";
            int? teacherIDVM = frm.EmployeeID;
            int? statusVM = frm.StatusVM;
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            List<int> lstEducationID = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            List<int> lstClassEducation = ClassProfileBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID && x.IsActive == true && lstEducationID.Contains(x.EducationLevelID)).Select(x => x.ClassProfileID).ToList();

            // danh sách lịch báo giảng
            List<TeachingSchedule> lstTS = TeachingScheduleBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID).ToList();
            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
            {
                if (lstClassEducation.Count != 0)
                    lstTS = lstTS.Where(x => lstClassEducation.Contains(x.ClassID)).ToList();
                else
                    lstTS = new List<TeachingSchedule>();
            }

            List<TeachingSchedule> lstTSTemp = new List<TeachingSchedule>();
            TeachingSchedule objTS = new TeachingSchedule();

            // danh sách phê duyệt
            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    //{"TeacherID",teacherID},
                    //{"FromDate", fromDate},
                    {"AcademicYearID", academicID},
                    //{"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };

            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                SearchInfoTeachingScheduleApproval["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;

            if (frm.EmployeeID != 0)
                SearchInfoTeachingScheduleApproval["TeacherID"] = frm.EmployeeID;

            List<TeachingScheduleApproval> lstApproval = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)
            .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();


            // danh sách giáo viên           
            //admin
            List<Employee> lstE = new List<Employee>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
            {
                lstE = EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                    (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList();
            }
            else
            {
                List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()).ToList().Where(x => x.HeadMasterID != null).ToList();
                lstSf = lstSf.Where(x => x.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();

                if (lstSf.Count > 0) // là cán bộ quản lý bộ môn
                {
                    lstE = (from e in EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                        (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList()
                            join sf in lstSf on e.SchoolFacultyID equals sf.SchoolFacultyID
                            select e).ToList();
                }
                else //là giáo viên thường
                {
                    Session["CheckLevel"] = "4";
                }
            }

            // nếu giáo viên thường trả về NULL
            if (frm.TypeReportVM == 1 && lstE.Count == 0)
            {
                ViewData["Total"] = 0;
                return PartialView("_ListToTeacher", listResult);
            }


            List<int> lstSWID = new List<int>();
            List<SchoolWeek> lstSW = SchoolWeekBusiness.All.Where(x => x.SchoolID == schoolID && x.AcademicYearID == academicID).ToList();
            ForTeacherViewModel objResult = null;

            // Tìm theo giáo viên
            if (frm.EmployeeID != 0)
            {
                lstE = lstE.Where(x => x.EmployeeID == frm.EmployeeID).ToList();
                lstTS = lstTS.Where(x => x.TeacherID == frm.EmployeeID).ToList();
            }

            int numberDate = 0;
            DateTime fromDate1 = DateTime.Now;
            //Xét bao nhiêu tuần
            if (fromWeekIDVM != "" && toWeekIDVM != "")
            {
                string[] fromDate = fromWeekIDVM.Split('*');
                string[] toDate = toWeekIDVM.Split('*');

                fromDate1 = Convert.ToDateTime(fromDate[0]);
                DateTime fromDate2 = Convert.ToDateTime(toDate[0]);
                DateTime toDate1 = Convert.ToDateTime(fromDate[1]);

                TimeSpan difference = fromDate2 - fromDate1;
                double days = difference.TotalDays / 7;
                numberDate = Convert.ToInt32(days + 1);
            }

            // load View Nếu có 4 giáo viên và 3 tuần thì số record là 4*3
            int countFirst = 0;
            DateTime a = DateTime.Now;
            DateTime b = DateTime.Now;
            for (int i = 0; i < numberDate; i++)
            {
                if (countFirst == 0)
                {
                    a = fromDate1;
                    b = fromDate1.AddDays((i + 1) * 6);
                }
                foreach (Employee itemID in lstE)
                {
                    int lessionOfTeacher = 0;
                    //List<long> lstSchoolWeekID = lstSW.Where(x => x.FromDate == a && x.ToDate == b).Select(x => x.SchoolWeekID).ToList();
                    //if (lstSchoolWeekID.Count() != 0)
                    //    lessionOfTeacher = lstTS.Where(x => lstSchoolWeekID.Contains(x.SchoolWeekID) && x.TeacherID == itemID.EmployeeID).Count();
                    lessionOfTeacher = lstTS.Where(x => x.ScheduleDate <= b && x.ScheduleDate >= a && x.TeacherID == itemID.EmployeeID).Count();

                    objResult = new ForTeacherViewModel();
                    objResult.teacherID = itemID.EmployeeID;
                    objResult.teacherName = itemID.FullName;
                    objResult.Week = (a.ToString("dd/MM/yyyy")) + " - " + b.ToString("dd/MM/yyyy");

                    var objApproval = lstApproval.Where(x => x.TeacherID == itemID.EmployeeID && x.FromDate == a).OrderByDescending(x => x.TeachingScheduleApprovalID).FirstOrDefault();

                    string ApprovalStatus = "Hủy phê duyệt";
                    objResult.statusApproval = 5;
                    if (lessionOfTeacher == 0)
                    {
                        objResult.Status = "Chưa lên lịch";
                        objResult.statusApproval = 2;
                    }
                    else if (objApproval == null)
                    {
                        objResult.Status = "Chưa phê duyệt";
                        objResult.statusApproval = 4;
                    }
                    else
                    {
                        if (objApproval.Status == LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL)
                        {
                            ApprovalStatus = "Đã phê duyệt";
                            objResult.statusApproval = 3;
                        }
                        if (objApproval.EmployeeID == 0)
                            objResult.Status = string.Format("{0}  - bởi Quản trị trường lúc {1}", ApprovalStatus, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        else
                        {
                            var employeeEntity = lstE.Where(x => x.EmployeeID == objApproval.EmployeeID).FirstOrDefault();
                            objResult.Status = string.Format("{0} - {1} lúc {2}", ApprovalStatus, employeeEntity.FullName, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        }
                    }

                    objResult.numberOfLession = lessionOfTeacher;
                    objResult.orderByDate = a;
                    listResult.Add(objResult);
                }
                a = b.AddDays(1);
                b = a.AddDays(6);
                countFirst++;
            }

            if (frm.StatusVM != null)
            {
                if (frm.StatusVM == 1)
                    listResult = listResult.Where(x => x.statusApproval == 3 || x.statusApproval == 4 || x.statusApproval == 5).ToList();
                else if (frm.StatusVM == 2)
                    listResult = listResult.Where(x => x.statusApproval == 2).ToList();
                else if (frm.StatusVM == 3)
                    listResult = listResult.Where(x => x.statusApproval == 3).ToList();
                else if (frm.StatusVM == 4)
                    listResult = listResult.Where(x => x.statusApproval == 4).ToList();
                else if (frm.StatusVM == 5)
                    listResult = listResult.Where(x => x.statusApproval == 5).ToList();
            }

            int totalRecord = listResult.Count();
            ViewData["Total"] = totalRecord;
            listResult = listResult.OrderByDescending(x => x.orderByDate).ThenBy(x => x.teacherName).Skip((currentPage - 1) * pageSize).Take(20).ToList();

            return View(new GridModel<ForTeacherViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        #region report for class
        public FileResult ExportFileForClass(int SchoolVM, int AcademicVM,
                                             string WeekVM, int DayOfWeekVM,
                                             int EducationLevelVM, int ClassVM, int SectionVM)
        {
            int schoolID;
            int academicID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                schoolID = SchoolVM;
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == AcademicVM) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                academicID = academic;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
                academicID = _globalInfo.AcademicYearID.Value;
            }


            Stream excel = ExportForClass(schoolID, academicID,
                                  WeekVM, DayOfWeekVM,
                                  EducationLevelVM, ClassVM, SectionVM);

            //string fileName = string.Format("LichBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objEmployee.FullName), lstFromTo[0].Replace("/", ""));
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "LichBaoGiang_LopHoc.xls";
            return result;
        }


        public Stream ExportForClass(int SchoolVM, int AcademicVM,
                                     string WeekVM, int DayOfWeekVM,
                                     int EducationLevelVM, int ClassVM, int SectionVM)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/LichBaoGiang_LopHoc.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet1 = oBook.GetSheet(1);

            SchoolProfile objSchool = SchoolProfileBusiness.Find(SchoolVM);
            sheet1.SetCellValue("B2", objSchool != null ? objSchool.SchoolName : "");

            string[] Title = WeekVM.Split('*');
            sheet1.SetCellValue("F4", Title.Count() > 0 ? "TỪ " + Title[0] + " ĐẾN " + Title[1] : "");

            #region Lấy dữ liệu

            Dictionary<string, object> dic = new Dictionary<string, object>();
            Dictionary<string, object> dicCalendar = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolVM;
            dic["AcademicYearID"] = AcademicVM;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dicCalendar = dic;

            if (EducationLevelVM != 0)
            {
                dic["EducationLevelID"] = EducationLevelVM;
                dic["EducationLevel"] = EducationLevelVM;
                dicCalendar["EducationLevelID"] = EducationLevelVM;
            }
            if (ClassVM != 0)
            {
                dic["ClassProfileID"] = ClassVM;
                dic["ClassID"] = ClassVM;
                dicCalendar["ClassID"] = ClassVM;
            }
            if (SectionVM != 0)
            {
                dicCalendar["Section"] = SectionVM;
                dic["Section"] = SectionVM;
            }

            List<CalendarBusiness.ClassSection> lstSection = new List<CalendarBusiness.ClassSection>();
            if (SectionVM != 0)
            {
                lstSection.Add(new CalendarBusiness.ClassSection(SectionVM));
            }

            var queryClass = ClassProfileBusiness.Search(dic).OrderBy(a => a.DisplayName).Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12)
                .Select(o => new ClassProfileTempBO
                {
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    DisplayName = o.DisplayName,
                    EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty,
                    OrderNumber = o.OrderNumber,
                    Section = o.Section
                }).ToList();

            var lstClass = queryClass.ToList();


            // danh sách môn học
            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.ToList();

            // danh sách phân môn
            List<AssignSubjectConfig> lstSubjectConfig = AssignSubjectConfigBusiness.All.Where(x => x.SchoolID == SchoolVM &&
                                                                                               x.AcademicYearID == AcademicVM).ToList();
            // danh sách nhân giáo viên
            List<Employee> lstE = EmployeeBusiness.All.Where(x => x.SchoolID == SchoolVM && x.IsActive == true).ToList();


            string[] lstDate = WeekVM.Split('*');
            DateTime fromDate = Convert.ToDateTime(lstDate[0]);
            DateTime toDate = Convert.ToDateTime(lstDate[1]);

            // danh sách lịch báo giảng
            IQueryable<TeachingSchedule> lstTS = TeachingScheduleBusiness.All.Where(x => x.AcademicYearID == AcademicVM &&
                                                                                   x.SchoolID == SchoolVM &&
                                                                                   x.ScheduleDate <= toDate &&
                                                                                   x.ScheduleDate >= fromDate);

            int day = 0;
            if (DayOfWeekVM != 0)
            {
                lstTS = lstTS.Where(x => x.DayOfWeek == DayOfWeekVM);
                day = DayOfWeekVM;
            }
            if (ClassVM != 0)
                lstTS = lstTS.Where(x => x.ClassID == ClassVM);
            if (SectionVM != 0)
                lstTS = lstTS.Where(x => x.SectionID == SectionVM);


            List<TeachingScheduleForStudentViewModel> lstCalendar = (from cal in lstTS
                                                                     join c in ClassProfileBusiness.All on cal.ClassID equals c.ClassProfileID
                                                                     where c.IsActive.Value
                                                                     select new TeachingScheduleForStudentViewModel
                                                                     {
                                                                         AcademicYearID = cal.AcademicYearID,
                                                                         ClassID = cal.ClassID,
                                                                         DayOfWeek = cal.DayOfWeek,
                                                                         NumberOfPeriod = cal.SubjectOrder,
                                                                         Section = cal.SectionID,
                                                                         SubjectID = cal.SubjectID,
                                                                         AssignSubjectID = cal.AssignSubjectID,
                                                                         TeachingScheduleID = cal.TeachingScheduleID,
                                                                         TeacherID = cal.TeacherID,
                                                                         TeachingScheduleOrder = cal.TeachingScheduleOrder,
                                                                         EducationLevelID = c.EducationLevelID
                                                                     }).ToList();

            List<TeachingScheduleForStudentViewModel> lstModel = null;
            // Lay danh sach Distribute Program
            IDictionary<string, object> dicDP = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicVM},
                    {"SchoolID",SchoolVM}
                    //{"SchoolWeekID",weekID}
                };
            List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
            DistributeProgram objDP = null;
            lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();

            if (lstClass != null && lstClass.Count > 0)
            {
                lstModel = new List<TeachingScheduleForStudentViewModel>();
                foreach (var cls in lstClass)
                {

                    // Lấy danh sách calendar theo classID
                    List<TeachingScheduleForStudentViewModel> clsCalendar = lstCalendar.Where(a => a.ClassID == cls.ClassProfileID).ToList();
                    if (clsCalendar != null && clsCalendar.Count > 0)
                    {
                        foreach (TeachingScheduleForStudentViewModel cal in clsCalendar)
                        {
                            SubjectCat objSubject = lstSubjectCat.Find(x => x.SubjectCatID == cal.SubjectID);
                            if (cal.AssignSubjectID != null)
                            {
                                cal.SubjectName = lstSubjectConfig.Find(x => x.AssignSubjectConfigID == cal.AssignSubjectID).AssignSubjectName;
                                var b = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == cal.SubjectID
                                                        && p.AssignSubjectID == cal.AssignSubjectID
                                                        && p.EducationLevelID == cal.EducationLevelID
                                                        && p.ClassID == cal.ClassID
                                                        && p.DistributeProgramOrder == cal.TeachingScheduleOrder);
                                if (b != null)
                                    cal.SubjectContent = b.LessonName;
                                else
                                    cal.SubjectContent = "";
                            }
                            else
                            {
                                cal.SubjectName = objSubject.DisplayName;
                                var b = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == cal.SubjectID
                                                        && p.AssignSubjectID.HasValue == false
                                                        && p.ClassID == cal.ClassID
                                                        && p.EducationLevelID == cal.EducationLevelID
                                                        && p.DistributeProgramOrder == cal.TeachingScheduleOrder);
                                if (b != null)
                                    cal.SubjectContent = b.LessonName;
                                else
                                    cal.SubjectContent = "";
                            }

                            cal.SubjectColor = objSubject.Color;
                            cal.NameTeacher = lstE.Find(x => x.EmployeeID == cal.TeacherID).FullName;

                            lstModel.Add(cal);
                        }
                    }
                }

                if (lstSection.Count == 0)
                {
                    foreach (var cl in lstClass)
                    {
                        var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                        CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                        var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                        lstSection = lstSection.Union(listSectionOther).ToList();

                        if (lstSection.Count >= 3)
                            break;
                    }
                }
                lstSection = (lstSection != null) ? lstSection.OrderBy(a => a.Section).ToList() : new List<CalendarBusiness.ClassSection>();
            }



            //ngày chọn
            ViewData["DAY"] = day;

            // danh sách buổi
            ViewData["lstSectionSearch"] = lstSection;

            // danh sách lớp
            ViewData["lstClassSearch"] = lstClass;


            #endregion

            #region Fill du lieu
            int countClass = lstClass.Count();
            int countSection = lstSection.Count();
            int startRow = 6;
            int curDay = -1;
            int curSection = -1;

            if (lstModel.Count != 0)
            {
                IVTRange rang = sheet1.GetRange("E6", "E6");
                for (int i = 0; i < countClass; i++)
                {
                    sheet1.CopyPasteSameSize(rang, startRow, i + 5);
                    sheet1.SetCellValue(startRow, i + 5, lstClass[i].DisplayName);
                    sheet1.SetColumnWidth(i + 5, 40);
                }
                startRow++;

                int lstDayOfWeek = day == 0 ? LookUpSchedulesConstants.DAY_IN_WEEK_END : day;
                int startDay = day == 0 ? LookUpSchedulesConstants.DAY_IN_WEEK_START : day;
                for (int i = startDay; i <= lstDayOfWeek; i++)
                {
                    curSection = -1;
                    curDay = -1;
                    if (countSection > 0)
                    {
                        for (int s = 0; s < countSection; s++)
                        {
                            for (int t = 1; t <= LookUpSchedulesConstants.NUMBER_OF_TIME_UNIT_IN_SECTION; t++)
                            {
                                int startCol = 2;
                                if (curDay != i)
                                {
                                    if (i == 8)
                                    {
                                        sheet1.GetRange(startRow, startCol, startRow + countSection * LookUpSchedulesConstants.NUMBER_OF_TIME_UNIT_IN_SECTION - 1, startCol).Merge();
                                        sheet1.SetCellValue(startRow, startCol, "Chủ nhật"); //Chủ nhật
                                        curDay = i;
                                    }
                                    else
                                    {
                                        sheet1.GetRange(startRow, startCol, startRow + countSection * LookUpSchedulesConstants.NUMBER_OF_TIME_UNIT_IN_SECTION - 1, startCol).Merge();
                                        sheet1.SetCellValue(startRow, startCol, "Thứ " + i); //Chủ nhật
                                        curDay = i;
                                    }
                                }

                                startCol++;

                                if (curSection != s)
                                {
                                    sheet1.GetRange(startRow, startCol, startRow - 1 + LookUpSchedulesConstants.NUMBER_OF_TIME_UNIT_IN_SECTION, startCol).Merge();
                                    sheet1.SetCellValue(startRow, startCol, lstSection[s].SectionName);
                                    curSection = s;
                                }
                                startCol++;

                                sheet1.SetCellValue(startRow, startCol, t);
                                startCol++;

                                for (int k = 0; k < countClass; k++)
                                {
                                    var calendarItem = lstModel.Where(a => a.ClassID == lstClass[k].ClassProfileID && a.Section.HasValue
                                                        && a.Section.Value == lstSection[s].Section && a.DayOfWeek == i
                                                        && a.NumberOfPeriod.HasValue && a.NumberOfPeriod.Value == t);
                                    if (calendarItem != null)
                                    {
                                        //int count = 0;
                                        foreach (var iz in calendarItem)
                                        {
                                            string content = iz.SubjectName + " \nGV: " + iz.NameTeacher + " \n " + iz.SubjectContent;
                                            sheet1.SetCellValue(startRow, startCol, content);
                                            sheet1.GetRange(startRow, startCol, startRow, startCol).WrapText();
                                        }
                                    }
                                    startCol++;
                                }
                                //Kết thúc phần class
                                startRow++;
                            }
                        }
                    }
                }
                sheet1.GetRange(7, 2, startRow - 1, 4 + countClass).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            #endregion

            //sheet1.FitAllColumnsOnOnePage = true;
            //Xoa sheet template
            return oBook.ToStream();
        }


        #endregion

        #region report for detail
        private static List<WeekDefaultViewModel> ListWeekDefault;
        public FileResult ExportExcelBookTeachingSchedule(int TeacherID, string CurrentWeekID,
                                                            int TypeExport, int SchoolID, int AcademicID)
        {
            List<WeekDefaultViewModel> lstWeekDefault = ListWeekDefault;
            List<string> lstFromTo = new List<string>();
            string fromDateToDate = string.Empty;
            // theo tuần

            lstWeekDefault = lstWeekDefault.Where(x => x.WeekDefaultID == CurrentWeekID).ToList();

            lstFromTo = CurrentWeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            fromDateToDate = CurrentWeekID;

            Employee objEmployee = EmployeeBusiness.Find(TeacherID);

            int schoolID;
            int academicID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                schoolID = SchoolID;
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == AcademicID) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                academicID = academic;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
                academicID = _globalInfo.AcademicYearID.Value;
            }


            Stream excel = Export(TeacherID, objEmployee,
                                    lstFromTo, fromDateToDate,
                                    lstWeekDefault, 1,
                                    schoolID, academicID);

            string fileName = string.Format("LichBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objEmployee.FullName), lstFromTo[0].Replace("/", ""));
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        public Stream Export(
            int TeacherID,
            Employee objEmployee,
            List<string> lstFromTo,
            string fromDateToDate,
            List<WeekDefaultViewModel> lstWeekDefault,
            int TypeExport, int schoolID, int academicID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", LookUpSchedulesConstants.TEMPLATE_FILE_BOOK_TS);

            IDictionary<string, object> dicCP = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicID},
                {"SchoolID",schoolID},              
            };
            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                dicCP["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<int> lstSectionID = new List<int>();
            lstSectionID.Add(1);
            lstSectionID.Add(2);
            lstSectionID.Add(3);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            // sheet bìa
            IVTWorksheet sheetFirst = oBook.GetSheet(1);
            // sheet thông tin giảng dạy
            IVTWorksheet sheetInfo = oBook.GetSheet(2);
            // sheet nội dung
            IVTWorksheet sheetContent = oBook.GetSheet(3);

            // Get data
            #region
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear objYear = AcademicYearBusiness.Find(academicID);

            IDictionary<string, object> dicTS = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicID},
                {"SchoolID",schoolID},
                {"lstFromTo",lstFromTo},
                {"TeacherID",TeacherID},
            };
            IQueryable<TeachingSchedule> lstTeachingSchedule = TeachingScheduleBusiness.Search(dicTS);

            IDictionary<string, object> dicSW = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicID},
                {"SchoolID",schoolID},
                {"fromDateToDate", fromDateToDate}
            };
            List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dicSW).ToList();
            List<int> lstSchoolWeekID = lstSchoolWeek.Select(x => (int)x.SchoolWeekID).Distinct().ToList();

            IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicID},
                {"SchoolID",schoolID},
                {"lstSchoolWeekID", lstSchoolWeekID}
            };
            List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
            DistributeProgram objDP = null;
            lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();

            DateTime fromDate = Convert.ToDateTime(lstFromTo[0]);
            DateTime toDate = Convert.ToDateTime(lstFromTo[1]);

            // phê duyệt    
            IDictionary<string, object> dicTSA = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    {"TeacherID",TeacherID},
                    //{"FromDate", fromDate},
                    {"AcademicYearID",academicID},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };
            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                dicTSA["AppliedLevel"] = _globalInfo.AppliedLevel;

            var lstTeachScheduleApproval = this.TeachingScheduleApprovalBusiness.Search(dicTSA)
                                                .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();

            lstTeachingSchedule = lstTeachingSchedule.Where(p => p.ScheduleDate >= fromDate && p.ScheduleDate <= toDate);

            List<TeachingScheduleViewModel> lstResult = new List<TeachingScheduleViewModel>();
            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
            {
                lstResult = (from t in lstTeachingSchedule
                             join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                             join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                             join _asc in AssignSubjectConfigBusiness.All on t.AssignSubjectID equals _asc.AssignSubjectConfigID
                             into temp
                             from asc in temp.Where(x => x.SubjectID == t.SubjectID).DefaultIfEmpty()
                             where c.IsActive.Value && sc.AppliedLevel == _globalInfo.AppliedLevel
                             select new TeachingScheduleViewModel
                             {
                                 ScheduleDate = t.ScheduleDate,
                                 SchoolWeekID = t.SchoolWeekID,
                                 Section = t.SectionID,
                                 SubjectOrder = t.SubjectOrder,
                                 SubjectID = t.SubjectID,
                                 SubjectName = sc.SubjectName,
                                 ClassID = t.ClassID,
                                 ClassName = c.DisplayName,
                                 TeachingScheduleOrder = t.TeachingScheduleOrder,
                                 TeachingUtensil = t.TeachingUtensil,
                                 Number = t.Quantity,
                                 InRoom = t.InRoom,
                                 IsSelfMade = t.IsSelfMade,
                                 EducationLevelID = c.EducationLevelID,
                                 AssignSubjectID = t.AssignSubjectID,
                                 AssignSubjectName = asc != null ? asc.AssignSubjectName : "",
                                 Status = t.Status
                             }).ToList();
            }
            else
            {
                lstResult = (from t in lstTeachingSchedule
                             join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                             join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                             join _asc in AssignSubjectConfigBusiness.All on t.AssignSubjectID equals _asc.AssignSubjectConfigID
                             into temp
                             from asc in temp.Where(x => x.SubjectID == t.SubjectID).DefaultIfEmpty()
                             where c.IsActive.Value
                             select new TeachingScheduleViewModel
                             {
                                 ScheduleDate = t.ScheduleDate,
                                 SchoolWeekID = t.SchoolWeekID,
                                 Section = t.SectionID,
                                 SubjectOrder = t.SubjectOrder,
                                 SubjectID = t.SubjectID,
                                 SubjectName = sc.SubjectName,
                                 ClassID = t.ClassID,
                                 ClassName = c.DisplayName,
                                 TeachingScheduleOrder = t.TeachingScheduleOrder,
                                 TeachingUtensil = t.TeachingUtensil,
                                 Number = t.Quantity,
                                 InRoom = t.InRoom,
                                 IsSelfMade = t.IsSelfMade,
                                 EducationLevelID = c.EducationLevelID,
                                 AssignSubjectID = t.AssignSubjectID,
                                 AssignSubjectName = asc != null ? asc.AssignSubjectName : "",
                                 Status = t.Status
                             }).ToList();
            }

            List<TeachingScheduleViewModel> lstSubjectFlowClass = lstTeachingSchedule
                .Select(x => new TeachingScheduleViewModel
                {
                    ClassID = x.ClassID,
                    SubjectID = x.SubjectID
                }).Distinct().ToList();
            List<TeachingScheduleViewModel> lstSubjectName = lstResult
                .Select(x => new TeachingScheduleViewModel
                {
                    SubjectName = x.SubjectName,
                    SubjectID = x.SubjectID
                }).Distinct().ToList();
            List<int> lstSubjectID = lstSubjectName.Select(x => x.SubjectID).Distinct().ToList();
            #endregion
            //Fill Nội dung tuần
            #region

            List<int> lstSectionApplied = new List<int>();

            int mergeThu = 5;
            int StartRow = 5;
            IVTWorksheet sheetCopy = null;
            string dateName = string.Empty;
            string description = string.Empty;

            bool IsMorning = true;
            bool IsAfternoon = true;
            bool IsNight = true;
            string userApproval = string.Empty;
            string dateApproval = "Ngày.... tháng.... năm....";
            TeachingScheduleViewModel objTSVM = null;
            for (int i = 0; i < lstWeekDefault.Count(); i++)
            {
                DateTime startDate = lstWeekDefault[i].StartDate;

                lstSectionApplied = lstResult.Where(x => x.ScheduleDate >= startDate && x.ScheduleDate <= startDate.AddDays(6))
                                            .Select(x => x.Section).Distinct()
                                            .OrderBy(x => x).ToList();
                if (lstSectionApplied.Count() == 0)
                {
                    continue;
                }

                DateTime valFromDateConvert = Convert.ToDateTime(lstWeekDefault[i].FromDate);
                sheetCopy = oBook.CopySheetToLast(sheetContent);
                sheetCopy.SetCellValue("A3", string.Format("Giáo viên: {0} - Từ ngày {1} đến ngày {2}", objEmployee.FullName, lstWeekDefault[i].FromDate, lstWeekDefault[i].ToDate));

                var objApproval = lstTeachScheduleApproval.Where(x => x.FromDate == valFromDateConvert).FirstOrDefault();
                if (objApproval != null)
                {
                    if (objApproval.EmployeeID != 0)
                    {
                        var objEm = EmployeeBusiness.Find(objApproval.EmployeeID);
                        if (objEm != null)
                        {
                            userApproval = objEm.FullName;
                        }
                    }
                    else
                    {
                        userApproval = "Quản Trị Trường";
                    }
                    sheetCopy.SetCellValue("A111", objApproval.Note);
                }
                //sheetCopy.SetCellValue("G110", dateApproval);
                sheetCopy.SetCellValue("G114", userApproval);
                userApproval = string.Empty;
                dateApproval = "Ngày.... tháng.... năm....";

                // kiểm tra trong tuần GV có dạy buổi sáng - chiều - tối hay không?
                var checkIsMorning = lstSectionApplied.Where(x => x == lstSectionID[0]).ToList();
                var checkIsAfternoon = lstSectionApplied.Where(x => x == lstSectionID[1]).ToList();
                var checkIsNight = lstSectionApplied.Where(x => x == lstSectionID[2]).ToList();

                if (checkIsMorning.Count <= 0)
                    IsMorning = false; // không dạy buổi sáng
                if (checkIsAfternoon.Count <= 0)
                    IsAfternoon = false; // không dạy buổi chiều
                if (checkIsNight.Count <= 0)
                    IsNight = false; // không dạy buổi tối

                for (int n = 1; n <= 7; n++)//for 1 tuan 7 ngay co dinh
                {
                    dateName = this.GetNameToDay(startDate.DayOfWeek.ToString());
                    #region
                    if (IsMorning && IsAfternoon && IsNight)// Dạy 3 buổi
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 7), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 8)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && IsAfternoon && !IsNight) // Dạy sáng - chiều
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                        //sheetCopy.GetRange((mergeThu + 4), 1, (mergeThu + 4), 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && !IsAfternoon && IsNight) // Dạy sáng - tối
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && IsAfternoon && IsNight) // Dạy Chiều - tối
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), dateName);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (IsMorning && !IsAfternoon && !IsNight) // Dạy Sáng
                    {
                        sheetCopy.SetCellValue(("A" + mergeThu), dateName);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).Merge();
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange(mergeThu, 1, (mergeThu + 2), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 3)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 3), 1, (mergeThu + 4), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && IsAfternoon && !IsNight) // Dạy Chiều
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 5)), dateName);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 5), 1, (mergeThu + 7), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);
                        sheetCopy.GetRange((mergeThu + 9), 1, (mergeThu + 9), 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 8)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 8), 1, (mergeThu + 9), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    else if (!IsMorning && !IsAfternoon && IsNight) // Dạy Tối
                    {
                        sheetCopy.SetCellValue(("A" + (mergeThu + 10)), dateName);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).SetFontStyle(true, System.Drawing.Color.Black, true, 16, false, false);
                        sheetCopy.GetRange((mergeThu + 10), 1, (mergeThu + 12), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignBottom);

                        sheetCopy.SetCellValue(("A" + (mergeThu + 13)), startDate.ToString("dd/MM/yyyy"));
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).Merge();
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).SetFontStyle(false, System.Drawing.Color.Black, true, 9, false, false);
                        sheetCopy.GetRange((mergeThu + 13), 1, (mergeThu + 14), 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);

                        mergeThu += 15;
                    }
                    #endregion


                    if (n == 7 && dateName.Contains("CN"))
                    {
                        var checkSunday = lstResult.Where(x => x.ScheduleDate == startDate).Select(x => x.Section)
                                                    .Distinct().OrderBy(x => x).ToList();
                        if (checkSunday.Count() == 0)
                        {
                            for (int p = 95; p <= 109; p++)
                            {
                                sheetCopy.SetRowHeight(p, 0);
                            }
                            continue;
                        }
                    }

                    #region // fill buoi hoc
                    for (int j = 0; j < lstSectionID.Count; j++)//for buoi hoc
                    {
                        if (IsMorning == false && lstSectionID[j] == 1)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        if (IsAfternoon == false && lstSectionID[j] == 2)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        if (IsNight == false && lstSectionID[j] == 3)
                        {
                            for (int p = 1; p <= 5; p++)
                            {
                                sheetCopy.SetRowHeight(StartRow, 0);
                                StartRow++;
                            }
                            continue;
                        }

                        for (int k = 1; k < 6; k++)//for tiet hoc co dinh 5 tiet
                        {
                            objTSVM = lstResult.FirstOrDefault(p => p.ScheduleDate == startDate
                                                            && p.Section == lstSectionID[j]
                                                            && p.SubjectOrder == k);
                            if (objTSVM != null)
                            {
                                //Mon
                                sheetCopy.SetCellValue(StartRow, 4, !string.IsNullOrEmpty(objTSVM.AssignSubjectName) ? objTSVM.AssignSubjectName : objTSVM.SubjectName);
                                //Lop
                                sheetCopy.SetCellValue(StartRow, 5, objTSVM.ClassName);
                                //Tiet PPCT
                                sheetCopy.SetCellValue(StartRow, 6, objTSVM.TeachingScheduleOrder);
                                //Ten bai day
                                if (objTSVM.AssignSubjectID.HasValue)
                                {
                                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                    && p.AssignSubjectID == objTSVM.AssignSubjectID
                                    && p.EducationLevelID == objTSVM.EducationLevelID
                                    && p.ClassID == objTSVM.ClassID
                                    && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                                }
                                else
                                {
                                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTSVM.SubjectID
                                    && p.AssignSubjectID.HasValue == false
                                    && p.ClassID == objTSVM.ClassID
                                    && p.EducationLevelID == objTSVM.EducationLevelID
                                    && p.DistributeProgramOrder == objTSVM.TeachingScheduleOrder);
                                }
                                sheetCopy.SetCellValue(StartRow, 7, objDP != null ? objDP.LessonName : "");
                                //Chuan bi
                                if (!string.IsNullOrEmpty(objTSVM.TeachingUtensil))
                                    description += objTSVM.TeachingUtensil;

                                if (objTSVM.Number > 0)
                                {
                                    description = !string.IsNullOrEmpty(description) ? (description + ". Số lượng: " + objTSVM.Number) : ("Số lượng: " + objTSVM.Number);
                                }

                                if (objTSVM.InRoom.HasValue && objTSVM.InRoom.Value == true)
                                {
                                    description = !string.IsNullOrEmpty(description) ? (description + ". Có ở phòng thiết bị") : ("Có ở phòng thiết bị");
                                }

                                if (objTSVM.Status != 0)
                                {
                                    if (objTSVM.Status == 1)
                                        description = !string.IsNullOrEmpty(description) ? (description + ". " + Res.Get("Offset_Teaching")) : (Res.Get("Offset_Teaching"));
                                    else
                                        description = !string.IsNullOrEmpty(description) ? (description + ". " + Res.Get("Cease_Teaching")) : (Res.Get("Cease_Teaching"));
                                }
                                sheetCopy.SetCellValue(StartRow, 8, description);
                                description = string.Empty;
                            }
                            StartRow++;
                        }
                    }
                    #endregion

                    startDate = startDate.AddDays(1);
                }

                sheetCopy.Name = lstWeekDefault[i].WeekDefaultID.Replace("/", "").Replace("*", "-");

                IsMorning = true;
                IsAfternoon = true;
                IsNight = true;
                StartRow = 5;
            }
            var objtmp = lstTeachScheduleApproval.OrderByDescending(p=>p.CreateDate).FirstOrDefault();
            if (objtmp != null)
            {
                dateApproval = string.Format("Ngày {0} tháng {1} năm {2}", objtmp.CreateDate.Day, objtmp.CreateDate.Month, objtmp.CreateDate.Year);    
            }
            sheetCopy.SetCellValue("G110", dateApproval);
            if (TypeExport == 1)
            {
                sheetFirst.Delete();
                sheetInfo.Delete();
            }

            sheetContent.Delete();
            #endregion

            string fileName = string.Format("LichBaoGiang_[{0}]_[{1}].xls", Utils.Utils.StripVNSignAndSpace(objEmployee.FullName), lstFromTo[0].Replace("/", ""));
            return oBook.ToStream();
        }

        #endregion

        #region report for teacher
        [SMAS.Web.Filter.ActionAudit]
        public FileResult ExportFileForTeacher()
        {
            try
            {
                int schoolVM = SMAS.Business.Common.Utils.GetInt(Request["schoolVM"]);
                int academicVM = SMAS.Business.Common.Utils.GetInt(Request["academicVM"]);
                int teacherIDVM = SMAS.Business.Common.Utils.GetInt(Request["teacherIDVM"]);
                int statusVM = SMAS.Business.Common.Utils.GetInt(Request["statusVM"]);

                string fromWeekIDVM = Request["fromWeekIDVM"];
                string toWeekIDVM = Request["toWeekIDVM"];


                Stream excel = ExportExcelTeacher(schoolVM, academicVM, teacherIDVM, statusVM, fromWeekIDVM, toWeekIDVM);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                result.FileDownloadName = Utils.Utils.StripVNSignAndSpace("GV_THCS_TinhHinhNhapLichBaoGiang.xls");
                return result;
            }
            catch
            {
                //Do nothing
            }
            return null;
        }

        public Stream ExportExcelTeacher(int schoolVM, int academicVM, int teacherIDVM, int statusVM, string fromWeekIDVM, string toWeekIDVM)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/GV_THCS_TinhHinhNhapLichBaoGiang.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet1 = oBook.GetSheet(1);

            #region lấy danh sách fill excel
            int schoolID;
            int academicID;
            List<ForTeacherViewModel> listResult = new List<ForTeacherViewModel>();
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                schoolID = schoolVM;
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == academicVM) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                academicID = academic;
            }
            else
            {
                schoolID = _globalInfo.SchoolID.Value;
                academicID = _globalInfo.AcademicYearID.Value;
            }

            List<int> lstEducationID = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            List<int> lstClassEducation = ClassProfileBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID && x.IsActive == true && lstEducationID.Contains(x.EducationLevelID)).Select(x => x.ClassProfileID).ToList();

            // danh sách lịch báo giảng
            List<TeachingSchedule> lstTS = TeachingScheduleBusiness.All.Where(x => x.AcademicYearID == academicID && x.SchoolID == schoolID).ToList();
            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
            {
                if (lstClassEducation.Count != 0)
                    lstTS = lstTS.Where(x => lstClassEducation.Contains(x.ClassID)).ToList();
                else
                    lstTS = new List<TeachingSchedule>();
            }

            List<TeachingSchedule> lstTSTemp = new List<TeachingSchedule>();

            // danh sách phê duyệt
            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    //{"TeacherID",teacherID},
                    //{"FromDate", fromDate},
                    {"AcademicYearID", academicID},
                    //{"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };

            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                SearchInfoTeachingScheduleApproval["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;

            if (teacherIDVM != 0)
                SearchInfoTeachingScheduleApproval["TeacherID"] = teacherIDVM;

            List<TeachingScheduleApproval> lstApproval = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)
            .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();

            // danh sách giáo viên
            List<Employee> lstE = new List<Employee>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsSuperVisingDeptRole)
            {
                lstE = EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                    (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList();
            }
            else
            {
                List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()).ToList().Where(x => x.HeadMasterID != null).ToList();
                lstSf = lstSf.Where(x => x.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                //.Where(o => o.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString())).ToList();
                if (lstSf.Count > 0)
                {
                    lstE = (from e in EmployeeBusiness.All.Where(x => x.SchoolID == schoolID && x.IsActive == true && x.EmploymentStatus == 1 &&
                        (x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || x.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)).ToList()
                            join sf in lstSf on e.SchoolFacultyID equals sf.SchoolFacultyID
                            select e).ToList();
                }
            }

            // danh sách tuần của trường
            List<SchoolWeek> lstSW = SchoolWeekBusiness.All.Where(x => x.SchoolID == schoolID && x.AcademicYearID == academicID).ToList();

            // Tìm theo giáo viên
            if (teacherIDVM != 0)
            {
                lstE = lstE.Where(x => x.EmployeeID == teacherIDVM).ToList();
                lstTS = lstTS.Where(x => x.TeacherID == teacherIDVM).ToList();
            }

            int numberDate = 0;
            DateTime fromDate1 = DateTime.Now;
            //Xét bao nhiêu tuần
            if (fromWeekIDVM != "" && toWeekIDVM != "")
            {
                string[] fromDate = fromWeekIDVM.Split('*');
                string[] toDate = toWeekIDVM.Split('*');

                fromDate1 = Convert.ToDateTime(fromDate[0]);
                DateTime fromDate2 = Convert.ToDateTime(toDate[0]);
                DateTime toDate1 = Convert.ToDateTime(fromDate[1]);

                TimeSpan difference = fromDate2 - fromDate1;
                double days = difference.TotalDays / 7;
                numberDate = Convert.ToInt32(days + 1.0);
            }

            // load View Nếu có 4 giáo viên và 3 tuần thì số record là 4*3

            ForTeacherViewModel objResult = null;
            int countFirst = 0;
            DateTime a = DateTime.Now;
            DateTime b = DateTime.Now;
            for (int i = 0; i < numberDate; i++)
            {
                if (countFirst == 0)
                {
                    a = fromDate1;
                    b = fromDate1.AddDays((i + 1) * 6);
                }
                foreach (Employee itemID in lstE)
                {
                    int lessionOfTeacher = 0;

                    lessionOfTeacher = lstTS.Where(x => x.ScheduleDate <= b && x.ScheduleDate >= a && x.TeacherID == itemID.EmployeeID).Count();

                    objResult = new ForTeacherViewModel();
                    objResult.teacherID = itemID.EmployeeID;
                    objResult.teacherName = itemID.FullName;
                    objResult.Week = (a.ToString("dd/MM/yy")) + " - " + b.ToString("dd/MM/yy");
                    objResult.numberOfLession = lessionOfTeacher;

                    var objApproval = lstApproval.Where(x => x.TeacherID == itemID.EmployeeID && x.FromDate.Date == a.Date).OrderByDescending(x => x.TeachingScheduleApprovalID).FirstOrDefault();

                    string ApprovalStatus = "Hủy phê duyệt";
                    objResult.statusApproval = 5;
                    if (lessionOfTeacher == 0)
                    {
                        objResult.Status = "Chưa lên lịch";
                        objResult.statusApproval = 2;
                    }
                    else if (objApproval == null)
                    {
                        objResult.Status = "Chưa phê duyệt";
                        objResult.statusApproval = 4;
                    }
                    else
                    {
                        if (objApproval.Status == LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL)
                        {
                            ApprovalStatus = "Đã phê duyệt";
                            objResult.statusApproval = 3;
                        }
                        if (objApproval.EmployeeID == 0)
                            objResult.Status = string.Format("{0}  - bởi Quản trị trường lúc {1}", ApprovalStatus, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        else
                        {
                            var employeeEntity = lstE.Where(x => x.EmployeeID == objApproval.EmployeeID).FirstOrDefault();
                            objResult.Status = string.Format("{0} - bởi {1} lúc {2}", ApprovalStatus, employeeEntity.FullName, objApproval.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        }
                    }

                    objResult.orderByDate = a;
                    objResult.isSuperVising = Session["CheckLevel"].ToString();
                    listResult.Add(objResult);
                }
                a = b.AddDays(1);
                b = a.AddDays(6);
                countFirst++;
            }

            if (statusVM != 0)
            {
                if (statusVM == 1)
                    listResult = listResult.Where(x => x.statusApproval == 3 || x.statusApproval == 4 || x.statusApproval == 5).ToList();
                else if (statusVM == 2)
                    listResult = listResult.Where(x => x.statusApproval == 2).ToList();
                else if (statusVM == 3)
                    listResult = listResult.Where(x => x.statusApproval == 3).ToList();
                else if (statusVM == 4)
                    listResult = listResult.Where(x => x.statusApproval == 4).ToList();
                else if (statusVM == 5)
                    listResult = listResult.Where(x => x.statusApproval == 5).ToList();
            }

            #endregion

            listResult = listResult.OrderByDescending(x => x.orderByDate).ThenBy(x => x.teacherName).ToList();

            // fill du lieu
            int startRow = 7;
            int stt = 0;
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            sheet1.SetCellValue("A2", school.SchoolName);
            sheet1.SetCellValue("A4", "Từ tuần " + fromWeekIDVM.Replace('*', '-') + " đến " + toWeekIDVM.Replace('*', '-'));
            foreach (var item in listResult)
            {
                stt++;
                sheet1.SetCellValue(startRow, 1, stt);
                sheet1.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet1.SetCellValue(startRow, 2, item.Week);
                sheet1.SetCellValue(startRow, 3, item.teacherName);
                sheet1.SetCellValue(startRow, 4, item.numberOfLession);
                sheet1.GetRange(startRow, 4, startRow, 4).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet1.SetCellValue(startRow, 5, item.Status);
                sheet1.GetRange(startRow, 5, startRow, 5).WrapText();

                sheet1.SetRowHeight(startRow, 31.5);

                startRow++;
            }
            //sheet1.GetRange(startRow, 1, startRow, 10).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet1.SetColumnWidth(2, 18.43);
            sheet1.GetRange(7, 1, startRow - 1, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet1.GetRange(7, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            sheet1.FitAllColumnsOnOnePage = true;
            //Xoa sheet template
            return oBook.ToStream();
        }
        #endregion

        #region hàm lấy dữ liệu bảng khác
        private bool CheckPermissionButtonApproval(int teacherID, int schoolID, int academic)
        {
            EmployeeBusiness.SetAutoDetectChangesEnabled(false);
            SchoolFacultyBusiness.SetAutoDetectChangesEnabled(false);

            ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = false;
            if (_globalInfo.IsAdmin || _globalInfo.IsAdminSchoolRole || (_globalInfo.EmployeeID.HasValue && UtilsBusiness.HasEmployeeAdminPermission(_globalInfo.EmployeeID.Value)))
            {
                ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = true;
                return true;
            }
            else if (_globalInfo.EmployeeID.HasValue)
            {
                IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>() { 
                    {"CurrentSchoolID",schoolID},
                    {"EmployeeID",teacherID},
                    {"AcademicYearID",academic}
                };
                var employeeEntity = EmployeeBusiness.Search(SearchInfoEmployee).FirstOrDefault();
                if (employeeEntity != null && employeeEntity.SchoolFacultyID.HasValue)
                {
                    IDictionary<string, object> SearchInfoSchoolFaculty = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    {"SchoolFacultyID",employeeEntity.SchoolFacultyID.Value}
                };
                    var schoolFacultyEntity = SchoolFacultyBusiness.Search(SearchInfoSchoolFaculty).FirstOrDefault();
                    if (schoolFacultyEntity != null && !string.IsNullOrEmpty(schoolFacultyEntity.HeadMasterID) && schoolFacultyEntity.HeadMasterID.Contains(_globalInfo.EmployeeID.Value.ToString()))
                    {
                        ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_PERMISSION_APPROVAL] = true;
                        return true;
                    }
                }
            }
            return false;
        }

        private bool GetTeachingScheduleApprovalCustom(string fromDate, int teacherID, int schoolID, int academic)
        {
            bool result = false;
            ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVALED] = false;
            ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = true;
            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    {"TeacherID",teacherID},
                    {"FromDate", fromDate},
                    {"AcademicYearID",academic},
                    
                };

            if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
            {
                SearchInfoTeachingScheduleApproval["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;
            }

            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var objResult = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)
                .OrderByDescending(x => x.TeachingScheduleApprovalID).FirstOrDefault();
            if (objResult != null)
            {
                string msg = string.Empty;
                string statusString = "Đã hủy phê duyệt";
                if (objResult.Status == LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    statusString = "Đã phê duyệt";
                    ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVALED] = true;
                    result = true;
                }

                if (objResult.EmployeeID == 0)
                {
                    msg = string.Format("{0}  - bởi Quản trị trường lúc {1}", statusString, objResult.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                    ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = msg;
                }
                else
                {
                    IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>() { 
                        {"CurrentSchoolID",schoolID},
                        {"EmployeeID",objResult.EmployeeID},
                        {"AcademicYearID",academic}
                    };
                    var employeeEntity = EmployeeBusiness.Search(SearchInfoEmployee).FirstOrDefault();
                    if (employeeEntity != null)
                    {
                        msg = string.Format("{0} - {1} lúc {2}", statusString, employeeEntity.FullName, objResult.CreateDate.ToString("HH:mm dd/MM/yyyy"));
                        ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_LOG_APPROVAL] = msg;
                    }
                }
                if (objResult.Status == LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL)
                {
                    ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = false;
                }
            }
            return result;
        }

        private string GetDateName(string strInput)
        {
            string strOutput = string.Empty;
            switch (strInput)
            {
                case "Monday":
                    strOutput = Res.Get("Teaching_Schedule_THU_2");
                    break;
                case "Tuesday":
                    strOutput = Res.Get("Teaching_Schedule_THU_3");
                    break;
                case "Wednesday":
                    strOutput = Res.Get("Teaching_Schedule_THU_4");
                    break;
                case "Thursday":
                    strOutput = Res.Get("Teaching_Schedule_THU_5");
                    break;
                case "Friday":
                    strOutput = Res.Get("Teaching_Schedule_THU_6");
                    break;
                case "Saturday":
                    strOutput = Res.Get("Teaching_Schedule_THU_7");
                    break;
                case "Sunday":
                    strOutput = Res.Get("Message_Sunday_Label");
                    break;
            };
            return strOutput;
        }

        private string GetTeacherName(int employeeID, int schoolID, int year)
        {
            EmployeeBusiness.SetAutoDetectChangesEnabled(false);

            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
            dicToGetTeacher["CurrentSchoolID"] = schoolID;
            dicToGetTeacher["EmployeeID"] = employeeID;

            IQueryable<TeacherComboboxViewModel> query = (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                          where (q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)
                                                          select new TeacherComboboxViewModel
                                                          {
                                                              EmployeeID = q.EmployeeID,
                                                              DisplayName = q.FullName
                                                          });
            var techerEntity = query.FirstOrDefault();
            EmployeeBusiness.SetAutoDetectChangesEnabled(true);

            if (techerEntity != null) return techerEntity.DisplayName;
            return string.Empty;
        }

        private string GetNameToDay(string strInput)
        {
            switch (strInput)
            {
                case "Monday":
                    return "Hai";
                case "Tuesday":
                    return "Ba";
                case "Wednesday":
                    return "Tư";
                case "Thursday":
                    return "Năm";
                case "Friday":
                    return "Sáu";
                case "Saturday":
                    return "Bảy";
                case "Sunday":
                    return "CN";
            };
            return "";
        }
        #endregion

        #region phần chi tiết
        public PartialViewResult DetailOfSchedule(string WeekID, int teacherID, int YearVM, int SchoolVM)
        {
            string WeekIDDate = !string.IsNullOrEmpty(WeekID) ? WeekID : string.Empty;
            string a = WeekIDDate.Substring(0, 10);
            string b = WeekIDDate.Substring(13);
            WeekIDDate = a + "*" + b;
            //int SectionID = !string.IsNullOrEmpty(frm["SectionID"]) ? Int32.Parse(frm["SectionID"]) : 0;
            int SectionID = 0;
            int TeacherID = 0; Int32.TryParse(teacherID.ToString() ?? "0", out TeacherID);

            ViewData["teacherName"] = EmployeeBusiness.Find(teacherID).FullName;
            ViewData["fromToWeek"] = "Từ <strong>" + a + "</strong> đến <strong>" + b + "</strong>";
            ViewData["weekID"] = a + "*" + b;
            ViewData["teacherID"] = teacherID;

            int academic;
            int schoolID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == SchoolVM && x.ProvinceID == _globalInfo.ProvinceID)
                            join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == SchoolVM && x.Year == YearVM) on sp.SchoolProfileID equals ay.SchoolID
                            select ay.AcademicYearID).SingleOrDefault();
                schoolID = SchoolVM;
            }
            else
            {
                academic = _globalInfo.AcademicYearID.Value;
                schoolID = _globalInfo.SchoolID.Value;
            }

            ViewData["schoolID"] = schoolID;
            ViewData["academic"] = academic;

            List<string> lstFromTo = WeekIDDate.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            CheckPermissionButtonApproval(TeacherID, schoolID, academic);
            ViewData[LookUpSchedulesConstants.TEACHING_SCHEDULE_SHOW_APPROVAL] = true;
            GetTeachingScheduleApprovalCustom(fromDate, TeacherID, schoolID, academic);

            #region Nội dung báo giảng
            List<TeachingScheduleViewModel> lstResult = new List<TeachingScheduleViewModel>();

            #region [Get lstResult]
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",academic},
                {"SchoolID",schoolID},
                {"lstFromTo",lstFromTo},
                {"SectionID",SectionID}
            };
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                lstResult = (from t in TeachingScheduleBusiness.Search(dic)
                             join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                             join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                             join asc in AssignSubjectConfigBusiness.All on t.AssignSubjectID equals asc.AssignSubjectConfigID
                             into l
                             from l1 in l.Where(xx => xx.SubjectID == t.SubjectID).DefaultIfEmpty()
                             where c.IsActive.Value //&& sc.AppliedLevel == _globalInfo.AppliedLevel
                             && t.TeacherID == TeacherID
                             select new TeachingScheduleViewModel
                             {
                                 ScheduleDate = t.ScheduleDate,
                                 SchoolWeekID = t.SchoolWeekID,
                                 Section = t.SectionID,
                                 SubjectOrder = t.SubjectOrder,
                                 SubjectID = t.SubjectID,
                                 SubjectName = sc.SubjectName,
                                 ClassID = t.ClassID,
                                 ClassName = c.DisplayName,
                                 TeachingScheduleOrder = t.TeachingScheduleOrder,
                                 TeachingUtensil = t.TeachingUtensil,
                                 Number = t.Quantity,
                                 InRoom = t.InRoom,
                                 IsSelfMade = t.IsSelfMade,
                                 EducationLevelID = c.EducationLevelID,
                                 TeachingScheduleID = t.TeachingScheduleID,
                                 AssignSubjectID = t.AssignSubjectID,
                                 AssignSubjectName = l1 != null ? l1.AssignSubjectName : "",
                                 Status = t.Status
                             }).ToList();
            }
            else
            {
                lstResult = (from t in TeachingScheduleBusiness.Search(dic)
                             join c in ClassProfileBusiness.All on t.ClassID equals c.ClassProfileID
                             join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                             join asc in AssignSubjectConfigBusiness.All on t.AssignSubjectID equals asc.AssignSubjectConfigID
                             into l
                             from l1 in l.Where(xx => xx.SubjectID == t.SubjectID).DefaultIfEmpty()
                             where c.IsActive.Value && sc.AppliedLevel == _globalInfo.AppliedLevel
                             && t.TeacherID == TeacherID
                             select new TeachingScheduleViewModel
                             {
                                 ScheduleDate = t.ScheduleDate,
                                 SchoolWeekID = t.SchoolWeekID,
                                 Section = t.SectionID,
                                 SubjectOrder = t.SubjectOrder,
                                 SubjectID = t.SubjectID,
                                 SubjectName = sc.SubjectName,
                                 ClassID = t.ClassID,
                                 ClassName = c.DisplayName,
                                 TeachingScheduleOrder = t.TeachingScheduleOrder,
                                 TeachingUtensil = t.TeachingUtensil,
                                 Number = t.Quantity,
                                 InRoom = t.InRoom,
                                 IsSelfMade = t.IsSelfMade,
                                 EducationLevelID = c.EducationLevelID,
                                 TeachingScheduleID = t.TeachingScheduleID,
                                 AssignSubjectID = t.AssignSubjectID,
                                 AssignSubjectName = l1 != null ? l1.AssignSubjectName : "",
                                 Status = t.Status
                             }).ToList();
            }
            #endregion

            TeachingScheduleViewModel objTS = null;

            IDictionary<string, object> dicDP = new Dictionary<string, object>()
            {
                {"AcademicYearID",academic},
                {"SchoolID",schoolID}
                //{"SchoolWeekID",weekID}
            };
            List<DistributeProgram> lstDistributeProgram = new List<DistributeProgram>();
            DistributeProgram objDP = null;
            lstDistributeProgram = DistributeProgramBusiness.Search(dicDP).ToList();
            for (int i = 0; i < lstResult.Count; i++)
            {
                objTS = lstResult[i];
                if (objTS.AssignSubjectID.HasValue)
                {
                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTS.SubjectID
                    && p.AssignSubjectID == objTS.AssignSubjectID
                    && p.EducationLevelID == objTS.EducationLevelID
                    && p.ClassID == objTS.ClassID
                        //&& p.SchoolWeekID == objTS.SchoolWeekID
                    && p.DistributeProgramOrder == objTS.TeachingScheduleOrder);
                }
                else
                {
                    objDP = lstDistributeProgram.FirstOrDefault(p => p.SubjectID == objTS.SubjectID
                    && p.AssignSubjectID.HasValue == false
                    && p.ClassID == objTS.ClassID
                    && p.EducationLevelID == objTS.EducationLevelID
                        //&& p.SchoolWeekID == objTS.SchoolWeekID
                    && p.DistributeProgramOrder == objTS.TeachingScheduleOrder);
                }


                if (objDP != null)
                {
                    objTS.LessonName = objDP.LessonName;
                }
            }

            ViewData[LookUpSchedulesConstants.LIST_TEACHING_SCHEDULE] = lstResult;
            //tinh ra cac thu theo tuan
            //SchoolWeek objSW = SchoolWeekBusiness.Find(weekID);
            DateTime input = Convert.ToDateTime(fromDate); //objSW.FromDate.Value;
            int delta = DayOfWeek.Monday - input.DayOfWeek;
            List<DateName> lstDate = new List<DateName>();
            DateName objDate = null;
            DateTime valtmp = Convert.ToDateTime(fromDate); //objSW.FromDate.Value;
            DateTime toDatetime = Convert.ToDateTime(toDate);
            while (valtmp <= toDatetime)
            {
                objDate = new DateName();
                objDate.Date = input.AddDays(delta);
                objDate.Name = this.GetDateName(input.AddDays(delta).DayOfWeek.ToString()) + " (" + input.AddDays(delta).ToString("dd/MM/yyyy") + ")";
                lstDate.Add(objDate);
                valtmp = valtmp.AddDays(1);
                delta++;
            };
            ViewData[LookUpSchedulesConstants.LIST_DATE] = lstDate;
            List<int> lstSectionID = new List<int>();
            if (SectionID > 0)
            {
                lstSectionID.Add(SectionID);
            }
            else
            {
                //tinh ra so buoi lon nhat
                IDictionary<string, object> dicCP = new Dictionary<string, object>()
                {
                    {"AcademicYearID",academic},
                    {"SchoolID",schoolID},
                    {"AppliedLevel",_globalInfo.AppliedLevel}
                };

                if (Session["CheckLevel"] != "1" && Session["CheckLevel"] != "2")
                    dicCP["AppliedLevel"] = _globalInfo.AppliedLevel;

                List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicCP).ToList();
                //int maxSectionID = lstCP.Count > 0 ? lstCP.Max(p => p.Section).Value : 0;
                lstSectionID = UtilsBusiness.GetListSectionID(lstCP);
            }
            ViewData[LookUpSchedulesConstants.LIST_SECTION_GRID] = lstSectionID;
            ViewData[LookUpSchedulesConstants.TEACHER_ID] = TeacherID;

            Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>()
            {
                {"CurrentSchoolID", schoolID}
            };

            ViewData[LookUpSchedulesConstants.IS_TEACHER] = _globalInfo.EmployeeID.HasValue && _globalInfo.EmployeeID.Value > 0 &&
                                                        (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                         where (q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN) && q.EmployeeID == TeacherID
                                                         select q.EmployeeID).Any();

            this.SetViewDataPermission("TeachingSchedule", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            AcademicYear objAca = AcademicYearBusiness.Find(academic);
            DateTime datetimeNow = DateTime.Now;
            bool enabledButton = (objAca.FirstSemesterStartDate <= datetimeNow && datetimeNow < objAca.FirstSemesterEndDate)
                                    || (objAca.SecondSemesterStartDate <= datetimeNow && datetimeNow <= objAca.SecondSemesterEndDate);
            // Trong năm học, và giáo viên chính là người đang đăng nhập
            ViewData[LookUpSchedulesConstants.ENABLED_BUTTON] = enabledButton && TeacherID == _globalInfo.EmployeeID;

            #endregion

            #region Nội dung phê duyệt

            IDictionary<string, object> SearchInfoTeachingScheduleApproval = new Dictionary<string, object>() { 
                    {"SchoolID",schoolID},
                    {"TeacherID",TeacherID},
                    {"FromDate", a},
                    {"AcademicYearID",academic},
                    {"AppliedLevelID",_globalInfo.AppliedLevel.Value}
                };
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var TeachingScheduleApprovalEntities = TeachingScheduleApprovalBusiness.Search(SearchInfoTeachingScheduleApproval)

                .OrderByDescending(x => x.TeachingScheduleApprovalID).ToList();

            var employeeIDs = TeachingScheduleApprovalEntities.Where(x => x.EmployeeID > 0).Select(x => x.EmployeeID).ToList();

            var models = new List<ViewDetailApprovalViewModel>();
            if (employeeIDs.Count > 0)
            {
                var employeeEntities = EmployeeBusiness.All.Where(x => employeeIDs.Contains(x.EmployeeID)).ToList();
                ViewData["lstApprovalContent"] = TeachingScheduleApprovalEntities.Select(x => new ViewDetailApprovalViewModel
                {
                    TeachingScheduleApprovalID = x.TeachingScheduleApprovalID,
                    Status = x.Status,
                    EmployeeID = x.EmployeeID,
                    EmployeeName = employeeEntities.Where(e => e.EmployeeID == x.EmployeeID).Select(e => e.FullName).FirstOrDefault(),
                    CreatedDate = x.CreateDate,
                    Note = x.Note
                }).ToList();
            }
            else
            {
                ViewData["lstApprovalContent"] = TeachingScheduleApprovalEntities.Select(x => new ViewDetailApprovalViewModel
                {
                    TeachingScheduleApprovalID = x.TeachingScheduleApprovalID,
                    Status = x.Status,
                    EmployeeID = x.EmployeeID,
                    CreatedDate = x.CreateDate,
                    Note = x.Note
                }).ToList();
            }

            #endregion

            return PartialView("_DetailOfSchedule");
        }

        public PartialViewResult GetApprovalTeachingScheduleDetail(string WeekID, int TeacherID, int SchoolID, int Academic)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            int academic;
            int schoolID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == SchoolID && x.ProvinceID == _globalInfo.ProvinceID)
                            join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == SchoolID && x.Year == Academic) on sp.SchoolProfileID equals ay.SchoolID
                            select ay.AcademicYearID).SingleOrDefault();
                schoolID = SchoolID;
            }
            else
            {
                academic = _globalInfo.AcademicYearID.Value;
                schoolID = _globalInfo.SchoolID.Value;
            }

            string teacherName = GetTeacherName(TeacherID, schoolID, academic);

            return PartialView("_ApprovalTeachingForDetail",
                new ApprovalTeachingScheduleModel()
                {
                    DisplayNote = string.Format("Thực hiện phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    WeekIDDate = WeekID,
                    StrFromDate = fromDate,
                    TeacherID = TeacherID,
                    TeacherName = teacherName,
                    SchoolID = schoolID,
                    Year = academic
                });
        }

        public PartialViewResult GetApprovalCancelTeachingScheduleDetail(string WeekID, int TeacherID, int SchoolID, int Academic)
        {
            List<string> lstFromTo = WeekID.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo[1];

            int academic;
            int schoolID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == SchoolID && x.ProvinceID == _globalInfo.ProvinceID)
                            join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == SchoolID && x.Year == Academic) on sp.SchoolProfileID equals ay.SchoolID
                            select ay.AcademicYearID).SingleOrDefault();
                schoolID = SchoolID;
            }
            else
            {
                academic = _globalInfo.AcademicYearID.Value;
                schoolID = _globalInfo.SchoolID.Value;
            }

            string teacherName = GetTeacherName(TeacherID, schoolID, academic);

            return PartialView("_ApprovalCancelTeachingForDetail",
                new ApprovalCancelTeachingScheduleModel()
                {
                    DisplayNote = string.Format("Thực hiện hủy phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    StrFromDate = fromDate,
                    WeekIDDate = WeekID,
                    TeacherID = TeacherID,
                    TeacherName = teacherName,
                    SchoolID = schoolID,
                    Year = academic
                });
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalTeachingScheduleDetail(ApprovalTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 510)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }

            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
            {
                SchoolID = model.SchoolID,
                AcademicYearID = model.Year,
                AppliedLevelID = _globalInfo.AppliedLevel.Value,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                TeacherID = model.TeacherID,
                Status = LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL,
                SchoolWeekID = 0,
                FromDate = Convert.ToDateTime(model.StrFromDate),
                Note = model.Note,
                EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
            });
            TeachingScheduleApprovalBusiness.Save();
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(true);
            return Json(new JsonMessage("Lưu thành công"));
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalCancelTeachingScheduleDetail(ApprovalCancelTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 510)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
            {
                SchoolID = model.SchoolID,
                AcademicYearID = model.Year,
                AppliedLevelID = _globalInfo.AppliedLevel.Value,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                TeacherID = model.TeacherID,
                SchoolWeekID = 0,
                FromDate = Convert.ToDateTime(model.StrFromDate),
                Status = LookUpSchedulesConstants.TEACHING_SCHEDULE_REJECT,
                Note = model.Note,
                EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
            });
            TeachingScheduleApprovalBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }

        #endregion

        #region phê và hủy phê cho grid teacher
        public PartialViewResult GetApprovalTeachingSchedule(string WeekID, string TeacherID,
                                                            string FromWeekIDVM, string ToWeekIDVM,
                                                            int YearVM, int SchoolVM)
        {
            List<string> lstFromTo = FromWeekIDVM.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            List<string> lstFromTo1 = ToWeekIDVM.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo1[1];
            string teacherName = "";

            List<string> FromDate = JsonConvert.DeserializeObject<List<string>>(WeekID);
            FromDate = FromDate.Select(x => x.Substring(0, 10)).ToList();
            List<int> Teacher = JsonConvert.DeserializeObject<List<int>>(TeacherID);

            int academic;
            int schoolID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == SchoolVM && x.ProvinceID == _globalInfo.ProvinceID)
                            join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == SchoolVM && x.Year == YearVM) on sp.SchoolProfileID equals ay.SchoolID
                            select ay.AcademicYearID).SingleOrDefault();
                schoolID = SchoolVM;
            }
            else
            {
                academic = _globalInfo.AcademicYearID.Value;
                schoolID = _globalInfo.SchoolID.Value;
            }

            for (int i = 0; i < Teacher.Count(); i++)
            {
                teacherName = teacherName + "," + GetTeacherName(Teacher[i], academic, schoolID);
            }

            teacherName = teacherName.Substring(1);

            return PartialView("_ApprovalTeachingSchedule",
                new ApprovalTeachingScheduleModel()
                {
                    DisplayNote = string.Format("Thực hiện phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    lstFromDate = FromDate,
                    StrFromDate = fromDate,
                    lstTeacher = Teacher,
                    TeacherName = teacherName,
                    SchoolID = schoolID,
                    Year = academic
                });
        }

        public PartialViewResult GetApprovalCancelTeachingSchedule(string WeekID, string TeacherID,
                                                                    string FromWeekIDVM, string ToWeekIDVM,
                                                                    int YearVM, int SchoolVM)
        {
            List<string> lstFromTo = FromWeekIDVM.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            List<string> lstFromTo1 = ToWeekIDVM.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
            string fromDate = lstFromTo[0];
            string toDate = lstFromTo1[1];
            string teacherName = "";

            List<string> FromDate = JsonConvert.DeserializeObject<List<string>>(WeekID);
            FromDate = FromDate.Select(x => x.Substring(0, 10)).ToList();
            List<int> Teacher = JsonConvert.DeserializeObject<List<int>>(TeacherID);

            int academic;
            int schoolID;
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == SchoolVM && x.ProvinceID == _globalInfo.ProvinceID)
                            join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == SchoolVM && x.Year == YearVM) on sp.SchoolProfileID equals ay.SchoolID
                            select ay.AcademicYearID).SingleOrDefault();
                schoolID = SchoolVM;
            }
            else
            {
                academic = _globalInfo.AcademicYearID.Value;
                schoolID = _globalInfo.SchoolID.Value;
            }

            for (int i = 0; i < Teacher.Count(); i++)
            {
                teacherName = teacherName + "," + GetTeacherName(Teacher[i], academic, schoolID);
            }

            teacherName = teacherName.Substring(1);

            return PartialView("_ApprovalCancelTeachingSchedule",
                new ApprovalCancelTeachingScheduleModel()
                {

                    DisplayNote = string.Format("Thực hiện hủy phê duyệt lịch báo giảng từ {0} đến {1} của giáo viên {2}", fromDate, toDate, teacherName),
                    StrFromDate = fromDate,
                    lstFromDate = FromDate,
                    lstTeacher = Teacher,
                    TeacherName = teacherName,
                    SchoolID = schoolID,
                    Year = academic
                });
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalTeachingSchedule(ApprovalTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 500)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }
            List<int> lstTeacherID = model.lstTeacherVM.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<string> lstFromDate = model.lstFromDateVM.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
            //List<string> lstFromDate = model.lstFromDateVM;
            //List<int> lstTeacherID = model.lstTeacherVM;


            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            for (int i = 0; i < lstTeacherID.Count(); i++)
            {
                TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
                {
                    SchoolID = model.SchoolID,
                    AcademicYearID = model.Year,
                    AppliedLevelID = _globalInfo.AppliedLevel.Value,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    TeacherID = lstTeacherID[i],
                    Status = LookUpSchedulesConstants.TEACHING_SCHEDULE_APPROVAL,
                    SchoolWeekID = 0,
                    FromDate = Convert.ToDateTime(lstFromDate[i].Substring(0, 10)),
                    Note = model.Note,
                    EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
                });
            }

            TeachingScheduleApprovalBusiness.Save();
            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(true);
            return Json(new JsonMessage("Lưu thành công"));
        }

        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ApprovalCancelTeachingSchedule(ApprovalCancelTeachingScheduleModel model)
        {
            if (model != null && model.Note != null && model.Note.Length > 510)
            {
                return Json(new JsonMessage("Cột ghi chú lớn hơn 500 ký tự.", "error"));
            }

            List<int> lstTeacherID = model.lstTeacherVM.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<string> lstFromDate = model.lstFromDateVM.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();

            TeachingScheduleApprovalBusiness.SetAutoDetectChangesEnabled(false);

            for (int i = 0; i < lstTeacherID.Count(); i++)
            {
                TeachingScheduleApprovalBusiness.Insert(new TeachingScheduleApproval()
                {
                    SchoolID = model.SchoolID,
                    AcademicYearID = model.Year,
                    AppliedLevelID = _globalInfo.AppliedLevel.Value,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    TeacherID = lstTeacherID[i],
                    SchoolWeekID = 0,
                    FromDate = Convert.ToDateTime(lstFromDate[i].Substring(0, 10)),
                    Status = LookUpSchedulesConstants.TEACHING_SCHEDULE_REJECT,
                    Note = model.Note,
                    EmployeeID = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0
                });
            }

            TeachingScheduleApprovalBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }
        #endregion

        #region Load combobox
        public ActionResult LoadTeacher(int schoolID)
        {
            List<TeacherComboboxViewModel> lstTeacher = new List<TeacherComboboxViewModel>();
            TeacherComboboxViewModel objTeacher = new TeacherComboboxViewModel();
            objTeacher.EmployeeID = 0;
            objTeacher.DisplayName = LookUpSchedulesConstants.PLACE_ALL;
            lstTeacher.Add(objTeacher);

            if (schoolID != 0)
            {
                Dictionary<string, object> dicToGetTeacher = new Dictionary<string, object>();
                dicToGetTeacher["CurrentSchoolID"] = schoolID;
                //dicToGetTeacher["EmployeeID"] = _globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0;

                List<TeacherComboboxViewModel> query = (from q in EmployeeBusiness.Search(dicToGetTeacher)
                                                        where (q.EmploymentStatus == 1 && q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_TEACHER || q.WorkGroupTypeID == SystemParamsInFile.WORK_GROUP_TYPE_EMPLOYEE_ADMIN)
                                                        select new TeacherComboboxViewModel
                                                        {
                                                            EmployeeID = q.EmployeeID,
                                                            DisplayName = q.FullName + " - " + q.EmployeeCode + " - " + q.SchoolFaculty.FacultyName
                                                        }).ToList();
                lstTeacher.AddRange(query);
            }

            ViewData[LookUpSchedulesConstants.LIST_TEACHER] = lstTeacher;

            return PartialView("_TeacherCombobox");
        }

        public JsonResult LoadEducationLevel(int schoolID)
        {
            List<EducationLevel> lstEducation = new List<EducationLevel>();
            if (schoolID == 0)
            {
                return Json(lstEducation.Select(u => new SelectListItem { Value = u.EducationLevelID.ToString(), Text = u.Resolution, Selected = false }).ToList());
            }
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            int educationGrade = school.EducationGrade;
            if (educationGrade == 31 || educationGrade == 7)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 1 || x.Grade == 2 || x.Grade == 3).ToList();
            else if (educationGrade == 6)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 2 || x.Grade == 3).ToList();
            else if (educationGrade == 3)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 2 || x.Grade == 1).ToList();
            else if (educationGrade == 4)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 3).ToList();
            else if (educationGrade == 2)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 2).ToList();
            else if (educationGrade == 1)
                lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 1).ToList();
            // EducationLevelID, Resolution
            return Json(lstEducation.Select(u => new SelectListItem { Value = u.EducationLevelID.ToString(), Text = u.Resolution, Selected = false }).ToList());

        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId, int schoolID, int year)
        {
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> lstClass = new List<SelectListItem>();
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                if (schoolID != 0)
                {
                    int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                    join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == year) on sp.SchoolProfileID equals ay.SchoolID
                                    select ay.AcademicYearID).SingleOrDefault();
                    dicClass.Add("AcademicYearID", academic);
                    dicClass.Add("EducationLevelID", eduId);


                    var lstClassCheckNull = this.ClassProfileBusiness.SearchBySchool(schoolID, dicClass).ToList();

                    if (lstClassCheckNull.Count > 0)
                        lstClass = lstClassCheckNull.Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12).OrderBy(u => u.DisplayName).Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();
                }
            }
            else
            {
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
                dicClass.Add("EducationLevelID", eduId);
                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12).OrderBy(u => u.DisplayName).ToList()
                                                        .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                        .ToList();
            }
            return Json(lstClass);
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadSchool(int? yearCB, int? districtCB)
        {

            districtCB = districtCB == null ? 0 : districtCB;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["DistrictID"] = districtCB;
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            // tim theo nam
            IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                                x.EducationGrade != 16 &&
                                                                                                x.EducationGrade != 24);

            if (_globalInfo.IsSubSuperVisingDeptRole == true)
                lstSP = lstSP.Where(x => (x.EducationGrade == 1 || x.EducationGrade == 2 || x.EducationGrade == 3) && x.SupervisingDeptID == _globalInfo.SupervisingDeptID);


            IQueryable<SchoolProfileBO> lstSPBO = (from sp in lstSP
                                                   join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                   where sp.IsActive == true && ac.IsActive == true &&
                                                       sp.IsActive == true && ac.Year == yearCB &&
                                                       sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                   select new SchoolProfileBO
                                                   {
                                                       SchoolProfileID = sp.SchoolProfileID,
                                                       SchoolCode = sp.SchoolCode,
                                                       SchoolName = sp.SchoolName,
                                                       AcademicYearID = ac.AcademicYearID,
                                                       EducationGrade = sp.EducationGrade
                                                   }).OrderBy(o => o.SchoolName.ToLower());


            List<SelectListItem> lstSchool = new List<SelectListItem>();
            lstSchool = lstSPBO.ToList().Select(u => new SelectListItem { Value = u.SchoolProfileID.ToString(), Text = u.SchoolName, Selected = false })
                                                                .ToList();

            return Json(lstSchool);
        }

        public static long selected = 0;
        public JsonResult LoadFromToWeek(int? yearCB, int? schoolID)
        {
            List<WeekDefaultViewModel> lstWeek = new List<WeekDefaultViewModel>();
            if (schoolID != null)
            {
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == yearCB) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();

                IDictionary<string, object> dicCP = new Dictionary<string, object>();

                if (_globalInfo.IsSubSuperVisingDeptRole == true || _globalInfo.IsSuperVisingDeptRole == true)
                {
                    dicCP["AcademicYearID"] = academic;
                    dicCP["SchoolID"] = schoolID;
                }
                else
                {
                    academic = _globalInfo.AcademicYearID.Value;
                    dicCP["AcademicYearID"] = _globalInfo.AcademicYearID;
                    dicCP["SchoolID"] = _globalInfo.SchoolID;
                    dicCP["AppliedLevel"] = _globalInfo.AppliedLevel;
                }

                List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicCP).ToList();

                if (lstCP.Count > 0)
                {
                    AcademicYear academicYear = AcademicYearBusiness.Find(academic);
                    dicCP["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
                    dicCP["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
                    lstWeek = AutoGenericWeekDefaul(dicCP);
                    ListWeekDefault = AutoGenericWeekDefaul(dicCP);

                }
            }
            //ViewData[TeachingScheduleConstants.LIST_WEEKNAME] = new SelectList(lstWeek, "WeekID", "WeekName", selected);
            return Json(lstWeek);
        }

        public JsonResult LoadSection(int? ClassID, int? EducationLevelID, int schoolID, int year)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Session["CheckLevel"] == "1" || Session["CheckLevel"] == "2")
            {
                int academic = (from sp in SchoolProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolProfileID == schoolID && x.ProvinceID == _globalInfo.ProvinceID)
                                join ay in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.SchoolID == schoolID && x.Year == year) on sp.SchoolProfileID equals ay.SchoolID
                                select ay.AcademicYearID).SingleOrDefault();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academic;
                if (ClassID.HasValue)
                {
                    dic["ClassProfileID"] = ClassID;
                }
                if (EducationLevelID.HasValue)
                {
                    dic["EducationLevelID"] = EducationLevelID.Value;
                }
            }
            else
            {
                dic["SchoolID"] = _globalInfo.SchoolID.Value;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                if (ClassID.HasValue)
                {
                    dic["ClassProfileID"] = ClassID;
                }
                if (EducationLevelID.HasValue)
                {
                    dic["EducationLevelID"] = EducationLevelID.Value;
                }
            }
            IEnumerable<ClassProfile> lstClass = ClassProfileBusiness.Search(dic).ToList();
            var lstSection = new List<CalendarBusiness.ClassSection>();
            foreach (ClassProfile cl in lstClass)
            {
                var lstSectionFor = new List<CalendarBusiness.ClassSection>();
                CalendarBusiness.GetSection(cl.Section != null ? cl.Section.Value : 0, ref lstSectionFor);
                var listSectionOther = lstSectionFor.Where(o => !lstSection.Select(u => u.Section).Contains(o.Section));
                lstSection = lstSection.Union(listSectionOther).ToList();

                if (lstSection.Count >= 3)
                    break;
            }
            return Json(lstSection.OrderBy(o => o.Section));
        }
        #endregion
    }
}