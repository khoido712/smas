﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class ForTeacherViewModel
    {
        
        public int teacherID { get; set; }
        
        [DisplayName("Tuần")]
        public string Week { get; set; }

        [DisplayName("Giáo viên")]
        public string teacherName { get; set; }

        [DisplayName("Trạng thái")]
        public string Status { get; set; }

        [DisplayName("Số tiết đã lên lịch")]
        public int numberOfLession { get; set; }

        public DateTime orderByDate { get; set; }

        public string isSuperVising { get; set; }

        public int statusApproval { get; set; }
    }
}