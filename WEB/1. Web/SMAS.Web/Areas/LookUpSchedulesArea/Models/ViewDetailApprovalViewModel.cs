﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class ViewDetailApprovalViewModel
    {
        public int TeachingScheduleApprovalID { get; set; }
        public int? Status { get; set; }
        public string EmployeeName { get; set; }
        public int EmployeeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Note { get; set; }
    }
}