﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class TeachingScheduleForStudentViewModel
    {
        /// <summary>
        /// ID cua calendar Object
        /// </summary>
        public long TeachingScheduleID { get; set; }
        /// <summary>
        /// ID giáo viên
        /// </summary>
        public int TeacherID { get; set; }
        /// <summary>
        /// Nam hoc
        /// </summary>
        public int AcademicYearID { get; set; }
        /// <summary>
        /// ClassID
        /// </summary>
        public int ClassID { get; set; }
        /// <summary>
        /// ID Mon hoc duoc chon
        /// </summary>
        public int SubjectID { get; set; }
        /// <summary>
        /// ID Mon hoc phân môn duoc chon
        /// </summary>
        public int? AssignSubjectID { get; set; }
        /// <summary>
        /// Thứ
        /// </summary>
        public int DayOfWeek { get; set; }
        /// <summary>
        /// Buổi
        /// </summary>
        public int? Section { get; set; }
        /// <summary>
        /// Mau mon hoc
        /// </summary>
        public string SubjectColor { get; set; }
        /// <summary>
        /// Ten mon hoc
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// Tiết học
        /// </summary>
        public int? NumberOfPeriod { get; set; }
        /// <summary>
        /// Danh sach giao vien duoc phan cong giang day mon nay
        /// </summary>
        public List<string> TeacherNames { get; set; }

        /// <summary>
        /// Ten Giao Vien Kieu String
        /// </summary>
        public string NameTeacher { get; set; }

        /// <summary>
        /// Thong tin chi tiet giao vien 
        /// </summary>
        public string TeacherTooltip { get; set; }

        public string SubjectContent { get; set; }

        public int TeachingScheduleOrder { get; set; }

        public int EducationLevelID { get; set; }

    }
}