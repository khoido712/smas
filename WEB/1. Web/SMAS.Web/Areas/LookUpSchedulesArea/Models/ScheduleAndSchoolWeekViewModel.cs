﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.LookUpSchedulesArea.Models;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class ScheduleAndSchoolWeekViewModel
    {

        public List<TeachingSchedule> lstTeachingSchedules { get; set; }
        public List<SchoolWeek> lstSchoolWeek { get; set; }

        public ScheduleAndSchoolWeekViewModel() 
        {
            lstTeachingSchedules = new List<TeachingSchedule>();
            lstSchoolWeek = new List<SchoolWeek>();
        }

    }
}