﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class ApprovalTeachingScheduleModel
    {
        public int SchoolWeekID { get; set; }
        public int SchoolWeekOrder { get; set; }
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string Note { get; set; }
        public DateTime FromDate { get; set; }
        public string WeekIDDate { get; set; }
        public string DisplayNote { get; set; }
        public string StrFromDate { get; set; }
        public int SchoolID {get;set;}
        public int Year { get; set; }

        public List<string> lstFromDate { get; set; }
        public List<int> lstTeacher { get; set; }
        public string lstTeacherVM { get; set; }
        public string lstFromDateVM { get; set; }

    }
}