﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.LookUpSchedulesArea;
using SMAS.Models.Models;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.LookUpSchedulesArea.Models
{
    public class SearchModel
    {
        

        /// <summary>
        /// Học kỳ
        /// </summary>
        [ResourceDisplayName("Calendar_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", "lstSemester")]
        public int? Semester { get; set; }
        /// <summary>
        /// Khối
        /// </summary>
        [ResourceDisplayName("Calendar_Control_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", "lstLevel")]
        [AdditionalMetadata("PlaceHolder", "None")]
        [AdditionalMetadata("OnChange", "LoadClass(this)")]
        public int? Level { get; set; }
        /// <summary>
        /// Lớp
        /// </summary>
        [ResourceDisplayName("Calendar_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", "lstClass")]
        [AdditionalMetadata("OnChange", "LoadSection(this)")]
        [AdditionalMetadata("class", "SelectBoxClass")]
        [AdditionalMetadata("style", "opacity: 0; height: 20px;")]
        public int? ClassID { get; set; }
        /// <summary>
        /// Buổi
        /// </summary>
        [ResourceDisplayName("Calendar_Label_Section")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", "lstSection")]
        [AdditionalMetadata("class", "SelectBoxClass")]
        [AdditionalMetadata("style", "opacity: 0; height: 20px;")]
        public int? Section { get; set; }
        /// <summary>
        /// Tên giáo viên được chọn
        /// </summary>
        [ResourceDisplayName("Calendar_Label_Teacher")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("class", "InputTextStyle InputText1Style")]
        public string TeacherName { get; set; }

        [ResourceDisplayName("NutritionalNorm_Label_EffectDate")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", "lstAppliedDate")]
        [AdditionalMetadata("PlaceHolder", "None")]
        public string AppliedDate { get; set; }
        public int? CurrentPage { get; set; }

        [ResourceDisplayName("DistributeProgram_Label_Week")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookUpSchedulesConstants.FROMLISTWEEKNAME)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public string FromWeekID { get; set; }

        [ResourceDisplayName("DistributeProgram_Label_Week")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", LookUpSchedulesConstants.TOLISTWEEKNAME)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public string ToWeekID { get; set; }

        [ResourceDisplayName("ReportResultLearningByPeriod_Label_Employee")]
        public int? EmployeeID { get; set; }

        public int? YearVM { get; set; }
        public int? DistrictVM { get; set; }
        public int? SchoolVM { get; set; }
        public int? TypeReportVM { get; set; }
        public string FromWeekIDVM { get; set; }
        public string ToWeekIDVM { get; set; }
        public string WeekVM { get; set; }
        public int? DayOfWeekVM { get; set; }
        public int? StatusVM { get; set; }
        public int? EducationLevelVM { get; set; }
        public int? ClassVM { get; set; }
        public int? SectionVM { get; set; }

    }
}