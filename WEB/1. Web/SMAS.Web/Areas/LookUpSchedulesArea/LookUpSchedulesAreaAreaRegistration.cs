﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LookUpSchedulesArea
{
    public class LookUpSchedulesAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LookUpSchedulesArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LookUpSchedulesArea_default",
                "LookUpSchedulesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
