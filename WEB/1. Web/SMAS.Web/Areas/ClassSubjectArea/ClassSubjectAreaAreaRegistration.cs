﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassSubjectArea
{
    public class ClassSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassSubjectArea_default",
                "ClassSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}