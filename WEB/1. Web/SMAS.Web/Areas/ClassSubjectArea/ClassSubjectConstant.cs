﻿namespace SMAS.Web.Areas.ClassSubjectArea
{
    public class ClassSubjectConstant
    {
        //public const string LS_ = "LS_";
        public const string LS_EDUCATIONLEVEL = "LS_EDUCATIONLEVEL";

        public const string LS_CLASS = "LS_CLASS";
        public const string LS_CLASSSUBJECTOBJECT = "LS_CLASSSUBJECTOBJECT";

        public const string LS_APPLIEDTYPE = "LS_APPLIEDTYPE";
        public const string LS_ISCOMMENTING = "LS_ISCOMMENTING";
        public const string LS_FIRSTSEMESTERCOEFFICIENT = "LS_FIRSTSEMESTERCOEFFICIENT";
        public const string LS_SECONDSEMESTERCOEFFICIENT = "LS_SECONDSEMESTERCOEFFICIENT";
        public const string LS_DROPDOWNLISTCLASS = "LS_DROPDOWNLISTCLASS";

        public const int COMMITINGMARKED = 0;
        public const int COMMITINGCOMMENTED = 1;
        public const int COMMITINGCOMMENTEDMARKED = 2;

        public const string TEMP_DATA = "TEMP_DATA";

        public const string CAP1 = "CAP1";
        public const string TRUONG_CHUYEN = "TRUONG_CHUYEN";

        public const string DEFAULT_EDUCATIONlEVEL = "DEFAULT_EDUCATIONlEVEL";
        public const string DEFAULT_CLASS = "DEFAULT_CLASS";
    }
}