using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ClassSubjectArea.Models
{
    public class ClassSubjectObject
    {
        #region get from metadata
        public int? OrderInSubject { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public long ClassSubjectID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID { get; set; }

        public bool ReadAndWriteMiddleTest { get; set; }

        public bool IsEditIsCommentting { get; set; }

        #endregion get from metadata

        #region addtion

        [ResourceDisplayName("SubjectCat_Label_SubjectName1")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("SubjectCat_Label_SubjectName1")]
        public string DisplayName { get; set; }

        [ResourceDisplayName("SubjectCat_Label_Abbreviation")]
        public string Abbreviation { get; set; }

        [ResourceDisplayName("ClassSubject_Label_AppliedType")]
        public Nullable<int> AppliedType { get; set; }

        [ResourceDisplayName("ClassSubject_Label_IsCommenting")]
        public int? IsCommenting { get; set; }

        [ResourceDisplayName("ClassSubject_Label_FirstSemesterCoefficient")]
        public Nullable<int> FirstSemesterCoefficient { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SecondSemesterCoefficient")]
        public Nullable<int> SecondSemesterCoefficient { get; set; }

        [ResourceDisplayName("ClassSubject_Label_IsSpecializedSubject")]
        public Nullable<bool> IsSpecializedSubject { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekFirstSemester")]
        public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }

        [ResourceDisplayName("ClassSubject_Label_IsSecondForeignLanguage")]
        public Nullable<bool> IsSecondForeignLanguage { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekSecondSemester")]
        public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }

        [ResourceDisplayName("ClassForwarding_Label_Choice1")]
        public Nullable<bool> Choice { get; set; }

        [ResourceDisplayName("ClassSubject_Label_IsSecondForeignLanguage")]
        public bool IsForeignLanguage { get; set; }

        public string DisableCoefficient { get; set; }

        #endregion addtion
    }
}
