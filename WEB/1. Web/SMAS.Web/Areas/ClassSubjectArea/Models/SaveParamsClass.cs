﻿using SMAS.Business.BusinessObject;
namespace SMAS.Web.Areas.ClassSubjectArea.Models
{
    public class SaveParamsClass
    {
        public int[] AppliedType
        {
            get;
            set;
        }

        public int[] IsCommenting
        {
            get;
            set;
        }

        public int[] FirstSemesterCoefficient
        {
            get;
            set;
        }

        public int[] SecondSemesterCoefficient
        {
            get;
            set;
        }

        public string[] SectionPerWeekFirstSemester
        {
            get;
            set;
        }

        public string[] SectionPerWeekSecondSemester
        {
            get;
            set;
        }

        public int[] HiddenClassSubjectID
        {
            get;
            set;
        }

        public int[] HiddenSubjectID
        {
            get;
            set;
        }

        public int[] HiddenCheckOnChange
        {
            get;
            set;
        }

        public int cboEducationLevel
        {
            get;
            set;
        }

        public int cboClass
        {
            get;
            set;
        }

        public int ButtonClickType
        {
            get;
            set;
        }

        public int[] IsSecondForeignLanguage
        {
            get;
            set;
        }

        public int[] IsSpecializedSubject
        {
            get;
            set;
        }

        public int[] checkedClass
        {
            get;
            set;
        }

        public ClassSubjectBO[] ClassSubjectChangeTypes
        {
            get;
            set;
        }

        public SaveParamsClass(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
             int[] HiddenClassSubjectID, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int cboEducationLevel, int cboClass, int ButtonClickType,
            ClassSubjectBO[] ClassSubjectChangeTypes)
        {
            this.AppliedType = AppliedType;
            this.IsCommenting = IsCommenting;
            this.FirstSemesterCoefficient = FirstSemesterCoefficient;
            this.SecondSemesterCoefficient = SecondSemesterCoefficient;
            this.SectionPerWeekFirstSemester = SectionPerWeekFirstSemester;
            this.SectionPerWeekSecondSemester = SectionPerWeekSecondSemester;
            this.HiddenClassSubjectID = HiddenClassSubjectID;
            this.HiddenSubjectID = HiddenSubjectID;
            this.HiddenCheckOnChange = HiddenCheckOnChange;
            this.cboEducationLevel = cboEducationLevel;
            this.cboClass = cboClass;
            this.ButtonClickType = ButtonClickType;
            this.IsSecondForeignLanguage = IsSecondForeignLanguage;
            this.IsSpecializedSubject = IsSpecializedSubject;
            this.checkedClass = checkedClass;
            this.ClassSubjectChangeTypes = ClassSubjectChangeTypes;
        }

        public SaveParamsClass()
        {
        }
    }
}