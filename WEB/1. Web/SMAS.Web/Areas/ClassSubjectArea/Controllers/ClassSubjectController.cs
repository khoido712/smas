﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ClassSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ClassSubjectArea.Controllers
{
    public class ClassSubjectController : BaseController
    {
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        public ClassSubjectController(IClassSubjectBusiness classsubjectBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , ISchoolSubjectBusiness SchoolSubjectBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , ISummedUpRecordBusiness SummedUpRecordBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IJudgeRecordBusiness JudgeRecordBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IPupilOfSchoolBusiness PupilOfSchoolBusiness
            , IPupilAbsenceBusiness PupilAbsenceBusiness
            , IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IExemptedSubjectBusiness ExemptedSubjectBusiness
            , IPupilProfileBusiness PupilProfileBusiness)
        {
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ClassSubjectBusiness = classsubjectBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.RegisterSubjectSpecializeBusiness = RegisterSubjectSpecializeBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }

        private List<ComboObject> listEd;
        private const int MAX_COEFFICIENT = 3;

        #region Index

        public ViewResult Index()
        {
            SetViewDataPermission("ClassSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("ClassSubject", globalInfo.UserAccountID, globalInfo.IsAdmin);

            return View();
        }

        #endregion Index

        #region SetViewData

        public void SetViewData()
        {
            FillDropDownListDefault();
            List<ClassSubjectObject> newls = new List<ClassSubjectObject>();

            //IQueryable<ClassSubjectObject> res = newls.AsQueryable();
            //// Thuc hien phan trang tung phan
            //Paginate<ClassSubjectObject> paging = new Paginate<ClassSubjectObject>(res);
            //paging.page = 1;
            //paging.paginate();
            //ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT] = paging;

            ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT] = newls;
            GlobalInfo global = new GlobalInfo();

            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);

            ViewData[ClassSubjectConstant.TRUONG_CHUYEN] = sp.SchoolTypeID.HasValue ? (sp.SchoolTypeID.Value == SystemParamsInFile.SCHOOL_TYPE_TRUONG_CHUYEN) : false;
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[ClassSubjectConstant.CAP1] = globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY;



        }

        public void FillDropDownListDefault()
        {
            int defaultEducationLevel = 0;
            //ViewData[ClassSubjectConstant.LS_EDUCATIONLEVEL] = new SelectList(listEd.ToList(), "key", "value");
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[ClassSubjectConstant.LS_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
                defaultEducationLevel = lsEducationLevel.FirstOrDefault().EducationLevelID;
            }
            else
            {
                ViewData[ClassSubjectConstant.LS_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }
            ViewData[ClassSubjectConstant.DEFAULT_EDUCATIONlEVEL] = defaultEducationLevel;

            //List<ComboObject> listAppliedType = new List<ComboObject>();
            //listAppliedType.Add(new ComboObject(Res.Get("ClassSubject_Label_AppliedTypeRequiredMark"), "0"));
            //listAppliedType.Add(new ComboObject(Res.Get("ClassSubject_Label_AppliedTypeOption"), "1"));
            //listAppliedType.Add(new ComboObject(Res.Get("ClassSubject_Label_AppliedTypeOptionAndPlus"), "2"));
            //listAppliedType.Add(new ComboObject(Res.Get("ClassSubject_Label_AppliedTypeJob"), "3"));
            ViewData[ClassSubjectConstant.LS_APPLIEDTYPE] = CommonList.AppliedTypeSubject1();

            //List<ComboObject> listIsCommenting = new List<ComboObject>();
            //listIsCommenting.Add(new ComboObject(Res.Get("ClassSubject_Label_CommitingMarked"), "0"));
            //listIsCommenting.Add(new ComboObject(Res.Get("ClassSubject_Label_CommitingCommented"), "1"));
            //listIsCommenting.Add(new ComboObject(Res.Get("ClassSubject_Label_CommitingCommentedMarked"), "2"));
            ViewData[ClassSubjectConstant.LS_ISCOMMENTING] = CommonList.IsCommentingSubject1();

            //
            List<ComboObject> listFirstSemesterCoefficient = new List<ComboObject>();
            for (int i = 0; i <= MAX_COEFFICIENT; i++)
            {
                listFirstSemesterCoefficient.Add(new ComboObject(i.ToString(), i.ToString()));
            }

            ViewData[ClassSubjectConstant.LS_FIRSTSEMESTERCOEFFICIENT] = listFirstSemesterCoefficient;

            //
            List<ComboObject> listSecondSemesterCoefficient = new List<ComboObject>();
            for (int i = 0; i <= MAX_COEFFICIENT; i++)
            {
                listSecondSemesterCoefficient.Add(new ComboObject(i.ToString(), i.ToString()));
            }

            ViewData[ClassSubjectConstant.LS_SECONDSEMESTERCOEFFICIENT] = listSecondSemesterCoefficient;

            // IQueryable<ClassProfile> lsClass = getAllClass(int.Parse(listEd.FirstOrDefault().key));
            // ViewData[ClassSubjectConstant.LS_DROPDOWNLISTCLASS] = new SelectList(lsClass);
            ViewData[ClassSubjectConstant.LS_DROPDOWNLISTCLASS] = new SelectList(new string[] { });
        }

        #endregion setviewdata

        #region SelectClassSubjectObject

        [GridAction]
        public ActionResult SelectClassSubjectObject()
        {
            SetViewData();
            ////lay ra default  khoi
            //int DefaultEducationLevelID = int.Parse(listEd.FirstOrDefault().key);
            ////lay ra default lop
            //int DefaultClassID = getAllClass(DefaultEducationLevelID).FirstOrDefault().ClassProfileID;
            //GetGrid(DefaultClassID);
            //Paginate<ClassSubjectObject> paging = (Paginate<ClassSubjectObject>)ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT];
            List<ClassSubjectObject> paging = (List<ClassSubjectObject>)ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT];

            return View(new GridModel(paging));
        }

        #endregion SelectClassSubjectObject

        #region Fill Dropdown List

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClass(int? EducationLevelId)
        {
            return _GetClass(EducationLevelId);
        }

        private JsonResult _GetClass(int? ID)
        {
            if (ID.HasValue)
            {
                List<ClassProfile> lsClass = getAllClass(ID.Value).ToList();
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<ClassProfile> lsClass = new List<ClassProfile>();
                ViewData[ClassSubjectConstant.LS_DROPDOWNLISTCLASS] = new SelectList(lsClass);
                return Json(new SelectList(lsClass), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// lay tat ca lop theo khoi
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        private IQueryable<ClassProfile> getAllClass(int EducationLevelID)
        {
            var globalInfo = new GlobalInfo();
            int? academicYearID = globalInfo.AcademicYearID;
            int? schoolID = globalInfo.SchoolID;

            IDictionary<string, object> dic = new Dictionary<string, object>();

            //dic.Add("SchoolID", 4);
            dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("SchoolID", schoolID.Value);
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchByAcademicYear(academicYearID.Value, dic);
            lsClass = lsClass.OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName);
            return lsClass;

        }

        #endregion Fill Dropdown List

        #region ChangeClassSubjectGrid
        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeClassSubjectGrid(int? EducationLevelID, int? ClassID)
        {
            SetViewDataPermission("ClassSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            CheckPermissionForAction(ClassID, "ClassProfile");
            if (!ClassID.HasValue || !EducationLevelID.HasValue)
            {
                SetViewData();
            }
            else
            {
                GetGrid(EducationLevelID.Value, ClassID.Value);
            }
            return PartialView("ClassSubjectFormGrid");
        }
        public PartialViewResult CallPartialSearch()
        {
            FillDropDownListDefault();
            return PartialView("_Search");
        }
        #endregion ChangeClassSubjectGrid

        #region Get Gird

        public void GetGrid(int EducationLevelID, int id)
        {
            // Hệ thống hiển thị giao diện thông tin khai báo môn học cho lớp và thực hiện gọi hàm Search
            //trong nghiệp vụ SchoolSubject truyền vào SchoolID, AcademicYearID lấy từ Session,
            //lấy lên các môn học khai báo cho trường để fill các thông tin mặc định lên Gridview
            Dictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[ClassSubjectConstant.CAP1] = globalInfo.AppliedLevel == 1;

            int? AcademicYearID = globalInfo.AcademicYearID;
            int? SchoolID = globalInfo.SchoolID;
            dic["SchoolID"] = SchoolID.Value;
            dic["AcademicYearID"] = AcademicYearID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = globalInfo.AppliedLevel.Value;
            List<SchoolSubject> lsAllSchoolSubject = SchoolSubjectBusiness.Search(dic).Where(o => o.SubjectCat.IsActive).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            //lop duoc chon co cac mon
            List<ClassSubject> lsClassSubject = ClassSubjectBusiness.SearchByClass(id, dic).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            List<ClassSubjectObject> lsClassSubjectObject = new List<ClassSubjectObject>();
            foreach (SchoolSubject item in lsAllSchoolSubject)
            {
                ClassSubjectObject newObj = new ClassSubjectObject();
                newObj.Abbreviation = item.SubjectCat.Abbreviation;
                newObj.AppliedType = item.AppliedType;

                newObj.SecondSemesterCoefficient = item.SecondSemesterCoefficient;
                newObj.FirstSemesterCoefficient = item.FirstSemesterCoefficient;
                newObj.IsForeignLanguage = item.SubjectCat.IsForeignLanguage;
                newObj.SubjectID = item.SubjectID;
                SubjectCat lsSubjectCat2 = SubjectCatBusiness.Find(item.SubjectID);
                newObj.SubjectName = lsSubjectCat2.DisplayName;
                newObj.IsEditIsCommentting = lsSubjectCat2.IsEditIsCommentting;
                newObj.ReadAndWriteMiddleTest = lsSubjectCat2.ReadAndWriteMiddleTest;
                newObj.DisplayName = lsSubjectCat2.DisplayName;
                newObj.SectionPerWeekFirstSemester = item.SectionPerWeekFirstSemester.HasValue ? item.SectionPerWeekFirstSemester.Value : 0;
                newObj.SectionPerWeekSecondSemester = item.SectionPerWeekSecondSemester.HasValue ? item.SectionPerWeekSecondSemester.Value : 0;
                newObj.OrderInSubject = lsSubjectCat2.OrderInSubject;
                //  newObj.SubjectName = item.SubjectCat.SubjectName;

                //du lieu neu lop khong co thi khong fill,co thi fill
                ClassSubject temp = lsClassSubject.Where(o => (o.SubjectID == item.SubjectID)).Where(o => (o.ClassID == id)).FirstOrDefault();
                if (temp != null)
                {
                    newObj.ClassSubjectID = temp.ClassSubjectID;
                    newObj.AppliedType = temp.AppliedType;
                    newObj.IsSecondForeignLanguage = temp.IsSecondForeignLanguage;
                    newObj.SectionPerWeekFirstSemester = temp.SectionPerWeekFirstSemester;
                    newObj.SectionPerWeekSecondSemester = temp.SectionPerWeekSecondSemester;
                    newObj.SecondSemesterCoefficient = temp.SecondSemesterCoefficient;
                    newObj.FirstSemesterCoefficient = temp.FirstSemesterCoefficient;
                    newObj.IsSpecializedSubject = temp.IsSpecializedSubject;
                    newObj.Choice = true;
                    newObj.ClassID = id;
                    newObj.IsCommenting = temp.IsCommenting;
                    if (temp.IsCommenting == 1)
                    {
                        newObj.DisableCoefficient = "disabled";
                    }
                    else
                    {
                        newObj.DisableCoefficient = "";
                    }
                }
                else
                {
                    //newObj.IsCommenting = item.SubjectCat.IsCommenting;
                    newObj.IsCommenting = item.IsCommenting;
                    newObj.IsSpecializedSubject = false;
                    newObj.Choice = false;
                    newObj.IsSecondForeignLanguage = false;
                    if (item.IsCommenting == 1)
                    {
                        newObj.DisableCoefficient = "disabled";
                    }
                    else
                    {
                        newObj.DisableCoefficient = "";
                    }
                }

                lsClassSubjectObject.Add(newObj);
            }

            //Paginate<ClassSubjectObject> paging = new Paginate<ClassSubjectObject>(lsClassSubjectObject.AsQueryable());
            //paging.page = 1;
            //paging.size = 50;
            //paging.paginate();
            //ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT] = paging;
            GlobalInfo global = new GlobalInfo();
            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);

            ViewData[ClassSubjectConstant.TRUONG_CHUYEN] = sp.SchoolTypeID.HasValue ? (sp.SchoolTypeID.Value == SystemParamsInFile.SCHOOL_TYPE_TRUONG_CHUYEN) : false;
            ViewData[ClassSubjectConstant.LS_CLASSSUBJECTOBJECT] = lsClassSubjectObject.OrderByDescending(o => o.Choice).ThenBy(o => o.OrderInSubject).ToList();
            FillDropDownListDefault();
        }

        #endregion Get Gird

        #region SaveClassSubjectObject

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult SaveClassSubjectObject(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
             int[] HiddenClassSubjectID, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int? cboEducationLevel, int? cboClass, int? ButtonClickType)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ClassSubject", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if ((!cboClass.HasValue) || (!cboEducationLevel.HasValue) || (!cboClass.HasValue))
            {
                return Json(new JsonMessage(Res.Get("SubjectClass_Label_NoGridError")));
            }

            if (HiddenSubjectID != null)
            {
                //anhnph1_20150702thay doi SecondSemesterCoefficient = FirstSemesterCoefficient theo yêu cầu dac ta Giai doan 10
                return UpdateOrInsert(AppliedType, IsCommenting, FirstSemesterCoefficient, FirstSemesterCoefficient, SectionPerWeekFirstSemester,
                     SectionPerWeekSecondSemester, IsSecondForeignLanguage, IsSpecializedSubject, checkedClass, HiddenClassSubjectID, HiddenSubjectID,
                     HiddenCheckOnChange, cboEducationLevel.Value, cboClass.Value, ButtonClickType.Value);
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Validate_NotCompatible")));
            }
        }

        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult UpdateOrInsert(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
           int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
           int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
            int[] HiddenClassSubjectID, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int cboEducationLevel, int cboClass, int ButtonClickType)
        {
            #region Kiem tra co doi kieu mon hay khong
            // Mang luu tru gia tri vi tri cua lop dc CHECK
            List<int> CheckedLocation = new List<int>();
            for (int i = 0; i < HiddenSubjectID.Length; i++)
            {
                if (checkedClass == null)
                {
                    checkedClass = new int[] { };
                }
                if (checkedClass.Contains(HiddenSubjectID[i]))
                {
                    CheckedLocation.Add(i);
                }
            }
            // Lay mon hoc cho lop
            Dictionary<string, object> dicCS = new Dictionary<string, object>();
            dicCS["SchoolID"] = _globalInfo.SchoolID;
            dicCS["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicCS["AppliedLevel"] = _globalInfo.AppliedLevel;
            // Neu la ap dung theo khoi hoac lop thi chi lay danh sach tuong ung
            if (ButtonClickType == 1)
            {
                dicCS["ClassID"] = cboClass;
            }
            else if (ButtonClickType == 2)
            {
                dicCS["EducationLevelID"] = cboEducationLevel;
            }
            // Mon hoc cua lop
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicCS).ToList();
            // Kiem tra co thay doi kieu mon hay khong
            List<ClassSubjectBO> listClassSubjectChangeType = this.GetListClassSubjectChangeType(IsCommenting, HiddenSubjectID, CheckedLocation, listClassSubject);
            if (listClassSubjectChangeType != null && listClassSubjectChangeType.Count > 0)
            {
                SaveParamsClass SaveParams = new SaveParamsClass(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient, SectionPerWeekFirstSemester,
                    SectionPerWeekSecondSemester, IsSecondForeignLanguage, IsSpecializedSubject, checkedClass, HiddenClassSubjectID, HiddenSubjectID, HiddenCheckOnChange, cboEducationLevel
                    , cboClass, ButtonClickType, listClassSubjectChangeType.ToArray());
                TempData[ClassSubjectConstant.TEMP_DATA] = SaveParams;
                JsonMessage json = new JsonMessage(Res.Get("ClassSubject_Label_ConfirmDeleteMessage"), "confirm");
                return Json(json);
            }
            #endregion

            if (ButtonClickType == 1)
            {
                if (_globalInfo.AppliedLevel.HasValue && _globalInfo.AppliedLevel.Value > GlobalConstants.APPLIED_LEVEL_PRIMARY)//chi xu ly mon chuyen mon tu chon cho cap 2 3
                {
                    // QuangNN2 - Khai bao mon chuyen, mon tu chon cho lop
                    DeclareSpecializedSubjectAndElectiveSubjectForClass(AppliedType, SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSpecializedSubject,
                       checkedClass, HiddenSubjectID, HiddenCheckOnChange, cboEducationLevel, cboClass);
                }

                // Mon hoc cua truong
                List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, new Dictionary<string, object>() { { "SchoolID", _globalInfo.SchoolID } }).ToList();
                SetSubject4Class(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient, SectionPerWeekFirstSemester,
                    SectionPerWeekSecondSemester, IsSecondForeignLanguage, IsSpecializedSubject, checkedClass, HiddenClassSubjectID, HiddenSubjectID,
                    cboEducationLevel, cboClass, listClassSubject, listSchoolSubject);
                //ClassSubjectBusiness.Save();
                GetGrid(cboEducationLevel, cboClass);

                return Json(new JsonMessage(Res.Get("ClassSubject_Label_Create_Suc")));
            }
            else if (ButtonClickType == 2)
            {
                return LevelAppliedButtonClick(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient, SectionPerWeekFirstSemester,
                 SectionPerWeekSecondSemester, IsSecondForeignLanguage, IsSpecializedSubject, checkedClass, HiddenClassSubjectID, HiddenSubjectID, HiddenCheckOnChange, cboEducationLevel, cboClass, listClassSubject);
            }
            else if (ButtonClickType == 3)
            {
                return SchoolAppliedButtonClick(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient, SectionPerWeekFirstSemester,
                 SectionPerWeekSecondSemester, IsSecondForeignLanguage, IsSpecializedSubject, checkedClass, HiddenClassSubjectID, HiddenSubjectID, HiddenCheckOnChange, cboEducationLevel,
                 cboClass, listClassSubject);
            }

            return Json(new JsonMessage(Res.Get("ClassSubject_Label_Create_Suc")));
        }

        #endregion SaveClassSubjectObject

        #region SetSubject4Class

        /// <summary>
        /// Thiet lap mon hoc cho 1 lop hoc
        /// </summary>
        /// <param name="AppliedType"></param>
        /// <param name="IsCommenting"></param>
        /// <param name="FirstSemesterCoefficient"></param>
        /// <param name="SecondSemesterCoefficient"></param>
        /// <param name="SectionPerWeekFirstSemester"></param>
        /// <param name="SectionPerWeekSecondSemester"></param>
        /// <param name="IsSecondForeignLanguage"></param>
        /// <param name="IsSpecializedSubject"></param>
        /// <param name="checkedClass"></param>
        /// <param name="HiddenClassID"></param>
        /// <param name="HiddenSubjectID"></param>
        /// <param name="cboEducationLevel"></param>
        /// <param name="cboClass"></param>
        private void SetSubject4Class(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
            int[] HiddenClassSubjectID, int[] HiddenSubjectID, int cboEducationLevel, int cboClass,
            List<ClassSubject> lsClassSubject, List<SchoolSubject> listSchoolSubject)
        {
            // Dic de luu cac gia tri lay ra dc. 1 classID -> 1 dic cac gia tri khac
            IDictionary<int, IDictionary<string, object>> Classdic = new Dictionary<int, IDictionary<string, object>>();
            GlobalInfo global = new GlobalInfo();
            //xu ly string truoc
            //co 1 mang int HiddenClassID va 1 mang checkedClass
            //mang checkClass la ID nhung lop da duoc check de xu ly
            //Mang HiddenClassID la ID tat ca cac lop theo thu tu
            //vi vay,de lay cac gia tri AppliedType,IsCommenting,... cua cac lop DUOC CHECK
            //don gian la ta tim vi tri cua cac lop DUOC CHECK ay ben trong mang HiddenClassID
            //khi do cac gia tri AppliedType,IsCommenting,... cua cac lop DUOC CHECK se nam o vi tri tuong ung
            //tru IsSecondForeignLanguage va IsSpecializedSubject

            //mang luu tru gia tri vi tri cua lop dc CHECK
            List<int> CheckedLocation = new List<int>();
            for (int i = 0; i < HiddenSubjectID.Length; i++)
            {
                if (checkedClass == null)
                {
                    checkedClass = new int[] { };
                }
                if (checkedClass.Contains(HiddenSubjectID[i]))
                {
                    CheckedLocation.Add(i);
                }
            }

            // //mang luu tru gia tri vi tri cua lop KO CHECK
            List<int> listNoCheckSubjectID = new List<int>();
            for (int i = 0; i < HiddenSubjectID.Length; i++)
            {
                if (!checkedClass.Contains(HiddenSubjectID[i]))
                {
                    listNoCheckSubjectID.Add(HiddenSubjectID[i]);
                }
            }

            //sau khi co vi tri cac phan tu,viec con lai la dua vao Dictionary
            foreach (int location in CheckedLocation)
            {
                IDictionary<string, object> newDic = new Dictionary<string, object>();
                newDic.Add("AppliedType", AppliedType[location]);
                newDic.Add("IsCommenting", IsCommenting[location]);
                newDic.Add("FirstSemesterCoefficient", (FirstSemesterCoefficient != null) ? FirstSemesterCoefficient[location] : 0);
                newDic.Add("SecondSemesterCoefficient", SecondSemesterCoefficient != null ? SecondSemesterCoefficient[location] : 0);
                newDic.Add("SectionPerWeekFirstSemester", ToNullableDecimal(SectionPerWeekFirstSemester[location], "ClassSubject_Label_SectionPerWeekFirstSemester"));
                newDic.Add("SectionPerWeekSecondSemester", ToNullableDecimal(SectionPerWeekSecondSemester[location], "ClassSubject_Label_SectionPerWeekSecondSemester"));
                newDic.Add("ClassID", cboClass);

                if (ToNullableDecimal(SectionPerWeekFirstSemester[location], "ClassSubject_Label_SectionPerWeekFirstSemester") == 0 && ToNullableDecimal(SectionPerWeekSecondSemester[location], "ClassSubject_Label_SectionPerWeekSecondSemester") == 0)
                {
                    throw new BusinessException("Validate_Class_Subject_HK");
                }


                //rieng IsSecondForeignLanguage,ta so sanh id tra ve voi id cua checked,neu co thi
                //IsSecondForeignLanguage=true con lai la fasle
                if (IsSecondForeignLanguage == null)
                {
                    IsSecondForeignLanguage = new int[] { };
                }
                if (IsSecondForeignLanguage.Contains(HiddenSubjectID[location]))
                {
                    newDic["IsSecondForeignLanguage"] = null;
                }
                else
                {
                    newDic["IsSecondForeignLanguage"] = null;
                }

                //tuong tu
                if (IsSpecializedSubject == null)
                {
                    IsSpecializedSubject = new int[] { };
                }
                if (IsSpecializedSubject.Contains(HiddenSubjectID[location]))
                {
                    newDic["IsSpecializedSubject"] = true;
                }
                else
                {
                    newDic["IsSpecializedSubject"] = false;
                }

                //gio xet neu la mon chuyen thi hs hk1 va hk2 la 3\
                //if ((bool)newDic["IsSpecializedSubject"])
                //{
                //    newDic["FirstSemesterCoefficient"] = 3;
                //    newDic["SecondSemesterCoefficient"] = 3;
                //}                
                if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                {
                    newDic["FirstSemesterCoefficient"] = null;
                    newDic["SecondSemesterCoefficient"] = null;
                }
                Classdic.Add(HiddenSubjectID[location], newDic);
            }

            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID);
            if (acaYear == null)
            {
                throw new BusinessException("Common_IsCurrentYear_Err");
            }

            //Chiendd1: 07/07/2015. Bo sung partitionId cho bang ClassSubject;
            int partitionId = UtilsBusiness.GetPartionId(acaYear.SchoolID);
            List<ClassSubject> lstInsert = new List<ClassSubject>();
            List<ClassSubject> lstUpdate = new List<ClassSubject>();
            List<ClassSubject> lstClassSubjectTemp = lsClassSubject;
            ClassSubject newClassSubject;
            List<ClassSubject> lstClassSubjectThreadMark = new List<ClassSubject>();
            foreach (var pair in Classdic)
            {
                //Hệ thống thực hiện gọi hàm Search trong nghiệp vụ ClassSubject
                //-	Nếu chưa tồn tại bản ghi tương ứng với môn:
                //Hệ thống thực hiện gọi hàm Insert trong nghiệp vụ ClassSubject
                IDictionary<string, object> searchDic = new Dictionary<string, object>();
                newClassSubject = lsClassSubject.Where(o => o.SubjectID == pair.Key).FirstOrDefault();

                //neu khong tim thay doi tuong classsubject da co mon hoc nla subjectid roi thi insert
                if (newClassSubject == null)
                {
                    if (listSchoolSubject.Any(o => o.EducationLevelID == cboEducationLevel && o.SubjectID == pair.Key))
                    {
                        newClassSubject = new ClassSubject();
                        newClassSubject.AppliedType = (int?)pair.Value["AppliedType"];

                        newClassSubject.FirstSemesterCoefficient = (int?)pair.Value["FirstSemesterCoefficient"];
                        newClassSubject.SecondSemesterCoefficient = (int?)pair.Value["SecondSemesterCoefficient"];
                        newClassSubject.IsCommenting = (int?)pair.Value["IsCommenting"];
                        newClassSubject.IsSecondForeignLanguage = (bool?)pair.Value["IsSecondForeignLanguage"];
                        newClassSubject.IsSpecializedSubject = (bool?)pair.Value["IsSpecializedSubject"];
                        newClassSubject.SectionPerWeekFirstSemester = (decimal?)pair.Value["SectionPerWeekFirstSemester"];
                        newClassSubject.SectionPerWeekSecondSemester = (decimal?)pair.Value["SectionPerWeekSecondSemester"];
                        newClassSubject.SubjectID = (int)pair.Key;
                        newClassSubject.ClassID = (int)pair.Value["ClassID"];
                        newClassSubject.StartDate = acaYear.FirstSemesterStartDate;
                        newClassSubject.EndDate = acaYear.FirstSemesterEndDate;
                        newClassSubject.Last2digitNumberSchool = partitionId;
                        lstInsert.Add(newClassSubject);
                        //Neu khong phai la mon hoc VNEN thi moi thuc hien luu vao threadMark
                        if (_globalInfo.AppliedLevel>=GlobalConstants.APPLIED_LEVEL_SECONDARY &&
                                ( !newClassSubject.IsSubjectVNEN.HasValue || 
                                (newClassSubject.IsSubjectVNEN.HasValue && newClassSubject.IsSubjectVNEN.Value == false)))
                        {
                            lstClassSubjectThreadMark.Add(newClassSubject);
                        }
                    }
                }
                else
                {
                    //Neu co thay doi ve kieu mon, loai mon, he so, so tiet/tuan thi luu lai vao ThreadMark de tinh TBM tu dong
                    if (_globalInfo.AppliedLevel >= GlobalConstants.APPLIED_LEVEL_SECONDARY)
                    {
                        if (!newClassSubject.AppliedType.Equals(pair.Value["AppliedType"])
                            || !newClassSubject.FirstSemesterCoefficient.Equals(pair.Value["FirstSemesterCoefficient"])                            
                            || !newClassSubject.IsCommenting.Equals(pair.Value["IsCommenting"])
                            || !newClassSubject.IsSecondForeignLanguage.Equals(pair.Value["IsSecondForeignLanguage"])
                            || !newClassSubject.IsSpecializedSubject.Equals(pair.Value["IsSpecializedSubject"])
                            || !newClassSubject.SectionPerWeekFirstSemester.Equals(pair.Value["SectionPerWeekFirstSemester"])
                            || !newClassSubject.SectionPerWeekSecondSemester.Equals(pair.Value["SectionPerWeekSecondSemester"]))
                        {
                            //Neu khong phai la mon hoc VNEN thi moi thuc hien luu vao threadMark
                            if (!newClassSubject.IsSubjectVNEN.HasValue || (newClassSubject.IsSubjectVNEN.HasValue && newClassSubject.IsSubjectVNEN.Value == false))
                            {
                                lstClassSubjectThreadMark.Add(newClassSubject);
                            }
                        }
                    }
                    newClassSubject.AppliedType = (int?)pair.Value["AppliedType"];
                    newClassSubject.FirstSemesterCoefficient = (int?)pair.Value["FirstSemesterCoefficient"];
                    newClassSubject.SecondSemesterCoefficient = (int?)pair.Value["SecondSemesterCoefficient"];
                    newClassSubject.IsCommenting = (int?)pair.Value["IsCommenting"];
                    newClassSubject.IsSecondForeignLanguage = (bool?)pair.Value["IsSecondForeignLanguage"];
                    newClassSubject.IsSpecializedSubject = (bool?)pair.Value["IsSpecializedSubject"];
                    newClassSubject.SectionPerWeekFirstSemester = (decimal?)pair.Value["SectionPerWeekFirstSemester"];
                    newClassSubject.SectionPerWeekSecondSemester = (decimal?)pair.Value["SectionPerWeekSecondSemester"];
                    newClassSubject.SubjectID = (int)pair.Key;
                    newClassSubject.ClassID = (int)pair.Value["ClassID"];
                    newClassSubject.StartDate = acaYear.FirstSemesterStartDate;
                    newClassSubject.EndDate = acaYear.FirstSemesterEndDate;
                    newClassSubject.Last2digitNumberSchool = partitionId;
                    lstUpdate.Add(newClassSubject);
                }
            }

            if (lstInsert.Count > 0)
            {
                ClassSubjectBusiness.Insert(lstInsert, acaYear.SchoolID, acaYear.AcademicYearID, global.AppliedLevel.Value);
            }
            if (lstUpdate.Count > 0)
            {
                ClassSubjectBusiness.Update(lstUpdate, acaYear.SchoolID, acaYear.AcademicYearID, global.AppliedLevel.Value);
            }
            ClassSubjectBusiness.Save();

            //Đối với các môn không được lựa chọn hệ thống thực hiện gọi hàm
            //Xoa cac mon hoc khong duoc chon
            List<ClassSubject> listClassSubjectDel = lsClassSubject.Where(o => listNoCheckSubjectID.Contains(o.SubjectID)).ToList();


            if (listClassSubjectDel.Count > 0)
            {
                //thực hiện gọi hàm Delete trong nghiệp vụ ClassSubject
                ClassSubjectBusiness.Delete(listClassSubjectDel, acaYear.AcademicYearID);
                ClassSubjectBusiness.Save();
            }

            if (lstClassSubjectThreadMark != null && lstClassSubjectThreadMark.Count > 0)
            {
                ClassSubjectBusiness.InsertThreadMark(lstClassSubjectTemp, acaYear.AcademicYearID, acaYear.SchoolID, global.Semester.Value);
            }

        }

        #endregion SetSubject4Class

        #region SetSubject4EducationLevel

        private void SetSubject4EducationLevel(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
            int[] HiddenClassID, int[] HiddenSubjectID, int cboEducationLevel, List<ClassSubject> listClassSubjectEducation)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            //dic["AcademicYearID"] = new GlobalInfo().UserAccountID;
            //dic["SchoolID"] = new GlobalInfo().SchoolID;
            GlobalInfo globalInfo = new GlobalInfo();
            int? AcademicYearID = globalInfo.AcademicYearID;
            int? SchoolID = globalInfo.SchoolID;
            dic["AcademicYearID"] = AcademicYearID.Value;
            dic["SchoolID"] = SchoolID.Value;
            dic["EducationLevelID"] = cboEducationLevel;
            dic["AppliedLevel"] = globalInfo.AppliedLevel;
            IQueryable<ClassProfile> QueryClassProfile = ClassProfileBusiness.Search(dic);
            List<ClassProfile> ListClassProfile = QueryClassProfile.ToList();
            // Mon hoc cua truong
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value,
                new Dictionary<string, object>() { { "SchoolID", _globalInfo.SchoolID }, { "EducationLevelID", cboEducationLevel } }).ToList();
            foreach (ClassProfile ClassProfile in ListClassProfile)
            {
                List<ClassSubject> listClassSubject = listClassSubjectEducation.Where(o => o.ClassID == ClassProfile.ClassProfileID).ToList();
                SetSubject4Class(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient,
                    SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSecondForeignLanguage,
                    IsSpecializedSubject, checkedClass, HiddenClassID, HiddenSubjectID, ClassProfile.EducationLevelID,
                    ClassProfile.ClassProfileID, listClassSubject, listSchoolSubject);
            }
            //ClassSubjectBusiness.Save();
        }

        #endregion SetSubject4EducationLevel

        #region SetSubject4School

        private void SetSubject4School(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
             int[] HiddenClassID, int[] HiddenSubjectID, int cboEducationLevel, int cboClass, List<ClassSubject> listClassSubjectAll)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            //dic["AcademicYearID"] = new GlobalInfo().UserAccountID;
            //dic["SchoolID"] = new GlobalInfo().SchoolID;
            GlobalInfo globalInfo = new GlobalInfo();
            int? AcademicYearID = globalInfo.AcademicYearID;
            int? SchoolID = globalInfo.SchoolID;
            dic["AcademicYearID"] = AcademicYearID.Value;
            dic["SchoolID"] = SchoolID.Value;
            dic["AppliedLevel"] = globalInfo.AppliedLevel;
            // Mon hoc cua truong
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, new Dictionary<string, object>() { { "SchoolID", _globalInfo.SchoolID } }).ToList();
            IQueryable<ClassProfile> QueryClassProfile = ClassProfileBusiness.Search(dic).OrderBy(o => o.EducationLevelID);
            List<ClassProfile> ListClassProfile = QueryClassProfile.ToList();
            if (ListClassProfile.Count > 0)
            {
                foreach (ClassProfile ClassProfile in ListClassProfile)
                {
                    // Mon hoc cua truong lay theo khoi
                    List<SchoolSubject> listSchoolSubjectEdu = listSchoolSubject.Where(o => o.EducationLevelID == ClassProfile.EducationLevelID).ToList();
                    List<ClassSubject> listClassSubject = listClassSubjectAll.Where(o => o.ClassID == ClassProfile.ClassProfileID).ToList();
                    SetSubject4Class(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient,
                        SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSecondForeignLanguage,
                        IsSpecializedSubject, checkedClass, HiddenClassID, HiddenSubjectID, ClassProfile.EducationLevelID,
                        ClassProfile.ClassProfileID, listClassSubject, listSchoolSubject);
                }
                //ClassSubjectBusiness.Save();
            }
        }

        #endregion SetSubject4School

        #region Convert String -> Int?

        public Nullable<decimal> ToNullableDecimal(string s, string resKey)
        {
            decimal i;
            if (s == null || s == "")
            {
                return 0;
            }
            if (decimal.TryParse(s, out i)) return i;
            else
            {
                List<object> Params = new List<object>();
                Params.Add(resKey);
                throw new BusinessException("Common_Validate_NotIsNumber", Params);
            }
        }

        #endregion Convert String -> Int?

        private List<ClassSubjectBO> GetListClassSubjectChangeType(int[] IsCommenting, int[] ArrSubjectID, List<int> CheckedLocation, List<ClassSubject> listClassSubject)
        {
            //int AcademicYearID = new GlobalInfo().AcademicYearID;
            List<ClassSubjectBO> listCsChangeType = new List<ClassSubjectBO>();

            foreach (int location in CheckedLocation)
            {
                int SubjectID = ArrSubjectID[location];

                // Lay thong tin mon hoc cua lop nay ra de kiem tra
                List<ClassSubject> listCsBySubject = listClassSubject.Where(o => o.SubjectID == SubjectID).ToList();
                if (listCsBySubject.Count > 0)
                {
                    foreach (ClassSubject cs in listCsBySubject)
                    {
                        // Kiem tra co su thay doi kieu mon tinh diem sang nhan xet hoac nguoc lai
                        if (cs.IsCommenting.HasValue && cs.IsCommenting != IsCommenting[location])
                        {
                            // Danh dau de xu ly
                            listCsChangeType.Add(new ClassSubjectBO { ClassID = cs.ClassID, SubjectID = cs.SubjectID, IsCommenting = cs.IsCommenting });
                        }
                    }
                }
            }
            return listCsChangeType;
        }

        #region Delete Point Ajax Call
        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMarkRecord()
        {
            SaveParamsClass SaveParams = (SaveParamsClass)TempData[ClassSubjectConstant.TEMP_DATA];
            List<ClassSubjectBO> listCsChangeType = SaveParams.ClassSubjectChangeTypes.ToList();
            if (listCsChangeType.Count > 0)
            {
                listCsChangeType = listCsChangeType.Distinct().ToList();
                Dictionary<string, object> searchInfo = new Dictionary<string, object>();
                searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                foreach (ClassSubjectBO cs in listCsChangeType)
                {
                    searchInfo["ClassID"] = cs.ClassID;
                    searchInfo["SubjectID"] = cs.SubjectID;
                    if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        List<JudgeRecord> lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchInfo).ToList();
                        if (lstJudgeRecord.Count() > 0)
                        {
                            JudgeRecordBusiness.DeleteAll(lstJudgeRecord);
                        }
                    }
                    //Môn tính điểm
                    if (cs.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        List<MarkRecord> lstMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchInfo).ToList();
                        if (lstMarkRecord.Count() > 0)
                        {
                            MarkRecordBusiness.DeleteAll(lstMarkRecord);
                        }
                    }
                    //Điểm tổng kết
                    List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchInfo).ToList();
                    if (lstSummedUpRecord.Count() > 0)
                    {
                        SummedUpRecordBusiness.DeleteAll(lstSummedUpRecord);
                    }
                }
            }
            // Lay mon hoc cho lop
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", _globalInfo.AcademicYearID } }).ToList();
            // Thuc hien insert du lieu khai bao mon hoc cho toan truong
            return SchoolAppliedButtonClick(SaveParams.AppliedType, SaveParams.IsCommenting, SaveParams.FirstSemesterCoefficient, SaveParams.SecondSemesterCoefficient, SaveParams.SectionPerWeekFirstSemester,
                    SaveParams.SectionPerWeekSecondSemester, SaveParams.IsSecondForeignLanguage, SaveParams.IsSpecializedSubject, SaveParams.checkedClass, SaveParams.HiddenClassSubjectID, SaveParams.HiddenSubjectID, SaveParams.HiddenCheckOnChange, SaveParams.cboEducationLevel
                    , SaveParams.cboClass, listClassSubject);
        }

        #endregion Delete Point Ajax Call

        #region LevelAppliedButtonClick

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult LevelAppliedButtonClick(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
            int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
             int[] HiddenClassID, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int cboEducationLevel, int cboClass, List<ClassSubject> listClassSubject)
        {
            if (_globalInfo.AppliedLevel.HasValue && _globalInfo.AppliedLevel.Value > GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                // QuangNN2 - Khai bao mon chuyen, mon tu chon - Ap dung toan khoi
                DeclareSpecializedSubjectAndElectiveSubjectForEducationLevel(AppliedType, SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSpecializedSubject,
                    checkedClass, HiddenSubjectID, HiddenCheckOnChange, cboEducationLevel);
            }
            SetSubject4EducationLevel(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient,
                SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSecondForeignLanguage,
                IsSpecializedSubject, checkedClass, HiddenClassID, HiddenSubjectID, cboEducationLevel, listClassSubject);

            return Json(new JsonMessage(Res.Get("ClassSubject_Label_Create_Suc")));
        }

        #endregion LevelAppliedButtonClick

        #region SchoolAppliedButtonClick

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult SchoolAppliedButtonClick(int[] AppliedType, int[] IsCommenting, int[] FirstSemesterCoefficient,
           int[] SecondSemesterCoefficient, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
           int[] IsSecondForeignLanguage, int[] IsSpecializedSubject, int[] checkedClass,
            int[] HiddenClassID, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int cboEducationLevel, int cboClass, List<ClassSubject> listClassSubject)
        {
            if (_globalInfo.AppliedLevel.HasValue && _globalInfo.AppliedLevel.Value > GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                // QuangNN2 - Khai bao mon chuyen, mon tu chon - Ap dung toan truong
                DeclareSpecializedSubjectAndElectiveSubjectForSchool(AppliedType, SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSpecializedSubject,
                    checkedClass, HiddenSubjectID, HiddenCheckOnChange);
            }

            SetSubject4School(AppliedType, IsCommenting, FirstSemesterCoefficient, SecondSemesterCoefficient,
                SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSecondForeignLanguage,
                IsSpecializedSubject, checkedClass, HiddenClassID, HiddenSubjectID, cboEducationLevel, cboClass, listClassSubject);

            return Json(new JsonMessage(Res.Get("ClassSubject_Label_Create_Suc")));
        }

        #endregion SchoolAppliedButtonClick

        #region QuangNN2 - Khai bao mon chuyen, mon tu chon

        public void DeclareSpecializedSubjectAndElectiveSubjectForClass(int[] AppliedType, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSpecializedSubject, int[] checkedClass, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int EducationLevel, int Class)
        {
            // Lay danh sach hoc sinh và hoc sinh mien giam cua lop
            List<PupilOfClassBO> listPupilOfClass = null;
            List<PupilOfClassBO> listPupilOfClassActive = null;
            List<ExemptedSubject> listPupilEx = null;

            IDictionary<string, object> searchDic = new Dictionary<string, object>();
            searchDic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            searchDic.Add("ClassID", Class);
            searchDic.Add("EducationLevelID", EducationLevel);
            searchDic.Add("SchoolID", _globalInfo.SchoolID.Value);
            searchDic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            listPupilOfClass = (from pp in PupilProfileBusiness.All
                                join pc in PupilOfClassBusiness.All on pp.PupilProfileID equals pc.PupilID
                                where pc.AcademicYearID == _globalInfo.AcademicYearID.Value && pc.ClassID == Class &&
                                pc.SchoolID == _globalInfo.SchoolID.Value && pp.IsActive == true
                                select new PupilOfClassBO
                                {
                                    PupilOfClassID = pc.PupilOfClassID,
                                    PupilID = pc.PupilID,
                                    SchoolID = pc.SchoolID,
                                    ClassID = pc.ClassID,
                                    AcademicYearID = pc.AcademicYearID,
                                    OrderInClass = pc.OrderInClass,
                                    Status = pc.Status

                                }).ToList();

            listPupilEx = ExemptedSubjectBusiness.Search(searchDic).ToList();
            listPupilOfClassActive = listPupilOfClass.Where(o => o.Status == 1).ToList();

            // Lay danh sach nhung mon duoc check la mon chuyen hoac mon tu chon
            List<RegisterSubjectSpecializeBO> listSubject = new List<RegisterSubjectSpecializeBO>();
            RegisterSubjectSpecializeBO subjectObj = null;

            List<int> listChecked = new List<int>();
            int countHidden = HiddenSubjectID.Count();
            for (int i = 0; i < countHidden; i++)
            {
                int temp = 0;
                int countCheck = checkedClass.Count();
                for (int j = 0; j < countCheck; j++)
                {
                    if (checkedClass[j] == HiddenSubjectID[i])
                    {
                        temp = 1;
                        break;
                    }
                }
                listChecked.Add(temp);
            }

            if (IsSpecializedSubject != null && IsSpecializedSubject.Count() > 0)
            {
                List<int> listSpecializedSubject = new List<int>();
                for (int i = 0; i < countHidden; i++)
                {
                    int temp = 0;
                    int countCheck = IsSpecializedSubject.Count();
                    for (int j = 0; j < countCheck; j++)
                    {
                        if (IsSpecializedSubject[j] == HiddenSubjectID[i])
                        {
                            temp = 1;
                            break;
                        }
                    }
                    listSpecializedSubject.Add(temp);
                }

                // Lay danh sach nhung mon duoc check la mon chuyen
                for (int i = 0; i < listSpecializedSubject.Count; i++)
                {
                    if (listChecked[i] == 1 && listSpecializedSubject[i] == 1 && AppliedType[i] == 0)
                    {
                        subjectObj = new RegisterSubjectSpecializeBO();

                        subjectObj.SubjectID = HiddenSubjectID[i];
                        subjectObj.SubjectTypeID = AppliedType[i];
                        subjectObj.SectionPerWeekFirstSemester = SectionPerWeekFirstSemester[i];
                        subjectObj.SectionPerWeekSecondSemester = SectionPerWeekSecondSemester[i];

                        subjectObj.CheckOnChange = HiddenCheckOnChange[i];

                        listSubject.Add(subjectObj);
                    }
                }
            }

            // Lay danh sach nhung mon duoc check la mon tu chon
            int countElec = AppliedType.Count();
            for (int i = 0; i < countElec; i++)
            {
                if (listChecked[i] == 1 && (AppliedType[i] == 1 || AppliedType[i] == 2 || AppliedType[i] == 3))
                {
                    subjectObj = new RegisterSubjectSpecializeBO();

                    subjectObj.SubjectID = HiddenSubjectID[i];
                    subjectObj.SubjectTypeID = AppliedType[i];
                    subjectObj.SectionPerWeekFirstSemester = SectionPerWeekFirstSemester[i];
                    subjectObj.SectionPerWeekSecondSemester = SectionPerWeekSecondSemester[i];

                    subjectObj.CheckOnChange = HiddenCheckOnChange[i];

                    listSubject.Add(subjectObj);
                }
            }

            RegisterSubjectSpecializeBusiness.RegisterForClass(listSubject, listPupilOfClassActive, listPupilEx, searchDic);
        }

        public void DeclareSpecializedSubjectAndElectiveSubjectForEducationLevel(int[] AppliedType, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSpecializedSubject, int[] checkedClass, int[] HiddenSubjectID, int[] HiddenCheckOnChange, int EducationLevel)
        {
            // Lay danh sach cac lop trong khoi
            IDictionary<string, object> searchDic = new Dictionary<string, object>();
            searchDic.Add("AcademicYearID", _globalInfo.AcademicYearID);
            searchDic.Add("EducationLevelID", EducationLevel);
            searchDic.Add("SchoolID", _globalInfo.SchoolID.Value);

            List<ClassProfile> listClass = ClassProfileBusiness.Search(searchDic).ToList();

            for (int i = 0; i < listClass.Count; i++)
            {
                DeclareSpecializedSubjectAndElectiveSubjectForClass(AppliedType, SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSpecializedSubject,
                    checkedClass, HiddenSubjectID, HiddenCheckOnChange, EducationLevel, listClass[i].ClassProfileID);
            }
        }

        public void DeclareSpecializedSubjectAndElectiveSubjectForSchool(int[] AppliedType, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester,
            int[] IsSpecializedSubject, int[] checkedClass, int[] HiddenSubjectID, int[] HiddenCheckOnChange)
        {
            // Lay danh sach cac khoi trong truong
            IDictionary<string, object> searchDic = new Dictionary<string, object>();
            searchDic.Add("IsActive", true);
            searchDic.Add("Grade", _globalInfo.AppliedLevel.Value);

            List<EducationLevel> listEducationLevel = EducationLevelBusiness.Search(searchDic).ToList();

            for (int i = 0; i < listEducationLevel.Count; i++)
            {
                DeclareSpecializedSubjectAndElectiveSubjectForEducationLevel(AppliedType, SectionPerWeekFirstSemester, SectionPerWeekSecondSemester, IsSpecializedSubject,
                    checkedClass, HiddenSubjectID, HiddenCheckOnChange, listEducationLevel[i].EducationLevelID);
            }
        }

        #endregion QuangNN2 - Khai bao mon chuyen, mon tu chon
    }
}
