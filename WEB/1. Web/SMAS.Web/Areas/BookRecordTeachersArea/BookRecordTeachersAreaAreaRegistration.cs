﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.BookRecordTeachersArea
{
    public class BookRecordTeachersAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BookRecordTeachersArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BookRecordTeachersArea_default",
                "BookRecordTeachersArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
