﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.BookRecordTeachersArea;
using SMAS.Business.BusinessObject;
using System.Threading;
using System.Globalization;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.BookRecordTeachersArea.Controllers
{
    public class BookRecordTeachersController : BaseController
    {
        //
        // GET: /BookRecordTeachersArea/BookRecordTeachers/
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IMonthCommentsBusiness MonthCommentsBusiness;

        public BookRecordTeachersController(IPupilProfileBusiness pupilProfileBusiness,
                                        ISummedUpRecordBusiness summedUpRecordBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                        IMarkRecordBusiness markrecordBusiness,
                                        ISubjectCatBusiness subjectCatBusiness,
                                        ITeachingAssignmentBusiness teachingAssignmentBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IClassSubjectBusiness classSubjectBusiness,
                                        IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                        IPupilOfClassBusiness PupilOfClassBusiness,
                                        IMarkTypeBusiness MarkTypeBusiness,
                                        IExemptedSubjectBusiness exemptedSubjectBusiness,
                                        IUserAccountBusiness UserAccountBusiness,
                                        IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
                                        IMonthCommentsBusiness monthCommentsBusiness)
        {
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.SemesterDeclarationBusiness = semeterDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.MonthCommentsBusiness = monthCommentsBusiness;
        }

        [CompressFilter(Order = 1)]
        public ActionResult Index(int? ClassID)
        {
            SetViewData(ClassID);
            return View();
        }
        public void SetViewData(int? ClassID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[BookRecordTeachersConstant.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[BookRecordTeachersConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;
            int a = 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = _globalInfo.Semester;

            List<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();


            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                a = 1;
                for (int k = 0; k < lstPeriod.Count; k++)
                {

                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = lstPeriod[k];
                    if (periodDeclaration.FromDate < DateTime.Now && periodDeclaration.EndDate > DateTime.Now)
                    {
                        a = k;
                    }
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            ViewData[BookRecordTeachersConstant.CHECK_DEFAULT] = a;
            int period = 0;
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
            }
            foreach (PeriodDeclaration pd in listPeriodDeclaration)
            {
                if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                {
                    period = pd.PeriodDeclarationID;
                }
            }

            //ViewData[BookRecordConstants.LIST_PERIOD] = listPeriodDeclaration;
            ViewData[BookRecordTeachersConstant.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);

            dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[BookRecordTeachersConstant.LIST_CLASS] = listClass;

                var listCP = listClass.ToList();

                if (listCP != null && listCP.Count() > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                        if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                        {
                            // Ưu tiên trước đối với giáo viên bộ môn
                            dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                            List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                            {
                                cp = listSubjectTeacher.First();
                            }
                            else
                            {
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                        .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listHead != null && listHead.Count > 0)
                                {
                                    cp = listHead.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            //lay list mon hoc
            if (cp != null)
            {
                if (_globalInfo.AcademicYearID.HasValue && _globalInfo.SchoolID.HasValue)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, _globalInfo.Semester.Value, cp.ClassProfileID, _globalInfo.IsRolePrincipal ? true : _globalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                        //.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                    .ToList();
                }
            }
            else
            {
                ViewData[BookRecordTeachersConstant.CLASS_NULL] = true;
            }
            

            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ViewData[BookRecordTeachersConstant.SubjectID] = lsClassSubject.Select(c => c.SubjectID).FirstOrDefault();
            ViewData[BookRecordTeachersConstant.FIRT_SEMESTER_STARTDATE] = objAca.FirstSemesterStartDate;
            ViewData[BookRecordTeachersConstant.FIRT_SEMESTER_ENDDATE] = objAca.FirstSemesterEndDate;
            ViewData[BookRecordTeachersConstant.SECOND_SEMESTER_STARTDATE] = objAca.SecondSemesterStartDate;
            ViewData[BookRecordTeachersConstant.SECOND_SEMESTER_ENDDATE] = objAca.SecondSemesterEndDate;
            //ViewData[BookRecordTeachersConstant.MONTH] = _globalInfo.GetMonthBySemester(_globalInfo.Semester.Value, AcademicYearBusiness.Find(_globalInfo.AcademicYearID));

            ViewData[BookRecordTeachersConstant.LIST_SUBJECT] = null;

            ViewData[BookRecordTeachersConstant.ShowList] = false;

            ViewData[BookRecordTeachersConstant.HAS_ERROR_DATA] = false;
            ViewData[BookRecordTeachersConstant.LIST_IMPORTDATA] = null;
            ViewData[BookRecordTeachersConstant.CLASS_12] = false;
            ViewData[BookRecordTeachersConstant.SEMESTER_1] = true;
            ViewData[BookRecordTeachersConstant.ERROR_BASIC_DATA] = false;
            ViewData[BookRecordTeachersConstant.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookRecordTeachersConstant.ERROR_IMPORT] = true;
            ViewData[BookRecordTeachersConstant.ENABLE_ENTRYMARK] = "true";

        }
        [HttpPost]
        //[CacheFilter(Duration = 60)]
        public PartialViewResult GetSubjectPanel()
        {
            int? classid = SMAS.Business.Common.Utils.GetInt(Request["id"]);
            int? semesterid = SMAS.Business.Common.Utils.GetInt(Request["semester"]);
            int semester = semesterid == null ? 0 : semesterid.Value;
            ViewData[BookRecordTeachersConstant.ClassID] = classid.Value;
            //cho phep tai khoan xem tat ca sanh sach mon hoc
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            //Chiendd1: 06/07/2015
            //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classid.HasValue && semesterid.HasValue)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterid.Value, classid.Value, ViewAll).Where(u => semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                Session["ListSubject"] = lsClassSubject;
            }
            List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
            int UserAccountID = _globalInfo.UserAccountID;

            // Neu la admin truong thi hien thi het
            if (new GlobalInfo().IsAdminSchoolRole)
            {
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                }
            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = null; ;
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classid &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                        /*listPermitSubjectID = TeachingAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classid &&
                                                            o.Semester == semester &&
                                                            o.IsActive).Select(o => o.SubjectID).ToList();*/
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = classid;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = semester;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                    }
                }
                foreach (SubjectCatBO item in lsClassSubject)
                {
                    // todo: haivt9 review code, GetTypeSubject su dung Session["ListSubject"] cung chinh la lsClassSubject, mo 2 tab -> sai typeSubject -> fix su dung lsClassSubject
                    bool typeSubject = this.GetTypeSubject(item.SubjectCatID) == SubjectMark;
                    if (listPermitSubjectID.Contains(item.SubjectCatID))
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, true, typeSubject));
                    }
                    else
                    {
                        listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, typeSubject));
                    }
                }
            }
            ViewData["ListSubjectComment"] = lsClassSubject;
            ViewData[BookRecordTeachersConstant.LIST_SUBJECT] = listSJ;
            ViewData[BookRecordTeachersConstant.ShowList] = false;
            return PartialView("_ListSubject");
        }

        public const int SubjectMark = 1;
        public const int SubjectJudge = 2;

        [HttpPost]
        public int GetTypeSubject(int? SubjectID)
        {
            int typeSubject = 0;
            //const 1 mon tinh diem va mon tinh diem ket hop nhan xet; 2 mon nhan xet

            if (Session["ListSubject"] != null)
            {
                List<SubjectCatBO> lsClassSubject = Session["ListSubject"] as List<SubjectCatBO>;

                var lclassSubject = lsClassSubject.Where(o => o.SubjectCatID == SubjectID.Value);

                if (lclassSubject != null && lclassSubject.Count() > 0)
                {
                    SubjectCatBO classSubject = lclassSubject.FirstOrDefault();

                    if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        return SubjectMark;
                    }
                    else
                    {
                        return SubjectJudge;
                    }
                }
            }
            return typeSubject;
        }


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    foreach (var item in listPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                        listPeriodDeclaration.Add(periodDeclaration);
                    }
                }
                ViewData[BookRecordTeachersConstant.LIST_SEMESTER] = SemesterID.Value;
                ViewData[BookRecordTeachersConstant.ShowList] = false;
                return Json(new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution"));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

    }
}
