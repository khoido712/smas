﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.JudgeRecordSemesterArea
{
    public class JudgeRecordSemesterAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "JudgeRecordSemesterArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "JudgeRecordSemesterArea_default",
                "JudgeRecordSemesterArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
