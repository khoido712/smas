﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.JudgeRecordSemesterArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using SMAS.Models.Models.CustomModels;
using System.Configuration;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.JudgeRecordSemesterArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class JudgeRecordSemesterController : BaseController
    {
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        public JudgeRecordSemesterController(IJudgeRecordBusiness judgerecordBusiness, IJudgeRecordHistoryBusiness judgeRecordHistoryBusiness, IPeriodDeclarationBusiness perioddeclarationbusiness,
            IClassProfileBusiness classprofilebusiness, IClassSubjectBusiness classsubjectbusiness, ITeachingAssignmentBusiness teachingassignmentbusiness,
            IMarkTypeBusiness marktypebusiness, ISemeterDeclarationBusiness semeterdeclarationbusiness, IAcademicYearBusiness academicyearbusiness,
            ISummedUpRecordBusiness summeduprecordbusiness, ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness, ILockedMarkDetailBusiness lockedmarkdetailbusiness,
            IMarkRecordBusiness markrecordbusiness, IMarkRecordHistoryBusiness markRecordHistoryBusiness,
            ISubjectCatBusiness subjectcatbusiness, IExemptedSubjectBusiness exemptedsubjectbusiness, IPupilOfClassBusiness pupilOfClassBusiness, IPupilProfileBusiness pupilProfileBusiness
            , IUserAccountBusiness UserAccountBusiness, IEmployeeBusiness employeeBusiness,
            IRestoreDataBusiness RestoreDataBusiness,
            IRestoreDataDetailBusiness RestoreDataDetailBusiness)
        {
            this.JudgeRecordBusiness = judgerecordBusiness;
            this.JudgeRecordHistoryBusiness = judgeRecordHistoryBusiness;
            this.PeriodDeclarationBusiness = perioddeclarationbusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.ClassSubjectBusiness = classsubjectbusiness;
            this.TeachingAssignmentBusiness = teachingassignmentbusiness;
            this.MarkTypeBusiness = marktypebusiness;
            this.SemeterDeclarationBusiness = semeterdeclarationbusiness;
            this.AcademicYearBusiness = academicyearbusiness;
            this.SummedUpRecordBusiness = summeduprecordbusiness;
            this.SummedUpRecordHistoryBusiness = summedUpRecordHistoryBusiness;
            this.LockedMarkDetailBusiness = lockedmarkdetailbusiness;
            this.MarkRecordBusiness = markrecordbusiness;
            this.MarkRecordHistoryBusiness = markRecordHistoryBusiness;
            this.SubjectCatBusiness = subjectcatbusiness;
            this.ExemptedSubjectBusiness = exemptedsubjectbusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
        }

        public ActionResult Index()
        {
            var global = new GlobalInfo();
            if (global.HasSubjectTeacherPermission(0, 0))
            {
                //rptSemester: CommonList.Semester, mặc định: UserInfo.Semester
                var lstSemester = CommonList.Semester();
                List<ViettelCheckboxList> lstViettelCheckboxListSemester = new List<ViettelCheckboxList>();
                lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[0].value, lstSemester[0].key, int.Parse(lstSemester[0].key) == global.Semester ? true : false, false));
                lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[1].value, lstSemester[1].key, int.Parse(lstSemester[1].key) == global.Semester ? true : false, false));
                ViewData[JudgeRecordSemesterConstants.LIST_SEMESTER] = lstViettelCheckboxListSemester;
                //rptEducationLevel: lấy thông tin UserInfo.EducationLevels (mặc định tích chọn giá trị đầu tiên trả về)
                var lstEducationlevel = global.EducationLevels;
                List<ViettelCheckboxList> lstViettelCheckboxListEducationlevel = new List<ViettelCheckboxList>();
                lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(lstEducationlevel[0].Resolution, lstEducationlevel[0].EducationLevelID, true, false));
                foreach (var item in lstEducationlevel.Skip(1))
                {
                    lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, false, false));
                }
                ViewData[JudgeRecordSemesterConstants.LIST_EDUCATIONLEVEL] = lstViettelCheckboxListEducationlevel;
                //- rptClass: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary) với
                //  + Dictionnary[“EducationLevelID”] = cboEducationLevel.Value
                //  + Dictionnary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                //  + Dictionnary[“UserAccountID”] = UserInfo.UserAccountID  
                //  + Dictionary[“Type”] = TEACHER_ROLE_SUBJECTTEACHER (GVBM). 
                IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
                ClassSearchInfo["EducationLevelID"] = lstEducationlevel.FirstOrDefault().EducationLevelID;
                ClassSearchInfo["AcademicYearID"] = global.AcademicYearID;
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    ClassSearchInfo["UserAccountID"] = global.UserAccountID;
                    ClassSearchInfo["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                var lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, ClassSearchInfo).OrderBy(o => o.DisplayName).ToList();
                ViewData[JudgeRecordSemesterConstants.LIST_CLASS] = lstClass;
                ViewData[JudgeRecordSemesterConstants.ERROR_BASIC_DATA] = false;
                ViewData[JudgeRecordSemesterConstants.LIST_SEMESTERDECLARATION] = new SemeterDeclaration();
                ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDOFCLASS] = new List<JudgeRecordBO>();
                ViewData[JudgeRecordSemesterConstants.ERROR_IMPORT_MESSAGE] = string.Empty;
                ViewData[JudgeRecordSemesterConstants.SEMESTER] = global.Semester;

                ViewData[JudgeRecordSemesterConstants.SEARCHTITLE] = string.Empty;
                return View();
            }
            return RedirectToAction("LogOn", "Home");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetListClass(int? id)
        {
            if (id.HasValue)
            {
                var global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID.Value;
                dic["EducationLevelID"] = id.Value;
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
                ViewData[JudgeRecordSemesterConstants.LIST_CLASS] = listClass;
            }
            return PartialView("_ListClass");
        }

        [HttpPost]
        public PartialViewResult GetListSubject()
        {
            int classid = string.IsNullOrWhiteSpace(Request["id"]) ? 0 : int.Parse(Request["id"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["semesterid"]) ? 0 : int.Parse(Request["semesterid"]);
            if (classid > 0 && semesterid > 0)
            {
                var global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["IsCommenting"] = SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                dic["ClassID"] = classid;
                dic["Semester"] = semesterid;
                List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
                //Lấy danh sách môn học dựa vào quyền của giáo viên và có  IsCommenting = ISCOMMENTING_TYPE_JUDGE (Môn nhận xét)

                List<SubjectCat> listSubject = new List<SubjectCat>();
                if (global.AcademicYearID.HasValue && global.SchoolID.HasValue)
                {
                    listSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(global.UserAccountID,
                        global.AcademicYearID.Value, global.SchoolID.Value, semesterid, classid,
                        global.IsViewAll)
                                 .Where(o => o.SubjectCat.IsActive == true)
                                 .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                 .Select(o => o.SubjectCat)
                                 .ToList();
                }
                listSubject = listSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                foreach (var item in listSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                }

                ViewData[JudgeRecordSemesterConstants.LIST_SUBJECT] = listSJ;
                var subject = (listSubject != null && listSubject.Count > 0) ? listSubject.FirstOrDefault() : null;
                ViewData[JudgeRecordSemesterConstants.HASPERMISION] = subject != null ? new GlobalInfo().HasSubjectTeacherPermission(classid, subject.SubjectCatID) : false;
            }
            return PartialView("_ListSubject");
        }
        //
        // GET: /JudgeRecord/Search

        [HttpPost]
        public PartialViewResult Search()
        {
            ViewData[JudgeRecordSemesterConstants.ERROR_BASIC_DATA] = false;
            ViewData[JudgeRecordSemesterConstants.ERROR_IMPORT_MESSAGE] = string.Empty;

            var global = new GlobalInfo();
            int classid = string.IsNullOrWhiteSpace(Request["classid"]) ? 0 : int.Parse(Request["classid"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["semesterid"]) ? 0 : int.Parse(Request["semesterid"]);
            int subjectid = string.IsNullOrWhiteSpace(Request["subjectid"]) ? 0 : int.Parse(Request["subjectid"]);
            int educationlevelid = string.IsNullOrWhiteSpace(Request["educationlevelid"]) ? 0 : int.Parse(Request["educationlevelid"]);

            string listPupilExempted = string.Empty;
            //Load tiêu đề hiển thị con điểm: gọi hàm MarkTypeBusiness.Search() với điều kiện truyền vào AppliedLevel = UserInfo.AppliedLevel
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            var lstMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();

            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            //Load thông tin số các con điểm M, P, V
            IDictionary<string, object> SearchInfoSemester = new Dictionary<string, object>();
            SearchInfoSemester["AcademicYearID"] = global.AcademicYearID;
            SearchInfoSemester["SchoolID"] = global.SchoolID;
            //SearchInfoSemester["Year"] = academicYear.Year;
            SearchInfoSemester["Semester"] = semesterid;
            SemeterDeclaration SemesterDecalaration = SemeterDeclarationBusiness.Search(SearchInfoSemester).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            if (SemesterDecalaration == null)
                throw new BusinessException("Validate_School_NotMark");

            ClassSubject classSubject = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", classid }, { "SubjectID", subjectid } }).SingleOrDefault();

            IDictionary<string, object> SearchInfoJudgeRecord = new Dictionary<string, object>();
            SearchInfoJudgeRecord["SchoolID"] = global.SchoolID;
            SearchInfoJudgeRecord["AcademicYearID"] = global.AcademicYearID;
            SearchInfoJudgeRecord["Semester"] = ViewData[JudgeRecordSemesterConstants.SEMESTER] = semesterid;
            SearchInfoJudgeRecord["ClassID"] = ViewData["classid"] = classid;
            SearchInfoJudgeRecord["SubjectID"] = ViewData["subjectid"] = subjectid;
            var lstJudgeRecordOfClass = JudgeRecordBusiness.GetJudgeRecordOfClass(global.AcademicYearID.Value, global.SchoolID.Value, subjectid, (int)semesterid, classid, null).ToList();

            IDictionary<string, object> SearchInfoSummedUpRecord = new Dictionary<string, object>();
            SearchInfoJudgeRecord["SchoolID"] = global.SchoolID;
            SearchInfoJudgeRecord["AcademicYearID"] = global.AcademicYearID;
            SearchInfoJudgeRecord["Semester"] = semesterid;
            SearchInfoJudgeRecord["ClassID"] = classid;
            SearchInfoJudgeRecord["SubjectID"] = subjectid;
            var lstSummedUpRecordOfClass = SummedUpRecordBusiness.GetSummedUpRecordOfClass(global.AcademicYearID.Value, global.SchoolID.Value, semesterid, classid, null, subjectid)
                                                                .Where(o => o.SubjectID == subjectid && o.SchoolID == global.SchoolID && o.AcademicYearID == global.AcademicYearID).ToList();

            List<SummedUpRecordBO> lstSummedUpRecordI = null;
            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                lstSummedUpRecordI = lstSummedUpRecordOfClass;
            else
                lstSummedUpRecordI = SummedUpRecordBusiness.GetSummedUpRecordOfClass(global.AcademicYearID.Value, global.SchoolID.Value, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, classid, null, subjectid).ToList();

            IDictionary<string, object> SearchInfoSurIII = new Dictionary<string, object>();
            SearchInfoSurIII["AcademicYearID"] = global.AcademicYearID;
            SearchInfoSurIII["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            SearchInfoSurIII["ClassID"] = classid;
            SearchInfoSurIII["SubjectID"] = subjectid;
            List<SummedUpRecord> lstSummedUpRecordIII = SummedUpRecordBusiness.SearchBySchool(global.SchoolID.Value, SearchInfoSurIII).Where(u => !u.PeriodID.HasValue).ToList();

            //+ Lấy danh sách khóa con điểm  
            string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(global.SchoolID.Value, global.AcademicYearID.Value, classid, (int)semesterid, (int)subjectid);
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(",", " ");
            lstLockedMarkDetail = lstLockedMarkDetail.TrimStart();
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(" ", ", ");
            List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classid, semesterid).Where(u => u.SubjectID == subjectid).ToList();

            ViewData[JudgeRecordSemesterConstants.LIST_SEMESTERDECLARATION] = SemesterDecalaration;
            ViewData[JudgeRecordSemesterConstants.LIST_MARKTYPE] = lstMarkType;

            List<JudgeRecordBO> List_JudgeRecordBO = new List<JudgeRecordBO>();
            List<JudgeRecordBO> listPupilNotTBMHKI = new List<JudgeRecordBO>();

            #region Lay du lieu cho grid
            foreach (var JudgeRecordBO in lstJudgeRecordOfClass)
            {
                bool flag = true;
                bool displayMark = true;
                //int m = 0; int p = 0; int v = 0;
                //-	Với học sinh có trạng thái thuộc một trong các trạng thái chuyển trường (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL),
                //chuyển lớp (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_CLASS),
                //thôi học (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_LEAVED_OFF) (lấy trạng thái học sinh trong bảng PupilOfClass)
                //thì không hiển thị điểm, chỉ hiển thị danh sách.
                if (JudgeRecordBO.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                    JudgeRecordBO.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    JudgeRecordBO.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_LEAVED_OFF)
                {
                    JudgeRecordBO.NotStudyingPupil = true;
                    flag = false;
                    if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (JudgeRecordBO.EndDate.HasValue && academicYear.FirstSemesterEndDate >= JudgeRecordBO.EndDate.Value)
                        {
                            displayMark = false;
                            //flag = false;
                        }
                    }
                    else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (JudgeRecordBO.EndDate.HasValue && academicYear.SecondSemesterEndDate >= JudgeRecordBO.EndDate.Value)
                        {
                            displayMark = false;
                            //flag = false;
                        }
                    }
                    // Neu ko co ngay ket thuc cung ko hien thi diem
                    if (!JudgeRecordBO.EndDate.HasValue)
                    {
                        displayMark = false;
                    }
                }
                // Học sinh tốt nghiệp không được nhập điểm
                if (JudgeRecordBO.Status == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    flag = false;
                }
                //Ø	Học sinh được miễn giảm không được nhập điểm: 
                bool IsExemptedSubject = lstExempted.Any(u => u.PupilID == JudgeRecordBO.PupilID);
                if (IsExemptedSubject)
                {
                    flag = false;
                    listPupilExempted += JudgeRecordBO.PupilID.Value + ",";
                }

                //namdv3 // 17-04-2013 fix bug Truong xet quyen cho giao vien xem tat ca.
                //JudgeRecordBO.EnableMark = flag || global.IsViewAll;
                //JudgeRecordBO.DisplayMark = displayMark || global.IsViewAll;
                JudgeRecordBO.EnableMark = flag;
                JudgeRecordBO.DisplayMark = displayMark;
                List<JudgeRecordBO> listPupilJudgeRecord = lstJudgeRecordOfClass.Where(o => o.PupilID == JudgeRecordBO.PupilID).ToList();
                JudgeRecordBO.InterviewMark = new string[Convert.ToByte(SemesterDecalaration.InterviewMark)];
                JudgeRecordBO.WritingMark = new string[Convert.ToByte(SemesterDecalaration.WritingMark)];
                JudgeRecordBO.TwiceCoeffiecientMark = new string[Convert.ToByte(SemesterDecalaration.TwiceCoeffiecientMark)];

                foreach (JudgeRecordBO mr in listPupilJudgeRecord)
                {
                    if (mr.Title != null)
                    {
                        if (mr.Title.Contains("M"))
                        {
                            for (int m = 1; m <= SemesterDecalaration.InterviewMark; m++)
                            {
                                if (mr.Title == ("M" + m.ToString()))
                                {
                                    JudgeRecordBO.InterviewMark[m - 1] = mr.Judgement;
                                }
                            }
                        }
                        if (mr.Title.Contains("P"))
                        {
                            for (int p = 1; p <= SemesterDecalaration.WritingMark; p++)
                            {
                                if (mr.Title == ("P" + p.ToString()))
                                {
                                    JudgeRecordBO.WritingMark[p - 1] = mr.Judgement;
                                }
                            }
                        }
                        if (mr.Title.Contains("V"))
                        {
                            for (int v = 1; v <= SemesterDecalaration.TwiceCoeffiecientMark; v++)
                            {
                                if (mr.Title == ("V" + v.ToString()))
                                {
                                    JudgeRecordBO.TwiceCoeffiecientMark[v - 1] = mr.Judgement;
                                }
                            }
                        }
                        if (mr.Title == "HK")
                        {
                            JudgeRecordBO.SemesterMark = mr.Judgement;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                //Điểm TBM cả năm
                if (JudgeRecordBO.Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    var sum = lstSummedUpRecordIII.Where(u => u.PupilID == JudgeRecordBO.PupilID).FirstOrDefault();
                    JudgeRecordBO.TBMYearMark = sum != null ? sum.JudgementResult : string.Empty;
                }

                if (!List_JudgeRecordBO.Any(o => o.PupilID == JudgeRecordBO.PupilID))
                {
                    List_JudgeRecordBO.Add(JudgeRecordBO);
                }

                //Những học sinh chưa tổng kết điểm học kì 1 mà không phải thuộc diện miễn giảm
                if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    var sur1 = lstSummedUpRecordI.Where(u => u.PupilID == JudgeRecordBO.PupilID).FirstOrDefault();
                    if ((sur1 == null || string.IsNullOrEmpty(sur1.JudgementResult)) && !IsExemptedSubject)
                    {
                        listPupilNotTBMHKI.Add(JudgeRecordBO);
                    }
                }
            }
            #endregion

            ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDOFCLASS] = List_JudgeRecordBO.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDNOTTBMHKI] = listPupilNotTBMHKI.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_SUMMEDUPRECORDOFCLASS] = lstSummedUpRecordOfClass.ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_LOCKEDMARKDETAIL] = lstLockedMarkDetail;

            Session["List_JudgeRecordBO"] = List_JudgeRecordBO;
            //Được lưu dưới dạng: “Sổ điểm môn nhận xét -” + tên môn học chọn + “- Lớp ” + tên lớp
            var strTitle = Res.Get("JudgeRecordSemester_Label_SearchTitle");

            ViewData[JudgeRecordSemesterConstants.SEARCHTITLE] = string.Format(strTitle, classSubject.SubjectCat.DisplayName.ToUpper()
                                                                                        , classSubject.ClassProfile.DisplayName.ToUpper()
                                                                                        , semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II");

            var lstJudgeRecordMark = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordSemesterConstants.JUDGERECORDMARK] = new SelectList(lstJudgeRecordMark, "Key", "Value");
            var lstJudgeRecordMarkTBM = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordSemesterConstants.JUDGERECORDMARKTBM] = new SelectList(lstJudgeRecordMarkTBM, "Key", "Value");
            var m_type = lstMarkType.Where(o => o.Title == "M").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.M_COEFFICIENT] = m_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.M_TYPE] = m_type.Title;
            var p_type = lstMarkType.Where(o => o.Title == "P").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.P_COEFFICIENT] = p_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.P_TYPE] = p_type.Title;
            var v_type = lstMarkType.Where(o => o.Title == "V").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.V_COEFFICIENT] = v_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.V_TYPE] = v_type.Title;
            var hk_type = lstMarkType.Where(o => o.Title == "HK").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.HK_COEFFICIENT] = hk_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.HK_TYPE] = hk_type.Title;

            //ViewData[JudgeRecordSemesterConstants.HASPERMISION] = new GlobalInfo().HasSubjectTeacherPermission(classid, subjectid);
            ViewData["listPupilExempted"] = listPupilExempted;

            bool isPermissionSupervising = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, classid, subjectid, semesterid);
            if (isPermissionSupervising)
            {
                ViewData[JudgeRecordSemesterConstants.ISPERMISSIONSUPERVISING] = true;

            }
            else
            {
                ViewData[JudgeRecordSemesterConstants.ISPERMISSIONSUPERVISING] = false;
            }

            ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
            bool check = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, classid, subjectid, semesterid);
            if (global.IsCurrentYear == false)
            {
                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
            }
            else
            {
                if (check == false)
                {
                    ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                }
                else
                {
                    if (global.IsAdminSchoolRole)
                    {
                        ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                    }
                    else
                    {
                        if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (DateTime.Now <= academicYear.FirstSemesterEndDate && DateTime.Now >= academicYear.FirstSemesterStartDate)
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                            }
                            else
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                            }
                        }
                        else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (DateTime.Now <= academicYear.SecondSemesterEndDate && DateTime.Now >= academicYear.SecondSemesterStartDate)
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                            }
                            else
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                            }
                        }
                    }
                }
            }
            return PartialView("_List");
        }
        /// <summary>
        /// Namta
        /// </summary>
        /// <returns></returns>
        [HttpPost]

        public PartialViewResult SearchNew()
        {
            ViewData[JudgeRecordSemesterConstants.ERROR_BASIC_DATA] = false;
            ViewData[JudgeRecordSemesterConstants.ERROR_IMPORT_MESSAGE] = string.Empty;

            var global = new GlobalInfo();
            int classid = string.IsNullOrWhiteSpace(Request["classid"]) ? 0 : int.Parse(Request["classid"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["semesterid"]) ? 0 : int.Parse(Request["semesterid"]);
            int subjectid = string.IsNullOrWhiteSpace(Request["subjectid"]) ? 0 : int.Parse(Request["subjectid"]);
            int educationlevelid = string.IsNullOrWhiteSpace(Request["educationlevelid"]) ? 0 : int.Parse(Request["educationlevelid"]);

            string listPupilExempted = string.Empty;
            //Load tiêu đề hiển thị con điểm: gọi hàm MarkTypeBusiness.Search() với điều kiện truyền vào AppliedLevel = UserInfo.AppliedLevel
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            var lstMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();

            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            //Load thông tin số các con điểm M, P, V
            IDictionary<string, object> SearchInfoSemester = new Dictionary<string, object>();
            SearchInfoSemester["AcademicYearID"] = global.AcademicYearID;
            SearchInfoSemester["SchoolID"] = global.SchoolID;
            SearchInfoSemester["Year"] = academicYear.Year;
            SearchInfoSemester["Semester"] = semesterid;
            SemeterDeclaration SemesterDecalaration = SemeterDeclarationBusiness.Search(SearchInfoSemester).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
            if (SemesterDecalaration == null)
                throw new BusinessException("Validate_School_NotMark");

            ClassSubject classSubject = ClassSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", classid }, { "SubjectID", subjectid } }).SingleOrDefault();

            IDictionary<string, object> SearchInfoJudgeRecord = new Dictionary<string, object>();
            SearchInfoJudgeRecord["SchoolID"] = global.SchoolID;
            SearchInfoJudgeRecord["AcademicYearID"] = global.AcademicYearID;
            SearchInfoJudgeRecord["Semester"] = ViewData[JudgeRecordSemesterConstants.SEMESTER] = semesterid;
            SearchInfoJudgeRecord["ClassID"] = ViewData["classid"] = classid;
            SearchInfoJudgeRecord["SubjectID"] = ViewData["subjectid"] = subjectid;
            SearchInfoJudgeRecord["PeriodID"] = 0;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            Dictionary<string, object> dicSearchPOC = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classid}
            };
            IQueryable<PupilOfClass> iqueryPOC = PupilOfClassBusiness.Search(dicSearchPOC).AddPupilStatus(isNotShowPupil);
            List<int> lstPupilID = iqueryPOC.Select(p => p.PupilID).Distinct().ToList();

            var listJudge = JudgeRecordBusiness.GetJudgeRecordOfClassNew(SearchInfoJudgeRecord);
            if (listJudge != null)
            {
                listJudge = listJudge.Where(p => lstPupilID.Contains(p.PUPIL_ID)).ToList();
            }

            //lay ra danh logchange cua hoc sinh theo tung con diem
            List<BookRecordLogChangeBO> listLogChange = new List<BookRecordLogChangeBO>();

            if (UtilsBusiness.IsMoveHistory(aca))
            {
                listLogChange = (from mr in JudgeRecordHistoryBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            else
            {
                listLogChange = (from mr in JudgeRecordBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            List<int?> lstEmployeeID = listLogChange.Select(p => p.LogChange).Distinct().ToList();

            //viethd4: lay ra cac cap nhat gan nhat
            var lastUpdate = listLogChange.OrderByDescending(o => o.UpdateDate).FirstOrDefault();
            List<MarkRecordBO> lstLastMarkRecordID = new List<MarkRecordBO>();

            if (lastUpdate != null)
            {
                string lastUpdateTime = lastUpdate.UpdateDate != null ? lastUpdate.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") : null;
                lstLastMarkRecordID = listLogChange.Where(o => lastUpdateTime != null && o.UpdateDate != null ? o.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") == lastUpdateTime : false).Select(o =>
                         new MarkRecordBO
                         {
                             PupilID = o.PupilID,
                             Title = o.Title
                         }).ToList();

            }
            ViewData[JudgeRecordSemesterConstants.LAST_UPDATE_MARK_RECORD] = lstLastMarkRecordID;

            List<Employee> lstEmployeeChange = new List<Employee>();
            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = (from e in EmployeeBusiness.All
                                     where e.SchoolID == _globalInfo.SchoolID
                                     && lstEmployeeID.Contains(e.EmployeeID)
                                     select e).ToList();
            }
            Employee objEmployee = null;

            //viethd4: lay user va time cap nhat sau cung

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            if (lastUpdate != null)
            {
                string lastUserName = String.Empty;
                if (lastUpdate.LogChange == 0)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    Employee lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChange).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }

                tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.UpdateDate.Value.Hour.ToString(),
                        lastUpdate.UpdateDate.Value.Minute.ToString("D2"),
                        lastUpdate.UpdateDate,
                        !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                        lastUserName});

                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
            }

            ViewData[JudgeRecordSemesterConstants.LAST_UPDATE_STRING] = strLastUpdate;

            List<JudgeRecordNewBO> lstJudgeRecordOfClass = new List<JudgeRecordNewBO>();

            if (listJudge != null && listJudge.Count() > 0)
            {
                lstJudgeRecordOfClass = listJudge.ToList();
            }

            //tao cac ban ghi summedup
            List<SummedUpRecordBO> lstSummedUpRecordI = lstJudgeRecordOfClass.Where(u => u.HK1 != null).Select(o => new SummedUpRecordBO()
            {
                PupilID = o.PUPIL_ID,
                ClassID = classid,
                SubjectID = subjectid,
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST,
                JudgementResult = o.HK1,
            }).ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_SUMMEDUPRECORDOFCLASS] = lstSummedUpRecordI;

            List<SummedUpRecordBO> lstSummedUpRecordII = lstJudgeRecordOfClass.Where(u => u.HK2 != null).Select(o => new SummedUpRecordBO()
            {
                PupilID = o.PUPIL_ID,
                ClassID = classid,
                SubjectID = subjectid,
                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND,
                JudgementResult = o.HK2,
            }).ToList();
            List<SummedUpRecordBO> lstSummedUpRecordIII = new List<SummedUpRecordBO>();
            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                ViewData[JudgeRecordSemesterConstants.LIST_SUMMEDUPRECORDOFCLASS] = lstSummedUpRecordI;
            }
            else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                ViewData[JudgeRecordSemesterConstants.LIST_SUMMEDUPRECORDOFCLASS] = lstSummedUpRecordII;
                // HK2 moi lay du lieu ca nam
                lstSummedUpRecordIII = lstJudgeRecordOfClass.Where(u => u.CN != null).Select(o => new SummedUpRecordBO()
                {
                    PupilID = o.PUPIL_ID,
                    ClassID = classid,
                    SubjectID = subjectid,
                    Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                    JudgementResult = o.CN,
                }).ToList();
            }

            //+ Lấy danh sách khóa con điểm  
            string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(global.SchoolID.Value, global.AcademicYearID.Value, classid, (int)semesterid, (int)subjectid);
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(",", " ");
            lstLockedMarkDetail = lstLockedMarkDetail.TrimStart();
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(" ", ", ");
            List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classid, semesterid).Where(u => u.SubjectID == subjectid).ToList();

            ViewData[JudgeRecordSemesterConstants.LIST_SEMESTERDECLARATION] = SemesterDecalaration;
            ViewData[JudgeRecordSemesterConstants.LIST_MARKTYPE] = lstMarkType;

            List<JudgeRecordBO> List_JudgeRecordBO = new List<JudgeRecordBO>();
            List<JudgeRecordBO> listPupilNotTBMHKI = new List<JudgeRecordBO>();

            #region Lay du lieu cho grid
            foreach (var JudgeBO in lstJudgeRecordOfClass)
            {
                JudgeRecordBO JudgeRecordBO = new JudgeRecordBO();
                JudgeRecordBO.PupilID = JudgeBO.PUPIL_ID;
                JudgeRecordBO.FullName = JudgeBO.FULL_NAME;
                JudgeRecordBO.Name = JudgeBO.S_NAME;
                JudgeRecordBO.OrderInClass = JudgeBO.ORDER_IN_CLASS;
                JudgeRecordBO.EndDate = JudgeBO.END_DATE;
                JudgeRecordBO.Status = JudgeBO.STATUS;
                bool flag = true;
                bool displayMark = true;
                //int m = 0; int p = 0; int v = 0;
                //-	Với học sinh có trạng thái thuộc một trong các trạng thái chuyển trường (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL),
                //chuyển lớp (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_CLASS),
                //thôi học (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_LEAVED_OFF) (lấy trạng thái học sinh trong bảng PupilOfClass)
                //thì không hiển thị điểm, chỉ hiển thị danh sách.
                if (JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                    JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_LEAVED_OFF)
                {
                    JudgeRecordBO.NotStudyingPupil = true;
                    flag = false;
                    if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (JudgeBO.END_DATE.HasValue && academicYear.FirstSemesterEndDate >= JudgeBO.END_DATE.Value)
                        {
                            displayMark = false;
                            //flag = false;
                        }
                    }
                    else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (JudgeBO.END_DATE.HasValue && academicYear.SecondSemesterEndDate >= JudgeBO.END_DATE.Value)
                        {
                            displayMark = false;
                            //flag = false;
                        }
                    }
                    // Neu ko co ngay ket thuc cung ko hien thi diem
                    if (!JudgeBO.END_DATE.HasValue)
                    {
                        displayMark = false;
                    }
                }
                // Học sinh tốt nghiệp không được nhập điểm
                if (JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    flag = false;
                }
                //Ø	Học sinh được miễn giảm không được nhập điểm: 
                bool IsExemptedSubject = lstExempted.Any(u => u.PupilID == JudgeBO.PUPIL_ID);
                if (IsExemptedSubject)
                {
                    flag = false;
                    listPupilExempted += JudgeBO.PUPIL_ID + ",";
                }

                //namdv3 // 17-04-2013 fix bug Truong xet quyen cho giao vien xem tat ca.
                //JudgeRecordBO.EnableMark = flag || global.IsViewAll;
                //JudgeRecordBO.DisplayMark = displayMark || global.IsViewAll;
                JudgeRecordBO.EnableMark = flag;
                JudgeRecordBO.DisplayMark = displayMark;
                JudgeRecordBO.InterviewMark = new string[Convert.ToByte(SemesterDecalaration.InterviewMark)];
                JudgeRecordBO.WritingMark = new string[Convert.ToByte(SemesterDecalaration.WritingMark)];
                JudgeRecordBO.TwiceCoeffiecientMark = new string[Convert.ToByte(SemesterDecalaration.TwiceCoeffiecientMark)];
                JudgeRecordBO.LogChangeM = new string[SemesterDecalaration.InterviewMark];
                JudgeRecordBO.LogChangeP = new string[SemesterDecalaration.WritingMark];
                JudgeRecordBO.LogChangeV = new string[SemesterDecalaration.TwiceCoeffiecientMark];

                //Kiem tra xem hoc sinh co diem HK1 khong
                if (JudgeBO.HK1 != "" && JudgeBO.HK1 != null)
                {
                    JudgeRecordBO.HasSumUpSemester1 = true;
                }
                else
                {
                    JudgeRecordBO.HasSumUpSemester1 = false;
                }

                for (int m = 1; m <= SemesterDecalaration.InterviewMark; m++)
                {
                    objEmployee = null;
                    object Judge = Utils.Utils.GetValueByProperty(JudgeBO, "M" + m.ToString());
                    var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("M" + m)).FirstOrDefault();
                    if (Judge != null)
                        JudgeRecordBO.InterviewMark[m - 1] = Judge.ToString();
                    if (objLogChange != null && objLogChange.LogChange > 0)
                    {
                        objEmployee = lstEmployeeChange.Where(e => e.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                    }
                    JudgeRecordBO.LogChangeM[m - 1] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                }

                for (int p = 1; p <= SemesterDecalaration.WritingMark; p++)
                {
                    objEmployee = null;
                    object Judge = Utils.Utils.GetValueByProperty(JudgeBO, "P" + p.ToString());
                    var objLogChange = listLogChange.Where(l => l.PupilID == JudgeBO.PUPIL_ID && l.Title.Equals("P" + p)).FirstOrDefault();
                    if (Judge != null)
                        JudgeRecordBO.WritingMark[p - 1] = Judge.ToString();
                    if (objLogChange != null && objLogChange.LogChange > 0)
                    {
                        objEmployee = lstEmployeeChange.Where(e => e.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                    }
                    JudgeRecordBO.LogChangeP[p - 1] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                }
                for (int v = 1; v <= SemesterDecalaration.TwiceCoeffiecientMark; v++)
                {
                    objEmployee = null;
                    object Judge = Utils.Utils.GetValueByProperty(JudgeBO, "V" + v.ToString());
                    var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("V" + v)).FirstOrDefault();
                    if (Judge != null)
                        JudgeRecordBO.TwiceCoeffiecientMark[v - 1] = Judge.ToString();
                    if (objLogChange != null && objLogChange.LogChange > 0)
                    {
                        objEmployee = lstEmployeeChange.Where(e => e.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                    }
                    JudgeRecordBO.LogChangeV[v - 1] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                }

                objEmployee = null;
                object JudgeHK = Utils.Utils.GetValueByProperty(JudgeBO, "HK");
                if (JudgeHK != null)
                    JudgeRecordBO.SemesterMark = JudgeHK.ToString();
                var objLogChangeHK = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("HK")).FirstOrDefault();
                if (objLogChangeHK != null && objLogChangeHK.LogChange > 0)
                {
                    objEmployee = lstEmployeeChange.Where(e => e.EmployeeID == objLogChangeHK.LogChange).FirstOrDefault();
                }
                JudgeRecordBO.LogChangeHK = objLogChangeHK != null ? "Cập nhật " + (objLogChangeHK.ModifiedDate.HasValue ? objLogChangeHK.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChangeHK.CreateDate != null ? objLogChangeHK.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChangeHK.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";

                JudgeRecordBO.Title = "V" + SemesterDecalaration.TwiceCoeffiecientMark.ToString();
                //Điểm TBM cả năm
                if (semesterid == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    var sum = lstSummedUpRecordIII.Where(u => u.PupilID == JudgeBO.PUPIL_ID).FirstOrDefault();
                    JudgeRecordBO.TBMYearMark = sum != null ? sum.JudgementResult : string.Empty;
                }

                if (!List_JudgeRecordBO.Any(o => o.PupilID == JudgeBO.PUPIL_ID))
                {
                    List_JudgeRecordBO.Add(JudgeRecordBO);
                }

                //Những học sinh chưa tổng kết điểm học kì 1 mà không phải thuộc diện miễn giảm
                if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    var sur1 = lstSummedUpRecordI.Where(u => u.PupilID == JudgeBO.PUPIL_ID).FirstOrDefault();
                    if ((sur1 == null || string.IsNullOrEmpty(sur1.JudgementResult)) && !IsExemptedSubject)
                    {
                        listPupilNotTBMHKI.Add(JudgeRecordBO);
                    }
                }
            }
            #endregion

            ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDOFCLASS] = List_JudgeRecordBO.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDNOTTBMHKI] = listPupilNotTBMHKI.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            ViewData[JudgeRecordSemesterConstants.LIST_LOCKEDMARKDETAIL] = lstLockedMarkDetail;

            Session["List_JudgeRecordBO"] = List_JudgeRecordBO;
            //Được lưu dưới dạng: “Sổ điểm môn nhận xét -” + tên môn học chọn + “- Lớp ” + tên lớp
            var strTitle = Res.Get("JudgeRecordSemester_Label_SearchTitle");

            ViewData[JudgeRecordSemesterConstants.SEARCHTITLE] = string.Format(strTitle, classSubject.SubjectCat.DisplayName.ToUpper()
                                                                                        , classSubject.ClassProfile.DisplayName.ToUpper()
                                                                                        , semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II");

            var lstJudgeRecordMark = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordSemesterConstants.JUDGERECORDMARK] = new SelectList(lstJudgeRecordMark, "Key", "Value");
            var lstJudgeRecordMarkTBM = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordSemesterConstants.JUDGERECORDMARKTBM] = new SelectList(lstJudgeRecordMarkTBM, "Key", "Value");
            var m_type = lstMarkType.Where(o => o.Title == "M").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.M_COEFFICIENT] = m_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.M_TYPE] = m_type.Title;
            var p_type = lstMarkType.Where(o => o.Title == "P").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.P_COEFFICIENT] = p_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.P_TYPE] = p_type.Title;
            var v_type = lstMarkType.Where(o => o.Title == "V").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.V_COEFFICIENT] = v_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.V_TYPE] = v_type.Title;
            var hk_type = lstMarkType.Where(o => o.Title == "HK").FirstOrDefault();
            ViewData[JudgeRecordSemesterConstants.HK_COEFFICIENT] = hk_type.Coefficient;
            ViewData[JudgeRecordSemesterConstants.HK_TYPE] = hk_type.Title;

            //ViewData[JudgeRecordSemesterConstants.HASPERMISION] = new GlobalInfo().HasSubjectTeacherPermission(classid, subjectid);
            ViewData["listPupilExempted"] = listPupilExempted;

            bool isPermissionSupervising = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, classid, subjectid, semesterid);
            if (isPermissionSupervising)
            {
                ViewData[JudgeRecordSemesterConstants.ISPERMISSIONSUPERVISING] = true;

            }
            else
            {
                ViewData[JudgeRecordSemesterConstants.ISPERMISSIONSUPERVISING] = false;
            }

            ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
            bool check = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, classid, subjectid, semesterid);
            if (global.IsCurrentYear == false)
            {
                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
            }
            else
            {
                if (check == false)
                {
                    ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                }
                else
                {
                    if (global.IsAdminSchoolRole)
                    {
                        ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                    }
                    else
                    {
                        if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (DateTime.Now <= academicYear.FirstSemesterEndDate && DateTime.Now >= academicYear.FirstSemesterStartDate
                                && !lstLockedMarkDetail.Contains("LHK"))
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                            }
                            else
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                            }
                        }
                        else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (DateTime.Now <= academicYear.SecondSemesterEndDate && DateTime.Now >= academicYear.SecondSemesterStartDate
                                && !lstLockedMarkDetail.Contains("LHK"))
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = false;
                            }
                            else
                            {
                                ViewData[JudgeRecordSemesterConstants.LOCKACTION] = true;
                            }
                        }
                    }
                }
            }
            //Bien check so con diem toi thieu
            bool isMinSemester = false;
            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                isMinSemester = classSubject != null && ((classSubject.MMinFirstSemester.HasValue && classSubject.MMinFirstSemester.Value > 0)
                                   || classSubject.PMinFirstSemester.HasValue && classSubject.PMinFirstSemester.Value > 0
                                   || classSubject.VMinFirstSemester.HasValue && classSubject.VMinFirstSemester.Value > 0);
                ViewData[JudgeRecordSemesterConstants.MMIN] = classSubject != null && classSubject.MMinFirstSemester.HasValue ? classSubject.MMinFirstSemester.Value : 0;
                ViewData[JudgeRecordSemesterConstants.PMIN] = classSubject != null && classSubject.PMinFirstSemester.HasValue ? classSubject.PMinFirstSemester.Value : 0;
                ViewData[JudgeRecordSemesterConstants.VMIN] = classSubject != null && classSubject.VMinFirstSemester.HasValue ? classSubject.VMinFirstSemester.Value : 0;
            }
            else
            {
                isMinSemester = classSubject != null && ((classSubject.MMinSecondSemester.HasValue && classSubject.MMinSecondSemester.Value > 0)
                                   || classSubject.PMinSecondSemester.HasValue && classSubject.PMinSecondSemester.Value > 0
                                   || classSubject.VMinSecondSemester.HasValue && classSubject.VMinSecondSemester.Value > 0);
                ViewData[JudgeRecordSemesterConstants.MMIN] = classSubject != null && classSubject.MMinSecondSemester.HasValue ? classSubject.MMinSecondSemester.Value : 0;
                ViewData[JudgeRecordSemesterConstants.PMIN] = classSubject != null && classSubject.PMinSecondSemester.HasValue ? classSubject.PMinSecondSemester.Value : 0;
                ViewData[JudgeRecordSemesterConstants.VMIN] = classSubject != null && classSubject.VMinSecondSemester.HasValue ? classSubject.VMinSecondSemester.Value : 0;
            }
            ViewData[JudgeRecordSemesterConstants.ISMINSEMESTER] = isMinSemester;

            ViewData[JudgeRecordSemesterConstants.ISCLASSIFICATION] = academicYear.IsClassification;

            //Kiem tra khoa nhap lieu
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + semesterid);
            }
            ViewData[JudgeRecordSemesterConstants.IS_LOCK_INPUT] = isLockInput;
            // Tạo dữ liệu ghi log
            String subjectName = (classSubject != null && classSubject.SubjectCat != null) ? classSubject.SubjectCat.SubjectName : String.Empty;
            String className = (classSubject != null && classSubject.ClassProfile != null) ? classSubject.ClassProfile.DisplayName : String.Empty;
            SetViewDataActionAudit(String.Empty, String.Empty,
                classid.ToString(),
                "View Judge_record class_id: " + classid + ", subject_id: " + subjectid + ", semester: " + semesterid,
                "class_id: " + classid + ", subject_id: " + subjectid + ", semester: " + semesterid,
                "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_VIEW,
                "Xem sổ điểm lớp " + className + " học kì " + semesterid + " môn học " + subjectName);

            ViewData[JudgeRecordSemesterConstants.TITLE_STRING] = String.Format("sổ điểm môn <span style='color:#d56900 '>{0}</span> học kỳ <span style='color:#d56900 '>{1}</span>, lớp <span style='color:#d56900 '>{2}</span>",
                subjectName, semesterid, className);

            return PartialView("_List");
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJudgeSemesterRecord(FormCollection col)
        {
            string keyNames = "";
            try
            {
                bool isDBChange = false;
                //var listjudgerecord = col["rptJudgeRecord"];

                var listKeySelect = col.AllKeys.Where(o => o.Contains("rptJudgeRecord_"));

                StringBuilder listjudge = new StringBuilder();
                if (listKeySelect != null && listKeySelect.Count() > 0)
                {
                    listKeySelect.ToList().ForEach(u => listjudge.Append("," + u.Replace("rptJudgeRecord_", "")));
                }

                var listjudgerecord = listjudge.ToString();

                var listPupilExempted = col["txtlistPupilExempted"];
                if (string.IsNullOrWhiteSpace(listjudgerecord))
                {
                    return Json(new
                    {
                        Message = "Thầy/cô chưa chọn học sinh để lưu điểm",
                        Type = "success",
                        isDBChange = isDBChange
                    });
                }

                List<string> lstChangeMark = new List<string>();
                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;
                bool isInsertLog = false;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                List<OldMark> lstOldMark = new List<OldMark>();

                var global = new GlobalInfo();
                var AcademicYearID = global.AcademicYearID;
                var SchoolID = global.SchoolID;
                int classid = string.IsNullOrWhiteSpace(Request["txtclassid"]) ? 0 : int.Parse(Request["txtclassid"]);
                int subjectid = string.IsNullOrWhiteSpace(Request["txtsubjectid"]) ? 0 : int.Parse(Request["txtsubjectid"]);
                int semesterid = string.IsNullOrWhiteSpace(Request["txtsemesterid"]) ? 0 : int.Parse(Request["txtsemesterid"]);

                //Lock action de tranh giao dich dong thoi
                List<string> lstKeyName = new List<string>();
                lstKeyName.Add("MarkRecord");
                lstKeyName.Add(_globalInfo.UserAccountID.ToString());
                lstKeyName.Add(classid.ToString());//
                lstKeyName.Add(semesterid.ToString());
                lstKeyName.Add(subjectid.ToString());

                keyNames = VTUtils.LockAction.LockManager.CreatedKey(lstKeyName);
                if (!VTUtils.LockAction.LockManager.Lock(keyNames, Session.SessionID))
                {
                    return Json(new
                    {
                        Message = Res.Get("MarkRecord_Label_SavingMark"),
                        Type = JsonMessage.ERROR,
                        isDBChange = isDBChange
                    });
                }


                string lockTitle = Request["LockTitle"];
                // Kiem tra co mo khoa diem hay khong de khong bi xoa nham diem vua duoc mo khoa
                LockedMarkDetailBusiness.CheckCurrentLockMark(lockTitle, new Dictionary<string, object>()
                    {
                        {"Semester", semesterid},
                        {"ClassID", classid},
                        {"SubjectID", subjectid},
                        {"SchoolID", SchoolID.Value},
                        {"AcademicYearID", AcademicYearID},
                    });

                AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                //Load tiêu đề hiển thị con điểm: gọi hàm MarkTypeBusiness.Search() với điều kiện truyền vào AppliedLevel = UserInfo.AppliedLevel
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AppliedLevel"] = global.AppliedLevel;
                var lstMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();

                MarkType markTypeM = lstMarkType.SingleOrDefault(u => u.Title == "M");
                MarkType markTypeP = lstMarkType.SingleOrDefault(u => u.Title == "P");
                MarkType markTypeV = lstMarkType.SingleOrDefault(u => u.Title == "V");
                MarkType markTypeHK = lstMarkType.SingleOrDefault(u => u.Title == "HK");
                ClassSubject classSubject = ClassSubjectBusiness.SearchBySchool(SchoolID.Value, new Dictionary<string, object>() { { "ClassID", classid }, { "SubjectID", subjectid } }).SingleOrDefault();

                //Load thông tin số các con điểm M, P, V, …: gọi hàm SemesterDeclarationBussiness.SearchBySchool(UserInfo.SchoolID, IDictionary), 
                IDictionary<string, object> SearchInfoSemester = new Dictionary<string, object>();
                SearchInfoSemester["AcademicYearID"] = global.AcademicYearID;
                SearchInfoSemester["SchoolID"] = global.SchoolID;
                SearchInfoSemester["Year"] = acaYear.Year;
                SearchInfoSemester["Semester"] = semesterid;
                SemeterDeclaration SemesterDeclaration = SemeterDeclarationBusiness.Search(SearchInfoSemester).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();

                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", global.AcademicYearID }, { "ClassID", classid }, { "Check", "Check" } }).ToList();
                List<JudgeRecordHistory> listTempHistory = null;
                List<JudgeRecord> listTemp = null;
                int modSchoolID = global.SchoolID.Value % 100;
                if (isMovedHistory)
                {
                    var queryHis = from m in JudgeRecordHistoryBusiness.All
                                   where m.Last2digitNumberSchool == modSchoolID
                                   && m.AcademicYearID == global.AcademicYearID
                                   && m.ClassID == classid
                                   && m.SubjectID == subjectid
                                   && m.Semester == semesterid
                                   select m;
                    listTempHistory = queryHis.ToList();
                }
                else
                {
                    var query = from m in JudgeRecordBusiness.All
                                where m.Last2digitNumberSchool == modSchoolID
                                && m.AcademicYearID == global.AcademicYearID
                                && m.ClassID == classid
                                && m.SubjectID == subjectid
                                && m.Semester == semesterid
                                select m;
                    listTemp = query.ToList();
                }

                string[] pupilids = listjudgerecord.Split(new Char[] { ',' });
                string[] listPupilExempteds = listPupilExempted.Split(new Char[] { ',' });

                //-	Thao tác trên những học sinh được check chọn và những con điểm không bị null. 
                // Map các thông tin tương ứng trên vào JudgeRecord => List<JudgeRecord> lstJudgeRecord.
                // Chú ý: chỉ lấy thông tin của các trường trên grid có dữ liệu và được enable.
                List<int> listpupilid = new List<int>();
                List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
                List<SummedUpRecord> ListSummedUpRecord = new List<SummedUpRecord>();

                //1 ví dụ định dạng tên mỗi combobox chứa điểm là  8_M2_M
                //trong đó: 8 là ID của Pupil, M2 là Title của con điểm, M là Type của con điểm (có thể lấy dc từ Title, tuy nhiên nó để dùng cho xử lý ngoài view)
                //Thứ tự con điểm lấy từ Title của nó.
                string mark_format = "{0}_{1}_{2}";
                //1 ví dụ định dạng điểm trung bình là 8TBM
                //trong đó 8 là ID của Pupil
                //TBM là mã để nhận biết là combobox tính điểm TBM
                string tbm_format = "{0}TBM";
                JudgeRecord JudgeRecord = new JudgeRecord();
                SummedUpRecord SummedUpRecord = new SMAS.Models.Models.SummedUpRecord();
                foreach (var pupilid in pupilids)
                {
                    isInsertLog = false;
                    lstOldMark = new List<OldMark>();
                    if (pupilid.Trim() == "")
                        continue;
                    int pid = string.IsNullOrWhiteSpace(pupilid) ? 0 : int.Parse(pupilid);
                    // Tạo dữ liệu ghi log
                    PupilOfClass pop = lstPOC.Where(p => p.PupilID == Convert.ToInt32(pupilid)).FirstOrDefault();
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Cập nhật điểm HS " + pop.PupilProfile.FullName);
                    inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                    inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                    inforLog.Append("/" + classSubject.SubjectCat.SubjectName);
                    inforLog.Append("/Học kỳ " + semesterid);
                    inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                    newObjectStr = new StringBuilder();

                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == pid).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                OldMark objOldMark = new OldMark();
                                JudgeRecordHistory record = listOldRecord[j];
                                if (col["isChangeJudge_" + pid + record.Title] != null && "1".Equals(col["isChangeJudge_" + pid + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                    isInsertLog = true;
                                    objOldMark.JudgeRecordID = record.JudgeRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Judgement = record.Judgement;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(", ");
                                    }
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listTemp != null)
                        {
                            List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == pid).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                OldMark objOldMark = new OldMark();
                                JudgeRecord record = listOldRecord[j];
                                if (col["isChangeJudge_" + pid + record.Title] != null && "1".Equals(col["isChangeJudge_" + pid + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                    isInsertLog = true;
                                    objOldMark.JudgeRecordID = record.JudgeRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Judgement = record.Judgement;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(",");
                                    }
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    // end
                    descriptionStrtmp.Append("Update mark record pupil_id:" + pid);
                    paramsStrtmp.Append("pupil_id:" + pid);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(pid);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    OldMark objOM = null;

                    for (int i = 0; i < SemesterDeclaration.InterviewMark; i++)
                    {
                        var strname = string.Format(mark_format, pid.ToString(), string.Format("M{0}", i + 1), "M");

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        JudgeRecord = new JudgeRecord();
                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = pid;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = (int)subjectid;
                        JudgeRecord.MarkTypeID = markTypeM.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = acaYear.Year;
                        JudgeRecord.Semester = (int)semesterid;
                        JudgeRecord.Title = string.Format("M{0}", i + 1);
                        if (col["isChangeJudge_" + pid + JudgeRecord.Title] != null && "1".Equals(col["isChangeJudge_" + pid + JudgeRecord.Title]))
                        {
                            //Danh dau cac o da luu
                            lstChangeMark.Add(col["isChangeJudge_" + pid + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    for (var i = 0; i < SemesterDeclaration.WritingMark; i++)
                    {
                        var strname = string.Format(mark_format, pid.ToString(), string.Format("P{0}", i + 1), "P");

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        JudgeRecord = new JudgeRecord();
                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = pid;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = (int)subjectid;
                        JudgeRecord.MarkTypeID = markTypeP.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = acaYear.Year;
                        JudgeRecord.Semester = (int)semesterid;
                        JudgeRecord.Title = string.Format("P{0}", i + 1);
                        if (col["isChangeJudge_" + pid + JudgeRecord.Title] != null && "1".Equals(col["isChangeJudge_" + pid + JudgeRecord.Title]))
                        {
                            //Danh dau cac o da luu
                            lstChangeMark.Add(col["isChangeJudge_" + pid + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    for (var i = 0; i < SemesterDeclaration.TwiceCoeffiecientMark; i++)
                    {
                        var strname = string.Format(mark_format, pid.ToString(), string.Format("V{0}", i + 1), "V");

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        JudgeRecord = new JudgeRecord();
                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = pid;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = (int)subjectid;
                        JudgeRecord.MarkTypeID = markTypeV.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = acaYear.Year;
                        JudgeRecord.Semester = (int)semesterid;
                        JudgeRecord.Title = string.Format("V{0}", i + 1);
                        if (col["isChangeJudge_" + pid + JudgeRecord.Title] != null && "1".Equals(col["isChangeJudge_" + pid + JudgeRecord.Title]))
                        {
                            //Danh dau cac o da luu
                            lstChangeMark.Add(col["isChangeJudge_" + pid + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    var strhkname = string.Format(mark_format, pid.ToString(), "HK", "HK");
                    if (col[strhkname] != "" && col[strhkname] != null && col[strhkname].ToString().Contains("-1"))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    JudgeRecord = new JudgeRecord();
                    #region Assign Data for JudgeRecord item
                    JudgeRecord.AcademicYearID = AcademicYearID.Value;
                    JudgeRecord.PupilID = pid;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.MarkTypeID = markTypeHK.MarkTypeID;
                    JudgeRecord.CreatedAcademicYear = acaYear.Year;
                    JudgeRecord.Semester = (int)semesterid;
                    JudgeRecord.Title = "HK";
                    if (col["isChangeJudge_" + pid + JudgeRecord.Title] != null && "1".Equals(col["isChangeJudge_" + pid + JudgeRecord.Title]))
                    {
                        //Danh dau cac o da luu
                        lstChangeMark.Add(col["isChangeJudge_" + pid + JudgeRecord.Title]);
                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                        }
                        if (!string.IsNullOrWhiteSpace(col[strhkname]))
                        {
                            JudgeRecord.Judgement = col[strhkname].ToString();
                            userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                            newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(JudgeRecord.Title + ":");
                            JudgeRecord.Judgement = "-1";
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        JudgeRecord.Judgement = "-1";
                    }
                    JudgeRecord.MarkedDate = DateTime.Now;
                    JudgeRecord.CreatedDate = DateTime.Now;
                    JudgeRecord.ModifiedDate = DateTime.Now;
                    #endregion
                    ListJudgeRecord.Add(JudgeRecord);
                    var strname_tbm = string.Format(tbm_format, pid.ToString());
                    if (col[strname_tbm] != "" && col[strname_tbm] != null && col[strname_tbm].ToString().Contains("-1"))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    SummedUpRecord = new SummedUpRecord();
                    SummedUpRecord.AcademicYearID = AcademicYearID.Value;
                    SummedUpRecord.ClassID = classid;
                    SummedUpRecord.PupilID = pid;
                    SummedUpRecord.SchoolID = SchoolID.Value;
                    SummedUpRecord.Semester = (int)semesterid;
                    SummedUpRecord.SubjectID = (int)subjectid;
                    SummedUpRecord.SummedUpDate = DateTime.Now;
                    if (!string.IsNullOrWhiteSpace(col[strname_tbm]))
                    {
                        SummedUpRecord.JudgementResult = col[strname_tbm].ToString();
                    }
                    else
                    {
                        SummedUpRecord.JudgementResult = "-1";
                    }
                    SummedUpRecord.IsCommenting = classSubject.IsCommenting.Value;
                    SummedUpRecord.CreatedAcademicYear = acaYear.Year;
                    ListSummedUpRecord.Add(SummedUpRecord);

                    // Tạo dữ liệu ghi log
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }
                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }
                List<JudgeRecordHistory> lstJudgeRecordHistory = new List<JudgeRecordHistory>();
                List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                if (isMovedHistory)
                {
                    JudgeRecordHistory objJudgeRecordHistory = null;
                    JudgeRecord objJudgeRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < ListJudgeRecord.Count; i++)
                    {
                        objJudgeRecordHistory = new JudgeRecordHistory();
                        objJudgeRecord = ListJudgeRecord[i];
                        objJudgeRecordHistory.JudgeRecordID = objJudgeRecord.JudgeRecordID;
                        objJudgeRecordHistory.AcademicYearID = objJudgeRecord.AcademicYearID;
                        objJudgeRecordHistory.PupilID = objJudgeRecord.PupilID;
                        objJudgeRecordHistory.SchoolID = objJudgeRecord.SchoolID;
                        objJudgeRecordHistory.ClassID = objJudgeRecord.ClassID;
                        objJudgeRecordHistory.Judgement = objJudgeRecord.Judgement;
                        objJudgeRecordHistory.MarkTypeID = objJudgeRecord.MarkTypeID;
                        objJudgeRecordHistory.Semester = objJudgeRecord.Semester;
                        objJudgeRecordHistory.Title = objJudgeRecord.Title;
                        objJudgeRecordHistory.Last2digitNumberSchool = objJudgeRecord.Last2digitNumberSchool;
                        objJudgeRecordHistory.MarkedDate = objJudgeRecord.MarkedDate;
                        objJudgeRecordHistory.ModifiedDate = objJudgeRecord.ModifiedDate;
                        objJudgeRecordHistory.SubjectID = objJudgeRecord.SubjectID;
                        objJudgeRecordHistory.CreatedDate = objJudgeRecord.CreatedDate;
                        objJudgeRecordHistory.CreatedAcademicYear = objJudgeRecord.CreatedAcademicYear;
                        objJudgeRecordHistory.OrderNumber = objJudgeRecord.OrderNumber;
                        lstJudgeRecordHistory.Add(objJudgeRecordHistory);
                    }
                    for (int i = 0; i < ListSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = ListSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                }

                if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    List<int> lstPupilExempted = new List<int>();
                    PupilOfClass pupilOfClass = new SMAS.Models.Models.PupilOfClass();
                    int PupilProfileID = 0;
                    foreach (var pupilid in listPupilExempteds)
                    {
                        if (pupilid != "" && int.Parse(pupilid.Trim()) > 0)
                        {
                            PupilProfileID = int.Parse(pupilid.Trim());
                            pupilOfClass = lstPOC.Where(p => p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                && p.PupilID == PupilProfileID).FirstOrDefault();
                            if (pupilOfClass != null)
                            {
                                lstPupilExempted.Add(int.Parse(pupilid));
                            }
                        }
                    }
                    if (isMovedHistory)
                    {
                        isDBChange = JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(global.UserAccountID, lstJudgeRecordHistory, lstSummedUpRecordHistory, semesterid, null, lstPupilExempted, SchoolID.Value, global.AcademicYearID.Value, classid, subjectid, _globalInfo.EmployeeID);
                        //JudgeRecordHistoryBusiness.Save();
                    }
                    else
                    {
                        isDBChange = JudgeRecordBusiness.InsertJudgeRecord(global.UserAccountID, ListJudgeRecord, ListSummedUpRecord, semesterid, null, _globalInfo.EmployeeID, lstPupilExempted, SchoolID.Value, global.AcademicYearID.Value, classid, subjectid);
                        //JudgeRecordBusiness.Save();
                    }


                }
                else
                {
                    if (isMovedHistory)
                    {
                        isDBChange = JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(global.UserAccountID, lstJudgeRecordHistory, lstSummedUpRecordHistory, semesterid, null, null, SchoolID.Value, global.AcademicYearID.Value, classid, subjectid, _globalInfo.EmployeeID);
                        //JudgeRecordHistoryBusiness.Save();
                    }
                    else
                    {
                        isDBChange = JudgeRecordBusiness.InsertJudgeRecord(global.UserAccountID, ListJudgeRecord, ListSummedUpRecord, semesterid, null, _globalInfo.EmployeeID, null, SchoolID.Value, global.AcademicYearID.Value, classid, subjectid);
                        //JudgeRecordBusiness.Save();
                    }

                }

                ViewData[JudgeRecordSemesterConstants.LIST_CHANGE_MARK] = lstChangeMark;
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                //SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

                return Json(new
                {
                    Message = Res.Get("Common_Label_UpdateMarkSuccess"),
                    Type = "success",
                    isDBChange = isDBChange
                });
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "CreateJudgeSemesterRecord", "null", ex);
                return Json(new
                {
                    Message = "Lỗi trong quá trình thực hiện lưu điểm",
                    Type = "success",
                    isDBChange = false
                });
            }
            finally
            {
                //Giai phong log action
                VTUtils.LockAction.LockManager.ReleaseLock(keyNames, Session.SessionID);                
            }
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(FormCollection col)
        {
            var global = new GlobalInfo();
            var listKeySelect = col.AllKeys.Where(o => o.Contains("rptJudgeRecord_"));
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            StringBuilder listjudge = new StringBuilder();
            if (listKeySelect != null && listKeySelect.Count() > 0)
            {
                listKeySelect.ToList().ForEach(u => listjudge.Append("," + u.Replace("rptJudgeRecord_", "")));
            }

            var listjudgerecord = listjudge.ToString();
            string[] pupilids = listjudgerecord.Split(new Char[] { ',' }).Where(u => !string.IsNullOrEmpty(u)).ToArray();
            if (pupilids.Length <= 0)
            {
                return Json(new JsonMessage("Common_Validate_NoPupilSelected", JsonMessage.ERROR));
            }

            List<int> lstPupilIDs = pupilids.Select(u => int.Parse(u)).ToList();
            int classid = string.IsNullOrWhiteSpace(Request["txtclassid"]) ? 0 : int.Parse(Request["txtclassid"]);
            int subjectid = string.IsNullOrWhiteSpace(Request["txtsubjectid"]) ? 0 : int.Parse(Request["txtsubjectid"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["txtsemesterid"]) ? 0 : int.Parse(Request["txtsemesterid"]);

            // Tạo dữ liệu ghi log
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilIDs.Contains(o.PupilProfileID)).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(classid);
            SubjectCat subject = SubjectCatBusiness.Find(subjectid);
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            StringBuilder userDescriptionstmp = new StringBuilder();
            StringBuilder oldObjectstr = new StringBuilder();
            StringBuilder markStr = null;

            int modSchoolID = global.SchoolID.Value % 100;
            ClassSubject classSubject = ClassSubjectBusiness.All.FirstOrDefault(s => s.ClassID == classid && s.SubjectID == subjectid && s.Last2digitNumberSchool == modSchoolID);

            var query = from m in JudgeRecordBusiness.All
                        where lstPupilIDs.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID && m.AcademicYearID == global.AcademicYearID
                        && m.Semester == semesterid && m.SubjectID == subjectid
                        && m.ClassID == classid
                        orderby m.Title, m.MarkTypeID
                        select m;
            var queryHis = from m in JudgeRecordHistoryBusiness.All
                           where lstPupilIDs.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID && m.AcademicYearID == global.AcademicYearID
                           && m.Semester == semesterid && m.SubjectID == subjectid
                           && m.ClassID == classid
                           orderby m.Title, m.MarkTypeID
                           select m;
            List<JudgeRecord> allMarks = null;
            List<JudgeRecordHistory> allMarksHis = null;
            if (isMovedHistory)
            {
                allMarksHis = queryHis.ToList();
            }
            else
            {
                allMarks = query.ToList();
            }
            bool isCheckDelete = false;
            List<int> lstPupilDeleteID = new List<int>();

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = acaYear.AcademicYearID,
                SCHOOL_ID = acaYear.SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK,
                SHORT_DESCRIPTION = string.Format("Điểm HS lớp: {0}, học kỳ:{1}, môn: {2}, ", classProfile.DisplayName, semesterid, subject.DisplayName),
            };

            objRes.SHORT_DESCRIPTION += string.Format(" năm học: {0}, ", classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));


            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();


            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                string strHK = string.Empty;
                isCheckDelete = false;
                oldObjectstr = new StringBuilder();
                oldObjectstr.Append("Giá trị trước khi xóa: ");
                if (isMovedHistory)
                {
                    if (allMarksHis != null)
                    {
                        JudgeRecordHistory mark = null;
                        markStr = new StringBuilder();
                        List<JudgeRecordHistory> marks = allMarksHis.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + mark.Judgement + "; ";
                                }
                                else
                                {
                                    oldObjectstr.Append(mark.Title + ":" + mark.Judgement);
                                    oldObjectstr.Append("; ");
                                }
                            }
                            oldObjectstr.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                else
                {
                    if (allMarks != null)
                    {
                        JudgeRecord mark = null;
                        markStr = new StringBuilder();
                        List<JudgeRecord> marks = allMarks.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            RESTORE_DATA_DETAIL objRestoreDetail;
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + mark.Judgement + "; ";
                                }
                                else
                                {
                                    oldObjectstr.Append(mark.Title + ":" + mark.Judgement);
                                    oldObjectstr.Append("; ");
                                }

                                #region Luu thong tin phuc vu phuc hoi du lieu

                                //Luu thong tin khoi phuc diem
                                KeyDelMarkBO objKeyMark = new KeyDelMarkBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    MarkTypeID = mark.MarkTypeID,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID
                                };

                                BakJudgeRecordBO objDelMark = new BakJudgeRecordBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    CreatedAcademicYear = mark.CreatedAcademicYear,
                                    CreatedDate = mark.CreatedDate,
                                    IsOldData = mark.IsOldData,
                                    Last2digitNumberSchool = mark.Last2digitNumberSchool,
                                    LogChange = mark.LogChange,
                                    MarkedDate = mark.MarkedDate,
                                    MarkTypeID = mark.MarkTypeID,
                                    ModifiedDate = mark.ModifiedDate,
                                    MSourcedb = mark.MSourcedb,
                                    M_OldID = mark.M_OldID,
                                    OrderNumber = mark.OrderNumber,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID,
                                    SynchronizeID = mark.SynchronizeID,
                                    Title = mark.Title,
                                    Year = mark.Year,
                                    IsSMS = false,
                                    Judgement = mark.Judgement,
                                    JudgeRecordID = mark.JudgeRecordID,
                                    MJudgement = mark.MJudgement,
                                    M_ProvinceID = mark.M_ProvinceID,
                                    OldJudgement = mark.OldJudgement,
                                    ReTestJudgement = mark.ReTestJudgement
                                };
                                objRestoreDetail = new RESTORE_DATA_DETAIL
                                {
                                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                                    CREATED_DATE = DateTime.Now,
                                    END_DATE = null,
                                    IS_VALIDATE = 1,
                                    LAST_2DIGIT_NUMBER_SCHOOL = mark.Last2digitNumberSchool,
                                    ORDER_ID = 1,
                                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                                    SCHOOL_ID = objRes.SCHOOL_ID,
                                    SQL_DELETE = JsonConvert.SerializeObject(objKeyMark),
                                    SQL_UNDO = JsonConvert.SerializeObject(objDelMark),
                                    TABLE_NAME = RestoreDataConstant.TABLE_JUDGE_RECORD
                                };

                                lstRestoreDetail.Add(objRestoreDetail);
                                #endregion
                            }
                            oldObjectstr.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                if (isCheckDelete)
                {
                    userDescriptionstmp = new StringBuilder();
                    string tmp = string.Empty;
                    tmp = oldObjectstr.ToString().Substring(0, oldObjectstr.Length - 2);
                    objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                    descriptionObject.Append("Delete mark_record:" + listPupilProfile[i].PupilProfileID.ToString());
                    paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                    userFunctions.Append("Sổ điểm");
                    userActions.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptionstmp.Append("Xóa điểm HS " + listPupilProfile[i].FullName
                        + ", mã " + listPupilProfile[i].PupilCode
                        + ", Lớp " + classProfile.DisplayName
                        + "/" + subject.SubjectName
                        + "/Học kỳ " + semesterid
                        + "/" + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)
                        + ". " + tmp);
                    if (i < size - 1)
                    {
                        oldObject.Append(tmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        objectID.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFunctions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionstmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                    userDescriptions.Append(userDescriptionstmp);

                    objRes.SHORT_DESCRIPTION += string.Format(" HS: {0}, mã: {1}", listPupilProfile[i].FullName, listPupilProfile[i].PupilCode);

                }
            }
            // end
            if (isMovedHistory)
            {
                JudgeRecordHistoryBusiness.DeleteJudgeRecordHistory(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, null, subjectid, lstPupilDeleteID);
                SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, null, subjectid, lstPupilDeleteID);
                //MarkRecordHistoryBusiness.Save();
            }
            else
            {
                JudgeRecordBusiness.DeleteJudgeRecord(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, null, subjectid, lstPupilDeleteID);
                List<SummedUpRecord> lstSummed = SummedUpRecordBusiness.DeleteSummedUpRecord(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, null, subjectid, lstPupilDeleteID);

                if (lstSummed != null)
                {
                    List<RESTORE_DATA_DETAIL> lstResSummed = SummedUpRecordBusiness.BackUpSummedMark(lstSummed, objRes);
                    if (lstResSummed != null)
                    {
                        lstRestoreDetail.AddRange(lstResSummed);
                        if (lstRestoreDetail.Count > 0)
                        {
                            if (objRes.SHORT_DESCRIPTION.Length > 1000)
                            {
                                objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(0, 1000) + "...";
                            }

                            RestoreDataBusiness.Insert(objRes);
                            RestoreDataBusiness.Save();
                            RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                        }
                    }
                }
            }

            if (lstPupilDeleteID.Count > 0)
            {
                // Tạo dữ liệu ghi log   
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectID.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFunctions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                //Tinh lai TBM cho dot
                JudgeRecordBusiness.SummedJudgeMark(lstPupilDeleteID, acaYear, semesterid, null, classSubject);
            }

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public bool CheckEnableButton(int classid, int subjectid, int periodid)
        {
            var flag = false;
            //-	Kiểm tra quyền của người dùng đăng nhập: btnImport, btnExport, btnSave, btnDelete, chkEntryMark
            //    + UserInfo.HasSubjectTeacherPermission(ClassID, SubjectID) = true => enable
            //    + PeriodDeclaration(PeriodID).EndDate > DateTime.Now => Disable
            if (UtilsBusiness.HasSubjectTeacherPermission(new GlobalInfo().UserAccountID, classid, subjectid))
                flag = true;
            else flag = false;
            if (PeriodDeclarationBusiness.All.Where(o => o.PeriodDeclarationID == periodid).FirstOrDefault().EndDate > DateTime.Now)
                flag = false;
            else flag = true;
            return flag;
        }

        [HttpPost]

        public FileResult ExportFile(FormCollection form)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            int AcademicYearID = globalInfo.AcademicYearID.Value;
            int SchoolID = globalInfo.SchoolID.Value;
            int ClassID = SMAS.Business.Common.Utils.GetInt(form["ClassID"]);
            int SubjectID = SMAS.Business.Common.Utils.GetShort(form["SubjectID"]);
            int SemesterID = SMAS.Business.Common.Utils.GetByte(form["Semester"]);
            int PeriodID = SMAS.Business.Common.Utils.GetInt(form["PeriodID"]);

            List<JudgeRecordBO> lstJudgeRecordBO = (List<JudgeRecordBO>)Session["List_JudgeRecordBO"];
            Dictionary<int, bool> dicDisplay = new Dictionary<int, bool>();
            lstJudgeRecordBO.Select(u => new { PupilID = u.PupilID.Value, u.DisplayMark }).Distinct().ToList().ForEach(u => dicDisplay[u.PupilID] = u.DisplayMark);

            String filename = "";

            // Tạo dữ liệu ghi log
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);

            Stream excel = this.JudgeRecordBusiness.ExportReportForSemester(SchoolID, AcademicYearID, SemesterID, globalInfo.AppliedLevel.Value, classProfile.EducationLevelID, ClassID, PeriodID, SubjectID, out filename, _globalInfo.IsAdminSchoolRole, dicDisplay);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            // Tạo dữ liệu ghi log
            SetViewDataActionAudit(String.Empty, String.Empty,
                classProfile.ClassProfileID.ToString(),
                "Export excel mark record class_profile_id:" + classProfile.ClassProfileID, "ClassID: " + ClassID + ", SubjectID: " + SubjectID + ", SemesterID: " + SemesterID, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_EXPORT,
                "Xuất excel sổ điểm lớp " + classProfile.DisplayName + " HK " + SemesterID + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1) + " môn " + subject.SubjectName);


            return result;
        }

        #region ImportExcel

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int? semesterid, int? educationlevelid, int? periodid, int? classid, int? subjectid)
        {
            GlobalInfo global = new GlobalInfo();
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            var file = attachments.FirstOrDefault();
            //if (file == null || !semesterid.HasValue || semesterid.Value <= 0
            //            || !classid.HasValue || classid.Value <= 0
            //            || !subjectid.HasValue || subjectid.Value <= 0)
            //    return Json(new JsonMessage("Common_Validate_DataInvalid", JsonMessage.ERROR));
            if (file != null)
            {
                var extension = Path.GetExtension(file.FileName);

                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 1024)
                {
                    return Json(new JsonMessage(Res.Get("JudgeRecord_Validate_FileMaxSize"), "error"));
                }
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);

                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classid.Value, new Dictionary<string, object> { { "SubjectID", subjectid.Value } }).SingleOrDefault();

                ViewData[JudgeRecordSemesterConstants.SEMESTER] = semesterid;
                var strTitle = Res.Get("JudgeRecordSemester_Label_SearchTitle");
                ViewData[JudgeRecordSemesterConstants.SEARCHTITLE] = string.Format(strTitle, classSubject.SubjectCat.DisplayName.ToUpper(),
                                                                                            classSubject.ClassProfile.DisplayName.ToUpper(),
                                                                                            semesterid.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II");

                IDictionary<string, object> SearchInfoSemester = new Dictionary<string, object>();
                SearchInfoSemester["AcademicYearID"] = global.AcademicYearID;
                SearchInfoSemester["SchoolID"] = global.SchoolID;
                SearchInfoSemester["Year"] = acaYear.Year;
                SearchInfoSemester["Semester"] = semesterid;
                var semesterDecalaration = SemeterDeclarationBusiness.Search(SearchInfoSemester).Where(o => o.AppliedDate <= DateTime.Now).OrderByDescending(o => o.AppliedDate).FirstOrDefault();

                List<SummedUpRecord> listSurIII = new List<SummedUpRecord>();
                if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    IDictionary<string, object> search = new Dictionary<string, object>();
                    search = new Dictionary<string, object>();
                    search["AcademicYearID"] = global.AcademicYearID;
                    search["SchoolID"] = global.SchoolID;
                    search["ClassID"] = classid;
                    search["SubjectID"] = subjectid;
                    search["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                    listSurIII = SummedUpRecordBusiness.SearchBySchool(global.SchoolID.Value, search).Where(o => o.PeriodID == null).ToList();
                }
                var dic = OnImport(physicalPath, semesterid.Value, educationlevelid.Value, classid.Value, subjectid.Value);
                if (dic.FirstOrDefault().Value == false)
                {
                    ViewData[JudgeRecordSemesterConstants.ERROR_BASIC_DATA] = true;
                    ViewData[JudgeRecordSemesterConstants.LIST_SEMESTERDECLARATION] = semesterDecalaration;
                    ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDOFCLASS] = new List<JudgeRecordBO>();
                    ViewData[JudgeRecordSemesterConstants.ERROR_IMPORT_MESSAGE] = dic.FirstOrDefault().Key;
                    return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                }
                else
                {
                    var model = getDataFromImportFile(physicalPath, semesterid.Value, educationlevelid.Value, classid.Value, subjectid.Value);
                    List<JudgeRecordSemesterViewModel> lsTemp = model.ListJudgeRecordSemesterViewModel.ToList();
                    Session["ImportData_ListJudgeRecord"] = lsTemp;
                    Session["ImportData_ListSummedUpRecord"] = model.ListSummedUpRecord;
                    Session["semesterid"] = semesterid;

                    ViewData[JudgeRecordSemesterConstants.ERROR_BASIC_DATA] = false;
                    //ViewData[JudgeRecordSemesterConstants.LIST_IMPORTDATA] = ListJudgeRecord;
                    ViewData[JudgeRecordSemesterConstants.HAS_ERROR_DATA] = lsTemp.Where(o => o.ErrorDescription != null).Count() > 0;
                    List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
                    List<JudgeRecord> ListJudgeRecordDelete = new List<JudgeRecord>();
                    //Hungnd8 16/04/2013 Fix 0167471
                    bool checkValidate = lsTemp.Where(o => o.IsLegalBot.HasValue && o.IsLegalBot.Value == false).Count() > 0;
                    if (checkValidate)
                    {
                        ViewData[JudgeRecordSemesterConstants.ERROR_IMPORT_MESSAGE] = "Có lỗi trong file excel";
                        List<JudgeRecordBO> List_JudgeRecordBO = new List<JudgeRecordBO>();
                        JudgeRecord JudgeRecord = new JudgeRecord();
                        SummedUpRecord SummedUpRecord = new SummedUpRecord();

                        // Lay danh sach pupilid
                        var lstPupilModel = lsTemp.Select(o => new { o.PupilID, o.flagPupil }).Distinct().ToList();
                        foreach (var ObjPupil in lstPupilModel)
                        {
                            List<JudgeRecordSemesterViewModel> lstjudgeRecord = lsTemp.Where(o => o.PupilID == ObjPupil.PupilID && o.flagPupil == ObjPupil.flagPupil).ToList();
                            JudgeRecord = new JudgeRecord();
                            JudgeRecordBO JudgeRecordBO = new Business.BusinessObject.JudgeRecordBO();
                            JudgeRecordBO.PupilID = lstjudgeRecord[0].PupilID;
                            JudgeRecordBO.PupilCode = lstjudgeRecord[0].PupilCode;
                            JudgeRecordBO.FullName = lstjudgeRecord[0].PupilName;
                            JudgeRecordBO.AcademicYearID = lstjudgeRecord[0].AcademicYearID;
                            JudgeRecordBO.ClassID = lstjudgeRecord[0].ClassID;
                            JudgeRecordBO.SchoolID = lstjudgeRecord[0].SchoolID;
                            JudgeRecordBO.SubjectID = lstjudgeRecord[0].SubjectID;
                            JudgeRecordBO.MarkTypeID = lstjudgeRecord[0].MarkTypeID;
                            JudgeRecordBO.Semester = lstjudgeRecord[0].Semester;
                            JudgeRecordBO.Judgement = lstjudgeRecord[0].Judgement;
                            JudgeRecordBO.OrderNumber = lstjudgeRecord[0].OrderNumber;
                            JudgeRecordBO.Title = lstjudgeRecord[0].Title;
                            JudgeRecordBO.IsLegalBot = lstjudgeRecord.Where(o => o.IsLegalBot == false).Count() > 0 ? false : true;

                            JudgeRecordBO.InterviewMark = new string[Convert.ToByte(semesterDecalaration.InterviewMark)];
                            JudgeRecordBO.WritingMark = new string[Convert.ToByte(semesterDecalaration.WritingMark)];
                            JudgeRecordBO.TwiceCoeffiecientMark = new string[Convert.ToByte(semesterDecalaration.TwiceCoeffiecientMark)];
                            JudgeRecordBO.Status = lstjudgeRecord[0].Status;
                            foreach (JudgeRecordSemesterViewModel mr in lstjudgeRecord)
                            {
                                if (mr.Title != null)
                                {
                                    for (int m = 1; m <= semesterDecalaration.InterviewMark; m++)
                                    {
                                        if (mr.Title == ("M" + m.ToString()))
                                        {
                                            JudgeRecordBO.InterviewMark[m - 1] = mr.Judgement;
                                        }
                                    }
                                    for (int p = 0; p < semesterDecalaration.WritingMark; p++)
                                    {
                                        if (mr.Title == ("P" + p.ToString()))
                                        {
                                            JudgeRecordBO.WritingMark[p - 1] = mr.Judgement;
                                        }
                                    }
                                    for (int v = 0; v < semesterDecalaration.WritingMark; v++)
                                    {
                                        if (mr.Title == ("V" + v.ToString()))
                                        {
                                            JudgeRecordBO.TwiceCoeffiecientMark[v - 1] = mr.Judgement;
                                        }

                                    }
                                    if (mr.Title == "HK")
                                    {
                                        JudgeRecordBO.SemesterMark = mr.Judgement;
                                    }
                                    if ((JudgeRecordBO.ErrorDescription == null) || (JudgeRecordBO.ErrorDescription != null && mr.ErrorDescription != null && !JudgeRecordBO.ErrorDescription.Contains(mr.ErrorDescription)))
                                    {
                                        JudgeRecordBO.ErrorDescription += mr.ErrorDescription;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            List_JudgeRecordBO.Add(JudgeRecordBO);
                        }
                        ViewData[JudgeRecordSemesterConstants.LIST_SUMMEDUPRECORDOFCLASS] = model.ListSummedUpRecord.Count > 0 ? model.ListSummedUpRecord : new List<SummedUpRecordBO>();
                        ViewData[JudgeRecordSemesterConstants.LIST_SEMESTERDECLARATION] = semesterDecalaration;
                        ViewData[JudgeRecordSemesterConstants.LIST_JUDGERECORDOFCLASS] = List_JudgeRecordBO;
                        return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                    }
                    else
                    {
                        // Thong tin mien giam cua lop trong ky
                        List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(classid.Value, semesterid.Value)
                            .Where(o => o.SubjectID == subjectid)
                            .ToList();

                        // Tạo data ghi log
                        StringBuilder objectIDStr = new StringBuilder();
                        StringBuilder descriptionStr = new StringBuilder();
                        StringBuilder paramsStr = new StringBuilder();
                        StringBuilder oldObjectStr = new StringBuilder();
                        StringBuilder newObjectStr = new StringBuilder();
                        StringBuilder userFuntionsStr = new StringBuilder();
                        StringBuilder userActionsStr = new StringBuilder();
                        StringBuilder userDescriptionsStr = new StringBuilder();
                        StringBuilder inforLog = null;
                        int iLog = 0;
                        List<int> pupilIds = lsTemp.Select(s => s.PupilID).Distinct().ToList();
                        List<PupilOfClass> lstPOC = (from p in PupilOfClassBusiness.All
                                                     where pupilIds.Contains(p.PupilID)
                                                     //&& p.Last2digitNumberSchool == (global.SchoolID % 100)
                                                     && p.ClassID == classid
                                                     && p.AcademicYearID == acaYear.AcademicYearID
                                                     && p.Year == acaYear.Year
                                                     select p).ToList();
                        List<JudgeRecordHistory> listTempHistory = null;
                        List<JudgeRecord> listTemp = null;
                        int modSchoolID = global.SchoolID.Value % 100;
                        if (isMovedHistory)
                        {
                            var queryHis = from m in JudgeRecordHistoryBusiness.All
                                           where m.Last2digitNumberSchool == modSchoolID
                                           && m.AcademicYearID == global.AcademicYearID
                                           && m.ClassID == classid
                                           && m.SubjectID == subjectid
                                           && m.Semester == semesterid
                                           select m;
                            listTempHistory = queryHis.ToList();
                        }
                        else
                        {
                            var query = from m in JudgeRecordBusiness.All
                                        where m.Last2digitNumberSchool == modSchoolID
                                        && m.AcademicYearID == global.AcademicYearID
                                        && m.ClassID == classid
                                        && m.SubjectID == subjectid
                                        && m.Semester == semesterid
                                        select m;
                            listTemp = query.ToList();
                        }
                        for (int i = 0, size1 = pupilIds.Count; i < size1; i++)
                        {
                            // Tạo dữ liệu ghi log
                            int id = pupilIds[i];
                            if (listExemptedSubject.Any(u => u.PupilID == id))
                                continue;
                            PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                            objectIDStr.Append(id);
                            descriptionStr.Append("Import mark record pupil_id:" + id);
                            paramsStr.Append("pupil_id:" + id);
                            userFuntionsStr.Append("Sổ điểm");
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                            inforLog = new StringBuilder();
                            inforLog.Append("Import điểm cho " + pop.PupilProfile.FullName);
                            inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                            inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                            inforLog.Append(" môn " + classSubject.SubjectCat.SubjectName);
                            inforLog.Append(" học kì " + semesterid + " ");
                            inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                            userDescriptionsStr.Append(inforLog.ToString());
                            if (isMovedHistory)
                            {
                                if (listTempHistory != null)
                                {
                                    List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id).ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                                    oldObjectStr.Append("{");
                                    oldObjectStr.Append("pupil_profile_id:" + id + ",");
                                    for (int j = 0; j < listOldRecord.Count; j++)
                                    {
                                        JudgeRecordHistory record = listOldRecord[j];
                                        oldObjectStr.Append("{JudgeRecordID:" + record.JudgeRecordID + "," + record.Title + ":" + record.Judgement + "}");
                                        if (j < listOldRecord.Count - 1)
                                        {
                                            oldObjectStr.Append(",");
                                        }
                                    }
                                    oldObjectStr.Append("}");
                                }
                            }
                            else
                            {
                                if (listTemp != null)
                                {
                                    List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == id).ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                                    oldObjectStr.Append("{");
                                    oldObjectStr.Append("pupil_profile_id:" + id + ",");
                                    for (int j = 0; j < listOldRecord.Count; j++)
                                    {
                                        JudgeRecord record = listOldRecord[j];
                                        oldObjectStr.Append("{JudgeRecordID:" + record.JudgeRecordID + "," + record.Title + ":" + record.Judgement + "}");
                                        if (j < listOldRecord.Count - 1)
                                        {
                                            oldObjectStr.Append(",");
                                        }
                                    }
                                    oldObjectStr.Append("}");
                                }
                            }
                            // end

                            List<JudgeRecordSemesterViewModel> temp = lsTemp.Where(t => t.PupilID == id).ToList();
                            for (int j = 0, size2 = temp.Count; j < size2; j++)
                            {
                                if (!String.IsNullOrEmpty(temp[j].Judgement) && !"-1".Equals(temp[j].Judgement))
                                {
                                    userDescriptionsStr.Append(temp[j].Title + ":" + temp[j].Judgement + " ");
                                }
                            }

                            // Tạo dữ liệu ghi log
                            if (iLog < pupilIds.Count - 1)
                            {
                                objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            }
                            iLog++;
                        }
                        // end

                        foreach (JudgeRecordSemesterViewModel item in lsTemp)
                        {
                            if (listExemptedSubject.Any(u => u.PupilID == item.PupilID))
                                continue;
                            if (!string.IsNullOrWhiteSpace(item.Judgement) && item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                JudgeRecord judgeRecord = new SMAS.Models.Models.JudgeRecord();
                                Utils.Utils.BindTo(item, judgeRecord);
                                ListJudgeRecord.Add(judgeRecord);
                            }
                            else if (item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                JudgeRecord judgeRecord = new SMAS.Models.Models.JudgeRecord();
                                Utils.Utils.BindTo(item, judgeRecord);
                                judgeRecord.Judgement = "-1";
                                ListJudgeRecord.Add(judgeRecord);
                            }


                        }
                        List<SummedUpRecord> lstSummedUp = new List<SummedUpRecord>();
                        model.ListSummedUpRecord.ForEach(o =>
                        {
                            if (o.Status.HasValue && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                && !listExemptedSubject.Any(e => e.PupilID == o.PupilID))
                            {
                                SummedUpRecord obj = new SummedUpRecord();
                                Utils.Utils.BindTo(o, obj);
                                obj.CreatedAcademicYear = o.Year.Value;
                                lstSummedUp.Add(obj);
                            }
                        });
                        if (isMovedHistory)
                        {
                            List<JudgeRecordHistory> lstJudgeRecordHistory = new List<JudgeRecordHistory>();
                            List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                            JudgeRecordHistory objJudgeRecordHistory = null;
                            JudgeRecord objJudgeRecord = null;
                            SummedUpRecord objSummedUpRecord = null;
                            SummedUpRecordHistory objSummedUpRecordHistory = null;
                            for (int i = 0; i < ListJudgeRecord.Count; i++)
                            {
                                objJudgeRecordHistory = new JudgeRecordHistory();
                                objJudgeRecord = ListJudgeRecord[i];
                                objJudgeRecordHistory.AcademicYearID = objJudgeRecord.AcademicYearID;
                                objJudgeRecordHistory.PupilID = objJudgeRecord.PupilID;
                                objJudgeRecordHistory.SchoolID = objJudgeRecord.SchoolID;
                                objJudgeRecordHistory.ClassID = objJudgeRecord.ClassID;
                                objJudgeRecordHistory.Judgement = objJudgeRecord.Judgement;
                                //objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                                objJudgeRecordHistory.MarkTypeID = objJudgeRecord.MarkTypeID;
                                objJudgeRecordHistory.Semester = objJudgeRecord.Semester;
                                objJudgeRecordHistory.Title = objJudgeRecord.Title;
                                objJudgeRecordHistory.Last2digitNumberSchool = objJudgeRecord.Last2digitNumberSchool;
                                objJudgeRecordHistory.MarkedDate = objJudgeRecord.MarkedDate;
                                objJudgeRecordHistory.ModifiedDate = objJudgeRecord.ModifiedDate;
                                objJudgeRecordHistory.SubjectID = objJudgeRecord.SubjectID;
                                objJudgeRecordHistory.CreatedDate = objJudgeRecord.CreatedDate;
                                objJudgeRecordHistory.CreatedAcademicYear = objJudgeRecord.CreatedAcademicYear;
                                objJudgeRecordHistory.OrderNumber = objJudgeRecord.OrderNumber;
                                //objJudgeRecordHistory.ClassProfile = objJudgeRecord.ClassProfile;
                                //objJudgeRecordHistory.AcademicYear = objJudgeRecord.AcademicYear;
                                //objJudgeRecordHistory.MarkType = objJudgeRecord.MarkType;
                                //objJudgeRecordHistory.PupilProfile = objJudgeRecord.PupilProfile;
                                //objJudgeRecordHistory.SchoolProfile = objJudgeRecord.SchoolProfile;
                                //objJudgeRecordHistory.SubjectCat = objJudgeRecord.SubjectCat;
                                lstJudgeRecordHistory.Add(objJudgeRecordHistory);
                            }
                            for (int i = 0; i < lstSummedUp.Count; i++)
                            {
                                objSummedUpRecordHistory = new SummedUpRecordHistory();
                                objSummedUpRecord = lstSummedUp[i];
                                objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                                objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                                objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                                objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                                objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                                objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                                objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                                objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                                objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                                //objSummedUpRecordHistory.PeriodDeclaration = objSummedUpRecord.PeriodDeclaration;
                                objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                                objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                                objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                                objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                                objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                                objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                                objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                                objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                                //objSummedUpRecordHistory.ClassProfile = objSummedUpRecord.ClassProfile;
                                //objSummedUpRecordHistory.PeriodDeclaration = objSummedUpRecord.PeriodDeclaration;
                                //objSummedUpRecordHistory.PupilProfile = objSummedUpRecord.PupilProfile;
                                //objSummedUpRecordHistory.SchoolProfile = objSummedUpRecord.SchoolProfile;
                                //objSummedUpRecordHistory.SubjectCat = objSummedUpRecord.SubjectCat;
                                lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                            }
                            JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(global.UserAccountID, lstJudgeRecordHistory.Where(o => string.IsNullOrWhiteSpace(o.Judgement) == false).ToList(), lstSummedUpRecordHistory, (int)semesterid.Value, null, null, global.SchoolID.Value, global.AcademicYearID.Value, classid.Value, subjectid.Value, _globalInfo.EmployeeID);
                            
                        }
                        else
                        {
                            JudgeRecordBusiness.InsertJudgeRecord(global.UserAccountID, ListJudgeRecord.Where(o => string.IsNullOrWhiteSpace(o.Judgement) == false).ToList(), lstSummedUp, (int)semesterid.Value, null, _globalInfo.EmployeeID, null, global.SchoolID.Value, global.AcademicYearID.Value, classid.Value, subjectid.Value);
                            
                        }

                        // Tạo dữ liệu ghi log
                        SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

                        return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
                    }
                }
            }
            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private Dictionary<string, bool> OnImport(string filePath, int semesterid, int educationlevelid, int classid, int subjectid)
        {
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
            GlobalInfo global = new GlobalInfo();
            string subject = SubjectCatBusiness.Find(subjectid).DisplayName;
            //string period = PeriodDeclarationBusiness.Find(periodid).Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(semesterid);
            string className = ClassProfileBusiness.Find(classid).DisplayName;

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            string School = (string)sheet.GetCellValue(2, VTVector.dic['A']);
            string subjectAndSemesterAndClass = (string)sheet.GetCellValue(4, VTVector.dic['A']);
            string YearTitle = (string)sheet.GetCellValue(5, VTVector.dic['A']);



            string faultDescription = "";
            StringBuilder stringBuilder = new StringBuilder();
            bool isLegalTop = true;
            var dic = new Dictionary<string, bool>();

            if (subject == null || semester == null || className == null || subjectAndSemesterAndClass == null || YearTitle == null)
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel Không đúng định dạng");
                //return 
                dic = new Dictionary<string, bool>();
                faultDescription = stringBuilder.ToString();
                dic.Add(faultDescription, isLegalTop);
                return dic;
            }
            subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
            YearTitle = YearTitle.ToUpper();
            stringBuilder.Append(faultDescription);
            if (!subjectAndSemesterAndClass.Contains(subject.ToUpper()))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel Không phải là file điểm của Môn " + subject + "</br>");
            }
            if (!subjectAndSemesterAndClass.Contains(semester.ToUpper() + " "))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel Không phải là file điểm của kỳ " + semester + "</br>");
            }
            if (!subjectAndSemesterAndClass.Contains(className.ToUpper()))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel không phải là file điểm của lớp " + className + "</br>");
            }
            faultDescription = stringBuilder.ToString();
            dic = new Dictionary<string, bool>();
            dic.Add(faultDescription, isLegalTop);
            return dic;
        }

        private JudgeRecodeSemesterImportViewModel getDataFromImportFile(string filePath, int semesterid, int educationlevelid, int classid, int subjectid)
        {
            JudgeRecodeSemesterImportViewModel model = new JudgeRecodeSemesterImportViewModel();
            List<JudgeRecordSemesterViewModel> ListJudgeRecordSemester = new List<JudgeRecordSemesterViewModel>();
            List<SummedUpRecordBO> ListSummedUpRecord = new List<SummedUpRecordBO>();
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);

            GlobalInfo global = new GlobalInfo();

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classid, new Dictionary<string, object> { { "SubjectID", subjectid } }).SingleOrDefault();
            AcademicYear acaYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);

            string subject = classSubject.SubjectCat.DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(semesterid);
            string className = classSubject.ClassProfile.DisplayName;

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            string School = (string)sheet.GetCellValue(2, VTVector.dic['A']);
            string subjectAndSemesterAndClass = (string)sheet.GetCellValue(4, VTVector.dic['A']);
            string YearTitle = (string)sheet.GetCellValue(5, VTVector.dic['A']);
            subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
            YearTitle = YearTitle.ToUpper();

            //var lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(global.SchoolID.Value, global.AcademicYearID.Value, classid, (int)semesterid, (int)subjectid);
            List<List<string>> lsTemp = new List<List<string>>();
            int i = 0;

            //Load tiêu đề hiển thị con điểm
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            var lstMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();
            MarkType markTypeM = lstMarkType.Where(u => u.Title == "M").SingleOrDefault();
            MarkType markTypeP = lstMarkType.Where(u => u.Title == "P").SingleOrDefault();
            MarkType markTypeV = lstMarkType.Where(u => u.Title == "V").SingleOrDefault();
            MarkType markTypeHK = lstMarkType.Where(u => u.Title == "HK").SingleOrDefault();

            //Load thông tin số các con điểm M, P, V 
            IDictionary<string, object> SearchInfoSemester = new Dictionary<string, object>();
            SearchInfoSemester["AcademicYearID"] = global.AcademicYearID;
            SearchInfoSemester["SchoolID"] = global.SchoolID;
            SearchInfoSemester["Year"] = acaYear.Year;
            SearchInfoSemester["Semester"] = semesterid;
            var SemesterDeclaration = SemeterDeclarationBusiness.Search(SearchInfoSemester)
                .Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();

            var lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> {
                            { "AcademicYearID", global.AcademicYearID.Value }, { "ClassID", classid }, { "Check", "Check" }
                            })
                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                          select new { poc.PupilID, pp.FullName, pp.PupilCode, poc.Status }
                          )
                .ToList();
            // Kiem tra trung du lieu hay khong?
            List<string> lstPupilCode = new List<string>();
            int flagPupil = 1;
            bool checkDuplicate = false;
            while (sheet.GetCellValue(i + 8, VTVector.dic['A']) != null || sheet.GetCellValue(i + 8, VTVector.dic['C']) != null || sheet.GetCellValue(i + 8, VTVector.dic['D']) != null)
            {
                int colum_start = VTVector.dic['E'];
                bool isLegalBot = true;
                string PupilCode = (string)sheet.GetCellValue(i + 8, 3);
                string FullName = (string)sheet.GetCellValue(i + 8, 4);

                if (lstPupilCode.Contains(PupilCode))
                {
                    checkDuplicate = true;
                }
                else
                {
                    lstPupilCode.Add(PupilCode);
                    checkDuplicate = false;
                }
                string ErrorDescription = string.Empty;
                #region Kiem tra du lieu hoc sinh
                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    isLegalBot = false;
                    ErrorDescription = "- Trùng mã học sinh.";
                }
                var poc = lstPOC.Where(u => u.PupilCode.Equals(PupilCode, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (poc == null)
                {
                    isLegalBot = false;
                    ErrorDescription = "- Mã học sinh không hợp lệ </br>";
                }
                else if (!poc.FullName.Equals(FullName, StringComparison.InvariantCultureIgnoreCase))
                {
                    isLegalBot = false;
                    ErrorDescription = "- Họ và tên học sinh không hợp lệ </br>";
                }
                #endregion

                #region Assign Data for JudgeRecord item - M
                for (int j = 0; j < SemesterDeclaration.InterviewMark; j++)
                {
                    var celvalue = (string)sheet.GetCellValue(i + 8, colum_start);
                    colum_start++;
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    JudgeRecordSemesterViewModel JudgeRecord = new JudgeRecordSemesterViewModel();
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = acaYear.Year;
                    JudgeRecord.Semester = (int)semesterid;

                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        JudgeRecord.MarkTypeID = markTypeM.MarkTypeID;
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(j + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("M{0}", j + 1);
                        // Chỉ cần check khoá con điểm trong business
                        //if (lstLockedMarkDetail.Contains(JudgeRecord.Title))
                        //{
                        //    isLegalBot = false;
                        //    JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        //}
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(j + 1);
                        JudgeRecord.Title = string.Format("M{0}", j + 1);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    ListJudgeRecordSemester.Add(JudgeRecord);
                }
                #endregion

                #region Diem P
                for (var j = 0; j < SemesterDeclaration.WritingMark; j++)
                {
                    var celvalue = (string)sheet.GetCellValue(i + 8, colum_start);
                    colum_start++;
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    JudgeRecordSemesterViewModel JudgeRecord = new JudgeRecordSemesterViewModel();
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = acaYear.Year;
                    JudgeRecord.Semester = (int)semesterid;
                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        JudgeRecord.MarkTypeID = markTypeP.MarkTypeID;
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(j + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("P{0}", j + 1);
                        //if (lstLockedMarkDetail.Contains(JudgeRecord.Title))
                        //{
                        //    isLegalBot = false;
                        //    JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        //}
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(1 + j);
                        JudgeRecord.Title = string.Format("P{0}", 1 + j);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    ListJudgeRecordSemester.Add(JudgeRecord);
                }
                #endregion

                #region Diem mon V
                for (var j = 0; j < SemesterDeclaration.TwiceCoeffiecientMark; j++)
                {
                    var celvalue = (string)sheet.GetCellValue(i + 8, colum_start);
                    colum_start++;
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    JudgeRecordSemesterViewModel JudgeRecord = new JudgeRecordSemesterViewModel();
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = acaYear.Year;
                    JudgeRecord.Semester = (int)semesterid;
                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.MarkTypeID = markTypeV.MarkTypeID;
                        JudgeRecord.OrderNumber = Convert.ToByte(j + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("V{0}", j + 1);
                        //if (lstLockedMarkDetail.Contains(JudgeRecord.Title))
                        //{
                        //    isLegalBot = false;
                        //    JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        //}
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(1 + j);
                        JudgeRecord.Title = string.Format("V{0}", 1 + j);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    ListJudgeRecordSemester.Add(JudgeRecord);
                }
                #endregion

                #region lấy điểm kiểm tra học kì
                var celvalue_hk = (string)sheet.GetCellValue(i + 8, colum_start);
                JudgeRecordSemesterViewModel JudgeRecord_hk = new JudgeRecordSemesterViewModel();
                JudgeRecord_hk.ErrorDescription = ErrorDescription;
                if (poc != null)
                {
                    JudgeRecord_hk.Status = poc.Status;
                    JudgeRecord_hk.PupilID = poc.PupilID;
                }
                JudgeRecord_hk.AcademicYearID = global.AcademicYearID.Value;
                JudgeRecord_hk.PupilName = FullName;
                JudgeRecord_hk.PupilCode = PupilCode;
                JudgeRecord_hk.ClassID = classid;
                JudgeRecord_hk.SchoolID = global.SchoolID.Value;
                JudgeRecord_hk.SubjectID = (int)subjectid;
                JudgeRecord_hk.Year = acaYear.Year;
                JudgeRecord_hk.Semester = (int)semesterid;
                if (string.IsNullOrWhiteSpace(celvalue_hk) || celvalue_hk == "CĐ" || celvalue_hk == "Đ")
                {
                    JudgeRecord_hk.Judgement = celvalue_hk;
                    JudgeRecord_hk.MarkTypeID = markTypeHK.MarkTypeID;
                    JudgeRecord_hk.MarkedDate = DateTime.Now;
                    JudgeRecord_hk.CreatedDate = DateTime.Now;
                    JudgeRecord_hk.ModifiedDate = DateTime.Now;
                    JudgeRecord_hk.Title = "HK";
                    //if (lstLockedMarkDetail.Contains(JudgeRecord_hk.Title))
                    //{
                    //    isLegalBot = false;
                    //    JudgeRecord_hk.ErrorDescription += "- Con điểm " + JudgeRecord_hk.Title + " bị khóa </br>";
                    //}
                }
                else
                {
                    JudgeRecord_hk.Judgement = celvalue_hk;
                    JudgeRecord_hk.Title = "HK";
                    isLegalBot = false;
                    JudgeRecord_hk.ErrorDescription += "- Điểm " + JudgeRecord_hk.Title + " không hợp lệ </br>";
                }
                JudgeRecord_hk.IsLegalBot = isLegalBot;
                JudgeRecord_hk.flagPupil = flagPupil;
                ListJudgeRecordSemester.Add(JudgeRecord_hk);
                #endregion

                colum_start++;
                var tbm_celvalue = (string)sheet.GetCellValue(i + 8, colum_start);
                if (!string.IsNullOrWhiteSpace(tbm_celvalue))
                {
                    SummedUpRecordBO SummedUpRecord = new SummedUpRecordBO();
                    SummedUpRecord.AcademicYearID = global.AcademicYearID.Value;
                    SummedUpRecord.ClassID = classid;
                    if (poc != null)
                    {
                        SummedUpRecord.PupilID = poc.PupilID;
                        SummedUpRecord.Status = poc.Status;
                    }
                    SummedUpRecord.SchoolID = global.SchoolID.Value;
                    SummedUpRecord.Semester = (int)semesterid;
                    SummedUpRecord.SubjectID = (int)subjectid;
                    SummedUpRecord.SummedUpDate = DateTime.Now;
                    SummedUpRecord.JudgementResult = tbm_celvalue;
                    SummedUpRecord.IsCommenting = classSubject.IsCommenting.Value;
                    SummedUpRecord.Year = acaYear.Year;
                    SummedUpRecord.IsLegalBot = isLegalBot;
                    ListSummedUpRecord.Add(SummedUpRecord);
                }
                else
                {
                    SummedUpRecordBO SummedUpRecord = new SummedUpRecordBO();
                    SummedUpRecord.AcademicYearID = global.AcademicYearID.Value;
                    SummedUpRecord.ClassID = classid;
                    if (poc != null)
                    {
                        SummedUpRecord.PupilID = poc.PupilID;
                        SummedUpRecord.Status = poc.Status;
                    }
                    SummedUpRecord.SchoolID = global.SchoolID.Value;
                    SummedUpRecord.Semester = (int)semesterid;
                    SummedUpRecord.SubjectID = (int)subjectid;
                    SummedUpRecord.SummedUpDate = DateTime.Now;
                    SummedUpRecord.JudgementResult = "-1";
                    SummedUpRecord.IsCommenting = classSubject.IsCommenting.Value;
                    SummedUpRecord.Year = acaYear.Year;
                    SummedUpRecord.IsLegalBot = isLegalBot;
                    ListSummedUpRecord.Add(SummedUpRecord);
                }
                model.ListJudgeRecordSemesterViewModel = ListJudgeRecordSemester;
                model.ListSummedUpRecord = ListSummedUpRecord;
                i++;
                flagPupil++;
            }
            return model;
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportLegalData()
        {
            GlobalInfo glo = new GlobalInfo();
            List<JudgeRecordSemesterViewModel> lsTemp = (List<JudgeRecordSemesterViewModel>)Session["ImportData_ListJudgeRecord"];
            List<int> listPupilNotImport = lsTemp.Where(u => u.IsLegalBot == false).Select(u => u.PupilID).Distinct().ToList();
            lsTemp = lsTemp.Where(u => !listPupilNotImport.Contains(u.PupilID)).ToList();
            List<SummedUpRecordBO> lstSummedUpRecord = (List<SummedUpRecordBO>)Session["ImportData_ListSummedUpRecord"];

            lstSummedUpRecord = lstSummedUpRecord.Where(o => o.IsLegalBot.HasValue && o.IsLegalBot.Value).ToList();
            // khong co hoc sinh thi bo
            if (lstSummedUpRecord.Count == 0)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
            }
            int semesterid = (int)Session["semesterid"];
            int? periodid = (int?)Session["periodid"];

            lsTemp = lsTemp.Where(o => o.IsLegalBot.HasValue && o.IsLegalBot.Value && !string.IsNullOrWhiteSpace(o.Judgement)).ToList();
            List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
            // Lay thong tin mien giam
            List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(lstSummedUpRecord.First().ClassID, semesterid)
                    .Where(o => o.SubjectID == lstSummedUpRecord.First().SubjectID.Value)
                    .ToList();
            foreach (var item in lsTemp)
            {
                if (listExemptedSubject.Any(u => u.PupilID == item.PupilID))
                    continue;
                // Bo qua cac hoc sinh mien giam
                JudgeRecord JudgeRecord = new JudgeRecord();
                #region Assign Data for JudgeRecord item
                JudgeRecord.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                JudgeRecord.PupilID = item.PupilID;
                JudgeRecord.ClassID = item.ClassID;
                JudgeRecord.SchoolID = item.SchoolID;
                JudgeRecord.SubjectID = item.SubjectID;
                JudgeRecord.MarkTypeID = item.MarkTypeID;
                JudgeRecord.CreatedAcademicYear = item.Year;
                JudgeRecord.Semester = item.Semester;
                JudgeRecord.Judgement = item.Judgement;
                JudgeRecord.OrderNumber = item.OrderNumber;
                JudgeRecord.MarkedDate = item.MarkedDate;
                JudgeRecord.CreatedDate = item.CreatedDate;
                JudgeRecord.ModifiedDate = item.ModifiedDate;
                JudgeRecord.Title = item.Title;
                #endregion
                ListJudgeRecord.Add(JudgeRecord);
            }
            List<SummedUpRecord> lstSummedUp = new List<SummedUpRecord>();
            lstSummedUpRecord.ForEach(o =>
            {
                if (o.Status.HasValue && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING &&
                    !listExemptedSubject.Any(u => u.PupilID == o.PupilID))
                // Bo qua cac hoc sinh mien giam)
                {
                    SummedUpRecord obj = new SummedUpRecord();
                    Utils.Utils.BindTo(o, obj);
                    obj.CreatedAcademicYear = o.Year.Value;
                    lstSummedUp.Add(obj);
                }
            });
            if (ListJudgeRecord.Count > 0)
            {
                JudgeRecordBusiness.InsertJudgeRecord(new GlobalInfo().UserAccountID, ListJudgeRecord, lstSummedUp, (int)semesterid, null, _globalInfo.EmployeeID, null, glo.SchoolID.Value, glo.AcademicYearID.Value, ListJudgeRecord[0].ClassID, ListJudgeRecord[0].SubjectID);
                JudgeRecordBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
        }
        #endregion
        private class OldMark
        {
            public long JudgeRecordID { get; set; }
            public string Title { get; set; }
            public string Judgement { get; set; }
        }
    }
}




