/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.JudgeRecordSemesterArea
{
    public class JudgeRecordSemesterConstants
    {
        public const string LIST_JUDGERECORD = "listJudgeRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string SEMESTER = "semester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUMMEDUPRECORDOFCLASS = "listSummedUpRecordOfClass";
        public const string LIST_LOCKEDMARKDETAIL = "listLockedMarkDetail";
        public const string LIST_JUDGERECORDOFCLASS = "listJudgeRecordOfClass";
        public const string LIST_JUDGERECORDNOTTBMHKI = "list_JudgeRecordNotTBMHKI";
        
        public const string LIST_SEMESTERDECLARATION = "listSemesterDeclaration";
        public const string LIST_MARKTYPE = "listMarkType";
        public const string SEARCHTITLE = "SearchTitle";
        public const string JUDGERECORDMARK = "JudgeRecordMark";
        public const string JUDGERECORDMARKTBM = "JudgeRecordMarkTBM";
        public const string M_COEFFICIENT = "m_coefficient";
        public const string P_COEFFICIENT = "p_coefficient";
        public const string V_COEFFICIENT = "v_coefficient";
        public const string M_TYPE = "m_type";
        public const string P_TYPE = "p_type";
        public const string V_TYPE = "v_type";
        public const string HK_COEFFICIENT = "hk_coefficient";
        public const string HK_TYPE = "hk_type";
        public const string HASPERMISION = "haspermision";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ISPERMISSIONSUPERVISING = "isPermissionSupervising";
        public const string LOCKACTION = "LockAction";
        public const string ISCLASSIFICATION = "IsClassification";
        public const string ISMINSEMESTER = "IsMinSemester";
        public const string MMIN = "MMin";
        public const string PMIN = "PMin";
        public const string VMIN = "VMin";
        public const string LAST_UPDATE_MARK_RECORD = "LAST_UPDATE_MARK_RECORD";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string TITLE_STRING = "TITLE_STRING";
        public const string LIST_CHANGE_MARK = "LIST_CHANGE_MARK";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";
    }
}