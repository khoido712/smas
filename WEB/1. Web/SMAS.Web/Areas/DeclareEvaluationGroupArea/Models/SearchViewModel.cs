﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareEvaluationGroupArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassAssignment_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", VnenClassSubjectConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevelID { get; set; }
    }
}