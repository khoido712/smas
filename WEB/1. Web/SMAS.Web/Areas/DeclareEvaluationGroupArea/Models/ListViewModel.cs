﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DeclareEvaluationGroupArea.Models
{
    public class ListViewModel
    {
        [ResourceDisplayName("VnenClassSubject_Is_Vnen")]
        public long DeclareEvaluationGroupID { get; set; }
        public int SubjectID { get; set; }
        [ResourceDisplayName("EvaluationDevelopmentGroupName")]
        public string EvaluationDevelopmentGroupName { get; set; }
        [ResourceDisplayName("Area_Label_Description")]
        public string Note { get; set; }

        public int EducationId { get; set; }

        //public int? AppliedType { get; set; }
        //public int? IsCommenting { get; set; }
        //public bool? IsVnenSubject { get; set; }
    }
}