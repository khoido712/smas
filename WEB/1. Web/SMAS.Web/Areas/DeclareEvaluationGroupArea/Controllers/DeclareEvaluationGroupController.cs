﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DeclareEvaluationGroupArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SMAS.Web.Areas.DeclareEvaluationGroupArea.Controllers
{
    public class DeclareEvaluationGroupController : BaseController
    {
        #region properties
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private List<EducationLevel> lstEducationLevel;
        private static Boolean checkCreateButton;
        #endregion

        #region Constructor
        public DeclareEvaluationGroupController(
            IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness
            )
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
        }
        #endregion
        public ActionResult Index()
        {
            SetViewData();
            //Lay danh sach mon hoc cua lop
            int? educationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                ViewData["Evaluation"] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
                educationLevelID = lstEducationLevel.First().EducationLevelID;
                ViewBag.educationID = educationLevelID;
            }
            // viet linq trong _Search ViewData se la du lieu bang?
            ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = new List<ListViewModel>();
            if (educationLevelID != null)
            {
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = _Search(educationLevelID.Value);
            }

            checkCreateButton = CheckButtonPermission();
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = checkCreateButton;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(string selectedIDs, string eduforCb1, string selectedNotes, int allOrNot, string AcademicYearID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = eduforCb1;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            int educationForCb1 = Int32.Parse(eduforCb1);

            //tim duoc danh sach khoa cua class_subject
            List<long> lstClassSubjectID = GetListIDFromString(selectedIDs);
            List<EvaluationDevelopmentGroup> lstEDG = EvaluationDevelopmentGroupBusiness.
                                                      All.
                                                      Where(p => lstClassSubjectID.Contains(p.EvaluationDevelopmentGroupID)
                                                                                                    && p.AppliedLevel == _globalInfo.AppliedLevel
                                                                                                    && p.EducationLevelID == educationForCb1).ToList();

            List<DeclareEvaluationIndex> lstDeleteIndex = DeclareEvaluationIndexBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID &&
                                                                    x.SchoolID == _globalInfo.SchoolID &&
                                                                                                          x.EducationLevelID == educationForCb1 &&
                                                                                                          !lstClassSubjectID.Contains(x.EvaluationDevelopmentGroID)).ToList();

            EvaluationDevelopmentGroup objEDG = null;
            List<DeclareEvaluationGroup> lstDEG = new List<DeclareEvaluationGroup>();
            DeclareEvaluationGroup objDEG = null;
            for (int i = 0; i < lstEDG.Count; i++)
            {
                objEDG = lstEDG[i];
                objDEG = new DeclareEvaluationGroup();
                objDEG.EducationLevelID = objEDG.EducationLevelID;
                objDEG.SchoolID = Int32.Parse(_globalInfo.SchoolID.ToString());
                objDEG.AcademicYearID = Int32.Parse(_globalInfo.AcademicYearID.ToString());
                objDEG.CreateDate = DateTime.Now;
                objDEG.ModifiedDate = DateTime.Now;
                objDEG.EvaluationGroupID = objEDG.EvaluationDevelopmentGroupID;
                objDEG.Note = objEDG.Description;
                lstDEG.Add(objDEG);
            }

            if (allOrNot == 1)
            {
                List<EvaluationDevelopmentGroup> lspEval = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == _globalInfo.AppliedLevel
                                                                    && x.EducationLevelID == educationForCb1 && x.IsActive == true && x.ProvinceID == -1).ToList();
                if (lspEval.Where(x => lstClassSubjectID.Contains(x.EvaluationDevelopmentGroupID)).Count() == 0)
                {
                    // xoa va them group khi chon ap dung
                    DeclareEvaluationGroupBusiness.Insert(lstDEG, dic, 0);
                    //DeclareEvaluationIndexBusiness.DeleteWhenUpdateGroup(lstDeleteIndex);
                    int academicYear = Int32.Parse(AcademicYearID);

                    // xoa va them index khi chon ap dung
                    List<DeclareEvaluationIndex> lstDeleteAllIndex = DeclareEvaluationIndexBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID &&
                                                                                                                    x.EducationLevelID == educationForCb1).ToList();
                    DeclareEvaluationIndexBusiness.DeleteWhenUpdateGroup(lstDeleteAllIndex.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID).ToList());

                    List<DeclareEvaluationIndex> lstIndexWhenUpdateGroup = lstDeleteAllIndex.Where(x => lstClassSubjectID.Contains(x.EvaluationDevelopmentGroID)).ToList();
                    DeclareEvaluationIndexBusiness.InsertWhenUpdateGroup(lstIndexWhenUpdateGroup.Where(x => x.AcademicYearID == academicYear).ToList(), _globalInfo.AcademicYearID.Value);
                    DeclareEvaluationGroupBusiness.Save();
                    DeclareEvaluationIndexBusiness.Save();
                }
            }
            else
            {
                DeclareEvaluationGroupBusiness.Insert(lstDEG, dic, 0);
                DeclareEvaluationIndexBusiness.DeleteWhenUpdateGroup(lstDeleteIndex);
                DeclareEvaluationGroupBusiness.Save();
                DeclareEvaluationIndexBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveAll(string selectedIDs, string AcademicYearID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            List<long> lstClassSubjectID = GetListIDFromString(selectedIDs);

            List<EvaluationDevelopmentGroup> lstEDG = EvaluationDevelopmentGroupBusiness.All.Where(p => lstClassSubjectID.Contains(p.EvaluationDevelopmentGroupID) && p.AppliedLevel == _globalInfo.AppliedLevel).ToList();
            EvaluationDevelopmentGroup objEDG = null;
            List<DeclareEvaluationGroup> lstDEG = new List<DeclareEvaluationGroup>();
            DeclareEvaluationGroup objDEG = null;
            for (int i = 0; i < lstEDG.Count; i++)
            {
                objEDG = lstEDG[i];
                objDEG = new DeclareEvaluationGroup();
                objDEG.EducationLevelID = objEDG.EducationLevelID;
                objDEG.SchoolID = Int32.Parse(_globalInfo.SchoolID.ToString());
                objDEG.AcademicYearID = Int32.Parse(_globalInfo.AcademicYearID.ToString());
                objDEG.CreateDate = DateTime.Now;
                objDEG.ModifiedDate = DateTime.Now;
                objDEG.EvaluationGroupID = objEDG.EvaluationDevelopmentGroupID;
                objDEG.Note = objEDG.Description;
                lstDEG.Add(objDEG);
            }


            List<int> lstEduIDtemp = lstDEG.Select(x => x.EducationLevelID).Distinct().ToList();

            for (int i = 0; i < lstEduIDtemp.Count; i++)
            {
                int educationLevelID = lstEduIDtemp[i];
                int appliLevelID = _globalInfo.AppliedLevel.Value;
                List<DeclareEvaluationGroup> lstDEGTemp = lstDEG.Where(x => x.EducationLevelID == lstEduIDtemp[i]).ToList();
                List<EvaluationDevelopmentGroup> lspEval = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == appliLevelID
                    && x.EducationLevelID == educationLevelID && x.IsActive == true && x.ProvinceID == -1).ToList();
                if (lspEval.Where(x => lstClassSubjectID.Contains(x.EvaluationDevelopmentGroupID)).Count() == 0)
                {
                    // xoa va them group khi chon ap dung
                    DeclareEvaluationGroupBusiness.InsertAll(lstDEGTemp, dic, educationLevelID);
                    //DeclareEvaluationIndexBusiness.DeleteWhenUpdateGroup(lstDeleteIndex);
                    int academicYear = Int32.Parse(AcademicYearID);

                    // xoa va them index khi chon ap dung
                    List<DeclareEvaluationIndex> lstDeleteAllIndex = DeclareEvaluationIndexBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID &&
                                                                                                                    x.EducationLevelID == educationLevelID).ToList();
                    DeclareEvaluationIndexBusiness.DeleteWhenUpdateGroup(lstDeleteAllIndex.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID).ToList());

                    List<DeclareEvaluationIndex> lstIndexWhenUpdateGroup = lstDeleteAllIndex.Where(x => lstClassSubjectID.Contains(x.EvaluationDevelopmentGroID)).ToList();
                    DeclareEvaluationIndexBusiness.InsertWhenUpdateGroup(lstIndexWhenUpdateGroup.Where(x => x.AcademicYearID == academicYear).ToList(), _globalInfo.AcademicYearID.Value);
                    DeclareEvaluationGroupBusiness.Save();
                    DeclareEvaluationIndexBusiness.Save();
                }
            }
            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        private List<long> GetListIDFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                str = str.Remove(str.Length - 1);
                idArr = str.Split(',');
            }
            else
            {
                idArr = new string[] { };
            }
            List<long> lstID = idArr.Length > 0 ? idArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            return lstID;
        }

        private List<string> GetNoteFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                str = str.Remove(str.Length - 1);
                idArr = str.Split(',');
            }
            else
            {
                idArr = new string[] { };
            }


            List<string> lstID = idArr.Length > 0 ? idArr.ToList() :
                                            new List<string>();

            return lstID;
        }

        private bool CheckButtonPermission()
        {
            return IsCurrentYear() &&
                GetMenupermission("VnenClassSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) >= SystemParamsInFile.PER_CREATE;
        }

        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }

        public PartialViewResult Search(string EducationLevelID)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            if (EducationLevelID != null)
            {
                lstResult = _Search(Int32.Parse(EducationLevelID));
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
            }
            ViewBag.educationID = EducationLevelID;
            var lstAcademicYear = AcademicYearBusiness.All.Where(x => x.AcademicYearID != _globalInfo.AcademicYearID
                                                                && x.SchoolID == _globalInfo.SchoolID
                                                                && x.IsActive == true
                                                                && x.SchoolProfile.IsActive == true).ToList();
            ViewBag.cbAcademicYear = lstAcademicYear;
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = checkCreateButton;
            return PartialView("_List", lstResult);
        }

        public List<ListViewModel> SortForCheck(List<ListViewModel> lstResult, List<ListViewModel> kl)
        {
            List<ListViewModel> lstCorrect = new List<ListViewModel>();
            ListViewModel objCorrect = null;
            List<ListViewModel> lstIscorrect = new List<ListViewModel>();
            ListViewModel objIscorrect = null;
            for (int i = 0; i < lstResult.Count(); i++)
            {
                int dem = 0;
                int temp = 0;
                for (int j = 0; j < kl.Count(); j++)
                {
                    if (lstResult[i].DeclareEvaluationGroupID == kl[j].DeclareEvaluationGroupID)
                    {
                        dem++;
                        temp = j;
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (dem != 0)
                {
                    objCorrect = new ListViewModel();
                    objCorrect.EvaluationDevelopmentGroupName = lstResult[i].EvaluationDevelopmentGroupName;
                    objCorrect.DeclareEvaluationGroupID = lstResult[i].DeclareEvaluationGroupID;
                    objCorrect.Note = lstResult[i].Note;
                    lstCorrect.Add(objCorrect);
                }
                else
                {
                    objIscorrect = new ListViewModel();
                    objIscorrect.EvaluationDevelopmentGroupName = lstResult[i].EvaluationDevelopmentGroupName;
                    objIscorrect.DeclareEvaluationGroupID = lstResult[i].DeclareEvaluationGroupID;
                    objIscorrect.Note = lstResult[i].Note;
                    lstIscorrect.Add(objIscorrect);
                }
            }
            lstResult = new List<ListViewModel>();
            // dua 2 danh sach thanh 1 danh sach da sap xep
            for (int i = 0; i < lstCorrect.Count(); i++)
            {
                objCorrect = new ListViewModel();
                objCorrect.EvaluationDevelopmentGroupName = lstCorrect[i].EvaluationDevelopmentGroupName;
                objCorrect.DeclareEvaluationGroupID = lstCorrect[i].DeclareEvaluationGroupID;
                objCorrect.Note = lstCorrect[i].Note;
                lstResult.Add(objCorrect);
            }
            for (int i = 0; i < lstIscorrect.Count(); i++)
            {
                objIscorrect = new ListViewModel();
                objIscorrect.EvaluationDevelopmentGroupName = lstIscorrect[i].EvaluationDevelopmentGroupName;
                objIscorrect.DeclareEvaluationGroupID = lstIscorrect[i].DeclareEvaluationGroupID;
                objIscorrect.Note = lstIscorrect[i].Note;
                lstResult.Add(objIscorrect);
            }
            return lstResult;
        }

        private List<ListViewModel> _Search(int educationLevelID)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            // Tra ve du lieu trong bang voi classID => co the tra du lieu cua? minh' o day trong wa SeachByClass
            List<DeclareEvaluationGroup> lstClassSubject = DeclareEvaluationGroupBusiness.SearchByClass(educationLevelID, dic).ToList();
            // new => listViewModel duoc khai bao trong Models
            List<EvaluationDevelopmentGroup> lspEval = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == _globalInfo.AppliedLevel
                                                                        && x.EducationLevelID == educationLevelID && x.IsActive == true).ToList();

            List<EvaluationDevelopmentGroup> lstEvalTempAll = lspEval.Where(x => x.ProvinceID == -1).ToList();
            List<EvaluationDevelopmentGroup> lstEvalTempPro = lspEval.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
            if (lstEvalTempPro.Count != 0)
            {
                lspEval = lstEvalTempPro.ToList();
            }
            else
            {
                lspEval = lstEvalTempAll.ToList();
            }

            lstResult = lspEval.OrderBy(x => x.OrderID).Select(o => new ListViewModel
                                                    {
                                                        DeclareEvaluationGroupID = o.EvaluationDevelopmentGroupID,
                                                        EvaluationDevelopmentGroupName = o.EvaluationDevelopmentGroupName,
                                                        Note = o.Description
                                                    }).ToList();
            List<ListViewModel> kl = (from oik in lstClassSubject
                                      join b in lspEval on oik.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                                      select new ListViewModel
                                      {
                                          DeclareEvaluationGroupID = oik.EvaluationGroupID,
                                          EvaluationDevelopmentGroupName = b.EvaluationDevelopmentGroupName,
                                          Note = b.Description
                                      }).ToList();
            ViewBag.totalcolumn = kl.Count();
            ViewBag.compare = kl;
            lstResult = SortForCheck(lstResult, kl);
            return lstResult;
        }

        public ActionResult SearchforCB1(string educationId, string ecademicId)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            if (educationId != null && ecademicId != null)
            {
                lstResult = SearchforAcademic(Int32.Parse(educationId), Int32.Parse(ecademicId));
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
            }
            var lstAcademicYear = AcademicYearBusiness.All.Where(x => x.AcademicYearID != _globalInfo.AcademicYearID
                                                                && x.SchoolID == _globalInfo.SchoolID
                                                                && x.IsActive == true
                                                                && x.SchoolProfile.IsActive == true).ToList();
            ViewBag.cbAcademicYear = lstAcademicYear;
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = checkCreateButton;
            ViewBag.educationID = educationId;
            return Json(lstResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchforCB1All(string ecademicId)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            if (ecademicId != null)
            {
                lstResult = SearchforAcademicAll(Int32.Parse(ecademicId));
                ViewData[VnenClassSubjectConstants.LIST_CLASS_SUBJECT] = lstResult;
            }
            var lstAcademicYear = AcademicYearBusiness.All.Where(x => x.AcademicYearID != _globalInfo.AcademicYearID
                                                                && x.SchoolID == _globalInfo.SchoolID
                                                                && x.IsActive == true
                                                                && x.SchoolProfile.IsActive == true).ToList();
            ViewBag.cbAcademicYear = lstAcademicYear;
            ViewData[VnenClassSubjectConstants.PER_CHECK_BUTTON] = checkCreateButton;
            return Json(lstResult, JsonRequestBehavior.AllowGet);
        }

        private List<ListViewModel> SearchforAcademic(int educationLevelID, int ecademicId)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            // Tra ve du lieu trong bang voi classID => co the tra du lieu cua? minh' o day trong wa SeachByClass
            List<DeclareEvaluationGroup> lstClassSubject = DeclareEvaluationGroupBusiness.All.Where(x => x.AcademicYearID == ecademicId && x.SchoolID == _globalInfo.SchoolID &&
                                                            x.EducationLevelID == educationLevelID).ToList();
            // new => listViewModel duoc khai bao trong Models
            List<EvaluationDevelopmentGroup> lspEval = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == _globalInfo.AppliedLevel
                                                                                                     && x.EducationLevelID == educationLevelID).ToList();
            ViewBag.totalcolumn = lstClassSubject.Count();
            // xem xet dat dieu kien o day
            lstResult = (from oik in lstClassSubject
                         join b in lspEval on oik.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                         select new ListViewModel
                         {
                             DeclareEvaluationGroupID = oik.EvaluationGroupID,
                             EvaluationDevelopmentGroupName = b.EvaluationDevelopmentGroupName,
                             Note = b.Description
                         }).ToList();
            return lstResult;
        }

        private List<ListViewModel> SearchforAcademicAll(int ecademicId)
        {
            List<ListViewModel> lstResult = new List<ListViewModel>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            // Tra ve du lieu trong bang voi classID => co the tra du lieu cua? minh' o day trong wa SeachByClass
            List<DeclareEvaluationGroup> lstClassSubject = DeclareEvaluationGroupBusiness.All.Where(x => x.AcademicYearID == ecademicId && x.SchoolID == _globalInfo.SchoolID).ToList();
            // new => listViewModel duoc khai bao trong Models
            List<EvaluationDevelopmentGroup> lspEval = EvaluationDevelopmentGroupBusiness.All.Where(x => x.AppliedLevel == _globalInfo.AppliedLevel
                                                                                                    ).ToList();
            ViewBag.totalcolumn = lstClassSubject.Count();
            // xem xet dat dieu kien o day
            lstResult = (from oik in lstClassSubject
                         join b in lspEval on oik.EvaluationGroupID equals b.EvaluationDevelopmentGroupID
                         select new ListViewModel
                         {
                             EducationId = oik.EducationLevelID,
                             DeclareEvaluationGroupID = oik.EvaluationGroupID,
                             EvaluationDevelopmentGroupName = b.EvaluationDevelopmentGroupName,
                             Note = b.Description
                         }).ToList();
            return lstResult;
        }

        #region Private methods
        private void SetViewData()
        {
            //Lay danh sach khoi
            lstEducationLevel = _globalInfo.EducationLevels;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;

            // thiet lap cho cb academic tru nam hien tai // != _globalInfo.AcademicYearID
            var lstAcademicYear = AcademicYearBusiness.All.Where(x => x.AcademicYearID != _globalInfo.AcademicYearID
                                                                && x.SchoolID == _globalInfo.SchoolID
                                                                && x.IsActive == true
                                                                && x.SchoolProfile.IsActive == true).ToList();

            ViewBag.cbAcademicYear = lstAcademicYear;
        }
        #endregion

    }
}
