﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DeclareEvaluationGroupArea
{
    public class DeclareEvaluationGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DeclareEvaluationGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DeclareEvaluationGroupArea_default",
                "DeclareEvaluationGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
