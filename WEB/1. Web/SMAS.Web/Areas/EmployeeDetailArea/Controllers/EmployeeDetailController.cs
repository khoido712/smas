﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeDetailArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;
using System.Text;
using Telerik.Web.Mvc;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.EmployeeDetailArea.Controllers
{
    public class EmployeeDetailController : BaseController
    {
        #region variable
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBreatherBusiness EmployeeBreatherBusiness;
        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;
        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        private readonly IContractBusiness ContractBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IQualificationGradeBusiness QualificationGradeBusiness;
        private IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private IEmployeeScaleBusiness EmployeeScaleBusiness;
        private string GRADE = Res.Get("Common_Label_Grade");
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private string TEMPLATE_EMPLOYEE = "GV_DanhSachCanBo.xls";
        private string TEMPLATE_EMPLOYEE_NAME = "GV_DanhSachCanBo_{0}.xls";
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;

        private readonly IUserAccountBusiness UserAccountBusiness;

        #endregion variable

        #region contructor

        public EmployeeDetailController(IEmployeeBusiness employeeBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness
            , IWorkTypeBusiness WorkTypeBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            IContractBusiness ContractBusiness
            , IWorkGroupTypeBusiness WorkGroupTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
            , IQualificationGradeBusiness QualificationGradeBusiness
            , IEmployeeQualificationBusiness EmployeeQualificationBusiness
            , IEmployeeSalaryBusiness EmployeeSalaryBusiness
            , IEmployeeScaleBusiness EmployeeScaleBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IProvinceBusiness ProvinceBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness
            , IEmployeeWorkMovementBusiness employeeWorkMovementBusiness
            , IEmployeeBreatherBusiness employeeBreatherBusiness
            , IUserAccountBusiness UserAccountBusiness)
        {
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;
            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.ContractBusiness = ContractBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.EmployeeWorkingHistoryBusiness = EmployeeWorkingHistoryBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.EmployeeWorkMovementBusiness = employeeWorkMovementBusiness;
            this.EmployeeBreatherBusiness = employeeBreatherBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
        }

        #endregion contructor

        #region index

        //
        // GET: /EmployeeProfileManagement/

        public ActionResult Index()
        {
            int employeeId = _globalInfo.EmployeeID.Value;
            Employee employee = this.EmployeeBusiness.Find(employeeId);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeDetailConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeDetailConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeDetailViewModel EmployeeProfileManagementViewModel = new EmployeeDetailViewModel();

            SetViewData();

            #region load du lieu cho tab thu nhat
            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            EmployeeProfileManagementViewModel.AutoGenCode = CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);

            if (employee.FatherBirthDate.HasValue) EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            if (employee.MotherBirthDate.HasValue) EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            if (employee.SpouseBirthDate.HasValue) EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;

            //EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkTypeID.HasValue ? employee.WorkType.WorkGroupTypeID : new Nullable<int>();

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                //them vao viewdata worktype
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeDetailConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() {
                { "AppliedLevel", employee.AppliedLevel },
                { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[EmployeeDetailConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName");
            }
            ViewData[EmployeeDetailConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            #endregion load du lieu cho tab thu nhat

            return View();
        }

        #endregion index

        #region edit,update

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EmployeeDetailViewModel EmployeeProfileManagementViewModel)
        {
            //CheckPermissionForAction(EmployeeProfileManagementViewModel.EmployeeID, "Employee");            
            //if (this.GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            //{
            //    throw new BusinessException("Validate_Permission_Teacher");
            //}
            Employee employee = EmployeeBusiness.Find(EmployeeProfileManagementViewModel.EmployeeID);
            //string oldObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });// convert to json string for old object
            string oldObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            if (!CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
            {
                EmployeeProfileManagementViewModel.EmployeeCode = employee.EmployeeCode;
            }
            EmployeeProfileManagementViewModel.Image = employee.Image;
            Utils.Utils.BindTo(EmployeeProfileManagementViewModel, employee);
            if (EmployeeProfileManagementViewModel.iFatherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error");
                }
                if (EmployeeProfileManagementViewModel.iFatherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Father_Error");
                }
                employee.FatherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iMotherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error");
                }
                if (EmployeeProfileManagementViewModel.iMotherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Mother_Error");
                }
                employee.MotherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iSpouseBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error");
                }
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_SPouse_Error");
                }
                employee.SpouseBirthDate = new DateTime(EmployeeProfileManagementViewModel.iSpouseBirthDate.Value, 1, 1);
            }

            if (EmployeeProfileManagementViewModel.ContractTypeID != 0 && EmployeeProfileManagementViewModel.ContractTypeID != null)
            {
                ContractType contractType = ContractTypeBusiness.Find(EmployeeProfileManagementViewModel.ContractTypeID);
                if (contractType.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME)
                {
                    if (EmployeeProfileManagementViewModel.JoinedDate == null)
                    {
                        throw new SMAS.Business.Common.BusinessException("Employee_ContractType_Label_JoinedDate");
                    }
                }
            }
            //truong Name lay tu FullName
            int space = employee.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                employee.Name = employee.FullName;
            }
            else
            {
                employee.Name = employee.FullName.Substring(space + 1);
            }

            //dua image vao employee
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                employee.Image = imageBytes;
            }

            employee.SchoolID = _globalInfo.SchoolID;
            employee.IsActive = true;


            if (CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER)     //Neu truong duoc tu dong generate code
                && CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER) //Truong duoc sua
                && EmployeeProfileManagementViewModel.AutoGenCode)                                                                       //Nguoi dung chon sua
            {
                employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(_globalInfo.SchoolID.Value);
            }

            employee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            this.EmployeeBusiness.UpdateTeacher(employee);
            this.EmployeeBusiness.Save();

            // Tạo giá trị ghi log
            //string newObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            string newObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            SetViewDataActionAudit(oldObject, newObject, employee.EmployeeID.ToString(), "Update employee_id:" + employee.EmployeeID.ToString(), employee.EmployeeID.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_UPDATE, "Cập nhật hồ sơ " + employee.FullName + " mã " + employee.EmployeeCode);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion edit,update

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        private byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #region setViewData

        public void SetViewData()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //_globalInfoInfo _globalInfo = _globalInfoInfo.getInstance();
            //var hasPermission = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            //ViewData[EmployeeWorkingHistoryConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeQualificationConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeSalaryConstants.HAS_PERMISSION] = hasPermission;
            //ViewData[EmployeeDetailConstant.Has_Permission_TeachingAssignment] = hasPermission;

            // SchoolFacultyBussiness.SearchBySchool(IDictionary)
            //với IDictionary[“SchoolID”] = UserInfo.SchoolID
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            ViewData[EmployeeDetailConstant.LISTSCHOOLFACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //cboWorkType: Lấy danh sách từ WorkTypeBusiness.Search()
            //IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>() { 
            //    //{ "SchoolID", _globalInfo.SchoolID.Value }, 
            //    { "IsActive", true } })
            //    .OrderBy(o => o.Resolution);
            List<WorkType> lsWorkType = this.getListOfWorkType();
            ViewData[EmployeeDetailConstant.LISTWORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

            //cboEmploymentStatus:
            //+ EMPLOYMENT_STATUS_WORKING : Đang làm việc
            //+ EMPLOYMENT_STATUS_WORK_CHANGED : Chuyển công tác
            //+ EMPLOYMENT_STATUS_RETIRED : Nghỉ hưu
            //+ EMPLOYMENT_STATUS_BREATHER: Tạm nghỉ
            List<ComboObject> listEmploymentStatus = new List<ComboObject>();
            listEmploymentStatus.Add(new ComboObject(EmployeeDetailConstant.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Employee_Label_EmployeeStatusWorking")));
            listEmploymentStatus.Add(new ComboObject(EmployeeDetailConstant.EMPLOYMENT_STATUS_WORK_CHANGED.ToString(), Res.Get("Employee_Label_EmployeeStatusWorkingChange")));
            listEmploymentStatus.Add(new ComboObject(EmployeeDetailConstant.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Employee_Label_EmployeeStatusRetired")));
            listEmploymentStatus.Add(new ComboObject(EmployeeDetailConstant.EMPLOYMENT_STATUS_SEVERANCE.ToString(), Res.Get("Employee_Label_EmployeeStatusSeverance")));
            listEmploymentStatus.Add(new ComboObject(EmployeeDetailConstant.EMPLOYMENT_STATUS_BREATHER.ToString(), Res.Get("Common_Label_EmployeeStatusLeavedOff")));
            ViewData[EmployeeDetailConstant.LISTEMPLOYMENTSTATUS] = new SelectList(listEmploymentStatus, "key", "value");

            //   cboSex:
            //+ 1 : Nam
            //+ 2 : Nữ
            List<ComboObject> listSex = new List<ComboObject>();
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
            ViewData[EmployeeDetailConstant.LISTSEX] = new SelectList(listSex, "key", "value");

            //cboTrainingLevel: TrainingLevelBussiness.Search()    
            //IQueryable<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
            List<TrainingLevel> lsTrainingLevel = this.getListOfTrainingLevel();
            ViewData[EmployeeDetailConstant.LISTGRADUATIONLEVEL] = new SelectList(lsTrainingLevel, "TrainingLevelID", "Resolution");

            //cboGraduationLevel: Lấy danh sách từ GraduationLevelBussiness.Search(Dictionary) với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel ->cho form create,edit
            //IQueryable<TrainingLevel> lsGraduationLevelUpdateOrEdit = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
            List<TrainingLevel> lsGraduationLevelUpdateOrEdit = this.getListOfTrainingLevel();
            ViewData[EmployeeDetailConstant.LS_GRADUATIONLEVEL] = new SelectList(lsGraduationLevelUpdateOrEdit, "TrainingLevelID", "Resolution");

            //cboContractType: Lấy danh sách từ ContractTypeBusiness.Search()
            //IQueryable<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<ContractType> lsContractType = this.getListOfContractType();
            ViewData[EmployeeDetailConstant.LISTCONTRACTTYPE] = new SelectList(lsContractType, "ContractTypeID", "Resolution");

            // Danh sach cac dan toc
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("IsActive", true);
            //List<Ethnic> listEthnic = EthnicBusiness.Search(dic).OrderBy(o => o.EthnicName).ToList();
            List<Ethnic> listEthnic = this.getListOfEthnic();
            ViewData[EmployeeDetailConstant.LS_ETHNIC] = new SelectList(listEthnic, "EthnicID", "EthnicName", SMAS.Business.Common.SystemParamsInFile.DEFAULT_ETHNIC);

            //List<Religion> listReligion = ReligionBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<Religion> listReligion = ReligionBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<Religion> listReligion = this.getListOfReligion();
            ViewData[EmployeeDetailConstant.LS_RELIGION] = new SelectList(listReligion, "ReligionID", "Resolution", SMAS.Business.Common.SystemParamsInFile.DEFAULT_RELIGION);

            // List<FamilyType> listFamilyType = FamilyTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<FamilyType> listFamilyType = this.getListOfFamilyType();
            ViewData[EmployeeDetailConstant.LS_FAMILYTYPE] = new SelectList(listFamilyType, "FamilyTypeID", "Resolution");

            //List<QualificationType> listQualificationType = QualificationTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<QualificationType> listQualificationType = this.getListOfQualificationType();
            ViewData[EmployeeDetailConstant.LS_QUALIFICATIONTYPE] = new SelectList(listQualificationType, "QualificationTypeID", "Resolution");

            //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<SpecialityCat> listSpecialityCat = this.getListOfSpecialityCat();
            ViewData[EmployeeDetailConstant.LS_SPECIALITYCAT] = new SelectList(listSpecialityCat, "SpecialityCatID", "Resolution");

            // List< QualificationLevel> listQualificationLevel =  QualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<QualificationLevel> listQualificationLevel = this.getListOfQualificationLevel();
            ViewData[EmployeeDetailConstant.LS_QUALIFICATIONLEVEL] = new SelectList(listQualificationLevel, "QualificationLevelID", "Resolution");

            //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<ForeignLanguageGrade> listForeignLanguageGrade = this.getListOfForeignLanguageGrade();
            ViewData[EmployeeDetailConstant.LS_FOREIGNLANGUAGEGRADE] = new SelectList(listForeignLanguageGrade, "ForeignLanguageGradeID", "Resolution");

            //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<ITQualificationLevel> listITQualificationLevel = this.getListOfITQualificationLevel();
            ViewData[EmployeeDetailConstant.LS_ITQUALIFICATIONLEVEL] = new SelectList(listITQualificationLevel, "ITQualificationLevelID", "Resolution");

            //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<StateManagementGrade> listStateManagementGrade = this.getListOfStateManagementGrade();
            ViewData[EmployeeDetailConstant.LS_STATEMANAGEMENTGRADE] = new SelectList(listStateManagementGrade, "StateManagementGradeID", "Resolution");

            //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<PoliticalGrade> listPoliticalGrade = this.getListOfPoliticalGrade();
            ViewData[EmployeeDetailConstant.LS_POLITICALGRADE] = new SelectList(listPoliticalGrade, "PoliticalGradeID", "Resolution");

            //List<EducationalManagementGrade> listEducationalManagementGrade =  EducationalManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            List<EducationalManagementGrade> listEducationalManagementGrade = this.getListOfEducationalManagementGrade();
            ViewData[EmployeeDetailConstant.LS_EDUCATIONALMANAGEMENTGRADE] = new SelectList(listEducationalManagementGrade, "EducationalManagementGradeID", "Resolution");

            //List<ContractType> listContractType = ContractTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
            ////List<Contract> listContract = ContractBusiness.Search(dic).ToList();
            //ViewData[EmployeeDetailConstant.LS_CONTRACT] = new SelectList(listContractType, "ContractTypeID", "Resolution");

            //List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
            //List<Contract> listContract = ContractBusiness.Search(dic).ToList();
            List<Contract> listContract = this.getListOfContract();
            ViewData[EmployeeDetailConstant.LS_CONTRACT] = new SelectList(listContract, "ContractID", "ContractID");


            // List<StaffPosition> listStaffPosition = StaffPositionBusiness.Search(dic).ToList();
            //List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();
            List<StaffPosition> listStaffPosition = this.getListOfStaffPosition();
            ViewData[EmployeeDetailConstant.LS_STAFFPOSITION] = new SelectList(listStaffPosition, "StaffPositionID", "Resolution");

            //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.GetDependantDept(UserInfo.SupervisingDeptID()).ToList();
            //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(dic).OrderBy(o => o.SupervisingDeptName).ToList();
            List<SupervisingDept> listSupervisingDept = this.getListOfSupervisingDept();
            ViewData[EmployeeDetailConstant.LS_SUPERVISINGDEPT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");

            //List<GroupCat> listGroupCat = GroupCatBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.GroupName).ToList();

            //IQueryable<SupervisingDept> lsSuperTemp = SupervisingDeptBusiness.All.Where(em=>(em.SupervisingDeptID==UserInfo.SupervisingDeptID()));
            //List<GroupCat> listGroupCat = GroupCatBusiness.GetGroupByRoleAndCreatedUser(UserInfo.RoleID(),lsSuperTemp.FirstOrDefault().AdminID.Value).ToList();
            //ViewData[EmployeeDetailConstant.LS_GROUPCAT] = new SelectList(listGroupCat, "GroupCatID", "GroupName");

            //cboWorkGroupType: Lấy danh sách từ WorkGroupTypeBussiness.Search()
            //IQueryable<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<WorkGroupType> lsWorkGroupType = this.getListOfWorkGroupType();
            ViewData[EmployeeDetailConstant.LS_WORKGROUPTYPE] = new SelectList(lsWorkGroupType, "WorkGroupTypeID", "Resolution");

            //cboWorkType: Không có dữ liệu
            ViewData[EmployeeDetailConstant.LS_WORKTYPE] = new SelectList(new string[] { });

            //cboPrimarilyAssignedSubject: Lấy danh sách từ SubjectCat.Search(Dictionary)
            //với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", _globalInfo.AppliedLevel.Value }, { "IsActive", true } }).OrderBy(o => o.DisplayName);
            ViewData[EmployeeDetailConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(new string[] { });

            //SchoolProfileBusiness.GetListAppliedLevel(UserInfo.SchoolID)
            ViewData[EmployeeDetailConstant.LS_APPLIEDLEVEL] = new SelectList(new List<ComboObject>(), "key", "value");
            ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK] = false;
            ViewData[EmployeeDetailConstant.IS_SCHOOL_MODIFIED] = false;
            ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
            ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
            if (_globalInfo.SchoolID != null)
            {
                List<int> lsAL = SchoolProfileBusiness.GetListAppliedLevel(_globalInfo.SchoolID.Value);
                lsAL.Sort();
                List<ComboObject> listAppliedLevel = new List<ComboObject>();

                //listAppliedLevel.Add(new ComboObject("1", "Cấp 1"));
                //listAppliedLevel.Add(new ComboObject("2", "Cấp 2"));
                //listAppliedLevel.Add(new ComboObject("3", "Cấp 3"));
                //listAppliedLevel.Add(new ComboObject("4", "Cấp 4"));
                foreach (var item in lsAL)
                {
                    listAppliedLevel.Add(new ComboObject(item.ToString(), CommonConvert.ConvertToResolutionApplied(item)));
                }
                ViewData[EmployeeDetailConstant.LS_APPLIEDLEVEL] = new SelectList(listAppliedLevel, "key", "value");

                //Autogen code
                //Nếu  CodeConfigBusiness.IsAutoGenCode(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE thì chkAutoCode được chọn
                if (CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK] = true;
                }
                else
                {
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK] = false;
                }

                //-	Nếu CodeConfigBusiness.IsSchoolModify(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE
                if (CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = true;
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = false;
                    ViewData[EmployeeDetailConstant.IS_SCHOOL_MODIFIED] = true;
                }
                else
                {
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
                    ViewData[EmployeeDetailConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
                    ViewData[EmployeeDetailConstant.IS_SCHOOL_MODIFIED] = false;
                }
            }
            //var lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ProvinceID, u.ProvinceName }).OrderBy(o => o.ProvinceName).ToList();
            var lstProvince = this.getListOfProvince();
            ViewData[EmployeeDetailConstant.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");
            if (_globalInfo.IsCurrentYear)
            {
                ViewData[EmployeeDetailConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeDetailConstant.IS_CURRENT_YEAR] = false;
            }
        }

        #endregion setViewData

        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        #region LAY CAC DANH SACH CAN THIET TU CO SO DU LIEU VA LUU CACHE        
        int CachingTime = 2; // Thoi gian cache du lieu

        /// <summary>
        /// Lay danh sach loai cong viec
        /// </summary>
        /// <returns></returns>
        public List<WorkType> getListOfWorkType()
        {
            string key = "CK_LISTWORKTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkType> lsWorkType = this.WorkTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkType;
            }
            else
            {
                return value as List<WorkType>;
            }
        }
        public List<TrainingLevel> getListOfTrainingLevel()
        {
            string key = "CK_LISTGRADUATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsTrainingLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsTrainingLevel;
            }
            else
            {
                return value as List<TrainingLevel>;
            }
        }
        /// <summary>
        /// Danh sach loai hop dong
        /// </summary>
        /// <returns></returns>
        public List<ContractType> getListOfContractType()
        {
            string key = "CK_LISTCONTRACTTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsContractType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsContractType;
            }
            else
            {
                return value as List<ContractType>;
            }
        }
        /// <summary>
        /// Lay danh sach nhom cong viec
        /// </summary>
        /// <returns></returns>
        public List<WorkGroupType> getListOfWorkGroupType()
        {
            string key = "CK_LS_WORKGROUPTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkGroupType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkGroupType;
            }
            else
            {
                return value as List<WorkGroupType>;
            }
        }
        /// <summary>
        /// Lay danh sach cac tinh thanh
        /// </summary>
        /// <returns></returns>
        public List<Province> getListOfProvince()
        {
            string key = "CK_LIST_PROVINCE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Province> lsProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsProvince, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsProvince;
            }
            else
            {
                return value as List<Province>;
            }
        }
        /// <summary>
        /// Lay danh sach cac dan toc
        /// </summary>
        /// <returns></returns>
        public List<Ethnic> getListOfEthnic()
        {
            string key = "CK_LS_ETHNIC";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Ethnic> listEthnic = EthnicBusiness.Search(paras).OrderBy(o => o.EthnicName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEthnic, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEthnic;
            }
            else
            {
                return value as List<Ethnic>;
            }
        }
        /// <summary>
        /// Lay danh sach cac ton giao
        /// </summary>
        /// <returns></returns>
        public List<Religion> getListOfReligion()
        {
            string key = "CK_LS_RELIGION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Religion> listReligion = ReligionBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listReligion, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listReligion;
            }
            else
            {
                return value as List<Religion>;
            }
        }
        public List<FamilyType> getListOfFamilyType()
        {
            string key = "CK_LS_FAMILYTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listFamilyType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listFamilyType;
            }
            else
            {
                return value as List<FamilyType>;
            }
        }

        public List<QualificationType> getListOfQualificationType()
        {
            string key = "CK_LS_QUALIFICATIONTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationType;
            }
            else
            {
                return value as List<QualificationType>;
            }
        }
        public List<SpecialityCat> getListOfSpecialityCat()
        {
            string key = "CK_LS_SPECIALITYCAT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSpecialityCat, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSpecialityCat;
            }
            else
            {
                return value as List<SpecialityCat>;
            }
        }
        public List<QualificationLevel> getListOfQualificationLevel()
        {
            string key = "CK_LS_QUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationLevel;
            }
            else
            {
                return value as List<QualificationLevel>;
            }
        }
        public List<ForeignLanguageGrade> getListOfForeignLanguageGrade()
        {
            string key = "CK_LS_FOREIGNLANGUAGEGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listForeignLanguageGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listForeignLanguageGrade;
            }
            else
            {
                return value as List<ForeignLanguageGrade>;
            }
        }
        public List<ITQualificationLevel> getListOfITQualificationLevel()
        {
            string key = "CK_LS_ITQUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listITQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listITQualificationLevel;
            }
            else
            {
                return value as List<ITQualificationLevel>;
            }
        }
        public List<StateManagementGrade> getListOfStateManagementGrade()
        {
            string key = "CK_LS_STATEMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStateManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStateManagementGrade;
            }
            else
            {
                return value as List<StateManagementGrade>;
            }
        }
        public List<PoliticalGrade> getListOfPoliticalGrade()
        {
            string key = "CK_LS_POLITICALGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listPoliticalGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listPoliticalGrade;
            }
            else
            {
                return value as List<PoliticalGrade>;
            }
        }
        public List<EducationalManagementGrade> getListOfEducationalManagementGrade()
        {
            string key = "CK_LS_EDUCATIONALMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEducationalManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEducationalManagementGrade;
            }
            else
            {
                return value as List<EducationalManagementGrade>;
            }
        }

        public List<Contract> getListOfContract()
        {
            string key = "CK_LS_CONTRACT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listContract, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listContract;
            }
            else
            {
                return value as List<Contract>;
            }
        }
        public List<StaffPosition> getListOfStaffPosition()
        {
            string key = "CK_LS_STAFFPOSITION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStaffPosition, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStaffPosition;
            }
            else
            {
                return value as List<StaffPosition>;
            }
        }
        public List<SupervisingDept> getListOfSupervisingDept()
        {
            string key = "CK_LS_SUPERVISINGDEPT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(paras).OrderBy(o => o.SupervisingDeptName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSupervisingDept, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSupervisingDept;
            }
            else
            {
                return value as List<SupervisingDept>;
            }
        }
        #endregion
    }
}
