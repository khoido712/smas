﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeDetailArea
{
    public class EmployeeDetailAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeDetailArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeDetailArea_default",
                "EmployeeDetailArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
