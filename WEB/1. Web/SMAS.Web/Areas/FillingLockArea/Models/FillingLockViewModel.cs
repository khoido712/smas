﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.ComponentModel;

namespace SMAS.Web.Areas.FillingLockArea.Models
{
    public class FillingLockViewModel
    {
        public int SchoolID { get; set; }

        [DisplayName("Tên trường")]
        public string SchoolName { get; set; }
        [DisplayName("Mã trường")]
        public string SchoolCode { get; set; }
        public int AppliLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public string AppliLevelInput { get; set; }
        public Boolean HK1 { get; set; }
        public Boolean HK2 { get; set; }
        public string StatusHK1 { get; set; }
        public string StatusHK2 {get; set;}
        public string UserLock { get; set; }
        public string UserUnLock { get; set; }
        public int? SuperID { get; set; }
        public int checkSoHK1 { get; set; }
        public int checkSoHK2 { get; set; }

        public string LockTitle { get; set; }
        public int EducationGrade { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserLockID { get; set; }

        public int C1 { get; set; }
        public int C2 { get; set; }
        public int C3 { get; set; }

        public int C1HK1 { get; set; }
        public int C2HK1 { get; set; }
        public int C3HK1 { get; set; }
        public int C1HK2 { get; set; }
        public int C2HK2 { get; set; }
        public int C3HK2 { get; set; }

        public int checkSoC1HK1 { get; set; }
        public int checkSoC2HK1 { get; set; }
        public int checkSoC3HK1 { get; set; }
        public int checkSoC1HK2 { get; set; }
        public int checkSoC2HK2 { get; set; }
        public int checkSoC3HK2 { get; set; }

        public string StatusC1HK1 { get; set; }
        public string StatusC2HK1 { get; set; }
        public string StatusC3HK1 { get; set; }
        public string StatusC1HK2 { get; set; }
        public string StatusC2HK2 { get; set; }
        public string StatusC3HK2 { get; set; }
 


       
    }
}