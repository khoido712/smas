﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.FillingLockArea.Models
{
    public class LockInput
    {
        public int SchoolID { get; set; }
        public int AcademicID { get; set; }
        public int AppliedLevlID { get; set; }
        public string Lock_Title { get; set; }
        public int User_LockID { get; set; }
        public int User_Unlock { get; set; }
        public DateTime Modified_Date { get; set; }
        public DateTime Create_Date { get; set; }
    }
}