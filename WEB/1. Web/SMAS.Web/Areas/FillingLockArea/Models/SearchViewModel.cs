﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.FillingLockArea.Models
{
    public class SearchViewModel
    {
        public string StudyYear { get; set; }
        public string SchoolLevel { get; set; }
        public string District { get; set; }
        public string SchoolName { get; set; }
        public string AdminName { get; set; }
        public string Status { get; set; }
        public string Semester { get; set; }
    }
}