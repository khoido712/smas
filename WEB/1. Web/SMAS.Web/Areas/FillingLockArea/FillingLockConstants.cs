﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.FillingLockArea
{
    public class FillingLockConstants
    {
        //loai account dang nhap
        public const string LIST_ACCOUNTTYPE = "LIST_ACCOUNTTYPE";


        public const string LIST_LOOKUPINFOREPORTPUPIL = "listSchoolReport";
        public const string LIST_PDISTRICT = "LIST_PDISTRICT";
        public const string LIST_DEFAULT_PDISTRICT = "LIST_DEFAULT_PDISTRICT";
        public const string LIST_PACADEMICYEAR = "LIST_PACADEMICYEAR";
        public const string LIST_PPUPILCODE = "LIST_PPUPILCODE";
        public const string LIST_PEDUCATIONGRADE = "LIST_PEDUCATIONGRADE";
        public const string LIST_PEDUCATIONLEVEL = "LIST_PEDUCATIONLEVEL";
        public const string LIST_PFULLNAME = "LIST_PFULLNAME";
        public const string LIST_PSCHOOL = "LIST_PSCHOOL";
        public const string LIST_PCLASS = "LIST_PCLASS";
        public const string LIST_PSEX = "LIST_PSEX";
        public const string LIST_PSTATUS = "LIST_PSTATUS";
        public const string PAGING = "PAGING";
        public const string YEAR = "YEAR";
        public const string SCHOOL_LEVEL = "SCHOOL_LEVEL";
        public const string DISTRICT = "DISTRICT";
        public const string SCHOOL_NAME = "SCHOOL_NAME";
        public const string ADMIN_NAME = "ADMIN_NAME";
        public const string STATUS_LOCK = "STATUS_LOCK";
        public const string SEMESTER_LOCK = "SEMESTER_LOCK";
    }
}