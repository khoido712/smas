﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Areas.FillingLockArea.Models;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.FillingLockArea.Controllers
{
    public class FillingLockController : BaseController
    {
        //
        // GET: /FillingLockArea/FillingLock/
        #region properties
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ILockInputSupervisingDeptBusiness LockInputSupervisingDeptBusiness;
        public static int YearSearch;
        #endregion

        #region Constructor
        public FillingLockController(
            IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness,
            IProvinceSubjectBusiness ProvinceSubjectBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IDistrictBusiness DistrictBusiness,
            IUserAccountBusiness UserAccountBusiness,
            ILockInputSupervisingDeptBusiness LockInputSupervisingDeptBusiness
            )
        {
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.LockInputSupervisingDeptBusiness = LockInputSupervisingDeptBusiness;
        }
        #endregion
        public ActionResult Index()
        {
            GetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            List<FillingLockViewModel> listResult = new List<FillingLockViewModel>();
            ViewData[FillingLockConstants.LIST_PSCHOOL] = listResult;
            ViewData["Total"] = 0;
            ViewData["checkIsENABLE"] = true;
            ViewData["isSub"] = false;
            if (_globalInfo.IsSubSuperVisingDeptRole == true)
            {
                ViewData["isSub"] = true;
            }
            return View();
        }

        public void GetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            int currentYear = DateTime.Now.Year;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }

            ViewData[FillingLockConstants.PAGING] = true;
            if (global.IsSuperVisingDeptRole == true)
            {
                ViewData[FillingLockConstants.LIST_ACCOUNTTYPE] = true;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };
                ViewData["IsSuperVisingDeptRole"] = 1;
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);
                ViewData[FillingLockConstants.LIST_PDISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
                ViewData[FillingLockConstants.LIST_PEDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForProvince().Take(3), "key", "value");
                ViewData[FillingLockConstants.LIST_PSEX] = new SelectList(CommonList.GenreAndAll(), "key", "value");
                ViewData[FillingLockConstants.LIST_PSTATUS] = new SelectList(CommonList.PupilStatus(), "key", "value");

                ViewData["checkIsENABLE"] = true;

                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }
                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + "-" + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }
                ViewData[FillingLockConstants.LIST_PACADEMICYEAR] = new SelectList(lscbYear, "key", "value", currentYear);

                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID},
                };
                List<SchoolProfile> lstSPTemp = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName.ToLower()).ToList();

                List<FillingLockViewModel> LstSchool = new List<FillingLockViewModel>();
                FillingLockViewModel objSchool = null;

                ViewData["count"] = LstSchool.Count();
                ViewData["Status"] = new SelectList(CommonList.FillingLock(), "key", "value");
                ViewData["Semester"] = new SelectList(CommonList.Semester(), "key", "value");

            }
            else if (global.IsSubSuperVisingDeptRole == true)
            {
                ViewData[FillingLockConstants.LIST_ACCOUNTTYPE] = false;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",global.ProvinceID}
                };

                ViewData["IsSuperVisingDeptRole"] = 2;
                int? DistrictID = global.DistrictID;
                IQueryable<District> lstDistrict = DistrictBusiness.Search(dic);

                ViewData[FillingLockConstants.LIST_DEFAULT_PDISTRICT] = DistrictID;
                ViewData[FillingLockConstants.LIST_PDISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName", DistrictID);
                ViewData[FillingLockConstants.LIST_PEDUCATIONGRADE] = new SelectList(CommonList.AppliedLevelForDistrict().Take(2), "key", "value");
                ViewData[FillingLockConstants.LIST_PSEX] = new SelectList(CommonList.GenreAndAllPS(), "key", "value");
                ViewData[FillingLockConstants.LIST_PSTATUS] = new SelectList(CommonList.PupilStatus(), "key", "value");
                ViewData["checkIsENABLE"] = true;

                List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
                if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
                {
                    currentYear = lsYear.FirstOrDefault();
                }

                List<ComboObject> lscbYear = new List<ComboObject>();
                if (lsYear != null && lsYear.Count > 0)
                {
                    foreach (var year in lsYear)
                    {
                        string value = year.ToString() + "-" + (year + 1).ToString();
                        ComboObject cb = new ComboObject(year.ToString(), value);
                        lscbYear.Add(cb);
                    }
                }
                ViewData[FillingLockConstants.LIST_PACADEMICYEAR] = new SelectList(lscbYear, "key", "value", currentYear);

                SupervisingDept su = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
                int SupervisingDeptID = global.SupervisingDeptID.Value;
                if (su != null && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDeptID = su.ParentID.Value;
                }
                IDictionary<string, object> searchSchool = new Dictionary<string, object>()
                {
                    {"SupervisingDeptID", SupervisingDeptID},
                    {"ProvinceID",global.ProvinceID}
                };
                List<SchoolProfile> lstSPTemp = SchoolProfileBusiness.Search(searchSchool).OrderBy(o => o.SchoolName.ToLower()).ToList();

                List<FillingLockViewModel> LstSchool = new List<FillingLockViewModel>();
                FillingLockViewModel objSchool = null;

                ViewData["count"] = LstSchool.Count();

                ViewData["Status"] = new SelectList(CommonList.FillingLock(), "key", "value");
                ViewData["Semester"] = new SelectList(CommonList.Semester(), "key", "value");
            }
        }

        [ValidateInput(true)]
        public PartialViewResult Search(SearchViewModel frm, GridCommand command)
        {
            Utils.Utils.TrimObject(frm);
            List<FillingLockViewModel> listResult;

            if (frm.StudyYear == null)
            {
                listResult = new List<FillingLockViewModel>();
                ViewData["Total"] = 0;
                return PartialView("_List", listResult);
            }
            // So?
            SupervisingDept Super = SupervisingDeptBusiness.All.Where(x => x.ProvinceID == _globalInfo.ProvinceID && x.SupervisingDeptID == _globalInfo.SupervisingDeptID).FirstOrDefault();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = frm.SchoolLevel;
            SearchInfo["DistrictID"] = frm.District;
            SearchInfo["SchoolName"] = frm.SchoolName;
            SearchInfo["Administrator"] = frm.AdminName;
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;

            int academic = Int32.Parse(frm.StudyYear);
            YearSearch = academic;
            // tim theo nam
            IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                                x.EducationGrade != 16 &&
                                                                                                x.EducationGrade != 24);

            ViewData["isSub"] = false;
            if (_globalInfo.IsSubSuperVisingDeptRole == true) {
                lstSP = lstSP.Where(x => (x.EducationGrade == 1 || x.EducationGrade == 2 || x.EducationGrade == 3) && x.SupervisingDeptID == _globalInfo.SupervisingDeptID);
                ViewData["isSub"] = true;
            }

            IQueryable<SchoolProfileBO> lstSPtmp = (from sp in lstSP
                                                    join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                    where sp.IsActive == true && ac.IsActive == true &&
                                                        sp.IsActive == true && ac.Year == academic &&
                                                        sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                    select new SchoolProfileBO 
                                                    {
                                                        SchoolProfileID = sp.SchoolProfileID,
                                                        SchoolCode = sp.SchoolCode,
                                                        SchoolName = sp.SchoolName,
                                                        AcademicYearID = ac.AcademicYearID,
                                                        EducationGrade = sp.EducationGrade
                                                    }).OrderBy(o => o.SchoolName.ToLower());

            List<int> lstSchoolIDtmp = lstSPtmp.Select(p => p.SchoolProfileID).Distinct().ToList();
            List<int> lstAcademicYearID = lstSPtmp.Select(p => p.AcademicYearID).Distinct().ToList();

            List<LockInputSupervisingDept> lstLI = LockInputSupervisingDeptBusiness.All.Where(x => lstSchoolIDtmp.Contains(x.SchoolID) && lstAcademicYearID.Contains(x.AcademicYearID)).ToList();
            List<int> lstSchoolLockID = lstLI.Select(p=>p.SchoolID).Distinct().ToList();

            List<int> lstIDtmp = new List<int>();
            int schoolLevel = !string.IsNullOrEmpty(frm.SchoolLevel) ? int.Parse(frm.SchoolLevel) : 0;

            #region xet du lieu tim kiem
            if (frm.Status == "1")
            {
                if (frm.Semester == "1")
                { 
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock1ID.HasValue && (x.LockTitle == "HK1" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
                else if (frm.Semester == "2")
                {
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock2ID.HasValue && (x.LockTitle == "HK2" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
                else if (frm.Semester == null)
                {
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && ((x.UserLock2ID.HasValue || x.UserLock1ID.HasValue) && !string.IsNullOrEmpty(x.LockTitle))).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
            }
            else if (frm.Status == "2")
            {
                if (frm.Semester == "1")
                {
                    lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0))  && (x.LockTitle != "HK1") && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));             
                }
                else if (frm.Semester == "2")
                {
                    lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0)) && x.LockTitle != "HK2" && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));
                    
                }
                else if (frm.Semester == null)
                {
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && (string.IsNullOrEmpty(x.LockTitle) || x.LockTitle == "HK1" || x.LockTitle == "HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));
                    
                }
            }
            #endregion

            int totalRecord = lstSPtmp.Count();
            ViewData["count"] = totalRecord;
            ViewData["Total"] = totalRecord;
            lstSPtmp = lstSPtmp.Take(10);

            List<SchoolProfileBO> lstSPTemp = lstSPtmp.ToList();
            List<int> lstSchoolID = lstSPTemp.Select(x => x.SchoolProfileID).ToList();
            
            
            List<int?> lstUserAcountIDLock1 = lstLI.Select(p => p.UserLock1ID).Distinct().ToList();
            List<int?> lstUserAcountIDLock2= lstLI.Select(p => p.UserLock2ID).Distinct().ToList();
            List<int?> lstUserAcountIDUnLock1 = lstLI.Select(p => p.UserUnlock1ID).Distinct().ToList();
            List<int?> lstUserAcountIDUnLock2 = lstLI.Select(p => p.UserUnlock2ID).Distinct().ToList();

            List<int?> lstUserAccountID = lstUserAcountIDLock1.Union(lstUserAcountIDLock2).Union(lstUserAcountIDUnLock1).Union(lstUserAcountIDUnLock2).Distinct().ToList();

            List<UserAccount> lstSupervisingDept = UserAccountBusiness.All.Where(x => lstUserAccountID.Contains(x.UserAccountID) && x.IsActive == true).ToList();
            List<FillingLockViewModel> LstSchool = new List<FillingLockViewModel>();
            FillingLockViewModel objSchool = null;
            
            #region temp
            for (int i = 0; i < lstSPTemp.Count(); i++)
            {
                objSchool = new FillingLockViewModel();
                objSchool.SchoolID = lstSPTemp[i].SchoolProfileID;
                objSchool.SchoolCode = lstSPTemp[i].SchoolCode;
                objSchool.SchoolName = lstSPTemp[i].SchoolName;
                objSchool.AppliLevelID = lstSPTemp[i].EducationGrade;
                objSchool.AcademicYearID = lstSPTemp[i].AcademicYearID;
                objSchool.StatusHK1 = "";
                objSchool.StatusHK2 = "";
                objSchool.SuperID = Super.AdminID; //so 
                objSchool.C1HK1 = 0;
                objSchool.C2HK1 = 0;
                objSchool.C3HK1 = 0;
                objSchool.C1HK2 = 0;
                objSchool.C2HK2 = 0;
                objSchool.C3HK2 = 0;

                LstSchool.Add(objSchool);
            }
            #endregion
            #region xet du lieu fill vao grid
            for (int i = 0; i < LstSchool.Count(); i++)
            {
                List<LockInputSupervisingDept> lstLock = new List<LockInputSupervisingDept>();
                lstLock = lstLI.Where(x => x.SchoolID == LstSchool[i].SchoolID && x.AcademicYearID == LstSchool[i].AcademicYearID).OrderBy(x => x.AppliedLevelID).ToList();
                if (lstLock.Count() != 0)
                {
                    for (int j = 0; j < lstLock.Count; j++)
                    {
                        int? SuperId1 = lstLock[j].UserLock1ID;
                        int? SuperId2 = lstLock[j].UserLock2ID;
                        int? SuperUnId1 = lstLock[j].UserUnlock1ID;
                        int? SuperUnId2 = lstLock[j].UserUnlock2ID;
                        List<string> NameSup1 = lstSupervisingDept.Where(x => x.UserAccountID == SuperId1).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSup2 = lstSupervisingDept.Where(x => x.UserAccountID == SuperId2).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSupUn1 = lstSupervisingDept.Where(x => x.UserAccountID == SuperUnId1).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSupUn2 = lstSupervisingDept.Where(x => x.UserAccountID == SuperUnId2).Select(x => x.aspnet_Users.UserName).ToList();

                        // cap 1
                        if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC1HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC1HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK1 = 1;
                            LstSchool[i].C1HK2 = 1;
                            LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC1HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK1 = 1;
                            LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            
                            if (lstLock[j].UserUnlock2ID != null)
                            {
                                LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC1HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK2 = 1;                            
                            LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C1HK1 = 0;
                            LstSchool[i].C1HK2 = 0;
                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC1HK2 = "Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }

                        // cap 2
                        if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC2HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC2HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK1 = 1;
                            LstSchool[i].C2HK2 = 1;
                            LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC2HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK1 = 1;
                            LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock2ID != null)
                            {
                                LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }

                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC2HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK2 = 1;
                            LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C2HK1 = 0;
                            LstSchool[i].C2HK2 = 0;

                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }

                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }


                        // cap 3
                        if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC3HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC3HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK1 = 1;
                            LstSchool[i].C3HK2 = 1;
                            LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC3HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK1 = 1;
                            LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            if (lstLock[j].UserUnlock2ID != null)
                            {
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                        }
                            
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC3HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK2 = 1;
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C3HK1 = 0;
                            LstSchool[i].C3HK2 = 0;

                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }

                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            if (_globalInfo.IsSuperVisingDeptRole == true)
            {
                for (int i = 0; i < LstSchool.Count; i++)
                {
                    LstSchool[i].checkSoC1HK1 = 1;
                    LstSchool[i].checkSoC1HK2 = 1;
                    LstSchool[i].checkSoC2HK1 = 1;
                    LstSchool[i].checkSoC2HK2 = 1;
                    LstSchool[i].checkSoC3HK1 = 1;
                    LstSchool[i].checkSoC3HK2 = 1;
                }
            }
            listResult = LstSchool.ToList();
            ViewData["checkIsENABLE"] = LstSchool.Count() == 0 ? true : false;
            ViewData[FillingLockConstants.YEAR] = frm.StudyYear;
            ViewData[FillingLockConstants.SCHOOL_LEVEL] = frm.SchoolLevel;
            ViewData[FillingLockConstants.DISTRICT] = frm.District;
            ViewData[FillingLockConstants.SCHOOL_NAME] = frm.SchoolName;
            ViewData[FillingLockConstants.ADMIN_NAME] = frm.AdminName;
            ViewData[FillingLockConstants.STATUS_LOCK] = frm.Status;
            ViewData[FillingLockConstants.SEMESTER_LOCK] = frm.Semester;
            return PartialView("_List", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(SearchViewModel form, GridCommand command, FormCollection frm)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            Utils.Utils.TrimObject(form);

            List<FillingLockViewModel> listResult;
            string chkHK1 = frm["checkFirst"];
            if (chkHK1 == "0")
            {
                listResult = new List<FillingLockViewModel>();

                return View(new GridModel<FillingLockViewModel>
                {
                    Total = 0,
                    Data = listResult
                });
            }

            // So?
            SupervisingDept Super = SupervisingDeptBusiness.All.Where(x => x.ProvinceID == _globalInfo.ProvinceID && x.SupervisingDeptID == _globalInfo.SupervisingDeptID).FirstOrDefault();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = form.SchoolLevel;
            SearchInfo["DistrictID"] = form.District;
            SearchInfo["SchoolName"] = form.SchoolName;
            SearchInfo["Administrator"] = form.AdminName;
            SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                           x.EducationGrade != 16 &&
                                                                                           x.EducationGrade != 24).OrderBy(o => o.SchoolName.ToLower());
            if (_globalInfo.IsSubSuperVisingDeptRole == true)
            {
                lstSP = lstSP.Where(x => (x.EducationGrade == 1 || x.EducationGrade == 2 || x.EducationGrade == 3) && x.SupervisingDeptID == _globalInfo.SupervisingDeptID);
            }         
            int academic = Int32.Parse(form.StudyYear);
            IQueryable<SchoolProfileBO> lstSPtmp = (from sp in lstSP
                                                    join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                    where sp.IsActive == true && ac.IsActive == true &&
                                                        sp.IsActive == true && ac.Year == academic &&
                                                        sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                    select new SchoolProfileBO
                                                    {
                                                        SchoolProfileID = sp.SchoolProfileID,
                                                        SchoolCode = sp.SchoolCode,
                                                        SchoolName = sp.SchoolName,
                                                        AcademicYearID = ac.AcademicYearID,
                                                        EducationGrade = sp.EducationGrade
                                                    }).OrderBy(o => o.SchoolName.ToLower());

            List<int> lstSchoolIDtmp = lstSPtmp.Select(p => p.SchoolProfileID).Distinct().ToList();
            List<int> lstAcademicYearID = lstSPtmp.Select(p => p.AcademicYearID).Distinct().ToList();

            List<LockInputSupervisingDept> lstLI = LockInputSupervisingDeptBusiness.All.Where(x => lstSchoolIDtmp.Contains(x.SchoolID) && lstAcademicYearID.Contains(x.AcademicYearID)).ToList();
            List<int> lstSchoolLockID = lstLI.Select(p => p.SchoolID).Distinct().ToList();

            List<int> lstIDtmp = new List<int>();
            int schoolLevel = !string.IsNullOrEmpty(form.SchoolLevel) ? int.Parse(form.SchoolLevel) : 0;

            #region xet du lieu tim kiem
            if (form.Status == "1")
            {
                if (form.Semester == "1")
                { //x.LockTitle.Contains("HK1")
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock1ID.HasValue && (x.LockTitle == "HK1" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
                else if (form.Semester == "2")
                {
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock2ID.HasValue && (x.LockTitle == "HK2" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
                else if (form.Semester == null)
                {
                    lstIDtmp = lstLI.Where(x => /*(x.AppliedLevelID == schoolLevel || schoolLevel == 0) &&*/ ((x.UserLock2ID.HasValue || x.UserLock1ID.HasValue) && !string.IsNullOrEmpty(x.LockTitle))).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                }
            }
            else if (form.Status == "2")
            {
                if (form.Semester == "1")
                {
                    lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0)) && (x.LockTitle != "HK1") && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));
                }
                else if (form.Semester == "2")
                {
                    lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0)) && x.LockTitle != "HK2" && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));

                }
                else if (form.Semester == null)
                {
                    lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && (string.IsNullOrEmpty(x.LockTitle) || x.LockTitle == "HK1" || x.LockTitle == "HK2")).Select(x => x.SchoolID).Distinct().ToList();
                    lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));

                }
            }
            #endregion

            int totalRecord = lstSPtmp.Count();
            ViewData["count"] = totalRecord;
            List<SchoolProfileBO> lstSPTemp = lstSPtmp.Skip((currentPage - 1) * pageSize).Take(10).ToList();
            List<int> lstSchoolID = lstSPTemp.Select(x => x.SchoolProfileID).ToList();
            List<FillingLockViewModel> LstSchool = new List<FillingLockViewModel>();
            FillingLockViewModel objSchool = null;
            
            List<int?> lstUserAcountIDLock1 = lstLI.Select(p => p.UserLock1ID).Distinct().ToList();
            List<int?> lstUserAcountIDLock2 = lstLI.Select(p => p.UserLock2ID).Distinct().ToList();
            List<int?> lstUserAcountIDUnLock1 = lstLI.Select(p => p.UserUnlock1ID).Distinct().ToList();
            List<int?> lstUserAcountIDUnLock2 = lstLI.Select(p => p.UserUnlock2ID).Distinct().ToList();

            List<int?> lstUserAccountID = lstUserAcountIDLock1.Union(lstUserAcountIDLock2).Union(lstUserAcountIDUnLock1).Union(lstUserAcountIDUnLock2).Distinct().ToList();
            List<UserAccount> lstSupervisingDept = UserAccountBusiness.All.Where(x => lstUserAccountID.Contains(x.UserAccountID) && x.IsActive == true).ToList();
            #region temp
            for (int i = 0; i < lstSPTemp.Count(); i++)
            {
                objSchool = new FillingLockViewModel();
                objSchool.SchoolID = lstSPTemp[i].SchoolProfileID;
                objSchool.SchoolCode = lstSPTemp[i].SchoolCode;
                objSchool.SchoolName = lstSPTemp[i].SchoolName;
                objSchool.AppliLevelID = lstSPTemp[i].EducationGrade;
                objSchool.AcademicYearID = lstSPTemp[i].AcademicYearID;
                objSchool.StatusHK1 = "";
                objSchool.StatusHK2 = "";
                objSchool.SuperID = Super.AdminID; //so 
                objSchool.C1HK1 = 0;
                objSchool.C2HK1 = 0;
                objSchool.C3HK1 = 0;
                objSchool.C1HK2 = 0;
                objSchool.C2HK2 = 0;
                objSchool.C3HK2 = 0;
                LstSchool.Add(objSchool);
            }
            #endregion
            #region xet du lieu fill vao grid
            for (int i = 0; i < LstSchool.Count(); i++)
            {
                List<LockInputSupervisingDept> lstLock = new List<LockInputSupervisingDept>();
                lstLock = lstLI.Where(x => x.SchoolID == LstSchool[i].SchoolID && x.AcademicYearID == LstSchool[i].AcademicYearID).OrderBy(x => x.AppliedLevelID).ToList();
                if (lstLock.Count() != 0)
                {
                    for (int j = 0; j < lstLock.Count; j++)
                    {
                        int? SuperId1 = lstLock[j].UserLock1ID;
                        int? SuperId2 = lstLock[j].UserLock2ID;
                        int? SuperUnId1 = lstLock[j].UserUnlock1ID;
                        int? SuperUnId2 = lstLock[j].UserUnlock2ID;
                        List<string> NameSup1 = lstSupervisingDept.Where(x => x.UserAccountID == SuperId1).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSup2 = lstSupervisingDept.Where(x => x.UserAccountID == SuperId2).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSupUn1 = lstSupervisingDept.Where(x => x.UserAccountID == SuperUnId1).Select(x => x.aspnet_Users.UserName).ToList();
                        List<string> NameSupUn2 = lstSupervisingDept.Where(x => x.UserAccountID == SuperUnId2).Select(x => x.aspnet_Users.UserName).ToList();

                        // cap 1
                        if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC1HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC1HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK1 = 1;
                            LstSchool[i].C1HK2 = 1;
                            LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC1HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK1 = 1;                           
                            LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            
                            if (lstLock[j].UserUnlock2ID != null)
                            {
                                LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }                           
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC1HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C1HK2 = 1;
                            LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            
                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 1 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C1HK1 = 0;
                            LstSchool[i].C1HK2 = 0;
                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC1HK1 = "- Cấp 1 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC1HK2 = "- Cấp 1 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC1HK2 = "Cấp 1 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }

                        // cap 2
                        if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC2HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC2HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK1 = 1;
                            LstSchool[i].C2HK2 = 1;
                            LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC2HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK1 = 1;
                            LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock2ID != null)
                            {
                                LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC2HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C2HK2 = 1;
                            LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");

                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 2 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C2HK1 = 0;
                            LstSchool[i].C2HK2 = 0;

                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }

                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC2HK2 = "- Cấp 2 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC2HK1 = "- Cấp 2 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }


                        // cap 3
                        if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK1,HK2")
                        {
                            LstSchool[i].checkSoC3HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].checkSoC3HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK1 = 1;
                            LstSchool[i].C3HK2 = 1;
                            LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK1")
                        {
                            LstSchool[i].checkSoC3HK1 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId1.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK1 = 1;
                            LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            if (lstLock[j].UserUnlock2ID != null)
                            {
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                        }
                            
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == "HK2")
                        {
                            LstSchool[i].checkSoC3HK2 = GetHierachyLevelIDByUserAccountID(Int32.Parse(SuperId2.ToString())) == 5 ? 1 : 2;
                            LstSchool[i].C3HK2 = 1;
                            LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                            if (lstLock[j].UserUnlock1ID != null)
                            {
                                LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                        }
                        else if (lstLock[j].AppliedLevelID == 3 && lstLock[j].LockTitle == null)
                        {
                            LstSchool[i].C3HK1 = 0;
                            LstSchool[i].C3HK2 = 0;

                            if (SuperUnId1 != null)
                            {
                                LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSupUn1[0] + " lúc " + lstLock[j].ModifiedLock1Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock1Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock1Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId1 != null)
                                {
                                    LstSchool[i].StatusC3HK1 = "- Cấp 3 bởi " + NameSup1[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }

                            if (SuperUnId2 != null)
                            {
                                LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSupUn2[0] + " lúc " + lstLock[j].ModifiedLock2Date.Value.Hour.ToString("00") + ":" + lstLock[j].ModifiedLock2Date.Value.Minute.ToString("00") + " " + lstLock[j].ModifiedLock2Date.Value.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                if (SuperId2 != null)
                                {
                                    LstSchool[i].StatusC3HK2 = "- Cấp 3 bởi " + NameSup2[0] + " lúc " + lstLock[j].CreateDate.Hour.ToString("00") + ":" + lstLock[j].CreateDate.Minute.ToString("00") + " " + lstLock[j].CreateDate.ToString("dd/MM/yyyy");
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            if (_globalInfo.IsSuperVisingDeptRole == true)
            {
                for (int i = 0; i < LstSchool.Count; i++)
                {
                    LstSchool[i].checkSoC1HK1 = 1;
                    LstSchool[i].checkSoC1HK2 = 1;
                    LstSchool[i].checkSoC2HK1 = 1;
                    LstSchool[i].checkSoC2HK2 = 1;
                    LstSchool[i].checkSoC3HK1 = 1;
                    LstSchool[i].checkSoC3HK2 = 1;
                }
            }
            
            ViewData["checkIsENABLE"] = LstSchool.Count() == 0 ? true : false;
            listResult = LstSchool.ToList();

            return View(new GridModel<FillingLockViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        public ActionResult Save(FormCollection fm)
        {
            #region Save
            string arrSchoolId = !string.IsNullOrEmpty(fm["arrSchoolId"]) ? fm["arrSchoolId"] : "";
            string arrC1HK1 = !string.IsNullOrEmpty(fm["arrC1HK1"]) ? fm["arrC1HK1"] : "";
            string arrC2HK1 = !string.IsNullOrEmpty(fm["arrC2HK1"]) ? fm["arrC2HK1"] : "";
            string arrC3HK1 = !string.IsNullOrEmpty(fm["arrC3HK1"]) ? fm["arrC3HK1"] : "";
            string arrC1HK2 = !string.IsNullOrEmpty(fm["arrC1HK2"]) ? fm["arrC1HK2"] : "";
            string arrC2HK2 = !string.IsNullOrEmpty(fm["arrC2HK2"]) ? fm["arrC2HK2"] : "";
            string arrC3HK2 = !string.IsNullOrEmpty(fm["arrC3HK2"]) ? fm["arrC3HK2"] : "";
            string arrAppliedLevelID = !string.IsNullOrEmpty(fm["arrAppliedLevelID"]) ? fm["arrAppliedLevelID"] : "";
            string arrAcademicYearID = !string.IsNullOrEmpty(fm["arrAcademicYearID"]) ? fm["arrAcademicYearID"] : "";
            string chkAppli = fm["checkChangeSubjectIncrease"];
            string chkHK1 = fm["chkHK1"];
            string chkHK2 = fm["chkHK2"];
            string chkC1 = fm["chkC1"];
            string chkC2 = fm["chkC2"];
            string chkC3 = fm["chkC3"];
            int Year = !string.IsNullOrEmpty(fm["Year"]) ? int.Parse(fm["Year"]) : 0;
            #region khong nhan Ap dung
            if (chkAppli == null)
            {
                List<int> lstSchoolID = arrSchoolId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<string> lstC1HK1 = arrC1HK1.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstC2HK1 = arrC2HK1.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstC3HK1 = arrC3HK1.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstC1HK2 = arrC1HK2.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstC2HK2 = arrC2HK2.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstC3HK2 = arrC3HK2.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                List<string> lstAppliedLevelID = arrAppliedLevelID.Split(new string[] { ";" }, StringSplitOptions.None).Select(p => p).ToList();
                List<string> lstAcademicYearID = arrAcademicYearID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();

                List<LockInputSupervisingDept> lstViewModel = new List<LockInputSupervisingDept>();
                LockInputSupervisingDept objViewModel = null;
                List<SchoolProfileBO> lstSchoolProfile = (from sp in SchoolProfileBusiness.All
                                                          join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                          where ac.IsActive == true &&
                                                              sp.IsActive == true &&
                                                              sp.ProvinceID == _globalInfo.ProvinceID
                                                              && ac.Year == Year
                                                              && lstSchoolID.Contains(sp.SchoolProfileID)
                                                          select new SchoolProfileBO
                                                          {
                                                              SchoolName = sp.SchoolName,
                                                              SchoolProfileID = sp.SchoolProfileID,
                                                              AcademicYearID = ac.AcademicYearID,
                                                              EducationGrade = sp.EducationGrade
                                                          }).OrderBy(p=>p.SchoolName.ToLower()).ToList();
                List<LockInputSupervisingDept> lstLI = LockInputSupervisingDeptBusiness.All.Where(p => lstSchoolID.Contains(p.SchoolID)).ToList();
                SchoolProfileBO objSP = null;
                for (int i = 0; i < lstSchoolProfile.Count; i++)
                {
                    objSP = lstSchoolProfile[i];
                    int schoolID = objSP.SchoolProfileID;
                    int academicID = objSP.AcademicYearID;
                    List<LockInputSupervisingDept> a = lstLI.Where(x => x.SchoolID == schoolID && x.AcademicYearID == academicID).ToList();

                    // cap 1
                    if (lstC1HK1[i] != "0" && lstC1HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 1;
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        objViewModel.LockTitle = "HK1,HK2";
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC1HK1[i] != "0" && lstC1HK2[i] == "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 1;
                        objViewModel.LockTitle = "HK1";
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());

                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC1HK1[i] == "0" && lstC1HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 1;
                        objViewModel.LockTitle = "HK2";

                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC1HK1[i] == "0" && lstC1HK2[i] == "0")
                    {
                        objViewModel = null;
                        if (a.Where(x => x.AppliedLevelID == 1).Count() != 0)
                        {
                            objViewModel = new LockInputSupervisingDept();
                            objViewModel.SchoolID = schoolID;
                            objViewModel.AcademicYearID = academicID;
                            objViewModel.AppliedLevelID = 1;
                            objViewModel.LockTitle = "";
                        }
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }



                    // cap 2
                    if (lstC2HK1[i] != "0" && lstC2HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 2;
                        objViewModel.LockTitle = "HK1,HK2";
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC2HK1[i] != "0" && lstC2HK2[i] == "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 2;
                        objViewModel.LockTitle = "HK1";
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());

                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC2HK1[i] == "0" && lstC2HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 2;
                        objViewModel.LockTitle = "HK2";

                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC2HK1[i] == "0" && lstC2HK2[i] == "0")
                    {
                        objViewModel = null;

                        if (a.Where(x => x.AppliedLevelID == 2).Count() != 0)
                        {
                            objViewModel = new LockInputSupervisingDept();
                            objViewModel.SchoolID = schoolID;
                            objViewModel.AcademicYearID = academicID;
                            objViewModel.AppliedLevelID = 2;
                            objViewModel.LockTitle = "";
                        }
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }


                    // cap 3
                    if (lstC3HK1[i] != "0" && lstC3HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 3;
                        objViewModel.LockTitle = "HK1,HK2";
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC3HK1[i] != "0" && lstC3HK2[i] == "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 3;
                        objViewModel.LockTitle = "HK1";
                        objViewModel.UserLock1ID = Int32.Parse(_globalInfo.UserAccountID.ToString());

                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC3HK1[i] == "0" && lstC3HK2[i] != "0")
                    {
                        objViewModel = new LockInputSupervisingDept();
                        objViewModel.SchoolID = schoolID;
                        objViewModel.AcademicYearID = academicID;
                        objViewModel.AppliedLevelID = 3;
                        objViewModel.LockTitle = "HK2";

                        objViewModel.UserLock2ID = Int32.Parse(_globalInfo.UserAccountID.ToString());
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                    else if (lstC3HK1[i] == "0" && lstC3HK2[i] == "0")
                    {
                        objViewModel = null;
                        if (a.Where(x => x.AppliedLevelID == 3).Count() != 0)
                        {
                            objViewModel = new LockInputSupervisingDept();
                            objViewModel.SchoolID = schoolID;
                            objViewModel.AcademicYearID = academicID;
                            objViewModel.AppliedLevelID = 3;
                            objViewModel.LockTitle = "";
                        }
                        if (objViewModel != null)
                        {
                            lstViewModel.Add(objViewModel);
                        }
                    }
                }
                LockInputSupervisingDeptBusiness.InsertTemp(lstViewModel, Int32.Parse(_globalInfo.UserAccountID.ToString()));
                LockInputSupervisingDeptBusiness.Save();
            }
            #endregion

            else {

                int schoolLevel = !string.IsNullOrEmpty(fm["SchoolLevel"]) ? int.Parse(fm["SchoolLevel"]) : 0;
                int district = !string.IsNullOrEmpty(fm["District"]) ? int.Parse(fm["District"]) : 0;
                string schoolName = fm["SchoolName"];
                string AdminName = fm["AdminName"];
                string status = fm["Status"];
                string semester = fm["Semester"];
                
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AppliedLevel"] = schoolLevel;
                SearchInfo["DistrictID"] = district;
                SearchInfo["SchoolName"] = schoolName;
                SearchInfo["Administrator"] = AdminName;
                SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
                // tim theo nam
                IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade != 8 &&
                                                                                                    x.EducationGrade != 16 &&
                                                                                                    x.EducationGrade != 24);

                ViewData["isSub"] = false;
                if (_globalInfo.IsSubSuperVisingDeptRole == true)
                {
                    lstSP = lstSP.Where(x => (x.EducationGrade == 1 || x.EducationGrade == 2 || x.EducationGrade == 3) && x.SupervisingDeptID == _globalInfo.SupervisingDeptID);
                    ViewData["isSub"] = true;
                }

                IQueryable<SchoolProfileBO> lstSPtmp = (from sp in lstSP
                                                        join ac in AcademicYearBusiness.All on sp.SchoolProfileID equals ac.SchoolID
                                                        where sp.IsActive == true && ac.IsActive == true &&
                                                            sp.IsActive == true && ac.Year == Year &&
                                                            sp.ProvinceID == _globalInfo.ProvinceID && sp.ProvinceID == _globalInfo.ProvinceID
                                                        select new SchoolProfileBO
                                                        {
                                                            SchoolProfileID = sp.SchoolProfileID,
                                                            SchoolCode = sp.SchoolCode,
                                                            SchoolName = sp.SchoolName,
                                                            AcademicYearID = ac.AcademicYearID,
                                                            EducationGrade = sp.EducationGrade
                                                        }).OrderBy(o => o.SchoolName.ToLower());

                List<int> lstSchoolIDtmp = lstSPtmp.Select(p => p.SchoolProfileID).Distinct().ToList();
                List<int> lstAcademicYearID = lstSPtmp.Select(p => p.AcademicYearID).Distinct().ToList();

                List<LockInputSupervisingDept> lstLI = LockInputSupervisingDeptBusiness.All.Where(x => lstSchoolIDtmp.Contains(x.SchoolID) && lstAcademicYearID.Contains(x.AcademicYearID)).ToList();
                List<int> lstSchoolLockID = lstLI.Select(p => p.SchoolID).Distinct().ToList();

                List<int> lstIDtmp = new List<int>();
                #region xet du lieu tim kiem
                if (status == "1")
                {
                    if (semester == "1")
                    {
                        lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock1ID.HasValue && (x.LockTitle == "HK1" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                    }
                    else if (semester == "2")
                    {
                        lstIDtmp = lstLI.Where(x => (x.AppliedLevelID == schoolLevel || schoolLevel == 0) && x.UserLock2ID.HasValue && (x.LockTitle == "HK2" || x.LockTitle == "HK1,HK2")).Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                    }
                    else if (semester == null)
                    {
                        lstIDtmp = lstLI.Where(x => ((x.UserLock2ID.HasValue || x.UserLock1ID.HasValue) && !string.IsNullOrEmpty(x.LockTitle))).Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => lstSchoolLockID.Contains(p.SchoolProfileID) && lstIDtmp.Contains(p.SchoolProfileID));
                    }
                }
                else if (status == "2")
                {
                    if (semester == "1")
                    {
                        lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0)) && x.UserUnlock1ID.HasValue && (x.LockTitle != "HK1") && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));
                    }
                    else if (semester == "2")
                    {
                        lstIDtmp = lstLI.Where(x => ((x.AppliedLevelID == schoolLevel || schoolLevel == 0)) && x.UserUnlock2ID.HasValue && x.LockTitle != "HK2" && x.LockTitle != "HK1,HK2").Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));

                    }
                    else if (semester == null)
                    {
                        lstIDtmp = lstLI.Where(x => ((x.UserUnlock1ID.HasValue || x.UserUnlock2ID.HasValue) && string.IsNullOrEmpty(x.LockTitle))).Select(x => x.SchoolID).Distinct().ToList();
                        lstSPtmp = lstSPtmp.Where(p => !lstSchoolLockID.Contains(p.SchoolProfileID) || lstIDtmp.Contains(p.SchoolProfileID));

                    }
                }

                List<SchoolProfileBO> lstInsertAll = lstSPtmp.ToList();

                List<LockInputSupervisingDept> lstViewModel = new List<LockInputSupervisingDept>();
                LockInputSupervisingDept objViewModel = null;
                SchoolProfileBO objSPBO = null;
                for (int i = 0; i < lstInsertAll.Count; i++)
                {
                    objSPBO = lstInsertAll[i];
                    int schoolID = objSPBO.SchoolProfileID;
                    objViewModel = new LockInputSupervisingDept();
                    objViewModel.SchoolID = schoolID;
                    objViewModel.AcademicYearID = objSPBO.AcademicYearID;
                    //Grade cua truong
                    objViewModel.AppliedLevelID = objSPBO.EducationGrade;
                    objViewModel.CreateDate = DateTime.Now;

                    // xet TitleLock
                    if (chkHK1 == null && chkHK2 == null)
                    {                        
                        objViewModel.LockTitle = null;
                    }
                    else if (chkHK1 != null && chkHK2 != null)
                    {
                        objViewModel.UserLock1ID = _globalInfo.UserAccountID;
                        objViewModel.UserLock2ID = _globalInfo.UserAccountID;
                        objViewModel.LockTitle = "HK1,HK2";
                    }
                    else if (chkHK2 != null)
                    {
                        objViewModel.UserLock2ID = _globalInfo.UserAccountID;
                        objViewModel.LockTitle = "HK2";
                    }
                    else if (chkHK1 != null)
                    {
                        objViewModel.LockTitle = "HK1";
                        objViewModel.UserLock1ID = _globalInfo.UserAccountID;
                    }

                    //xet Applied
                  lstViewModel.Add(objViewModel);
                }

                string AppliedString = "";
                if (chkC1 == "1" && chkC2 == "2" && chkC3 == "3")
                   AppliedString = "1,2,3";
                else if (chkC1 == "1" && chkC2 == "2")
                   AppliedString = "1,2";
                else if (chkC2 == "2" && chkC3 == "3")
                   AppliedString = "2,3";
                else if (chkC1 == "1")
                   AppliedString = "1";
                else if (chkC3 == "3")
                   AppliedString = "3";
                else if (chkC2 == "2")
                   AppliedString = "2";

                if (_globalInfo.IsSuperVisingDeptRole == true)
                {
                    LockInputSupervisingDeptBusiness.InsertTempAll(lstViewModel, Int32.Parse(_globalInfo.UserAccountID.ToString()), AppliedString, chkHK1, chkHK2);
                    LockInputSupervisingDeptBusiness.Save();
                }
                else {
                    LockInputSupervisingDeptBusiness.InsertTempAllSubSuper(lstViewModel, Int32.Parse(_globalInfo.UserAccountID.ToString()), AppliedString, chkHK1, chkHK2);
                    LockInputSupervisingDeptBusiness.Save();
                }           
            }
            #endregion

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            #endregion
        }
    }
}
