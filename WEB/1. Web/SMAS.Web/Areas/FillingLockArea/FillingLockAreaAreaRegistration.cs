﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FillingLockArea
{
    public class FillingLockAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FillingLockArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FillingLockArea_default",
                "FillingLockArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
