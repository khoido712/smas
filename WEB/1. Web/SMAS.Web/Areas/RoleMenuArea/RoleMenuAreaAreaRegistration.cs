﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RoleMenuArea
{
    public class RoleMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RoleMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RoleMenuArea_default",
                "RoleMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
