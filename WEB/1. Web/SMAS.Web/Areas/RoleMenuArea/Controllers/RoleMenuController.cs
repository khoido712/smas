﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.RoleMenuArea.Controllers
{   
    public class RoleMenuController : BaseController
    {        
        private readonly IRoleMenuBusiness RoleMenuBusiness;
		
		public RoleMenuController (IRoleMenuBusiness rolemenuBusiness)
		{
			this.RoleMenuBusiness = rolemenuBusiness;
		}
		
		//
        // GET: /RoleMenu/

        public ViewResult Index()
        {
            
			return View(this.RoleMenuBusiness.All.ToList());
        }

        //
        // GET: /RoleMenu/Details/5

        public ViewResult Details(int id)
        {            
            RoleMenu rolemenu = this.RoleMenuBusiness.Find(id);
			return View(rolemenu);
        }

        //
        // GET: /RoleMenu/Create

        public ActionResult Create()
        {

            return View();
        } 

        //
        // POST: /RoleMenu/Create

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(RoleMenu rolemenu)
        {
            if (ModelState.IsValid)
            {				
				this.RoleMenuBusiness.Insert(rolemenu);
                return RedirectToAction("Index");  
            }


            return View(rolemenu);
        }
        
        //
        // GET: /RoleMenu/Edit/5
 
        public ActionResult Edit(int id)
        {
            RoleMenu rolemenu = this.RoleMenuBusiness.Find(id);

            return View(rolemenu);
        }

        //
        // POST: /RoleMenu/Edit/5

        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Edit(RoleMenu rolemenu)
        {
            if (ModelState.IsValid)
            {
				this.RoleMenuBusiness.Update(rolemenu);                
                return RedirectToAction("Index");
            }

            return View(rolemenu);
        }

        //
        // GET: /RoleMenu/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            RoleMenu rolemenu = this.RoleMenuBusiness.Find(id);
            return View(rolemenu);
        }

        //
        // POST: /RoleMenu/Delete/5

        [HttpPost, ActionName("Delete")]		

        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.RoleMenuBusiness.Delete(id);
            return RedirectToAction("Index");
        }


    }
}
