/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DishInspectionCostArea
{
    public class DishInspectionCostConstants
    {
        public const string LIST_DISHINSPECTION = "listDishInspection";
        public const string LIST_EATINGGROUP = "listEatingGroup";
        public const string LIST_OTHERSERVICE = "listOtherService";
        public const string MAX_PRICE = "maxPrice";
        public const string NUMBER_CHILD = "numberOfChildren";
        public const string COUNT_FOOD_MONEY = "countFoodMoney";
        public const string COUNT_SERVICE_MONEY = "countServiceMoney";
    }
}