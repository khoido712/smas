/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.DishInspectionCostArea.Models
{
    public class DishInspectionViewModel
    {
        public int FoodID { get; set; }
        public string FoodName { get; set; }
        public int MenuType { get; set; }

        public decimal SumWeight { get; set; }
        public decimal SumPrice { get; set; }
        public decimal TotalMoney { get; set; }

        public int EatingGroupID { get; set; }
        public string EatingGroupName { get; set; }

        // Danh sach chua thong tin con
        public List<DishInspectionViewModel> ListDishInspectionViewModel { get; set; }
    }
}


