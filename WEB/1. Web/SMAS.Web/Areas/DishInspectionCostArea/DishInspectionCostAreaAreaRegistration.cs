﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DishInspectionCostArea
{
    public class DishInspectionCostAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DishInspectionCostArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DishInspectionCostArea_default",
                "DishInspectionCostArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
