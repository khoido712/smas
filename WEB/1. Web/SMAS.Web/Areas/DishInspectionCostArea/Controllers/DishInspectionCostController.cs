/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DishInspectionCostArea.Models;
using SMAS.Business.BusinessObject;

using SMAS.Models.Models;
using System.IO;

namespace SMAS.Web.Areas.DishInspectionCostArea.Controllers
{
    public class DishInspectionCostController : BaseController
    {
        private readonly IDishInspectionBusiness DishInspectionBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IDailyDishCostBusiness DailyDishCostBusiness;
        private readonly IFoodCatBusiness FoodCatBusiness;
        private readonly GlobalInfo globalInfo = new GlobalInfo();

        public DishInspectionCostController(IDishInspectionBusiness dishinspectionBusiness, IEatingGroupBusiness eatingGroupBusiness,
            IDailyDishCostBusiness dailyDishCostBusiness, IFoodCatBusiness foodCatBusiness)
        {
            this.DishInspectionBusiness = dishinspectionBusiness;
            this.EatingGroupBusiness = eatingGroupBusiness;
            this.DailyDishCostBusiness = dailyDishCostBusiness;
            this.FoodCatBusiness = foodCatBusiness;
        }

        //
        // GET: /DishInspectionCost/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here
            SearchViewModel Model = new SearchViewModel();

            // Test
            List<int> listFood = new List<int>() { 1, 3, 2 };
            List<int> listType = new List<int>() { 1, 2, 3, 4, 5 };
            List<decimal> listW = new List<decimal>() { (decimal)679.33333333333333333333333333, 868, 0, (decimal) 134.6666666666667, 0 };
            List<decimal> listRes = FoodCatBusiness.CaculatorFoodToAdd(listFood, listType, listW, 20);

            Model.DishInspectionDate = DateTime.Now;
            // Tim kiem doi voi ngay hien tai
            this.SetViewData(Model.DishInspectionDate.Value);
            return View(Model);
        }

        //
        // GET: /DishInspectionCost/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            //add search info
            //
            if (frm.DishInspectionDate.HasValue)
            {
                this.SetViewData(frm.DishInspectionDate.Value);
            }

            //Get view data here

            return PartialView("_ListAll", frm);
        }

        /// <summary>
        /// Quanglm
        /// Thiet lap du lieu vao view
        /// </summary>
        /// <param name="date"></param>
        private void SetViewData(DateTime date)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int SchoolID = globalInfo.SchoolID.Value;
            SearchInfo["AcademicYearID"] = globalInfo.AcademicYearID.Value;
            List<EatingGroupBO> ListEatingGroup = EatingGroupBusiness.SearchBySchool(SchoolID, SearchInfo).OrderBy(p=>p.EatingGroupName).ToList();
            ViewData[DishInspectionCostConstants.LIST_EATINGGROUP] = ListEatingGroup;
            // Tinh tong so tre theo truong
            int NumberOfChildren = DishInspectionBusiness.GetNumberOfChildren(SchoolID, date);
            ViewData[DishInspectionCostConstants.NUMBER_CHILD] = NumberOfChildren;

            IEnumerable<DishInspectionViewModel> lstDishInspectionViewModel = this._Search(date, NumberOfChildren).OrderBy(p => p.EatingGroupName);
            ViewData[DishInspectionCostConstants.LIST_DISHINSPECTION] = lstDishInspectionViewModel;
            // Lay thong tin tien thuc pham
            decimal countFoodMoney = 0;
            foreach (DishInspectionViewModel item in lstDishInspectionViewModel)
            {
                countFoodMoney += item.TotalMoney;
            }
            ViewData[DishInspectionCostConstants.COUNT_FOOD_MONEY] = countFoodMoney;

            List<DishInspectionBO> ListOtherService = DishInspectionBusiness.GetListOtherService(SchoolID, date).ToList();
            ViewData[DishInspectionCostConstants.LIST_OTHERSERVICE] = ListOtherService;

            // Tien dich vu
            decimal countServiceMoney = 0;
            foreach (DishInspectionBO item in ListOtherService)
            {
                countServiceMoney += item.PriceService * NumberOfChildren;
            }
            ViewData[DishInspectionCostConstants.COUNT_SERVICE_MONEY] = countServiceMoney;

            int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(date, SchoolID);
            ViewData[DishInspectionCostConstants.MAX_PRICE] = DailyPrice;
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DishInspectionViewModel> _Search(DateTime date, int NumberOfChildren)
        {
            int SchoolID = globalInfo.SchoolID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = globalInfo.AcademicYearID.Value;

            List<DishInspectionBO> LstFood = DishInspectionBusiness.GetListFood(SchoolID, date).ToList();

            // Danh sach hien thi tren grid
            List<DishInspectionViewModel> ListDishInspectionViewModel = new List<DishInspectionViewModel>();
            foreach (DishInspectionBO DishInspectionBO in LstFood)
            {
                int FoodID = DishInspectionBO.FoodID;
                DishInspectionViewModel item = ListDishInspectionViewModel.Find(o => o.FoodID == FoodID);
                // Lay thong tin cho danh sach con
                DishInspectionViewModel child = new DishInspectionViewModel();
                child.EatingGroupID = DishInspectionBO.EatingGroupID;
                child.EatingGroupName = DishInspectionBO.EatingGroupName;
                child.MenuType = DishInspectionBO.MenuType;
                child.SumWeight = DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                child.SumPrice = DishInspectionBO.PricePerOnce;
                child.TotalMoney = DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                List<DishInspectionViewModel> ListChild = new List<DishInspectionViewModel>();
                if (item != null)
                {
                    ListChild = item.ListDishInspectionViewModel;
                    // Tinh tong khoi luong va thanh tien
                    item.SumWeight += child.SumWeight;
                    item.TotalMoney += DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * NumberOfChildren / 1000;
                }
                else
                {
                    // Lay thong tin ve thuc pham
                    item = new DishInspectionViewModel();
                    item.FoodID = DishInspectionBO.FoodID;
                    item.FoodName = DishInspectionBO.FoodName;
                    item.SumWeight = DishInspectionBO.Weight * DishInspectionBO.NumberOfChildren / 1000;
                    item.TotalMoney = DishInspectionBO.PricePerOnce * DishInspectionBO.Weight * NumberOfChildren / 1000;
                    ListDishInspectionViewModel.Add(item);
                }
                ListChild.Add(child);
                item.ListDishInspectionViewModel = ListChild;

            }

            return ListDishInspectionViewModel;
        }


        public FileResult ExportExcel(SearchViewModel frm)
        {

            GlobalInfo global = new GlobalInfo();
            DateTime reportDate = frm.DishInspectionDate.Value;
            Stream excel = DishInspectionBusiness.CreateReportDishInspectionCost(global.SchoolID.Value, global.AcademicYearID.Value, reportDate);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName TamTinhTienAn_[ReportDate]
            string ReportName = "TamTinhTienAn_[ReportDate].xls";
            string strDate = reportDate.ToShortDateString().Replace("/", "");

            ReportName = ReportName.Replace("[ReportDate]", strDate);
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);

            result.FileDownloadName = ReportName;

            return result;
        }

    }
}





