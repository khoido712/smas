﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTestArea
{
    public class ApprenticeshipTestAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipTestArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipTestArea_default",
                "ApprenticeshipTestArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
