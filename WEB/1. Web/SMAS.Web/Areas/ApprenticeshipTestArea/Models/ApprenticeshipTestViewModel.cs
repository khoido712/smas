/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ApprenticeshipTestArea.Models
{
    public class ApprenticeshipTestViewModel
    {
        public System.Int32 ApprenticeshipTrainingID { get; set; }
        public System.Nullable<System.Int32> ClassID { get; set; }
        public System.Nullable<System.Int32> ApprenticeshipClassID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Nullable<System.Int32> AcademicYearID { get; set; }
        public System.Int32 PupilID { get; set; }
        public System.Nullable<System.Int32> EducationLevelID { get; set; }
        public System.Int32 ApprenticeshipSubjectID { get; set; }
        public System.Nullable<System.Decimal> Result { get; set; }
        public System.String Description { get; set; }
        public System.Nullable<System.DateTime> CreatedDate { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Note { get; set; }

        public bool Pass { get; set; }

        public string Grank { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_FullName")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_PupilCode")]
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_Class")]
        public System.String ClassName { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_NPTSubject")]
        public System.String NPTSubject { get; set; }
        public int? GradeID { get; set; }

        [ResourceDisplayName("ApprenticeshipTest_Label_Class")]
        public System.String ApprenticeshipClassName { get; set; } 
        private decimal? _ActionMark;
        public Nullable<decimal> ActionMark
        {
            get
            {
                if (_ActionMark.HasValue)
                {
                    return Math.Round(_ActionMark.Value, 1);
                }
                return null;

            }
            set
            {
                _ActionMark = value;
            }
        }
        public string ActionMarkConvert
        {
            get
            {
                if (_ActionMark.HasValue)
                {
                    return Math.Round(_ActionMark.Value, 1).ToString();
                }
                return null;
            }
        }

        private decimal? _TheoryMark;
        public Nullable<decimal> TheoryMark
        {
            get
            {
                if (_TheoryMark.HasValue)
                {
                    return Math.Round(_TheoryMark.Value, 1);
                }
                return null;

            }
            set
            {
                _TheoryMark = value;
            }
        }
        public string TheoryMarkConvert
        {
            get
            {
                if (_TheoryMark.HasValue)
                {
                    return Math.Round(_TheoryMark.Value, 1).ToString();
                }
                return null;
            }
        }

        public decimal? TotalMark { get; set; }
        public string ExcellentOption { get; set; }
        public string GoodOption { get; set; }
        public string NormalOption { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
    }
}


