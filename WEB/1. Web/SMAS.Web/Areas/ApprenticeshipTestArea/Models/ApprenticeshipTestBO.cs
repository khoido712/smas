﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ApprenticeshipTestArea.Models
{
    public class ApprenticeshipTestBO
    {
        public int PupilID { get; set; }

        public int? AcademicYearID { get; set; }

        public int ClassID { get; set; }
       
        public string PupilCode { get; set; }

        public string FullName { get; set; }

        public string TheoryMark { get; set; }

        public string ActionMark { get; set; }

        public string Grade { get; set; }

        public string Grank { get; set; }

        public string birthdate { get; set; }

        public string birthplate { get; set; }

        public string Subject { get; set; }

        public string SchoolName { get; set; }

        public string Note { get; set; }

        public bool Pass { get; set; }
    }
}