﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ApprenticeshipTestArea.Models;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Web.Areas.ApprenticeshipTestArea.Controllers
{
    public class ApprenticeshipTestController : BaseController
    {
        private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        public string Excellent = Res.Get("Common_Label_GradeExcellent");
        public string Good = Res.Get("Common_Label_GradeGood");
        public string Normal = Res.Get("Common_Label_GradeNormal");

        public ApprenticeshipTestController(IApprenticeshipTrainingBusiness apprenticeshiptrainingBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness,
            IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness, IApprenticeshipClassBusiness ApprenticeshipClassBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IPupilProfileBusiness PupilProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ApprenticeshipTrainingBusiness = apprenticeshiptrainingBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.ApprenticeshipSubjectBusiness = ApprenticeshipSubjectBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }

        public ActionResult Index()
        {
            SearchViewModel abc = new SearchViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();

            // Lay thong tin nam hoc
            IQueryable<AcademicYear> IQAcademicYear = AcademicYearBusiness.SearchBySchool(globalInfo.SchoolID.Value).OrderByDescending(o => o.Year);
            SelectList ListAcademicYear = new SelectList((IEnumerable<AcademicYear>)IQAcademicYear.ToList(), "AcademicYearID", "DisplayTitle", globalInfo.AcademicYearID);
            ViewData[ApprenticeshipTestConstants.LIST_ACADEMICYEAR] = ListAcademicYear;

            // Lay du lieu lop nghe pho thong
            IQueryable<ApprenticeshipClass> IQApprenticeshipClass = ApprenticeshipClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo);
            SelectList ListApprenticeshipClass = new SelectList((IEnumerable<ApprenticeshipClass>)IQApprenticeshipClass.ToList(), "ApprenticeshipClassID", "ClassName");
            ViewData[ApprenticeshipTestConstants.LIST_CLASS] = ListApprenticeshipClass;
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            List<ApprenticeshipTestViewModel> paging = this._Search(frm);
            foreach (ApprenticeshipTestViewModel item in paging)
            {
                string expertInfo = "value=\"" + SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT.ToString() + "\"";
                string goodInfo = "value=\"" + SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD.ToString() + "\"";
                string normalInfo = "value=\"" + SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL.ToString() + "\""; ;
                if (item.GradeID.HasValue)
                {
                    item.ExcellentOption = expertInfo;
                    item.GoodOption = goodInfo;
                    item.NormalOption = normalInfo;
                    int gradeID = item.GradeID.Value;
                    string gradeInfo = "value=\"" + gradeID + "\" selected=\"selected\"";
                    if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT)
                    {
                        item.ExcellentOption = gradeInfo;
                    }
                    else if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD)
                    {
                        item.GoodOption = gradeInfo;
                    }
                    else if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL)
                    {
                        item.NormalOption = gradeInfo;
                    }
                }
            }
            ViewData[ApprenticeshipTestConstants.LIST_APPRENTICESHIPTRAINING] = paging;
            // Du lieu xep loai
            List<ComboObject> listGrade = new List<ComboObject>();
            listGrade.Add(new ComboObject("", Res.Get("SelectRK")));
            listGrade.Add(new ComboObject(SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT.ToString(), Res.Get("Common_Label_GradeExcellent")));
            listGrade.Add(new ComboObject(SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD.ToString(), Res.Get("Common_Label_GradeGood")));
            listGrade.Add(new ComboObject(SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL.ToString(), Res.Get("Common_Label_GradeNormal")));
            ViewData[ApprenticeshipTestConstants.LIST_GRADE] = listGrade;

            return PartialView("_List");
        }

        private List<ApprenticeshipTestViewModel> _Search(SearchViewModel frm)
        {
            if (frm == null)
            {
                frm = new SearchViewModel();
            }

            Utils.Utils.TrimObject(frm);
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            if (frm.ClassID.HasValue)
            {
                SearchInfo["ApprenticeshipClassID"] = frm.ClassID.Value;
                ViewData[ApprenticeshipTestConstants.IS_PAGEABLE] = false;
            }
            else
            {
                ViewData[ApprenticeshipTestConstants.IS_PAGEABLE] = true;
            }

            if (frm.AcademicYearID.HasValue)
            {
                SearchInfo["AcademicYearID"] = frm.AcademicYearID.Value;
            }

            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            //
            IQueryable<ApprenticeshipTraining> lstFirstApprenticeshipTraining = ApprenticeshipTrainingBusiness
                .SearchBySchool(globalInfo.SchoolID.Value, SearchInfo);

            // Lay danh sach tong ket diem
            if (globalInfo.SchoolID.HasValue)
            {
                SearchInfo["SchoolID"] = globalInfo.SchoolID.Value;
            }
            // Lay thong tin mon hoc
            if (frm.ClassID.HasValue)
            {
                ApprenticeshipClass ApprenticeshipClass = ApprenticeshipClassBusiness.Find(frm.ClassID.Value);
                if (ApprenticeshipClass != null && ApprenticeshipClass.ApprenticeshipSubject != null)
                {
                    int SubjectID = ApprenticeshipClassBusiness.Find(frm.ClassID).ApprenticeshipSubject.SubjectID;
                    SearchInfo["SubjectID "] = SubjectID;
                }
            }

            decimal NomalMark = SMAS.Business.Common.GlobalConstants.NORMAL_MARK;
            // Bo va them cac dieu kien ko tim kiem tuong ung
            SearchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            SearchInfo["SummedUpFromMark"] = NomalMark;
            SearchInfo.Remove("PupilCode");
            SearchInfo.Remove("FullName");
            // Lay dung theo diem ca nam
            IQueryable<SummedUpRecord> ListSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, SearchInfo)
                                                                                    .Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
            IEnumerable<ApprenticeshipTraining> LstApprenticeshipTraining = from at in lstFirstApprenticeshipTraining.AsEnumerable()
                                                                            join su in ListSummedUpRecord.AsEnumerable() on new { at.PupilID, at.ApprenticeshipSubject.SubjectID } equals new { su.PupilID, su.SubjectID } into g1
                                                                            from g2 in g1
                                                                            where g2.SummedUpMark.HasValue && g2.SummedUpMark >= NomalMark
                                                                            select at;

            List<ApprenticeshipTestViewModel> listModel = LstApprenticeshipTraining
                                                            .Select(o => new ApprenticeshipTestViewModel
                                                            {
                                                                PupilID = o.PupilID,
                                                                PupilCode = o.PupilCode,
                                                                FullName = o.PupilProfile.FullName,
                                                                BirthDate = o.PupilProfile.BirthDate,
                                                                BirthPlace = o.PupilProfile.BirthPlace,
                                                                ClassID = o.ClassID,
                                                                ClassName = o.ClassProfile.DisplayName,
                                                                NPTSubject = o.ApprenticeshipSubject.SubjectName,
                                                                TheoryMark = o.TheoryMark,
                                                                ActionMark = o.ActionMark,
                                                                GradeID = o.Ranking,
                                                                TotalMark = o.Result,
                                                                AcademicYearID = o.AcademicYearID,
                                                                ApprenticeshipSubjectID = o.ApprenticeshipSubjectID,
                                                                ApprenticeshipTrainingID = o.ApprenticeshipTrainingID,
                                                                ApprenticeshipClassID = o.ApprenticeShipClassID,
                                                                ApprenticeshipClassName = o.ApprenticeshipClass.ClassName
                                                            })
                                                            .ToList();
            return listModel;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create(int[] ArrCheckedPupilClass, int[] ArrApprenticeshipTrainingID, string[] ArrTheoryMark, string[] ArrActionMark, string[] ArrTotalMark, int?[] ArrGrade)
        {
            if (ArrCheckedPupilClass == null || ArrCheckedPupilClass.Length == 0)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ErrorCheckRow"), "error"));
            }
            List<ApprenticeshipTraining> ListApprenticeshipTraining = new List<ApprenticeshipTraining>();
            GlobalInfo globalInfo = new GlobalInfo();
            int len = ArrCheckedPupilClass.Length;
            int lenGrid = ArrApprenticeshipTrainingID.Length;
            // Danh sach hoc sinh trong lop nghe
            List<ApprenticeshipTraining> ListAssignedApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(globalInfo.SchoolID.Value,
                new Dictionary<string, object>() { { "AcademicYearID", globalInfo.AcademicYearID } })
                .Where(o => ArrCheckedPupilClass.Contains(o.ApprenticeshipTrainingID)).ToList();

            for (int i = 0; i < len; i++)
            {
                int ApprenticeshipTrainingID = ArrCheckedPupilClass[i];
                // Tim phan tu tuong ung trong danh sach Pupil truyen len 
                // De lay du lieu tu cac mang khac
                int pos = -1;
                for (int j = 0; j < lenGrid; j++)
                {
                    if (ArrApprenticeshipTrainingID[j] == ApprenticeshipTrainingID)
                    {
                        pos = j;
                        break;
                    }
                }
                if (pos > -1)
                {
                    string theoryMark = ArrTheoryMark[pos];
                    string actionMark = ArrActionMark[pos];
                    string totalMark = ArrTotalMark[pos];
                    int? grade = ArrGrade[pos];
                    if (theoryMark == null || actionMark == null || totalMark == null)
                    {
                        return Json(new JsonMessage(Res.Get("ApprenticeshipTest_Label_ErrInfo"), "error"));
                    }
                    ApprenticeshipTraining apprenticeshiptraining = ListAssignedApprenticeshipTraining.Find(o => o.ApprenticeshipTrainingID == ApprenticeshipTrainingID);
                    if (apprenticeshiptraining != null)
                    {
                        apprenticeshiptraining.AcademicYearID = globalInfo.AcademicYearID.Value;
                        apprenticeshiptraining.SchoolID = globalInfo.SchoolID.Value;
                        apprenticeshiptraining.Ranking = grade;
                        apprenticeshiptraining.TheoryMark = decimal.Parse(theoryMark);
                        apprenticeshiptraining.ActionMark = decimal.Parse(actionMark);
                        apprenticeshiptraining.Result = decimal.Parse(totalMark);
                        ListApprenticeshipTraining.Add(apprenticeshiptraining);
                    }
                }

            }
            if (ListApprenticeshipTraining.Count > 0)
            {
                this.ApprenticeshipTrainingBusiness.UpdateApprenticeshipTraining(ListApprenticeshipTraining);
                this.ApprenticeshipTrainingBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("ApprenticeshipTest_Label_AddNewMessage")));
        }

        #region Excel


        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int YearID)
        {
            string Error = "";
            string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = null;

            AcademicYear acaYear = AcademicYearBusiness.Find(YearID);

            try
            {
                sheet = oBook.GetSheet(1);
            }
            catch
            {
                Error = "Mẫu file excel không đúng";
                return Json(new { success = Error });
            }

            string SchoolName = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value).SchoolName;
            string TitleYear = (string)sheet.GetCellValue(3, 1);
            string TitleSchool = (string)sheet.GetCellValue(4, 1);

            if (TitleYear == null || TitleSchool == null)
            {
                Error = "Mẫu file excel không đúng";
                return Json(new { success = Error });
            }

            if (TitleSchool.ToUpper().Contains(SchoolName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = "Trường học không đúng";
                else Error = Error + " - Trường học không đúng";
            }

            if (TitleYear.Contains(DateTime.Now.Year.ToString()) == false)
            {
                if (Error == "")
                    Error = "Năm học không đúng";
                else Error = Error + " - Năm học không đúng";
            }

            if (Error != "")
            {
                return Json(new { success = Error });
            }

            List<ApprenticeshipTestBO> lstAppTraining = new List<ApprenticeshipTestBO>();
            for (int i = 8; i < 200; i++)
            {
                if (sheet.GetCellValue(i, 4) == null)
                {
                    break;
                }

                ApprenticeshipTestBO AppTraining = new ApprenticeshipTestBO();
                string PupilCode = sheet.GetCellValue(i, 3) == null ? null : sheet.GetCellValue(i, 3).ToString();
                string FullName = sheet.GetCellValue(i, 4) == null ? null : sheet.GetCellValue(i, 4).ToString();
                string birthdate = sheet.GetCellValue(i, 5) == null ? null : sheet.GetCellValue(i, 5).ToString();
                string birthplate = sheet.GetCellValue(i, 6) == null ? null : sheet.GetCellValue(i, 6).ToString();
                string SchoolPupil = sheet.GetCellValue(i, 7) == null ? null : sheet.GetCellValue(i, 7).ToString();
                string SujectNameExcel = sheet.GetCellValue(i, 8) == null ? null : sheet.GetCellValue(i, 8).ToString();
                string TheoryMark = sheet.GetCellValue(i, 9) == null ? null : sheet.GetCellValue(i, 9).ToString();
                string ActionMark = sheet.GetCellValue(i, 10) == null ? null : sheet.GetCellValue(i, 10).ToString();
                string Grade = sheet.GetCellValue(i, 11) == null ? null : sheet.GetCellValue(i, 11).ToString();
                string grank = sheet.GetCellValue(i, 12) == null ? null : sheet.GetCellValue(i, 12).ToString();

                AppTraining.PupilCode = PupilCode;
                AppTraining.FullName = FullName;
                AppTraining.TheoryMark = TheoryMark;
                AppTraining.ActionMark = ActionMark;
                AppTraining.Grade = Grade;
                AppTraining.Grank = grank;
                AppTraining.birthdate = birthdate;
                AppTraining.birthplate = birthplate;
                AppTraining.SchoolName = SchoolPupil;
                AppTraining.Subject = SujectNameExcel;
                AppTraining.AcademicYearID = acaYear.AcademicYearID;
                AppTraining.Pass = true;
                lstAppTraining.Add(AppTraining);
            }

            bool PASS = true;

            if (lstAppTraining.Count == 0)
            {
                Error = "Dữ liệu đầu vào không hợp lệ";
                return Json(new { success = Error });
            }

            var pupilOfSchool = PupilProfileBusiness.All.Where(o => o.CurrentAcademicYearID == acaYear.AcademicYearID && o.IsActive)
                                                    .Select(u => new { u.PupilProfileID, u.PupilCode, u.FullName })
                                                    .ToList();


            foreach (var item in lstAppTraining)
            {
                List<string> notes = new List<string>();
                var pp = pupilOfSchool.Where(o => o.PupilCode == item.PupilCode).SingleOrDefault();

                item.PupilID = pp != null ? pp.PupilProfileID : 0;

                if (pp == null)
                {
                    item.Pass = false;
                    notes.Add("Mã học sinh không hợp lệ");
                    PASS = false;
                }

                if (pp != null && !item.FullName.ToLower().Contains(pp.FullName.ToLower()))
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Họ và tên học sinh không hợp lệ");
                }

                DateTime date;
                if (DateTime.TryParse(item.birthdate, out date) == false)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Ngày sinh học sinh không đúng định dạng");
                }

                if (item.Grank != null && item.Grank.ToUpper() != Excellent.ToUpper() && item.Grank.ToUpper() != Good.ToUpper() && item.Grank.ToUpper() != Normal.ToUpper() && item.Grank != null)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Xếp loại không hợp lệ");
                }

                if (item.SchoolName != SchoolName && item.SchoolName != null)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Tên trường không đúng");
                }

                decimal theoryMark = 0;
                if (item.TheoryMark != null)
                {
                    if (decimal.TryParse(item.TheoryMark, out theoryMark) == false)
                    {
                        PASS = false;
                        item.Pass = false;
                        notes.Add("Điểm lý thuyết ko đúng định dạng");
                    }
                    else
                    {
                        item.TheoryMark = Math.Round(theoryMark, 1, MidpointRounding.AwayFromZero).ToString("0.#");
                    }
                }

                if (theoryMark < 0 || theoryMark > 10)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Điểm lý thuyết phải nằm trong khoảng 0 > 10");
                }

                decimal actionMark = 0;
                if (item.ActionMark != null)
                {
                    if (decimal.TryParse(item.ActionMark, out actionMark) == false)
                    {
                        PASS = false;
                        item.Pass = false;
                        notes.Add("Điểm thực hành ko đúng định dạng");
                    }
                    else
                    {
                        item.ActionMark = Math.Round(actionMark, 1, MidpointRounding.AwayFromZero).ToString("0.#");
                    }
                }

                if (actionMark < 0 || actionMark > 10)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Điểm thực hành phải nằm trong khoảng 0 > 10");
                }

                decimal grade = 0;
                if (item.Grade != null)
                {
                    if (decimal.TryParse(item.Grade, out grade) == false)
                    {
                        PASS = false;
                        item.Pass = false;
                        notes.Add("Điểm kết quả ko đúng định dạng");
                    }
                    else
                    {
                        item.Grade = Math.Round(grade, 1, MidpointRounding.AwayFromZero).ToString("0.#");
                    }
                }

                if (grade < 0 || grade > 10)
                {
                    PASS = false;
                    item.Pass = false;
                    notes.Add("Điểm kết quả phải nằm trong khoảng 0 > 10");
                }

                item.Note = string.Join(", ", notes);
            }
            Session["lstAppTraining"] = lstAppTraining;

            if (PASS == true)
            {
                //không có lỗi thì hệ thống thực hiện giống chức năng nhập điểm môn tính điểm
                ImportMark();
                return Json(new { success = 1 });
            }
            else
            {
                //Có lỗi thì hệ thống hiển thị màn hình Lựa chọn thao tác import
                return Json(new { success = 0 });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportMark()
        {
            GlobalInfo glo = new GlobalInfo();

            List<ApprenticeshipTestBO> lstAppTest = (List<ApprenticeshipTestBO>)Session["lstAppTraining"];

            if (lstAppTest == null) return Json(new JsonMessage(Res.Get("ApprenticeshipTest_Label_Error")));

            //Chi lay cac hoc sinh dat yeu cau va co diem
            lstAppTest = lstAppTest.Where(u => string.IsNullOrEmpty(u.Note) && u.Pass).ToList();

            List<ApprenticeshipTraining> ListApprenticeshipTraining = new List<ApprenticeshipTraining>();

            foreach (var item in lstAppTest)
            {
                int? grank = null;

                if (item.Grank != null && item.Grank.ToUpper() == Res.Get("Common_Label_GradeExcellent").ToUpper())
                    grank = 1;

                if (item.Grank != null && item.Grank.ToUpper() == Res.Get("Common_Label_GradeGood").ToUpper())
                    grank = 2;

                if (item.Grank != null && item.Grank.ToUpper() == Res.Get("Common_Label_GradeNormal").ToUpper())
                    grank = 3;

                //Moi hoc sinh chi hoc mot mon nghe nen chi co mot ban ghi cua hoc sinh trong bang ApprenticeshipTraining ung voi 1 nam hoc
                ApprenticeshipTraining AppTraining = ApprenticeshipTrainingBusiness.All.Where(o => o.AcademicYearID == item.AcademicYearID && o.PupilID == item.PupilID).SingleOrDefault();
                AppTraining.TheoryMark = item.TheoryMark != null ? Convert.ToDecimal(item.TheoryMark) : new Nullable<decimal>();
                AppTraining.ActionMark = item.ActionMark != null ? Convert.ToDecimal(item.ActionMark) : new Nullable<decimal>();
                AppTraining.Result = item.Grade != null ? Convert.ToDecimal(item.Grade) : new Nullable<decimal>();
                AppTraining.Ranking = grank;
                ListApprenticeshipTraining.Add(AppTraining);
            }

            if (ListApprenticeshipTraining.Count > 0)
            {
                this.ApprenticeshipTrainingBusiness.UpdateApprenticeshipTraining(ListApprenticeshipTraining);
                this.ApprenticeshipTrainingBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("ApprenticeshipTest_Label_AddNewMessage")));

        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            ImportMark();
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
        }

        public PartialViewResult ViewDataImportExcel()
        {
            List<ApprenticeshipTestBO> lstAppTest = (List<ApprenticeshipTestBO>)Session["lstAppTraining"];
            ViewData[ApprenticeshipTestConstants.LIST_APPRENTICESHIPTRAINING] = lstAppTest;
            return PartialView("ViewData");
        }


        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                return Json(new JsonMessage(""));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        public FileResult ExportExcel(int? academicYearID, int? apprenticeshipClassID, string pupilCode, string fullName)
        {
            SearchViewModel frm = new SearchViewModel();
            frm.AcademicYearID = academicYearID;
            frm.ClassID = apprenticeshipClassID;
            frm.FullName = fullName;
            frm.PupilCode = pupilCode;
            List<ApprenticeshipTestViewModel> listApprenticeshipTest = this._Search(frm);
            #region lay cac sheet ra
            GlobalInfo global = new GlobalInfo();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", ApprenticeshipTestConstants.TEMPLATE_EXCEL);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet templateSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "M7");
            #endregion

            #region fill
            IVTRange firstRange = templateSheet.GetRange("A8", "M8");
            IVTRange middleRange = templateSheet.GetRange("A9", "M9");
            IVTRange lastRange = templateSheet.GetRange("A10", "M10");
            IVTRange infoRange = templateSheet.GetRange("A12", "M13");
            int startDataRow = 8;

            Dictionary<string, object> dic = new Dictionary<string, object>();
            DateTime now = DateTime.Now;
            dic["MonthYear"] = now.Month + "/" + now.Year;
            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);
            string shoolName = school.SchoolName;
            dic["SchoolName"] = shoolName;
            string province = school.Province != null ? school.Province.ProvinceName + ", " : string.Empty;
            dic["ProvinceNameAndDate"] = province + "Ngày " + now.Day + " Tháng " + now.Month + " Năm " + now.Year;
            List<object> listData = new List<object>();
            for (int i = 0; i < listApprenticeshipTest.Count(); i++)
            {
                Dictionary<string, object> dicDetail = new Dictionary<string, object>();
                ApprenticeshipTestViewModel item = listApprenticeshipTest[i];
                dicDetail["STT"] = i + 1;
                dicDetail["PupilCode"] = item.PupilCode;
                dicDetail["FullName"] = item.FullName;
                dicDetail["BirthDate"] = item.BirthDate;
                dicDetail["BirthPlace"] = item.BirthPlace;
                dicDetail["SchoolName"] = shoolName;
                dicDetail["Subject"] = item.NPTSubject;
                dicDetail["LTMark"] = item.TheoryMarkConvert;
                dicDetail["THMark"] = item.ActionMarkConvert;
                string resultTest = string.Empty;
                int? gradeID = item.GradeID;
                if (gradeID.HasValue)
                {
                    if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_EXPERT)
                    {
                        resultTest = Res.Get("Common_Label_GradeExcellent");
                    }
                    else if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_GOOD)
                    {
                        resultTest = Res.Get("Common_Label_GradeGood");
                    }
                    else if (gradeID == SystemParamsInFile.APPRENTICESHIP_TRAINING_REANKING_NORMAL)
                    {
                        resultTest = Res.Get("Common_Label_GradeGood");
                    }
                }
                dicDetail["Result"] = item.TotalMark;
                dicDetail["Capacity"] = resultTest;
                dicDetail["Note"] = item.Note;
                listData.Add(dicDetail);
                if ((i + 1) % 5 == 0 || i == listApprenticeshipTest.Count - 1)
                {
                    sheet.CopyPaste(lastRange, startDataRow + i, 1);
                }
                else if ((i + 1) % 5 == 1)
                {
                    sheet.CopyPaste(firstRange, startDataRow + i, 1);
                }
                else
                {
                    sheet.CopyPaste(middleRange, startDataRow + i, 1);
                }
            }
            dic.Add("Rows", listData);
            // copy thong tin o cuoi
            sheet.CopyPaste(infoRange, startDataRow + listApprenticeshipTest.Count + 1, 1);

            sheet.FillVariableValue(dic);
            // copy lai style

            templateSheet.Delete();
            #endregion

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = "HS_THPT_DSThiSinhDuThiNPT_" + school.SchoolName;
            if (apprenticeshipClassID.HasValue)
            {
                fileName += "_" + ApprenticeshipClassBusiness.Find(apprenticeshipClassID.Value).ClassName;
            }
            fileName = ReportUtils.StripVNSign(fileName);
            result.FileDownloadName = fileName + ".xls";
            return result;
        }

        #endregion
    }
}





