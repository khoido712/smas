/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ApprenticeshipTestArea
{
    public class ApprenticeshipTestConstants
    {
        public const string LIST_APPRENTICESHIPTRAINING = "listApprenticeshipTraining";
        public const string LIST_PUPIL = "listPupil";
        public const string LIST_CLASS = "listClass";
        public const string LIST_GRADE = "listGrade";
        public const string LIST_ACADEMICYEAR = "listAcademicYear";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUMMEDUPRECORD = "listSummedUpRecord";
        public const string IS_PAGEABLE = "isPageAble";
        public const decimal MARK_NORMAL = 5;
        public const string TEMPLATE_EXCEL = "HS_THPT_DSThiSinhDuThiNPT.xls";
    }
}