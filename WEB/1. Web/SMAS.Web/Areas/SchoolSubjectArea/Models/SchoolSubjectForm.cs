using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SchoolSubjectArea.Models
{
    public class SchoolSubjectForm
    {
        public int SchoolSubjectID { get; set; }

        public int SubjectID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? FirstSemesterCoefficient { get; set; }

        public int? SecondSemesterCoefficient { get; set; }

        public int? AppliedType { get; set; }

        public int? IsCommenting { get; set; }

        public bool IsEditIsCommenting { get; set; }

        public string SchoolName { get; set; }

        public string SubjectName { get; set; }

        public string Abbreviation { get; set; }

        public string AppliedTypeName { get; set; }

        public string IsCommentingName { get; set; }

        public bool checkMark { get; set; }

        public bool IsChecked { get; set; }

        public bool IsNotChanged { get; set; }

        public bool IsSubjectWasInClass { get; set; }

        public string[] ArrAppliedType { get; set; }

        public string[] ArrCommiting { get; set; }

        public string[] ArrFirstSemester { get; set; }

        public string[] ArrSecondSemester { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekFirstSemester")]
        public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }

       
        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekSecondSemester")]
        public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekFirstSemester")]
        public string strSectionPerWeekFirstSemester { get; set; }


        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekSecondSemester")]
        public string strSectionPerWeekSecondSemester { get; set; }

        public Nullable<int> EducationLevelID { get; set; }
        public Nullable<int> OrderInSubject { get; set; }
    }
}
