﻿namespace SMAS.Web.Areas.SchoolSubjectArea
{
    public class SchoolSubjectConstants
    {
        public const string LS_SCHOOL_SUBJECT = "ListSchoolSubject";
        public const string LS_SCHOOL_SUBJECT_DB = "ListSchoolSubjectDb";
        public const string LS_SEMESTER_COEFFICIENT = "ListSemesterCoefficient";
        public const string LS_APPLIED_TYPE = "ListAppliedType";
        public const string LS_COMMITING = "ListCommenting";
        public const int APPLIED_TYPE_MARK_REQUIRED = 0;
        public const int APPLIED_TYPE_MARK_OPTION = 1;
        public const int APPLIED_TYPE_MARK_OPTAPLUS = 2;
        public const int COMMITING_COMMENTED = 0;
        public const int COMMITING_MARKED = 1;
        public const int COMMITING_COMMENTEDAMARKED = 2;
        public const string SEMESTER_COEFFICIENT = "0,1,2";
        public const string EDUCATION_LEVEL = "EDUCATION_LEVEL";
        public const string DEFAULT_EDUCATION_LEVEL = "DEFAULT_EDUCATION_LEVEL";
    }
}