﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SchoolSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.SchoolSubjectArea.Controllers
{
    public class SchoolSubjectController : BaseController
    {
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEducationProgramBusiness EducationProgramBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        public SchoolSubjectController(ISchoolSubjectBusiness SchoolSubjectBusiness, ISubjectCatBusiness SubjectCatBusiness, IClassProfileBusiness ClassProfileBusiness,
            IMarkRecordBusiness MarkRecordBusiness, IJudgeRecordBusiness JudgeRecordBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IProvinceSubjectBusiness ProvinceSubjectBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IEducationProgramBusiness EducationProgramBusiness
            , IClassSubjectBusiness ClassSubjectBusiness)
        {
            this.EducationProgramBusiness = EducationProgramBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ProvinceSubjectBusiness = ProvinceSubjectBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }

        //
        // GET: /SchoolSubject/

        #region quanglm

        public ViewResult Index()
        {
            SetViewDataPermission("SchoolSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            // Lay du lieu dau vao
            // PrepareComboData();
            GlobalInfo global = new GlobalInfo();
            var lsEducationLevel = EducationLevelBusiness.GetByGrade(global.AppliedLevel.Value);
            PrepareComboDataWithParams(lsEducationLevel.FirstOrDefault().EducationLevelID);
            return View();
        }

        public PartialViewResult Search()
        {
            //search
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("IsActive", true);
            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int AppliedLevel = globalInfo.AppliedLevel.HasValue ? globalInfo.AppliedLevel.Value : 0;
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            if (AppliedLevel > 0)
            {
                dic["AppliedLevel"] = AppliedLevel;
            }

            if (AcademicYearID > 0)
            {
                dic["AcademicYearID"] = AcademicYearID;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (academicYear != null)
                {
                    dic["Year"] = academicYear.Year;
                }
            }
            if (SchoolID > 0)
            {
                dic["SchoolID"] = SchoolID;
            }
            ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT] = this._SearchSchoolSubject(dic);
            return PartialView("_GridSchoolSubject");
        }

        public PartialViewResult CallSearch()
        {
            //search
            GlobalInfo globalInfo = new GlobalInfo();
            var lsEducationLevel = EducationLevelBusiness.GetByGrade(globalInfo.AppliedLevel.Value);
            ViewData[SchoolSubjectConstants.EDUCATION_LEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            return PartialView("_Search");
        }
        private void PrepareComboData()
        {
            List<ComboObject> listSemester = new List<ComboObject>();
            List<ComboObject> listAppliedType = new List<ComboObject>();
            List<ComboObject> listCommiting = new List<ComboObject>();
            string strSemesterCoeff = SchoolSubjectConstants.SEMESTER_COEFFICIENT;
            string[] arrSemesterCoeff = strSemesterCoeff.Split(',');
            foreach (string semesterCoeff in arrSemesterCoeff)
            {
                listSemester.Add(new ComboObject(semesterCoeff, semesterCoeff));
            }
            ViewData[SchoolSubjectConstants.LS_SEMESTER_COEFFICIENT] = listSemester;

            // Kieu mon
            listAppliedType.Add(new ComboObject(SchoolSubjectConstants.APPLIED_TYPE_MARK_REQUIRED.ToString(), Res.Get("SchoolSubject_Label_AppliedTypeRequiredMark")));
            listAppliedType.Add(new ComboObject(SchoolSubjectConstants.APPLIED_TYPE_MARK_OPTION.ToString(), Res.Get("SchoolSubject_Label_AppliedTypeOption")));
            listAppliedType.Add(new ComboObject(SchoolSubjectConstants.APPLIED_TYPE_MARK_OPTAPLUS.ToString(), Res.Get("SchoolSubject_Label_AppliedTypeOptionAndPlus")));
            ViewData[SchoolSubjectConstants.LS_APPLIED_TYPE] = listAppliedType;

            // Loai mon
            listCommiting.Add(new ComboObject(SchoolSubjectConstants.COMMITING_COMMENTED.ToString(), Res.Get("SchoolSubject_Label_CommitingCommented")));
            listCommiting.Add(new ComboObject(SchoolSubjectConstants.COMMITING_COMMENTEDAMARKED.ToString(), Res.Get("SchoolSubject_Label_CommitingCommentedMarked")));
            listCommiting.Add(new ComboObject(SchoolSubjectConstants.COMMITING_MARKED.ToString(), Res.Get("SchoolSubject_Label_CommitingMarked")));
            ViewData[SchoolSubjectConstants.LS_COMMITING] = listCommiting;

            GlobalInfo globalInfo = new GlobalInfo();
            var lsEducationLevel = EducationLevelBusiness.GetByGrade(globalInfo.AppliedLevel.Value);
            ViewData[SchoolSubjectConstants.EDUCATION_LEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            int defaultEducationLevel = lsEducationLevel.FirstOrDefault().EducationLevelID;
            ViewData[SchoolSubjectConstants.DEFAULT_EDUCATION_LEVEL] = defaultEducationLevel;

            //search
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("IsActive", true);
            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int AppliedLevel = globalInfo.AppliedLevel.HasValue ? globalInfo.AppliedLevel.Value : 0;
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            if (AppliedLevel > 0)
            {
                dic.Add("AppliedLevel", AppliedLevel);
            }
            if (AcademicYearID > 0)
            {
                dic.Add("AcademicYearID", AcademicYearID);
            }
            if (SchoolID > 0)
            {
                dic.Add("SchoolID", SchoolID);
            }
            dic["EducationLevelID"] = defaultEducationLevel;
            ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT] = this._SearchSchoolSubject(dic);
        }

        /// <summary>
        /// Tim kiem va thuc hien phan trang tung phan
        /// </summary>
        /// <param name="page"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        private List<SchoolSubjectForm> _SearchSchoolSubject(IDictionary<string, object> dic)
        {
            SetViewDataPermission("SchoolSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo global = new GlobalInfo();

            //lấy ra môn học của trường cho vào viewdata
            IQueryable<SchoolSubject> queryablelSubject = SchoolSubjectBusiness.Search(dic);
            var listSchoolSubject = queryablelSubject.ToList();
            ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT_DB] = listSchoolSubject;


            // Convert du lieu sang doi tuong SupervisingDeptForm
            IQueryable<SubjectCat> listSubjectCat = SubjectCatBusiness.Search(dic);
            IQueryable<SchoolSubjectForm> res = listSubjectCat.Select(o => new SchoolSubjectForm
            {
                SubjectID = o.SubjectCatID,
                SubjectName = o.DisplayName,
                Abbreviation = o.Abbreviation
            });
            var listSchoolSubjectForm = res;

            List<int> listSubjectID = listSchoolSubjectForm.Select(o => o.SubjectID).Distinct().ToList();
            //if (listSchoolSubject == null)
            //{
            //    listSchoolSubject = new List<SchoolSubject>();
            //}

            // Kiem tra xem mon hoc da co diem hay chua
            Dictionary<string, object> dicMark = new Dictionary<string, object>();

            dicMark.Add("AcademicYearID", global.AcademicYearID);
            dicMark.Add("SchoolID", global.SchoolID);
            //lấy ra bản ghi chứa điểm của môn học có trong listSubjectID
            IQueryable<MarkRecord> listMarkRecord = MarkRecordBusiness.SearchBySchool(global.SchoolID.Value, dic)
                .Where(o => listSubjectID.Contains(o.SubjectID));
            //lấy ra môn học nào có điểm rồi
            List<int> lstSubjectMarkID = listMarkRecord.Select(o => o.SubjectID).Distinct().ToList();
            //lấy ra bản ghi chứa điểm của môn học có trong listSubjectID
            IQueryable<JudgeRecord> listJudgeRecord = JudgeRecordBusiness.SearchBySchool(global.SchoolID.Value, dic)
                .Where(o => listSubjectID.Contains(o.SubjectID));
            //lấy ra môn học nào có điểm rồi
            List<int> lstSubjectJudgeID = listJudgeRecord.Select(o => o.SubjectID).Distinct().ToList();
            //duyệt từng đối tượng trong listSchoolSubjectForm
            foreach (SchoolSubjectForm item in listSchoolSubjectForm)
            {
                SchoolSubject schoolSubject = listSchoolSubject.Where(o => o.SubjectID == item.SubjectID).FirstOrDefault();
                item.IsNotChanged = false;
                if (schoolSubject != null)
                {
                    item.IsChecked = true;
                    item.IsCommenting = schoolSubject.IsCommenting;
                    item.AppliedType = schoolSubject.AppliedType;
                    item.FirstSemesterCoefficient = schoolSubject.FirstSemesterCoefficient;
                    item.SecondSemesterCoefficient = schoolSubject.SecondSemesterCoefficient;
                    if (MarkRecordBusiness.SearchBySchool(global.SchoolID.Value, dic).Count() > 0)
                    {
                        item.checkMark = true;
                    }
                    // Kiem tra neu da cham diem thi khong cho phep sua nua
                    if (lstSubjectMarkID.Contains(item.SubjectID) || lstSubjectJudgeID.Contains(item.SubjectID))
                    {
                        item.IsNotChanged = true;
                    }
                }
                else
                {
                    item.IsChecked = false;
                }
            }
            return listSchoolSubjectForm.ToList();
        }

        /// <summary>
        /// Kiem tra mon hoc cua truong trong nam hoc nay da duoc cham diem hay chua
        /// </summary>
        /// <param name="SubjectID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        private bool CheckMarkRecord(int SubjectID, int SchoolID, int AcademicYearID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (AcademicYearID > 0)
            {
                dic.Add("AcademicYearID", AcademicYearID);
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (academicYear != null)
                {
                    dic["Year"] = academicYear.Year;
                }
            }
            if (SchoolID > 0)
            {
                dic.Add("SchoolID", SchoolID);
            }
            if (SubjectID > 0)
            {
                dic.Add("SubjectID", SubjectID);
            }
            IQueryable<MarkRecord> listMarkRecord = MarkRecordBusiness.SearchBySchool(SchoolID, dic);
            if (listMarkRecord.Count() > 0)
            {
                return true;
            }
            IQueryable<JudgeRecord> listJudgeRecord = JudgeRecordBusiness.SearchBySchool(SchoolID, dic);
            if (listJudgeRecord.Count() > 0)
            {
                return true;
            }
            return false;
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxChangeGrid(int? EducationLevelID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("IsActive", true);
            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int AppliedLevel = globalInfo.AppliedLevel.HasValue ? globalInfo.AppliedLevel.Value : 0;
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            if (AppliedLevel > 0)
            {
                dic.Add("AppliedLevel", AppliedLevel);
            }

            if (AcademicYearID > 0)
            {
                dic.Add("AcademicYearID", AcademicYearID);
            }
            if (SchoolID > 0)
            {
                dic.Add("SchoolID", SchoolID);
            }
            if (EducationLevelID.HasValue)
            {
                dic["EducationLevelID"] = EducationLevelID;
            }

            //ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT] = this._SearchSchoolSubject(dic);
            //PrepareComboData();

            //dungnt77

            PrepareComboDataWithParams(EducationLevelID);
            return PartialView("_GridSchoolSubject");
        }

        #endregion quanglm

        #region dungnt77

        private void PrepareComboDataWithParams(int? EducationLevel)
        {
            EducationLevel = EducationLevel.HasValue ? EducationLevel.Value : (int)0;
            List<SchoolSubjectForm> lsSSF = new List<SchoolSubjectForm>();
            GlobalInfo globalInfo = new GlobalInfo();

            List<ComboObject> listSemester = new List<ComboObject>();
            string strSemesterCoeff = SchoolSubjectConstants.SEMESTER_COEFFICIENT;
            string[] arrSemesterCoeff = strSemesterCoeff.Split(',');
            foreach (string semesterCoeff in arrSemesterCoeff)
            {
                listSemester.Add(new ComboObject(semesterCoeff, semesterCoeff));
            }
            ViewData[SchoolSubjectConstants.LS_SEMESTER_COEFFICIENT] = listSemester;

            //cboEducationLevel: EducationLevelBusiness.GetByGrade(cboAppliedLevel.Value). Mặc định là khối đầu cấp

            var lsEducationLevel = EducationLevelBusiness.GetByGrade(globalInfo.AppliedLevel.Value);
            ViewData[SchoolSubjectConstants.EDUCATION_LEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            int defaultEducationLevel = lsEducationLevel.FirstOrDefault().EducationLevelID;
            ViewData[SchoolSubjectConstants.DEFAULT_EDUCATION_LEVEL] = defaultEducationLevel;

            //cboIsCommenting: CommonList.IsCommentingSubject
            ViewData[SchoolSubjectConstants.LS_COMMITING] = CommonList.IsCommentingSubject();

            //- cboAppliedType: CommonList.AppliedTypeSubject
            ViewData[SchoolSubjectConstants.LS_APPLIED_TYPE] = CommonList.AppliedTypeSubject();

            SchoolProfile sp = SchoolProfileBusiness.Find(globalInfo.SchoolID.Value);
            AcademicYear ay = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            var lstProvinceSubject = ProvinceSubjectBusiness.SearchByProvince(sp.ProvinceID.Value, new Dictionary<string, object>()
            {
                {"Year",ay.Year},
                {"EducationLevelID",EducationLevel}
                ,{"IsRegularEducation",sp.TrainingType!=null?sp.TrainingType.Resolution=="GDTX":false}
            }).Where(o => o.SubjectCat.IsActive == true);

            //Danh sach mon hoc 
            var lsSC = SubjectCatBusiness.Search(new Dictionary<string, object>()
                {
                    {"AppliedLevel",globalInfo.AppliedLevel}
                    ,{"IsActive",true}
                }).ToList();

            List<int> lstSubjectID = lsSC.Select(o => o.SubjectCatID).ToList();

            #region lay chuong trinh giao duc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic["SubjectCatID"] = SubjectID;
            dic["EducationLevelID"] = EducationLevel;

            var lsEducationProgram = EducationProgramBusiness.Search(dic).Where(o => lstSubjectID.Contains(o.SubjectCatID));

            string TrainningType = sp.TrainingType != null ? sp.TrainingType.Resolution : "";
            if (TrainningType == "GDTX")
            {
                lsEducationProgram = lsEducationProgram.Where(o => o.TrainingType.Resolution.Contains("GDTX"));
            }
            else
            {
                lsEducationProgram = lsEducationProgram.Where(o => o.TrainingTypeID == null);
            }
            List<EducationProgram> lstEducationProgram = lsEducationProgram.ToList();
            #endregion

            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(globalInfo.SchoolID.Value,
                   new Dictionary<string, object>()
                    {
                        {"AcademicYearID",globalInfo.AcademicYearID}
                        ,  {"EducationLevelID",EducationLevel}
                    }
                   ).ToList();

            #region So chua khai bao mon hoc
            if (lstProvinceSubject.Count() == 0)    //Nếu lstProvinceSubject.Count = 0 //Sở chưa khai báo môn học
            {
                //SubjectCatBusiness.Search và truyền vào UserInfo.AppliedLevel. Kết quả thu được lstSubjectCat


                //Kiểm tra từng phần tử trong lstSubjectCat  nếu IsEditIsCommentting = 1
                //thì khi đó mới hiển thị combobox kiểu môn. Nếu IsEditIsCommentting = 0 khi đó hiển thị kiểu môn là label.

                //Nếu tồn tại bản ghi hệ thống sẽ hiển thị check ở các bản ghi môn học tương ừng
                var lsSchoolSubject = SchoolSubjectBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",globalInfo.AcademicYearID}
                    ,  {"EducationLevelID",EducationLevel}
                }).Where(o => o.SubjectCat.IsActive == true).ToList();

                List<int> lsSubjectIDinListSchoolSubject = lsSchoolSubject.Select(o => o.SubjectID).ToList();
                List<SchoolSubject> lsss = lsSchoolSubject.Where(o => o.EducationLevelID == EducationLevel).ToList();
                foreach (var subjectCat in lsSC)
                {
                    SchoolSubjectForm ssf = new SchoolSubjectForm();

                    ssf.Abbreviation = subjectCat.Abbreviation;
                    ssf.AcademicYearID = globalInfo.AcademicYearID.Value;
                    ssf.EducationLevelID = EducationLevel;
                    ssf.IsEditIsCommenting = subjectCat.IsEditIsCommentting;
                    //ssf.IsNotChanged = IsSubjectHasMarkRecord(subjectCat.SubjectCatID);
                    ssf.SchoolID = globalInfo.SchoolID.Value;
                    ssf.SubjectID = subjectCat.SubjectCatID;
                    ssf.SubjectName = subjectCat.DisplayName;
                    ssf.OrderInSubject = subjectCat.OrderInSubject;
                    if (lsSubjectIDinListSchoolSubject.Where(o => o == subjectCat.SubjectCatID).Count() > 0 && lsss.Count > 0)
                    {
                        SchoolSubject ss = lsss.Where(o => o.SubjectID == subjectCat.SubjectCatID).FirstOrDefault();
                        ssf.AppliedType = ss.AppliedType;
                        //ssf.IsCommenting = subjectCat.IsEditIsCommentting ? ss.IsCommenting : subjectCat.IsCommenting;
                        ssf.IsCommenting = subjectCat.IsEditIsCommentting != true ? subjectCat.IsCommenting : ss != null ? ss.IsCommenting : subjectCat.IsCommenting;
                        ssf.FirstSemesterCoefficient = ss.FirstSemesterCoefficient;
                        ssf.SecondSemesterCoefficient = ss.SecondSemesterCoefficient;

                        ssf.SectionPerWeekFirstSemester = ss.SectionPerWeekFirstSemester;
                        ssf.SectionPerWeekSecondSemester = ss.SectionPerWeekSecondSemester;
                        ssf.IsChecked = true;
                    }
                    else
                    {
                        //decimal? HasEduProgram = NumberOfLessionInEducationProgram(subjectCat.SubjectCatID, EducationLevel);
                        EducationProgram edu = lstEducationProgram.Where(o => o.SubjectCatID == subjectCat.SubjectCatID).FirstOrDefault();
                        decimal? HasEduProgram = edu != null ? edu.NumberOfLesson : null;
                        ssf.AppliedType = 0;
                        //ssf.IsCommenting = subjectCat.IsEditIsCommentting ? (int)1 : subjectCat.IsCommenting;
                        ssf.IsCommenting = subjectCat.IsCommenting;
                        ssf.FirstSemesterCoefficient = 1;
                        ssf.SecondSemesterCoefficient = 1;
                        if (HasEduProgram.HasValue)
                        {
                            ssf.SectionPerWeekFirstSemester = HasEduProgram.Value;
                            ssf.SectionPerWeekSecondSemester = HasEduProgram.Value;
                        }
                        ssf.IsChecked = HasEduProgram.HasValue;

                    }
                    // Neu la so thap phan thi hien thi 1 chu so
                    // Neu la so nguyen hoac lon hon 10 (khong nhap duoc so thap phan lon hon 10 do tren giao dien chan)
                    // thi chi hien thi so nguyen
                    ssf.strSectionPerWeekFirstSemester = (ssf.SectionPerWeekFirstSemester.HasValue ? (ssf.SectionPerWeekFirstSemester.Value >= 10 || ssf.SectionPerWeekFirstSemester.Value % 1 == 0) ? string.Format("{0:0}", ssf.SectionPerWeekFirstSemester) : ssf.SectionPerWeekFirstSemester.Value.ToString("N1")
                        : "");
                    ssf.strSectionPerWeekSecondSemester = (ssf.SectionPerWeekSecondSemester.HasValue ? (ssf.SectionPerWeekSecondSemester.Value >= 10 || ssf.SectionPerWeekSecondSemester.Value % 1 == 0) ? string.Format("{0:0}", ssf.SectionPerWeekSecondSemester) : ssf.SectionPerWeekSecondSemester.Value.ToString("N1")
                        : "");
                    ssf.IsSubjectWasInClass = CheckSubjectWasInClass(ssf, listClassSubject);



                    lsSSF.Add(ssf);
                }
            }
            #endregion
            else       //Nếu lstProvinceSubject.Count > 0 ,Sở khai báo môn học
            #region So da khai bao mon hoc
            {
                //Nếu tồn tại bản ghi hệ thống sẽ hiển thị check ở các bản ghi môn học tương ừng
                List<SchoolSubject> lsSchoolSubject = SchoolSubjectBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",globalInfo.AcademicYearID}
                     ,{"EducationLevelID",EducationLevel}
                }).Where(o => o.SubjectCat.IsActive == true).ToList();

                var lsSubjectIDinListSchoolSubject = lsSchoolSubject.Select(o => o.SubjectID).ToList();
                var lsSubjectIDinProvinceSubject = lstProvinceSubject.Select(o => o.SubjectID).ToList();
                var lsAllSubjectID = lsSubjectIDinListSchoolSubject.Union(lsSubjectIDinProvinceSubject);
                List<ProvinceSubject> lsps = ProvinceSubjectBusiness.Search(new Dictionary<string, object>()
                        {
                            {"AppliedLevel",globalInfo.AppliedLevel}
                            ,{"EducationLevelID",EducationLevel}
                            ,{"ProvinceID",globalInfo.ProvinceID}
                        }).Where(o => o.SubjectCat.IsActive == true).ToList();
                foreach (var SubjectID in lsAllSubjectID)
                {
                    SchoolSubjectForm ssf = new SchoolSubjectForm();
                    bool ProvinceHasSubject = lsSubjectIDinProvinceSubject.Where(o => o == SubjectID).Count() > 0;
                    bool SchoolHasSubject = lsSubjectIDinListSchoolSubject.Where(o => o == SubjectID).Count() > 0;

                    /*có 3 trường hợp:
                     * -Sở có môn,trường ko có ->update theo sở
                     * -Trường có sở không ->update theo trường
                     * Cả 2 đều có -> update theo trường
                    */
                    if (SchoolHasSubject)    //trường có môn thì cứ update theo trường
                    {
                        SchoolSubject schoolSubject = lsSchoolSubject.Where(o => o.SubjectID == SubjectID).FirstOrDefault();

                        ssf.Abbreviation = schoolSubject.SubjectCat.Abbreviation;
                        ssf.AcademicYearID = globalInfo.AcademicYearID.Value;
                        ssf.EducationLevelID = EducationLevel.HasValue ? EducationLevel.Value : (int)0;
                        ssf.IsEditIsCommenting = schoolSubject.SubjectCat.IsEditIsCommentting;
                        //ssf.IsNotChanged = IsSubjectHasMarkRecord(schoolSubject.SubjectID);
                        ssf.SchoolID = globalInfo.SchoolID.Value;
                        ssf.SubjectID = schoolSubject.SubjectID;
                        ssf.SubjectName = schoolSubject.SubjectCat.DisplayName;
                        ssf.OrderInSubject = schoolSubject.SubjectCat.OrderInSubject;
                        ssf.AppliedType = schoolSubject.AppliedType;
                        ssf.IsCommenting = schoolSubject.IsCommenting;
                        ssf.FirstSemesterCoefficient = schoolSubject.FirstSemesterCoefficient;
                        ssf.SecondSemesterCoefficient = schoolSubject.SecondSemesterCoefficient;

                        ssf.SectionPerWeekFirstSemester = schoolSubject.SectionPerWeekFirstSemester;
                        ssf.SectionPerWeekSecondSemester = schoolSubject.SectionPerWeekSecondSemester;
                        ssf.IsChecked = true;


                    }
                    else //trường chưa khai báo thì cho update theo sở
                    {
                        ProvinceSubject ProvinceSubject = lsps.Where(o => o.SubjectID == SubjectID).FirstOrDefault();

                        ssf.Abbreviation = ProvinceSubject.SubjectCat.Abbreviation;
                        ssf.AcademicYearID = globalInfo.AcademicYearID.Value;
                        ssf.EducationLevelID = EducationLevel.HasValue ? EducationLevel.Value : (int)0;
                        ssf.IsEditIsCommenting = ProvinceSubject.SubjectCat.IsEditIsCommentting;
                        // ssf.IsNotChanged = IsSubjectHasMarkRecord(ProvinceSubject.SubjectID);
                        ssf.SchoolID = globalInfo.SchoolID.Value;
                        ssf.SubjectID = ProvinceSubject.SubjectID;
                        ssf.SubjectName = ProvinceSubject.SubjectCat.DisplayName;
                        ssf.OrderInSubject = ProvinceSubject.SubjectCat.OrderInSubject;

                        //decimal? HasEduProgram = NumberOfLessionInEducationProgram(ProvinceSubject.SubjectID, EducationLevel);
                        EducationProgram edu = lstEducationProgram.Where(o => o.SubjectCatID == ProvinceSubject.SubjectID).FirstOrDefault();
                        decimal? HasEduProgram = edu != null ? edu.NumberOfLesson : null;

                        ssf.AppliedType = ProvinceSubject.AppliedType;
                        ssf.IsCommenting = ProvinceSubject.IsCommenting;
                        //ssf.FirstSemesterCoefficient = 0;
                        //ssf.SecondSemesterCoefficient = 0;
                        ssf.FirstSemesterCoefficient = ProvinceSubject.Coefficient;
                        ssf.SecondSemesterCoefficient = ProvinceSubject.Coefficient;

                        //ssf.SectionPerWeekFirstSemester = HasEduProgram.HasValue ? HasEduProgram.Value : ProvinceSubject.SectionPerWeekFirstSemester;
                        ssf.SectionPerWeekFirstSemester = ProvinceSubject.SectionPerWeekFirstSemester.HasValue ? ProvinceSubject.SectionPerWeekFirstSemester : HasEduProgram;
                        //ssf.SectionPerWeekSecondSemester = HasEduProgram.HasValue ? HasEduProgram.Value : ProvinceSubject.SectionPerWeekSecondSemester;
                        ssf.SectionPerWeekSecondSemester = ProvinceSubject.SectionPerWeekSecondSemester.HasValue ? ProvinceSubject.SectionPerWeekSecondSemester : HasEduProgram;
                        ssf.IsChecked = false;
                    }

                    //ssf.strSectionPerWeekFirstSemester = (ssf.SectionPerWeekFirstSemester.HasValue ? string.Format("{0:0}", ssf.SectionPerWeekFirstSemester) : "");
                    ssf.strSectionPerWeekFirstSemester = (ssf.SectionPerWeekFirstSemester.HasValue ? (ssf.SectionPerWeekFirstSemester.Value >= 10 || ssf.SectionPerWeekFirstSemester.Value % 1 == 0) ? string.Format("{0:0}", ssf.SectionPerWeekFirstSemester) : ssf.SectionPerWeekFirstSemester.Value.ToString("N1") : "");
                    //ssf.strSectionPerWeekSecondSemester = (ssf.SectionPerWeekSecondSemester.HasValue ? string.Format("{0:0}", ssf.SectionPerWeekSecondSemester) : "");
                    ssf.strSectionPerWeekSecondSemester = (ssf.SectionPerWeekSecondSemester.HasValue ? (ssf.SectionPerWeekSecondSemester.Value >= 10 || ssf.SectionPerWeekSecondSemester.Value % 1 == 0) ? string.Format("{0:0}", ssf.SectionPerWeekSecondSemester) : ssf.SectionPerWeekSecondSemester.Value.ToString("N1") : "");
                    ssf.IsSubjectWasInClass = CheckSubjectWasInClass(ssf, listClassSubject);
                    lsSSF.Add(ssf);
                }
            }
            #endregion

            //lsSSF = lsSSF.OrderBy(o=>o.IsSubjectWasInClass).ThenBy(o => o.IsChecked).ThenBy(o => o.OrderInSubject).ToList();
            lsSSF = lsSSF.OrderByDescending(o => o.IsChecked).ThenByDescending(o => o.IsSubjectWasInClass).ThenBy(o => o.OrderInSubject).ToList();
            dic["AcademicYearID"] = globalInfo.AcademicYearID;
            dic["Year"] = ay.Year;
            List<int> lsmr = MarkRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Select(o => o.SubjectID).Distinct().ToList();
            List<int> lsjr = JudgeRecordBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Select(o => o.SubjectID).Distinct().ToList();
            foreach (SchoolSubjectForm sf in lsSSF)
            {

                if (lsmr.Where(o => o == sf.SubjectID).Count() > 0)
                {
                    sf.checkMark = true;
                }

                if (lsjr.Where(o => o == sf.SubjectID).Count() > 0)
                {
                    sf.checkMark = true;
                }
            }
            ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT] = lsSSF;
            ViewData[SchoolSubjectConstants.LS_SCHOOL_SUBJECT_DB] = lsSSF;
        }

        private bool IsSubjectHasMarkRecord(int SubjectID)
        {
            GlobalInfo global = new GlobalInfo();
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            bool hasMarkRecord = MarkRecordBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID}
                ,{"SubjectID",SubjectID}
                ,{"Year", academicYear.Year}
            }).Count() > 0;

            bool hasJudgeRecord = JudgeRecordBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID}
                ,{"SubjectID",SubjectID}
                ,{"Year", academicYear.Year}
            }).Count() > 0;

            return hasJudgeRecord || hasMarkRecord;
        }
        //ham ko dung nua do turning code
        private decimal? NumberOfLessionInEducationProgram(int SubjectID, int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SubjectCatID"] = SubjectID;
            dic["EducationLevelID"] = EducationLevelID;

            var lsEducationProgram = EducationProgramBusiness.Search(dic);

            string TrainningType = sp.TrainingType != null ? sp.TrainingType.Resolution : "";
            if (TrainningType == "GDTX")
            {
                lsEducationProgram = lsEducationProgram.Where(o => o.TrainingType.Resolution.Contains("GDTX"));
            }
            else
            {
                lsEducationProgram = lsEducationProgram.Where(o => o.TrainingTypeID == null);
            }

            if (lsEducationProgram.Count() > 0)
            {
                EducationProgram ep = lsEducationProgram.FirstOrDefault();
                return ep.NumberOfLesson;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult SetSchoolSubject(int[] HiddenSubjectID, int[] ArrSubjectID, string[] ArrAppliedType, string[] ArrCommenting,
            string[] ArrFirstSemester, string[] ArrSecondSemester, int? EducationLevel, string[] SectionPerWeekFirstSemester, string[] SectionPerWeekSecondSemester, int page = 1, string orderBy = "")
        {
            if (GetMenupermission("SchoolSubject", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }


            if (ArrSubjectID == null)
            {
                ArrSubjectID = new int[0];
            }

            List<int> lsCheckLocation = new List<int>();
            for (int i = 0; i < HiddenSubjectID.Count(); i++)
            {
                if (ArrSubjectID.Contains(HiddenSubjectID[i]))
                {
                    lsCheckLocation.Add(i);
                }
            }

            // Thuc hien gan mon hoc cho nha truong
            // Lay danh sach nhung mon hoc da duoc gan cho truong tu truoc
            Dictionary<string, object> dic = new Dictionary<string, object>();
            int AcademicYearID = _globalInfo.AcademicYearID.HasValue ? _globalInfo.AcademicYearID.Value : 0;
            int SchoolID = _globalInfo.SchoolID.HasValue ? _globalInfo.SchoolID.Value : 0;            
            if (AcademicYearID > 0)
            {
                dic["AcademicYearID"] = AcademicYearID;
            }
            if (SchoolID > 0)
            {
                dic["SchoolID"] = SchoolID;
            }
            if (EducationLevel.HasValue)
            {
                dic["EducationLevelID"] = EducationLevel;
            }
            List<SchoolSubject> listSchoolSubject = this.SchoolSubjectBusiness.Search(dic).ToList();
            List<SchoolSubject> listSchoolSubjectUpdate = new List<SchoolSubject>();
            List<SchoolSubject> listSchoolSubjectInsert = new List<SchoolSubject>();
            List<SchoolSubject> listSchoolSubjectDel = new List<SchoolSubject>();
            List<String> listAppliedType = ArrAppliedType.ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                   new Dictionary<string, object>()
                    {
                        {"AcademicYearID",_globalInfo.AcademicYearID}
                        ,  {"EducationLevelID",EducationLevel}
                    }
                   ).ToList();
            foreach (var i in lsCheckLocation)
            {
                int subjectID = HiddenSubjectID[i];
                SchoolSubject schoolSubjectNew = new SchoolSubject();
                //Mon hoc ma co diem roi thi bao loi
                // MarkRecordBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).Count();
                // Tim kiem cac phan tu trong grid truyen len
                string strAppliedType = ArrAppliedType.First(a => a.StartsWith(subjectID + "-"));
                string strCommenting = "";
                SubjectCat sc = SubjectCatBusiness.Find(subjectID);
                if (!sc.IsEditIsCommentting)
                {
                    //SchoolSubject ss = SchoolSubjectBusiness.All.Where(o => o.EducationLevelID == EducationLevel)
                    //    .Where(o => o.SubjectID == subjectID).FirstOrDefault();

                    strCommenting = subjectID + "-" + sc.IsCommenting;
                }
                else
                {
                    strCommenting = ArrCommenting.First(a => a.StartsWith(subjectID + "-"));
                }

                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    string strFirstSemester = ArrFirstSemester.First(a => a.StartsWith(subjectID + "-"));
                    string strSecondSemester = ArrFirstSemester.First(a => a.StartsWith(subjectID + "-"));//anhnph1_20150702_cột hệ số HKII = cột hệ số HKI = hệ số
                    schoolSubjectNew.FirstSemesterCoefficient = int.Parse(strFirstSemester.Split('-')[1]);
                    schoolSubjectNew.SecondSemesterCoefficient = int.Parse(strSecondSemester.Split('-')[1]);
                }

                //string strSectionPerWeekFirstSemester = SectionPerWeekFirstSemester.First(a => a.StartsWith(subjectID + "-"));
                //string strSectionPerWeekSecondSemester = SectionPerWeekSecondSemester.First(a => a.StartsWith(subjectID + "-"));

                // Thiet lap du lieu post len
                schoolSubjectNew.AcademicYearID = AcademicYearID;
                schoolSubjectNew.SchoolID = SchoolID;
                schoolSubjectNew.SubjectID = (int)subjectID;
                schoolSubjectNew.AppliedType = int.Parse(strAppliedType.Split('-')[1]);
                schoolSubjectNew.IsCommenting = int.Parse(strCommenting.Split('-')[1]);

                schoolSubjectNew.EducationLevelID = EducationLevel;
                string temp1 = SectionPerWeekFirstSemester[i];
                string temp2 = SectionPerWeekSecondSemester[i];
                temp1 = temp1.Trim().Length > 0 ? temp1 : "0";
                temp2 = temp2.Trim().Length > 0 ? temp2 : "0";
                if (temp1.Equals("0") && temp2.Equals("0"))
                {
                    return Json(new JsonMessage(Res.Get("Lbl_Section_Per_Week_Error"), "error"));
                }
                schoolSubjectNew.SectionPerWeekFirstSemester = decimal.Parse(temp1);
                schoolSubjectNew.SectionPerWeekSecondSemester = decimal.Parse(temp2);

                // Kiem tra cac phan tu da co trong CSDL de quyet dinh update hoac insert
                SchoolSubject schoolSubject = listSchoolSubject.Where(o => o.SubjectID == subjectID)
                                                                .Where(o => o.EducationLevelID == EducationLevel)
                                                                .FirstOrDefault();
                if (schoolSubject != null)
                {
                    // Neu da co trong CSDL thi update
                    // Thiet lap lai cac gia tri lay tu form
                    schoolSubject.AcademicYearID = schoolSubjectNew.AcademicYearID;
                    schoolSubject.EducationLevelID = schoolSubjectNew.EducationLevelID;
                    schoolSubject.SchoolID = schoolSubjectNew.SchoolID;
                    schoolSubject.SubjectID = schoolSubjectNew.SubjectID;
                    schoolSubject.AppliedType = schoolSubjectNew.AppliedType;
                    schoolSubject.IsCommenting = schoolSubjectNew.IsCommenting;
                    schoolSubject.FirstSemesterCoefficient = schoolSubjectNew.FirstSemesterCoefficient;
                    schoolSubject.SecondSemesterCoefficient = schoolSubjectNew.SecondSemesterCoefficient;
                    schoolSubject.SectionPerWeekFirstSemester = schoolSubjectNew.SectionPerWeekFirstSemester;
                    schoolSubject.SectionPerWeekSecondSemester = schoolSubjectNew.SectionPerWeekSecondSemester;

                    listSchoolSubjectUpdate.Add(schoolSubject);
                }
                else
                {
                    // Neu chua co thi insert
                    listSchoolSubjectInsert.Add(schoolSubjectNew);
                }

                // Loai ra khoi list de lay cac phan tu bi xoa khoi CSDL
                listAppliedType.Remove(strAppliedType);
            }
            foreach (string strAppliedType in listAppliedType)
            {
                int subjectIDDel = int.Parse(strAppliedType.Split('-')[0]);
                SchoolSubject schoolSubject = listSchoolSubject.Where(o => o.SubjectID == subjectIDDel).FirstOrDefault();

                // Chi nhung mon hoc nao duoc khai bao thi moi thuc hien xoa
                if (schoolSubject != null)
                {
                    SchoolSubjectForm form = new SchoolSubjectForm();
                    form.SubjectID = schoolSubject.SubjectID;
                    form.EducationLevelID = schoolSubject.EducationLevelID;
                    form.SchoolID = schoolSubject.SchoolID;
                    if (!CheckSubjectWasInClass(form, listClassSubject))
                    {
                        listSchoolSubjectDel.Add(schoolSubject);
                    }
                }
            }

            // Xoa du lieu
            List<SchoolSubject> listSchoolSubjectDelValid = new List<SchoolSubject>();
            foreach (SchoolSubject schoolSubject in listSchoolSubjectDel)
            {
                // Kiem tra rang buoc du lieu
                // Kiem tra ko can thiet - Bỏ
                //if (SchoolSubjectBusiness.CheckSchoolSubjectContraints(schoolSubject.SchoolSubjectID))
                //{
                //    listSchoolSubjectDelValid.Add(schoolSubject);
                //}
                listSchoolSubjectDelValid.Add(schoolSubject);
            }

            // Xoa theo list
            SchoolSubjectBusiness.Delete(listSchoolSubjectDelValid, AcademicYearID);

            // Them moi nhung mon hoc chua duoc gan cho truong
            if (listSchoolSubjectInsert.Count > 0)
            {
                SchoolSubjectBusiness.Insert(listSchoolSubjectInsert);
            }

            // Cap nhat lai thong tin nhung mon hoc da co
            if (listSchoolSubjectUpdate.Count > 0)
            {
                SchoolSubjectBusiness.Update(listSchoolSubjectUpdate);
            }
            SchoolSubjectBusiness.Save();

            //Mac dinh insert cac mon hoc cua truong cho cac lop trong khoi
            //Danh sach mon hoc cho truong
            List<SchoolSubject> lstSchoolSubject = this.SchoolSubjectBusiness.Search(dic).ToList();
            ClassSubjectBusiness.InsertClassSubjectbySchoolSubject(SchoolID, lstSchoolSubject, dic);
            return Json(new JsonMessage(Res.Get("SchoolSubject_Label_Create_Suc"),"success"));
        }

        private bool CheckSubjectWasInClass(SchoolSubjectForm ssf, List<ClassSubject> ListClassSubject)
        {
            if (ListClassSubject == null)
            {
                return false;
            }
            if (ListClassSubject.Any(o => o.SubjectID == ssf.SubjectID))
            {
                return true;
            }
            return false;
        }
        #endregion dungnt77
    }
}
