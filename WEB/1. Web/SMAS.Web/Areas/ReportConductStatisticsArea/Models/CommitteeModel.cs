﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportConductStatisticsTertiaryArea.Models
{
    public class CommitteModel
    {
        /// <summary>
        /// 1: school, 2 : committee; 3: dítrict
        /// </summary>
        public int EducationLevelID { get; set; }
        public string EducationLevelName { get; set; }
    }
}