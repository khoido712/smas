﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportConductStatisticsArea.Models
{
    public class SearchViewModel
    {
        public int? ReportType { get; set; }
        public int? DistrictID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? TrainingTypeID { get; set; }
        public int? Year { get; set; }
        public int? Semester { get; set; }
        public int? SubCommitteeID { get; set; }
    }
}