﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportConductStatisticsArea
{
    public class ReportConductStatisticsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportConductStatisticsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportConductStatisticsArea_default",
                "ReportConductStatisticsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
