﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportConductStatisticsArea
{
    public class ReportConductStatisticsConstants
    {
        public const string LIST_TYPESTATICS = "list_typestatics";
        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMIC_YEAR = "list_academic_year";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_TRAINING_TYPE = "list_training_type";
        public const string LIST_SUB_COMMITTEE = "list_sub_committee";
        public const string LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY = "lstConductStatisticsOfProvinceTertiary";
        public const string GRID_REPORT_ID = "grid_report_id";
    }
}