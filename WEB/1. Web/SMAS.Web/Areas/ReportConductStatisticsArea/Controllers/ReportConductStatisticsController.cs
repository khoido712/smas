﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportConductStatisticsArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using System.IO;
using SMAS.Web.Areas.ReportCapacityStatisticsTertiaryArea;
using SMAS.Web.Areas.ReportConductStatisticsTertiaryArea.Models;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportConductStatisticsArea.Controllers
{
    public class ReportConductStatisticsController : BaseController
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ITrainingTypeBusiness TrainingTypeBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        public ReportConductStatisticsController(IDistrictBusiness districtBusiness,
                                                            IAcademicYearBusiness academicYearBusiness,
                                                            ITrainingTypeBusiness trainingTypeBusiness,
                                                            ISubCommitteeBusiness subCommitteeBusiness,
            ICapacityStatisticBusiness capacityStatisticBusiness, IProcessedReportBusiness processedReportBusiness,
            IConductStatisticBusiness conductStatisticBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        public JsonResult Search(SearchViewModel model, FormCollection col)
        {
            GlobalInfo glo = new GlobalInfo();
            int reportType = int.Parse(col["ReportType"]);
            #region Thống kê theo trường
            if (reportType == 1)
            {
                //I. Nếu là account cấp Sở
                if (true/*glo.IsSuperVisingDeptRole*/)
                {
                    CapacityConductReport capConductRpt = new CapacityConductReport();
                    capConductRpt.Year = model.Year.HasValue ? model.Year.Value : 0;
                    capConductRpt.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                    capConductRpt.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                    capConductRpt.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                    capConductRpt.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                    capConductRpt.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                    capConductRpt.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capConductRpt);
                    string ReportCode = SystemParamsInFile.SGD_THPT_THONGKEHANHKIEM;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else if (entity != null)
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
            #endregion
            }
            else if (reportType == 2)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            else if (reportType == 3)
            {
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;
                ccr.SupervisingDeptID = glo.SupervisingDeptID.Value;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                string ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }
            }
            return null;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            if (model.ReportType == 1)
            {
                CapacityConductReport capConductRpt = new CapacityConductReport();
                capConductRpt.Year = model.Year.HasValue ? model.Year.Value : 0;
                capConductRpt.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                capConductRpt.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                capConductRpt.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                capConductRpt.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                capConductRpt.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                capConductRpt.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.Year;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
                dic["ReportCodeSGD"] = SystemParamsInFile.SGD_THPT_THONGKEHANHKIEM;
                dic["SubCommitteeID"] = model.SubCommitteeID;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = model.DistrictID;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                int FileID = 0;
                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capConductRpt);
                var lstConductStatisticsOfProvinceTertiary = ConductStatisticBusiness.CreateSGDConductStatisticsTertiary(dic, InputParameterHashKey, out FileID);
                ViewData[ReportConductStatisticsConstants.LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY] = lstConductStatisticsOfProvinceTertiary;
                ViewData[ReportConductStatisticsConstants.GRID_REPORT_ID] = FileID;
                return PartialView("_ListConducts");
            }
            else if (model.ReportType == 2)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.Year;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = 0;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = model.DistrictID;
                dic["SubCommitteeID"] = 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                CapacityConductReport ccr = new CapacityConductReport();
                ccr.Year = model.Year.HasValue ? model.Year.Value : 0;
                ccr.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                ccr.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                ccr.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                ccr.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                ccr.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                ccr.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ccr);
                int FileID = 0;
                IEnumerable<ConductStatisticsOfProvinceGroupBySubCommittee23> listCapacityStatisticsOfProvinceGroupBySubCommittee23 = ConductStatisticBusiness.CreateSGDConductStatisticsBySubCommitteeTertiary(dic, input, out FileID);
                ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE] = listCapacityStatisticsOfProvinceGroupBySubCommittee23;
                if (listCapacityStatisticsOfProvinceGroupBySubCommittee23 != null && listCapacityStatisticsOfProvinceGroupBySubCommittee23.Count() > 0)
                {
                    var listEducationLevel = from lcs in listCapacityStatisticsOfProvinceGroupBySubCommittee23.Select(u => u.EducationLevel).Distinct()
                                             select new CommitteModel
                                             {
                                                 EducationLevelID = lcs.Value,
                                                 EducationLevelName = listCapacityStatisticsOfProvinceGroupBySubCommittee23.Where(o => o.EducationLevel == lcs.Value).FirstOrDefault().EducationLevelName

                                             };
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = listEducationLevel;
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_COMITTEE_EDUCATIONLEVEL] = new List<CommitteModel>();
                    ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_ID] = FileID;
                }

                return PartialView("_GridSubCommittee");
            }
            else if (model.ReportType == 3)
                {
                    CapacityConductReport capConductRpt = new CapacityConductReport();
                    capConductRpt.Year = model.Year.HasValue ? model.Year.Value : 0;
                    capConductRpt.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                    capConductRpt.DistrictID = model.DistrictID.HasValue ? model.DistrictID.Value : 0;
                    capConductRpt.ProvinceID = glo.ProvinceID.HasValue ? glo.ProvinceID.Value : 0;
                    capConductRpt.Semester = model.Semester.HasValue ? model.Semester.Value : 0;
                    capConductRpt.SubcommitteeID = model.SubCommitteeID.HasValue ? model.SubCommitteeID.Value : 0;
                    capConductRpt.TrainingTypeID = model.TrainingTypeID.HasValue ? model.TrainingTypeID.Value : 0;

                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["Year"] = model.Year;
                    dic["Semester"] = model.Semester;
                    dic["EducationLevelID"] = model.EducationLevelID;
                    dic["TrainingTypeID"] = model.TrainingTypeID;
                    dic["ReportCode"] = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
                    dic["SubCommitteeID"] = model.SubCommitteeID;
                    dic["SentToSupervisor"] = true;
                    dic["ProvinceID"] = glo.ProvinceID.Value;
                    dic["DistrictID"] = model.DistrictID;
                    dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                    int FileID = 0;
                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capConductRpt);
                    var lstConductStatisticsOfProvinceTertiary = ConductStatisticBusiness.CreateSGDConductStatisticsByDistrictTertiary(dic, out FileID, InputParameterHashKey);
                    ViewData[ReportConductStatisticsConstants.LIST_CONDUCTSTATISTICSOFPROVINCETERTIARY] = lstConductStatisticsOfProvinceTertiary;
                    ViewData[ReportConductStatisticsConstants.GRID_REPORT_ID] = FileID;
                    return PartialView("_ListDistrict");
                }

            return PartialView("");
        }

        public FileResult DownloadReport(int ProcessedReportID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.SGD_THPT_THONGKEHANHKIEM,
                SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN,
                SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[ReportConductStatisticsConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{ Label=Res.Get("ConductStatistics_Label_ReportBySchool"), Value=1, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label=Res.Get("ConductStatistics_Label_ReportBySubCommittee"), Value=2, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label=Res.Get("ConductStatistics_Label_ReportByDistrict"), Value=3, cchecked=false, disabled= false}
                                                };

            List<District> listDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", glo.ProvinceID.Value }, { "IsActive", true } }).ToList();
            ViewData[ReportConductStatisticsConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");

            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept(glo.SupervisingDeptID.Value);
            ViewData[ReportConductStatisticsConstants.LIST_ACADEMIC_YEAR] = listAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList();

            ViewData[ReportConductStatisticsConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");
            ViewData[ReportConductStatisticsConstants.LIST_EDUCATION_LEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");

            List<TrainingType> listTrainingType = TrainingTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportConductStatisticsConstants.LIST_TRAINING_TYPE] = new SelectList(listTrainingType, "TrainingTypeID", "Resolution");

            List<SubCommittee> listSubComm = SubCommitteeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportConductStatisticsConstants.LIST_SUB_COMMITTEE] = new SelectList(listSubComm, "SubCommitteeID", "Resolution");
            ViewData["Flag"] = true;
        }
    }
}
