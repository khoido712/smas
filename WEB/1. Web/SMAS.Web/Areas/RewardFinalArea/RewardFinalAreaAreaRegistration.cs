﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.RewardFinalArea
{
    public class RewardFinalAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "RewardFinalArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "RewardFinalArea_default",
                "RewardFinalArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
