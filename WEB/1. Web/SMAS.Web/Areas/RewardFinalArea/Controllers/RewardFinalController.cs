﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.RewardFinalArea.Model;
using System.Web.Mvc;
using SMAS.Web.Areas.RewardFinalArea.Models;

namespace SMAS.Web.Areas.RewardFinalArea.Controllers
{
    public class RewardFinalController : BaseController
    {

        private readonly IRewardFinalBusiness RewardFinalBusiness;
        private readonly IRewardCommentFinalBusiness RewardCommentFinalBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IUpdateRewardBusiness UpdateRewardBusiness;
        public RewardFinalController(IRewardFinalBusiness rewardFinalBusiness, IRewardCommentFinalBusiness rewardCommentFinalBusiness, IAcademicYearBusiness academicYearBusiness,
            IUpdateRewardBusiness UpdateRewardBusiness)
        {
            this.RewardFinalBusiness = rewardFinalBusiness;
            this.RewardCommentFinalBusiness = rewardCommentFinalBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.UpdateRewardBusiness = UpdateRewardBusiness;
        }

        public ActionResult Index()
        {
            SetViewDataPermission("RewardFinal", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            SearchInfo["IsActive"] = true;
            List<RewardFinalViewModel> lst = this._Search(SearchInfo);
            ViewData[RewardFinalConstants.LIST_REWARDFINAL] = lst;
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
            SearchInfo["RewardFinal"] = frm.RewardMode;
            List<RewardFinalViewModel> lst = this._Search(SearchInfo);
            ViewData[RewardFinalConstants.LIST_REWARDFINAL] = lst;
            //Get view data here
            return PartialView("_List");
        }
        #region "Actions"
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            if (GetMenupermission("RewardFinal", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Reward_Validate_Insert_Teacher");
            }
            RewardFinal rewardFinal = new RewardFinal();
            TryUpdateModel(rewardFinal);
            rewardFinal.SchoolID = _globalInfo.SchoolID.Value;
            rewardFinal.CreateTime = DateTime.Now;
            rewardFinal.RewardFinalID = RewardFinalBusiness.GetNextSeq<int>();

            Utils.Utils.TrimObject(rewardFinal);
            this.RewardFinalBusiness.Insert(rewardFinal);
            this.RewardFinalBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int RewardFinalID)
        {            
            this.CheckPermissionForAction(RewardFinalID, "RewardFinal");
            if (GetMenupermission("RewardFinal", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Reward_Validate_Update_Teacher");
            }
            RewardFinal rewardFinal = this.RewardFinalBusiness.Find(RewardFinalID);
            TryUpdateModel(rewardFinal);
            rewardFinal.SchoolID = _globalInfo.SchoolID.Value;
            rewardFinal.UpdateTime = DateTime.Now;
            //Nếu trong năm hiện tại mới được cập nhật
            //if (!_globalInfo.IsCurrentYear)
            //{
            //    throw new BusinessException("Reward_Validate_Update_Year_Fail");
            //}
            this.RewardFinalBusiness.Update(rewardFinal);
            this.RewardFinalBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "RewardFinal");
            if (GetMenupermission("RewardFinal", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Reward_Validate_Delete_Teacher");
            }
            string _id = id.ToString();
            int cout = RewardCommentFinalBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID && p.RewardID.Contains(_id)).Count()
                + UpdateRewardBusiness.All.Where(p => p.SchoolID == _globalInfo.SchoolID && p.Rewards.Contains(_id)).Count();
            if (cout > 0)
            {
                throw new BusinessException("Reward_Delete_Fail");
            }
            RewardFinal objRewardFinal = new RewardFinal();
            objRewardFinal = RewardFinalBusiness.Find(id);
            this.RewardFinalBusiness.Delete(id);
            this.RewardFinalBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region "Private methods"
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<RewardFinalViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("RewardFinal", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<RewardFinal> query = this.RewardFinalBusiness.Search(SearchInfo);
            List<RewardFinalViewModel> lst = query.Select(o => new RewardFinalViewModel
            {
                RewardFinalID = o.RewardFinalID,
                RewardMode = o.RewardMode,
                SchoolID = o.SchoolID,
                Note = o.Note,
                CreatedTime = o.CreateTime,
                UpdateTime = o.UpdateTime
            }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.RewardMode)).ToList();
            return lst;
        }
        #endregion
    }
}
