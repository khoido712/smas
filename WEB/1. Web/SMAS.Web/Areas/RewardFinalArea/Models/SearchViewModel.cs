using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.RewardFinalArea.Model
{
    public class SearchViewModel
    {
        [ResourceDisplayName("RewardFinal_Control_Resolution")]
        [UIHint("Textbox")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string RewardMode { get; set; }
    }
}