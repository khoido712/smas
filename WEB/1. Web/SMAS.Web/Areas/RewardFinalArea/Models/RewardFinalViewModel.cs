using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.RewardFinalArea.Models
{
    public class RewardFinalViewModel
    {
        [ScaffoldColumn(false)]
        public int RewardFinalID { get; set; }

        [ResourceDisplayName("RewardFinal_Control_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string RewardMode { get; set; }

        [ScaffoldColumn(false)]
        public int SchoolID { get; set; }

        [ResourceDisplayName("RewardFinal_Label_Note")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreatedTime { get; set; }
        [ScaffoldColumn(false)]
        public System.Nullable<System.DateTime> UpdateTime { get; set; }
    }
}


