﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkStatisticsBySemesterTertiaryArea.Models
{
    public class AcademicYearOB
    {
        public int AcademicYearID { get; set; }
        public string Year { get; set; }
    }
}