﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Areas.MarkStatisticsBySemesterTertiaryArea;

namespace SMAS.Web.Areas.MarkStatisticsBySemesterTertiaryArea.Models
{
    public class MarkStatisticsBySemesterTertiaryViewModel
    {

        [ResourceDisplayName("MarkStatistics_Label_SchoolName")]
        public string SchoolName { get; set; }
        public int SchoolID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_SendDate")]
        public DateTime? SentDate { get; set; }

        public int TotalSchool { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_M00")]
        public int M00 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M03")]
        public int M03 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M05")]
        public int M05 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M08")]
        public int M08 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M10")]
        public int M10 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M13")]
        public int M13 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M15")]
        public int M15 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M18")]
        public int M18 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M20")]
        public int M20 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M23")]
        public int M23 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M25")]
        public int M25 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M28")]
        public int M28 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M30")]
        public int M30 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M33")]
        public int M33 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M35")]
        public int M35 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M38")]
        public int M38 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_40")]
        public int M40 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M43")]
        public int M43 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M45")]
        public int M45 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M48")]
        public int M48 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_50")]
        public int M50 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_53")]


        public int M53 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M55")]
        public int M55 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M58")]
        public int M58 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M60")]
        public int M60 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M63")]
        public int M63 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M65")]
        public int M65 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M68")]
        public int M68 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M70")]
        public int M70 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M73")]
        public int M73 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M75")]
        public int M75 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M78")]
        public int M78 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M80")]
        public int M80 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M83")]
        public int M83 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M85")]
        public int M85 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M88")]
        public int M88 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M90")]
        public int M90 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M93")]
        public int M93 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M95")]
        public int M95 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_M98")]
        public int M98 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_100")]
        public int M100 { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalTest")]
        public int TotalTest { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalBelowAverage")]

        public int TotalBelowAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_PercentBelowAverage")]
        public double PercentBelowAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_TotalAboveAverage")]
        public int TotalAboveAverage { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_PercentAboveAverage")]
        public double PercentAboveAverage { get; set; }

    }
}