﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkStatisticsBySemesterTertiaryArea.Models
{
    public class SearchViewModel
    {
        public int? DistrictID { get; set; }
        public int Year { get; set; }
        public int? EducationLevelID { get; set; }
        public int? TrainingTypeID { get; set; }
        public int SemesterID { get; set; }
        public int SubjectID { get; set; }
        public int SubCommitteeID { get; set; }



    }
}