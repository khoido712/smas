﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkStatisticsBySemesterTertiaryArea
{
    public class MarkStatisticsBySemesterTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkStatisticsBySemesterTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkStatisticsBySemesterTertiaryArea_default",
                "MarkStatisticsBySemesterTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
