﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DOETExamPupilRegisSubArea.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea.Controllers
{
    public class DOETExamPupilRegisSubController:BaseController
    {
        #region Properties
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IDOETExamMarkBusiness DOETExamMarkBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private List<ComboObject> lstYear;
        private List<DOETExaminations> lstExaminations;
        private List<DOETExamGroup> lstExamGroup;
        private List<DOETExamSubjectBO> lstExamSubject;
        #endregion

        #region Constructor
        public DOETExamPupilRegisSubController(IDOETExamGroupBusiness examGroupBusiness, IDOETExaminationsBusiness examinationsBusiness,
            IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness, ISupervisingDeptBusiness SupervisingDeptBusiness,
            IDOETExamMarkBusiness DOETExamMarkBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IPupilProfileBusiness PupilProfileBusiness,
            IClassProfileBusiness ClassProfileBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.DOETExamGroupBusiness = examGroupBusiness;
            this.DOETExaminationsBusiness = examinationsBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.DOETExamMarkBusiness = DOETExamMarkBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        #endregion

        #region Action
        public ActionResult Index()
        {

            SetViewData();

            //Search du lieu
            int? year = null;
            if (lstYear.Count > 0)
            {
                year = Int32.Parse(lstYear.First().key);
            }

            int? examinationsId = null;
            if (lstExaminations.Count > 0)
            {
                examinationsId = lstExaminations.First().ExaminationsID;
            }

            int? examGroupId = null;
            if (lstExamGroup.Count > 0)
            {
                examGroupId = lstExamGroup.First().ExamGroupID;
            }

            int? doetSubjectId = null;
            if (lstExamSubject.Count > 0)
            {
                doetSubjectId = lstExamSubject.First().DOETSubjectID;
            }

            List<ListViewModel> lstModel = new List<ListViewModel>();
            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && doetSubjectId.HasValue)
            {
                lstModel = _Search(year.Value, examinationsId.Value, examGroupId.Value, doetSubjectId.Value,1,out total);
            }

            ViewData[DOETExamPupilRegisSubConstants.LIST_RESULT] = lstModel;
            ViewData[DOETExamPupilRegisSubConstants.TOTAL] = total;
            SearchViewModel searchModel = new SearchViewModel();
            searchModel.Year = year;
            searchModel.ExaminationsId = examinationsId;
            searchModel.ExamGroupId = examGroupId;
            searchModel.DOETSubjectId = doetSubjectId;
            ViewData[DOETExamPupilRegisSubConstants.SEARCH_VIEW_MODEL] = searchModel;

            CheckButtonPermission(examinationsId, examGroupId);

            return View();
        }

        [HttpPost]
        public PartialViewResult Search(SearchViewModel form)
        {
            int? year = form.Year;
            int? examinationsId = form.ExaminationsId;
            int? examGroupId = form.ExamGroupId;
            int? subjectId = form.DOETSubjectId;

            List<ListViewModel> lstModel = new List<ListViewModel>();
            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && subjectId.HasValue)
            {
                lstModel = _Search(year.Value, examinationsId.Value, examGroupId.Value, subjectId.Value, 1, out total);
            }

            ViewData[DOETExamPupilRegisSubConstants.TOTAL] = total;

            SearchViewModel searchModel = new SearchViewModel();
            searchModel.Year = year;
            searchModel.ExaminationsId = examinationsId;
            searchModel.ExamGroupId = examGroupId;
            searchModel.DOETSubjectId = subjectId;
            ViewData[DOETExamPupilRegisSubConstants.SEARCH_VIEW_MODEL] = searchModel;

            CheckButtonPermission(examinationsId, examGroupId);

            return PartialView("_List", lstModel);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchAjax(SearchViewModel form, GridCommand command)
        {
            int currentPage = command.Page;

            int? year = form.Year;
            int? examinationsId = form.ExaminationsId;
            int? examGroupId = form.ExamGroupId;
            int? subjectId = form.DOETSubjectId;

            List<ListViewModel> lstModel = new List<ListViewModel>();
            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && subjectId.HasValue)
            {
                lstModel = _Search(year.Value, examinationsId.Value, examGroupId.Value, subjectId.Value, currentPage, out total);
            }

            return View(new GridModel<ListViewModel>
            {
                Total = total,
                Data = lstModel
            });
        }

        public ActionResult ExamInfo(int? examinationsId)
        {
            DOETExaminations exam = DOETExaminationsBusiness.Find(examinationsId);
            return PartialView("_ExamInfo", exam);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(List<int> selectedIds)
        {
            if (GetMenupermission("DOETExamPupilRegisSub", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SMAS.Business.Common.SystemParamsInFile.PER_DELETE)
            {
                throw new SMAS.Business.Common.BusinessException("Validate_Permission_Teacher");
            }

            if (selectedIds != null)
            {
                //Xoa diem thi sinh
                List<DOETExamMark> lstMark = DOETExamMarkBusiness.All.Where(o => selectedIds.Contains(o.ExamPupilID)).ToList();
                for (int i = 0; i < lstMark.Count; i++)
                {
                    DOETExamMarkBusiness.Delete(lstMark[i].ExamMarkID);
                }

                for (int i = 0; i < selectedIds.Count; i++)
                {
                    DOETExamPupilBusiness.Delete(selectedIds[i]);
                }

                DOETExamPupilBusiness.Save();
            }

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int regisYear ,int regisExaminationsId, int regisExamGroupId)
        {
            SetViewDataForCreate();

            //Search du lieu
            int? year = null;
            if (lstYear.Count > 0)
            {
                year = Int32.Parse(lstYear.First().key);
            }

            int? examinationsId = null;
            if (lstExaminations.Count > 0)
            {
                examinationsId = lstExaminations.First().ExaminationsID;
            }

            int? examGroupId = null;
            if (lstExamGroup.Count > 0)
            {
                examGroupId = lstExamGroup.First().ExamGroupID;
            }

            int? doetSubjectId = null;
            if (lstExamSubject.Count > 0)
            {
                doetSubjectId = lstExamSubject.First().DOETSubjectID;
            }

            //Lay cac thi sinh da dang ky
            List<int> lstRegistedPupilId=DOETExamPupilBusiness.All.Where(o=>o.Year == regisYear && o.ExaminationsID==regisExaminationsId
                        && o.ExamGroupID==regisExamGroupId).Select(o=>o.PupilID).ToList();

            List<ListViewModel> lstModel = new List<ListViewModel>();
            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && doetSubjectId.HasValue)
            {
                lstModel = _CreateSearch(year.Value, examinationsId.Value, examGroupId.Value, doetSubjectId.Value, 1, out total, null, null, lstRegistedPupilId);
            }

            DOETExaminations regisExam = DOETExaminationsBusiness.Find(regisExaminationsId);
            DOETExamGroup regisExamGroup = DOETExamGroupBusiness.Find(regisExamGroupId);
            ViewData[DOETExamPupilRegisSubConstants.REGIS_YEAR] = regisYear;
            ViewData[DOETExamPupilRegisSubConstants.REGIS_EXAMINATIONS] = regisExaminationsId;
            ViewData[DOETExamPupilRegisSubConstants.REGIS_EXAM_GROUP] = regisExamGroupId;
            ViewData[DOETExamPupilRegisSubConstants.REGIS_EXAMINATIONS_NAME] = regisExam.ExaminationsName;
            ViewData[DOETExamPupilRegisSubConstants.REGIS_EXAM_GROUP_NAME] = regisExamGroup.ExamGroupName;
            ViewData[DOETExamPupilRegisSubConstants.LIST_RESULT] = lstModel;
            ViewData[DOETExamPupilRegisSubConstants.TOTAL] = total;

            return PartialView();
        }

        [HttpPost]
        public PartialViewResult CreateSearch(CreateSearchViewModel form, int regisYear, int regisExaminationsId, int regisExamGroupId)
        {
            int? year = form.Year;
            int? examinationsId = form.ExaminationsId;
            int? examGroupId = form.ExamGroupId;
            int? subjectId = form.DOETSubjectId;
            string examineeNumber = form.ExamineeNumber;
            decimal? mark = form.Mark;

            //Lay cac thi sinh da dang ky
            List<int> lstRegistedPupilId = DOETExamPupilBusiness.All.Where(o => o.Year == regisYear && o.ExaminationsID == regisExaminationsId
                        && o.ExamGroupID == regisExamGroupId).Select(o => o.PupilID).ToList();
            List<ListViewModel> lstModel = new List<ListViewModel>();

            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && subjectId.HasValue)
            {
                lstModel = _CreateSearch(year.Value, examinationsId.Value, examGroupId.Value, subjectId.Value, 1, out total, examineeNumber, mark, lstRegistedPupilId);
            }

            ViewData[DOETExamPupilRegisSubConstants.TOTAL] = total;
            return PartialView("_CreateList", lstModel);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult CreateSearchAjax(CreateSearchViewModel form, int regisYear, int regisExaminationsId, int regisExamGroupId, GridCommand command)
        {
            int currentPage = command.Page;

            int? year = form.Year;
            int? examinationsId = form.ExaminationsId;
            int? examGroupId = form.ExamGroupId;
            int? subjectId = form.DOETSubjectId;
            string examineeNumber = form.ExamineeNumber;
            decimal? mark = form.Mark;

            //Lay cac thi sinh da dang ky
            List<int> lstRegistedPupilId = DOETExamPupilBusiness.All.Where(o => o.Year == regisYear && o.ExaminationsID == regisExaminationsId
                        && o.ExamGroupID == regisExamGroupId).Select(o => o.PupilID).ToList();
            List<ListViewModel> lstModel = new List<ListViewModel>();

            int total = 0;
            if (year.HasValue && examinationsId.HasValue && examGroupId.HasValue && subjectId.HasValue)
            {
                lstModel = _CreateSearch(year.Value, examinationsId.Value, examGroupId.Value, subjectId.Value, currentPage, out total, examineeNumber, mark, lstRegistedPupilId);
            }

            return View(new GridModel<ListViewModel>
            {
                Total = total,
                Data = lstModel
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Regis(List<int> selectedIds, int examinationsId, int examGroupId)
        {
            if (GetMenupermission("DOETExamPupilRegisSub", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SMAS.Business.Common.SystemParamsInFile.PER_CREATE)
            {
                throw new SMAS.Business.Common.BusinessException("Validate_Permission_Teacher");
            }

            if (selectedIds != null)
            {
                //lay ra ky thi
                DOETExaminations exam = DOETExaminationsBusiness.Find(examinationsId);
                List<DOETExamPupil> lstExamPupil = DOETExamPupilBusiness.All.Where(o => selectedIds.Contains(o.ExamPupilID)).ToList();

                DOETExamPupil examPupil;
                for (int i = 0; i < lstExamPupil.Count; i++)
                {
                    DOETExamPupil oldExamPupil = lstExamPupil[i];

                    examPupil = new DOETExamPupil();
                    examPupil.ExamPupilID = DOETExamPupilBusiness.GetNextSeq<int>("DOET_EXAM_PUPIL_SEQ");
                    examPupil.CreateTime = DateTime.Now;
                    examPupil.EducationLevelID = oldExamPupil.EducationLevelID;
                    examPupil.ExamGroupID = examGroupId;
                    examPupil.ExaminationsID = examinationsId;
                    examPupil.PupilID = oldExamPupil.PupilID;
                    examPupil.SchoolID = oldExamPupil.SchoolID;
                    examPupil.UnitID = exam.UnitID;
                    examPupil.Year = exam.Year;

                    DOETExamPupilBusiness.Insert(examPupil);
                }

                DOETExamPupilBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_RegisSuccessMessage")));
        }

        public JsonResult AjaxLoadExaminations(int year, bool forCreate)
        {
            int? unitId;
            if (forCreate)
            {
                unitId = GetUnitID();
            }
            else
            {
                unitId = GetParentID();
            }
            List<DOETExaminations> lstExaminations;
            if (forCreate == false)
            {
                lstExaminations = DOETExaminationsBusiness.GetExaminationsOfDept(unitId.GetValueOrDefault(), year)
                        .Where(o => o.AppliedType == GlobalConstants.DOET_EXAM_APPLIED_TYPE_SUB_SUPERVISORY).OrderByDescending(o => o.CreateTime).ToList();
            }
            else
            {
                lstExaminations = DOETExaminationsBusiness.GetExaminationsOfDept(unitId.GetValueOrDefault(), year).OrderByDescending(o => o.CreateTime).ToList();
            }

            return Json(new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName"));
        }

        public JsonResult AjaxLoadExamGroup(int? examinationsID)
        {
            List<DOETExamGroup> lstExamGroup = new List<DOETExamGroup>();
            if (examinationsID != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID).OrderBy(o => o.ExamGroupCode).ToList();
            }

            return Json(new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(int? examinationsID, int? examGroupID)
        {
            List<DOETExamSubjectBO> lstExamSubject = new List<DOETExamSubjectBO>();
            if (examinationsID != null && examGroupID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                dic["ExamGroupID"] = examGroupID;
                lstExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.DOETSubjectName).ToList();

            }

            return Json(new SelectList(lstExamSubject, "DOETSubjectID", "DOETSubjectName"));
        }

        [HttpPost]
        public JsonResult AjaxLoadRegisButton(int? examinationsId, int? examGroupId)
        {
            SetViewDataPermission("DOETExamPupilRegisSub", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            bool checkRegis = false;

            if (examinationsId.HasValue && examGroupId.HasValue)
            {
                DOETExaminations exam = DOETExaminationsBusiness.Find(examinationsId);
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                   && DateTime.Now.Date <= exam.DeadLine)
                {
                    checkRegis = true;
                }
            }

            return Json(new { enable = checkRegis });
        }
        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["UnitId"] = GetUnitID();
            dic["Year"] = form.Year;
            dic["ExaminationsId"] = form.ExaminationsId;
            dic["ExamGroupId"] = form.ExamGroupId;
            dic["DOETSubjectId"] = form.DOETSubjectId;

            string reportCode = SMAS.Business.Common.SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_PHONG_DK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            ProcessedReport processedReport = null;

            Stream excel = DOETExamPupilBusiness.CreateExamPupilRegisSubReport(dic);
            processedReport = DOETExamPupilBusiness.InsertExamPupilRegisSubReport(dic, excel);
            excel.Close();
            
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = SMAS.Business.Common.UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = SMAS.Business.Common.ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion
        #endregion

        #region Private methods
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lay ra id cua so
            int? parrentId = GetParentID();

            //Lay ra cac nam co ky thi cua so tao
            List<int> lstExamYear = DOETExaminationsBusiness.GetExaminationsOfDept(parrentId.GetValueOrDefault()).Select(o => o.Year).Distinct().OrderByDescending(o=>o).ToList();
            lstYear = new List<ComboObject>();
            for (int i = 0; i < lstExamYear.Count; i++)
            {
                int year = lstExamYear[i];
                lstYear.Add(GetYearObject(year));
            }
            int? defaultYear = null;
            if (lstExamYear.Count > 0)
            {
                defaultYear = lstExamYear.First();
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_YEAR] = new SelectList(lstYear, "key", "value");

            //Lay danh sach ky thi
            lstExaminations = new List<DOETExaminations>();
            if (defaultYear != null)
            {
                lstExaminations = DOETExaminationsBusiness.GetExaminationsOfDept(parrentId.GetValueOrDefault(), defaultYear)
                    .Where(o => o.AppliedType==GlobalConstants.DOET_EXAM_APPLIED_TYPE_SUB_SUPERVISORY).OrderByDescending(o => o.CreateTime).ToList();
            }

            int? defaultExaminationsId = null;
            if (lstExaminations.Count > 0)
            {
                defaultExaminationsId = lstExaminations.First().ExaminationsID;
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAMINATIONS] = new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName");

            //Lay danh sach nhom thi
            lstExamGroup = new List<DOETExamGroup>();
            if (defaultExaminationsId != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == defaultExaminationsId)
                    .OrderBy(o=>o.ExamGroupCode).ToList();
            }
            int? defaultExamGroupId = null;
            if (lstExamGroup.Count > 0)
            {
                defaultExamGroupId = lstExamGroup.First().ExamGroupID;
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAM_GROUP] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");

            //Lay danh sach mon thi
            lstExamSubject = new List<DOETExamSubjectBO>();
            if (defaultExamGroupId != null)
            {
                IDictionary<string,object> dic=new Dictionary<string,object>();
                dic["ExaminationsID"]=defaultExaminationsId;
                dic["ExamGroupID"]=defaultExamGroupId;
                lstExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o=>o.DOETSubjectName).ToList();
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAM_SUBJECT] = new SelectList(lstExamSubject, "DOETSubjectID", "DOETSubjectName");
        }

        private void SetViewDataForCreate()
        {
            //Lay ra id cua so
            int? unitId = GetUnitID();

            //Lay ra cac nam co ky thi cua phong tao
            List<int> lstExamYear = DOETExaminationsBusiness.GetExaminationsOfDept(unitId.GetValueOrDefault()).Select(o => o.Year).Distinct().OrderByDescending(o => o).ToList();
            lstYear = new List<ComboObject>();
            for (int i = 0; i < lstExamYear.Count; i++)
            {
                int year = lstExamYear[i];
                lstYear.Add(GetYearObject(year));
            }
            int? defaultYear = null;
            if (lstExamYear.Count > 0)
            {
                defaultYear = lstExamYear.First();
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_YEAR] = new SelectList(lstYear, "key", "value");

            //Lay danh sach ky thi
            lstExaminations = new List<DOETExaminations>();
            if (defaultYear != null)
            {
                lstExaminations = DOETExaminationsBusiness.GetExaminationsOfDept(unitId.GetValueOrDefault(), defaultYear).OrderByDescending(o => o.CreateTime).ToList();
            }

            int? defaultExaminationsId = null;
            if (lstExaminations.Count > 0)
            {
                defaultExaminationsId = lstExaminations.First().ExaminationsID;
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAMINATIONS] = new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName");

            //Lay danh sach nhom thi
            lstExamGroup = new List<DOETExamGroup>();
            if (defaultExaminationsId != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == defaultExaminationsId)
                    .OrderBy(o => o.ExamGroupCode).ToList();
            }
            int? defaultExamGroupId = null;
            if (lstExamGroup.Count > 0)
            {
                defaultExamGroupId = lstExamGroup.First().ExamGroupID;
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAM_GROUP] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");

            //Lay danh sach mon thi
            lstExamSubject = new List<DOETExamSubjectBO>();
            if (defaultExamGroupId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExaminationsId;
                dic["ExamGroupID"] = defaultExamGroupId;
                lstExamSubject = DOETExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.DOETSubjectName).ToList();
            }
            ViewData[DOETExamPupilRegisSubConstants.CBO_EXAM_SUBJECT] = new SelectList(lstExamSubject, "DOETSubjectID", "DOETSubjectName");
        }

        private int GetUnitID()
        {
            int unitId = 0;
            int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
            SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
            //Neu la accont phong ban thuoc phong
            if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                SupervisingDept subSupervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                unitId = subSupervisingDept.SupervisingDeptID;

            }
            //Neu la account phong
            else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                unitId = supervisingDeptID;
            }
            return unitId;
        }

        private int? GetParentID()
        {
            int? parrentId = null;
            int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
            SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
            //Neu la accont phong ban thuoc phong
            if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                SupervisingDept subSupervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                parrentId = subSupervisingDept.ParentID;

            }
            //Neu la account phong
            else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                parrentId = dept.ParentID;
            }
            return parrentId;
        }
        private ComboObject GetYearObject(int year)
        {
            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        private List<ListViewModel> _Search(int year, int examinationsId, int examGroupId, int doetSubjectId, int page, out int total)
        {
            int unitId = GetUnitID();
            IQueryable<ListViewModel> query = (from ep in DOETExamPupilBusiness.All
                                               join pp in PupilProfileBusiness.All on ep.PupilID equals pp.PupilProfileID
                                               join poc in PupilOfClassBusiness.All.Where(o => o.Year == year && o.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS) on new { ep.SchoolID, ep.PupilID } equals new { poc.SchoolID, poc.PupilID } into des1
                                               from x in des1.DefaultIfEmpty()
                                               join cp in ClassProfileBusiness.All on x.ClassID equals cp.ClassProfileID into des2
                                               from y in des2.DefaultIfEmpty()
                                               join sp in SchoolProfileBusiness.All on ep.SchoolID equals sp.SchoolProfileID
                                               join em in DOETExamMarkBusiness.All.Where(o => o.DoetSubjectID == doetSubjectId) on ep.ExamPupilID equals em.ExamPupilID into des3
                                               from z in des3.DefaultIfEmpty()
                                               where ep.Year == year
                                                   && ep.ExaminationsID == examinationsId
                                                   && ep.ExamGroupID == examGroupId
                                                   && sp.SupervisingDeptID == unitId
                                                   && (!y.IsActive.HasValue || (y.IsActive.HasValue && y.IsActive.Value))
                                               select new ListViewModel
                                               {
                                                   ExamPupilID = ep.ExamPupilID,
                                                   PupilID=ep.PupilID,
                                                   BirthDate = pp.BirthDate,
                                                   ClassName = y != null ? y.DisplayName : String.Empty,
                                                   ExamineeNumber = ep.ExamineeNumber,
                                                   ExamRoom = ep.ExamRoom,
                                                   Genre = pp.Genre == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE : GlobalConstants.GENRE_MALE_TITLE,
                                                   Location = ep.Location,
                                                   Mark = z.Mark,
                                                   PupilCode = pp.PupilCode,
                                                   FullName = pp.FullName,
                                                   Name = pp.Name,
                                                   SchoolName = sp.SchoolName
                                               }).OrderBy(o => o.ExamineeNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName);
          
            total = query.Count();

            return query.Skip((page - 1) * DOETExamPupilRegisSubConstants.PageSize).Take(DOETExamPupilRegisSubConstants.PageSize).ToList();
        }

        private List<ListViewModel> _CreateSearch(int year, int examinationsId, int examGroupId, int doetSubjectId, int page, out int total, string examineeNumber, decimal? mark, List<int> lstPupilId)
        {
            IQueryable<ListViewModel> query = (from ep in DOETExamPupilBusiness.All
                                               join pp in PupilProfileBusiness.All on ep.PupilID equals pp.PupilProfileID
                                               join poc in PupilOfClassBusiness.All.Where(o => o.Year == year && o.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS) on new { ep.SchoolID, ep.PupilID } equals new { poc.SchoolID, poc.PupilID } into des1
                                               from x in des1.DefaultIfEmpty()
                                               join cp in ClassProfileBusiness.All on x.ClassID equals cp.ClassProfileID into des2
                                               from y in des2.DefaultIfEmpty()
                                               join sp in SchoolProfileBusiness.All on ep.SchoolID equals sp.SchoolProfileID
                                               join em in DOETExamMarkBusiness.All.Where(o => o.DoetSubjectID == doetSubjectId) on ep.ExamPupilID equals em.ExamPupilID into des3
                                               from z in des3.DefaultIfEmpty()
                                               where ep.Year == year
                                                   && ep.ExaminationsID == examinationsId
                                                   && ep.ExamGroupID == examGroupId
                                                   && (!y.IsActive.HasValue || (y.IsActive.HasValue && y.IsActive.Value))
                                               select new ListViewModel
                                               {
                                                   ExamPupilID = ep.ExamPupilID,
                                                   PupilID = ep.PupilID,
                                                   BirthDate = pp.BirthDate,
                                                   ClassName = y != null ? y.DisplayName : String.Empty,
                                                   ExamineeNumber = ep.ExamineeNumber,
                                                   ExamRoom = ep.ExamRoom,
                                                   Genre = pp.Genre == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE : GlobalConstants.GENRE_MALE_TITLE,
                                                   Location = ep.Location,
                                                   Mark = z.Mark,
                                                   PupilCode = pp.PupilCode,
                                                   FullName = pp.FullName,
                                                   Name = pp.Name,
                                                   SchoolName = sp.SchoolName
                                               }).OrderBy(o => o.ExamineeNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName);
            if (!String.IsNullOrEmpty(examineeNumber))
            {
                query = query.Where(o => o.ExamineeNumber == examineeNumber);
            }

            if (mark.HasValue)
            {
                query = query.Where(o => o.Mark >= mark);
            }

            query = query.Where(o => !lstPupilId.Contains(o.PupilID));
            
            total = query.Count();

            return query.Skip((page - 1) * DOETExamPupilRegisSubConstants.PageSize).Take(DOETExamPupilRegisSubConstants.PageSize).ToList();
        }

        private void CheckButtonPermission(int? examinationsId, int? examGroupId)
        {
            SetViewDataPermission("DOETExamPupilRegisSub", _globalInfo.UserAccountID, _globalInfo.IsAdmin);    
            bool checkRegis=false;

            if (examinationsId.HasValue && examGroupId.HasValue)
            {
                DOETExaminations exam = DOETExaminationsBusiness.Find(examinationsId);
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                   && DateTime.Now.Date <= exam.DeadLine)
                {
                    checkRegis = true;
                }
            }

            ViewData[DOETExamPupilRegisSubConstants.PER_BUTTON_REGIS] = checkRegis;

            ViewData[DOETExamPupilRegisSubConstants.PER_BUTTON_DELETE] = ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE];
        }
        #endregion
    }
}