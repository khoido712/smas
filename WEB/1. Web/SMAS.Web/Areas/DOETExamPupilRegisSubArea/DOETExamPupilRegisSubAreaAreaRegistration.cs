﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea
{
    public class DOETExamPupilRegisSubAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExamPupilRegisSubArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExamPupilRegisSubArea_default",
                "DOETExamPupilRegisSubArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
