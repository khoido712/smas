﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DOETExamPupilRegisSub_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_YEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onYearChange()")]
        public int? Year { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public int? ExaminationsId { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public int? ExamGroupId { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_DOETSubject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? DOETSubjectId { get; set; }

    }
}