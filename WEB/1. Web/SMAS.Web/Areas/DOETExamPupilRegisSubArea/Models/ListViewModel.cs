﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea.Models
{
    public class ListViewModel
    {

        public int ExamPupilID { get; set; }
        public int PupilID { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_PupilCode")]
        public string PupilCode { get; set; }
        
        public string Name { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_Genre")]
        public string Genre { get; set; }
         [ResourceDisplayName("DOETExamPupilRegisSub_BirthDate")]
        public DateTime BirthDate { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_ClassName")]
        public string ClassName { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_SchoolName")]
        public string SchoolName { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_ExamineeNumber")]
        public string ExamineeNumber { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_ExamRoom")]
        public string ExamRoom { get; set; }
        [ResourceDisplayName("DOETExamPupilRegisSub_Location")]
        public string Location { get; set; }
        
        public decimal? Mark { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_Mark2")]
        public string strMark
        {
            get
            {
                string str = String.Empty;
                if (Mark.HasValue)
                {
                    if (Mark == 10 || Mark == 0)
                    {
                        str = Mark.Value.ToString();

                    }
                    else
                    {
                        str = Mark.Value.ToString("0.0");
                    }

                }
                return str;
            }
        }

    }
}