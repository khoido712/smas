﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea.Models
{
    public class CreateSearchViewModel
    {
        [ResourceDisplayName("DOETExamPupilRegisSub_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_YEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onYearChange()")]
        public int Year { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_CreateExaminations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public int? ExaminationsId { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public int? ExamGroupId { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_ExamineeNumber")]
        [StringLength(20, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamineeNumber { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_DOETSubject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamPupilRegisSubConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? DOETSubjectId { get; set; }

        [ResourceDisplayName("DOETExamPupilRegisSub_Mark")]
        public decimal? Mark { get; set; }
    }
}