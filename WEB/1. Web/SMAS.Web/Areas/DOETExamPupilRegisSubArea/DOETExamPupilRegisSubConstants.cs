﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExamPupilRegisSubArea
{
    public class DOETExamPupilRegisSubConstants
    {
        public const string CBO_YEAR = "CBO_YEAR";
        public const string CBO_EXAMINATIONS = "CBO_EXAMINATIONS";
        public const string CBO_EXAM_GROUP = "CBO_EXAM_GROUP";
        public const string CBO_EXAM_SUBJECT = "CBO_EXAM_SUBJECT";
        public const string LIST_RESULT = "LIST_RESULT";
        public const string REGIS_YEAR = "REGIS_YEAR";
        public const string REGIS_EXAMINATIONS = "REGIS_EXAMINATIONS";
        public const string REGIS_EXAM_GROUP = "REGIS_EXAM_GROUP";
        public const string REGIS_EXAMINATIONS_NAME = "REGIS_EXAMINATIONS_NAME";
        public const string REGIS_EXAM_GROUP_NAME = "REGIS_EXAM_GROUP_NAME";
        public const string PER_BUTTON_REGIS = "PER_BUTTON_REGIS";
        public const string PER_BUTTON_DELETE = "PER_BUTTON_DELETE";
        public const string SEARCH_VIEW_MODEL = "SEARCH_VIEW_MODEL";

        public const int PageSize = 20;
        public const string TOTAL = "total";
        public const string PAGE = "page";
    }
}