﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SyntheticdataSecondaryArea
{
    public class SyntheticdataSecondaryConstants
    {
        public const string LIST_MARKSTATISTICS = "LIST_MARKSTATISTICS";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_AcademicYear = "LIST_AcademicYear";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_TrainingType = "LIST_TrainingType";
        public const string LIST_Semester = "LIST_Semmester";
        public const string LIST_Subject = "LIST_Subject";
        public const string DisableDistrict = "DisableDistrict";
        public const bool Show_List = false;
        public const string ProcessedDate = "01/01/2012";

        public const string LIST_TYPESTATICS = "list_typestatics";
        public const string LIST_SUB_COMMITTEE = "list_sub_committee";
        public const string LIST_CAPACITYSTATISTIC = "list_capacitystatistic";
        public const string LIST_CAPACITYSTATISTICDistrict = "list_capacitystatisticdistrict";

        public const string GRID_REPORT_DATA = "grid_report_data";
        public const string GRID_REPORT_COMITTEE = "grid_report_comittee";
        public const string GRID_REPORT_COMITTEE_EDUCATIONLEVEL = "grid_report_comittee_educationlevel";
        public const string GRID_REPORT_ID = "grid_report_id";
        public const string LIST_CAPACITYSTATISTIC_BY_SCHOOL = "listSchool";
        public const string FILE_ID = "fileid";
        public const string DISABLE_COMBOBOX = "disablecbb";
        public const string VIEW_SGD = "VIEW_SGD";
    }
}