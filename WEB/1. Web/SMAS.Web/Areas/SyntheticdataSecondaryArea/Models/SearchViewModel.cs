﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.SyntheticdataSecondaryArea.Models
{
    public class SearchViewModel
    {
        /*[ResourceDisplayName("MarkStatistics_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? DistrictID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_AcademicYear)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? AcademicYearID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_EducationLevel)]
        [AdditionalMetadata("Placeholder", "SelectRK")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_TrainingType)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? TrainingTypeID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Semmeter")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_Semester)]
        [AdditionalMetadata("Placeholder", "null")]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SyntheticdataSecondaryConstants.LIST_Subject)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SubjectID { get; set; }*/
        public int ReportType { get; set; }
        public int? EducationLevelID { get; set; }
        public int AcademicYearID { get; set; }
        public int Semester { get; set; }
        public int YearID { get; set; }
        public int SubjectCatID { get; set; }
        public bool IsGenre { get; set; }
        public bool IsEthnic { get; set; }
        public bool IsGenreEthnic { get; set; }
    }
}