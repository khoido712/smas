﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SyntheticdataSecondaryArea
{
    public class SyntheticdataSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SyntheticdataSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SyntheticdataSecondaryArea_default",
                "SyntheticdataSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
