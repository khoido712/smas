﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.SyntheticdataSecondaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using System.Configuration;

namespace SMAS.Web.Areas.SyntheticdataSecondaryArea.Controllers
{
    public class SyntheticdataSecondaryController : Controller
    {
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly IConductStatisticBusiness ConductStatisticBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        //
        // GET: /SyntheticdataSecondaryArea/SyntheticdataSecondary/
        public SyntheticdataSecondaryController(IMarkStatisticBusiness markStatisticBusiness, IDistrictBusiness districtBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, IEducationLevelBusiness educationLevelBusiness, ISubjectCatBusiness subjectCatBusiness,
            IAcademicYearBusiness academicYearBusiness, IReportDefinitionBusiness reportDefinitionBusiness, IProcessedReportBusiness processedReportBusiness,
            ICapacityStatisticBusiness capacityStatisticBusiness, IConductStatisticBusiness conductStatisticBusiness,
            ISupervisingDeptBusiness supervisingDeptBusiness,
            IAcademicYearOfProvinceBusiness academicYearOfProvinceBusiness)
        {
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.DistrictBusiness = districtBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.AcademicYearOfProvinceBusiness = academicYearOfProvinceBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }
        private void GetViewData()
        {
            ViewData[SyntheticdataSecondaryConstants.DISABLE_COMBOBOX] = false;
            GlobalInfo global = new GlobalInfo();
            //glo.ProvinceID = 3;
            //glo.SupervisingDeptID = 4;

            ViewData[SyntheticdataSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{Label="Thống kê điểm kiểm tra định kỳ", Value=1, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê điểm kiểm tra học kỳ", Value=2, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label="Thống kê  học lực môn", Value=3, cchecked=false, disabled= false}
                                                    //new ViettelCheckboxList{Label="Thống kê học lực", Value=4, cchecked=false, disabled= false},
                                                    //new ViettelCheckboxList{Label="Thống kê hạnh kiểm", Value=5, cchecked=false, disabled= false}
                                                };
            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THCS(global.SupervisingDeptID.Value);
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();
            // Lay nam hoc hien tai
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(global.ProvinceID.GetValueOrDefault(),out curSemester, stringFirstStartDate);
            ViewData[SyntheticdataSecondaryConstants.LIST_AcademicYear] = new SelectList(ListAcademicYear, "Year", "DisplayTitle", curYear);

            ViewData[SyntheticdataSecondaryConstants.LIST_Semester] = new SelectList(CommonList.Semester(), "key", "value",curSemester);

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY).ToList();
            ViewData[SyntheticdataSecondaryConstants.LIST_EducationLevel] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            IDictionary<string, object> TrainingTypesearchInfo = new Dictionary<string, object>();
            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            
            MarkStatisticsearchInfo["Year"] = curYear;
            MarkStatisticsearchInfo["EducationLevelID"] = SystemParamsInFile.EDUCATION_LEVEL_SIXTH;
            MarkStatisticsearchInfo["ProvinceID"] = global.ProvinceID;
            MarkStatisticsearchInfo["SupervisingDeptID"] = global.SupervisingDeptID;
            MarkStatisticsearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            MarkStatisticsearchInfo["IsCommenting"] = 0;
            List<SubjectCatBO> ListSubject = SupervisingDeptBusiness.SearchSubject(MarkStatisticsearchInfo);
            ViewData[SyntheticdataSecondaryConstants.LIST_Subject] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");
            if (!global.IsSuperVisingDeptRole)
            {
                ViewData[SyntheticdataSecondaryConstants.VIEW_SGD] = "true";
            }
            else
            {
                ViewData[SyntheticdataSecondaryConstants.VIEW_SGD] = "false";
            }
        }
        private string GetReportCode(int supervisingDeptID, int reportType)
        {
            if (reportType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_DINH_KY)
            {
                if (supervisingDeptID == 0)
                {
                    return SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraDinhKyCap2;
                }
                else
                {
                    return SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraDinhKyCap2;
                }
            }
            else if (reportType == SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                if (supervisingDeptID == 0)
                {
                    return SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraHocKy;
                }
                else
                {
                    return SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraHocKy;
                }
            }
            else if (reportType == SystemParamsInFile.AdditionReport.HLM)
            {
                if (supervisingDeptID == 0)
                {
                    return SystemParamsInFile.REPORT_SGD_THCS_ThongKeHLM;
                }
                else
                {
                    return SystemParamsInFile.REPORT_PGD_THCS_ThongKeHLM;
                }
            }

            return "";
        }

       
        public JsonResult LoadSemester(int? reportTypeId)
        {
            GlobalInfo global = new GlobalInfo();
            string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
            int curSemester = 1;
            AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(global.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
            if (reportTypeId <= SystemParamsInFile.AdditionReport.DIEM_KIEM_TRA_HOC_KY)
            {
                return Json(new SelectList(CommonList.Semester(), "key", "value",curSemester));
            }
            else
            {
                return Json(new SelectList(CommonList.SemesterAndAll(), "key", "value",curSemester));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? academicYearID, int? educationLevelID, int? semester, int? valueRadio)
        {
            IEnumerable<SubjectCatBO> lst = new List<SubjectCatBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            dic["Year"] = academicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["ProvinceID"] = global.ProvinceID;
            dic["SupervisingDeptID"] = global.SupervisingDeptID;
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            if (valueRadio <= 2)
            {
                dic["IsCommenting"] = 0;   
            }
            lst = SupervisingDeptBusiness.SearchSubject(dic);
            return Json(new SelectList(lst, "SubjectCatID", "DisplayName"));

        }

        [ValidateAntiForgeryToken]
        public JsonResult CreatedReport(SearchViewModel data)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = global.ProvinceID;
            int superVisingDeptId=0;
            if (!global.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                SearchInfo["DistrictID"] = global.DistrictID.HasValue ? global.DistrictID.Value : 0;
                SearchInfo["SuperVisingDeptID"] = 1;
                superVisingDeptId=1;
                
            }
            else
            {
                //so
                SearchInfo["DistrictID"] = 0;
                SearchInfo["SuperVisingDeptID"] = 0;
                superVisingDeptId=0;
            }
            
            SearchInfo["Year"] = data.YearID;
            SearchInfo["ReportCode"] = GetReportCode(superVisingDeptId, data.ReportType);
            SearchInfo["MarkType"] = data.ReportType;
            SearchInfo["Semester"] = data.Semester;
            SearchInfo["SubjectID"] = data.SubjectCatID;
            SearchInfo["EducationLevelID"] = data.EducationLevelID;
            SearchInfo["IsFemale"] = data.IsGenre;
            SearchInfo["IsEthnic"] = data.IsEthnic;
            SearchInfo["IsFemaleAndEthnic"] = data.IsGenreEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            SearchInfo["UnitId"] = global.SupervisingDeptID;
            MarkStatisticBusiness.CreateMarkStatistic(SearchInfo);
            return Json(new { Type = "success" });
        }
        #region "Xuat excel diem kiem tra dinh ky,hoc ky, HLM"
        public FileResult ExportReport(int reportType, int year, int semester,int educationLevelID, int subjectId, bool isFemale, bool isEthnic, bool isFemaleEthnic)
        {
            if (subjectId == 0)
            {
                throw new Exception("Chưa khai báo môn học");
            }
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = global.ProvinceID;
            int superVisingDeptId = 0;
            if (!global.IsSuperVisingDeptRole)
            {
                //Tong hop theo quan/huyen
                SearchInfo["DistrictID"] = global.DistrictID.HasValue ? global.DistrictID.Value : 0;
                SearchInfo["SuperVisingDeptID"] = 1;
                superVisingDeptId = 1;
            }
            else
            {
                //so
                SearchInfo["DistrictID"] = 0;
                SearchInfo["SuperVisingDeptID"] = 0;
                superVisingDeptId = 0;
            }

            string reportCode = GetReportCode(superVisingDeptId, reportType);
            SearchInfo["ReportCode"] = reportCode;
            SearchInfo["MarkType"] = reportType;
            SearchInfo["Semester"] = semester;
            SearchInfo["SubjectID"] = subjectId;
            SearchInfo["EducationLevelID"] = educationLevelID;
            SearchInfo["IsFemale"] = isFemale;
            SearchInfo["IsEthnic"] = isEthnic;
            SearchInfo["IsFeMaleAndEthnic"] = isFemaleEthnic;
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            SearchInfo["ReportType"] =reportType;
            SearchInfo["UnitId"] = global.SupervisingDeptID;
            SearchInfo["Year"] = year;
            Stream excel = MarkStatisticBusiness.ExportMarkStatistic(SearchInfo);
            SubjectCat objSubject = SubjectCatBusiness.Find(subjectId);
            string subjectName = (objSubject == null) ? "" :Utils.Utils.StripVNSignAndSpace(objSubject.SubjectName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = String.Format("{0}_Khoi{1}_HK{2}_{3}.xls", reportCode, educationLevelID, semester,subjectName);
            result.FileDownloadName = fileName;
            return result;            
        }

        #endregion
    }
}
