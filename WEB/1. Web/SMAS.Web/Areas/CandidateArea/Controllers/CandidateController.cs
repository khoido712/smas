﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.CandidateArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using System.Web.Script.Serialization;

namespace SMAS.Web.Areas.CandidateArea.Controllers
{
    public class CandidateController : BaseController
    {
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public CandidateController(ICandidateBusiness candidateBusiness
            , IExaminationBusiness ExaminationBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IExaminationSubjectBusiness ExaminationSubjectBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.CandidateBusiness = candidateBusiness;
            this.ExaminationBusiness = ExaminationBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        //
        // GET: /Candidate/

        public ActionResult Index()
        {
            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here

            //IEnumerable<CandidateViewModel> lst = this._Search(SearchInfo);
            //ViewData[CandidateConstants.LIST_CANDIDATE] = lst;
            return View();
        }

        //
        // GET: /Candidate/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["EducationLevelID1"] = frm.EducationLevel1;

            SearchInfo["ClassID"] = frm.ClassID;


            SearchInfo["PupilName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Genre"] = frm.Genre;

            //
            Examination exam = ExaminationBusiness.Find(frm.ExaminationID);
            if (exam.UsingSeparateList == 1)
            {
                SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID;
            }

            List<CandidateViewModel> lst = this._Search(SearchInfo).ToList();
            lst = lst.OrderBy(p => p.FullName.getOrderingName(p.EthnicCode)).ToList();

            //Nếu kì thi không chia theo môn thì distinct danh sách theo học sinh
            if (exam.UsingSeparateList == 0)
            {
                List<CandidateViewModel> list = new List<CandidateViewModel>();
                lst.ForEach(u =>
                {
                    if (!list.Select(o => o.PupilID).Contains(u.PupilID))
                    {
                        list.Add(u);
                    }
                });
                ViewData[CandidateConstants.LIST_CANDIDATE] = list;

            }
            else
            {
                ViewData[CandidateConstants.LIST_CANDIDATE] = lst;
            }

            if (lst.Count() > 0)
            {
                ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE] = true && global.IsCurrentYear;
            }
            else
            {
                ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE] = false;
            }

            //add new button
            //  if(frm.EducationLevel!=null && frm.ExaminationID !=null)
            //{


            //Nếu Examination(cboExamination.Value).UsingSeparateList = 0  thì cboExaminationSubject phải được chọn
            if (exam.UsingSeparateList == 1)
            {
                if (frm.ExaminationSubjectID == null)
                {
                    throw new BusinessException("Candidate_Label_ExaminationSubjectNotChoice");
                }
            }

            if (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_CREATED)
            {
                if (frm.ExaminationSubjectID.HasValue)
                {
                    ViewData[CandidateConstants.ENABLE_ADD_NEW_BUTTON] = global.IsCurrentYear;
                }
                else
                {
                    ViewData[CandidateConstants.ENABLE_ADD_NEW_BUTTON] = (exam.UsingSeparateList == 0) && global.IsCurrentYear;
                }
            }
            else
            {
                ViewData[CandidateConstants.ENABLE_ADD_NEW_BUTTON] = false;
            }

            //}
            ViewData[CandidateConstants.IS_CURRENT_YEAR] = (lst.Count() > 0) && global.IsCurrentYear;
            ViewData[CandidateConstants.GRID_HAS_DATA] = (lst.Count() > 0);
            ViewData[CandidateConstants.NAME_LIST_CODE_GRID_HAS_DATA] = false;

            //
            if (frm.ClassID.HasValue)
            {
                ViewData[CandidateConstants.CREATE_GRID_PAGING] = false;
            }
            else
            {
                ViewData[CandidateConstants.CREATE_GRID_PAGING] = true;
            }
            return PartialView("_List");
        }


        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            Candidate candidate = new Candidate();
            TryUpdateModel(candidate);
            Utils.Utils.TrimObject(candidate);

            this.CandidateBusiness.Insert(candidate);
            this.CandidateBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int CandidateID)
        {
            Candidate candidate = this.CandidateBusiness.Find(CandidateID);
            CheckPermissionForAction(candidate.ClassID.Value, "ClassProfile");

            TryUpdateModel(candidate);
            Utils.Utils.TrimObject(candidate);
            this.CandidateBusiness.Update(candidate);
            this.CandidateBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Candidate candidate = this.CandidateBusiness.Find(id);
            CheckPermissionForAction(candidate.ClassID.Value, "ClassProfile");
            this.CandidateBusiness.Delete(id);
            this.CandidateBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<CandidateViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<Candidate> query = this.CandidateBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            int EducationLevelID = SMAS.Business.Common.Utils.GetInt(SearchInfo, "EducationLevelID1");
            IQueryable<CandidateViewModel> lst = (from p in query
                                                  join pp in PupilProfileBusiness.All on p.PupilID equals pp.PupilProfileID
                                                  join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                                                  where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                  select new CandidateViewModel
                                                    {
                                                        CandidateID = p.CandidateID,
                                                        ExaminationID = p.ExaminationID,
                                                        // SubjectID = o.SubjectID,
                                                        NamedListNumber = p.NamedListNumber,
                                                        RoomID = p.RoomID,
                                                        ClassID = p.ClassID,
                                                        EducationLevelOfPupilID = cp.EducationLevelID,
                                                        ClassName = cp.DisplayName,
                                                        PupilID = p.PupilID,
                                                        OrderNumber = p.OrderNumber,
                                                        Mark = p.Mark,
                                                        EducationLevelID = p.EducationLevelID,
                                                        PupilCode = p.PupilCode,
                                                        Judgement = p.Judgement,
                                                        NamedListCode = p.NamedListCode,
                                                        HasReason = p.HasReason,
                                                        ReasonDetail = p.ReasonDetail,
                                                        FullName = pp.FullName,
                                                        Name = pp.Name,
                                                        BirthDate = pp.BirthDate,
                                                        BirthPlace = pp.BirthPlace,
                                                        GenreName = (pp.Genre == 1) ? male : female,
                                                        EthnicCode = pp.Ethnic != null ? pp.Ethnic.EthnicCode : string.Empty
                                                    })
                                                  .Distinct();
            return lst;
        }

        #region SetViewData

        private void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            List<Examination> lsE = ExaminationBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value}
            }).OrderByDescending(o => o.ToDate).ToList();
            string examID = Request["ExaminationID"];
            int ExaminationID = 0;
            if (examID == null || examID.Trim().Equals(string.Empty) || !int.TryParse(examID, out ExaminationID))
            {
                ViewData[CandidateConstants.LISTEXAMINATION] = new SelectList(lsE, "ExaminationID", "Title");
                ViewData[CandidateConstants.LISTEDUCATIONLEVELEXAM] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[CandidateConstants.LISTEXAMINATION] = new SelectList(lsE, "ExaminationID", "Title", ExaminationID);
                // Lay thong tin khoi theo ky thi
                List<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",global.AcademicYearID.Value},
                    {"AppliedLevel",global.AppliedLevel.Value},
                    {"ExaminationID",ExaminationID}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
                ViewData[CandidateConstants.LISTEDUCATIONLEVELEXAM] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            ViewData[CandidateConstants.LISTEDUCATIONLEVEL] = new SelectList(global.EducationLevels, "EducationLevelID", "Resolution");
            ViewData[CandidateConstants.LISTGENRE] = new SelectList(CommonList.GenreAndAll(), "key", "value");

            //mac dinh gia tri cac cocboobox khac la rong
            ViewData[CandidateConstants.LISTEXAMINATIONSUBJECT] = new SelectList(new string[] { });
            ViewData[CandidateConstants.LISTCLASS] = new SelectList(new string[] { });
            ViewData[CandidateConstants.LIST_CREATE_CLASS] = new SelectList(new string[] { });
            ViewData[CandidateConstants.CREATE_GRID_PAGING] = true;

            //check xem nut add new co disable?
            ViewData[CandidateConstants.ENABLE_ADD_NEW_BUTTON] = false;
            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE] = false;
            ViewData[CandidateConstants.ENABLE_COMBOBOX_CLASS] = false;

            //--------------------------------------------
            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = false;
            ViewData[CandidateConstants.NAME_LIST_CODE_GRID_HAS_DATA] = false;
            ViewData[CandidateConstants.GRID_HAS_DATA] = false;
            ViewData[CandidateConstants.IS_CURRENT_YEAR] = false;

            ViewData[CandidateConstants.ENABLE_EXAMINATIONSUBJECT_COMBO] = true;
        }

        #endregion SetViewData

        #region ho tro

        public IQueryable<ExaminationSubject> GetExaminationSubject(int? ExaminationID, int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            return ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"EducationLevelID",EducationLevelID},
                {"ExaminationID",ExaminationID}
            });
        }

        public IQueryable<ClassProfile> GetClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            return ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"EducationLevelID",EducationLevelID}
            });
        }

        public IQueryable<PupilOfClass> GetCreateGrid(int ExaminationID,int ExaminationLevelID, int EducationLevelID, int? ClassID, int? ExaminationSubjectID)
        {
            // Tim danh sach hoc sinh trong lop duoc chon
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            search["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            search["EducationLevelID"] = EducationLevelID;
            search["ClassID"] = ClassID;
            if (ClassID != null && ClassID > 0)
            {
                search["Check"] = "Checked";
            }
            else
            {
                search["CheckWithClass"] = "Checked";
            }
            IQueryable<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, search);
            // Tim danh sach thi sinh da lap
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["ClassID"] = ClassID;
            if (ExaminationBusiness.Find(ExaminationID).UsingSeparateList == 1)
            {
                dic["ExaminationSubjectID"] = ExaminationSubjectID;
            }
            dic["ExaminationID"] = ExaminationID;
            dic["EducationLevelID"] = ExaminationLevelID;
            IQueryable<Candidate> ListCandidate = CandidateBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            // Lay danh sach cac hoc sinh chua duoc lap danh sach thi sinh
            IQueryable<PupilOfClass> lsPOCtoGrid = ListPupilOfClass
                .Where(o => !ListCandidate.Any(u => u.PupilID == o.PupilID))
                .Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                .Where(o => o.PupilProfile.IsActive == true);

            if (ClassID.HasValue && ClassID > 0)
            {
                lsPOCtoGrid = lsPOCtoGrid.OrderBy(o => o.OrderInClass);
            }
            return lsPOCtoGrid;
        }

        #endregion ho tro

        #region ajax load cocboobox

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEduationLevelExam(int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",ExaminationID}
            }).Select(o => o.EducationLevel).Distinct();
            lsEducationLevel = lsEducationLevel.OrderBy(o => o.EducationLevelID);
            return Json(new SelectList(lsEducationLevel, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AjaxLoadExaminationSubject(int EducationLevelID, int ExaminationID)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<ExaminationSubject> lsES = GetExaminationSubject(ExaminationID, EducationLevelID);
            IQueryable<ExaminationSubjectObject> lsESO = lsES
                .OrderBy(s=>s.SubjectCat.OrderInSubject) // Sap xep theo thu tu mon hoc trong bang subject_cat
                .Select(o => new ExaminationSubjectObject
            {
                DurationInMinute = o.DurationInMinute,
                EducationLevelID = o.EducationLevelID,
                ExaminationID = o.ExaminationID,
                ExaminationSubjectID = o.ExaminationSubjectID,
                HasDetachableHead = o.HasDetachableHead,
                IsLocked = o.IsLocked,
                OrderNumberPrefix = o.OrderNumberPrefix,
                StartTime = o.StartTime,
                SubjectID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName
            });
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            int usingSeparateList = exam.UsingSeparateList;
            ViewData[CandidateConstants.ENABLE_EXAMINATIONSUBJECT_COMBO] = exam.UsingSeparateList == 1;


            return Json(new SelectList(lsESO, "ExaminationSubjectID", "SubjectName"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            if (EducationLevelID.HasValue)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevelID.Value)
                    .OrderBy(o => o.DisplayName)
                    .ToList();
                return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AjaxHasExaminationSubject(int ExaminationID)
        {
            if (ExaminationBusiness.HasExaminationSubject(ExaminationID))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_TransferSuccessMessage")));
            }

            else
            {
                return Json(new JsonMessage(Res.Get("Candidate_Label_ExaminationError"), "error"));
            }
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadCreateGrid(int ExaminationID, int ExaminationLevelID, int? EducationLevelID, int? ClassID, int? ExaminationSubjectID)
        {
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            // Cho phép phân trang đối với cả việc chọn từng lớp => comment đoạn code bên dưới
            if (ClassID.HasValue && ClassID > 0)
            {
                ViewData[CandidateConstants.CREATE_GRID_PAGING] = false;
            }
            else
            {
                ViewData[CandidateConstants.CREATE_GRID_PAGING] = true;
            }
            // Neu khoi hoc khong co gia tri thi gan gia tri mac dinh cho no bang voi khoi thi
            if (!EducationLevelID.HasValue)
            {
                EducationLevelID = ExaminationLevelID;
            }
            IQueryable<PupilOfClass> lsPOCtoGrid = this.GetCreateGrid(ExaminationID, ExaminationLevelID, EducationLevelID.Value, ClassID, ExaminationSubjectID);
                List<PupilOfClassViewModel> lsPOCVM = (from poc in lsPOCtoGrid
                                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                       join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                       where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                       select new PupilOfClassViewModel
                                                       {
                                                           AcademicYearID = poc.AcademicYearID,
                                                           EducationLevelID = cp.EducationLevelID,
                                                           ClassOrderNumber = cp.OrderNumber ?? 0,
                                                           ClassID = poc.ClassID,
                                                           PupilID = poc.PupilID,
                                                           PupilOfClassID = poc.PupilOfClassID,
                                                           SchoolID = poc.SchoolID,
                                                           Status = poc.Status,
                                                           PupilCode = pp.PupilCode,
                                                           OrderOfPupilInClass = poc.OrderInClass??0,
                                                           FullName = pp.FullName,
                                                           Name = pp.Name,
                                                           EthnicCode = pp.Ethnic.EthnicCode,
                                                           BirthDate = pp.BirthDate,
                                                           BirthPlace = pp.BirthPlace,
                                                           GenreName = (pp.Genre == 1) ? male : female,
                                                           ClassName = cp.DisplayName,
                                                       })
                                                       .ToList()
                                                       .OrderBy(o => o.EducationLevelID) // Sap theo khoi
                                                       .ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName) // Sap theo lop
                                                        .ThenBy(p=> p.OrderOfPupilInClass)
                                                        .ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName.getOrderingName(p.EthnicCode))) // Sap theo ten hoc sinh
                                                       .ToList();    
            ViewData[CandidateConstants.LIST_PUPIL] = lsPOCVM;
            return PartialView("_ListPupil");
        }

        #endregion ajax load cocboobox

        #region CreateListPupil

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateListPupil(int? Create_Class, int EducationLevelID, int? SubjectID
            , int[] checkedPupil , string checkAllPupil, int? ExaminationID, int? Create_EducaionLevel)
        {
            GlobalInfo global = _globalInfo;

            if (checkAllPupil == null && checkedPupil == null)
            {
                return Json(new JsonMessage("Chưa chọn thí sinh.","error"));
            }

            if (!ExaminationID.HasValue)
            {
                List<object> Params = new List<object>();
                Params.Add("Candidate_Label_Examination");
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
            else
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
                if (exam.UsingSeparateList == 1)
                {
                    if (!SubjectID.HasValue || SubjectID == 0)
                    {
                        List<object> Params = new List<object>();
                        Params.Add("Candidate_Label_ExaminationSubject");
                        throw new BusinessException("Common_Validate_NotAvailable", Params);
                    }
                }
            }
            SubjectID = SubjectID ?? 0;
            // Neu khoi hoc khong co gia tri thi gan gia tri mac dinh cho no bang voi khoi thi
            if (!Create_EducaionLevel.HasValue)
            {
                Create_EducaionLevel = EducationLevelID;
            }
            // Lay danh sach hoc sinh duoc them vao
            IQueryable<PupilOfClass> lsPOCVM;
            if (checkAllPupil != null) // Check chon tat ca hoc sinh
                                                {
                lsPOCVM = this.GetCreateGrid(ExaminationID.Value, EducationLevelID, Create_EducaionLevel.Value, Create_Class, SubjectID);
            }
            else // Check chon tung hoc sinh
            {
                lsPOCVM = this.PupilOfClassBusiness.All.Where(poc => checkedPupil.Contains(poc.PupilOfClassID));
            }
            CandidateBusiness.InsertGroupCandidate(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationID.Value
                , EducationLevelID, SubjectID.Value, lsPOCVM);
            CandidateBusiness.Save();
            return Json(new JsonMessage(Res.Get("Candidate_Label_AddListPupilSuccess")));
        }

        #endregion CreateListPupil

        #region DeleteCandidate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCandidate(int ExaminationID)
        {
            var listInt = new JavaScriptSerializer().Deserialize<List<int>>(Request["checkedCandidate"]);
            GlobalInfo global = new GlobalInfo();
            CandidateBusiness.Delete(global.SchoolID.Value, ExaminationID, (int)global.AppliedLevel.Value, listInt);
            CandidateBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion DeleteCandidate

        #region name list code

        public ActionResult RedirectNameListCodePage()
        {
            SetViewData();
            ViewData[CandidateConstants.NAME_LIST_CODE_PUPILCOUNT] = 0;
            ViewData[CandidateConstants.NAME_LIST_CODE_GRID_HAS_DATA] = false;
            return PartialView("NameListCode");
        }


        public PartialViewResult SearchNameListCode(NameListCodeSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["ExaminationID"] = frm.ExaminationID;
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["ExaminationSubjectID"] = frm.ExaminationSubjectID;

            Examination exam = ExaminationBusiness.Find(frm.ExaminationID);
            if (exam.UsingSeparateList == 0) //mon hoc co tinh chung thi sinh =>examsubject = null =>phai dat 1 id nao do
            {
                var lsExSu = GetExaminationSubject(frm.ExaminationID, frm.EducationLevel);
                SearchInfo["ExaminationSubjectID"] = lsExSu.FirstOrDefault() != null ? lsExSu.FirstOrDefault().ExaminationSubjectID : 0;
            }

            List<CandidateViewModel> lst = this._Search(SearchInfo).ToList();
            lst.Sort();

            ViewData[CandidateConstants.LIST_CANDIDATE_NAMELISTCODE] = lst;
            ViewData[CandidateConstants.NAME_LIST_CODE_PUPILCOUNT] = lst.Count();

            //  if(frm.EducationLevel!=null && frm.ExaminationID !=null)
            {

                //Nếu Examination(cboExamination.Value).UsingSeparateList != 0  thì cboExaminationSubject phải được chọn
                if (exam.UsingSeparateList != 0)
                {
                    if (frm.ExaminationSubjectID == null)
                    {
                        throw new BusinessException("Candidate_Label_ExaminationSubjectNotChoice");
                    }
                }

                if (global.IsCurrentYear)
                {
                    ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = true && global.IsCurrentYear;
                }
                else
                {
                    ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = false;
                }

                if (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_CREATED)
                {
                    if (exam.UsingSeparateList != 0)
                    {
                        if (frm.ExaminationSubjectID.HasValue && lst.Count() > 0)
                        {
                            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = true && global.IsCurrentYear;
                        }
                        else
                        {
                            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = false;
                        }
                    }
                    else
                    {
                        if (lst.Count() > 0)
                        {
                            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = true && global.IsCurrentYear;
                        }
                        else
                        {
                            ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = false;
                        }
                    }

                }
                else
                {
                    ViewData[CandidateConstants.ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID] = false;
                }
            }

            ViewData[CandidateConstants.NAME_LIST_CODE_GRID_HAS_DATA] = lst.Count() > 0;
            ViewData[CandidateConstants.GRID_HAS_DATA] = false;

            //
            return PartialView("_ListNameListCode");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateNameListCode(int? EducationLevel, int? ExaminationID, int? ExaminationSubjectID, string chkLength, string chkOrderByClass
                    , string chkOrderByFullName, string chkPrefix, string txtLength, string txtPrefix)
        {
            if (!ExaminationID.HasValue || !EducationLevel.HasValue)
            {
                throw new BusinessException("Common_Error_InternalError");
            }
            if (!ExaminationSubjectID.HasValue)
            {
                ExaminationSubjectID = 0;
            }
            GlobalInfo global = new GlobalInfo();
            Examination exam = ExaminationBusiness.Find(ExaminationID);

            //Lấy danh sách thông tin thí sinh ListCandidate :
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"ExaminationID",ExaminationID},
                {"EducationLevelID",EducationLevel}
            };
            if (exam.UsingSeparateList == 1)
            {
                dic["ExaminationSubjectID"] = ExaminationSubjectID;
            }
            IQueryable<Candidate> lsC = CandidateBusiness.SearchBySchool(global.SchoolID.Value, dic);

            //* Nếu tồn tại 1 bản ghi có NamedListCode != “” thì đưa ra thông báo xác nhận lại cho người dùng:
            IQueryable<Candidate> lsCode = lsC.Where(o => o.NamedListCode.Trim().Length != 0);
            if (lsCode.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("Candidate_Label_ConfirmReNumbered"), "confirm"));
            }
            else
            {
                Step3(EducationLevel, ExaminationID, ExaminationSubjectID, chkLength, chkOrderByClass
                       , chkOrderByFullName, chkPrefix, txtLength, txtPrefix);
                return Json(new JsonMessage(Res.Get("Candidate_Label_NumberedSuccess")));
            }
        }

        public void Step3(int? EducationLevel, int? ExaminationID, int? ExaminationSubjectID, string chkLength, string chkOrderByClass
                    , string chkOrderByFullName, string chkPrefix, string txtLength, string txtPrefix)
        {
            GlobalInfo global = new GlobalInfo();

            //buoc 3
            int sLength = 0;
            if (chkLength == null)
            {
                chkLength = "";
            }
            if (chkOrderByClass != null)
            {
                chkOrderByClass = chkOrderByClass.Trim();
            }
            if (chkOrderByFullName != null)
            {
                chkOrderByFullName = chkOrderByFullName.Trim();
            }
            if (txtLength == "0")
            {
                throw new BusinessException("Candidate_NameListCode_Prefix0");
            }

            if (string.IsNullOrEmpty(txtLength))
            {
                txtLength = "0";
            }

            if (chkLength != null && chkLength.Trim().Length > 0)
            {
                sLength = int.Parse(txtLength);
            }
            if (chkPrefix == null)
            {
                txtPrefix = "";
            }

            if (!ExaminationSubjectID.HasValue)
            {
                ExaminationSubject examSub = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { 
                {"ExaminatonID",ExaminationID }
                ,{"AcademicYearID",global.AcademicYearID}
                }).FirstOrDefault();

                if (examSub == null)
                {
                    throw new BusinessException("Candidate_Label_NoExamSubject");
                }

                ExaminationSubjectID = examSub.ExaminationSubjectID;
            }
            CandidateBusiness.NameListCode(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationID.Value,
                EducationLevel.Value, ExaminationSubjectID.Value, txtPrefix, sLength, (chkOrderByClass != null && !chkOrderByClass.Equals(string.Empty)),
                (chkOrderByFullName != null && !chkOrderByFullName.Equals(string.Empty)));
            CandidateBusiness.Save();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GoStep3AfterConfirm(int? EducationLevel, int? ExaminationID, int? ExaminationSubjectID, string chkLength, string chkOrderByClass
                    , string chkOrderByFullName, string chkPrefix, string txtLength, string txtPrefix)
        {
            Step3(EducationLevel, ExaminationID, ExaminationSubjectID, chkLength, chkOrderByClass
           , chkOrderByFullName, chkPrefix, txtLength, txtPrefix);
            return Json(new JsonMessage(Res.Get("Candidate_Label_NumberedSuccess")));
        }


        public ActionResult GetCountPupil(int? ExaminationID, int? EducationLevel, int? ExaminationSubjectID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["EducationLevelID"] = EducationLevel;
            Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
            if (exam.UsingSeparateList == 0)
            {
                var lsExamSub1 = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevel}
                    ,{"ExaminationID",ExaminationID}
                }).Select(o => o.ExaminationSubjectID);
                if (lsExamSub1.Count() > 0)
                {
                    SearchInfo["ExaminationSubjectID"] = lsExamSub1.FirstOrDefault();
                }
            }
            else
            {
                SearchInfo["ExaminationSubjectID"] = ExaminationSubjectID;
            }

            IQueryable<Candidate> lst = this.CandidateBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            ViewData[CandidateConstants.NAME_LIST_CODE_PUPILCOUNT] = lst.Count();
            return PartialView("_CreateNameListCode");
        }

        public FileResult ExportExcelNameListCode()
        {
            CandidateBO cbo = new CandidateBO();

            int ExaminationID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationID"]);
            int EducationLevel = SMAS.Business.Common.Utils.GetInt(Request["EducationLevel"]);
            int ExaminationSubjectID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationSubjectID"]);
            cbo.AcademicYearID = _globalInfo.AcademicYearID.Value;
            cbo.AppliedLevel = _globalInfo.AppliedLevel.Value;
            cbo.EducationLevel = EducationLevel;
            cbo.ExaminationID = ExaminationID;
            cbo.ExaminationSubjectID = ExaminationSubjectID;
            cbo.SchoolID = _globalInfo.SchoolID.Value;
            cbo.SupervisingDeptID = _globalInfo.SupervisingDeptID.Value;
            string fileName = "";
            Stream excel = CandidateBusiness.CreateSBD(cbo, out fileName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        #endregion name list code

        #region report

        public FileResult ExportExcel()
        {
            CandidateBO cbo = new CandidateBO();

            int ExaminationID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationID"]);
            int EducationLevel = SMAS.Business.Common.Utils.GetInt(Request["EducationLevel"]);
            int ExaminationSubjectID = SMAS.Business.Common.Utils.GetInt(Request["ExaminationSubjectID"]);
            int ClassID = SMAS.Business.Common.Utils.GetInt(Request["ClassID"]);
            string FullName = SMAS.Business.Common.Utils.GetString(Request["FullName"]);
            string PupilCode = SMAS.Business.Common.Utils.GetString(Request["PupilCode"]);
            string Genre = SMAS.Business.Common.Utils.GetString(Request["Genre"]);

            cbo.AcademicYearID = _globalInfo.AcademicYearID.Value;
            cbo.AppliedLevel = _globalInfo.AppliedLevel.Value;
            if (ClassID != 0)
            {
                cbo.ClassID = ClassID;
            }
            cbo.EducationLevel = EducationLevel;
            cbo.ExaminationID = ExaminationID;
            cbo.ExaminationSubjectID = ExaminationSubjectID;
            cbo.FullName = FullName;
            cbo.Genre = Genre;
            cbo.PupilCode = PupilCode;
            cbo.SchoolID = _globalInfo.SchoolID.Value;
            string fileName = "";
            Stream excel = CandidateBusiness.CreateDSTS(cbo, out fileName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }

        #endregion report

        #region disabled or enabled combo box


        public JsonResult DisabledOrEnabledExaminationSubject(int? ExaminationID)
        {
            if (ExaminationID.HasValue)
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
                string returnMessage = exam.UsingSeparateList == 0 ? "True" : "False";
                return Json(new JsonMessage(returnMessage));
            }
            else
            {
                return Json(new JsonMessage("False"));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult IsCandidateFromMultipleLevel(int? ExaminationID)
        {
            if (ExaminationID.HasValue)
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
                string returnMessage = exam.CandidateFromMultipleLevel == 0 ? "True" : "False";
                return Json(new JsonMessage(returnMessage));
            }
            else
            {
                return Json(new JsonMessage("False"));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult CheckEnableAddNew(int? ExaminationID, int? EducationLevelID, int? ExaminationSubjectID)
        {
            GlobalInfo global = new GlobalInfo();

            if (ExaminationID.HasValue && EducationLevelID.HasValue)
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
                if (exam.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_CREATED)
                    return Json(0);
                if (exam.UsingSeparateList == 0 && global.IsCurrentYear)
                    return Json(1);
                else if (exam.UsingSeparateList == 1 && global.IsCurrentYear && ExaminationSubjectID != null && ExaminationSubjectID != 0)
                    return Json(1);
                else
                    return Json(0);
            }
            else
            {
                return Json(0);
            }
        }
        #endregion disabled or enabled combo box
    }
}
