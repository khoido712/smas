﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CandidateArea
{
    public class CandidateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CandidateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CandidateArea_default",
                "CandidateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}