/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.CandidateArea
{
    public class CandidateConstants
    {
        public const string LIST_CANDIDATE = "listCandidate";
        public const string LISTEXAMINATION = "LISTEXAMINATION";
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTEDUCATIONLEVELEXAM = "LISTEDUCATIONLEVELEXAM";
        public const string LISTEXAMINATIONSUBJECT = "LISTEXAMINATIONSUBJECT";
        public const string LISTCLASS = "LISTCLASS";
        public const string LISTGENRE = "LISTGENRE";
        public const string LIST_PUPIL = "LIST_PUPIL";
        public const string LIST_CREATE_CLASS = "LIST_CREATE_CLASS";
        public const string CREATE_GRID_PAGING = "CREATE_GRID_PAGING";
        public const string ENABLE_ADD_NEW_BUTTON = "ENABLE_ADD_NEW_BUTTON";
        public const string ENABLE_BUTTON_NAMELISTCODE = "ENABLE_BUTTON_NAMELISTCODE";
        public const string ENABLE_COMBOBOX_CLASS = "ENABLE_COMBOBOX_CLASS";

        //----------------------------------------------------------------
        public const string ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID = "ENABLE_BUTTON_NAMELISTCODE_IN_NAMELISTCODEGRID";

        public const string LIST_CANDIDATE_NAMELISTCODE = "LIST_CANDIDATE_NAMELISTCODE";
        public const string NAME_LIST_CODE_PUPILCOUNT = "NAME_LIST_CODE_PUPILCOUNT";

        public const string GRID_HAS_DATA = "GRID_HAS_DATA";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string NAME_LIST_CODE_GRID_HAS_DATA = "NAME_LIST_CODE_GRID_HAS_DATA";

        public const string ENABLE_EXAMINATIONSUBJECT_COMBO = "ENABLE_EXAMINATIONSUBJECT_COMBO";
    }
}