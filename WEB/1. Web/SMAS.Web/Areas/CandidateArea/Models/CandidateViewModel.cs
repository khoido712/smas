/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using SMAS.Web.Models.Attributes;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Collections.Generic;

namespace SMAS.Web.Areas.CandidateArea.Models
{
    public class CandidateViewModel : IComparable<CandidateViewModel>
    {
        public System.Int32 CandidateID { get; set; }
        [ResourceDisplayName("Candidate_Label_ExaminationID")]
        public System.Nullable<System.Int32> ExaminationID { get; set; }

        //public System.Nullable<System.Int32> SubjectID { get; set; }

        public System.Nullable<System.Int32> RoomID { get; set; }

        public System.Nullable<System.Int32> ClassID { get; set; }

        public System.Int32 EducationLevelOfPupilID { get; set; }

        public System.Int32 PupilID { get; set; }

        public System.Nullable<System.Int32> OrderNumber { get; set; }

        public System.Nullable<System.Decimal> Mark { get; set; }

        public System.Nullable<System.Int32> EducationLevelID { get; set; }

        public System.String Judgement { get; set; }

        public System.Nullable<System.Int32> NamedListNumber { get; set; }

        public System.Nullable<System.Boolean> HasReason { get; set; }

        public System.String ReasonDetail { get; set; }

        public System.String EthnicCode { get; set; }

        #region grid

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]
        public System.String BirthPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        public int OrderOfPupilInClass { get; set; }

        [ResourceDisplayName("Candidate_Label_NameListCode")]
        public string NamedListCode { get; set; }

        #endregion grid

        public int CompareTo(CandidateViewModel other)
        {
            return CandidateViewModel.Compare(this, other);
        }

        public static int Compare(CandidateViewModel candiadte1, CandidateViewModel candidate2)
        {
            int compareInNamedListNumber = Comparer<int?>.Default.Compare(candiadte1.NamedListNumber, candidate2.NamedListNumber);
            if (compareInNamedListNumber != 0) return compareInNamedListNumber;
            int compareInNamedListCode = string.Compare(candiadte1.NamedListCode, candidate2.NamedListCode);
            if (compareInNamedListCode != 0) return compareInNamedListCode;
            string thisNameForOrder = SMAS.Business.Common.Utils.SortABC(candiadte1.FullName.getOrderingName(candiadte1.EthnicCode));
            string otherNameForOrder = SMAS.Business.Common.Utils.SortABC(candidate2.FullName.getOrderingName(candidate2.EthnicCode));
            int compareInName = string.Compare(thisNameForOrder, otherNameForOrder, true);
            if (compareInName != 0) return compareInName;
            int compareInBirthDate = DateTime.Compare(candiadte1.BirthDate, candidate2.BirthDate);
            return compareInBirthDate;
        }
    }
}
