using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.CandidateArea.Models
{
    public class PupilOfClassViewModel
    {
        public System.Int32 PupilID { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int ClassID { get; set; }

        public int EducationLevelID { get; set; }

        public int? ClassOrderNumber { get; set; }

        public int PupilOfClassID { get; set; }

        public int Status { get; set; }

        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDate { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthPlace")]
        public System.String BirthPlace { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public string GenreName { get; set; }

        public int? OrderOfPupilInClass { get; set; }

        public string EthnicCode { get; set; }
    }
}
