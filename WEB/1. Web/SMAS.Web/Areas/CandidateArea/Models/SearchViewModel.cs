/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.CandidateArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_TitleChoice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEXAMINATION)]
        [AdditionalMetadata("OnChange", "onExaminationChange(this)")]
        public System.Int32 ExaminationID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevelChoice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEDUCATIONLEVELEXAM)]
        [AdditionalMetadata("OnChange", "onEducationLevelChange(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("Candidate_Label_Subject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "SelectRK")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEXAMINATIONSUBJECT)]
        [AdditionalMetadata("OnChange", "onExaminationSubjectChange(this)")]
        public int? ExaminationSubjectID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EduLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "onEducationLevelChange1(this)")]
        public int? EducationLevel1 { get; set; }

        [ResourceDisplayName("Candidate_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTCLASS)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("Candidate_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("Candidate_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("Candidate_Label_Genre")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTGENRE)]
        public string Genre { get; set; }
    }
}