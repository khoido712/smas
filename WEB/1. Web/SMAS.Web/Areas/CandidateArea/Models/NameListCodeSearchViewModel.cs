﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.CandidateArea.Models
{
    public class NameListCodeSearchViewModel
    {
        [ResourceDisplayName("Examination_Label_TitleChoice")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEXAMINATION)]
        [AdditionalMetadata("OnChange", "onNameListCodeExaminationChange(this)")]
        public System.Int32 ExaminationID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevelChoice")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEDUCATIONLEVELEXAM)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [AdditionalMetadata("OnChange", "onNameListCodeEducationLevelChange(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("Candidate_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", CandidateConstants.LISTEXAMINATIONSUBJECT)]
        [AdditionalMetadata("OnChange", "onNameListCodeExaminationSubjectChange(this)")]
        public int? ExaminationSubjectID { get; set; }
    }
}