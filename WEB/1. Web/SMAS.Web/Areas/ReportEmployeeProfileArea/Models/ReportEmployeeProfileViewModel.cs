﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportEmployeeProfileArea.Models
{
    public class ReportEmployeeProfileViewModel
    {
        public int? SchoolFaculty { get; set; }
        public int? Math { get; set; }
        [ResourceDisplayName("ReportEmployeeProfile_Label_SeniorYear")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SeniorYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime ReportDate { get; set; }
        public int rptReport { get; set; }
    }
}