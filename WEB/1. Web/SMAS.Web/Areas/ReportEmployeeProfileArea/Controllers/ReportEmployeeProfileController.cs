﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ReportEmployeeProfileArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;

namespace SMAS.Web.Areas.ReportEmployeeProfileArea.Controllers
{
    public class ReportEmployeeProfileController : BaseController
    {
        //
        // GET: /ReportEmployeeProfileArea/ReportEmployeeProfile/
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness;
        public ReportEmployeeProfileController(IProcessedReportBusiness ProcessedReportBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportEmployeeProfileBusiness = ReportEmployeeProfileBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData
        public void SetViewData()
        {
            ViewData[ReportEmployeeProfileConstants.CBO_FACULTY] = new List<SchoolFaculty>();
            
            
            
            
            //-	cboFaculty: : lấy danh sách từ SchoolFacultyBusiness.SearchBySchool(Dictionary) 
        //    với Dictionary[“SchoolID”] = UserInfo.SchoolID và Dictionary[“IsActive”] = 1. Thêm giá trị mặc định [Tất cả]
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["IsActive"] = true;

            IQueryable<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(Global.SchoolID.Value,Dictionary);
            if (lsFaculty.Count() > 0)
            {
                List<SchoolFaculty> lstFaculty = lsFaculty.ToList();
                lstFaculty = lstFaculty.OrderBy(o => o.FacultyName).ToList();
                ViewData[ReportEmployeeProfileConstants.CBO_FACULTY] = lstFaculty;
            }

            List<SelectListItem> lstMath = new List<SelectListItem>();

            lstMath.Add(new SelectListItem { Text = ">=", Value = "1" });
            lstMath.Add(new SelectListItem { Text = "<=", Value = "2" });
            ViewData[ReportEmployeeProfileConstants.CBO_MATH] = new SelectList(lstMath, "Value", "Text");

            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();


            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportEmployeeProfile_Label_ReportEmployeeList");
            viettelradio.cchecked = true;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportEmployeeProfile_Label_ReportEmployeeContact");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportEmployeeProfile_Label_ReportEmployeePayroll");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 3;
            lsradio.Add(viettelradio);

            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("ReportEmployeeProfile_Label_ReportEmployeeBySenior");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 4;
            lsradio.Add(viettelradio);

            ViewData[ReportEmployeeProfileConstants.LIST_RADIO] = lsradio.ToList();

        }
        #endregion


        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportEmployeeProfileViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            DateTime dt = frm.ReportDate;
            if (dt == DateTime.MinValue)
            {
                throw new BusinessException("ReportEmpoyeeProfile_Validate_DateTimeRequire");
            }
            if (dt > DateTime.Now)
            {
                throw new BusinessException("ReportEmpoyeeProfile_Validate_DateTimeNow");
            }
            switch (frm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BCGV;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeList(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeList(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeList(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 2: //Báo cáo danh sách cán bộ hợp đồng
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeContract(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeContract(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeContract(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 3: //Báo cáo danh sách cán bộ biên chế
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeePayroll(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeePayroll(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeePayroll(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 4: //Báo cáo danh sách cán bộ theo thâm niên
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        if (frm.Math != null)
                        {
                            reportEmployeeProfile.Compare = frm.Math.Value;
                        }
                        reportEmployeeProfile.SeniorYear = frm.SeniorYear;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeProfileBusiness.GetReportEmployeeSenior(reportEmployeeProfile);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeSenior(reportEmployeeProfile);
                            processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeSenior(reportEmployeeProfile, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportEmployeeProfileViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            switch (frm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeList(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeList(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 2: //Báo cáo danh sách cán bộ hợp đồng
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeContract(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeContract(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 3: //Báo cáo danh sách cán bộ biên chế
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeePayroll(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeePayroll(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 4: //Báo cáo danh sách cán bộ theo thâm niên
                    {
                        ReportEmployeeProfileBO reportEmployeeProfile = new ReportEmployeeProfileBO();
                        reportEmployeeProfile.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                        reportEmployeeProfile.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployeeProfile.FacultyID = frm.SchoolFaculty != null ? frm.SchoolFaculty.Value : 0;
                        reportEmployeeProfile.ReportDate = frm.ReportDate;
                        if (frm.Math != null)
                        {
                            reportEmployeeProfile.Compare = frm.Math.Value;
                        }
                        reportEmployeeProfile.SeniorYear = frm.SeniorYear;
                        Stream excel = ReportEmployeeProfileBusiness.CreateReportEmployeeSenior(reportEmployeeProfile);
                        ProcessedReport processedReport = ReportEmployeeProfileBusiness.InsertReportEmployeeSenior(reportEmployeeProfile, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID},
                {"AppliedLevel", GlobalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_HOP_DONG,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_BIEN_CHE,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_THAM_NIEN
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;

        }

    }
}
