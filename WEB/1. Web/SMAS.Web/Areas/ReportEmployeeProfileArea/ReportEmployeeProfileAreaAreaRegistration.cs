﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEmployeeProfileArea
{
    public class ReportEmployeeProfileAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportEmployeeProfileArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportEmployeeProfileArea_default",
                "ReportEmployeeProfileArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
