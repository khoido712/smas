﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportEmployeeProfileArea
{
    public class ReportEmployeeProfileConstants
    {

        public const string CBO_REPORTDATE = "CboReportDate";
        public const string CBO_FACULTY = "CboFaculty";
        public const string CBO_MATH = "CboMath";
        public const string LIST_RADIO = "List_Radio";
    }
}