﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticalPeriodicMarkOfPrimarySchoolArea
{
    public class StatisticalPeriodicMarkOfPrimarySchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticalPeriodicMarkOfPrimarySchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticalPeriodicMarkOfPrimarySchoolArea_default",
                "StatisticalPeriodicMarkOfPrimarySchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}