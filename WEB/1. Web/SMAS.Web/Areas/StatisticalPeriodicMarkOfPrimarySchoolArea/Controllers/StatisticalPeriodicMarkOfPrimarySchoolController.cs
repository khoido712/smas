﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.StatisticalPeriodicMarkOfPrimarySchoolArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.StatisticalPeriodicMarkOfPrimarySchoolArea.Controllers
{
    public class StatisticalPeriodicMarkOfPrimarySchoolController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        //
        // GET: /StatisticalPeriodicMarkOfPrimarySchoolArea/StatisticalPeriodicMarkOfPrimarySchool/
        public StatisticalPeriodicMarkOfPrimarySchoolController(IAcademicYearBusiness AcademicYearBusiness
            , ITrainingTypeBusiness TrainingTypeBusiness
            , IMarkStatisticBusiness MarkStatisticBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TrainingTypeBusiness = TrainingTypeBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region set view data

        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();

            //[AdditionalMetadata("ViewDataKey", StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTACADEMICYEAR)]
            List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
            List<ComboObject> lscbYear = new List<ComboObject>();
            foreach (var year in lsYear)
            {
                string value = year.ToString() + "-" + (year + 1).ToString();
                ComboObject cb = new ComboObject(year.ToString(), value);
                lscbYear.Add(cb);
            }
            ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTACADEMICYEAR] = new SelectList(lscbYear, "key", "value");

            // [AdditionalMetadata("ViewDataKey", StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTSEMESTER)]
            ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTSEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            //[AdditionalMetadata("ViewDataKey", StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTEDUCATIONLEVEL)]
            List<EducationLevel> lsEducationLevel = global.EducationLevels;

            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }

            //[AdditionalMetadata("ViewDataKey", StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTTRAININGTYPE)]
            IQueryable<TrainingType> lsTT = TrainingTypeBusiness.Search(new Dictionary<string, object>() { });
            if (lsTT.Count() > 0)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTTRAININGTYPE] = new SelectList(lsTT, "TrainingTypeID", "Resolution");
            }
            else
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTTRAININGTYPE] = new SelectList(new string[] { });
            }

            // [AdditionalMetadata("ViewDataKey", StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTSUBJECT)]
            ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LISTSUBJECT] = new SelectList(new string[] { });
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
            }
            else
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
            }

            ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
        }

        #endregion set view data

        #region load combo box

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? Year, int? EducationLevelID, int? SemesterID, int? TrainingTypeID)
        {
            GlobalInfo global = new GlobalInfo();
            int EducationLevel = EducationLevelID.HasValue ? EducationLevelID.Value : 0;
            int TrainingType = TrainingTypeID.HasValue ? TrainingTypeID.Value : 0;
            int? ProvinceID = null;
            int? SupervisingDeptID = null;
            if (global.IsSuperVisingDeptRole)
            {
                ProvinceID = global.ProvinceID;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                SupervisingDeptID = global.SupervisingDeptID;
            }
            int iYear = Year.HasValue ? Year.Value : 0;
            if (iYear == 0)
            {
                return Json(new SelectList(new string[] { }));
            }
            List<SubjectCat> lsSC = MarkStatisticBusiness.SearchSubjectHasReport(SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1, iYear, SemesterID,
                 true, EducationLevel, 0, TrainingType, SupervisingDeptID, ProvinceID);
            if (lsSC != null)
            {
                return Json(new SelectList(lsSC.ToList(), "SubjectCatID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion load combo box

        #region search

        public JsonResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.HasValue ? frm.AcademicYear.Value : 0;
                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);
                string ReportCode = "SGD_TH_TongHopDiemKiemTraDinhKy";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    List<MarkStatisticsByDistrictBO> lsmsbd = MarkStatisticBusiness.CreateSGDPriodicMarkStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",frm.Semester.HasValue ? frm.Semester.Value : 0},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"ReportCode",SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1},
                            {"SentToSupervisor",true},
                              {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                    if (lsmsbd != null)
                    {
                        List<GridViewModel> lsGVM = new List<GridViewModel>();
                        foreach (var item in lsmsbd)
                        {
                            GridViewModel gvm = new GridViewModel();
                            gvm.AboveAverage = item.TotalAboveAverage;
                            gvm.BelowAverage = item.TotalBelowAverage;
                            gvm.DistrictID = item.DistrictID;
                            gvm.DistrictName = item.DistrictName;
                            gvm.MarkAll = item.MarkInfo.Select(d => d.Value).ToList();
                            gvm.PercentAboveAverage = string.Format("{0:P2}", item.PercentAboveAverage);
                            gvm.PercentBelowAverage = string.Format("{0:P2}", item.PercentBelowAverage);
                            lsGVM.Add(gvm);
                        }
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                    }
                    else
                    {
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                    }
                    return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
                }
                else
                {
                    string type = JsonReportMessage.OLD;
                    return Json(new JsonReportMessage(entity, type));
                }
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);
                string ReportCode = "PGD_TH_TongHopDiemKiemTraDinhKy";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    List<MarkStatisticsOfSchoolBO> lsmsosbo = MarkStatisticBusiness.CreatePGDPriodicMarkStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",mrbo.Semester},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"DistrictID",global.DistrictID},
                            {"ReportCode",SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1},
                            {"SentToSupervisor",true},
                               {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                    if (lsmsosbo != null)
                    {
                        List<GridViewModel> lsGVM = new List<GridViewModel>();
                        foreach (var item in lsmsosbo)
                        {
                            GridViewModel gvm = new GridViewModel();
                            gvm.AboveAverage = item.TotalAboveAverage;
                            gvm.BelowAverage = item.TotalBelowAverage;
                            gvm.SchoolID = item.SchoolID;
                            gvm.SchoolName = item.SchoolName;
                            gvm.MarkAll = item.MarkInfo.Select(d => d.Value).ToList();
                            gvm.PercentAboveAverage = string.Format("{0:P2}", item.PercentAboveAverage);
                            gvm.PercentBelowAverage = string.Format("{0:P2}", item.PercentBelowAverage);
                            lsGVM.Add(gvm);
                        }
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                    }
                    else
                    {
                        ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                    }
                    return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
                }
                else
                {
                    string type = JsonReportMessage.OLD;
                    return Json(new JsonReportMessage(entity, type));
                }
            }
            return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
        }

        #endregion search

        #region report

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                 {"SupervisingDeptID",GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> {
               "SGD_TH_TongHopDiemKiemTraDinhKy","PGD_TH_TongHopDiemKiemTraDinhKy"
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);

                //lam nhu voi entity = null
                List<MarkStatisticsByDistrictBO> lsmsbd = MarkStatisticBusiness.CreateSGDPriodicMarkStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",mrbo.Semester},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",frm.TrainingType},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"ReportCode",SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1},
                            {"SentToSupervisor",true},
                              {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                if (lsmsbd != null)
                {
                    List<GridViewModel> lsGVM = new List<GridViewModel>();
                    foreach (var item in lsmsbd)
                    {
                        GridViewModel gvm = new GridViewModel();
                        gvm.AboveAverage = item.TotalAboveAverage;
                        gvm.BelowAverage = item.TotalBelowAverage;
                        gvm.DistrictID = item.DistrictID;
                        gvm.DistrictName = item.DistrictName;
                        gvm.TotalTest = item.TotalTest;
                        gvm.MarkAll = item.MarkInfo.Select(d => d.Value).ToList();
                        gvm.PercentAboveAverage = string.Format("{0:P2}", item.PercentAboveAverage);
                        gvm.PercentBelowAverage = string.Format("{0:P2}", item.PercentBelowAverage);
                        lsGVM.Add(gvm);
                    }
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                }
                else
                {
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                }
                return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);
                List<MarkStatisticsOfSchoolBO> lsmsosbo = MarkStatisticBusiness.CreatePGDPriodicMarkStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",frm.Semester.HasValue ? frm.Semester.Value : 0},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"DistrictID",global.DistrictID},
                            {"ReportCode",SystemParamsInFile.THONGKEDIEMKIEMTRADINHKYCAP1},
                            {"SentToSupervisor",true},
                            {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                if (lsmsosbo != null)
                {
                    List<GridViewModel> lsGVM = new List<GridViewModel>();
                    foreach (var item in lsmsosbo)
                    {
                        GridViewModel gvm = new GridViewModel();
                        gvm.AboveAverage = item.TotalAboveAverage;
                        gvm.BelowAverage = item.TotalBelowAverage;
                        gvm.SchoolID = item.SchoolID;
                        gvm.SchoolName = item.SchoolName;
                        gvm.MarkAll = item.MarkInfo.Select(d => d.Value).ToList();
                        gvm.PercentAboveAverage = string.Format("{0:P2}", item.PercentAboveAverage);
                        gvm.PercentBelowAverage = string.Format("{0:P2}", item.PercentBelowAverage);
                        lsGVM.Add(gvm);
                    }
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                }
                else
                {
                    ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                }
                return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
            }
            ViewData[StatisticalPeriodicMarkOfPrimarySchoolConstant.SHOW_EXCEL] = false;
            return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }


        public JsonResult ExportExcel(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);
                string ReportCode = "SGD_TH_TongHopDiemKiemTraDinhKy";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                return Json(new JsonMessage(entity.ProcessedReportID.ToString()));
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                MarkReportBO mrbo = new MarkReportBO();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(mrbo);
                string ReportCode = "PGD_TH_TongHopDiemKiemTraDinhKy";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                return Json(new JsonMessage(entity.ProcessedReportID.ToString()));
            }
            throw new BusinessException("Common_Error_InternalError");
        }

        #endregion report
    }
}
