﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PhysicalFacilitiesArea
{
    public class PhysicalFacilitiesConstant
    {
        public const string LIST_TITLE_TEMPLATE = "LIST_TITLE_TEMPLATE";
        public const string HTML_TEMPLATE = "HTML_TEMPLATE";
        /// <summary>
        /// {0} : vi tri cell (cell address)
        /// {1} : ma tieu chi
        /// {2} : gia tri cua cell
        /// {3} : cong thuc
        /// {4} : them class danh dau cong thuc
        /// {5} : class để biết cell sẽ tính giá trị từ cell khác có công thức
        /// </summary>
        /// 
        public const string TEXT_BOX_HIDDEN = "<input type='hidden'  id='{0}' name='{1}' value='{2}' fomular='{3}' class='cldatainput clTextBox {4} {5}'/>";

        /// <summary>
        /// Phan tren giong boi text_box_hidden
        /// {5}: Maxlength
        /// {6}: gia tri max cho phep nhap
        /// {7}: cong thuc validateinput
        /// </summary>
        public const string TEXT_BOX = "<input type='text' onkeyup='onchangeInput(this)' onkeypress='return validateInput(event)' onchange='validateChange(this)'  id='{0}' name='{1}' value='{2}' fomular='{3}' class='cldatainput clTextBox {4}' maxlength='{5}' maxvalue='{6}' inputvalidate='{7}' />";

        /// <summary>s
        /// {0}: gia tri cua cell     
        /// </summary>
        public const string DIV_VIEW_DATA = "<div class='clSpanBox'>{0}</div>";

        public const string HTML_CONTENT_CODE_ID = "HTML_CONTENT_CODE_ID";
        public const string PERIOD_ID = "PeriodId";

    }
}