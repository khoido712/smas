﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using Telerik.Web.Mvc;
using System.Globalization;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.PhysicalFacilitiesArea.Controllers
{
    public class PhysicalFacilitiesController : BaseController
    {
        private IHtmlContentCodeBusiness HtmlContentCodeBusiness;
        private IHtmlTemplateBusiness HtmlTemplateBusiness;
        private IIndicatorBusiness IndicatorBusiness;
        private IIndicatorDataBusiness IndicatorDataBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        public PhysicalFacilitiesController(IHtmlContentCodeBusiness HtmlContentCodeBusiness, IHtmlTemplateBusiness HtmlTemplateBusiness
            , IIndicatorBusiness IndicatorBusiness, IIndicatorDataBusiness IndicatorDataBusiness,
            IAcademicYearBusiness AcademicYearBusiness)
        {
            this.HtmlContentCodeBusiness = HtmlContentCodeBusiness;
            this.HtmlTemplateBusiness = HtmlTemplateBusiness;
            this.IndicatorBusiness = IndicatorBusiness;
            this.IndicatorDataBusiness = IndicatorDataBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
            SetViewDataPermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            var TemplateCode = SystemParamsInFile.REPORT_PHYSICAL_FACILITIES;
            if (_globalInfo.AppliedLevel > 3) 
            {
                TemplateCode = SystemParamsInFile.REPORT_PHYSICAL_FACILITIES_PRIAMRY_LEVEL;
            }
            List<HtmlContentCode> listContent = HtmlContentCodeBusiness.GetListHtmlContentCode(TemplateCode);
            int htmlContentCodeId = 0;
            int period = IndicatorBusiness.GetCurrentPeriodReport(_globalInfo.AcademicYearID.Value);
            if (listContent != null && listContent.Count > 0)
            {
                htmlContentCodeId = listContent.Select(p => p.HtmlContentCodeID).FirstOrDefault();
                //Lay html template
                HtmlTemplate htmlTemplateObj = HtmlTemplateBusiness.GetHtmlTemplate(htmlContentCodeId);
                ViewData[PhysicalFacilitiesConstant.LIST_TITLE_TEMPLATE] = listContent;
                ViewData[PhysicalFacilitiesConstant.HTML_TEMPLATE] = ReplaceIndicator(htmlTemplateObj.HtmlContent, htmlTemplateObj.HtmlTemplateID, period);
                ViewData[PhysicalFacilitiesConstant.HTML_CONTENT_CODE_ID] = htmlContentCodeId;
                ViewData[PhysicalFacilitiesConstant.PERIOD_ID] = period;
            }
            return View();
        }

        public JsonResult GetDataPreviosPeriod(int htmlContentCodeId, int period)
        {
            string _html = string.Empty;
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
            int permission = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
            if (permission >= 2 || permission >= 3 || permission >= 4)
            {
                //Xoa du lieu hien tai va lay du lieu moi cua ky truoc
                IndicatorDataBusiness.InsertDataPreviosPeriod(_globalInfo.AcademicYearID.Value, _globalInfo.ProvinceID.Value, period, htmlContentCodeId);
                HtmlTemplate htmlTemplateObj = HtmlTemplateBusiness.GetHtmlTemplate(htmlContentCodeId);
                if (htmlTemplateObj != null)
                {
                    _html = ReplaceIndicator(htmlTemplateObj.HtmlContent, htmlTemplateObj.HtmlTemplateID, period);
                }
            }
            else
            {
                return Json(new { Type = "error", strMessage = Res.Get("Error_Permision_Assign") });
            }
            return Json(new { Type = "success", strHtml = _html, strMessage = "Kế thừa dữ liệu thành công" });
        }

        public JsonResult GetHtmlTemplate(int htmlContentCodeId, int period)
        {
            HtmlTemplate htmlTemplateObj = HtmlTemplateBusiness.GetHtmlTemplate(htmlContentCodeId);
            string _html = string.Empty;
            if (htmlTemplateObj != null)
            {
                _html = ReplaceIndicator(htmlTemplateObj.HtmlContent, htmlTemplateObj.HtmlTemplateID, period);
            }
            return Json(new { strHtml = _html });
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveData(FormCollection fr)
        {
            try
            {
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString(); ;
                int permission = GetMenupermissionWithArea(controllerName, _globalInfo.UserAccountID, _globalInfo.IsAdminSchoolRole, _globalInfo.RoleID);
                if (permission >= 2 || permission >= 3 || permission >= 4)
                {
                    int htmlTemplateCodeId = fr["htmlcontentcode"] != null ? Int32.Parse(fr["htmlcontentcode"]) : 0;
                    if (htmlTemplateCodeId == 0)
                    {
                        return Json(new JsonMessage(Res.Get("Lbl_Error_Processing"), "error"));
                    }
                    int period = fr["selectperiod"] != null ? Int32.Parse(fr["selectperiod"]) : 0;
                    if (period == 0)
                    {
                        return Json(new JsonMessage(Res.Get("Lbl_Error_Processing"), "error"));
                    }
                    List<Indicator> listIndicator = IndicatorBusiness.GetListIndicatorByHtmlTemplateID(htmlTemplateCodeId);
                    List<int> listIndicatorIdDelete = listIndicator.Select(p => p.IndicatorID).Distinct().ToList();
                    string indicatorCode = string.Empty;
                    int indicatorId = 0;
                    List<IndicatorData> listDataInsert = new List<IndicatorData>();
                    IndicatorData dataObj = null;
                    AcademicYear ac = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

                    int chckNumber = 0;
                    if (listIndicator != null && listIndicator.Count > 0)
                    {
                        for (int i = 0; i < listIndicator.Count; i++)
                        {
                            indicatorCode = listIndicator[i].IndicatorCode;
                            indicatorId = listIndicator[i].IndicatorID;
                            dataObj = new IndicatorData();
                            dataObj.IndicatorID = indicatorId;
                            dataObj.SchoolId = _globalInfo.SchoolID.Value;

                            if (fr[indicatorCode] == null) continue;
                            if (fr[indicatorCode] != null)
                            {
                                if (fr[indicatorCode].Equals(string.Empty))
                                {
                                    continue;
                                }
                                else
                                {
                                    bool chk = Int32.TryParse(fr[indicatorCode].ToString(), out chckNumber);
                                    if (!chk)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (Int32.Parse(fr[indicatorCode]) >= 0)
                                        {
                                            dataObj.CellValue = fr[indicatorCode];
                                        }
                                    }
                                }
                            }

                            dataObj.ProvinceID = _globalInfo.ProvinceID.Value;
                            dataObj.LastDigitProvinceID = _globalInfo.ProvinceID.Value % SystemParamsInFile.PARTITION_INDICATOR_DATA;
                            dataObj.DistrictID = _globalInfo.DistrictID != null ? _globalInfo.DistrictID.Value : 0;
                            dataObj.SemesterID = period;
                            dataObj.AcademicYearID = ac.AcademicYearID;
                            dataObj.EducationGrade = _globalInfo.AppliedLevel.Value;
                            dataObj.CreateTime = DateTime.Now;
                            dataObj.CreateYear = ac.Year;
                            listDataInsert.Add(dataObj);
                        }
                    }

                    //goi ham insert
                    IndicatorDataBusiness.InsertData(listDataInsert, listIndicatorIdDelete, _globalInfo.ProvinceID.Value, period, _globalInfo.AcademicYearID.Value);
                    return Json(new JsonMessage(Res.Get("Common_Label_Save"), "success"));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Error_Permision_Assign"), "error"));
                }
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "SaveData", "null", ex);
                return Json(new JsonMessage(Res.Get("Lbl_Error_Processing"), "error"));
            }
        }

        public JsonResult GetMessageCopyDate(int period)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string message = string.Empty;
            string periodName = string.Empty;
            if (period == SystemParamsInFile.FIRST_PERIOD)
            {
                int preYear = aca.Year - 1;
                AcademicYear preAca = AcademicYearBusiness.All.Where(p => p.SchoolID == aca.SchoolID && p.Year == preYear).FirstOrDefault();
                if (preAca == null)
                {
                    return Json(new JsonMessage("Trường không có năm học cũ", "error"));
                }
                else
                {
                    periodName = this.GetNameByPeriodId(IndicatorDataBusiness.GetPreviosPeriod(period));
                    message = string.Format(Res.Get("Lbl_Message_Copy_Date_Previos_Period"), periodName, preAca.DisplayTitle);
                }
            }
            else
            {
                periodName = this.GetNameByPeriodId(IndicatorDataBusiness.GetPreviosPeriod(period));
                message = string.Format(Res.Get("Lbl_Message_Copy_Date_Previos_Period"), periodName, aca.DisplayTitle);
            }
            return Json(new JsonMessage(message, "success"));
        }

        private string ReplaceIndicator(string htmlTemplate, int htmlTemplateId, int period)
        {
            List<Indicator> listIndicator = IndicatorBusiness.GetListIndicatorByHtmlTemplateID(htmlTemplateId);
            Indicator indicatorObj = null;
            string _htmlTemplate = htmlTemplate;
            string textbox = string.Empty;
            if (listIndicator != null && listIndicator.Count > 0)
            {
                List<int> listIndicatorId = listIndicator.Select(p => p.IndicatorID).ToList();
                //Lay du lieu co trong database
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("ListIndicatorID", listIndicatorId);
                dic.Add("Period", period); // truyen dot 1:dau ky, 2:giua ky, 3:cuoi ky
                dic.Add("ProvinceID", _globalInfo.ProvinceID.Value);
                dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                dic.Add("SchoolID", _globalInfo.SchoolID.Value);
                List<IndicatorData> listIndicatorData = IndicatorDataBusiness.SearchIndicatorData(dic).ToList();
                IndicatorData indicatorDataObj = null;
                string strClassFormular = string.Empty;
                string strClassSum = string.Empty;
                for (int i = 0; i < listIndicator.Count; i++)
                {
                    textbox = string.Empty;
                    strClassFormular = string.Empty;
                    strClassSum = string.Empty;
                    indicatorObj = listIndicator[i];
                    indicatorDataObj = listIndicatorData.Where(p => p.IndicatorID == indicatorObj.IndicatorID).FirstOrDefault();
                    if (!string.IsNullOrEmpty(indicatorObj.Formula))
                    {
                        strClassFormular = "clformular";
                        if (indicatorObj.IsSum)
                        {
                            strClassSum = "clsum";
                        }
                        textbox = string.Format(PhysicalFacilitiesConstant.TEXT_BOX_HIDDEN, indicatorObj.CellAddress, indicatorObj.IndicatorCode, indicatorDataObj != null ? indicatorDataObj.CellValue : string.Empty, indicatorObj.Formula, strClassFormular, strClassSum);
                        textbox += string.Format(PhysicalFacilitiesConstant.DIV_VIEW_DATA, indicatorDataObj != null ? indicatorDataObj.CellValue : string.Empty);
                    }
                    else
                    {
                        textbox = string.Format(PhysicalFacilitiesConstant.TEXT_BOX, indicatorObj.CellAddress, indicatorObj.IndicatorCode, indicatorDataObj != null ? indicatorDataObj.CellValue : string.Empty,
                            indicatorObj.Formula, strClassFormular,
                            indicatorObj.MaxLength, indicatorObj.MaxValue, indicatorObj.FormulaValidate);
                    }
                    _htmlTemplate = SafeReplace(_htmlTemplate, indicatorObj.IndicatorCode, textbox);
                }
            }
            return _htmlTemplate;
        }

        private string SafeReplace(string input, string find, string replace, bool matchWholeWord = true)
        {
            string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
            return Regex.Replace(input, textToFind, replace);
        }

        private string GetNameByPeriodId(int periodId)
        {
            string periodName = string.Empty;
            if (periodId == SystemParamsInFile.FIRST_PERIOD)
            {
                periodName = Res.Get("Lbl_First_Semester");
            }
            else if (periodId == SystemParamsInFile.MID_PERIOD)
            {
                periodName = Res.Get("Lbl_Mid_Semester");
            }
            else if (periodId == SystemParamsInFile.END_PERIOD)
            {
                periodName = Res.Get("Lbl_End_Semester");
            }
            return periodName;
        }
    }
}





