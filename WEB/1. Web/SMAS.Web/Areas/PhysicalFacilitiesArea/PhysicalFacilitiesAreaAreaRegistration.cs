﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PhysicalFacilitiesArea
{
    public class PhysicalFacilitiesAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PhysicalFacilitiesArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PhysicalFacilitiesArea_default",
                "PhysicalFacilitiesArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
