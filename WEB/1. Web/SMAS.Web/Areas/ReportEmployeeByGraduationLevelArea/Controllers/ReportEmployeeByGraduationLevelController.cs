﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea.Controllers
{
    
        public class ReportEmployeeByGraduationLevelController : BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IReportEmployeeByGraduationLevelBusiness ReportEmployeeByGraduationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        public ReportEmployeeByGraduationLevelController(ISpecialityCatBusiness SpecialityCatBusiness,
            ITrainingLevelBusiness TrainingLevelBusiness,
            IEmployeeSalaryBusiness EmployeeSalaryBusiness, 
            IProcessedReportBusiness ProcessedReportBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IReportEmployeeByGraduationLevelBusiness ReportEmployeeByGraduationLevelBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.ReportEmployeeByGraduationLevelBusiness = ReportEmployeeByGraduationLevelBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
        }

        public ActionResult Index()
        {
            this.SetViewData();
            return View();
        }

        public ActionResult _Index()
        {
            this.SetViewData();
            return PartialView();
        }

        #region SetViewData
        public void SetViewData()
        {
            ViewData[ReportEmployeeByGraduationLevelConstants.CBO_FACULTY] = new List<SchoolFaculty>();
            ViewData[ReportEmployeeByGraduationLevelConstants.CBO_GRADUATIONLEVEL] = new List<GraduationLevel>();



            //-	cboFaculty: : lấy danh sách từ SchoolFacultyBusiness.SearchBySchool(Dictionary) 
            //    với Dictionary[“SchoolID”] = UserInfo.SchoolID và Dictionary[“IsActive”] = 1. Thêm giá trị mặc định [Tất cả]
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["IsActive"] = true;

            IQueryable<SchoolFaculty> lsFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, Dictionary);
            if (lsFaculty.Count() > 0)
            {
                List<SchoolFaculty> lstFaculty = lsFaculty.ToList();
                lstFaculty = lstFaculty.OrderBy(o => o.FacultyName).ToList();
                ViewData[ReportEmployeeByGraduationLevelConstants.CBO_FACULTY] = lstFaculty;
            }
            //            -	cboGraduationLevel: Lấy danh sách trình độ bằng cách gọi GraduationLevelBusiness.Search(Dictionary<string, object>).
            //                Giá trị mặc định là [Tất cả]
            //-	cboSpecialityCat: lấy danh sách chuyên ngành đào tạo bằng cách gọi hàm SpecialityCatBusiness.Search(Dictionary<String, Object>).
            //    Giá trị mặc định là [Tất cả].
            //Dictionary["SchoolID"] = Global.SchoolID;
            //Sua lai tu GraduationLevel => TrainingLevel
            Dictionary = new Dictionary<string, object>();
            IQueryable<TrainingLevel> lsGraduationLevel = TrainingLevelBusiness.Search(Dictionary);
            if (lsGraduationLevel.Count() > 0)
            {
                List<TrainingLevel> lstGraduationLevel = lsGraduationLevel.ToList();
                lstGraduationLevel = lstGraduationLevel.OrderBy(o => o.Resolution).ToList();
                ViewData[ReportEmployeeByGraduationLevelConstants.CBO_GRADUATIONLEVEL] = lstGraduationLevel;
            }

            IQueryable<SpecialityCat> lsSpecialityCat = SpecialityCatBusiness.Search(Dictionary);
            if (lsSpecialityCat.Count() > 0)
            {
                List<SpecialityCat> lstSpecialityCat = lsSpecialityCat.ToList();
                lstSpecialityCat = lstSpecialityCat.OrderBy(o => o.Resolution).ToList();
                ViewData[ReportEmployeeByGraduationLevelConstants.CBO_SPECIALITYCAT] = lstSpecialityCat;
            }


        }
        #endregion

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel svm)
        {
            if (svm.ReportDate > DateTime.Now)
            {
                throw new BusinessException("ReportEmployeeSalary_Label_ErrorDate");
            }
            switch (svm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ theo chuyên ngành đào tạo
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        reportEmployee.GraduationLevelID = svm.GraduationLevel != null ? svm.GraduationLevel : 0;
                        reportEmployee.SpecialityCatID = svm.SpecialityCat != null ? svm.SpecialityCat : 0;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeByGraduationLevelBusiness.GetReportEmployeeByGraduationLevel(reportEmployee);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeByGraduationLevel(reportEmployee);
                            processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeByGraduationLevel(reportEmployee, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 2: //Báo cáo cán bộ đạt và vượt chuẩn
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeByGraduationLevelBusiness.GetReportEmployeeAboveStandard(reportEmployee);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeAboveStandard(reportEmployee);
                            processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeAboveStandard(reportEmployee, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                case 3: //báo cáo danh sách cán bộ dưới chuẩn
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN;
                        ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                        string type = JsonReportMessage.NEW;
                        ProcessedReport processedReport = null;
                        if (reportDef.IsPreprocessed == true)
                        {
                            processedReport = ReportEmployeeByGraduationLevelBusiness.GetReportEmployeeUnderStandard(reportEmployee);
                            if (processedReport != null)
                            {
                                type = JsonReportMessage.OLD;
                            }
                        }
                        if (type == JsonReportMessage.NEW)
                        {
                            Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeUnderStandard(reportEmployee);
                            processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeUnderStandard(reportEmployee, excel);
                            excel.Close();

                        }
                        return Json(new JsonReportMessage(processedReport, type));
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel svm)
        {
            if (svm.ReportDate > DateTime.Now)
            {
                throw new BusinessException("ReportEmployeeSalary_Label_ErrorDate");
            }
            switch (svm.rptReport)
            {
                case 1: //Báo cáo danh sách cán bộ theo chuyên ngành đào tạo
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        reportEmployee.GraduationLevelID = svm.GraduationLevel != null ? svm.GraduationLevel : 0;
                        reportEmployee.SpecialityCatID = svm.SpecialityCat != null ? svm.SpecialityCat : 0;
                        Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeByGraduationLevel(reportEmployee);
                        ProcessedReport processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeByGraduationLevel(reportEmployee, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 2: //Báo cáo danh sách cán bộ đạt và vượt chuẩn
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeAboveStandard(reportEmployee);
                        ProcessedReport processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeAboveStandard(reportEmployee, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
                case 3: //Báo cáo danh sách cán bộ dưới chuẩn
                    {
                        ReportEmployeeByGraduationLevelBO reportEmployee = new ReportEmployeeByGraduationLevelBO();
                        reportEmployee.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                        reportEmployee.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
                        reportEmployee.FacultyID = svm.SchoolFaculty != null ? svm.SchoolFaculty.Value : 0;
                        reportEmployee.ReportDate = svm.ReportDate;
                        Stream excel = ReportEmployeeByGraduationLevelBusiness.CreateReportEmployeeUnderStandard(reportEmployee);
                        ProcessedReport processedReport = ReportEmployeeByGraduationLevelBusiness.InsertReportEmployeeUnderStandard(reportEmployee, excel);
                        excel.Close();
                        return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
                    }
              
                default:
                    {
                        return null;
                    }
            }
        }
        public FileResult DownloadReport(int idProcessedReport)
        {

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        public FileResult DownloadPDFReport(int idProcessedReport)
        {

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN,
                SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
    }
}
