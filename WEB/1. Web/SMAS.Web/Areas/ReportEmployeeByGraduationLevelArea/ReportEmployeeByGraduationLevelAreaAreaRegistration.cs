﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea
{
    public class ReportEmployeeByGraduationLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportEmployeeByGraduationLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportEmployeeByGraduationLevelArea_default",
                "ReportEmployeeByGraduationLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
