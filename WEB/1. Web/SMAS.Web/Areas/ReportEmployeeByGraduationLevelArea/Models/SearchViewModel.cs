﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea.Models
{
    public class SearchViewModel
    {
        public int? SchoolFaculty { get; set; }

        [UIHint("DateTimePicker")]
        [ResourceDisplayName("ReportEmployeeProfile_Label_ReportDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public DateTime ReportDate { get; set; }

        public int? GraduationLevel { get; set; }
        public int? SpecialityCat { get; set; }
        public int? EmployeeBySpecialityCat { get; set; }
        public int? AboveStandard { get; set; }
        public int? UnderStandard { get; set; }
        public int rptReport { get; set; }

    }
}