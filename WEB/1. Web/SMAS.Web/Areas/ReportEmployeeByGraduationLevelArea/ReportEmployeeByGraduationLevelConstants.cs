﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportEmployeeByGraduationLevelArea
{
    public class ReportEmployeeByGraduationLevelConstants
    {
        public const string CBO_REPORTDATE = "CboReportDate";
        public const string CBO_FACULTY = "CboFaculty";
        public const string CBO_GRADUATIONLEVEL = "cboGraduationLevel";
        public const string CBO_SPECIALITYCAT = "cboSpecialityCat";
    }
}