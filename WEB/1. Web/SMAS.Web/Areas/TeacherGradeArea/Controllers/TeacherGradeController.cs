﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.TeacherGradeArea.Models;


namespace SMAS.Web.Areas.TeacherGradeArea.Controllers
{
    public class TeacherGradeController : BaseController
    {        
        private readonly ITeacherGradeBusiness TeacherGradeBusiness;
		
		public TeacherGradeController (ITeacherGradeBusiness teachergradeBusiness)
		{
			this.TeacherGradeBusiness = teachergradeBusiness;
		}
		
		//
        // GET: /TeacherGrade/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here

            IEnumerable<TeacherGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[TeacherGradeConstants.LIST_TEACHERGRADE] = lst;
            return View();
        }

		//
        // GET: /TeacherGrade/Search

        
        public PartialViewResult Search(TeacherGradeSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["GradeResolution"] = frm.GradeResolution;
            SearchInfo["IsActive"] = true;

            IEnumerable<TeacherGradeViewModel> lst = this._Search(SearchInfo);
            ViewData[TeacherGradeConstants.LIST_TEACHERGRADE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(TeacherGradeViewModel frm)
        {
            TeacherGrade teachergrade = new TeacherGrade();
            TryUpdateModel(teachergrade); 
            Utils.Utils.TrimObject(teachergrade);

            teachergrade.GradeResolution = frm.GradeResolution;
            teachergrade.Description = frm.Description;
            teachergrade.IsActive = true;

            this.TeacherGradeBusiness.Insert(teachergrade);
            this.TeacherGradeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TeacherGradeID)
        {
            TeacherGrade teachergrade = this.TeacherGradeBusiness.Find(TeacherGradeID);
            TryUpdateModel(teachergrade);
            Utils.Utils.TrimObject(teachergrade);
            this.TeacherGradeBusiness.Update(teachergrade);
            this.TeacherGradeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.TeacherGradeBusiness.Delete(id);
            this.TeacherGradeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TeacherGradeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<TeacherGrade> query = this.TeacherGradeBusiness.Search(SearchInfo).OrderBy(i=>i.GradeResolution);
            IQueryable<TeacherGradeViewModel> lst = query.Select(o => new TeacherGradeViewModel {               
						TeacherGradeID = o.TeacherGradeID,								
						GradeResolution = o.GradeResolution,								
						Description = o.Description,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate								
					
				
            });

            return lst.ToList();
        }        
    }
}





