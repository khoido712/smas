﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeacherGradeArea
{
    public class TeacherGradeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeacherGradeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeacherGradeArea_default",
                "TeacherGradeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
