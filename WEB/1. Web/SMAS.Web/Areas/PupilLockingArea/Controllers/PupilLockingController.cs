﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilLockingArea.Controllers
{
    public class PupilLockingController : BaseController
    {
        #region Properties
        private readonly  IAcademicYearBusiness AcademicYearBusiness;
        #endregion

        #region Constructor
        public PupilLockingController(IAcademicYearBusiness AcademicYearBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        public ActionResult Index()
        {
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            ViewData[PupilLockingConstants.IS_ORDER_LOCK] = ay.IsLockNumOrdinal;
            ViewData[PupilLockingConstants.IS_PROFILE_LOCK] = ay.IsLockPupilProfile;

            return View();
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Save(int? isOrderLock, int? isProfileLock)
        {
            if (GetMenupermission("PupilLocking", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_VIEW)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Lay ra nam hoc hien tai
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (ay != null)
            {
                if (isOrderLock == 1)
                {
                    ay.IsLockNumOrdinal = true;
                }
                else
                {
                    ay.IsLockNumOrdinal = false;
                }
                if (isProfileLock == 1)
                {
                    ay.IsLockPupilProfile = true;
                }
                else
                {
                    ay.IsLockPupilProfile = false;
                }

                AcademicYearBusiness.Update(ay);
                AcademicYearBusiness.Save();
            }

            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }
    }
}