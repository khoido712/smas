﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilLockingArea
{
    public class PupilLockingConstants
    {
        public const string IS_ORDER_LOCK = "isOrderLock";
        public const string IS_PROFILE_LOCK = "isProfileLock";
    }
}