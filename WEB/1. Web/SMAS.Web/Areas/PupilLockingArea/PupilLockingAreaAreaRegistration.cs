﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilLockingArea
{
    public class PupilLockingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilLockingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilLockingArea_default",
                "PupilLockingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
