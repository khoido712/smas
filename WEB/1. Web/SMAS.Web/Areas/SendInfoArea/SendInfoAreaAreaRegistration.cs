﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SendInfoArea
{
    public class SendInfoAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SendInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SendInfoArea_default",
                "SendInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
