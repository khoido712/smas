﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SendInfoArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.SendInfoArea.Controllers
{
    public class SendInfoController : BaseController
    {        
        private readonly ISendInfoBusiness SendInfoBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IActivityBusiness ActivityBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        //private readonly IActivityOfChildrenBusiness ActivityOfChildrenBusiness;
		public SendInfoController (ISendInfoBusiness sendinfoBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IActivityBusiness ActivityBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            //, IActivityOfChildrenBusiness ActivityOfChildrenBusiness
            )
		{
			this.SendInfoBusiness = sendinfoBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ActivityBusiness = ActivityBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            //this.ActivityOfChildrenBusiness = ActivityOfChildrenBusiness;
		}
		
		//
        // GET: /SendInfo/

        public ActionResult Index()
        {
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            ////Get view data here

            //IEnumerable<SendInfoViewModel> lst = this._Search(SearchInfo);
            //ViewData[SendInfoConstants.LIST_SENDINFO] = lst;
            GlobalInfo g = new GlobalInfo();
            if(!g.IsAdminSchoolRole){
                throw new BusinessException("SendInfo_Label_CheckRole");
            }
            setViewData();
            return View();
        }

        public ActionResult IndexInformation()
        {
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            ////Get view data here

            //IEnumerable<SendInfoViewModel> lst = this._Search(SearchInfo);
            //ViewData[SendInfoConstants.LIST_SENDINFO] = lst;
            GlobalInfo g = new GlobalInfo();
            if (!g.IsAdminSchoolRole)
            {
                throw new BusinessException("SendInfo_Label_CheckRole");
            }
            setViewData();
            return View();
        }
		//
        // GET: /SendInfo/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ActivatedDate"] = frm.Date;

			//
            
            IEnumerable<SendInfoViewModel> lst = this._Search(SearchInfo);
            ViewData[SendInfoConstants.LIST_SENDINFO] = lst;

            //Get view data here

            return PartialView("_List");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchInformation(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["EducationLevelID"] = frm.EducationLevel;
            SearchInfo["CurrentClassID"] = frm.Class;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["ActivatedDate"] = DateTime.Now;

            //

            IEnumerable<SendInfoViewModel> lst = this._Search(SearchInfo);
            ViewData[SendInfoConstants.LIST_SENDINFO] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            SendInfo sendinfo = new SendInfo();
            TryUpdateModel(sendinfo); 
            Utils.Utils.TrimObject(sendinfo);

            this.SendInfoBusiness.Insert(sendinfo);
            this.SendInfoBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int SendInfoID)
        {
            SendInfo sendinfo = this.SendInfoBusiness.Find(SendInfoID);
            TryUpdateModel(sendinfo);
            Utils.Utils.TrimObject(sendinfo);
            this.SendInfoBusiness.Update(sendinfo);
            this.SendInfoBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.SendInfoBusiness.Delete(id);
            this.SendInfoBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SendInfoViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            List<ActivityPlanToSMS> query = ActivityBusiness.GetActivityToSMS(SearchInfo).ToList();
            List<SendInfoViewModel> lst = new List<SendInfoViewModel>();
            foreach (var item in query)
            {
                PupilProfile pp = PupilProfileBusiness.Find(item.PupilID);
                SendInfoViewModel sivm = new SendInfoViewModel();
                sivm.ClassID =  item.ClassID;
                sivm.PupilID = item.PupilID;
                sivm.ContentActivity = item.ContentActivity;

                ClassProfile cp = ClassProfileBusiness.Find(item.ClassID);
                sivm.ClassName = cp.DisplayName;
              
                sivm.PupilCode = pp.PupilCode;
                sivm.FullName = pp.FullName;
                sivm.FatherMobile = pp.FatherMobile;
                sivm.MotherMobile = pp.MotherMobile;
                sivm.PupilCode = pp.PupilCode;
                sivm.ContentActivity = item.ContentActivity;
                lst.Add(sivm);
            }
            return lst;
        }

        /// <summary>
        /// SearchInformation
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SendInfoViewModel> _SearchInformation(IDictionary<string, object> SearchInfo)
        {
            //List<ActivityOfChildrenToSMS> query = ActivityOfChildrenBusiness.GetActivityOfChildrenToSMS(SearchInfo).ToList();
            List<SendInfoViewModel> lst = new List<SendInfoViewModel>();
            //foreach (var item in query)
            //{
            //    SendInfoViewModel sivm = new SendInfoViewModel();
            //    sivm.ClassID = item.ClassID;
            //    sivm.PupilID = item.PupilID;
            //    sivm.ContentActivity = item.ContentActivity;

            //    ClassProfile cp = ClassProfileBusiness.Find(item.ClassID);
            //    sivm.ClassName = cp.DisplayName;
            //    PupilProfile pp = PupilProfileBusiness.Find(item.PupilID);
            //    sivm.PupilCode = pp.PupilCode;
            //    sivm.FullName = pp.FullName;
            //    sivm.FatherMobile = pp.FatherMobile;
            //    sivm.MotherMobile = pp.MotherMobile;
            //    sivm.PupilCode = pp.PupilCode;
            //    sivm.ContentActivity = item.ContentActivity;
            //    lst.Add(sivm);
            //}

            //

            //
            return lst;
        }

        public void setViewData() {
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;

            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[SendInfoConstants.LIST_EDUCATION] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[SendInfoConstants.LIST_EDUCATION] = new SelectList(new string[] { });
            }
            //IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            //Dictionary["EducationLevelID"] = global.AcademicYearID;
            //Dictionary["AcademicYearID"] = global.AcademicYearID;
            //List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID, Dictionary).ToList();
        }

        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
                };
            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        public void ShowGrid(int[] PupilID, int[] ClassID, string[] ClassName
                            , string[] PupilCode, string[] FullName, string[] FatherMobile
                            , string[] MotherMobile, string[] ContentActivity, int[] checkedNewPupil)
        {
            GlobalInfo global = new GlobalInfo();
            List<SendInfo> lstSendInfo = new List<SendInfo>();
            List<int> listLocation = new List<int>();
            for (int i = 0; i < PupilID.Count(); i++)
            {
                if (checkedNewPupil.Contains(PupilID[i]))
                {
                    listLocation.Add(i);
                }
            }
            foreach (var item in listLocation)
            {
                int pupilID = PupilID[item];
                int classID = ClassID[item];
                string className = ClassName[item];
                string pupilCode = PupilCode[item];
                string fullName = FullName[item];
                string fatherMobile = FatherMobile[item];
                string motherMobile = MotherMobile[item];
                string contentActivity = ContentActivity[item];
                if (fatherMobile == null)
                {
                    fatherMobile = "";
                }
                if (motherMobile == null)
                {
                    motherMobile = "";
                }
                if ((fatherMobile.Trim().Length > 0) && (motherMobile.Trim().Length > 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    fatherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                    SendInfo s2 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    motherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);
                    lstSendInfo.Add(s2);
                }
                if ((fatherMobile.Trim().Length == 0) && (motherMobile.Trim().Length > 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    motherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }
                if ((fatherMobile.Trim().Length > 0) && (motherMobile.Trim().Length == 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    fatherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }
                if ((fatherMobile.Trim().Length == 0) && (motherMobile.Trim().Length == 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    null,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }

            }
            SendInfoBusiness.InsertSendInfo(lstSendInfo);
            SendInfoBusiness.Save();
        }
        public void ShowGridInformation(int[] PupilID, int[] ClassID, string[] ClassName
                            , string[] PupilCode, string[] FullName, string[] FatherMobile
                            , string[] MotherMobile, string[] ContentActivity, int[] checkedNewPupil)
        {
            GlobalInfo global = new GlobalInfo();
            List<SendInfo> lstSendInfo = new List<SendInfo>();
            List<int> listLocation = new List<int>();
            for (int i = 0; i < PupilID.Count(); i++)
            {
                if (checkedNewPupil.Contains(PupilID[i]))
                {
                    listLocation.Add(i);
                }
            }
            foreach (var item in listLocation)
            {
                int pupilID = PupilID[item];
                int classID = ClassID[item];
                string className = ClassName[item];
                string pupilCode = PupilCode[item];
                string fullName = FullName[item];
                string fatherMobile = FatherMobile[item];
                string motherMobile = MotherMobile[item];
                string contentActivity = ContentActivity[item];
                if (fatherMobile == null)
                {
                    fatherMobile = "";
                }
                if(motherMobile==null){
                    motherMobile = "";
                }
                if ((fatherMobile.Trim().Length > 0) && (motherMobile.Trim().Length > 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    fatherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);
                    
                    lstSendInfo.Add(s1);
                    SendInfo s2 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    motherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);
                    lstSendInfo.Add(s2);
                }
                if ((fatherMobile.Trim().Length == 0) && (motherMobile.Trim().Length > 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    motherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }
                if ((fatherMobile.Trim().Length > 0) && (motherMobile.Trim().Length == 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    fatherMobile,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }
                if ((fatherMobile.Trim().Length == 0) && (motherMobile.Trim().Length == 0))
                {
                    SendInfo s1 = InsertToSendInfo(global.SchoolID,
                                                    PupilCode[item],
                                                    0,
                                                    SMAS.Web.Constants.GlobalConstants.SMS_RECEIVER_PARENT,
                                                    SystemParamsInFile.SERVICE_CODE_ACTIVITY,
                                                    global.AcademicYearID.Value,
                                                    Res.Get("SendInfo_Label_Title"),
                                                    ContentActivity[item],
                                                    null,
                                                    SMAS.Web.Constants.GlobalConstants.SEND_TYPE_MOBILE);

                    lstSendInfo.Add(s1);
                }
                
            }
            SendInfoBusiness.InsertSendInfo(lstSendInfo);
            SendInfoBusiness.Save();
        }
        public SendInfo InsertToSendInfo(int? schoolID, string pupilCode, int? infoType, int? receiveType, string serviceCode
                                       , int academicYearID,string title,string content,string mobileOfFa, int? sendType)
        {

            SendInfo s1 = new SendInfo();
            s1.SchoolID = schoolID;
            s1.PupilCode = pupilCode;
            s1.InfoType = infoType;
            s1.TypeOfReceiver = receiveType;
            //s1.ServiceCode = serviceCode;
            AcademicYear ay1 = AcademicYearBusiness.Find(academicYearID);
            s1.Year = ay1.Year;
            s1.Title = title;
            s1.Content = content;
            s1.ReceiverMobile = mobileOfFa;
            s1.SendType = sendType;
            SchoolProfile sp1 = SchoolProfileBusiness.Find(schoolID);
            s1.CreateUser = sp1.SchoolName;
            return s1;
        }
    }
}





