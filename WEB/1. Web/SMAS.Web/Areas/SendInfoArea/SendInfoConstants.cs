/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SendInfoArea
{
    public class SendInfoConstants
    {
        public const string LIST_SENDINFO = "listSendInfo";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_EDUCATION = "LIST_EDUCATION";
    }
}