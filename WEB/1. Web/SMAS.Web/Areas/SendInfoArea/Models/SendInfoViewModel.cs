/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SendInfoArea.Models
{
    public class SendInfoViewModel
    {
		public System.Int32 SendInfoID { get; set; }								
		public System.String PackageCode { get; set; }								
		public System.String ServiceCode { get; set; }								
		public System.Nullable<System.Int32> SchoolID { get; set; }
        [ResourceDisplayName("SendInfo_Label_PupilCode")]				
		public System.String PupilCode { get; set; }								
		public System.Nullable<System.Int32> SchedularType { get; set; }								
		public System.Nullable<System.Int32> InfoType { get; set; }								
		public System.Nullable<System.Int32> ReceiveType { get; set; }								
		public System.Nullable<System.Int32> Year { get; set; }								
		public System.String Title { get; set; }								
		public System.String Content { get; set; }								
		public System.String EmailOfFa { get; set; }								
		public System.String MobileOfFa { get; set; }								
		public System.String EmailOfPu { get; set; }								
		public System.String MobileOfPu { get; set; }								
		public System.Nullable<System.Int32> SendType { get; set; }								
		public System.Nullable<System.DateTime> create_time { get; set; }								
		public System.String create_user { get; set; }								
		public System.Nullable<System.Int32> Retry_num { get; set; }

        public int PupilID { get; set; }
        public int ClassID { get; set; }
        [ResourceDisplayName("SendInfo_Label_ContentActivity")]
        public string ContentActivity { get; set; }

        [ResourceDisplayName("SendInfo_Label_ClassName")]
        public string ClassName { get; set; }
        [ResourceDisplayName("SendInfo_Label_FullName")]
        public string FullName { get; set; }
        [ResourceDisplayName("SendInfo_Label_FatherMobile")]
        public string FatherMobile { get; set; }
        [ResourceDisplayName("SendInfo_Label_MotherMobile")]
        public string MotherMobile { get; set; }

        
    }
}


