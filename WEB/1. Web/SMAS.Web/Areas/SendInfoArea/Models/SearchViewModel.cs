/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SendInfoArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("SendInfo_Label_EducationLevelSearch")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SendInfoConstants.LIST_EDUCATION)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public string EducationLevel { get; set; }
        [ResourceDisplayName("SendInfo_Label_ClassSearch")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SendInfoConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public string Class { get; set; }
        [ResourceDisplayName("SendInfo_Label_DateSearch")]
        [UIHint("DateTimePicker")]
        public DateTime? Date { get; set; }

    }
}