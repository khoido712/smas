﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.HonourAchivementTypeArea.Models;


namespace SMAS.Web.Areas.HonourAchivementTypeArea.Controllers
{
    public class HonourAchivementTypeController : BaseController
    {        
        private readonly IHonourAchivementTypeBusiness HonourAchivementTypeBusiness;
		
		public HonourAchivementTypeController (IHonourAchivementTypeBusiness honourachivementtypeBusiness)
		{
			this.HonourAchivementTypeBusiness = honourachivementtypeBusiness;
		}
		
		//
        // GET: /HonourAchivementType/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //SearchInfo["Type"] = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL;
            //Get view data here
            GetTypeList();
            IEnumerable<HonourAchivementTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[HonourAchivementTypeConstants.LIST_HONOURACHIVEMENTTYPE] = lst.ToList();
            return View();
        }

		//
        // GET: /HonourAchivementType/Search

        
        public PartialViewResult Search(HonourAchivementTypeSearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo Global = new GlobalInfo();
            SearchInfo["Resolution"] = frm.Resolution;
            if (frm.HonourAchivementType.HasValue)
            {
                if (frm.HonourAchivementType.Value == 1)
                {
                    SearchInfo["Type"] = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_EMPLOYEE;
                }
                else if (frm.HonourAchivementType == 2)
                {
                    SearchInfo["Type"] = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL;
                }
                else if (frm.HonourAchivementType == 3)
                {
                    SearchInfo["Type"] = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_FACULTY;
                }
            }
            IEnumerable<HonourAchivementTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[HonourAchivementTypeConstants.LIST_HONOURACHIVEMENTTYPE] = lst.ToList();
            ViewData[HonourAchivementTypeConstants.ISYEAR] = true;
            if (!Global.IsCurrentYear)
            {
                ViewData[HonourAchivementTypeConstants.ISYEAR] = false;
            }

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// Thêm mới danh hiệu thi đua học sinh
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(HonourAchivementTypeViewModel ho)
        {
            GetTypeList();
            HonourAchivementType honourachivementtype = new HonourAchivementType();
            TryUpdateModel(honourachivementtype); 
            Utils.Utils.TrimObject(honourachivementtype);
            if (ho.Type == 2)
                honourachivementtype.Type = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL;//Danh hiệu thi đua cho học sinh
            else if (ho.Type == 1)
                honourachivementtype.Type = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_EMPLOYEE; //Danh hiệu thi đua giáo viên
            else
                honourachivementtype.Type = SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_FACULTY; //Danh hiệu thi đua tập thể
            honourachivementtype.IsActive = true;
            this.HonourAchivementTypeBusiness.Insert(honourachivementtype);
            this.HonourAchivementTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(HonourAchivementTypeViewModel ho, int HonourAchivementTypeID)
        {
            //GetTypeList();
            
            HonourAchivementType honourachivementtype = this.HonourAchivementTypeBusiness.Find(HonourAchivementTypeID);
            TryUpdateModel(honourachivementtype);
            Utils.Utils.TrimObject(honourachivementtype);
            this.HonourAchivementTypeBusiness.Update(honourachivementtype);
            this.HonourAchivementTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.HonourAchivementTypeBusiness.Delete(id);
            this.HonourAchivementTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<HonourAchivementTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<HonourAchivementType> query = this.HonourAchivementTypeBusiness.Search(SearchInfo);
            IQueryable<HonourAchivementTypeViewModel> lst = query.Select(o => new HonourAchivementTypeViewModel {               
						HonourAchivementTypeID = o.HonourAchivementTypeID,								
						Resolution = o.Resolution,								
						Type = o.Type,								
						Description = o.Description,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate								
					
				
            });
            List<HonourAchivementTypeViewModel> lstHonourAchivementTypeViewModel = lst.ToList();
            foreach (HonourAchivementTypeViewModel item in lstHonourAchivementTypeViewModel)
            {
                if (item.Type == SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL)
                {
                    item.TypeName = @Res.Get("HonourAchivementType_Label_Pupil");
                }
                else if (item.Type == SMAS.Web.Constants.GlobalConstants.HONOUR_ACHIVEMENT_TYPE_EMPLOYEE)
                {
                    item.TypeName = @Res.Get("HonourAchivementType_Label_Employee");
                }
                else item.TypeName = @Res.Get("HonourAchivementType_Label_Faculty");
            }


            return lstHonourAchivementTypeViewModel.AsQueryable().OrderBy(i => i.Resolution);
        }
        private void GetTypeList()
        {
            List<ComboObject> listArchivementType = new List<ComboObject>();
            listArchivementType.Add(new ComboObject("2", @Res.Get("HonourAchivementType_Label_Pupil")));
            listArchivementType.Add(new ComboObject("1", @Res.Get("HonourAchivementType_Label_Employee")));
            listArchivementType.Add(new ComboObject("3", @Res.Get("HonourAchivementType_Label_Faculty")));

            ViewData[HonourAchivementTypeConstants.LIST_TYPE] = new SelectList(listArchivementType, "key", "value");
        }
    }
}





