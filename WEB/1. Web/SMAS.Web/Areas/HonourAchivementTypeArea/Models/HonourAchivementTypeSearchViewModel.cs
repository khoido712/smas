/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.HonourAchivementTypeArea.Models
{
    public class HonourAchivementTypeSearchViewModel
    {

        [ResourceDisplayName("HonourAchivementType_Label_Type")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", HonourAchivementTypeConstants.LIST_TYPE)]
        [AdditionalMetadata("Placeholder","All")]
        public Nullable<int> HonourAchivementType { get; set; }

        [ResourceDisplayName("HonourAchivementType_Label_Resolution")]
        public string Resolution { get; set; }

        [ScaffoldColumn(false)]
        public int Type { get; set; }
    }
}
