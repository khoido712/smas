/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using System.Web.Mvc;

namespace SMAS.Web.Areas.HonourAchivementTypeArea.Models
{
    public class HonourAchivementTypeViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 HonourAchivementTypeID { get; set; }

        [ResourceDisplayName("HonourAchivementType_Label_Type")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]	
        [AdditionalMetadata("ViewDataKey", HonourAchivementTypeConstants.LIST_TYPE)]
        public Nullable<int> Type { get; set; }

        [ResourceDisplayName("HonourAchivementType_Label_Resolution")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]			
		public System.String Resolution { get; set; }

        //[ScaffoldColumn(false)]
        //public System.Nullable<System.Int32> Type { get; set; }

        [ResourceDisplayName("HonourAchivementType_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
		public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ScaffoldColumn(false)]
        public System.Boolean IsActive { get; set; }

        [ScaffoldColumn(false)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
       
        [ScaffoldColumn(false)]
        [ResourceDisplayName("HonourAchivementType_Label_TypeName")]
        public string TypeName { get; set; }
    }
}


