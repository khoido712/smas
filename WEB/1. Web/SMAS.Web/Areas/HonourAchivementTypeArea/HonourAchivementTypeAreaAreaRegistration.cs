﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HonourAchivementTypeArea
{
    public class HonourAchivementTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HonourAchivementTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HonourAchivementTypeArea_default",
                "HonourAchivementTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
