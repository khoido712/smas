/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.HonourAchivementTypeArea
{
    public class HonourAchivementTypeConstants
    {
        public const string LIST_TYPE = "listType";
        public const string LIST_HONOURACHIVEMENTTYPE = "listHonourAchivementType";
        public const string ISYEAR = "ISYEAR";
    }
}