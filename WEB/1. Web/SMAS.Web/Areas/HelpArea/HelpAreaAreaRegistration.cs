﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HelpArea
{
    public class HelpAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HelpArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HelpArea_default",
                "HelpArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
