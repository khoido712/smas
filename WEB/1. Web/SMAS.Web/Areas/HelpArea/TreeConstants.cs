﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Areas.HelpArea.Models;
namespace SMAS.Web.Areas.HelpArea
{
    public class TreeConstants
    {
        public const string Folder = "~/Help";
        public const string FileName = "HelpMenu.xml";
        public const string Menu_List = "Menu_List";
    }
}
