﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.HelpArea.Models;
using SMAS.Web.Areas.HelpArea;
using System.IO;
using SMAS.Web.Utils;
using System.Xml;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.HelpArea.Controllers
{
    public class HelpController : Controller
    {
        //
        // GET: /HelpArea/Help/

        public ActionResult Index(string extPath)
        {
            string root = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "");
            string hostName = UtilsBusiness.UrlSmas;
            GlobalInfo global = new GlobalInfo();
            int userAccountID = global.UserAccountID;
            //Quyền của người dùng
            //Phòng sở: 1 
            //Quản trị trường: 2
            //GVCN: 3
            //GVBM: 4
            //Admin Hệ thống: 5
            string HelpName = string.Empty;
            string HelpFolder = string.Empty;
            if (global.SchoolID > 0)
            {
                if (global.IsAdminSchoolRole || global.IsEmployeeManager)
                {
                    if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                    {
                        HelpFolder = "/HDSD/QuantriTruong/Cap1";
                    }
                    else if (global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE ||
                              global.AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
                    {
                        HelpFolder = "/HDSD/QuantriTruong/CapMN";
                    }
                    else
                    {
                        HelpFolder = "/HDSD/QuantriTruong/Cap23";
                    }
                }
                else
                {
                    if (SMAS.Business.Common.UtilsBusiness.HasHeadTeacherPermission(userAccountID, 0))
                    {
                        HelpFolder = "/HDSD/GVCN";
                    }
                    else
                    {
                        if (SMAS.Business.Common.UtilsBusiness.HasSubjectTeacherPermission_BM_HELP(userAccountID))
                        {
                            HelpFolder = "/HDSD/GVBM";
                        }
                        else
                        {
                            HelpFolder = "/HDSD/GVCN";
                        }
                    }
                }
            }
            else
            {
                if (global.IsSystemAdmin)
                {
                    HelpFolder = "/HDSD/QuantriTruong";
                }
                else
                {
                    HelpFolder = "/HDSD/PhongSo";
                }
            }
            HelpName = hostName + HelpFolder;
            if (!String.IsNullOrEmpty(extPath))
            {
                HelpName = HelpName + extPath;
            }
            ViewData[HelpConstants.HELP_NAME] = HelpName;
            return View();
        }
    }
}
