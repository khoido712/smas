﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HelpArea.Models
{
    public class TreeHelp
    {
        public TreeObject TreeObject { get; set; }
        public List<TreeHelp> MenuChildren { get; set; }
        public int CountChild {get{
            if (MenuChildren == null)
            {
                return 0;
            }
            else
            {
                return MenuChildren.Count();
            }
        }}

        public TreeHelp()
        {
            
        }
        public TreeHelp(string sMenuName)
        {
            TreeObject = new TreeObject(sMenuName);
            MenuChildren = null;
        }
        public TreeHelp(string sMenuName,List<TreeHelp> menu)
        {
            TreeObject = new TreeObject(sMenuName);
            MenuChildren = menu;
        }

    }
}