﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HelpArea.Models
{
    public class TreeObject
    {
        public string MenuName{get;set;}
        public string Title { get; set; }
        public string ShortURL { get; set; }
        public string URL { get; set; }
        public string MenuLevel { get; set; }

        public TreeObject(string tMenuName, string tTitle, string tShortURL, string tURL, string tMenuLevel)
        {
            this.MenuName = tMenuLevel;
            this.Title = tTitle;
            this.ShortURL = tShortURL;
            this.URL = tURL;
            this.MenuLevel = tMenuLevel;
        }
        public TreeObject(string tMenuName, string tShortURL)
        {
            this.MenuName = tMenuName;
            this.ShortURL = tShortURL;
        }
        public TreeObject(string tMenuName, string tShortURL,string tURl)
        {
            this.MenuName = tMenuName;
            this.ShortURL = tShortURL;
            this.URL = tURl;
        }
        public TreeObject(string tMenuName)
        {
            this.MenuName = tMenuName;
        }
    }
}