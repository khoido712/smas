﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamRoomAssignArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.ExamRoomAssignArea.Controllers
{
    [SkipCheckRole]
    public class ExamRoomAdjustController : BaseController
    {

        #region properties
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamRoomViewModel> listLeftRoom;
        private List<ExamRoomViewModel> listRightRoom;
        private GlobalInfo global;
        #endregion

        #region Constructor
        public ExamRoomAdjustController(IExamPupilBusiness ExamPupilBusiness, IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamRoomBusiness ExamRoomBusiness)
        {
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            global = new GlobalInfo();
        }
        #endregion


        #region Action
        //
        // GET: /ExamRoomAssignArea/ExamRoomAdjust/
        public ActionResult Index(long? ExaminationsID)
        {
            if (GetMenupermission("ExamRoomAssign", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Lay ra danh sach ky thi cua truong
            //Lấy danh sách kỳ thi
            List<Examinations> listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                             .Where(o => o.SchoolID == global.SchoolID)
                                             .Where(o => o.AppliedLevel == global.AppliedLevel)
                                             .OrderByDescending(o => o.CreateTime).ToList();
           //Kiem tra tham so hop le
            if (ExaminationsID == null)
            {
                throw new BusinessException("");
            }
            if (listExaminations.Find(o => o.ExaminationsID == ExaminationsID.Value) == null)
            {
                throw new BusinessException("");
            }

            SetViewData(ExaminationsID);

            long? defaultExamID = ExaminationsID;
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
               defaultExamGroupID =  listExamGroup.First().ExamGroupID ;
            }

            if (defaultExamID == null || defaultExamGroupID == null)
            {
                AdjustContentViewModel content = new AdjustContentViewModel();
                content.leftList = new List<ExamPupilViewModel>();
                content.rightList = new List<ExamPupilViewModel>();
                ViewData[ExamRoomAssignConstants.CONTENT] = content;
                return View();
            }

            //Dictionary de tim kiem thi sinh
            IDictionary<string, object> dic=new Dictionary<string,object>();
            dic["SchoolID"] = global.SchoolID;

            //Lay danh sach thi sinh trong phong thi ben trai
            long? defaultLeftRoomID = null;
            if (listLeftRoom.Count > 0) defaultLeftRoomID = listLeftRoom.First().ExamRoomID;
            List<ExamPupilViewModel> leftList = new List<ExamPupilViewModel>();
            if (defaultLeftRoomID != null)
            {
                leftList = this._Search(dic, defaultExamID, defaultExamGroupID, defaultLeftRoomID.Value).ToList();
            }

            //Lay danh sach thi sinh trong phong thi ben phai
            long? defaultRightRoomID = null;
            if (listRightRoom.Count > 0) defaultRightRoomID = listRightRoom.First().ExamRoomID;
            List<ExamPupilViewModel> rightList = new List<ExamPupilViewModel>();
            if (defaultRightRoomID != null)
            {
                rightList = this._Search(dic, defaultExamID, defaultExamGroupID, defaultRightRoomID.Value).ToList();
            }

            ////Set vao model
            AdjustContentViewModel model = new AdjustContentViewModel();
            model.leftList = leftList;
            model.rightList = rightList;

            ViewData[ExamRoomAssignConstants.CONTENT] = model;

            return PartialView();
        }

        //
        // GET: /ExamRoomAdjust/Search
        public PartialViewResult Search(SearchViewModel form)
        {
            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            GetRoomListForEachSide(examinationsID, examGroupID, null, null);

            if (examinationsID == null || examGroupID == null)
            {
                AdjustContentViewModel content = new AdjustContentViewModel();
                content.leftList = new List<ExamPupilViewModel>();
                content.rightList = new List<ExamPupilViewModel>();

                return PartialView("_Content", content);
            }

            //Dictionary de tim kiem thi sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;

            //Lay danh sach thi sinh trong phong thi ben trai
            long? defaultLeftRoomID = null;
            if (listLeftRoom.Count > 0) defaultLeftRoomID = listLeftRoom.First().ExamRoomID;
            List<ExamPupilViewModel> leftList = new List<ExamPupilViewModel>();
            if (defaultLeftRoomID != null)
            {
                leftList = this._Search(dic, examinationsID, examGroupID, defaultLeftRoomID.Value).ToList();
            }

            //Lay danh sach thi sinh trong phong thi ben phai
            long? defaultRightRoomID = null;
            if (listRightRoom.Count > 0) defaultRightRoomID = listRightRoom.First().ExamRoomID;
            List<ExamPupilViewModel> rightList = new List<ExamPupilViewModel>();
            if (defaultRightRoomID != null)
            {
                rightList = this._Search(dic, examinationsID, examGroupID, defaultRightRoomID.Value).ToList();
            }

            ////Set vao model
            AdjustContentViewModel model = new AdjustContentViewModel();
            model.leftList = leftList;
            model.rightList = rightList;

            return PartialView("_Content", model);
        }

        //
        // GET: /ExamRoomAdjust/EachSideSearch
        public PartialViewResult EachSideSearch(long? ExaminationsID, long? ExamGroupId, long? RoomID, string side)
        {

            if (ExaminationsID == null || ExamGroupId == null)
            {
                List<ExamRoomViewModel> content = new List<ExamRoomViewModel>();

                return PartialView(side, content);
            }

            //Dictionary de tim kiem thi sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;

            //Lay danh sach thi sinh trong phong thi ben phai
            long? defaultRoomID = RoomID;
         
            List<ExamPupilViewModel> examPupilList = new List<ExamPupilViewModel>();
            if (defaultRoomID != null)
            {
                examPupilList = this._Search(dic, ExaminationsID, ExamGroupId, defaultRoomID.Value).ToList();
            }

            return PartialView(side, examPupilList);
        }

        /// <summary>
        /// MoveTo
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult MoveTo(long examinationsID, long examGroupID, long leftRoomID, long? rightRoomID, string arrayID, int moveType)
        {
            if (rightRoomID == null)
            {
                throw new BusinessException("ExamRoomAssign_Message_NotDesRoom_ToMove");
            }
           
            long fromRoomID = moveType == 1 ? leftRoomID : rightRoomID.Value;
            long toRoomID = moveType == 1 ? rightRoomID.Value : leftRoomID;

            

            ExamRoom fromRoom = ExamRoomBusiness.Find(fromRoomID);
            ExamRoom toRoom = ExamRoomBusiness.Find(toRoomID);

            string[] examPupilIDArr;
            if (arrayID != "")
            {
                arrayID = arrayID.Remove(arrayID.Length - 1);
                examPupilIDArr = arrayID.Split(',');
            }
            else
            {
                examPupilIDArr = new string[] { };
            }

            List<long> listExamPupilID = examPupilIDArr.Length > 0 ? examPupilIDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            if (listExamPupilID.Count <= 0)
            {
                throw new BusinessException("ExamRoomAssign_Message_NotExamPupil_ToMove");
            }
            //Lay danh sach thi sinh can move
           int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
           List<ExamPupil> listExamPupilToMove = this.ExamPupilBusiness.GetExamPupilOfExaminations
                (examinationsID, global.SchoolID.Value, global.AcademicYearID.Value, partitionID)
                .Where(o=>o.ExamGroupID==examGroupID).Where(o=>listExamPupilID.Contains(o.ExamPupilID)).ToList();

           if (toRoomID > 0)
           {
               //Lay so cho trong cua phong chuyen den
               int inUsedSeatNumber = this.ExamPupilBusiness.GetExamPupilOfExaminations
                   (examinationsID, global.SchoolID.Value, global.AcademicYearID.Value, partitionID)
                   .Where(o => o.ExamGroupID == examGroupID)
                   .Where(o => o.ExamRoomID == toRoomID).Count();

               int availableSeatNumber = toRoom.SeatNumber - inUsedSeatNumber;
               if (listExamPupilToMove.Count > availableSeatNumber)
               {
                   throw new BusinessException(String.Format(Res.Get("ExamRoomAssign_Message_NotEnoughSeat"), listExamPupilToMove.Count));
               }
               for (int i = 0; i < listExamPupilToMove.Count; i++)
               {
                   if (availableSeatNumber == 0)
                   {
                       break;
                   }

                   listExamPupilToMove[i].ExamRoomID = toRoomID;

                   availableSeatNumber--;
               }
           }
           else
           {
               for (int i = 0; i < listExamPupilToMove.Count; i++)
               {
                   listExamPupilToMove[i].ExamRoomID = null;
               }
           }
           ExamPupilBusiness.UpdateList(listExamPupilToMove);

           return Json(new JsonMessage(Res.Get("ExamRoomAssign_Message_MoveSuccess")));
        }


        /// <summary>
        /// Devide
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Devide(long examinationsID, long examGroupID, long roomID)
        {

            //Lay danh sach thi sinh trong nhom
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            List<ExamPupil> listExamPupil = this.ExamPupilBusiness.GetExamPupilOfExaminations
                 (examinationsID, global.SchoolID.Value, global.AcademicYearID.Value, partitionID)
                 .Where(o => o.ExamGroupID == examGroupID)
                 .Where(o=>o.ExamRoomID!=null).ToList();

            //Lay danh sach thi sinh cua phong thi can chia

            List<ExamPupil> listExamPupilToMove = listExamPupil.Where(o => o.ExamRoomID == roomID).ToList();
            
            //Lay danh sach phong thi 
             List<ExamRoom> listExamRoom = this.ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                  .Where(o => o.ExamGroupID == examGroupID).ToList(); 
            
            //Lay phong thi can chia deu
             ExamRoom srcRoom = this.ExamRoomBusiness.Find(roomID);
             if (srcRoom == null)
             {
                 throw new BusinessException(Res.Get("ExamRoomAssign_Message_NotRoom_ToDevide"));
             }

            //Lay danh sach phong thi dich, loai tru phong nguon
             List<ExamRoom> listDesRoom = listExamRoom.Where(o => o.ExamRoomID != srcRoom.ExamRoomID).ToList();

            //Lay so cho trong trong moi phong thi dich
             var listCountByRoom = (from q in listExamPupil
                                    group q by q.ExamRoomID into qg
                                    select new
                                    {
                                        ExamRoomID = qg.Key,
                                        ExamPupilCount = qg.Count()
                                    }).ToList();

             List<ExamRoomViewModel> listDesRoomWithCount = (from er in listDesRoom
                                            join cbr in listCountByRoom
                                                on er.ExamRoomID equals cbr.ExamRoomID into des
                                            from x in des.DefaultIfEmpty()
                                            select new ExamRoomViewModel
                                            {
                                                ExamRoomID = er.ExamRoomID,
                                                SeatNumber = er.SeatNumber,
                                                SettedNumber = x != null ? x.ExamPupilCount : 0,
                                                ExamRoomCode = er.ExamRoomCode
                                            }).OrderBy(o => o.ExamRoomCode).ToList();

            //Lay tong so cho ngoi con trong
             int totalAvailableSeat = listDesRoomWithCount.Sum(o => o.availableSeatNumber);
             if (listExamPupilToMove.Count > totalAvailableSeat)
             {
                 throw new BusinessException(String.Format(Res.Get("ExamRoomAssign_Message_NotEnoughSeat"), listExamPupilToMove.Count));
             }

             ExamRoomAssignUtils utils = new ExamRoomAssignUtils();
             utils.DividedEqually(listExamPupilToMove, listExamPupil, listDesRoomWithCount);

             ExamPupilBusiness.UpdateList(listExamPupilToMove);

            return Json(new JsonMessage(Res.Get("ExamRoomAssign_Message_DevideSuccess")));
        }

        public JsonResult AjaxLoadExamGroup(long? paraExaminationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(paraExaminationsID.GetValueOrDefault())
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadRightRoom(long ExaminationsID, long ExamGroupID, long leftRoomID, long rightRoomID)
        {
            List<ExamRoomViewModel> listRoom = this.GetListExamRoom(ExaminationsID, ExamGroupID);

            //Neu khong con thi sinh chua duoc xep phong thi thi khong hien thi dong nay
            if (listRoom[0].ExamRoomCode == "[Chưa xếp phòng](0)")
            {
                listRoom.RemoveAt(0);
            }
            //Tao danh sach phong thi cho phan ben phai
            listRightRoom = listRoom.Where(o=>o.ExamRoomID!=leftRoomID).ToList();

            return Json(new SelectList(listRightRoom, "ExamRoomID", "ExamRoomCode",rightRoomID));
        }

        #endregion

        #region Private method
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData(long? ExaminationsID)
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                             .Where(o => o.SchoolID == global.SchoolID)
                                             .Where(o => o.AppliedLevel == global.AppliedLevel)
                                             .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamRoomAssignConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");

            long? defaultExamID = ExaminationsID;

            //Lấy danh sách nhóm thi
            listExamGroup = defaultExamID!=null? ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                .OrderBy(o => o.ExamGroupCode).ToList(): new List<ExamGroup>();
            ViewData[ExamRoomAssignConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            //Lay danh sach phong thi
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
                defaultExamGroupID =  listExamGroup.First().ExamGroupID ;
            }

            GetRoomListForEachSide(defaultExamID,defaultExamGroupID, null, null);

        }

        private List<ExamRoomViewModel> GetListExamRoom(long? examinationsID, long? examGroupID)
        {
            if (examinationsID == null || examGroupID == null)
            {
                return new List<ExamRoomViewModel>();
            }
            //Lay danh sach phong thi
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID).Where(o => o.ExamGroupID == examGroupID).ToList();

            //Dem so luong thi sinh da co trong phong thi
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationsID"] = examinationsID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["ExamGroupID"] = examGroupID;
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            IQueryable<ExamPupilBO> query = this.ExamPupilBusiness.Search(SearchInfo, global.AcademicYearID.GetValueOrDefault(), partitionID);

            var listCountByRoom = (from q in query
                                   group q by q.ExamRoomID into qg
                                   select new
                                   {
                                       ExamRoomID = qg.Key,
                                       ExamPupilCount = qg.Count()
                                   }).ToList();

            List<ExamRoomViewModel> ret = (from er in lstExamRoom
                                           join cbr in listCountByRoom
                                               on er.ExamRoomID equals cbr.ExamRoomID into des
                                           from x in des.DefaultIfEmpty()
                                           select new ExamRoomViewModel
                                           {
                                               ExamRoomID = er.ExamRoomID,
                                               SeatNumber = er.SeatNumber,
                                               SettedNumber = x != null ? x.ExamPupilCount : 0,
                                               ExamRoomCode = er.ExamRoomCode
                                           }).OrderBy(o => o.ExamRoomCode).ToList();

            long defaultExamRoomID = ret.FirstOrDefault() != null ? ret.FirstOrDefault().ExamRoomID : 0;
            ViewData[ExamRoomAssignConstants.DEFAULT_EXAM_ROOM] = defaultExamRoomID;

            //Lay so thi sinh chua duoc xep phong thi
            int notAsignedRoomExamPupilCount = query.Where(o => o.ExamRoomID == null).Count();
            ExamRoomViewModel firstRow = new ExamRoomViewModel();
            firstRow.ExamRoomID = -1;
            firstRow.ExamRoomCode = "[Chưa xếp phòng](" + notAsignedRoomExamPupilCount.ToString() + ")";
            ret.Insert(0, firstRow);

            return ret;
        }

        private void GetRoomListForEachSide(long? examinationsID, long? examGroupID, long? defaultLeftRoomID, long? defaultRightRoomID)
        {
            
            List<ExamRoomViewModel> listRoom = this.GetListExamRoom(examinationsID, examGroupID);

            if (listRoom.Count <= 0)
            {
                listLeftRoom = new List<ExamRoomViewModel>();
                listRightRoom = new List<ExamRoomViewModel>();
               ViewData[ExamRoomAssignConstants.CBO_LEFT_ROOM] =new SelectList(new List<ExamRoomViewModel>());
               ViewData[ExamRoomAssignConstants.CBO_RIGHT_ROOM] = new SelectList(new List<ExamRoomViewModel>());
            }
            else if (listRoom.Count == 1 && listRoom[0].ExamRoomCode == "[Chưa xếp phòng](0)")
            {
                listLeftRoom = new List<ExamRoomViewModel>();
                listRightRoom = new List<ExamRoomViewModel>();
                ViewData[ExamRoomAssignConstants.CBO_LEFT_ROOM] = new SelectList(new List<ExamRoomViewModel>());
                ViewData[ExamRoomAssignConstants.CBO_RIGHT_ROOM] = new SelectList(new List<ExamRoomViewModel>());
            }
            else
            {
                //Neu khong con thi sinh chua duoc xep phong thi thi khong hien thi dong nay
                if (listRoom[0].ExamRoomCode == "[Chưa xếp phòng](0)")
                {
                    listRoom.RemoveAt(0);
                }

                //Tao danh sach phong thi cho phan ben trai
                listLeftRoom = new List<ExamRoomViewModel>(listRoom);
                if (defaultLeftRoomID != null)
                    ViewData[ExamRoomAssignConstants.CBO_LEFT_ROOM] = new SelectList(listLeftRoom, "ExamRoomID", "ExamRoomCode", defaultLeftRoomID.Value);
                else
                    ViewData[ExamRoomAssignConstants.CBO_LEFT_ROOM] = new SelectList(listLeftRoom, "ExamRoomID", "ExamRoomCode");

                //Tao danh sach phong thi cho phan ben phai
                //Lay ra phong phai xoa ben trai
                ExamRoomViewModel removeRoom;
                if (defaultLeftRoomID != null)
                    removeRoom = listRoom.Find(o => o.ExamRoomID == defaultLeftRoomID);
                else
                    removeRoom = listRoom[0];
                if (listRoom.Count > 0)
                {
                    listRoom.Remove(removeRoom);
                }
                listRightRoom = new List<ExamRoomViewModel>(listRoom);
                if (defaultRightRoomID != null)
                {
                    ViewData[ExamRoomAssignConstants.CBO_RIGHT_ROOM] = new SelectList(listRightRoom, "ExamRoomID", "ExamRoomCode", defaultRightRoomID.Value);
                }
                else
                {
                    ViewData[ExamRoomAssignConstants.CBO_RIGHT_ROOM] = new SelectList(listRightRoom, "ExamRoomID", "ExamRoomCode");
                }
            }
        }

        /// <summary>
        /// Hàm tìm kiếm
        /// </summary>
        /// <param name="SearchInfo">Dictionary chứa các điều kiện search</param>
        /// <returns>Danh sách kết quả</returns>
        private IEnumerable<ExamPupilViewModel> _Search(IDictionary<string, object> SearchInfo, long? ExaminationsID, long? ExamGroupID, long ExamRoomID)
        {

            if (ExaminationsID == null || ExamGroupID == null)
            {
                return new List<ExamPupilViewModel>();
            }
            SearchInfo["ExaminationsID"] = ExaminationsID.Value;
            SearchInfo["ExamGroupID"] = ExamGroupID.Value;
            if (ExamRoomID > 0)
            {
                SearchInfo["ExamRoomID"] = ExamRoomID;
            }
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            //Tim kiem 
            IQueryable<ExamPupilBO> query = this.ExamPupilBusiness.Search(SearchInfo, global.AcademicYearID.GetValueOrDefault(), partitionID);
            if (ExamRoomID < 0)
            {
                query = query.Where(o => o.ExamRoomID == null);
            }

            IQueryable<ExamPupilViewModel> ret = query.Select(o => new ExamPupilViewModel
            {
                BirthDay = o.BirthDate,
                ClassDisplayName = o.ClassName,
                ClassID = o.ClassID,
                ExamineeNumber = o.ExamineeNumber,
                ExamPupilID = o.ExamPupilID,
                Genre = o.Genre,
                PupilCode = o.PupilCode,
                PupilName = o.PupilFullName

            }).OrderBy(o => o.ExamineeNumber);

            return ret;
        }

        #endregion
    }
}
