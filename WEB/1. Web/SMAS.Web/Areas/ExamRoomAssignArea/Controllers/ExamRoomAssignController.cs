﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamRoomAssignArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.ExamRoomAssignArea.Controllers
{
    public class ExamRoomAssignController : BaseController
    {

        #region properties
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness;
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;
        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private GlobalInfo global;
        #endregion

        #region Constructor
        public ExamRoomAssignController(IExamPupilBusiness ExamPupilBusiness, IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamRoomBusiness ExamRoomBusiness, IAcademicYearBusiness AcademicYearBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness, IExamPupilViolateBusiness ExamPupilViolateBusiness, IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness,
            IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness)
        {
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.ExamSupervisoryViolateBusiness = ExamSupervisoryViolateBusiness;
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
            global = new GlobalInfo();
        }
        #endregion

        public int totalRecord = 0;
        #region Action
        //
        // GET: /ExamRoomAssignArea/ExamRoomAssign/
        public ActionResult Index()
        {

            SetViewData();
            ContentViewModel model = new ContentViewModel();

            //Lay danh sach phong thi
            long? defaultExamID=null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.First().ExaminationsID ;
            }
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
                defaultExamGroupID =listExamGroup.First().ExamGroupID ;
            }

            if (defaultExamID == null || defaultExamGroupID == null)
            {
                ContentViewModel content = new ContentViewModel();
                content.listExamPupil = new List<ExamPupilViewModel>();
                content.listExamRoom = new List<ExamRoomViewModel>();
                CheckCommandPermision(null, null, null);

                ViewData[ExamRoomAssignConstants.CONTENT] = content;
                return View();
            }
            List<ExamRoomViewModel> listRoom = this.GetListExamRoom(defaultExamID, defaultExamGroupID);

            //Lay danh sach thi sinh trong phong thi
            IDictionary<string,object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = defaultExamID;
            dic["ExamGroupID"] = defaultExamGroupID;
            dic["SchoolID"] = global.SchoolID;
            long examRomID = listRoom.FirstOrDefault().ExamRoomID;
            List<ExamPupilViewModel> listExamPupil = this._Search(dic, defaultExamID, defaultExamGroupID, examRomID, 1, ref totalRecord).ToList();
            ViewData[ExamRoomAssignConstants.TOTAL] = totalRecord;

            //Set vao model
            model.listExamRoom = listRoom;
            model.listExamPupil = listExamPupil;

            ViewData[ExamRoomAssignConstants.CONTENT] = model;

            Examinations exam = ExaminationsBusiness.Find(defaultExamID);
            ExamGroup examGroup = ExamGroupBusiness.Find(defaultExamGroupID);
            CheckCommandPermision(exam, examGroup, null);
            return View();
        }

        //
        // GET: /ExamRoomAssign/Search
        public PartialViewResult Search(SearchViewModel form)
        {
            if (form.ExaminationsID == null || form.ExamGroupID == null)
            {
                ContentViewModel content = new ContentViewModel();
                content.listExamPupil = new List<ExamPupilViewModel>();
                content.listExamRoom = new List<ExamRoomViewModel>();
                CheckCommandPermision(null, null, null);
                return PartialView("_Content", content);
            }
            //Lay danh sach phong thi
            List<ExamRoomViewModel> listRoom = this.GetListExamRoom(form.ExaminationsID, form.ExamGroupID);

            //Add search info - Navigate to Search function in business
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;
            long examRoomID = listRoom.FirstOrDefault().ExamRoomID;
            List<ExamPupilViewModel> listExamPupil = _Search(dic, form.ExaminationsID, form.ExamGroupID, examRoomID, 1, ref totalRecord);
            ViewData[ExamRoomAssignConstants.TOTAL] = totalRecord;

            //Set vao model
            ContentViewModel model = new ContentViewModel();
            model.listExamRoom = listRoom;
            model.listExamPupil = listExamPupil;

            Examinations exam = ExaminationsBusiness.Find(form.ExaminationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(form.ExamGroupID);
            CheckCommandPermision(exam, examGroup, null);

            return PartialView("_Content", model);
        }

        //
        // GET: /ExamRoomAssign/Search
        public PartialViewResult SearchByRoom(long?  ExaminationsID, long? ExamGroupId, long ExamRoomID, GridCommand command)
        {
            if (ExaminationsID == null || ExamGroupId == null)
            {
                CheckCommandPermision(null, null, null);
                return PartialView("_ExamPupilList", new List<ExamPupilViewModel>()); 
            }
            //Lay danh sach thi sinh trong phong thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID;


            //Add search info - Navigate to Search function in business
            List<ExamPupilViewModel> listResult = _Search(dic, ExaminationsID, ExamGroupId, ExamRoomID, command.Page, ref totalRecord);
            ViewData[ExamRoomAssignConstants.TOTAL] = totalRecord;

            //Kiem tra button
            Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(ExamGroupId);
            ExamRoom examRoom = ExamRoomBusiness.Find(ExamRoomID);
            CheckCommandPermision(exam, examGroup, examRoom);

            return PartialView("_ExamPupilList", listResult);
        }

        [HttpPost]
        [GridAction(EnableCustomBinding = true)]
        public ActionResult SearchExamPupilAjax(long? ExaminationsID, long? ExamGroupId, long? ExamRoomID, GridCommand command)
        {
            int currentPage = command.Page;
            int pageSize = command.PageSize;

            //Lay danh sach thi sinh trong phong thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
          
            dic["SchoolID"] = global.SchoolID;

            long examRoomID = ExamRoomID.GetValueOrDefault();
            //Add search info - Navigate to Search function in business
            List<ExamPupilViewModel> listResult = _Search(dic, ExaminationsID, ExamGroupId, examRoomID, command.Page, ref totalRecord);
            return View(new GridModel<ExamPupilViewModel>
            {
                Total = totalRecord,
                Data = listResult
            });
        }

        /// <summary>
        /// Auto Asign
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult AutoAssign(AutoAssignViewModel form)
        {
            if (ModelState.IsValid)
            {
                if (GetMenupermission("ExamRoomAssign", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                Utils.Utils.TrimObject(form);
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("SchoolID", _globalInfo.SchoolID);
                search.Add("ExaminationsID", form._ExaminationsID);
                int assignScale = form.AssignScale;
                long examinationsID = form._ExaminationsID;
                //Lay danh sach phong thi trong ky thi
                int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
                List<ExamRoom> listExamRoom = this.ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID).OrderBy(o => o.ExamRoomCode).ToList();
                if (listExamRoom.Count == 0)
                {
                    throw new BusinessException("Validate_Declare_ExamRoom");
                }
                List<ExamPupilBO> listExamPupilBO = ExamPupilBusiness.GetListExamPupilOrderBy(search).ToList();
                listExamPupilBO = listExamPupilBO.OrderBy(p => p.ExamineeNumber).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilFullName.getOrderingName(p.EthnicCode))).ToList();
                int totalPupil = 0;
                int countExamineeCode = 0;
                int checkAll = assignScale == ExamRoomAssignConstants.ASSIGN_SCALE_ALL ? 1 : 0;
                List<ExamPupil> listToUpdate = new List<ExamPupil>();
                List<ExamPupil> listToUpdateTotal = new List<ExamPupil>();
                //Neu xep phong thi cho nhom thi hien tai
                //Lay danh sach thi sinh cua ky thi
                List<ExamPupil> listExamPupil = listExamPupilBO.Select(o => new ExamPupil
                {
                    ExamPupilID = o.ExamPupilID,
                    ExaminationsID = o.ExaminationsID,
                    ExamGroupID = o.ExamGroupID,
                    ExamRoomID = o.ExamRoomID,
                    EducationLevelID = o.EducationLevelID,
                    SchoolID = o.SchoolID,
                    LastDigitSchoolID = o.LastDigitSchoolID,
                    PupilID = o.PupilID,
                    ExamineeNumber = o.ExamineeNumber,
                    CreateTime = o.CreateTime,
                    UpdateTime = DateTime.Now

                }).Distinct().ToList();

                List<ExamRoom> lstExamRoomtmp = null;
                bool isUpdateExamineeCode = false;
                if (assignScale == ExamRoomAssignConstants.ASSIGN_SCALE_CURRENT)
                {
                    search.Add("ExamGroupID", form._ExamGroupID);
                    ValidateBeforeAssign(form, listExamPupil, listExamRoom);
                    countExamineeCode = listExamPupil.Where(p => p.ExamGroupID == form._ExamGroupID && p.ExamineeNumber == null).Count();//so luong HS chua danh so bao danh
                    totalPupil = listExamPupil.Where(p => p.ExamGroupID == form._ExamGroupID).Count();
                    lstExamRoomtmp = listExamRoom.Where(p => p.ExamGroupID == form._ExamGroupID).ToList();
                    if ("on".Equals(form.chkRetypeExamineeCode))
                    {
                        listToUpdate = this.AutoAssignHandle(form, listExamPupil, lstExamRoomtmp);
                        isUpdateExamineeCode = true;
                    }
                    else
                    {
                        if (countExamineeCode > 0 && countExamineeCode < totalPupil)//Tim thay hon 1 HS chua duoc danh SBD
                        {
                            //return Json(new JsonMessage(Res.Get("ExamRoomAssign_Validate_NotExaminee"), "error"));
                            throw new BusinessException("ExamRoomAssign_Validate_NotExaminee");
                        }
                        else
                        {
                            listToUpdate = this.AutoAssignHandle(form, listExamPupil, lstExamRoomtmp);//tat ca HS chua duoc danh SBD thi chia phong xong danh lai SBD
                           // isUpdateExamineeCode = true;
                        }
                    }

                    listToUpdateTotal.AddRange(listToUpdate);
                }
                else if (assignScale == ExamRoomAssignConstants.ASSIGN_SCALE_ALL)
                {
                    listToUpdate = new List<ExamPupil>();
                    //Lay danh sach nhom thi cua ky thi
                    List<ExamGroup> listExamGroup = this.ExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID).ToList();
                    for (int i = 0; i < listExamGroup.Count; i++)
                    {
                        ExamGroup examGroup = listExamGroup[i];
                        form._ExamGroupID = examGroup.ExamGroupID;
                        ValidateBeforeAssign(form, listExamPupil, listExamRoom);
                        countExamineeCode = listExamPupil.Where(p => p.ExamGroupID == form._ExamGroupID && p.ExamineeNumber == null).Count();//so luong HS chua danh so bao danh
                        totalPupil = listExamPupil.Where(p => p.ExamGroupID == form._ExamGroupID).Count();
                        lstExamRoomtmp = listExamRoom.Where(p => p.ExamGroupID == form._ExamGroupID).ToList();
                        if ("on".Equals(form.chkRetypeExamineeCode))//chon danh lai SBD
                        {
                            listToUpdate.AddRange(this.AutoAssignHandle(form, listExamPupil, lstExamRoomtmp));
                            isUpdateExamineeCode = true;
                        }
                        else
                        {
                            if (countExamineeCode > 0 && countExamineeCode < totalPupil)//Tim thay hon 1 HS chua duoc danh SBD
                            {
                               // return Json(new JsonMessage(Res.Get("ExamRoomAssign_Validate_NotExaminee"), "error"));
                                throw new BusinessException("ExamRoomAssign_Validate_NotExaminee");
                            }
                            else
                            {
                                listToUpdate = this.AutoAssignHandle(form, listExamPupil, lstExamRoomtmp);//tat ca HS chua duoc danh SBD thi chia phong xong danh lai SBD
                            }
                        }

                        listToUpdateTotal.AddRange(listToUpdate);
                    }
                }
                this.ExamPupilBusiness.UpdateList(listToUpdateTotal);
                //sau khi xep phong thi xong thi danh lai SBD cho HS
                if (form.AssignType == 4 && isUpdateExamineeCode)
                {
                    this.ExamPupilBusiness.UpdateExamineeCodeByRoom(search, form.Criteria, checkAll, form.CriteriaIIText, form.CriteriaIIIText);    
                }
                return Json(new JsonMessage(Res.Get("ExamRoomAssign_Message_AssignSuccess")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(long ExaminationsID, long ExamGroupID,long ExamRoomID)
        {
            if (GetMenupermission("ExamRoomAssign", global.UserAccountID, global.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            //Lay ra danh sach thi sinh cua phong thi
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            List<ExamPupil> listExamPupil = this.ExamPupilBusiness.GetExamPupilOfExaminations
                 (ExaminationsID, global.SchoolID.Value, global.AcademicYearID.Value, partitionID)
                 .Where(o => o.ExamGroupID == ExamGroupID)
                 .Where(o=>o.ExamRoomID==ExamRoomID).ToList();

            for (int i = 0; i < listExamPupil.Count; i++)
            {
                listExamPupil[i].ExamRoomID = null;
            }

            this.ExamPupilBusiness.UpdateList(listExamPupil);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public JsonResult AjaxLoadExamGroup(long? paraExaminationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(paraExaminationsID.GetValueOrDefault())
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }
        #endregion

        #region Private method
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(global.AcademicYearID.GetValueOrDefault())
                                             .Where(o => o.SchoolID == global.SchoolID)
                                             .Where(o => o.AppliedLevel == global.AppliedLevel)
                                             .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamRoomAssignConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");

            long defaultExamID = listExaminations.FirstOrDefault() != null ? listExaminations.FirstOrDefault().ExaminationsID : 0;

            //Lấy danh sách nhóm thi

            listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID)
                                                    .OrderBy(o => o.ExamGroupCode).ToList();
            ViewData[ExamRoomAssignConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

        }

        private List<ExamRoomViewModel> GetListExamRoom(long? examinationsID, long? examGroupID)
        {
            if (examinationsID == null || examGroupID == null)
            {
                return new List<ExamRoomViewModel>();
            }
            //Lay danh sach phong thi
            List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID && o.ExamGroupID == examGroupID).ToList();

            //Dem so luong thi sinh da co trong phong thi
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationsID"] = examinationsID;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["ExamGroupID"] = examGroupID;
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            List<ExamPupilBO> query = ExamPupilBusiness.Search(SearchInfo, global.AcademicYearID.GetValueOrDefault(), partitionID).ToList();

            List<RoomExamPupilBO> listCountByRoom = (from q in query
                                   group q by q.ExamRoomID into qg
                                   select new RoomExamPupilBO
                                   {
                                       ExamRoomID = qg.Key,
                                       ExamPupilCount = qg.Count()
                                   }).ToList();

            List<ExamRoomViewModel> ret = (from er in lstExamRoom
                                           join cbr in listCountByRoom on er.ExamRoomID equals cbr.ExamRoomID into des
                                           from x in des.DefaultIfEmpty()
                                           select new ExamRoomViewModel
                                           {
                                               ExamRoomID = er.ExamRoomID,
                                               SeatNumber = er.SeatNumber,
                                               SettedNumber = (x!=null?x.ExamPupilCount:0),
                                               ExamRoomCode=er.ExamRoomCode
                                           }).OrderBy(o=>o.ExamRoomCode).ToList();

            //Lay so thi sinh chua duoc xep phong thi
            int notAsignedRoomExamPupilCount = query.Count(o => o.ExamRoomID == null);
            ExamRoomViewModel firstRow = new ExamRoomViewModel();
            firstRow.ExamRoomID = -1;
            firstRow.DisplayString = "[Chưa xếp phòng thi](" + notAsignedRoomExamPupilCount.ToString() + ")";
            ret.Insert(0, firstRow);

            return ret;
        }

        /// <summary>
        /// Hàm tìm kiếm
        /// </summary>
        /// <param name="SearchInfo">Dictionary chứa các điều kiện search</param>
        /// <returns>Danh sách kết quả</returns>
        private List<ExamPupilViewModel> _Search(IDictionary<string, object> SearchInfo, long? examinationsID, long? examGroupID, long ExamRoomID, int currentPage, ref int totaRecord)
        {
            if (examinationsID == null || examGroupID == null)
            {
                return new List<ExamPupilViewModel>();
            }
            SearchInfo["ExaminationsID"] = examinationsID.Value;
            SearchInfo["ExamGroupID"] = examGroupID.Value;
            if (ExamRoomID > 0)
            {
                SearchInfo["ExamRoomID"] = ExamRoomID;
            }
            int partitionID = UtilsBusiness.GetPartionId(global.SchoolID.Value);
            //Tim kiem 
            IQueryable<ExamPupilBO> query = this.ExamPupilBusiness.Search(SearchInfo, _globalInfo.AcademicYearID.Value, partitionID);
            if (ExamRoomID < 0)
            {
                query = query.Where(o => o.ExamRoomID == null);
            }
            totalRecord = query.Count();
            //phan trang
            List<ExamPupilBO> listExamPupilBOList = query.OrderBy(p=>p.ExamineeNumber).Skip((currentPage - 1) * ExamRoomAssignConstants.PageSize).Take(ExamRoomAssignConstants.PageSize).ToList();
           //Chuyen kieu tra ve
            List<ExamPupilViewModel> ret = listExamPupilBOList.Select(o => new ExamPupilViewModel
            {
                BirthDay = o.BirthDate,
                ClassDisplayName = o.ClassName,
                ClassID = o.ClassID,
                ExamineeNumber=o.ExamineeNumber,
                ExamPupilID = o.ExamPupilID,
                Genre = o.Genre,
                PupilCode = o.PupilCode,
                PupilName = o.PupilFullName
            }).ToList();

            return ret;
        }

        /// <summary>
        /// Ham xu ly xep phong thi tu dong
        /// </summary>
        /// <param name="assignType">Loai xep</param>
        /// <param name="examPupilNum">So luong thi sinh neu la xep theo so thi sinh/phong thi</param>
        /// <param name="examRoomNum">So luong phong thi neu la xep theo so phong thi</param>
        /// <param name="examGroupID">ID nhom thi</param>
        /// <param name="examinationsID">ID ky thi</param>
        /// <param name="listExamPupilOfExaminations">List tat ca thi sinh cua ky thi</param>
        /// <param name="listExamRoomOfExaminations">List tat ca phong thi cua ky thi</param>
        /// <returns></returns>
        private List<ExamPupil> AutoAssignHandle(AutoAssignViewModel form, List<ExamPupil> listExamPupilOfExaminations, List<ExamRoom> listExamRoomOfExaminations)
        {
            int assignType = form.AssignType;
            int assignScale = form.AssignScale;
            int examPupilNum = Convert.ToInt32(form.ExamPupilNumber);
            int examRoomNum = Convert.ToInt32(form.RoomNumber);
            long examGroupID = form._ExamGroupID;
            long examinationsID = form._ExaminationsID;
            ExamRoomAssignUtils utils = new ExamRoomAssignUtils();

            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupil> listPupilInRoom = listExamPupilOfExaminations.Where(p => p.ExamGroupID == examGroupID).ToList();
            //Reset phong thi cho cac thi sinh
            for (int i = 0; i < listPupilInRoom.Count; i++)
            {
                listPupilInRoom[i].ExamRoomID = null; 
            }
            //Neu nhom thi chua co phong thi thi thoat
            List<ExamRoomViewModel> listExamRoomViewModel = listExamRoomOfExaminations.Select(o => new ExamRoomViewModel
                                                                                {
                                                                                    ExamRoomID = o.ExamRoomID,
                                                                                    ExamRoomCode = o.ExamRoomCode,
                                                                                    SeatNumber = o.SeatNumber,
                                                                                    SettedNumber = 0
                                                                                }).OrderBy(o => o.ExamRoomCode).ToList();

            //Chia theo so luong thi sinh/phong thi
            if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_EXAM_PUPIL_PER_ROOM)
            {
                int totalExamPupil = listPupilInRoom.Count;
                //Lay so luong phong thi can thiet
                int roomNumber = totalExamPupil % examPupilNum != 0 ? totalExamPupil / examPupilNum + 1 : totalExamPupil / examPupilNum;

                if (roomNumber > listExamRoomOfExaminations.Count)
                {
                    roomNumber = listExamRoomOfExaminations.Count;
                }
                int indexEP = 0;
                //Duyet cac phong thi
                for (int i = 0; i < roomNumber; i++)
                {
                    ExamRoom er = listExamRoomOfExaminations[i];
                    int indexER=0;
                    while (indexER < examPupilNum && indexER<er.SeatNumber && indexEP<totalExamPupil)
                    {
                        ExamPupil ep = listPupilInRoom[indexEP];
                        ep.ExamRoomID = er.ExamRoomID;

                        indexER++;
                        indexEP++;
                    }

                    if (indexEP == totalExamPupil)
                    {
                        break;
                    }
                }
            }
            //Chia theo so phong thi
            else if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_ROOM_NUMBER)
            {

                utils.DividedEqually(listPupilInRoom, listPupilInRoom, listExamRoomViewModel.Take(examRoomNum).ToList());
            }
            //Chia theo so phong thi hien co
            else if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_REMAIN_ROOM)
            {
                utils.DividedEqually(listPupilInRoom, listPupilInRoom, listExamRoomViewModel);
            }
            //Chia deu thi sinh cac khoi
            else if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_EDUCATION_PER_ROOM)
            {
                utils.DividedEquallyRetype(listPupilInRoom, listExamRoomViewModel);
            }

            return listPupilInRoom;

        }

        /// <summary>
        /// Ham kiem tra du lieu truoc khi xep phong thi tu dong
        /// </summary>
        /// <param name="?"></param>
        private void ValidateBeforeAssign(AutoAssignViewModel form, List<ExamPupil> listExamPupil, List<ExamRoom> listExamRoom)
        {

            int assignType = form.AssignType;
            int assignScale = form.AssignScale;
            long examGroupID = form._ExamGroupID;

            //Kiem tra thi sinh da duoc danh so bao hay chua
            List<ExamPupil> listTempEP=listExamPupil;
            if(assignScale==ExamRoomAssignConstants.ASSIGN_SCALE_CURRENT)
            {
                listTempEP = listExamPupil.Where(o => o.ExamGroupID == examGroupID).ToList();
            }
            //if (listTempEP.Where(o => o.ExamineeNumber == null).ToList().Count > 0)
            //{
            //    throw new BusinessException("ExamRoomAssign_Validate_NotExaminee");
            //}

            //Neu chia theo so luong thi sinh/phong thi
            if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_EXAM_PUPIL_PER_ROOM)
            {
                //Chua nhap so thi sinh
                if (String.IsNullOrEmpty(form.ExamPupilNumber))
                {
                    throw new BusinessException("ExamRoomAssign_Validate_Required_ExamPupilNumber");
                }

                //So thi sinh nhap vao lon hon so cho ngoi nho nhat
                int examPupilNum = Convert.ToInt32(form.ExamPupilNumber);
                List<ExamRoom> listTempER = listExamRoom.Where(o => o.ExamGroupID == examGroupID).ToList();
                if (listTempER.Count > 0)
                {
                    int minSeatNumber = listTempER.Min(o => o.SeatNumber);
                    if (examPupilNum > minSeatNumber)
                    {
                        throw new BusinessException(String.Format(Res.Get("ExamRoomAssign_Validate_OutOfRange_ExamPupilNumber"), minSeatNumber.ToString()));
                    }
                }
            }

            //Neu chia theo so luong phong thi
            if (assignType == ExamRoomAssignConstants.ASSIGN_TYPE_BY_ROOM_NUMBER)
            {
                //Chua nhap so phong thi
                if (String.IsNullOrEmpty(form.RoomNumber))
                {
                    throw new BusinessException("ExamRoomAssign_Validate_Required_RoomNumber");
                }
                int examRoomNum = Convert.ToInt32(form.RoomNumber);

                //Nhap qua so phong thi cho phep
                int roomNum = Convert.ToInt32(form.RoomNumber);
                List<ExamRoom> listTempER = listExamRoom.Where(o => o.ExamGroupID == examGroupID).ToList();
                if (listTempER.Count < roomNum)
                {
                    throw new BusinessException("ExamRoomAssign_Validate_OutOfRange_RoomNum");
                }
            }
        }

        /// <summary>
        /// Kiem tra disable/enable
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations, ExamGroup examGroup, ExamRoom examRoom)
        {
            SetViewDataPermission("ExamRoomAssign", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            //Kiem tra quyen xep phong thi,hieu chinh
            bool checkAssignOrAjust = false;

            if (examinations != null && examGroup!=null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                    && examinations.MarkInput.GetValueOrDefault() != true
                    && examinations.MarkClosing.GetValueOrDefault() != true
                    && global.IsCurrentYear)
                {
                    checkAssignOrAjust = true;
                }
            }
            ViewData[ExamRoomAssignConstants.PER_CHECK_ASSIGN] = checkAssignOrAjust;

            //Kiem tra quyen xoa
            bool checkDelete = false;
            AcademicYear academicYearObj= AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);            
            DateTime currentDate = DateTime.Now;
            if (examinations != null && examGroup != null && examRoom!=null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE]
                    && examinations.MarkInput.GetValueOrDefault() != true
                    && examinations.MarkClosing.GetValueOrDefault() != true
                    && checkRoomContraint(examRoom)
                    && ((currentDate >= academicYearObj.FirstSemesterStartDate && currentDate <= academicYearObj.FirstSemesterEndDate) 
                    ||(currentDate >= academicYearObj.SecondSemesterStartDate && currentDate<=academicYearObj.SecondSemesterEndDate)))
                {
                    checkDelete = true;
                }
            }

            ViewData[ExamRoomAssignConstants.PER_CHECK_DELETE] = checkDelete;
        }

        private bool checkRoomContraint(ExamRoom examRoom)
        {
            bool result = true;
            long examRoomID = examRoom.ExamRoomID;
            //Chua phan cong giam thi cho phong thi
            IQueryable<ExamSupervisoryAssignment> listAssign = ExamSupervisoryAssignmentBusiness.All.Where(o => o.ExamRoomID == examRoomID);
            if (listAssign.Count() > 0)
            {
                result = false;
            }
            //Chua co thi sinh vi pham quy che
            IQueryable<ExamPupilViolate> listPupilViolate = ExamPupilViolateBusiness.All.Where(o => o.ExamRoomID == examRoomID);
            if (listPupilViolate.Count() > 0)
            {
                result = false;
            }
            //Chua co giam thi vi pham quy che thi
            IQueryable<ExamSupervisoryViolate> listSupervisoryViolate = ExamSupervisoryViolateBusiness.All.Where(o => o.ExamRoomID == examRoomID);
            if (listSupervisoryViolate.Count() > 0)
            {
                result = false;
            }
            //Chua co thi sinh vang thi
            IQueryable<ExamPupilAbsence> listPupilAbsence = ExamPupilAbsenceBusiness.All.Where(o => o.ExamRoomID == examRoomID);
            if (listPupilAbsence.Count() > 0)
            {
                result = false;
            }
            return result;
        }
        #endregion
    }
}
