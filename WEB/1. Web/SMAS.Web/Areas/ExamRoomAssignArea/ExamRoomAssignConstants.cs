﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomAssignArea
{
    public class ExamRoomAssignConstants
    {
        public const string LIST_EXAM_ROOM = "list_exam_room";
        public const string LIST_EXAM_PUPIL = "list_exam_pupil";
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string RADIO_ASSIGN_TYPE = "radio_assign_type";
        public const string RADIO_ASSIGN_SCALE = "radio_assign_scale";
        public const string DEFAULT_EXAM_ROOM = "default_exam_room";
        public const string CONTENT = "content";
        public const string TOTAL = "total";
        public const string PAGE = "page";
        public const int PageSize = 20;

        public const string PER_CHECK_ASSIGN = "per_check_assign";
        public const string PER_CHECK_DELETE = "per_check_delete";
        //Assign type
        //Xep theo so phong hien co
        public const int ASSIGN_TYPE_BY_REMAIN_ROOM = 1;
        //Xep theo so thi sinh/phong
        public const int ASSIGN_TYPE_BY_EXAM_PUPIL_PER_ROOM = 2;
        //Xep theo so phong thi
        public const int ASSIGN_TYPE_BY_ROOM_NUMBER = 3;
        //chia deu thi sinh cac khoi theo phong thi
        public const int ASSIGN_TYPE_BY_EDUCATION_PER_ROOM = 4;

        //Assign scale
        public const int ASSIGN_SCALE_CURRENT = 1;
        public const int ASSIGN_SCALE_ALL = 2;

        //Constant for ExamRoomAdjustController
        public const string CBO_LEFT_ROOM = "cbo_left_room";
        public const string CBO_RIGHT_ROOM = "cbo_right_room";
        public const string LIST_EXAMPUPIL_LEFT = "list_exampupil_left";
        public const string LIST_EXAMPUPIL_RIGHT = "list_exampupil_right";
        
    }
}