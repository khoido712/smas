﻿using SMAS.Web.Constants;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class ExamPupilViewModel
    {
        public long ExamPupilID { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_ExamineeNumber")]
        public string ExamineeNumber { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_PupilName")]
        public string PupilName { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_BirthDay")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDay { get; set; }

        public int? Genre { get; set; }

        public int ClassID { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_ClassDisplayName")]
        public string ClassDisplayName { get; set; }

        [ResourceDisplayName("ExamRoomAssign_Grid_Label_Genre")]
        public string DisplayGenre
        {
            get
            {
                return Genre.GetValueOrDefault() == GlobalConstants.GENRE_MALE ? GlobalConstants.GENRE_MALE_TITLE :
                                (this.Genre.GetValueOrDefault() == GlobalConstants.GENRE_FEMALE ? GlobalConstants.GENRE_FEMALE_TITLE :
                                (this.Genre.GetValueOrDefault() == 2 ? Res.Get("Common_Label_GenreUnidentified") : String.Empty));
            }
        }
    }
}