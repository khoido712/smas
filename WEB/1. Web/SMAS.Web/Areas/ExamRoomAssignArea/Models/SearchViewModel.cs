﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamPupil_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamRoomAssignConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamPupil_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamRoomAssignConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        public long? ExamRoomID { get; set; }
    }
}