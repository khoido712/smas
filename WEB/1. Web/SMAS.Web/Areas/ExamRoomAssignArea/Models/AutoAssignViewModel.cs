﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class AutoAssignViewModel
    {
        /// <summary>
        /// Loai xep phong
        /// </summary>
        public int AssignType { get; set; }

        /// <summary>
        /// So luong thi sinh neu sap xep xep theo so luong thi sinh/phong thi
        /// </summary>
        [Range(1, 999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamRoomAssign_Validate_PosInteger_ExamNumber")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamRoomAssign_Label_ExamPupilNum")]
        public string ExamPupilNumber { get; set; }

        /// <summary>
        /// So luong phong thi neu Chia theo so luong phong thi
        /// </summary>
        [Range(1, 999, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamRoomAssign_Validate_PosInteger_RoomNumber")]
        [RegularExpression(@"[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ResourceDisplayName("ExamRoomAssign_Label_RoomNum")]
        public string RoomNumber { get; set; }

        /// <summary>
        /// Xep cho nhom thi hien tai hoac tat ca
        /// </summary>
        public int AssignScale { get; set; }

        [Required]
        public long _ExamGroupID { get; set; }

        [Required]
        public long _ExaminationsID { get; set; }
        public string[] Criteria { get; set; }
        public string CriteriaIIText { get; set; }
        public string CriteriaIIIText { get; set; }
        public string chkRetypeExamineeCode { get; set; }
    }
}