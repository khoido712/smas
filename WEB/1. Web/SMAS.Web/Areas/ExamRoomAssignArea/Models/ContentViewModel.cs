﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class ContentViewModel
    {
        public List<ExamRoomViewModel> listExamRoom { get; set; }
        public List<ExamPupilViewModel> listExamPupil { get; set; }
    }
}