﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class AdjustContentViewModel
    {
        public List<ExamPupilViewModel> leftList { get; set; }

        public List<ExamPupilViewModel> rightList { get; set; }
    }
}