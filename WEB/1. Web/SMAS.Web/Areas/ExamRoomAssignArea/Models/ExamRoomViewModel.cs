﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomAssignArea.Models
{
    public class ExamRoomViewModel
    {
        public long ExamRoomID { get; set; }
        public string ExamRoomCode { get; set; }
        public int SeatNumber { get; set; }
        public int SettedNumber { get; set; }
        private string displayString;
        public int availableSeatNumber
        {
            get
            {
                return SeatNumber - SettedNumber;
            }
        }

        [ResourceDisplayName("ExamRoomAssign_Label_ExamRooms")]
        public string DisplayString {
            get
            {
                if (String.IsNullOrEmpty(this.displayString))
                    return ExamRoomCode + "(" + SettedNumber.ToString() + "/" + SeatNumber.ToString() + ")";
                else
                {
                    return displayString;
                }
            }
            set
            {
                displayString = value;
            }
        }
    }
}