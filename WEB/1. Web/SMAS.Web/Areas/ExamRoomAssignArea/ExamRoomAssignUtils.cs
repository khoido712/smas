﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Areas.ExamRoomAssignArea.Models;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.ExamRoomAssignArea
{
    public class ExamRoomAssignUtils
    {
        public void DividedEqually(List<ExamPupil> listExamPupilToSet, List<ExamPupil> listTotalExamPupil, List<ExamRoomViewModel> listDesRoom)
        {
            //Lay tong so cho ngoi con trong cua cac phong thi con lai
            int totalAvailableSeatNumber = listDesRoom.Sum(o => o.availableSeatNumber);

            //Lay so thi sinh can chia
            int examPupilCount = listExamPupilToSet.Count;

            //Truong hop so thi sinh phai chia >= so cho trong
            if (examPupilCount >= totalAvailableSeatNumber)
            {
                int epIndex = 0;
                for (int i = 0; i < listDesRoom.Count; i++)
                {
                    if (epIndex == examPupilCount)
                        break;
                    ExamRoomViewModel desRoom = listDesRoom[i];
                    int available = desRoom.availableSeatNumber;
                    while (available > 0)
                    {
                        listExamPupilToSet[epIndex].ExamRoomID = desRoom.ExamRoomID;
                        epIndex++;
                        available--;
                    }
                }
            }
            else
            {
                //So thi sinh can chia
                int n = listTotalExamPupil.Count;
                //Tong so cho ngoi cua cac phong con lai
                int m = listDesRoom.Sum(o => o.SeatNumber);

                //Loai cac phong da full cho ngoi
                for (int i = listDesRoom.Count - 1; i >= 0; i--)
                {
                    ExamRoomViewModel er = listDesRoom[i];
                    if (er.availableSeatNumber == 0)
                    {
                        listDesRoom.RemoveAt(i);
                        n = n - er.SeatNumber;
                        m = m - er.SeatNumber;
                    }
                }

                //Thuc hien vong lap tren cac phong thi
                int epIndex = 0;
                for (int i = 0; i < listDesRoom.Count; i++)
                {
                    if (epIndex == examPupilCount)
                        break;

                    ExamRoomViewModel er = listDesRoom[i];
                    //So cho ngoi toi da
                    int maxSeatNum =(int)Math.Ceiling((double)n * (double)er.SeatNumber / (double)m);

                    //Lay so cho ngoi con lai
                    int available = maxSeatNum - er.SettedNumber;

                    while (available > 0)
                    {
                        if (epIndex == examPupilCount)
                            break;
                        listExamPupilToSet[epIndex].ExamRoomID = er.ExamRoomID;
                        epIndex++;
                        available--;
                    }

                    n = n - maxSeatNum;
                    m = m - er.SeatNumber;
                }
            }
        }
        public void DividedEquallyRetype(List<ExamPupil> listExamPupilToSet, List<ExamRoomViewModel> listDesRoom)
        {
            //Lay so thi sinh can chia
            int examPupilCount = listExamPupilToSet.Count;
            //lay cac khoi cua toan bo thi sinh de chia theo khoi
            List<int> lstEducationLevelID = listExamPupilToSet.OrderBy(p=>p.EducationLevelID).Select(p => p.EducationLevelID).Distinct().ToList();
            int EducationLevelID = 0;
            List<ExamPupil> lstExamPupilByEducationLevel = null;
            //ExamRoomViewModel objExamRoom = null;
            int countPupilEdu = 0;
            int roundNum = 0;
            //int index = 0;
            int remain = 0;
            //int tmp = 0;
            ExamRoomViewModel objDesRoom = null;
            List<PupilInRoom> lstPupilInRoom = new List<PupilInRoom>();
            PupilInRoom objPupilInRoom = new PupilInRoom();
            for (int i = 0; i < listDesRoom.Count; i++)//tao du lieu ban dau
            {
                objDesRoom = listDesRoom[i];
                objPupilInRoom = new PupilInRoom();
                objPupilInRoom.RoomID = objDesRoom.ExamRoomID;
                objPupilInRoom.CountPupil = 0;
                lstPupilInRoom.Add(objPupilInRoom);
            }
            for (int i = 0; i < lstEducationLevelID.Count; i++)
            {
                EducationLevelID = lstEducationLevelID[i];
                lstExamPupilByEducationLevel = listExamPupilToSet.Where(p => p.EducationLevelID == EducationLevelID).ToList();
                countPupilEdu = lstExamPupilByEducationLevel.Count;
                roundNum = countPupilEdu / listDesRoom.Count;
                remain = countPupilEdu % listDesRoom.Count;
                if (countPupilEdu > listDesRoom.Count - 1)
                {
                    for (int j = 0; j < listDesRoom.Count; j++)
                    {
                        objDesRoom = listDesRoom[j];
                        if (roundNum > objDesRoom.SeatNumber)
                        {
                            roundNum = objDesRoom.SeatNumber;
                        }

                        if (objDesRoom.SeatNumber > 0 && countPupilEdu > 0)
                        {
                            objPupilInRoom = new PupilInRoom();
                            objPupilInRoom.EducationLevelID = EducationLevelID;
                            objPupilInRoom.RoomID = objDesRoom.ExamRoomID;
                            objPupilInRoom.CountPupil = roundNum;
                            lstPupilInRoom.Add(objPupilInRoom);
                            countPupilEdu = countPupilEdu - roundNum;
                            objDesRoom.SeatNumber = objDesRoom.SeatNumber - roundNum;
                        }
                    }
                }
                int countSeatNumber = 0;
                if (remain > 0)
                {
                    var tmp = lstPupilInRoom.GroupBy(p => p.RoomID).Select(p => new {roomID = p.Key,total = p.Sum(c => c.CountPupil) }).OrderBy(p=>p.total).ToList();
                    for (int j = 0; j < tmp.Count; j++)
                    {
                        if (remain > 0)
                        {
                            objPupilInRoom = lstPupilInRoom.Where(p => p.RoomID == tmp[j].roomID && p.EducationLevelID == EducationLevelID).FirstOrDefault();
                            objDesRoom = listDesRoom.Where(p => p.ExamRoomID == tmp[j].roomID).FirstOrDefault();
                            countSeatNumber = listDesRoom.Where(p => p.SeatNumber > 0).Count();
                            if (objDesRoom.SeatNumber > 0)
                            {
                                if (objPupilInRoom != null)
                                {
                                    objPupilInRoom.CountPupil += 1;
                                    objDesRoom.SeatNumber = objDesRoom.SeatNumber - 1;
                                    countPupilEdu = countPupilEdu - 1;
                                    objPupilInRoom.EducationLevelID = EducationLevelID;
                                    remain--;    
                                }
                                else
                                {
                                    objPupilInRoom = new PupilInRoom();
                                    objPupilInRoom.CountPupil += 1;
                                    objPupilInRoom.RoomID = objDesRoom.ExamRoomID;
                                    objDesRoom.SeatNumber = objDesRoom.SeatNumber - 1;
                                    countPupilEdu = countPupilEdu - 1;
                                    objPupilInRoom.EducationLevelID = EducationLevelID;
                                    lstPupilInRoom.Add(objPupilInRoom);
                                    remain--;
                                }
                                
                            }
                            else if (countSeatNumber == 0)
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                while (countPupilEdu > 0)
                {
                    var tmp = lstPupilInRoom.GroupBy(p => p.RoomID).Select(p => new {roomID = p.Key,total = p.Sum(c => c.CountPupil) }).OrderBy(p=>p.total).ToList();
                    for (int j = 0; j < tmp.Count; j++)
                    {
                        objPupilInRoom = lstPupilInRoom.Where(p => p.RoomID == tmp[j].roomID && p.EducationLevelID == EducationLevelID).FirstOrDefault();
                        objDesRoom = listDesRoom.Where(p => p.ExamRoomID == tmp[j].roomID).FirstOrDefault();
                        countSeatNumber = listDesRoom.Where(p => p.SeatNumber > 0).Count();
                        if (objDesRoom.SeatNumber > 0)
                        {
                            if (objPupilInRoom != null)
                            {
                                objPupilInRoom.CountPupil += 1;
                                objPupilInRoom.EducationLevelID = EducationLevelID;
                                objDesRoom.SeatNumber = objDesRoom.SeatNumber - 1;
                                countPupilEdu--;    
                            }
                            else
                            {
                                objPupilInRoom = new PupilInRoom();
                                objPupilInRoom.CountPupil += 1;
                                objPupilInRoom.EducationLevelID = EducationLevelID;
                                objDesRoom.SeatNumber = objDesRoom.SeatNumber - 1;
                                lstPupilInRoom.Add(objPupilInRoom);
                                countPupilEdu--;
                            }
                        }
                        else if (countSeatNumber == 0)
                        {
                            countPupilEdu = 0;
                            break;
                        }
                    }
                }
            }
            List<ExamPupil> lsttmp = null;
            lstPupilInRoom = lstPupilInRoom.Where(p => p.CountPupil > 0).ToList();
            for (int i = 0; i < lstPupilInRoom.Count; i++)
            {
                objPupilInRoom = lstPupilInRoom[i];
                lsttmp = listExamPupilToSet.Where(p => p.EducationLevelID == objPupilInRoom.EducationLevelID && !p.ExamRoomID.HasValue).Take(objPupilInRoom.CountPupil).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    lsttmp[j].ExamRoomID = objPupilInRoom.RoomID;
                }
            }
        }
        public class PupilInRoom
        {
            public string RoomName { get; set; }
            public long RoomID { get; set; }
            public int EducationLevelID { get; set; }
            public int CountPupil { get; set; }
        }
    }
}