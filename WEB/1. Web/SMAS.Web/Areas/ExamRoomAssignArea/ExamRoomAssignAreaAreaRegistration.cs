﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamRoomAssignArea
{
    public class ExamRoomAssignAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamRoomAssignArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamRoomAssignArea_default",
                "ExamRoomAssignArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
