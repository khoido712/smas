/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.EmployeeQualificationArea.Models
{
    public class EmployeeQualificationViewModel
    {

        [ScaffoldColumn(false)]
		public System.Int32 EmployeeQualificationID { get; set; }

        [HiddenInput]
		public System.Int32 EmployeeID { get; set; }

        [ResourceDisplayName("Employee_Column_FullName")]        
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        [ScaffoldColumn(false)]
		public System.Nullable<System.Int32> SchoolID { get; set; }

        [ScaffoldColumn(false)]
		public System.Nullable<System.Int32> SupervisingDeptID { get; set; }

        [ResourceDisplayName("Qualification_Control_GraduationLevel")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_GRADUATIONLEVEL)]
        public int GraduationLevelID { get; set; }
        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_GraduationLevel")]
        public string GraduationLevelName { get; set; }

        [ResourceDisplayName("Qualification_Control_Speciality")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_SPECIALITYCAT)]			
		public System.Int32 SpecialityID { get; set; }
        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_Speciality")]
        public string SpecialityName { get; set; }

        [ResourceDisplayName("Qualification_Control_QualificationType")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONTYPE)]
		public System.Nullable<System.Int32> QualificationTypeID { get; set; }
        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_QualificationType")]
        public string QualificationTypeName { get; set; }


        [ResourceDisplayName("Qualification_Control_Result")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String Result { get; set; }

        [ResourceDisplayName("Qualification_Control_QualifiedAt")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]		
		public System.String QualifiedAt { get; set; }

        [ResourceDisplayName("Qualification_Control_Fromdate")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
		public System.Nullable<System.DateTime> FromDate { get; set; }

        [ScaffoldColumn(false)]		
		public System.Nullable<System.DateTime> QualifiedDate { get; set; }

        [ScaffoldColumn(false)]		
		public System.Nullable<System.Boolean> IsPrimaryQualification { get; set; }

        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_ISREQUALIFIED)]
        public Nullable<bool> IsRequalified { get; set; }
        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        public string IsRequalifiedName { get; set; }

        [ResourceDisplayName("Qualification_Control_QualificationGrade")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONGRADE)]
        public System.Nullable<System.Int32> QualificationGradeID { get; set; }
        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_QualificationGrade")]
        public string QualificationGradeName { get; set; }

        [ResourceDisplayName("Qualification_Control_Description")]
		[StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]		
		public System.String Description { get; set; }								
	       
    }
}


