﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeQualificationArea
{
    public class EmployeeQualificationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeQualificationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeQualificationArea_default",
                "EmployeeQualificationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
