﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EmployeeQualificationArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.EmployeeQualificationArea.Controllers
{
    public class EmployeeQualificationController : BaseController
    {
        private readonly IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private readonly IQualificationGradeBusiness QualificationGradeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public EmployeeQualificationController(IEmployeeQualificationBusiness EmployeeQualificationBusiness, 
            IQualificationGradeBusiness QualificationGradeBusiness,
            IGraduationLevelBusiness GraduationLevelBusiness, 
            ISpecialityCatBusiness SpecialityCatBusiness,
            IQualificationTypeBusiness QualificationTypeBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }



        [ValidateAntiForgeryToken]
        public ActionResult Create(int EmployeeID)
        {
            // Lay du lieu combobox
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IQueryable<GraduationLevel> IQGraduationLevel = GraduationLevelBusiness.Search(SearchInfo);
            SelectList listIQGraduationLevel =
                new SelectList((IEnumerable<GraduationLevel>)IQGraduationLevel.ToList(), "GraduationLevelID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_GRADUATIONLEVEL] = listIQGraduationLevel;
            // QualificationType
            IQueryable<QualificationType> IQQualificationType = QualificationTypeBusiness.Search(SearchInfo);
            SelectList listQualificationType =
                new SelectList((IEnumerable<QualificationType>)IQQualificationType.ToList(), "QualificationTypeID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_QUALIFICATIONTYPE] = listQualificationType;
            // QualificationGrade
            IQueryable<QualificationGrade> IQQualificationGrade = QualificationGradeBusiness.All.Where(o => o.IsActive == true);
            SelectList listQualificationGrade =
                new SelectList((IEnumerable<QualificationGrade>)IQQualificationGrade.ToList(), "QualificationGradeID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_QUALIFICATIONGRADE] = listQualificationGrade;
            // SpecialityCat
            IQueryable<SpecialityCat> IQSpecialityCat = SpecialityCatBusiness.Search(SearchInfo);
            SelectList listSpecialityCat =
                new SelectList((IEnumerable<SpecialityCat>)IQSpecialityCat.ToList(), "SpecialityCatID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_SPECIALITYCAT] = listSpecialityCat;
            // Ket qua
            List<ComboObject> listCobObj = new List<ComboObject>();
            listCobObj.Add(new ComboObject(EmployeeQualificationConstants.FIRST_TRAINING_VAL, Res.Get("EmployeeQualification_Label_FirstTraining")));
            listCobObj.Add(new ComboObject(EmployeeQualificationConstants.RE_TRAINING_VAL, Res.Get("EmployeeQualification_Label_ReTraining")));
            SelectList listResult = new SelectList(listCobObj, "key", "value");
            ViewData[EmployeeQualificationConstants.LIST_ISREQUALIFIED] = listResult;
            Employee employee = EmployeeBusiness.Find(EmployeeID);
            EmployeeQualificationViewModel model = new EmployeeQualificationViewModel();
            model.EmployeeID = EmployeeID;
            model.EmployeeName = employee.FullName;            
            return View("_Create", model);
        }

        //
        // GET: /EmployeeQualification/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            //Get view data here
            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            int EmployeeID = 0;
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["EmployeeID"] = EmployeeID;
            IEnumerable<EmployeeQualificationViewModel> IQEmployeeQualification = this._Search(SearchInfo);
            ViewData[EmployeeQualificationConstants.LIST_EMPLOYEEQUALIFICATION] = IQEmployeeQualification;

            return View();
        }

        //
        // GET: /EmployeeQualification/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //

            IEnumerable<EmployeeQualificationViewModel> lst = this._Search(SearchInfo);
            ViewData[EmployeeQualificationConstants.LIST_EMPLOYEEQUALIFICATION] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            EmployeeQualification employeequalification = new EmployeeQualification();
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            employeequalification.EmployeeID = 71;

            this.EmployeeQualificationBusiness.Insert(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmployeeQualificationID)
        {
            EmployeeQualification employeequalification = this.EmployeeQualificationBusiness.Find(EmployeeQualificationID);
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            this.EmployeeQualificationBusiness.Update(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EmployeeQualificationBusiness.Delete(id);
            this.EmployeeQualificationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmployeeQualificationViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeQualification> query = this.EmployeeQualificationBusiness.SearchBySchool(global.SchoolID.Value,SearchInfo);
            IQueryable<EmployeeQualificationViewModel> lst = query.Select(o => new EmployeeQualificationViewModel
            {
                EmployeeQualificationID = o.EmployeeQualificationID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SchoolID = o.SchoolID,
                SupervisingDeptID = o.SupervisingDeptID,
                SpecialityID = o.SpecialityID,
                SpecialityName = o.SpecialityCat.Resolution,
                QualificationTypeID = o.QualificationTypeID,
                QualificationTypeName = o.QualificationType.Resolution,
                GraduationLevelID = o.TrainingLevelID,
                GraduationLevelName = o.TrainingLevel.Resolution,
                QualificationGradeID = o.QualificationGradeID,
                QualificationGradeName = o.QualificationGrade.Resolution,
                Result = o.Result,
                QualifiedAt = o.QualifiedAt,
                FromDate = o.FromDate,
                QualifiedDate = o.QualifiedDate,
                IsPrimaryQualification = o.IsPrimaryQualification,
                IsRequalified = o.IsRequalified

            });
            IEnumerable<EmployeeQualificationViewModel> listRes = lst.ToList();
            foreach (EmployeeQualificationViewModel item in listRes)
            {
                item.IsRequalifiedName = (item.IsRequalified.HasValue) ?
                    (item.IsRequalified.Value ?
                    Res.Get("EmployeeQualification_Label_ReTraining") : Res.Get("EmployeeQualification_Label_FirstTraining"))
                    : string.Empty;
            }
            return listRes;
        }
    }
}





