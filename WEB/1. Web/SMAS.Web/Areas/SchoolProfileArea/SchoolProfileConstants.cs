﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolProfileArea
{
    public class SchoolProfileConstants
    {
        public const string LS_SchoolProfile = "ListSchoolProfile";
        public const string LS_Area = "ListArea";
        public const string NBR_Area = "NombreArea";
        public const string LS_Province = "ListProvince";
        public const string NBR_Province = "NombreProvince";
        public const string LS_Province_Create = "ListProvince_Create";
        public const string NBR_Province_Create = "NombreProvince_Create";
        public const string LS_District = "ListDistrict";
        public const string LS_District_Create = "ListDistrict_Create";
        public const string NBR_District = "NombreDistrict";
        public const string NBR_District_Create = "NombreDistrict_Create";
        public const string LS_Commune = "ListCommune";
        public const string NBR_Commune = "NombreCommune";
        public const string LS_SupervisingDept = "ListSupervisingDept";
        public const string NBR_SupervisingDept = "NombreSupervisingDept";
        public const string LS_TrainingType = "ListTrainingType";
        public const string SCHOOL_MANAGER = "SchoolManager";
        public const string NBR_TrainingType = "NombreTrainingType";
        public const string NBR_Grade = "NombreGrade";
        public const string LS_SchoolType = "ListSchoolType";
        public const string LS_SchoolPropertyCat = "ListSchoolPropertyCat";
        public const string LS_SchoolSubsidiary = "ListSchoolSubsidiary";
        public const string SchoolID = "SchoolProfileID";
        public const string LS_EDUCATIONGRADE = "ListEducationGrade";
        public const string ADMIN_SCHOOL = "AdminSchool";
        public const string LIST_GRADE = "LIST_GRADE";
        public const string INDEX_DISTRICT = "INDEX_DISTRICT";

        public const string SEARCH_EDUCATIONGRADE = "SEARCH_EDUCATIONGRADE";
        public const string SEARCH_PROVINCE = "SEARCH_PROVINCE";
        public const string SEARCH_DISTRICT = "SEARCH_DISTRICT";
        public const string SEARCH_SCHOOLNAME = "SEARCH_SCHOOLNAME";
        public const string SEARCH_SCHOOLCODE = "SEARCH_SCHOOLCODE";
        public const string SEARCH_ADMINISTRATOR = "SEARCH_ADMINISTRATOR";
        public const string PERMISSION_MENU = "PERMISSION_MENU";
        public const string SEARCH_ORDERBY = "_SEARCH_ORDERBY_";
        public const string SEARCH_PAGE = "_SEARCH_PAGE_";

        public const int SCHOOL_PROPERTY_CATEGORY_IN_PARTICULARLY_DIFFICULT_AREA = 1;
        public const string LST_DIFFICULT_AREA = "list particularly difficult area";
    }
}