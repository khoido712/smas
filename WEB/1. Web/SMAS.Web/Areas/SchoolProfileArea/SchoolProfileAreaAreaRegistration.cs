﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolProfileArea
{
    public class SchoolProfileAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolProfileArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolProfileArea_default",
                "SchoolProfileArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
