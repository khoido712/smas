﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class SchoolSubsidiaryViewModel
    {
        [Display(Name = "SchoolSubsidiary_Label_SchoolSubsidiaryID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SchoolSubsidiaryID { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_SubsidiaryCode")]
        [StringLength(120, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string SubsidiaryCode { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_SubsidiaryName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string SubsidiaryName { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_Address")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Address { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_Telephone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Telephone { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_Fax")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Fax { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_DistanceFromHQ")]
        public Nullable<decimal> DistanceFromHQ { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_DistanceFromHQ")]
        public string DistanceFromHQName
        {
            get 
            { 
                string str = string.Empty;
                if (DistanceFromHQ.HasValue)
                {
                    str = DistanceFromHQ.Value.ToString("#.#").Replace(".", ",");
                }
                return str;
            }
        }

        [Display(Name = "SchoolSubsidiary_Label_SquareArea")]
        public Nullable<decimal> SquareArea { get; set; }

        [Display(Name = "SchoolSubsidiary_Label_SquareArea")]
        public string SquareAreaName 
        {
            get
            {
                string str = string.Empty;
                if (SquareArea.HasValue)
                {
                    str = SquareArea.Value.ToString("#.#").Replace(".", ",");
                }
                return str;
            }
        }

        [Display(Name = "SchoolSubsidiary_Label_CreatedDate")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [Display(Name = "Label_Transport")]
        public string CommutedBy { get; set; }

    }
}