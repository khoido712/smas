﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class ExcelViewModel
    {
        public string SchoolID {get;set;}
        public string SchoolCode { get; set; }
        public DateTime EstablishedDate { get; set; }
        public string TrainingType { get; set; }
        public string HeadMasterName { get; set; }
        public string SupervisingDeptName { get; set; }
        public string UserName { get; set; }
        


    }
}