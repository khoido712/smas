﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class RowExcelViewModel
    {
        public int RowId { get; set; }
        public string RowName { get; set; }
    }
}