﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Security;
using SMAS.Models.CustomAttribute;


namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class SchoolProfileForm
    {
        public int SchoolProfileID { get; set; }
        public bool IsAdminSchool { get; set; }
        public bool IsTeacher { get; set; }

        [ResourceDisplayName("Label_Standard_Code")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(20, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtSyncCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Area")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]

        public int cboArea { get; set; }
        public string Administrator { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int EducationGradeSchool { get; set; }

        public string AreaName { get; set; }

        public string tooltip { get; set; }


        [ResourceDisplayName("SchoolProfile_Label_SchoolTypeID")]
        public int SchoolTypeID { get; set; }
        public string SchoolTypeName { get; set; }


        [ResourceDisplayName("SchoolProfile_Label_Province")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboProvince { get; set; }
        public string ProvinceName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_District")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboDistrict { get; set; }
        public string DistrictName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Commune")]
        public int? cboCommune { get; set; }
        [ResourceDisplayName("SchoolProfile_Label_Commune")]
        public string CommuneName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtSchoolName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtSchoolCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EducationGrade")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Nullable<int> EducationGrade { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EstablishedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
       // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //[RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? dtpEstablishedDate { get; set; }

        // [ResDisplayName("SchoolProfile_Label_EstablishedDate")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        //public DateTime? dtpEstablishedDateCreate { get; set; }
        
        public string EstablishedDate { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SupervisingDeptID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboSupervisingDept { get; set; }
        public string SupervisingDeptName { get; set; }


        [ResourceDisplayName("SchoolProfile_Label_Resolution")]
        public string Resolution { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(60, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtHeadMasterName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterPhone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string txtHeadMasterPhone { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_UserName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //[RegularExpression(@"^([0-9]|[a-z]|[A-Z]_)*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_CharSpecial")]
        [RegularExpression(@"^\w*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_CharSpecial")]
        //[RegularExpression(@"^([A-Za-z0-9_])*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_CharSpecial")]
        public string txtUserName { get; set; }
        [ResourceDisplayName("Password")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ChangePass { get; set; }
        [ResourceDisplayName("ConfirmPassword")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ConfirmChangePass { get; set; }

        [ScaffoldColumn(false)]
        public bool IsLoggedIn { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Password")]
        public string Password { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_IsActive")]
        public bool chkIsActive { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_TrainingType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboTrainingType { get; set; }
        public string TrainingTypeName { get; set; }

        [ResourceDisplayName("Trường quản lý")]
        public int? cboSchoolManager { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolPropertyCatID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public List<int> rptPropertyOfSchool { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolTypeID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int[] rptSchoolType { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Image")]
        public byte[] Image { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string txtTelephone { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Fax")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string txtFax { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Email")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtEmail { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Adress")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtAdress { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Website")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string txtWebsite { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_ReportTile")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ReportTile { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HeadMasterPhone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string HeadMasterPhone { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_HasSubsidiary")]
        public bool HasSubsidiary { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_EstablishedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_ModifiedDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? ModifiedDate { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_AdminID")]
        public int? AdminID { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_AutoCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public bool chkAutoCode { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_ShortName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string txtShortName { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Primary")]
        public bool chkPrimary { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Secondary")]
        public bool chkSecondary { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Tertiary")]
        public bool chkTertiary { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Childcare")]
        public bool chkChildcare { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Children")]
        public bool chkChildren { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_ResetPassword")]
        public bool chkResetPassword { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_IsApproved")]
        public bool chkIsApproved { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_RemoveImage")]
        public bool chkRemoveImage { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Active")]
        public bool chkIsActiveSMAS { get; set; }

        public bool? IsIndependent { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SMSActiveType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboSMSActiveType { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SMSParentActiveType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int cboSMSParentActiveType { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_PropertyOfSchoolID")]
        public int[] PropertyOfSchoolID { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolYearTitle")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string SchoolYearTitle { get; set; }


        /// <summary>
        /// ///////////////////////
        /// </summary>
        [ResourceDisplayName("SchoolSubsidiary_Label_SubsidiaryName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string SubsidiaryName { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SubsidiaryCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string SubsidiaryCode { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SquareArea")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string SquareArea { get; set; }


        [ResourceDisplayName("SchoolSubsidiary_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SchoolID { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SchoolSubsidiaryID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SchoolSubsidiaryID { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_DistanceFromHQ")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1,2})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public string DistanceFromHQ { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_Address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string Address { get; set; }

        [ResourceDisplayName("School_Profile_NewModel")]
        public bool IsNewModel { get; set; }

        public int InDiffZone { get; set; }

        public string CommutedBy { get; set; }

    }
}
