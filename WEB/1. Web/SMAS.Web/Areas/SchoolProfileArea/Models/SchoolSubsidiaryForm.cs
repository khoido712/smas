﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;


namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class SchoolSubsidiaryForm
    {
        // SchoolSubidiary
        [ResourceDisplayName("SchoolSubsidiary_Label_SubsidiaryName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string SubsidiaryName { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SubsidiaryCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string SubsidiaryCode { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SquareArea")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public decimal SquareArea { get; set; }


        [ResourceDisplayName("SchoolSubsidiary_Label_SchoolID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SchoolID { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_SchoolSubsidiaryID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int SchoolSubsidiaryID { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_DistanceFromHQ")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public decimal DistanceFromHQ { get; set; }

        [ResourceDisplayName("SchoolSubsidiary_Label_Address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string Address { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_Telephone")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string Telephone { get; set; }

        [ResourceDisplayName("Label_Transport")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string CommutedBy { get; set; }


    }
}