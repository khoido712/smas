﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.Utils;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class SchoolInfoViewedBySupervisingDeptViewModel
    {
        /// <summary>
        /// SchoolProfileBusiness dung de lay cac thong tin lien quan den truong dang xet.
        /// Phai gan gia tri cho SchoolProfileBusiness truoc khi su dung
        /// </summary>
        public ISchoolProfileBusiness SchoolProfileBusiness;
        
        public SchoolInfoViewedBySupervisingDeptViewModel()
        {
            this.SchoolProfileBusiness = DependencyResolver.Current.GetService<ISchoolProfileBusiness>();
        }

        [HiddenInput(DisplayValue = false)]
        public int SchoolID { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolProfileName")]
        public string SchoolName 
        {
            get
            {
                SchoolProfile schoolProfile = this.getSchoolProfile();
                return schoolProfile.SchoolName;
            }
        }

        [HiddenInput(DisplayValue = false)]
        public int? AcademicYearID { get; set; }

        [HiddenInput(DisplayValue= false)]
        public int? AppliedLevelID { get; set; }

        public int? tab { get; set; }

        ////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Lay schoolprofile tu database
        /// </summary>
        /// <returns></returns>
        public SchoolProfile getSchoolProfile()
        {
            if (this.SchoolProfileBusiness == null)
            {
                throw new Exception("SchoolProfileBusiness dung de lay cac thong tin lien quan den truong dang xet.\nPhai gan gia tri cho SchoolProfileBusiness truoc khi su dung.");
            }
            SchoolProfile schoolProfile = this.SchoolProfileBusiness.Find(this.SchoolID);
            return schoolProfile;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Lay danh sach cac nam hoc cua truong tu database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AcademicYear> getAcademicYearsOfSchool()
        {
            SchoolProfile schoolProfile = this.getSchoolProfile();
            IEnumerable<AcademicYear> academicYearsOfSchool;
            if (schoolProfile == null)
            {
                academicYearsOfSchool = new List<AcademicYear>();
            }
            else
            {
                academicYearsOfSchool = schoolProfile.AcademicYears.Where(p=>p.IsActive.HasValue && p.IsActive.Value).OrderByDescending(ay => ay.Year);
            }
            return academicYearsOfSchool;
        }
        /// <summary>
        /// Lay danh sach cac nam hoc cua truong tu database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> getAcademicYearItemsOfSchool()
        {
            IEnumerable<AcademicYear> academicYearsOfSchool = getAcademicYearsOfSchool();
            // Lay cac thong tin co ban cua nam hoc
            IEnumerable<SelectListItem> academicYearsOfSchoolSummary = academicYearsOfSchool
                .Select(ay => new SelectListItem()
                {
                    Value = ay.AcademicYearID.ToString(),
                    Text = ay.Year + "-" + (ay.Year + 1),
                    Selected = ay.AcademicYearID == this.AcademicYearID
                });
            return academicYearsOfSchoolSummary;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Lay danh sach cac cap hoc cua truong tu database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AppliedLevel> getAppliedLevelsOfSchool()
        {
            SchoolProfile schoolProfile = this.getSchoolProfile();
            List<int> listAppliedLevelID = new List<int>(5);
            if ((schoolProfile.EducationGrade & 1) == 1) listAppliedLevelID.Add(1);
            if ((schoolProfile.EducationGrade & 2) == 2) listAppliedLevelID.Add(2);
            if ((schoolProfile.EducationGrade & 4) == 4) listAppliedLevelID.Add(3);
            if ((schoolProfile.EducationGrade & 8) == 8) listAppliedLevelID.Add(4);
            if ((schoolProfile.EducationGrade & 16) == 16) listAppliedLevelID.Add(5);
            // Danh sach cac cap hoc cua truong duoc chon
            List<AppliedLevel> levelsOfSelectedSchool = (new ListAppliedLevel()).GetListAppliedLevel(listAppliedLevelID);
            return levelsOfSelectedSchool;
        }

        /// <summary>
        /// Lay danh sach cac cap hoc cua truong tu database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> getAppliedLevelItemsOfSchool()
        {
            var appliedLevelsOfSchool = getAppliedLevelsOfSchool();
            // Lay cac thong tin co ban cua nam hoc
            IEnumerable<SelectListItem> appliedLevelItemsOfSchool = appliedLevelsOfSchool
                .Select(ay => new SelectListItem()
                {
                    Value = ay.AppliedLevelID.ToString(),
                    Text = ay.AppliedLevelName,
                    Selected = ay.AppliedLevelID == this.AppliedLevelID
                });
            return appliedLevelItemsOfSchool;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}