﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolProfileArea.Models
{
    public class ColumnDescriptionViewModel
    {
        public int ColumnDescriptionId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
        public string Resolution { get; set; }
        public int ColumnOrder { get; set; }
        public int ColumnIndex { get; set; }
        public bool RequiredColumn { get; set; }
        public bool CheckValue { get; set; }
    }

    public class SchoolColumnTemplateViewModel
    {
        public int ColDesId { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
    }
}