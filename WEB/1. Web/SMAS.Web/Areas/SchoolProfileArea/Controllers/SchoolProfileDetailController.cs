﻿/**
* The Viettel License
*
* Copyright 2012 Viettel Telecom. All rights reserved.
* VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.SchoolProfileArea.Models;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.DAL.Repository;
using System.Threading;
using System.Globalization;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.Web.Filter;


namespace SMAS.Web.Areas.SchoolProfileArea.Controllers
{
    public class SchoolProfileDetailController : BaseController
    {

        private const string SSK_SEARCH_TERM_PAGE = "_PAGE_";
        private const string SSK_SEARCH_TERM_ORDERBY = "_ORDERBY_";

        #region private member variable
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly ISchoolTypeBusiness SchoolTypeBusiness;
        private readonly ISchoolPropertyCatBusiness SchoolPropertyCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IPropertyOfSchoolBusiness PropertyOfSchoolBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        private readonly IUserGroupBusiness UserGroupBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly Iaspnet_MembershipBusiness aspnet_MembershipBusiness;
        private const int COUNT_PAGE = 1; //Biến phân trang
        private readonly IEmployeeBusiness EmployeeBusiness;

        #endregion

        #region Constructors
        public SchoolProfileDetailController(IGroupCatBusiness GroupCatBusiness, IGroupMenuBusiness GroupMenuBusiness,
            IUserGroupBusiness UserGroupBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            ISchoolProfileBusiness schoolprofileBusiness, IAreaBusiness areabusiness, IProvinceBusiness provincebusiness,
            IDistrictBusiness districtbusiness, ICommuneBusiness communeBusiness, ISupervisingDeptBusiness supervisingDeptBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, ISchoolTypeBusiness schoolTypeBusiness,
            ISchoolPropertyCatBusiness schoolPropertyCatBusiness, ISchoolSubsidiaryBusiness schoolSubsidiaryBusiness,
            IPropertyOfSchoolBusiness propertyOfSchoolBusiness, IUserAccountBusiness userAccountBusiness, Iaspnet_UsersBusiness Aspnet_UsersBusiness
            , Iaspnet_MembershipBusiness aspnet_MembershipBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeBusiness = EmployeeBusiness;
            this.aspnet_MembershipBusiness = aspnet_MembershipBusiness;
            this.GroupCatBusiness = GroupCatBusiness;
            this.UserGroupBusiness = UserGroupBusiness;
            this.GroupMenuBusiness = GroupMenuBusiness;
            this.SchoolProfileBusiness = schoolprofileBusiness;
            this.AreaBusiness = areabusiness;
            this.ProvinceBusiness = provincebusiness;
            this.DistrictBusiness = districtbusiness;
            this.CommuneBusiness = communeBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SchoolTypeBusiness = schoolTypeBusiness;
            this.SchoolPropertyCatBusiness = schoolPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = schoolSubsidiaryBusiness;
            this.PropertyOfSchoolBusiness = propertyOfSchoolBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.aspnet_UsersBusiness = Aspnet_UsersBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;


        }
        #endregion


        public ActionResult Index(SchoolInfoViewedBySupervisingDeptViewModel SchoolInfo)
        {
            return View(SchoolInfo);
        }
        public ActionResult _index(SchoolInfoViewedBySupervisingDeptViewModel SchoolInfo)
        {
            // Lay cac thong tin cua truong duoc chon
            SchoolProfile selectedSchool = SchoolInfo.getSchoolProfile();
            if (selectedSchool == null)
            {
                throw new BusinessException("Truong khong ton tai");
            }
            // Danh sach cac nam hoc
            IEnumerable<AcademicYear> academicYearsOfSelectedSchool = SchoolInfo.getAcademicYearsOfSchool();
            if (academicYearsOfSelectedSchool == null || academicYearsOfSelectedSchool.Count() == 0)
            {
                //throw new BusinessException("Truong chua khai bao du lieu");
                // Tra lai trang voi thong bao loi
                return PartialView(SchoolInfo);
            }
            // Chon nam hoc. Nam hoc duoc chon la nam gan nhat co ngay bat dau truoc ngay hien tai.
            if (!SchoolInfo.AcademicYearID.HasValue)
            {
                AcademicYear selectedAcademicYear = null;
                if (DateTime.Now < academicYearsOfSelectedSchool.Last().FirstSemesterStartDate)
                {
                    selectedAcademicYear = academicYearsOfSelectedSchool.Last();
                }
                else
                {
                    foreach (var ay in academicYearsOfSelectedSchool.Reverse())
                    {
                        if (ay.FirstSemesterStartDate <= DateTime.Now)
                        {
                            selectedAcademicYear = ay;
                        }
                    }
                }
                SchoolInfo.AcademicYearID = selectedAcademicYear.AcademicYearID;
            }

            // Danh sach cac cap hoc cua truong duoc chon
            IEnumerable<AppliedLevel> levelsOfSelectedSchool = SchoolInfo.getAppliedLevelsOfSchool();
            if (levelsOfSelectedSchool == null || levelsOfSelectedSchool.Count() == 0)
            {
                //throw new BusinessException("Truong chua khai bao du lieu");
                return PartialView(SchoolInfo);
            }
            // Chon cap hoc
            if (!SchoolInfo.AppliedLevelID.HasValue)
            {
                AppliedLevel selectedLevel = levelsOfSelectedSchool.Last();
                SchoolInfo.AppliedLevelID = selectedLevel.AppliedLevelID;
            }
            //////////////////////////////////////////////////////////////////////////////////////////
            // Tao doi tuong de luu cac thong tin co ban vao sesstion
            SchoolProfileDetailViewedBySupervisingDept schoolInfoForSavingToSession = new SchoolProfileDetailViewedBySupervisingDept()
            {
                SchoolID = SchoolInfo.SchoolID,
                AcademicYearID = SchoolInfo.AcademicYearID.Value,
                AppliedLevel = SchoolInfo.AppliedLevelID.Value
            };
            SchoolProfileDetailViewedBySupervisingDept.setGlobalInstanceToSession(schoolInfoForSavingToSession); // Luu thong tin vao session
            //////////////////////////////////////////////////////////////////////////////////////////
            return PartialView(SchoolInfo);
        }
        public ActionResult _pupil()
        {
            return PartialView();
        }
        public ActionResult _mark()
        {
            return PartialView();
        }

    }
}
