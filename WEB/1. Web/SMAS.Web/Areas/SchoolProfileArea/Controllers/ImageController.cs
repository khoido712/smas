﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;

namespace SMAS.Web.Areas.SchoolProfileArea.Controllers
{
    public class ImageController : Controller
    {
        //
        // GET: /EmployeeArea/Image/

        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public ImageController(ISchoolProfileBusiness schoolProfileBusiness)
        {
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }


        public ActionResult Index()
        {
            return View();
        }

        #region show image from byte[](database) to image tag in view
        [SkipCheckRole]
        public ActionResult Show(int id)
        {
            string defaultImagePath = Server.MapPath("~/Content/images/question.png");
            SchoolProfile ep = SchoolProfileBusiness.Find(id);
            byte[] imageData;
            if (ep != null)
            {
                if (ep.Image != null)
                {
                    imageData = ep.Image;
                    return File(imageData, "image/jpg");
                }
                else
                {
                    imageData = FileToByteArray(defaultImagePath);
                    return File(imageData, "image/jpg");
                }
            }

            //neu ko co tra ve anh default
            imageData = FileToByteArray(defaultImagePath);
            return File(imageData, "image/jpg");

        }




        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }


        #endregion

    }
}
