﻿/**
* The Viettel License
*
* Copyright 2012 Viettel Telecom. All rights reserved.
* VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Web.Areas.SchoolProfileArea.Models;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.DAL.Repository;
using System.Threading;
using System.Globalization;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.VTUtils.Log;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common.Util;
//using System.Net.Http;
using System.Threading.Tasks;

namespace SMAS.Web.Areas.SchoolProfileArea.Controllers
{
    public class SchoolProfileController : BaseController
    {
        private const string SSK_SEARCH_TERM_PAGE = "_PAGE_";
        private const string SSK_SEARCH_TERM_ORDERBY = "_ORDERBY_";
        public const int MinLengthPassword = 8;
        public const int MaxLengthPassword = 128;
        private string TEMPLATE_SCHOOL_INFO = "DanhSachThongTinTruong.xls";

        #region private member variable
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAreaBusiness AreaBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly ISchoolTypeBusiness SchoolTypeBusiness;
        private readonly ISchoolPropertyCatBusiness SchoolPropertyCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IPropertyOfSchoolBusiness PropertyOfSchoolBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IGroupMenuBusiness GroupMenuBusiness;
        private readonly IUserGroupBusiness UserGroupBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly Iaspnet_MembershipBusiness aspnet_MembershipBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        private const int COUNT_PAGE = 1; //Biến phân trang
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IProfileTemplateBusiness ProfileTemplateBusiness;
        private readonly IColumnDescriptionBusiness ColumnDescriptionBusiness;
        #endregion

        #region Constructors
        public SchoolProfileController(IGroupCatBusiness GroupCatBusiness, IGroupMenuBusiness GroupMenuBusiness,
            IUserGroupBusiness UserGroupBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            ISchoolProfileBusiness schoolprofileBusiness, IAreaBusiness areabusiness, IProvinceBusiness provincebusiness,
            IDistrictBusiness districtbusiness, ICommuneBusiness communeBusiness, ISupervisingDeptBusiness supervisingDeptBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, ISchoolTypeBusiness schoolTypeBusiness,
            ISchoolPropertyCatBusiness schoolPropertyCatBusiness, ISchoolSubsidiaryBusiness schoolSubsidiaryBusiness,
            IPropertyOfSchoolBusiness propertyOfSchoolBusiness, IUserAccountBusiness userAccountBusiness, Iaspnet_UsersBusiness Aspnet_UsersBusiness
            , Iaspnet_MembershipBusiness aspnet_MembershipBusiness
            , IEmployeeBusiness EmployeeBusiness,
            IRestoreDataBusiness _RestoreDataBusiness,
            IRestoreDataDetailBusiness _RestoreDataDetailBusiness,
            IProfileTemplateBusiness ProfileTemplateBusiness,
            IColumnDescriptionBusiness ColumnDescriptionBusiness
            )
        {
            this.EmployeeBusiness = EmployeeBusiness;
            this.aspnet_MembershipBusiness = aspnet_MembershipBusiness;
            this.GroupCatBusiness = GroupCatBusiness;
            this.UserGroupBusiness = UserGroupBusiness;
            this.GroupMenuBusiness = GroupMenuBusiness;
            this.SchoolProfileBusiness = schoolprofileBusiness;
            this.AreaBusiness = areabusiness;
            this.ProvinceBusiness = provincebusiness;
            this.DistrictBusiness = districtbusiness;
            this.CommuneBusiness = communeBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SchoolTypeBusiness = schoolTypeBusiness;
            this.SchoolPropertyCatBusiness = schoolPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = schoolSubsidiaryBusiness;
            this.PropertyOfSchoolBusiness = propertyOfSchoolBusiness;
            this.UserAccountBusiness = userAccountBusiness;
            this.aspnet_UsersBusiness = Aspnet_UsersBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.RestoreDataBusiness = _RestoreDataBusiness;
            this.RestoreDataDetailBusiness = _RestoreDataDetailBusiness;
            this.ProfileTemplateBusiness = ProfileTemplateBusiness;
            this.ColumnDescriptionBusiness = ColumnDescriptionBusiness;

        }
        #endregion

        // Dữ liệu mẫu cho table SCHOOL_PROFILE
        private void InsertColumnDes()
        {
            List<ColumnDescriptionBO> lstInsert = new List<ColumnDescriptionBO>();

            for (int i = 1; i <= 43; i++)
            {
                ColumnDescriptionBO objInsert = new ColumnDescriptionBO();
                objInsert.ColumnIndex = i;
                objInsert.ColumnOrder = i;

                switch (i)
                {
                    case 1:
                        objInsert.ColumnName = "STT";
                        objInsert.ColumnType = "Number";
                        objInsert.FieldName = "STT";
                        objInsert.Resolution = "STT";                       
                        break;
                    case 2:
                        objInsert.ColumnName = "SYNC_CODE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "SyncCode";
                        objInsert.Resolution = "Mã chuẩn";
                        break;
                    case 3:
                        objInsert.ColumnName = "AREA";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Area";
                        objInsert.Resolution = "Khu vực";
                        break;
                     case 4:
                        objInsert.ColumnName = "PROVINCE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Province";
                        objInsert.Resolution = "Tỉnh/Thành phố";
                        break;
                     case 5:
                        objInsert.ColumnName = "DISTRICT";
                        objInsert.ColumnType = "String"; 
                        objInsert.FieldName = "District";
                        objInsert.Resolution = "Quận/Huyện";
                        break;
                     case 6:
                        objInsert.ColumnName = "COMMUNE";
                        objInsert.ColumnType = "String"; 
                        objInsert.FieldName = "Commune";
                        objInsert.Resolution = "Xã/Phường";
                        break;
                     case 7:
                        objInsert.ColumnName = "SCHOOL_NAME";
                        objInsert.ColumnType = "String"; 
                        objInsert.FieldName = "SchoolName";
                        objInsert.Resolution = "Tên trường";
                        break;
                      case 8:
                        objInsert.ColumnName = "SHORT_NAME";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "ShortName";
                        objInsert.Resolution = "Tên viết tắt";
                        break;
                      case 9:
                        objInsert.ColumnName = "SCHOOL_CODE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "SchoolCode";
                        objInsert.Resolution = "Mã trường";
                        break;
                    case 10:
                        objInsert.ColumnName = "SUPER_VISING_DEPT";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "SupervisingDept";
                        objInsert.Resolution = "Đơn vị quản lý";
                        break;
                    case 11:
                        objInsert.ColumnName = "EDUCATION_GRADE_SCHOOL";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "EducationGradeSchool";
                        objInsert.Resolution = "Cấp trường";
                        break;
                    case 12:
                        objInsert.ColumnName = "TRAINING_TYPE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "TrainingType";
                        objInsert.Resolution = "Loại hình đào tạo";
                        break;
                    case 13:
                        objInsert.ColumnName = "ESTABLISHED_DATE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "EstablishedDate";
                        objInsert.Resolution = "Ngày thành lập";
                        break;
                    case 14:
                        objInsert.ColumnName = "HEAD_MASTER_NAME";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "HeadMasterName";
                        objInsert.Resolution = "Hiệu trưởng";
                        break;
                    case 15:
                        objInsert.ColumnName = "HEAD_MASTER_PHONE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "HeadMasterPhone";
                        objInsert.Resolution = "SĐT Hiệu trưởng";
                        break;
                    case 16:
                        objInsert.ColumnName = "PHONE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Phone";
                        objInsert.Resolution = "Điện thoại trường";
                        break;
                    case 17:
                        objInsert.ColumnName = "EMAIL";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Email";
                        objInsert.Resolution = "Email";
                        break;
                    case 18:
                        objInsert.ColumnName = "FAX";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Fax";
                        objInsert.Resolution = "Fax";
                        break;
                    case 19:
                        objInsert.ColumnName = "WEBSITE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Website";
                        objInsert.Resolution = "Website";
                        break;
                    case 20:
                        objInsert.ColumnName = "ADDRESS";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Address";
                        objInsert.Resolution = "Địa chỉ";
                        break;
                    case 21:
                        objInsert.ColumnName = "SCHOOL_YEAR_TITLE";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "SchoolYearTitle";
                        objInsert.Resolution = "Khóa đào tạo";
                        break;
                    case 22:
                        objInsert.ColumnName = "IS_INDEPENDENT";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "IsIndependent";
                        objInsert.Resolution = "Nhóm trẻ độc lập";
                        break;

                    case 23:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_01";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool01";
                        objInsert.Resolution = "Đạt chuẩn quốc gia";
                        break;
                    case 24:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_02";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool02";
                        objInsert.Resolution = "Trường dành cho người tàn tật, khuyết tật";
                        break;
                    case 25:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_03";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool03";
                        objInsert.Resolution = "Có tổ chức dạy nghề PT";
                        break;
                    case 26:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_04";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool04";
                        objInsert.Resolution = "Có học sinh bán trú";
                        break;
                    case 27:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_05";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool05";
                        objInsert.Resolution = "Trường đạt chuẩn THTT-HSTC";
                        break;
                    case 28:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_06";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool06";
                        objInsert.Resolution = "Dạy học 2 buổi/ngày";
                        break;
                    case 29:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_07";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool07";
                        objInsert.Resolution = "Có học sinh nội trú";
                        break;
                    case 30:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_08";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool08";
                        objInsert.Resolution = "Đạt mức chất lượng tối thiểu";
                        break;
                    case 31:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_09";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool09";
                        objInsert.Resolution = "Thuộc vùng đặc biệt khó khăn";
                        break;
                    case 32:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_10";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool10";
                        objInsert.Resolution = "Có thể liên lạc bằng sóng di động";
                        break;
                    case 33:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_11";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool11";
                        objInsert.Resolution = "Có chi bộ Đảng";
                        break;
                    case 34:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_12";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool12";
                        objInsert.Resolution = "Trường quốc tế";
                        break;
                    case 35:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_13";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool13";
                        objInsert.Resolution = "Có điện thoại cố định";
                        break;
                    case 36:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_14";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool14";
                        objInsert.Resolution = "Có học sinh khuyết tật";
                        break;
                    case 37:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_15";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool15";
                        objInsert.Resolution = "Đóng trên địa bàn xã biên giới";
                        break;
                    case 38:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_16";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool16";
                        objInsert.Resolution = "Phổ biến cho CM HS về các CTGD phòng, chống HIV";
                        break;
                    case 39:
                        objInsert.ColumnName = "FEATURE_OF_SCHOOL_17";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "FeatureOfSchool17";
                        objInsert.Resolution = "Có lớp không chuyên (đối với trường chuyên)";
                        break;
                    case 40:
                        objInsert.ColumnName = "ACCOUNT_LOGIN";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "AccountLogin";
                        objInsert.Resolution = "Tên đăng nhập";
                        break;
                    case 41:
                        objInsert.ColumnName = "PASSWORD";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "Password";
                        objInsert.Resolution = "Mật khẩu";
                        break;
                    case 42:
                        objInsert.ColumnName = "ACTIVE_ACCOUNT";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "ActiveAccount";
                        objInsert.Resolution = "Kích hoạt tài khoản trị";
                        break;
                    case 43:
                        objInsert.ColumnName = "ACTIVE_SMAS";
                        objInsert.ColumnType = "String";
                        objInsert.FieldName = "ActiveSMAS";
                        objInsert.Resolution = "Kích hoạt cấp trường";
                        break;
                }
                lstInsert.Add(objInsert);
            }

            foreach (var item in lstInsert) {

                ColumnDescription input = new ColumnDescription();
                input.TableName = "SCHOOL_PROFILE";
                input.ColumnSize = 20;
                input.RequiredColumn = false;
                input.CreatedDate = DateTime.Now;
                input.Description = null;
                input.ModifiedDate = null;
                input.ReportType = 2;
                input.ColumnIndex = item.ColumnIndex;
                input.ColumnOrder = item.ColumnOrder;
                input.ColumnName = item.ColumnName;
                input.ColumnType = item.ColumnType;
                input.FieldName = item.FieldName;
                input.Resolution = item.Resolution;
                ColumnDescriptionBusiness.Insert(input);
            }

            ColumnDescriptionBusiness.Save();
        }

        public ViewResult Index(int? LoadSS)
        {           
            GlobalInfo globalInfo = new GlobalInfo();
            //neu la quan tri cap cao hien thi tat ca cac cap
            if (globalInfo.IsSystemAdmin)
            {
                ViewData[SchoolProfileConstants.SEARCH_EDUCATIONGRADE] = CommonList.EducationGradeSchool();
            }
            else // neu la admin phong so lay theo cap quan ly
            {
                if (LoadSS == 1)
                {
                    ViewData[SchoolProfileConstants.SEARCH_EDUCATIONGRADE] = Session["SearchEducationGrade"];
                }
            }
            if (LoadSS == 1)
            {
                ViewData[SchoolProfileConstants.SEARCH_PROVINCE] = Session["SearchProvince"];

                ViewData[SchoolProfileConstants.SEARCH_DISTRICT] = Session["SearchDistrict"];

                ViewData[SchoolProfileConstants.SEARCH_SCHOOLNAME] = Session["SearchSchoolName"];
                ViewData[SchoolProfileConstants.SEARCH_SCHOOLCODE] = Session["SearchSchoolCode"];
                ViewData[SchoolProfileConstants.SEARCH_ADMINISTRATOR] = Session["SearchAdministrator"];

                ViewData[SchoolProfileConstants.SEARCH_PAGE] = Session[SSK_SEARCH_TERM_PAGE];
                ViewData[SchoolProfileConstants.SEARCH_ORDERBY] = Session[SSK_SEARCH_TERM_ORDERBY];
            }
            else
            {
                RemoveSS();
            }
            SetViewDataSchoolProfile();
            if (LoadSS == 1)
            {
                if (Session["SearchDistrict"] != null && Session["SearchProvince"] != null)
                {
                    int? ProvinceID = (int)Session["SearchProvince"];
                    int? DistrictID = (int)Session["SearchDistrict"];
                    if (ProvinceID != 0 && DistrictID != 0)
                    {
                        List<District> listDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == ProvinceID).OrderBy(o => o.DistrictName).ToList();
                        ViewData[SchoolProfileConstants.LS_District] = listDistrict;
                    }
                }
            }
            //Search(SearchForm);
            SetViewDataPermission("SchoolProfile", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);

            return View();
        }

        #region Tìm kiếm trường

        public PartialViewResult Search(SchoolProfileSearchForm SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            SetViewDataPermission("SchoolProfile", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo global = new GlobalInfo();
            Session["SearchEducationGrade"] = SearchForm.EducationGrade;
            Session["SearchProvince"] = SearchForm.Province;
            if (!global.IsSubSuperVisingDeptRole)
            {
                Session["SearchDistrict"] = SearchForm.District;
                Session.Remove("SupervisingDeptID");
            }
            else
            {
                Session.Remove("SearchDistrict");
                Session["SupervisingDeptID"] = global.SupervisingDeptID;
            }
            Session["SearchSchoolName"] = SearchForm.SchoolName;
            Session["SearchSchoolCode"] = SearchForm.SchoolCode;
            Session["SearchAdministrator"] = SearchForm.Administrator;
            Session[SSK_SEARCH_TERM_PAGE] = page;
            Session[SSK_SEARCH_TERM_ORDERBY] = orderBy;

            Paginate<SchoolProfileForm> paging = this._Search(SearchForm, page, orderBy);
            ViewData[SchoolProfileConstants.LS_SchoolProfile] = paging;
         
            return PartialView("_GridSchoolProfile");

        }
        #endregion

        #region SearchSub

        public PartialViewResult SearchSub(int? SchoolProfileID)
        {
            this.CheckPermissionForAction(SchoolProfileID, "schoolprofile", 4);
            this.SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = new List<SchoolSubsidiaryViewModel>();
            if (SchoolProfileID != null)
            {
                IDictionary<string, object> dic3 = new Dictionary<string, object>();
                dic3["SchoolID"] = SchoolProfileID;
                dic3["IsActive"] = true;
                List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic3).ToList();
                ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = lsSchoolSubsidiary.Select(u => new SchoolSubsidiaryViewModel
                {
                    Address = u.Address,
                    CreatedDate = u.CreatedDate,
                    DistanceFromHQ = u.DistanceFromHQ,
                    Fax = u.Fax,
                    SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                    SquareArea = u.SquareArea,
                    SubsidiaryCode = u.SubsidiaryCode,
                    SubsidiaryName = u.SubsidiaryName,
                    Telephone = u.Telephone,
                    CommutedBy = u.CommutedBy
                }).OrderBy(x=>x.SubsidiaryCode).ToList();
            }
            return PartialView("_ListSchoolSubsidiary");
        }
        #endregion

        #region index

        [ValidateAntiForgeryToken]
        public PartialViewResult GetSchoolProfile(string SchoolName, string SchoolCode, int ProvinceID, int DistrictID, string EducationGrade, string HeadMasterName)
        {
            SetViewDataSchoolProfile();

            int Education = int.Parse(EducationGrade);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolName"] = SchoolName;
            dic["SchoolCode"] = SchoolCode;
            dic["ProvinceID"] = ProvinceID;
            dic["DistrictID"] = DistrictID;
            dic["EducationGrade"] = Education;
            dic["HeadMasterName"] = HeadMasterName;
            ViewData[SchoolProfileConstants.LS_SchoolProfile] = this.SchoolProfileBusiness.Search(dic);

            return PartialView("_GridSchoolProfile");
        }

        public PartialViewResult IndexSubsidiarySchool()
        {
            SetViewDataSchoolProfile();
            return PartialView("IndexSubsidiarySchool");
        }
        #endregion

        #region Create SchoolProfile
        private void PrepareCreate()
        {
            SetViewDataSchoolProfile();
        }

        public PartialViewResult Create()
        {
            PrepareCreate();
            SchoolProfileForm spf = new SchoolProfileForm();
            spf.dtpEstablishedDate = DateTime.Today;
            List<SelectListItem> lsSMS = new List<SelectListItem>();
            lsSMS.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
            //lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
            lsSMS.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
            ViewData["SMS"] = new SelectList(lsSMS, "Value", "Text");
            return PartialView("Create", spf);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(SchoolProfileForm frm, FormCollection frc, List<int> rptPropertyOfSchool)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SchoolProfile SchoolProfile = new SchoolProfile();
            //  AnhVD 20140806 - Add in scope transaction 
            using (TransactionScope scope = new TransactionScope())
            {
                string x = frc["chkAutoCode"];
                string y = frc["chkRemoveImage"];
                int account = GlobalInfo.UserAccountID;
                int roleID = GlobalInfo.RoleID;
                //Nếu tài khoản là cấp sở
                if (GlobalInfo.IsSuperVisingDeptRole)
                {
                    frm.cboProvince = GlobalInfo.ProvinceID.Value;
                }
                //Nếu tài khoản là cấp phòng
                if (GlobalInfo.IsSubSuperVisingDeptRole)
                {
                    frm.cboProvince = GlobalInfo.ProvinceID.Value;
                    frm.cboDistrict = GlobalInfo.DistrictID.Value;
                }
                bool AutoCode = false;
                //nếu người dùng check vào mã tự động
                if (x == "1")
                {
                    AutoCode = true;
                }
                else
                {
                    SchoolProfile.SchoolCode = frm.txtSchoolCode.Trim();
                }
                //nếu người dùng check vào không chọn ảnh hiển thị
                if (y != "1")
                {
                    object imagePath = Session["ImagePath"];
                    if (imagePath != null && !string.IsNullOrEmpty(imagePath.ToString()))
                    {
                        byte[] imageBytes = FileToByteArray((string)imagePath);
                        frm.Image = imageBytes;
                        SchoolProfile.Image = frm.Image;
                    }
                }

                SchoolProfile.SyncCode = frm.txtSyncCode.Trim();
                SchoolProfile.SchoolName = frm.txtSchoolName.Trim();
                SchoolProfile.ShortName = frm.txtShortName.Trim();
                SchoolProfile.SchoolYearTitle = frm.SchoolYearTitle;
                SchoolProfile.AreaID = frm.cboArea;


                if (GlobalInfo.IsSuperVisingDeptRole)
                {
                    SchoolProfile.ProvinceID = GlobalInfo.ProvinceID.Value;
                }
                if (GlobalInfo.IsSuperRole)
                {
                    SchoolProfile.ProvinceID = frm.cboProvince;

                }
                if (GlobalInfo.IsSubSuperVisingDeptRole)
                {
                    SchoolProfile.ProvinceID = frm.cboProvince;

                }
                SchoolProfile.DistrictID = frm.cboDistrict;
                SchoolProfile.CommuneID = frm.cboCommune;
                SchoolProfile.SupervisingDeptID = frm.cboSupervisingDept;
                SchoolProfile.TrainingTypeID = frm.cboTrainingType;

                string SchoolTypeID = frc["rptSchoolType"];
                SchoolProfile.SchoolTypeID = int.Parse(SchoolTypeID);

                SchoolProfile.Telephone = frm.txtTelephone.Trim();
                SchoolProfile.Fax = frm.txtFax;
                SchoolProfile.Email = frm.txtEmail;
                SchoolProfile.Address = frm.txtAdress;
                SchoolProfile.EstablishedDate = frm.dtpEstablishedDate;
                SchoolProfile.Website = frm.txtWebsite;
                //SchoolProfile.ReportTile = "";

                SchoolProfile.EducationGrade = (int)frm.EducationGradeSchool;
                SchoolProfile.HeadMasterName = frm.txtHeadMasterName.Trim();
                SchoolProfile.HeadMasterPhone = frm.txtHeadMasterPhone;
                SchoolProfile.HasSubsidiary = true;
                SchoolProfile.CreatedDate = DateTime.Now;
                SchoolProfile.IsActive = true;
                //chiendd1: 25/11/2016: chi admin moi kich hoat
                //if (_globalInfo.IsSystemAdmin)
                //{
                SchoolProfile.SMSTeacherActiveType = 2; //frm.cboSMSActiveType;
                SchoolProfile.SMSParentActiveType = 1; //frm.cboSMSParentActiveType;
                SchoolProfile.IsActiveSMAS = frm.chkIsActiveSMAS;
                //}
                //Bổ sung các thông tin: account = UserInfo.UserAccountID, roleID = UserInfo.RoleID  
                //Map tương ứng vào đối tượng SchoolProfile, bổ sung thêm thuộc tính CreateDate = Sys (Ngày hiện tại), IsActive = 1
                SchoolProfile.CreatedDate = DateTime.Now;
                List<int> lstRole = new List<int>();
                if ((SchoolProfile.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_CHILDREN) != 0)
                {
                    lstRole.Add(SMAS.Web.Constants.GlobalConstants.ROLE_ADMIN_MAUGIAO);
                }
                if ((SchoolProfile.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_CHILDCARE) != 0)
                {
                    lstRole.Add(SMAS.Web.Constants.GlobalConstants.ROLE_ADMIN_NHATRE);
                }
                if ((SchoolProfile.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY) != 0)
                {
                    lstRole.Add(SMAS.Web.Constants.GlobalConstants.ROLE_ADMIN_TRUONGC3);
                }
                if ((SchoolProfile.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY) != 0)
                {
                    lstRole.Add(SMAS.Web.Constants.GlobalConstants.ROLE_ADMIN_TRUONGC2);
                }
                if ((SchoolProfile.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY) != 0)
                {
                    lstRole.Add(SMAS.Web.Constants.GlobalConstants.ROLE_ADMIN_TRUONGC1);
                }

                if (_globalInfo.IsSystemAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                {
                    if ((SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE
                        || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                        || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL))
                    {
                        SchoolProfile.IsIndependent = "True".Equals(frc["chkIndependentID"]) ? true : false;
                        if ("True".Equals(frc["chkIndependentID"]))
                        {
                            SchoolProfile.SchoolManagerID = frc["cboSchoolManager"] != null ? Int32.Parse(frc["cboSchoolManager"]) : 0;
                        }
                        else
                        {
                            SchoolProfile.SchoolManagerID = null;
                        }
                    }
                }

                if (SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS)
                {
                    SchoolProfile.IsNewSchoolModel = frm.IsNewModel;
                }
                else
                {
                    SchoolProfile.IsNewSchoolModel = false;
                }

                if (rptPropertyOfSchool == null)
                {
                    rptPropertyOfSchool = new List<int>();
                }

                if (rptPropertyOfSchool.Contains(SchoolProfileConstants.SCHOOL_PROPERTY_CATEGORY_IN_PARTICULARLY_DIFFICULT_AREA))
                {
                    if (frm.InDiffZone > 0) SchoolProfile.InSpecialDifficultZone = frm.InDiffZone;
                    else SchoolProfile.InSpecialDifficultZone = SystemParamsInFile.IN_PARTICULARLY_DIFFICULT_COMMUNE;
                }

                Utils.Utils.TrimObject(SchoolProfile);
                //cap mat khau cho account
                //Kiểm tra độ dài password
                if (frm.ChangePass.Length < 8)
                {
                    return Json(new JsonMessage("Mật khẩu phải lớn hơn 8 ký tự.", "error"));
                }
                else if (!frm.ChangePass.Equals(frm.ConfirmChangePass))
                {
                    return Json(new JsonMessage("Xác nhận mật khẩu mới không hợp lệ.", "error"));
                }
                else if (!SMAS.Business.Common.Utils.CheckPasswordInValid(frm.ChangePass))
                {
                    return Json(new JsonMessage("Mật khẩu mới không được nhỏ hơn 8 ký tự, phải bao gồm số, chữ thường, chữ in hoa và ký tự đặc biệt.", "error"));
                }
                else if (frm.ChangePass != frm.ConfirmChangePass)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFail"), "error"));
                }
                else if (!CheckPassRequire(frm.ChangePass))
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFormatFail"), "error"));

                }
                else if (frm.ChangePass.Length < MinLengthPassword || frm.ChangePass.Length > MaxLengthPassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_MaxlengthPassword"), "error"));
                }

                //Insert thong tin truong - bao gom Tao tai khoan Admin - AnhVD 20140807
                this.SchoolProfileBusiness.Insert(SchoolProfile, lstRole, frm.txtUserName.Trim(), frm.chkIsApproved, rptPropertyOfSchool, AutoCode, account, roleID, frm.ChangePass);
                this.SchoolProfileBusiness.Save();
                ModelState.Clear();

                UserAccount ua = UserAccountBusiness.All.FirstOrDefault(o => o.aspnet_Users.UserName.Trim().ToLower() == frm.txtUserName.Trim().ToLower() && o.IsActive == true);
                if (AutoCode)
                {
                    if (ua != null)
                    {
                        // Sinh ma truong
                        SetSchoolCode(ua.UserAccountID);
                    }
                }
                scope.Complete();

            }
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage"), "success"));
        }
        #endregion

        #region Create SchoolSubsidiary
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult CreateSubsi(SchoolSubsidiaryForm frms, FormCollection fc)
        {
            ClassProfile ab = new ClassProfile();
           
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            if (frms.SchoolSubsidiaryID == 0)
            {
                GlobalInfo GlobalInfo = new GlobalInfo();
                if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }

                SchoolSubsidiary SchoolSubsidiary = new SchoolSubsidiary();
                SchoolSubsidiary.SchoolID = int.Parse(fc["SchoolProfileID"]);
                SchoolSubsidiary.SubsidiaryCode = frms.SubsidiaryCode.Trim();
                SchoolSubsidiary.SubsidiaryName = frms.SubsidiaryName.Trim();
                SchoolSubsidiary.Address = frms.Address;
                SchoolSubsidiary.Telephone = fc["txtTelephone"] != null ? fc["txtTelephone"] : "";//frms.Telephone;
                SchoolSubsidiary.CommutedBy = frms.CommutedBy;
                SchoolSubsidiary.CreatedDate = DateTime.Now;

                if (!string.IsNullOrEmpty(fc["SquareArea"]))
                {
                    if (fc["SquareArea"].Length > 8)
                    {
                        SchoolSubsidiary.SquareArea = decimal.Parse(fc["SquareArea"].Substring(0, 8).Replace(",", "."));
                    }
                    else
                    {
                        SchoolSubsidiary.SquareArea = decimal.Parse(fc["SquareArea"].Replace(",", "."));
                    }
                }
                if (!string.IsNullOrEmpty(fc["DistanceFromHQ"]))
                {
                    if (fc["DistanceFromHQ"].Length > 8)
                    {
                        SchoolSubsidiary.DistanceFromHQ = decimal.Parse(fc["DistanceFromHQ"].Substring(0, 8).Replace(",", "."));
                    }
                    else
                    {
                        SchoolSubsidiary.DistanceFromHQ = decimal.Parse(fc["DistanceFromHQ"].Replace(",", "."));
                    }
                }
                SchoolSubsidiary.IsActive = true;
                SchoolSubsidiaryBusiness.Insert(SchoolSubsidiary);
                SchoolSubsidiaryBusiness.Save();
                IDictionary<string, object> dic3 = new Dictionary<string, object>();
                dic3["SchoolID"] = SchoolSubsidiary.SchoolID;
                dic3["IsActive"] = true;
                List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic3).ToList();
                ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = lsSchoolSubsidiary.Select(u => new SchoolSubsidiaryViewModel
                {
                    Address = u.Address,
                    CreatedDate = u.CreatedDate,
                    DistanceFromHQ = u.DistanceFromHQ,
                    Fax = u.Fax,
                    SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                    SquareArea = u.SquareArea,
                    SubsidiaryCode = u.SubsidiaryCode,
                    SubsidiaryName = u.SubsidiaryName,
                    Telephone = u.Telephone,
                    CommutedBy = u.CommutedBy
                }).OrderBy(x=>x.SubsidiaryCode).ToList();
                this.SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
                return View("_ListSchoolSubsidiary");
            }
            else
            {
                GlobalInfo GlobalInfo = new GlobalInfo();
                if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                SchoolSubsidiary SchoolSubsidiary = SchoolSubsidiaryBusiness.Find(frms.SchoolSubsidiaryID);
                //SchoolSubsidiary SchoolSubsidiary = new SchoolSubsidiary();
                SchoolSubsidiary.SchoolSubsidiaryID = frms.SchoolSubsidiaryID;
                SchoolSubsidiary.SubsidiaryName = frms.SubsidiaryName.Trim();
                SchoolSubsidiary.Address = frms.Address;
                SchoolSubsidiary.Telephone = fc["txtTelephone"] != null ? fc["txtTelephone"] : "";
                SchoolSubsidiary.CommutedBy = frms.CommutedBy;
                SchoolSubsidiary.ModifiedDate = DateTime.Now;
                if (!string.IsNullOrEmpty(fc["SquareArea"]))
                {
                    if (fc["SquareArea"].Length > 8)
                    {
                        SchoolSubsidiary.SquareArea = decimal.Parse(fc["SquareArea"].Substring(0, 8).Replace(",", "."));
                    }
                    else
                    {
                        SchoolSubsidiary.SquareArea = decimal.Parse(fc["SquareArea"].Replace(",", "."));
                    }
                }
                if (!string.IsNullOrEmpty(fc["DistanceFromHQ"]))
                {
                    if (fc["DistanceFromHQ"].Length > 8)
                    {
                        SchoolSubsidiary.DistanceFromHQ = decimal.Parse(fc["DistanceFromHQ"].Substring(0, 8).Replace(",", "."));
                    }
                    else
                    {
                        SchoolSubsidiary.DistanceFromHQ = decimal.Parse(fc["DistanceFromHQ"].Replace(",", "."));
                    }
                }
                SchoolSubsidiary.IsActive = true;
                string a = SchoolSubsidiary.SubsidiaryCode;

                SchoolSubsidiaryBusiness.Update(SchoolSubsidiary);
                SchoolSubsidiaryBusiness.Save();

                IDictionary<string, object> dic3 = new Dictionary<string, object>();
                dic3.Add("SchoolID", SchoolSubsidiary.SchoolID);
                dic3["IsActive"] = true;
                List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic3).ToList();
                ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = lsSchoolSubsidiary.Select(u => new SchoolSubsidiaryViewModel
                {
                    Address = u.Address,
                    CreatedDate = u.CreatedDate,
                    DistanceFromHQ = u.DistanceFromHQ,
                    Fax = u.Fax,
                    SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                    SquareArea = u.SquareArea,
                    SubsidiaryCode = u.SubsidiaryCode,
                    SubsidiaryName = u.SubsidiaryName,
                    Telephone = u.Telephone,
                    CommutedBy = u.CommutedBy
                }).OrderBy(x => x.SubsidiaryCode).ToList();
                this.SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
                return View("_ListSchoolSubsidiary");

            }
        }
        #endregion

        #region Edit SchoolSubsidiary
        public SchoolSubsidiaryForm PrepareEditSubsi(int id)
        {
            SchoolSubsidiary SchoolSubsidiary = SchoolSubsidiaryBusiness.Find(id);
            this.CheckPermissionForAction(SchoolSubsidiary.SchoolID, "schoolprofile", 4);
            SchoolSubsidiaryForm ssf = new SchoolSubsidiaryForm();
            ssf.Address = SchoolSubsidiary.Address;
            ssf.DistanceFromHQ = SchoolSubsidiary.DistanceFromHQ.HasValue ? decimal.Parse(SMAS.Business.Common.Utils.FormatMark(SchoolSubsidiary.DistanceFromHQ.Value)) : 0;
            ssf.SchoolID = SchoolSubsidiary.SchoolID;
            ssf.SchoolSubsidiaryID = id;
            ssf.SquareArea =SchoolSubsidiary.SquareArea.HasValue ? decimal.Parse(SMAS.Business.Common.Utils.FormatMark(SchoolSubsidiary.SquareArea.Value)) : 0;
            ssf.SubsidiaryCode = SchoolSubsidiary.SubsidiaryCode;
            ssf.SubsidiaryName = SchoolSubsidiary.SubsidiaryName;
            ssf.Telephone = SchoolSubsidiary.Telephone;
            ssf.CommutedBy = SchoolSubsidiary.CommutedBy;
            return ssf;
        }


        public ActionResult EditSubsi(int id)
        {

            SchoolSubsidiaryForm ssf = PrepareEditSubsi(id);
            this.CheckPermissionForAction(ssf.SchoolID, "schoolprofile", 4);
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            SchoolProfileForm spf = new SchoolProfileForm();
            spf.Address = ssf.Address;
            spf.DistanceFromHQ = ssf.DistanceFromHQ.ToString().Replace(".", ",");
            spf.SchoolProfileID = ssf.SchoolID;
            spf.SchoolSubsidiaryID = id;
            spf.SquareArea = ssf.SquareArea.ToString().Replace(".", ",");
            spf.SubsidiaryCode = ssf.SubsidiaryCode;
            spf.SubsidiaryName = ssf.SubsidiaryName;
            IDictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3["SchoolID"] = ssf.SchoolID;
            dic3["IsActive"] = true;
            List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic3).ToList();
            ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = lsSchoolSubsidiary.Select(u => new SchoolSubsidiaryViewModel
            {
                Address = u.Address,
                CreatedDate = u.CreatedDate,
                DistanceFromHQ = u.DistanceFromHQ,
                Fax = u.Fax,
                SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                SquareArea = u.SquareArea,
                SubsidiaryCode = u.SubsidiaryCode,
                SubsidiaryName = u.SubsidiaryName,
                Telephone = u.Telephone
            }).OrderBy(x=>x.SubsidiaryCode).ToList();

            //return Json(new { data = spf }, JsonRequestBehavior.AllowGet);

            return PartialView("_SchoolSubsidiary", spf);
        }
        #endregion

        #region Delete SchoolSubsidiary

        [ValidateAntiForgeryToken]
        public ActionResult DeleteSubsi(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SchoolSubsidiary SchoolSubsidiary = SchoolSubsidiaryBusiness.Find(id);
            this.CheckPermissionForAction(SchoolSubsidiary.SchoolID, "schoolprofile", 4);
            IDictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3["SchoolID"] = SchoolSubsidiary.SchoolID;
            dic3["IsActive"] = true;

            this.SchoolSubsidiaryBusiness.Delete(id);
            this.SchoolSubsidiaryBusiness.Save();

            List<SchoolSubsidiary> lsSchoolSubsidiary = SchoolSubsidiaryBusiness.Search(dic3).ToList();
            ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = lsSchoolSubsidiary.Select(u => new SchoolSubsidiaryViewModel
            {
                Address = u.Address,
                CreatedDate = u.CreatedDate,
                DistanceFromHQ = u.DistanceFromHQ,
                Fax = u.Fax,
                SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                SquareArea = u.SquareArea,
                SubsidiaryCode = u.SubsidiaryCode,
                SubsidiaryName = u.SubsidiaryName,
                Telephone = u.Telephone
            }).OrderBy(x=>x.SubsidiaryCode).ToList();
            SchoolProfileForm spf = new SchoolProfileForm();

            return Json(Res.Get("SchoolSubsidiary_Label_DeleteSuccessMessage"));

        }
        #endregion

        #region Edit School
        /// <summary>
        /// PrepareEdit
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// SchoolProfileForm
        /// </returns>
        /// <author>hath</author>
        /// <date>12/11/2012</date>
        private SchoolProfileForm PrepareEdit(int id)
        {
            SetViewDataSchoolProfile();
            SetViewDataWithSchoolID(id);

            // Chi tiết trường
            SchoolProfile SchoolProfile = this.SchoolProfileBusiness.Find(id);
            SchoolSubsidiaryForm ssf = new SchoolSubsidiaryForm();
            SchoolProfileForm frm = new SchoolProfileForm();
            Session["ImagePath"] = "";
            frm.Image = SchoolProfile.Image;
            ssf.SchoolID = SchoolProfile.SchoolProfileID;
            frm.txtSyncCode = !string.IsNullOrEmpty(SchoolProfile.SyncCode) ? SchoolProfile.SyncCode : "";
            frm.SchoolProfileID = SchoolProfile.SchoolProfileID;
            frm.txtSchoolCode = SchoolProfile.SchoolCode;
            frm.txtSchoolName = SchoolProfile.SchoolName;
            frm.txtShortName = SchoolProfile.ShortName;
            frm.SchoolYearTitle = SchoolProfile.SchoolYearTitle;
            frm.cboArea = SchoolProfile.AreaID.Value;
            frm.cboProvince = SchoolProfile.ProvinceID.HasValue ? SchoolProfile.ProvinceID.Value : 0;
            frm.cboDistrict = SchoolProfile.DistrictID.HasValue ? SchoolProfile.DistrictID.Value : 0;
            frm.chkIsActiveSMAS = SchoolProfile.IsActiveSMAS.HasValue ? SchoolProfile.IsActiveSMAS.Value : false;
            frm.chkIsActive = SchoolProfile.IsActiveSMAS.HasValue ? SchoolProfile.IsActiveSMAS.Value : false;
            frm.CreatedDate = SchoolProfile.CreatedDate.HasValue ? SchoolProfile.CreatedDate.Value : DateTime.Now;
            frm.cboSupervisingDept = SchoolProfile.SupervisingDeptID.HasValue ? SchoolProfile.SupervisingDeptID.Value : 0;
            if (SchoolProfile.CommuneID != null) frm.cboCommune = SchoolProfile.CommuneID.Value;
            if (SchoolProfile.TrainingTypeID != null) frm.cboTrainingType = SchoolProfile.TrainingTypeID.Value;
            frm.SchoolTypeID = SchoolProfile.SchoolTypeID.HasValue ? SchoolProfile.SchoolTypeID.Value : 0;
            frm.AdminID = SchoolProfile.AdminID;
            frm.txtUserName = UserAccountBusiness.Find(frm.AdminID).aspnet_Users.UserName;
            frm.txtTelephone = SchoolProfile.Telephone;
            frm.txtFax = SchoolProfile.Fax;
            frm.txtEmail = SchoolProfile.Email;
            frm.txtAdress = SchoolProfile.Address;
            frm.dtpEstablishedDate = SchoolProfile.EstablishedDate.HasValue ? SchoolProfile.EstablishedDate.Value : DateTime.Now;
            frm.txtWebsite = SchoolProfile.Website;
            frm.EducationGradeSchool = SchoolProfile.EducationGrade;
            frm.cboSchoolManager = SchoolProfile.SchoolManagerID;
            frm.IsIndependent = SchoolProfile.IsIndependent;
            frm.rptPropertyOfSchool = new List<int>();
            List<int> lsPropertyOfSchool = PropertyOfSchoolBusiness.Search(new Dictionary<string, object>(){
                            {   "SchoolID", id },
                            {  "IsActive", true }
                    }).Select(o => o.SchoolPropertyCatID).ToList();

            if (lsPropertyOfSchool != null && lsPropertyOfSchool.Count > 0)
            {
                frm.rptPropertyOfSchool = lsPropertyOfSchool;
            }
            if (SchoolProfile.InSpecialDifficultZone != null) frm.InDiffZone = SchoolProfile.InSpecialDifficultZone.Value;

            frm.txtHeadMasterName = SchoolProfile.HeadMasterName;
            frm.txtHeadMasterPhone = SchoolProfile.HeadMasterPhone;
            frm.cboSMSActiveType = SchoolProfile.SMSTeacherActiveType;
            frm.cboSMSParentActiveType = SchoolProfile.SMSParentActiveType;
            UserAccount UserAcc = UserAccountBusiness.Find(SchoolProfile.AdminID);
            //Kiểm tra phân quyền
            if (UserAccountBusiness.IsSchoolAdmin(_globalInfo.UserAccountID))
            {
                frm.IsAdminSchool = true;
            }
            else
            {
                frm.IsAdminSchool = false;
                if (_globalInfo.SchoolID.HasValue && _globalInfo.EmployeeID.HasValue)
                {
                    frm.IsTeacher = true;
                }
            }

            aspnet_Membership usermb = aspnet_MembershipBusiness.Find(UserAcc.GUID);
            if (usermb != null)
            {
                frm.chkIsApproved = (usermb != null ? (usermb.IsApproved == 1 ? true : false) : false);
            }

            frm.IsNewModel = SchoolProfile.IsNewSchoolModel.HasValue ? SchoolProfile.IsNewSchoolModel.Value : false;
            return frm;
        }

        public PartialViewResult Edit(int? id)
        {
            this.CheckPermissionForAction(id, "schoolprofile", SystemParamsInFile.PERMISSION_LEVEL_DELETE);
            if (!id.HasValue) id = _globalInfo.SchoolID;

            if (!_globalInfo.IsAdminSchoolRole) //Neu khong phai la admin cap truong
            {
                string FullURL = HttpContext.Request.Path;
                // lấy Nhóm tài khoản
                IQueryable<int> lstUserGroup = UserGroupBusiness.GetGroupsOfUser(_globalInfo.UserAccountID).Select(a => a.GroupCatID);
                IQueryable<GroupMenu> lstGroupMenu = GroupMenuBusiness.All.Where(a => lstUserGroup.Contains(a.GroupID));

                // lấy lên quyền lớn nhất
                ViewData[SchoolProfileConstants.PERMISSION_MENU] = lstGroupMenu.Where(a => a.Menu.URL == FullURL).Max(a => a.Permission);
                if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                {
                    ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
                }
                if (_globalInfo.IsSchoolRole)
                {
                    if (GetMenupermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin) > 1)
                    {
                        ViewData[SystemParamsInFile.PERMISSION] = SystemParamsInFile.PER_DELETE;
                    }
                }
            }

            SchoolProfileForm sfrm = PrepareEdit(id.Value);
            ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = SchoolSubsidiaryBusiness.All.Where(o => o.SchoolID == sfrm.SchoolProfileID && o.IsActive == true).OrderBy(o => o.SubsidiaryName)
                                                                                .Select(u => new SchoolSubsidiaryViewModel
                                                                                {
                                                                                    Address = u.Address,
                                                                                    CreatedDate = u.CreatedDate,
                                                                                    DistanceFromHQ = u.DistanceFromHQ,
                                                                                    Fax = u.Fax,
                                                                                    SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                                                                                    SquareArea = u.SquareArea,
                                                                                    SubsidiaryCode = u.SubsidiaryCode,
                                                                                    SubsidiaryName = u.SubsidiaryName,
                                                                                    Telephone = u.Telephone,
                                                                                    CommutedBy = u.CommutedBy
                                                                                }).OrderBy(x=>x.SubsidiaryCode).ToList();
            IQueryable<SchoolProfile> iquery = (from sp in SchoolProfileBusiness.All
                                                where (sp.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                                                || sp.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE)
                                                select sp);
            if (sfrm.cboCommune.HasValue)
            {
                iquery = iquery.Where(p => p.CommuneID == sfrm.cboCommune.Value);
            }
            else if (sfrm.cboDistrict > 0)
            {
                iquery = iquery.Where(p => p.DistrictID == sfrm.cboDistrict);
            }

            List<int> lstSchoolManagerID = new List<int>();
            lstSchoolManagerID = iquery.Where(p => p.IsIndependent.HasValue && p.IsIndependent.Value).Select(p => p.SchoolProfileID).Distinct().ToList();
            lstSchoolManagerID.Add(id.Value);
            iquery = iquery.Where(p => !lstSchoolManagerID.Contains(p.SchoolProfileID));
            ViewData[SchoolProfileConstants.SCHOOL_MANAGER] = iquery.ToList();
            return PartialView("Edit", sfrm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult EditSchoolProfile(SchoolProfileForm frm, FormCollection frc, List<int> rptPropertyOfSchool)
        {
            this.CheckPermissionForAction(frm.SchoolProfileID, "SchoolProfile", SystemParamsInFile.PERMISSION_LEVEL_DELETE);
            if (GetMenupermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            SchoolProfile SchoolProfile = this.SchoolProfileBusiness.Find(frm.SchoolProfileID);
            UserAccount UserAcc = UserAccountBusiness.Find(SchoolProfile.AdminID);
            aspnet_Membership usermb = aspnet_MembershipBusiness.Find(UserAcc.GUID);//.All.Where(o => o.UserId == UserAcc.GUID).FirstOrDefault();
            // AnhVD 20140806 - Add in scope transaction 
            object imagePath = Session["ImagePath"];
            bool IsApproved = usermb.IsApproved == 1 ? true : false;
            bool IsForGetPassword = false;
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                SchoolProfile.Image = imageBytes;
                frm.Image = SchoolProfile.Image;

                if (frm.chkRemoveImage == true)
                {
                    SchoolProfile.Image = new byte[1];
                }
                else
                {
                    SchoolProfile.Image = frm.Image;
                }
            }
            //Nếu tài khoản đăng nhập không phải là tài khoản cấp trường
            if (!_globalInfo.IsAdminSchoolRole)
            {
                if ("1".Equals(frc["chkAutoCode"])) SetSchoolCode(SchoolProfile.AdminID.Value);
                else SchoolProfile.SchoolCode = frm.txtSchoolCode.Trim();

                //Chi 
                if (_globalInfo.IsSystemAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
                {
                    SchoolProfile.SupervisingDeptID = frm.cboSupervisingDept;
                    SchoolProfile.AreaID = frm.cboArea;
                    //if (frm.cboDistrict != 0) SchoolProfile.DistrictID = frm.cboDistrict;
                    SchoolProfile.EducationGrade = frm.EducationGradeSchool;
                    SchoolProfile.TrainingTypeID = frm.cboTrainingType;
                    SchoolProfile.SchoolTypeID = int.Parse(frc["rptSchoolType"]);
                    IsApproved = frm.chkIsApproved;
                }


                IsForGetPassword = frm.chkResetPassword;
                //21/04/2016: Mac dinh kich hoat SMS
                //SchoolProfile.SMSTeacherActiveType = 2;
                //SchoolProfile.SMSParentActiveType = 1;
                //SchoolProfile.IsActiveSMAS = true;// //frm.chkIsActiveSMAS;
                // Chi co supper admin he thong moi co quyen thay doi thong tin nay
                //if (_globalInfo.IsSystemAdmin)
                //{
                SchoolProfile.SyncCode = frm.txtSyncCode;
                SchoolProfile.SMSTeacherActiveType = SchoolProfile.SMSTeacherActiveType; //frm.cboSMSActiveType;
                SchoolProfile.SMSParentActiveType = SchoolProfile.SMSParentActiveType; //frm.cboSMSParentActiveType;
                SchoolProfile.IsActiveSMAS = frm.chkIsActiveSMAS;
                //}
            }
            else
            {
                if ("1".Equals(frc["chkAutoCode"])) SetSchoolCode(SchoolProfile.AdminID.Value);
                else SchoolProfile.SchoolCode = frm.txtSchoolCode.Trim();
            }


            SchoolProfile.SchoolName = frm.txtSchoolName;
            SchoolProfile.ShortName = frm.txtShortName;
            SchoolProfile.SchoolYearTitle = frm.SchoolYearTitle;
            SchoolProfile.CommuneID = frm.cboCommune;

            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                SchoolProfile.ProvinceID = _globalInfo.ProvinceID.Value;
                SchoolProfile.DistrictID = _globalInfo.DistrictID;
            }

            if (_globalInfo.IsSuperVisingDeptRole)
                SchoolProfile.ProvinceID = _globalInfo.ProvinceID.Value;

            if (_globalInfo.IsSuperRole)
                SchoolProfile.ProvinceID = frm.cboProvince;

            SchoolProfile.Telephone = frm.txtTelephone;
            SchoolProfile.Fax = frm.txtFax;
            SchoolProfile.Email = frm.txtEmail;
            SchoolProfile.Address = frm.txtAdress;
            SchoolProfile.EstablishedDate = frm.dtpEstablishedDate;
            SchoolProfile.Website = frm.txtWebsite;

            if (frm.cboDistrict != 0)
                SchoolProfile.DistrictID = frm.cboDistrict;

            SchoolProfile.HeadMasterName = frm.txtHeadMasterName;
            SchoolProfile.HeadMasterPhone = frm.txtHeadMasterPhone;
            SchoolProfile.HasSubsidiary = true;
            SchoolProfile.IsActive = true;
            SchoolProfile.ModifiedDate = DateTime.Now;
            if (_globalInfo.IsSystemAdmin || _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole)
            {
                if ((SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL))
                {
                    SchoolProfile.IsIndependent = "True".Equals(frc["chkIndependentID"]) ? true : false;
                    if ("True".Equals(frc["chkIndependentID"]))
                    {
                        SchoolProfile.SchoolManagerID = frc["cboSchoolManager"] != null ? Int32.Parse(frc["cboSchoolManager"]) : 0;
                    }
                    else
                    {
                        SchoolProfile.SchoolManagerID = null;
                    }
                }
            }
            if (SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY
            || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY
            || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY
            || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL
            || SchoolProfile.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS)
            {
                SchoolProfile.IsNewSchoolModel = frm.IsNewModel;
            }
            else
            {
                SchoolProfile.IsNewSchoolModel = false;
            }
            List<int> Property = new List<int>();
            if (rptPropertyOfSchool != null)
            {
                Property = new List<int>(rptPropertyOfSchool.Count);
                for (int i = 0; i < rptPropertyOfSchool.Count; i++)
                {
                    Property.Add(rptPropertyOfSchool[i]);
                }
            }
            if (Property.Contains(SchoolProfileConstants.SCHOOL_PROPERTY_CATEGORY_IN_PARTICULARLY_DIFFICULT_AREA))
            {
                if (frm.InDiffZone > 0) SchoolProfile.InSpecialDifficultZone = frm.InDiffZone;
                else SchoolProfile.InSpecialDifficultZone = SystemParamsInFile.IN_PARTICULARLY_DIFFICULT_COMMUNE;
            }
            else SchoolProfile.InSpecialDifficultZone = null;

            Utils.Utils.TrimObject(SchoolProfile);
            if (frm.chkResetPassword)//neu cap lai mat khau
            {
                //cap mat khau cho account
                //Kiểm tra độ dài password
                if (frm.ChangePass.Length < 8)
                {
                    return Json(new JsonMessage("Mật khẩu phải lớn hơn 8 ký tự.", "error"));
                }
                else if (!frm.ChangePass.Equals(frm.ConfirmChangePass))
                {
                    return Json(new JsonMessage("Xác nhận mật khẩu mới không hợp lệ.", "error"));
                }
                else if (!SMAS.Business.Common.Utils.CheckPasswordInValid(frm.ChangePass))
                {
                    // return Res.Get("Mật khẩu mới phải có chữ in hoa, chữ thường và ký tự đặt biệt."); //kí tự đặt biệt không chưa dấu ' và / \
                    return Json(new JsonMessage("Mật khẩu mới không được nhỏ hơn 8 ký tự, phải bao gồm số, chữ thường, chữ in hoa và ký tự đặc biệt.", "error"));
                }
                else if (frm.ChangePass != frm.ConfirmChangePass)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFail"), "error"));
                }
                else if (!CheckPassRequire(frm.ChangePass))
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFormatFail"), "error"));

                }
                else if (frm.ChangePass.Length < MinLengthPassword || frm.ChangePass.Length > MaxLengthPassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_MaxlengthPassword"), "error"));
                }
            }
            // Cập nhật thông tin trường - KO thay đổi được UserName
            this.SchoolProfileBusiness.Update(SchoolProfile, Property, frm.chkResetPassword, IsApproved, frm.chkIsActiveSMAS, _globalInfo.IsSuperRole, _globalInfo.RoleID, frm.ChangePass);
            this.SchoolProfileBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }

        public JsonResult AjaxLoadSchoolManager(int? DistrictID, int? CommuneID, int SchoolID)
        {
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            IQueryable<SchoolProfile> iquery = (from sp in SchoolProfileBusiness.All
                                                where (sp.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                                                || sp.EducationGrade == SMAS.Business.Common.SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE)
                                                select sp);
            if (CommuneID.HasValue)
            {
                iquery = iquery.Where(p => p.CommuneID == CommuneID.Value);
            }
            else if (DistrictID.HasValue)
            {
                iquery = iquery.Where(p => p.DistrictID == DistrictID.Value);
            }

            List<int> lstSchoolManagerID = new List<int>();
            lstSchoolManagerID = iquery.Where(p => p.IsIndependent.HasValue && p.IsIndependent.Value).Select(p => p.SchoolProfileID).Distinct().ToList();
            lstSchoolManagerID.Add(SchoolID);
            iquery = iquery.Where(p => !lstSchoolManagerID.Contains(p.SchoolProfileID));
            List<SchoolProfile> lstSP = iquery.OrderBy(p => p.SchoolName).ToList();
            return Json(new SelectList(lstSP, "SchoolProfileID", "SchoolName", objSP != null ? objSP.SchoolManagerID : 0));
        }
        #endregion

        #region Details
        private SchoolProfileForm PrepareDetails(int id)
        {
            this.CheckPermissionForAction(id, "schoolprofile", 4);
            SetViewDataSchoolProfile();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolProfileID"] = id;
            SchoolProfile SchoolProfile = this.SchoolProfileBusiness.Find(id);
            IEnumerable<District> lstDistrict = new List<District>();
            if (DistrictBusiness.All.Where(o => o.ProvinceID == SchoolProfile.ProvinceID && o.IsActive == true).Count() != 0)
            {
                lstDistrict = this.DistrictBusiness.All.Where(o => o.ProvinceID == SchoolProfile.ProvinceID && o.IsActive == true).ToList();
            }
            ViewData[SchoolProfileConstants.LS_District_Create] = lstDistrict;
            IEnumerable<Commune> lstCommune = new List<Commune>();
            if (CommuneBusiness.All.Where(p => p.DistrictID == SchoolProfile.DistrictID && p.IsActive == true).Count() != 0)
            {
                lstCommune = this.CommuneBusiness.All.Where(p => p.DistrictID == SchoolProfile.DistrictID && p.IsActive == true).ToList();
            }
            ViewData[SchoolProfileConstants.LS_Commune] = lstCommune;
            SchoolProfileForm frm = new SchoolProfileForm();
            frm.SchoolProfileID = SchoolProfile.SchoolProfileID;
            frm.txtSchoolCode = SchoolProfile.SchoolCode;
            frm.txtSchoolName = SchoolProfile.SchoolName;
            frm.txtShortName = SchoolProfile.ShortName;
            frm.cboArea = SchoolProfile.AreaID.Value;
            frm.AreaName = this.AreaBusiness.Find(SchoolProfile.AreaID.Value).AreaName;
            frm.cboProvince = SchoolProfile.ProvinceID.Value;
            frm.ProvinceName = this.ProvinceBusiness.Find(SchoolProfile.ProvinceID.Value).ProvinceName;
            frm.cboDistrict = SchoolProfile.DistrictID.Value;
            frm.DistrictName = this.DistrictBusiness.Find(SchoolProfile.DistrictID.Value).DistrictName;
            if (SchoolProfile.CommuneID != null)
            {
                frm.cboCommune = SchoolProfile.CommuneID.Value;
                frm.CommuneName = this.CommuneBusiness.Find(SchoolProfile.CommuneID.Value).CommuneName;
            }
            frm.cboSupervisingDept = SchoolProfile.SupervisingDeptID.Value;
            frm.SupervisingDeptName = this.SupervisingDeptBusiness.Find(SchoolProfile.SupervisingDeptID.Value).SupervisingDeptName;
            frm.cboTrainingType = SchoolProfile.TrainingTypeID.Value;
            frm.TrainingTypeName = this.TrainingTypeBusiness.Find(SchoolProfile.TrainingTypeID.Value).Resolution;
            frm.SchoolTypeID = SchoolProfile.SchoolTypeID.Value;
            frm.SchoolTypeName = this.TrainingTypeBusiness.Find(SchoolProfile.TrainingTypeID.Value).Resolution;
            frm.txtTelephone = SchoolProfile.Telephone;
            frm.txtFax = SchoolProfile.Fax;
            frm.txtEmail = SchoolProfile.Email;
            frm.txtAdress = SchoolProfile.Address;
            frm.EstablishedDate = SchoolProfile.EstablishedDate.HasValue ? SchoolProfile.EstablishedDate.Value.ToString("dd/MM/yyyy") : "";
            frm.txtWebsite = SchoolProfile.Website;
            frm.Image = SchoolProfile.Image;
            frm.AdminID = SchoolProfile.AdminID;
            frm.txtUserName = UserAccountBusiness.Find(frm.AdminID).aspnet_Users.UserName;
            frm.EducationGrade = SchoolProfile.EducationGrade;
            frm.SchoolYearTitle = SchoolProfile.SchoolYearTitle;
            if ((frm.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_CHILDREN) != 0)
                frm.chkChildren = true;
            if ((frm.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_CHILDCARE) != 0)
                frm.chkChildcare = true;
            if ((frm.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY) != 0)
                frm.chkTertiary = true;
            if ((frm.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY) != 0)
                frm.chkSecondary = true;
            if ((frm.EducationGrade & SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY) != 0)
                frm.chkPrimary = true;
            frm.txtHeadMasterName = SchoolProfile.HeadMasterName;
            frm.txtHeadMasterPhone = SchoolProfile.HeadMasterPhone;
            frm.txtShortName = SchoolProfile.ShortName;
            frm.chkIsActiveSMAS = SchoolProfile.IsActiveSMAS.HasValue ? SchoolProfile.IsActiveSMAS.Value : false;
            UserAccount UserAcc = UserAccountBusiness.Find(SchoolProfile.AdminID);
            aspnet_Membership usermb = aspnet_MembershipBusiness.All.Where(o => o.UserId == UserAcc.GUID).FirstOrDefault();
            var lsE = EmployeeBusiness.SearchTeacher(SchoolProfile.SchoolProfileID, new Dictionary<string, object>() { { "SchoolID", SchoolProfile.SchoolProfileID } }).Select(o => o.EmployeeID).ToList();
            var lsUA = UserAccountBusiness.All.Where(o => lsE.Contains(o.EmployeeID.Value)).Select(o => o.GUID).ToList();
            var lsM = aspnet_MembershipBusiness.All.Where(o => lsUA.Contains(o.UserId)).Any(o => o.IsApproved == 0);
            if (lsE.Count() > 0)
            {
                frm.chkIsActive = !lsM;
            }
            else
            {
                frm.chkIsActive = false;
            }
            if (usermb != null)
            {
                frm.chkIsApproved = usermb.IsApproved == 1 ? true : false;

            }
            else
            {
                frm.chkIsActive = false;
                frm.chkIsApproved = false;
            }


            return frm;
        }

        public PartialViewResult Details(int id)
        {
            this.CheckPermissionForAction(id, "schoolprofile", 4);
            SchoolProfileForm sfrm = PrepareDetails(id);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic.Add("SchoolID", sfrm.SchoolProfileID);
            ViewData[SchoolProfileConstants.LS_SchoolSubsidiary] = SchoolSubsidiaryBusiness.Search(dic)
                                                                                            .ToList()
                                                                                            .Select(u => new SchoolSubsidiaryViewModel
                                                                                            {
                                                                                                Address = u.Address,
                                                                                                CreatedDate = u.CreatedDate,
                                                                                                DistanceFromHQ = u.DistanceFromHQ,
                                                                                                Fax = u.Fax,
                                                                                                SchoolSubsidiaryID = u.SchoolSubsidiaryID,
                                                                                                SquareArea = u.SquareArea,
                                                                                                SubsidiaryCode = u.SubsidiaryCode,
                                                                                                SubsidiaryName = u.SubsidiaryName,
                                                                                                Telephone = u.Telephone,
                                                                                                CommutedBy = u.CommutedBy
                                                                                            }).OrderBy(x=>x.SubsidiaryCode).ToList();
            return PartialView("Details", sfrm);
        }
        #endregion

        #region Delete School
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "schoolprofile", 4);
            GlobalInfo Global = new GlobalInfo();
            if (GetMenupermission("SchoolProfile", Global.UserAccountID, Global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(id);
            int Admin = 0;
            if (sp.AdminID.HasValue)
            {
                Admin = sp.AdminID.Value;
            }

            UserInfoBO userInfo = _globalInfo.GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = 0,
                SCHOOL_ID = id,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_SCHOOL,
                SHORT_DESCRIPTION = sp.SchoolName
            };


            List<RESTORE_DATA_DETAIL> lstResDetial = this.SchoolProfileBusiness.Delete(id, Admin, objRes);

            this.SchoolProfileBusiness.Save();
            //Luu hanh dong xoa
            if (lstResDetial != null)
            {
                RestoreDataBusiness.Insert(objRes);
                RestoreDataBusiness.Save();
                RestoreDataDetailBusiness.BulkInsert(lstResDetial, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
            }

            return Json(Res.Get("SchoolProfile_Label_DeleteSuccessMessage"));


        }
        #endregion

        #region SetViewData
        public void SetViewDataSchoolProfile()
        {
            SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            // Khu vực
            ViewData[SchoolProfileConstants.LS_Area] = this.AreaBusiness.All.Where(o => o.IsActive == true).ToList();

            // Nếu tài khoản là admin
            List<Province> lsProvince = ProvinceBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.ProvinceName).ToList();
            ViewData[SchoolProfileConstants.LS_Province] = lsProvince; // Sử dụng ở hàm search
            ViewData[SchoolProfileConstants.LS_Province_Create] = lsProvince; // Sử dụng để hiển thị chi tiết

            //Nếu tài khoản là nhân viên phòng sở hoặc quản trị trường
            if (_globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole || _globalInfo.IsAdminSchoolRole)
            {
                IQueryable<Province> lspro = ProvinceBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true);
                if (lspro.Count() > 0)
                {
                    lsProvince = lspro.OrderBy(o => o.ProvinceName).ToList();
                    ViewData[SchoolProfileConstants.LS_Province] = lsProvince;
                    ViewData[SchoolProfileConstants.LS_Province_Create] = lsProvince;
                }
            }
            #region Loại trường và Tính chất trường

            List<SchoolPropertyCat> lstSchoolPropertyCat = SchoolPropertyCatBusiness.All.Where(o => o.IsActive == true)
                                                .OrderBy(o => o.Resolution).ToList();
            lstSchoolPropertyCat = StandardSchoolPropertyCat(lstSchoolPropertyCat);
            ViewData[SchoolProfileConstants.LS_SchoolPropertyCat] = lstSchoolPropertyCat;

            List<SchoolType> lsSchoolType = SchoolTypeBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.Resolution).ToList();
            /*List<ViettelCheckboxList> lstTypeRadio = new List<ViettelCheckboxList>();
            ViettelCheckboxList item = new ViettelCheckboxList();
            if (lsSchoolType != null)
            {
                for (int i = 0, Count = lsSchoolType.Count; i < Count; i++)
                {
                    item = new ViettelCheckboxList();
                    item.Label = lsSchoolType[i].Resolution;
                    item.cchecked = (i == 0 ? true : false);
                    item.disabled = false;
                    item.Value = lsSchoolType[i].SchoolTypeID;
                    lstTypeRadio.Add(item);
                }
            }*/
            ViewData[SchoolProfileConstants.LS_SchoolType] = new SelectList(lsSchoolType, "SchoolTypeID", "Resolution", 1);
            #endregion

            // Quận/Huyện
            List<District> lstDistrict = new List<District>();
            ViewData[SchoolProfileConstants.LS_District] = lstDistrict;
            ViewData[SchoolProfileConstants.LS_District_Create] = lstDistrict;
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                #region Sở
                lstDistrict = this.DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true).OrderBy(o => o.DistrictName).ToList();
                ViewData[SchoolProfileConstants.LS_District] = lstDistrict;
                ViewData[SchoolProfileConstants.LS_District_Create] = lstDistrict;
                ViewData[SchoolProfileConstants.INDEX_DISTRICT] = lstDistrict.FindIndex(o => o.DistrictID == _globalInfo.DistrictID);
                #endregion
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                #region Phòng
                lstDistrict = this.DistrictBusiness.All.Where(o => o.DistrictID == _globalInfo.DistrictID).ToList();
                ViewData[SchoolProfileConstants.LS_District] = lstDistrict;
                ViewData[SchoolProfileConstants.LS_District_Create] = lstDistrict;
                #endregion
            }

            //Xã/Phường
            ViewData[SchoolProfileConstants.LS_Commune] = new List<Commune>();


            if (_globalInfo.IsSystemAdmin)
            {
                #region Admin hệ thống
                List<SupervisingDept> ListSupervisingDept = new List<SupervisingDept>();
                IQueryable<SupervisingDept> IqSupervising = SupervisingDeptBusiness.All.Where(o => o.HierachyLevel == 3 && o.IsActive == true)
                            .OrderBy(o => o.SupervisingDeptName);

                List<SupervisingDept> lstSuper = new List<SupervisingDept>();
                if (IqSupervising.Count() > 0)
                {
                    foreach (SupervisingDept sd in IqSupervising)
                    {
                        ListSupervisingDept.Add(sd);
                        IQueryable<SupervisingDept> IqChildDept = SupervisingDeptBusiness.All.Where(o => o.HierachyLevel == 5 && o.ParentID == sd.SupervisingDeptID
                            && o.IsActive == true).OrderBy(o => o.SupervisingDeptName);
                        if (IqChildDept.Count() > 0)
                        {
                            foreach (SupervisingDept child in IqChildDept)
                            {
                                child.SupervisingDeptName = string.Format("...........{0}", child.SupervisingDeptName);
                                ListSupervisingDept.Add(child);
                            }
                        }
                    }
                }
                ViewData[SchoolProfileConstants.LS_SupervisingDept] = ListSupervisingDept;
                #endregion
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                #region SỞ
                List<SupervisingDept> ListSupervisingDept = new List<SupervisingDept>();
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID.Value);
                if (supervisingDept != null && supervisingDept.IsActive)
                {

                    ListSupervisingDept.Add(supervisingDept);
                    IQueryable<SupervisingDept> IqChild = SupervisingDeptBusiness.All
                        .Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true && o.HierachyLevel == 5 && o.ParentID == supervisingDept.SupervisingDeptID)
                        .OrderBy(o => o.SupervisingDeptName);
                    if (IqChild.Count() > 0)
                    {
                        foreach (SupervisingDept child in IqChild)
                        {
                            child.SupervisingDeptName = string.Format("...........{0}", child.SupervisingDeptName);
                            ListSupervisingDept.Add(child);
                        }
                    }
                }

                ViewData[SchoolProfileConstants.LS_SupervisingDept] = ListSupervisingDept;
                #endregion
            }
            else if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                #region Phòng
                ViewData[SchoolProfileConstants.LS_SupervisingDept] = SupervisingDeptBusiness.All.Where(o => o.SupervisingDeptID == _globalInfo.SupervisingDeptID
                            && o.HierachyLevel == 5).OrderBy(o => o.SupervisingDeptName).ToList();
                #endregion
            }
            else if (_globalInfo.IsSchoolRole)
            {
                #region Cấp trường
                SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                ViewData[SchoolProfileConstants.LS_SupervisingDept] = this.SupervisingDeptBusiness.All.Where(o => o.SupervisingDeptID == sp.SupervisingDeptID).ToList();
                #endregion
            }

            // Loại đào tạo
            ViewData[SchoolProfileConstants.LS_TrainingType] = TrainingTypeBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.Resolution);

            // Gói SMSTeacher
            List<SelectListItem> lstSMSTeacher = new List<SelectListItem>();
            lstSMSTeacher.Add(new SelectListItem { Text = Res.Get("Not_Active"), Value = "0" });
            //lstSMSTeacher.Add(new SelectListItem { Text = Res.Get("Active_package_limit"), Value = "1" });
            lstSMSTeacher.Add(new SelectListItem { Text = Res.Get("Active_package_not_limit"), Value = "2" });
            //lstSMSTeacher.Add(new SelectListItem { Text = Res.Get("SMS_blocked"), Value = "3" });
            ViewData["SMS"] = new SelectList(lstSMSTeacher, "Value", "Text");

            // Gói SMSParent
            List<SelectListItem> lstSMSParent = new List<SelectListItem>();
            lstSMSParent.Add(new SelectListItem { Text = Res.Get("SMS_NotActiveParent"), Value = SystemParamsInFile.COMMON_STATUS_NOTISACTIVE.ToString() });
            lstSMSParent.Add(new SelectListItem { Text = Res.Get("SMS_ActiveParent"), Value = SystemParamsInFile.COMMON_STATUS_ISACTIVE.ToString() });
            ViewData["SMSParent"] = new SelectList(lstSMSParent, "Value", "Text");

            // Vùng đặc biệt khó khăn
            ViewData[SchoolProfileConstants.LST_DIFFICULT_AREA] = CommonList.DifficultZone();

            ViewData[SchoolProfileConstants.SCHOOL_MANAGER] = new List<SchoolProfile>();

            // Cấp học
            List<ComboObject> lsGrade = new List<ComboObject>();
            List<ComboObject> lst = new List<ComboObject>();
            if (!_globalInfo.IsSystemAdmin)
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));
                    ViewData["Grade"] = CommonList.EducationGradeSchool();
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));

                    ViewData["Grade"] = lst;
                }

                ViewData[SchoolProfileConstants.LIST_GRADE] = lst;
            }
            else
            {
                ViewData["Grade"] = CommonList.EducationGradeSchool();
                ViewData[SchoolProfileConstants.LIST_GRADE] = CommonList.EducationGradeSchool();
            }
        }

        private List<SchoolPropertyCat> StandardSchoolPropertyCat(List<SchoolPropertyCat> lstInput)
        {
            List<SchoolPropertyCat> lstResult = new List<SchoolPropertyCat>();

            SchoolPropertyCat obj = null ;
            // Sắp xếp lại thứ tự các tính chất của trường
            List<int> lstID = new List<int> { 2, 13, 10, 7, 12, 9, 8, 3, 25, 5, 4, 24, 6, 23, 22, 11, 1};
            foreach (var item in lstID)
            {
                obj = lstInput.Where(x => x.SchoolPropertyCatID == item).FirstOrDefault();
                if (obj != null)
                    lstResult.Add(obj);
            }

            return lstResult;
                
        }
        #endregion

        #region Load Data Combobox

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingDistrict(int? provinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName).ToList();
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingSupervisingDept(int? provinceId)
        {
            List<SupervisingDept> lst = new List<SupervisingDept>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                IQueryable<SupervisingDept> lsSupervising = SupervisingDeptBusiness.Search(dic)
                    .Where(o => o.HierachyLevel == 3).OrderBy(o => o.SupervisingDeptName);

                List<SupervisingDept> lstSuper = new List<SupervisingDept>();
                if (lsSupervising.Count() > 0)
                {
                    lstSuper = lsSupervising.ToList();

                    foreach (SupervisingDept sd in lstSuper)
                    {
                        lst.Add(sd);
                        IQueryable<SupervisingDept> lsParentSupervising = SupervisingDeptBusiness.Search(dic)
                        .Where(o => o.HierachyLevel == 5 && o.ParentID == sd.SupervisingDeptID).OrderBy(o => o.SupervisingDeptName);
                        if (lsParentSupervising.Count() > 0)
                        {
                            List<SupervisingDept> lstParentSuper = lsParentSupervising.ToList();

                            foreach (SupervisingDept sdp in lstParentSuper)
                            {
                                sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                                lst.Add(sdp);
                            }

                        }
                    }
                }
                ViewData[SchoolProfileConstants.LS_SupervisingDept] = lst;
            }
            if (lst == null)
                lst = new List<SupervisingDept>();
            return Json(new SelectList(lst, "SupervisingDeptID", "SupervisingDeptName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingDistrictCreate(int? provinceId)
        {
            IEnumerable<District> lst = new List<District>();
            if (provinceId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ProvinceID"] = provinceId;
                dic["IsActive"] = true;
                lst = this.DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName).ToList();
            }
            if (lst == null)
                lst = new List<District>();
            return Json(new SelectList(lst, "DistrictID", "DistrictName"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationGrade(int? idSupervisingDept)
        {
            List<ComboObject> lst = new List<ComboObject>();
            if (idSupervisingDept != null)
            {
                SupervisingDept super = SupervisingDeptBusiness.Find(idSupervisingDept.Value);
                //Nếu là phòng
                if (super != null)
                {
                    if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                    {
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
                    }
                    //Neu la so
                    if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                    {
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));
                        lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));
                    }
                }
            }
            return Json(new SelectList(lst, "key", "value"));
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadingCommune(int? districtId)
        {
            IEnumerable<Commune> lst = new List<Commune>();
            if (districtId != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DistrictID"] = districtId;
                dic["IsActive"] = true;
                lst = this.CommuneBusiness.Search(dic).OrderBy(o => o.CommuneName).ToList();
            }
            if (lst == null)
                lst = new List<Commune>();
            return Json(new SelectList(lst, "CommuneID", "CommuneName"));
        }

        #endregion

        #region paginate
        /// <summary>
        /// _Search
        /// </summary>
        /// <param name="SearchForm">The search form.</param>
        /// <param name="page">The page.</param>
        /// <param name="orderBy">The order by.</param>
        /// <returns>
        /// Paginate{SchoolProfileForm}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/11/2012</date>
        private Paginate<SchoolProfileForm> _Search(SchoolProfileSearchForm SearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            Utils.Utils.TrimObject(SearchForm);
            SearchInfo["SchoolName"] = SearchForm.SchoolName;
            SearchInfo["SchoolCode"] = SearchForm.SchoolCode;

            //Chiendd:13.01.2015: Sua lai cach lay du lieu truong cho dung voi quyen
            if (Global.IsSuperRole) // admin truong
            {
                SearchInfo["ProvinceID"] = SearchForm.Province;
                SearchInfo["DistrictID"] = SearchForm.District;
            }
            else
            {
                SearchInfo["ProvinceID"] = Global.ProvinceID;
                if (Global.IsSuperVisingDeptRole) // So
                {
                    SearchInfo["DistrictID"] = SearchForm.District;
                }
                else
                {
                    SearchInfo["DistrictID"] = Global.DistrictID;
                }
            }

            SearchInfo["SupervisingDeptID"] = Global.SupervisingDeptID;
            SearchInfo["EducationGrade"] = SearchForm.EducationGrade;
            SearchInfo["Administrator"] = SearchForm.Administrator;
            SearchInfo["IsActive"] = true;

            if (SearchForm.EducationGrade != 0)
            {
                if (SearchForm.EducationGrade == 1)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY;
                }
                if (SearchForm.EducationGrade == 2)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY;
                }
                if (SearchForm.EducationGrade == 4)
                {
                    SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY;
                }
            }
            // Thuc hien truy van du lieu
            IQueryable<SchoolProfile> lst = this.SchoolProfileBusiness.Search(SearchInfo);

            // Sap xep
            string sort = "";
            string order = "";
            if (orderBy != "")
            {
                sort = orderBy.Split('-')[0];
                order = orderBy.Split('-')[1];
            }

            if (sort == "SchoolName" || sort == "")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SchoolName.ToLower());
                }
                else
                {
                    lst = lst.OrderBy(o => o.SchoolName.ToLower());
                }
            }

            else if (sort == "SchoolCode")
            {
                if (order == "desc")
                {
                    lst = lst.OrderByDescending(o => o.SchoolCode);
                }
                else
                {
                    lst = lst.OrderBy(o => o.SchoolCode);
                }
            }

            // Convert du lieu sang doi tuong SupervisingDeptForm
            IQueryable<SchoolProfileForm> res = lst.Select(o => new SchoolProfileForm
            {

                SchoolProfileID = o.SchoolProfileID,
                txtSchoolName = o.SchoolName,
                txtSchoolCode = o.SchoolCode,
                cboDistrict = o.DistrictID.Value,
                dtpEstablishedDate = o.EstablishedDate.Value,
                cboSupervisingDept = o.SupervisingDept.SupervisingDeptID,
                txtHeadMasterName = o.HeadMasterName,
                Administrator = o.UserAccount.aspnet_Users.UserName,
                txtUserName = o.UserAccount.aspnet_Users.UserName,
                chkIsActive = o.IsActive,
                chkIsApproved = o.UserAccount.aspnet_Users.aspnet_Membership.IsApproved == 1 ? true : false,
                SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                Resolution = o.TrainingType.Resolution,
                IsLoggedIn = o.UserAccount.FirstLoginDate != null,
                Password = "********"
            });

            // Thuc hien phan trang tung phan
            Paginate<SchoolProfileForm> Paging = new Paginate<SchoolProfileForm>(res);
            Paging.page = page;
            Paging.size = 15;
            Paging.paginate();

            return Paging;
        }

        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(SchoolProfileSearchForm SchoolProfileSearchForm, int page = COUNT_PAGE, string orderBy = "")
        {
            Session[SSK_SEARCH_TERM_PAGE] = page;
            Session[SSK_SEARCH_TERM_ORDERBY] = orderBy;
            Paginate<SchoolProfileForm> paging = this._Search(SchoolProfileSearchForm, page, orderBy);
            GridModel<SchoolProfileForm> gm = new GridModel<SchoolProfileForm>();
            gm.Total = paging.total;
            gm.Data = paging.List;
            return View(gm);
        }
        #endregion

        #region Upload Image

        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            // The Name of the Upload component is "attachments" 
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {

                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName); fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }
            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

        }

        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            return Content("");
        }
        public byte[] FileToByteArray(string _FileName)
        {
            byte[] _Buffer = null;

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // attach filestream to binary reader
                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);

                // get total int length of the file
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);

                // close file reader
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception ex)
            {
                string paramList = "_FileName=" + _FileName;
                LogExtensions.ErrorExt(logger, DateTime.Now, "FileToByteArray", paramList, ex);
            }

            return _Buffer;
        }
        #endregion

        #region ExportExcel
        //[ValidateAntiForgeryToken]
        public FileResult ExportExcel()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int ProvinceID = 0;
            int DistrictID = 0;
            int EducationGrade = 0;
            string SchoolName = Request["SchoolName"];
            string SchoolCode = Request["SchoolCode"];
            string Administrator = Request["Administrator"];
            if (_globalInfo.ProvinceID != null)
            {
                ProvinceID = _globalInfo.ProvinceID.Value;
            }
            if (Request["Province"] != "")
            {
                ProvinceID = int.Parse(Request["Province"]);
            }
            if (Request["District"] != "")
            {
                DistrictID = int.Parse(Request["District"]);
            }
            SearchInfo["EducationGrade"] = Request["EducationGrade"];
            if (Request["EducationGrade"] != "") // Tim theo cap hoc
            {
                EducationGrade = int.Parse(Request["EducationGrade"]);
                if (EducationGrade != 0)
                {
                    if (EducationGrade == 1)
                    {
                        SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_PRIMARY;
                    }
                    if (EducationGrade == 2)
                    {
                        SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_SECONDARY;
                    }
                    if (EducationGrade == 4)
                    {
                        SearchInfo["EducationGrade"] = SMAS.Web.Constants.GlobalConstants.COUNT_TERTIARY;
                    }
                }
            }
            SearchInfo["SupervisingDeptID"] = _globalInfo.SupervisingDeptID;

            if (_globalInfo.IsSuperRole) // admin truong
            {
                SearchInfo["ProvinceID"] = ProvinceID;
                SearchInfo["DistrictID"] = DistrictID;
            }
            else
            {
                SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
                if (_globalInfo.IsSuperVisingDeptRole) // So
                {
                    SearchInfo["DistrictID"] = DistrictID;
                }
                else
                {
                    SearchInfo["DistrictID"] = _globalInfo.DistrictID;
                }
            }
            SearchInfo["SchoolName"] = SchoolName;
            SearchInfo["SchoolCode"] = SchoolCode;
            SearchInfo["Administrator"] = Administrator;
            SearchInfo["IsActive"] = true;

            // Thuc hien truy van du lieu
            IQueryable<SchoolProfile> lst = this.SchoolProfileBusiness.Search(SearchInfo);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HT" + "/" + "DanhSachBanGiaoTaiKhoan.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange("A1", "G6"), 1, 1);
            if (lst.Count() > 0)
            {
                List<SchoolProfile> lstSchool = lst.OrderBy(o => o.SchoolName).ToList();
                int StartRow = 6;
                int StartCol = 1;
                for (int i = 0; i < lstSchool.Count; i++)
                {
                    SchoolProfile sp = lstSchool[i];
                    Sheet.SetCellValue(StartRow + i, StartCol, i + 1);
                    Sheet.SetCellValue(StartRow + i, StartCol + 1, sp.SchoolCode);
                    Sheet.SetCellValue(StartRow + i, StartCol + 2, sp.SchoolName);
                    if (sp.UserAccount != null && sp.UserAccount.aspnet_Users != null)
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 3, sp.UserAccount.aspnet_Users.UserName);
                    }
                    else
                    {
                        Sheet.SetCellValue(StartRow + i, StartCol + 3, "");
                    }
                    if (sp.UserAccount != null && sp.UserAccount.aspnet_Users != null)
                    {
                        MembershipUser objUser = Membership.GetUser(sp.UserAccount.aspnet_Users.UserName);
                        if (sp.UserAccount.FirstLoginDate != null)
                        {
                            Sheet.SetCellValue(StartRow + i, StartCol + 4, "********");
                        }
                        else
                        {
                            if (objUser != null && objUser.IsLockedOut == true)
                            {
                                Sheet.SetCellValue(StartRow + i, StartCol + 4, "Tài khản đã bị khóa");
                            }
                            else
                            {
                                Sheet.SetCellValue(StartRow + i, StartCol + 4, "********");
                            }
                        }
                    }
                    Sheet.SetCellValue(StartRow + i, StartCol + 5, sp.TrainingType.Resolution);
                    Sheet.SetCellValue(StartRow + i, StartCol + 6, sp.HeadMasterName);
                    Sheet.CopyPaste(Sheet.GetRange("A6", "G6"), StartRow + i, 1, true);
                }
            }
            oBook.GetSheet(3).Delete();
            oBook.GetSheet(1).Delete();
            Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            string ReportName = "DanhSachBanGiaoTaiKhoan.xls";
            result.FileDownloadName = ReportName;
            return result;
        }

        #endregion

        public JsonResult RemoveSS()
        {
            Session["SearchEducationGrade"] = null;
            Session["SearchProvince"] = null;
            Session["SearchDistrict"] = null;
            Session["SearchSchoolName"] = null;
            Session["SearchSchoolCode"] = null;
            Session["SearchAdministrator"] = null;
            return Json("");

        }
        private void SetSchoolCode(int AdminID)
        {
            IQueryable<SchoolProfile> lstsp = SchoolProfileBusiness.All.Where(o => o.AdminID == AdminID);
            if (lstsp.Count() > 0)
            {
                SchoolProfile sp = lstsp.OrderByDescending(o => o.SchoolProfileID).FirstOrDefault();
                //Lay theo ID truong vua tao
                sp.SchoolCode = sp.SchoolProfileID.ToString();
                //Neu ma truong da ton tai
                if (SchoolProfileBusiness.All.Where(o => o.SchoolCode == sp.SchoolCode && o.IsActive == true).Count() > 0)
                {
                    sp.SchoolCode = sp.SchoolProfileID.ToString() + sp.ProvinceID.ToString();
                    if (SchoolProfileBusiness.All.Where(o => o.SchoolCode == sp.SchoolCode && o.IsActive == true).Count() > 0)
                    {
                        sp.SchoolCode = sp.SchoolProfileID.ToString() + sp.ProvinceID.ToString() + sp.DistrictID.ToString();
                    }
                }
                SchoolProfileBusiness.Update(sp);
                SchoolProfileBusiness.Save();
            }
        }

        /// <summary>
        /// Prepare view data with school school id 
        /// </summary>
        /// <author>AnhVD9</author>
        /// <param name="SchoolID"></param>
        private void SetViewDataWithSchoolID(int SchoolID)
        {
            SchoolProfile SchoolProfile = this.SchoolProfileBusiness.Find(SchoolID);
            // Thông tin Quận/Huyện
            IQueryable<District> lstDistrict = this.DistrictBusiness.Search(new Dictionary<string, object>() {
                                        {   "ProvinceID", SchoolProfile.ProvinceID },
                                        {   "IsActive", true }});
            if (lstDistrict.Count() != 0)
            {
                ViewData[SchoolProfileConstants.LS_District_Create] = lstDistrict.OrderBy(o => o.DistrictName).ToList();
            }
            else
            {
                ViewData[SchoolProfileConstants.LS_District_Create] = new List<District>();
            }

            // Thông tin Xã/Phường
            ViewData[SchoolProfileConstants.LS_Commune] = this.CommuneBusiness.All.Where(p => p.DistrictID == SchoolProfile.DistrictID && p.IsActive == true)
                                        .OrderBy(o => o.CommuneName).ToList();

            // Hungnd 14/05/2013 Fix load SupervisingDept theo Province
            if (_globalInfo.IsSystemAdmin)
            {
                List<SupervisingDept> ListSuperVising = new List<SupervisingDept>();
                List<SupervisingDept> lsSupervising = SupervisingDeptBusiness.All.Where(o => o.ProvinceID == SchoolProfile.ProvinceID && o.HierachyLevel == 3)
                                                    .OrderBy(o => o.SupervisingDeptName).ToList();
                if (lsSupervising.Count > 0)
                {
                    foreach (SupervisingDept sd in lsSupervising)
                    {
                        ListSuperVising.Add(sd);
                        List<SupervisingDept> lsParentSupervising = SupervisingDeptBusiness.All.Where(o => o.ProvinceID == SchoolProfile.ProvinceID && o.HierachyLevel == 5 && o.ParentID == sd.SupervisingDeptID)
                                        .OrderBy(o => o.SupervisingDeptName).ToList();
                        if (lsParentSupervising.Count > 0)
                        {
                            foreach (SupervisingDept sdp in lsParentSupervising)
                            {
                                sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                                ListSuperVising.Add(sdp);
                            }
                        }
                    }
                }
                ViewData[SchoolProfileConstants.LS_SupervisingDept] = ListSuperVising;
            }
            //End Load

            // AnhVD 20140917 Loại trường
            List<SchoolType> lsSchoolType = SchoolTypeBusiness.Search(new Dictionary<string, object>(){
                                                { "SchoolProfileID", SchoolID },
                                                { "IsActive", true}
                                            }).ToList();
            //List<ViettelCheckboxList> lscheckbox = new List<ViettelCheckboxList>();
            //ViettelCheckboxList viettelchk;
            /* if (lsSchoolType != null)
             {
                 foreach (SchoolType st in lsSchoolType)
                 {
                     if (!_globalInfo.IsAdminSchoolRole)
                     {
                         if (st.SchoolTypeID == SchoolProfile.SchoolTypeID.Value)
                         {
                             viettelchk = new ViettelCheckboxList();
                             viettelchk.Label = st.Resolution;
                             viettelchk.cchecked = true;
                             viettelchk.disabled = false;
                             viettelchk.Value = st.SchoolTypeID;
                             lscheckbox.Add(viettelchk);
                         }
                         else
                         {
                             viettelchk = new ViettelCheckboxList();
                             viettelchk.Label = st.Resolution;
                             viettelchk.cchecked = false;
                             viettelchk.disabled = false;
                             viettelchk.Value = st.SchoolTypeID;
                             lscheckbox.Add(viettelchk);
                         }
                     }
                     else
                     {
                         if (st.SchoolTypeID == SchoolProfile.SchoolTypeID.Value)
                         {
                             viettelchk = new ViettelCheckboxList();
                             viettelchk.Label = st.Resolution;
                             viettelchk.cchecked = true;
                             viettelchk.disabled = false;
                             viettelchk.Value = st.SchoolTypeID;
                             lscheckbox.Add(viettelchk);
                         }
                         else
                         {
                             viettelchk = new ViettelCheckboxList();
                             viettelchk.Label = st.Resolution;
                             viettelchk.cchecked = false;
                             viettelchk.disabled = true;
                             viettelchk.Value = st.SchoolTypeID;
                             lscheckbox.Add(viettelchk);
                         }
                     }
                 }
             }*/

            if (_globalInfo.IsAdminSchoolRole)
            {
                lsSchoolType = lsSchoolType.Where(x => x.SchoolTypeID == SchoolProfile.SchoolTypeID.Value).ToList();
            }

            ViewData[SchoolProfileConstants.LS_SchoolType] = new SelectList(lsSchoolType, "SchoolTypeID", "Resolution", SchoolProfile.SchoolTypeID.Value);
            // End Loại trường

            // Cấp học theo Sở/Phòng
            List<ComboObject> lst = new List<ComboObject>();
            SupervisingDept super = SupervisingDeptBusiness.Find(SchoolProfile.SupervisingDeptID.Value);
            if (super != null)
            {
                if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) // Nếu là phòng
                {
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
                }
                else if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) // Sở
                {

                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));
                    //lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));

                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE.ToString(), Res.Get("Common_Label_SEG_CRECHE")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN.ToString(), Res.Get("Common_Label_SEG_KINDERGARTEN")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL.ToString(), Res.Get("Common_Label_SEG_PRESCHOOL")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY.ToString(), Res.Get("Common_Label_SEG_PRIMARY_SECONDARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY.ToString(), Res.Get("Common_Label_SEG_SECONDARY_TERTIARY")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL.ToString(), Res.Get("Common_Label_SEG_ALL")));
                    lst.Add(new ComboObject(SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS.ToString(), Res.Get("SchoolProfile_Label_BCIS")));
                }
            }
            ViewData[SchoolProfileConstants.LIST_GRADE] = lst;
            // End - Cấp học theo Sở/Phòng
        }
        public bool CheckPassRequire(string Password)
        {
            bool character = false;
            bool number = false;

            //kiem tra co kytu khong
            foreach (char ch in Password)
            {
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122))
                {
                    character = true;
                }
                if (ch >= 48 && ch <= 64)
                {
                    number = true;
                }
            }
            if (character && number)
                return true;
            return false;
        }

        public PartialViewResult PartalViewSchoolSub(int id)
        {
            SchoolProfileForm spf = new SchoolProfileForm();
            SetViewDataPermission("SchoolProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

            if (id <= 0)
            {
                return PartialView("_FormSchoolSubsidiary", spf);
            }

            SchoolSubsidiaryForm ssf = PrepareEditSubsi(id);
            this.CheckPermissionForAction(ssf.SchoolID, "schoolprofile", 4);
            GlobalInfo GlobalInfo = new GlobalInfo();

            if (GetMenupermission("SchoolProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            spf.Address = ssf.Address;
            spf.DistanceFromHQ = ssf.DistanceFromHQ.ToString().Replace(".", ",");
            spf.SchoolProfileID = ssf.SchoolID;
            spf.SchoolSubsidiaryID = id;
            spf.SquareArea = ssf.SquareArea.ToString().Replace(".", ",");
            spf.SubsidiaryCode = ssf.SubsidiaryCode;
            spf.SubsidiaryName = ssf.SubsidiaryName;
            spf.SchoolProfileID = ssf.SchoolID;
            spf.CommutedBy = ssf.CommutedBy;
            spf.txtTelephone = ssf.Telephone;
            return PartialView("_FormSchoolSubsidiary", spf);
        }

        public PartialViewResult PartalViewSearchAPI()
        {
            List<Province> lstProvince = ProvinceBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.ProvinceName).ToList();
            List<District> lstDistrict = new List<District>();

            if (!_globalInfo.IsSystemAdmin)
            {
                lstProvince = lstProvince.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
            }

            if (lstProvince.Count() > 0)
            {
                Province objProvince = lstProvince[0];
                lstDistrict = DistrictBusiness.All.Where(x => x.IsActive == true && x.ProvinceID == objProvince.ProvinceID).OrderBy(x => x.DistrictName).ToList();
            }

            ViewData["LIST_PROVINCE_API"] = lstProvince.Select(s => new SelectListItem { Value = s.ProvinceID.ToString(), Text = s.ProvinceName, Selected = false }).ToList();
            ViewData["LIST_DISTRICT_API"] = lstDistrict.Select(s => new SelectListItem { Value = s.DistrictID.ToString(), Text = s.DistrictName, Selected = false }).ToList();

            return PartialView("_APISearchSchoolProfile");
        }

        public PartialViewResult LoadGridSchoolAPI(int provinceId, int districtId, string schoolName, string schoolCode)
        {
            List<SchoolProfileAPI> result = this.GetSchoolInfo(provinceId, districtId, schoolName, schoolCode);
            ViewData["LIST_SCHOOL_API"] = result;
            return PartialView("_APIGridSchoolProfile");
        }

        private List<SchoolProfileAPI> GetSchoolInfo(int provinceId, int districtId, string schoolName, string schoolCode)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("IsSystemAdmin", _globalInfo.IsSystemAdmin);
            dic.Add("ProvinceID", !_globalInfo.IsSystemAdmin ? _globalInfo.ProvinceID : provinceId);
            dic.Add("DistrictID", !_globalInfo.IsSystemAdmin ? (_globalInfo.IsSubSuperVisingDeptRole ? _globalInfo.DistrictID : districtId) : districtId);
            dic.Add("SchoolName", schoolName);
            dic.Add("SchoolCode", schoolCode);
            List<SchoolProfileAPI> result = SchoolProfileBusiness.GetSchoolByAPI(dic);

            return result;
        }

        public JsonResult LoadDistrict(int provinceId)
        {
            if (provinceId <= 0)
                return Json(new List<SelectListItem>());

            List<District> lstDistrict = new List<District>();
            lstDistrict = DistrictBusiness.All.Where(x => x.IsActive == true && x.ProvinceID == provinceId).OrderBy(x => x.DistrictName).ToList();
            var lstResult = lstDistrict.Select(u => new SelectListItem { Value = u.DistrictID.ToString(), Text = u.DistrictName, Selected = false }).ToList();
            return Json(lstResult);
        }

        public JsonResult GetInfoSchoolBySyncCode(string syncCode)
        {
            int ProvinceID = 0; //Utils.GetInt(dic, "ProvinceID");
            int DistrictID = 0; //Utils.GetInt(dic, "DistrictID");
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolCode", syncCode);

            if (!_globalInfo.IsSystemAdmin)
            {
                if (_globalInfo.IsSuperVisingDeptRole) // Sở
                {
                    ProvinceID = _globalInfo.ProvinceID.Value;
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole) // Phòng
                {
                    ProvinceID = _globalInfo.ProvinceID.Value;
                    DistrictID = _globalInfo.DistrictID.Value;
                }
            }
            dic.Add("ProvinceID", ProvinceID);
            dic.Add("DistrictID", DistrictID);
            List<SchoolProfileAPI> result = SchoolProfileBusiness.GetSchoolByAPI(dic);
            SchoolProfileAPI objResult = null;

            if (result == null || (result.Count() == 0 || result.Count() > 1))
            {
                return Json(new { infoSchool = objResult }, JsonRequestBehavior.AllowGet);
            }
            //Thông tin trường
            objResult = result.FirstOrDefault();

            // Tỉnh/TP
            List<Province> lstProvince = ProvinceBusiness.All.Where(x => x.IsActive == true).OrderBy(x => x.ProvinceName).ToList();
            if (!_globalInfo.IsSystemAdmin)
            {
                lstProvince = lstProvince.Where(x => x.ProvinceID == _globalInfo.ProvinceID).ToList();
            }
            List<int> lstProvinceID = lstProvince.Select(x => x.ProvinceID).ToList();

            //Quận/Huyện
            List<District> lstDistrict = DistrictBusiness.All.Where(x => x.IsActive == true && x.ProvinceID.HasValue && lstProvinceID.Contains(x.ProvinceID.Value)).ToList();
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == _globalInfo.DistrictID.Value).ToList();
            }
            List<int> lstDistrictID = lstDistrict.Where(x => x.DistrictName.Equals(objResult.District)).Select(x => x.DistrictID).ToList();
            // Phường/Xã
            List<Commune> lstCommune = this.CommuneBusiness.All.Where(p => lstDistrictID.Contains(p.DistrictID.Value) && p.IsActive == true).OrderBy(o => o.CommuneName).ToList();
            // Loại trường
            List<SchoolType> lsSchoolType = SchoolTypeBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.Resolution).ToList();
            
            // Đơn vị quản lý
            //Province objProvince = lstProvince.Where(x => x.ProvinceName.Equals(objResult.Province)).FirstOrDefault();           
            //if (objProvince != null)
            //{
                /*IDictionary<string, object> dicSup = new Dictionary<string, object>();
                dicSup["ProvinceID"] = objProvince.ProvinceID;
                dicSup["IsActive"] = true;
                dicSup["DistrictID"] = _globalInfo.IsSubSuperVisingDeptRole ? _globalInfo.DistrictID : 0;
                
                IQueryable<SupervisingDept> lsSupervising = SupervisingDeptBusiness.Search(dicSup).Where(o => o.HierachyLevel == 3)
                                                                                                    .OrderBy(o => o.SupervisingDeptName);

                List<SupervisingDept> lstSuper = new List<SupervisingDept>();
                if (lsSupervising.Count() > 0)
                {
                    lstSuper = lsSupervising.ToList();

                    foreach (SupervisingDept sd in lstSuper)
                    {
                        lst.Add(sd);
                        IQueryable<SupervisingDept> lsParentSupervising = SupervisingDeptBusiness.Search(dicSup)
                           .Where(o => o.HierachyLevel == 5 && o.ParentID == sd.SupervisingDeptID).OrderBy(o => o.SupervisingDeptName);
                        if (lsParentSupervising.Count() > 0)
                        {
                            List<SupervisingDept> lstParentSuper = lsParentSupervising.ToList();

                            foreach (SupervisingDept sdp in lstParentSuper)
                            {
                                sdp.SupervisingDeptName = "..........." + sdp.SupervisingDeptName;
                                lst.Add(sdp);
                            }

                        }
                    }
                }*/
            //}

            List<SupervisingDept> listSupervisingDept = new List<SupervisingDept>();

            Province objProvince = lstProvince.Where(x => x.ProvinceName.Equals(objResult.Province)).FirstOrDefault();
            if (objProvince != null)
            {
                if (_globalInfo.IsSystemAdmin)
                {
                    #region Admin hệ thống
                    IQueryable<SupervisingDept> IqSupervising = SupervisingDeptBusiness.All.Where(o => o.HierachyLevel == 3 && o.IsActive == true)
                                                                                            .Where(x => x.ProvinceID == objProvince.ProvinceID)
                                                                                            .OrderBy(o => o.SupervisingDeptName);

                    List<SupervisingDept> lstSuper = new List<SupervisingDept>();
                    if (IqSupervising.Count() > 0)
                    {
                        foreach (SupervisingDept sd in IqSupervising)
                        {
                            listSupervisingDept.Add(sd);
                            IQueryable<SupervisingDept> IqChildDept = SupervisingDeptBusiness.All.Where(o => o.HierachyLevel == 5 
                                                                                                        && o.ParentID == sd.SupervisingDeptID 
                                                                                                        && o.IsActive == true)
                                                                                                    .Where(x => x.ProvinceID == objProvince.ProvinceID)
                                                                                                    .OrderBy(o => o.SupervisingDeptName);
                            if (IqChildDept.Count() > 0)
                            {
                                foreach (SupervisingDept child in IqChildDept)
                                {
                                    child.SupervisingDeptName = string.Format("...........{0}", child.SupervisingDeptName);
                                    listSupervisingDept.Add(child);
                                }
                            }
                        }
                    }
                    #endregion
                }
                else if (_globalInfo.IsSuperVisingDeptRole)
                {
                    #region SỞ
                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID.Value);
                    if (supervisingDept != null && supervisingDept.IsActive)
                    {

                        listSupervisingDept.Add(supervisingDept);
                        IQueryable<SupervisingDept> IqChild = SupervisingDeptBusiness.All
                                                                    .Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true 
                                                                        && o.HierachyLevel == 5 
                                                                        && o.ParentID == supervisingDept.SupervisingDeptID
                                                                        && o.ProvinceID == objProvince.ProvinceID).OrderBy(o => o.SupervisingDeptName);
                        if (IqChild.Count() > 0)
                        {
                            foreach (SupervisingDept child in IqChild)
                            {
                                child.SupervisingDeptName = string.Format("...........{0}", child.SupervisingDeptName);
                                listSupervisingDept.Add(child);
                            }
                        }
                    }
                    #endregion
                }
                else if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    #region Phòng
                    listSupervisingDept = SupervisingDeptBusiness.All.Where(o => o.SupervisingDeptID == _globalInfo.SupervisingDeptID && o.HierachyLevel == 5)
                                                                        .Where(x=>x.ProvinceID == objProvince.ProvinceID 
                                                                            && x.DistrictID == _globalInfo.DistrictID)
                                                                        .OrderBy(o => o.SupervisingDeptName).ToList();
                    #endregion
                }
            }

            // Loại hình đào tạo
            List<TrainingType> lstTrainingType = TrainingTypeBusiness.All.Where(o => o.IsActive == true).OrderBy(o => o.Resolution).ToList();

            var lsSchoolTypeItem = lsSchoolType.Select(u => new SelectListItem { Value = u.SchoolTypeID.ToString(), Text = u.Resolution, Selected = false }).ToList();
            var lstProvinceItem = lstProvince.Select(u => new SelectListItem { Value = u.ProvinceID.ToString(), Text = u.ProvinceName, Selected = false }).ToList();
            var lstDistrictItem = lstDistrict.Select(u => new SelectListItem { Value = u.DistrictID.ToString(), Text = u.DistrictName, Selected = false }).ToList();
            var lstCommuneItem = lstCommune.Select(u => new SelectListItem { Value = u.CommuneID.ToString(), Text = u.CommuneName, Selected = false }).ToList();
            var lstSuperItem = listSupervisingDept.Select(u => new SelectListItem { Value = u.SupervisingDeptID.ToString(), Text = u.SupervisingDeptName, Selected = false }).ToList();
            var lstTrainingTypeItem = lstTrainingType.Select(u => new SelectListItem { Value = u.TrainingTypeID.ToString(), Text = u.Resolution, Selected = false }).ToList();
            return Json(new
            {
                infoSchool = objResult,
                lstProvinceItem = lstProvinceItem,
                lstDistrictItem = lstDistrictItem,
                lstCommuneItem = lstCommuneItem,
                lstSuperItem = lstSuperItem,
                lsSchoolTypeItem = lsSchoolTypeItem,
                lstTrainingTypeItem = lstTrainingTypeItem
            }, JsonRequestBehavior.AllowGet);
        }

        #region // ExportExcel
        public PartialViewResult OpenDialogExcel()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE); // Mẫu của table SchoolProfile
            dic.Add("UnitID", _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID);
            dic.Add("checkUnit", _globalInfo.IsSystemAdmin ? "checkUnit" : "");

            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();
            lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });
            ViewData["LIST_PROFILE_TEMPLATE"] = new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", 0);
            return PartialView("_ListExcel");
        }

        [HttpPost]
        public PartialViewResult LoadTemplate(int id)
        {
            SetProfileTemplate(id);
            SetRowExcel();
            List<ColumnDescriptionViewModel> lstVal = GetColumnDescription(id);
            ViewData["LIST_COLUMN_TEMPLATE"] = lstVal;
            return PartialView("_ListTemplate");
        }

        private void SetProfileTemplate(int profileTemplateId)
        {
            int? unitId = _globalInfo.IsAdmin ? 0 : _globalInfo.SupervisingDeptID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("TemplateID", profileTemplateId);
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE); // Mẫu thông tin trường
            dic.Add("UnitID", _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID);
            dic.Add("checkUnit", _globalInfo.IsSystemAdmin ? "checkUnit" : "");

            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();
            if (profileTemplateId == 0)
            {
                if (lstProfileTemplate == null)
                {
                    lstProfileTemplate = new List<ProfileTemplate>();
                }
                lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });
            }

            int defaultId = 0;
            ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == 0 && s.UnitID == unitId);
            if (objProfileTemp != null)
            {
                defaultId = objProfileTemp.ProfileTemplateId;
            }
            else
            {
                objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                if (objProfileTemp != null)
                {
                    defaultId = objProfileTemp.ProfileTemplateId;
                }
            }

            ViewData["LIST_PROFILE_TEMPLATE"] = new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId);
        }

        private void SetRowExcel()
        {
            List<RowExcelViewModel> lstRow = new List<RowExcelViewModel>();
            for (int i = 1; i <= 20; i++)
            {
                lstRow.Add(new RowExcelViewModel { RowId = i, RowName = i.ToString() });
            }

            ViewData["LIST_ROW_EXCEL"] = new SelectList(lstRow, "RowId", "RowName", 1);
        }

        private List<ColumnDescriptionViewModel> GetColumnDescription(int profileTemplateId)
        {
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(profileTemplateId);

            List<SchoolColumnTemplateViewModel> lstTemplate = new List<SchoolColumnTemplateViewModel>();

            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SchoolColumnTemplateViewModel>>(profileTemplate.ColDescriptionIds);
            }

            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All.Where(x => x.ReportType == 2).OrderBy(c => c.ColumnIndex).ToList();


            List<ColumnDescriptionViewModel> lstColumnModel = new List<ColumnDescriptionViewModel>();
            VTExport export = new VTExport();
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {
                ColumnDescription column = lstColumnDescription[i];
                ColumnDescriptionViewModel objColumnModel = new ColumnDescriptionViewModel
                {
                    ColumnDescriptionId = column.ColumnDescriptionId,
                    ColumnName = column.ColumnName,
                    RequiredColumn = column.RequiredColumn,
                    Resolution = column.Resolution,
                };
                SchoolColumnTemplateViewModel employeeColumnTemplateBO = lstTemplate.FirstOrDefault(s => s.ColDesId == column.ColumnDescriptionId);
                if (employeeColumnTemplateBO != null)
                {
                    VTVector vTVector = new VTVector(employeeColumnTemplateBO.ColExcel + "1");
                    objColumnModel.ExcelName = employeeColumnTemplateBO.ExcelName;
                    objColumnModel.ColExcel = employeeColumnTemplateBO.ColExcel;
                    objColumnModel.CheckValue = true;
                    objColumnModel.ColumnIndex = vTVector.Y;
                }
                else
                {
                    string columnExcel = VTVector.ColumnIntToString(column.ColumnIndex);
                    objColumnModel.ColExcel = columnExcel;
                    objColumnModel.ExcelName = column.Resolution;
                    objColumnModel.CheckValue = false;
                    objColumnModel.ColumnIndex = column.ColumnIndex;

                }
                lstColumnModel.Add(objColumnModel);
            }
            lstColumnModel = lstColumnModel.OrderBy(s => s.ColumnIndex).ToList();
            return lstColumnModel;

        }

        public JsonResult ReLoadTemplate()
        {
            int? unitId = _globalInfo.IsAdmin ? 0 : _globalInfo.SupervisingDeptID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE);
            dic.Add("UnitID", _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID);
            dic.Add("checkUnit", _globalInfo.IsSystemAdmin ? "checkUnit" : "");

            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();

            if (lstProfileTemplate == null)
            {
                lstProfileTemplate = new List<ProfileTemplate>();
            }

            lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });

            int defaultId = 0;
            ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == 0 && s.UnitID == unitId);
            if (objProfileTemp != null)
            {
                defaultId = objProfileTemp.ProfileTemplateId;
            }
            else
            {
                objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                if (objProfileTemp != null)
                {
                    defaultId = objProfileTemp.ProfileTemplateId;
                }
            }

            return Json(new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveExcelTemplate(FormCollection frm)
        {
            int templapteId = Convert.ToInt32(frm["ProfileTemplateId"]);
            string templateName = frm["ProfileTemplateName"];
            if (string.IsNullOrEmpty(templateName))
            {

                return Json(new JsonMessage("Thầy/cô chưa nhập tên mẫu", JsonMessage.ERROR));
            }

            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templapteId);

            int? unitID = _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID;

            bool existstName = ProfileTemplateBusiness.All.Any(s => s.ProfileTemplateName.ToLower().Contains(templateName.ToLower()) && s.ReportType == 2 && s.UnitID == unitID);
            if (existstName && templapteId == 0)
            {
                return Json(new JsonMessage("Tên mẫu đã được khai báo", JsonMessage.ERROR));
            }

            if (profileTemplate != null && profileTemplate.SchoolId == 0 && !profileTemplate.UnitID.HasValue)
            {
                return Json(new JsonMessage("Thầy/cô không được sửa mẫu của hệ thống", JsonMessage.ERROR));
            }

            if (profileTemplate != null && profileTemplate.SchoolId == 0 && 
                profileTemplate.ReportType != SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE)
            {
                return Json(new JsonMessage("Mẫu excel không hợp lệ", JsonMessage.ERROR));
            }

            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All
                                                            .Where(x => x.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE)
                                                            .OrderBy(s => s.ColumnIndex).ToList();

           
            List<string> lstError = new List<string>();
            List<EmployeeColumnTemplateBO> listTemplateBO = new List<EmployeeColumnTemplateBO>();
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {
                ColumnDescription columnDescription = lstColumnDescription[i];
                string chk = frm["chk_" + columnDescription.ColumnDescriptionId];
                if ((chk == null || chk != "on") && !columnDescription.RequiredColumn)
                {
                    continue;
                }
                string excelName = frm["ExcelName_" + columnDescription.ColumnDescriptionId];
                string colExcel = frm["ColExcel_" + columnDescription.ColumnDescriptionId];

                EmployeeColumnTemplateBO objTemplateBO = new EmployeeColumnTemplateBO
                {
                    ColDesId = columnDescription.ColumnDescriptionId,
                    ColExcel = colExcel,
                    ExcelName = excelName
                };

                if (string.IsNullOrEmpty(colExcel))
                {
                    lstError.Add("Chưa nhập cột excel");
                    continue;
                }

                if (string.IsNullOrEmpty(excelName))
                {
                    lstError.Add("Chưa nhập tên cột excel");
                    continue;
                }

                VTVector vTVector = new VTVector(objTemplateBO.ColExcel + "1");
                if (vTVector.X == 0)
                {
                    lstError.Add(String.Format("Tên cột excel {0} không hợp lệ", objTemplateBO.ColExcel));
                    continue;
                }
                if (listTemplateBO.Any(s => s.ColExcel == objTemplateBO.ColExcel))
                {
                    lstError.Add(String.Format("Tên cột excel {0} đã được sử dụng", objTemplateBO.ColExcel));
                    continue;
                }

                listTemplateBO.Add(objTemplateBO);
            }

            //Validate cot excel;
            if (lstError.Count > 0)
            {
                string msg = string.Join(",", lstError);
                return Json(new JsonMessage(msg, JsonMessage.ERROR));
            }

            if (listTemplateBO.Count() == 0)
            {
                return Json(new JsonMessage("Thầy/cô chưa chọn thông tin báo cáo", JsonMessage.ERROR));
            }

            if (templapteId > 0)
            {
                profileTemplate.ProfileTemplateName = templateName;
                profileTemplate.ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO);
                profileTemplate.ModifiedDate = DateTime.Now;
                profileTemplate.SchoolId = 0;
                profileTemplate.ReportType = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE;
                profileTemplate.UnitID = _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID;
                ProfileTemplateBusiness.Update(profileTemplate);
                ProfileTemplateBusiness.Save();
                return Json(new JsonMessage(Res.Get("Cập nhật thành công")));
            }
            else
            {
                ProfileTemplate profileTemplateInsert = new ProfileTemplate
                {
                    ProfileTemplateName = templateName,
                    SchoolId = 0,
                    ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO),
                    CreatedDate = DateTime.Now,
                    ReportType = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE,
                    UnitID = _globalInfo.IsSystemAdmin ? 0 : _globalInfo.SupervisingDeptID
                };

                ProfileTemplateBusiness.Insert(profileTemplateInsert);
                ProfileTemplateBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }

        [HttpPost]
        public JsonResult DeleteTemplate(int profileTemplateId)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            ProfileTemplate profile = ProfileTemplateBusiness.Find(profileTemplateId);
            if (profile == null)
            {
                return Json(new JsonMessage("Mẫu excel không hợp lệ", JsonMessage.ERROR));
            }

            if (profile.SchoolId == 0 && !profile.UnitID.HasValue)
            {
                return Json(new JsonMessage("Thầy/cô không xóa được mẫu excel của hệ thống", JsonMessage.ERROR));
            }

            if (profile.ReportType != SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_SCHOOL_PROFILE)
            {
                return Json(new JsonMessage("Mẫu excel không phải của hồ sơ trường", JsonMessage.ERROR));
            }

            ProfileTemplateBusiness.Delete(profileTemplateId);
            ProfileTemplateBusiness.Save();
            return Json(new JsonMessage(Res.Get("Xóa mẫu excel thành công")));
        }

        public FileResult ExportExcelInfoSchool(int? provinceId, int? districtId, int profileTemplateId, int rowId)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ProvinceID", _globalInfo.IsSystemAdmin ? provinceId : _globalInfo.ProvinceID);
            dic.Add("DistrictID", _globalInfo.IsSystemAdmin ? districtId : (_globalInfo.IsSuperVisingDeptRole ? districtId : _globalInfo.DistrictID));
            dic.Add("TemplateID", profileTemplateId);
            dic.Add("StartRow", rowId);

            Stream excel = ProfileTemplateBusiness.ExportInfoSchool(dic);
            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            result.FileDownloadName = TEMPLATE_SCHOOL_INFO; 
            return result;
        }
        #endregion
    }
}
