﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.SchoolProfileArea
{
    public class SchoolProfileDetailViewedBySupervisingDept
    {
        const string GLOBAL_KEY = "SchoolProfileDetailViewedBySupervisingDept";
        public static SchoolProfileDetailViewedBySupervisingDept getGlobalInstanceFromSession()
        {
            SchoolProfileDetailViewedBySupervisingDept schoolInfo = SessionObject.Get<SchoolProfileDetailViewedBySupervisingDept>(GLOBAL_KEY);
            return schoolInfo;
        }

        public static void setGlobalInstanceToSession(SchoolProfileDetailViewedBySupervisingDept schoolInfo)
        {
            SessionObject.Set<SchoolProfileDetailViewedBySupervisingDept>(GLOBAL_KEY, schoolInfo);
        }

        public static SchoolProfileDetailViewedBySupervisingDept getPersistentInstanceFromSession(string key)
        {
            SchoolProfileDetailViewedBySupervisingDept schoolInfo = SessionObject.Get<SchoolProfileDetailViewedBySupervisingDept>(key);
            return schoolInfo;
        }

        public static void setPersistentInstanceToSession(string key, SchoolProfileDetailViewedBySupervisingDept schoolInfo)
        {
            SessionObject.Set<SchoolProfileDetailViewedBySupervisingDept>(key, schoolInfo);
        }

        public SchoolProfileDetailViewedBySupervisingDept()
        {
        }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int AppliedLevel { get; set; }

    }

    public class BackupGlobalInfo
    {

        public BackupGlobalInfo()
        {
        }

        public int? SchoolID { get; set; }

        public string SchoolName { get; set; }

        public int? AcademicYearID { get; set; }

        public int? AppliedLevel { get; set; }

        public List<SMAS.Models.Models.EducationLevel> EducationLevels { get; set; }

        public bool IsAdminSchoolRole { get; set; }

        public bool IsViewAll { get; set; }

        public SMAS.Models.Models.UserAccount UserAccount { get; set; }

        public int UserAccountID { get; set; }

        public bool IsCurrentYear { get; set; }

        public int? Semester { get; set; }
    }
}