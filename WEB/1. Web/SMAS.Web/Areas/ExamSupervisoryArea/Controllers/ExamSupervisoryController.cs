﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Business.Common;
using SMAS.Web.Controllers;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamSupervisoryArea.Models;

namespace SMAS.Web.Areas.ExamSupervisoryArea.Controllers
{
    public class ExamSupervisoryController : BaseController
    {
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IExamSupervisoryBusiness ExamSupervisoryBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;

        public ExamSupervisoryController(IExaminationsBusiness ExaminationsBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IExamSupervisoryBusiness ExamSupervisoryBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ExamSupervisoryBusiness = ExamSupervisoryBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        private List<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ExaminationsID", ExaminationsID},
                };

            List<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).ToList().OrderBy(p => p.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                IEnumerable<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        public void SetViewData()
        {
            ViewData["Status"] = new SelectList(CommonList.AssignmentStatus(), "key", "value");
            SetViewDataPermission("ExamSupervisoryArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExamSupervisoryConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamSupervisoryConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);
            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamSupervisoryConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            ViewData["PupilAutoComplete"] = new List<PupilAutoComplete>();
            search["AppliedLevel"] = 0;
            if (listExam.Count > 0)
            {
                Examinations exam = listExam.FirstOrDefault();
                List<ExamGroup> examGroupList = GetExamGroupFromExaminations(listExam.FirstOrDefault().ExaminationsID);
                search.Add("ExaminationsID", exam.ExaminationsID);
                search.Add("EmployeeFullName", string.Empty);
                List<ExamSupervisoryViewModel> listSupervisory = this._Search(search);
                ViewData[ExamSupervisoryConstants.LIST_EXAM_SUPERVISORY] = listSupervisory;

                if (examGroupList != null && examGroupList.Count() > 0)
                {
                    ViewData[ExamSupervisoryConstants.LIST_EXAMGROUP] = new SelectList(examGroupList, "ExamGroupID", "ExamGroupName");
                }

                if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
                {
                    ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = false;
                }
            }
            else
            {
                List<ExamSupervisoryViewModel> listSupervisory = new List<ExamSupervisoryViewModel>();
                ViewData[ExamSupervisoryConstants.LIST_EXAMGROUP] = new SelectList(new string[] { });
                ViewData[ExamSupervisoryConstants.LIST_EXAM_SUPERVISORY] = listSupervisory;
                ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = false;
                search.Add("ExaminationsID", 0);
            }

            search.Add("SearchInfoText", string.Empty);
            List<ExamSupervisoryViewModel> listEmployee = this._SearchEmployee(search);
            ViewData[ExamSupervisoryConstants.LIST_EMPLOYEE] = listEmployee;
        }

        #endregion SetViewData

        #region Search

        
        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData["Status"] = new SelectList(CommonList.AssignmentStatus(), "key", "value");
            SetViewDataPermission("ExamSupervisoryArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            ViewData[ExamSupervisoryConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            Examinations exam = ExaminationsBusiness.Find(form.Examinations);
            if (_globalInfo.IsCurrentYear == true && exam != null && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
            {
                ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = true;
            }
            else
            {
                ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = false;
            }

            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID);
            search.Add("ExaminationsID", form.Examinations);
            search.Add("EmployeeFullName", form.EmployeeFullName);
            search.Add("ExamGroupID", form.ExamGroup);
            search.Add("SchoolID", _globalInfo.SchoolID);

            List<ExamSupervisoryViewModel> list = this._Search(search);
            ViewData[ExamSupervisoryConstants.LIST_EXAM_SUPERVISORY] = list;

            return PartialView("_List");
        }

        private List<ExamSupervisoryViewModel> _Search(IDictionary<string, object> search)
        {
            List<ExamSupervisoryBO> listExam = ExamSupervisoryBusiness.GetListExamSupervisoryDev(search);
            List<ExamSupervisoryViewModel> listExamSupervisory = listExam.Select(o => new ExamSupervisoryViewModel
                                                                {
                                                                    ExamSupervisoryID = o.ExamSupervisoryID,
                                                                    ExaminationsID = o.ExaminationsID,
                                                                    ExamGroupID = o.ExamGroupID,
                                                                    ExamGroupName = o.ExamGroupName,
                                                                    AcademicYearID = o.AcademicYearID,
                                                                    TeacherID = o.TeacherID,
                                                                    TeacherName = o.TeacherName,
                                                                    CreateTime = o.CreateTime,
                                                                    UpdateTime = o.UpdateTime,
                                                                    EmployeeCode = o.EmployeeCode,
                                                                    FullName = o.FullName,
                                                                    Birthday = (o.Birthday.HasValue) ? o.Birthday.Value.ToString("dd/MM/yyyy") : "",
                                                                    Genre = o.Genre,
                                                                    FacultyID = o.FacultyID,
                                                                    FacultyName = o.FacultyName,
                                                                    Telephone = o.Telephone,
                                                                    IsDelete = o.IsDelete

                                                                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.TeacherName + " " + o.FullName)).ToList();

            return listExamSupervisory;
        }

        public ActionResult LoadTeacher(long examinationsID)
        {
            ViewData["Status"] = new SelectList(CommonList.AssignmentStatus(), "key", "value");
            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("ExaminationsID", examinationsID);
            search.Add("AppliLevelID", _globalInfo.AppliedLevel);

            List<ExamSupervisoryViewModel> list = this._SearchEmployee(search);
            ViewData[ExamSupervisoryConstants.LIST_EMPLOYEE] = list;
            ViewData["examGroupComponent"] = new List<ExamGroup>();
            ViewData[ExamSupervisoryConstants.LIST_EXAM_SUPERVISORY] = new List<ExamSupervisoryViewModel>();
            List<PupilAutoComplete> lstTeacher = new List<PupilAutoComplete>();
            PupilAutoComplete objTeacher = new PupilAutoComplete();
            List<ExamSupervisoryViewModel> lstE = new List<ExamSupervisoryViewModel>();

            List<SchoolFaculty> lstSf = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()).Where(x => x.IsActive == true).ToList();

            lstE = (from e in list
                    join sf in lstSf on e.FacultyID equals sf.SchoolFacultyID
                    select e).ToList();

            List<PupilAutoComplete> query = (from q in lstE
                                             select new PupilAutoComplete
                                             {
                                                 EmployeeID = q.TeacherID,
                                                 DisplayName = q.FullName + " - " + q.EmployeeCode + " - " + q.FacultyName
                                             }).ToList();
            lstTeacher.AddRange(query);
            ViewData["PupilAutoComplete"] = lstTeacher;

            return PartialView("_PupilAutoComplete");
        }

        public PartialViewResult SearchEmployee(string SearchInfoText, long? ExaminationsID, int status)
        {
            ViewData["Status"] = new SelectList(CommonList.AssignmentStatus(), "key", "value");
            if (SearchInfoText.CompareTo(Res.Get("ExamSupervisory_Value_SearchInfo")) == 0)
            {
                SearchInfoText = string.Empty;
            }
            ViewData[ExamSupervisoryConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            if (ExaminationsID.HasValue)
            {
                Examinations exam = ExaminationsBusiness.Find(ExaminationsID);
                if (_globalInfo.IsCurrentYear == true && exam.MarkInput.HasValue && exam.MarkInput.Value == false && exam.MarkClosing.HasValue && exam.MarkClosing.Value == false)
                {
                    ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = true;
                }
                else
                {
                    ViewData[ExamSupervisoryConstants.BTN_COMMON_ENABLE] = false;
                }
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("SchoolID", _globalInfo.SchoolID.Value);
                search.Add("ExaminationsID", ExaminationsID);
                List<ExamSupervisoryViewModel> listSupervisory = this._Search(search);
                ViewData[ExamSupervisoryConstants.LIST_EXAM_SUPERVISORY] = listSupervisory;

                search = new Dictionary<string, object>();
                search.Add("SchoolID", _globalInfo.SchoolID.Value);
                search.Add("SearchInfoText", SearchInfoText);
                search.Add("Status", status);
                search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
                search.Add("ExaminationsID", ExaminationsID.Value);
                search.Add("AppliLevelID", _globalInfo.AppliedLevel);

                List<ExamSupervisoryViewModel> list = this._SearchEmployee(search);
                ViewData[ExamSupervisoryConstants.LIST_EMPLOYEE] = list;


            }
            return PartialView("_ListInsert");
        }

        private List<ExamSupervisoryViewModel> _SearchEmployee(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamSupervisoryArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            List<ExamSupervisoryBO> listEmployee = ExamSupervisoryBusiness.GetListEmployee(search);

            long ExaminationsID = SMAS.Business.Common.Utils.GetLong(search["ExaminationsID"]);
            List<ExamGroup> examGroupList = GetExamGroupFromExaminations(ExaminationsID);
            if (examGroupList != null && examGroupList.Count() > 0)
            {
                ViewData["examGroupComponent"] = examGroupList;
            }
            else 
            {
                ViewData["examGroupComponent"] = new List<ExamGroup>();
            }
            List<ExamSupervisoryViewModel> listEmployeeInsert = listEmployee.Select(o => new ExamSupervisoryViewModel
                {
                    TeacherID = o.TeacherID,
                    EmployeeCode = o.EmployeeCode,
                    TeacherName = o.TeacherName,
                    FullName = o.FullName,
                    Birthday = (o.Birthday.HasValue) ? o.Birthday.Value.ToString("dd/MM/yyyy") : "",
                    Genre = o.Genre,
                    Telephone = o.Telephone,
                    FacultyID = o.FacultyID,
                    FacultyName = o.FacultyName,
                    ExamGroupID = o.ExamGroupID,
                    IsDelete = o.IsDelete
                }).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.TeacherName + " " + o.FullName)).ToList();

            return listEmployeeInsert;
        }

        #endregion Search

        #region Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(long? HiddenExaminationsID, int[] CheckEmployee, FormCollection frm)
        {
            if (GetMenupermission("ExamSupervisoryArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }           
            if (HiddenExaminationsID.HasValue && CheckEmployee != null)
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("AcademicYearID", _globalInfo.AcademicYearID);
                search.Add("ExaminationsID", HiddenExaminationsID);
                search.Add("SchoolID", _globalInfo.SchoolID);
                List<ExamSupervisoryViewModel> lstESDB = this._Search(search);
                ExamSupervisoryViewModel objESDB = new ExamSupervisoryViewModel();
                int examGroupID, examGroupIDNew;

                Examinations exam = ExaminationsBusiness.Find(HiddenExaminationsID.Value);
                if (exam != null && exam.MarkInput == false && exam.MarkClosing == false)
                {
                    List<ExamSupervisory> listSupervisory = new List<ExamSupervisory>();
                    ExamSupervisory supervisory = null;
                    List<ExamSupervisory> listSupervisoryUpdate = new List<ExamSupervisory>();
                    ExamSupervisory supervisoryUpdate = null;
                    for (int i = 0; i < CheckEmployee.Length; i++)
                    {
                        //ExamGroup cũ
                        examGroupID = SMAS.Business.Common.Utils.GetLong(frm["examGroupID" + CheckEmployee[i]]);
                        examGroupIDNew = SMAS.Business.Common.Utils.GetLong(frm["AppliedType" + CheckEmployee[i]]);
                        objESDB = lstESDB.Where(x => x.TeacherID == CheckEmployee[i] && x.ExamGroupID == examGroupID).FirstOrDefault();
                        if (objESDB == null)
                        {
                            supervisory = new ExamSupervisory();
                            supervisory.ExamSupervisoryID = ExamSupervisoryBusiness.GetNextSeq<long>();
                            supervisory.ExaminationsID = HiddenExaminationsID.Value;
                            supervisory.ExamGroupID = examGroupIDNew;
                            supervisory.TeacherID = CheckEmployee[i];
                            supervisory.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            supervisory.CreateTime = DateTime.Now;
                            listSupervisory.Add(supervisory);
                        }
                        else 
                        {
                            supervisoryUpdate = new ExamSupervisory();
                            supervisoryUpdate.ExamSupervisoryID = objESDB.ExamSupervisoryID;
                            supervisoryUpdate.ExaminationsID = objESDB.ExaminationsID;
                            supervisoryUpdate.AcademicYearID = objESDB.AcademicYearID;
                            supervisoryUpdate.TeacherID = objESDB.TeacherID;
                            supervisoryUpdate.CreateTime = objESDB.CreateTime;
                            supervisoryUpdate.ExamGroupID = examGroupIDNew;
                            supervisoryUpdate.UpdateTime = DateTime.Now;
                            listSupervisoryUpdate.Add(supervisoryUpdate);
                        }                       
                    }
                    ExamSupervisoryBusiness.UpdateListExamSupervisory(listSupervisoryUpdate);
                    ExamSupervisoryBusiness.InsertListExamSupervisory(listSupervisory);
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ExamPupilViolate_Error_Examinations"), "error"));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Thầy/cô chưa chọn giám thị"), "error"));
            }
        }

        #endregion Create

        #region Delete

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(string stringExamSupervisory)
        {
            if (GetMenupermission("ExamSupervisoryArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            string[] listDelete = stringExamSupervisory.Split(',');
            List<long> listExamSupervisory = new List<long>();
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                listExamSupervisory.Add(long.Parse(listDelete[i]));
            }
            ExamSupervisoryBusiness.DeleteExamSupervisory(listExamSupervisory);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete
    }
}
