﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryArea
{
    public class ExamSupervisoryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamSupervisoryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamSupervisoryArea_default",
                "ExamSupervisoryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
