﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryArea.Models
{
    public class ExamSupervisoryViewModel
    {
        public long ExamSupervisoryID { get; set; }
        public long ExaminationsID { get; set; }
        public long? ExamGroupID { get; set; }

        [DisplayName("Nhóm thi")]
        public string ExamGroupName { get; set; }
        public int AcademicYearID { get; set; }

        public int TeacherID { get; set; }
        public string TeacherName { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_TeacherID")]
        public string EmployeeCode { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_Birthday")]
        public string Birthday { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_Genre")]
        public bool? Genre { get; set; }

        public int FacultyID { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_FacultyName")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("ExamSupervisory_Column_Telephone")]
        public string Telephone { get; set; }

        public bool IsDelete { get; set; }

    }
}