﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamSupervisoryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamSupervisoryConstants.LIST_EXAMGROUP)]
        [AdditionalMetadata("Placeholder", "All")]
        public long? ExamGroup { get; set; }

        [ResourceDisplayName("ExamSupervisory_Label_EmployeeFullName")]
        [UIHint("Textbox")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EmployeeFullName { get; set; }
    }
}