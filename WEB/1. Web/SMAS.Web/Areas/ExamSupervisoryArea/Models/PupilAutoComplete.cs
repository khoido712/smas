﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryArea.Models
{
    public class PupilAutoComplete
    {
        public int EmployeeID { get; set; }
        public string DisplayName { get; set; }
    }
}