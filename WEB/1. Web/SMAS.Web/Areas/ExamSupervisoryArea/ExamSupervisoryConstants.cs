﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamSupervisoryArea
{
    public class ExamSupervisoryConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";

        public const string BTN_COMMON_ENABLE = "BtnCommonEnable";

        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string LIST_EXAM_SUPERVISORY = "ListExamSupervisory";

        public const string LIST_EMPLOYEE = "ListEmployee";

        public const string LIST_EXAMGROUP = "ListExamGroup";
    }
}