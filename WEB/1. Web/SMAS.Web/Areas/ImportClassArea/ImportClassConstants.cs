﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ImportClassArea
{
    public class ImportClassConstants
    {
        public const string LIST_CLASSPROFILE = "listClassProfile";
        public const string SHOW_MESSAGE_ERROR = "SHOW_MESSAGE_ERROR";
        public const string INVISIBLE_SUBCOMMITTEE = "INVISIBLE_SUBCOMMITTEE";
        public const string DISABLE_BTNSAVE = "DISABLE_BTNSAVE";
        public const string STRING_SECTION_MORNING = "Sáng";
        public const string STRING_SECTION_AFTERNOON = "Chiều";
        public const string STRING_SECTION_MORNING_AFTERNOON = "Sáng - Chiều";
        public const string STRING_SECTION_EVENING = "Tối";
        public const string STRING_SECTION_MORNING_EVENING = "Sáng - Tối";
        public const string STRING_SECTION_AFTERNOON_EVENING = "Chiều - Tối";
        public const string STRING_SECTION_ALL = "Sáng - Chiều - Tối";

        public const int SECTION_MORNING = 1;
        public const int SECTION_AFTERNOON = 2;
        public const int SECTION_MORNING_AFTERNOON = 4;
        public const int SECTION_EVENING = 3;
        public const int SECTION_MORNING_EVENING = 5;
        public const int SECTION_AFTERNOON_EVENING = 6;
        public const int SECTION_ALL = 7;
    }
}