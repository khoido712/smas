﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportClassArea
{
    public class ImportClassAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportClassArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportClassArea_default",
                "ImportClassArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
