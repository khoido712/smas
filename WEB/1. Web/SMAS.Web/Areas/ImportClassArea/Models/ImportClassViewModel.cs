﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ImportClassArea.Models
{
    public class ImportClassViewModel
    {
		public System.Int32 ClassProfileID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }								
		public System.Int32 SchoolID { get; set; }																
		public System.Int32 SubCommitteeID { get; set; }
        [ResourceDisplayName("ImportClass_Label_SubCommitteeName")]
        public System.String SubCommitteeName { get; set; }						
		public System.Int32 HeadTeacherID { get; set; }
        [ResourceDisplayName("ImportClass_Label_HeadTeacherName")]
        public System.String HeadTeacherName { get; set; }						
		public System.Int32 EducationLevelID { get; set; }
        [ResourceDisplayName("ImportClass_Label_EducationLevelName")]
        public System.String EducationLevelName { get; set; }
        [ResourceDisplayName("ImportClass_Label_DisplayName")]
		public System.String DisplayName { get; set; }
        public System.Int32 FacultyID { get; set; }		
        public System.String FacultyName { get; set; }
        [ResourceDisplayName("ImportClass_Label_Description")]
        public System.String Description { get; set; }
        public System.Double STT { get; set; }
        [ResourceDisplayName("ImportClass_Label_SchoolFalcutyName")]
        public System.String SchoolFalcutyName { get; set; }
        //Start: 2014/11/24 viethd4 Bổ sung buổi học
         [ResourceDisplayName("ImportClass_Label_Section")]
        public System.String Section { get; set; }
        [ResourceDisplayName("ImportClass_Label_SectionKey")]
         public System.String SectionKey { get; set; }
        //End: 2014/11/24 viethd4 Bổ sung buổi học
        public bool? Pass { get; set; }	
													       
    }
}


