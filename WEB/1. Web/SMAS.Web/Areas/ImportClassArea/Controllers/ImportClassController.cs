﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh 28/12/2012
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ImportClassArea.Models;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Transactions;
using System.Text;

namespace SMAS.Web.Areas.ImportClassArea.Controllers
{
    public class ImportClassController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly ITeacherOfFacultyBusiness TeacherOfFacultyBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;

        public ImportClassController(ITeacherOfFacultyBusiness TeacherOfFacultyBusiness,
                                        IAcademicYearBusiness AcademicYearBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IReportDefinitionBusiness reportdefinitionBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IClassProfileBusiness classprofileBusiness,
            ISubCommitteeBusiness SubCommitteeBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IEmployeeBusiness EmployeeBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IClassSubjectBusiness ClassSubjectBusiness)
        {
            this.ClassProfileBusiness = classprofileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ReportDefinitionBusiness = reportdefinitionBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TeacherOfFacultyBusiness = TeacherOfFacultyBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
        }

        public FileResult DownloadFileTemplate()
        {
            string ReportName = string.Empty;
            Stream excel = this.SetDataFileTemplate(ref ReportName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            result.FileDownloadName = ReportName;
            return result;
        }

        //[ValidateAntiForgeryToken]
        private bool ImportClass(List<ClassProfile> lstClassProfile)
        {
            IQueryable<ClassProfile> iqClassInCurrent = ClassProfileBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.AcademicYearID == _globalInfo.AcademicYearID && o.IsActive.Value);
            List<string> lstClassName = iqClassInCurrent.Select(o => o.DisplayName).ToList();
            int Count = lstClassProfile.Count;
            // Danh sach phai co ten lop khac nhau    
            if (Count > 0)
            {
                ClassProfile cpImport;
                ClassProfile cp;
                for (int i = 0; i < Count; i++)
                {
                    cpImport = lstClassProfile[i];
                    cpImport.IsActive = true;
                    //Gán loại lớp cho cấp GDTX
                    var school = SchoolProfileBusiness.Find(_globalInfo.SchoolID != null ? _globalInfo.SchoolID.Value : 0);
                    if (school != null && school.TrainingTypeID.Value == 3)
                    {
                        var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                        var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                        var listEduTerrary = new List<int>() { 10, 11, 12 };
                        if (listEduLvPrimary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 1;
                        }
                        else if (listEduSecondary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 2;
                        }
                        else if (listEduTerrary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 3;
                        }
                    }
                    if (lstClassName.Contains(cpImport.DisplayName))
                    {
                        cp = iqClassInCurrent.FirstOrDefault(o => cpImport.DisplayName.Equals(o.DisplayName));
                        cp.EducationLevelID = cpImport.EducationLevelID;
                        cp.SubCommitteeID = cpImport.SubCommitteeID;
                        cp.HeadTeacherID = cpImport.HeadTeacherID;
                        cp.Section = cpImport.Section;
                        cp.SeperateKey = cpImport.SeperateKey;
                        cp.IsActive = true;
                        if (school != null && school.TrainingTypeID.Value == 3)
                        {
                            var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                            var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                            var listEduTerrary = new List<int>() { 10, 11, 12 };
                            if (listEduLvPrimary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 1;
                            }
                            else if (listEduSecondary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 2;
                            }
                            else if (listEduTerrary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 3;
                            }
                        }
                        ClassProfileBusiness.Update(cp);
                    }
                    else ClassProfileBusiness.Insert(cpImport);
                }
                ClassProfileBusiness.Save();
                Session["ListAllClass"] = null;
                Session["ListClassImport"] = null;
                return true;
            }
            else
            {
                return false;
            }
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            List<ClassProfile> ListClassProfile = (List<ClassProfile>)Session["ListClassImport"];
            List<ImportClassViewModel> lstImportClass = (List<ImportClassViewModel>)Session["ListAllClass"];
            bool retval = ImportClass(ListClassProfile);
            if (retval && ListClassProfile.Count > 0 && lstImportClass.Count > 0)
            {
                //return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
                //Chiendd: 14/03/2015, sua thong bao import thanh cong n/m lop hoc
                string retMsg = String.Format("{0} {1}/{2} lớp học", Res.Get("Common_Label_ImportSuccessMessage"), ListClassProfile.Count, lstImportClass.Count);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            else
            {
                return Json(new JsonMessage("Không có dữ liệu hợp lệ."));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        public JsonResult ImportExcel()
        {
            /*Lưu file lên server - DungVA - 31/05/2013*/
            HttpPostedFileBase file = (HttpPostedFileBase)Session["stream"];
            string physicalPath = string.Empty;
            if (file != null)
            {
                String FileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), FileName);
                file.SaveAs(physicalPath);
                //Session["FilePath"] = physicalPath;
            }
            /*End Lưu file lên server*/
            if (String.IsNullOrEmpty(physicalPath))
            {
                throw new BusinessException(Res.Get("ImportClass_Label_FailedTemplated"));
            }
            //string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTRange range = sheet.GetRow(1);

            #region Kiểm tra template
            string SchoolNameTitle = (string)sheet.GetCellValue(2, 1);
            string ClassTitle = (string)sheet.GetCellValue(3, 1);
            string OrderNumberTitle = sheet.GetCellValue(5, 1) != null ? sheet.GetCellValue(5, 1).ToString() : string.Empty;
            string educationLevelExcel = (string)sheet.GetCellValue(5, 2);
            string classNameExcel = (string)sheet.GetCellValue(5, 3);
            string subCommityNameExcel = (string)sheet.GetCellValue(5, 4);
            string teacherNameExcel = (string)sheet.GetCellValue(5, 5);
            string session = (string)sheet.GetCellValue(5, 6);
            string sessionKey = (string)sheet.GetCellValue(5, 7);

            string schoolName = Res.Get("ImportClass_Label_SchoolNameExcel") + _globalInfo.SchoolName.ToUpper();
            string EducationLevelName = Res.Get("ImportClass_Label_educationLevelExcel");
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                EducationLevelName = Res.Get("PupilProfile_Label_Class_MN");

            }
            if (String.Compare(SchoolNameTitle, schoolName) != 0 ||
                String.Compare(ClassTitle, Res.Get("ImportClass_Label_listClassExcel")) != 0 ||
                String.Compare(OrderNumberTitle, Res.Get("ImportClass_Label_orderNumberExcel")) != 0 ||
                String.Compare(educationLevelExcel, EducationLevelName) != 0 ||
                String.Compare(classNameExcel, Res.Get("ImportClass_Label_classNameExcel")) != 0 ||
                String.Compare(subCommityNameExcel, Res.Get("ImportClass_Label_subCommityNameExcel")) != 0 ||
                !teacherNameExcel.Contains("Giáo viên chủ nhiệm") ||
                String.Compare(session, Res.Get("ImportClass_Label_Section")) != 0 ||
                (String.Compare(sessionKey, Res.Get("Buổi học chính (*)")) != 0))
            {
                if ( _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                        || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    throw new BusinessException(Res.Get("ImportClass_Label_FailedTemplated"));
                }
            }
            #endregion

            // Khởi tạo dữ liệu
            int START = 6;
            int i = START;
            string educationLevel, className, headTeacher, subComittee, section, sectionkey;
            StringBuilder error = new StringBuilder();

            int SchoolID = _globalInfo.SchoolID.Value;

            List<SubCommittee> lstSubCommittee = SubCommitteeBusiness.All.ToList();
            List<string> lstSubCommitteeName = lstSubCommittee.Select(o => o.Resolution).ToList();
            IQueryable<Employee> iqEmployee = EmployeeBusiness.Search(new Dictionary<string, object> { 
                                                            {
                                                                "CurrentSchoolID", SchoolID
                                                            }});
            List<string> lsEmployeeName = iqEmployee.Select(o => o.FullName.ToLower()).ToList();

            //IQueryable<SchoolFaculty> iqFaculty = SchoolFacultyBusiness.Search(new Dictionary<string, object> { 
            //                                                {
            //                                                    "SchoolID", SchoolID
            //                                                }});
            //List<string> lstFacultyName = iqFaculty.Select(o => o.FacultyName).ToList();
            //IQueryable<TeacherOfFaculty> iqTeacherOfFaculty = TeacherOfFacultyBusiness.Search(new Dictionary<string, object> { 
            //                                                {
            //                                                    "LstFacultyID", iqFaculty.Select(o => o.SchoolFacultyID).ToList()
            //                                                }});

            List<ImportClassViewModel> lstImportClass = new List<ImportClassViewModel>();
            ImportClassViewModel importClass;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClassName = new Dictionary<string, object>();
            dicClassName["AcademicYearID"] = academicYearID;
            dicClassName["SchoolID"] = SchoolID;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dicClassName).ToList();
            IDictionary<int, string> infoClass = new Dictionary<int, string>();
            lstClassProfile.ForEach(o => infoClass.Add(o.ClassProfileID, o.DisplayName));
            IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicClassName).Where(o => infoClass.Keys.Contains(o.ClassID));
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["AcademicYearID"] = academicYearID;
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicClassSubject);
            IDictionary<int, string> infoEducation = new Dictionary<int, string>();
            _globalInfo.EducationLevels.ForEach(o => infoEducation.Add(o.EducationLevelID, o.Resolution));
            List<string> lsClassName = new List<string>();
            ClassProfile objClassProfile;
            List<ClassProfile> lstClassIns = new List<ClassProfile>();

            int EducationLevelID = 0;
            int SubComitteeID = 0; // Ban học
            int HeadTeacherID = 0;
            int SchoolFalcutyID = 0;
            int Section = 0;
            int SectionKey = 0;
            string employeeCode = string.Empty;
            do
            {
                educationLevel = (string)sheet.GetCellValue(i, 2);
                className = (string)sheet.GetCellValue(i, 3);
                subComittee = (string)sheet.GetCellValue(i, 4);
                headTeacher = (string)sheet.GetCellValue(i, 5);
                //schoolFaculty = (string)sheet.GetCellValue(i, 6);
                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                section = (string)sheet.GetCellValue(i, 6);
                //End: 2014/11/24 viethd4 Bổ sung buổi học
                sectionkey = (string)sheet.GetCellValue(i, 7);
                
                // Điều kiện dừng
                if (string.IsNullOrEmpty(educationLevel) && string.IsNullOrEmpty(className) && string.IsNullOrEmpty(subComittee)
                    && string.IsNullOrEmpty(headTeacher) && string.IsNullOrEmpty(section) && string.IsNullOrEmpty(sectionkey))
                    break;
 
                #region  Kiểm tra Import
                error = new StringBuilder();
                if (string.IsNullOrEmpty(className)) error.Append(Res.Get("Import_Validate_ClassName"));
                else if (infoClass.Values.Contains(className.Trim()))
                {
                    // Kiểm tra đã chọn khối
                    if (!string.IsNullOrEmpty(educationLevel))
                    {
                        objClassProfile = lstClassProfile.FirstOrDefault(o => className.Equals(o.DisplayName));
                        // Kiểm tra cùng tên khác khối
                        if (objClassProfile.EducationLevelID != infoEducation.FirstOrDefault(o => o.Value.Equals(educationLevel)).Key)
                        {
                            // Kiểm tra đã có HS hoặc đã khai báo môn học
                            bool IsExists = iqPupilOfClass.Count(o => o.ClassID == objClassProfile.ClassProfileID) > 0 || iqClassSubject.Count(o => o.ClassID == objClassProfile.ClassProfileID) > 0;
                            if (IsExists)
                            {
                                error.Append(Res.Get("Import_Validate_DuplicateClassName"));
                            }
                        }
                    }
                }
                else if (lsClassName.Contains(className)) error.Append(Res.Get("Import_Validate_ClassNameExist"));
                else lsClassName.Add(className);

                // Reset data
                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                EducationLevelID = SubComitteeID = HeadTeacherID = SchoolFalcutyID = Section = 0;
                //End: 2014/11/24 viethd4 Bổ sung buổi học

                // Ban hoc
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY) // Cấp 3 có ban học
                {
                    if (string.IsNullOrEmpty(subComittee))
                        error.Append(Res.Get("Import_Validate_SubCommittee"));
                    else if (!lstSubCommitteeName.Contains(subComittee))
                        error.Append(Res.Get("Import_Validate_SubCommitteeExist"));
                    else SubComitteeID = lstSubCommittee.FirstOrDefault(o => o.Resolution.Equals(subComittee)).SubCommitteeID;
                }

                // Giáo viên
                ////Edit code lai k validate GVCN đồng bộ với khi validate tạo mới
                //if (string.IsNullOrEmpty(headTeacher))
                //    error.Append(Res.Get("Import_Validate_HeadTeacher"));
                if (!string.IsNullOrEmpty(headTeacher))
                {
                    var arr = headTeacher.Split('\\');
                    employeeCode = arr[arr.Length - 1].Trim();
                    Employee objE = iqEmployee.FirstOrDefault(o => o.EmployeeCode.Equals(employeeCode));
                    if (objE != null)
                    {
                        HeadTeacherID = objE.EmployeeID;
                    }
                    //else
                    //{
                    //    error.Append("Không tồn tại giáo viên " + headTeacher);
                    //}
                }
                // To bo mon
                //if (string.IsNullOrEmpty(schoolFaculty))
                //    error.Append(Res.Get("Import_Validate_SchoolFaculty"));
                //else if (!lstFacultyName.Contains(schoolFaculty))
                //    error.Append(Res.Get("Import_Validate_SchoolFacultyExist"));
                //else SchoolFalcutyID = iqFaculty.FirstOrDefault(o => o.FacultyName.Equals(schoolFaculty)).SchoolFacultyID;

                // Cap hoc
                if (string.IsNullOrEmpty(educationLevel))
                    error.Append(Res.Get("Import_Validate_EducationLevel"));
                else if (!infoEducation.Values.Contains(educationLevel))
                    error.Append(Res.Get("Import_Validate_EducationLevelExist"));
                else EducationLevelID = infoEducation.FirstOrDefault(o => o.Value.Equals(educationLevel)).Key;

                // Giao vien thuoc To bo mon
                //if (SchoolFalcutyID > 0 && HeadTeacherID > 0 && !iqTeacherOfFaculty.Any(o => o.FacultyID == SchoolFalcutyID && o.TeacherID == HeadTeacherID))
                //    error.Append(Res.Get("Import_Validate_TeacherOfFaculty"));

                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                //Buổi học
                if (string.IsNullOrEmpty(section))
                {
                    error.Append(Res.Get("Import_Validate_Section_Required"));
                }
                else if (string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_ALL) != 0)
                {
                    error.Append(Res.Get("Import_Validate_Section_NotExist"));
                }
                else
                {
                    switch (section)
                    {
                        case ImportClassConstants.STRING_SECTION_MORNING:
                            Section = ImportClassConstants.SECTION_MORNING;
                            break;
                        case ImportClassConstants.STRING_SECTION_AFTERNOON:
                            Section = ImportClassConstants.SECTION_AFTERNOON;
                            break;
                        case ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON:
                            Section = ImportClassConstants.SECTION_MORNING_AFTERNOON;
                            break;
                        case ImportClassConstants.STRING_SECTION_EVENING:
                            Section = ImportClassConstants.SECTION_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_MORNING_EVENING:
                            Section = ImportClassConstants.SECTION_MORNING_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING:
                            Section = ImportClassConstants.SECTION_AFTERNOON_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_ALL:
                            Section = ImportClassConstants.SECTION_ALL;
                            break;
                    }
                }
                //End: 2014/11/24 viethd4 Bổ sung buổi học

                //check buoi hoc chinh
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                    || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {

                    if (string.IsNullOrEmpty(sectionkey))
                    {
                        error.Append(Res.Get("Import_Validate_SectionKey_Required"));
                    }
                    else if (string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_ALL) != 0)
                    {
                        error.Append(Res.Get("Import_Validate_SectionKey_NotExist"));
                    }
                    else
                    {
                        switch (sectionkey)
                        {
                            case ImportClassConstants.STRING_SECTION_MORNING:
                                SectionKey = ImportClassConstants.SECTION_MORNING;
                                break;
                            case ImportClassConstants.STRING_SECTION_AFTERNOON:
                                SectionKey = ImportClassConstants.SECTION_AFTERNOON;
                                break;
                            case ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON:
                                SectionKey = ImportClassConstants.SECTION_MORNING_AFTERNOON;
                                break;
                            case ImportClassConstants.STRING_SECTION_EVENING:
                                SectionKey = ImportClassConstants.SECTION_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_MORNING_EVENING:
                                SectionKey = ImportClassConstants.SECTION_MORNING_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING:
                                SectionKey = ImportClassConstants.SECTION_AFTERNOON_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_ALL:
                                SectionKey = ImportClassConstants.SECTION_ALL;
                                break;
                        }

                        if (Section == ImportClassConstants.SECTION_MORNING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_AFTERNOON)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_AFTERNOON)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_MORNING_AFTERNOON)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING && SectionKey != ImportClassConstants.SECTION_AFTERNOON && SectionKey != ImportClassConstants.SECTION_MORNING_AFTERNOON)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_MORNING_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING && SectionKey != ImportClassConstants.SECTION_EVENING && SectionKey != ImportClassConstants.SECTION_MORNING_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_AFTERNOON_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_AFTERNOON && SectionKey != ImportClassConstants.SECTION_EVENING && SectionKey != ImportClassConstants.SECTION_AFTERNOON_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                    }
                }
                // Danh sách hiển thị lên
                importClass = new ImportClassViewModel();
                importClass.DisplayName = className;
                importClass.EducationLevelName = educationLevel;
                importClass.SubCommitteeName = subComittee;
                importClass.HeadTeacherName = headTeacher;
                //importClass.SchoolFalcutyName = schoolFaculty;
                importClass.Section = section;
                importClass.SectionKey = sectionkey;

                if (error.Length == 0)
                {
                    importClass.Pass = true;
                    objClassProfile = new ClassProfile();
                    objClassProfile.HeadTeacherID = HeadTeacherID;
                    objClassProfile.EducationLevelID = EducationLevelID;
                    objClassProfile.SubCommitteeID = SubComitteeID;
                    objClassProfile.AcademicYearID = academicYearID;
                    objClassProfile.SchoolID = SchoolID;
                    objClassProfile.DisplayName = className;
                    objClassProfile.Section = Section;
                    objClassProfile.SeperateKey = SectionKey;
                    //Gán loại lớp cho cấp GDTX
                    var school = SchoolProfileBusiness.Find(SchoolID);
                    if (school != null && school.TrainingTypeID.Value == 3)
                    {
                        var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                        var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                        var listEduTerrary = new List<int>() { 10, 11, 12 };
                        if (listEduLvPrimary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 1;
                        }
                        else if (listEduSecondary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 2;
                        }
                        else if (listEduTerrary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 3;
                        }
                    }
                    lstClassIns.Add(objClassProfile);
                }
                else
                {
                    importClass.Description = error.ToString();
                    importClass.Pass = false;
                }
                lstImportClass.Add(importClass);

                // Tăng chỉ số dòng
                i++;
                #endregion

            } while (true);

            if (lstClassIns.Count == lstImportClass.Count)
            {
                ImportClass(lstClassIns);
                //Chiendd: 14/03/2015, sua thong bao import thanh cong n/m lop hoc
                string retMsg = String.Format("{0} {1}/{2} lớp học", Res.Get("Common_Label_ImportSuccessMessage"), lstClassIns.Count, lstImportClass.Count);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            else
            {
                ViewData[ImportClassConstants.LIST_CLASSPROFILE] = lstImportClass;
                // DungVA - Invisible Subcommittee 
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                    ViewData[ImportClassConstants.INVISIBLE_SUBCOMMITTEE] = true;
                else ViewData[ImportClassConstants.INVISIBLE_SUBCOMMITTEE] = false;
                // End of DungVA - Invisible Subcommittee 

                if (_globalInfo.IsCurrentYear) ViewData[ImportClassConstants.DISABLE_BTNSAVE] = false;
                else ViewData[ImportClassConstants.DISABLE_BTNSAVE] = true;

                Session["ListClassImport"] = lstClassIns;
                Session["ListAllClass"] = lstImportClass;
                return Json(new JsonMessage("", JsonMessage.ERROR));
                //Có lỗi thì hệ thống hiển thị màn hình Lựa chọn thao tác import
            }
        }

        public FileResult DowloadDataErr()
        {
            string ReportName = string.Empty;
            Stream excel = SetDataFileTemplate(ref ReportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }

        private Stream SetDataFileTemplate(ref string reportName, bool isDataErr = false)
        {
            string reportCode = SystemParamsInFile.MAU_IMPORT_LOP_HOC;
            string ReportName = string.Empty;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook;
            try
            {
                oBook = VTExport.OpenWorkbook(templatePath);
            }
            catch
            {
                throw new BusinessException(Res.Get("Import_Validate_FileInUsed"));
            }
            //Lấy sheet 
            IVTWorksheet refSheet = null, sheet1 = null;
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                sheet1 = oBook.GetSheet(1);
                refSheet = oBook.GetSheet(2);
            }
            else
            {
                sheet1 = oBook.GetSheet(1);
                refSheet = oBook.GetSheet(2);
                // Ẩn cột ban học với cấp 1&2, Mầm non
                sheet1.HideColumn(4);
            }
            int START = 6;
            int startRow = START;
            IVTRange range = refSheet.GetRange("A5", "F5");
            IVTRange range1 = sheet1.GetRange("A5", "F5");

            string schoolName = Res.Get("ImportClass_Label_SchoolNameExcel") + _globalInfo.SchoolName.ToUpper();
            refSheet.SetCellValue("A2", schoolName);
            sheet1.SetCellValue("A2", schoolName); // chứa thông tin Import

            //// Title
            //sheet1.SetCellValue("A3", Res.Get("ImportClass_Label_listClassExcel"));
            //sheet1.SetCellValue("A4", Res.Get("ImportClass_Label_orderNumberExcel"));
            //sheet1.SetCellValue("B4", Res.Get("ImportClass_Label_educationLevelExcel"));
            //sheet1.SetCellValue("C4", Res.Get("ImportClass_Label_classNameExcel"));
            //sheet1.SetCellValue("D4", Res.Get("ImportClass_Label_subCommityNameExcel"));
            //sheet1.SetCellValue("E4", Res.Get("ImportClass_Label_teacherNameExcel"));
            ////sheet1.SetCellValue("F4", Res.Get("ImportClass_Label_schoolFacultyNameExcel"));
            ////Start: 2014/11/24 viethd4 Bổ sung buổi học
            //sheet1.SetCellValue("F4", Res.Get("ImportClass_Label_Section"));
            ////End: 2014/11/24 viethd4 Bổ sung buổi học
            // Nếu Cấp Mầm non - Set lại thông tin Nhóm lớp/Tên Lớp
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                sheet1.SetCellValue("B4", Res.Get("PupilProfile_Label_Class_MN"));
                sheet1.HideColumn(7);
            }
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
            // Xuất DS cấp học
            int d = START;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                refSheet.CopyPasteSameRowHeigh(range, startRow);
                refSheet.SetCellValue("K" + d, lstEducationLevel[i].Resolution);
                d++;
            }
            int rowTeacher = START;
            // Danh sách GV hiện có
            IDictionary<string, object> dicTeacher = new Dictionary<string, object>();
            dicTeacher["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            //dicTeacher["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dicTeacher["isFromEduActive"] = true;
            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, dicTeacher).ToList();
            // Sắp xếp Tiếng việt có dấu
            lstEmployee = lstEmployee.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name))
                 .ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName)).ToList();

            IDictionary<string, object> dicFalcuty = new Dictionary<string, object>();

            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicFalcuty).OrderBy(o => o.FacultyName).ToList();

            IDictionary<int, string> DicSchoolFaculty = new Dictionary<int, string>();
            lstSchoolFaculty.ForEach(p =>
            {
                DicSchoolFaculty[p.SchoolFacultyID] = p.FacultyName;
            });

            int SchoolFacultyID = 0;

            for (int i = 0; i < lstEmployee.Count; i++)
            {
                SchoolFacultyID = lstEmployee[i].SchoolFacultyID.HasValue ? lstEmployee[i].SchoolFacultyID.Value : 0;
                refSheet.CopyPasteSameRowHeigh(range, startRow);
                refSheet.SetCellValue("M" + rowTeacher, lstEmployee[i].FullName + "\\ " + DicSchoolFaculty[SchoolFacultyID] + "\\ " + lstEmployee[i].EmployeeCode);
                rowTeacher++;
            }

            // Ban học
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                int rowSubcommitee = START;
                IDictionary<string, object> dicSubcommitee = new Dictionary<string, object>();
                dicSubcommitee["IsActive"] = true;
                List<SubCommittee> lstSubCommittee = SubCommitteeBusiness.Search(dicSubcommitee).OrderBy(o => o.Resolution).ToList();
                for (int i = 0; i < lstSubCommittee.Count; i++)
                {
                    refSheet.CopyPasteSameRowHeigh(range, startRow);
                    refSheet.SetCellValue("L" + rowSubcommitee, lstSubCommittee[i].Resolution);
                    rowSubcommitee++;
                }
            }


            //for (int i = 0; i < lstSchoolFaculty.Count; i++)
            //{
            //    refSheet.CopyPasteSameRowHeigh(range, startRow);
            //    refSheet.SetCellValue("N" + rowFalcuty, lstSchoolFaculty[i].FacultyName);
            //    rowFalcuty++;
            //}

            if (isDataErr)//fill dữ liệu TH download dữ liệu lỗi
            {
                List<ImportClassViewModel> lstImportClassEr = new List<ImportClassViewModel>();
                lstImportClassEr = (List<ImportClassViewModel>)Session["ListAllClass"];
                lstImportClassEr = lstImportClassEr.Where(p => p.Pass == false).ToList();
                ImportClassViewModel objClassErr = new ImportClassViewModel();
                int startRowErr = START;
                for (int i = 0; i < lstImportClassEr.Count; i++)
                {
                    objClassErr = lstImportClassEr[i];
                    //STT
                    sheet1.SetCellValue(startRowErr, 1, i + 1);
                    //Khoi lop
                    sheet1.SetCellValue(startRowErr, 2, objClassErr.EducationLevelName);
                    //Ten lop
                    sheet1.SetCellValue(startRowErr, 3, objClassErr.DisplayName);
                    //Ban hoc
                    sheet1.SetCellValue(startRowErr, 4, objClassErr.SubCommitteeName);
                    //GVCN
                    sheet1.SetCellValue(startRowErr, 5, objClassErr.HeadTeacherName);
                    //To bo mon
                    //sheet1.SetCellValue(startRowErr, 6, objClassErr.SchoolFalcutyName);
                    //Buoi hoc
                    sheet1.SetCellValue(startRowErr, 7, objClassErr.Section);
                    //Mo ta
                    sheet1.SetCellValue(startRowErr, 8, objClassErr.Description);
                    startRowErr++;
                }
                sheet1.GetRange(START, 1, START + lstImportClassEr.Count - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            else
            {
                sheet1.SetColumnWidth('H', 0);
            }

            reportName = reportDef.OutputNamePattern + "." + reportDef.OutputFormat;
            return oBook.ToStream() ;
        }

        public ActionResult Index()
        {
            if (!_globalInfo.SchoolID.HasValue) throw new BusinessException(Res.Get("Common_Validate_NotDataPermintion"));
            ViewData[ImportClassConstants.SHOW_MESSAGE_ERROR] = true;
            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            Session["stream"] = file;
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }
                JsonResult res1 = Json(new JsonMessage());
                res1.ContentType = "text/plain";
                return res1;

            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        public PartialViewResult ViewDataImportExcel()
        {
            List<ImportClassViewModel> lstImportClass = (List<ImportClassViewModel>)Session["ListAllClass"];
            List<ClassProfile> lstClass = (List<ClassProfile>)Session["ListClassImport"];
            ViewData[ImportClassConstants.LIST_CLASSPROFILE] = lstImportClass;
            ViewData[ImportClassConstants.SHOW_MESSAGE_ERROR] = (lstClass.Count != lstImportClass.Count);
            if (_globalInfo.IsCurrentYear) ViewData[ImportClassConstants.DISABLE_BTNSAVE] = false;
            else ViewData[ImportClassConstants.DISABLE_BTNSAVE] = true;
            return PartialView("ViewDataImport");
        }
    }
}





