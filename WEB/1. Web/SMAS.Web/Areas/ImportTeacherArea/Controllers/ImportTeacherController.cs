﻿/** 
* @author trangdd
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Web.Areas.ImportTeacherArea.Models;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.Globalization;

namespace SMAS.Web.Areas.ImportTeacherArea.Controllers
{
    public class ImportTeacherController : BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private readonly IEmployeeHistoryStatusBusiness EmployeeHistoryStatusBusiness;
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;
        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        public ImportTeacherController(ISchoolFacultyBusiness schoolFacultyBusiness, IQualificationLevelBusiness qualificationLevelBusiness, IWorkTypeBusiness workTypeBusiness,
            IContractTypeBusiness contractTypeBusiness, IEthnicBusiness ethnicBusiness, IReligionBusiness religionBusiness, IEmployeeBusiness employeeBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IEmployeeHistoryStatusBusiness EmployeeHistoryStatusBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness
            , ISpecialityCatBusiness SpecialityCatBusiness
            , IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness
            , IITQualificationLevelBusiness ITQualificationLevelBusiness
            , IPoliticalGradeBusiness PoliticalGradeBusiness
            , IStateManagementGradeBusiness StateManagementGradeBusiness
            , IEducationalManagementGradeBusiness EducationalManagementGradeBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness
            , IProvinceBusiness provinceBusiness,
            IDistrictBusiness districtBusiness,
            ICommuneBusiness communeBusiness
            )
        {
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.QualificationLevelBusiness = qualificationLevelBusiness;
            this.WorkTypeBusiness = workTypeBusiness;
            this.ContractTypeBusiness = contractTypeBusiness;
            this.EthnicBusiness = ethnicBusiness;
            this.ReligionBusiness = religionBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EmployeeHistoryStatusBusiness = EmployeeHistoryStatusBusiness;
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.ProvinceBusiness = provinceBusiness;
            this.DistrictBusiness = districtBusiness;
            this.CommuneBusiness = communeBusiness;
        }
        // GET: /ImportTeacherArea/ImportTeacher/

        public ActionResult Index()
        {
            ViewData[ImportTeacherConstants.ROLE] = CheckActionPermissionMinAddReturn();
            return View();
        }

        public FileResult DownloadFileTemplate()
        {
            Stream excel = this.SetValueTemplate();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "Mau_Import_Canbo.xls";
            result.FileDownloadName = ReportName;
            return result;
        }

        public FileResult ExportExcelErr()
        {
            Stream excel = this.SetValueTemplate(true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "Mau_Import_Canbo.xls";
            result.FileDownloadName = ReportName;
            return result;
        }

        private Stream SetValueTemplate(bool isExportErr = false)
        {
            GlobalInfo global = new GlobalInfo();
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Import" + "/" + "Mau_Import_Canbo.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet tempSheet = oBook.GetSheet(1);
            IVTWorksheet refSheet = oBook.GetSheet(2);
            IVTWorksheet provinceSheet = oBook.GetSheet(3);
            IVTWorksheet districtSheet = oBook.GetSheet(4);
            IVTWorksheet communeSheet = oBook.GetSheet(5);

            //Lay du lieu
            List<SchoolFaculty> LstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.FacultyName).ToList();
            List<QualificationLevel> LstQualificationLevel = QualificationLevelBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<WorkType> LstWorkType = WorkTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ContractType> LstContractType = ContractTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<Ethnic> LstEthnicTemp = EthnicBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.EthnicName).ToList();
            List<Religion> LstReligionTemp = ReligionBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<Ethnic> LstEthnic = new List<Ethnic>();
            Ethnic ethnic = LstEthnicTemp.Where(u => u.EthnicName.ToLower().Equals("kinh")).FirstOrDefault();
            if (ethnic != null)
            {
                LstEthnic.Add(ethnic);
                LstEthnic.AddRange(LstEthnicTemp.Where(u => u.EthnicID != ethnic.EthnicID));
            }

            List<Religion> LstReligion = new List<Religion>();
            Religion religion = LstReligionTemp.Where(u => u.Resolution.ToLower().Equals("không")).FirstOrDefault();
            if (religion != null)
            {
                LstReligion.Add(religion);
                LstReligion.AddRange(LstReligionTemp.Where(u => u.ReligionID != religion.ReligionID));
            }
            List<TrainingLevel> LstTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<SpecialityCat> LstSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ForeignLanguageGrade> LstForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ITQualificationLevel> LstITQualificationLevel = ITQualificationLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<PoliticalGrade> LstPoliticalGrade = PoliticalGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<StateManagementGrade> LstStateManagementGrade = StateManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<EducationalManagementGrade> LstEducationalManagementGrade = EducationalManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<FamilyType> LstFamilyType = FamilyTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();

            SchoolProfile school = SchoolProfileBusiness.Find(global.SchoolID.Value);

            tempSheet.SetCellValue("A2", school.SchoolName);
            #region fill ref sheet data
            //Cấp học - Cột B
            List<string> lst = SMAS.Business.Common.Utils.GetListGrade(school.EducationGrade);
            for (int i = 0; i < lst.Count; i++)
                refSheet.SetCellValue("B" + (i + 2), lst[i]);

            //Tổ bộ môn - Cột C
            for (int i = 0; i < LstSchoolFaculty.Count(); i++)
                refSheet.SetCellValue("C" + (i + 2), LstSchoolFaculty[i].FacultyName);

            //Trình độ văn hóa - Cột D
            for (int i = 0; i < LstQualificationLevel.Count(); i++)
                refSheet.SetCellValue("D" + (i + 2), LstQualificationLevel[i].Resolution);

            //Loại công việc - Cột E
            for (int i = 0; i < LstWorkType.Count(); i++)
                refSheet.SetCellValue("E" + (i + 2), LstWorkType[i].Resolution);

            //Loại hợp đồng - Cột F
            for (int i = 0; i < LstContractType.Count(); i++)
                refSheet.SetCellValue("F" + (i + 2), LstContractType[i].Resolution);

            //Dân tộc - Cột G
            for (int i = 0; i < LstEthnic.Count(); i++)
                refSheet.SetCellValue("G" + (i + 2), LstEthnic[i].EthnicName);

            //Tôn giáo - Cột H
            for (int i = 0; i < LstReligion.Count(); i++)
                refSheet.SetCellValue("H" + (i + 2), LstReligion[i].Resolution);

            //Trinh do dao tao - Cột J
            for (int i = 0; i < LstTrainingLevel.Count(); i++)
                refSheet.SetCellValue("J" + (i + 2), LstTrainingLevel[i].Resolution);
            //Chuyen nganh dao tao - Cột K
            for (int i = 0; i < LstSpecialityCat.Count(); i++)
                refSheet.SetCellValue("K" + (i + 2), LstSpecialityCat[i].Resolution);
            //Trinh do ngoai ngu - Cột L
            for (int i = 0; i < LstForeignLanguageGrade.Count(); i++)
                refSheet.SetCellValue("L" + (i + 2), LstForeignLanguageGrade[i].Resolution);
            //Trinh do tin hoc - Cột M
            for (int i = 0; i < LstITQualificationLevel.Count(); i++)
                refSheet.SetCellValue("M" + (i + 2), LstITQualificationLevel[i].Resolution);
            //Trinh do Lyluan chinh tri - Cột N
            for (int i = 0; i < LstPoliticalGrade.Count(); i++)
                refSheet.SetCellValue("N" + (i + 2), LstPoliticalGrade[i].Resolution);
            //Trinh do Quan ly nha nuoc - Cột O
            for (int i = 0; i < LstStateManagementGrade.Count(); i++)
                refSheet.SetCellValue("O" + (i + 2), LstStateManagementGrade[i].Resolution);
            //Trinh do Quan ly Giao duc - Cột P
            for (int i = 0; i < LstEducationalManagementGrade.Count(); i++)
                refSheet.SetCellValue("P" + (i + 2), LstEducationalManagementGrade[i].Resolution);
            //Thanh phan gia dinh - Cột Q
            for (int i = 0; i < LstFamilyType.Count(); i++)
                refSheet.SetCellValue("Q" + (i + 2), LstFamilyType[i].Resolution);
            #endregion

            #region Tinh/thanh, quan/huyen, xa phuong

            List<Province> lstProvince = ProvinceBusiness.All.Where(p => p.IsActive == true && p.ProvinceID != GlobalConstants.ProvinceID_NA).OrderBy(p => p.ProvinceName).ToList();
            List<District> lstDistrict = DistrictBusiness.All.Where(d => d.IsActive == true).ToList();
            List<Commune> lstCommune = CommuneBusiness.All.Where(s => s.IsActive == true).ToList();

            //Fill sheet Province;
            int provinceRow = 4;
            int districtRow = 5;
            int districtOder = 0;
            IVTRange rangeDistrict = districtSheet.GetRange("A4", "D4");

            //row xa phuong
            IVTRange rgCommuneProvince = communeSheet.GetRange("A4", "E4");
            IVTRange rgcommenu = communeSheet.GetRange("A5", "E5");
            int communeRow = 6;
            //int commnueProvinceRow = 4;
            int communeOrder = 1;
            for (int i = 0; i < lstProvince.Count; i++)
            {
                int provinceOrder = i + 1;
                Province province = lstProvince[i];
                provinceSheet.SetCellValue(provinceRow, 1, provinceOrder);
                provinceSheet.SetCellValue(provinceRow, 2, province.ProvinceCode);
                provinceSheet.SetCellValue(provinceRow, 3, province.ProvinceName);
                provinceRow++;

                //Dien du lieu quan huyen
                List<District> lstDistrictbyProvince = lstDistrict.Where(d => d.ProvinceID == province.ProvinceID).OrderBy(d => d.DistrictName).ToList();
                if (lstDistrictbyProvince == null)
                {
                    continue;
                }
                int firstDistrictRow = districtRow; //(i > 0) ? districtRow + 1 : districtRow;
                for (int k = 0; k < lstDistrictbyProvince.Count; k++)
                {
                    District district = lstDistrictbyProvince[k];
                    districtOder++;
                    if (k > 0)
                    {
                        districtSheet.CopyPasteSameSize(rangeDistrict, districtRow, 1);
                    }
                    districtSheet.SetCellValue(districtRow, 1, districtOder);
                    districtSheet.SetCellValue(districtRow, 2, province.ProvinceName);
                    districtSheet.SetCellValue(districtRow, 3, district.DistrictCode);
                    districtSheet.SetCellValue(districtRow, 4, district.DistrictName);
                    districtRow++;
                }

                IVTRange range = districtSheet.GetRange(firstDistrictRow, 2, districtRow - 1, 2);
                range.Merge();

                //Dien du lieu xa phuong
                List<Commune> lstCommuneDistrict = (from c in lstCommune
                                                    join d in lstDistrictbyProvince on c.DistrictID equals d.DistrictID
                                                    orderby d.DistrictName, c.CommuneName
                                                    select c).ToList();
                if (lstCommuneDistrict == null || lstCommuneDistrict.Count == 0)
                {
                    continue;
                }


                communeSheet.CopyPasteSameSize(rgCommuneProvince, communeRow, 1);
                communeSheet.SetCellValue(communeRow, 1, i + 1);
                communeSheet.SetCellValue(communeRow, 2, province.ProvinceName);
                communeRow++;

                for (int j = 0; j < lstCommuneDistrict.Count; j++)
                {
                    Commune objCommune = lstCommuneDistrict[j];
                    communeSheet.CopyPasteSameSize(rgcommenu, communeRow, 1);
                    communeSheet.SetCellValue(communeRow, 1, communeOrder);
                    communeSheet.SetCellValue(communeRow, 2, objCommune.District.DistrictCode);
                    communeSheet.SetCellValue(communeRow, 3, objCommune.District.DistrictName);
                    communeSheet.SetCellValue(communeRow, 4, objCommune.CommuneCode);
                    communeSheet.SetCellValue(communeRow, 5, objCommune.CommuneName);
                    communeRow++;
                    communeOrder++;
                }
                //commnueProvinceRow += 1;
                //commnueProvinceRow += lstCommuneDistrict.Count;

            }
            #endregion
            districtSheet.DeleteRow(4);
            communeSheet.DeleteRow(4);
            communeSheet.DeleteRow(4);


            if (isExportErr)
            {
                #region view error
                List<ImportTeacherViewModel> ListEmployee = (List<ImportTeacherViewModel>)Session["ListEmployee"];
                //fill dữ liệu lỗi
                ListEmployee = ListEmployee.Where(p => p.PASS == false).ToList();
                int startRow = 5;
                //int startColumn = 2;
                ImportTeacherViewModel objEmployee = null;
                for (int i = 0; i < ListEmployee.Count; i++)
                {
                    objEmployee = ListEmployee[i];
                    //STT
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.STT_1, i + 1);
                    //To bo mon
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TO_BO_MON_2, objEmployee.FacultyName);
                    //Ma giao vien
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.MA_CAN_BO_3, objEmployee.EmployeeCode);
                    //Ho va ten
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.HO_VA_TEN_4, objEmployee.FullName);
                    //Gioi tinh
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.GIOI_TINH_5, objEmployee.Genre ? "" : "x");
                    //Ngay sinh
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGAY_SINH_6, objEmployee.BirthDateString);
                    //Loai cong viec
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.LOAI_CONG_VIEC_7, objEmployee.WorkTypeName);
                    //Loai hop dong
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.HINH_THUC_HD_8, objEmployee.ContractTypeName);
                    //Dan toc
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.DAN_TOC_9, objEmployee.EthnicName);
                    //Ton giao
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TON_GIAO_10, objEmployee.ReligionName);
                    //Cap day chinh
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.CAP_DAY_CHINH_11, objEmployee.AppliedLevelName);
                    //Ngay vao truong
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_TRUONG_12, string.Format("{0:dd/MM/yyyy}", objEmployee.IntoSchoolDateString));
                    //So CMND
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.SO_CMND_13, objEmployee.IdentityNumber);
                    //Ngay cap
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGAY_CAP_CMND_14, string.Format("{0:dd/MM/yyyy}", objEmployee.IdentityIssuedDate));
                    //Noi cap
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NOI_CAP_CMND_15, objEmployee.IdentityIssuedPlace);
                    //SDT
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_16, objEmployee.Telephone);
                    //SDT di dong
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_DI_DONG_17, objEmployee.Mobile);
                    //Email
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.EMAIL_18, objEmployee.Email);
                    //So so BHXH
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.SO_BHXH_19, objEmployee.IdentityNumber);
                    //Que quan
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.QUE_QUAN_20, objEmployee.HomeTown);

                    //Tinh/thanh
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TINH_THANH_21, objEmployee.ProvinceName);

                    //Quan/Huyen
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.QUAN_HUYEN_22, objEmployee.DistrictName);

                    //xa/phuong
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.XA_PHUONG_23, objEmployee.CommuneName);

                    //Dia chi thuong tru
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.DIA_CHI_THUONG_TRU_24, objEmployee.PermanentResidentalAddress);
                    //Suc Khoe
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.SUC_KHOE_25, objEmployee.HealthStatus);


                    //Doan vien
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.DOAN_VIEN_26, objEmployee.IsYouthLeageMember ? "x" : "");
                    //Dang vien
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.DANG_VIEN_27, objEmployee.IsCommunistPartyMember ? "x" : "");
                    //Cong doan vien
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.CONG_DOAN_VIEN_28, objEmployee.IsSyndicate ? "x" : "");
                    //Thanh phan gia dinh
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.THANH_PHAN_GD_29, objEmployee.FamilyTypeName);
                    //Ho ten bo
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.HO_TEN_BO_30, objEmployee.FatherFullName);
                    //Nam sinh bo
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_BO_31, string.Format("{0:yyyy}", objEmployee.FatherBirthDate));
                    //Nghe nghiep bo
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_BO_32, objEmployee.FatherJob);
                    //Noi lam viec cua bo
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_BO_33, objEmployee.FatherWorkingPlace);
                    //Ho ten me
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.HO_TEN_ME_34, objEmployee.MotherFullName);
                    //Nam sinh me
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_ME_35, string.Format("{0:yyyy}", objEmployee.MotherBirthDate));
                    //Nghe nghiep me
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_ME_36, objEmployee.MotherJob);
                    //Noi lam viec cua me
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_ME_37, objEmployee.MotherWorkingPlace);
                    //Ho ten v/c
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.HO_VA_TEN_VC_38, objEmployee.SpouseFullName);
                    //Nam sinh v/v
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_VC_39, string.Format("{0:yyyy}", objEmployee.SpouseBirthDate));
                    //Nghe nghiep v/c
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_VC_40, objEmployee.SpouseJob);
                    //Noi lam viec cua v/c
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_VC_41, objEmployee.SpouseWorkingPlace);

                    //Trinh do dao tao
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_CMNV_CAONHAT_42, objEmployee.TrainingLevelName);
                    //Chuyen nganh dao tao
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.CHUYEN_NGANH_DAO_TAO_43, objEmployee.SpecialityCatName);
                    //Trinh do van hoa
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_VAN_HOA_44, objEmployee.QualificationLevelName);
                    //Trinh do ngoai ngu
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_NGOAI_NGU_45, objEmployee.ForeignLanguageGradeName);
                    //Trinh do tin hoc
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_TIN_HOC_46, objEmployee.ITQualificationLevelName);
                    //Trinh do ly luan chinh tri
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_LY_LUAN_CT_47, objEmployee.PoliticalGradeName);
                    //Trinh do quan ly nha nuoc
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLNN_48, objEmployee.StateManagementGradeName);
                    //Trinh do quan ly gia duc
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLGD_49, objEmployee.EducationalManagementGradeName);
                    //Chuyen trach doan do
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.CHUYEN_TRACH_DD_50, objEmployee.DedicatedForYoungLeague ? "x" : "");
                    //Boi duong thuong xuyen
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.BOI_DUONG_TX_51, objEmployee.RegularRefresher ? "x" : "");
                    //Ngay vao nghe
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_NGHE_52, string.Format("{0:dd/MM/yyyy}", objEmployee.StartingDate));
                    //Mo ta loi
                    tempSheet.SetCellValue(startRow, ExportEmployeeConstants.MO_TA_LOI_53, objEmployee.Note);
                    startRow++;
                }

                tempSheet.GetRange(5, ExportEmployeeConstants.STT_1, 5 + ListEmployee.Count - 1, ExportEmployeeConstants.MO_TA_LOI_53).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                #endregion
            }
            else
            {
                tempSheet.HideColumn(ExportEmployeeConstants.MO_TA_LOI_53);

            }
            Stream excel = oBook.ToStream();
            return excel;
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage(Res.Get("Lbl_Role_Error"), "RoleError"));
            }
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res1.ContentType = "text/plain";
                    return res1;
                    //return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                JsonResult res = Json(new JsonMessage(""));
                res.ContentType = "text/plain";
                return res;
                //return Json(new JsonMessage(""));
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel()
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage(Res.Get("Lbl_Role_Error"), "RoleError"));
            }
            List<ImportTeacherViewModel> ListEmployee = new List<ImportTeacherViewModel>();
            string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            GlobalInfo global = new GlobalInfo();
            //neu khong dung template thi ban exception
            string SchoolName = sheet.GetCellValue(2, VTVector.dic['A']) != null ? (string)sheet.GetCellValue(2, VTVector.dic['A']) : "";
            SchoolProfile school = SchoolProfileBusiness.Find(global.SchoolID.Value);
            if (school.SchoolName.Trim().ToUpper() != SchoolName.Trim().ToUpper())
            {
                throw new BusinessException(Res.Get("ImportClass_Label_FailedTemplated"));
            }
            //lay truoc employeecode
            string OriginalTeacherCode = string.Empty;
            int MaxOrderNumber = 0;
            int NumberLength = 0;
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);
            if (isAutoGenCode)
            {
                CodeConfigBusiness.GetTeacherCodeForImport(global.SchoolID.Value, out OriginalTeacherCode, out MaxOrderNumber, out NumberLength);
            }
            //Lay du lieu
            List<SchoolFaculty> LstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            SchoolFaculty sf = new SchoolFaculty();
            List<QualificationLevel> LstQualificationLevel = QualificationLevelBusiness.Search(new Dictionary<string, object>()).ToList();
            QualificationLevel ql = new QualificationLevel();
            List<WorkType> LstWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>()).ToList();
            WorkType wt = new WorkType();
            List<ContractType> LstContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).ToList();
            ContractType ct = new ContractType();
            List<Ethnic> LstEthnic = EthnicBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            Ethnic et = new Ethnic();
            List<Religion> LstReligion = ReligionBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            Religion religion = new Religion();

            List<FamilyType> LstFamilyType = FamilyTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            FamilyType familyType = new FamilyType();

            List<TrainingLevel> LstTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            TrainingLevel trainingLevel = new TrainingLevel();

            List<SpecialityCat> LstSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            SpecialityCat specialityCat = new SpecialityCat();

            List<ForeignLanguageGrade> LstForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            ForeignLanguageGrade foreignLanguageGrade = new ForeignLanguageGrade();

            List<ITQualificationLevel> LstITQualificationLevel = ITQualificationLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            ITQualificationLevel iTQualificationLevel = new ITQualificationLevel();

            List<PoliticalGrade> LstPoliticalGrade = PoliticalGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            PoliticalGrade politicalGrade = new PoliticalGrade();

            List<StateManagementGrade> LstStateManagementGrade = StateManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            StateManagementGrade stateManagementGrade = new StateManagementGrade();

            List<EducationalManagementGrade> LstEducationalManagementGrade = EducationalManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).ToList();
            EducationalManagementGrade educationalManagementGrade = new EducationalManagementGrade();

            Employee ee = new Employee();
            //Lấy danh sách employeeCode của giáo viên ở trạng thái nghỉ hưu hoặc chuyển công tác
            List<string> LstEmployeeNotWorking = EmployeeHistoryStatusBusiness.All.Where(o => o.SchoolID == global.SchoolID)
                                                   .Where(o => o.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_MOVED || o.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_RETIRED)
                                                   .Select(o => o.Employee.EmployeeCode).ToList();

            List<Province> lstProvince = ProvinceBusiness.All.Where(p => p.IsActive == true && p.ProvinceCode != "N/A").ToList();
            List<District> lstDistrict = DistrictBusiness.All.Where(d => d.IsActive == true).ToList();
            List<Commune> lstCommune = CommuneBusiness.All.Where(s => s.IsActive == true).ToList();

            List<int> LstAppliedLevel = SchoolProfileBusiness.GetListAppliedLevel(global.SchoolID.Value);
            int startRow = 5;
            ImportTeacherViewModel Employee = null;
            //Lay du lieu tu excel dua vao 1 list                           
            while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null || sheet.GetCellValue(startRow, 4) != null)
            {
                Employee = new ImportTeacherViewModel();
                #region Check du lieu de map employee
                //To bo mon
                Employee.FacultyName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TO_BO_MON_2) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.TO_BO_MON_2).ToString();
                Employee.PASS = true;
                if (Employee.FacultyName != "")
                {
                    sf = LstSchoolFaculty.Where(o => o.FacultyName.ToLower()==Employee.FacultyName.ToLower()).FirstOrDefault();
                    if (sf != null)
                    {
                        Employee.SchoolFacultyID = sf.SchoolFacultyID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = "Tổ bộ môn không tồn tại. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = "Tổ bộ môn không để trống. ";
                }
                // ma giao vien 3
                Employee.EmployeeCode = (sheet.GetCellValue(startRow, ExportEmployeeConstants.MA_CAN_BO_3) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.MA_CAN_BO_3).ToString().Trim();
                string TeacherCode = OriginalTeacherCode + ToStringWithFixedLength(MaxOrderNumber + 1 + startRow - 5, NumberLength);

                if (!isAutoGenCode)
                {
                    if (string.IsNullOrEmpty(Employee.EmployeeCode))
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Mã giáo viên không để trống. ";
                    }
                    else
                    {
                        if (Employee.EmployeeCode.Length > 30)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Độ dài mã giáo viên > 30 ký tự. ";
                        }
                        string[] arrayExpectedChar = { "\\" };

                        if (Utils.Utils.IsDangerousString(Employee.EmployeeCode) || Utils.Utils.IsUnexpectedChar(Employee.EmployeeCode, arrayExpectedChar))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Mã giáo viên chứa ký tự nguy hiểm. ";
                        }
                    }
                }
                else
                {
                    // So cau hinh khong cho sua ma GV
                    if (!isSchoolModify)
                    {
                        if (Employee.EmployeeCode.Length >= TeacherCode.Length)
                        {
                            int Number = 0;
                            string NumLengPattern = Employee.EmployeeCode.Substring(OriginalTeacherCode.Length, Employee.EmployeeCode.Length - OriginalTeacherCode.Length);
                            bool result2 = Int32.TryParse(NumLengPattern, out Number);
                            if (Number > 0)
                            {
                                if (!Employee.EmployeeCode.StartsWith(OriginalTeacherCode) || NumLengPattern.Length != NumberLength || result2 == false)
                                {
                                    Employee.PASS = false;
                                    Employee.Note = Employee.Note + "Mã giáo viên không theo cấu hình quy định.";
                                }
                            }
                            else
                            {
                                Employee.PASS = false;
                                Employee.Note = Employee.Note + "Mã giáo viên không theo cấu hình quy định.";
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(Employee.EmployeeCode))
                            {
                                Employee.EmployeeCode = TeacherCode;
                            }
                            else
                            {
                                Employee.PASS = false;
                                Employee.Note = Employee.Note + "Mã giáo viên không theo cấu hình quy định.";
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Employee.EmployeeCode))
                        {
                            Employee.EmployeeCode = TeacherCode;
                        }
                    }
                }

                if (ListEmployee.Any(u => u.EmployeeCode.ToLower().Trim() == Employee.EmployeeCode.ToLower().Trim()))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Mã giáo viên trùng với giáo viên khác. ";
                }


                //Nếu mã giáo viên trùng với giáo viên đã nghỉ hưu hoặc chuyển công tác
                if (!string.IsNullOrEmpty(Employee.EmployeeCode) && LstEmployeeNotWorking.Contains(Employee.EmployeeCode))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Giáo viên không ở trạng thái đang làm việc. ";
                }
                //Ho ten  giao vien 4
                object hovaten = sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_VA_TEN_4);
                Employee.FullName = hovaten == null ? "" : hovaten.ToString();
                Employee.FullName = Employee.FullName.Trim();
                if (string.IsNullOrEmpty(Employee.FullName))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Tên giáo viên không để trống. ";
                }
                else
                {
                    if (Employee.FullName.Length > 100)
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Độ dài tên giáo viên > 100 ký tự. ";
                    }
                    else
                    {
                        //truong Name lay tu FullName
                        int space = Employee.FullName.LastIndexOf(" ");
                        if (space == -1)
                        {
                            Employee.Name = Employee.FullName;
                        }
                        else
                        {
                            Employee.Name = Employee.FullName.Substring(space + 1);
                        }
                        if (Employee.Name.Length > 10)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Tên giáo viên không đúng. ";
                        }
                        if (Utils.Utils.IsDangerousString(Employee.FullName))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Tên giáo viên chứa ký tự nguy hiểm. ";
                        }
                    }
                }
                // gioi tinh 5
                object gioitinh = sheet.GetCellValue(startRow, ExportEmployeeConstants.GIOI_TINH_5);
                Employee.GenreName = gioitinh == null ? "" : gioitinh.ToString();
                if (Employee.GenreName.ToUpper() == "")
                {
                    Employee.GenreName = "Nam";
                    Employee.Genre = true;
                }
                else if (Employee.GenreName.ToUpper() == "X")
                {
                    Employee.GenreName = "Nữ";
                    Employee.Genre = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Giới tính không hợp lệ ";
                }
                //ngay sinh 6
                object BirthDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_SINH_6) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_SINH_6);
                Employee.BirthDateString = (BirthDate == null) ? "" : BirthDate.ToString();
                if (!string.IsNullOrEmpty(Employee.BirthDateString))
                {
                    DateTime Date;
                    if (DateTime.TryParseExact(Employee.BirthDateString, "d/M/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date)
                        || DateTime.TryParseExact(Employee.BirthDateString, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date)
                        || DateTime.TryParseExact(Employee.BirthDateString, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date))
                    {
                        Employee.BirthDate = DateTime.Parse(Employee.BirthDateString);
                        if (Employee.BirthDate < new DateTime(1900, 1, 1))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày sinh của giáo viên phải lớn hơn hoặc bằng ngày 01/01/1900. ";
                        }
                        else if (Employee.BirthDate.Value.Year > DateTime.Now.Year - 16)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Giáo viên phải lớn hơn 16 tuổi. ";
                        }
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Ngày sinh không đúng định dạng. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Ngày sinh không được để trống. ";
                }
                //loai cong viec 7
                Employee.WorkTypeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.LOAI_CONG_VIEC_7) == null) ? "Giáo viên" : sheet.GetCellValue(startRow, ExportEmployeeConstants.LOAI_CONG_VIEC_7).ToString();
                if (!string.IsNullOrEmpty(Employee.WorkTypeName))
                {
                    wt = LstWorkType.Where(o => o.Resolution == Employee.WorkTypeName).FirstOrDefault();
                    if (wt != null)
                    {
                        Employee.WorkTypeID = wt.WorkTypeID;
                        Employee.WorkGroupTypeID = wt.WorkGroupTypeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Loại công việc không tồn tại. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Loại công việc không được để trống. ";
                }


                //loai hop dong 8
                Employee.ContractTypeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.HINH_THUC_HD_8) == null) ? "Biên chế" : sheet.GetCellValue(startRow, ExportEmployeeConstants.HINH_THUC_HD_8).ToString();
                if (!string.IsNullOrEmpty(Employee.ContractTypeName))
                {
                    ct = LstContractType.Where(o => o.Resolution == Employee.ContractTypeName).FirstOrDefault();
                    if (ct != null)
                    {
                        Employee.ContractTypeID = ct.ContractTypeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Loại hợp đồng không tồn tại. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Loại hợp đồng không được để trống. ";
                }
                // dan toc 9
                Employee.EthnicName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.DAN_TOC_9) == null) ? "Kinh" : sheet.GetCellValue(startRow, ExportEmployeeConstants.DAN_TOC_9).ToString();
                if (!string.IsNullOrEmpty(Employee.EthnicName))
                {
                    et = LstEthnic.Where(o => o.EthnicName == Employee.EthnicName).FirstOrDefault();
                    if (et != null)
                    {
                        Employee.EthnicID = et.EthnicID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Dân tộc không tồn tại. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Dân tộc không được để trống. ";
                }
                //ton giao 10
                Employee.ReligionName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TON_GIAO_10) == null) ? "Không" : sheet.GetCellValue(startRow, ExportEmployeeConstants.TON_GIAO_10).ToString();
                if (!string.IsNullOrEmpty(Employee.ReligionName))
                {
                    religion = LstReligion.Where(o => o.Resolution == Employee.ReligionName).FirstOrDefault();
                    if (religion != null)
                    {
                        Employee.ReligionID = religion.ReligionID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Tôn giáo không tồn tại. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Tôn giáo không được để trống. ";
                }


                //Cap day chinh 11
                string AppliedLevel = (sheet.GetCellValue(startRow, ExportEmployeeConstants.CAP_DAY_CHINH_11) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.CAP_DAY_CHINH_11).ToString();
                if (!string.IsNullOrEmpty(AppliedLevel))
                {
                    int Applied = CommonConvert.ConvertToApplied(AppliedLevel);
                    if (Applied <= 0)
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Cấp dạy phải là số nguyên dương. ";
                    }
                    else
                    {
                        Employee.AppliedLevel = CommonConvert.ConvertToApplied(AppliedLevel);
                        if (!LstAppliedLevel.Contains(Employee.AppliedLevel.Value))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + Res.Get("Common_Validate_AppliedLevel");
                        }
                    }
                }
                else if (Employee.WorkTypeID == 10 || Employee.WorkTypeID == 12) // neu loai cong viec la giao vien thi bat buoc nhap
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Cấp dạy không được để trống. ";
                }

                //Ngay vao truong 12
                string IntoSchoolDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_TRUONG_12) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_TRUONG_12).ToString();
                Employee.IntoSchoolDateString = IntoSchoolDate;
                if (!string.IsNullOrEmpty(IntoSchoolDate))
                {
                    DateTime Date;
                    if (DateTime.TryParseExact(IntoSchoolDate, "d/M/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date)
                        || DateTime.TryParseExact(IntoSchoolDate, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date))
                    {
                        Employee.IntoSchoolDate = DateTime.Parse(IntoSchoolDate);
                        if (Employee.IntoSchoolDate.Value < new DateTime(1900, 1, 1))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào trường phải lớn hơn hoặc bằng ngày 01/01/1900. ";
                        }
                        else if (Employee.IntoSchoolDate.Value > DateTime.Now)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào trường phải nhỏ hơn ngày hiện tại. ";
                        }
                        else if (Employee.BirthDate.HasValue && Employee.IntoSchoolDate.Value.Year < (Employee.BirthDate.Value.Year + 16))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào trường phải lớn hơn hoặc bằng ngày 01/01/ " + (Employee.BirthDate.Value.Year + 16) + ". ";
                        }

                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Ngày vào trường không đúng định dạng. ";
                    }
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Ngày vào trường không được để trống. ";
                }

                // cmnd 13
                Employee.IdentityNumber = (sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_CMND_13) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_CMND_13).ToString().Trim();
                //Validate IndentityNumber
                if (!string.IsNullOrEmpty(Employee.IdentityNumber) && Employee.IdentityNumber.Length > 15)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài CMT > 15 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.IdentityNumber))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Số CMT chứa ký tự nguy hiểm. ";
                }

                if (!Utils.Utils.ValidateIsNumber(Employee.IdentityNumber))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Số CMT không đúng kiểu số. ";
                }
                if (!string.IsNullOrEmpty(Employee.IdentityNumber) && ListEmployee.Any(u => u.IdentityNumber.ToLower().Trim() == Employee.IdentityNumber.ToLower()))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Số chứng minh thư đã trùng với giáo viên khác. ";
                }

                //if (EmployeeBusiness.All.Any(u => u.IsActive && u.IdentityNumber == Employee.IdentityNumber && u.EmployeeCode != Employee.EmployeeCode))
                //{
                //    Employee.PASS = false;
                //    Employee.Note = Employee.Note + "Số chứng minh thư đã tồn tại. ";
                //}
                //ngay cap cmnd 14
                string IdentityIssuedDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_CAP_CMND_14) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_CAP_CMND_14).ToString();
                Employee.IdentityIssuedDateString = IdentityIssuedDate;
                if (!string.IsNullOrEmpty(IdentityIssuedDate))
                {
                    DateTime Date;
                    if (DateTime.TryParseExact(IdentityIssuedDate, "d/M/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date)
                        || DateTime.TryParseExact(IdentityIssuedDate, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date))
                    {
                        Employee.IdentityIssuedDate = DateTime.Parse(IdentityIssuedDate);
                        if (Employee.IdentityIssuedDate.Value < new DateTime(1900, 1, 1))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày cấp phải lớn hơn hoặc bằng ngày 01/01/1900. ";
                        }
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Ngày cấp không đúng định dạng. ";
                    }
                }
                if (Employee.IdentityIssuedDate.HasValue && Employee.IdentityIssuedDate.Value > DateTime.Now)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Ngày cấp không hợp lệ. ";
                }
                // noi cap cmnd 15
                Employee.IdentityIssuedPlace = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_CAP_CMND_15) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_CAP_CMND_15).ToString();
                if (!string.IsNullOrEmpty(Employee.IdentityIssuedPlace) && Employee.IdentityIssuedPlace.Length > 400)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nơi cấp > 400 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.IdentityIssuedPlace))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nơi cấp chứa ký tự nguy hiểm. ";
                }
                //So dien thoai 16
                string telephone = (sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_16) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_16).ToString();
                if (!String.IsNullOrEmpty(telephone) && !String.IsNullOrWhiteSpace(telephone))
                {
                    Employee.Telephone = telephone;
                }
                else
                {
                    Employee.Telephone = string.Empty;
                }
                if (Employee.Telephone != null && Employee.Telephone.Length > 15)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài số điện thoại > 15 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.Telephone))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Số điện thoại chứa ký tự nguy hiểm. ";
                }
                int n;
                if (!String.IsNullOrEmpty(Employee.Telephone) && !int.TryParse(Employee.Telephone, out n))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Số điện thoại không đúng định dạng. ";
                }
                //So dien thoai di dong 17
                string mobile = (sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_DI_DONG_17) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_DIEN_THOAI_DI_DONG_17).ToString();
                if (!String.IsNullOrEmpty(mobile) && !String.IsNullOrWhiteSpace(mobile))
                {
                    Employee.Mobile = mobile;
                }
                else
                {
                    Employee.Mobile = string.Empty;
                }
                if (Employee.Mobile != null && Employee.Mobile.Length > 15)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài SĐT Di động > 15 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.Mobile))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "SĐT Di động chứa ký tự nguy hiểm. ";
                }

                if (!String.IsNullOrEmpty(Employee.Mobile) && !Utils.Utils.CheckMobileNumber(Employee.Mobile))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "SĐT Di động không đúng định dạng. ";
                }
                //email 18
                Employee.Email = (sheet.GetCellValue(startRow, ExportEmployeeConstants.EMAIL_18) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.EMAIL_18).ToString().Trim();
                if (!string.IsNullOrEmpty(Employee.Email) && Employee.Email.Length > 100)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài Email > 100 ký tự. ";
                }

                if (!string.IsNullOrEmpty(Employee.Email) && Utils.Utils.IsDangerousString(Employee.Email))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Email chứa ký tự nguy hiểm. ";
                }
                if (!string.IsNullOrEmpty(Employee.Email) && !Utils.Utils.IsValidEmail(Employee.Email))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Email không đúng định dạng. ";
                }

                // So so bao hiem 19
                Employee.InsuranceNumber = (sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_BHXH_19) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.SO_BHXH_19).ToString().Trim();
                if (Employee.InsuranceNumber != null && Employee.InsuranceNumber.Length > 15)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài số sổ bảo hiểm xã hội > 15 ký tự. ";
                }


                //que quan 20
                Employee.HomeTown = (sheet.GetCellValue(startRow, ExportEmployeeConstants.QUE_QUAN_20) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.QUE_QUAN_20).ToString();
                if (!string.IsNullOrEmpty(Employee.HomeTown) && Employee.HomeTown.Length > 400)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài quê quán > 400 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.HomeTown))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Quê quán chứa ký tự nguy hiểm. ";
                }

                // Tinh/thanh 21
                Employee.ProvinceName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TINH_THANH_21) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TINH_THANH_21).ToString();
                if (!string.IsNullOrEmpty(Employee.ProvinceName))
                {
                    Province objProvince = lstProvince.Where(o => o.ProvinceName == Employee.ProvinceName || o.ProvinceCode == Employee.ProvinceName).FirstOrDefault();
                    if (objProvince != null)
                    {
                        Employee.ProvinceId = objProvince.ProvinceID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Tỉnh/thành phố không tồn tại. ";
                    }
                }


                // Quan/Huyen 22
                Employee.DistrictName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.QUAN_HUYEN_22) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.QUAN_HUYEN_22).ToString();
                if (!string.IsNullOrEmpty(Employee.DistrictName))
                {
                    District objDistrict = lstDistrict.Where(o => (o.DistrictName == Employee.DistrictName || o.DistrictCode == Employee.DistrictName) && o.ProvinceID == Employee.ProvinceId).FirstOrDefault();
                    if (objDistrict != null)
                    {
                        Employee.DistrictId = objDistrict.DistrictID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Quận/huyện không tồn tại. ";
                    }
                }

                // Quan/Huyen 23
                Employee.CommuneName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.XA_PHUONG_23) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.XA_PHUONG_23).ToString();
                if (!string.IsNullOrEmpty(Employee.CommuneName))
                {
                    Commune objCommune = lstCommune.FirstOrDefault(o => (o.CommuneName == Employee.CommuneName || o.CommuneCode == Employee.CommuneName) && o.DistrictID == Employee.DistrictId);
                    if (objCommune != null)
                    {
                        Employee.CommuneId = objCommune.CommuneID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Xã/phường không tồn tại. ";
                    }
                }


                //dia chi thuong tru 24
                Employee.PermanentResidentalAddress = (sheet.GetCellValue(startRow, ExportEmployeeConstants.DIA_CHI_THUONG_TRU_24) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.DIA_CHI_THUONG_TRU_24).ToString();
                if (Employee.PermanentResidentalAddress != null && Employee.PermanentResidentalAddress.Length > 400)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài địa chỉ thường trú > 400 ký tự. ";
                }

                if (Utils.Utils.IsDangerousString(Employee.PermanentResidentalAddress))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Địa chỉ chứa ký tự nguy hiểm. ";
                }
                //suc khoe 25
                Employee.HealthStatus = (sheet.GetCellValue(startRow, ExportEmployeeConstants.SUC_KHOE_25) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.SUC_KHOE_25).ToString();
                if (Employee.HealthStatus != null && Employee.HealthStatus.Length > 400)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài sức khỏe > 400 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.HealthStatus))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Sức khỏe chứa ký tự nguy hiểm. ";
                }
                //doan vien 26
                string IsYouthLeageMemberData = (sheet.GetCellValue(startRow, ExportEmployeeConstants.DOAN_VIEN_26) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.DOAN_VIEN_26).ToString();
                if (IsYouthLeageMemberData.ToUpper() == "X")
                {
                    Employee.IsYouthLeageMember = true;
                }
                else if (IsYouthLeageMemberData.ToUpper() == "")
                {
                    Employee.IsYouthLeageMember = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Đoàn viên không hợp lệ ";
                }
                // dang vien 27
                string IsCommunistPartyMemberData = (sheet.GetCellValue(startRow, ExportEmployeeConstants.DANG_VIEN_27) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.DANG_VIEN_27).ToString();
                if (IsCommunistPartyMemberData.ToUpper() == "X")
                {
                    Employee.IsCommunistPartyMember = true;
                }
                else if (IsCommunistPartyMemberData.ToUpper() == "")
                {
                    Employee.IsCommunistPartyMember = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Đảng viên không hợp lệ ";
                }
                //cong doan vien 38
                string IsSyndicateData = (sheet.GetCellValue(startRow, ExportEmployeeConstants.CONG_DOAN_VIEN_28) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.CONG_DOAN_VIEN_28).ToString();
                if (IsSyndicateData.ToUpper() == "X")
                {
                    Employee.IsSyndicate = true;
                }
                else if (IsSyndicateData.ToUpper() == "")
                {
                    Employee.IsSyndicate = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Công đoàn viên không hợp lệ ";
                }
                //Thanh phan gia dinh 25
                Employee.FamilyTypeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.THANH_PHAN_GD_29) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.THANH_PHAN_GD_29).ToString();
                if (Employee.FamilyTypeName != null)
                {
                    familyType = LstFamilyType.Where(o => o.Resolution == Employee.FamilyTypeName).FirstOrDefault();
                    if (familyType != null)
                    {
                        Employee.FamilyTypeID = familyType.FamilyTypeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Thành phần gia đình không tồn tại. ";
                    }
                }

                //Ho ten bo 26
                Employee.FatherFullName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_TEN_BO_30) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_TEN_BO_30).ToString();
                if (Employee.FatherFullName != null && Employee.FatherFullName.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài họ tên bố > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.FatherFullName))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Họ tên bố chứa ký tự nguy hiểm. ";
                }
                int result = 0;
                //Nam sinh bo 27
                string fatherDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_BO_31) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_BO_31).ToString();
                if (fatherDate != null && fatherDate.Trim() != "")
                {
                    if (int.TryParse(fatherDate, out result) && fatherDate.Trim().Length == 4)
                    {
                        Employee.FatherBirthDateStr = "01/01/" + fatherDate;
                        Employee.FatherBirthDate = DateTime.Parse(Employee.FatherBirthDateStr);
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Năm sinh bố không hợp lệ. ";
                    }
                }

                // nghe nghiep bo 28
                Employee.FatherJob = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_BO_32) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_BO_32).ToString();
                if (Employee.FatherJob != null && Employee.FatherJob.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nghề nghiệp bố > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.FatherJob))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nghề nghiệp bố chứa ký tự nguy hiểm. ";
                }
                //noi lam viec bo 29
                Employee.FatherWorkingPlace = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_BO_33) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_BO_33).ToString();
                if (Employee.FatherWorkingPlace != null && Employee.FatherWorkingPlace.Length > 800)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nơi làm việc của bố > 800 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.FatherWorkingPlace))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nơi làm việc bố chứa ký tự nguy hiểm. ";
                }
                //ho ten me 30
                Employee.MotherFullName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_TEN_ME_34) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_TEN_ME_34).ToString();
                if (Employee.MotherFullName != null && Employee.MotherFullName.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài họ tên mẹ > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.MotherFullName))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Họ tên mẹ chứa ký tự nguy hiểm. ";
                }
                // nam sinh me 31
                string motherDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_ME_35) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_ME_35).ToString();
                if (motherDate != null && motherDate.Trim() != "")
                {
                    if (int.TryParse(motherDate, out result) && motherDate.Trim().Length == 4)
                    {
                        Employee.MotherBirthDateStr = "01/01/" + motherDate;
                        Employee.MotherBirthDate = DateTime.Parse(Employee.MotherBirthDateStr);
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Năm sinh mẹ không hợp lệ. ";
                    }
                }
                // nghe nghiep me 32
                Employee.MotherJob = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_ME_36) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_ME_36).ToString();
                if (Employee.MotherJob != null && Employee.MotherJob.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nghề nghiệp mẹ > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.MotherJob))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nghề nghiệp mẹ chứa ký tự nguy hiểm. ";
                }
                // noi lam viec me 33
                Employee.MotherWorkingPlace = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_ME_37) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_ME_37).ToString();
                if (Employee.MotherWorkingPlace != null && Employee.MotherWorkingPlace.Length > 800)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nơi làm việc của mẹ > 800 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.MotherWorkingPlace))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nơi làm việc của mẹ chứa ký tự nguy hiểm. ";
                }
                //ho ten vc 34
                Employee.SpouseFullName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_VA_TEN_VC_38) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.HO_VA_TEN_VC_38).ToString();
                if (Employee.SpouseFullName != null && Employee.SpouseFullName.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài họ tên vợ/chồng > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.SpouseFullName))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Họ tên vợ/chồng chứa ký tự nguy hiểm. ";
                }
                //nam sinh vc 35
                string spouserDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_VC_39) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NAM_SINH_VC_39).ToString();
                if (spouserDate != null && spouserDate.Trim() != "")
                {
                    if (int.TryParse(spouserDate, out result) && spouserDate.Trim().Length == 4)
                    {
                        Employee.SpouseBirthDateStr = "01/01/" + spouserDate;
                        Employee.SpouseBirthDate = DateTime.Parse(Employee.SpouseBirthDateStr);
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Năm sinh vợ/chồng không hợp lệ. ";
                    }
                }
                // nghe nghiep vc 36
                Employee.SpouseJob = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_VC_40) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGHE_NGHIEP_VC_40).ToString();
                if (Employee.SpouseJob != null && Employee.SpouseJob.Length > 200)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nghề nghiệp vợ/chồng > 200 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.SpouseJob))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nghề nghiệp vợ/chồng chứa ký tự nguy hiểm. ";
                }
                // noi lam viec vc 37
                Employee.SpouseWorkingPlace = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_VC_41) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.NOI_LAM_VIEC_VC_41).ToString();
                if (Employee.SpouseWorkingPlace != null && Employee.SpouseWorkingPlace.Length > 800)
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Độ dài nơi làm việc của vợ/chồng > 800 ký tự. ";
                }
                if (Utils.Utils.IsDangerousString(Employee.SpouseWorkingPlace))
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Nơi làm việc của vợ/chồng chứa ký tự nguy hiểm. ";
                }
                // trinh do dao tao 38
                Employee.TrainingLevelName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_CMNV_CAONHAT_42) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_CMNV_CAONHAT_42).ToString();
                if (Employee.TrainingLevelName != null)
                {
                    trainingLevel = LstTrainingLevel.Where(o => o.Resolution == Employee.TrainingLevelName).FirstOrDefault();
                    if (trainingLevel != null)
                    {
                        Employee.TrainingLevelID = trainingLevel.TrainingLevelID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ đào tạo không tồn tại. ";
                    }
                }
                //Chuyen nganh dao tao .
                Employee.SpecialityCatName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.CHUYEN_NGANH_DAO_TAO_43) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.CHUYEN_NGANH_DAO_TAO_43).ToString();
                if (Employee.SpecialityCatName != null)
                {
                    specialityCat = LstSpecialityCat.Where(o => o.Resolution == Employee.SpecialityCatName).FirstOrDefault();
                    if (specialityCat != null)
                    {
                        Employee.SpecialityCatID = specialityCat.SpecialityCatID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Chuyên ngành đào tạo không tồn tại. ";
                    }
                }
                //Trinh do van hoa 40
                Employee.QualificationLevelName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_VAN_HOA_44) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_VAN_HOA_44).ToString();
                if (Employee.QualificationLevelName != null)
                {
                    ql = LstQualificationLevel.Where(o => o.Resolution == Employee.QualificationLevelName).FirstOrDefault();
                    if (ql != null)
                    {
                        Employee.QualificationLevelID = ql.QualificationLevelID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ văn hóa không tồn tại. ";
                    }
                }
                //Trinh do ngoai ngu 41
                Employee.ForeignLanguageGradeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_NGOAI_NGU_45) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_NGOAI_NGU_45).ToString();
                if (Employee.ForeignLanguageGradeName != null)
                {
                    foreignLanguageGrade = LstForeignLanguageGrade.Where(o => o.Resolution == Employee.ForeignLanguageGradeName).FirstOrDefault();
                    if (foreignLanguageGrade != null)
                    {
                        Employee.ForeignLanguageGradeID = foreignLanguageGrade.ForeignLanguageGradeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ ngoại ngữ không tồn tại. ";
                    }
                }
                //Trinh do tin hoc 42
                Employee.ITQualificationLevelName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_TIN_HOC_46) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_TIN_HOC_46).ToString();
                if (Employee.ITQualificationLevelName != null)
                {
                    iTQualificationLevel = LstITQualificationLevel.Where(o => o.Resolution == Employee.ITQualificationLevelName).FirstOrDefault();
                    if (iTQualificationLevel != null)
                    {
                        Employee.ITQualificationLevelID = iTQualificationLevel.ITQualificationLevelID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ tin học không tồn tại. ";
                    }
                }
                //Trinh do ly luan chinh tri 43
                Employee.PoliticalGradeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_LY_LUAN_CT_47) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_LY_LUAN_CT_47).ToString();
                if (Employee.PoliticalGradeName != null)
                {
                    politicalGrade = LstPoliticalGrade.Where(o => o.Resolution == Employee.PoliticalGradeName).FirstOrDefault();
                    if (politicalGrade != null)
                    {
                        Employee.PoliticalGradeID = politicalGrade.PoliticalGradeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ lý luận chính trị không tồn tại. ";
                    }
                }
                //Trinh do quan ly nha nuoc 44
                Employee.StateManagementGradeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLNN_48) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLNN_48).ToString();
                if (Employee.StateManagementGradeName != null)
                {
                    stateManagementGrade = LstStateManagementGrade.Where(o => o.Resolution == Employee.StateManagementGradeName).FirstOrDefault();
                    if (stateManagementGrade != null)
                    {
                        Employee.StateManagementGradeID = stateManagementGrade.StateManagementGradeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ quản lý nhà nước không tồn tại. ";
                    }
                }
                //Trinh do quan ly giao duc 45
                Employee.EducationalManagementGradeName = (sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLGD_49) == null) ? null : sheet.GetCellValue(startRow, ExportEmployeeConstants.TRINH_DO_QLGD_49).ToString();
                if (Employee.EducationalManagementGradeName != null)
                {
                    educationalManagementGrade = LstEducationalManagementGrade.Where(o => o.Resolution == Employee.EducationalManagementGradeName).FirstOrDefault();
                    if (educationalManagementGrade != null)
                    {
                        Employee.EducationalManagementGradeID = educationalManagementGrade.EducationalManagementGradeID;
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Trình độ quản lý giáo dục không tồn tại. ";
                    }
                }
                //Chuyen trach doan doi 46
                string dedicatedForYoungLeague = (sheet.GetCellValue(startRow, ExportEmployeeConstants.CHUYEN_TRACH_DD_50) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.CHUYEN_TRACH_DD_50).ToString();
                if (dedicatedForYoungLeague.ToUpper() == "X")
                {
                    Employee.DedicatedForYoungLeague = true;
                }
                else if (dedicatedForYoungLeague.ToUpper() == "")
                {
                    Employee.DedicatedForYoungLeague = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Chuyên trách đoàn đội không hợp lệ ";
                }
                //Boi duong thuong xuyen 47
                string regularRefresher = (sheet.GetCellValue(startRow, ExportEmployeeConstants.BOI_DUONG_TX_51) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.BOI_DUONG_TX_51).ToString();
                if (regularRefresher.ToUpper() == "X")
                {
                    Employee.RegularRefresher = true;
                }
                else if (regularRefresher.ToUpper() == "")
                {
                    Employee.RegularRefresher = false;
                }
                else
                {
                    Employee.PASS = false;
                    Employee.Note = Employee.Note + "Bồi dưỡng thường xuyên không hợp lệ ";
                }
                //Ngay vao nghe 48
                string StartingDate = (sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_NGHE_52) == null) ? "" : sheet.GetCellValue(startRow, ExportEmployeeConstants.NGAY_VAO_NGHE_52).ToString();
                Employee.StartingDateString = StartingDate;
                if (StartingDate != "")
                {
                    DateTime Date;
                    if (DateTime.TryParseExact(StartingDate, "d/M/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date)
                        || DateTime.TryParseExact(StartingDate, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out Date))
                    {
                        Employee.StartingDate = DateTime.Parse(StartingDate);

                        if (Employee.StartingDate.Value < new DateTime(1900, 1, 1))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào nghề phải lớn hơn hoặc bằng ngày 01/01/1900. ";
                        }
                        else if (Employee.StartingDate.Value > DateTime.Now)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào nghề phải nhỏ hơn ngày hiện tại. ";
                        }
                        else if (Employee.IntoSchoolDate.HasValue && Employee.StartingDate.Value > Employee.IntoSchoolDate.Value)
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào nghề phải nhỏ hơn ngày vào trường. ";
                        }
                        else if (Employee.BirthDate.HasValue && Employee.StartingDate.Value.Year < (Employee.BirthDate.Value.Year + 16))
                        {
                            Employee.PASS = false;
                            Employee.Note = Employee.Note + "Ngày vào nghề phải lớn hơn hoặc bằng ngày 01/01/ " + (Employee.BirthDate.Value.Year + 16) + ". ";
                        }
                    }
                    else
                    {
                        Employee.PASS = false;
                        Employee.Note = Employee.Note + "Ngày vào nghề không đúng định dạng. ";
                    }
                }
                startRow++;
                #endregion
                ListEmployee.Add(Employee);
            }
            if (!ListEmployee.Any(o => o.PASS == false))
            {
                //không có lỗi thì hệ thống thực hiện giống chức năng nhập điểm môn tính điểm
                //Insert vao database
                ImportTeacher(ListEmployee);
                //return Json(new { success = 1 });
                //Chiendd: 14/03/2015, sua thong bao import thanh cong n/m giáo viên
                string retMsg = String.Format("{0} {1}/{2} giáo viên", Res.Get("Common_Label_ImportSuccessMessage"), ListEmployee.Count, ListEmployee.Count);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            else
            {
                //Có lỗi thì hệ thống hiển thị màn hình Lựa chọn thao tác import
                //Gan list vao Session
                Session["ListEmployee"] = ListEmployee;
                return Json(new JsonMessage("", JsonMessage.ERROR));
            }
        }


        //[ValidateAntiForgeryToken]
        private bool ImportTeacher(List<ImportTeacherViewModel> ListTeacher)
        {
            GlobalInfo global = new GlobalInfo();

            List<Employee> LstEmployee = EmployeeBusiness.SearchTeacher(global.SchoolID.Value, new Dictionary<string, object>()
                    {
                        {"AcademicYearID",global.AcademicYearID},
                        {"CurrentEmployeeStatus",SystemParamsInFile.EMPLOYMENT_STATUS_WORKING}
                    }).ToList();

            Employee ee = new Employee();
            List<Employee> lstImportTeacher = new List<Employee>();
            List<Employee> lstUpdateImportTeacher = new List<Employee>();

            foreach (var item in ListTeacher)
            {
                if (item.PASS == true)
                {
                    Employee employee = new Employee();
                    Utils.Utils.BindTo(item, employee, true, false);
                    Utils.Utils.TrimObject(employee);
                    employee.EmployeeType = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                    employee.SchoolID = global.SchoolID;
                    if (ModelState.IsValid)
                    {
                        ee = LstEmployee.Where(o => o.EmployeeCode == item.EmployeeCode).FirstOrDefault();
                        if (ee != null) //Sua
                        {
                            Utils.Utils.BindTo(item, ee, true, false);
                            ee.SchoolID = global.SchoolID;
                            ee.IsActive = true;
                            ee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                            ee.EmployeeType = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                            lstUpdateImportTeacher.Add(ee);
                        }
                        else //Them
                        {
                            employee.SchoolID = global.SchoolID;
                            employee.IsActive = true;
                            employee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                            employee.EmployeeType = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                            lstImportTeacher.Add(employee);
                        }
                    }
                }
            }

            try
            {

                EmployeeBusiness.SetAutoDetectChangesEnabled(false);
                if (lstImportTeacher.Count() > 0)
                    this.EmployeeBusiness.ImportTeacher(lstImportTeacher);

                if (lstUpdateImportTeacher.Count() > 0)
                    this.EmployeeBusiness.ImportUpdateTeacher(lstUpdateImportTeacher);

                this.EmployeeBusiness.Save();
                return true;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                return false;
            }
            finally
            {
                EmployeeBusiness.SetAutoDetectChangesEnabled(true);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            List<ImportTeacherViewModel> ListEmployee = (List<ImportTeacherViewModel>)Session["ListEmployee"];
            int countVal = (ListEmployee == null) ? 0 : ListEmployee.Count;
            if (countVal == 0)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_ImportErrorMessage")));
            }
            int countValImport = ListEmployee.Where(c => c.PASS == true).Count();
            bool retVal = ImportTeacher(ListEmployee);
            if (retVal)
            {
                string retMsg = String.Format("{0} {1}/{2} giáo viên", Res.Get("Common_Label_ImportSuccessMessage"), countValImport, countVal);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_ImportErrorMessage")));
        }

        public PartialViewResult ViewDataImportExcel()
        {
            //Đưa ra list thường xuyên
            List<ImportTeacherViewModel> ListEmployee = (List<ImportTeacherViewModel>)Session["ListEmployee"];
            //Đưa thông tin lên MarkRecord_Label_Message            
            ViewData[ImportTeacherConstants.LIST_Employee] = ListEmployee;
            return PartialView("ViewDataImport");
        }

        private void ValidateImportedTeacher(ImportTeacherViewModel teacher)
        {
            ValidationContext vc = new ValidationContext(teacher, null, null);

            List<string> messages = new List<string>();
            foreach (PropertyInfo pi in teacher.GetType().GetProperties())
            {
                string resKey = string.Empty;
                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is DisplayAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }

                    if (att is DisplayNameAttribute)
                    {
                        resKey = ((DisplayNameAttribute)att).DisplayName;
                        break;
                    }

                }

                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is ValidationAttribute)
                    {
                        ValidationAttribute attV = (ValidationAttribute)att;
                        vc.MemberName = pi.Name;

                        if (attV.GetValidationResult(pi.GetValue(teacher, null), vc) != ValidationResult.Success)
                        {
                            List<object> Params = new List<object>();
                            Params.Add(Res.Get(resKey));

                            if (att is StringLengthAttribute)
                            {
                                StringLengthAttribute attS = (StringLengthAttribute)att;
                                Params.Add(attS.MaximumLength);
                            }

                            messages.Add(string.Format(Res.Get(attV.ErrorMessageResourceName), Params.ToArray()));
                        }
                    }
                }
            }

            if (messages.Count > 0)
            {
                teacher.PASS = false;
                teacher.Note += string.Join("\n-", messages);
            }
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

    }
}
