﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ImportTeacherArea.Models
{
    public class ImportTeacherViewModel
    {                        
        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string EmployeeCode { get; set; }


        public int EmployeeType { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string Name { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string FullName { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }    
            
        [ResourceDisplayName("Employee_Label_BirthDate")]
        public string BirthDateString { get; set; }
            
        [ResourceDisplayName("Employee_Label_Mobile")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string Mobile { get; set; }

        [ResourceDisplayName("Employee_Label_Telephone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Telephone { get; set; }

        [ResourceDisplayName("Employee_Label_Email")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Email { get; set; }

        [ResourceDisplayName("Employee_Label_PermanentResidentalAddress")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PermanentResidentalAddress { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        public string GenreName { get; set; }
        
        public bool Genre { get; set; }
                        
        [ResourceDisplayName("Employee_Label_HealthStatus")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string HealthStatus { get; set; }        

        [ResourceDisplayName("Employee_Label_StartingDate")]
        public Nullable<System.DateTime> StartingDate { get; set; }

        [ResourceDisplayName("Employee_Label_StartingDate")]
        public string StartingDateString { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityNumber")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string IdentityNumber { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedDate")]
        public Nullable<System.DateTime> IdentityIssuedDate { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedDate")]
        public string IdentityIssuedDateString { get; set; }

        [ResourceDisplayName("Employee_Label_IdentityIssuedPlace")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string IdentityIssuedPlace { get; set; }

        [ResourceDisplayName("Employee_Label_HomeTown")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string HomeTown { get; set; }
              
        public Nullable<int> EthnicID { get; set; }
        [ResourceDisplayName("Employee_Label_Ethnic")]
        public string EthnicName { get; set; }
        public Nullable<int> ReligionID { get; set; }
        [ResourceDisplayName("Employee_Label_Religion")]
        public string ReligionName { get; set; }        
        public Nullable<int> ContractID { get; set; }
        [ResourceDisplayName("Employee_Label_ContractName")]
        public string ContractName { get; set; }        
        
        public Nullable<int> QualificationLevelID { get; set; }
        [ResourceDisplayName("Employee_Label_QualificationLevel")]
        public string QualificationLevelName { get; set; }
        
        public Nullable<int> WorkTypeID { get; set; }
        [ResourceDisplayName("Employee_Label_WorkType")]
        public string WorkTypeName { get; set; }        

        public Nullable<int> WorkGroupTypeID { get; set; }
        public Nullable<int> SchoolFacultyID { get; set; }
        [ResourceDisplayName("Employee_Label_FacultyName")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("Employee_Label_IntoSchoolDate")]
        public System.DateTime? IntoSchoolDate { get; set; }

        [ResourceDisplayName("Employee_Label_IntoSchoolDate")]
        public string IntoSchoolDateString { get; set; }

         [ResourceDisplayName("Employee_Label_AppliedLevel")]
        public int? AppliedLevel { get; set; }

        [ResourceDisplayName("Employee_Label_AppliedLevel")]
        public string AppliedLevelName 
        {
            get
            {
                switch (this.AppliedLevel)
                {
                    case SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_PRIMARY:
                        return "Cấp 1";
                    case SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_SECONDARY:
                        return "Cấp 2";
                    case SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_TERTIARY:
                        return "Cấp 3";
                    case SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE:
                        return "Nhà trẻ";
                    case SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN:
                        return "Mẫu giáo";
                    default:
                        return "";
                }
            }
        }

        public Nullable<int> ContractTypeID { get; set; }
        [ResourceDisplayName("Employee_Label_Contract")]
        public string ContractTypeName { get; set; }

        public Boolean PASS { get; set; }
        public string Note { get; set; }

        public bool IsYouthLeageMember { get; set; }
        public bool IsCommunistPartyMember { get; set; }
        public bool IsSyndicate { get; set; }
        public string FatherFullName { get; set; }
        public DateTime? FatherBirthDate { get; set; }
        public string FatherBirthDateStr { get; set; }
        public string FatherMobile { get; set; }
        public string FatherJob { get; set; }
        public string FatherWorkingPlace { get; set; }

        public string MotherFullName { get; set; }
        public DateTime? MotherBirthDate { get; set; }
        public string MotherBirthDateStr { get; set; }
        public string MotherMobile { get; set; }
        public string MotherJob { get; set; }
        public string MotherWorkingPlace { get; set; }

        public string SpouseFullName { get; set; }
        public DateTime? SpouseBirthDate { get; set; }
        public string SpouseBirthDateStr { get; set; }
        public string SpouseMobile { get; set; }
        public string SpouseJob { get; set; }
        public string SpouseWorkingPlace { get; set; }


        public int FamilyTypeID { get; set; }
        public string FamilyTypeName { get; set; }

        public int TrainingLevelID { get; set; }
        public string TrainingLevelName { get; set; }

        public int SpecialityCatID { get; set; }
        public string SpecialityCatName { get; set; }

        public int ForeignLanguageGradeID { get; set; }
        public string ForeignLanguageGradeName { get; set; }

        public int ITQualificationLevelID { get; set; }
        public string ITQualificationLevelName { get; set; }

        public int PoliticalGradeID { get; set; }
        public string PoliticalGradeName { get; set; }

        public int StateManagementGradeID { get; set; }
        public string StateManagementGradeName { get; set; }

        public int EducationalManagementGradeID { get; set; }
        public string EducationalManagementGradeName { get; set; }

        public bool DedicatedForYoungLeague { get; set; }
        public bool RegularRefresher { get; set; }

        public string ProvinceName { get; set; }
        public string DistrictName { get; set; }
        public string CommuneName { get; set; }
        /// <summary>
        /// So so bao hiem
        /// </summary>
        public string InsuranceNumber { get; set; }
        public Nullable<int> ProvinceId { get; set; }
        public Nullable<int> DistrictId { get; set; }
        public Nullable<int> CommuneId { get; set; }

    }
}

