﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportTeacherArea
{
    public class ImportTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportTeacherArea_default",
                "ImportTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
