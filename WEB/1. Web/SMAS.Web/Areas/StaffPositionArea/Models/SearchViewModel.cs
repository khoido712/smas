﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.StaffPositionArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("StaffPosition_Label_Resolution")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }
    }
}