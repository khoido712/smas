﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StaffPositionArea
{
    public class StaffPositionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StaffPositionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StaffPositionArea_default",
                "StaffPositionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
