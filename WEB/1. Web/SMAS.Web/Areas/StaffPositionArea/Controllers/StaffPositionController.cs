﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.StaffPositionArea.Models;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.StaffPositionArea.Controllers
{
    public class StaffPositionController : BaseController
    {
        private readonly IStaffPositionBusiness StaffPositionBusiness;

        public StaffPositionController(IStaffPositionBusiness StaffPositionBusiness)
        {
            this.StaffPositionBusiness = StaffPositionBusiness;
        }
        //
        // GET: /StaffPositionArea/StaffPosition/

        #region Index
        // GET: /StaffPosition/  
        public ActionResult Index()
        {
            SetViewDataPermission("StaffPosition", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //SearchInfo["IsActive"] = true;            
            //Get view data here

            IEnumerable<StaffPositionModel> lst = this._Search(SearchInfo);
            ViewData[StaffPositionConstants.LIST_STAFF] = lst.ToList().OrderBy(o => o.Resolution).ToList();
            return View();
        }

        #endregion

        #region search
        // GET: /StaffPosition/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Resolution"] = frm.Resolution;
            //add search info
            //

            IEnumerable<StaffPositionModel> lst = this._Search(SearchInfo);
            ViewData[StaffPositionConstants.LIST_STAFF] = lst.ToList().OrderBy(o => o.Resolution).ToList();

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        /// 
        
        private IEnumerable<StaffPositionModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("StaffPosition", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<StaffPosition> query = this.StaffPositionBusiness.Search(SearchInfo);
            IQueryable<StaffPositionModel> lst = query.Select(o => new StaffPositionModel
            {
                StaffPositionID = o.StaffPositionID,
                Resolution = o.Resolution


            });

            return lst.OrderBy(o => o.Resolution).ToList();
        }

        #endregion

        #region Create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("StaffPosition", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            StaffPosition StaffPosition = new StaffPosition();
            TryUpdateModel(StaffPosition);
            StaffPosition.IsActive = true;
            Utils.Utils.TrimObject(StaffPosition);

            this.StaffPositionBusiness.Insert(StaffPosition);
            this.StaffPositionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int StaffPositionID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("StaffPosition", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            StaffPosition StaffPosition = this.StaffPositionBusiness.Find(StaffPositionID);
            TryUpdateModel(StaffPosition);
            Utils.Utils.TrimObject(StaffPosition);
            this.StaffPositionBusiness.Update(StaffPosition);
            this.StaffPositionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("StaffPosition", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.StaffPositionBusiness.Delete(id);
            this.StaffPositionBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Detail

        public PartialViewResult Detail(int id)
        {
            StaffPosition StaffPosition = this.StaffPositionBusiness.Find(id);
            StaffPositionModel frm = new StaffPositionModel();
            Utils.Utils.BindTo(StaffPosition, frm);
            return PartialView("_Detail", frm);
        }
        #endregion

    }
}
