﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SkillManagementArea
{
    public class SkillManagementConstants
    {
       
        public const string LISTEDUCATIONLEVEL = "LISTEDUCATIONLEVEL";
        public const string LISTCLASS = "LISTCLASS";
        public const string LIST_SKILLTITLE = "LIST_SKILLTITLE";
        public const string LIST_SKILLTITLEID_IN_ROLE = "LIST_SKILLTITLEID_IN_ROLE";
        public const string LIST_SKILL_TYPE = "LIST_SKILL_TYPE";
        public const string LIST_SKILL_TYPE_ITEM = "LIST_SKILL_TYPE_ITEM";
        public const string SEARCHFORM = "SEARCHFORM";

        public const string VISIBLE_ORDER = "VISIBLE_ORDER";

        public const string PERVIOUSGRADUATION_LEVEL1 = "PERVIOUSGRADUATION_LEVEL1";

        public const string ENABLE_PAGING = "ENABLE_PAGING";
        public const string PUPIL_NOT_STUDY = "PUPIL_NOT_STUDY";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
    }
}