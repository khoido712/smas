﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class DataViewModel
    {
        public int? SkillTitleID { get; set; }

        [ResourceDisplayName("SkillManagement_Label_Subject")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string SkillTitle { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_Note")]
        public string Description { get; set; }

        [ResourceDisplayName("Common_Label_Time")]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = "Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [AdditionalMetadata("OnChange", "GetToDate")]
        public System.DateTime? FromDate { get; set; }

        [ResourceDisplayName("ActivityPlan_Label_ToTime")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$", ErrorMessage = "Dữ liệu nhập vào không đúng định dạng ngày tháng")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public System.DateTime? ToDate { get; set; }

        public List<SkillViewModel> ListSkill { get; set; }

        public System.DateTime? CreatedDate { get; set; }
        public System.DateTime? ModifiedDate { get; set; }

        public List<int> lstClassApplied { get; set; }
        public List<int> lstClassInRole { get; set; }
        public List<EducationLevelItem> ListEducationLevel { get; set; }
        public DataViewModel()
        {
            ListSkill = new List<SkillViewModel>();
            ListEducationLevel = new List<EducationLevelItem>();
            lstClassApplied = new List<int>();
            lstClassInRole = new List<int>();
        }
    }
}