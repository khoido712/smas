﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class SkillTitleViewModel
    {
        [ResourceDisplayName("Common_Column_Order")]
        public int? STT { get; set; }

        [ResourceDisplayName("SkillManagement_Label_Subject")]
        public string SkillTitle { get; set; }

        [ResourceDisplayName("ActivityPlan_Label_AppliedTime")]
        public string AppliedTime { get; set; }

        [ResourceDisplayName("ActivityPlan_Label_AppliedClass")]
        public string AppliedClass { get; set; }


        public int SkillTitleID { get; set; }
    }
}