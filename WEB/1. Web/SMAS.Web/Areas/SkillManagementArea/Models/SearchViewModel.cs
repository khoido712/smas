﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Models.CustomAttribute;
using Resources;

namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PupilProfile_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SkillManagementConstants.LISTEDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("PupilRanking_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SkillManagementConstants.LISTCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("SkillManagement_Label_Subject")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Subject { get; set; }

        [ResourceDisplayName("Common_Label_Time")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "ToDate", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        public DateTime? FromDate { get; set; }

        [ResourceDisplayName("-")]
        [DataType(DataType.Date)]
        [UIHint("DateTimePicker")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public DateTime? ToDate { get; set; }
    }
}