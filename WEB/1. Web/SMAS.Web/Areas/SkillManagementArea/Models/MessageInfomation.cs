﻿using System;
using System.Collections.Generic;


namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class MessageInfomation
    {
        public MessageInfomation(int type, string content)
        {
            this.Type = type;
            this.Content = content;
        }
        public int Type { get; set; }
        public string Content { get; set; }
    }
}
