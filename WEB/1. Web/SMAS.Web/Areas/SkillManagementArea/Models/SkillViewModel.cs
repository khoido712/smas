﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;

namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class SkillViewModel
    {
        public int? SkillID { get; set; }
        public int? SkillTitleID { get; set; }
        public string Period { get; set; }
        public string Description { get; set; }
        public System.DateTime? FromDate { get; set; }
        public System.DateTime? ToDate { get; set; }
        public System.DateTime? CreatedDate { get; set; }
        public System.DateTime? ModifiedDate { get; set; }
    }
}