﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using SMAS.Web.Areas.SkillManagementArea.Models;

namespace SMAS.Web.Areas.SkillManagementArea.Models
{
    public class EducationLevelItem
    {
        public int EducationLevelID { get; set; }
        public string Resolution { get; set; }
        public List<ClassProfileItem> LstClass { get; set; }
        public bool? Checked { get; set; }
        public EducationLevelItem()
        {
            LstClass = new List<ClassProfileItem>();
        }
    }
}