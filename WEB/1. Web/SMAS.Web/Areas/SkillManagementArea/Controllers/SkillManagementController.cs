﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SkillManagementArea.Models;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.SkillManagementArea.Controllers
{
    public class SkillManagementController : BaseController
    {
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISkillBusiness SkillBusiness;
        private readonly ISkillOfClassBusiness SkillOfClassBusiness;
        private readonly ISkillTitleBusiness SkillTitleBusiness;
        private readonly ISkillTitleClassBusiness SkillTitleClassBusiness;
        private readonly ISkillOfPupilBusiness SkillOfPupilBusiness;

        public SkillManagementController(IPupilProfileBusiness pupilprofileBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , ISkillBusiness SkillBusiness
            , ISkillOfClassBusiness SkillOfClassBusiness
            , ISkillTitleBusiness SkillTitleBusiness
            , ISkillTitleClassBusiness SkillTitleClassBusiness
            , ISkillOfPupilBusiness SkillOfPupilBusiness
            )
        {
            this.PupilProfileBusiness = pupilprofileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SkillBusiness = SkillBusiness;
            this.SkillTitleBusiness = SkillTitleBusiness;
            this.SkillTitleClassBusiness = SkillTitleClassBusiness;
            this.SkillOfClassBusiness = SkillOfClassBusiness;
            this.SkillOfPupilBusiness = SkillOfPupilBusiness;
        }

        //
        // GET: /SkillManagementArea/SkillManagement/

        public ActionResult Index()
        {
            SetViewData();
            if (Session[SkillManagementConstants.SEARCHFORM] != null)
            {
                SearchViewModel frm = (SearchViewModel)Session[SkillManagementConstants.SEARCHFORM];
                this.Search(frm);
            }
            return View();
        }

        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();
            if (global.IsViewAll || global.IsAdminSchoolRole || global.IsRolePrincipal) // cau hinh xem thong tin toan truong
            {
                List<EducationLevel> lsEducationLevel = global.EducationLevels;
                if (lsEducationLevel != null)
                {
                    ViewData[SkillManagementConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
                }
                else
                {
                    ViewData[SkillManagementConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
                }
            }
            else
            {
                int TeacherID = global.EmployeeID.Value;
                int AcademicYearID = global.AcademicYearID.Value;
                IQueryable<ClassProfile> lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(TeacherID, AcademicYearID).Union(ClassProfileBusiness.GetListClassByOverSeeing(TeacherID, AcademicYearID));
                List<EducationLevel> lsEducationLevel = lstClass.Select(o => o.EducationLevel).Distinct().ToList();
                if (lsEducationLevel != null)
                {
                    ViewData[SkillManagementConstants.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
                }
                else
                {
                    ViewData[SkillManagementConstants.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
                }
            }
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            Session[SkillManagementConstants.SEARCHFORM] = frm;
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search infor
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["Subject"] = frm.Subject;
            SearchInfo["FromDate"] = frm.FromDate;
            SearchInfo["ToDate"] = frm.ToDate;
            SearchInfo["IsActive"] = true;

            List<SkillTitleViewModel> lst = this._Search(SearchInfo);
            ViewData[SkillManagementConstants.LIST_SKILLTITLE] = lst;

            if (global.IsCurrentYear)
            {
                ViewData[SkillManagementConstants.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[SkillManagementConstants.IS_CURRENT_YEAR] = false;
            }
            return PartialView("_ListSkill");
        }

        #region Điều hướng
        public ActionResult RedirectCreatePage()
        {
            GlobalInfo global = new GlobalInfo();
            int schoolId = global.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            if (global.IsAdminSchoolRole == false && global.IsRolePrincipal == false)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(global.EmployeeID.Value, global.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Where(p => p.EducationLevel.Grade == global.AppliedLevel);
            }
            // Lấy về danh sách khối - lớp
            List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().ToList();
            List<int> lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();
            List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
            EducationLevelItem level;
            foreach (var item in lstLevel)
            {
                level = new EducationLevelItem();
                level.EducationLevelID = item.EducationLevelID;
                level.Resolution = item.Resolution;
                List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).ToList();
                foreach (var itm in iqTemp)
                {
                    ClassProfileItem tmp = new ClassProfileItem();

                    lstClsItem.Add(new ClassProfileItem()
                    {
                        ClasslID = itm.ClassProfileID,
                        ClassName = itm.DisplayName,
                        Checked = false
                    });
                }
                level.LstClass = lstClsItem;

                lstLevelItem.Add(level);
            }
            //end

            DataViewModel data = new DataViewModel();
            data.ListEducationLevel = lstLevelItem;
            data.lstClassInRole = lstClassInRole;
            data.ListSkill = new List<SkillViewModel>(); ;
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            return PartialView("_CreateOrEdit", data);
        }
        #endregion

        public PartialViewResult Edit(int SkillTitleID)
        {
            int schoolId = _globalInfo.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            SearchInfo["EmployeeID"] = _globalInfo.EmployeeID;
            IQueryable<ClassProfile> lstClass;
            if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsRolePrincipal == false)
            {
                lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchInfo).Where(p => p.EducationLevel.Grade == _globalInfo.AppliedLevel);
            }
            // Lấy về danh sách khối - lớp
            List<int> lstClassIDInSkillTitle = SkillTitleClassBusiness.GetListClass(SkillTitleID).Select(o => o.ClassProfileID).ToList();
            List<EducationLevel> lstLevel = lstClass.Select(o => o.EducationLevel).Distinct().ToList();
            List<EducationLevelItem> lstLevelItem = new List<EducationLevelItem>();
            EducationLevelItem educationLevelItem;
            foreach (var item in lstLevel)
            {
                educationLevelItem = new EducationLevelItem();
                educationLevelItem.EducationLevelID = item.EducationLevelID;
                educationLevelItem.Resolution = item.Resolution;
                List<ClassProfileItem> lstClsItem = new List<ClassProfileItem>();
                List<ClassProfile> iqTemp = lstClass.Where(o => o.EducationLevelID == item.EducationLevelID).ToList();
                foreach (var itm in iqTemp)
                {
                    lstClsItem.Add(new ClassProfileItem()
                    {
                        ClasslID = itm.ClassProfileID,
                        ClassName = itm.DisplayName,
                        Checked = lstClassIDInSkillTitle.Contains(itm.ClassProfileID)
                    });
                }
                // dem so item duoc check
                int CountItemChecked = lstClsItem.Where(o => o.Checked == true).Count();
                // checked item neu toan bo lop trong khoi duoc check
                if (CountItemChecked == iqTemp.Count)
                {
                    educationLevelItem.Checked = true;
                }
                educationLevelItem.LstClass = lstClsItem;
                lstLevelItem.Add(educationLevelItem);
            }
            //end

            // model for Edit
            DataViewModel data = new DataViewModel();
            data.ListEducationLevel = lstLevelItem;
            data.lstClassInRole = lstClass.Select(o => o.ClassProfileID).ToList();
            SearchInfo["AppliedLevel"] = _globalInfo.AppliedLevel;
            SearchInfo["SkillTitleID"] = SkillTitleID;

            SkillTitle skillTitle = this.SkillTitleBusiness.Find(SkillTitleID);
            data.SkillTitleID = SkillTitleID;
            data.SkillTitle = skillTitle.Title;
            data.Description = skillTitle.Description;
            data.FromDate = skillTitle.FromDate;
            data.ToDate = skillTitle.ToDate;

            List<SkillViewModel> list = new List<SkillViewModel>();
            // Lấy danh sach ky nang liên quan trong SkillOfClass
            if (!(_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)) //  neu ko phai ban quan tri - BGH
            {
                List<SkillOfClass> ListSkillOfClass = this.SkillOfClassBusiness.Search(SearchInfo).ToList();
                SkillOfClass skillOfClass;
                for (int i = 0; i < ListSkillOfClass.Count; i++)
                {
                    skillOfClass = ListSkillOfClass[i];
                    list.Add(new SkillViewModel()
                    {
                        Period = skillOfClass.Period,
                        SkillTitleID = skillOfClass.SkillTitleID,
                        SkillID = skillOfClass.SkillOfClassID,
                        Description = skillOfClass.Description,
                        FromDate = skillOfClass.FromDate,
                        ToDate = skillOfClass.ToDate
                    });
                }
            }

            // nếu ko có lấy trong bang Skill
            if (list.Count == 0)
            {
                List<Skill> ListSkill = this.SkillBusiness.Search(SearchInfo).ToList();
                Skill skill;
                for (int i = 0; i < ListSkill.Count; i++)
                {
                    skill = ListSkill[i];
                    list.Add(new SkillViewModel()
                    {
                        Period = skill.Period,
                        SkillTitleID = skill.SkillTitleID,
                        SkillID = skill.SkillID,
                        Description = skill.Description,
                        FromDate = skill.FromDate,
                        ToDate = skill.ToDate
                    });
                }
            }

            data.ListSkill = list;
            return PartialView("_CreateOrEdit", data);
        }


        public MessageInfomation checkValidate(DataViewModel frm, bool CheckName, bool CheckDate, bool checkPeriod)
        {
            string skillTitle = frm.SkillTitle.Trim();
            DateTime fromDate = frm.FromDate.Value.Date;
            DateTime toDate = frm.ToDate.Value.Date;
            GlobalInfo global = new GlobalInfo();
            int schoolId = global.SchoolID.Value;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            SearchInfo["EmployeeID"] = global.EmployeeID;
            SearchInfo["AppliedLevel"] = global.AppliedLevel.Value;

            List<SkillTitleBO> LstSkillTitle = SkillTitleBusiness.SearchBySchool(schoolId, SearchInfo).ToList();
            List<String> LstSkillTitleName = LstSkillTitle.Select(o => o.SkillTitle).ToList();
            List<DateTime> LstSkillTitleFromDate = LstSkillTitle.Select(o => o.FromDate).ToList();
            List<DateTime> LstSkillTitleToDate = LstSkillTitle.Select(o => o.ToDate).ToList();
            if (CheckName && LstSkillTitleName.Contains(skillTitle))
            {
                return new MessageInfomation(1, "Chủ đề đã tồn tại trong hệ thống");
            }

            int Count = LstSkillTitle.Count;
            DateTime? fDate;
            DateTime? tDate;
            for (int i = 0; i < Count; i++)
            {
                fDate = LstSkillTitleFromDate[i];
                tDate = LstSkillTitleToDate[i];
                if ((fromDate >= fDate && fromDate <= tDate) || (fromDate <= fDate && toDate >= fDate))
                {
                    return new MessageInfomation(2, String.Format("Trong khoảng thời gian {0} – {1} đã có chủ đề {2}", fDate.Value.ToString("dd/MM/yyyy"), tDate.Value.ToString("dd/MM/yyyy"), LstSkillTitle[i].SkillTitle));
                }
            }

            // kiem tra thoi gian cac ky nang chong cheo
            if (checkPeriod)
            {
                List<DateTime?> lstFromDate = frm.ListSkill.Select(o => o.FromDate).ToList();
                List<DateTime?> lstToDate = frm.ListSkill.Select(o => o.ToDate).ToList();
                int countSkill = frm.ListSkill.Count;
                int k = 0;
                DateTime? tmpFDate;
                DateTime? tmpTDate;
                while (k < countSkill)
                {
                    fDate = lstFromDate[k];
                    tDate = lstToDate[k];
                    for (int i = 0; i < countSkill; i++)
                    {
                        if (i != k)
                        {
                            tmpFDate = lstFromDate[i];
                            tmpTDate = lstToDate[i];
                            if ((fDate >= tmpFDate && fDate <= tmpTDate) || (fDate <= tmpFDate && tDate >= tmpFDate))
                            {
                                return new MessageInfomation(3, "Thời gian các kỹ năng không được giao nhau");
                            }
                        }
                    }
                    k++;
                }
            }

            return new MessageInfomation(0, "Ok");
        }
        /// <summary>
        /// sua va them moi
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult InsertORUpdate(DataViewModel frm)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (frm == null)
            {
                return Json(new { Type = "error", Message = "Có lỗi" });
            }
            DateTime createdDate = DateTime.Now;
            int? SkillTitleID = frm.SkillTitleID;
            #region Thêm mới
            if (SkillTitleID == null || !SkillTitleID.HasValue || (SkillTitleID.HasValue && SkillTitleID.Value == 0)) // truong hop them moi
            {
                // validate thong tin
                MessageInfomation result = checkValidate(frm, true, true, true);
                if (result.Type == 1)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                else if (result.Type == 2)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                else if (result.Type == 3)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                // them moi ke hoach ky nang

                string skillTitle = frm.SkillTitle.Trim();
                SkillTitle obj = new SkillTitle();
                obj.Title = skillTitle;
                obj.Description = frm.Description;
                obj.CreatedDate = createdDate;
                obj.FromDate = frm.FromDate.Value;
                obj.AcademicYearID = globalInfo.AcademicYearID.Value;
                obj.SchoolID = globalInfo.SchoolID.Value;
                obj.ToDate = frm.ToDate.Value;
                obj.IsActive = true;
                try
                {
                    SkillTitleBusiness.Insert(obj);
                    SkillTitleBusiness.Save();
                }
                catch (Exception ex)
                {
                    LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate","null", ex);
                    return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                }
                // them moi SkillTitleClass
                int STID = obj.SkillTitleID;
                if (STID <= 0)
                {
                    STID = SkillTitleBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                            {
                                {"Subject", skillTitle},
                                {"Description", obj.Description},
                                {"FromDate", obj.FromDate},
                                {"ToDate", obj.ToDate},
                                {"AcademicYearID", obj.AcademicYearID},
                                {"IsActive", true},
                            }).FirstOrDefault().SkillTitleID;

                }
                List<int> ClassApplied = frm.lstClassApplied;
                List<SkillTitleClass> lstSkillTitleClassIns = new List<SkillTitleClass>();
                for (int i = 0, CountClass = ClassApplied.Count; i < CountClass; i++)
                {
                    if (ClassApplied[i] > 0)
                    {
                        lstSkillTitleClassIns.Add(new SkillTitleClass()
                        {
                            SkillTitleID = STID,
                            ClassID = ClassApplied[i]
                        });
                    }
                }
                try
                {
                    SkillTitleClassBusiness.Insert(lstSkillTitleClassIns);
                    SkillTitleClassBusiness.Save();
                }
                catch (Exception ex)
                {
                    LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                    return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                }
                // them moi vao Skill HOAC SkillOfClass
                if (globalInfo.IsAdminSchoolRole || globalInfo.IsRolePrincipal) // neu bgh - quan tri truong
                {
                    List<SkillViewModel> lstSkillClient = frm.ListSkill;
                    SkillViewModel item;
                    int CountNumAct = lstSkillClient.Count;
                    List<Skill> lstIns = new List<Skill>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstSkillClient[i];
                        lstIns.Add(new Skill()
                        {
                            SkillTitleID = STID,
                            Description = item.Description,
                            Period = item.Period,
                            FromDate = item.FromDate.Value,
                            ToDate = item.ToDate.Value,
                            CreatedDate = createdDate
                        });
                    }
                    try
                    {
                        SkillBusiness.Insert(lstIns);
                        SkillBusiness.Save();
                    }
                    catch (Exception ex)
                    {
                        LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                        return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                    }

                }
                else
                {
                    List<SkillViewModel> lstSkillClient = frm.ListSkill;
                    SkillViewModel item;
                    int CountNumAct = lstSkillClient.Count;
                    List<SkillOfClass> lstIns = new List<SkillOfClass>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstSkillClient[i];
                        lstIns.Add(new SkillOfClass()
                        {
                            SkillTitleID = STID,
                            Description = item.Description,
                            Period = item.Period,
                            FromDate = item.FromDate.Value,
                            ToDate = item.ToDate.Value,
                            CreatedDate = createdDate
                        });
                    }
                    try
                    {
                        SkillOfClassBusiness.Insert(lstIns);
                        SkillOfClassBusiness.Save();
                    }
                    catch (Exception ex)
                    {
                        
                        LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                        return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                    }
                }
                return Json(new { Type = "success", Message = Res.Get("success_create"), Id = STID });
            }
            #endregion
            else // truong hop cap nhat
            #region Cap nhat
            {
                // validate thong tin
                MessageInfomation result = checkValidate(frm, false, false, true);
                if (result.Type == 3)
                {
                    return Json(new { Type = "error", Message = result.Content });
                }
                SkillTitle SkillTitle = SkillTitleBusiness.Find(SkillTitleID.Value);
                // neu quan tri truong duoc quyen thay doi ten plan
                if (globalInfo.IsAdminSchoolRole || globalInfo.IsRolePrincipal)
                {
                    string skillTitle = frm.SkillTitle.Trim();
                    if (!String.IsNullOrEmpty(skillTitle))
                    {
                        SkillTitle.Title = skillTitle;
                        SkillTitle.ModifiedDate = createdDate;
                        SkillTitle.IsActive = true;
                        try
                        {
                            SkillTitleBusiness.Update(SkillTitle);
                            SkillTitleBusiness.Save();
                        }
                        catch (Exception ex)
                        {
                            
                            LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                            return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                        }
                    }
                }

                // ap dung tren cac lop 
                List<int> ClassApplied = frm.lstClassApplied;
                List<int> ClassInRole = frm.lstClassInRole;
                List<int> ClassNotChecked = ClassInRole.Except(ClassApplied).ToList();
                List<int> lstClassIDInPlan = SkillTitleClassBusiness.GetListClass(SkillTitleID.Value).Select(o => o.ClassProfileID).ToList();

                // 
                List<int> lstClassINS = ClassApplied.Except(lstClassIDInPlan).ToList();
                List<int> lstClassDel = lstClassIDInPlan.Intersect(ClassNotChecked).ToList();
                List<SkillTitleClass> lstSkillTitleClassIns = new List<SkillTitleClass>();
                for (int i = 0, countClass = lstClassINS.Count; i < countClass; i++)
                {
                    if (lstClassINS[i] > 0)
                    {
                        lstSkillTitleClassIns.Add(new SkillTitleClass()
                        {
                            SkillTitleID = SkillTitleID.Value,
                            ClassID = lstClassINS[i]
                        });
                    }

                }
                // viet ham them va xoa SkillTitle - Class trong SkillTitleClassBusiness
                try
                {
                    SkillTitleClassBusiness.Insert(lstSkillTitleClassIns);
                    SkillTitleClassBusiness.Delete(SkillTitleID.Value, lstClassDel);
                    SkillTitleClassBusiness.Save();
                }
                catch (Exception)
                {
                    return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                }
                // cap nhat - them - xoa kỹ năng
                List<SkillViewModel> lstSkillClient = frm.ListSkill; // danh sach gui len
                // neu bgh - quan tri truong --> Skill
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SkillTitleID"] = SkillTitleID.Value;
                if (globalInfo.IsAdminSchoolRole || globalInfo.IsRolePrincipal)
                {
                    IQueryable<Skill> lstSkill = SkillBusiness.Search(dic); // danh sach ky nang
                    List<int> lstSkillID = lstSkill.Select(o => o.SkillID).ToList();
                    List<int> lstSkillIDEdit = new List<int>();
                    SkillViewModel item;
                    int CountNumSkill = lstSkillClient.Count;
                    List<Skill> lstIns = new List<Skill>();
                    for (int i = 0; i < CountNumSkill; i++)
                    {
                        item = lstSkillClient[i];
                        if (!item.SkillID.HasValue || item.SkillID.Value <= 0) // neu chua co id thi insert
                        {
                            lstIns.Add(new Skill()
                            {
                                SkillTitleID = SkillTitleID.Value,
                                Description = item.Description,
                                Period = item.Period,
                                FromDate = item.FromDate.Value,
                                ToDate = item.ToDate.Value,
                                CreatedDate = createdDate
                            });
                        }
                        else // neu da co id thi update
                        {
                            if (lstSkillID.Contains(item.SkillID.Value)) // neu da co thi cap nhat
                            {
                                Skill obj = lstSkill.Where(o => o.SkillID == item.SkillID.Value).FirstOrDefault();
                                obj.FromDate = item.FromDate.Value;
                                obj.ToDate = item.ToDate.Value;
                                obj.Description = item.Description;
                                lstSkillIDEdit.Add(item.SkillID.Value);
                                SkillBusiness.Update(obj); // update
                            }
                        }
                    }
                    try
                    {
                        SkillBusiness.Insert(lstIns); // insert
                        //xoa ky nang cu                    
                        List<int> lstDel = lstSkillID.Except(lstSkillIDEdit).ToList();
                        SkillBusiness.Delete(lstDel);
                        SkillBusiness.Save();
                    }
                    catch (Exception ex)
                    {
                        
                        LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                        return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                    }
                }
                else // neu giao vien chu nhiem --> SkillOfClass
                {
                    IQueryable<SkillOfClass> lstSkill = SkillOfClassBusiness.Search(dic); // danh sach ky nang
                    List<int> lstSkillID = lstSkill.Select(o => o.SkillOfClassID).ToList();
                    List<int> lstSkillIDEdit = new List<int>();
                    SkillViewModel item;
                    int CountNumAct = lstSkillClient.Count;
                    List<SkillOfClass> lstIns = new List<SkillOfClass>();
                    for (int i = 0; i < CountNumAct; i++)
                    {
                        item = lstSkillClient[i];
                        if (!item.SkillID.HasValue || item.SkillID.Value <= 0) // neu chua co id thi insert
                        {
                            lstIns.Add(new SkillOfClass()
                            {
                                SkillTitleID = SkillTitleID.Value,
                                Description = item.Description,
                                Period = item.Period,
                                FromDate = item.FromDate.Value,
                                ToDate = item.ToDate.Value,
                                CreatedDate = createdDate
                            });
                        }
                        else // neu da co id thi update
                        {
                            if (lstSkillID.Contains(item.SkillID.Value)) // neu da co thi cap nhat
                            {
                                SkillOfClass obj = lstSkill.Where(o => o.SkillOfClassID == item.SkillID.Value).FirstOrDefault();
                                obj.FromDate = item.FromDate.Value;
                                obj.ToDate = item.ToDate.Value;
                                obj.Description = item.Description;
                                lstSkillIDEdit.Add(item.SkillID.Value);
                                SkillOfClassBusiness.Update(obj); // update
                            }
                        }
                    }

                    try
                    {
                        SkillOfClassBusiness.Insert(lstIns); // insert
                        //xoa ky nang cu                    
                        List<int> lstDel = lstSkillID.Except(lstSkillIDEdit).ToList();
                        SkillOfClassBusiness.Delete(lstDel);
                        SkillOfClassBusiness.Save();
                    }
                    catch (Exception ex)
                    {
                        
                        LogExtensions.ErrorExt(logger, DateTime.Now, "InsertORUpdate", "null", ex);
                        return Json(new { Type = "error", Message = Res.Get("SCSMonthlyGrowth_Message_Error") });
                    }

                }
                return Json(new { Type = "success", Message = Res.Get("Common_Label_UpdateSuccessMessage"), Id = SkillTitleID });
            }
            #endregion
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<SkillTitleViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            /* Kiem tra chi enable Edit, Delete voi giao vien co quyen thao tac */
            int EducationLevelID = 0;
            if (SearchInfo["EducationLevelID"] == null) EducationLevelID = 0;
            else
            {
                Int32.TryParse(SearchInfo["EducationLevelID"].ToString(), out EducationLevelID);
            }

            IQueryable<ClassProfile> lstClass = this.getClassFromEducationLevel(EducationLevelID); ;
            List<int> LstClassID = lstClass.Select(o => o.ClassProfileID).ToList();
            int schoolID = global.SchoolID.Value;
            List<SkillTitleViewModel> lstModel = new List<SkillTitleViewModel>();

            List<SkillTitleBO> lstSkillTitle = SkillTitleBusiness.SearchBySchool(schoolID, SearchInfo).OrderByDescending(o => o.FromDate).ToList();
            List<int> lstSkillTitleIDInRole = new List<int>();
            int Count = lstSkillTitle.Count;
            if (lstSkillTitle != null && Count > 0)
            {
                int index = 0;
                List<String> lstClassName = new List<string>();
                SkillTitleBO skillTitle;
                SkillTitleBO skillTitleNext;
                while (index <= lstSkillTitle.Count - 1)
                {
                    Count = lstSkillTitle.Count;
                    skillTitle = lstSkillTitle[index];
                    // neu Ky nang co pham vi thao tac trong lop
                    if (skillTitle.ClassID.HasValue && LstClassID.Contains(skillTitle.ClassID.Value))
                    {
                        lstSkillTitleIDInRole.Add(skillTitle.SkillTitleID);
                    }
                    StringBuilder Content = new StringBuilder();
                    Content.Append(skillTitle.ClassName).Append(",");
                    int i = index + 1;
                    while (i < Count)
                    {
                        skillTitleNext = lstSkillTitle[i];
                        if (skillTitleNext.SkillTitleID == skillTitle.SkillTitleID)
                        {
                            Content.Append(skillTitleNext.ClassName).Append(",");
                            lstSkillTitle.RemoveAt(i);
                            Count = lstSkillTitle.Count;
                        }
                        else i++;
                    }
                    Content.Remove(Content.Length - 1, 1);
                    lstClassName.Add(Content.ToString());
                    index++;
                }
                SkillTitleBO skillTitleTemp;
                for (int i = 0; i < lstSkillTitle.Count; i++)
                {
                    skillTitleTemp = lstSkillTitle[i];
                    lstModel.Add(new SkillTitleViewModel()
                    {
                        STT = i + 1,
                        SkillTitle = skillTitleTemp.SkillTitle,
                        SkillTitleID = skillTitleTemp.SkillTitleID,
                        AppliedTime = skillTitleTemp.FromDate.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_DDMMYY) + " - " + skillTitleTemp.ToDate.ToString(Constants.GlobalConstants.CUSTOM_FORMAT_DATETIME_DDMMYY),
                        AppliedClass = lstClassName.ElementAt(i)
                    });
                }
            }
            ViewData[SkillManagementConstants.LIST_SKILLTITLEID_IN_ROLE] = lstSkillTitleIDInRole.Distinct().ToList();
            return lstModel;
        }

        #region Xóa
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int SkillTitleID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            SkillTitle objSkillTitle = SkillTitleBusiness.Find(SkillTitleID);
            if (objSkillTitle.SkillOfClasses.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("SCSActivityPlan_Label_DeleteError")));
            }

            // delete
            SkillTitleBusiness.Delete(globalInfo.UserAccountID, SkillTitleID, globalInfo.SchoolID.Value);
            SkillTitleBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region load combobox
        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"EducationLevelID", EducationLevelID},
                    {"AcademicYearID", global.AcademicYearID.Value}
                };
            IQueryable<ClassProfile> lstClass;
            if (!global.IsViewAll) // neu cau hinh ko xem thong tin toan truong
            {
                if (!global.IsAdminSchoolRole && !global.IsRolePrincipal)
                {
                    int TeacherID = global.EmployeeID.Value;
                    int AcademicYearID = global.AcademicYearID.Value;
                    lstClass = ClassProfileBusiness.GetListClassByHeadTeacher(TeacherID, AcademicYearID).Union(ClassProfileBusiness.GetListClassByOverSeeing(TeacherID, AcademicYearID));
                }
                else
                {
                    lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
                }

            }
            else
            {
                lstClass = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            }


            return lstClass;
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                IQueryable<ClassProfile> lsCP = getClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP.ToList(), "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion
    }
}
