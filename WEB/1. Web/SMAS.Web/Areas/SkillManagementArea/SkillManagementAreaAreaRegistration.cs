﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SkillManagementArea
{
    public class SkillManagementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SkillManagementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SkillManagementArea_default",
                "SkillManagementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
