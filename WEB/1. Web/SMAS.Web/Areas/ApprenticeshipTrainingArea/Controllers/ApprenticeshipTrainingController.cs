﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ApprenticeshipTrainingArea.Models;

using SMAS.Models.Models;
using Telerik.Web.Mvc;
using System.Transactions;

namespace SMAS.Web.Areas.ApprenticeshipTrainingArea.Controllers
{
    public class ApprenticeshipTrainingController : BaseController
    {
        private readonly IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IApprenticeshipClassBusiness ApprenticeshipClassBusiness;
        private const int COUNT_PAGE = 1; //Biến phân trang

        public ApprenticeshipTrainingController(IApprenticeshipTrainingBusiness apprenticeshiptrainingBusiness
            , IAcademicYearBusiness AcademicYearBusiness, IClassProfileBusiness ClassProfileBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness, IPupilProfileBusiness PupilProfileBusiness
            , IApprenticeshipClassBusiness ApprenticeshipClassBusiness
            )
        {
            this.ApprenticeshipTrainingBusiness = apprenticeshiptrainingBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ApprenticeshipClassBusiness = ApprenticeshipClassBusiness;
        }

        public ActionResult Index()
        {
            //Giao diện gồm 2 Grid: Grid chứa danh sách học sinh đang học trong lớp và Grid chứa danh sách học sinh học lớp nghề 
            //KHi chọn combobox Lớp thì hiển thị ra danh sách học sinh đang học của lớp đó
            //Khi chọn combobox Lớp nghề thì load lại Grid danh sách học sinh đang học của lớp, chỉ hiển thị những học sinh chưa học trong lớp nghề, 
            //và load mới Grid chứa danh sách học sinh đang học trong lớp nghề đó
            SetViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult SearchOnRegister(ApprenticeshipTrainingViewModel frm)
        {
            GlobalInfo gl = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            if (!frm.EducationLevelID.HasValue)
                throw new BusinessException("ApprenticeshipTraining_Validate_NotEducationLevel");
            if (!frm.ClassID.HasValue)
                throw new BusinessException("ApprenticeshipTraining_Validate_NotOldClass");

            AcademicYear ay = AcademicYearBusiness.Find(gl.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["AcademicYearID"] = gl.AcademicYearID;
            SearchInfo["SchoolID"] = gl.SchoolID;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["Status"] = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING };
            SearchInfo["Year"] = ay.Year;
            SearchInfo["Check"] = "Check";
            IQueryable<PupilOfClass> query = PupilOfClassBusiness.SearchBySchool(gl.SchoolID.Value, SearchInfo).OrderBy(o => o.OrderInClass);
            if (frm.Gender.HasValue) query = query.Where(o => o.PupilProfile.Genre == frm.Gender.Value);
            List<PupilOfClass> lstPupilOfClass = query.ToList();

            IDictionary<string, object> ApprenticeshipClassDic = new Dictionary<string, object>();
            ApprenticeshipClassDic["AcademicYearID"] = gl.AcademicYearID.Value;
            ApprenticeshipClassDic["EducationLevelID"] = frm.EducationLevelID;

            List<ApprenticeshipTraining> lstApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(gl.SchoolID.Value, ApprenticeshipClassDic).ToList();

            //Chi lay cac hoc sinh chua dang ky hoc nghe
            lstPupilOfClass = lstPupilOfClass.Where(o => !lstApprenticeshipTraining.Any(a => a.PupilID == o.PupilID)).ToList();
            List<ApprenticeshipTrainingViewModel> lsTraining = lstPupilOfClass.Select(o => new ApprenticeshipTrainingViewModel
                                                               {
                                                                   PupilID = o.PupilID,
                                                                   ClassID = o.ClassID,
                                                                   SchoolID = o.SchoolID,
                                                                   AcademicYearID = o.AcademicYearID,
                                                                   PupilCode = o.PupilProfile.PupilCode,
                                                                   FullName = o.PupilProfile.FullName,
                                                                   ClassName = o.ClassProfile.DisplayName,
                                                                   GenderName = o.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female")
                                                               }).ToList();

            ViewData[ApprenticeshipTrainingConstants.LS_LISTOLDCLASS] = lsTraining;
            return PartialView("OldClassGrid");
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            if (frm.PupilCode != null)
            {
                if (frm.PupilCode.Length > 30)
                {
                    throw new BusinessException(Res.Get("SchoolMovement_Validate_PupilCode"));
                }
            }
            if (frm.FullName != null)
            {
                if (frm.FullName.Length > 100)
                {
                    throw new BusinessException(Res.Get("SchoolMovement_Validate_FullName"));
                }
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ApprenticeshipClassID"] = frm.ApprenticeshipTrainingClassID;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<ApprenticeshipTrainingViewModel> lst = this._Search(SearchInfo);
            ViewData[ApprenticeshipTrainingConstants.LIST_APPRENTICESHIPTRAINING] = lst;
            return PartialView("_List");
        }

        private List<ApprenticeshipTrainingViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo gl = new GlobalInfo();
            IQueryable<ApprenticeshipTraining> query = this.ApprenticeshipTrainingBusiness.SearchBySchool(gl.SchoolID.Value, SearchInfo).OrderBy(o => o.PupilProfile.FullName);
            IQueryable<ApprenticeshipTrainingViewModel> lst = query.Select(o => new ApprenticeshipTrainingViewModel
            {
                ApprenticeshipTrainingID = o.ApprenticeshipTrainingID,
                ClassID = o.ClassID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                PupilID = o.PupilID,
                EducationLevelID = o.EducationLevelID,
                ApprenticeshipSubjectID = o.ApprenticeshipSubjectID,
                Result = o.Result,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                ModifiedDate = o.ModifiedDate,
                SubjectName = o.ApprenticeshipClass.ApprenticeshipSubject.SubjectName,
                PupilCode = o.PupilCode,
                FullName = o.PupilProfile.FullName,
                ApprenticeshipClassID = o.ApprenticeShipClassID,
                Gender = o.PupilProfile.Genre,
                ClassName = o.ClassProfile.DisplayName
            });
            List<ApprenticeshipTrainingViewModel> lstApprenticeshipTrainingViewModel = lst.ToList();
            List<ApprenticeshipClass> iQ = ApprenticeshipClassBusiness.SearchBySchool(gl.SchoolID.Value, new Dictionary<string, object>() { 
                    {"AcademicYearID",gl.AcademicYearID.GetValueOrDefault()}
            }).ToList();
            foreach (ApprenticeshipTrainingViewModel item in lstApprenticeshipTrainingViewModel)
            {
                if (item.Gender == 1)
                {
                    item.GenderName = Res.Get("Common_Label_Male");
                }
                else
                {
                    item.GenderName = Res.Get("Common_Label_Female");
                }
                if (item.ApprenticeshipClassID.HasValue)
                {
                    int AppClassID = item.ApprenticeshipClassID.Value;
                    item.ClassApprenticeshipTrainingName = iQ.Where(o => o.ApprenticeshipClassID == AppClassID).FirstOrDefault().ClassName;
                }
            }

            //Paginate<ApprenticeshipTrainingViewModel> Paging = new Paginate<ApprenticeshipTrainingViewModel>(lstApprenticeshipTrainingViewModel.AsQueryable());
            //Paging.page = page;
            //Paging.paginate();

            return lstApprenticeshipTrainingViewModel;

        }

        private void SetViewData()
        {
            Session["ApprenticeshipClassID"] = null;
            Session["OldClassID"] = null;
            Session["EducationLevelID"] = null;
            Session["NewClassID"] = null;
            GlobalInfo global = new GlobalInfo();
            ViewData[ApprenticeshipTrainingConstants.ENABLE_BUTTON_ABORT] = true;
            if (!global.IsCurrentYear)
            {
                ViewData[ApprenticeshipTrainingConstants.ENABLE_BUTTON_ABORT] = false;
            }
            //List<EducationLevel> lsEducationLevel = global.EducationLevels;
            List<EducationLevel> lsEducationLevel = new List<EducationLevel>();
            if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                lsEducationLevel = global.EducationLevels.Where(a => a.EducationLevelID == 8).ToList(); // Lay khoi 8
            }
            if (global.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                lsEducationLevel = global.EducationLevels.Where(a => a.EducationLevelID == 11).ToList(); // lay khoi 11
            }
            if (lsEducationLevel != null)
            {
                ViewData[ApprenticeshipTrainingConstants.LS_EDUCATIONLEVELLASTYEAR] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[ApprenticeshipTrainingConstants.LS_EDUCATIONLEVELLASTYEAR] = new SelectList(new string[] { });
            }

            List<ComboObject> lsSemester = new List<ComboObject>();
            lsSemester.Add(new ComboObject(SystemParamsInFile.GENRE_MALE.ToString(), Res.Get("Common_Label_Male")));
            lsSemester.Add(new ComboObject(SystemParamsInFile.GENRE_FEMALE.ToString(), Res.Get("Common_Label_Female")));
            ViewData[ApprenticeshipTrainingConstants.LS_GENDER] = new SelectList(lsSemester, "key", "value");

            List<ApprenticeshipTrainingViewModel> newls2 = new List<ApprenticeshipTrainingViewModel>();
            IQueryable<ApprenticeshipTrainingViewModel> res2 = newls2.AsQueryable();
            // Thuc hien phan trang tung phan
            Paginate<ApprenticeshipTrainingViewModel> paging2 = new Paginate<ApprenticeshipTrainingViewModel>(res2);
            paging2.page = 1;
            paging2.paginate();
            ViewData[ApprenticeshipTrainingConstants.LS_LISTNEWCLASS] = newls2;
            ViewData[ApprenticeshipTrainingConstants.LS_LISTOLDCLASS] = newls2;

            ViewData[ApprenticeshipTrainingConstants.PERMISSION] = true;
            ViewData[ApprenticeshipTrainingConstants.PERMISSION_FORM] = true;

            ViewData[ApprenticeshipTrainingConstants.LS_CBNEWCLASS] = new SelectList(new string[] { });
            ViewData[ApprenticeshipTrainingConstants.LS_CBOLDCLASS] = new SelectList(new string[] { });
            ViewData[ApprenticeshipTrainingConstants.LIST_APPRENTICESHIPTRAINING] = newls2;

            ViewData[ApprenticeshipTrainingConstants.DISABLED_SAVE] = "display:block";
            if (new GlobalInfo().IsCurrentYear)
            {
                ViewData[ApprenticeshipTrainingConstants.DISABLED_SAVE] = "display:none";
            }
            LoadApprenticeshipTraining();
        }

        private void LoadApprenticeshipTraining()
        {
            GlobalInfo gl = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = gl.SchoolID;
            dic["AcademicYearID"] = gl.AcademicYearID;
            List<ApprenticeshipClass> lst = ApprenticeshipClassBusiness.SearchBySchool(gl.SchoolID.Value, dic).ToList();
            if (lst.Count > 0)
            {
                ViewData[ApprenticeshipTrainingConstants.LS_CBNEWCLASS] = new SelectList(lst, "ApprenticeshipClassID", "ClassName");
            }
            else
            {
                ViewData[ApprenticeshipTrainingConstants.LS_CBNEWCLASS] = new SelectList(new string[] { });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingOldClass(int? EducationLevelId)
        {
            if (EducationLevelId.HasValue)
            {
                GlobalInfo globalInfo = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("SchoolID", globalInfo.SchoolID.Value);
                dic.Add("EducationLevelID", EducationLevelId.Value);
                List<ClassProfile> lsClass = ClassProfileBusiness.SearchByAcademicYear(globalInfo.AcademicYearID.Value, dic).ToList();
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new List<ClassProfile>()), JsonRequestBehavior.AllowGet);
            }
        }

        //#region Change Old Class Grid
        ////id: id cua lop dang chon
        //public ActionResult ChangeOldClassGrid(int? id, int? idEdu, string pupilCode, string fullName, int? gender)
        //{
        //    if (Session["OldClassID"] != null) Session["OldClassID"] = null;
        //    Session["OldClassID"] = id;
        //    if (Session["EducationLevelID"] != null) Session["EducationLevelID"] = null;
        //    Session["EducationLevelID"] = idEdu;
        //    if (idEdu.HasValue)
        //    {
        //        if (!id.HasValue)
        //        {
        //            List<ApprenticeshipTrainingViewModel> newls2 = new List<ApprenticeshipTrainingViewModel>();
        //            ViewData[ApprenticeshipTrainingConstants.LS_LISTNEWCLASS] = newls2;
        //            ViewData[ApprenticeshipTrainingConstants.LS_LISTOLDCLASS] = newls2;
        //        }
        //        else
        //        {
        //            ViewData[ApprenticeshipTrainingConstants.LS_LISTOLDCLASS] = GetOldClassGridData(id.Value, idEdu.Value, pupilCode, fullName, gender);
        //        }
        //    }
        //    else
        //    {
        //        List<ApprenticeshipTrainingViewModel> newls2 = new List<ApprenticeshipTrainingViewModel>();
        //        ViewData[ApprenticeshipTrainingConstants.LS_LISTNEWCLASS] = newls2;
        //        ViewData[ApprenticeshipTrainingConstants.LS_LISTOLDCLASS] = newls2;
        //    }
        //    return PartialView("OldClassGrid");

        //}

        //public List<ApprenticeshipTrainingViewModel> GetOldClassGridData(int id, int idEdu, string pupilCode, string fullName, int? gender)
        //{

        //    GlobalInfo globalInfo = new GlobalInfo();
        //    int? SchoolID = globalInfo.SchoolID;
        //    int? AcademicYearID = globalInfo.AcademicYearID;
        //    int? ApprenticeshipClassID;
        //    if (Session["ApprenticeshipClassID"] != null) ApprenticeshipClassID = (int)Session["ApprenticeshipClassID"];
        //    else ApprenticeshipClassID = null;
        //    AcademicYear ay = AcademicYearBusiness.Find(globalInfo.AcademicYearID);

        //    IDictionary<string, object> dic = new Dictionary<string, object>();
        //    dic["AcademicYearID"] = AcademicYearID.Value;
        //    dic["SchoolID"] = SchoolID.Value;
        //    dic["EducationLevelID"] = idEdu;
        //    dic["ClassID"] = id;
        //    dic["Year"] = ay.Year;
        //    dic["Status"] = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING };
        //    dic["PupilCode"] = pupilCode;
        //    dic["FullName"] = fullName;
        //    IDictionary<string, object> dicApprenticeshipTraining = new Dictionary<string, object>();
        //    dicApprenticeshipTraining["AcademicYearID"] = AcademicYearID.Value;
        //    dicApprenticeshipTraining["SchoolID"] = SchoolID.Value;
        //    dicApprenticeshipTraining["EducationLevelID"] = idEdu;
        //    dicApprenticeshipTraining["ClassID"] = id;
        //    if (ApprenticeshipClassID.HasValue) dicApprenticeshipTraining["ApprenticeshipClassID"] = ApprenticeshipClassID;
        //    List<PupilOfClass> lstPupilOfClass = new List<SMAS.Models.Models.PupilOfClass>();
        //    if (gender.HasValue && gender >= 0)
        //    {
        //        lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).Where(o => o.PupilProfile.Genre == gender).ToList();
        //    }
        //    else
        //    {
        //        lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, dic).ToList();
        //    }
        //    List<int> lstPupilOfClassID = lstPupilOfClass.Select(o => o.PupilID).ToList();
        //    List<PupilOfClass> lstNewPupilOfClass = new List<SMAS.Models.Models.PupilOfClass>();
        //    foreach (var item in lstPupilOfClass)
        //    {
        //        lstNewPupilOfClass.Add(item);
        //    }
        //    List<int> lsApprenticeshipTraining = new List<int>();

        //    if (lstPupilOfClass.Count > 0)
        //    {
        //        List<ApprenticeshipTrainingViewModel> lsTraining = new List<ApprenticeshipTrainingViewModel>();
        //        if (ApprenticeshipClassID.HasValue)
        //        {

        //            lsApprenticeshipTraining = ApprenticeshipTrainingBusiness.SearchBySchool(SchoolID.Value, dicApprenticeshipTraining).Select(o => o.PupilID).ToList();//lay ra danh sach id hoc sinh da hoc nghe
        //            if (lsApprenticeshipTraining.Count > 0)
        //            {
        //                lstNewPupilOfClass = new List<SMAS.Models.Models.PupilOfClass>();
        //                for (int i = 0; i < lstPupilOfClass.Count; i++)
        //                {
        //                    if (lsApprenticeshipTraining.Contains(lstPupilOfClass[i].PupilID))
        //                    {
        //                        continue;
        //                    }
        //                    lstNewPupilOfClass.Add(lstPupilOfClass[i]);
        //                }
        //            }
        //        }
        //        foreach (PupilOfClass item in lstNewPupilOfClass)
        //        {
        //            ApprenticeshipTrainingViewModel newItem = new ApprenticeshipTrainingViewModel();
        //            newItem.PupilID = item.PupilID;
        //            newItem.ClassID = item.ClassID;
        //            newItem.SchoolID = item.SchoolID;
        //            newItem.AcademicYearID = item.AcademicYearID;
        //            PupilProfile pupilPro = PupilProfileBusiness.Find(item.PupilID);
        //            newItem.PupilCode = pupilPro.PupilCode;
        //            newItem.FullName = pupilPro.FullName;
        //            newItem.ClassName = pupilPro.ClassProfile.DisplayName;
        //            if (pupilPro.Genre == 1) newItem.GenderName = Res.Get("Common_Label_Male");
        //            else newItem.GenderName = Res.Get("Common_Label_Female");
        //            lsTraining.Add(newItem);

        //        }
        //        return lsTraining;
        //    }
        //    else
        //    {
        //        List<ApprenticeshipTrainingViewModel> newls2 = new List<ApprenticeshipTrainingViewModel>();
        //        return newls2;
        //    }
        //}
        //#endregion

        [ValidateAntiForgeryToken]
        public ActionResult ChangeNewClassGrid(int? EducationLevelID, int? OldClassID, int ApprenticeshipClassID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = globalInfo.AcademicYearID;
            dic["SchoolID"] = globalInfo.SchoolID;
            dic["ClassID"] = OldClassID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ApprenticeshipClassID"] = ApprenticeshipClassID;
            List<ApprenticeshipTraining> lstAppTraining = ApprenticeshipTrainingBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).OrderBy(o => o.ApprenticeshipTrainingID).ToList();
            List<ApprenticeshipTrainingViewModel> lst = lstAppTraining.Select(o => new ApprenticeshipTrainingViewModel
                                                                {
                                                                    ApprenticeshipTrainingID = o.ApprenticeshipTrainingID,
                                                                    ClassID = o.ClassID,
                                                                    SchoolID = o.SchoolID,
                                                                    AcademicYearID = o.AcademicYearID,
                                                                    PupilID = o.PupilID,
                                                                    EducationLevelID = o.EducationLevelID,
                                                                    ApprenticeshipSubjectID = o.ApprenticeshipSubjectID,
                                                                    Result = o.Result,
                                                                    Description = o.Description,
                                                                    CreatedDate = o.CreatedDate,
                                                                    ModifiedDate = o.ModifiedDate,
                                                                    ApprenticeshipClassID = o.ApprenticeShipClassID.Value,
                                                                    PupilCode = o.PupilCode,
                                                                    Gender = o.PupilProfile.Genre,
                                                                    FullName = o.PupilProfile.FullName,
                                                                    GenderName = o.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female")
                                                                }).ToList();
            ViewData[ApprenticeshipTrainingConstants.LS_LISTNEWCLASS] = lst;
            return PartialView("NewClassGird");
        }

        #region button transfer click
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransferDataForward(int? ApprenticeshipClassID, int? EducationLevelID, int? OldClassID, int[] checkedOldPupil)
        {
            if (!ApprenticeshipClassID.HasValue)
                return Json(new JsonMessage(Res.Get("ApprenticeshipTraining_Validate_NotApprenticeshipTrainingClass"), JsonMessage.ERROR));

            if (!EducationLevelID.HasValue)
                return Json(new JsonMessage("ApprenticeshipTraining_Validate_NotEducationLevel", JsonMessage.ERROR));

            if (!OldClassID.HasValue)
                return Json(new JsonMessage("ApprenticeshipTraining_Validate_NotOldClass", JsonMessage.ERROR));

            if (checkedOldPupil == null || checkedOldPupil.Length == 0)
                return Json(new JsonMessage("Common_Validate_NoPupilSelected", JsonMessage.ERROR));

            GlobalInfo globalInfo = new GlobalInfo();
            ApprenticeshipTraining at = new ApprenticeshipTraining();
            ApprenticeshipClass ac = ApprenticeshipClassBusiness.Find(ApprenticeshipClassID.Value);
            List<ApprenticeshipTraining> lstApprenticeshipTraining = new List<ApprenticeshipTraining>();
            foreach (int oldPupil in checkedOldPupil)
            {
                PupilProfile pp = PupilProfileBusiness.Find(oldPupil);
                at = new SMAS.Models.Models.ApprenticeshipTraining();
                at.AcademicYearID = globalInfo.AcademicYearID;
                at.ClassID = OldClassID;
                at.SchoolID = globalInfo.SchoolID;
                at.PupilID = oldPupil;
                at.PupilCode = pp.PupilCode;
                at.EducationLevelID = (int)EducationLevelID;
                at.ApprenticeShipClassID = ApprenticeshipClassID.Value;
                at.CreatedDate = DateTime.Now;
                at.ModifiedDate = DateTime.Now;
                at.ApprenticeshipSubjectID = ac.ApprenticeshipSubjectID;
                lstApprenticeshipTraining.Add(at);
            }
            ApprenticeshipTrainingBusiness.InsertApprenticeshipTraining(globalInfo.SchoolID.Value, globalInfo.AcademicYearID.Value, EducationLevelID.Value,
                OldClassID.Value, lstApprenticeshipTraining);
            ApprenticeshipTrainingBusiness.Save();
            return Json(new JsonMessage(Res.Get("ApprenticeshipTraining_Label_InsertSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransferDataBackward(int? ApprenticeshipClassID, int? EducationLevelID, int? OldClassID, int[] checkedNewPupil)
        {
            if (!ApprenticeshipClassID.HasValue)
                return Json(new JsonMessage(Res.Get("ApprenticeshipTraining_Validate_NotApprenticeshipTrainingClass"), JsonMessage.ERROR));

            if (!EducationLevelID.HasValue)
                return Json(new JsonMessage("ApprenticeshipTraining_Validate_NotEducationLevel", JsonMessage.ERROR));

            if (!OldClassID.HasValue)
                return Json(new JsonMessage("ApprenticeshipTraining_Validate_NotOldClass", JsonMessage.ERROR));

            if (checkedNewPupil == null || checkedNewPupil.Length == 0)
                return Json(new JsonMessage("Common_Validate_NoPupilSelected", JsonMessage.ERROR));

            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", globalInfo.AcademicYearID.Value);
            dic.Add("ClassID", OldClassID);
            dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("ApprenticeshipClassID", ApprenticeshipClassID);
            List<ApprenticeshipTraining> lstAllPupil = ApprenticeshipTrainingBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).ToList();

            lstAllPupil = lstAllPupil.Where(u => checkedNewPupil.Contains(u.PupilID)).ToList();

            foreach (ApprenticeshipTraining newPupil in lstAllPupil)
                ApprenticeshipTrainingBusiness.DeleteApprenticeshipTraining(newPupil.ApprenticeshipTrainingID);

            ApprenticeshipTrainingBusiness.Save();

            return Json(new JsonMessage(Res.Get("ApprenticeshipTraining_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Abort Registration
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AbortRegister(int[] checkedOldPupil)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo gl = new GlobalInfo();
            dic.Add("SchoolID", gl.SchoolID);
            dic.Add("AcademicYearID", gl.AcademicYearID);
            checkedOldPupil = checkedOldPupil ?? new int[] { };
            if (checkedOldPupil.Count() > 0)
            {
                foreach (int apprenticeshipTrainingID in checkedOldPupil)
                {
                    ApprenticeshipTraining at = ApprenticeshipTrainingBusiness.Find(apprenticeshipTrainingID);
                    if (at.ActionMark != null || at.TheoryMark != null)
                    {
                        throw new BusinessException(Res.Get("ApprenticeshipTraining_Validate_Marked"));
                    }
                    else
                    {
                        ApprenticeshipTrainingBusiness.DeleteApprenticeshipTraining(apprenticeshipTrainingID);
                        ApprenticeshipTrainingBusiness.Save();
                    }
                }
            }

            return Json(new JsonMessage(Res.Get("")));
        }
        #endregion
    }
}





