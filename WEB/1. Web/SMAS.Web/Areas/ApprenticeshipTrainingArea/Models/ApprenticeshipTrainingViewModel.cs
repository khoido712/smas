/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.ApprenticeshipTrainingArea.Models
{
    public class ApprenticeshipTrainingViewModel
    {
        public System.Int32 ApprenticeshipTrainingID { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_ClassName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.Int32> ClassID { get; set; }
        public System.Nullable<System.Int32> SchoolID { get; set; }
        public System.Nullable<System.Int32> AcademicYearID { get; set; }
        public System.Int32 PupilID { get; set; }
        [ResourceDisplayName("ApprenticeshipTraining_Label_Education")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.Int32> EducationLevelID { get; set; }
        public System.Int32 ApprenticeshipSubjectID { get; set; }
        public decimal? Result { get; set; }
        public System.String Description { get; set; }
        public System.Nullable<System.DateTime> CreatedDate { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Column_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String PupilCode { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public System.String FullName { get; set; }
        public int? Gender { get; set; }
        [ResourceDisplayName("ApprenticeshipTraining_Label_ClassApprenticeshipTrainingName")]
        public string ClassApprenticeshipTrainingName { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_ClassName")]
        public string ClassName { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_SubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_Gender")]
        public string GenderName { get; set; }
        public int? ApprenticeshipClassID { get; set; }
    }
}


