/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ApprenticeshipTrainingArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ApprenticeshipTraining_Label_ClassApprenticeshipTrainingName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ApprenticeshipTrainingConstants.LS_CBNEWCLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("id", "cboApprenticeshipTrainingClass")]
        public int? ApprenticeshipTrainingClassID { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_FullName")]
        [UIHint("Textbox")]
        //[StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public System.String FullName { get; set; }

        [ResourceDisplayName("ApprenticeshipTraining_Label_PupilCode")]
        [UIHint("Textbox")]
        //[StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public System.String PupilCode { get; set; }
    }
}