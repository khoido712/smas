﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ApprenticeshipTrainingArea
{
    public class ApprenticeshipTrainingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApprenticeshipTrainingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApprenticeshipTrainingArea_default",
                "ApprenticeshipTrainingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
