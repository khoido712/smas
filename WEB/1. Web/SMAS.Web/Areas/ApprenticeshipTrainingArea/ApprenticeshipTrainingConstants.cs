/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ApprenticeshipTrainingArea
{
    public class ApprenticeshipTrainingConstants
    {
        public const string LIST_APPRENTICESHIPTRAINING = "listApprenticeshipTraining";
  
        public const string LS_EDUCATIONLEVELLASTYEAR = "LISTEDUCATIONLEVELLASTYEAR";
 
        public const string LS_LISTEDUCATIONLEVEL = "LS_LISTEDUCATIONLEVEL";
        public const string LS_LISTOLDCLASS = "LS_LISTOLDCLASS";
        public const string LS_LISTNEWCLASS = "LS_LISTNEWCLASS";

        public const string PERMISSION = "PERMISSION";
        public const string PERMISSION_FORM = "PERMISSION_FORM";

        public const string LS_CBOLDCLASS = "LS_CBOLDCLASS";
        public const string LS_CBNEWCLASS = "LS_CBNEWCLASS";
        public const string LS_GENDER = "LS_GENDER";
        public const string ENABLE_BUTTON_ABORT = "enable";
        public const string DISABLED_SAVE = "DISABLED_SAVE";
    }
}