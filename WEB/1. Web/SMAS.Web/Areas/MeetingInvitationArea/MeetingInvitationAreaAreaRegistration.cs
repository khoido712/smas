﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MeetingInvitationArea
{
    public class MeetingInvitationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MeetingInvitationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MeetingInvitationArea_default",
                "MeetingInvitationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
