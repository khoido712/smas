﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.PupilProfileReportArea;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SMAS.Web.Areas.MeetingInvitationArea.Controllers
{
    public class MeetingInvitationController : BaseController
    {
        //
        // GET: /MeetingInvitationArea/MeetingInvitation/

        
       #region properties

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        private readonly IPhysicalTestBusiness PhysicalTestBusiness;
        private readonly IMonitoringBookBusiness MonitoringBookBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;

        public MeetingInvitationController(
            IClassProfileBusiness classProfileBusiness,
            ISchoolProfileBusiness schoolProfileBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness,
            IEmployeeBusiness employeeBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IPupilProfileBusiness pupilProfileBusiness,
            IAcademicYearBusiness academicYearBusiness,
            IDeclareEvaluationIndexBusiness declareEvaluationIndexBusiness,
            IDeclareEvaluationGroupBusiness declareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness evaluationDevelopmentGroupBusiness,
            IEvaluationDevelopmentBusiness evaluationDevelopmentBusiness,
            IGrowthEvaluationBusiness growthEvaluationBusiness,
            IPhysicalTestBusiness physicalTestBusiness,
            IMonitoringBookBusiness monitoringBookBusiness,
            IPupilAbsenceBusiness pupilAbsenceBusiness,
            IGoodChildrenTicketBusiness goodChildrenTicketBusiness,
            IClassAssigmentBusiness classAssigmentBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.DeclareEvaluationGroupBusiness = declareEvaluationGroupBusiness;
            this.DeclareEvaluationIndexBusiness = declareEvaluationIndexBusiness;
            this.EvaluationDevelopmentGroupBusiness = evaluationDevelopmentGroupBusiness;
            this.EvaluationDevelopmentBusiness = evaluationDevelopmentBusiness;
            this.GrowthEvaluationBusiness = growthEvaluationBusiness;
            this.PhysicalTestBusiness = physicalTestBusiness;
            this.MonitoringBookBusiness = monitoringBookBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.GoodChildrenTicketBusiness = goodChildrenTicketBusiness;
            this.ClassAssigmentBusiness = classAssigmentBusiness;
        }

        #endregion

        public static int countForBreak = 0;
        public ActionResult Index()
        {
            //index
            var lstEducation = _globalInfo.EducationLevels;
            ViewData["Khoi"] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> classList = new List<SelectListItem>();
            if (lstEducation.Any())
            {
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("EducationLevelID", lstEducation.FirstOrDefault().EducationLevelID);

                classList = Enumerable.ToList(this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                        .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName))
                    .Select(u => new SelectListItem
                    {
                        Value = u.ClassProfileID.ToString(),
                        Text = u.DisplayName,
                        Selected = false
                    }).ToList();
            }
            ViewData["Lop"] = classList;
            return View();
        }

        //[ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
            {
                return Json(new List<SelectListItem>());
            }
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            var lstClass = Enumerable.ToList(this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName))
                                                    
                                                    .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName)
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }

        int startRow = 3;
        public Stream ExportExcel(int? educationLevelID, int? classID, string Content, string Time, string Date, string Place, string SignalPerson, string Position)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "MN", SystemParamsInFile.MeetingInvitationReport + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = oBook.GetSheet(1);
           

            IVTWorksheet newSheet = null;
            IVTWorksheet newSheet1 = oBook.GetSheet(2);
           

            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevel",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID},
                                                        {"CurrentClassID",classID}
                                                    };

            List<PupilProfileBO> lstPupilBO = this.PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(a => a.ProfileStatus == 1 || a.ProfileStatus == 2).OrderBy(a => a.OrderInClass).ToList();
            List<ClassProfile> lstClassProfile = new List<ClassProfile>();
            if (classID != 0)
            {
                lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(x => x.ClassProfileID == classID).OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();
            }
            else {
                lstClassProfile = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber).ThenBy(u => u.DisplayName).ToList();
            }
            
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();
            List<string> lstClassName = lstClassProfile.Select(p => p.DisplayName).Distinct().ToList();


            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("lstPupil", lstPupilBO);

            for (int i = 0; i < lstClassID.Count(); i++)
            {
                int ClassId = lstClassID[i];
                newSheet = oBook.CopySheetToLast(sheet);

                if (lstClassName[i].Length > 30) {
                    lstClassName[i] = lstClassName[i].Substring(0, 29);
                }

                this.SetValueToFile(newSheet, dicClass, ClassId, lstClassName[i]);
                startRow = 3;
                countForBreak = 0;
            }
            
            sheet.Delete();
            #region du lieu cua sheet thong tin chung
            IVTWorksheet sheetInfo = oBook.CopySheetToLast(newSheet1);
            int startRowInfo = 2;

            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            sheetInfo.SetCellValue("A" + startRowInfo, "Đơn vị quản lý");
            sheetInfo.SetCellValue("B" + startRowInfo, UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, _globalInfo.AppliedLevel.Value).ToUpper());
            sheetInfo.SetColumnWidth(1, 19.14);
            sheetInfo.SetColumnWidth(2, 85.14);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Tên trường");
            sheetInfo.SetCellValue("B" + startRowInfo, _globalInfo.SchoolName.ToUpper());
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Tiêu đề");
            sheetInfo.SetCellValue("B" + startRowInfo, "GIẤY MỜI");
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Kính mời");
            sheetInfo.SetCellValue("B" + startRowInfo, "Trân trọng kính mời phụ huynh của em:");
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Nội dung");
            sheetInfo.SetCellValue("B" + startRowInfo, Content == "" ? "" : Content);
            startRowInfo++;

            string MeetingDate = "";
            if (Date.ToString() != "" && Time.ToString() != "" && Date.GetHashCode() != 0 && Time.GetHashCode() != 0)
            {
                List<int> lstYear = GetListIDFromString(Date);
                DateTime dt = new DateTime(lstYear[2], lstYear[1], lstYear[0]);
                string dateTranslate = TranslateDate(dt.DayOfWeek.ToString());
                MeetingDate = Time.Replace(':', 'h') + ", " + dateTranslate + ", ngày " + Date;
            }
            else if (Date.ToString() != "" && Date.GetHashCode() != 0)
            {
                List<int> lstYear = GetListIDFromString(Date);
                DateTime dt = new DateTime(lstYear[2], lstYear[1], lstYear[0]);
                string dateTranslate = TranslateDate(dt.DayOfWeek.ToString());
                MeetingDate = dateTranslate + ", ngày " + Date;
            }
            else if (Time.ToString() != "" && Time.GetHashCode() != 0)
            {
                List<int> lstYear = GetListIDFromString(Date);
                DateTime dt = new DateTime(lstYear[2], lstYear[1], lstYear[0]);
                MeetingDate = Time.Replace(':', 'h');
            }

            sheetInfo.SetCellValue("A" + startRowInfo, "Thời gian");
            sheetInfo.SetCellValue("B" + startRowInfo, MeetingDate == "" ? "" : MeetingDate);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Điạ điểm");
            sheetInfo.SetCellValue("B" + startRowInfo, Place == "" ? "" : Place);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Lời kết");
            sheetInfo.SetCellValue("B" + startRowInfo, "Rất mong quý vị phụ huynh có mặt đầy đủ và đúng giờ để buổi họp đạt kết quả tốt.");
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Cảm ơn");
            sheetInfo.SetCellValue("B" + startRowInfo, "Xin chân thành cảm ơn!");
            startRowInfo++;
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);

            string strDate = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Date.Day.ToString("00") + " tháng " + DateTime.Now.Date.Month.ToString("00") + " năm " + DateTime.Now.Date.Year;
            sheetInfo.SetCellValue("A" + startRowInfo, "Ngày tháng ký tên");
            sheetInfo.SetCellValue("B" + startRowInfo, strDate);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Chức vụ người ký tên");
            sheetInfo.SetCellValue("B" + startRowInfo, Position == "" ? "" : Position);
            startRowInfo++;
            sheetInfo.SetCellValue("A" + startRowInfo, "Tên người ký tên");
            sheetInfo.SetCellValue("B" + startRowInfo, SignalPerson == "" ? "" : SignalPerson);
            startRowInfo++;
            sheetInfo.GetRange(2, 2, startRowInfo - 1, 2).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheetInfo.GetRange(2, 1, startRowInfo - 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetInfo.Name = "Thong Tin Chung";
            sheetInfo.SetFontName("Times New Roman", 11);
            sheetInfo.FitAllColumnsOnOnePage = true;
            #endregion

            newSheet1.Delete();
            
            return oBook.ToStream();
        }

        private void SetValueToFile(IVTWorksheet newSheet, IDictionary<string, object> dic, int ClassId, string className)
        {
            int startRowTemp = startRow - 1;
            newSheet.SetRowHeight(startRowTemp - 1, 0);
            newSheet.SetRowHeight(1, 0);
            List<PupilProfileBO> lstPupil = (List<PupilProfileBO>)dic["lstPupil"];
            for (int i = 0; i < lstPupil.Count(); i++)
            {
                if (lstPupil[i].CurrentClassID == ClassId)
                {
                    countForBreak++;
                    #region page

                    newSheet.SetColumnWidth(1, 2.73);
                    newSheet.SetColumnWidth(2, 9.57);
                    newSheet.SetColumnWidth(3, 8.43);
                    newSheet.SetColumnWidth(4, 8.43);
                    newSheet.SetColumnWidth(5, 5.14);
                    newSheet.SetColumnWidth(6, 10.86);
                    newSheet.SetColumnWidth(7, 10.29);
                    newSheet.SetColumnWidth(8, 9.86);
                    newSheet.SetColumnWidth(9, 6.57);
                    newSheet.SetColumnWidth(10, 8.57);
                    newSheet.SetColumnWidth(11, 8.43);
  
                    newSheet.SetRowHeight(startRow, 15.75);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$2");
                    newSheet.GetRange(startRow, 2, startRow, 6).Merge();
                    newSheet.SetCellValue("G" + startRow, "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
                    newSheet.GetRange(startRow, 7, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 7, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 15.75);
                    newSheet.SetRowHeight(startRow + 1, 15.75);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$3");
                    newSheet.GetRange(startRow, 2, startRow, 6).Merge();
                    newSheet.GetRange(startRow, 2, startRow, 6).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, true);
                    newSheet.SetCellValue("G" + startRow, "Độc lập - Tự do - Hạnh phúc");
                    newSheet.GetRange(startRow, 7, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 7, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, true);
                    startRow = startRow + 2;

                    newSheet.SetRowHeight(startRow, 40);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$4");
                    newSheet.GetRange(startRow, 2, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 2, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 18, false, false);
                    startRow = startRow + 2;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$5");
                    newSheet.GetRange(startRow, 2, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.SetCellValue("F" + startRow, lstPupil[i].FullName);
                    newSheet.GetRange(startRow, 6, startRow, 8).Merge();
                    newSheet.GetRange(startRow, 6, startRow, 8).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 6, startRow, 8).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.SetCellValue("I" + startRow, "Lớp: ");
                    newSheet.GetRange(startRow, 9, startRow, 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.SetCellValue("J" + startRow, lstPupil[i].ClassName);
                    newSheet.GetRange(startRow, 10, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 10, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 10, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$6");
                    newSheet.GetRange(startRow, 2, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 2, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 2, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "Thời gian: ");
                    newSheet.GetRange(startRow, 2, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.SetCellValue("C" + startRow, "='Thong Tin Chung'!$B$7");
                    newSheet.GetRange(startRow, 3, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 3, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 3, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "Địa điểm: ");
                    newSheet.GetRange(startRow, 2, startRow, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.SetCellValue("C" + startRow, "='Thong Tin Chung'!$B$8");
                    newSheet.GetRange(startRow, 3, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 3, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 3, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$9");
                    newSheet.GetRange(startRow, 2, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 2, startRow, 11).SetHAlign(VTHAlign.xlHAlignLeft);
                    newSheet.GetRange(startRow, 2, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    startRow = startRow + 2;

                    newSheet.SetRowHeight(startRow - 1, 20);
                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("B" + startRow, "='Thong Tin Chung'!$B$10");
                    newSheet.GetRange(startRow, 2, startRow, 5).Merge();
                    newSheet.GetRange(startRow, 2, startRow, 5).SetHAlign(VTHAlign.xlHAlignCenter);
                    newSheet.GetRange(startRow, 2, startRow, 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.GetRange(startRow, 2, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, true, 11, false, false);
                    newSheet.SetCellValue("G" + startRow, "='Thong Tin Chung'!$B$11");
                    newSheet.GetRange(startRow, 7, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlign(VTHAlign.xlHAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetFontStyle(false, System.Drawing.Color.Black, true, 11, false, false);
                    startRow++;

                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetCellValue("G" + startRow, "='Thong Tin Chung'!$B$12");
                    newSheet.GetRange(startRow, 7, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlign(VTHAlign.xlHAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    startRow = startRow + 3;

                    newSheet.SetRowHeight(startRow - 2, 20);
                    newSheet.SetRowHeight(startRow - 1, 20);
                    newSheet.SetRowHeight(startRow, 20);
                    newSheet.SetRowHeight(startRow + 1, 20);
                    newSheet.SetCellValue("G" + startRow, "='Thong Tin Chung'!$B$13");
                    newSheet.GetRange(startRow, 7, startRow, 11).Merge();
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlign(VTHAlign.xlHAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    newSheet.GetRange(startRow, 7, startRow, 11).SetFontStyle(false, System.Drawing.Color.Black, false, 11, false, false);

                    newSheet.GetRange(startRowTemp, 1, startRow + 1, 11).SetBorder(VTBorderStyle.Double, VTBorderWeight.Thick, VTBorderIndex.Around);

                    startRowTemp = startRow + 3;
                    startRow = startRow + 4;
                    
                    if (countForBreak % 2 == 0) {
                        newSheet.SetBreakPage(startRowTemp - 1);
                    }
                    else
                    {
                        newSheet.SetRowHeight(startRowTemp - 1, 70);
                    }
                    
                    newSheet.SetFontName("Times New Roman", 0);

                    #endregion
                }
            }

            
            newSheet.FitAllColumnsOnOnePage = true;
            className = className.Replace("?", "_");
            newSheet.Name = className;//Utils.Utils.StripVNSignAndSpace(className);
            newSheet.SetFontName("Times New Roman", 0);
            newSheet.PageMaginTop = 0.4;
            newSheet.PageMaginBottom = 0.4;
        }

        public string TranslateDate(string str) {
            str = str.ToLower();

            if(str=="monday"){
                str = "thứ hai";
            }
            else if(str == "tuesday"){
                str="thứ ba";
            }
            else if(str == "wednesday"){
                str="thứ tư";
            }
            else if(str == "thursday"){
                str="thứ năm";
            }
            else if(str == "friday"){
                str="thứ sáu";
            }
            else if(str == "saturday"){
                str="thứ bảy";
            }
            else if(str == "sunday"){
                str="chủ nhật";
            }

            return str;
        }

        public JsonResult GetNumberOfClassReport_03(FormCollection frm)
        {
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            int? educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            string content = !string.IsNullOrEmpty(frm["Content"]) ? frm["Content"] : "";
            string Time = frm["Time"];
            string Date = frm["Date"];
            string Place = !string.IsNullOrEmpty(frm["Place"]) ? frm["Place"] : "";
            string SignalPerson = !string.IsNullOrEmpty(frm["SignalPerson"]) ? frm["SignalPerson"] : "";
            string Position = !string.IsNullOrEmpty(frm["Position"]) ? frm["Position"] : "";
            //int ChildrenID = !string.IsNullOrEmpty(frm["ChildrenID"]) ? int.Parse(frm["ChildrenID"]) : 0;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MeetingInvitationReport);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                                                    {
                                                        {"AcademicYearID",_globalInfo.AcademicYearID},
                                                        {"SchoolID",_globalInfo.SchoolID},
                                                        {"AppliedLevelID",_globalInfo.AppliedLevel},
                                                        {"EducationLevelID",educationLevelID},
                                                        {"ClassID",classID}
                                                    };
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilOfClassBusiness.GetProcessReportMeetingInvitation(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.ExportExcel(educationLevelID, classID, content, Time, Date, Place, SignalPerson, Position);
                processedReport = PupilOfClassBusiness.InsertProcessReportMeetingInvitation(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        
        public JsonResult GetNewReport(FormCollection frm)
        {
            int educationLevelID = !string.IsNullOrEmpty(frm["EducationLevelID"]) ? int.Parse(frm["EducationLevelID"]) : 0;
            int classID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            string content = !string.IsNullOrEmpty(frm["Content"]) ? frm["Content"] : "";
            string Time = frm["Time"];
            string Date = frm["Date"];
            string Place = !string.IsNullOrEmpty(frm["Place"]) ? frm["Place"] : "";
            string SignalPerson = !string.IsNullOrEmpty(frm["SignalPerson"]) ? frm["SignalPerson"] : "";
            string Position = !string.IsNullOrEmpty(frm["Position"]) ? frm["Position"] : "";
            //int ChildrenID = !string.IsNullOrEmpty(frm["ChildrenID"]) ? int.Parse(frm["ChildrenID"]) : 0;
            ClassProfile objClassProFile = ClassProfileBusiness.Find(classID);
            ProcessedReport processedReport = null;
            IDictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel},
                {"EducationLevelID",educationLevelID},
                {"ClassID",classID}
                //{"ChildrenId",ChildrenID}
            };
            Stream excel = this.ExportExcel(educationLevelID, classID, content, Time, Date, Place, SignalPerson, Position);
            processedReport = PupilOfClassBusiness.InsertProcessReportMeetingInvitation(dicInsert, excel);
            excel.Close();
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MeetingInvitationReport
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.MeetingInvitationReport
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        private List<int> GetListIDFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                idArr = str.Split('/');
            }
            else
            {
                idArr = new string[] { };
            }
            List<int> lstID = idArr.Length > 0 ? idArr.ToList().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return lstID;
        }

    }
}
