﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeachingAssignmentArea
{
    public class TeachingAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeachingAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeachingAssignmentArea_default",
                "TeachingAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
