﻿using Resources;
using SMAS.Business.BusinessObject;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class ImportTeachingAssignmentViewModel
    {
        public int? ClassSubjectID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ClassID { get; set; }

        /// <summary>
        /// ma to bo mon
        /// </summary>
        public int FacultyID { get; set; }

        /// <summary>
        /// to bo mon
        /// </summary>
        public string FacultyName { get; set; }

        [ResourceDisplayName("PupilProfile_Label_ClassName")]
        public string ClassName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int SubjectID { get; set; }
        [ResourceDisplayName("SubjectCat_Label_SubjectName1")]
        public string SubjectName { get; set; }

        /// <summary>
        /// ma giao vien
        /// </summary>
        public int TeacherID { get; set; }

        /// <summary>
        /// ma ky hieu giao vien
        /// </summary>
        [ResourceDisplayName("ImportClassSubjectAndTeacher_Label_TeacherCode")]
        public string TeacherCode { get; set; }

        //ho va ten
        [ResourceDisplayName("ImportClassSubjectAndTeacher_Label_TeacherName")]
        public string TeacherName { get; set; }

        /// <summary>
        /// phan cong giang day hoc ki 1
        /// </summary>
        public string AssginSemesterI { get; set; }

        /// <summary>
        /// Phan cogn giang day hoc ky 2
        /// </summary>
        public string AssignSemesterII { get; set; }

        public int? AppliedType { get; set; }
        [ResourceDisplayName("ClassSubject_Label_AppliedType")]
        public string AppliedTypeName { get; set; }

        public int? IsCommenting { get; set; }
        [ResourceDisplayName("ClassSubject_Label_IsCommenting")]
        public string IsCommentingName { get; set; }

        [ResourceDisplayName("ClassSubject_Label_FirstSemesterCoefficient1")]
        public Nullable<int> FirstSemesterCoefficient { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SecondSemesterCoefficient1")]
        public Nullable<int> SecondSemesterCoefficient { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekFirstSemester1")]
        public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }

        [ResourceDisplayName("ClassSubject_Label_SectionPerWeekSecondSemester1")]
        public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        public int? Semester { get; set; }

        [ResourceDisplayName("Common_Label_Semester")]
        public string SemesterName { get; set; }

        public bool isLegal { get; set; }

        public string ErrorDescription { get; set; }

        /// <summary>
        /// luu cac mon hoc duoc khai bao cho lop o hoc ki I
        /// </summary>
        public List<SubjectByClassBO> lstSubjectByClassHKI { get; set; }

        /// <summary>
        /// luu cac mon hoc duoc khai bao cho lop o hoc ki II
        /// </summary>
        public List<SubjectByClassBO> lstSubjectByClassHKII { get; set; }
    }
}