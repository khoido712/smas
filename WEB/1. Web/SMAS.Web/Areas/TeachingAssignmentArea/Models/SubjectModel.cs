﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class SubjectModel
    {
        public int SubjectID { get; set; }
        public string SubjcetAbbreviation { get; set; }
        public string SubjectName { get; set; }
        public int? OrderInSubject { get; set; }
    }
}