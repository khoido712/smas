using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class SearchByTeacherViewModel
    {
        [ResourceDisplayName("TeachingAssignment_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "Choice")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_SUBJECT)]
        public Nullable<int> SubjectID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_SEMESTER)]
        public Nullable<int> Semester { get; set; }

         [ScaffoldColumn(false)]
        public Nullable<int> TeacherID { get; set; }
    }
}
