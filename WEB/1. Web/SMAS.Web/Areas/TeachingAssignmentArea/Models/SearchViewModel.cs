/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("TeachingAssignment_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_SEMESTER)]
        public Nullable<int> Semester { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public Nullable<int> EducationLevel { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_CLASS)]
        public Nullable<int> ClassID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Faculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherSearch(this)")]
        public Nullable<int> FacultyID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Teacher")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_TEACHER)]
        public Nullable<int> TeacherID { get; set; }

        [ResourceDisplayName("")]
        [UIHint("ListRadio")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_TYPE)]
        public Nullable<int> Type { get; set; }
    }
}
