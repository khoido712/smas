﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class TeachingAssignmentByTeacherViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int? TeachingAssignmentID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Nullable<int> ClassID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Class")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string ClassName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Subject")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string SubjectName { get; set; }

      
        [ResourceDisplayName("TeachingAssignment_Label_Teacher")]
     
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_TEACHER)]
        public string TeacherName { get; set; }

        public decimal NumberSection1 { get; set; }
        public decimal NumberSection2 { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Semester")]
        public bool SemesterApp { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_ApplyToSemester")]
        public bool ApplyToSemester { get; set; }
        public bool ExistInOtherSemester { get; set; }

        public bool FirstSemester { get; set; }
        public bool SecondSemester { get; set; }
        public bool DisableFirstSemester { get; set; }
        public bool DisableSecondSemester { get; set; }

    }
    public class ChildrenTeachingAssignmentTreeViewModel
    {
        public int TeacherID { get; set; }
        public string TeacherName { get; set; }
    }


    public class TeacherInfoViewModel
    {
        public int TeacherID { get; set; }
        public decimal Semester1 { get; set; }
        public decimal Semester2 { get; set; }
        public int? SchoolFacultyID { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
    }

    public class TeachingAssignmentTreeViewModel
    {
        public int FacultyID { get; set; }
        public string FacultyName { get; set; }
        public string TraversalPath { get; set; }

        public List<ChildrenTeachingAssignmentTreeViewModel> ListChildren { get; set; }

        public TeachingAssignmentTreeViewModel()
        {
        }

        public TeachingAssignmentTreeViewModel(int FacultyID,
                                            string FacultyName,
                                            string TraversalPath,
                                            List<ChildrenTeachingAssignmentTreeViewModel> ListChildren)
        {
            this.FacultyID = FacultyID;
            this.FacultyName = FacultyName;
            this.TraversalPath = TraversalPath;
            this.ListChildren = ListChildren;
        }
    }
}
