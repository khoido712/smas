/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using System.ComponentModel;
namespace SMAS.Web.Areas.TeachingAssignmentArea.Models
{
    public class TeachingAssignmentViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public long? TeachingAssignmentID { get; set; }


        [HiddenInput(DisplayValue = false)]
        public Nullable<int> Semester { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Semester")]
        [UIHint("Display")]
        public string SemesterName
        {
            get
            {
                return this.Semester == GlobalConstants.FIRST_SEMESTER ?
                                        Res.Get("Common_Label_FirstSemester") :
                                        Res.Get("Common_Label_SecondSemester");
            }
        }


        [HiddenInput(DisplayValue = false)]
        public Nullable<int> ClassID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Class")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string ClassName { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Subject")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string SubjectName { get; set; }


        [ResourceDisplayName("TeachingAssignment_Label_Faculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacher(this)")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_FACULTY)]
        public Nullable<int> FacultyID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeachingAssignment_Label_Faculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherName(this)")]
        public string FacultyName { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_Teacher")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_TEACHER)]
        public int? TeacherID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeachingAssignment_Label_Teacher")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeachingAssignmentConstants.LIST_TEACHER)]
        public string TeacherName { get; set; }

        [ResourceDisplayName("TeachingAssignment_Label_ApplyToAllSemester")]
        [AdditionalMetadata("OnChange", "OnChangeApplyToAllSemester(this)")]
        public bool ApplyToAllSemester { get; set; }

        [ScaffoldColumn(false)]
        public bool CanAdd { get; set; }

        [ScaffoldColumn(false)]
        public bool CanEdit { get; set; }

        [ScaffoldColumn(false)]
        public bool CanDelete { get; set; }

        [ScaffoldColumn(false)]
        public int? ClassOrderNumber { get; set; }

        [ScaffoldColumn(false)]
        public int? EducationLevelID { get; set; }


        [ScaffoldColumn(false)]
        public string Name { get; set; }
    }
}


