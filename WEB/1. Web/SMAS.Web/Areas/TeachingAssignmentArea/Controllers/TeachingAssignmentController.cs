﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.TeachingAssignmentArea.Models;
using SMAS.Web.Areas.TeachingAssignmentArea;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using System.Text;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.TeachingAssignmentArea.Controllers
{
    public class TeachingAssignmentController : BaseController
    {
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            // Danh sách học kỳ
            ViewData[TeachingAssignmentConstants.LIST_SEMESTER] = CommonFunctions.GetListSemesterSelectedDefault();

            // Danh sách tổ bộ môn
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[TeachingAssignmentConstants.LIST_FACULTY] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");

            // Danh sách giáo viên
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[TeachingAssignmentConstants.LIST_TEACHER] = new SelectList(ListTeacher, "EmployeeID", "FullName");


            // Danh sách khối
            List<EducationLevel> ListEducationLevel = GlobalInfo.EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }
            ViewData[TeachingAssignmentConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution");

            // Danh sách lớp
            List<ClassProfile> ListClassProfile = new List<ClassProfile>();
            ViewData[TeachingAssignmentConstants.LIST_CLASS] = new SelectList(ListClassProfile, "ClassProfileID", "DisplayName");

            // Phân loại
            List<ViettelCheckboxList> ListType = new List<ViettelCheckboxList>();
            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_All"),
                "",
                true,
                false
            ));

            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_UnAssign"),
                "1",
                false,
                false
            ));

            ListType.Add(new ViettelCheckboxList(
                Res.Get("TeachingAssignment_Label_Type_Assign"),
                "2",
                false,
                false
            ));
            ViewData[TeachingAssignmentConstants.LIST_TYPE] = ListType;


            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["AppliedLevel"] = GlobalInfo.AppliedLevel;
            SearchInfo["Semester"] = GlobalInfo.Semester.Value;
            IEnumerable<TeachingAssignmentViewModel> lst = this._Search(SearchInfo);
            ViewData[TeachingAssignmentConstants.LIST_TEACHINGASSIGNMENT] = lst;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            List<SubjectCat> lstSubject = SchoolSubjectBusiness.SearchBySchool(
                GlobalInfo.SchoolID.GetValueOrDefault(), dic).Select(x => x.SubjectCat).
                Distinct().OrderBy(x => x.OrderInSubject).ToList();

            ViewData[TeachingAssignmentConstants.LIST_SUBJECT] = lstSubject;
            ViewData[TeachingAssignmentConstants.HAS_PERMISSION] = GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            //var lsttree = CommonFunctions.TreeTreeFacultyModelByUser(TeachingAssignmentBusiness, SchoolFacultyBusiness, EmployeeBusiness);
            //IDictionary<string, object> MinenalSearchInfo = new Dictionary<string, object>();

            //List<TeachingAssignmentTreeViewModel> ListEatingGroupNode = new List<TeachingAssignmentTreeViewModel>();
            //if (lsttree != null)
            //{
            //    ListEatingGroupNode = lsttree;
            //}
            //ViewData[TeachingAssignmentConstants.LT_TEACHING_ASSIGNMENT_TREE] = ListEatingGroupNode;
            ViewData["ListSchoolFaculty"] = ListFaculty.OrderBy(x => x.FacultyName).ToList();

            var ListEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(GlobalInfo.SchoolID.Value).ToList(); ;

            ViewData["ListTeacherOfFaculty"] = ListEmployee;
            // Lay danh sach giao vien va so tiet day tuong ung
            Dictionary<int, string> dicTA = TeachingAssignmentBusiness.GetSectionPerWeekInYear(GlobalInfo.SchoolID.GetValueOrDefault(),
                GlobalInfo.AcademicYearID.GetValueOrDefault(), GlobalInfo.AppliedLevel.GetValueOrDefault());

            //lay thong tin giao vien
            List<TeacherInfoViewModel> listTeacherInfoViewModel = new List<TeacherInfoViewModel>();
            foreach (Employee emp in ListEmployee)
            {
                TeacherInfoViewModel modelTeacher = new TeacherInfoViewModel();
                modelTeacher.TeacherID = emp.EmployeeID;
                if (dicTA.ContainsKey(emp.EmployeeID))
                {
                    string spwInfo = dicTA[emp.EmployeeID];
                    string[] arrSpw = spwInfo.Split('_');
                    //int spw1 = int.Parse(arrSpw[0]);
                    //int spw2 = int.Parse(arrSpw[1]);                    
                    decimal spw1, spw2;
                    spw1 = spw2 = 0;
                    decimal.TryParse(arrSpw[0].Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out spw1);
                    decimal.TryParse(arrSpw[1].Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out spw2);
                    modelTeacher.Semester1 = spw1;
                    modelTeacher.Semester2 = spw2;
                }
                modelTeacher.Name = emp.Name;
                modelTeacher.FullName = emp.FullName;
                modelTeacher.SchoolFacultyID = emp.SchoolFacultyID;
                listTeacherInfoViewModel.Add(modelTeacher);
            }
            ViewData["LIST_TEACHER_INFO"] = listTeacherInfoViewModel;


        }



        public TeachingAssignmentController(ITeachingAssignmentBusiness TeachingAssignmentBusiness,
                                            ISchoolFacultyBusiness SchoolFacultyBusiness,
                                            IClassProfileBusiness ClassProfileBusiness,
                                            IEmployeeBusiness EmployeeBusiness,
                                            ISubjectCatBusiness SubjectCatBusiness,
                                            ISchoolSubjectBusiness SchoolSubjectBusiness,
                                            IClassSubjectBusiness ClassSubjectBusiness,
                                            IAcademicYearBusiness AcademicYearBusiness,
                                            IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness,
                                            IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness,
                                            ISchoolProfileBusiness SchoolProfileBusiness,
                                            ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassSupervisorAssignmentBusiness = ClassSupervisorAssignmentBusiness;
            this.ConcurrentWorkAssignmentBusiness = ConcurrentWorkAssignmentBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        //
        // GET: /TeachingAssignment/
        public struct ErrSubjectClass
        {
            public string ErrSubjectName;
            public string ErrClass;
        }
        public ActionResult Index(int? EmployeeId)
        {
            CheckPermissionForAction(EmployeeId, "Employee");
            if (EmployeeId == null)
            {
                EmployeeId = 0;
            }
            else
            {
                Employee Employee = EmployeeBusiness.Find(EmployeeId);
                if (Employee != null)
                {
                    ViewData[TeachingAssignmentConstants.SCHOOLFACULTYID] = Employee.SchoolFacultyID;
                }
            }
            ViewData[TeachingAssignmentConstants.EMPLOYEEID] = EmployeeId;


            SetViewData();
            //Get view data here
            return View();
        }

        //
        // GET: /TeachingAssignment/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            //add search info
            SearchInfo["Semester"] = frm.Semester;
            SearchInfo["EducationLevel"] = frm.EducationLevel;
            SearchInfo["ClassID"] = frm.ClassID;
            SearchInfo["FacultyID"] = frm.FacultyID;
            SearchInfo["TeacherID"] = frm.TeacherID;
            SearchInfo["AppliedLevel"] = GlobalInfo.AppliedLevel;
            SearchInfo["Type"] = frm.Type;
            //

            List<TeachingAssignmentViewModel> lst = this._Search(SearchInfo).ToList();
           
            ViewData[TeachingAssignmentConstants.LIST_TEACHINGASSIGNMENT] = lst;

            //Get view data here
            // Danh sách tổ bộ môn phục vụ cho việc render UIHint khi Edit trên Grid
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[TeachingAssignmentConstants.LIST_FACULTY] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");
            ViewData[TeachingAssignmentConstants.HAS_PERMISSION] = GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(TeachingAssignmentViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            TeachingAssignment teachingassignment = new TeachingAssignment();
            TryUpdateModel(teachingassignment);
            teachingassignment.SchoolID = GlobalInfo.SchoolID.Value;
            Utils.Utils.TrimObject(teachingassignment);
            if (frm.FacultyID == null || frm.FacultyID == ' ' || frm.FacultyID == 0)
            {
                return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Exception1"), "error"));
            }
            if (frm.TeacherID == null || frm.TeacherID == ' ' || frm.TeacherID == 0)
            {
                return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Exception2"), "error"));
            }

            teachingassignment.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            teachingassignment.IsActive = true;
            teachingassignment.Last2digitNumberSchool = partitionId;
            string year = DateTime.Now.Year.ToString(); ;
            string month = DateTime.Now.Month.ToString("00");
            string day = DateTime.Now.Day.ToString("00");
            string date = year + "-" + month + "-" + day;
            teachingassignment.AssignedDate = Convert.ToDateTime(date);
            teachingassignment.TeachingAssignmentID = 0;
            this.TeachingAssignmentBusiness.Insert(teachingassignment);
            this.TeachingAssignmentBusiness.Save();

            if (frm.ApplyToAllSemester)
            {
                teachingassignment = new TeachingAssignment();
                TryUpdateModel(teachingassignment);
                teachingassignment.SchoolID = GlobalInfo.SchoolID.Value;
                Utils.Utils.TrimObject(teachingassignment);

                teachingassignment.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                teachingassignment.IsActive = true;
                teachingassignment.Last2digitNumberSchool = partitionId;
                if (teachingassignment.Semester == SMAS.Web.Constants.GlobalConstants.FIRST_SEMESTER)
                {
                    teachingassignment.Semester = SMAS.Web.Constants.GlobalConstants.SECOND_SEMESTER;
                }
                else
                {
                    teachingassignment.Semester = SMAS.Web.Constants.GlobalConstants.FIRST_SEMESTER;
                }
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = teachingassignment.AcademicYearID;
                SearchInfo["TeacherID"] = teachingassignment.TeacherID;
                SearchInfo["ClassID"] = teachingassignment.ClassID;
                SearchInfo["Semester"] = teachingassignment.Semester;
                SearchInfo["SubjectID"] = teachingassignment.SubjectID;
                SearchInfo["IsActive"] = true;                
                if (_Search(SearchInfo).Count() == 0)
                {
                    this.TeachingAssignmentBusiness.Insert(teachingassignment);
                    this.TeachingAssignmentBusiness.Save();
                }
            }


            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int? id, TeachingAssignmentViewModel frm)
        {
            if (id == null)
            {
                return Create(frm);
            }
            else
            {
                this.CheckPermissionForAction(id, "TeachingAssignment");
                GlobalInfo GlobalInfo = new GlobalInfo();
                if (this.GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                if (frm.FacultyID == null || frm.FacultyID == ' ' || frm.FacultyID == 0)
                {
                    return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Exception1"), "error"));
                }
                if (frm.TeacherID == null || frm.TeacherID == ' ' || frm.TeacherID == 0)
                {
                    return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Exception2"), "error"));
                }
                this.TeachingAssignmentBusiness.Update(GlobalInfo.SchoolID.Value, id.Value, frm.FacultyID.Value, frm.TeacherID.Value, frm.ApplyToAllSemester);
                this.TeachingAssignmentBusiness.Save();

                /*
                if (frm.ApplyToAllSemester)
                {
                    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                    SearchInfo["SchoolID"] = GlobalInfo.SchoolID;
                    SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
                    //add search info
                    SearchInfo["Semester"] = frm.Semester == GlobalConstants.FIRST_SEMESTER ? GlobalConstants.SECOND_SEMESTER : GlobalConstants.FIRST_SEMESTER;
                    SearchInfo["ClassID"] = frm.ClassID;
                    SearchInfo["FacultyID"] = oldFacultyID;
                    SearchInfo["AppliedLevel"] = GlobalInfo.AppliedLevel;
                    SearchInfo["TeacherID"] = oldTeacherID;
                    //

                    IEnumerable<TeachingAssignmentViewModel> lst = this._Search(SearchInfo);

                    TeachingAssignmentViewModel taViewModel = lst.FirstOrDefault();
                    if (taViewModel != null)
                    {
                        TeachingAssignment teachingassignment2 = this.TeachingAssignmentBusiness.Find(taViewModel.TeachingAssignmentID);
                        TryUpdateModel(teachingassignment2);
                        Utils.Utils.TrimObject(teachingassignment2);
                        this.TeachingAssignmentBusiness.Update(teachingassignment2, GlobalInfo.SchoolID.Value);
                        this.TeachingAssignmentBusiness.Save();
                    }
                    else
                    {
                        TeachingAssignment teachingassignment2 = new TeachingAssignment();
                        TryUpdateModel(teachingassignment2);
                        Utils.Utils.TrimObject(teachingassignment2);

                        teachingassignment.AcademicYearID = GlobalInfo.AcademicYearID.Value;
                        teachingassignment.IsActive = true;

                        teachingassignment.Semester = frm.Semester == GlobalConstants.FIRST_SEMESTER ? (int)GlobalConstants.SECOND_SEMESTER : (int)GlobalConstants.FIRST_SEMESTER;

                        this.TeachingAssignmentBusiness.Insert(teachingassignment);
                        this.TeachingAssignmentBusiness.Save();
                    }

                }
                */
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id, bool ApplyToAllSemester)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            this.CheckPermissionForAction(id, "TeachingAssignment");
            if (this.GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.TeachingAssignmentBusiness.Delete(GlobalInfo.SchoolID.Value, id, ApplyToAllSemester);
            this.TeachingAssignmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Load dữ liệu cho commbobox Giáo viên khi chọn Tổ bộ môn
        /// </summary>
        /// <param name="FacultyID"></param>
        /// <returns></returns>
        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacher(int? FacultyID, int? TeacherIDHid)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (FacultyID != null)
            {

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolFacultyID"] = FacultyID;
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value).OrderBy(o => o.FullName).ToList();
                if (TeacherIDHid != 0)
                {
                    lst = lst.Where(o => o.EmployeeID != TeacherIDHid);
                }
            }
            if (lst == null)
            {
                lst = new List<Employee>();
            }
            else
            {
                lst = lst.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            }

            // lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
        [SkipCheckRole]
        public JsonResult AjaxLoadTeacherSearch(int? FacultyID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (FacultyID != null)
            {

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolFacultyID"] = FacultyID;
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value).OrderBy(o => o.FullName).ToList();
            }
            if (lst == null)
            {
                lst = new List<Employee>();
            }
            else
            {
                lst = lst.OrderBy(p=> SMAS.Business.Common.Utils.SortABC(p.Name +" "+p.FullName)).ToList();
            }

            // lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacherAdd(int? FacultyID, int TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (FacultyID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolFacultyID"] = FacultyID;
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value).Where(o => o.EmployeeID != TeacherID).OrderBy(o => o.FullName).AsEnumerable();
            }
            if (lst == null)
            {
                lst = new List<Employee>();
            }
            else
            {
                lst = lst.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
            }

            // lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            GlobalInfo glo = new GlobalInfo();
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = EducationLevelID;
                dic["AcademicYearID"] = glo.AcademicYearID;
                lst = this.ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value, dic).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TeachingAssignmentViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            int? UNASSIGN = 1;
            int? ASSIGNED = 2;
            //IQueryable<TeachingAssignmentBO> query = this.TeachingAssignmentBusiness.GetTeachingAssigment(GlobalInfo.SchoolID.Value, SearchInfo).Where(o => o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING || o.EmploymentStatus == null);
            IQueryable<TeachingAssignmentBO> query = this.TeachingAssignmentBusiness.GetTeachingAssigment(GlobalInfo.SchoolID.Value, SearchInfo).Where(o => o.SectionPerWeek > 0);
            int Type = SMAS.Business.Common.Utils.GetInt(SearchInfo, "Type");
            if (Type == UNASSIGN)
            {
                query = query.Where(o => o.TeacherID == null && o.FacultyID == null);
            }
            if (Type == ASSIGNED)
            {
                query = query.Where(o => o.TeacherID != null && o.FacultyID != null);
            }

            IQueryable<TeachingAssignmentViewModel> lst = query.Select(o => new TeachingAssignmentViewModel
            {
                TeachingAssignmentID = o.TeachingAssignmentID,
                Semester = o.Semester,
                EducationLevelID = o.EducationLevelID,
                ClassID = o.ClassID,
                ClassName = o.ClassName,
                ClassOrderNumber = o.ClassOrderNumber,
                SubjectID = o.SubjectID,
                SubjectName = o.SubjectName,
                FacultyID = o.FacultyID,
                FacultyName = o.FacultyName,
                TeacherID = o.TeacherID,
                TeacherName = o.TeacherName,
                ApplyToAllSemester = !o.OnlyOneSemester,
                CanAdd = o.TeacherID != null,
                CanEdit = true,
                CanDelete = o.TeacherID != null,
                Name = o.Name
            });

            //return lst.OrderBy(o => o.SubjectName).ThenBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();
            return lst.OrderBy(p=> SMAS.Business.Common.Utils.SortABC(p.Name +" "+p.TeacherName));
        }

        public PartialViewResult LoadTabByTeacher()
        {
            SetViewData();
            ViewData[TeachingAssignmentConstants.EMPLOYEEID] = 0;
            return PartialView("_TAByTeacher");
        }

        public PartialViewResult LoadTabByClass()
        {
            SetViewData();
            return PartialView("_TAByClass");
        }
        /// <summary>
        /// phuongh1
        /// 18/04/2013
        /// </summary>
        /// <param name="SubjectID"></param>
        /// <param name="Semester"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchByTeacher(int SubjectID, int TeacherID)
        {
            GlobalInfo gi = new GlobalInfo();
            Employee teacher = EmployeeBusiness.Find(TeacherID);
            bool isCurentYear = gi.IsCurrentYear;
            bool isSecondSemester = AcademicYearBusiness.IsSecondSemesterTime(gi.AcademicYearID.GetValueOrDefault());
            List<TeachingAssignmentByTeacherViewModel> lst = new List<TeachingAssignmentByTeacherViewModel>();
            //Lấy danh sách phân công giảng dạy
            IQueryable<TeachingAssignment> iQTA = TeachingAssignmentBusiness.SearchBySchool(gi.SchoolID.GetValueOrDefault(), new Dictionary<string, object>
            {
                {"AcademicYearID", gi.AcademicYearID.GetValueOrDefault()},
                {"AppliedLevel", gi.AppliedLevel.GetValueOrDefault()},
                {"SubjectID", SubjectID}
            });
            List<TeachingAssignmentBO> listTA = new List<TeachingAssignmentBO>();
            if (gi.IsCurrentYear)
            {
                listTA = (from p in iQTA
                          join q in EmployeeBusiness.All on p.TeacherID equals q.EmployeeID
                          where q.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                          select new TeachingAssignmentBO
                          {
                              ClassID = p.ClassID,
                              SubjectID = p.SubjectID,
                              TeacherID = p.TeacherID,
                              Semester = p.Semester,
                              TeacherName = q.FullName
                          }).ToList();
            }
            else
            {
                listTA = iQTA.Select(p => new TeachingAssignmentBO
                          {
                              ClassID = p.ClassID,
                              SubjectID = p.SubjectID,
                              TeacherID = p.TeacherID,
                              Semester = p.Semester,
                              TeacherName = p.Employee.FullName
                          }).ToList();
            }
            //Lấy danh sách lớp học theo môn
            var listClassSubject = ClassSubjectBusiness.SearchBySchool(gi.SchoolID.GetValueOrDefault(), new Dictionary<string, object>
            {
                {"AcademicYearID", gi.AcademicYearID.GetValueOrDefault()},
                {"AppliedLevel", gi.AppliedLevel.GetValueOrDefault()},
                {"SubjectID", SubjectID}
            }).Select(x => new
            {
                x.ClassProfile.EducationLevelID,
                ClassName = x.ClassProfile.DisplayName,
                SubjectName = x.SubjectCat.DisplayName,
                x.ClassID,
                x.SubjectID,
                x.SectionPerWeekFirstSemester,
                x.SectionPerWeekSecondSemester
            })
            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.ClassName).ToList();
            // Lay danh sach lop hoc theo mon dau vao va tong so tiet tuong ung
            Dictionary<int, string> dicCS = TeachingAssignmentBusiness.GetSectionPerClassBySubjectInYear(gi.SchoolID.GetValueOrDefault(), gi.AcademicYearID.GetValueOrDefault(), SubjectID);

            //Tạo list model để hiển thị
            foreach (var cs in listClassSubject)
            {
                TeachingAssignmentByTeacherViewModel model = new TeachingAssignmentByTeacherViewModel();
                model.ClassID = cs.ClassID;
                model.ClassName = cs.ClassName;
                model.SubjectID = cs.SubjectID;
                model.SubjectName = cs.SubjectName;
                model.FirstSemester = false;
                if (listTA.Exists(x => x.ClassID == cs.ClassID
                    && x.SubjectID == cs.SubjectID
                    && x.TeacherID == TeacherID
                    && x.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST))
                {
                    model.FirstSemester = true;
                }
                model.SecondSemester = false;
                if (listTA.Exists(x => x.ClassID == cs.ClassID
                    && x.SubjectID == cs.SubjectID
                    && x.TeacherID == TeacherID
                    && x.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND))
                {
                    model.SecondSemester = true;
                }
                model.DisableFirstSemester = false;
                if (cs.SectionPerWeekFirstSemester.GetValueOrDefault() == 0
                    || !isCurentYear || isSecondSemester)
                {
                    model.DisableFirstSemester = true;
                }
                model.DisableSecondSemester = false;
                if (cs.SectionPerWeekSecondSemester.GetValueOrDefault() == 0
                    || !isCurentYear)
                {
                    model.DisableSecondSemester = true;
                }
                List<string> listTeacher = listTA.FindAll(x => x.ClassID == cs.ClassID
                    && x.SubjectID == cs.SubjectID
                    && x.TeacherID != TeacherID)
                    .Select(x => x.TeacherName).Distinct().ToList();
                if (model.FirstSemester || model.SecondSemester)
                {
                    listTeacher.Add(teacher.FullName);
                }
                if (dicCS.ContainsKey(cs.ClassID))
                {
                    string spwInfo = dicCS[cs.ClassID];
                    string[] arrSpw = spwInfo.Split('_');
                    decimal NumberSection1 = 0;
                    decimal.TryParse(arrSpw[0].Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out NumberSection1);
                    model.NumberSection1 = NumberSection1;
                    decimal NumberSection2 = 0;
                    decimal.TryParse(arrSpw[1].Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out NumberSection2);
                    model.NumberSection2 = NumberSection2;
                }
                model.TeacherName = string.Join(", ", listTeacher);
                lst.Add(model);
            }

            ViewData[TeachingAssignmentConstants.LT_TABY_TEACHER] = lst;
            ViewData["TeacherName"] = teacher != null ? teacher.FullName : "";
            ViewData["SubjectID"] = SubjectID;
            ViewData["TeacherID"] = TeacherID;
            ViewData["IsCurrentYear"] = gi.IsCurrentYear;
            ViewData[TeachingAssignmentConstants.HAS_PERMISSION] = GetMenupermission("TeachingAssignment", gi.UserAccountID, gi.IsAdmin);
            return PartialView("_ListByTeacher");

        }

        [GridAction(EnableCustomBinding = true)]

        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(int SubjectID, int Semester, int? TeacherID)
        {
            IEnumerable<TeachingAssignmentByTeacherViewModel> paging = this._Search(SubjectID, Semester, TeacherID);
            GridModel<TeachingAssignmentByTeacherViewModel> gm = new GridModel<TeachingAssignmentByTeacherViewModel>(paging);
            gm.Total = paging.Count();
            return View(gm);
        }

        /// <summary>
        /// Tamhm1
        /// 18/01/2013
        /// </summary>
        /// <param name="SubjectID"></param>
        /// <param name="Semester"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        private IEnumerable<TeachingAssignmentByTeacherViewModel> _Search(int SubjectID, int Semester, int? TeacherID)
        {
            ViewData["Semester"] = Semester;
            GlobalInfo GlobalInfo = new GlobalInfo();
            // Dieu kien truy van du lieu

            if (TeacherID == null || TeacherID == 0)
            {
                return null;
            }

            //Môn học
            string SubjectName = SubjectCatBusiness.Find(SubjectID).SubjectName;
            //Giáo viên
            string TeacherName = EmployeeBusiness.Find(TeacherID).FullName;
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            // dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
            IQueryable<TeachingAssignmentBO> lsTA = TeachingAssignmentBusiness.GetTeachingAssigment(GlobalInfo.SchoolID.Value, dic);
            List<TeachingAssignmentBO> lstTA = new List<TeachingAssignmentBO>();
            if (lsTA != null && lsTA.Count() > 0)
            {
                lstTA = lsTA.OrderBy(o => o.SubjectName).ThenBy(o => o.ClassName).ToList();
            }
            //Số tiết dạy của giáo viên
            decimal NumberOfTeaching = lstTA.Where(o => o.TeacherID == TeacherID).Sum(o => o.SectionPerWeek).GetValueOrDefault();
            ViewData[TeachingAssignmentConstants.lblMessage] = "Giáo viên " + TeacherName + " đã dạy " + NumberOfTeaching + " tiết/tuần ở học kỳ " + Semester;
            var list = (from p in lstTA
                        where p.SectionPerWeek != 0 && (p.TeacherID == TeacherID || p.TeacherID == null)
                        select new TeachingAssignmentByTeacherViewModel
                        {
                            ClassID = p.ClassID,
                            ClassName = p.ClassName,
                            SubjectName = SubjectName,
                            TeacherName = TeacherName,
                            SemesterApp = p.TeacherID == TeacherID ? true : false,
                            ApplyToSemester = p.OnlyOneSemester == false ? true : false,
                            ExistInOtherSemester = p.ExistInOtherSemester == false ? true : false
                        }).ToList();
            //KHởi tạo biến để check/uncheck header
            int numberOfCheck = 0;
            foreach (var q in list)
            {
                if (q.SemesterApp == true)
                {
                    numberOfCheck++;
                }
            }
            ViewData[TeachingAssignmentConstants.NumberOfCheck] = numberOfCheck;
            ViewData[TeachingAssignmentConstants.HAS_PERMISSION] = GetMenupermission("TeachingAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            return list;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(FormCollection col, int SubjectID, int Semester, int? TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            //Hàm lấy khoa tổ bộ môn
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            SearchInfo["EmployeeID"] = TeacherID;
            SearchInfo["IsActive"] = 1;
            var employee = EmployeeBusiness.SearchTeacher(global.SchoolID.Value, SearchInfo).FirstOrDefault();
            if (employee == null)
            {
                throw new BusinessException("Employee_Label_NotWorkingStatusErrr");
            }
            int? FacultyID = employee.SchoolFacultyID;
            var lstSemester1 = col["SemesterApp"];
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            //dic["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            List<TeachingAssignmentBO> lstTA = TeachingAssignmentBusiness.GetTeachingAssigment(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.TeacherName).ToList();
            //Môn học
            string SubjectName = SubjectCatBusiness.Find(SubjectID).SubjectName;
            //Giáo viên            
            string TeacherName = employee.FullName;
            var query = from p in lstTA
                        where p.SectionPerWeek != 0 && (p.TeacherID == TeacherID || p.TeacherID == null)
                        select new TeachingAssignmentByTeacherViewModel
                        {
                            ClassID = p.ClassID,
                            ClassName = p.ClassName,
                            SubjectName = SubjectName,
                            TeacherName = TeacherName,
                            SemesterApp = p.TeacherID == TeacherID ? true : false,
                            ApplyToSemester = p.OnlyOneSemester == false ? true : false,
                            ExistInOtherSemester = p.ExistInOtherSemester == true ? true : false
                        };
            IEnumerable<TeachingAssignmentByTeacherViewModel> listTeacher = query.ToList();
            List<TeachingAssignment> lstTeachingAssignment = new List<TeachingAssignment>();

            foreach (TeachingAssignmentByTeacherViewModel TeachingAssignmentByTeacherViewModel in listTeacher)
            {
                int ClassID = TeachingAssignmentByTeacherViewModel.ClassID.Value;
                string valCheck = col["SemesterApp" + ClassID.ToString()];
                if (valCheck != null && valCheck.ToLower() == "true")
                {
                    TeachingAssignment ta = new TeachingAssignment();
                    ta.AcademicYearID = global.AcademicYearID.Value;
                    ta.SchoolID = global.SchoolID.Value;
                    ta.TeacherID = TeacherID.Value;
                    ta.SubjectID = Convert.ToInt16(SubjectID);
                    ta.Semester = Convert.ToByte(Semester);
                    ta.FacultyID = employee.SchoolFacultyID;
                    ta.ClassID = ClassID;
                    ta.AssignedDate = DateTime.Now;
                    ta.IsActive = true;
                    ta.Last2digitNumberSchool = partitionId;
                    lstTeachingAssignment.Add(ta);
                    //-----
                }
                string valAppSemester = col["ApplyToSemester" + ClassID.ToString()];
                if (valAppSemester != null && valAppSemester == "true")
                {
                    TeachingAssignment ta = new TeachingAssignment();
                    ta.AcademicYearID = global.AcademicYearID.Value;
                    ta.SchoolID = global.SchoolID.Value;
                    ta.TeacherID = TeacherID.Value;
                    ta.SubjectID = Convert.ToInt16(SubjectID);
                    ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    ta.FacultyID = FacultyID;
                    ta.ClassID = ClassID;
                    ta.AssignedDate = DateTime.Now;
                    ta.IsActive = true;
                    ta.Last2digitNumberSchool = partitionId;
                    lstTeachingAssignment.Add(ta);
                }
            }


            //Thực hiện gọi hàm           
            TeachingAssignmentBusiness.UpdateForEmployee(global.SchoolID.Value, global.AcademicYearID.Value, global.AppliedLevel.Value, TeacherID.Value, Semester, lstTeachingAssignment);
            this.TeachingAssignmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Success")));
        }

        /// <summary>
        /// phuongh1
        /// 18/04/2013
        /// </summary>
        /// <param name="data"></param>
        /// <param name="SubjectID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveTeacher(string data, int SubjectID, int TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("TeachingAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Lấy dữ liệu từ data
            List<TeachingAssignment> listData = new List<TeachingAssignment>();
            string[] arrObj = data.Split(',');
            foreach (string ele in arrObj)
            {
                if (!string.IsNullOrWhiteSpace(ele))
                {
                    TeachingAssignment ta = new TeachingAssignment();
                    string[] arrEle = ele.Split('_');
                    ta.ClassID = Int32.Parse(arrEle[0]);
                    ta.Semester = Int32.Parse(arrEle[2]);
                    listData.Add(ta);
                }
            }

            TeachingAssignmentBusiness.UpdateForSubjectAndEmployee(global.SchoolID.GetValueOrDefault(),
                global.AcademicYearID.GetValueOrDefault(), global.AppliedLevel.GetValueOrDefault(),
                TeacherID, SubjectID, listData);

            return Json(new JsonMessage(Res.Get("TeachingAssignment_Label_Success")));
        }

        /// <summary>
        /// phuongh1
        /// 22/04/2013
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetTeacherInfo(int TeacherID)
        {
            CheckPermissionForAction(TeacherID, "Employee");
            GlobalInfo gi = new GlobalInfo();
            Employee teacher = EmployeeBusiness.Find(TeacherID);
            string message = Res.Get("TeachingAssignment_Message_SectionPerWeek");
            decimal spw1 = TeachingAssignmentBusiness.GetSectionPerWeek(gi.SchoolID.GetValueOrDefault(),
                gi.AcademicYearID.GetValueOrDefault(), gi.AppliedLevel.GetValueOrDefault(),
                TeacherID, 0, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
            decimal spw2 = TeachingAssignmentBusiness.GetSectionPerWeek(gi.SchoolID.GetValueOrDefault(),
                gi.AcademicYearID.GetValueOrDefault(), gi.AppliedLevel.GetValueOrDefault(),
                TeacherID, 0, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            return Json(new JsonMessage(string.Format(message, teacher.FullName, spw1, spw2), spw1 + "-" + spw2));
        }

        public FileResult ExportExcel()
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/" + TeachingAssignmentConstants.FILE_TEMPLATE;
            int applicationLevel = _globalInfo.AppliedLevel.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            AcademicYear objay = AcademicYearBusiness.Find(academicYearId);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lay ra danh sach ca sheet
            IVTWorksheet tempSheet = oBook.GetSheet(1);
            tempSheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper());
            tempSheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            tempSheet.SetCellValue("A6", "Năm học: " + objay.DisplayTitle);
            //lay du lieu
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID", academicYearId},
                {"SchoolID", schoolId},
                {"AppliedLevel", applicationLevel},
                {"IsActive", true}
            };

            //ds phan cong giang day cua can bo
            List<TeachingAssignmentBO> lstTAbo = TeachingAssignmentBusiness.GetTeachingAssignmentbyAcademicYear(schoolId, academicYearId, applicationLevel);
            //danh sach can bộ
            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolId, new Dictionary<string, object>() {
                            { "IsActive", true },
                            { "CurrentEmployeeStatus", SystemParamsInFile.EMPLOYMENT_STATUS_WORKING },
                            {"AcademicYearID",academicYearId},
                            {"EmploymentStatus",SystemParamsInFile.EMPLOYMENT_STATUS_WORKING}
                            }).ToList().OrderBy((c => c.SchoolFacultyID))
                                        .OrderBy(c => Utils.Utils.SortABC(c.Name))
                                        .ThenBy(c => Utils.Utils.SortABC(c.FullName))
                                        .ToList();

            //khoi tao 
            Employee objEmployee;
            List<TeachingAssignmentBO> lstTempSemesterI = new List<TeachingAssignmentBO>();
            List<TeachingAssignmentBO> lstTempSemesterII = new List<TeachingAssignmentBO>();
            string teachingAssignSemester1;
            string teachingAssignSemester2;
            int countInfo = lstEmployee.Count;
            int startRowPCGD = 9;
            for (int i = 0; i < lstEmployee.Count; i++)
            {
                objEmployee = lstEmployee[i];
                lstTempSemesterI = lstTAbo.Where(t => t.Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST
                                                  && t.TeacherID == objEmployee.EmployeeID).ToList();
                lstTempSemesterII = lstTAbo.Where(t => t.Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND
                                                  && t.TeacherID == objEmployee.EmployeeID).ToList();
                tempSheet.SetCellValue(startRowPCGD, 1, i + 1);
                tempSheet.SetCellValue(startRowPCGD, 2, objEmployee.FullName); //ho ten
                tempSheet.SetCellValue(startRowPCGD, 3, objEmployee.SchoolFaculty.FacultyName); //to bo mon 
                teachingAssignSemester1 = GetAssignSemesterByTeacher(lstTempSemesterI);
                teachingAssignSemester2 = GetAssignSemesterByTeacher(lstTempSemesterII);
                tempSheet.SetCellValue(startRowPCGD, 4, teachingAssignSemester1); //PCGD hoc ky I
                tempSheet.SetCellValue(startRowPCGD, 5, teachingAssignSemester2); //PCGD hoc ky II
                startRowPCGD++;
            }
            tempSheet.GetRange(9, 1, startRowPCGD - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = "TeachingAssignment.xls";
            string ReportName = fileName;
            result.FileDownloadName = ReportName;
            return result;
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res1.ContentType = "text/plain";
                    return res1;
                    //return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                JsonResult res = Json(new JsonMessage(""));
                res.ContentType = "text/plain";
                return res;
                //return Json(new JsonMessage(""));
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel()
        {
            string FilePath = (string)Session["FilePath"];
            IVTWorkbook book = VTExport.OpenWorkbook(FilePath);
            bool GoodTemplate = checkExcelTemplate(book);
            if (!GoodTemplate)
            {
                JsonResult res2 = Json(new JsonMessage("File import không đúng mẫu.", "errortemplate"));
                return res2;
            }
            else
            {
                //neu dung mau template,bat dau kiem tra du lieu,neu co loi ->ban ra popup va hien grid
                List<ImportTeachingAssignmentViewModel> lsData = getDataFromImportFile(book);//TODO:chua viet getDataFromImportFile
                Session["ImportData"] = lsData;
                if (lsData.Where(o => !o.isLegal).Count() > 0)
                {
                    ViewData[TeachingAssignmentConstants.ERROR_IMPORT_DATA_IN_GRID] = "Trong file excel có dữ liệu không hợp lệ.";
                    ViewData[TeachingAssignmentConstants.LIST_IMPORTDATA] = lsData;
                    ViewData[TeachingAssignmentConstants.ERROR_IMPORT_MESSAGE] = "File import không đúng mẫu.Thầy/cô hãy sử dụng mẫu import theo link bên dưới";
                    JsonResult res3 = Json(new JsonMessage(RenderPartialViewToString("_ListData", null), "grid"));
                    return res3;
                }
                else 
                {
                    return SaveLegalDataImport(lsData);
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveLegalDataImport(List<ImportTeachingAssignmentViewModel> LegalData)
        {
            if (LegalData == null || LegalData.Count == 0)
            {
                LegalData = (List<ImportTeachingAssignmentViewModel>)Session["ImportData"];
            }
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            ViewData[TeachingAssignmentConstants.IS_PERMISSION] = _globalInfo.IsCurrentYear;
            // Lay tat ca thong tin mon hoc cua lop trong truong
            List<ClassSubject> lsCS = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AcademicYearID",academicYearId}
                }).ToList();

            IDictionary<string, object> searchInfoTA = new Dictionary<string, object>();
            searchInfoTA["AcademicYearID"] = academicYearId;
            searchInfoTA["IsActive"] = true;
            List<TeachingAssignment> listTA = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfoTA).ToList();
            //List<TeachingAssignment> listTA = TeachingAssignmentBusiness.All.Where(o => o.SchoolID == global.SchoolID.Value && o.AcademicYearID == global.AcademicYearID.Value && o.IsActive == true && o.Last2digitNumberSchool == partitionId).ToList();
            List<TeachingAssignment> lsTA = new List<TeachingAssignment>();
            // Danh sach phan cong giang day cua lop se thuc hien cap nhat vao CSDL
            List<TeachingAssignment> lsTAInsertOrUpdate = new List<TeachingAssignment>();
            // Dictionary luu danh sach lop duoc phan cong
            Dictionary<int, List<TeachingAssignment>> dicTa = new Dictionary<int, List<TeachingAssignment>>();
            List<TeachingAssignment> lstDelete = new List<TeachingAssignment>();
            SubjectByClassBO objSBC = null;
            foreach (var item in LegalData)
            {
                #region xu ly du lieu cho HKI
                if (!String.IsNullOrEmpty(item.AssginSemesterI) && item.lstSubjectByClassHKI != null)
                {
                    //duyet danh sach cac mon hoc phan cong cho lop cua giao vien trong file excel
                    for (int i = 0; i < item.lstSubjectByClassHKI.Count; i++)
                    {
                        objSBC = item.lstSubjectByClassHKI[i];
                        //lay ra danh sach cac lop trong db cua giao vien
                        dicTa[objSBC.SubjectID] = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                            && p.TeacherID == item.TeacherID).ToList();

                        //TH1: giao vien chua duoc phan cong giang day
                        if (dicTa[objSBC.SubjectID] == null || dicTa[objSBC.SubjectID].Count <= 0)
                        {
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                TeachingAssignment ta = new TeachingAssignment();
                                ta.AcademicYearID = academicYearId;
                                ta.AssignedDate = DateTime.Now;
                                ta.ClassID = objSBC.listClassID[j];
                                ta.FacultyID = item.FacultyID;
                                ta.IsActive = true;
                                ta.SchoolID = schoolId;
                                ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                                ta.SubjectID = objSBC.SubjectID;
                                ta.TeacherID = item.TeacherID;
                                ta.Last2digitNumberSchool = partitionId;
                                lsTAInsertOrUpdate.Add(ta);
                            }
                        }
                        else //TH2: giao vien duoc phan cong giang day cho mon hoc do
                        {
                            //duyet danh sach lop
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                                TeachingAssignment check_ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                    && p.ClassID == objSBC.listClassID[j]
                                    && p.TeacherID == item.TeacherID
                                    && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (check_ta != null)
                                {
                                    continue;
                                }
                                else
                                {
                                    TeachingAssignment ta = new TeachingAssignment();
                                    ta.AcademicYearID = academicYearId;
                                    ta.AssignedDate = DateTime.Now;
                                    ta.ClassID = objSBC.listClassID[j];
                                    ta.FacultyID = item.FacultyID;
                                    ta.IsActive = true;
                                    ta.SchoolID = schoolId;
                                    ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                                    ta.SubjectID = objSBC.SubjectID;
                                    ta.TeacherID = item.TeacherID;
                                    ta.Last2digitNumberSchool = partitionId;
                                    lsTAInsertOrUpdate.Add(ta);
                                }
                            }
                        }
                    }
                }
                else//TH: giáo viên đã được phân công giang dạy nhưng import từ file excel => bỏ phân công giảng dạy 
                {
                    if (objSBC != null)
                    {
                        //duyet danh sach lop
                        for (int j = 0; j < objSBC.listClassID.Count; j++)
                        {
                            //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                            TeachingAssignment ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                && p.ClassID == objSBC.listClassID[j]
                                && p.TeacherID == item.TeacherID
                                && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            if (ta != null)
                            {
                                TeachingAssignmentBusiness.Delete(ta.TeachingAssignmentID);
                            }
                        }
                    }
                }
                #endregion xu ly du lieu cho HKI

                #region xu ly du lieu cho HKII
                if (!String.IsNullOrEmpty(item.AssignSemesterII) && item.lstSubjectByClassHKII != null)
                {
                    //duyet danh sach cac mon hoc phan cong cho lop cua giao vien trong file excel
                    for (int i = 0; i < item.lstSubjectByClassHKII.Count; i++)
                    {
                        objSBC = item.lstSubjectByClassHKII[i];
                        //lay ra danh sach cac lop trong db cua giao vien
                        dicTa[objSBC.SubjectID] = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && p.TeacherID == item.TeacherID).ToList();

                        //TH1: giao vien chua duoc phan cong giang day
                        if (dicTa[objSBC.SubjectID] == null || dicTa[objSBC.SubjectID].Count <= 0)
                        {
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                TeachingAssignment ta = new TeachingAssignment();
                                ta.AcademicYearID = academicYearId;
                                ta.AssignedDate = DateTime.Now;
                                ta.ClassID = objSBC.listClassID[j];
                                ta.FacultyID = item.FacultyID;
                                ta.IsActive = true;
                                ta.SchoolID = schoolId;
                                ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                                ta.SubjectID = objSBC.SubjectID;
                                ta.TeacherID = item.TeacherID;
                                ta.Last2digitNumberSchool = partitionId;
                                lsTAInsertOrUpdate.Add(ta);
                            }
                        }
                        else //TH2: giao vien duoc phan cong giang day cho mon hoc do
                        {
                            //duyet danh sach lop
                            for (int j = 0; j < objSBC.listClassID.Count; j++)
                            {
                                //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                                TeachingAssignment check_ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                    && p.ClassID == objSBC.listClassID[j]
                                    && p.TeacherID == item.TeacherID
                                    && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (check_ta != null)
                                {
                                    //Xét trường hợp AssignHKI null. Kiềm tra tồn tại phân công giảng dạy đang xét ở HKI thì xóa đi                                  
                                    if (item.AssginSemesterI == string.Empty)
                                    {
                                        TeachingAssignment tahkI = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                            && p.ClassID == objSBC.listClassID[j]
                                            && p.TeacherID == item.TeacherID
                                            && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                        if (tahkI != null)
                                        {
                                            lstDelete.Add(tahkI);
                                        }
                                    }
                                    continue;
                                }
                                else
                                {
                                    TeachingAssignment ta = new TeachingAssignment();
                                    ta.AcademicYearID = academicYearId;
                                    ta.AssignedDate = DateTime.Now;
                                    ta.ClassID = objSBC.listClassID[j];
                                    ta.FacultyID = item.FacultyID;
                                    ta.IsActive = true;
                                    ta.SchoolID = schoolId;
                                    ta.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                                    ta.SubjectID = objSBC.SubjectID;
                                    ta.TeacherID = item.TeacherID;
                                    ta.Last2digitNumberSchool = partitionId;
                                    lsTAInsertOrUpdate.Add(ta);
                                }
                            }
                        }
                    }


                }
                else//TH: giáo viên đã được phân công giang dạy nhưng import từ file excel => bỏ phân công giảng dạy 
                {

                    if (objSBC != null)
                    {
                        //duyet danh sach lop
                        for (int j = 0; j < objSBC.listClassID.Count; j++)
                        {
                            //kiem tra da ton tai phan cong trong db chua. Neu chua thuc hien insert, ton tai thi bo qua 
                            TeachingAssignment ta = listTA.Where(p => p.SubjectID == objSBC.SubjectID
                                && p.ClassID == objSBC.listClassID[j]
                                && p.TeacherID == item.TeacherID
                                && p.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            if (ta != null)
                            {
                                lstDelete.Add(ta);
                            }
                        }
                    }
                }
                #endregion xu ly du lieu cho HKII
            }
            if (lstDelete.Count > 0 && lstDelete != null)
            {
                TeachingAssignmentBusiness.DeleteAll(lstDelete);
            }

            // Luu thong tin phan cong giang day
            TeachingAssignmentBusiness.InsertOrUpdateListTeachingAssignment(schoolId, academicYearId, lsTAInsertOrUpdate);

            TeachingAssignmentBusiness.Save();
            List<ImportTeachingAssignmentViewModel> lsTemp = (List<ImportTeachingAssignmentViewModel>)Session["ImportData"];
            int total = ((lsTemp == null) ? 0 : lsTemp.Count);
            int retVal = (LegalData == null) ? 0 : LegalData.Where(c => String.IsNullOrEmpty(c.ErrorDescription)).Count();
            string retMsg = String.Format("{0} {1}/{2} giáo viên", Res.Get("Import phân công giảng dạy thành công cho "), retVal, total);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
        }
        public List<ImportTeachingAssignmentViewModel> getDataFromImportFile(IVTWorkbook oBook)
        {
            #region Khai bao + lay du lieu truy van
            List<ImportTeachingAssignmentViewModel> lsICSATVM = new List<ImportTeachingAssignmentViewModel>();
            int schoolId = _globalInfo.SchoolID.Value;
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int appliedLevel = _globalInfo.AppliedLevel.Value;
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            int count = 0;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"AcademicYearID",academicYearId}
                }).ToList();
            ClassProfile cp = new ClassProfile();
            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"IsActive",true}
                }).ToList();
            SubjectCat sc = new SubjectCat();

            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.All.Where(o => o.SchoolID == schoolId && o.IsActive == true).ToList();
            SchoolFaculty sf = new SchoolFaculty();

            List<SchoolSubjectBO> lstSchoolSubject = SchoolSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
                {
                    {"AppliedLevel",appliedLevel},
                    {"AcademicYearID",academicYearId},
                }).Select(o => new SchoolSubjectBO
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName,
                    EducationLevelID = o.EducationLevelID,
                    Abbreviation = o.SubjectCat.Abbreviation
                }).ToList();

            List<ClassSubjectBO> lstClassSubjectHKI = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
            {
                {"AppliedLevel",appliedLevel},
                {"AcademicYearID",academicYearId},
                {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_FIRST},
            }).Select(o => new ClassSubjectBO
            {
                ClassSubjectID = o.ClassSubjectID,
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
            }).ToList();

            List<ClassSubjectBO> lstClassSubjectHKII = ClassSubjectBusiness.SearchBySchool(schoolId, new Dictionary<string, object>()
            {
                {"AppliedLevel",appliedLevel},
                {"AcademicYearID",academicYearId},
                {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_SECOND},
            }).Select(o => new ClassSubjectBO
            {
                ClassSubjectID = o.ClassSubjectID,
                ClassID = o.ClassID,
                SubjectID = o.SubjectID
            }).ToList();

            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(schoolId, new Dictionary<string, object>()
            {
                //{"CurrentEmployeeStatus", SystemParamsInFile.EMPLOYMENT_STATUS_WORKING}
            }).ToList();

            List<Employee> listEmployeeCheck = new List<Employee>();
            List<string> ErrClassI = null;
            List<string> ErrSubjectI = null;
            List<string> ErrClassII = null;
            List<string> ErrSubjectII = null;

            List<ErrSubjectClass> lstErrSubjectByClassI = null;
            List<ErrSubjectClass> lstErrSubjectByClassII = null;
            List<SubjectByClassBO> lstTemp_HKI = null;
            List<SubjectByClassBO> lstTemp_HKII = null;
            #endregion Khai bao + lay du lieu truy van

            while (true)
            {
                bool isLegalBot = true;
                string TeacherCode, TeacherName, FacultyName, AssignSemesterI, AssignSemesterII;

                int index = count + TeachingAssignmentConstants.BEGIN_ROW_PCGD;
                TeacherCode = sheet.GetCellValue(index, VTVector.dic['B']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['B']).ToString();
                TeacherCode = String.IsNullOrEmpty(TeacherCode) ? "" : TeacherCode.Trim();
                TeacherName = sheet.GetCellValue(index, VTVector.dic['C']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['C']).ToString();
                TeacherName = String.IsNullOrEmpty(TeacherName) ? "" : TeacherName.Trim();
                FacultyName = sheet.GetCellValue(index, VTVector.dic['D']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['D']).ToString();
                FacultyName = String.IsNullOrEmpty(FacultyName) ? "" : FacultyName.Trim();

                AssignSemesterI = sheet.GetCellValue(index, VTVector.dic['E']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['E']).ToString();
                AssignSemesterI = String.IsNullOrEmpty(AssignSemesterI) ? "" : AssignSemesterI.Trim();

                AssignSemesterII = sheet.GetCellValue(index, VTVector.dic['F']) == null ? "" : sheet.GetCellValue(index, VTVector.dic['F']).ToString();
                AssignSemesterII = String.IsNullOrEmpty(AssignSemesterII) ? "" : AssignSemesterII.Trim();

                if (String.IsNullOrEmpty(TeacherCode)
                    && String.IsNullOrEmpty(TeacherName)
                    && String.IsNullOrEmpty(FacultyName)
                    && String.IsNullOrEmpty(AssignSemesterI)
                    && String.IsNullOrEmpty(AssignSemesterII)
                    )
                {
                    break;
                }

                ImportTeachingAssignmentViewModel ICSATVM = new ImportTeachingAssignmentViewModel();

                //----------------bat dau validate-----------------------------
                //to bo mon
                sf = lstSchoolFaculty.Where(p => p.FacultyName.ToUpper().Contains(FacultyName.ToUpper())).FirstOrDefault();
                if (sf != null)
                {
                    ICSATVM.FacultyID = sf.SchoolFacultyID;
                }
                ICSATVM.FacultyName = FacultyName;

                //giao vien
                if (String.IsNullOrEmpty(TeacherCode))//&& (TeacherName == null || TeacherName.Trim().Length == 0))
                {
                    // Neu khong co ma va ten giao vien thi bao loi
                    isLegalBot = false;
                    ICSATVM.TeacherCode = TeacherCode;
                    ICSATVM.TeacherName = TeacherName;
                    ICSATVM.ErrorDescription += " - Chưa nhập mã giáo viên <br/>";
                }
                else
                {
                    Employee e = null;
                    // Neu co ma thi tim theo ma, khong co thi tim theo ten
                    if (!String.IsNullOrEmpty(TeacherCode))
                    {
                        listEmployeeCheck = lstEmployee.Where(o => o.EmployeeCode.ToLower() == TeacherCode.Trim().ToLower()).ToList();
                        // Neu trung ma ma co nhap ten thi tim theo ten
                        // Neu duy nhat thi co the thuc hien tiep
                        // Con trong ca ma va ca ten thi bo tay, bao loi de truong sua thong tin giao vien
                        if (listEmployeeCheck.Count > 1)
                        {
                            e = listEmployeeCheck.Where(o => o.FullName.ToLower() == TeacherName.Trim().ToLower()).FirstOrDefault();
                        }
                        else
                        {
                            e = listEmployeeCheck.FirstOrDefault();
                        }
                    }
                    else
                    {
                        e = lstEmployee.Where(o => o.FullName.ToLower() == TeacherName.Trim().ToLower()).FirstOrDefault();
                    }

                    if (e != null)
                    {
                        if (e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                        {
                            ICSATVM.TeacherID = e.EmployeeID;
                            ICSATVM.TeacherCode = TeacherCode;
                            ICSATVM.TeacherName = TeacherName;
                        }
                        else
                        {
                            ICSATVM.TeacherCode = TeacherCode;
                            ICSATVM.TeacherName = TeacherName;
                            isLegalBot = false;
                            if (TeacherCode != null && TeacherCode.Trim().Length > 0)
                            {
                                ICSATVM.ErrorDescription += " - Giáo viên đang ở trạng thái khác đang làm việc <br/>";
                            }
                        }

                    }
                    else
                    {
                        ICSATVM.TeacherCode = TeacherCode;
                        ICSATVM.TeacherName = TeacherName;
                        isLegalBot = false;
                        if (TeacherCode != null && TeacherCode.Trim().Length > 0)
                        {
                            ICSATVM.ErrorDescription += " - Mã giáo viên chưa được khai báo <br/>";
                        }
                    }
                }

                //Phan cong giang day
                if (String.IsNullOrEmpty(AssignSemesterI) && String.IsNullOrEmpty(AssignSemesterII))
                {
                    // Neu khong phan cong giang day HKI va HKII
                    isLegalBot = false;
                    ICSATVM.ErrorDescription += " - Giáo viên chưa được phân công giảng dạy <br/>";
                }
                else
                {
                    ////Kiem tra đúng quy tắc quy định trong file excel
                    if (CheckRule(AssignSemesterI) == false || CheckRule(AssignSemesterII) == false)
                    {
                        isLegalBot = false;
                        ICSATVM.AssginSemesterI = AssignSemesterI;
                        ICSATVM.AssignSemesterII = AssignSemesterII;
                        ICSATVM.ErrorDescription += "- Dữ liệu phân công giảng dạy không hợp lệ <br/>";
                    }
                    else
                    {
                        //kiem tra lop hoc chua duoc khai bao trong csdl theo cau truc "Lop hoc y1, y2,..,yn chua khai bao" (o 2 ky)
                        //kiem tra mon hoc chua duoc khai bao trong csdl theo cau truc "Mon hoc x1, x2,...xn chua duoc khai bao" (o 2 ky)
                        ErrClassI = new List<string>();
                        ErrClassII = new List<string>();
                        ErrSubjectI = new List<string>();
                        ErrSubjectII = new List<string>();
                        string error = string.Empty;
                        CheckErrExsistClassAndSubject(AssignSemesterI, ErrClassI, ErrSubjectI, lstClassProfile, lstSchoolSubject);
                        CheckErrExsistClassAndSubject(AssignSemesterII, ErrClassII, ErrSubjectII, lstClassProfile, lstSchoolSubject);
                        if ((ErrClassI != null && ErrClassI.Count > 0) || (ErrClassII != null && ErrClassII.Count > 0))
                        {
                            isLegalBot = false;
                            ICSATVM.AssginSemesterI = AssignSemesterI;
                            ICSATVM.AssignSemesterII = AssignSemesterII;
                            error = TransString(ErrClassI, ErrClassII);
                            ICSATVM.ErrorDescription += " - Lớp học " + error + " chưa khai báo <br/>";
                        }
                        if ((ErrSubjectI != null && ErrSubjectI.Count > 0) || (ErrSubjectII != null && ErrSubjectII.Count > 0))
                        {
                            isLegalBot = false;
                            ICSATVM.AssginSemesterI = AssignSemesterI;
                            ICSATVM.AssignSemesterII = AssignSemesterII;
                            error = TransString(ErrSubjectI, ErrSubjectII);
                            ICSATVM.ErrorDescription += " - Môn học " + error + " chưa được khai báo <br/>";
                        }
                        else if (ErrSubjectI.Count == 0 && ErrSubjectII.Count == 0 && ErrClassI.Count == 0 && ErrClassII.Count == 0)
                        {
                            //Môn học chưa được khai báo cho lớp ở học kỳ I. Cau truc "Mon hoc X chua khai bao cho lop Y1, Y2,.., Yn o hoc ky I"
                            lstTemp_HKI = new List<SubjectByClassBO>();
                            lstTemp_HKII = new List<SubjectByClassBO>();
                            lstErrSubjectByClassI = new List<ErrSubjectClass>();
                            lstErrSubjectByClassII = new List<ErrSubjectClass>();
                            CheckErrSubjectByClass(AssignSemesterI, lstErrSubjectByClassI, lstTemp_HKI, lstClassSubjectHKI, lstClassProfile, lstSubjectCat);
                            if (lstErrSubjectByClassI != null && lstErrSubjectByClassI.Count > 0)
                            {
                                isLegalBot = false;
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                for (int i = 0; i < lstErrSubjectByClassI.Count; i++)
                                {
                                    ICSATVM.ErrorDescription += " - Môn học " + lstErrSubjectByClassI[i].ErrSubjectName + " chưa được khai báo cho lớp "
                                                            + lstErrSubjectByClassI[i].ErrClass + " ở học kỳ I <br/>";
                                }
                            }

                            //Môn học chưa được khai báo cho lớp ở học kỳ II. Cau truc "Mon hoc X chua khai bao cho lop Y1, Y2,.., Yn o hoc ky II"
                            CheckErrSubjectByClass(AssignSemesterII, lstErrSubjectByClassII, lstTemp_HKII, lstClassSubjectHKII, lstClassProfile, lstSubjectCat);
                            if (lstErrSubjectByClassII != null && lstErrSubjectByClassII.Count > 0)
                            {
                                isLegalBot = false;
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                for (int i = 0; i < lstErrSubjectByClassII.Count; i++)
                                {
                                    ICSATVM.ErrorDescription += " - Môn học " + lstErrSubjectByClassII[i].ErrSubjectName + " chưa được khai báo cho lớp "
                                                            + lstErrSubjectByClassII[i].ErrClass + " ở học kỳ II <br/>";
                                }
                            }
                            else
                            {
                                ICSATVM.AssginSemesterI = AssignSemesterI;
                                ICSATVM.AssignSemesterII = AssignSemesterII;
                                ICSATVM.lstSubjectByClassHKI = lstTemp_HKI;
                                ICSATVM.lstSubjectByClassHKII = lstTemp_HKII;
                            }
                        }
                    } //cho not kiem tra quy tac
                }

                //hoc ky
                if (AssignSemesterI != null && AssignSemesterII != null)
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                }
                else if (AssignSemesterI != null)
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    ICSATVM.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                }
                ICSATVM.isLegal = isLegalBot;
                lsICSATVM.Add(ICSATVM);
                //-------------------------------------------------------------
                count++;
            }
            return lsICSATVM;
        }
        public bool checkExcelTemplate(IVTWorkbook oBook)
        {
            try
            {
                IVTWorksheet sheet = oBook.GetSheet(1);
                int indexheader = TeachingAssignmentConstants.BEGIN_ROW_PCGD - 1;
                string TeacherCode = sheet.GetCellValue(indexheader, VTVector.dic['B']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['B']).ToString();
                string FullName = sheet.GetCellValue(indexheader, VTVector.dic['C']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['C']).ToString();
                string FaultyName = sheet.GetCellValue(indexheader, VTVector.dic['D']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['D']).ToString();
                string AssginHKI = sheet.GetCellValue(indexheader, VTVector.dic['E']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['E']).ToString();
                string AssginHKII = sheet.GetCellValue(indexheader, VTVector.dic['F']) == null ? "" : sheet.GetCellValue(indexheader, VTVector.dic['F']).ToString();

                bool isTeacherCode = !TeacherCode.ToUpper().Contains(TeachingAssignmentConstants.MA_GIAO_VIEN.ToUpper());
                bool isFullName = !FullName.ToUpper().Contains(TeachingAssignmentConstants.HO_TEN_GIAO_VIEN.ToUpper());
                bool isFaultyName = !FaultyName.ToUpper().Contains(TeachingAssignmentConstants.TO_BO_MON.ToUpper());
                bool isAssginHKI = !AssginHKI.ToUpper().Contains(TeachingAssignmentConstants.PHAN_CONG_KYI.ToUpper());
                bool isAssginHKII = !AssginHKII.ToUpper().Contains(TeachingAssignmentConstants.PHAN_CONG_KYII.ToUpper());

                if (isTeacherCode || isFullName || isFaultyName || isAssginHKI || isAssginHKII)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "checkExcelTemplate", "null", ex);

                return false;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetTeachingPeriod(int TeacherID)
        {
            GlobalInfo gi = new GlobalInfo();
            Employee teacher = EmployeeBusiness.Find(TeacherID);
            string message = Res.Get("TeachingAssignment_Message_TeachingLesson");
            int spw1 = countTeachingPeriod(TeacherID);

            return Json(new JsonMessage(string.Format(message, spw1), spw1.ToString()));
        }

        //tính số lớp chủ nhiệm của giáo viên
        public int CountClassHeadTeacher(int TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["HeadTeacherID"] = TeacherID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["AppliedLevel"] = global.AppliedLevel.Value;
            IQueryable<ClassProfile> cpr = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return cpr.Count();
        }

        /// <summary>
        /// Tính số tiet duoc giam do lam cac công việc kiêm nhiệm
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        public int CountDecreaseLessonForConcurrentWork(int TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["TeacherID"] = TeacherID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            SearchInfo["SchoolID"] = global.SchoolID.Value;
            SearchInfo["IsActive"] = true;
            List<ConcurrentWorkAssignment> cwa = ConcurrentWorkAssignmentBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).ToList();
            int count = 0;
            cwa.ForEach((p) => { count += p.ConcurrentWorkType.SectionPerWeek; });
            return count;
        }
        //Tính định mức tiết dạy dựa theo giáo viên
        public int countTeachingPeriod(int TeacherID)
        {
            GlobalInfo global = new GlobalInfo();
            //Số tiết định mức chuẩn
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(global.SchoolID.Value);
            int SchoolTypeID = schoolProfile.SchoolTypeID.Value;
            //Lấy lên cấp học
            int appliedLevel = global.AppliedLevel.Value;

            SystemParamsInFile.TeachingAssignment teachingAssign = new SystemParamsInFile.TeachingAssignment(appliedLevel, SchoolTypeID);
            int countStandard = teachingAssign.DINH_MUC_TIET_DAY;
            int decreaseLessonHeadTeacher = CountClassHeadTeacher(TeacherID) * teachingAssign.GVCN; // Tinh so stiet duoc giam do lam giao vien chu nhiem
            int decreaseLessonConcurrent = CountDecreaseLessonForConcurrentWork(TeacherID); // Tinh so tiet duoc giam do thuc hien cong viec kiem nhiem
            int countTeachingPeriod = (countStandard - decreaseLessonHeadTeacher - decreaseLessonConcurrent) > 0 ? (countStandard - decreaseLessonHeadTeacher - decreaseLessonConcurrent) : 0;

            return countTeachingPeriod;
        }
        private string GetAssignSemesterByTeacher(List<TeachingAssignmentBO> lstTempSemester)
        {
            StringBuilder assignSemeter = new StringBuilder();
            StringBuilder retVal = new StringBuilder();
            int countSe = lstTempSemester.Count;

            if (lstTempSemester == null || lstTempSemester.Count == 0)
            {
                return "";
            }
            //tach thanh 2 nhom, 1 nhom mon hoc va mot nhom lop hoc. Thuc hien vong for de ghep chuoi de duoc ket qua theo cau truc
            // Toan (7/1, 7/2) + VL (7/1, 7/3)
            List<SubjectModel> lstSubjectId = lstTempSemester.Select(c => new SubjectModel
            {
                SubjectID = c.SubjectID.Value,
                SubjcetAbbreviation = c.SubjcetAbbreviation,
                SubjectName = c.SubjectName,
                OrderInSubject = c.OrderInSubject
            }).Distinct().ToList();
            lstSubjectId = lstSubjectId.OrderBy(c => c.OrderInSubject).ThenBy(c => c.SubjectName).ToList();
            List<int> listSubjectID = lstSubjectId.Select(p => p.SubjectID).Distinct().ToList();

            List<string> lstClassName;
            SubjectModel objSubjectModel;
            for (int i = 0; i < listSubjectID.Count; i++)
            {
                objSubjectModel = lstSubjectId.Where(p => p.SubjectID == listSubjectID[i]).FirstOrDefault();
                lstClassName = lstTempSemester.Where(c => c.SubjectID == objSubjectModel.SubjectID)
                                                        .OrderBy(c => c.EducationLevelID)
                                                        .ThenBy(c => c.ClassOrderNumber)
                                                        .Select(c => c.ClassName).ToList();
                if (lstClassName == null || lstClassName.Count == 0)
                {
                    continue;
                }
                if (assignSemeter.Equals(null) || String.IsNullOrEmpty(assignSemeter.ToString()))
                {
                    assignSemeter.Append(objSubjectModel.SubjectName).Append(": ");
                }
                else
                {
                    assignSemeter.Append("\r\n").Append(objSubjectModel.SubjectName).Append(": ");
                }

                for (int j = 0; j < lstClassName.Count; j++)
                {
                    if (j == lstClassName.Count - 1)
                    {
                        assignSemeter.Append(lstClassName[j]);
                    }
                    else
                    {
                        assignSemeter.Append(lstClassName[j]).Append(",");
                    }
                }
            }
            return assignSemeter.ToString();
        }

        private string TransString(List<string> lstErr1, List<string> lstErr2)
        {
            string error = string.Empty;
            List<string> lstTemp = new List<string>();
            if (lstErr1.Count == 0)
            {
                lstTemp = lstErr2;
            }
            else if (lstErr2.Count == 0)
            {
                lstTemp = lstErr1;
            }
            else
            {
                for (int i = 0; i < lstErr2.Count; i++)
                {
                    lstErr1.Add(lstErr2[i]);
                }
                lstTemp = lstErr1;
            }
            lstTemp = lstTemp.Select(s => s.Trim()).Distinct().ToList();
            for (int j = 0; j < lstTemp.Count; j++)
            {
                error += lstTemp[j] + ", ";
            }
            error = error != string.Empty ? error.Remove(error.Length - 2) : string.Empty;
            return error;
        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// Kiem tra mon hoc co duoc khai bao cho lop trong hoc ki I or hoc ky II
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <param name="lstErrSubjectByClass"></param>
        /// <param name="lstClassSubjectHK"></param>
        /// <param name="lstClassProfile"></param>
        /// <param name="lstSubjectCat"></param>
        private void CheckErrSubjectByClass(string AssignSemester, List<ErrSubjectClass> lstErrSubjectByClass, List<SubjectByClassBO> lstTemp_HK, List<ClassSubjectBO> lstClassSubjectHK,
                                List<ClassProfile> lstClassProfile, List<SubjectCat> lstSubjectCat)
        {
            //VL(6A1, 6A2, 6A3) + TC2(7A1, 7A2)
            //cat chuoi "+" lay ra tung mon hoc va lop rieng biet
            //Sua loi exception khi xu ly chuoi.
            if (String.IsNullOrEmpty(AssignSemester) || string.IsNullOrWhiteSpace(AssignSemester))
            {
                return;
            }
            List<string> lstAS = AssignSemester.Split('+').Select(s => s.Trim()).ToList(); //VL(6A1, 6A2, 6A3)
            if (lstAS == null || lstAS.Count == 0)
            {
                return;
            }
            lstAS = lstAS.Distinct().ToList();
            List<string> lstobjAS = null; //chua mang gia tri mon hoc va cac lop sau khi cat chuoi theo "("
            string subject = string.Empty;
            List<string> lstclass = null;
            ClassProfile cp = new ClassProfile();
            SubjectCat sc = new SubjectCat();
            ClassSubjectBO scBO = new ClassSubjectBO();
            ErrSubjectClass errSC; //struct chua cac lop chua dc khia bao mon hoc 
            SubjectByClassBO objSubjectByClassBO = null; // lop dc khia bao mon hoc
            string temp;
            for (int i = 0; i < lstAS.Count; i++)
            {
                errSC = new ErrSubjectClass();
                objSubjectByClassBO = new SubjectByClassBO();
                objSubjectByClassBO.listClassID = new List<int>();

                lstobjAS = lstAS[i].Split('(').ToList();
                if (lstobjAS == null)
                {
                    continue;
                }
                subject = lstobjAS[0].Trim();
                temp = lstobjAS[1].Replace(")", " ").Trim();
                if (String.IsNullOrEmpty(temp))
                {
                    continue;
                }
                lstclass = temp.Split(',').Select(s => s.Trim()).ToList();
                lstclass = lstclass.Distinct().ToList();
                if (lstclass == null)
                {
                    continue;
                }

                //lay SubjectID
                sc = lstSubjectCat.Where(p => p.Abbreviation.Equals(subject)).FirstOrDefault();
                errSC.ErrSubjectName = subject;
                errSC.ErrClass = string.Empty;
                string className;
                for (int j = 0; j < lstclass.Count; j++)
                {
                    //lay ClassID
                    className = lstclass[j].Trim();
                    cp = lstClassProfile.Where(p => p.DisplayName != null && p.DisplayName.Equals(className)).FirstOrDefault();
                    if (cp == null)
                    {
                        errSC.ErrClass += lstclass[j] + ", ";
                        continue;
                    }
                    //tim kiem mon hoc co duoc khai bao cho lop
                    scBO = lstClassSubjectHK.Where(p => p.ClassID == cp.ClassProfileID && p.SubjectID == sc.SubjectCatID).FirstOrDefault();
                    if (scBO == null)
                    {
                        errSC.ErrClass += lstclass[j] + ", ";
                    }
                    else
                    {
                        objSubjectByClassBO.listClassID.Add(cp.ClassProfileID);
                    }
                }
                objSubjectByClassBO.SubjectID = sc.SubjectCatID;
                lstTemp_HK.Add(objSubjectByClassBO);
                errSC.ErrClass = errSC.ErrClass.Length != 0 ? errSC.ErrClass.Remove(errSC.ErrClass.Length - 2).Trim() : string.Empty;
                if (errSC.ErrClass != string.Empty)
                {
                    lstErrSubjectByClass.Add(errSC);
                }
            }
        }

        /// <summary>
        /// Kiem tra mon hoc va lop co ton tai trong csdl
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <param name="ErrClass"></param>
        /// <param name="ErrSubject"></param>
        /// <param name="lstClassProfile"></param>
        /// <param name="lstSchoolSubject"></param>
        private void CheckErrExsistClassAndSubject(string AssignSemester, List<string> ErrClass, List<string> ErrSubject,
                                        List<ClassProfile> lstClassProfile, List<SchoolSubjectBO> lstSchoolSubject)
        {
            //VL(6A1, 6A2, 6A3) + TC2(7A1, 7A2)
            //cat chuoi "+" lay ra tung mon hoc va lop rieng biet
            if (String.IsNullOrEmpty(AssignSemester) || string.IsNullOrWhiteSpace(AssignSemester))
            {
                return;
            }
            List<string> lstAS = AssignSemester.Split('+').ToList(); //VL(6A1, 6A2, 6A3)
            List<string> lstobjAS = null; //chua mang gia tri mon hoc va cac lop sau khi cat chuoi theo "("
            string subject = string.Empty;
            List<string> lstclass = null;
            int countSubject = 0;
            int countClass = 0;

            for (int i = 0; i < lstAS.Count; i++)
            {
                lstobjAS = lstAS[i].Split('(').ToList();
                subject = lstobjAS[0].Trim();
                lstclass = lstobjAS[1].Replace(")", " ").Trim().Split(',').Select(s => s.Trim()).ToList();
                lstclass = lstclass.Distinct().ToList();
                //kiem tra mon hoc
                countSubject = lstSchoolSubject.Where(p => p.Abbreviation.Equals(subject)).Count();
                if (countSubject == 0)
                {
                    ErrSubject.Add(subject);
                }

                //kiem tra lop hoc
                for (int j = 0; j < lstclass.Count; j++)
                {
                    countClass = lstClassProfile.Where(p => p.DisplayName != null && p.DisplayName.Equals(lstclass[j].Trim())).Count();
                    if (countClass == 0)
                    {
                        ErrClass.Add(lstclass[j]);
                    }
                }
            }
        }

        /// <summary>
        /// Kiểm tra khai báo phân công giảng day trong file excel co dung quy tac quy dinh
        /// </summary>
        /// <param name="AssignSemester"></param>
        /// <returns></returns>
        private bool CheckRule(string AssignSemester)
        {
            AssignSemester = Utils.Utils.StripVNSign(AssignSemester);
            bool check = true;
            Regex rgx = new Regex(@"[a-zA-Z0-9\s]+\(+[a-zA-Z0-9\s,./_-]+\)");
            List<string> lstAS = AssignSemester.Split('+').Select(s => s.Trim()).ToList();
            int countRegex = 0;
            if (AssignSemester != string.Empty)
            {
                if (lstAS.Count > 1)
                {
                    if (!AssignSemester.Contains("+"))
                        check = false;
                }
                else
                {
                    countRegex = rgx.Matches(lstAS[0]).Count;
                    if (countRegex > 1)
                        check = false;
                }
                foreach (string AS in lstAS)
                {
                    if (!rgx.IsMatch(AS))
                        check = false;
                }
            }
            return check;
        }
    }
}





