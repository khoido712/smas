﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Areas.ExamInputMarkTransferArea;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea.Models
{
    public class FormViewModel
    {
        public string SelectedID { get; set; }
        public int Semester { get; set; }
        public string MarkColumn { get; set; }
        public int TransferType { get; set; }
        public int MarkType { get; set; }
        public string MarkTypeTitle {
            get
            {
                if (MarkType == ExamInputMarkTransferConstants.MARK_TYPE_15P)
                {
                    return "P";
                }
                else if (MarkType == ExamInputMarkTransferConstants.MARK_TYPE_V)
                {
                    return "V";
                }
                else
                {
                    return "HK";
                }
            }
        }
        public int MarkTypeID { get; set; }
        public long examinationsID { get; set; }
        public long examGroupID { get; set; }
        public int subjectID { get; set; }
        public int MarkColumn2ID { get; set; }
        public int SelectVNEN { get; set; }
    }
}