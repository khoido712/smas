﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkTransferConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkTransferConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamInputMarkTransferConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        public long? SubjectID { get; set; }

        public int? SelectVNEN { get; set; }
    }
}