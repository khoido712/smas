﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea.Models
{
    public class ListViewModel
    {

        public long PupilID { get; set; }

        [ResourceDisplayName("ExamInputMarkTransfer_Label_PupilCode")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_PupilName")]
        public string PupilName { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ClassName")]
        public string ClassName { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExamRoomCode")]
        public string ExamRoomCode { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExamineeNumber")]
        public string ExamineeNumber { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ExamMark")]
        public object ExamMark { get; set; }
        [ResourceDisplayName("ExamInputMarkTransfer_Label_ActualMark")]
        public object ActualMark { get; set; }

        public long? ExamInputMarkID { get; set; }
        public long ExamPupilID { get; set; }
        public bool? IsVNENClass { get; set; }
        public int ClassID { get; set; }
    }
}