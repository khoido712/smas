﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ExamInputMarkTransferArea;
using SMAS.Web.Areas.ExamInputMarkTransferArea.Models;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using System.Text.RegularExpressions;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea.Controllers
{
    [SkipCheckRole]
    public class ExamInputMarkTransferController : BaseController
    {

        #region properties
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamSubjectBO> listExamSubject;
        #endregion

        #region Constructor
        public ExamInputMarkTransferController(IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness, IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness,
            IExamSubjectBusiness ExamSubjectBusiness, IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness,
            IExamInputMarkBusiness ExamInputMarkBusiness, IExamPupilBusiness ExamPupilBusiness, ISemeterDeclarationBusiness SemeterDeclarationBusiness,
            ISubjectCatBusiness SubjectCatBusiness, IPupilOfClassBusiness PupilOfClassBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IMarkRecordBusiness MarkRecordBusiness, IAcademicYearBusiness AcademicYearBusiness, IMarkTypeBusiness MarkTypeBusiness, ISummedEvaluationBusiness SummedEvaluationBusiness,
            IJudgeRecordBusiness JudgeRecordBusiness, ISchoolSubjectBusiness SchoolSubjectBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness, IRatedCommentPupilBusiness RatedCommentPupilBusiness,
            ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness, ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness,
            ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness)
        {
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ExamInputMarkAssignedBusiness = ExamInputMarkAssignedBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.SemeterDeclarationBusiness = SemeterDeclarationBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.SummedEvaluationBusiness = SummedEvaluationBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.RatedCommentPupilBusiness = RatedCommentPupilBusiness;
            this.RatedCommentPupilHistoryBusiness = RatedCommentPupilHistoryBusiness;
            this.TeacherNoteBookMonthBusiness = TeacherNoteBookMonthBusiness;
            this.TeacherNoteBookSemesterBusiness = TeacherNoteBookSemesterBusiness;
            this.LockRatedCommentPupilBusiness = LockRatedCommentPupilBusiness;
        }
        #endregion


        #region Action
        //
        // GET: /ExamInputMarkTransferArea/ExamInputMarkTransfer/
        public ActionResult Index()
        {

            SetViewData();

            SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            ViewData[ExamInputMarkTransferConstants.IS_NEW_SCHOOL_MODEL] = objSchool.IsNewSchoolModel.HasValue && objSchool.IsNewSchoolModel.Value == true ? true : false;

            List<Object> listMarkColumn2 = new List<object>();
            //Cap 1 && VNEN
            listMarkColumn2.Add(new { key = 1, value = "KTĐK GK" });
            listMarkColumn2.Add(new { key = 2, value = "KTĐK CK" });
            ViewData[ExamInputMarkTransferConstants.CBO_MARK_COLUMN2] = new SelectList(listMarkColumn2, "key", "value", 2);

            //Lay ky thi mac dinh
            long? defaultExamID = null;
            if (listExaminations.Count > 0)
            {
                defaultExamID = listExaminations.First().ExaminationsID;
            }

            //Lay nhom thi mac dinh
            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0)
            {
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
            }

            //Lay mon thi mac dinh
            int? defaultSubjectID = null;
            if (listExamSubject.Count > 0)
            {
                defaultSubjectID = listExamSubject.First().SubjectID;
            }

            if (defaultExamID == null || defaultExamGroupID == null || defaultSubjectID == null)
            {
                ViewData[ExamInputMarkTransferConstants.LIST_RESULT] = new List<ListViewModel>();
                CheckCommandPermision(this.ExaminationsBusiness.Find(defaultExamID));
                return View();
            }

            List<ListViewModel> listResult = this._Search(defaultExamID.Value, defaultExamGroupID.Value, defaultSubjectID.Value).ToList();
            ViewData[ExamInputMarkTransferConstants.LIST_RESULT] = listResult;

            CheckCommandPermision(this.ExaminationsBusiness.Find(defaultExamID));

            return View();
        }

        // GET: /ExamInputMarkTransferArea/Search
        public PartialViewResult Search(SearchViewModel form)
        {
            Utils.Utils.TrimObject(form);

            long? examinationsID = form.ExaminationsID;
            long? examGroupID = form.ExamGroupID;
            long? subjectID = form.SubjectID;
            bool? checkVNEN = (form.SelectVNEN.HasValue && form.SelectVNEN == 1) ? true : false;
            ViewData[ExamInputMarkTransferConstants.VALUE_CHECK_VNEN] = form.SelectVNEN.Value;
            SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            ViewData[ExamInputMarkTransferConstants.IS_NEW_SCHOOL_MODEL] = objSchool.IsNewSchoolModel.HasValue && objSchool.IsNewSchoolModel.Value == true ? true : false;
            //Lay du lieu cho phan form
            SetViewDataForTransferForm(examinationsID, subjectID);

            if (examinationsID == null || examGroupID ==     null || subjectID == null)
            {
                CheckCommandPermision(ExaminationsBusiness.Find(examinationsID));
                return PartialView("_Content", new List<ListViewModel>());
            }
            List<ListViewModel> result = _Search(examinationsID.Value, examGroupID.Value, subjectID.Value).ToList();

            if ((checkVNEN.HasValue && checkVNEN.Value == true) && subjectID.HasValue
                && (objSchool.IsNewSchoolModel.HasValue && objSchool.IsNewSchoolModel.Value == true))
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() {
                    {"SubjectID", (int)subjectID.Value},
                    {"SchoolID", _globalInfo.SchoolID.Value},
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                };
                List<ClassSubject> lstClassSubject = ClassSubjectBusiness.Search(dic).Where(x => x.IsSubjectVNEN.HasValue && x.IsSubjectVNEN.Value == true).ToList();
                List<int> lstClassID = lstClassSubject.Select(x => x.ClassID).ToList();
                if (lstClassID.Count() > 0)
                {
                    result = result.Where(x => x.IsVNENClass.HasValue && x.IsVNENClass == checkVNEN && lstClassID.Contains(x.ClassID)).ToList();
                }            
            }       

            CheckCommandPermision(ExaminationsBusiness.Find(examinationsID));
            return PartialView("_Content", result);
        }

        /// <summary>
        /// Transfer
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        [ActionAudit]
        public JsonResult Transfer(FormViewModel form)
        {
            if (GetMenupermission("ExamInputMarkTransfer", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            SchoolProfile objSchool = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAy) ? true : false;
            bool isNewModel = (objSchool.IsNewSchoolModel.HasValue && objSchool.IsNewSchoolModel.Value == true) ? true : false;

            //Lay danh sach mon thi cua ky thi
            IDictionary<string, object> dic;
            dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = form.examinationsID;
            List<ExamSubjectBO> lstExamSubjectOfExam = ExamSubjectBusiness.GetListExamSubject(dic).ToList();

            //Lay MarkTypeID
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                string MarkTypeTitle = "";
                MarkTypeTitle = form.MarkTypeTitle;
                MarkType markType = MarkTypeBusiness.All.Where(o => o.AppliedLevel == _globalInfo.AppliedLevel && o.Title == MarkTypeTitle)
                                                    .FirstOrDefault();
                form.MarkTypeID = markType.MarkTypeID;
            }

            //Lay danh sach ID diem thi duoc chon
            List<long> listInputMarkID = GetIDsFromString(form.SelectedID);

            //Danh sach diem thi cua ky thi (chi cac hoc sinh co trang thai dang hoc)
            List<ExamInputMark> listInputMark = ExamInputMarkBusiness.SearchForTransfer(_globalInfo.AcademicYearID, _globalInfo.SchoolID, form.examinationsID).ToList();

            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_SUBJECT)
            {
                listInputMark = listInputMark.Where(o => listInputMarkID.Contains(o.ExamInputMarkID)).ToList();
            }

            //Danh sach hoc sinh vang thi
            List<ExamPupilAbsence> lstExamPupilAbsence = new List<ExamPupilAbsence>();
            List<long> lstExamPupilID = new List<long>();
            dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = form.examinationsID;
            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_SUBJECT)
            {
                dic["SubjectID"] = form.subjectID;
                dic["ExamGroupID"] = form.examGroupID;
            }
            lstExamPupilAbsence = ExamPupilAbsenceBusiness.Search(dic).ToList();

            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_EXAM_GROUP)
            {
                //Lay danh sach mon thi
                List<ExamSubjectBO> lstExamSubject = lstExamSubjectOfExam.Where(o => o.ExamGroupID == form.examGroupID).ToList();
                List<int> lstSubjectID = lstExamSubject.Select(x => x.SubjectID).ToList();
                lstExamPupilAbsence = lstExamPupilAbsence.Where(x => lstSubjectID.Contains(x.SubjectID)).ToList();
            }
            lstExamPupilID = lstExamPupilAbsence.Select(x => ((long)x.ExamPupilID)).ToList();
            List<int> subjectIDOfPupilAbsence = lstExamPupilAbsence.Select(x => x.SubjectID).ToList();

            dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = form.examinationsID;
            dic["SchoolID"] = objSchool.SchoolProfileID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["lstExamPupil"] = lstExamPupilID;
            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_SUBJECT 
                || form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_EXAM_GROUP)
            {
                dic["ExamGroupID"] = form.examGroupID;
            }
            List<ExamPupil> lstExamPupil = ExamPupilBusiness.Search(dic).ToList();

            //Danh sach PupilOfClass
            List<PupilOfClassBO> listPoc = PupilOfClassBusiness.GetStudyingPupil(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value,
                                                                                _globalInfo.AppliedLevel.Value, new Dictionary<string, object>()).ToList();
            // Loai bo nhung thi sinh vang thi
            if (lstExamPupil.Count() > 0)
            {
                for (int i = 0; i < lstExamPupilAbsence.Count(); i++)
                { 
                    var objExamPupil = lstExamPupil.Where(x=>x.ExamPupilID == lstExamPupilAbsence[i].ExamPupilID).FirstOrDefault();
                    if(objExamPupil != null)
                    {
                        var objInputMark = listInputMark.Where(x => x.PupilID == objExamPupil.PupilID 
                                                                && x.SubjectID == lstExamPupilAbsence[i].SubjectID).FirstOrDefault();
                        if (objInputMark != null)
                        { 
                            listInputMark.Remove(objInputMark); 
                        }
                    }    
                }     
            }

            if (isNewModel && form.SelectVNEN == 1)
            {
                listPoc = listPoc.Where(x => x.IsVNEN.HasValue && x.IsVNEN.Value == true).ToList();
            }

            List<int> lstPupilID = listPoc.Select(x => x.PupilID).ToList();
            List<int> lstClassID = listPoc.Select(x => x.ClassID).Distinct().ToList();
            //Danh sach ClassSubject
            List<ClassSubject> listClassSubject;
            dic = new Dictionary<string, object>();
            //dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = form.Semester;

            listClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Danh sach SchoolSubject
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic).ToList();

            //Danh sach MarkRecord
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["Semester"] = form.Semester;
            dic["MarkTypeID"] = form.MarkTypeID;
            dic["Title"] = form.MarkColumn;
            List<MarkRecord> listMarkRecord = new List<MarkRecord>();
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic).ToList();
            }

            //Danh sach JudgeRecord
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID; ;
            dic["Semester"] = form.Semester;
            dic["MarkTypeID"] = form.MarkTypeID;
            dic["Title"] = form.MarkColumn;
            List<JudgeRecord> listJudgeRecord = new List<JudgeRecord>();
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                listJudgeRecord = JudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic).ToList();
            }

            List<SummedEvaluation> listSummedEvaluation = new List<SummedEvaluation>();
            List<RatedCommentPupil> lstRatedCommentPupil = new List<RatedCommentPupil>();
            List<RatedCommentPupilHistory> lstRatedCommentHistory = new List<RatedCommentPupilHistory>();
            List<ClassSubject> lstSubject = new List<ClassSubject>();
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemster = new List<TeacherNoteBookSemester>();
            List<LockRatedCommentPupil> lstDataLock = new List<LockRatedCommentPupil>();
            bool lockGK = false;
            bool lockCK = false;
            //Neu la tieu hoc
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.GetValueOrDefault());
                /*listSummedEvaluation = SummedEvaluationBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        && o.LastDigitSchoolID == partition
                                                                                        && o.SchoolID == _globalInfo.SchoolID
                                                                                        && o.SemesterID == form.Semester)
                                                                                        .ToList();*/
                if (!isMoveHistory)
                {
                    lstRatedCommentPupil = this.RatedCommentPupilBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        && x.SemesterID == form.Semester
                                                                                        && x.SchoolID == _globalInfo.SchoolID
                                                                                        && x.LastDigitSchoolID == partition
                                                                                        && lstPupilID.Contains(x.PupilID)).ToList();
                }
                else
                {
                    lstRatedCommentHistory = this.RatedCommentPupilHistoryBusiness.All.Where(x => x.AcademicYearID == _globalInfo.AcademicYearID
                                                                                        && x.SemesterID == form.Semester
                                                                                        && x.SchoolID == _globalInfo.SchoolID
                                                                                        && x.LastDigitSchoolID == partition
                                                                                        && lstPupilID.Contains(x.PupilID)).ToList();
                }
            }
            else if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                if (isNewModel && form.SelectVNEN == 1)
                {
                    IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                    dicSearch["SchoolID"] = _globalInfo.SchoolID;
                    dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID;
                    dicSearch["SemesterID"] = form.Semester;
                    lstTeacherNoteBookSemster = TeacherNoteBookSemesterBusiness.Search(dicSearch).Where(x => lstClassID.Contains(x.ClassID)).ToList();
                    lstSubject = ClassSubjectBusiness.All.Where(x => lstClassID.Contains(x.ClassID) && x.IsSubjectVNEN == true).ToList();

                    IDictionary<string, object> dicLock = new Dictionary<string, object>();
                    dicLock["SchoolID"] = _globalInfo.SchoolID.Value;
                    dicLock["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                    lstDataLock = LockRatedCommentPupilBusiness.Search(dicLock).Where(x => lstClassID.Contains(x.ClassID) && x.EvaluationID == 4).ToList();
                }
            }

            //Danh sach insert, update
            List<Object> listMarkToInsert = new List<Object>();
            List<Object> listMarkToUpdate = new List<Object>();

            List<Object> listJudgeMarkInsert = new List<Object>();
            List<Object> listJudgeMarkUpdate = new List<Object>();

            int totalRecord = 0;

            List<ClassSubject> lstClassSubjectNoneVNEN = new List<ClassSubject>();
            if ((!isNewModel || form.SelectVNEN != 1))
            {
                lstClassSubjectNoneVNEN = listClassSubject.Where(x => (!x.IsSubjectVNEN.HasValue
                                                       || (x.IsSubjectVNEN.HasValue && x.IsSubjectVNEN.Value == false))).ToList();
            }

            #region
            //Mon thi hien tai
            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_SUBJECT)
            {
                #region
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == form.subjectID).FirstOrDefault();
                    if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        if (isNewModel && form.SelectVNEN == 1)
                        {
                            var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                            var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                            var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                            this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                            lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                            lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                            InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                        }
                        else
                        {
                            InputMarkTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listMarkRecord, ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                        }
                    }
                    else if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (isNewModel && form.SelectVNEN == 1)
                        {
                            var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                            var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                            var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                            this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                            lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                            lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                            InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                        }
                        else
                        {
                            InputMarkJudgeTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listJudgeRecord, listJudgeMarkInsert, listJudgeMarkUpdate, ref totalRecord);
                        }                        
                    }
                }
                else
                {
                    InputMarkTransferHandle1(form, listInputMark, listPoc, listClassSubject, lstRatedCommentPupil, lstRatedCommentHistory, isMoveHistory, listMarkToInsert, listMarkToUpdate, ref totalRecord);
                }
                #endregion
            }
            else if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_EXAM_GROUP)
            {
                #region
                //Lay danh sach mon thi
                List<ExamSubjectBO> lstExamSubject = lstExamSubjectOfExam.Where(o => o.ExamGroupID == form.examGroupID).ToList();

                for (int i = 0; i < lstExamSubject.Count; i++)
                {
                    ExamSubjectBO subject = lstExamSubject[i];
                    form.subjectID = subject.SubjectID;

                    if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                    {
                        SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == form.subjectID).FirstOrDefault();
                        if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                        {
                            if (isNewModel && form.SelectVNEN == 1)
                            {
                                var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                                var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                                var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                                this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                                lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                                lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                                InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                    ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                            }
                            else
                            {
                                InputMarkTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listMarkRecord, ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                            }
                        }
                        else if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                        {
                            if (isNewModel && form.SelectVNEN == 1)
                            {
                                var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                                var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                                var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                                this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                                lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                                lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                                InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                    ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                            }
                            else
                            {
                                InputMarkJudgeTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listJudgeRecord, listJudgeMarkInsert, listJudgeMarkUpdate, ref totalRecord);
                            }
                        }
                    }
                    else
                    {
                        InputMarkTransferHandle1(form, listInputMark, listPoc, listClassSubject, lstRatedCommentPupil, lstRatedCommentHistory, isMoveHistory, listMarkToInsert, listMarkToUpdate, ref totalRecord);
                    }
                }
                #endregion
            }
            else if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_ALL)
            {
                #region
                List<ExamGroup> lstExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(form.examinationsID)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
                for (int i = 0; i < lstExamGroup.Count; i++)
                {
                    ExamGroup eg = lstExamGroup[i];
                    form.examGroupID = eg.ExamGroupID;

                    //Lay danh sach mon thi
                    List<ExamSubjectBO> lstExamSubject = lstExamSubjectOfExam.Where(o => o.ExamGroupID == form.examGroupID).ToList();

                    for (int j = 0; j < lstExamSubject.Count; j++)
                    {
                        ExamSubjectBO subject = lstExamSubject[j];
                        form.subjectID = subject.SubjectID;

                        if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                        {
                            SchoolSubject ss = listSchoolSubject.Where(o => o.SubjectID == form.subjectID).FirstOrDefault();
                            if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                            {
                                if (isNewModel && form.SelectVNEN == 1)
                                {
                                    var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                                    var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                                    var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                                    this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                                    lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                                    lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                                    InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                        ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                                }
                                else
                                {
                                    InputMarkTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listMarkRecord, ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                                }
                            }
                            else if (ss != null && ss.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                            {
                                if (isNewModel && form.SelectVNEN == 1)
                                {
                                    var lstSubjectVNEN = lstSubject.Where(x => x.SubjectID == form.subjectID).ToList();
                                    var lstTeacherNoteBookSemsterBySubject = lstTeacherNoteBookSemster.Where(x => x.SubjectID == form.subjectID).ToList();
                                    var lstDataLockBySubject = lstDataLock.Where(x => x.SubjectID == form.subjectID).ToList();
                                    this.IsDataLockKTDK(form.Semester, lstDataLockBySubject);
                                    lockGK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK];
                                    lockCK = (bool)ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK];
                                    InputMarkTransferHandleVNEN2(form, listInputMark, listPoc, listClassSubject, lstTeacherNoteBookSemsterBySubject, lstSubjectVNEN, lockGK, lockCK,
                                        ref listMarkToInsert, ref listMarkToUpdate, ref totalRecord);
                                }
                                else
                                {
                                    InputMarkJudgeTransferHandle23(form, listInputMark, listPoc, lstClassSubjectNoneVNEN, listJudgeRecord, listMarkToInsert, listMarkToUpdate, ref totalRecord);
                                }
                            }
                        }
                        else
                        {
                            InputMarkTransferHandle1(form, listInputMark, listPoc, listClassSubject, lstRatedCommentPupil, lstRatedCommentHistory, isMoveHistory, listMarkToInsert, listMarkToUpdate, ref totalRecord);
                        }
                    }
                }
                #endregion
            }
            #endregion

            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                if (isNewModel && form.SelectVNEN == 1)
                {
                    TeacherNoteBookSemesterBusiness.ImportFromInputMark(listMarkToInsert, listMarkToUpdate);
                }
                else
                {
                    if (listMarkToInsert.Count > 0 || listMarkToUpdate.Count > 0)
                    {
                        MarkRecordBusiness.ImportFromInputMark(listMarkToInsert, listMarkToUpdate);
                    }

                    if (listJudgeMarkInsert.Count > 0 || listJudgeMarkUpdate.Count > 0)
                    {
                        JudgeRecordBusiness.ImportFromInputMark(listJudgeMarkInsert, listJudgeMarkUpdate);
                    }
                }

            }
            else
            {
                if (listMarkToInsert.Count > 0 || listMarkToUpdate.Count > 0)
                {
                    //SummedEvaluationBusiness.ImportFromInputMark(listMarkToInsert, listMarkToUpdate);
                    if (!isMoveHistory)
                    {
                        RatedCommentPupilBusiness.ImportFromInputMark(listMarkToInsert, listMarkToUpdate);
                    }
                    else
                    {
                        RatedCommentPupilHistoryBusiness.ImportFromInputMark(listMarkToInsert, listMarkToUpdate);
                    }
                }
            }

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , String.Empty
            , Res.Get("ExamInputMarkTransfer_Log_FunctionName")
            , String.Empty
            , Res.Get("ExamInputMarkTransfer_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_ADD
            , Res.Get("ExamInputMarkTransfer_Log_FunctionName"));

            if (form.TransferType == ExamInputMarkTransferConstants.TRANSFER_CURRENT_SUBJECT)
            {
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    if (isNewModel && form.SelectVNEN == 1)
                    {
                        return Json(new JsonMessage(String.Format(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess_WithCount_VNEN"), totalRecord, listInputMarkID.Count)));
                    }

                    return Json(new JsonMessage(String.Format(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess_WithCount"), totalRecord, listInputMarkID.Count)));
                }

                return Json(new JsonMessage(String.Format(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess_WithCount_Primary"), totalRecord, listInputMarkID.Count)));             
            }
            
            if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
           {
                    if (isNewModel && form.SelectVNEN == 1)
                    {
                        return Json(new JsonMessage(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess_VNEN")));
                    }

                    return Json(new JsonMessage(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess")));
                }

                return Json(new JsonMessage(Res.Get("ExamInputMarkTransfer_Message_TransferSuccess_Primary")));
            
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamSubject(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }

        public JsonResult AjaxLoadMarkColumn(int markType, int semester)
        {

            Dictionary<string, object> dicToGetSD = new Dictionary<string, object>();
            dicToGetSD["SchoolID"] = _globalInfo.SchoolID.Value;
            dicToGetSD["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicToGetSD["Semester"] = semester;

            IQueryable<SemeterDeclaration> lstSD = SemeterDeclarationBusiness.Search(dicToGetSD).OrderByDescending(o => o.AppliedDate);
            SemeterDeclaration sd = lstSD.FirstOrDefault();

            //So con diem viet he so 1
            int writingMarkNum = 0;
            //So con diem viet he so 2
            int twiceCoeffiecientMarkNum = 0;
            if (sd != null)
            {
                //So con diem viet he so 1
                writingMarkNum = sd.WritingMark;
                //So con diem viet he so 2
                twiceCoeffiecientMarkNum = sd.TwiceCoeffiecientMark;
            }
            List<ComboObject> lstMarkColumn = new List<ComboObject>();

            if (markType == ExamInputMarkTransferConstants.MARK_TYPE_15P)
            {
                for (int i = 1; i <= writingMarkNum; i++)
                {
                    string markColumn = "P" + i.ToString();
                    lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
                }
            }
            else if (markType == ExamInputMarkTransferConstants.MARK_TYPE_V)
            {

                for (int i = 1; i <= twiceCoeffiecientMarkNum; i++)
                {
                    string markColumn = "V" + i.ToString();
                    lstMarkColumn.Add(new ComboObject(markColumn, markColumn));
                }
            }
            else if (markType == ExamInputMarkTransferConstants.MARK_TYPE_HK)
            {
                lstMarkColumn.Add(new ComboObject("HK", "KTHK"));
            }

            return Json(new SelectList(lstMarkColumn, "key", "value"));
        }

        #endregion

        #region Private method
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID && o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamInputMarkTransferConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamInputMarkTransferConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach mon thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (defaultExamGroupID != null)
            {
                dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList();
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }

            //Neu khong phai la quan tri truong
            if (!_globalInfo.IsAdmin)
            {
                //Lay danh sach mon thi can bo duoc phan cong nhap diem thi
                dic = new Dictionary<string, object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                int EmployeeID = _globalInfo.EmployeeID.GetValueOrDefault();
                List<int> listSubjectID = ExamInputMarkAssignedBusiness.Search(dic)
                                                                        .Where(o => o.TeacherID == EmployeeID)
                                                                        .Select(o => o.SubjectID).Distinct().ToList();
                listExamSubject = listExamSubject.Where(o => listSubjectID.Contains(o.SubjectID)).ToList();
            }

            ViewData[ExamInputMarkTransferConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            int? defaultSubjectID = null;
            if (listExamSubject.Count > 0)
            {
                defaultSubjectID = listExamSubject.First().SubjectID;
            }

            SetViewDataForTransferForm(defaultExamID, defaultSubjectID);
        }

        private void SetViewDataForTransferForm(long? examinationsID, long? subjectID)
        {
            //Lay thong tin cap hoc
            ViewData[ExamInputMarkTransferConstants.APPLY_LEVEL] = _globalInfo.AppliedLevel;

            //Lay thong tin hoc ky cua ky thi
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ViewData[ExamInputMarkTransferConstants.SEMESTER_NAME] = String.Empty;
            ViewData[ExamInputMarkTransferConstants.SEMESTER] = 0;
            if (exam != null)
            {
                if (exam.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    ViewData[ExamInputMarkTransferConstants.SEMESTER_NAME] = SystemParamsInFile.SEMESTER_I;
                }
                else if (exam.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    ViewData[ExamInputMarkTransferConstants.SEMESTER_NAME] = SystemParamsInFile.SEMESTER_II;
                }
                ViewData[ExamInputMarkTransferConstants.SEMESTER] = exam.SemesterID;
            }

            //Lay danh sach cot diem
            List<Object> listMarkColumn = new List<object>();
            //Cap 2&3
            listMarkColumn.Add(new { key = "HK", value = "KTHK" });



            ViewData[ExamInputMarkTransferConstants.CBO_MARK_COLUMN] = new SelectList(listMarkColumn, "key", "value");

        }
        /// <summary>
        /// Ham tim kiem
        /// </summary>
        /// <param name="examinationsID"></param>
        /// <param name="examGroupID"></param>
        /// <param name="ExamRoomID"></param>
        /// <returns></returns>
        private IEnumerable<ListViewModel> _Search(long examinationsID, long examGroupID, long subjectID)
        {
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Lay danh sach diem thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["LastDigitSchoolID"] = partition;
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SubjectID"] = subjectID;

            List<ExamInputMark> listInputMark = ExamInputMarkBusiness.Search(dic).ToList();

            //Lay danh sach thi sinh cua nhom thi
            dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SchoolID"] = _globalInfo.SchoolID;

            List<ExamPupilBO> listExamPupil = ExamPupilBusiness.Search(dic, _globalInfo.AcademicYearID.Value, partition).ToList();

            //Lay kieu mon
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SubjectID"] = subjectID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic).FirstOrDefault();
            int isCommentSubject = GlobalConstants.ISCOMMENTING_TYPE_MARK;
            if (schoolSubject != null && schoolSubject.IsCommenting != null)
            {
                isCommentSubject = schoolSubject.IsCommenting.Value;
            }

            IEnumerable<ListViewModel> ret = (from ep in listExamPupil
                                              join im in listInputMark
                                                  on new { ep.ExaminationsID, ep.ExamGroupID, ep.PupilID } equals new { im.ExaminationsID, im.ExamGroupID, im.PupilID } into des
                                              from x in des.DefaultIfEmpty()
                                              select new ListViewModel
                                              {
                                                  ActualMark = x != null ?
                                                  (_globalInfo.AppliedLevel.GetValueOrDefault() == GlobalConstants.APPLIED_LEVEL_PRIMARY ? String.Format("{0:0}", x.ActualMark)
                                                  : isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK ? (x.ActualMark != 0 && x.ActualMark != 10 ? String.Format("{0:0.0}", x.ActualMark) : String.Format("{0:0}", x.ActualMark)) : x.ExamJudgeMark)
                                                  : null,
                                                  ClassName = ep.ClassName,
                                                  ExamineeNumber = ep.ExamineeNumber,
                                                  ExamInputMarkID = x != null ? (long?)x.ExamInputMarkID : null,
                                                  ExamMark = x != null ?
                                                  (_globalInfo.AppliedLevel.GetValueOrDefault() == GlobalConstants.APPLIED_LEVEL_PRIMARY ? String.Format("{0:0}", x.ExamMark)
                                                  : isCommentSubject == GlobalConstants.ISCOMMENTING_TYPE_MARK ? (x.ExamMark != 0 && x.ExamMark != 10 ? String.Format("{0:0.0}", x.ExamMark) : String.Format("{0:0}", x.ExamMark)) : x.ExamJudgeMark)
                                                  : null,
                                                  ExamRoomCode = ep.ExamRoomCode,
                                                  PupilCode = ep.PupilCode,
                                                  PupilName = ep.PupilFullName,
                                                  ExamPupilID = ep.ExamPupilID,
                                                  PupilID = ep.PupilID,
                                                  IsVNENClass = ep.IsVNENClass,
                                                  ClassID = ep.ClassID
                                              });

            return ret.OrderBy(o => o.ExamineeNumber);

        }

        /// <summary>
        /// Chuyen diem mon tinh diem cho cap 1
        /// </summary>
        /// <param name="form"></param>
        /// <param name="listExamInputMark"></param>
        /// <param name="listPupilOfClass"></param>
        /// <param name="listClassSubject"></param>
        /// <param name="listMarkRecord"></param>
        /// <param name="listToInsert"></param>
        /// <param name="listToUpdate"></param>
        private void InputMarkTransferHandle1(FormViewModel form, List<ExamInputMark> listExamInputMark, List<PupilOfClassBO> listPupilOfClass,
            List<ClassSubject> listClassSubject, List<RatedCommentPupil> lstRatedComment, List<RatedCommentPupilHistory> lstRatedCommentHistory,
            bool isMoveHistory, List<Object> listToInsert, List<Object> listToUpdate, ref int totalRecord)
        {
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            //Lay danh sach diem thi cua nhom thi va mon thi
            List<ExamInputMark> listInputMark = listExamInputMark.Where(o => o.ExamGroupID == form.examGroupID && o.SubjectID == form.subjectID).ToList();

            PupilOfClassBO poc = null;
            ClassSubject cs = null;
            RatedCommentPupil objRatedComment = null;
            RatedCommentPupilHistory objRatedCommentHistory = null;
            bool isMathOrTV5 = false; // môn toán/TV lớp 5
            for (int i = 0; i < listInputMark.Count; i++)
            {
                ExamInputMark entity = listInputMark[i];
                if (entity.ExamMark == null || entity.ActualMark == null)
                {
                    continue;
                }
                long PupilID = entity.PupilID;

                poc = listPupilOfClass.Where(o => o.PupilID == PupilID).FirstOrDefault();
                if (poc == null)
                {
                    continue;
                }


                int ClassID = poc.ClassID;
                cs = listClassSubject.Where(o => o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();

                if (cs == null)
                {
                    continue;
                }
                isMathOrTV5 = ((entity.SubjectID == ExamInputMarkTransferConstants.SubjectMath
                            || entity.SubjectID == ExamInputMarkTransferConstants.SubjectTV)
                            && poc.EducationLevelID == 5) ? true : false;

                if (cs.IsCommenting.HasValue && cs.IsCommenting == 0)
                {
                    if (isMoveHistory)
                    {
                        #region
                        objRatedCommentHistory = lstRatedCommentHistory.Where(x => x.PupilID == PupilID && x.ClassID == ClassID && x.SubjectID == form.subjectID).FirstOrDefault();
                        if (objRatedCommentHistory != null)
                        {
                            if (isMathOrTV5)
                            {
                                if (form.MarkColumn2ID == 1) // KTDK GK
                                {
                                    objRatedCommentHistory.PeriodicMiddleMark = (int)entity.ActualMark;
                                }
                                else // KTDK CK
                                {
                                    objRatedCommentHistory.PeriodicEndingMark = (int)entity.ActualMark;
                                }

                                objRatedCommentHistory.UpdateTime = DateTime.Now;
                                listToUpdate.Add(objRatedComment);
                            }
                            else
                            {
                                if (form.MarkColumn2ID == 2) // KTDK CK
                                {
                                    objRatedCommentHistory.PeriodicEndingMark = (int)entity.ActualMark;

                                    objRatedCommentHistory.UpdateTime = DateTime.Now;
                                    listToUpdate.Add(objRatedCommentHistory);
                                }
                            }
                        }
                        else
                        {
                            objRatedCommentHistory = new RatedCommentPupilHistory();
                            objRatedCommentHistory.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRatedCommentHistory.CapacityEndingEvaluation = null;
                            objRatedCommentHistory.CapacityMiddleEvaluation = null;
                            objRatedCommentHistory.ClassID = ClassID;
                            objRatedCommentHistory.Comment = null;
                            objRatedCommentHistory.CreateTime = DateTime.Now;
                            objRatedCommentHistory.EndingEvaluation = null;
                            objRatedCommentHistory.EvaluationID = 1;
                            objRatedCommentHistory.EvaluationRetraning = null;
                            objRatedCommentHistory.FullNameComment = null;
                            objRatedCommentHistory.LastDigitSchoolID = partition;
                            objRatedCommentHistory.LogChangeID = LogChangeID;
                            objRatedCommentHistory.MiddleEvaluation = null;
                            objRatedCommentHistory.PeriodicEndingJudgement = null;
                            objRatedCommentHistory.PeriodicMiddleJudgement = null;
                            objRatedCommentHistory.PupilID = (int)PupilID;
                            objRatedCommentHistory.QualityEndingEvaluation = null;
                            objRatedCommentHistory.QualityMiddleEvaluation = null;
                            objRatedCommentHistory.RetestMark = null;
                            objRatedCommentHistory.SchoolID = _globalInfo.SchoolID.Value;
                            objRatedCommentHistory.SemesterID = form.Semester;
                            objRatedCommentHistory.SubjectID = entity.SubjectID;
                            objRatedCommentHistory.UpdateTime = null;

                            if (isMathOrTV5)
                            {
                                if (form.MarkColumn2ID == 1) // KTDK GK
                                {
                                    objRatedCommentHistory.PeriodicMiddleMark = (int)entity.ActualMark;
                                }
                                else // KTDK CK
                                {
                                    objRatedCommentHistory.PeriodicEndingMark = (int)entity.ActualMark;
                                }
                                listToInsert.Add(objRatedCommentHistory);
                            }
                            else
                            {
                                if (form.MarkColumn2ID == 2) // KTDK CK
                                {
                                    objRatedCommentHistory.PeriodicEndingMark = (int)entity.ActualMark;
                                    listToInsert.Add(objRatedCommentHistory);
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region
                        objRatedComment = lstRatedComment.Where(x => x.PupilID == PupilID && x.ClassID == ClassID && x.SubjectID == form.subjectID).FirstOrDefault();
                        if (objRatedComment != null)
                        {

                            if (isMathOrTV5)
                            {
                                if (form.MarkColumn2ID == 1) // KTDK GK
                                {
                                    objRatedComment.PeriodicMiddleMark = (int)entity.ActualMark;
                                }
                                else // KTDK CK
                                {
                                    objRatedComment.PeriodicEndingMark = (int)entity.ActualMark;
                                }

                                objRatedComment.UpdateTime = DateTime.Now;
                                listToUpdate.Add(objRatedComment);
                            }
                            else
                            {
                                if (form.MarkColumn2ID == 2) // KTDK CK
                                {
                                    objRatedComment.PeriodicEndingMark = (int)entity.ActualMark;

                                    objRatedComment.UpdateTime = DateTime.Now;
                                    listToUpdate.Add(objRatedComment);
                                }
                            }


                        }
                        else
                        {
                            objRatedComment = new RatedCommentPupil();
                            objRatedComment.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objRatedComment.CapacityEndingEvaluation = null;
                            objRatedComment.CapacityMiddleEvaluation = null;
                            objRatedComment.ClassID = ClassID;
                            objRatedComment.Comment = null;
                            objRatedComment.CreateTime = DateTime.Now;
                            objRatedComment.EndingEvaluation = null;
                            objRatedComment.EvaluationID = 1;
                            objRatedComment.EvaluationRetraning = null;
                            objRatedComment.FullNameComment = null;
                            objRatedComment.LastDigitSchoolID = partition;
                            objRatedComment.LogChangeID = LogChangeID;
                            objRatedComment.MiddleEvaluation = null;
                            objRatedComment.PeriodicEndingJudgement = null;
                            objRatedComment.PeriodicMiddleJudgement = null;
                            objRatedComment.PupilID = (int)PupilID;
                            objRatedComment.QualityEndingEvaluation = null;
                            objRatedComment.QualityMiddleEvaluation = null;
                            objRatedComment.RetestMark = null;
                            objRatedComment.SchoolID = _globalInfo.SchoolID.Value;
                            objRatedComment.SemesterID = form.Semester;
                            objRatedComment.SubjectID = entity.SubjectID;
                            objRatedComment.UpdateTime = null;

                            if (isMathOrTV5)
                            {
                                if (form.MarkColumn2ID == 1) // KTDK GK
                                {
                                    objRatedComment.PeriodicMiddleMark = (int)entity.ActualMark;
                                }
                                else // KTDK CK
                                {
                                    objRatedComment.PeriodicEndingMark = (int)entity.ActualMark;
                                }
                                listToInsert.Add(objRatedComment);
                            }
                            else
                            {
                                if (form.MarkColumn2ID == 2) // KTDK CK
                                {
                                    objRatedComment.PeriodicEndingMark = (int)entity.ActualMark;
                                    listToInsert.Add(objRatedComment);
                                }
                            }
                        }
                        #endregion
                    }                
                }
            }
            totalRecord = listToInsert.Count() + listToUpdate.Count();
        }

        /// <summary>
        /// Chuyen diem mon nhan xet cho cap 2,3
        /// </summary>
        /// <param name="form"></param>
        /// <param name="listExamInputMark"></param>
        /// <param name="listPupilOfClass"></param>
        /// <param name="listClassSubject"></param>
        /// <param name="listMarkRecord"></param>
        /// <param name="listToInsert"></param>
        /// <param name="listToUpdate"></param>
        private void InputMarkJudgeTransferHandle23(FormViewModel form, List<ExamInputMark> listExamInputMark, List<PupilOfClassBO> listPupilOfClass,
            List<ClassSubject> listClassSubject, List<JudgeRecord> listJudgeRecord, List<Object> listToInsert, List<Object> listToUpdate, ref int totalRecord)
        {
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Lay danh sach diem thi cua nhom thi va mon thi
            List<ExamInputMark> listInputMark = listExamInputMark.Where(o => o.ExamGroupID == form.examGroupID && o.SubjectID == form.subjectID)
                                                                .ToList();
            int orderNumber = 1;
            if (!String.IsNullOrEmpty(form.MarkColumn) && form.MarkColumn != "HK")
            {
                int.TryParse(Regex.Match(form.MarkColumn, @"\d+").Value, out orderNumber);
            }

            for (int i = 0; i < listInputMark.Count; i++)
            {
                ExamInputMark entity = listInputMark[i];
                if (String.IsNullOrEmpty(entity.ExamJudgeMark))
                {
                    continue;
                }
                long PupilID = entity.PupilID;

                PupilOfClassBO poc = listPupilOfClass.FirstOrDefault(o => o.PupilID == PupilID);
                if (poc == null)
                {
                    continue;
                }
                int ClassID = poc.ClassID;
                ClassSubject cs = listClassSubject.Where(o => o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();

                if (cs == null)
                {
                    continue;
                }
                //Lay ra diem trong so diem cua hoc sinh
                JudgeRecord jr = listJudgeRecord.Where(o => o.PupilID == PupilID && o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();
                if (jr != null)
                {
                    jr.Judgement = entity.ExamJudgeMark;
                    jr.MarkedDate = DateTime.Now;
                    jr.ModifiedDate = DateTime.Now;
                    listToUpdate.Add(jr);
                }
                else
                {
                    jr = new JudgeRecord();
                    jr.AcademicYearID = _globalInfo.AcademicYearID.GetValueOrDefault();
                    jr.ClassID = ClassID;
                    AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());
                    jr.CreatedAcademicYear = aca.Year;
                    jr.CreatedDate = DateTime.Now;
                    jr.Judgement = entity.ExamJudgeMark;
                    jr.JudgeRecordID = JudgeRecordBusiness.GetNextSeq<long>();
                    jr.Last2digitNumberSchool = partition;
                    jr.MarkedDate = DateTime.Now;
                    jr.MarkTypeID = form.MarkTypeID;
                    //OrderNumber
                    jr.OrderNumber = orderNumber;
                    jr.PupilID = (int)PupilID;
                    jr.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                    jr.Semester = form.Semester;
                    jr.SubjectID = form.subjectID;
                    jr.Title = form.MarkColumn;
                    jr.Year = aca.Year;

                    listToInsert.Add(jr);
                }            
            }
            totalRecord = listToInsert.Count() + listToUpdate.Count();
        }    

        private void InputMarkTransferHandle23(FormViewModel form, List<ExamInputMark> listExamInputMark, List<PupilOfClassBO> listPupilOfClass, List<ClassSubject> listClassSubject,
            List<MarkRecord> listMarkRecord, ref List<Object> listToInsert, ref List<Object> listToUpdate, ref int totalRecord)
        {
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());
            //Lay danh sach diem thi cua nhom thi va mon thi
            List<ExamInputMark> listInputMark = listExamInputMark.Where(o => o.ExamGroupID == form.examGroupID && o.SubjectID == form.subjectID).ToList();

            int orderNumber = 1;
            if (!String.IsNullOrEmpty(form.MarkColumn) && form.MarkColumn != "HK")
            {
                int.TryParse(Regex.Match(form.MarkColumn, @"\d+").Value, out orderNumber);
            }


            for (int i = 0; i < listInputMark.Count; i++)
            {
                ExamInputMark entity = listInputMark[i];
                if (entity.ExamMark == null || entity.ActualMark == null)
                {
                    continue;
                }
                int PupilID = entity.PupilID;

                PupilOfClassBO poc = listPupilOfClass.Where(o => o.PupilID == PupilID).FirstOrDefault();
                if (poc == null)
                {
                    continue;
                }
                int ClassID = poc.ClassID;
                ClassSubject cs = listClassSubject.Where(o => o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();

                if (cs == null)
                {
                    continue;
                }
                //Lay ra diem trong so diem cua hoc sinh

                MarkRecord mr = listMarkRecord.Where(o => o.PupilID == PupilID && o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();
                if (mr != null)
                {
                    mr.Mark = Business.Common.Utils.RoundMark(entity.ActualMark.Value);
                    mr.MarkedDate = DateTime.Now;
                    mr.ModifiedDate = DateTime.Now;
                    listToUpdate.Add(mr);
                }
                else
                {
                    mr = new MarkRecord();
                    mr.AcademicYearID = aca.AcademicYearID;
                    mr.ClassID = ClassID;

                    mr.CreatedAcademicYear = aca.Year;
                    mr.CreatedDate = DateTime.Now;
                    mr.Last2digitNumberSchool = partition;
                    mr.Mark = Business.Common.Utils.RoundMark(entity.ActualMark.Value);// entity.ActualMark.Value;
                    mr.MarkedDate = DateTime.Now;
                    mr.MarkTypeID = form.MarkTypeID;
                    mr.OrderNumber = orderNumber;
                    mr.PupilID = PupilID;
                    mr.SchoolID = aca.SchoolID;
                    mr.Semester = form.Semester;
                    mr.SubjectID = form.subjectID;
                    mr.Title = form.MarkColumn;
                    mr.Year = aca.Year;
                    mr.MSourcedb = "CDT";
                    listToInsert.Add(mr);
                }
            }
            totalRecord = listToInsert.Count() + listToUpdate.Count();
        }

        private void InputMarkTransferHandleVNEN2(FormViewModel form, List<ExamInputMark> listExamInputMark, List<PupilOfClassBO> listPupilOfClass, List<ClassSubject> listClassSubject,
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemster, List<ClassSubject> lstSubjectVNEN, bool lockGK, bool lockCK, 
            ref List<Object> listToInsert, ref List<Object> listToUpdate, ref int totalRecord)
        {
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());
            //Lay danh sach diem thi cua nhom thi va mon thi
            List<ExamInputMark> listInputMark = listExamInputMark.Where(o => o.ExamGroupID == form.examGroupID && o.SubjectID == form.subjectID).ToList();

            int orderNumber = 1;
            if (!String.IsNullOrEmpty(form.MarkColumn) && form.MarkColumn != "HK")
            {
                int.TryParse(Regex.Match(form.MarkColumn, @"\d+").Value, out orderNumber);
            }

            PupilOfClassBO poc = null;
            ClassSubject cs = null;
            //TeacherNoteBookMonth objNoteBookMonth = null;
            TeacherNoteBookSemester objNoteBookSemester = null;
            for (int i = 0; i < listInputMark.Count; i++)
            {
                ExamInputMark entity = listInputMark[i];
                
                int PupilID = entity.PupilID;

                poc = listPupilOfClass.Where(o => o.PupilID == PupilID).FirstOrDefault();
                if (poc == null)
                {
                    continue;
                }
                int ClassID = poc.ClassID;

                cs = listClassSubject.Where(o => o.ClassID == ClassID && o.SubjectID == form.subjectID).FirstOrDefault();
                if (cs == null)
                {
                    continue;
                }

                var objSubjectVNEN = lstSubjectVNEN.Where(x => x.SubjectID == cs.SubjectID && x.ClassID == poc.ClassID).FirstOrDefault();
                if (objSubjectVNEN == null || (objSubjectVNEN.IsSubjectVNEN == null || !objSubjectVNEN.IsSubjectVNEN.HasValue))
                {
                    continue;
                }              

                if (objSubjectVNEN.IsSubjectVNEN.Value == true)
                {
                    if (objSubjectVNEN.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        if (entity.ExamMark == null || entity.ActualMark == null)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(entity.ExamJudgeMark))
                        {
                            continue;
                        }
                    }
                    
                    objNoteBookSemester = lstTeacherNoteBookSemster.Where(x => x.PupilID == PupilID && x.SubjectID == entity.SubjectID).FirstOrDefault();

                    // Mon tinh diem
                    if (objSubjectVNEN.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        #region
                        if (objNoteBookSemester != null)
                        {
                            if (form.MarkColumn2ID == 1) // KTDK GK
                            {
                                if (!lockGK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_MIDDLE = entity.ActualMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;

                                    if (form.Semester == 1 && aca.Year == 2016)
                                    {
                                        objNoteBookSemester.AVERAGE_MARK = objNoteBookSemester.PERIODIC_SCORE_END;
                                    }
                                    else
                                    {
                                        objNoteBookSemester.AVERAGE_MARK = CalculateAverageMark(objNoteBookSemester.PERIODIC_SCORE_MIDDLE, objNoteBookSemester.PERIODIC_SCORE_END);
                                    }
                                    listToUpdate.Add(objNoteBookSemester);
                                }
                            }
                            else // KTDK CK
                            {
                                if (!lockCK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_END = entity.ActualMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;

                                    if (form.Semester == 1 && aca.Year == 2016)
                                    {
                                        objNoteBookSemester.AVERAGE_MARK = objNoteBookSemester.PERIODIC_SCORE_END;
                                    }
                                    else
                                    {
                                        objNoteBookSemester.AVERAGE_MARK = CalculateAverageMark(objNoteBookSemester.PERIODIC_SCORE_MIDDLE, objNoteBookSemester.PERIODIC_SCORE_END);
                                    }
                                    listToUpdate.Add(objNoteBookSemester);
                                }
                            }
                        }
                        else
                        {
                            objNoteBookSemester = new TeacherNoteBookSemester();
                            objNoteBookSemester.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objNoteBookSemester.ClassID = ClassID;
                            objNoteBookSemester.AVERAGE_MARK_JUDGE = null;
                            objNoteBookSemester.CreateTime = DateTime.Now;
                            objNoteBookSemester.PartitionID = partition;
                            objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = null;
                            objNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = null;
                            objNoteBookSemester.PupilID = PupilID;
                            objNoteBookSemester.SchoolID = aca.SchoolID;
                            objNoteBookSemester.SemesterID = form.Semester;
                            objNoteBookSemester.SubjectID = entity.SubjectID;
                            objNoteBookSemester.Rate = null;
                            objNoteBookSemester.UpdateTime = null;

                            if (form.MarkColumn2ID == 1) // KTDK GK
                            {
                                if (!lockGK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_MIDDLE = entity.ActualMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    listToInsert.Add(objNoteBookSemester);
                                }
                            }
                            else // KTDK CK
                            {
                                if (!lockCK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_END = entity.ActualMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    listToInsert.Add(objNoteBookSemester);
                                }
                            }
                        }
                        #endregion
                    }
                    else // Môn nhận xét
                    {
                        #region
                        if (objNoteBookSemester != null)
                        {
                            if (form.MarkColumn2ID == 1) // KTDK GK
                            {
                                if (!lockGK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = entity.ExamJudgeMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    objNoteBookSemester.AVERAGE_MARK_JUDGE = objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                                    listToUpdate.Add(objNoteBookSemester);
                                }
                            }
                            else // KTDK CK
                            {
                                if (!lockCK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = entity.ExamJudgeMark;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    objNoteBookSemester.AVERAGE_MARK_JUDGE = objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                                    listToUpdate.Add(objNoteBookSemester);
                                }
                            }
                        }
                        else
                        {
                            objNoteBookSemester = new TeacherNoteBookSemester();
                            objNoteBookSemester.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objNoteBookSemester.ClassID = ClassID;
                            objNoteBookSemester.CreateTime = DateTime.Now;
                            objNoteBookSemester.PartitionID = partition;
                            objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = null;
                            objNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = null;
                            objNoteBookSemester.PupilID = PupilID;
                            objNoteBookSemester.SchoolID = aca.SchoolID;
                            objNoteBookSemester.SemesterID = form.Semester;
                            objNoteBookSemester.SubjectID = entity.SubjectID;
                            objNoteBookSemester.Rate = null;
                            objNoteBookSemester.UpdateTime = null;

                            if (form.MarkColumn2ID == 1) // KTDK GK
                            {
                                if (!lockGK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = entity.ExamJudgeMark;
                                    objNoteBookSemester.AVERAGE_MARK_JUDGE = objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    listToInsert.Add(objNoteBookSemester);
                                }
                            }
                            else // KTDK CK
                            {
                                if (!lockCK)
                                {
                                    objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = entity.ExamJudgeMark;
                                    objNoteBookSemester.AVERAGE_MARK_JUDGE = objNoteBookSemester.PERIODIC_SCORE_END_JUDGLE;
                                    objNoteBookSemester.UpdateTime = DateTime.Now;
                                    listToInsert.Add(objNoteBookSemester);
                                }
                            }
                        }
                        #endregion
                    }                                    
                }
            }
            totalRecord = listToUpdate.Count() + listToInsert.Count();
        }      

        private decimal? CalculateAverageMark(decimal? coef1Mark, decimal? coef2Mark)
        {
            if (!coef1Mark.HasValue || !coef2Mark.HasValue)
            {
                return null;
            }
            return System.Math.Round((coef1Mark.Value + (coef2Mark.Value * 2)) / 3, 1);
        }

        private void IsDataLockKTDK(int SemesterID, List<LockRatedCommentPupil> lstDataLock)
        {
            // KTĐK Giữa kỳ
            ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK] = false;
            // KTĐK cuối kỳ
            ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK] = false;

            string lockTitle_GK = string.Empty;
            string lockTitle_CK = string.Empty;

            if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {// Học kỳ 1            
                lockTitle_GK = "DKGK1";
                lockTitle_CK = "DKCK1";
            }
            else
            {// Học kỳ 2
                lockTitle_GK = "DKGK2";
                lockTitle_CK = "DKCK2";
            }

            var check_GK = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_GK)
                                      && x.EvaluationID == 4).FirstOrDefault();
            if (check_GK != null)
            {
                ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTGK] = true;
            }

            var check_CK = lstDataLock.Where(x => x.LockTitle.Contains(lockTitle_CK)
                                     && x.EvaluationID == 4).FirstOrDefault();
            if (check_CK != null)
            {
                ViewData[ExamInputMarkTransferConstants.IS_DATA_LOCK_KTCK] = true;
            }
        }

        private List<long> GetIDsFromString(string str)
        {
            string[] IDArr;
            if (!String.IsNullOrEmpty(str))
            {
                str = str.Remove(str.Length - 1);
                IDArr = str.Split(',');
            }
            else
            {
                IDArr = new string[] { };
            }
            List<long> listID = IDArr.Length > 0 ? IDArr.ToList().Distinct().Select(o => Convert.ToInt64(o)).ToList() :
                                            new List<long>();

            return listID;
        }

        /// <summary>
        /// Kiem tra disable/enable
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(Examinations examinations)
        {
            SetViewDataPermission("ExamInputMarkTransfer", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            bool checkButton = false;
            if (examinations != null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE]
                    && examinations.MarkClosing.GetValueOrDefault() != true
                    && _globalInfo.IsCurrentYear)
                {
                    checkButton = true;
                }
            }

            ViewData[ExamInputMarkTransferConstants.PER_CHECK_BUTTON] = checkButton;
        }
        #endregion
    }
}
