﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea
{
    public class ExamInputMarkTransferAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamInputMarkTransferArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamInputMarkTransferArea_default",
                "ExamInputMarkTransferArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
