﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamInputMarkTransferArea
{
    public class ExamInputMarkTransferConstants
    {
        //ViewData
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string LIST_RESULT = "list_result";
        public const string SEMESTER_NAME = "semester_name";
        public const string SEMESTER = "semester";
        public const string APPLY_LEVEL = "apply_level";
        public const string CBO_MARK_COLUMN = "cbo_mark_column";
        public const string PER_CHECK_BUTTON = "per_check_button";
        public const string IS_NEW_SCHOOL_MODEL = "IS_NEW_SCHOOL_MODEL";
        public const string CBO_MARK_COLUMN2 = "cbo_mark_column2";
        public const string IS_DATA_LOCK_KTGK = "IS_DATA_LOCK_KTGK";
        public const string IS_DATA_LOCK_KTCK = "IS_DATA_LOCK_KTCK";
        public const string VALUE_CHECK_VNEN = "VALUE_CHECK_VNEN";

        public const int MARK_TYPE_15P = 1;
        public const int MARK_TYPE_V = 2;
        public const int MARK_TYPE_HK = 3;
        public const int TRANSFER_CURRENT_SUBJECT = 1;
        public const int TRANSFER_CURRENT_EXAM_GROUP = 2;
        public const int TRANSFER_ALL = 3;

        public const int SubjectMath = 19;
        public const int SubjectTV = 8;

    }
}