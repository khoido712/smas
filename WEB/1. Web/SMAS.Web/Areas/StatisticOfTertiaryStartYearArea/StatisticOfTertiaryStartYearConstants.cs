﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea
{
    public class StatisticOfTertiaryStartYearConstants
    {
        public const string SEARCH = "SEARCH";
        public const string LIST = "LIST";
        public const string LIST_EMPLOYEE = "LIST_EMPLOYEE";
        public const string LIST_COMMITTEE = "LIST_COMMITTEE";
        public const string OLD_YEAR = "OLD_YEAR";
    }
}