﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea
{
    public class StatisticOfTertiaryStartYearAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticOfTertiaryStartYearArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticOfTertiaryStartYearArea_default",
                "StatisticOfTertiaryStartYearArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
