﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Models
{
    public class StatisticOfTertiaryStartYearModel
    {
        //Grid pupil
        public int EducationLevelID { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_EducationLevel")]
        public string EducationLevel { get; set; }       
        public Dictionary<string, object> dicSubComittee { get; set; }
        public int SubCommitteeID { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_SubjectNC")]
        public int SubjectNC { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_Subject")]
        public int Subject { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalClass")]
        public int TotalClass { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalPupil")]
        public int TotalPupil { get; set; }

        public string Error { get; set; }

    }
}