﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_pupilTN")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? pupilTN { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_pupilfree")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? pupilfree { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalPupil")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? TotalPupil { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalRoomMVT")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? TotalRoomMVT { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalMVT")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? TotalMVT { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalMVTNet")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? TotalMVTNet { get; set; }

        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_RoomHBM")]
        [RegularExpression(@"^[0-9]*", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public int? RoomHBM { get; set; }

        
        public bool? IsWebsite { get; set; }

       
        public bool? IsCSDL { get; set; }

       
        public string Error { get; set; }
    }
}