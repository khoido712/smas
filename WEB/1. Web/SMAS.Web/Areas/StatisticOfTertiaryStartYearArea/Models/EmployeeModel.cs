﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Models
{
    public class EmployeeModel
    {
        //Grid Employee
       
        [ResourceDisplayName("FirstHighSchool_Label_TeacherTitle")]
        public string TeacherTitle { get; set; }

        public int? CountTotal { get; set; }
        public int? OrderNumber { get; set; }
        public string Error { get; set; }
    }
}