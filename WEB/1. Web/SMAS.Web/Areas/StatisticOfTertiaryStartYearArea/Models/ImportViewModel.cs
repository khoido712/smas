﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Models
{
    public class ImportViewModel
    {
        [ResourceDisplayName("FirstHighSchool_Label_TeacherTitle")]
        public string TeacherTitle { get; set; }
        public int CountTotal { get; set; }
        public int? OrderNumber { get; set; }
        public int? pupilTN { get; set; }
        public int? pupilfree { get; set; }
        public int? TotalPupilCount { get; set; }
        public int? TotalRoomMVT { get; set; }
        public int? TotalMVT { get; set; }
        public int? TotalMVTNet { get; set; }
        public int? RoomHBM { get; set; }
        public bool? IsWebsite { get; set; }
        public bool? IsCSDL { get; set; }
        public int EducationLevelID { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_EducationLevel")]
        public string EducationLevel { get; set; }
        public Dictionary<string, object> dicSubComittee { get; set; }
        public int SubCommitteeID { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_SubjectNC")]
        public int SubjectNC { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_Subject")]
        public int Subject { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalClass")]
        public int TotalClass { get; set; }
        [ResourceDisplayName("StatisticOfTertiaryStartYear_Label_TotalPupil")]
        public int TotalPupil { get; set; }
        public string Error { get; set; }
    }
}