﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.StatisticOfTertiaryStartYearArea.Controllers
{
    public class StatisticOfTertiaryStartYearController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IStatisticOfTertiaryStartYearBusiness StatisticOfTertiaryStartYearBusiness;
        private readonly IStatisticTertiaryStartYearBusiness StatisticTertiaryStartYearBusiness;
        public StatisticOfTertiaryStartYearController(
            ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness
            , IDistrictBusiness DistrictBusiness
            , IProvinceBusiness ProvinceBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , ISubCommitteeBusiness SubCommitteeBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IStatisticOfTertiaryStartYearBusiness StatisticOfTertiaryStartYearBusiness
            , IStatisticTertiaryStartYearBusiness StatisticTertiaryStartYearBusiness
            )
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.StatisticOfTertiaryStartYearBusiness = StatisticOfTertiaryStartYearBusiness;
            this.StatisticTertiaryStartYearBusiness = StatisticTertiaryStartYearBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            //Gan nam hoc truoc
            AcademicYear Aca = AcademicYearBusiness.Find(Global.AcademicYearID);
            ViewData[StatisticOfTertiaryStartYearConstants.OLD_YEAR] = Aca.Year - 1;
            return View();
        }
        private void SetViewData()
        {
            ViewData[StatisticOfTertiaryStartYearConstants.SEARCH] = new SearchViewModel();
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;

            #region Load Grid pupil
            List<StatisticOfTertiaryStartYearModel> lstStatis = new List<StatisticOfTertiaryStartYearModel>();
            //Tong so lop
            IQueryable<ClassProfile> lstClassSchool = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            //Danh sach ban hoc
            List<int> lstSubCommittee = lstClassSchool.Where(o => o.SubCommitteeID != null).Select(o => o.SubCommitteeID.Value).Distinct().ToList();
            List<SubCommittee> lstCommitee = new List<SubCommittee>();
            foreach (int sc in lstSubCommittee)
            {
                SubCommittee Sub = SubCommitteeBusiness.Find(sc);
                if (Sub != null)
                {
                    lstCommitee.Add(Sub);
                }
            }
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_COMMITTEE] = lstCommitee.ToList();
            //Tong so hoc sinh
            IQueryable<PupilOfClass> lstPupilSchool = PupilOfClassBusiness.SearchBySchool(Global.SchoolID.Value, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED);
            //Danh sach khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == Global.AppliedLevel).ToList();
            StatisticOfTertiaryStartYearModel sty = new StatisticOfTertiaryStartYearModel();
            foreach (EducationLevel edu in lstEdu)
            {
                //Danh sach ban hoc theo khoi
                sty = new StatisticOfTertiaryStartYearModel();
                sty.EducationLevel = edu.Resolution;
                sty.EducationLevelID = edu.EducationLevelID;
                sty.dicSubComittee = new Dictionary<string, object>();
                foreach (int SubID in lstSubCommittee)
                {
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_class"] = lstClassSchool.Where(o => o.SubCommitteeID != null && o.EducationLevelID == edu.EducationLevelID && o.SubCommitteeID == SubID).Count();
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_pupil"] = lstPupilSchool.Where(o => o.ClassProfile.SubCommitteeID != null && o.ClassProfile.EducationLevelID == edu.EducationLevelID && o.ClassProfile.SubCommitteeID == SubID).Count();
                }
                //sty.lstSub = lstSubCommittee.Select(o=>o.Resolution).ToList();
                sty.TotalClass = lstClassSchool.Where(o => o.EducationLevelID == edu.EducationLevelID).Count();
                sty.TotalPupil = lstPupilSchool.Where(o => o.ClassProfile.EducationLevelID == edu.EducationLevelID).Count();
                lstStatis.Add(sty);
            }
            ViewData[StatisticOfTertiaryStartYearConstants.LIST] = lstStatis;
            #endregion


            #region Load Gird Employee
            List<EmployeeModel> lstEmployee = new List<EmployeeModel>();
            //danh sach giao vien cua truong
            List<Employee> lstEmployeeSchool = EmployeeBusiness.SearchTeacher(Global.SchoolID.Value, dic).Where(o => o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).ToList();
            List<int> lstEmployeeID = lstEmployeeSchool.Select(o => o.EmployeeID).ToList();
            //Giao vien trong bien che
            EmployeeModel em = new EmployeeModel();
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStaff");
            int ContracttypeID = ContractTypeBusiness.All.Where(o => o.Resolution.ToUpper().Trim() == em.TeacherTitle.ToUpper().Trim()).FirstOrDefault().ContractTypeID;
            em.CountTotal = lstEmployeeSchool.Where(o => o.ContractTypeID == ContracttypeID).Count();
            lstEmployee.Add(em);
            //Giao vien theo phan cong giang day
            //Danh sach mon duoc phan cong
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            var a = (from g1 in TeachingAssignmentBusiness.All.Where(o => lstEmployeeID.Contains(o.TeacherID) && o.SubjectCat.AppliedLevel == Global.AppliedLevel && o.Last2digitNumberSchool == partitionId)
                     join sub in SubjectCatBusiness.All
                     on g1.SubjectID equals sub.SubjectCatID
                     group g1 by new { g1.SubjectID, sub.SubjectName, sub.OrderInSubject } into g
                     select new EmployeeModel
             {
                 TeacherTitle = g.Key.SubjectName,
                 CountTotal = g.Count(),
                 OrderNumber = g.Key.OrderInSubject
             }).OrderBy(o => o.OrderNumber);

            lstEmployee = lstEmployee.Union(a.ToList()).ToList();

            //Danh sach giao vien dat chuan,tren chuan,duoi chuan
            //Tren chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfOverStandard");
            em.CountTotal = lstEmployeeSchool.Where(o => o.TrainingLevelID > 4).Count();
            lstEmployee.Add(em);
            //Dat chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStandard");
            em.CountTotal = lstEmployeeSchool.Where(o => o.TrainingLevelID == 4).Count();
            lstEmployee.Add(em);
            //Duoi chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfUnderStandard");
            em.CountTotal = lstEmployeeSchool.Where(o => o.TrainingLevelID < 4).Count();
            lstEmployee.Add(em);

            //tong so
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiaryStartYear_Label_TotalPupil");
            em.CountTotal = lstEmployeeSchool.Count;
            lstEmployee.Add(em);
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_EMPLOYEE] = lstEmployee;

            #endregion

        }
        public JsonResult GetReportBySemester()
        {
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();
            return Json(new JsonReportMessage(processedReport, type));
        }
        public FileResult GetNewReport(FormCollection frm)
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            #region Lay du lieu
            //***Thong tin tuyen sinh vao lop 10
            //-Từ lớp 9 TN năm 2012
            //-Từ thí sinh tự do
            //-Tổng số
            int PupilTN = 0;
            int pupilFree = 0;
            int TotalPupil = pupilFree + PupilTN;
            if (frm["pupilTN"] != "" && frm["pupilTN"] != null)
            {
                PupilTN = int.Parse(frm["pupilTN"]);
            }
            if (frm["pupilfree"] != "" && frm["pupilfree"] != null)
            {
                pupilFree = int.Parse(frm["pupilfree"]);
            }
            //***Thong tin co so vat chat thiet bi tin hoc
            //Tổng số phòng MVT
            //Tổng số MVT
            //Tổng số MVT kết nối internet  
            //Có website
            //Có CSDL
            //Phòng học bộ môn
            int RoomMVT = 0;
            int TotalMVT = 0;
            int TotalMVTNet = 0;
            bool IsWebsite = false;
            bool IsCSDL = false;
            int RoomHBM = 0;
            if (frm["TotalRoomMVT"] != "" && frm["TotalRoomMVT"] != null)
            {
                RoomMVT = int.Parse(frm["TotalRoomMVT"]);
            }
            if (frm["TotalMVT"] != "" && frm["TotalMVT"] != null)
            {
                TotalMVT = int.Parse(frm["TotalMVT"]);
            }
            if (frm["TotalMVTNet"] != "" && frm["TotalMVTNet"] != null)
            {
                TotalMVTNet = int.Parse(frm["TotalMVTNet"]);
            }
            if (frm["IsWebsite"] != "" && frm["IsWebsite"] != null)
            {
                IsWebsite = bool.Parse(frm["IsWebsite"]);
            }
            if (frm["IsCSDL"] != "" && frm["IsCSDL"] != null)
            {
                IsCSDL = bool.Parse(frm["IsCSDL"]);
            }
            if (frm["RoomHBM"] != "" && frm["RoomHBM"] != null)
            {
                RoomHBM = int.Parse(frm["RoomHBM"]);
            }
            //*** Tinh hinh hoc sinh
            List<StatisticOfTertiaryStartYearModel> lstStatis = new List<StatisticOfTertiaryStartYearModel>();
            //Tong so lop
            IQueryable<ClassProfile> lstClassSchool = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            //Danh sach ban hoc
            List<int> lstSubCommittee = lstClassSchool.Where(o => o.SubCommitteeID != null).Select(o => o.SubCommitteeID.Value).Distinct().ToList();
            List<SubCommittee> lstCommitee = new List<SubCommittee>();
            foreach (int sc in lstSubCommittee)
            {
                SubCommittee Sub = SubCommitteeBusiness.Find(sc);
                if (Sub != null)
                {
                    lstCommitee.Add(Sub);
                }
            }
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_COMMITTEE] = lstCommitee.ToList();
            //Tong so hoc sinh
            IQueryable<PupilOfClass> lstPupilSchool = PupilOfClassBusiness.SearchBySchool(Global.SchoolID.Value, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED);
            //Danh sach khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == Global.AppliedLevel).ToList();
            StatisticOfTertiaryStartYearModel sty = new StatisticOfTertiaryStartYearModel();
            foreach (EducationLevel edu in lstEdu)
            {
                //Danh sach ban hoc theo khoi
                sty = new StatisticOfTertiaryStartYearModel();
                sty.EducationLevel = edu.Resolution;
                sty.EducationLevelID = edu.EducationLevelID;
                sty.dicSubComittee = new Dictionary<string, object>();
                foreach (int SubID in lstSubCommittee)
                {
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_class"] = lstClassSchool.Where(o => o.SubCommitteeID != null && o.EducationLevelID == edu.EducationLevelID && o.SubCommitteeID == SubID).Count();
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_pupil"] = lstPupilSchool.Where(o => o.ClassProfile.SubCommitteeID != null && o.ClassProfile.EducationLevelID == edu.EducationLevelID && o.ClassProfile.SubCommitteeID == SubID).Count();
                }
                //sty.lstSub = lstSubCommittee.Select(o=>o.Resolution).ToList();
                sty.TotalClass = lstClassSchool.Where(o => o.EducationLevelID == edu.EducationLevelID).Count();
                sty.TotalPupil = lstPupilSchool.Where(o => o.ClassProfile.EducationLevelID == edu.EducationLevelID).Count();
                lstStatis.Add(sty);
            }
            //*** Co cau giao vien
            string[] lsCountEmployee = frm["CountEmployee"].Split(new Char[] { ',' });
            List<EmployeeModel> lstEmployee = new List<EmployeeModel>();
            //danh sach giao vien cua truong
            List<Employee> lstEmployeeSchool = EmployeeBusiness.SearchTeacher(Global.SchoolID.Value, dic).Where(o => o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).ToList();
            List<int> lstEmployeeID = lstEmployeeSchool.Select(o => o.EmployeeID).ToList();
            //Giao vien trong bien che
            EmployeeModel em = new EmployeeModel();
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStaff");
            em.CountTotal = 0;
            if (lsCountEmployee[0] != null && lsCountEmployee[0] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[0]);
            }
            lstEmployee.Add(em);
            //Giao vien phan cong giang day theo mon
            //Danh sach mon duoc phan cong
            List<string> lstSubjet = TeachingAssignmentBusiness.All.Where(o => lstEmployeeID.Contains(o.TeacherID) && o.SubjectCat.AppliedLevel == Global.AppliedLevel && o.Last2digitNumberSchool == partitionId).OrderBy(o => o.SubjectCat.OrderInSubject).Select(o => o.SubjectCat.SubjectName).Distinct().ToList();
            int i = 1;
            foreach (string Sub in lstSubjet)
            {
                em = new EmployeeModel();
                em.TeacherTitle = Sub;
                em.CountTotal = 0;
                if (lsCountEmployee[i] != null && lsCountEmployee[i] != "")
                {
                    em.CountTotal = int.Parse(lsCountEmployee[i]);
                }
                i++;
            }
            //Danh sach giao vien dat chuan,tren chuan,duoi chuan
            //Tren chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfOverStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i] != null && lsCountEmployee[i] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i]);
            }
            lstEmployee.Add(em);
            //Dat chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i + 1] != null && lsCountEmployee[i + 1] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i + 1]);
            }
            lstEmployee.Add(em);
            //Duoi chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfUnderStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i + 2] != null && lsCountEmployee[i + 2] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i + 2]);
            }
            lstEmployee.Add(em);
            #endregion

            #region ExportExcel


            //*** Export Excel
            Stream excel = this.CreateStatisticOfTertiaryStartYear(lstEdu, lstSubjet, lstClassSchool,
            lstPupilSchool, lsCountEmployee, PupilTN, pupilFree, TotalPupil, RoomMVT, TotalMVT, TotalMVTNet
            , IsWebsite, IsCSDL, RoomHBM);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "Bao cao tong hop THPT dau nam.xls";
            result.FileDownloadName = ReportName;
            return result;
            #endregion

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendData(FormCollection frm)
        {
            GlobalInfo Global = new GlobalInfo();
            #region Luu lai file excel
            ProcessedReport processedReport = null;
            StatisticOfTertiaryStartYearBO entity = new StatisticOfTertiaryStartYearBO();
            entity.SchoolID = Global.SchoolID.Value;
            entity.AcademicYearID = Global.AcademicYearID.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            //***Thong tin tuyen sinh vao lop 10
            //-Từ lớp 9 TN năm 2012
            //-Từ thí sinh tự do
            //-Tổng số
            int PupilTN = 0;
            int pupilFree = 0;
            int TotalPupil = pupilFree + PupilTN;
            if (frm["pupilTN"] != "" && frm["pupilTN"] != null)
            {
                PupilTN = int.Parse(frm["pupilTN"]);
            }
            if (frm["pupilfree"] != "" && frm["pupilfree"] != null)
            {
                pupilFree = int.Parse(frm["pupilfree"]);
            }
            //***Thong tin co so vat chat thiet bi tin hoc
            //Tổng số phòng MVT
            //Tổng số MVT
            //Tổng số MVT kết nối internet  
            //Có website
            //Có CSDL
            //Phòng học bộ môn
            int RoomMVT = 0;
            int TotalMVT = 0;
            int TotalMVTNet = 0;
            bool IsWebsite = false;
            bool IsCSDL = false;
            int RoomHBM = 0;
            if (frm["TotalRoomMVT"] != "" && frm["TotalRoomMVT"] != null)
            {
                RoomMVT = int.Parse(frm["TotalRoomMVT"]);
            }
            if (frm["TotalMVT"] != "" && frm["TotalMVT"] != null)
            {
                TotalMVT = int.Parse(frm["TotalMVT"]);
            }
            if (frm["TotalMVTNet"] != "" && frm["TotalMVTNet"] != null)
            {
                TotalMVTNet = int.Parse(frm["TotalMVTNet"]);
            }
            if (frm["IsWebsite"] != "" && frm["IsWebsite"] != null)
            {
                IsWebsite = bool.Parse(frm["IsWebsite"]);
            }
            if (frm["IsCSDL"] != "" && frm["IsCSDL"] != null)
            {
                IsCSDL = bool.Parse(frm["IsCSDL"]);
            }
            if (frm["RoomHBM"] != "" && frm["RoomHBM"] != null)
            {
                RoomHBM = int.Parse(frm["RoomHBM"]);
            }
            //*** Tinh hinh hoc sinh
            List<StatisticOfTertiaryStartYearModel> lstStatis = new List<StatisticOfTertiaryStartYearModel>();
            //Tong so lop
            IQueryable<ClassProfile> lstClassSchool = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            //Danh sach ban hoc
            List<int> lstSubCommittee = lstClassSchool.Where(o => o.SubCommitteeID != null).Select(o => o.SubCommitteeID.Value).Distinct().ToList();
            List<SubCommittee> lstCommitee = new List<SubCommittee>();
            foreach (int sc in lstSubCommittee)
            {
                SubCommittee Sub = SubCommitteeBusiness.Find(sc);
                if (Sub != null)
                {
                    lstCommitee.Add(Sub);
                }
            }
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_COMMITTEE] = lstCommitee.ToList();
            //Tong so hoc sinh
            IQueryable<PupilOfClass> lstPupilSchool = PupilOfClassBusiness.SearchBySchool(Global.SchoolID.Value, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED);
            //Danh sach khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == Global.AppliedLevel).ToList();
            StatisticOfTertiaryStartYearModel sty = new StatisticOfTertiaryStartYearModel();
            foreach (EducationLevel edu in lstEdu)
            {
                //Danh sach ban hoc theo khoi
                sty = new StatisticOfTertiaryStartYearModel();
                sty.EducationLevel = edu.Resolution;
                sty.EducationLevelID = edu.EducationLevelID;
                sty.dicSubComittee = new Dictionary<string, object>();
                foreach (int SubID in lstSubCommittee)
                {
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_class"] = lstClassSchool.Where(o => o.SubCommitteeID != null && o.EducationLevelID == edu.EducationLevelID && o.SubCommitteeID == SubID).Count();
                    sty.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_pupil"] = lstPupilSchool.Where(o => o.ClassProfile.SubCommitteeID != null && o.ClassProfile.EducationLevelID == edu.EducationLevelID && o.ClassProfile.SubCommitteeID == SubID).Count();
                }
                //sty.lstSub = lstSubCommittee.Select(o=>o.Resolution).ToList();
                sty.TotalClass = lstClassSchool.Where(o => o.EducationLevelID == edu.EducationLevelID).Count();
                sty.TotalPupil = lstPupilSchool.Where(o => o.ClassProfile.EducationLevelID == edu.EducationLevelID).Count();
                lstStatis.Add(sty);
            }
            //*** Co cau giao vien
            string[] lsCountEmployee = frm["CountEmployee"].Split(new Char[] { ',' });
            List<EmployeeModel> lstEmployee = new List<EmployeeModel>();
            //danh sach giao vien cua truong
            List<Employee> lstEmployeeSchool = EmployeeBusiness.SearchTeacher(Global.SchoolID.Value, dic).Where(o => o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).ToList();
            List<int> lstEmployeeID = lstEmployeeSchool.Select(o => o.EmployeeID).ToList();
            //Giao vien trong bien che
            EmployeeModel em = new EmployeeModel();
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStaff");
            em.CountTotal = 0;
            if (lsCountEmployee[0] != null && lsCountEmployee[0] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[0]);
            }
            lstEmployee.Add(em);
            //Giao vien phan cong giang day theo mon
            //Danh sach mon duoc phan cong
            List<string> lstSubjet = TeachingAssignmentBusiness.All.Where(o => lstEmployeeID.Contains(o.TeacherID) && o.SubjectCat.AppliedLevel == Global.AppliedLevel && o.Last2digitNumberSchool == partitionId).OrderBy(o => o.SubjectCat.OrderInSubject).Select(o => o.SubjectCat.SubjectName).Distinct().ToList();
            int i = 1;
            foreach (string Sub in lstSubjet)
            {
                em = new EmployeeModel();
                em.TeacherTitle = Sub;
                em.CountTotal = 0;
                if (lsCountEmployee[i] != null && lsCountEmployee[i] != "")
                {
                    em.CountTotal = int.Parse(lsCountEmployee[i]);
                }
                i++;
            }
            //Danh sach giao vien dat chuan,tren chuan,duoi chuan
            //Tren chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfOverStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i] != null && lsCountEmployee[i] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i]);
            }
            lstEmployee.Add(em);
            //Dat chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i + 1] != null && lsCountEmployee[i + 1] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i + 1]);
            }
            lstEmployee.Add(em);
            //Duoi chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfUnderStandard");
            em.CountTotal = 0;
            if (lsCountEmployee[i + 2] != null && lsCountEmployee[i + 2] != "")
            {
                em.CountTotal = int.Parse(lsCountEmployee[i + 2]);
            }
            lstEmployee.Add(em);

            Stream excel = this.CreateStatisticOfTertiaryStartYear(lstEdu, lstSubjet, lstClassSchool,
            lstPupilSchool, lsCountEmployee, PupilTN, pupilFree, TotalPupil, RoomMVT, TotalMVT, TotalMVTNet
            , IsWebsite, IsCSDL, RoomHBM);

            processedReport = this.StatisticOfTertiaryStartYearBusiness.InsertStatisticOfTertiaryStartYear(entity, excel);
            #endregion

            #region Luu lai du lieu bao cao cua truong
            IQueryable<StatisticTertiaryStartYear> lstStartYear = StatisticTertiaryStartYearBusiness.All.Where(o => o.SchoolID == Global.SchoolID);
            if (lstStartYear.Count() > 0)
            {
                StatisticTertiaryStartYear stsy = lstStartYear.FirstOrDefault();
                stsy.ReportDate = DateTime.Now;
                StatisticTertiaryStartYearBusiness.Update(stsy);
                StatisticTertiaryStartYearBusiness.Save();
            }
            else
            {
                StatisticTertiaryStartYear stsy = new StatisticTertiaryStartYear();
                stsy.ReportDate = DateTime.Now;
                stsy.ReportStatus = true;
                stsy.SchoolID = Global.SchoolID.Value;
                stsy.SupervisingDeptID = Global.SupervisingDeptID.Value;
                StatisticTertiaryStartYearBusiness.Insert(stsy);
                StatisticTertiaryStartYearBusiness.Save();
            }

            #endregion

            return Json(new JsonMessage(Res.Get("Common_Label_SendataSuccessMessage")));
        }

        //Ham Tao file Excel
        private Stream CreateStatisticOfTertiaryStartYear(List<EducationLevel> lstEdu, List<string> lstSubjet, IQueryable<ClassProfile> lstClassSchool,
            IQueryable<PupilOfClass> lstPupilSchool, string[] lsCountEmployee, int PupilTN, int pupilFree, int TotalPupil, int RoomMVT, int TotalMVT, int TotalMVTNet
            , bool IsWebsite, bool IsCSDL, int RoomHBM)
        {
            GlobalInfo Global = new GlobalInfo();
            //*** Export Excel
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "Mau_2_THPT_Mau truong.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill du lieu 
            SchoolProfile school = SchoolProfileBusiness.Find(Global.SchoolID.Value);
            AcademicYear acaYear = AcademicYearBusiness.Find(Global.AcademicYearID.Value);
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["AcademicYear"] = acaYear.DisplayTitle;
            dicVarable["LastAcademicYear"] = acaYear.Year - 1;
            sheet.FillVariableValue(dicVarable);
            //Nguon tuyen sinh lop 10
            sheet.SetCellValue(10, 1, PupilTN);
            sheet.SetCellValue(10, 2, pupilFree);
            sheet.SetCellValue(10, 3, TotalPupil);
            //Co so vat chat thiet bi tin hoc
            sheet.SetCellValue(15, 1, RoomMVT);
            sheet.SetCellValue(15, 2, TotalMVT);
            sheet.SetCellValue(15, 3, TotalMVTNet);
            sheet.SetCellValue(15, 4, Utils.CommonConvert.ToYesNo(IsWebsite));
            sheet.SetCellValue(15, 5, Utils.CommonConvert.ToYesNo(IsCSDL));
            sheet.SetCellValue(15, 6, RoomHBM);
            //Tinh hinh hoc sinh
            int StartRowpp = 22;
            foreach (EducationLevel el in lstEdu)
            {
                sheet.SetCellValue(StartRowpp, 1, el.Resolution);
                //danh sach hoc sinh va danh sach lop theo khoi
                List<ClassProfile> lstClassEdu = lstClassSchool.Where(o => o.SubCommitteeID != null && o.EducationLevelID == el.EducationLevelID).ToList();
                List<PupilOfClass> lstPupilEdu = lstPupilSchool.Where(o => o.ClassProfile.SubCommitteeID != null && o.ClassProfile.EducationLevelID == el.EducationLevelID).ToList();
                //Ban KHTN
                sheet.SetCellValue(StartRowpp, 4, lstClassEdu.Where(o => o.SubCommitteeID == 1).Count());
                sheet.SetCellValue(StartRowpp, 5, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 1).Count());
                //Ban KHXH-NV
                sheet.SetCellValue(StartRowpp, 6, lstClassEdu.Where(o => o.SubCommitteeID == 2).Count());
                sheet.SetCellValue(StartRowpp, 7, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 2).Count());
                //Ban co ban
                sheet.SetCellValue(StartRowpp, 10, lstClassEdu.Where(o => o.SubCommitteeID == 4).Count());
                sheet.SetCellValue(StartRowpp, 11, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 4).Count());
                sheet.SetCellValue(StartRowpp, 12, lstClassEdu.Where(o => o.SubCommitteeID == 5).Count());
                sheet.SetCellValue(StartRowpp, 13, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 5).Count());
                sheet.SetCellValue(StartRowpp, 14, lstClassEdu.Where(o => o.SubCommitteeID == 6).Count());
                sheet.SetCellValue(StartRowpp, 15, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 6).Count());
                sheet.SetCellValue(StartRowpp, 16, lstClassEdu.Where(o => o.SubCommitteeID == 7).Count());
                sheet.SetCellValue(StartRowpp, 17, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 7).Count());
                sheet.SetCellValue(StartRowpp, 18, lstClassEdu.Where(o => o.SubCommitteeID == 8).Count());
                sheet.SetCellValue(StartRowpp, 19, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 8).Count());
                sheet.SetCellValue(StartRowpp, 20, lstClassEdu.Where(o => o.SubCommitteeID == 9).Count());
                sheet.SetCellValue(StartRowpp, 21, lstPupilEdu.Where(o => o.ClassProfile.SubCommitteeID == 9).Count());
                StartRowpp++;
            }

            //Co cau giao vien
            sheet.SetCellValue(31, 2, lsCountEmployee[0]);
            int startcol = 4;
            int startSub = StartRowpp + 5;
            int su = 1;
            foreach (string sc in lstSubjet)
            {
                sheet.CopyPaste(sheet.GetRange(29, 4, 29, 4), 29, startcol + 1);
                sheet.CopyPaste(sheet.GetRange(30, 4, 31, 4), 30, startcol + 1, true);
                sheet.SetCellValue(30, startcol, sc);
                int sl = 0;
                if (lsCountEmployee[su] != null && lsCountEmployee[su] != "")
                {
                    sl = int.Parse(lsCountEmployee[su]);
                }
                sheet.SetCellValue(31, startcol, sl);
                startcol++;
                su++;
            }
            sheet.GetRange(29, 4, 29, 4 + lstSubjet.Count - 1).Merge();
            sheet.GetRange(30, 4, 29, 4 + lstSubjet.Count - 1).AutoFit();
            sheet.CopyPaste(sheet.GetRange(33, 1, 35, 3), 29, 4 + lstSubjet.Count);
            sheet.CopyPaste(sheet.GetRange(36, 1, 38, 3), 33, 1);
            //Giao vien dat chuan,tren chuan,duoi chuan
            sheet.SetCellValue(31, startcol, lsCountEmployee[su]);
            sheet.SetCellValue(31, startcol + 1, lsCountEmployee[su + 1]);
            sheet.SetCellValue(31, startcol + 2, lsCountEmployee[su + 2]);

            Stream excel = oBook.ToStream();
            return excel;
        }

        
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments)
        {
            try
            {
                //Validate File
                HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;
                if (file == null)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), JsonMessage.ERROR));
                    return res;
                }
                if (!file.FileName.EndsWith(".xls"))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExtensionError"), JsonMessage.ERROR));
                    return res;
                }
                if (file.ContentLength > 1024000)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
                    return res;
                }

                //Import
                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                IVTWorksheet sheet = book.GetSheet(1);

                SearchViewModel lstSearchImported = SearchReadExcelFile(sheet);
                List<StatisticOfTertiaryStartYearModel> lstPupil = PupilReadExcel(sheet);
                List<EmployeeModel> lstEmployee = EmployeeReadExcel(sheet);
                Session["StatisticOfTertiaryStartYear_Search"] = lstSearchImported;
                Session["StatisticOfTertiaryStartYear_Pupil"] = lstPupil;
                Session["StatisticOfTertiaryStartYear_Employee"] = lstEmployee;
                JsonResult resx = Json(new JsonMessage(Res.Get("ImportPupil_Label_ImportSuccess"), JsonMessage.SUCCESS), "text/html");
                return resx;
            }
            catch (Exception ex)
            {
                JsonResult res = Json(new JsonMessage(Res.Get(ex.Message), JsonMessage.ERROR), "text/html");
                return res;
            }
        }

        public PartialViewResult LoadGridImport()
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            List<StatisticOfTertiaryStartYearModel> lstStatis = new List<StatisticOfTertiaryStartYearModel>();
            //Tong so lop
            IQueryable<ClassProfile> lstClassSchool = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            AcademicYear Aca = AcademicYearBusiness.Find(Global.AcademicYearID);
            //Danh sach ban hoc
            List<int> lstSubCommittee = lstClassSchool.Where(o => o.SubCommitteeID != null).Select(o => o.SubCommitteeID.Value).Distinct().ToList();
            List<SubCommittee> lstCommitee = new List<SubCommittee>();
            foreach (int sc in lstSubCommittee)
            {
                SubCommittee Sub = SubCommitteeBusiness.Find(sc);
                if (Sub != null)
                {
                    lstCommitee.Add(Sub);
                }
            }
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_COMMITTEE] = lstCommitee.ToList();
            ViewData[StatisticOfTertiaryStartYearConstants.OLD_YEAR] = Aca.Year - 1;
            ViewData[StatisticOfTertiaryStartYearConstants.SEARCH] = (SearchViewModel)Session["StatisticOfTertiaryStartYear_Search"];
            ViewData[StatisticOfTertiaryStartYearConstants.LIST] = (List<StatisticOfTertiaryStartYearModel>)Session["StatisticOfTertiaryStartYear_Pupil"];
            ViewData[StatisticOfTertiaryStartYearConstants.LIST_EMPLOYEE] = (List<EmployeeModel>)Session["StatisticOfTertiaryStartYear_Employee"];
            return PartialView("_Total");
        }

        private SearchViewModel SearchReadExcelFile(IVTWorksheet sheet)
        {
            SearchViewModel ivm = new SearchViewModel();
            ivm.Error = "";
            //Khai bao cac bien thu tu

            //***Nguon tuyen sinh khoi 10
            try
            {
                ivm.pupilTN = int.Parse(GetCellValue(sheet, "A10"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_pupilTN"));
            }
            try
            {
                ivm.pupilfree = int.Parse(GetCellValue(sheet, "B10"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_pupilfree"));
            }

            //** Co so vat chat thiet bi tin hoc
            try
            {
                ivm.TotalRoomMVT = int.Parse(GetCellValue(sheet, "A15"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_TotalRoomMVT"));
            }
            try
            {
                ivm.TotalMVT = int.Parse(GetCellValue(sheet, "B15"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_TotalMVT"));
            }
            try
            {
                ivm.TotalMVTNet = int.Parse(GetCellValue(sheet, "C15"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_TotalMVTNet"));
            }
            try
            {
                ivm.RoomHBM = int.Parse(GetCellValue(sheet, "F15"));
            }
            catch
            {
                ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_ValidateNumber"), Res.Get("StatisticOfTertiaryStartYear_Label_RoomHBM"));
            }
            string IsWebsite = GetCellValue(sheet, "D15");
            if (IsWebsite.ToLower() == "có" || IsWebsite.ToLower() == "co")
            {
                ivm.IsWebsite = true;
            }
            else
            {
                if (IsWebsite.ToLower() == "khong" || IsWebsite.ToLower() == "không")
                {
                    ivm.IsWebsite = false;
                }
                else
                {
                    ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate"), Res.Get("StatisticOfTertiaryStartYear_Label_IsWebsite"));
                }
            }
            string IsCSDL = GetCellValue(sheet, "E15");
            if (IsCSDL.ToLower() == "có" || IsCSDL.ToLower() == "co")
            {
                ivm.IsCSDL = true;
            }
            else
            {
                if (IsCSDL.ToLower() == "khong" || IsCSDL.ToLower() == "không")
                {
                    ivm.IsCSDL = false;
                }
                else
                {
                    ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate"), Res.Get("StatisticOfTertiaryStartYear_Label_IsCSDL"));
                }
            }
            return ivm;
        }
        private List<StatisticOfTertiaryStartYearModel> PupilReadExcel(IVTWorksheet sheet)
        {

            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            List<StatisticOfTertiaryStartYearModel> listImport = new List<StatisticOfTertiaryStartYearModel>();
            StatisticOfTertiaryStartYearModel ivm = new StatisticOfTertiaryStartYearModel();
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == Global.AppliedLevel).ToList();
            //Danh sach ban hoc
            IQueryable<ClassProfile> lstClassSchool = ClassProfileBusiness.SearchBySchool(Global.SchoolID.Value, dic);
            List<int> lstSubCommittee = lstClassSchool.Where(o => o.SubCommitteeID != null).Select(o => o.SubCommitteeID.Value).Distinct().ToList();
            List<SubCommittee> lstCommitee = new List<SubCommittee>();
            foreach (int sc in lstSubCommittee)
            {
                SubCommittee Sub = SubCommitteeBusiness.Find(sc);
                if (Sub != null)
                {
                    lstCommitee.Add(Sub);
                }
            }
            //*** tinh hinh hoc sinh
            int StartEdu = 22;
            foreach (EducationLevel edu in lstEdu)
            {
                //Danh sach ban hoc theo khoi
                ivm = new StatisticOfTertiaryStartYearModel();
                ivm.EducationLevel = edu.Resolution;
                ivm.EducationLevelID = edu.EducationLevelID;
                ivm.dicSubComittee = new Dictionary<string, object>();
                int startcol = 4;
                foreach (int SubID in lstSubCommittee)
                {
                    SubCommittee Sub = SubCommitteeBusiness.Find(SubID);
                    try
                    {
                        ivm.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_class"] = int.Parse(GetCellValueIndex(sheet, StartEdu, startcol));
                    }
                    catch
                    {
                        ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Pupil_Subconmittee"), Sub.Resolution);
                    }
                    startcol++;
                    try
                    {
                        ivm.dicSubComittee[edu.EducationLevelID.ToString() + "-" + SubID.ToString() + "_pupil"] = int.Parse(GetCellValueIndex(sheet, StartEdu, startcol));
                    }
                    catch
                    {
                        ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Pupil_Subconmittee"), Sub.Resolution);
                    }
                    startcol++;
                }
                //sty.lstSub = lstSubCommittee.Select(o=>o.Resolution).ToList();
                try
                {
                    ivm.TotalClass = int.Parse(GetCellValueIndex(sheet, StartEdu, 2));
                    startcol++;
                }
                catch
                {
                    ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_TotalClass"), edu.Resolution);
                    startcol++;
                }
                try
                {
                    ivm.TotalClass = int.Parse(GetCellValueIndex(sheet, StartEdu, 3));
                    startcol++;
                }
                catch
                {
                    ivm.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_TotalPupil"), edu.Resolution);
                    startcol++;
                }
                listImport.Add(ivm);
                StartEdu++;
            }
            return listImport;
        }
        private List<EmployeeModel> EmployeeReadExcel(IVTWorksheet sheet)
        {
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Global.SchoolID;
            dic["AcademicYearID"] = Global.AcademicYearID;
            int partitionId = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Danh sach giao vien cua truong
            List<Employee> lstEmployeeSchool = EmployeeBusiness.SearchTeacher(Global.SchoolID.Value, dic).Where(o => o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).ToList();
            List<int> lstEmployeeID = lstEmployeeSchool.Select(o => o.EmployeeID).ToList();


            List<EmployeeModel> lstEmployee = new List<EmployeeModel>();
            EmployeeModel em = new EmployeeModel();
            //Giao vien trong bien che
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStaff");
            try
            {
                em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, 2));
            }
            catch
            {
                em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
            };
            lstEmployee.Add(em);
            //Giao vien theo phan cong giang day
            //Danh sach mon duoc phan cong
            List<SubjectCat> lstSubject = TeachingAssignmentBusiness.All.Where(o => lstEmployeeID.Contains(o.TeacherID) && o.SubjectCat.AppliedLevel == Global.AppliedLevel && o.Last2digitNumberSchool == partitionId).Select(o => o.SubjectCat).OrderBy(o => o.OrderInSubject).ToList();
            int startcol = 3;
            foreach (SubjectCat sc in lstSubject)
            {
                em = new EmployeeModel();
                em.TeacherTitle = sc.SubjectName;
                try
                {
                    em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, startcol));
                }
                catch
                {
                    em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
                };
                lstEmployee.Add(em);
                startcol++;
            }

            //Danh sach giao vien dat chuan,tren chuan,duoi chuan
            //Tren chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfOverStandard");
            try
            {
                em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, startcol));
            }
            catch
            {
                em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
            };
            lstEmployee.Add(em);
            startcol++;
            //Dat chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfStandard");
            try
            {
                em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, startcol));
            }
            catch
            {
                em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
            };
            lstEmployee.Add(em);
            startcol++;
            //Duoi chuan
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiary_Label_NumOfUnderStandard");
            try
            {
                em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, startcol));
            }
            catch
            {
                em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
            };
            lstEmployee.Add(em);
            startcol++;
            //tong so
            em = new EmployeeModel();
            em.TeacherTitle = Res.Get("StatisticOfTertiaryStartYear_Label_TotalPupil");
            try
            {
                em.CountTotal = int.Parse(GetCellValueIndex(sheet, 31, startcol));
            }
            catch
            {
                em.Error += string.Format(Res.Get("StatisticTertiaryStartYear_Import_Validate_Employee"), em.TeacherTitle);
            };
            lstEmployee.Add(em);
            return lstEmployee;
        }

        private string GetCellValue(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
        private string GetCellValueIndex(IVTWorksheet sheet, int Row, int Col)
        {
            object o = sheet.GetRange(Row, Col, Row, Col).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
    }
}
