﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportEvaluationCommentsArea
{
    public class ReportEvaluationCommentsConstants
    {
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
    }
}