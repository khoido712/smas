﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEvaluationCommentsArea.Models
{
    public class ReportViewModel
    {
        [ResourceDisplayName("ReportEvaluationComments_Label_EducationLevelSearch")]
        [DataType("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportEvaluationCommentsConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public int EducationLevel { get; set; }

        [ResourceDisplayName("ReportEvaluationComments_Label_ClassSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_Required_ReportEvaluation_Class")]
        [AdditionalMetadata("ViewDataKey", ReportEvaluationCommentsConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int Class { get; set; }

        [ResourceDisplayName("ReportEvaluationComments_Label_SubjectSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_Required_ReportEvaluation_Subject")]
        [AdditionalMetadata("ViewDataKey", ReportEvaluationCommentsConstants.LIST_SUBJECT)]
        [AdditionalMetadata("Placeholder", "0")]
        public int Subject { get; set; }
    }
}