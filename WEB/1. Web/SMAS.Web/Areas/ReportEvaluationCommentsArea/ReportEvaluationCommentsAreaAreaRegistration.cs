﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportEvaluationCommentsArea
{
    public class ReportEvaluationCommentsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportEvaluationCommentsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportEvaluationCommentsArea_default",
                "ReportEvaluationCommentsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
