﻿using Ionic.Zip;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportAbsencePupilForSupArea.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea.Controllers
{
    public class ReportAbsencePupilForSupController : BaseController
    {
        //
        // GET: /ReportAbsencePupilForSupArea/ReportAbsencePupilForSup/

        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        // Thống kê học sinh nghỉ học
        // Mark_Level_37 : tổng hs cấp 1, nếu không check chọn hoặc trường không có cấp 1 thì là -1
        // Mark_Level_38 : tổng hs cấp 2 nếu không check chọn hoặc trường không có cấp 2 thì là -1
        // Mark_Level_39 : tổng hs cấp 3 nếu không check chọn hoặc trường không có cấp 3 thì là -1
        // Mark_Level_40 : tổng hs cấp MN nếu không check chọn hoặc trường không có cấp MN thì là -1
        // Mark_Level_01 : tổng hs nghỉ học cấp 1 
        // Mark_Level_02 : tổng hs nghỉ học cấp 2 
        // Mark_Level_03 : tổng hs nghỉ học cấp 3 
        // Mark_Level_04 : tổng hs nghỉ học cấp MN 


        // Danh sách hoc sinh nghỉ học
        // Mark_Level_36 : tổng hs cấp 1, nếu không check chọn hoặc trường không có cấp 1 thì là -1
        // Mark_Level_37 : tổng hs cấp 2 nếu không check chọn hoặc trường không có cấp 2 thì là -1
        // Mark_Level_38 : tổng hs cấp 3 nếu không check chọn hoặc trường không có cấp 3 thì là -1
        // Mark_Level_39 : tổng hs cấp NT nếu không check chọn hoặc trường không có cấp NT thì là -1
        // Mark_Level_40 : tổng hs cấp MG, nếu không check chọn hoặc trường không có cấp MG thì là -1
        // Mark_Level_01 : tổng hs nghỉ học cấp 1 
        // Mark_Level_02 : tổng hs nghỉ học cấp 2 
        // Mark_Level_03 : tổng hs nghỉ học cấp 3 
        // Mark_Level_04 : tổng hs nghỉ học cấp NT
        // Mark_Level_05 : tổng hs nghỉ học cấp MG
        // Mark_Level_41 : PupilID nghỉ học cấp 1 
        // Mark_Level_42 : PupilID nghỉ học cấp 2 
        // Mark_Level_43 : PupilID nghỉ học cấp 3 
        // Mark_Level_44 : PupilID nghỉ học cấp NT
        // Mark_Level_45 : PupilID nghỉ học cấp MG

        public ReportAbsencePupilForSupController(IAcademicYearBusiness academicYearID,
                      IProcessedReportBusiness processedReportBusiness, IClassProfileBusiness classProfileBusiness,
                      ISchoolProfileBusiness schoolProfileBusiness, IEducationLevelBusiness educationLevelBusiness,
                      IPupilAbsenceBusiness pupilAbsenceBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
                      IPupilOfClassBusiness pupilOfClassBusiness, ICalendarBusiness CalendarBusiness,
                      IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness, IDistrictBusiness DistrictBusiness,
                      IProvinceBusiness ProvinceBusiness, IMarkStatisticBusiness MarkStatisticBusiness,
                      IPupilProfileBusiness PupilProfileBusiness, IPupilOfSchoolBusiness PupilOfSchoolBusiness,
                      ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.AcademicYearBusiness = academicYearID;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.PupilAbsenceBusiness = pupilAbsenceBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.CalendarBusiness = CalendarBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.PupilOfSchoolBusiness = PupilOfSchoolBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        public void GetViewData()
        {
            ViewData[ReportAbsencePupilForSupConstant.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() {
                                                    new ViettelCheckboxList{Label= Res.Get("Danh sách học sinh nghỉ học"), Value=1, cchecked=true, disabled= false},
                                                    new ViettelCheckboxList{Label= Res.Get("Thống kê học sinh nghỉ học"), Value=2, cchecked=false, disabled= false},
                                                };

            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept_THPT(_globalInfo.SupervisingDeptID.Value);

            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = (int)lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.ToList();

            // Lấy năm học hiện tại
            GlobalInfo global = new GlobalInfo();
            int currentYear = DateTime.Now.Year;
            currentYear = currentYear - 1;
            if (DateTime.Now.Month >= 9)
            {
                currentYear++;
            }
            List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
            if (lsYear != null && lsYear.Count > 0 && !lsYear.Contains(currentYear))
            {
                currentYear = lsYear.FirstOrDefault();
            }
            List<ComboObject> lscbYear = new List<ComboObject>();
            if (lsYear != null && lsYear.Count > 0)
            {
                foreach (var year in lsYear)
                {
                    string value = year.ToString() + "-" + (year + 1).ToString();
                    ComboObject cb = new ComboObject(year.ToString(), value);
                    lscbYear.Add(cb);
                }
            }
            ViewData[ReportAbsencePupilForSupConstant.LIST_AcademicYear] = new SelectList(lscbYear, "key", "value", currentYear);

            // Lấy quận huyện
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ProvinceID",_globalInfo.ProvinceID}
                };
            ViewData["IsSuperVisingDeptRole"] = 1;
            IQueryable<District> lstDistrict = DistrictBusiness.Search(dic).OrderBy(o => o.DistrictName);
            if (_globalInfo.IsSuperVisingDeptRole)
                ViewData[ReportAbsencePupilForSupConstant.LIST_DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName");
            else
                ViewData[ReportAbsencePupilForSupConstant.LIST_DISTRICT] = new SelectList(lstDistrict.ToList(), "DistrictID", "DistrictName", _globalInfo.DistrictID);
        }

        // Gọi Store
        public JsonResult CreatedReport(int year, int reportType, string fromDate, string toDate, int? districtID,
            bool Grade4, bool Grade3, bool Grade2, bool Grade1)
        {
            DateTime? FromDate = Convert.ToDateTime(fromDate);
            DateTime? ToDate = Convert.ToDateTime(toDate);
            GlobalInfo global = GlobalInfo.getInstance();
            IDictionary<string, object> dicExport = new Dictionary<string, object>();
            dicExport.Add("Year", year);
            dicExport.Add("ReportType", reportType);
            //dicExport.Add("MonthID", month);
            dicExport.Add("DistrictID", districtID.HasValue ? districtID : 0);
            dicExport.Add("ProvinceID", _globalInfo.ProvinceID);
            dicExport.Add("FromDate", FromDate);
            dicExport.Add("ToDate", ToDate);
            //0: so GD, 1: phongGD
            int SupervisingDeptID = _globalInfo.IsSubSuperVisingDeptRole ? 1 : 0;
            dicExport.Add("SupervisingDeptID", SupervisingDeptID);
            //MarkStatisticBusiness.InsertReportAbsencePupilForSuperVising(dicExport);
            PupilOfClassBusiness.InsertReportAbsencePupilForSuperVising(dicExport);

            return Json(new { Type = "success" });
        }

        // Xuất thống kê học sinh nghỉ học
        public FileResult ExportReport(int reportType, int year, int? district, string fromDate, string toDate,
            bool SheetMN, bool SheetTH, bool SheetTHCS, bool SheetTHPT)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dicExport = new Dictionary<string, object>();
            dicExport.Add("ReportType", reportType);
            dicExport.Add("Year", year);
            dicExport.Add("DistrictID", district);
            dicExport.Add("FromDate", fromDate);
            dicExport.Add("ToDate", toDate);
            dicExport.Add("ProvinceID", _globalInfo.ProvinceID);
            dicExport.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
            dicExport.Add("SheetMN", SheetMN);
            dicExport.Add("SheetTH", SheetTH);
            dicExport.Add("SheetTHCS", SheetTHCS);
            dicExport.Add("SheetTHPT", SheetTHPT);
            dicExport.Add("isSub", global.IsSubSuperVisingDeptRole);
            //0: so GD, 1: phongGD
            int SupervisingDeptID = _globalInfo.IsSubSuperVisingDeptRole ? 1 : 0;
            dicExport.Add("SupervisingDeptID", SupervisingDeptID);

            FileStreamResult result = null;
            Stream excel = null;

            //excel = MarkStatisticBusiness.ReportStatisticsPupilAbsence(dicExport);
            excel = PupilOfClassBusiness.ReportStatisticsPupilAbsenceBySup(dicExport);
            result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = "TKHSNghiHoc_PhongSo.xls";
            result.FileDownloadName = fileName;

            return result;
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(int reportType, int year, int? district, string fromDate, string toDate,
            bool SheetMN, bool SheetTH, bool SheetTHCS, bool SheetTHPT)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ReportType", reportType);
            dic.Add("Year", year);
            dic.Add("DistrictID", district);
            dic.Add("FromDate", fromDate);
            dic.Add("ToDate", toDate);
            dic.Add("ProvinceID", _globalInfo.ProvinceID);
            dic.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
            dic.Add("SheetMN", SheetMN);
            dic.Add("SheetTH", SheetTH);
            dic.Add("SheetTHCS", SheetTHCS);
            dic.Add("SheetTHPT", SheetTHPT);

            List<int> lstEducationLevel = new List<int>();
            if (SheetTH)
                lstEducationLevel.Add(1);
            if (SheetTHCS)
                lstEducationLevel.Add(2);
            if (SheetTHPT)
                lstEducationLevel.Add(3);
            if (SheetMN)
                lstEducationLevel.Add(4);

            string reportCode = "";
            if (lstEducationLevel.Count == 1)
            {
                // Chọn xuất danh sách 1 cấp
                reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo_1;
            }
            else
            {
                // Chọn xuất danh sách 2 cấp trở lên
                reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo;
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            string FolderSaveFile = string.Empty;

            if (lstEducationLevel.Count == 1)
            {
                Stream excel = CreateTicketTakeExamPaperReport(dic, lstEducationLevel[0]);
                processedReport = PupilOfClassBusiness.InsertTicketTakeExamPaperReport(dic, excel);
                excel.Close();
            }
            else
            {
                DownloadZipFileTranscript(dic, out FolderSaveFile, lstEducationLevel);
                if (FolderSaveFile == "")
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                }

                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = PupilOfClassBusiness.InsertTicketTakeExamPaperReportZip(dic, output);
                    }
                }
            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic.Add("ReportType", form.ReportType);
            //dic.Add("Year", form.Year);
            //dic.Add("DistrictID", form.DistrictID);
            //dic.Add("FromDate", form.FromDate);
            //dic.Add("ToDate", form.ToDate);
            //dic.Add("ProvinceID", _globalInfo.ProvinceID);
            //dic.Add("SuperVisingDeptName", _globalInfo.SuperVisingDeptName.ToUpper());
            //dic.Add("SheetMN", form.Grade1);
            //dic.Add("SheetTH", form.Grade2);
            //dic.Add("SheetTHCS", form.Grade3);
            //dic.Add("SheetTHPT", form.Grade4);

            //string reportCode = "";
            //reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo;

            //List<int> lstEducationLevel = new List<int>();
            //if (SheetTH)
            //    lstEducationLevel.Add(1);
            //if (SheetTHCS)
            //    lstEducationLevel.Add(2);
            //if (SheetTHPT)
            //    lstEducationLevel.Add(3);
            //if (SheetMN)
            //    lstEducationLevel.Add(4);

            //string FolderSaveFile = string.Empty;
            //DownloadZipFileTranscript(dic, out FolderSaveFile, lstEducationLevel);
            ProcessedReport processedReport = null;
            //if (FolderSaveFile == "")
            //{
            //    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
            //}
            //DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
            //using (ZipFile zip = new ZipFile())
            //{
            //    zip.AddDirectory(Path.Combine(FolderSaveFile));
            //    using (MemoryStream output = new MemoryStream())
            //    {
            //        zip.Save(output);
            //        di.Delete(true);
            //        processedReport = PupilOfClassBusiness.InsertTicketTakeExamPaperReportZip(dic, output);
            //    }
            //}

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, 0, 0);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.DSHSNghiHoc_PhongSo.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }


            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        // Nén file zip khi xuất danh sách 2 cấp trở lên
        public void DownloadZipFileTranscript(IDictionary<string, object> dic, out string folderSaveFile, List<int> lstEducationLevel)
        {
            #region lay danh sach hoc sinh cua lop
            int reportType = SMAS.Business.Common.Utils.GetInt(dic, "ReportType");
            int year = SMAS.Business.Common.Utils.GetInt(dic, "Year");
            int provinceId = SMAS.Business.Common.Utils.GetInt(dic, "ProvinceID");
            int? districtID = SMAS.Business.Common.Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = SMAS.Business.Common.Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = SMAS.Business.Common.Utils.GetBool(dic, "SheetMN");
            bool SheetTH = SMAS.Business.Common.Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHPT");

            string fileName = "DSHSNghiHoc_{0}.xls";//DSHSNghiHoc_[Cấp học]
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);

            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);
            int paritionId = UtilsBusiness.GetPartionProvince(provinceId);
            List<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId).ToList();
            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value).ToList();
            }
            List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

            // Danh sach nam hoc         
            IQueryable<AcademicYearBO> iqSchoolAll = from sp in SchoolProfileBusiness.AllNoTracking
                                                     join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                                     where sp.ProvinceID == provinceId
                                                           && sp.IsActive == true
                                                           && sp.IsActiveSMAS == true
                                                     select new AcademicYearBO
                                                     {
                                                         AcademicYearID = ay.AcademicYearID,
                                                         SchoolID = sp.SchoolProfileID,
                                                         Year = year,
                                                         School = sp,
                                                         DistrictID = sp.DistrictID,
                                                         SuperVisingID = sp.SupervisingDeptID,
                                                         EducationGrade = sp.EducationGrade,
                                                         SchoolName = sp.SchoolName,
                                                     };

            if (districtID > 0)
            {
                iqSchoolAll = iqSchoolAll.Where(sp => sp.School.DistrictID == districtID);
            }
            List<AcademicYearBO> lstSchoolAll = iqSchoolAll.ToList();

            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.ReportCode == "SheetListPupilAbsence"
                                                              && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
                                                              && m.Last2DigitNumberProvince == paritionId
                                                              && m.MarkLevel36 != null
                                                              && m.MarkLevel37 != null
                                                              && m.MarkLevel38 != null
                                                              && m.MarkLevel39 != null
                                                              && m.MarkLevel40 != null
                                                              select m;
            if (districtID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            }

            List<MarkStatistic> lstMark = iQMarkStatisticDelete.ToList();


            //string stringPupilIDTHCS = "";
            //string stringPupilIDTHPT = "";
            //string stringPupilIDNT = "";
            //string stringPupilIDMG = "";
            List<PupilProfileCustom> lstPupilAll = new List<PupilProfileCustom>();
            for (int i = 0; i < lstMark.Count(); i++)
            {
                MarkStatistic objMark = lstMark[i];
                string stringPupilID = "";
                if (lstMark[i].MarkLevel41 != null)
                    stringPupilID = stringPupilID + lstMark[i].MarkLevel41 + ",";
                if (lstMark[i].MarkLevel42 != null)
                    stringPupilID = stringPupilID + lstMark[i].MarkLevel42 + ",";
                if (lstMark[i].MarkLevel43 != null)
                    stringPupilID = stringPupilID + lstMark[i].MarkLevel43 + ",";
                if (lstMark[i].MarkLevel44 != null)
                    stringPupilID = stringPupilID + lstMark[i].MarkLevel44 + ",";
                if (lstMark[i].MarkLevel45 != null)
                    stringPupilID = stringPupilID + lstMark[i].MarkLevel45 + ",";

                List<int> lstId = stringPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                var lstVal = (from poc in PupilOfClassBusiness.All
                              join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                              join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                              where lstId.Contains(poc.PupilID)
                              && poc.AcademicYearID == objMark.AcademicYearID
                              && poc.SchoolID == objMark.SchoolID
                              && cp.AcademicYearID == objMark.AcademicYearID
                              select new PupilProfileCustom
                              {
                                  ClassID = poc.ClassID,
                                  ClassName = cp.DisplayName,
                                  PupilID = poc.PupilID,
                                  PupilName = pp.FullName,
                                  Gender = pp.Genre,
                                  ClassType = pp.ClassType,
                                  ParentName = pp.FatherFullName == null ? pp.MotherFullName : pp.FatherFullName,
                                  Address = pp.PermanentResidentalAddress == null ? pp.TempResidentalAddress : pp.PermanentResidentalAddress,
                                  EthenicName = pp.Ethnic.EthnicName,
                                  SchoolID = poc.SchoolID,
                              }).ToList();
                if (lstVal != null && lstVal.Count > 0)
                {
                    lstPupilAll.AddRange(lstVal);
                    //lstPupilID.AddRange(lstId);
                }

            }

            //List<int> lstPupilIDTH = stringPupilIDTH.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //List<int> lstPupilIDTHCS = stringPupilIDTHCS.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //List<int> lstPupilIDTHPT = stringPupilIDTHPT.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //List<int> lstPupilIDNT = stringPupilIDNT.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //List<int> lstPupilIDMG = stringPupilIDMG.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            /*List<PupilProfileCustom> lstPupilAll = (from poc in PupilOfClassBusiness.All.Where(x => lstPupilIDTH.Contains(x.PupilID) || lstPupilIDTHCS.Contains(x.PupilID) || lstPupilIDTHPT.Contains(x.PupilID) || lstPupilIDNT.Contains(x.PupilID) || lstPupilIDMG.Contains(x.PupilID))
                                                    select new PupilProfileCustom
                                                    {
                                                        ClassID = poc.ClassID,
                                                        ClassName = poc.ClassProfile.DisplayName,
                                                        PupilID = poc.PupilID,
                                                        PupilName = poc.PupilProfile.FullName,
                                                        Gender = poc.PupilProfile.Genre,
                                                        ClassType = poc.PupilProfile.ClassType,
                                                        ParentName = poc.PupilProfile.FatherFullName == null ? poc.PupilProfile.MotherFullName : poc.PupilProfile.FatherFullName,
                                                        Address = poc.PupilProfile.PermanentResidentalAddress == null ? poc.PupilProfile.TempResidentalAddress : poc.PupilProfile.PermanentResidentalAddress,
                                                        EthenicName = poc.PupilProfile.Ethnic.EthnicName,
                                                        SchoolID = poc.SchoolID,
                                                    }).ToList();

            */

            List<int> lstSchoolByApplied = new List<int>();
            List<AcademicYearBO> lstSchoolTemp = new List<AcademicYearBO>();

            #endregion

            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Phong_So/" + SystemParamsInFile.DSHSNghiHoc_PhongSo + ".xls";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                if (lstEducationLevel[i] == 1)
                {
                    lstSchoolByApplied = lstMark.Where(x => x.MarkLevel36 != -1).Select(x => x.SchoolID).Distinct().ToList();
                    lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
                }
                else if (lstEducationLevel[i] == 2)
                {
                    lstSchoolByApplied = lstMark.Where(x => x.MarkLevel37 != -1).Select(x => x.SchoolID).Distinct().ToList();
                    lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
                }
                else if (lstEducationLevel[i] == 3)
                {
                    lstSchoolByApplied = lstMark.Where(x => x.MarkLevel38 != -1).Select(x => x.SchoolID).Distinct().ToList();
                    lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
                }
                else if (lstEducationLevel[i] == 4)
                {
                    lstSchoolByApplied = lstMark.Where(x => x.MarkLevel39 != -1 || x.MarkLevel40 != -1).Select(x => x.SchoolID).Distinct().ToList();
                    lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
                }

                CreateTicketTakeExamPaperReportZip(dic, lstDistrict, lstSchoolTemp, lstMark, lstPupilAll, lstEducationLevel[i], oBook);
                oBook.CreateZipFile(string.Format(fileName, lstEducationLevel[i] == 1 ? "TH" : lstEducationLevel[i] == 2 ? "THCS" : lstEducationLevel[i] == 3 ? "THPT" : "MN"), folder);
            }

            folderSaveFile = folder;
            #endregion
        }

        // Xuất danh sách học sinh nghỉ học khi chọn 2 cấp trở lên
        public Stream CreateTicketTakeExamPaperReportZip(IDictionary<string, object> dic,
            List<District> lstDistrict,
            List<AcademicYearBO> lstSchoolByApplied,
            List<MarkStatistic> lstMark,
            List<PupilProfileCustom> lstPupilAll,
            int educationID,
            IVTWorkbook oBook)
        {
            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            MarkStatistic objMarkTemp = new MarkStatistic();
            //MarkStatistic objMarkMGTemp = new MarkStatistic();
            //MarkStatistic objMarkNTTemp = new MarkStatistic();
            List<PupilProfile> lstPPTemp = new List<PupilProfile>();
            int reportType = SMAS.Business.Common.Utils.GetInt(dic, "ReportType");
            int? year = SMAS.Business.Common.Utils.GetInt(dic, "Year");
            int provinceId = SMAS.Business.Common.Utils.GetInt(dic, "ProvinceID");
            int? districtID = SMAS.Business.Common.Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = SMAS.Business.Common.Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = SMAS.Business.Common.Utils.GetBool(dic, "SheetMN");
            bool SheetTH = SMAS.Business.Common.Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHPT");
            GlobalInfo global = new GlobalInfo();

            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);
            DateTime DateTemp = fromDate;

            List<int> lstPupilID = new List<int>();
            List<PupilProfileCustom> lstPupilTemp = new List<PupilProfileCustom>();
            PupilProfileCustom objPupil = new PupilProfileCustom();
            int startRow;
            int DistrictTitle;
            int stt = 0;
            bool isSpace = true;
            bool isNull = true;
            int countPupil = 0;
            int countPupilNT = 0;
            int countPupilMG = 0;
            int rowDistrict = 0;
            int index = 0;
            int startTowTemp = 0;
            int startRowTHTemp = 0;
            int startRowTHCSTemp = 0;
            int startRowTHPTTemp = 0;
            int startTempMN = 0;
            int fomularRow = 0;
            string sumPupilRow = "=";
            string sumPupilAbsenceRow = "=";
            string sumPupilRowAll = "=";
            string sumPupilAbsenceRowAll = "=";
            #region // Fill nội dung

            for (int i = 0; i < distantOfDay; i++)
            {
                IVTWorksheet curSheet = null;
                if (educationID == 1 || educationID == 2 || educationID == 3)
                {
                    curSheet = oBook.CopySheetToLast(firstSheet, "P" + 1740.ToString());
                }
                else if (educationID == 4)
                {
                    curSheet = oBook.CopySheetToLast(secondSheet, "O" + 1740.ToString());
                }
                curSheet.SetCellValue("A3", superVisingDeptName);
                if (educationID == 1)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP TH");
                }
                else if (educationID == 2)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP THCS");
                }
                else if (educationID == 3)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP THPT");
                }
                else if (educationID == 4)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP MẦM NON");
                }
                curSheet.SetCellValue("A6", string.Format("Ngày {0}", DateTemp.ToShortDateString()));
                startRow = 10;
                startTowTemp = 0;
                stt = 0;
                sumPupilRowAll = "=";
                sumPupilAbsenceRowAll = "=";
                foreach (var itemDis in lstDistrict)
                {
                    sumPupilRowAll = sumPupilRowAll + "D" + startRow + "+";
                    sumPupilAbsenceRowAll = sumPupilAbsenceRowAll + "E" + startRow + "+";
                    countPupil = 0;
                    countPupilNT = 0;
                    countPupilMG = 0;
                    startRowTHTemp = 0;
                    sumPupilRow = "=";
                    sumPupilAbsenceRow = "=";
                    stt = 1;
                    isSpace = true;
                    isNull = true;
                    DistrictTitle = startRow;
                    curSheet.SetCellValue(startRow, 1, itemDis.DistrictName);
                    curSheet.SetRowHeight(startRow, 25);
                    curSheet.GetRange(startRow, 1, startRow, 6).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    curSheet.GetRange(startRow, 1, startRow, 3).Merge();
                    curSheet.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                    var lstSchoolProfile = lstSchoolByApplied.Where(x => x.DistrictID == itemDis.DistrictID);
                    startTowTemp = startRow + 1;
                    foreach (var itemSchool in lstSchoolProfile)
                    {
                        countPupilNT = 0;
                        countPupilMG = 0;
                        countPupil = 0;
                        isNull = true;
                        startRow++;
                        fomularRow = startRow;
                        if (districtID != 0)
                            fomularRow = startRow - 1;

                        #region cap 1
                        if (educationID == 1)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRowTHTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "TH");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel36);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel01);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + fomularRow + "/IF(D" + fomularRow + "<=0,1,D" + fomularRow + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel41 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel41.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isNull = false;
                                            isSpace = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }
                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHTemp, "A" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHTemp, "B" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHTemp, "C" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHTemp, "D" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHTemp, "E" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHTemp, "F" + (startRowTHTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHTemp, "A" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHTemp, "B" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHTemp, "C" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHTemp, "D" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHTemp, "E" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHTemp, "F" + (startRowTHTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region cap 2
                        if (educationID == 2)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRowTHCSTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "THCS");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel37);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel02);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + fomularRow + "/IF(D" + fomularRow + "<=0,1,D" + fomularRow + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel42 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel42.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isSpace = false;
                                            isNull = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }
                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHCSTemp, "A" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHCSTemp, "B" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHCSTemp, "C" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHCSTemp, "D" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHCSTemp, "E" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHCSTemp, "F" + (startRowTHCSTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHCSTemp, "A" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHCSTemp, "B" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHCSTemp, "C" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHCSTemp, "D" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHCSTemp, "E" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHCSTemp, "F" + (startRowTHCSTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region cap 3
                        if (educationID == 3)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRowTHPTTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "THPT");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel38);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel03);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + fomularRow + "/IF(D" + fomularRow + "<=0,1,D" + fomularRow + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel43 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel43.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isSpace = false;
                                            isNull = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }

                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHPTTemp, "A" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHPTTemp, "B" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHPTTemp, "C" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHPTTemp, "D" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHPTTemp, "E" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHPTTemp, "F" + (startRowTHPTTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHPTTemp, "A" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHPTTemp, "B" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHPTTemp, "C" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHPTTemp, "D" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHPTTemp, "E" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHPTTemp, "F" + (startRowTHPTTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region MN
                        if (educationID == 4)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                index = startRow;
                                //curSheet.SetCellValue("C" + startRow, "TH");
                                curSheet.SetCellValue("D" + startRow, (objMarkTemp.MarkLevel39 == -1 ? 0 : objMarkTemp.MarkLevel39) + (objMarkTemp.MarkLevel40 == -1 ? 0 : objMarkTemp.MarkLevel40));

                                #region fill nhà trẻ và mầm non
                                startTempMN = startRow;
                                for (int l = 0; l < 2; l++)
                                {
                                    // nhà trẻ                                    
                                    if (l == 0)
                                    {
                                        #region nếu có nhà trẻ mới xuất
                                        if (objMarkTemp != null && objMarkTemp.MarkLevel39 != -1)
                                        {
                                            curSheet.SetCellValue("C" + startRow, "Nhà trẻ");
                                            curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel39 == -1 ? 0 : objMarkTemp.MarkLevel39);
                                            curSheet.SetCellValue("F" + startRow, objMarkTemp.MarkLevel04);
                                            curSheet.SetCellValue("G" + startRow, "=ROUND(F" + fomularRow + "/IF(E" + fomularRow + "<=0,1,E" + fomularRow + "),3)*100");
                                            if (objMarkTemp.MarkLevel44 != null)
                                            {
                                                lstPupilID = objMarkTemp.MarkLevel44.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                                lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                                for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                                {
                                                    objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                                    if (objPupil != null)
                                                    {
                                                        isSpace = false;
                                                        countPupilNT++;
                                                        curSheet.SetCellValue("H" + startRow, objPupil.PupilName);
                                                        curSheet.SetCellValue("I" + startRow, objPupil.ClassName);
                                                        curSheet.SetCellValue("J" + startRow, objPupil.GenderName);
                                                        curSheet.SetCellValue("K" + startRow, objPupil.EthenicName);
                                                        curSheet.SetCellValue("L" + startRow, objPupil.Address);
                                                        curSheet.SetCellValue("M" + startRow, objPupil.ParentName);
                                                        startRow++;
                                                    }
                                                }
                                                int startRowNT = startTempMN + countPupilNT - 1;
                                                curSheet.GetRange("C" + startTempMN, "C" + startRowNT).Merge();
                                                curSheet.GetRange("E" + startTempMN, "E" + startRowNT).Merge();
                                                curSheet.GetRange("F" + startTempMN, "F" + startRowNT).Merge();
                                                curSheet.GetRange("G" + startTempMN, "G" + startRowNT).Merge();
                                                startRow = startRow - 1;
                                            }
                                        }
                                        #endregion
                                        startRow++;
                                    }
                                    if (l == 1)
                                    {
                                        #region nếu có mẫu giáo mới xuất
                                        fomularRow = startRow;
                                        if (districtID != 0)
                                            fomularRow = startRow - 1;
                                        if (objMarkTemp != null && objMarkTemp.MarkLevel39 != null)
                                            startTempMN = startRow;

                                        if (objMarkTemp != null && objMarkTemp.MarkLevel40 != -1)
                                        {
                                            startTempMN = startRow;
                                            curSheet.SetCellValue("C" + startRow, "Mẫu giáo");
                                            curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel40 == -1 ? 0 : objMarkTemp.MarkLevel40);
                                            curSheet.SetCellValue("F" + startRow, objMarkTemp.MarkLevel05);
                                            //curSheet.SetCellValue("G" + startRow, "=F" + startRow + "/E" + startRow + "%");
                                            curSheet.SetCellValue("G" + startRow, "=ROUND(F" + fomularRow + "/IF(E" + fomularRow + "<=0,1,E" + fomularRow + "),3)*100");
                                            if (objMarkTemp.MarkLevel45 != null)
                                            {
                                                lstPupilID = objMarkTemp.MarkLevel45.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                                lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                                for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                                {
                                                    objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                                    if (objPupil != null)
                                                    {
                                                        isSpace = false;
                                                        countPupilMG++;
                                                        curSheet.SetCellValue("H" + startRow, objPupil.PupilName);
                                                        curSheet.SetCellValue("I" + startRow, objPupil.ClassName);
                                                        curSheet.SetCellValue("J" + startRow, objPupil.GenderName);
                                                        curSheet.SetCellValue("K" + startRow, objPupil.EthenicName);
                                                        curSheet.SetCellValue("L" + startRow, objPupil.Address);
                                                        curSheet.SetCellValue("M" + startRow, objPupil.ParentName);
                                                        startRow++;
                                                    }
                                                }
                                                int startRowNT = startTempMN + countPupilMG - 1;

                                                curSheet.GetRange("C" + startTempMN, "C" + startRowNT).Merge();
                                                curSheet.GetRange("E" + startTempMN, "E" + startRowNT).Merge();
                                                curSheet.GetRange("F" + startTempMN, "F" + startRowNT).Merge();
                                                curSheet.GetRange("G" + startTempMN, "G" + startRowNT).Merge();
                                                startRow = startRow - 1;
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            #region Merge row
                            if (countPupilMG != 0 || countPupilNT != 0)
                            {
                                if (countPupilMG == 0 || countPupilNT == 0)
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilNT + countPupilMG)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilNT + countPupilMG)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilNT + countPupilMG)).Merge();
                                }                              
                                else 
                                {                                    
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                }
                            }
                            else
                            {                            
                                if (objMarkTemp != null && (objMarkTemp.MarkLevel39 == -1 || objMarkTemp.MarkLevel40 == -1))
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilMG + countPupilNT)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilMG + countPupilNT)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilMG + countPupilNT)).Merge();
                                }
                                else
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                }                               
                            }
                            #endregion
                        }
                        #endregion
                        stt++;
                    }
                    #region fill từng quận/huyện
                    fomularRow = DistrictTitle;
                    rowDistrict = DistrictTitle;
                    if (districtID != 0)
                        fomularRow = DistrictTitle - 1;
                    if (lstSchoolProfile.Count() > 0)// && (educationID == 1 || educationID == 2 || educationID == 3))
                    {
                        if (educationID == 4)
                        {
                            curSheet.SetCellValue(rowDistrict, 4, sumPupilRow.Substring(0, sumPupilRow.Length - 1));
                            curSheet.GetRange("D" + rowDistrict, "E" + rowDistrict).Merge();
                            curSheet.SetCellValue(rowDistrict, 6, sumPupilAbsenceRow.Substring(0, sumPupilAbsenceRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 7, "=ROUND(E" + rowDistrict + "/IF(D" + rowDistrict + "<=0,1,D" + rowDistrict + "),3)*100");
                        }
                        else
                        {
                            curSheet.SetCellValue(rowDistrict, 4, sumPupilRow.Substring(0, sumPupilRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 5, sumPupilAbsenceRow.Substring(0, sumPupilAbsenceRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 6, "=ROUND(E" + rowDistrict + "/IF(D" + rowDistrict + "<=0,1,D" + rowDistrict + "),3)*100");
                        }
                    }
                    else //if ((educationID == 1 || educationID == 2 || educationID == 3))
                    {
                        if (educationID == 4)
                        {
                            curSheet.SetCellValue(rowDistrict, 4, 0);
                            curSheet.GetRange("D" + rowDistrict, "E" + rowDistrict).Merge();
                            curSheet.SetCellValue(rowDistrict, 6, 0);
                            //curSheet.SetCellValue(rowDistrict, 7, "=E" + (rowDistrict) + "/D" + (rowDistrict) + "%");
                            curSheet.SetCellValue(rowDistrict, 7, "=ROUND(E" + rowDistrict + "/IF(D" + rowDistrict + "<=0,1,D" + rowDistrict + "),3)*100");
                        }
                        else
                        {
                            curSheet.SetCellValue(rowDistrict, 4, 0);
                            curSheet.SetCellValue(rowDistrict, 5, 0);
                            //curSheet.SetCellValue(rowDistrict, 6, "=E" + (rowDistrict) + "/D" + (rowDistrict) + "%");
                            curSheet.SetCellValue(rowDistrict, 6, "=ROUND(E" + rowDistrict + "/IF(D" + rowDistrict + "<=0,1,D" + rowDistrict + "),3)*100");
                        }
                    }
                    #endregion
                    startRow++;
                }
                #region fill toàn tỉnh

                curSheet.SetCellValue(9, 4, sumPupilRowAll.Substring(0, sumPupilRowAll.Length - 1));
                if (educationID == 1 || educationID == 2 || educationID == 3)
                {
                    curSheet.SetCellValue(9, 5, sumPupilAbsenceRowAll.Substring(0, sumPupilAbsenceRowAll.Length - 1));
                    //curSheet.SetCellValue(9, 6, "=E9/D9%");
                    curSheet.SetCellValue(9, 6, "=ROUND(E" + 9 + "/IF(D" + 9 + "<=0,1,D" + 9 + "),3)*100");
                    curSheet.GetRange(10, 1, startRow - 1, 16).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    curSheet.Name = DateTemp.Day + "." + DateTemp.Month;
                }
                else if (educationID == 4)
                {
                    curSheet.SetCellValue(9, 6, sumPupilAbsenceRowAll.Substring(0, sumPupilAbsenceRowAll.Length - 1));
                    curSheet.SetCellValue(9, 7, "=ROUND(E" + 9 + "/IF(D" + 9 + "<=0,1,D" + 9 + "),3)*100");
                    //curSheet.SetCellValue(9, 7, "=E9/D9%");
                    curSheet.GetRange(10, 1, startRow - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    curSheet.Name = DateTemp.Day + "." + DateTemp.Month + " MN";
                }

                #endregion
                if (districtID != 0)
                {
                    curSheet.DeleteRow(9);
                }
                curSheet.SetFontName("Times New Roman", 0);
                DateTemp = DateTemp.AddDays(1);
            }
            #endregion
            firstSheet.Delete();
            secondSheet.Delete();
            return oBook.ToStream();
        }

        // Xuất danh sách học sinh nghỉ học khi chọn 1 cấp
        public Stream CreateTicketTakeExamPaperReport(IDictionary<string, object> dic, int educationID)
        {
            //Lấy ra sheet đầu tiên
            string reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo_1;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            GlobalInfo global = new GlobalInfo();
            MarkStatistic objMarkTemp = new MarkStatistic();
            List<int> lstSchoolByApplied = new List<int>();
            List<AcademicYearBO> lstSchoolTemp = new List<AcademicYearBO>();

            List<PupilProfile> lstPPTemp = new List<PupilProfile>();
            int reportType = SMAS.Business.Common.Utils.GetInt(dic, "ReportType");
            int year = SMAS.Business.Common.Utils.GetInt(dic, "Year");
            int provinceId = SMAS.Business.Common.Utils.GetInt(dic, "ProvinceID");
            int? districtID = SMAS.Business.Common.Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = SMAS.Business.Common.Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = SMAS.Business.Common.Utils.GetBool(dic, "SheetMN");
            bool SheetTH = SMAS.Business.Common.Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = SMAS.Business.Common.Utils.GetBool(dic, "SheetTHPT");

            int partitionId = UtilsBusiness.GetPartionProvince(provinceId);
            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);

            List<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId && x.IsActive == true).ToList();

            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value).ToList();
            }
            List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

            // Danh sach nam hoc
            IQueryable<AcademicYearBO> iqSchoolAll = from sp in SchoolProfileBusiness.AllNoTracking
                                                     join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                                     where sp.ProvinceID == provinceId
                                                           && sp.IsActive == true
                                                           && sp.IsActiveSMAS == true
                                                     select new AcademicYearBO
                                                     {
                                                         AcademicYearID = ay.AcademicYearID,
                                                         SchoolID = sp.SchoolProfileID,
                                                         Year = year,
                                                         School = sp,
                                                         DistrictID = sp.DistrictID,
                                                         SuperVisingID = sp.SupervisingDeptID,
                                                         EducationGrade = sp.EducationGrade,
                                                         SchoolName = sp.SchoolName,
                                                     };
            if (districtID > 0)
            {
                iqSchoolAll = iqSchoolAll.Where(sp => sp.School.DistrictID == districtID);
            }
            List<AcademicYearBO> lstSchoolAll = iqSchoolAll.ToList();

            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.ReportCode == "SheetListPupilAbsence"
                                                              && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
                                                              && m.Last2DigitNumberProvince == partitionId
                                                              && m.MarkLevel36 != null
                                                              && m.MarkLevel37 != null
                                                              && m.MarkLevel38 != null
                                                              && m.MarkLevel39 != null
                                                              && m.MarkLevel40 != null
                                                              select m;
            if (districtID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            }
            List<MarkStatistic> lstMark = iQMarkStatisticDelete.ToList();

            if (educationID == 1)
            {
                lstMark = lstMark.Where(x => x.MarkLevel36 != -1).ToList();
                lstSchoolByApplied = lstMark.Where(x => x.MarkLevel36 != -1).Select(x => x.SchoolID).Distinct().ToList();
                lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
            }
            else if (educationID == 2)
            {
                lstMark = lstMark.Where(x => x.MarkLevel37 != -1).ToList();
                lstSchoolByApplied = lstMark.Where(x => x.MarkLevel37 != -1).Select(x => x.SchoolID).Distinct().ToList();
                lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
            }
            else if (educationID == 3)
            {
                lstMark = lstMark.Where(x => x.MarkLevel38 != -1).ToList();
                lstSchoolByApplied = lstMark.Where(x => x.MarkLevel38 != -1).Select(x => x.SchoolID).Distinct().ToList();
                lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
            }
            else if (educationID == 4)
            {
                lstMark = lstMark.Where(x => x.MarkLevel39 != -1 || x.MarkLevel40 != -1).ToList();
                lstSchoolByApplied = lstMark.Where(x => x.MarkLevel39 != -1 || x.MarkLevel40 != -1).Select(x => x.SchoolID).Distinct().ToList();
                lstSchoolTemp = lstSchoolAll.Where(x => lstSchoolByApplied.Contains(x.SchoolID)).ToList();
            }

            List<PupilProfileCustom> lstPupilAll = new List<PupilProfileCustom>();
            List<int> lstPupilID = new List<int>();
            for (int i = 0; i < lstMark.Count(); i++)
            {
                string stringPupilID = "";
                MarkStatistic objMark = lstMark[i];
                if (lstMark[i].MarkLevel41 != null)
                    stringPupilID = stringPupilID + objMark.MarkLevel41 + ",";
                if (lstMark[i].MarkLevel42 != null)
                    stringPupilID = stringPupilID + objMark.MarkLevel42 + ",";
                if (lstMark[i].MarkLevel43 != null)
                    stringPupilID = stringPupilID + objMark.MarkLevel43 + ",";
                if (lstMark[i].MarkLevel44 != null)
                    stringPupilID = stringPupilID + objMark.MarkLevel44 + ",";
                if (lstMark[i].MarkLevel45 != null)
                    stringPupilID = stringPupilID + objMark.MarkLevel45 + ",";

                List<int> lstId = stringPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                var lstVal = (from poc in PupilOfClassBusiness.All
                              join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                              join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                              where lstId.Contains(poc.PupilID)
                              && poc.AcademicYearID == objMark.AcademicYearID
                              && poc.SchoolID == objMark.SchoolID
                              && cp.AcademicYearID == objMark.AcademicYearID
                              select new PupilProfileCustom
                              {
                                  ClassID = poc.ClassID,
                                  ClassName = cp.DisplayName,
                                  PupilID = poc.PupilID,
                                  PupilName = pp.FullName,
                                  Gender = pp.Genre,
                                  ClassType = pp.ClassType,
                                  ParentName = pp.FatherFullName == null ? pp.MotherFullName : pp.FatherFullName,
                                  Address = pp.PermanentResidentalAddress == null ? pp.TempResidentalAddress : pp.PermanentResidentalAddress,
                                  EthenicName = pp.Ethnic.EthnicName,
                                  SchoolID = poc.SchoolID,
                              }).ToList();
                if (lstVal != null && lstVal.Count > 0)
                {
                    lstPupilAll.AddRange(lstVal);
                    lstPupilID.AddRange(lstId);
                }
            }

            //lstPupilID = stringPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //lstPupilAll = (from poc in PupilOfClassBusiness.All.Where(x => lstPupilID.Contains(x.PupilID))
            //                                        select new PupilProfileCustom
            //                                        {
            //                                            ClassID = poc.ClassID,
            //                                            ClassName = poc.ClassProfile.DisplayName,
            //                                            PupilID = poc.PupilID,
            //                                            PupilName = poc.PupilProfile.FullName,
            //                                            Gender = poc.PupilProfile.Genre,
            //                                            ClassType = poc.PupilProfile.ClassType,
            //                                            ParentName = poc.PupilProfile.FatherFullName == null ? poc.PupilProfile.MotherFullName : poc.PupilProfile.FatherFullName,
            //                                            Address = poc.PupilProfile.PermanentResidentalAddress == null ? poc.PupilProfile.TempResidentalAddress : poc.PupilProfile.PermanentResidentalAddress,
            //                                            EthenicName = poc.PupilProfile.Ethnic.EthnicName,
            //                                            SchoolID = poc.SchoolID,
            //                                        }).ToList();

            #endregion

            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);
            DateTime DateTemp = fromDate;

            List<PupilProfileCustom> lstPupilTemp = new List<PupilProfileCustom>();
            PupilProfileCustom objPupil = new PupilProfileCustom();
            int startRow;
            int DistrictTitle;
            int stt = 0;
            bool isSpace = true;
            bool isNull = true;
            int countPupil = 0;
            int countPupilNT = 0;
            int countPupilMG = 0;
            int rowDistrict = 0;
            int startTowTemp = 0;
            int startTempMN = 0;
            int startRowTHTemp = 0;
            int startRowTHCSTemp = 0;
            int startRowTHPTTemp = 0;
            int index = 0;
            string sumPupilRow = "=";
            string sumPupilAbsenceRow = "=";
            string sumPupilRowAll = "=";
            string sumPupilAbsenceRowAll = "=";
            #region // Fill nội dung

            for (int i = 0; i < distantOfDay; i++)
            {
                IVTWorksheet curSheet = null;
                if (educationID == 1 || educationID == 2 || educationID == 3)
                {
                    curSheet = oBook.CopySheetToLast(firstSheet, "P" + 1740.ToString());
                }
                else if (educationID == 4)
                {
                    curSheet = oBook.CopySheetToLast(secondSheet, "O" + 1740.ToString());
                }
                curSheet.SetCellValue("A3", superVisingDeptName);
                if (educationID == 1)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP TH");
                }
                else if (educationID == 2)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP THCS");
                }
                else if (educationID == 3)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP THPT");
                }
                else if (educationID == 4)
                {
                    curSheet.SetCellValue("A5", "DANH SÁCH HỌC SINH VẮNG CẤP MẦM NON");
                }
                //curSheet.SetCellValue("A2", supervisingDept.SupervisingDeptName.ToUpper());
                curSheet.SetCellValue("A6", string.Format("Ngày {0}", DateTemp.ToShortDateString()));
                startRow = 10;
                startTowTemp = 0;
                stt = 0;
                sumPupilRowAll = "=";
                sumPupilAbsenceRowAll = "=";
                //DateTemp = fromDate;
                foreach (var itemDis in lstDistrict)
                {
                    sumPupilRowAll = sumPupilRowAll + "D" + startRow + "+";
                    sumPupilAbsenceRowAll = sumPupilAbsenceRowAll + "E" + startRow + "+";
                    countPupil = 0;
                    countPupilNT = 0;
                    countPupilMG = 0;
                    startRowTHTemp = 0;
                    sumPupilRow = "=";
                    sumPupilAbsenceRow = "=";
                    stt = 0;
                    stt++;
                    isSpace = true;
                    isNull = true;
                    DistrictTitle = startRow;
                    curSheet.SetCellValue(startRow, 1, itemDis.DistrictName);
                    curSheet.SetRowHeight(startRow, 25);
                    curSheet.GetRange(startRow, 1, startRow, 6).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    curSheet.GetRange(startRow, 1, startRow, 3).Merge();
                    curSheet.GetRange(startRow, 1, startRow, 3).SetHAlign(VTHAlign.xlHAlignLeft);
                    var lstSchoolProfile = lstSchoolTemp.Where(x => x.DistrictID == itemDis.DistrictID);
                    startTowTemp = startRow + 1;
                    foreach (var itemSchool in lstSchoolProfile)
                    {
                        countPupilNT = 0;
                        countPupilMG = 0;
                        countPupil = 0;
                        isNull = true;
                        startRow++;
                        #region cap 1
                        if (educationID == 1)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "C" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            //curSheet.GetRange("B" + startRow, "C" + startRow).SetHAlignVer(NativeExcel.XlVAlign.);
                            startRowTHTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "TH");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel36);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel01);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + (startRow - 1) + "/IF(D" + (startRow - 1) + "<=0,1,D" + (startRow - 1) + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel41 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel41.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isNull = false;
                                            isSpace = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }
                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHTemp, "A" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHTemp, "B" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHTemp, "C" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHTemp, "D" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHTemp, "E" + (startRowTHTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHTemp, "F" + (startRowTHTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHTemp, "A" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHTemp, "B" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHTemp, "C" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHTemp, "D" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHTemp, "E" + (startRowTHTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHTemp, "F" + (startRowTHTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region cap 2
                        if (educationID == 2)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRowTHCSTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "THCS");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel37);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel02);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + (startRow - 1) + "/IF(D" + (startRow - 1) + "<=0,1,D" + (startRow - 1) + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel42 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel42.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isSpace = false;
                                            isNull = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }
                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHCSTemp, "A" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHCSTemp, "B" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHCSTemp, "C" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHCSTemp, "D" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHCSTemp, "E" + (startRowTHCSTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHCSTemp, "F" + (startRowTHCSTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHCSTemp, "A" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHCSTemp, "B" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHCSTemp, "C" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHCSTemp, "D" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHCSTemp, "E" + (startRowTHCSTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHCSTemp, "F" + (startRowTHCSTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region cap 3
                        if (educationID == 3)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            startRowTHPTTemp = startRow;
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                curSheet.SetCellValue("C" + startRow, "THPT");
                                curSheet.SetCellValue("D" + startRow, objMarkTemp.MarkLevel38);
                                curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel03);
                                //curSheet.SetCellValue("F" + startRow, "=E" + startRow + "/D" + startRow + "%");
                                curSheet.SetCellValue("F" + startRow, "=ROUND(E" + (startRow - 1) + "/IF(D" + (startRow - 1) + "<=0,1,D" + (startRow - 1) + "),3)*100");
                                if (objMarkTemp != null && objMarkTemp.MarkLevel43 != null)
                                {
                                    lstPupilID = objMarkTemp.MarkLevel43.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                    lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                    for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                    {
                                        objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                        if (objPupil != null)
                                        {
                                            isSpace = false;
                                            isNull = false;
                                            countPupil++;
                                            curSheet.SetCellValue("G" + startRow, objPupil.PupilName);
                                            curSheet.SetCellValue("H" + startRow, objPupil.ClassName);
                                            curSheet.SetCellValue("I" + startRow, objPupil.GenderName);
                                            curSheet.SetCellValue("J" + startRow, objPupil.EthenicName);
                                            curSheet.SetCellValue("K" + startRow, objPupil.Address);
                                            curSheet.SetCellValue("L" + startRow, objPupil.ParentName);
                                            if (objPupil.ClassType == 1)
                                            {
                                                curSheet.SetCellValue("M" + startRow, "x");
                                            }
                                            else if (objPupil.ClassType == 2 || objPupil.ClassType == 3)
                                            {
                                                curSheet.SetCellValue("N" + startRow, "x");
                                            }
                                            else
                                            {
                                                curSheet.SetCellValue("O" + startRow, "x");
                                            }
                                            startRow++;
                                        }
                                    }
                                    if (isNull == false)
                                        startRow--;
                                }

                            }
                            if (countPupil > 0)
                            {
                                curSheet.GetRange("A" + startRowTHPTTemp, "A" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("B" + startRowTHPTTemp, "B" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("C" + startRowTHPTTemp, "C" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("D" + startRowTHPTTemp, "D" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("E" + startRowTHPTTemp, "E" + (startRowTHPTTemp + countPupil - 1)).Merge();
                                curSheet.GetRange("F" + startRowTHPTTemp, "F" + (startRowTHPTTemp + countPupil - 1)).Merge();
                            }
                            else
                            {
                                curSheet.GetRange("A" + startRowTHPTTemp, "A" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("B" + startRowTHPTTemp, "B" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("C" + startRowTHPTTemp, "C" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("D" + startRowTHPTTemp, "D" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("E" + startRowTHPTTemp, "E" + (startRowTHPTTemp + countPupil)).Merge();
                                curSheet.GetRange("F" + startRowTHPTTemp, "F" + (startRowTHPTTemp + countPupil)).Merge();
                            }
                        }
                        #endregion
                        #region MN
                        if (educationID == 4)
                        {
                            objMarkTemp = lstMark.Where(x => x.SchoolID == itemSchool.SchoolID && x.ProcessedDate == DateTemp && x.DistrictID == itemSchool.DistrictID).FirstOrDefault();
                            curSheet.SetCellValue("A" + startRow, stt);
                            curSheet.SetCellValue("B" + startRow, itemSchool.SchoolName);
                            curSheet.GetRange("B" + startRow, "B" + startRow).SetHAlign(VTHAlign.xlHAlignLeft);
                            if (objMarkTemp != null)
                            {
                                sumPupilRow = sumPupilRow + "D" + startRow + "+";
                                sumPupilAbsenceRow = sumPupilAbsenceRow + "E" + startRow + "+";
                                index = startRow;
                                //curSheet.SetCellValue("C" + startRow, "TH");
                                curSheet.SetCellValue("D" + startRow, (objMarkTemp.MarkLevel39 == -1 ? 0 : objMarkTemp.MarkLevel39) + (objMarkTemp.MarkLevel40 == -1 ? 0 : objMarkTemp.MarkLevel40));

                                #region fill nhà trẻ và mầm non
                                startTempMN = startRow;
                                for (int l = 0; l < 2; l++)
                                {
                                    // nhà trẻ                                    
                                    if (l == 0)
                                    {
                                        #region nếu có nhà trẻ mới xuất
                                        if (objMarkTemp != null && objMarkTemp.MarkLevel39 != -1)
                                        {
                                            curSheet.SetCellValue("C" + startRow, "Nhà trẻ");
                                            curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel39 == -1 ? 0 : objMarkTemp.MarkLevel39);
                                            curSheet.SetCellValue("F" + startRow, objMarkTemp.MarkLevel04);
                                            //curSheet.SetCellValue("G" + startRow, "=F" + startRow + "/E" + startRow + "%");
                                            curSheet.SetCellValue("G" + startRow, "=ROUND(F" + (startRow - 1) + "/IF(E" + (startRow - 1) + "<=0,1,E" + (startRow - 1) + "),3)*100");
                                            if (objMarkTemp.MarkLevel44 != null)
                                            {
                                                lstPupilID = objMarkTemp.MarkLevel44.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                                lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                                for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                                {
                                                    objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                                    if (objPupil != null)
                                                    {
                                                        isSpace = false;
                                                        countPupilNT++;
                                                        curSheet.SetCellValue("H" + startRow, objPupil.PupilName);
                                                        curSheet.SetCellValue("I" + startRow, objPupil.ClassName);
                                                        curSheet.SetCellValue("J" + startRow, objPupil.GenderName);
                                                        curSheet.SetCellValue("K" + startRow, objPupil.EthenicName);
                                                        curSheet.SetCellValue("L" + startRow, objPupil.Address);
                                                        curSheet.SetCellValue("M" + startRow, objPupil.ParentName);
                                                        startRow++;
                                                    }
                                                }
                                                int startRowNT = startTempMN + countPupilNT - 1;
                                                //curSheet.GetRange("A" + startTempMN, "A" + (startTowTemp + countPupilMG + countPupilNT - 1)).Merge();
                                                //curSheet.GetRange("B" + startTempMN, "B" + (startTowTemp + countPupilMG + countPupilNT - 1)).Merge();
                                                //curSheet.GetRange("D" + startTempMN, "D" + (startTowTemp + countPupilMG + countPupilNT - 1)).Merge();

                                                curSheet.GetRange("C" + startTempMN, "C" + startRowNT).Merge();
                                                curSheet.GetRange("E" + startTempMN, "E" + startRowNT).Merge();
                                                curSheet.GetRange("F" + startTempMN, "F" + startRowNT).Merge();
                                                curSheet.GetRange("G" + startTempMN, "G" + startRowNT).Merge();
                                                startRow = startRow - 1;
                                            }
                                        }
                                        #endregion
                                        startRow++;
                                    }
                                    if (l == 1)
                                    {
                                        #region nếu có mẫu giáo mới xuất
                                        if (objMarkTemp != null && objMarkTemp.MarkLevel39 != null)
                                            startTempMN = startRow;

                                        if (objMarkTemp != null && objMarkTemp.MarkLevel40 != -1)
                                        {
                                            startTempMN = startRow;
                                            curSheet.SetCellValue("C" + startRow, "Mẫu giáo");
                                            curSheet.SetCellValue("E" + startRow, objMarkTemp.MarkLevel40 == -1 ? 0 : objMarkTemp.MarkLevel40);
                                            curSheet.SetCellValue("F" + startRow, objMarkTemp.MarkLevel05);
                                            //curSheet.SetCellValue("G" + startRow, "=F" + startRow + "/E" + startRow + "%");
                                            curSheet.SetCellValue("G" + startRow, "=ROUND(F" + (startRow - 1) + "/IF(E" + (startRow - 1) + "<=0,1,E" + (startRow - 1) + "),3)*100");
                                            if (objMarkTemp.MarkLevel45 != null)
                                            {
                                                lstPupilID = objMarkTemp.MarkLevel45.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).Distinct().ToList();
                                                lstPupilTemp = lstPupilAll.Where(x => lstPupilID.Contains(x.PupilID) && x.SchoolID == objMarkTemp.SchoolID).Distinct().ToList();

                                                for (int itemPP = 0; itemPP < lstPupilID.Count(); itemPP++)
                                                {
                                                    objPupil = lstPupilTemp.Where(x => x.PupilID == lstPupilID[itemPP]).FirstOrDefault();
                                                    if (objPupil != null)
                                                    {
                                                        isSpace = false;
                                                        countPupilMG++;
                                                        curSheet.SetCellValue("H" + startRow, objPupil.PupilName);
                                                        curSheet.SetCellValue("I" + startRow, objPupil.ClassName);
                                                        curSheet.SetCellValue("J" + startRow, objPupil.GenderName);
                                                        curSheet.SetCellValue("K" + startRow, objPupil.EthenicName);
                                                        curSheet.SetCellValue("L" + startRow, objPupil.Address);
                                                        curSheet.SetCellValue("M" + startRow, objPupil.ParentName);
                                                        startRow++;
                                                    }
                                                }
                                                int startRowNT = startTempMN + countPupilMG - 1;

                                                curSheet.GetRange("C" + startTempMN, "C" + startRowNT).Merge();
                                                curSheet.GetRange("E" + startTempMN, "E" + startRowNT).Merge();
                                                curSheet.GetRange("F" + startTempMN, "F" + startRowNT).Merge();
                                                curSheet.GetRange("G" + startTempMN, "G" + startRowNT).Merge();
                                                startRow = startRow - 1;
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            if (countPupilMG != 0 || countPupilNT != 0)
                            {
                                if (countPupilMG == 0 || countPupilNT == 0)
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilNT + countPupilMG)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilNT + countPupilMG)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilNT + countPupilMG)).Merge();
                                }
                                else
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilNT + countPupilMG - 1)).Merge();
                                }
                            }
                            else
                            {
                                if (objMarkTemp != null && (objMarkTemp.MarkLevel39 == -1 || objMarkTemp.MarkLevel40 == -1))
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilMG + countPupilNT)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilMG + countPupilNT)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilMG + countPupilNT)).Merge();
                                }
                                else
                                {
                                    curSheet.GetRange("A" + index, "A" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                    curSheet.GetRange("B" + index, "B" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                    curSheet.GetRange("D" + index, "D" + (index + countPupilMG + countPupilNT + 1)).Merge();
                                }
                            }
                        }
                        #endregion
                        stt++;
                    }
                    #region fill từng quận/huyện

                    if (lstSchoolProfile.Count() > 0)// && (educationID == 1 || educationID == 2 || educationID == 3))
                    {
                        if (educationID == 4)
                        {
                            rowDistrict = DistrictTitle;
                            curSheet.SetCellValue(rowDistrict, 4, sumPupilRow.Substring(0, sumPupilRow.Length - 1));
                            curSheet.GetRange("D" + rowDistrict, "E" + rowDistrict).Merge();
                            curSheet.SetCellValue(rowDistrict, 6, sumPupilAbsenceRow.Substring(0, sumPupilAbsenceRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 7, "=ROUND(E" + (rowDistrict - 1) + "/IF(D" + (rowDistrict - 1) + "<=0,1,D" + (rowDistrict - 1) + "),3)*100");
                        }
                        else
                        {
                            rowDistrict = DistrictTitle;
                            curSheet.SetCellValue(rowDistrict, 4, sumPupilRow.Substring(0, sumPupilRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 5, sumPupilAbsenceRow.Substring(0, sumPupilAbsenceRow.Length - 1));
                            curSheet.SetCellValue(rowDistrict, 6, "=ROUND(E" + (rowDistrict - 1) + "/IF(D" + (rowDistrict - 1) + "<=0,1,D" + (rowDistrict - 1) + "),3)*100");
                        }

                    }
                    else //if ((educationID == 1 || educationID == 2 || educationID == 3))
                    {
                        if (educationID == 4)
                        {
                            rowDistrict = DistrictTitle;
                            curSheet.SetCellValue(rowDistrict, 4, 0);
                            curSheet.GetRange("D" + rowDistrict, "E" + rowDistrict).Merge();
                            curSheet.SetCellValue(rowDistrict, 6, 0);
                            //curSheet.SetCellValue(rowDistrict, 7, "=E" + (rowDistrict) + "/D" + (rowDistrict) + "%");
                            curSheet.SetCellValue(rowDistrict, 7, "=ROUND(E" + (rowDistrict - 1) + "/IF(D" + (rowDistrict - 1) + "<=0,1,D" + (rowDistrict - 1) + "),3)*100");
                        }
                        else
                        {
                            rowDistrict = DistrictTitle;
                            curSheet.SetCellValue(rowDistrict, 4, 0);
                            curSheet.SetCellValue(rowDistrict, 5, 0);
                            //curSheet.SetCellValue(rowDistrict, 6, "=E" + (rowDistrict) + "/D" + (rowDistrict) + "%");
                            curSheet.SetCellValue(rowDistrict, 6, "=ROUND(E" + (rowDistrict - 1) + "/IF(D" + (rowDistrict - 1) + "<=0,1,D" + (rowDistrict - 1) + "),3)*100");
                        }

                    }
                    #endregion
                    startRow++;
                }
                #region fill toàn tỉnh              
                curSheet.SetCellValue(9, 4, sumPupilRowAll.Substring(0, sumPupilRowAll.Length - 1));
                if (educationID == 1 || educationID == 2 || educationID == 3)
                {
                    curSheet.SetCellValue(9, 5, sumPupilAbsenceRowAll.Substring(0, sumPupilAbsenceRowAll.Length - 1));
                    //curSheet.SetCellValue(9, 6, "=E9/D9%");
                    curSheet.SetCellValue(9, 6, "=ROUND(E" + 9 + "/IF(D" + 9 + "<=0,1,D" + 9 + "),3)*100");
                    curSheet.GetRange(10, 1, startRow - 1, 16).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    curSheet.Name = DateTemp.Day + "." + DateTemp.Month;
                }
                else if (educationID == 4)
                {
                    curSheet.SetCellValue(9, 6, sumPupilAbsenceRowAll.Substring(0, sumPupilAbsenceRowAll.Length - 1));
                    curSheet.SetCellValue(9, 7, "=ROUND(E" + 9 + "/IF(D" + 9 + "<=0,1,D" + 9 + "),3)*100");
                    //curSheet.SetCellValue(9, 7, "=E9/D9%");
                    curSheet.GetRange(10, 1, startRow - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    curSheet.Name = DateTemp.Day + "." + DateTemp.Month + " MN";
                }
                curSheet.DeleteRow(9);
                #endregion
                curSheet.SetFontName("Times New Roman", 0);
                DateTemp = DateTemp.AddDays(1);
            }
            #endregion
            firstSheet.Delete();
            secondSheet.Delete();
            return oBook.ToStream();
        }
    }
}
