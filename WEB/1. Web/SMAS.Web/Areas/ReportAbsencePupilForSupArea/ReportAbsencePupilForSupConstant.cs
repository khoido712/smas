﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea
{
    public class ReportAbsencePupilForSupConstant
    {
        public const string LIST_TYPESTATICS = "LIST_TYPESTATICS";
        public const string LIST_AcademicYear = "LIST_AcademicYear";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
    }
}