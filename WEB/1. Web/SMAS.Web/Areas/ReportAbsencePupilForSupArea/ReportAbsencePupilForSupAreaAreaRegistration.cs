﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea
{
    public class ReportAbsencePupilForSupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportAbsencePupilForSupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportAbsencePupilForSupArea_default",
                "ReportAbsencePupilForSupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
