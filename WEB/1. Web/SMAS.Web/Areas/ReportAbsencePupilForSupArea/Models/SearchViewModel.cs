﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea.Models
{
    public class SearchViewModel
    {
        public int ReportType { get; set; }
        public int Year { get; set; }
        public bool Grade1 { get; set; }
        public bool Grade2 { get; set; }
        public bool Grade3 { get; set; }
        public bool Grade4 { get; set; }        
        public int AcademicYearID { get; set; }
        public int? DistrictID { get; set; }
        public int? FromDate { get; set; }
        public int? ToDate { get; set; }
    }
}