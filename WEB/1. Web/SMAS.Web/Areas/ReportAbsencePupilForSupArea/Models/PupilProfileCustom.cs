﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea.Models
{
    public class PupilProfileCustom
    {
        public int SchoolID { get; set; }
        public int PupilID { get; set; }
        public string PupilName { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int? Gender { get; set; }
        public string GenderName 
        {
            get
            {
                if(Gender == 0)
                    return "Nữ";
                else if(Gender == 1)
                    return "Nam";
                else
                    return "";
            }
        }
        // 1 : nội trú, 2: bán trú, 3: bán trú dân nuôi
        public int? ClassType { get; set; }

        public string EthenicName { get; set; }
        public string Address { get; set; }
        public string ParentName { get; set; }
        
    }
}