﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportAbsencePupilForSupArea.Models
{
    public class ReportGetListPupilAbsence
    {
        public int districtID { get; set; }
        public int schoolID { get; set; }
        public int educationLevelID { get; set; }
        public int pupilID { get; set; }
        public string pupilName { get; set; }
        public int classID { get; set; }
        public string className { get; set; }
        public bool gender { get; set; }
        public int ethnicID { get; set; }
        public string ethnicNam { get; set; }
        public string address { get; set; }
        public string parentName { get; set; }
    }
}