/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DistributeProgramArea
{
    public class DistributeProgramConstants
    {
        public const string LIST_SUBJECT = "lstSubject";
        public const string LIST_ASSIGN_SUBJECT = "lstAssignSubject";
        public const string LIST_EDUCATION_LEVEL = "lstLevel";
        public const string LIST_CLASS = "lstClass";
        public const string LIST_WEEK = "lstWeek";
        public const string LIST_DISTRIBUTE_PROGRAM = "lstDistributeProgram";
        public const bool AUTO_GEN_WEEK_IF_NULL = true;
        public const string CHECK_CURRENT_ADCADEMIC_YEAR = "0";
        public const string LIST_PPCT = "0";
        public const string WEEK_SELECTED = "WeekSelected";
        public const string START_DATE = "START_DATE";
        public const string PERMISSION_PPCT_FROM_SYSTEM = "permission_ppct_from_system";

        /// <summary>
        /// So tiet PCTT duoc khai bao
        /// </summary>
        public const int MAX_TIET_PPCT = 500;
    }
}