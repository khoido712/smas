using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.DistributeProgramArea.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.DistributeProgramArea;
using SMAS.Web.Utils;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.DistributeProgramArea.Controllers
{
    public class DistributeProgramController : BaseController
    {
        private readonly IDistributeProgramBusiness DistributeProgramBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISchoolWeekBusiness SchoolWeekBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAssignSubjectConfigBusiness AssignSubjectConfigBusiness;
        private readonly IDistributeProgramSystemBusiness DistributeProgramSystemBusiness;

        public DistributeProgramController(
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDistributeProgramBusiness DistributeProgramBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            ISchoolWeekBusiness SchoolWeekBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            IEmployeeBusiness employeeBusiness,
            IClassProfileBusiness classProfileBusiness,
            IAssignSubjectConfigBusiness assignSubjectConfigBusiness,
            IDistributeProgramSystemBusiness distributeProgramSystemBusiness
        )
        {
            this.DistributeProgramBusiness = DistributeProgramBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SchoolWeekBusiness = SchoolWeekBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.AssignSubjectConfigBusiness = assignSubjectConfigBusiness;
            this.DistributeProgramSystemBusiness = distributeProgramSystemBusiness;
        }
        public ActionResult Index()
        {
            //ViewData[DistributeProgramConstants.CHECK_CURRENT_ADCADEMIC_YEAR] = 0;//Ko cho phep xem thong tin

            //AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //if (checkDateInRange(DateTime.Now, academicYear.FirstSemesterStartDate.Value, academicYear.SecondSemesterEndDate.Value))
            //{
            //    ViewData[DistributeProgramConstants.CHECK_CURRENT_ADCADEMIC_YEAR] = 1;//Cho phep xem thong tin
            //}
            getDataView();
            return View();
        }
        public bool checkDateInRange(DateTime myDate, DateTime intStart, DateTime intEnd)
        {
            return ((intStart <= myDate) && (myDate <= intEnd));
        }
        public void getDataView()
        {
            //Init Dic
            #region [Init Dic]
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int selectedEducationLevel = 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
            dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
            #endregion

            //Get List Subject
            #region [Get List Subject]
            List<SubjectCat> subjectList = SchoolSubjectBusiness.SearchBySchool(
                _globalInfo.SchoolID.GetValueOrDefault(), dic).Select(x => x.SubjectCat).
                Distinct().OrderBy(x => x.OrderInSubject).ToList();
            ViewData[DistributeProgramConstants.LIST_SUBJECT] = subjectList != null ? new SelectList(subjectList, "SubjectCatID", "SubjectName") : new SelectList(new List<SubjectCat>(), "SubjectCatID", "SubjectName");
            #endregion

            //Get List Assign Subject
            #region [Get List Assign Subject]
            if (subjectList != null && subjectList.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(0,subjectList[0].SubjectCatID), "AssignSubjectConfigID", "AssignSubjectName");
            }
            else
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(), "AssignSubjectConfigID", "AssignSubjectName");
            }
            #endregion

            //Get List EducationLevel
            #region [Get List EducationLevel]
            ViewData[DistributeProgramConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels != null ? new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution", selectedEducationLevel) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            #endregion

            //Get Class List
            #region [Get Class List]
            IEnumerable<ClassProfile> lstClass = GetClassListByEducationLevelId(_globalInfo.EducationLevels[0].EducationLevelID);
            if (_globalInfo.EducationLevels != null && _globalInfo.EducationLevels.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            }
            #endregion

            //Get PPCT
            #region [Get PPCT]
            //List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            //List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, new List<int>());
            //var items = new List<SelectListItem>();
            //foreach (int x in lstAvailablePPCT)
            //{
            //    items.Add(new SelectListItem()
            //    {
            //        Value = x.ToString(),
            //        Text = x.ToString(),
            //    });
            //}
            //ViewData[DistributeProgramConstants.LIST_PPCT] = new SelectList(items, "Value", "Text");
            #endregion

            //Get List Week
            #region [Get List Week]
           
            // lấy ra tất cả các lớp của trường để tạo schoolweek
            var _lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            List<int> lstClassID = _lstClass.Where(x => x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                            .Select(x => x.ClassProfileID).ToList();
            dic["lstClassID"] = lstClassID;
            int ClassID = lstClass.Count() > 0 ? lstClass.FirstOrDefault().ClassProfileID : 0;
            dic["ClassID"] = ClassID;
 
            this.SchoolWeekBusiness.AutoGenerateWeek(dic); 

            List<SchoolWeek> lstSW = SchoolWeekBusiness.Search(dic).ToList();
            List<SchoolWeekViewModel> lstSWView = new List<SchoolWeekViewModel>();
            SchoolWeekViewModel schoolWeekTemp;
            foreach (SchoolWeek sw in lstSW)
            {
                schoolWeekTemp = new SchoolWeekViewModel();
                schoolWeekTemp.SchoolWeekID = sw.SchoolWeekID;
                schoolWeekTemp.WeekView = sw.OrderWeek.ToString() + " (" + string.Format("{0:dd/MM/yyyy}", sw.FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", sw.ToDate) + ")";
                schoolWeekTemp.FromDateStr = string.Format("{0:dd/MM/yyyy}", sw.FromDate);
                schoolWeekTemp.ToDateStr = string.Format("{0:dd/MM/yyyy}", sw.ToDate);
                lstSWView.Add(schoolWeekTemp);
            }
            ViewData[DistributeProgramConstants.LIST_WEEK] = new SelectList(lstSWView, "SchoolWeekID", "WeekView");
            #endregion
        }
        public List<int> getListPPCTAvailable(List<DistributeProgramBO> lstDistributeProgram, List<int> exceptValue)
        {
            List<int> lstPPCT = new List<int>();
            foreach (DistributeProgramBO dpBO in lstDistributeProgram)
            {
                lstPPCT.Add(dpBO.DistributeProgramOrder.Value);
            }
            List<int> range = Enumerable.Range(1, DistributeProgramConstants.MAX_TIET_PPCT).ToList();
            return range.Except(lstPPCT.Except(exceptValue)).ToList();

        }
        private List<DistributeProgramBO> GetDataSearch(SearchViewModel model)
        {
            Utils.Utils.TrimObject(model);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>() { 
                {"SubjectID",model.SubjectID},
                {"EducationLevelID",model.EducationLevelID},
                {"SchoolWeekID",model.SchoolWeekID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",model.ClassID},
                {"AssignSubjectID",model.AssignSubjectID}
            };
            return DistributeProgramBusiness.getListDistributeProgram(SearchInfo);
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult Search(SearchViewModel frm)
        {
            List<DistributeProgramBO> lstDistributeProgram = GetDataSearch(frm);
            for (int i = 0; i < lstDistributeProgram.Count; i++)
            {
                lstDistributeProgram[i].WeekName = lstDistributeProgram[i].OrderWeek + " (" + string.Format("{0:dd/MM/yyyy}", lstDistributeProgram[i].FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", lstDistributeProgram[i].ToDate) + ")";
            }
            ViewData[DistributeProgramConstants.LIST_DISTRIBUTE_PROGRAM] = lstDistributeProgram;
            //gen PPCT
            List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, new List<int>());
            var items = new List<SelectListItem>();
            foreach (int x in lstAvailablePPCT)
            {
                items.Add(new SelectListItem()
                {
                    Value = x.ToString(),
                    Text = x.ToString(),
                });
            }
            ViewData[DistributeProgramConstants.LIST_PPCT] = new SelectList(items, "Value", "Text");
            this.SetViewDataPermission("DistributeProgram", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            #region [PERMISSION PPCT System]
            //var _PPCTDevelopmentStandardBusiness = new PPCTDevelopmentStandardBusiness();
            //_PPCTDevelopmentStandardBusiness.InitData(frm.SubjectID.Value);
            //ViewData[DistributeProgramConstants.PERMISSION_PPCT_FROM_SYSTEM] = _PPCTDevelopmentStandardBusiness.CheckShow(frm.SubjectID.Value, frm.EducationLevelID.Value);

            ViewData[DistributeProgramConstants.PERMISSION_PPCT_FROM_SYSTEM] = this.DistributeProgramSystemBusiness.CheckShowButon(frm.SubjectID.Value, frm.EducationLevelID.Value);
            #endregion
            return PartialView("_List");
        }
        private void SetViewDataWithHasValueCreate(int subjectId, int educationLevelId, int schoolWeekId, int classID, int? assignSubjectID, int? distributeProgramOrder = null)
        {
            //Init Dic
            #region [Init Dic]
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolWeekID"] = 0;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
            dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
            #endregion

            //Get Subject
            #region [Get Subject]
            List<SubjectCat> subjectList = SchoolSubjectBusiness.SearchBySchool(
                _globalInfo.SchoolID.GetValueOrDefault(), dic).Select(x => x.SubjectCat).
                Distinct().OrderBy(x => x.OrderInSubject).ToList();
            ViewData[DistributeProgramConstants.LIST_SUBJECT] = subjectList != null ? new SelectList(subjectList, "SubjectCatID", "SubjectName") : new SelectList(new List<SubjectCat>(), "SubjectCatID", "SubjectName");
            #endregion

            //Get List Assign Subject
            #region [Get List Assign Subject]
            if (subjectList != null && subjectList.Any(x => x.SubjectCatID == subjectId))
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(educationLevelId,subjectId), "AssignSubjectConfigID", "AssignSubjectName");
            }
            else if (subjectList != null && subjectList.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(educationLevelId,subjectList[0].SubjectCatID), "AssignSubjectConfigID", "AssignSubjectName");
            }
            else
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(), "AssignSubjectConfigID", "AssignSubjectName");
            }
            #endregion

            //Get Tiet PPCT
            #region [Get Tiet PPCT]
            dic["SubjectID"] = subjectId;
            dic["EducationLevelID"] = educationLevelId;
            dic["ClassID"] = classID;
            dic["AssignSubjectID"] = assignSubjectID;

            List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            var exceptValue = new List<int>();
            if (distributeProgramOrder.HasValue) { exceptValue.Add(distributeProgramOrder.Value); }
            List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, exceptValue);
            var items = new List<SelectListItem>();
            foreach (int x in lstAvailablePPCT)
            {
                items.Add(new SelectListItem()
                {
                    Value = x.ToString(),
                    Text = x.ToString(),
                });
            }
            ViewData[DistributeProgramConstants.LIST_PPCT] = new SelectList(items, "Value", "Text");
            #endregion

            //Get List EducationLevel
            #region [Get List EducationLevel]
            ViewData[DistributeProgramConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels != null ? new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution", 0) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            #endregion

            //Get Class List
            #region [Get Class List]
            if (_globalInfo.EducationLevels != null && _globalInfo.EducationLevels.Any(x => x.EducationLevelID == educationLevelId))
            {
                ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(GetClassListByEducationLevelId(educationLevelId), "ClassProfileID", "DisplayName");
            }
            else if (_globalInfo.EducationLevels != null && _globalInfo.EducationLevels.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(GetClassListByEducationLevelId(_globalInfo.EducationLevels[0].EducationLevelID), "ClassProfileID", "DisplayName");
            }
            #endregion

            //Get List Week And Selected Week
            #region [Get List Week And Selected Week]
            List<SchoolWeekViewModel> lstSWView = ListSchoolWeekByClass(dic); 

            int selectedSchoolWeekId = 0;
            if (schoolWeekId == 0 && lstDistributeProgram.Count > 0)
            {
                selectedSchoolWeekId = lstDistributeProgram.Where(c => c.SubjectID == subjectId && c.AdcademicYearID == _globalInfo.AcademicYearID.Value).Select(s => s.SchoolWeekID).Max();
            }
            ViewData[DistributeProgramConstants.WEEK_SELECTED] = selectedSchoolWeekId;
            ViewData[DistributeProgramConstants.LIST_WEEK] = new SelectList(lstSWView, "SchoolWeekID", "WeekView");
            #endregion
        }
        public PartialViewResult getCreateView(int subjectId, int educationLevelId, int schoolWeekId, int classID, int? assignSubjectID)
        {
            SetViewDataWithHasValueCreate(subjectId, educationLevelId, schoolWeekId, classID, assignSubjectID);
            return PartialView("_Create");
        }

        #region [Ajax Load Class]
        private IEnumerable<ClassProfile> GetClassListByEducationLevelId(int? educationLevelId = null)
        {
            if (educationLevelId.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() { 
                    {"UserAccountID",_globalInfo.UserAccountID},
                    {"EducationLevelID",educationLevelId.Value},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };

                var classList = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).AsEnumerable();
                if (classList != null)
                {
                    return classList;
                }
            }
            return new List<ClassProfile>();
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClassSearch(int? educationLevelId, int? subjectID)
        {
            IEnumerable<ClassProfile> lstClassDB = GetClassListByEducationLevelId(educationLevelId);
            var lstClass = lstClassDB.Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();

            int ClassID = lstClassDB.ToList().Count() > 0 ? lstClassDB.ToList().FirstOrDefault().ClassProfileID : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;

            List<SchoolWeekViewModel> lstSWView = ListSchoolWeekByClass(dic);       
            var lstWeek = lstSWView.Select(u => new SelectListItem { Value = u.SchoolWeekID.ToString(), Text = u.WeekView, Selected = false }).ToList();

            List<AssignSubjectConfig> lstASC = GetAssignSubjectListBySubjectID(educationLevelId, subjectID).ToList();
            var listASC = lstASC.Select(u => new SelectListItem { Value = u.AssignSubjectConfigID.ToString(), Text = u.AssignSubjectName, Selected = false }).ToList();

            return Json(new { listClass = lstClass, listWeek = lstWeek, listASC = listASC }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSchoolWeek(int classId)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
           
            List<SchoolWeekViewModel> lstSWView = ListSchoolWeekByClass(dic);           
            var lstWeek = lstSWView.Select(u => new SelectListItem { Value = u.SchoolWeekID.ToString(), Text = u.WeekView, Selected = false }).ToList();

            return Json(lstWeek);
        }

        
        #endregion

        //Ajax Load Phan Mon
        #region [Ajax Load Assign Subject]
        private IEnumerable<AssignSubjectConfig> GetAssignSubjectListBySubjectID(int? educatitonLevelID = null, int? subjectID = null)
        {
            if (subjectID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() { 
                    {"SchoolID",_globalInfo.SchoolID},
                    {"SubjectID",subjectID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"EducationLevelID",educatitonLevelID}
                };

                var assignSubjectList = AssignSubjectConfigBusiness.Search(dic).AsEnumerable();
                if (assignSubjectList != null)
                {
                    return assignSubjectList;
                }
            }
            return new List<AssignSubjectConfig>();
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadAssignSubjectSearch(int? educationLevelID,int? subjectID)
        {
            return Json(new SelectList(GetAssignSubjectListBySubjectID(educationLevelID.HasValue ? educationLevelID.Value : 0,subjectID), "AssignSubjectConfigID", "AssignSubjectName"));
        }
        #endregion

        [ValidateAntiForgeryToken]
        public JsonResult Create(DistributeProgramViewModel frm)
        {
            DistributeProgram dpInsert = new DistributeProgram();
            dpInsert.AcademicYearID = _globalInfo.AcademicYearID.Value;
            dpInsert.CreateTime = DateTime.Now;
            dpInsert.DistributeProgramOrder = frm.DistributeProgramOrder;
            dpInsert.EducationLevelID = frm.EducationLevelID.Value;
            dpInsert.ClassID = frm.ClassID.Value;
            dpInsert.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
            dpInsert.SchoolID = _globalInfo.SchoolID.Value;
            dpInsert.SchoolWeekID = frm.SchoolWeekID.Value;
            dpInsert.SubjectID = frm.SubjectID.Value;
            dpInsert.AssignSubjectID = frm.AssignSubjectID;
            dpInsert.UpdateTime = DateTime.Now;
            dpInsert.LessonName = frm.LessonName;
            dpInsert.Note = frm.Note;
            Utils.Utils.TrimObject(dpInsert);
            this.DistributeProgramBusiness.Insert(dpInsert);
            this.DistributeProgramBusiness.Save();
            return Json(new JsonMessage("Lưu thành công"));
        }

        private void SetViewDataWithHasValueEdit(int subjectId, int educationLevelId, int schoolWeekId, int classID, int? assignSubjectID, int? distributeProgramOrder)
        {
            //Init Dic
            #region [Init Dic]
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolWeekID"] = 0;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
            dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
            #endregion

            //Get Subject
            #region [Get Subject]
            List<SubjectCat> subjectList = SchoolSubjectBusiness.SearchBySchool(
                _globalInfo.SchoolID.GetValueOrDefault(), dic).Select(x => x.SubjectCat).
                Distinct().OrderBy(x => x.OrderInSubject).ToList();
            ViewData[DistributeProgramConstants.LIST_SUBJECT] = subjectList != null ? new SelectList(subjectList, "SubjectCatID", "SubjectName") : new SelectList(new List<SubjectCat>(), "SubjectCatID", "SubjectName");
            #endregion

            //Get List Assign Subject
            #region [Get List Assign Subject]
            if (subjectList != null && subjectList.Any(x => x.SubjectCatID == subjectId))
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(0,subjectId), "AssignSubjectConfigID", "AssignSubjectName");
            }
            else if (subjectList != null && subjectList.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(0,subjectList[0].SubjectCatID), "AssignSubjectConfigID", "AssignSubjectName");
            }
            else
            {
                ViewData[DistributeProgramConstants.LIST_ASSIGN_SUBJECT] = new SelectList(GetAssignSubjectListBySubjectID(), "AssignSubjectConfigID", "AssignSubjectName");
            }
            #endregion

            //Get Tiet PPCT
            #region [Get Tiet PPCT]
            dic["SubjectID"] = subjectId;
            dic["EducationLevelID"] = educationLevelId;
            dic["ClassID"] = classID;
            dic["AssignSubjectID"] = assignSubjectID;

            List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, new List<int>() { distributeProgramOrder.Value });
            var items = new List<SelectListItem>();
            foreach (int x in lstAvailablePPCT)
            {
                items.Add(new SelectListItem()
                {
                    Value = x.ToString(),
                    Text = x.ToString(),
                });
            }
            ViewData[DistributeProgramConstants.LIST_PPCT] = new SelectList(items, "Value", "Text");
            #endregion

            //Get List EducationLevel
            #region [Get List EducationLevel]
            ViewData[DistributeProgramConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels != null ? new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution", 0) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            #endregion

            //Get Class List
            #region [Get Class List]
            if (_globalInfo.EducationLevels != null && _globalInfo.EducationLevels.Any(x => x.EducationLevelID == educationLevelId))
            {
                ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(GetClassListByEducationLevelId(educationLevelId), "ClassProfileID", "DisplayName");
            }
            else if (_globalInfo.EducationLevels != null && _globalInfo.EducationLevels.Count > 0)
            {
                ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(GetClassListByEducationLevelId(_globalInfo.EducationLevels[0].EducationLevelID), "ClassProfileID", "DisplayName");
            }
            #endregion

            //Get List Week And Selected Week
            #region [Get List Week And Selected Week]
            List<SchoolWeek> lstSW = SchoolWeekBusiness.Search(dic).ToList();
            List<SchoolWeekViewModel> lstSWView = new List<SchoolWeekViewModel>();
            SchoolWeekViewModel schoolWeekTemp;
            foreach (SchoolWeek sw in lstSW)
            {
                schoolWeekTemp = new SchoolWeekViewModel();
                schoolWeekTemp.SchoolWeekID = sw.SchoolWeekID;
                schoolWeekTemp.WeekView = sw.OrderWeek.ToString() + " (" + string.Format("{0:dd/MM/yyyy}", sw.FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", sw.ToDate) + ")";
                schoolWeekTemp.FromDateStr = string.Format("{0:dd/MM/yyyy}", sw.FromDate);
                schoolWeekTemp.ToDateStr = string.Format("{0:dd/MM/yyyy}", sw.ToDate);
                lstSWView.Add(schoolWeekTemp);
            }

            int selectedSchoolWeekId = 0;
            if (schoolWeekId == 0 && lstDistributeProgram.Count > 0)
            {
                selectedSchoolWeekId = lstDistributeProgram.Where(c => c.SubjectID == subjectId && c.AdcademicYearID == _globalInfo.AcademicYearID.Value).Select(s => s.SchoolWeekID).Max();
            }
            ViewData[DistributeProgramConstants.WEEK_SELECTED] = selectedSchoolWeekId;
            ViewData[DistributeProgramConstants.LIST_WEEK] = new SelectList(lstSWView, "SchoolWeekID", "WeekView");
            #endregion
        }
        public PartialViewResult GetEditView(int id)
        {

            DistributeProgram dp = DistributeProgramBusiness.Find(id);
            DistributeProgramViewModel dpVM = new DistributeProgramViewModel()
            {
                DistributeProgramID = dp.DistributeProgramID,
                DistributeProgramOrder = dp.DistributeProgramOrder,
                SchoolWeekID = dp.SchoolWeekID,
                EducationLevelID = dp.EducationLevelID,
                LessonName = dp.LessonName,
                Note = dp.Note,
                AssignSubjectID = dp.AssignSubjectID,
                ClassID = dp.ClassID,
                SubjectID = dp.SubjectID
            };

            SetViewDataWithHasValueCreate(dp.SubjectID, dp.EducationLevelID, dp.SchoolWeekID, dp.ClassID, dp.AssignSubjectID, dp.DistributeProgramOrder);

            //AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
            //dic["SchoolWeekID"] = 0;
            //dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            //dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
            //dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
            //dic["SubjectID"] = dp.SubjectID;
            //List<SubjectCat> lstSubject = SchoolSubjectBusiness.SearchBySchool(
            //    _globalInfo.SchoolID.GetValueOrDefault(), dic).Select(x => x.SubjectCat).
            //    Distinct().OrderBy(x => x.OrderInSubject).ToList();
            //ViewData[DistributeProgramConstants.LIST_SUBJECT] = lstSubject != null ? new SelectList(lstSubject, "SubjectCatID", "SubjectName", dp.SubjectID) : new SelectList(new List<SubjectCat>(), "SubjectCatID", "SubjectName");
            ////get list edu level
            //ViewData[DistributeProgramConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels != null ? new SelectList(_globalInfo.EducationLevels.Where(p => p.EducationLevelID == dp.EducationLevelID), "EducationLevelID", "Resolution", dp.EducationLevelID) : new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            ////gen PPCT

            //dic["EducationLevelID"] = dp.EducationLevelID;
            //List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            //List<int> exceptValue = new List<int>();
            //exceptValue.Add(dp.DistributeProgramOrder.Value);
            //List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, exceptValue);
            //var items = new List<SelectListItem>();
            //foreach (int x in lstAvailablePPCT)
            //{
            //    items.Add(new SelectListItem()
            //    {
            //        Value = x.ToString(),
            //        Text = x.ToString(),
            //    });
            //}
            //ViewData[DistributeProgramConstants.LIST_PPCT] = new SelectList(items, "Value", "Text");
            ////get list week
            //List<SchoolWeek> lstSW = SchoolWeekBusiness.getListSchoolWeek(dic, DistributeProgramConstants.AUTO_GEN_WEEK_IF_NULL);
            //List<SchoolWeekViewModel> lstSWView = new List<SchoolWeekViewModel>();
            //SchoolWeekViewModel schoolWeekTemp;
            //foreach (SchoolWeek sw in lstSW)
            //{
            //    schoolWeekTemp = new SchoolWeekViewModel();
            //    schoolWeekTemp.SchoolWeekID = sw.SchoolWeekID;
            //    schoolWeekTemp.WeekView = sw.OrderWeek.ToString() + " (" + string.Format("{0:dd/MM/yyyy}", sw.FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", sw.ToDate) + ")";
            //    schoolWeekTemp.FromDateStr = string.Format("{0:dd/MM/yyyy}", sw.FromDate);
            //    schoolWeekTemp.ToDateStr = string.Format("{0:dd/MM/yyyy}", sw.ToDate);
            //    lstSWView.Add(schoolWeekTemp);
            //}
            //ViewData[DistributeProgramConstants.LIST_WEEK] = new SelectList(lstSWView, "SchoolWeekID", "WeekView");

            return PartialView("_Edit", dpVM);
        }
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int DistributeProgramID)
        {
            DistributeProgram dpUpdate = this.DistributeProgramBusiness.Find(DistributeProgramID);
            TryUpdateModel(dpUpdate);
            Utils.Utils.TrimObject(dpUpdate);
            this.DistributeProgramBusiness.Update(dpUpdate);
            this.DistributeProgramBusiness.Save();

            return Json(new JsonMessage("Lưu thành công"));
        }
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int DistributeProgramID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            this.DistributeProgramBusiness.Delete(DistributeProgramID);
            this.DistributeProgramBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAll(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //SearchInfo["SubjectID"] = frm.SubjectID;
            //SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            //SearchInfo["SchoolWeekID"] = frm.SchoolWeekID;
            //SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            //SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            //List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(SearchInfo);

            List<DistributeProgramBO> lstDistributeProgram = GetDataSearch(frm);
            for (int i = 0; i < lstDistributeProgram.Count; i++)
            {
                DistributeProgramBusiness.Delete(lstDistributeProgram[i].DistributeProgramID);
            }
            this.DistributeProgramBusiness.Save();
            ViewData[DistributeProgramConstants.LIST_DISTRIBUTE_PROGRAM] = lstDistributeProgram;
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public PartialViewResult GetWindowClassMultiApplied(int subjectId, int educationLevelId, int schoolWeekId, int classID, int? assignSubjectID, int? distributeProgramOrder)
        {
            var model = new WindowClassMultiAppliedModel();
            var searchModel = new SearchViewModel();
            #region [Get Value For Cell Title]
            var subjectCatEntity = SubjectCatBusiness.Find(subjectId);
            if (subjectCatEntity != null)
            {
                searchModel.SubjectID = subjectCatEntity.SubjectCatID;
                model.SubjectName = subjectCatEntity.SubjectName;
            }
            model.AssignSubjectName = Res.Get("ItSelf");
            if (assignSubjectID.HasValue)
            {
                var assignSubjectConfigEntity = AssignSubjectConfigBusiness.Find(assignSubjectID.Value);
                if (assignSubjectConfigEntity != null)
                {
                    searchModel.AssignSubjectID = assignSubjectID;
                    model.AssignSubjectName = assignSubjectConfigEntity.AssignSubjectName;
                }
            }
            var classEntity = ClassProfileBusiness.Find(classID);
            if (classEntity != null)
            {
                searchModel.ClassID = classID;
                model.ClassName = classEntity.DisplayName;
            }
            #endregion

            //Get List EducationLevel
            #region [Get List EducationLevel]
            var educationLevelEntity = _globalInfo.EducationLevels != null ? _globalInfo.EducationLevels.FirstOrDefault(x => x.EducationLevelID == educationLevelId) : null;
            if (educationLevelEntity != null)
            {
                model.EducationLevelName = educationLevelEntity.Resolution;
                searchModel.EducationLevelID = educationLevelEntity.EducationLevelID;
            }
            #endregion

            //Get Class List
            #region [Get Class List]
            if (educationLevelEntity != null)
            {
                model.ClassList = GetClassListByEducationLevelId(educationLevelId).Where(x => x.ClassProfileID != classID)
                    .Select(x => new WindowClassMultiApplied_ClassItemModel
                    {
                        ClassID = x.ClassProfileID,
                        ClassName = x.DisplayName
                    }).ToList();
            }

            #endregion

            model.SearchModel = searchModel;
            return PartialView("_WindowClassMultiApplied", model);
        }
        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult WindowClassMultiApplied(WindowClassMultiAppliedModel model)
        {
            DistributeProgramBusiness.SetAutoDetectChangesEnabled(false);
            var searchModel = model.SearchModel;
            var distributeProgramEntities = GetDataSearch(searchModel);

            List<int> lstClassID = new List<int>();
            lstClassID.Add(searchModel.ClassID.HasValue ? searchModel.ClassID.Value : 0);
            for (int i = 0; i < model.ClassList.Count; i++)
            {
                if (model.ClassList[i].Checked == true)
                {
                    lstClassID.Add(model.ClassList[i].ClassID);
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["lstClassID"] = lstClassID;
            List<SchoolWeek> lstSchoolWeek = SchoolWeekBusiness.Search(dic).ToList();
            for (int i = 0; i < model.ClassList.Count; i++)
            {
                if (model.ClassList[i].Checked == true)
                {
                    #region [Delete Old Data]
                    searchModel.ClassID = model.ClassList[i].ClassID;
                    var distributeProgramByClassIDEntities = GetDataSearch(searchModel);
                    for (int j = 0; j < distributeProgramByClassIDEntities.Count; j++)
                    {
                        DistributeProgramBusiness.Delete(distributeProgramByClassIDEntities[j].DistributeProgramID);
                    }
                    #endregion

                    #region [Insert New Data]
                    for (int j = 0; j < distributeProgramEntities.Count; j++)
                    {
                        var objSW = lstSchoolWeek.Where(x => x.SchoolWeekID == distributeProgramEntities[j].SchoolWeekID)
                                                    .FirstOrDefault();
                        if (objSW != null)
                        {
                            var objSWeekNew = lstSchoolWeek.Where(x => x.ClassID == model.ClassList[i].ClassID &&
                                                                x.OrderWeek == objSW.OrderWeek).FirstOrDefault();
                            if (objSWeekNew != null)
                            {
                                DistributeProgram dpInsert = new DistributeProgram()
                                {
                                    AcademicYearID = distributeProgramEntities[j].AdcademicYearID,
                                    CreateTime = DateTime.Now,
                                    DistributeProgramOrder = distributeProgramEntities[j].DistributeProgramOrder,
                                    EducationLevelID = distributeProgramEntities[j].EducationLevelID,
                                    ClassID = model.ClassList[i].ClassID,
                                    Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100,
                                    SchoolID = _globalInfo.SchoolID.Value,
                                    SchoolWeekID = Int32.Parse(objSWeekNew.SchoolWeekID.ToString()),
                                    SubjectID = distributeProgramEntities[j].SubjectID,
                                    AssignSubjectID = distributeProgramEntities[j].AssignSubjectID,
                                    UpdateTime = DateTime.Now,
                                    LessonName = distributeProgramEntities[j].LessonName,
                                    Note = distributeProgramEntities[j].Note,
                                };
                                this.DistributeProgramBusiness.Insert(dpInsert);
                            }
                           
                        }   
                    }

                    #endregion
                }
            }
            DistributeProgramBusiness.Save();
            DistributeProgramBusiness.SetAutoDetectChangesEnabled(true);
            return Json(new JsonMessage("Lưu thành công"));
        }

        public PartialViewResult GetWindowConfirmPPCTFromSystem(int subjectId, int educationLevelId, int schoolWeekId, int classID, int? assignSubjectID, int? distributeProgramOrder)
        {
            var model = new WindowConfirmPPCTFromSystemModel();
            var searchModel = new SearchViewModel();
            #region [Get Value For Cell Title]
            var subjectCatEntity = SubjectCatBusiness.Find(subjectId);
            if (subjectCatEntity != null)
            {
                searchModel.SubjectID = subjectCatEntity.SubjectCatID;
                model.SubjectName = subjectCatEntity.SubjectName;
            }
            model.AssignSubjectName = Res.Get("ItSelf");
            if (assignSubjectID.HasValue)
            {
                var assignSubjectConfigEntity = AssignSubjectConfigBusiness.Find(assignSubjectID.Value);
                if (assignSubjectConfigEntity != null)
                {
                    searchModel.AssignSubjectID = assignSubjectID;
                    model.AssignSubjectName = assignSubjectConfigEntity.AssignSubjectName;
                }
            }
            var classEntity = ClassProfileBusiness.Find(classID);
            if (classEntity != null)
            {
                searchModel.ClassID = classID;
                model.ClassName = classEntity.DisplayName;
            }
            #endregion

            //Get List EducationLevel
            #region [Get List EducationLevel]
            var educationLevelEntity = _globalInfo.EducationLevels != null ? _globalInfo.EducationLevels.FirstOrDefault(x => x.EducationLevelID == educationLevelId) : null;
            if (educationLevelEntity != null)
            {
                model.EducationLevelName = educationLevelEntity.Resolution;
                searchModel.EducationLevelID = educationLevelEntity.EducationLevelID;
            }
            #endregion

            model.SearchModel = searchModel;

            #region [Get PPCT System]
            //var _PPCTDevelopmentStandardBusiness = new PPCTDevelopmentStandardBusiness();
            //_PPCTDevelopmentStandardBusiness.InitData(model.SearchModel.SubjectID.Value);

            //model.PPCTDevelopmentStandards = _PPCTDevelopmentStandardBusiness.GetDatas(model.SearchModel.SubjectID.Value, model.SearchModel.EducationLevelID.Value);

            var SearchInfo_DistributeProgramSystem = new Dictionary<string, object>() { 
                {"SubjectID",model.SearchModel.SubjectID.Value},
                {"EducationLevelID",model.SearchModel.EducationLevelID.Value}
            };

            model.DistributeProgramSystemBOs = this.DistributeProgramSystemBusiness.getListDistributeProgramSystem(SearchInfo_DistributeProgramSystem);
            #endregion

            return PartialView("_ConfirmPPCTFromSystem", model);
        }
        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult WindowConfirmPPCTFromSystem(WindowConfirmPPCTFromSystemModel model)
        {
            //b1: xoa data cu
            //b2: insert data moi vao

            DistributeProgramBusiness.SetAutoDetectChangesEnabled(false);

            #region [Delete Old Data]
            var distributeProgramByClassIDEntities = GetDataSearch(model.SearchModel);
            for (int i = 0; i < distributeProgramByClassIDEntities.Count; i++)
            {
                DistributeProgramBusiness.Delete(distributeProgramByClassIDEntities[i].DistributeProgramID);
            }
            #endregion

            #region [Get PPCT Development Standard]
            //var _PPCTDevelopmentStandardBusiness = new PPCTDevelopmentStandardBusiness();
            //_PPCTDevelopmentStandardBusiness.InitData(model.SearchModel.SubjectID.Value);

            //var PPCTDevelopmentStandardEntities = _PPCTDevelopmentStandardBusiness.GetDatas(model.SearchModel.SubjectID.Value, model.SearchModel.EducationLevelID.Value).Where(x => x.AssignSubjectName == model.DistributeProgramSystemBO_AssignSubjectName).ToList();

            var SearchInfo_DistributeProgramSystem = new Dictionary<string, object>() { 
                {"SubjectID",model.SearchModel.SubjectID.Value},
                {"EducationLevelID",model.SearchModel.EducationLevelID.Value},
                {"AssignSubjectName",model.DistributeProgramSystemBO_AssignSubjectName}
            };

            var PPCTDevelopmentStandardEntities = this.DistributeProgramSystemBusiness.getListDistributeProgramSystem(SearchInfo_DistributeProgramSystem);

            #region [Insert New Data]
            if (PPCTDevelopmentStandardEntities.Count > 0) {
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
                dic["SchoolID"] = _globalInfo.SchoolID;                
                dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
                dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
                //Update cho lay ra 1 list

                // lấy ra tất cả các lớp của trường để tạo schoolweek
                var _lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
                List<int> lstClassID = _lstClass.Where(x => x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                            || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                                                            || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                                .Select(x => x.ClassProfileID).ToList();
                dic["lstClassID"] = lstClassID;
                dic["ClassID"] = model.SearchModel.ClassID.Value;
                this.SchoolWeekBusiness.AutoGenerateWeek(dic);
                var lstSchoolWeek = this.SchoolWeekBusiness.Search(dic).ToList();
                for (int i = 0; i < PPCTDevelopmentStandardEntities.Count; i++)
                {
                    //dic["OrderWeek"] = PPCTDevelopmentStandardEntities[i].SchoolWeekID;
                    var schoolWeekEntity = lstSchoolWeek.Where(x => x.OrderWeek == PPCTDevelopmentStandardEntities[i].SchoolWeekID).FirstOrDefault();
                    if (schoolWeekEntity != null) {
                        DistributeProgram dpInsert = new DistributeProgram()
                        {
                            AcademicYearID = _globalInfo.AcademicYearID.Value,
                            CreateTime = DateTime.Now,
                            UpdateTime = DateTime.Now,
                            EducationLevelID = model.SearchModel.EducationLevelID.Value,
                            ClassID = model.SearchModel.ClassID.Value,
                            Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100,
                            SchoolID = _globalInfo.SchoolID.Value,
                            SubjectID = model.SearchModel.SubjectID.Value,
                            AssignSubjectID = model.SearchModel.AssignSubjectID,
                            //Check SchoolWeekID co ton tai trong he thong cua PPCT ?
                            SchoolWeekID = (int)schoolWeekEntity.SchoolWeekID,
                            LessonName = PPCTDevelopmentStandardEntities[i].LessonName,
                            DistributeProgramOrder = PPCTDevelopmentStandardEntities[i].DistributeProgramOrder
                        };
                        this.DistributeProgramBusiness.Insert(dpInsert);
                    }
                }
            }
            #endregion
            #endregion

            if (distributeProgramByClassIDEntities.Count > 0 || PPCTDevelopmentStandardEntities.Count > 0)
            {
                DistributeProgramBusiness.Save();
            }
            DistributeProgramBusiness.SetAutoDetectChangesEnabled(true);

            return Json(new JsonMessage("Lưu thành công"));
        }

        #region [Contants Cell Index]
        private const string cellSchoolID = "R4";
        private const string cellSubjectID = "S4";
        private const string cellAssignSubjectID = "T4";
        private const string cellEducationLevelID = "U4";
        private const string cellClassID = "V4";
        private const string cellTitleFormat = "Môn {0}, phân môn {1}, lớp {2}";
        private const string reportNameFormat = "Mau_Import_PPCT_Mon_{0}_Phan_Mon_{1}_Lop_{2}.xls";
        #endregion

        public FileResult DownloadTemplate(int SubjectID, int? AssignSubjectID, int EducationLevelID, int ClassID /*, int SchoolWeekID*/)
        {
            #region thong tin truong,mon, khoi
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "GV" + "/" + "GV_PPCT.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet tempSheet = oBook.GetSheet(1);
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

            tempSheet.SetCellValue("A2", school.SchoolName);

            #region [Draw Track]
            tempSheet.SetCellValue(cellSchoolID, _globalInfo.SchoolID.Value);
            tempSheet.SetCellValue(cellSubjectID, SubjectID);
            if (AssignSubjectID.HasValue) { tempSheet.SetCellValue(cellAssignSubjectID, AssignSubjectID); }
            tempSheet.SetCellValue(cellEducationLevelID, EducationLevelID);
            tempSheet.SetCellValue(cellClassID, ClassID);

            tempSheet.GetRange(cellSchoolID, cellClassID).SetFontStyle(false, System.Drawing.Color.White, false, 12, false, false);
            #endregion

            #region [Get Value For Cell Title]
            var subjectCatEntity = SubjectCatBusiness.Find(SubjectID);
            string subjectName = string.Empty;
            if (subjectCatEntity != null)
            {
                subjectName = subjectCatEntity.SubjectName;
            }
            string assignSubjectName = Res.Get("ItSelf");
            if (AssignSubjectID.HasValue)
            {
                var assignSubjectConfigEntity = AssignSubjectConfigBusiness.Find(AssignSubjectID.Value);
                if (assignSubjectConfigEntity != null)
                {
                    assignSubjectName = assignSubjectConfigEntity.AssignSubjectName;
                }
            }
            string className = string.Empty;
            var classEntity = ClassProfileBusiness.Find(ClassID);
            if (classEntity != null)
            {
                className = classEntity.DisplayName;
            }
            #endregion

            tempSheet.SetCellValue("A5", string.Format(cellTitleFormat, subjectName, assignSubjectName, className));
            #endregion
            #region lay du lieu phan phoi
            tempSheet.GetRange(7, 1, 7, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = string.Format(reportNameFormat, Utils.Utils.StripVNSignAndSpace(subjectName), Utils.Utils.StripVNSignAndSpace(assignSubjectName), Utils.Utils.StripVNSignAndSpace(className));
            result.FileDownloadName = ReportName;
            #endregion
            return result;
        }
        public FileResult ExportExcel(int SubjectID, int? AssignSubjectID, int EducationLevelID, int ClassID, int SchoolWeekID)
        {
            #region thong tin truong ,mon, phan mon, lop
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "GV" + "/" + "GV_PPCT.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet tempSheet = oBook.GetSheet(1);

            tempSheet.SetCellValue("A2", _globalInfo.SchoolName);

            #region [Draw Track]
            tempSheet.SetCellValue(cellSchoolID, _globalInfo.SchoolID.Value);
            tempSheet.SetCellValue(cellSubjectID, SubjectID);
            if (AssignSubjectID.HasValue) { tempSheet.SetCellValue(cellAssignSubjectID, AssignSubjectID); }
            tempSheet.SetCellValue(cellEducationLevelID, EducationLevelID);
            tempSheet.SetCellValue(cellClassID, ClassID);

            tempSheet.GetRange(cellSchoolID, cellClassID).SetFontStyle(false, System.Drawing.Color.White, false, 12, false, false);
            #endregion

            #region [Get Value For Cell Title]
            var subjectCatEntity = SubjectCatBusiness.Find(SubjectID);
            string subjectName = string.Empty;
            if (subjectCatEntity != null)
            {
                subjectName = subjectCatEntity.SubjectName;
            }
            string assignSubjectName = Res.Get("ItSelf");
            if (AssignSubjectID.HasValue)
            {
                var assignSubjectConfigEntity = AssignSubjectConfigBusiness.Find(AssignSubjectID.Value);
                if (assignSubjectConfigEntity != null)
                {
                    assignSubjectName = assignSubjectConfigEntity.AssignSubjectName;
                }
            }
            string className = string.Empty;
            var classEntity = ClassProfileBusiness.Find(ClassID);
            if (classEntity != null)
            {
                className = classEntity.DisplayName;
            }
            #endregion

            tempSheet.SetCellValue("A5", string.Format(cellTitleFormat, subjectName, assignSubjectName, className));
            #endregion

            #region lay du lieu phan phoi
            //AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID.GetValueOrDefault()},
                {"SchoolWeekID",SchoolWeekID},
                {"SubjectID",SubjectID},
                {"SchoolID",_globalInfo.SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"AssignSubjectID",AssignSubjectID}
            };

            List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            #endregion

            #region fill du lieu
            if (lstDistributeProgram != null && lstDistributeProgram.Count > 0)
            {
                int startRow = 7;
                int startColumn = 2;
                for (int i = 0; i < lstDistributeProgram.Count; i++)
                {
                    tempSheet.SetCellValue(startRow, 1, i + 1);
                    tempSheet.SetCellValue(startRow, startColumn, lstDistributeProgram[i].OrderWeek.ToString());
                    tempSheet.SetCellValue(startRow, startColumn + 1, lstDistributeProgram[i].DistributeProgramOrder);
                    tempSheet.SetCellValue(startRow, startColumn + 2, lstDistributeProgram[i].LessonName);
                    tempSheet.SetCellValue(startRow, startColumn + 3, lstDistributeProgram[i].Note);
                    startRow++;
                }
                tempSheet.GetRange(7, 1, 6 + lstDistributeProgram.Count, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }

            Stream excel = oBook.ToStream();

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = string.Format(reportNameFormat, Utils.Utils.StripVNSignAndSpace(subjectName), Utils.Utils.StripVNSignAndSpace(assignSubjectName), Utils.Utils.StripVNSignAndSpace(className));
            result.FileDownloadName = ReportName;
            #endregion
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (!CheckActionPermissionMinAddReturn())
            {
                return Json(new JsonMessage(Res.Get("Lbl_Role_Error"), "RoleError"));
            }
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res1.ContentType = "text/plain";
                    return res1;
                    //return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                JsonResult res = Json(new JsonMessage(""));
                res.ContentType = "text/plain";
                return res;
                //return Json(new JsonMessage(""));
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(FormCollection frm)
        {
            try
            {
                DistributeProgramBusiness.SetAutoDetectChangesEnabled(false);
                int subjectId = Business.Common.Utils.GetInt(frm["hdfSubjectID"], 0);
                int? assginSubjectId = Business.Common.Utils.GetNullableInt(frm["hdfAssignSubjectID"]);
                int educationLevelId = Business.Common.Utils.GetInt(frm["hdfEducationLevelID"], 0);
                int classId = Business.Common.Utils.GetInt(frm["hdfClassID"], 0);

                int schoolWeekId = Business.Common.Utils.GetInt(frm["hdfWeekID"], 0);
                if (!CheckActionPermissionMinAddReturn())
                {
                    return Json(new JsonMessage(Res.Get("Lbl_Role_Error"), "RoleError"));
                }
                string FilePath = (string)Session["FilePath"];
                IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
                //Lấy sheet 
                IVTWorksheet sheet = oBook.GetSheet(1);
                //neu khong dung template thi ban exception
                #region neu khong dung template thi ban exception
                string SchoolName = sheet.GetCellValue(2, VTVector.dic['A']) != null ? (string)sheet.GetCellValue(2, VTVector.dic['A']) : "";

                if (SchoolName.Trim().Equals(_globalInfo.SchoolName.Trim(), StringComparison.OrdinalIgnoreCase) == false)
                {
                    return Json(new JsonMessage("File dữ liệu không hợp lệ. Vui lòng kiểm tra lại", JsonMessage.ERROR));
                }

                #region [Get Value For Cell Title]
                var subjectCatEntity = SubjectCatBusiness.Find(subjectId);
                string subjectName = string.Empty;
                if (subjectCatEntity != null)
                {
                    subjectName = subjectCatEntity.SubjectName;
                }
                string assignSubjectName = Res.Get("ItSelf");
                if (assginSubjectId.HasValue)
                {
                    var assignSubjectConfigEntity = AssignSubjectConfigBusiness.Find(assginSubjectId.Value);
                    if (assignSubjectConfigEntity != null)
                    {
                        assignSubjectName = assignSubjectConfigEntity.AssignSubjectName;
                    }
                }
                string className = string.Empty;
                var classEntity = ClassProfileBusiness.Find(classId);
                if (classEntity != null)
                {
                    className = classEntity.DisplayName;
                }
                #endregion
                //Lay tieu de mon hoc, khoi hoc
                var cellA5 = sheet.GetCellValue("A5");
                string titleFromFile = cellA5 != null ? cellA5.ToString() : "";
                string titleImport = string.Format(cellTitleFormat, subjectName, assignSubjectName, className);
                if (titleImport.Equals(titleFromFile.Trim(), StringComparison.OrdinalIgnoreCase) == false)
                {
                    String msgErr = String.Format("File dữ liệu không phải PPCT của môn {0}, phân môn {1}, lớp {2}. Vui lòng kiểm tra lại", subjectName, assignSubjectName, className);
                    return Json(new JsonMessage(msgErr, JsonMessage.ERROR));
                }
                #endregion

                //
                //dpInsert.AcademicYearID = _globalInfo.AcademicYearID.Value;
                //dpInsert.CreateTime = DateTime.Now;
                //dpInsert.DistributeProgramOrder = frm.DistributeProgramOrder;
                //dpInsert.EducationLevelID = frm.EducationLevelID.Value;
                //dpInsert.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
                //dpInsert.SchoolID = _globalInfo.SchoolID.Value;
                //dpInsert.SchoolWeekID = frm.SchoolWeekID.Value;
                //dpInsert.SubjectID = frm.SubjectID.Value;
                //dpInsert.UpdateTime = DateTime.Now;
                //dpInsert.LessonName = frm.LessonName;
                //dpInsert.Note = frm.Note;
                //Utils.Utils.TrimObject(dpInsert);
                //this.DistributeProgramBusiness.Insert(dpInsert);
                //this.DistributeProgramBusiness.Save();
                //return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
                dic["SchoolWeekID"] = 0;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["SubjectID"] = subjectId;
                dic["EducationLevelID"] = educationLevelId;
                dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
                dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;
                dic["ClassID"] = classId;
                //List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);

                var searchViewModel = new SearchViewModel() {
                    ClassID = classId,
                    EducationLevelID = educationLevelId,
                    SchoolWeekID = schoolWeekId,
                    SubjectID = subjectId,
                    AssignSubjectID = assginSubjectId
                };

                List<DistributeProgramBO> lstDistributeProgram = GetDataSearch(searchViewModel);
                List<SchoolWeek> lstSW = SchoolWeekBusiness.Search(dic).ToList();
                DistributeProgram dpInsert;
                List<DistributeProgram> lstdpImport = new List<DistributeProgram>();
                int i = 7;
                List<String> lstErr = new List<String>();
                while (sheet.GetCellValue(i, 1) != null || sheet.GetCellValue(i, 2) != null
                       || sheet.GetCellValue(i, 3) != null || sheet.GetCellValue(i, 4) != null)
                {
                    //Validate dữ liệu
                    if (sheet.GetCellValue(i, 2) == null || sheet.GetCellValue(i, 3) == null)
                    {
                        lstErr.Add(String.Format("Hàng {0} dữ liệu tuần hoặc tiết PCTT không hợp lệ", i));
                        break;
                    }

                    int weekOrder = 0;
                    if (!int.TryParse(sheet.GetCellValue(i, 2).ToString(), out weekOrder))
                    {
                        lstErr.Add(String.Format("Hàng {0} tuần PCTT không hợp lệ", i));
                        break;
                    }
                    bool existWeek = lstSW.Any(p => p.OrderWeek == weekOrder);
                    if (!existWeek)
                    {
                        lstErr.Add(String.Format("Hàng {0} tuần PCTT không hợp lệ", i));
                        break;
                    }

                    int tietPPCT = 0;
                    if (!int.TryParse(sheet.GetCellValue(i, 3).ToString(), out tietPPCT))
                    {
                        lstErr.Add(String.Format("Hàng {0} tiết PCTT không hợp lệ", i));
                        break;
                    }

                    if (tietPPCT <= 0 || tietPPCT >DistributeProgramConstants.MAX_TIET_PPCT)
                    {
                        lstErr.Add(String.Format("Hàng {0} tiết PCTT không hợp lệ", i));
                        break;
                    }

                    object topic = sheet.GetCellValue(i, 4);
                    if (topic == null)
                    {
                        lstErr.Add(String.Format("Hàng {0} chưa nhập tên bài học", i));
                        break;
                    }
                    if (topic != null && !String.IsNullOrWhiteSpace(topic.ToString()) && topic.ToString().Replace("\r\n", "").Trim().Length > 250)
                    {
                        lstErr.Add(String.Format("Hàng {0} tên bài học đang > 250 ký tự", i));
                        break;
                    }

                    object notes = sheet.GetCellValue(i, 5);
                    if (notes != null && !String.IsNullOrWhiteSpace(notes.ToString()) && notes.ToString().Replace("\r\n", "").Trim().Length > 250)
                    {
                        lstErr.Add(String.Format("Hàng {0} ghi chú đang > 250 ký tự", i));
                        break;
                    }

                    dpInsert = new DistributeProgram();
                    dpInsert.CreateTime = DateTime.Now;
                    dpInsert.DistributeProgramOrder = tietPPCT; //Int32.Parse(sheet.GetCellValue(i, 3).ToString());
                    dpInsert.EducationLevelID = educationLevelId;
                    dpInsert.ClassID = classId;
                    dpInsert.Last2DigitNumberSchool = _globalInfo.SchoolID.Value % 100;
                    dpInsert.SchoolID = _globalInfo.SchoolID.Value;
                    dpInsert.SchoolWeekID = Int32.Parse(lstSW.Where(x => x.OrderWeek == weekOrder).Select(x => x.SchoolWeekID).FirstOrDefault().ToString());
                    dpInsert.SubjectID = subjectId;
                    dpInsert.AssignSubjectID = assginSubjectId;
                    dpInsert.AcademicYearID = academicYear.AcademicYearID;
                    dpInsert.UpdateTime = DateTime.Now;
                    dpInsert.LessonName = topic.ToString().Trim();
                    dpInsert.Note = notes == null ? "" : notes.ToString().Trim();

                    DistributeProgramBO temp = lstDistributeProgram.Where(x => x.EducationLevelID == educationLevelId
                        && x.DistributeProgramOrder == dpInsert.DistributeProgramOrder
                        && x.AdcademicYearID == academicYear.AcademicYearID && x.schoolID == _globalInfo.SchoolID
                        && x.SubjectID == subjectId).FirstOrDefault();

                    if (temp != null) //update
                    {
                        dpInsert.DistributeProgramID = temp.DistributeProgramID;
                        DistributeProgramBusiness.Update(dpInsert);
                    }
                    else //insert
                    {
                        DistributeProgramBusiness.Insert(dpInsert);
                    }
                    i++;
                }

                if (lstErr.Any() && lstErr.Count > 0)
                {
                    return Json(new JsonMessage("Trong file tồn tại dữ liệu không hợp lệ. Thầy cô vui lòng kiểm tra lại.", JsonMessage.ERROR));
                }
                DistributeProgramBusiness.SaveDetect();
                return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "Success"));
            }
            catch (Exception ex)
            {                
                LogExtensions.ErrorExt(logger, DateTime.Now, "ImportExcel", "",ex);
                return Json(new JsonMessage("File dữ liệu không hợp lệ. Vui lòng kiểm tra lại", JsonMessage.ERROR));

            }
            finally
            {
                DistributeProgramBusiness.SetAutoDetectChangesEnabled(true);
            }
        }
        private bool ImportPPCT(List<DistributeProgramViewModel> lstPPCT)
        {
            return true;

        }
        public JsonResult AjaxLoadClassPPCT(int? EducationLevelID, int? ClassID, int? SubjectID, int? AssignSubjectID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            //get list subject
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolWeekID"] = 0;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["SubjectID"] = SubjectID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ClassID"] = ClassID;
            dic["AssignSubjectID"] = AssignSubjectID;

            List<DistributeProgramBO> lstDistributeProgram = DistributeProgramBusiness.getListDistributeProgram(dic);
            List<int> lstAvailablePPCT = getListPPCTAvailable(lstDistributeProgram, new List<int>());
            var lstPPCT = new List<SelectListItem>();
            foreach (int x in lstAvailablePPCT)
            {
                lstPPCT.Add(new SelectListItem()
                {
                    Value = x.ToString(),
                    Text = x.ToString(),
                });
            }         

            IDictionary<string, object> dicSW = new Dictionary<string, object>();
            dicSW["ClassID"] = ClassID;
            dicSW["SchoolID"] = _globalInfo.SchoolID.Value;
            dicSW["AcademicYearID"] = _globalInfo.AcademicYearID.Value;

            List<SchoolWeekViewModel> lstSWView = ListSchoolWeekByClass(dicSW);       
            var lstWeek = lstSWView.Select(u => new SelectListItem { Value = u.SchoolWeekID.ToString(), Text = u.WeekView, Selected = false }).ToList();

            return Json(new { listPPCT = lstPPCT, listWeek = lstWeek }, JsonRequestBehavior.AllowGet);
        }

        private List<SchoolWeekViewModel> ListSchoolWeekByClass(IDictionary<string, object> dic)
        {
            List<SchoolWeek> lstSW = SchoolWeekBusiness.Search(dic).ToList();
            List<SchoolWeekViewModel> lstSWView = new List<SchoolWeekViewModel>();
            SchoolWeekViewModel schoolWeekTemp;
            foreach (SchoolWeek sw in lstSW)
            {
                schoolWeekTemp = new SchoolWeekViewModel();
                schoolWeekTemp.SchoolWeekID = sw.SchoolWeekID;
                schoolWeekTemp.WeekView = sw.OrderWeek.ToString() + " (" + string.Format("{0:dd/MM/yyyy}", sw.FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", sw.ToDate) + ")";
                schoolWeekTemp.FromDateStr = string.Format("{0:dd/MM/yyyy}", sw.FromDate);
                schoolWeekTemp.ToDateStr = string.Format("{0:dd/MM/yyyy}", sw.ToDate);
                lstSWView.Add(schoolWeekTemp);
            }
            return lstSWView;
        }
    }
}