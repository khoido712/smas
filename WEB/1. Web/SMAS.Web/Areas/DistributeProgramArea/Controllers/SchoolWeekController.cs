﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DistributeProgramArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DistributeProgramArea;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.DistributeProgramArea.Controllers
{
    [SkipCheckRole]
    public class SchoolWeekController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IDistributeProgramBusiness DistributeProgramBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ISchoolWeekBusiness SchoolWeekBusiness;
        public SchoolWeekController(
            ISchoolProfileBusiness SchoolProfileBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IDistributeProgramBusiness DistributeProgramBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness,
            ISchoolWeekBusiness SchoolWeekBusiness,
            IClassProfileBusiness ClassProfileBusiness
        )
        {
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.DistributeProgramBusiness = DistributeProgramBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.SchoolWeekBusiness = SchoolWeekBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        public ActionResult Index(int? EducationID, int? ClassID)
        {
            getData(EducationID, ClassID);
            return View();
        }
        public void getData(int? EducationID, int? ClassID)
        {
            var lstEducation = _globalInfo.EducationLevels.ToList();
            ViewData[DistributeProgramConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution", EducationID);

            int? educationLevelID = EducationID.HasValue ? EducationID : lstEducation.FirstOrDefault().EducationLevelID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID);

            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                    .OrderBy(u => u.DisplayName).ToList();
            // lay tat ca cac lop cua toan truong de tao schoolweek
            List<int> lstClassID = lstClass.Where(x => x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                                                        || x.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                            .Select(x => x.ClassProfileID).ToList();

            lstClass = lstClass.Where(x => x.EducationLevelID == educationLevelID).ToList();
            int classID = lstClass.FirstOrDefault() != null ? lstClass.FirstOrDefault().ClassProfileID : 0;
            ViewData[DistributeProgramConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName", ClassID.HasValue ? ClassID.Value : classID);

            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            dic["ClassID"] = ClassID;
            dic["lstClassID"] = lstClassID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["FirstSemesterStartDate"] = academicYear.FirstSemesterStartDate;
            dic["SecondSemesterEndDate"] = academicYear.SecondSemesterEndDate;

            this.SchoolWeekBusiness.AutoGenerateWeek(dic);
            ViewData[DistributeProgramConstants.START_DATE] = academicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy");
            //ViewData["ClassIdSelected"] = ClassID.HasValue ? ClassID.Value : classID;
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int educationId)
        {
            if (educationId <= 0)
                return Json(new List<SelectListItem>());
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", educationId);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            int classID = lstClass.Count > 0 ? lstClass.FirstOrDefault().ClassProfileID : 0;

            var lstResult = lstClass.Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();
            return Json(new { classID = classID, listClass = lstResult }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Search(int? classId)
        {
            this.SetViewDataPermission("SchoolWeek", _globalInfo.UserAccountID, _globalInfo.IsAdmin);

             List<SchoolWeekViewModel> lstSWView = new List<SchoolWeekViewModel>();
         
            if (!classId.HasValue || classId.Value <= 0)
            {
                ViewData[DistributeProgramConstants.LIST_WEEK] = lstSWView;
                return PartialView("_List");
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dic.Add("ClassID", classId != null ? classId : 0);
            dic.Add("SchoolID", _globalInfo.SchoolID);

            List<SchoolWeek> lstSW = SchoolWeekBusiness.Search(dic).ToList();

            lstSWView = lstSW.Select(sw => new SchoolWeekViewModel
            {
                SchoolWeekID = sw.SchoolWeekID,
                WeekView = sw.OrderWeek.ToString() + " (" + string.Format("{0:dd/MM/yyyy}", sw.FromDate) + " - " + string.Format("{0:dd/MM/yyyy}", sw.ToDate) + ")",
                FromDateStr = string.Format("{0:dd/MM/yyyy}", sw.FromDate),
                ToDateStr = string.Format("{0:dd/MM/yyyy}", sw.ToDate),
                FromDate = sw.FromDate,
                ToDate = sw.ToDate,
                OrderWeek = sw.OrderWeek,
            }).ToList();

            ViewData[DistributeProgramConstants.LIST_WEEK] = lstSWView;

            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            ViewData[DistributeProgramConstants.START_DATE] = academicYear.FirstSemesterStartDate.Value.ToString("dd/MM/yyyy");

            return PartialView("_List");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateSchoolWeek(SchoolWeekUpdateModel frm)
        {
            SchoolWeek dpUpdate = this.SchoolWeekBusiness.Find(frm.SchoolWeekID);
            try
            {
                dpUpdate.FromDate = DateTime.ParseExact(frm.FromDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dpUpdate.ToDate = DateTime.ParseExact(frm.ToDateStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dpUpdate.UpdatedTime = DateTime.Now;
            }
            catch (Exception)
            {
                return Json(new JsonMessage("Nhập ngày không đúng định dạng", JsonMessage.ERROR));
            }

            this.SchoolWeekBusiness.Update(dpUpdate);
            this.SchoolWeekBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AppliedEducationAndSchool(int educationId, int classID,
            string lstSchoolWeek, string lstFromDate, string lstToDate, bool eduOrSchool)
        {
            try
            {
                List<int> listSchoolWeekID = JsonConvert.DeserializeObject<List<int>>(lstSchoolWeek);
                List<string> listFromDate = JsonConvert.DeserializeObject<List<string>>(lstFromDate);
                List<string> listToDate = JsonConvert.DeserializeObject<List<string>>(lstToDate);

                int schoolId = _globalInfo.SchoolID.Value;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dic.Add("SchoolID", schoolId);
                dic.Add("listSchoolWeekID", listSchoolWeekID);
                dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
                dic.Add("ClassIDSelected", classID);

                //eduOrSchool == true => ap dung cho khoi, nguoc lai ap dung cho toan truong
                if (eduOrSchool)
                {
                    dic.Add("EducationLevelID", educationId);
                }

                bool check = SchoolWeekBusiness.AppliedEducationAndSchool(dic, listFromDate, listToDate);

                if (!check)
                {
                    return Json(new JsonMessage("Cập nhật tuần PPCT thất bại", JsonMessage.ERROR));
                }
                    
            }
            catch (Exception)
            {
                return Json(new JsonMessage("Dữ liệu không hợp lệ", JsonMessage.ERROR));
            }

            if(eduOrSchool)
            {
                return Json(new JsonMessage(Res.Get("Cập nhật tuần PPCT cho toàn khối thành công")));
            }

            return Json(new JsonMessage(Res.Get("Cập nhật tuần PPCT cho toàn trường thành công")));
        }   
    }
}