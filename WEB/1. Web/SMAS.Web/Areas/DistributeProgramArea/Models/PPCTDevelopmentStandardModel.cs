﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class PPCTDevelopmentStandardModel
    {
        public int PPCTDevelopmentStandardID { get; set; }
        public int SchoolWeekID { get; set; }
        public int DistributeProgramOrder { get; set; }
        public string LessonName { get; set; }
        public int SubjectID { get; set; }
        public string AssignSubjectName { get; set; }
        public int EducationLevelID { get; set; }
    }

    public class PPCTDevelopmentStandardBusiness
    {
        public List<PPCTDevelopmentStandardModel> Datas { get; set; }

        public PPCTDevelopmentStandardBusiness()
        {
            Datas = new List<PPCTDevelopmentStandardModel>();
        }

        public void InitData(int subjectID)
        {
            Datas = new List<PPCTDevelopmentStandardModel>();
            if (subjectID == 19)
            {
                Datas.Add(new PPCTDevelopmentStandardModel
                {
                    PPCTDevelopmentStandardID = 1,
                    SchoolWeekID = 1,
                    DistributeProgramOrder = 1,
                    LessonName = "§1.Tập hợp.Phần tử của tập hợp",
                    SubjectID = 19,
                    AssignSubjectName = "Đại số",
                    EducationLevelID = 1
                });
                Datas.Add(new PPCTDevelopmentStandardModel
                {
                    PPCTDevelopmentStandardID = 2,
                    SchoolWeekID = 1,
                    DistributeProgramOrder = 2,
                    LessonName = "§2.Tập hợp các số tự nhiên",
                    SubjectID = 19,
                    AssignSubjectName = "Đại số",
                    EducationLevelID = 1
                });
                Datas.Add(new PPCTDevelopmentStandardModel
                {
                    PPCTDevelopmentStandardID = 3,
                    SchoolWeekID = 1,
                    DistributeProgramOrder = 3,
                    LessonName = "§3.Điểm. Đường thẳng",
                    SubjectID = 19,
                    AssignSubjectName = "Hình học",
                    EducationLevelID = 1
                });
            }
            else if(subjectID ==8)
            {
                Datas.Add(new PPCTDevelopmentStandardModel
                {
                    PPCTDevelopmentStandardID = 1,
                    SchoolWeekID = 1,
                    DistributeProgramOrder = 1,
                    LessonName = "§1.Tập Đọc 1",
                    SubjectID = 8,
                    AssignSubjectName = "Tập đọc",
                    EducationLevelID = 1
                });
                Datas.Add(new PPCTDevelopmentStandardModel
                {
                    PPCTDevelopmentStandardID = 2,
                    SchoolWeekID = 1,
                    DistributeProgramOrder = 2,
                    LessonName = "§2.Tập Đọc 2",
                    SubjectID = 8,
                    AssignSubjectName = "Tập đọc",
                    EducationLevelID = 1
                });
            }

        }

        public List<PPCTDevelopmentStandardModel> GetDatas(int subjectID, int educationLevelID)
        {
            return Datas.Where(x => x.SubjectID == subjectID && x.EducationLevelID == educationLevelID).ToList();
        }

        public bool CheckShow(int subjectID, int educationLevelID)
        {
            return Datas.Any(x => x.SubjectID == subjectID && x.EducationLevelID == educationLevelID);
        }
    }
}