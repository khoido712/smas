using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class SchoolWeekViewModel
    {
        [ScaffoldColumn(false)]
        public long SchoolWeekID { get; set; }
        public int OrderWeek { get; set; }
        public string FromDateStr { get; set; }
        [ScaffoldColumn(false)]
        public DateTime ? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string ToDateStr { get; set; }
        public string WeekView { get; set; }
    }
}
