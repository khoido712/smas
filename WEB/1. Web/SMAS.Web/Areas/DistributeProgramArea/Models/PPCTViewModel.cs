using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class PPCTVIewModel
    { 
        public int Value { get; set; }
       
        public int Name { get; set; }
    }
}
