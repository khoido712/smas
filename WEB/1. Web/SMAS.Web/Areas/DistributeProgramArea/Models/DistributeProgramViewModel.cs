/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.DistributeProgramArea;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class DistributeProgramViewModel
    {
        [ScaffoldColumn(false)]
        public long DistributeProgramID { get; set; }

        [ResourceDisplayName("Common_Label_SubjectTitle")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_SUBJECT)]
        [AdditionalMetadata("OnChange", "AjaxLoadAssignSubjectSearchCreate(this)")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("Common_Label_AssignSubjectTitle")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "ItSelf")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_ASSIGN_SUBJECT)]
        [AdditionalMetadata("OnChange", "AjaxLoadPPCT()")]
        public int? AssignSubjectID { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPEducationLevel")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassSearchCreate(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPClass")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_CLASS)]
        [AdditionalMetadata("OnChange", "AjaxLoadPPCT()")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("Common_Label_Week")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_WEEK)]
        public int? SchoolWeekID { get; set; }

        [ResourceDisplayName("Common_Label_DistributeProgramOrder")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_PPCT)]
        public int? DistributeProgramOrder { get; set; }

        [ResourceDisplayName("Common_Label_NameTitleOfSubject")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string LessonName { get; set; }

        [ResourceDisplayName("Common_Label_Note")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}