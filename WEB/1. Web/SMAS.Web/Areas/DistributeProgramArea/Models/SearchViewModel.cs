/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.DistributeProgramArea;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_SubjectTitle")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_SUBJECT)]
        [AdditionalMetadata("OnChange", "AjaxLoadAssignSubjectSearch(this)")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("Common_Label_AssignSubjectTitle")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "ItSelf")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_ASSIGN_SUBJECT)]
        [AdditionalMetadata("OnChange", "AjaxLoadDistributeProgram()")]
        public int? AssignSubjectID { get; set; }

        [ResourceDisplayName("Common_Label_Week")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_WEEK)]
        [AdditionalMetadata("OnChange", "AjaxLoadDistributeProgram()")]
        public int? SchoolWeekID { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPEducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClassSearch(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPClass")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", DistributeProgramConstants.LIST_CLASS)]
        [AdditionalMetadata("OnChange", "AjaxLoadSchoolWeek(this)")]
        public int? ClassID { get; set; }
    }
}