﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class WindowClassMultiAppliedModel
    {
        public WindowClassMultiAppliedModel()
        {
            ClassList = new List<WindowClassMultiApplied_ClassItemModel>();
        }

        public string SubjectName { get; set; }
        public string AssignSubjectName { get; set; }
        public string EducationLevelName { get; set; }
        public string ClassName { get; set; }
        public SearchViewModel SearchModel { get; set; }
        public List<WindowClassMultiApplied_ClassItemModel> ClassList { get; set; }
    }

    public class WindowClassMultiApplied_ClassItemModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public bool Checked { get; set; }
    }
}