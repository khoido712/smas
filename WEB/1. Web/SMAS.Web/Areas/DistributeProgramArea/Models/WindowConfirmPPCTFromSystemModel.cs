﻿using SMAS.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class WindowConfirmPPCTFromSystemModel
    {
        public WindowConfirmPPCTFromSystemModel()
        {
            DistributeProgramSystemBOs = new List<DistributeProgramSystemBO>();
        }

        public string SubjectName { get; set; }
        public string AssignSubjectName { get; set; }
        public string EducationLevelName { get; set; }
        public string ClassName { get; set; }
        public SearchViewModel SearchModel { get; set; }

        public string DistributeProgramSystemBO_AssignSubjectName { get; set; }
        public List<DistributeProgramSystemBO> DistributeProgramSystemBOs { get; set; }
    }
}