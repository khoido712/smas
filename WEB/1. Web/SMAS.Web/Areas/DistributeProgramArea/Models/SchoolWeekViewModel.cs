using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DistributeProgramArea.Models
{
    public class SchoolWeekUpdateModel
    { 
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public int SchoolWeekID { get; set; }
    }
}
