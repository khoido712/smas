﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DistributeProgramArea
{
    public class DistributeProgramAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DistributeProgramArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DistributeProgramArea_default",
                "DistributeProgramArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
