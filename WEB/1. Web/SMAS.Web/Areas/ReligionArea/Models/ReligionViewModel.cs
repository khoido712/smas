using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReligionArea.Models
{
    public class ReligionViewModel 
    {
        [ResourceDisplayName("Religion_Label_ReligionID")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ReligionID { get; set; }

        [ResourceDisplayName("Religion_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Resolution { get; set; }

        [ResourceDisplayName("Religion_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string Description { get; set; }	


    }
}
