﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReligionArea
{
    public class ReligionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReligionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReligionArea_default",
                "ReligionArea/{controller}/{action}/{ID}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
