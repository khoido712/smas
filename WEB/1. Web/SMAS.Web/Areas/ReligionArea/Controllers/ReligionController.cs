﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;



using SMAS.Business.Business;
using SMAS.Business.IBusiness;

using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Constants;
using System.Transactions;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ReligionArea.Controllers
{   
    public class ReligionController : BaseController
    {        
        private readonly IReligionBusiness ReligionBusiness;
	 
		public ReligionController (IReligionBusiness religionBusiness)
		{
			this.ReligionBusiness = religionBusiness;
		}
		
		//
        // GET: /Religion/

        #region index
        public ViewResult Index()
        {
            SetViewData();
            return View("Index");
        }
        #endregion
      


        #region old func
        //
        // GET: /Religion/Details/5

        public ViewResult Details(int id)
        {
            Religion religion = this.ReligionBusiness.Find(id);
            return View(religion);
        }

        //
        // GET: /Religion/Create

        public ActionResult Create()
        {

            return View();
        }

        //
        // POST: /Religion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Religion religion)
        {
            if (ModelState.IsValid)
            {
                this.ReligionBusiness.Insert(religion);
                this.ReligionBusiness.Save();
                return RedirectToAction("Index");
            }


            return View(religion);
        }

        //
        // GET: /Religion/Edit/5

        public ActionResult Edit(int id)
        {
            Religion religion = this.ReligionBusiness.Find(id);

            return View(religion);
        }

        //
        // POST: /Religion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Religion religion)
        {
            if (ModelState.IsValid)
            {
                this.ReligionBusiness.Update(religion);
                return RedirectToAction("Index");
            }

            return View(religion);
        }

        //
        // GET: /Religion/Delete/5


        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Religion religion = this.ReligionBusiness.Find(id);
            return View(religion);
        }

        //
        // POST: /Religion/Delete/5

        [HttpPost, ActionName("Delete")]

        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this.ReligionBusiness.Delete(id);
            return RedirectToAction("Index");
        }

        #endregion



        #region Create
        public ActionResult CreateOrEdit()
        {
            SetViewData();
            return PartialView("_CreateOrEdit");
        }

        //
        // POST: /Religion/Create


        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreateReligion(Religion religion)
        {
          

            if (ModelState.IsValid)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    religion.IsActive = true;
                    this.ReligionBusiness.Insert(religion);
                    this.ReligionBusiness.Save();

                    scope.Complete();
                }
                SetViewData();
                return PartialView("_GridReligion");
            }
            SetViewData();
            return PartialView("_GridReligion");

        }
        #endregion


        #region Detail

        [ValidateAntiForgeryToken]
        public ActionResult GetDetailReligion(int ReligionId)
        {
            Religion ep = ReligionBusiness.Find(ReligionId);
            return PartialView("_DetailReligion", ep);
        }
        #endregion


        #region Edit

        [ValidateAntiForgeryToken]
        public ActionResult GetEditReligion(int ReligionId)
        {
            Religion ep = ReligionBusiness.Find(ReligionId);
            return PartialView("_EditReligion", ep);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult EditReligion(Religion religion)
        {
            if (ModelState.IsValid)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    religion.IsActive = true;
                    this.ReligionBusiness.Update(religion);
                    this.ReligionBusiness.Save();

                    scope.Complete();
                }

                SetViewData();
                return PartialView("_GridReligion");
            }

            SetViewData();
            return PartialView("_GridReligion");
        }
        #endregion


        #region Delete

        [ValidateAntiForgeryToken]
        public ActionResult GetDeleteReligion(int ReligionId)
        {
            SetViewData();
            Religion ep = ReligionBusiness.Find(ReligionId);
            return PartialView("_DeleteReligion", ep);
        }

        //
        // POST: /EmployeePraiseDiscipline/Delete/5
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public ActionResult DeleteReligion(int ReligionId)
        {
            //throw (new Exception("errrr"));

            using (TransactionScope scope = new TransactionScope())
            {
                this.ReligionBusiness.Delete(ReligionId);
                this.ReligionBusiness.Save();

                scope.Complete();
            }
            SetViewData();
            return PartialView("_GridReligion");
        }

        #endregion


        #region Search
        
        public PartialViewResult GetReligionSearch(string searchData)
        {
            SetViewData();
            //search 
            Dictionary<string, object> dic = new Dictionary<string, object>();
            
            string descripton = "";
            bool isActive = true;   
            dic.Add("Resolution", searchData);
            dic.Add("Description", descripton);
            dic.Add("IsActive", isActive);
           
            ViewData[ReligionConstant.LS_RELIGION] = ReligionBusiness.Search(dic).ToList();

            return PartialView("_GridReligion");
        }      
        #endregion


        #region private funcion
        private void SetViewData()
        {

            ViewData[ReligionConstant.LS_RELIGION] = ReligionBusiness.All.Where(em => (em.IsActive == true)).ToList();
        }

        #endregion


    }
}
