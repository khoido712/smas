﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SummanyMarkArea.Models
{
    public class SearchViewModel
    {
        public int EducationLevelID { get; set; }
        public int? ClassID { get; set; }
    }
}