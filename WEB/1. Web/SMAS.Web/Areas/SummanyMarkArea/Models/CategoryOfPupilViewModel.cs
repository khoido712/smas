using System;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SummanyMarkArea.Models
{
    public class CategoryOfPupilViewModel
    {
        #region copy form PupilRankingBO

        /// <summary>
        /// Nam hoc
        /// </summary>
        public int? AcademicYearID { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh (id)
        /// </summary>
        [ResourceDisplayName("PupilRanking_Label_CapacityLevel")]
        public int? CapacityLevelID { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh (name)
        /// </summary>
        [ResourceDisplayName("PupilRanking_Label_CapacityLevel")]
        public string CapacityLevel { get; set; }

        /// <summary>
        /// Hoc luc cua hoc sinh cap 1
        /// </summary>
        public int? CapacityType { get; set; }

        /// <summary>
        /// Lop hoc
        /// </summary>
        public int? ClassID { get; set; }

        public DateTime? ClassMoveDate { get; set; }

        /// <summary>
        /// Nhan xet va danh gia
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// hanh kiem cua hoc sinh (id)
        /// </summary>
        [ResourceDisplayName("PupilRanking_Label_ConductLevel")]
        public int? ConductLevelID { get; set; }

        /// <summary>
        /// Khoi hoc
        /// </summary>
        public int? EducationLevelID { get; set; }

        /// <summary>
        /// hien thi hay disable
        /// </summary>
        public bool? Enable { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string FullName { get; set; }

        [DisplayName("Genre")]
        public int? Genre { get; set; }

        public int? HonourAchivementTypeID { get; set; }

        public DateTime? LeavingDate { get; set; }

        public int? NumberOfFault { get; set; }

        public int? PeriodID { get; set; }
        /// <summary>
        /// Hinh thuc hoc
        /// </summary>
        public int? LearningType { get; set; }

        /// <summary>
        /// Trang thai cua hoc sinh
        /// </summary>
        public int? ProfileStatus { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }

        /// <summary>
        /// danh hieu hoc sinh
        /// </summary>
        public int? PupilEmulationID { get; set; }

        /// <summary>
        /// ID hoc sinh
        /// </summary>
        public int? PupilID { get; set; }

        public long? PupilRankingID { get; set; }

        /// <summary>
        /// Truong hoc
        /// </summary>
        public int? SchoolID { get; set; }

        public DateTime? SchoolMoveDate { get; set; }

        /// <summary>
        /// Hoc ky
        /// </summary>
        public int? Semester { get; set; }

        /// <summary>
        ///hoc sinh thuoc dien
        /// </summary>
        [ResourceDisplayName("CategoryOfPupil_Label_StudyingJudgement")]
        public int? StudyingJudgementID { get; set; }

        /// <summary>
        /// Diem cong hanh kiem
        /// </summary>
        public decimal? sumConductMark { get; set; }

        /// <summary>
        /// Diem tru hanh kiem
        /// </summary>
        public decimal? sumConductMinusMark { get; set; }

        /// <summary>
        /// Tong diem
        /// </summary>
        public decimal? sumMark { get; set; }

        /// <summary>
        /// Diem tru vi pham
        /// </summary>
        public decimal? sumTotalPenalizedMark { get; set; }

        public int? Task1Evaluation { get; set; }

        public int? Task2Evaluation { get; set; }

        public int? Task3Evaluation { get; set; }

        public int? Task4Evaluation { get; set; }

        public int? Task5Evaluation { get; set; }

        /// <summary>
        /// So ngay nghi khong co phep
        /// </summary>
        public int? TotalAbsentDaysWithoutPermission { get; set; }

        /// <summary>
        /// So ngay nghi co phep
        /// </summary>
        public int? TotalAbsentDaysWithPermission { get; set; }

        /// <summary>
        /// Tong diem
        /// </summary>
        public decimal? TotalMark { get; set; }

        public int? Year { get; set; }

        #endregion copy form PupilRankingBO

        #region addtional variable

        /// <summary>
        /// hanh kiem cua hoc sinh (name)
        /// </summary>
        [ResourceDisplayName("PupilRanking_Label_ConductLevel")]
        public string ConductLevelName { get; set; }

        /// <summary>
        ///hoc sinh thuoc dien(name)
        /// </summary>
        [ResourceDisplayName("CategoryOfPupil_Label_StudyingJudgement")]
        public string StudyingJudgementName { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        public string GenreName { get; set; }

        [ResourceDisplayName("Fault_Class_Title")]
        public string ClassName { get; set; }

        public bool IsKXLHK { get; set; }
        [ResourceDisplayName("Message_Label_Status")]
        public string StatusName { get; set; }
        public int SectionKeyID { get; set; }
        #endregion addtional variable
    }
}
