using System.Collections.Generic;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.SummanyMarkArea.Models
{
    public class SummanyMarkViewModel
    {
        public int PupilID { get; set; }

        [ResourceDisplayName("MarkRecord_Label_PupilCode")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("MarkRecord_Label_FullName")]
        public string FullName { get; set; }

        [ResourceDisplayName("Fault_Class_Title")]
        public string ClassName { get; set; }

        public Dictionary<string, object> MarkData { get; set; }

        public string CapacityLevel { get; set; }

        public string ConductLEvel { get; set; }

        public int? CapacityLevelID { get; set; }

        public int? ConductLEvelID { get; set; }

        public int? ClassID { get; set; }

        public int? EducationLevelID { get; set; }
        
        public decimal? AverageMark {get;set;}
        [ResourceDisplayName("MarkRecord_Label_AverageMark")]
        public string AverageMarkStr
        {
            get
            {
                string result = string.Empty;
                if (AverageMark.HasValue)
                {
                    if (AverageMark == 0 || AverageMark == 10)
                    {
                        if (AverageMark == 0)
                        {
                            result = "0.0";
                        }
                        else
                        {
                            result = "10";
                        }
                    }
                    else
                    {
                        result = AverageMark.Value.ToString("0.#").Replace(",",".");
                    }
                }
                return result;
            }
        }

        public SummanyMarkViewModel()
        {
            MarkData = new Dictionary<string, object>();
        }
    }
}
