﻿namespace SMAS.Web.Areas.SummanyMarkArea
{
    public class SummanyMarkConstants
    {
        public const string LS_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LS_CLASS = "ListClass";
        public const string LS_PUPIL = "ListPupil";
        public const string LS_CATEGORYOFPUPIL = " LS_CATEGORYOFPUPIL";
        public const string DISABLE_BUTTON_CATEGORY = "DISABLE_BUTTON_CATEGORY";
        public const string LS_SUBJECT = "ListSubject";
        public const string LS_PUPILRETEST = "ListPupilRetest";
        public const string SELECTED_CLASSID = "SELECTED_CLASSID";
        public const string SELECTED_EDUCATIONLEVELID = "SELECTED_EDUCATIONLEVELID";
        public const string ENABLE_BUTTON_TK = "ENABLE_BUTTON_TK";
        public const string IS_PRIMARY = "IS_PRIMARY";
        public const string FILE_RESULT = "HS_{0}_KetQuaThiLai_{1}.xls"; 
        public const string FILE_RESULT_RETEST = "HS_THCS_{0}_KetQuaThiLaiReLuyenLai_{1}.xls";
    }
}