﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummanyMarkArea
{
    public class SummanyMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummanyMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummanyMarkArea_default",
                "SummanyMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}