﻿using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SummanyMarkArea.Models;
using SMAS.Web.Utils;
using System;
using System.Diagnostics;
using SMAS.Web.Filter;
using System.Configuration;
using SMAS.Web.Controllers;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.SummanyMarkArea.Controllers
{
    public class SummanyMarkController : BaseController
    {
        //
        // GET: /SummanyMarkArea/SummanyMark/
        private readonly IClassProfileBusiness ClassProfileBusiness;

        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IStudyingJudgementBusiness StudyingJudgementBusiness;
        private readonly ICapacityLevelBusiness CapacityLevelBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBu;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        //

        public SummanyMarkController(IClassProfileBusiness classProfileBusiness
            , IEducationLevelBusiness educationLevelBusiness
            , IPupilRetestRegistrationBusiness pupilRetestRegistrationBusiness
            , IPupilRankingBusiness PupilRankingBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IConductLevelBusiness ConductLevelBusiness
            , IStudyingJudgementBusiness StudyingJudgementBusiness
            , ICapacityLevelBusiness CapacityLevelBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IPupilProfileBusiness PupilProfileBusiness
            , IExemptedSubjectBusiness ExemptedSubjectBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IPupilOfClassBusiness pupilOfClassBusiness
            , IPupilRetestRegistrationBusiness pupilRetestRegistrationBu
            , IRegisterSubjectSpecializeBusiness registerSubjectSpecializeBusiness
            , IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness
            , IVPupilRankingBusiness vPupilRankingBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IExamInputMarkBusiness ExamInputMarkBusiness
            , IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.PupilRetestRegistrationBusiness = pupilRetestRegistrationBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ConductLevelBusiness = ConductLevelBusiness;
            this.StudyingJudgementBusiness = StudyingJudgementBusiness;
            this.CapacityLevelBusiness = CapacityLevelBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.RegisterSubjectSpecializeBusiness = registerSubjectSpecializeBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilRetestRegistrationBu = pupilRetestRegistrationBu;
            this.PupilRankingHistoryBusiness = _PupilRankingHistoryBusiness;
            this.VPupilRankingBusiness = vPupilRankingBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;

        }

        #region hath

        public ActionResult Index()
        {
            ViewData[SummanyMarkConstants.LS_EDUCATIONLEVEL] = _globalInfo.EducationLevels.Where(o => o.IsLastYear != true).ToList();
            int EducationLevelID = _globalInfo.EducationLevels.First().EducationLevelID;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = EducationLevelID;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicClass["IsVNEN"] = true;
			

            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<int> lstClassID = new List<int>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;
            }
            else // Nếu là GV
            {
                dicClass.Add("UserAccountID", _globalInfo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;
            }
            return View("Index");
        }

        public List<SummanyMarkViewModel> GetRetestPupils_History(int EducationLevelID, int? ClassID)
        {
            ViewData[SummanyMarkConstants.SELECTED_CLASSID] = ClassID;
            ViewData[SummanyMarkConstants.SELECTED_EDUCATIONLEVELID] = EducationLevelID;
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            SearchSubject["EducationLevelID"] = EducationLevelID;
            SearchSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            List<int> ListClassID = new List<int>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                SearchSubject["ClassID"] = ClassID;
            }
            else // tim kiem theo khoi
            {
                // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
                {
                    SearchSubject["EmployeeID"] = _globalInfo.EmployeeID;
                    ListClassID = ClassProfileBusiness.SearchByTeacherPermission(SearchSubject).Select(o => o.ClassID).ToList();
                    if (ListClassID != null && ListClassID.Count == 0)
                    {
                        ListClassID.Add(0); // Tìm với Ds rỗng
                    }
                    SearchSubject["ListClassID"] = ListClassID;
                }
            }
            //List<ClassSubject> lsClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject).ToList();

            List<ClassSubjectBO> lstClassSubjectBO = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                .Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting,
                    AppliedType = o.AppliedType,
                    Is_Specialize_Subject = o.IsSpecializedSubject
                })
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            List<SubjectCatBO> lstSubjectCatBO = lstClassSubjectBO
                                                .GroupBy(u => new { u.SubjectID, u.DisplayName, u.OrderInSubject })
                                                .Select(u => new SubjectCatBO { SubjectCatID = u.Key.SubjectID, OrderInSubject = u.Key.OrderInSubject, DisplayName = u.Key.DisplayName })
                                                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).Distinct().ToList();

            //List<SubjectCat> lstSubjectCat = lsClassSubject.Select(u => u.SubjectCat).OrderBy(o => o.OrderInSubject).Distinct().ToList();
            ViewData[SummanyMarkConstants.LS_SUBJECT] = lstSubjectCatBO;

            IDictionary<string, object> SearchClass = new Dictionary<string, object>();
            SearchClass["EducationLevelID"] = EducationLevelID;
            SearchClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager || _globalInfo.IsViewAll)
            {
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(o => o.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;
            }
            else
            {
                SearchClass.Add("UserAccountID", _globalInfo.UserAccountID);
                SearchClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(u => u.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;
            }
            List<int> lsClassID = ClassID.HasValue ? new List<int> { ClassID.Value } : lsClass.Select(u => u.ClassProfileID).ToList();
            List<PupilRetestToCategory> lsPupilRetest = PupilRetestRegistrationBusiness.CategoryPupilAfterRetest_History(lsClassID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, true);

            List<SummanyMarkViewModel> lsSummanyMarkViewModel = new List<SummanyMarkViewModel>();

            List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
            IDictionary<string, object> searchExempt = new Dictionary<string, object>();
            searchExempt["AcademicYearID"] = _globalInfo.AcademicYearID;
            searchExempt["EducationLevelID"] = EducationLevelID;
            searchExempt["HasFirstSemester"] = true;
            searchExempt["HasSecondSemester"] = true;
            searchExempt["ClassID"] = ClassID;
            lstExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchExempt).ToList();

            //lay danh sach thi sinh vang thi
            IDictionary<string, object> dicPupilRetest = new Dictionary<string, object>();
            dicPupilRetest.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicPupilRetest.Add("SchoolID", _globalInfo.SchoolID.Value);
            dicPupilRetest.Add("EducationLevelID", EducationLevelID);
            dicPupilRetest.Add("ClassID", ClassID);
            dicPupilRetest.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicPupilRetest.Add("IsVNEN", true);

            IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetestAC = this.PupilRetestRegistrationBu.GetListPupilRetestRegistrationBySubject(dicPupilRetest)
                                                                            .Where(p=>p.isAbsentContest.HasValue && p.isAbsentContest.Value).ToList();

            //Lay danh sach hoc sinh dang ky môn tự chọn
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"YearID",objAcademicYear.Year},
                {"SubjectTypeID",0}//0 Mon chuyen
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);
            List<RegisterSubjectSpecializeBO> lstRegister = null;

            ClassSubjectBO objClassSubjectBO = null;
            int countSubjectRegister = 0;
            bool isExempt = false;
            bool isAbsentContest = false;
            foreach (PupilRetestToCategory pupilRetest in lsPupilRetest)
            {
                SummanyMarkViewModel item = new SummanyMarkViewModel();
                item.PupilID = pupilRetest.PupilID;
                item.PupilCode = pupilRetest.PupilCode;
                item.FullName = pupilRetest.FullName;
                item.ClassID = pupilRetest.ClassID;
                item.AverageMark = pupilRetest.Vpr != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? pupilRetest.Vpr.AverageMark : new Nullable<decimal>();
                item.CapacityLevel = (pupilRetest.Vpr != null && pupilRetest.Vpr.CapacityLevel != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL) ? pupilRetest.Vpr.CapacityLevel : string.Empty;
                item.CapacityLevelID = pupilRetest.Vpr != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? pupilRetest.Vpr.CapacityLevelID : new Nullable<int>();
                item.ConductLEvel = pupilRetest.Vpr != null && pupilRetest.Vpr.ConductLevelName != null ? pupilRetest.Vpr.ConductLevelName : string.Empty;
                item.ConductLEvelID = pupilRetest.Vpr != null ? pupilRetest.Vpr.ConductLevelID : new Nullable<int>();
                item.EducationLevelID = EducationLevelID;
                item.ClassName = pupilRetest.ClassName;
                item.MarkData = new Dictionary<string, object>();
                lstRegister = lstRegisterSubjectBO.Where(p => p.PupilID == pupilRetest.PupilID).ToList();

                foreach (SubjectCatBO subjectBO in lstSubjectCatBO)
                {
                    objClassSubjectBO = lstClassSubjectBO.Where(o => o.ClassID == pupilRetest.ClassID && o.SubjectID == subjectBO.SubjectCatID).FirstOrDefault();
                    if (lstClassSubjectBO.Any(u => u.ClassID == pupilRetest.ClassID && u.SubjectID == subjectBO.SubjectCatID))
                    {
                        int index = pupilRetest.lstSummedUpRecordBO.FindIndex(u => u.SubjectID == subjectBO.SubjectCatID);
                        isExempt = lstExemptedSubject.Where(o => o.PupilID == pupilRetest.PupilID && o.SubjectID == subjectBO.SubjectCatID).Count() > 0;
                        isAbsentContest = lstPupilRetestAC.Where(p => p.PupilID == pupilRetest.PupilID && p.SubjectID == subjectBO.SubjectCatID).Count() > 0;

                        if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value))
                        {
                            countSubjectRegister = lstRegister.Where(p => p.SubjectID == objClassSubjectBO.SubjectID &&
                                                                                (p.SubjectTypeID == objClassSubjectBO.AppliedType)).Count();
                        }

                        if (isExempt)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "MG";
                        }
                        else if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value)
                            && (objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "KH";
                        }
                        else if (isAbsentContest)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "V";
                        }
                        else if (index > -1)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = pupilRetest.lstMark[index];
                        }
                        else
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "CN";
                        }

                    }
                    else
                    {
                        item.MarkData[subjectBO.SubjectCatID.ToString()] = string.Empty;
                    }
                }

                lsSummanyMarkViewModel.Add(item);

            }
            return lsSummanyMarkViewModel;
        }
        public List<SummanyMarkViewModel> GetRetestPupils_History_ExportSummany(List<int> ListEducationLevelID, List<int> ListClassID, int? ClassID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            //SearchSubject["EducationLevelID"] = EducationLevelID;
            SearchSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            List<ClassSubjectBO> lstClassSubjectBO = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                .Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting,
                    AppliedType = o.AppliedType,
                    Is_Specialize_Subject = o.IsSpecializedSubject
                })
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            List<SubjectCatBO> lstSubjectCatBO = lstClassSubjectBO
                                                .GroupBy(u => new { u.SubjectID, u.DisplayName, u.OrderInSubject })
                                                .Select(u => new SubjectCatBO { SubjectCatID = u.Key.SubjectID, OrderInSubject = u.Key.OrderInSubject, DisplayName = u.Key.DisplayName })
                                                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).Distinct().ToList();

            IDictionary<string, object> SearchClass = new Dictionary<string, object>();
            //SearchClass["EducationLevelID"] = EducationLevelID;
            SearchClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager || _globalInfo.IsViewAll)
            {
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(o => o.DisplayName).ToList();
            }
            else
            {
                SearchClass.Add("UserAccountID", _globalInfo.UserAccountID);
                SearchClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(u => u.DisplayName).ToList();
            }
            List<int> lsClassID = ClassID.HasValue ? new List<int> { ClassID.Value } : lsClass.Select(u => u.ClassProfileID).ToList();
            List<PupilRetestToCategory> lsPupilRetest = PupilRetestRegistrationBusiness.CategoryPupilAfterRetest_History(lsClassID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, true);

            List<SummanyMarkViewModel> lsSummanyMarkViewModel = new List<SummanyMarkViewModel>();

            List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
            IDictionary<string, object> searchExempt = new Dictionary<string, object>();
            searchExempt["AcademicYearID"] = _globalInfo.AcademicYearID;
            //searchExempt["EducationLevelID"] = EducationLevelID;
            searchExempt["HasFirstSemester"] = true;
            searchExempt["HasSecondSemester"] = true;
            searchExempt["ClassID"] = ClassID;
            lstExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchExempt).ToList();
            lstExemptedSubject = lstExemptedSubject.Where(x => ListEducationLevelID.Contains(x.EducationLevelID)).ToList();
            //lay danh sach thi sinh vang thi
            IDictionary<string, object> dicPupilRetest = new Dictionary<string, object>();
            dicPupilRetest.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dicPupilRetest.Add("SchoolID", _globalInfo.SchoolID.Value);
            //dicPupilRetest.Add("EducationLevelID", EducationLevelID);
            dicPupilRetest.Add("ClassID", ClassID);
            dicPupilRetest.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dicPupilRetest.Add("IsVNEN", true);

            IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetestAC = this.PupilRetestRegistrationBu.GetListPupilRetestExportSumManyMark(dicPupilRetest)
                                                                            .Where(p => p.isAbsentContest.HasValue && p.isAbsentContest.Value).ToList();
            lstPupilRetestAC = lstPupilRetestAC.Where(x => ListEducationLevelID.Contains(x.EducationLevelID)).ToList();
            //Lay danh sach hoc sinh dang ky môn tự chọn
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"YearID",objAcademicYear.Year},
                {"SubjectTypeID",0}//0 Mon chuyen
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);
            List<RegisterSubjectSpecializeBO> lstRegister = null;

            ClassSubjectBO objClassSubjectBO = null;
            int countSubjectRegister = 0;
            bool isExempt = false;
            bool isAbsentContest = false;
            foreach (PupilRetestToCategory pupilRetest in lsPupilRetest)
            {
                SummanyMarkViewModel item = new SummanyMarkViewModel();
                item.PupilID = pupilRetest.PupilID;
                item.PupilCode = pupilRetest.PupilCode;
                item.FullName = pupilRetest.FullName;
                item.ClassID = pupilRetest.ClassID;
                item.AverageMark = pupilRetest.Vpr != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? pupilRetest.Vpr.AverageMark : new Nullable<decimal>();
                item.CapacityLevel = (pupilRetest.Vpr != null && pupilRetest.Vpr.CapacityLevel != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL) ? pupilRetest.Vpr.CapacityLevel : string.Empty;
                item.CapacityLevelID = pupilRetest.Vpr != null && pupilRetest.Vpr.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? pupilRetest.Vpr.CapacityLevelID : new Nullable<int>();
                item.ConductLEvel = pupilRetest.Vpr != null && pupilRetest.Vpr.ConductLevelName != null ? pupilRetest.Vpr.ConductLevelName : string.Empty;
                item.ConductLEvelID = pupilRetest.Vpr != null ? pupilRetest.Vpr.ConductLevelID : new Nullable<int>();
                //item.EducationLevelID = EducationLevelID;
                item.ClassName = pupilRetest.ClassName;
                item.MarkData = new Dictionary<string, object>();
                lstRegister = lstRegisterSubjectBO.Where(p => p.PupilID == pupilRetest.PupilID).ToList();

                foreach (SubjectCatBO subjectBO in lstSubjectCatBO)
                {
                    objClassSubjectBO = lstClassSubjectBO.Where(o => o.ClassID == pupilRetest.ClassID && o.SubjectID == subjectBO.SubjectCatID).FirstOrDefault();
                    if (lstClassSubjectBO.Any(u => u.ClassID == pupilRetest.ClassID && u.SubjectID == subjectBO.SubjectCatID))
                    {
                        int index = pupilRetest.lstSummedUpRecordBO.FindIndex(u => u.SubjectID == subjectBO.SubjectCatID);
                        isExempt = lstExemptedSubject.Where(o => o.PupilID == pupilRetest.PupilID && o.SubjectID == subjectBO.SubjectCatID).Count() > 0;
                        isAbsentContest = lstPupilRetestAC.Where(p => p.PupilID == pupilRetest.PupilID && p.SubjectID == subjectBO.SubjectCatID).Count() > 0;

                        if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value))
                        {
                            countSubjectRegister = lstRegister.Where(p => p.SubjectID == objClassSubjectBO.SubjectID &&
                                                                                (p.SubjectTypeID == objClassSubjectBO.AppliedType)).Count();
                        }

                        if (isExempt)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "MG";
                        }
                        else if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value)
                            && (objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "KH";
                        }
                        else if (isAbsentContest)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "V";
                        }
                        else if (index > -1)
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = pupilRetest.lstMark[index];
                        }
                        else
                        {
                            item.MarkData[subjectBO.SubjectCatID.ToString()] = "CN";
                        }

                    }
                    else
                    {
                        item.MarkData[subjectBO.SubjectCatID.ToString()] = string.Empty;
                    }
                }

                lsSummanyMarkViewModel.Add(item);

            }
            return lsSummanyMarkViewModel;
        }
        #region Load Class List, Grid Data
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idEducationLevel)
        {
            ViewData[SummanyMarkConstants.LS_CLASS] = new List<ClassProfile>();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = idEducationLevel;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            dicClass["IsVNEN"] = true;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
            {
                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lstClass;
                return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
            {
                dicClass.Add("UserAccountID", _globalInfo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                var lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
                ViewData[SummanyMarkConstants.LS_CLASS] = lstClass;
                return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        public ActionResult AjaxLoadGrid(int? idClass, int EducationLevelID)
        {
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[SummanyMarkConstants.LS_PUPIL] = GetRetestPupils_History(EducationLevelID, idClass);
            }
            else
            {
                ViewData[SummanyMarkConstants.LS_PUPIL] = new List<SummanyMarkViewModel>();
            }
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = EducationLevelID;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName).ToList();
            }
            else
            {
                dicClass.Add("UserAccountID", _globalInfo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            }

            ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;
            List<int> lsClassID = idClass.HasValue ? new List<int> { idClass.Value } : lsClass.Select(u => u.ClassProfileID).ToList();
            //SetViewDataWithParam(EducationLevelID, lsClassID,Status);

            bool isHeadTeacher = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, idClass.HasValue ? idClass.Value : 0);
            ViewData[SummanyMarkConstants.ENABLE_BUTTON_TK] = false;
            ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = true;
            if (isHeadTeacher)
            {
                ViewData[SummanyMarkConstants.ENABLE_BUTTON_TK] = true;
                ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = false;
            }
            if (!_globalInfo.IsAdminSchoolRole && (idClass == null || idClass == 0))
            {
                ViewData[SummanyMarkConstants.ENABLE_BUTTON_TK] = false;
                ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = true;
            }

            //Session["CategoryOfPupilViewModel"] = ViewData[SummanyMarkConstants.LS_CATEGORYOFPUPIL];
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                ViewData[SummanyMarkConstants.IS_PRIMARY] = true;
            }
            else
            {
                ViewData[SummanyMarkConstants.IS_PRIMARY] = false;
            }
            return PartialView("_List");
        }

        public PartialViewResult AjaxLoadGridTab2(int? idClass, int EducationLevelID, int Status)
        {
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = EducationLevelID;
            dicClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName).ToList();
            }
            else
            {
                dicClass.Add("UserAccountID", _globalInfo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            }
            List<int> lsClassID = idClass.HasValue ? new List<int> { idClass.Value } : lsClass.Select(u => u.ClassProfileID).ToList();
            SetViewDataWithParam(EducationLevelID, lsClassID, Status);
            ViewData[SummanyMarkConstants.LS_EDUCATIONLEVEL] = EducationLevelID;
            ViewData[SummanyMarkConstants.LS_CLASS] = idClass;
            return PartialView("../CategoryOfPupil/_Index");
        }
        #endregion

        /// <summary>
        /// Thực hiện tổng kết theo Lớp thuộc khối
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public JsonResult Sum(FormCollection fc, int EducationLevelID, int? ClassID)
        {
            AcademicYear acaYear = this.AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            SearchSubject["EducationLevelID"] = EducationLevelID;
            SearchSubject["SchoolID"] = _globalInfo.SchoolID.Value;
            List<int> ListClassID = new List<int>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                SearchSubject["ClassID"] = ClassID;
            }
            else // tim kiem theo khoi
            {
                // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsViewAll)
                {
                    SearchSubject["EmployeeID"] = _globalInfo.EmployeeID;
                    ListClassID = ClassProfileBusiness.SearchByTeacherPermission(SearchSubject).Select(o => o.ClassID).ToList();
                    if (ListClassID != null && ListClassID.Count == 0)
                    {
                        ListClassID.Add(0); // Tìm với Ds rỗng
                    }
                    SearchSubject["ListClassID"] = ListClassID;
                }
            }

            List<ClassSubjectBO> lstClassSubjectBO = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                .Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting,
                    AppliedType = o.AppliedType,
                    Is_Specialize_Subject = o.IsSpecializedSubject,
                    FirstSemesterCoefficient = o.FirstSemesterCoefficient
                })
                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

            List<SubjectCatBO> lstSubjectCatBO = lstClassSubjectBO
                                                .GroupBy(u => new { u.SubjectID, u.DisplayName, u.OrderInSubject })
                                                .Select(u => new SubjectCatBO { SubjectCatID = u.Key.SubjectID, OrderInSubject = u.Key.OrderInSubject, DisplayName = u.Key.DisplayName })
                                                .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).Distinct().ToList();

            //Lay danh sach hoc sinh dang ky môn tự chọn
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"YearID",acaYear.Year},
                {"SubjectTypeID",0}//0 Mon chuyen
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);
            List<RegisterSubjectSpecializeBO> lstRegister = null;

            //List<ClassSubject> lsClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject).ToList();
            //List<SubjectCat> lstSubjectCat = lsClassSubject.Select(u => u.SubjectCat).Distinct().ToList();
            ViewData[SummanyMarkConstants.LS_SUBJECT] = lstSubjectCatBO;

            IDictionary<string, object> SearchClass = new Dictionary<string, object>();
            SearchClass["EducationLevelID"] = EducationLevelID;
            SearchClass["AcademicYearID"] = _globalInfo.AcademicYearID;
            // AnhVD 20140814 - Lay danh sach lop theo phan cong
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsEmployeeManager || _globalInfo.IsViewAll)
            {
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(o => o.DisplayName).ToList();
            }
            else
            {
                SearchClass.Add("UserAccountID", _globalInfo.UserAccountID);
                SearchClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lsClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchClass).OrderBy(u => u.DisplayName).ToList();
            }

            // Danh sach cac lop duoc nhap diem
            List<int> lsClassID = ClassID.HasValue ? new List<int> { ClassID.Value } : lsClass.Select(u => u.ClassProfileID).ToList();
            ViewData[SummanyMarkConstants.LS_CLASS] = lsClass;

            List<ExemptedSubject> lstExemptedSubject = new List<ExemptedSubject>();
            IDictionary<string, object> searchExempt = new Dictionary<string, object>();
            searchExempt["AcademicYearID"] = _globalInfo.AcademicYearID;
            searchExempt["EducationLevelID"] = EducationLevelID;
            searchExempt["HasFirstSemester"] = true;
            searchExempt["HasSecondSemester"] = true;
            searchExempt["ClassID"] = ClassID;
            lstExemptedSubject = ExemptedSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchExempt).ToList();


            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

            ClassSubjectBO objClassSubjectBO = null;
            int countSubjectRegister = 0;
            string mark = string.Empty;
            bool enoughMark = true;
            int index = 0;
            bool isExempt = false;
            #region PupilRetestHistory
            if (isMovedHistory) // Neu la cac nam hoc truoc nam 2014 thi lay cac du lieu lich su
            {
                // Danh sach trong ca nam Sem = 3
                List<PupilRetestHistoryToCategory> lsPupilRetest = PupilRetestRegistrationBusiness.CategoryPupilAfterRetestHistory(lsClassID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
                //Duyet danh sach, tim kiem xem co hoc sinh nao chua duoc nhap diem trung binh mon, hoac thi lai khong
                List<PupilRetestHistoryToCategory> lstPupilRetest = new List<PupilRetestHistoryToCategory>();

                foreach (PupilRetestHistoryToCategory pupilRetest in lsPupilRetest)
                {
                    enoughMark = true;
                    lstRegister = lstRegisterSubjectBO.Where(p => p.PupilID == pupilRetest.PupilID).ToList();
                    foreach (SubjectCatBO subjectBO in lstSubjectCatBO)
                    {
                        objClassSubjectBO = lstClassSubjectBO.Where(o => o.ClassID == pupilRetest.ClassID && o.SubjectID == subjectBO.SubjectCatID).FirstOrDefault();

                        if (lstClassSubjectBO.Any(u => u.ClassID == pupilRetest.ClassID && u.SubjectID == subjectBO.SubjectCatID))
                        {
                            index = pupilRetest.lstSummedUpRecordHistory.FindIndex(u => u.SubjectID == subjectBO.SubjectCatID);
                            isExempt = lstExemptedSubject.Where(o => o.PupilID == pupilRetest.PupilID && o.SubjectID == subjectBO.SubjectCatID).Count() > 0;
                            if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value))
                            {
                                countSubjectRegister = lstRegister.Where(p => p.SubjectID == objClassSubjectBO.SubjectID && p.SemesterID == _globalInfo.Semester &&
                                                                                    (p.SubjectTypeID == objClassSubjectBO.AppliedType)).Count();
                            }

                            if (isExempt)
                            {
                                mark = "MG";
                            }
                            else
                                if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value)
                                    && (objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                                    || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                            {
                                mark = "KH";
                            }
                            else if (index < 0)
                            {
                                mark = "CN";
                            }
                            else
                            {
                                mark = pupilRetest.lstMark[index];
                            }

                            if (string.IsNullOrEmpty(mark) || "CN".Equals(mark) || "MG".Equals(mark))
                            {
                                enoughMark = false;
                                break;
                            }

                        }
                    }
                    if (enoughMark)
                    {
                        lstPupilRetest.Add(pupilRetest);
                    }
                }

                if (lstPupilRetest.Count == 0)
                {
                    throw new BusinessException("SummanyMark_Error_NotEnoughMark");
                }
                PupilRankingHistoryBusiness.RankingPupilRetestHistory(_globalInfo.UserAccountID, lstPupilRetest, lstRegisterSubjectBO);
            }
            #endregion
            #region PupilRetest
            else
            {
                List<PupilRetestToCategory> lsPupilRetest = PupilRetestRegistrationBusiness.CategoryPupilAfterRetest(lsClassID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
                //Duyet danh sach, tim kiem xem co hoc sinh nao chua duoc nhap diem trung binh mon, hoac thi lai khong
                List<PupilRetestToCategory> lstPupilRetest = new List<PupilRetestToCategory>();
                foreach (PupilRetestToCategory pupilRetest in lsPupilRetest)
                {
                    enoughMark = true;
                    lstRegister = lstRegisterSubjectBO.Where(p => p.PupilID == pupilRetest.PupilID).ToList();
                    foreach (SubjectCatBO subjectBO in lstSubjectCatBO)
                    {
                        objClassSubjectBO = lstClassSubjectBO.Where(o => o.ClassID == pupilRetest.ClassID && o.SubjectID == subjectBO.SubjectCatID).FirstOrDefault();

                        if (lstClassSubjectBO.Any(u => u.ClassID == pupilRetest.ClassID && u.SubjectID == subjectBO.SubjectCatID))
                        {
                            index = pupilRetest.lstSummedUpRecord.FindIndex(u => u.SubjectID == subjectBO.SubjectCatID);
                            isExempt = lstExemptedSubject.Where(o => o.PupilID == pupilRetest.PupilID && o.SubjectID == subjectBO.SubjectCatID).Count() > 0;
                            if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value))
                            {
                                countSubjectRegister = lstRegister.Where(p => p.SubjectID == objClassSubjectBO.SubjectID &&
                                                                                    (p.SubjectTypeID == objClassSubjectBO.AppliedType)).Count();
                            }

                            if (isExempt)
                            {
                                continue;
                            }
                            else
                                if (!(objClassSubjectBO.Is_Specialize_Subject.HasValue && objClassSubjectBO.Is_Specialize_Subject.Value)
                                    && (objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                                    || objClassSubjectBO.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) && countSubjectRegister == 0)
                            {
                                mark = "KH";
                            }
                            else if (index < 0)
                            {
                                mark = "CN";
                            }
                            else
                            {
                                mark = pupilRetest.lstMark[index];
                            }

                            if (string.IsNullOrEmpty(mark) || ("CN".Equals(mark)  && objClassSubjectBO.AppliedType == 0))
                            {
                                enoughMark = false;
                                break;
                            }

                        }
                    }
                    if (enoughMark)
                    {
                        lstPupilRetest.Add(pupilRetest);
                    }
                }

                if (lstPupilRetest.Count == 0)
                {
                    throw new BusinessException("SummanyMark_Error_NotEnoughMark");
                }
                PupilRankingBusiness.RankingPupilRetest(_globalInfo.UserAccountID, lstPupilRetest, lstRegisterSubjectBO);
            }
            #endregion
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        public FileResult ExportExcel(int EducationLevelID, int? ClassID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "HS_THPT_KetQuaThiLaiKhoi10.xls");

            IVTWorkbook oBook;

            try { oBook = VTExport.OpenWorkbook(templatePath); }
            catch
            {
                throw new BusinessException(Res.Get("Import_Validate_FileInUsed"));
            }
            //Lấy sheet
            IVTWorksheet sheet = oBook.GetSheet(1);

            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            SearchSubject["EducationLevelID"] = EducationLevelID;
            SearchSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            List<int> ListClassID = new List<int>();
            if (ClassID.HasValue && ClassID.Value > 0)
            {
                SearchSubject["ClassID"] = ClassID;
                ListClassID = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                              .OrderBy(o => o.DisplayName).Select(p => p.ClassProfileID).Distinct().ToList();
            }
            else // tim kiem theo khoi
            {
                // AnhVD 20140814 - Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
                {
                    SearchSubject["EmployeeID"] = _globalInfo.EmployeeID;
                    ListClassID = ClassProfileBusiness.SearchByTeacherPermission(SearchSubject).Select(o => o.ClassID).ToList();
                    if (ListClassID != null && ListClassID.Count == 0)
                    {
                        ListClassID.Add(0); // Tìm với Ds rỗng
                    }
                    SearchSubject["ListClassID"] = ListClassID;
                }
                else
                {
                    ListClassID = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                                  .OrderBy(o => o.DisplayName).Select(p => p.ClassProfileID).Distinct().ToList();
                }
            }
            List<ClassSubject> lsClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject).ToList();
            List<SubjectCat> lstSubjectCat = lsClassSubject.Select(u => u.SubjectCat).OrderBy(o => o.OrderInSubject).Distinct().ToList();
            ViewData[SummanyMarkConstants.LS_SUBJECT] = lstSubjectCat;

            List<SummanyMarkViewModel> lstSummanyMark = new List<SummanyMarkViewModel>();

            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                lstSummanyMark = GetRetestPupils_History(EducationLevelID, ClassID);
            }

            #region fill template
            // Tạo header template
            IVTRange rangeHeader = sheet.GetRange("E9", "E9");
            IVTRange rangeHeaderButtom = sheet.GetRange("E10", "E15");
            IVTRange rangeHeaderLast = sheet.GetRange("F9", "F15");
            IVTRange rangeWidthSubject = sheet.GetRange("E10", "E10");
            IVTRange rangeHLHKTBCM = sheet.GetRange("AA9", "AA15");
            IVTRange rangeTD = sheet.GetRange("AB9", "AB15");
            int startcolhearder = 5;
            for (int i = 0; i < lstSubjectCat.Count; i++)
            {
                string titleHeaarder = "";
                if (i == 0)
                {
                    titleHeaarder = "ĐIỂM TRUNG BÌNH CÁC MÔN";
                }
                sheet.CopyPasteSameSize(rangeHeader, 9, startcolhearder);
                sheet.SetCellValue(9, startcolhearder, titleHeaarder);
                sheet.CopyPasteSameSize(rangeHeaderButtom, 10, startcolhearder);
                sheet.CopyPasteSameColumnWidth(rangeWidthSubject, 10, startcolhearder);
                sheet.SetCellValue(10, startcolhearder, lstSubjectCat[i].DisplayName);

                startcolhearder++;

            }

            sheet.MergeRow(9, 5, startcolhearder - 1);
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "TBcm");


            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Học lực");

            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeHLHKTBCM, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Hạnh kiểm");

            startcolhearder++;
            sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            sheet.MergeColumn(startcolhearder, 9, 10);
            sheet.CopyPasteSameColumnWidth(rangeTD, 9, startcolhearder);
            sheet.SetCellValue(9, startcolhearder, "Thuộc diện");

            //Lay danh sach mon hoc thi lại
            var lstClassSubject = (this.LoadClassSubject(EducationLevelID, ClassID)
                                        .Select(u => new
                                        {
                                            Label = u.SubjectCat.DisplayName,
                                            Value = u.SubjectID,
                                            OrderInSubject = u.SubjectCat.OrderInSubject
                                        }).Distinct().OrderBy(u => u.OrderInSubject).ThenBy(o => o.Label).ToList());
            List<int> lstSubjectID = lstClassSubject.Select(p => p.Value).ToList();

            //danh sách học sinh đăng ký thi lại
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("ClassID", ClassID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

            dic.Add("ListSubjectID", lstSubjectID);
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            List<PupilRetestRegistrationSubjectBO> lstPupilRetest = new List<PupilRetestRegistrationSubjectBO>();
            lstPupilRetest = this.PupilRetestRegistrationBu.GetListPupilRetestRegistrationBySubject(dic).ToList();
            List<PupilRankingBO> lsPRBO = GetData(EducationLevelID, ListClassID);
            List<CategoryOfPupilViewModel> lstCategory = lsPRBO != null ? ConvertPupilRankingBOToCategoryOfPupiliewModel(lsPRBO) : new List<CategoryOfPupilViewModel>();
            CategoryOfPupilViewModel objCategory = null;

            int rowIndex = 11;
            foreach (var pupil in lstSummanyMark)
            {
                objCategory = new CategoryOfPupilViewModel();
                objCategory = lstCategory.Where(p => p.PupilID == pupil.PupilID).FirstOrDefault();
                sheet.SetCellValue(rowIndex, 1, rowIndex - 10);
                sheet.SetCellValue(rowIndex, 2, pupil.FullName);
                sheet.SetCellValue(rowIndex, 3, pupil.PupilCode);
                sheet.SetCellValue(rowIndex, 4, pupil.ClassName);
                int colIndex = 5;
                string DisplayMark = string.Empty;
                foreach (SubjectCat subject in lstSubjectCat)
                {
                    DisplayMark = pupil.MarkData[subject.SubjectCatID.ToString()].ToString();
                    sheet.GetRange(rowIndex, colIndex, rowIndex, colIndex).SetHAlign(VTHAlign.xlHAlignCenter);
                    if (DisplayMark == null)
                    {
                        sheet.SetCellValue(rowIndex, colIndex++, "");
                    }
                    else
                    {
                        if (lstPupilRetest.Where(p => p.PupilID == pupil.PupilID && p.SubjectID == subject.SubjectCatID).Count() > 0)
                        {
                            sheet.GetRange(rowIndex, colIndex, rowIndex, colIndex).SetFontColour(System.Drawing.Color.Red);
                        }

                        if ("CN".Equals(DisplayMark) || "MG".Equals(DisplayMark) || "KH".Equals(DisplayMark))
                        {
                            sheet.SetCellValue(rowIndex, colIndex++, DisplayMark);
                        }
                        else if (DisplayMark == "10,0")
                        {
                            sheet.SetCellValue(rowIndex, colIndex++, "10");
                        }
                        else
                        {
                            sheet.SetCellValue(rowIndex, colIndex++, DisplayMark.Replace(",", "."));
                        }
                    }

                }
                if (pupil.AverageMark == null)
                {
                    sheet.SetCellValue(rowIndex, startcolhearder - 3, "");
                }
                else
                {
                    if (pupil.AverageMark != 10.0m)
                    {
                        sheet.SetCellValue(rowIndex, startcolhearder - 3, pupil.AverageMark.Value.ToString("0.0#").Replace(",", "."));
                    }
                    else
                    {
                        sheet.SetCellValue(rowIndex, startcolhearder - 3, pupil.AverageMark.Value.ToString("0"));
                    }
                }
                sheet.GetRange(rowIndex, startcolhearder - 3, rowIndex, startcolhearder - 3).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.SetCellValue(rowIndex, startcolhearder - 2, pupil.CapacityLevel);
                sheet.SetCellValue(rowIndex, startcolhearder - 1, pupil.ConductLEvel);
                sheet.SetCellValue(rowIndex, startcolhearder, (objCategory.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_RETEST
                                                                || objCategory.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING
                                                                || objCategory.StudyingJudgementID == GlobalConstants.STUDYING_JUDGEMENT_GRADUATION)
                                                                ? "" : objCategory.StudyingJudgementName);
                sheet.GetRange(rowIndex, 1, rowIndex, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);
                if (rowIndex > 11 && rowIndex % 5 == 1)
                {
                    sheet.GetRange(rowIndex, 1, rowIndex, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                else
                {

                    sheet.GetRange(rowIndex, 1, rowIndex, startcolhearder).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }
                rowIndex++;
            }
            Dictionary<string, object> dicVariable = new Dictionary<string, object>();
            dicVariable["AcademicYear"] = acaYear.DisplayTitle;
            dicVariable["SupervisingDept"] = _globalInfo.SuperVisingDeptName;
            dicVariable["SchoolName"] = acaYear.SchoolProfile.SchoolName;
            dicVariable["EducationLevelID"] = EducationLevelID;
            dicVariable["ProvinceDate"] = acaYear.SchoolProfile.Province.ProvinceName + ", " + string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);

            sheet.FillVariableValue(dicVariable);
            // Vẽ đường viền
            sheet.GetRange(10, 1, 10 + lstSummanyMark.Count, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
            sheet.GetRange(10, 1, 10 + lstSummanyMark.Count, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            sheet.GetRange(10, 1, 10 + lstSummanyMark.Count, startcolhearder).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
            // Tao chu ky
            sheet.SetCellValue(12 + lstSummanyMark.Count, startcolhearder - 4, "NGƯỜI LẬP BÁO CÁO");
            sheet.GetRange(12 + lstSummanyMark.Count, startcolhearder - 4, 12 + lstSummanyMark.Count, startcolhearder - 4).SetFontStyle(true, null, false, null, false, false);
            //sheet.GetRange(12 + lstSummanyMark.Count, startcolhearder - 4, 12 + lstSummanyMark.Count, startcolhearder - 2).SetFontStyle(true, null, false, null, false, false);
            sheet.FitAllColumnsOnOnePage = true;
            #endregion
            sheet.HideColumn(27);
            sheet.HideColumn(28);


            string AppliedName = _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY ? "TH" :
                                _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY ? "THCS" :
                                _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? "THPT" : string.Empty;
            string EducationName = ClassID > 0 ? Utils.Utils.StripVNSign(ClassProfileBusiness.Find(ClassID).DisplayName) : "Khoi" + EducationLevelID;

            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = string.Format(SummanyMarkConstants.FILE_RESULT, AppliedName, EducationName);
            return result;
        }
        private IQueryable<ClassSubject> LoadClassSubject(int eid, int? cid)
        {
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AppliedLevel"] = _globalInfo.AppliedLevel;
            searchPOC["AcademicYearID"] = _globalInfo.AcademicYearID;
            searchPOC["EducationLevelID"] = eid;
            searchPOC["ClassID"] = cid;


            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, searchPOC);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevelID", eid);
            dic.Add("ClassID", cid);
            var lstPupilRetest = this.PupilRetestRegistrationBu.SearchBySchool(_globalInfo.SchoolID.Value, dic);


            lstPupilRetest = from su in lstPupilRetest
                             join poc in iqPupilOfClass on new { su.PupilID, su.ClassID } equals new { poc.PupilID, poc.ClassID }
                             where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                             select su;
            return ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(o => lstPupilRetest.Any(x => x.SubjectID == o.SubjectID && x.ClassID == o.ClassID));

        }

        #endregion hath

        #region dungnt

        /// <summary>
        /// Lấy danh sách chi tiết Hs theo xếp hạng
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <param name="lsClassID"></param>
        /// <returns></returns>
        private List<PupilRankingBO> GetData(int EducationLevelID, List<int> lsClassID,int Status = 0)
        {
            bool HasHeadTeacherPermission = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, 0);
            if (!HasHeadTeacherPermission)
            {
                ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = true;
            }
            else
            {
                ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = false;
            }
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return null;
            }
            

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("lstClassID", lsClassID);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //HS da dk
            var lstPupilRetest = this.PupilRetestRegistrationBu.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            List<int> lstPupilRetestID = lstPupilRetest.Select(p => p.PupilID).Distinct().ToList();

            //HS ren luyen lai
            //Danh sach hoc sinh ren luyen lai
            List<int> lstPupilBacktrain = (from p in VPupilRankingBusiness.All
                                               .Where(u => u.Last2digitNumberSchool == _globalInfo.SchoolID.Value % 100
                                                        && u.CreatedAcademicYear == objAy.Year
                                                        && u.SchoolID == _globalInfo.SchoolID
                                                        && u.AcademicYearID == _globalInfo.AcademicYearID
                                                        && lsClassID.Contains(u.ClassID) && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                        && (u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING))
                                           select p.PupilID).ToList();
            
            List<PupilRankingBO> iquery = PupilRankingBusiness.GetPupilToRankAfterRetest(new Dictionary<string, object>()
                                                                                                            {
                                                                                                                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                                                                                {"EducationLevelID",EducationLevelID},
                                                                                                                {"SchoolID", _globalInfo.SchoolID.Value},
                                                                                                                {"ListClassID",lsClassID},
                                                                                                                {"IsVNEN",true}
                                                                                                            })
                                                                                       .Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            List<int> lstPupilResultID = new List<int>();
            if (Status == 1)//Da dang ky du thi/ Ren luyen lai
            {
                lstPupilResultID = lstPupilBacktrain.Union(lstPupilRetestID).ToList();
            }
            else if (Status == 2)//Chua dang ky du thi
            {
                lstPupilResultID = iquery.Where(p => !lstPupilRetestID.Contains(p.PupilID.Value) && !lstPupilBacktrain.Contains(p.PupilID.Value))
                    .Select(p => p.PupilID.Value).Distinct().ToList();
            }
            else if (Status == 3)//Vang thi
            {
                lstPupilResultID = lstPupilRetest.Where(p => p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Select(p => p.PupilID).Distinct().ToList();
            }

            if (Status != 0)
            {
                iquery = iquery.Where(p => lstPupilResultID.Contains(p.PupilID.Value)).ToList();
            }

            ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = iquery.Any(u => !u.CapacityLevelID.HasValue || !u.ConductLevelID.HasValue);
            return iquery.ToList();
        }

        #region Chuyển đổi kiểu dữ liệu
        public List<CategoryOfPupilViewModel> ConvertPupilRankingBOToCategoryOfPupiliewModel(List<PupilRankingBO> lsPRBO)
        {
            if (lsPRBO == null) return new List<CategoryOfPupilViewModel>();

            List<CategoryOfPupilViewModel> lsCOPVM = new List<CategoryOfPupilViewModel>();
            List<StudyingJudgement> listStudyingJudgement = StudyingJudgementBusiness.All.Where(u => u.IsActive).ToList();
            foreach (var item in lsPRBO)
            {
                CategoryOfPupilViewModel copvm = new CategoryOfPupilViewModel();
                Utils.Utils.BindTo(item, copvm);

                copvm.Genre = item.Genre;
                copvm.ConductLevelName = item.ConductLevel;
                copvm.CapacityLevel = item.CapacityLevel;
                copvm.ClassName = item.ClassName;
                if (copvm.StudyingJudgementID.HasValue)
                {
                    StudyingJudgement sj = listStudyingJudgement.SingleOrDefault(u => u.StudyingJudgementID == copvm.StudyingJudgementID.Value);
                    copvm.StudyingJudgementName = sj.Resolution;
                }

                if (copvm.Genre.HasValue)
                {
                    if (copvm.Genre.Value == SystemParamsInFile.GENRE_MALE)
                        copvm.GenreName = Res.Get("Common_Label_Male");

                    if (copvm.Genre.Value == SystemParamsInFile.GENRE_FEMALE)
                        copvm.GenreName = Res.Get("Common_Label_Female");
                }
                else
                {
                    copvm.GenreName = "";
                }
                lsCOPVM.Add(copvm);
            }
            return lsCOPVM;
        }

        public List<PupilRankingBO> ConvertCategoryOfPupilViewModelToPupilRankingBO(List<CategoryOfPupilViewModel> lsCOPVM)
        {
            List<PupilRankingBO> lsPRBO = new List<PupilRankingBO>();
            foreach (var item in lsCOPVM)
            {
                PupilRankingBO prbo = new PupilRankingBO();
                Utils.Utils.BindTo(item, prbo);
                lsPRBO.Add(prbo);
            }
            return lsPRBO;
        }

        public void SetViewDataWithParam(int EducationLevelID, List<int> lsClassID,int Status = 0)
        {
            List<PupilRankingBO> lsPRBO = GetData(EducationLevelID, lsClassID,Status);
            List<CategoryOfPupilViewModel> lsCOPVM = lsPRBO != null ? ConvertPupilRankingBOToCategoryOfPupiliewModel(lsPRBO) : new List<CategoryOfPupilViewModel>();
            Session["CategoryOfPupilViewModel"] = lsCOPVM;
            ViewData[SummanyMarkConstants.LS_CATEGORYOFPUPIL] = lsCOPVM;
            ViewData[SummanyMarkConstants.DISABLE_BUTTON_CATEGORY] = lsCOPVM.Count > 0 ? false : true;
        }
        #endregion

        #region Xếp loại
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult ClassifyPupil()
        {
            List<CategoryOfPupilViewModel> lsCOPVM = (List<CategoryOfPupilViewModel>)Session["CategoryOfPupilViewModel"];
            List<PupilRankingBO> lsPRBO = ConvertCategoryOfPupilViewModelToPupilRankingBO(lsCOPVM);
            List<int> lsClassID = lsPRBO.Select(p => p.ClassID.Value).Distinct().ToList();
            List<int> lstPupilID = lsPRBO.Select(p => p.PupilID.Value).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("ListClassID", lsClassID);
            dic.Add("lstPupilID", lstPupilID);
            
            List<PupilRetestRegistration> lstPupilRetest = this.PupilRetestRegistrationBu.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //lsPRBO = lsPRBO.Where(o => (o.ConductLevelID.HasValue || o.IsKXLHK) && o.CapacityLevelID.HasValue).ToList();
            //if (lsPRBO.Count == 0)
            //{
            //    throw new BusinessException("SummanyMark_Error_NotHaveSummany");
            //}
            ClassifyPupilCategory(lsPRBO,lstPupilRetest);
            return Json(new JsonMessage(Res.Get("Common_Label_CategorySuccessMessage")));
        }
        /// <summary>
        /// Thực hiện xếp loại sau thi lại đối với HS
        /// </summary>
        /// <param name="lsPRBO"></param>
        public void ClassifyPupilCategory(List<PupilRankingBO> lsPRBO,List<PupilRetestRegistration> lstPupilRetest)
        {
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                PupilRankingBusiness.ClassifyPupilPrimaryAfterRetest(lsPRBO, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                if (UtilsBusiness.IsMoveHistory(objAcademicYear))
                {
                    PupilRankingHistoryBusiness.ClassifyPupilAfterRetestHistory(lsPRBO, lstPupilRetest, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value);
                }
                else
                {
                    PupilRankingBusiness.ClassifyPupilAfterRetest(lsPRBO, lstPupilRetest, _globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value);
                }
            }

        }
        #endregion
        #endregion dungnt

        #region Export CategoryPupilRanking
        private List<PupilRankingBO> GetDataCategoryPupilRanking(int EducationLevelID, List<int> lsClassID)
        {
            List<PupilRankingBO> iquery = PupilRankingBusiness.GetPupilToRankAfterRetest(new Dictionary<string, object>()
                                                                                                            {
                                                                                                                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                                                                                {"EducationLevelID",EducationLevelID},
                                                                                                                {"SchoolID", _globalInfo.SchoolID.Value},
                                                                                                                {"ListClassID",lsClassID},
                                                                                                                {"IsVNEN",true}
                                                                                                            })
                                                                                       .Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
           return iquery.ToList();
        
        }
        public FileResult ExportExcelCategoryPupilRanking(int EducationLevelID, int? ClassID)
        {
            
            string AppliedName = _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY ? "TH" :
                                _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY ? "THCS" :
                                _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? "THPT" : string.Empty;
            string EducationName = ClassID > 0 ? Utils.Utils.StripVNSign(ClassProfileBusiness.Find(ClassID).DisplayName) : "Khoi" + EducationLevelID;

            Stream excel = this.GetDataForReport(EducationLevelID, ClassID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = string.Format(SummanyMarkConstants.FILE_RESULT_RETEST, AppliedName, EducationName);
            return result;
        }
        public Stream GetDataForReport(int EducationLevelID, int? ClassID)
        {
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", "HS_THCS_KetQuaThiLaiRenLuyenLai.xls");
            IVTWorkbook oBook;
            try { oBook = VTExport.OpenWorkbook(templatePath); }
            catch
            {
                throw new BusinessException(Res.Get("Import_Validate_FileInUsed"));
            }
            ClassID = null;
            List<int> listEducationLevel = null;
            if (_globalInfo.AppliedLevel == 2)
                listEducationLevel = new List<int>() { 6, 7, 8 };
            else
                listEducationLevel = new List<int>() { 10, 11 };
            //Lấy sheet
            IVTWorksheet sheet = oBook.GetSheet(1);
            IDictionary<string, object> SearchSubject = new Dictionary<string, object>();
            //SearchSubject["EducationLevelID"] = EducationLevelID;
            SearchSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            SearchSubject["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            List<int> ListClassID = new List<int>();
            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            List<ClassSubject> lstClassSubject = new List<ClassSubject>();
            List<ClassOfTeacherBO> ListClassIDByPermission = null;
            List<ClassProfile> ListClassIDAll =null;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
            {
                ListClassIDByPermission = ClassProfileBusiness.SearchByTeacherPermission(SearchSubject).ToList();
            }
            else
            {
                ListClassIDAll = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject)
                                  .OrderBy(o => o.DisplayName).ToList();
            }
            var lsClassSubjectTemp = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, SearchSubject).ToList();
            var lsClassSubjectTempRetest = this.LoadClassSubject(0, ClassID)
                                            .Distinct().OrderBy(u => u.SubjectCat.OrderInSubject)
                                            .ThenBy(o => o.SubjectCat.DisplayName).ToList();
            //foreach (var item in listEducationLevel)
            //{
                //SearchSubject["EducationLevelID"] = item;
                // Neu la Giao vien - Kiem tra chi lay lop theo phan cong
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsEmployeeManager)
                {
                    SearchSubject["EmployeeID"] = _globalInfo.EmployeeID;
                    ListClassID.AddRange(ListClassIDByPermission.Where(x => listEducationLevel.Contains(x.EducationLevelID) 
                                                                        && x.HeadTeacherID == _globalInfo.EmployeeID
                                                                        && x.SubstituedHeadTeacher == _globalInfo.EmployeeID)
                                                                .Select(o => o.ClassID).Distinct().ToList());
                    if (ListClassID != null && ListClassID.Count == 0)
                    {
                        ListClassID.Add(0); // Tìm với Ds rỗng
                    }
                    SearchSubject["ListClassID"] = ListClassID;
                }
                else
                {
                    ListClassID.AddRange(ListClassIDAll.Where(x => listEducationLevel.Contains(x.EducationLevelID)).Select(p => p.ClassProfileID).Distinct().ToList());
                }
                lsClassSubject.AddRange(lsClassSubjectTemp.Where(x => listEducationLevel.Contains(x.ClassProfile.EducationLevelID)));

                //Lay danh sach mon hoc thi lại
                lstClassSubject.AddRange(lsClassSubjectTempRetest.Where(x => listEducationLevel.Contains(x.ClassProfile.EducationLevelID)));
            //}

            List<int> lstSubjectID = lstClassSubject.Select(p => p.SubjectID).ToList();
            //danh sách học sinh đăng ký thi lại
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            dic.Add("SchoolID", _globalInfo.SchoolID.Value);
            //dic.Add("EducationLevelID", EducationLevelID);
            dic.Add("ClassID", ClassID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);
            dic.Add("ListSubjectID", lstSubjectID);
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<SubjectCat> lstSubjectCat = lsClassSubject.Select(u => u.SubjectCat).OrderBy(o => o.OrderInSubject).Distinct().ToList();
            List<SummanyMarkViewModel> lstSummanyMark = new List<SummanyMarkViewModel>();
            List<PupilRetestRegistrationSubjectBO> lstPupilRetest = new List<PupilRetestRegistrationSubjectBO>();
            List<PupilRetestRegistrationSubjectBO> lstPupilRetestTemp = this.PupilRetestRegistrationBu.GetListPupilRetestExportSumManyMark(dic).ToList();
            List<PupilRankingBO> lstPupil = new List<PupilRankingBO>();
            var lstPupilRetestSubject = new List<PupilRetestRegistration>();
            var lstPupilTemp = PupilRankingBusiness.GetPupilToRankAfterRetest(new Dictionary<string, object>()
                                                                                            {
                                                                                                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                                                                //{"EducationLevelID",0},
                                                                                                {"SchoolID", _globalInfo.SchoolID.Value},
                                                                                                {"ListClassID",ListClassID},
                                                                                                {"IsVNEN",true},
                                                                                                {"AppliedLevel",_globalInfo.AppliedLevel}
                                                                                            })
                                                                                       .Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            var lstPupilRetestSubjectTemp = this.PupilRetestRegistrationBu.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            var lstSummanyMarkTemp = new List<SummanyMarkViewModel>();
            lstSummanyMarkTemp = GetRetestPupils_History_ExportSummany(listEducationLevel,ListClassID,ClassID);
            //foreach (var item in listEducationLevel)
            //{
            lstSummanyMark.AddRange(lstSummanyMarkTemp);
                //SearchSubject["EducationLevelID"] = item;
                //dic["EducationLevelID"] = item;
                lstPupilRetest.AddRange(lstPupilRetestTemp.Where(x =>listEducationLevel.Contains(x.EducationLevelID)));
                lstPupilTemp = lstPupilTemp.Where(x => x.EducationLevelID != null && x.EducationLevelID > 0).ToList();
                lstPupil.AddRange(lstPupilTemp.Where(x => listEducationLevel.Contains(int.Parse(x.EducationLevelID.ToString()))));
                //HS da dk
                lstPupilRetestSubject.AddRange(lstPupilRetestSubjectTemp.Where(x => listEducationLevel.Contains(x.ClassProfile.EducationLevelID)));
            //}
            #region fill template
            //List include Retest & Repractice pupil
            List<CategoryOfPupilViewModel> lstPupilByCategory = lstPupil != null ? ConvertPupilRankingBOToCategoryOfPupiliewModel(lstPupil) : new List<CategoryOfPupilViewModel>();
            CategoryOfPupilViewModel objCategory = null;
            PupilRankingBO objPupilRk = null;
            lstPupilByCategory = lstPupilByCategory.GroupBy(x => x.PupilID).Select(x => x.FirstOrDefault()).OrderBy(x => x.EducationLevelID).OrderBy(x => x.ClassName).ThenBy(x => x.FullName).ToList();
            int index = 0;
            int rowIndex = 9;
            var JugetmentReason = string.Empty;
            List<SubjectCat> listSubCatByPupil = null;
            List<int> listSubCatIDByPupil = null;
            List<int> listBackTrainIDByPupil = null;
            List<PupilRetestRegistration> listPupilRetestByPupil = null;
            #region Lọc Hs thi lại theo môn và hs rèn luyện lại

            //Danh sach hoc sinh ren luyen lai
            List<int> lstPupilBacktrain = (from p in VPupilRankingBusiness.All
                                               .Where(u => u.Last2digitNumberSchool == _globalInfo.SchoolID.Value % 100
                                                        && u.CreatedAcademicYear == _globalInfo.AcademicYearID.Value
                                                        && u.SchoolID == _globalInfo.SchoolID
                                                        && u.AcademicYearID == _globalInfo.AcademicYearID
                                                        //&& (ClassID == null || (ClassID > 0 && ClassID == u.ClassID))
                                                        && ListClassID.Contains(u.ClassID)
                                                        && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                        && (u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING))
                                           select p.PupilID).ToList();
            IEnumerable<int> listPupilBackTrainID = lstPupil.Where(x => x.PupilID != null && x.IsBackTrain == true).Select(x => int.Parse(x.PupilID.ToString())).ToList();
            lstPupilBacktrain.AddRange(listPupilBackTrainID);
            #endregion
            var objPupilRetestBySj = new PupilRetestRegistration();
            foreach (var pupil in lstPupilByCategory)
            {
                index++;
                //HS retest 
                listPupilRetestByPupil = lstPupilRetestSubject.Where(x => x.PupilID == pupil.PupilID).ToList();
                listSubCatIDByPupil = listPupilRetestByPupil.Select(x => x.SubjectID).ToList();
                listSubCatByPupil = lstSubjectCat.Where(x => listSubCatIDByPupil.Contains(x.SubjectCatID)).ToList();
                //HS Backtrain
                listBackTrainIDByPupil = lstPupilBacktrain.Where(x => x == pupil.PupilID).ToList();

                objPupilRk = lstPupil.FirstOrDefault(x => x.PupilID == pupil.PupilID);
                objCategory = new CategoryOfPupilViewModel();
                objCategory = lstPupilByCategory.Where(p => p.PupilID == pupil.PupilID).FirstOrDefault();
                sheet.SetCellValue("A" + rowIndex, index);
                sheet.SetCellValue("B" + rowIndex, pupil.FullName);
                sheet.SetCellValue("C" + rowIndex, pupil.ClassName);
                sheet.SetCellValue("D" + rowIndex, objPupilRk != null ? objPupilRk.StudyingJudgementResolution : string.Empty);
                string DisplayMark = string.Empty;
                //string strMarkBySubject = string.Empty;
                var objSummanyMark = lstSummanyMark.FirstOrDefault(x => x.PupilID == pupil.PupilID);
                StringBuilder strMarkBySubject = new StringBuilder();
                foreach (SubjectCat subject in listSubCatByPupil)
                {
                    objPupilRetestBySj = listPupilRetestByPupil.Where(x => x.IsAbsentContest == true && x.SubjectID == subject.SubjectCatID).FirstOrDefault();
                    if (objSummanyMark != null)
                    {
                        DisplayMark = objSummanyMark.MarkData[subject.SubjectCatID.ToString()].ToString();
                    }
                    if (!string.IsNullOrEmpty(DisplayMark))
                    {
                        if (DisplayMark == "KH") // || "MG".Equals(DisplayMark) || "CN".Equals(DisplayMark)
                        {
                            DisplayMark = "Không đăng ký môn thi lại";
                        }
                        else if (DisplayMark == "V" || (objPupilRetestBySj != null && objPupilRetestBySj.IsAbsentContest == true)) // || "MG".Equals(DisplayMark) || "CN".Equals(DisplayMark)
                        {
                            DisplayMark = "Vắng thi";
                        }
                        else if (DisplayMark == "10,0")
                        {
                            DisplayMark = "10";
                        }
                        else
                        {
                            DisplayMark = DisplayMark.Replace(",", ".");
                        }
                    }
                    strMarkBySubject.Append(subject.SubjectName).Append(": ").Append(DisplayMark).Append("; ");
                }
                if (listBackTrainIDByPupil.Count > 0)
                {
                    strMarkBySubject.Append("Hạnh kiểm: ").Append(pupil.ConductLevelName);
                }
                //HS không đăng ký môn thi lại
                if (listSubCatByPupil.Count == 0 && listBackTrainIDByPupil.Count == 0)
                {
                    strMarkBySubject.Append("Không đăng ký môn thi lại");
                }
                sheet.SetCellValue("E" + rowIndex, strMarkBySubject.ToString().EndsWith("; ") ? strMarkBySubject.ToString().Trim().TrimEnd(';') : strMarkBySubject.ToString().ToString());
                sheet.SetCellValue("F" + rowIndex, pupil.CapacityLevel);
                sheet.SetCellValue("G" + rowIndex, objCategory.StudyingJudgementName);

                rowIndex++;
            }

            sheet.SetCellValue("A2", _globalInfo.SuperVisingDeptName);
            sheet.SetCellValue("A3", acaYear.SchoolProfile.SchoolName);
            sheet.SetCellValue("A5", string.Format("KẾT QUẢ THI LẠI, RÈN LUYỆN LẠI NĂM HỌC {0}", acaYear.DisplayTitle));
            sheet.GetRange(9, 1, rowIndex - 1, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            return oBook.ToStream();
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_SUMMANY_MARK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamInputMarkBusiness.GetSummanyMarkReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = this.GetDataForReport(form.EducationLevelID, form.ClassID);
                processedReport = ExamInputMarkBusiness.InsertSummanyMarkReport(dic, excel);
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;          
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            Stream excel = this.GetDataForReport(form.EducationLevelID, form.ClassID);
            ProcessedReport processedReport = ExamInputMarkBusiness.InsertSummanyMarkReport(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_SUMMANY_MARK,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion            
    }
}
