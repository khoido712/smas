﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea
{
    public class ReportTranscriptOfPrimaryClassAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportTranscriptOfPrimaryClassArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportTranscriptOfPrimaryClassArea_default",
                "ReportTranscriptOfPrimaryClassArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
