﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using System.IO;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea.Models;

namespace SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea.Controllers
{
    public class ReportTranscriptOfPrimaryClassController : BaseController
    {        
        private readonly ITranscriptOfPrimaryClassBusiness TranscriptOfPrimaryClassBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private GlobalInfo GlobalInfo = new GlobalInfo();
        public ReportTranscriptOfPrimaryClassController(ITranscriptOfPrimaryClassBusiness TranscriptOfPrimaryClassBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.TranscriptOfPrimaryClassBusiness = TranscriptOfPrimaryClassBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        public ActionResult Index()
        {
            ViewData[ReportTranscriptOfPrimaryClassConstants.LIST_EDUCATIONLEVEL] = new SelectList(GlobalInfo.EducationLevels, "EducationLevelID", "Resolution");
            int Educa = GlobalInfo.EducationLevels.FirstOrDefault().EducationLevelID;

            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.All.Where(o => o.EducationLevelID == Educa && o.SchoolID == GlobalInfo.SchoolID && o.AcademicYearID == GlobalInfo.AcademicYearID && o.IsActive.Value);
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (lsClass.Count() > 0)
            {
                lstClass = lsClass.ToList();
            }
            ViewData[ReportTranscriptOfPrimaryClassConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
            ViewData[ReportTranscriptOfPrimaryClassConstants.LIST_SUBJECT] = new SelectList(new List<SubjectCat>(), "SubjectCatID", "DisplayName");
            ViewData[ReportTranscriptOfPrimaryClassConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");
            return View();
        }

        [SkipCheckRole]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            IEnumerable<ClassProfile> lst = new List<ClassProfile>();
            if (EducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = GlobalInfo.AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                if (!GlobalInfo.IsAdminSchoolRole)
                {
                    dic["UserAccountID"] = GlobalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                }
                lst = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).OrderBy(o => o.DisplayName).ToList();
            }
            if (lst == null)
                lst = new List<ClassProfile>();
            return Json(new SelectList(lst, "ClassProfileID", "DisplayName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? ClassID)
        {
            IEnumerable<SubjectCat> lst = new List<SubjectCat>();
            if (ClassID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = ClassID;
                lst = this.ClassSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).Select(o => o.SubjectCat).ToList();
            }
            if (lst == null)
                lst = new List<SubjectCat>();
            return Json(new SelectList(lst, "SubjectCatID", "DisplayName"));
        }


        public JsonResult Export(ViewModel frm, int? GetNew)
        {
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            toc.AppliedLevel = GlobalInfo.AppliedLevel.Value;
            toc.SchoolID = GlobalInfo.SchoolID.Value;
            toc.ClassID = 0;
            if (frm.ClassID.HasValue)
            {
                toc.ClassID = frm.ClassID.Value;
            }
            toc.EducationLevelID = frm.EducationLevelID.Value;
            toc.Semester = frm.Semester.Value;
            if (frm.SubjectID.HasValue)
            {
                toc.SubjectID = frm.SubjectID.Value;
            }
            if (frm.Semester.Value == 3)
            {
                toc.Semester = 2;
            }
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (GetNew == null)
            {
                processedReport = this.TranscriptOfPrimaryClassBusiness.GetTranscriptOfPrimaryClass(toc);

                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptOfPrimaryClassBusiness.CreateTranscriptOfPrimaryClass(toc);
                processedReport = TranscriptOfPrimaryClassBusiness.InsertTranscriptOfPrimaryClass(toc, excel);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult ExportSummarise(ViewModelSummarise frm, int? GetNew)
        {
            TranscriptOfClass toc = new TranscriptOfClass();
            toc.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            toc.AppliedLevel = GlobalInfo.AppliedLevel.Value;
            toc.SchoolID = GlobalInfo.SchoolID.Value;
            toc.ClassID = 0;
            if (frm.ClassID.HasValue)
            {
                toc.ClassID = frm.ClassID.Value;
            }
            toc.EducationLevelID = frm.EducationLevelID.Value;
            toc.Semester = frm.Semester.Value;
            if (frm.Semester.Value == 3)
            {
                toc.Semester = 2;
            }
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (GetNew == null)
            {
                processedReport = this.TranscriptOfPrimaryClassBusiness.GetSummariseTranscriptOfPrimaryClass(toc);

                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = TranscriptOfPrimaryClassBusiness.CreateSummariseTranscriptOfPrimaryClass(toc);
                processedReport = TranscriptOfPrimaryClassBusiness.InsertSummariseTranscriptOfPrimaryClass(toc, excel);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1CN,
                SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1KYI,
                SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1KYII,
                SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP1
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


    }
}
