﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea
{
    public class ReportTranscriptOfPrimaryClassConstants
    {
        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
    }
}