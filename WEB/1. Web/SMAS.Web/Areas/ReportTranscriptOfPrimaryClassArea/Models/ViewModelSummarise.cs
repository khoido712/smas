﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea.Models
{
    public class ViewModelSummarise
    {
        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_CLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_SEMESTER)]
        public int? Semester { get; set; }
    }
}