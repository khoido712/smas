﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ReportTranscriptOfPrimaryClassArea.Models
{
    public class ViewModel
    {
        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_CLASS)]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_SUBJECT)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("ReportTranscriptOfPrimaryClass_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ReportTranscriptOfPrimaryClassConstants.LIST_SEMESTER)]
        public int? Semester { get; set; }
    }
}