﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.ReportExamResultArea.Controllers
{
    public class ReportExamResultController : Controller
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        private readonly IReportExamResultBusiness ReportExamResultBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        public ReportExamResultController(IClassProfileBusiness classProfileBusiness,
            IEducationLevelBusiness educationLevelBusiness,
            ICandidateBusiness candidateBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IExaminationBusiness examinationBusiness,
            IExaminationSubjectBusiness examinationSubjectBusiness,
            ISubjectCatBusiness subjectCatBusiness,
            IExaminationRoomBusiness examinationRoomBusiness,
            IReportExamResultBusiness ReportExamResultBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.CandidateBusiness = candidateBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.ReportExamResultBusiness = ReportExamResultBusiness;
        }

        #region Action on Page
        public ActionResult Index()
        {
            setViewData();
            return View();
        }

        public PartialViewResult ControlCombo()
        {
            setViewData();

            return PartialView("_ListControl");

        }



        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationRoom(int? idExaminationSubjectID, int? idExaminationID, int? idEducationLevelID)
        {
            ViewData[ReportExamResultConstants.LIST_EXAMINATION_ROOM] = new List<ExaminationRoom>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationSubjectID"] = idExaminationSubjectID;
            dic["ExaminationID"] = idExaminationID;
            dic["EducationLevelID"] = idEducationLevelID;
            //if (idExaminationSubjectID == null) return null;
            //if(ExaminationBusiness.Find(idExaminationID).UsingSeparateList == 0)


            if (idExaminationID != null)
            {
                Examination exam = ExaminationBusiness.Find(idExaminationID);
                if ((idExaminationSubjectID == 0 || idExaminationSubjectID == null) && exam.UsingSeparateList == 0)
                {
                    IDictionary<string, object> dic2 = new Dictionary<string, object>();
                    dic2["ExaminationID"] = idExaminationID;
                    dic2["EducationLevelID"] = idEducationLevelID;
                    dic2["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
                    dic2["SchoolID"] = GlobalInfo.SchoolID.GetValueOrDefault();
                    dic2["AppliedLevel"] = GlobalInfo.AppliedLevel.GetValueOrDefault();
                    ExaminationSubject examSubject = ExaminationSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.GetValueOrDefault(), dic2).Distinct().FirstOrDefault();
                    if (examSubject != null)
                    {
                        dic["ExaminationSubjectID"] = examSubject.ExaminationSubjectID;
                    }
                }
            }


            IQueryable<ExaminationRoom> lsRoom = ExaminationRoomBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic);
            var orderedlsRoom = lsRoom.ToList().OrderBy(r => SMAS.Business.Common.Utils.SortABC(r.RoomTitle)).ToList();
            if (lsRoom.Count() != 0)
            {
                ViewData[ReportExamResultConstants.LIST_EXAMINATION_ROOM] = orderedlsRoom;
            }

            //bool disable = false;
            //if (ExaminationBusiness.Find(idExaminationID).UsingSeparateList == 0)
            //{
            //    disable = true;
            //}

            return Json(new SelectList(orderedlsRoom, "ExaminationRoomID", "RoomTitle"), JsonRequestBehavior.AllowGet);
        }
        [ValidateAntiForgeryToken]
        public bool LoadPropertyExam(int? examinationID)
        {
            if (examinationID.HasValue && examinationID > 0)
            {
                if (ExaminationBusiness.All.Where(o => o.ExaminationID == examinationID).FirstOrDefault().UsingSeparateList == 1)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? idExaminationID, int? idEducationLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            List<ClassProfile> lsClass1 = new List<ClassProfile>() ;
            List<ClassProfile> lsClass2 = new List<ClassProfile>();
            List<ClassProfile> lsClass = new List<ClassProfile>();
            if (idEducationLevelID.HasValue && idEducationLevelID.Value > 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = idEducationLevelID;
                dic["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
                lsClass1 = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dic).ToList();
                //Lấy ra danh sách môn thi của khối thi
                List<int> lstExamSubject = ExaminationSubjectBusiness.All.Where(o => o.ExaminationID == idExaminationID && o.EducationLevelID == idEducationLevelID).Select(o => o.ExaminationSubjectID).ToList();
                lsClass2 = CandidateBusiness.All.Where(o => o.ExaminationID == idExaminationID).Where(o => (o.ClassProfile.EducationLevelID != idEducationLevelID && lstExamSubject.Contains(o.SubjectID.Value))).Select(o => o.ClassProfile).Distinct().ToList();
                lsClass = lsClass1.Concat(lsClass2).ToList();
                lsClass = lsClass.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                lsClass = new List<ClassProfile>();
            }
            ViewData[ReportExamResultConstants.LIST_CLASS] = lsClass;
            return Json(new SelectList(lsClass.ToList(), "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadDisableControl(int? idExaminationID)
        {

            Examination Examination = ExaminationBusiness.Find(idExaminationID);
            bool disable = false;
            if (Examination.UsingSeparateList == 0)
            {
                disable = true;
            }
            return Json(disable);

        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
            {
                dicExaminationSubject.Add("ExaminationID", examinationID.Value);
            }
            else
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct()
                .OrderBy(o => o.EducationLevelID).ToList();

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExaminationSubject(int? idExaminationID, int? idEducationLevelID)
        {
            ViewData[ReportExamResultConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubject>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["ExaminationID"] = idExaminationID;
            dic["EducationLevelID"] = idEducationLevelID;
            dic["AcademicYearID"] = GlobalInfo.AcademicYearID.GetValueOrDefault();
            dic["SchoolID"] = GlobalInfo.SchoolID.GetValueOrDefault();
            dic["AppliedLevel"] = GlobalInfo.AppliedLevel.GetValueOrDefault();
            List<ExaminationSubjectBO> result = new List<ExaminationSubjectBO>();
            if (idExaminationID > 0 && idEducationLevelID > 0)
            {
                IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.GetValueOrDefault(), dic).Distinct();
                IQueryable<ExaminationSubjectBO> lstExaminationSubject = (from es in qExaminationSubject
                                                                          join sc in SubjectCatBusiness.All on es.SubjectID equals sc.SubjectCatID
                                                                          orderby sc.OrderInSubject, sc.DisplayName
                                                                          select new ExaminationSubjectBO
                                                                       {
                                                                           ExaminationSubjectID = es.ExaminationSubjectID,
                                                                           DisplayName = sc.DisplayName
                                                                       });
                result = lstExaminationSubject.ToList();

            }

            if (result.Count() != 0)
            {
                ViewData[ReportExamResultConstants.LIST_EXAMINATION_SUBJECT] = result;
            }
            return Json(new SelectList(result, "ExaminationSubjectID", "DisplayName"), JsonRequestBehavior.AllowGet);
        }

        //Xuất báo cáo
        [HttpPost]
        public FileResult ExportExcel(FormCollection col)
        {
            int ExaminationID = 0;
            int EducationLevelID = 0;
            if (col["ExaminationID"] == null || col["ExaminationID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["ExaminationID"], out ExaminationID))
            {
                throw new BusinessException("Examination_Label_SetExaminationStageErrorMessage");
            }
            if (col["EducationLevelID"] == null || col["EducationLevelID"].Trim().Equals(string.Empty)
                || !int.TryParse(col["EducationLevelID"].Trim(), out EducationLevelID))
            {
                throw new BusinessException("InputExaminationMark_Label_EducationLevelNotAvailable");
            }
            GlobalInfo global = new GlobalInfo();
            ExaminationID = Convert.ToInt32(col["ExaminationID"]);
            EducationLevelID = Convert.ToInt32(col["EducationLevelID"]);
            int? ExaminationSubjectID;
            if (col["ExaminationSubjectID"] == "")
            {
                ExaminationSubjectID = 0;
            }
            else
            {
                ExaminationSubjectID = Convert.ToInt32(col["ExaminationSubjectID"]);
            }

            int? ExaminationRoomID;
            if (col["ExaminationRoomID"] == "")
            {
                ExaminationRoomID = 0;
            }
            else
            {
                ExaminationRoomID = Convert.ToInt32(col["ExaminationRoomID"]);
            }
            int? ClassID;
            if (col["ClassID"] == "")
            {
                ClassID = 0;
            }
            else
            {
                ClassID = Convert.ToInt32(col["ClassID"]);
            }
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = global.SchoolID.GetValueOrDefault();
            dic["AcademicYearID"] = global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = global.AppliedLevel.GetValueOrDefault();
            dic["ExaminationID"] = ExaminationID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["ExaminationSubjectID"] = ExaminationSubjectID;
            dic["ExaminationRoomID"] = ExaminationRoomID;
            dic["ClassID"] = ClassID;
            if (col["rdoReportType"] == "1") //Danh sách điểm thi theo lớp
            {
                Stream excel = ReportExamResultBusiness.CreateReportExamMarkByClass(global.SchoolID.Value, dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "THI_KetQuaThiTheoLop.xls";
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else if (col["rdoReportType"] == "2") //Danh sách điểm thi theo phòng thi
            {
                Examination exam = ExaminationBusiness.Find(ExaminationID);

                IDictionary<string, object> dic2 = new Dictionary<string, object>();
                dic2["ExaminationID"] = ExaminationID;
                dic2["EducationLevelID"] = EducationLevelID;
                dic2["AcademicYearID"] = global.AcademicYearID.GetValueOrDefault();
                dic2["SchoolID"] = global.SchoolID.GetValueOrDefault();
                dic2["AppliedLevel"] = global.AppliedLevel.GetValueOrDefault();
                dic["ExaminationSubjectID"] = ExaminationSubjectID;

                Stream excel = ReportExamResultBusiness.CreateReportExamMarkByRoom(global.SchoolID.Value, dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "THI_KetQuaThiTheoPhongThi.xls";
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else if (col["rdoReportType"] == "3") //Thống kê chất lượng lớp học
            {
                Stream excel = ReportExamResultBusiness.CreateReportQualityOfClass(global.SchoolID.Value, dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "Thi_ThongKeChatLuongCacLop.xls";
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else if (col["rdoReportType"] == "4") //Thống kê chất lượng môn thi
            {
                Stream excel = ReportExamResultBusiness.CreateReportQualityOfSubject(global.SchoolID.Value, dic);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                string ReportName = "Thi_ThongKeChatLuongMonThi.xls";
                ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
                result.FileDownloadName = ReportName;
                return result;
            }
            else
            {
                return null;
            }

        }
        #endregion

        #region Extent Method

        public void setViewData()
        {
            GlobalInfo Global = new GlobalInfo();
            //Danh sách khối học            
            ViewData[ReportExamResultConstants.LIST_EDUCATION_LEVEL] = new List<EducationLevel>();
            //Danh sách kỳ thi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Global.AcademicYearID.GetValueOrDefault();
            dic["AppliedLevel"] = Global.AppliedLevel.GetValueOrDefault();
            int schoolID = Global.SchoolID.Value;
            List<Examination> lstExamination = ExaminationBusiness.SearchBySchool(schoolID, dic).OrderByDescending(o => o.ToDate).ToList();

            ViewData[ReportExamResultConstants.LIST_EXAMINATION] = lstExamination;

            ViewData[ReportExamResultConstants.LIST_EXAMINATION_SUBJECT] = new List<ExaminationSubjectBO>();


            //Danh sách phòng thi
            IDictionary<string, object> dicRoom = new Dictionary<string, object>();
            ViewData[ReportExamResultConstants.LIST_EXAMINATION_ROOM] = new List<ExaminationRoom>();
            ViewData[ReportExamResultConstants.LIST_CLASS] = new List<ClassProfile>();
        }
        #endregion

        #region AjaxCheckExaminationProperty

        [ValidateAntiForgeryToken]
        public JsonResult AjaxCheckExaminationProperty(int? ExaminationID, int? EducationLevelID, int? ExaminationSubjectID, int? ButtonType)
        {
            ExaminationID = ExaminationID.HasValue ? ExaminationID.Value : 0;
            EducationLevelID = EducationLevelID.HasValue ? EducationLevelID.Value : 0;
            ExaminationSubjectID = ExaminationSubjectID.HasValue ? ExaminationSubjectID.Value : 0;
            ButtonType = ButtonType.HasValue ? ButtonType.Value : 0;
            Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
            if (exam != null)
            {
                if (exam.UsingSeparateList == 1 && ExaminationSubjectID == 0 && ButtonType == 2) //so thi sinh khac nhau,khong chon mon thi,danh sach theo phong thi
                {
                    return Json(new JsonMessage(Res.Get(""), "UsingSeparateListWithoutChooseSubject"));
                }
                else //so thi sinh giong nhau
                {
                    return Json(new JsonMessage(Res.Get("")));
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("")));
            }
        }
        #endregion

    }
}
