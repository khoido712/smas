﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportExamResultArea
{
    public class ReportExamResultAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportExamResultArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportExamResultArea_default",
                "ReportExamResultArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
