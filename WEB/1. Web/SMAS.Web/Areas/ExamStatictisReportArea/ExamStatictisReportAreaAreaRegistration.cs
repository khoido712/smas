﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamStatictisReportArea
{
    public class ExamStatictisReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamStatictisReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamStatictisReportArea_default",
                "ExamStatictisReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
