﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LockRateCommentPupilArea.Models
{
    public class LockRatedCommentViewModel
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public int DisabledGK { get; set; }
        public int DisabledCK { get; set; }
        public int? IsComment { get; set; }
        public List<string> Config { get; set; } 
    }
}