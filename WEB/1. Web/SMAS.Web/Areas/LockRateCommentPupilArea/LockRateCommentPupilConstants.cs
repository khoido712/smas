﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LockRateCommentPupilArea
{
    public class LockRateCommentPupilConstants
    {
        public const string LIST_RESULT = "List_Config_LockRateCommentPupilConstants_Result";
        public const string LIST_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LIST_CLASS = "LstClass";
        public const string LIST_SUBJECT = "LstSubject";

        public static string LIST_LRC = "LIST_LRC";

        public static string LIST_TALENT = "LIST_TALENT";

        public static string LIST_ETHIC = "LIST_ETHIC";
    }
}