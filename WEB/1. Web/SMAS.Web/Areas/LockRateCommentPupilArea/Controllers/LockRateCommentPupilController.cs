﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using Newtonsoft.Json;
using SMAS.Web.Areas.LockRateCommentPupilArea;
using SMAS.Web.Areas.LockRateCommentPupilArea.Models;

namespace SMAS.Web.Areas.LockRateCommentPupilArea.Controllers
{
    public class LockRateCommentPupilController : BaseController
    {
        //
        // GET: /LockRateCommentPupilArea/LockRateCommentPupil/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public LockRateCommentPupilController(IClassProfileBusiness classProfileBusiness,
                                          ILockRatedCommentPupilBusiness lockRatedCommentPupilBusiness,
                                          IClassSubjectBusiness classSubjectBusiness,
                                          IEducationLevelBusiness educationLevelBusiness,
                                          ISchoolProfileBusiness schoolProfileBusiness
                                         )
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.LockRatedCommentPupilBusiness = lockRatedCommentPupilBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }

        /// <summary>
        /// Trang chủ chức năng
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var lstEducation = _globalInfo.EducationLevels;
            ViewData[LockRateCommentPupilConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> classList = new List<SelectListItem>();
            if (lstEducation.Any())
            {
                dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
                dicClass.Add("EducationLevelID", lstEducation.FirstOrDefault().EducationLevelID);

                classList = Enumerable.ToList(this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass)
                        .OrderBy(u => u.DisplayName))
                        .Select(u => new SelectListItem
                        {
                            Value = u.ClassProfileID.ToString(),
                            Text = u.DisplayName,
                            Selected = false
                        }).ToList();
            }
            ViewData[LockRateCommentPupilConstants.LIST_CLASS] = classList;

            return View();
        }

        /// <summary>
        /// Tìm kiếm thông tin đã cấu hình theo danh sách khối và lớp
        /// </summary>
        /// <returns></returns>
        public ActionResult Search(int educationLevelId, int classId)
        {
            // Lấy dữ liệu đã cấu hình của lớp thuộc khối
            int academicYearId = (int)(_globalInfo.AcademicYearID ?? 0);
            int schoolId = (int)(_globalInfo.SchoolID ?? 0);
            List<LockRatedCommentPupil> list = LockRatedCommentPupilBusiness.All.Where(o => o.AcademicYearID == academicYearId
                                                    && o.SchoolID == schoolId && o.ClassID == classId).Select(o => o).ToList();

            var dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = classId;
            dicSubject["SchoolID"] = schoolId;
            dicSubject["EducationLevelID"] = educationLevelId;
            dicSubject["AcademicYearID"] = academicYearId;
            var lstSub = ClassSubjectBusiness.Search(dicSubject).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

            var result = new List<LockRatedCommentViewModel>();
            foreach (ClassSubject cs in lstSub)
            {
                result.Add(new LockRatedCommentViewModel()
                {
                    SubjectID = cs.SubjectID,
                    SubjectCode = cs.SubjectCat.Abbreviation,
                    SubjectName = cs.SubjectCat.DisplayName,
                    IsComment = cs.IsCommenting,
                    //Config = (list.Where(o => o.SubjectID == cs.SubjectID).Select(o => o.LockTitle).FirstOrDefault() ?? "").Split(',').ToList()
                });
            }

            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var config = list.FirstOrDefault(o => o.EvaluationID == 1 && o.SubjectID == item.SubjectID);
                    List<string> subConfig = new List<string>();
                    if (config != null)
                    {
                        if (!String.IsNullOrWhiteSpace(config.LockTitle))
                        {
                            string strConfig = config.LockTitle.Trim();
                            if (!String.IsNullOrWhiteSpace(strConfig))
                            {
                                subConfig = strConfig.Split(',').ToList();
                            }
                        }                       
                    }

                    item.Config = subConfig;

                    if (educationLevelId == 4 || educationLevelId == 5)
                    {
                        if (item.SubjectCode == "Tt" || item.SubjectCode == "TV")
                        {
                            item.DisabledGK = 0;
                        }
                        else
                        {
                            item.DisabledGK = 1;
                        }
                    }
                    else
                    {
                        item.DisabledGK = 1;
                    }
                }
            }

            List<string> listTalent = new List<string>();
            List<string> listEthic = new List<string>();

            var talentConfig = list.FirstOrDefault(o => o.EvaluationID == 2);
            if (talentConfig != null)
            {
                string talent = talentConfig.LockTitle.Trim();
                if (!string.IsNullOrWhiteSpace(talent))
                {
                    listTalent = talent.Split(',').ToList();
                }
            }

            var ethicConfig = list.FirstOrDefault(o => o.EvaluationID == 3);
            if (ethicConfig != null)
            {
                string ethic = ethicConfig.LockTitle.Trim();
                if (!string.IsNullOrWhiteSpace(ethic))
                {
                    listEthic = ethic.Split(',').ToList();
                }
            }

            ViewData[LockRateCommentPupilConstants.LIST_LRC] = result;
            ViewData[LockRateCommentPupilConstants.LIST_TALENT] = listTalent;
            ViewData[LockRateCommentPupilConstants.LIST_ETHIC] = listEthic;
            return PartialView("_LstClass");
        }

        /// <summary>
        /// Lấy danh sách lớp khi khối thay đổi
        /// </summary>
        /// <param name="eduId"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
            {
                return Json(new List<SelectListItem>());
            }
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            var lstClass = Enumerable.ToList(this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName))
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
            return Json(lstClass);
        }


        /// <summary>
        /// Lưu dữ liệu
        /// </summary>
        /// <param name="educationLevel"></param>
        /// <param name="classId"></param>
        /// <param name="lockTitleList">{LockTitle : true[false]}</param>
        /// <param name="submitType">0 lưu bình thường cho lớp, 1 Áp dụng toàn khối, 2 áp dụng toàn trường (hỏi lạ vì áp dụng thế hơi kinh, toàn cấp chắc hợp lý hơn)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Submit(int educationLevel, int classId, string subjectLockTitleStr, string talentStr, string ethicStr,string lstSub,
            int submitType = 0)
        {
            if (submitType == 0)
            {
                if (classId < 0)
                {
                    return Json(new JsonMessage(Res.Get("Chưa chọn lớp"), "error"));
                }
            }
            if (submitType == 1)
            {
                if (educationLevel < 0)
                {
                    return Json(new JsonMessage(Res.Get("Chưa chọn khối"), "error"));
                }
            }

            Dictionary<int, List<string>> subjectLockTitleDic = new Dictionary<int, List<string>>();
            if (!string.IsNullOrWhiteSpace(subjectLockTitleStr))
            {
                subjectLockTitleDic = JsonConvert.DeserializeObject<Dictionary<int, List<string>>>(subjectLockTitleStr);
            }
            List<string> talentLst = new List<string>();
            if (!string.IsNullOrWhiteSpace(talentStr))
            {
                talentLst = JsonConvert.DeserializeObject<List<string>>(talentStr);
            }
            List<string> ethicLst = new List<string>();
            if (!string.IsNullOrWhiteSpace(ethicStr))
            {
                ethicLst = JsonConvert.DeserializeObject<List<string>>(ethicStr);
            }
            List<int> subjectLst = new List<int>();
            if (!string.IsNullOrWhiteSpace(lstSub))
            {
                subjectLst = JsonConvert.DeserializeObject<List<int>>(lstSub);
            }

            // (1)
            // Lấy tất cả giá trị true từ lockTitleDic
            // Xóa tất cả các bản ghi trong csdl mà ko có LockTitle trong (in) nhóm khóa ở trên
            // Thêm mới vào csld nếu LockTitle chưa có

            // (2)
            // Kiểm tra submitType để áp dụng cho toàn khối toàn trường
            // Lấy danh sách tất cả các lớp
            // lặp lại các bước ở (1)
            // có thể tách (1) ra 1 hàm riêng (private)
            string result = "";
            GlobalInfo Global = new GlobalInfo();

            result = LockRatedCommentPupilBusiness.SaveLockRatedCommentPupil(submitType, Global.SchoolID.GetValueOrDefault(), educationLevel, classId, Global.AcademicYearID.GetValueOrDefault(), subjectLockTitleDic, talentLst, ethicLst, subjectLst);

            return Json(new JsonMessage(Res.Get("Lưu thành công"), "success"));
        }
    }
}
