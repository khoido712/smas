﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LockRateCommentPupilArea
{
    public class LockRateCommentPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LockRateCommentPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LockRateCommentPupilArea_default",
                "LockRateCommentPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
