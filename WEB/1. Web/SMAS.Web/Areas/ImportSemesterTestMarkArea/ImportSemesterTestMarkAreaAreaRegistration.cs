﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportSemesterTestMarkArea
{
    public class ImportSemesterTestMarkAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportSemesterTestMarkArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportSemesterTestMarkArea_default",
                "ImportSemesterTestMarkArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
