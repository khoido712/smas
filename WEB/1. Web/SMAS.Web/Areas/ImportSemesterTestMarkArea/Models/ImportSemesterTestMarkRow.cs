using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportSemesterTestMarkArea.Models
{
    public class ImportSemesterTestMarkRow
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public bool IsPupilValid { get; set; }

        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public bool IsClassValid { get; set; }

        public int MarkTypeID { get; set; }
        public int OrderNumber { get; set; }

        public string Title { get; set; }

        public List<ImportSemesterTestMarkSubject> ListSubject { get; set; }
        public List<string> ListError { get; set; }

        public int Status { get; set; }

        public bool IsValid { get; set; }

    }

    public class ImportSemesterTestMarkSubject
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public bool IsSubjectValid { get; set; }

        public string Mark { get; set; }
        public decimal? MarkValue { get; set; }
        public bool IsMarkValid { get; set; }

        public string Judgement { get; set; }
        public bool IsJudgementValid { get; set; }

        public bool IsCommenting { get; set; }
    }
}
