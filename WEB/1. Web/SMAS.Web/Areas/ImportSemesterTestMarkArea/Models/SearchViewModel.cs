﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportSemesterTestMarkArea.Models
{
    public class SearchViewModel
    {
        public int EducationLevelID { get; set; }

        public int? ClassID { get; set; }

        public string MarkType { get; set; }

        public int Semester { get; set; }
    }
}