﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.ImportSemesterTestMarkArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Common;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;
using SMAS.Web.Filter;
using System.Configuration;

namespace SMAS.Web.Areas.ImportSemesterTestMarkArea.Controllers
{
    //tai sao combo class chua it lop hon danh sach class trong lop
    public class ImportSemesterTestMarkController : Controller
    {
        private readonly IEducationLevelBusiness EducationLevelBu;
        private readonly IClassProfileBusiness ClassProfileBu;
        private readonly IPupilRetestRegistrationBusiness PupilRetestRegistrationBu;
        private readonly ISummedUpRecordBusiness SummedUpRecordBu;
        private readonly IMarkRecordBusiness MarkRecordBu;
        private readonly IPupilProfileBusiness PupilProfileBu;
        private readonly IMarkTypeBusiness MarkTypeBu;
        private readonly IAcademicYearBusiness AcademicYearBu;
        private readonly IJudgeRecordBusiness JudgeRecordBu;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBu;
        private readonly IReportDefinitionBusiness ReportDefinitionBu;
        private readonly IPupilOfClassBusiness PupilOfClassBu;
        private readonly ISchoolProfileBusiness SchoolProfileBu;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBu;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBu;
        private readonly IClassSubjectBusiness ClassSubjectBu;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;

        private const int START_ROW_INDEX = 9;
        private const int START_COLUMN_INDEX = 5;


        public ImportSemesterTestMarkController(IClassProfileBusiness classProfileBu,
                                                IPupilRetestRegistrationBusiness pupilRetestRegistrationBu,
                                                ISummedUpRecordBusiness summedUpRecordBu,
                                                IEducationLevelBusiness educationLevelBu,
                                                IMarkRecordBusiness markRecordBu,
                                                IPupilProfileBusiness pupilProfileBu,
                                                IMarkTypeBusiness markTypeBu,
                                                IAcademicYearBusiness academicYearBu,
                                                IJudgeRecordBusiness judgeRecordBu,
                                                IJudgeRecordHistoryBusiness judgeRecordHistoryBu,
                                                IReportDefinitionBusiness reportDefinitionBu,
                                                IPupilOfClassBusiness pupilOfClassBu,
                                                ISchoolProfileBusiness schoolProfileBu,
                                                IExemptedSubjectBusiness exemptedSubjectBu,
                                                ILockedMarkDetailBusiness lockedMarkDetailBu,
                                                IClassSubjectBusiness classSubjectBu,
                                                ISemeterDeclarationBusiness semesterDeclarationBu,
            ISubjectCatBusiness SubjectCatBusiness
            )
        {
            this.EducationLevelBu = educationLevelBu;
            this.ClassProfileBu = classProfileBu;
            this.PupilRetestRegistrationBu = pupilRetestRegistrationBu;
            this.SummedUpRecordBu = summedUpRecordBu;
            this.MarkRecordBu = markRecordBu;
            this.PupilProfileBu = pupilProfileBu;
            this.MarkTypeBu = markTypeBu;
            this.AcademicYearBu = academicYearBu;
            this.JudgeRecordBu = judgeRecordBu;
            this.JudgeRecordHistoryBu = judgeRecordHistoryBu;
            this.ReportDefinitionBu = reportDefinitionBu;
            this.PupilOfClassBu = pupilOfClassBu;
            this.SchoolProfileBu = schoolProfileBu;
            this.ExemptedSubjectBu = exemptedSubjectBu;
            this.LockedMarkDetailBu = lockedMarkDetailBu;
            this.ClassSubjectBu = classSubjectBu;
            this.SemeterDeclarationBusiness = semesterDeclarationBu;
            this.SubjectCatBusiness = SubjectCatBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadClass(int eduId)
        {
            if (eduId <= 0)
                return Json(new List<SelectListItem>());

            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", glo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            dicClass.Add("IsVNEN", true);
            if (glo.IsAdminSchoolRole || glo.IsViewAll || glo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
            {
                var lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
                return Json(lstClass);
            }
            else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
            {
                dicClass.Add("UserAccountID", glo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                var lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList()
                                                    .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                    .ToList();
                return Json(lstClass);
            }
            //end
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadMarkType(int eduId, int? cid, int sid)
        {
            if (eduId <= 0 || sid <= 0)
                return Json(new List<SelectListItem>());

            GlobalInfo glo = new GlobalInfo();

            List<SelectListItem> listMarkType = new List<SelectListItem>();

            // Lấy Danh sách điểm 1 tiết (Hệ số 2) và điểm kiểm tra HK
            listMarkType.AddRange(MarkRecordBu.GetMarkTitleForSemester(glo.SchoolID.Value, glo.AcademicYearID.Value, 0)
                                                .Split(',')
                                                .Where(u => !string.IsNullOrEmpty(u))
                                                .Select(u => new SelectListItem { Text = u == SystemParamsInFile.MARK_TYPE_HK ? "KTHK" : u, Value = u, Selected = false }));

            return Json(listMarkType);
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadLockMarkType(int eduId, int? cid, int sid)
        {
            if (eduId <= 0 || sid <= 0 || cid <= 0)
                return Json(new List<SelectListItem>());
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = glo.AcademicYearID.Value;
            dic["EducationLevelID"] = eduId;
            dic["ClassProfileID"] = cid;
            List<SelectListItem> listMarkType = new List<SelectListItem>();

            #region ds lop phan cong theo gv
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", glo.AcademicYearID);
            dicClass.Add("EducationLevelID", eduId);
            List<int> lstClass = new List<int>();
            if (glo.IsAdminSchoolRole || glo.IsViewAll || glo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
            {
                lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).Select(o => o.ClassProfileID).ToList();
            }
            else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
            {
                dicClass.Add("UserAccountID", glo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                lstClass = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).Select(o => o.ClassProfileID).ToList();
            }
            #endregion

            if (!cid.HasValue)
            {
                var listMark = this.MarkRecordBu
                                        .GetLockMarkTitleByEducation(glo.SchoolID.Value, glo.AcademicYearID.Value, eduId, sid)
                                        .Split(',')
                                        .Where(u => !string.IsNullOrEmpty(u.Trim()) && !u.ToUpper().StartsWith(SystemParamsInFile.MARK_TYPE_P) && !u.ToUpper().StartsWith(SystemParamsInFile.MARK_TYPE_M))
                                        .Select(u => new SelectListItem { Value = u, Text = u == SystemParamsInFile.MARK_TYPE_HK ? "KTHK" : u, Selected = false })
                                        .ToList();
                listMarkType.AddRange(listMark);
            }
            else
            {
                listMarkType = this.MarkRecordBu
                                        .GetLockMarkTitle(glo.SchoolID.Value, glo.AcademicYearID.Value, cid.Value, sid)
                                        .Split(',')
                                        .Where(u => !string.IsNullOrEmpty(u.Trim()) && !u.ToUpper().StartsWith(SystemParamsInFile.MARK_TYPE_P) && !u.ToUpper().StartsWith(SystemParamsInFile.MARK_TYPE_M))
                                        .Select(u => new SelectListItem { Value = u, Text = u == SystemParamsInFile.MARK_TYPE_HK ? "KTHK" : u, Selected = false })
                                        .ToList();
            }

            // AnhVD 201410 - Nếu khóa điểm HK(LHK) thì được Import tất cả
            List<SelectListItem> listMarkLoad = new List<SelectListItem>();
            if (listMarkType.Any(o => o.Text.Equals(SystemParamsInFile.MARK_TYPE_LHK)))
            {
                listMarkLoad = this.MarkRecordBu.GetMarkTitleForSemester(glo.SchoolID.Value, glo.AcademicYearID.Value, sid)
                                                .Split(',')
                                                .Where(u => !string.IsNullOrEmpty(u))
                                                .Select(u => new SelectListItem { Text = u == SystemParamsInFile.MARK_TYPE_HK ? "KTHK" : u, Value = u, Selected = false })
                                                .ToList();
            }
            else
            {
                listMarkLoad = listMarkType.OrderBy(o => o.Text).ToList();

            }
            return Json(listMarkLoad);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadDataGrid(SearchViewModel model, int? Page)
        {
            GlobalInfo glo = new GlobalInfo();
            if (ModelState.IsValid)
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object> {
                                                                                        { "EducationLevelID", model.EducationLevelID },
                                                                                        { "ClassID", model.ClassID },
                                                                                        { "AcademicYearID", glo.AcademicYearID.Value },
                                                                                        { "IsApprenticeShipSubject", false },
                                                                                        { "Semester", model.Semester },
                                                                                        { "IsVNEN",true}
                 };
                if (!glo.IsAdminSchoolRole || !glo.IsViewAll || !glo.IsEmployeeManager)
                {
                    SearchInfo.Add("UserAccountID", glo.UserAccountID);
                    SearchInfo.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                    var lstClassIDWithRole = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, SearchInfo).Select(o => o.ClassProfileID).ToList();
                    if (lstClassIDWithRole != null && lstClassIDWithRole.Count == 0)
                    {
                        lstClassIDWithRole.Add(0); // Tìm với dsach rỗng
                    }
                    SearchInfo.Add("LstClassID ", lstClassIDWithRole);
                }
                List<ClassSubjectBO> listSubject = ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                            .Select(u => new ClassSubjectBO
                                                            {
                                                                SubjectID = u.SubjectID,
                                                                SubjectName = u.SubjectCat.DisplayName,
                                                                DisplayName = u.SubjectCat.DisplayName,
                                                                OrderInSubject = u.SubjectCat.OrderInSubject,
                                                                IsCommenting = u.IsCommenting
                                                            })
                                                            .Distinct()
                                                            .OrderBy(u => u.OrderInSubject)
                                                            .ThenBy(u => u.DisplayName)
                                                            .ToList();
                List<List<PupilForImportBO>> listImportPupil = MarkRecordBu.GetListPupilForImport(glo.UserAccountID, glo.SchoolID.Value, glo.AcademicYearID.Value, model.EducationLevelID, model.ClassID, (int)model.Semester, model.MarkType).ToList();

                ViewData[ImportSemesterTestMarkContants.LIST_SUBJECT_CAT] = listSubject;
                AcademicYear acaYear = AcademicYearBu.Find(glo.AcademicYearID.Value);
                //Lay ngay ket thuc cua hoc ky
                if (model.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    ViewData[ImportSemesterTestMarkContants.SEMESTER_END_DATE] = acaYear.FirstSemesterEndDate;
                else
                    ViewData[ImportSemesterTestMarkContants.SEMESTER_END_DATE] = acaYear.SecondSemesterEndDate;

                //TungNT48 23/4/2013 start edit: Kiểm tra quyền-Chỉ cho Import đối với QTHT, giáo vụ có quyền GVBM (check theo thời gian học kỳ)

                bool disabled = false;

                bool permission = false;

                if (model.ClassID != null)
                {
                    permission = UtilsBusiness.HasSubjectTeacherPermissionSupervising(glo.UserAccountID, (int)model.ClassID);
                }
                if (glo.IsAdminSchoolRole)
                {
                    permission = true;
                }

                if (permission)
                {
                    int nowSemeter = 0;
                    if (DateTime.Now.Date >= acaYear.FirstSemesterStartDate && DateTime.Now.Date <= acaYear.FirstSemesterEndDate)
                    {
                        nowSemeter = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    }
                    else
                    {
                        nowSemeter = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    }
                    bool timeYear = model.Semester == nowSemeter;

                    if (timeYear)
                    {
                        disabled = true;
                    }
                    else
                    {
                        disabled = false;
                    }

                    if (glo.IsAdminSchoolRole == true)
                    {
                        disabled = true;
                    }

                    if (glo.IsAdminSchoolRole != true && glo.IsSchoolRole && disabled)
                    {
                        disabled = true;
                    }
                    //Kiểm tra thời gian AcademicYear(Semester).EndDate < DateTime.Now => Disable. Ngược lại thì Enable

                }
                ViewData[ImportSemesterTestMarkContants.ENABLE_BUTTON] = disabled;

                return PartialView("_DataGrid", listImportPupil);
            }
            return PartialView("_DataGrid", null);
        }

        public PartialViewResult LoadExcelGrid()
        {
            return PartialView("_ExcelGrid");
        }

        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Import(SearchViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

            if (file == null)
            {
                return Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), JsonMessage.ERROR));
            }

            if (!file.FileName.EndsWith(".xls"))
            {
                return Json(new JsonMessage(Res.Get("InputExaminationMark_Label_FileIsNotExcel"), JsonMessage.ERROR));
            }

            if (file.ContentLength > 1024000)
            {
                return Json(new JsonMessage(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"), JsonMessage.ERROR));
            }

            string markTitle = string.Empty;
            int orderNumber = 0;
            Regex rg = new Regex("^(\\w+)(\\d{1,2})$", RegexOptions.IgnoreCase);
            if (rg.IsMatch(model.MarkType))
            {
                Match m = rg.Match(model.MarkType);
                if (m.Groups.Count == 3)
                {
                    markTitle = m.Groups[1].Value;
                    orderNumber = Convert.ToInt32(m.Groups[2].Value);
                }
            }
            else
                markTitle = model.MarkType;

            MarkType markType = this.MarkTypeBu.Search(new Dictionary<string, object> { { "Title", markTitle }, { "AppliedLevel", glo.AppliedLevel.Value } }).FirstOrDefault();

            if ((string.IsNullOrEmpty(markTitle) || orderNumber <= 0 || markType == null) && markTitle != SystemParamsInFile.MARK_TYPE_HK)
            {
                return Json(new JsonMessage(Res.Get("ImportSemesterTestMark_Label_ImportMarkTypeError"), JsonMessage.ERROR));
            }

            IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTRange range = sheet.GetUsedRange();
            List<ClassProfile> listClassCheck = new List<ClassProfile>();

            #region ds lop phan cong theo gv
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", glo.AcademicYearID);
            dicClass.Add("EducationLevelID", model.EducationLevelID);
            List<int> lstClass = new List<int>();
            if (glo.IsAdminSchoolRole || glo.IsViewAll || glo.IsEmployeeManager) // nếu là QTHT: cho phép load ra tất cả các lớp
            {
                listClassCheck = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            }
            else // load ra lớp theo phân quyền giáo viên bộ môn hoặc giáo vụ có quyền giáo viên bộ môn hoac gvcn hoac giao vu co quyen gvcn
            {
                dicClass.Add("UserAccountID", glo.UserAccountID);
                dicClass.Add("Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECT_OVERSEEING);
                listClassCheck = this.ClassProfileBu.SearchBySchool(glo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            }
            #endregion

            Dictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["AcademicYearID"] = glo.AcademicYearID.Value;
            dicPupil["EducationLevelID"] = model.EducationLevelID;
            //dicPupil["Status"] = SystemParamsInFile.PUPIL_STATUS_STUDYING;
            //dicPupil["Check"] = "Check";
            if (model.ClassID.HasValue && model.ClassID.Value > 0) dicPupil["ClassID"] = model.ClassID.Value;
            var listPupilCheck = PupilOfClassBu.SearchBySchool(glo.SchoolID.Value, dicPupil)
                .Select(o => new
                {
                    o.PupilID,
                    o.ClassID,
                    o.PupilProfile.PupilCode,
                    o.Status
                }).ToList();

            Dictionary<string, object> dicLockMark = new Dictionary<string, object>();
            dicLockMark["AcademicYearID"] = glo.AcademicYearID.Value;
            dicLockMark["EducationLevelID"] = model.EducationLevelID;
            dicLockMark["Semester"] = model.Semester;
            if (model.ClassID.HasValue && model.ClassID.Value > 0) dicLockMark["ClassID"] = model.ClassID.Value;
            List<LockedMarkDetail> listLockedMarkDetailCheck = LockedMarkDetailBu.SearchBySchool(glo.SchoolID.Value, dicLockMark).ToList();

            List<SubjectCat> listSubject = MarkRecordBu.GetListSubject(glo.SchoolID.Value, glo.AcademicYearID.Value, model.EducationLevelID, model.ClassID, (int)model.Semester, model.MarkType).Distinct().ToList();
            // Danh sach mon hoc theo lop
            List<ClassSubjectBO> listClassSubject = (from p in ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, dicLockMark)
                                                     join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                     select new ClassSubjectBO
                                                     {
                                                         ClassSubjectID = p.ClassSubjectID,
                                                         ClassID = p.ClassID,
                                                         SubjectID = p.SubjectID,
                                                         DisplayName = q.DisplayName,
                                                         IsCommenting = p.IsCommenting,
                                                         AppliedType = p.AppliedType
                                                     })
                                                  .ToList();
            List<ImportSemesterTestMarkRow> listPupil = new List<ImportSemesterTestMarkRow>();

            //string title
            string titleExcel1 = GetCellString(sheet, 5, 4).ToLower();
            string titleExcel2 = GetCellString(sheet, 6, 4).ToLower();
            if (!titleExcel1.Contains(model.MarkType.ToLower()))
            {
                return Json(new JsonMessage("Loại điểm trong file excel không phải là loại điểm được chọn trên hệ thống", JsonMessage.ERROR));
            }
            if (!titleExcel2.Contains(model.Semester == 1 ? "hki " : "hkii"))
            {
                return Json(new JsonMessage("Học kỳ trong file excel không phải là học kỳ được chọn trên hệ thống", JsonMessage.ERROR));
            }
            AcademicYear acad = AcademicYearBu.Find(new GlobalInfo().AcademicYearID.Value);
            if (acad != null)
            {
                if (!titleExcel2.Contains(acad.Year + " - " + (acad.Year + 1).ToString()))
                {
                    return Json(new JsonMessage("Năm học trong file excel không phải là năm học được chọn trên hệ thống", JsonMessage.ERROR));
                }
            }

            #region read excel file
            //Lay danh sach cac mon hoc trong file excel
            List<string> listSubjectHead = new List<string>();
            for (int columnIndex = START_COLUMN_INDEX; columnIndex <= range.TotalColumn; columnIndex++)
            {
                listSubjectHead.Add(GetCellString(sheet, START_ROW_INDEX - 1, columnIndex));
            }

            //xoa cac cot mon hoc rong o cuoi (range.TotalColumn min la cot I : 9)
            while (listSubjectHead.Count > 0 && listSubjectHead[listSubjectHead.Count - 1] == "")
                listSubjectHead.RemoveAt(listSubjectHead.Count - 1);

            //Bat dau doc du lieu tu bat dau tu START_ROW_INDEX
            int rowIndex = START_ROW_INDEX;
            while (!string.IsNullOrEmpty(GetCellString(sheet, rowIndex, 1)))
            {
                ImportSemesterTestMarkRow pupil = new ImportSemesterTestMarkRow();
                pupil.ClassName = GetCellString(sheet, rowIndex, 2);
                pupil.PupilCode = GetCellString(sheet, rowIndex, 3);
                pupil.FullName = GetCellString(sheet, rowIndex, 4);
                pupil.MarkTypeID = markType.MarkTypeID;
                pupil.OrderNumber = orderNumber;
                pupil.Title = model.MarkType;

                pupil.ListSubject = new List<ImportSemesterTestMarkSubject>();
                pupil.ListError = new List<string>();

                //Gan du lieu cho cac mon hoc ung voi moi hoc sinh
                for (int columnIndex = START_COLUMN_INDEX; columnIndex < START_COLUMN_INDEX + listSubjectHead.Count; columnIndex++)
                {
                    ImportSemesterTestMarkSubject importSubject = new ImportSemesterTestMarkSubject();
                    importSubject.SubjectName = listSubjectHead[columnIndex - START_COLUMN_INDEX];
                    importSubject.Mark = importSubject.Judgement = GetCellString(sheet, rowIndex, columnIndex);
                    importSubject.MarkValue = GetCellDecimal(sheet, rowIndex, columnIndex);
                    pupil.ListSubject.Add(importSubject);
                }
                listPupil.Add(pupil);
                rowIndex++;
            }
            #endregion
            var listClassHas = listClassCheck.Select(o => o.ClassProfileID).Distinct().ToList();
            var listExemptedSubject = ExemptedSubjectBu.GetListExemptedSubject(listClassHas, (int)model.Semester).ToList();

            // Luu dictionary de khong phai tim kiem lai trong list
            Dictionary<int, List<ClassSubjectBO>> dicClassSubject = new Dictionary<int, List<ClassSubjectBO>>();

            foreach (int classID in listClassHas)
            {

                dicClassSubject[classID] = listClassSubject.Where(o => o.ClassID == classID && listLockedMarkDetailCheck.Any(u => u.SubjectID == o.SubjectID)).ToList();
            }

            #region check input data
            foreach (ImportSemesterTestMarkRow pupil in listPupil)
            {
                ClassProfile checkClass = listClassCheck.Where(u => u.DisplayName.ToLower().Equals(pupil.ClassName.Trim().ToLower())).FirstOrDefault();
                pupil.IsClassValid = checkClass != null;
                pupil.ClassID = checkClass != null ? checkClass.ClassProfileID : 0;
                if (!pupil.IsClassValid) pupil.ListError.Add(Res.Get("ImportSemesterTestMark_Label_ClassNotExist"));

                var checkPupil = !string.IsNullOrEmpty(pupil.PupilCode) ? listPupilCheck.Where(u => u.ClassID == pupil.ClassID && u.PupilCode.ToLower().Trim() == pupil.PupilCode.ToLower().Trim()).FirstOrDefault() : null;

                pupil.IsPupilValid = checkPupil != null;
                pupil.Status = checkPupil != null ? checkPupil.Status : 0;
                pupil.PupilID = checkPupil != null ? checkPupil.PupilID : 0;
                pupil.IsValid = true;


                if (!pupil.IsPupilValid)
                {
                    //break;
                    pupil.ListError.Add(Res.Get("PupilProfile_Label_Not_Pupil_Status_Studying"));
                    pupil.IsValid = false;
                    continue;
                }
                if (pupil.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }

                else if (checkClass != null) // Lop hoc ton tai
                {
                    int countSubject = pupil.ListSubject.Count;
                    for (int i = countSubject - 1; i >= 0; i--)
                    {
                        ImportSemesterTestMarkSubject importSubject = pupil.ListSubject[i];
                        SubjectCat subject = listSubject.Where(u => u.DisplayName.Trim().ToLower().Equals(importSubject.SubjectName.Trim().ToLower())).FirstOrDefault();
                        importSubject.IsSubjectValid = subject != null;
                        importSubject.SubjectID = subject != null ? subject.SubjectCatID : 0;

                        bool ValidMark = true;
                        if (subject != null) //Mon hoc ton tai
                        {
                            ClassSubjectBO classSubject = null;
                            if (dicClassSubject.ContainsKey(checkClass.ClassProfileID))
                            {
                                classSubject = dicClassSubject[checkClass.ClassProfileID].Where(u => u.SubjectID == subject.SubjectCatID).FirstOrDefault();
                            }

                            if (classSubject != null) //Mon hoc thuoc lop hoc
                            {
                                if (listLockedMarkDetailCheck.Any(u => u.ClassID == checkClass.ClassProfileID && u.SubjectID == subject.SubjectCatID && u.Semester == model.Semester
                                                                        && ((u.MarkType.Title + u.MarkIndex == model.MarkType) || (model.MarkType == "HK" && u.MarkType.Title == "HK") || u.MarkType.Title == "LHK")
                                                                        )) //Khoa con diem
                                {
                                    bool isExempted = listExemptedSubject.Any(o => o.ClassID == pupil.ClassID && o.PupilID == pupil.PupilID && o.AcademicYearID == glo.AcademicYearID && o.SubjectID == subject.SubjectCatID);
                                    if (!isExempted && importSubject.Mark.Length > 0) //Mon hoc khong duoc mien giam
                                    {
                                        importSubject.IsCommenting = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE;
                                        float mark = 0;

                                        if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        {
                                            //neu la mon nhan xet thi phai la C hoac CD
                                            importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.Judgement == "" || importSubject.Judgement == SystemParamsInFile.JUDGE_MARK_D || importSubject.Judgement == SystemParamsInFile.JUDGE_MARK_CD;
                                            if (!importSubject.IsJudgementValid) { ValidMark = false; pupil.ListError.Add(Res.Get("ImportSemesterTestMark_Label_JudgeMustCorCD")); }
                                        }
                                        else
                                        {
                                            if (importSubject.MarkValue.HasValue || importSubject.Mark.Length > 0)
                                            {
                                                //MarkValue = null neu nhu khong nhap vao va coi nhu khong co diem, = 0 neu nhu nhap vao la 0
                                                importSubject.IsSubjectValid = importSubject.IsMarkValid = importSubject.MarkValue >= 0 && importSubject.MarkValue <= 10;
                                                if (!importSubject.IsMarkValid || !float.TryParse(importSubject.Mark, out mark)) { ValidMark = false; importSubject.IsMarkValid = false; pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_MarkInvalid"), subject.DisplayName)); }
                                            }
                                            else
                                            {
                                                importSubject.IsSubjectValid = importSubject.IsMarkValid = importSubject.Mark == "";
                                                if (!importSubject.IsMarkValid || !float.TryParse(importSubject.Mark, out mark)) { ValidMark = false; importSubject.IsMarkValid = false; pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_MarkInvalid"), subject.DisplayName)); }
                                            }
                                        }
                                    }
                                    else //Mon hoc duoc mien giam
                                    {
                                        //comment NamTa bo qua voi hoc sinh mien giam
                                        if (!string.IsNullOrEmpty(importSubject.Mark) && importSubject.Mark.Trim().ToUpper() != Res.Get("Common_Label_MG").Trim().ToUpper())
                                        {
                                            importSubject.IsMarkValid = false;
                                            if (!importSubject.IsMarkValid) pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_SubjectExempted"), importSubject.SubjectName));
                                        }
                                        else
                                        {
                                            importSubject.IsMarkValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = true;
                                        }
                                    }
                                }
                                else //Chua khoa con diem
                                {
                                    //importSubject.IsSubjectValid = false;
                                    //importSubject.IsMarkValid = importSubject.IsJudgementValid = true;
                                    //importSubject.IsSubjectValid = importSubject.IsMarkValid = importSubject.IsJudgementValid = false;
                                    //if (!importSubject.IsMarkValid)
                                    //{
                                    //    pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_NotLockMark"), markTitle, importSubject.SubjectName));
                                    //}
                                    importSubject.IsSubjectValid = importSubject.IsMarkValid = importSubject.IsJudgementValid = true;
                                }
                            }
                            else
                            {
                                //Neu mon hoc khong thuoc lop hoc ma van nhap diem thi bao loi
                                //bo qua break

                                if (!string.IsNullOrEmpty(importSubject.Mark) || !string.IsNullOrEmpty(importSubject.Judgement))
                                {
                                    importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = false;
                                    pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_SubjectNotBelongToClass"), importSubject.SubjectName));
                                }
                                else //Khong nhap diem thi -> hop le
                                {
                                    importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = true;
                                }
                            }
                        }
                        else
                        {
                            if (listClassSubject.Any(o => o.ClassID == pupil.ClassID && o.DisplayName.Trim().ToLower().Equals(importSubject.SubjectName.Trim().ToLower())))
                            {
                                importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = true;
                            }
                            else if (!string.IsNullOrEmpty(importSubject.Judgement) || importSubject.MarkValue.HasValue) //Co nhap diem
                            {
                                importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = false;
                                pupil.ListError.Add(string.Format(Res.Get("ImportSemesterTestMark_Label_SubjectNotBelongToClass"), importSubject.SubjectName));
                            }
                            else
                            {

                                importSubject.IsSubjectValid = importSubject.IsJudgementValid = importSubject.IsMarkValid = true;
                            }
                        }
                        if (!ValidMark || !pupil.IsValid || !importSubject.IsSubjectValid
                            || (!importSubject.IsMarkValid && !importSubject.IsJudgementValid))
                        {
                            pupil.IsValid = false;
                        }
                        else
                        {
                            pupil.IsValid = true;
                        }

                    }
                }
                pupil.IsValid = pupil.IsClassValid && pupil.IsPupilValid && pupil.IsValid;
            }
            #endregion
            if (!listPupil.Any(u => !u.IsValid))
            {
                SaveData(model, listPupil);
                return Json(new JsonMessage(Res.Get("ImportSemesterTestMark_Label_ImportSuccess"), JsonMessage.SUCCESS));
            }
            else
            {
                Session[ImportSemesterTestMarkContants.LIST_IMPORTED_PUPIL] = listPupil;
                return Json(new JsonMessage(Res.Get("ImportSemesterTestMark_Label_ImportError"), "er_detail"));
            }
        }

        public FileStreamResult Export(SearchViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();

                ReportDefinition reportDefinition = ReportDefinitionBu.GetByCode(SystemParamsInFile.IMPORT_DIEMTHI_HOCKY);

                EducationLevel eduLevel = EducationLevelBu.Find(model.EducationLevelID);

                string template = ReportUtils.GetTemplatePath(reportDefinition);
                string reportName = reportDefinition.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(model.Semester);


                IVTWorkbook oBook = VTExport.OpenWorkbook(template);
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet styleSheet = oBook.GetSheet(2);

                IVTRange mgRange = styleSheet.GetRange("A1", "A1");
                IVTRange mgRange5 = styleSheet.GetRange("F1", "F1");
                IVTRange unlockRange = styleSheet.GetRange("B1", "B1");
                IVTRange unlockRange5 = styleSheet.GetRange("G1", "G1");
                IVTRange leavedOffRow = styleSheet.GetRange("A2", "D2");
                IVTRange movedClassRow = styleSheet.GetRange("A3", "D3");
                IVTRange movedSchoolRow = styleSheet.GetRange("A4", "D4");
                IVTRange NotStudyingRowDiv5 = styleSheet.GetRange("A10", "D10");


                List<ClassSubjectBO> listSubject = ClassSubjectBu.SearchBySchool(glo.SchoolID.Value
                                                                                , new Dictionary<string, object> {
                                                                                        { "EducationLevelID", model.EducationLevelID },
                                                                                        { "ClassID", model.ClassID },
                                                                                        { "AcademicYearID", glo.AcademicYearID.Value },
                                                                                        { "IsApprenticeShipSubject", false },
                                                                                        { "Semester", model.Semester },
                                                                                        { "IsVNEN",true}})
                                                            .Select(u => new ClassSubjectBO
                                                            {
                                                                SubjectID = u.SubjectID,
                                                                SubjectName = u.SubjectCat.DisplayName,
                                                                DisplayName = u.SubjectCat.DisplayName,
                                                                OrderInSubject = u.SubjectCat.OrderInSubject,
                                                                IsCommenting = u.IsCommenting
                                                            })
                                                            .Distinct()
                                                            .OrderBy(u => u.OrderInSubject)
                                                            .ThenBy(u => u.DisplayName)
                                                            .ToList();
                // Du lieu diem
                List<List<PupilForImportBO>> listImportPupil = MarkRecordBu.GetListPupilForImport(glo.UserAccountID, glo.SchoolID.Value, glo.AcademicYearID.Value, model.EducationLevelID, model.ClassID, (int)model.Semester, model.MarkType).ToList();

                Dictionary<string, object> dicLockMark = new Dictionary<string, object>();
                dicLockMark["AcademicYearID"] = glo.AcademicYearID.Value;
                dicLockMark["EducationLevelID"] = model.EducationLevelID;
                dicLockMark["Semester"] = model.Semester;
                if (model.ClassID.HasValue && model.ClassID.Value > 0) dicLockMark["ClassID"] = model.ClassID.Value;
                List<LockedMarkDetail> listLockedMarkDetailCheck = LockedMarkDetailBu.SearchBySchool(glo.SchoolID.Value, dicLockMark).ToList();

                // Diem mon tinh diem
                List<PupilForImportBO> listMark = listImportPupil[0];
                // Diem mon nhan xet
                List<PupilForImportBO> listJudge = listImportPupil[1];

                var distinctPupil = listJudge.GroupBy(u => new { u.PupilID, u.ClassID, u.FullName, u.PupilCode, u.ClassName, u.Status }).ToList();
                int totalPupil = distinctPupil.Count();
                #region fill data
                IVTRange subjectCol = tempSheet.GetRange("E8", "E10");
                for (int i = 0; i < listSubject.Count; i++)
                {
                    tempSheet.CopyPaste(subjectCol, START_ROW_INDEX - 1, START_COLUMN_INDEX + i);
                    tempSheet.SetCellValue(START_ROW_INDEX - 1, START_COLUMN_INDEX + i, listSubject[i].DisplayName);
                }
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, VTVector.ColumnIntToString(START_COLUMN_INDEX + listSubject.Count - 1) + 10);
                IVTRange dataRow = dataSheet.GetRange(START_ROW_INDEX, 1, START_ROW_INDEX, START_COLUMN_INDEX + listSubject.Count - 1);
                IVTRange RowDiv5 = dataSheet.GetRange((START_ROW_INDEX + 1), 1, (START_ROW_INDEX + 1), START_COLUMN_INDEX + listSubject.Count - 1);
                int rowIndex = 1;
                AcademicYear ay = AcademicYearBu.Find(glo.AcademicYearID);
                DateTime? startSemester1 = ay.FirstSemesterStartDate;
                DateTime? endSemester1 = ay.FirstSemesterEndDate;
                DateTime? startSemester2 = ay.SecondSemesterStartDate;
                DateTime? endSemester2 = ay.SecondSemesterEndDate;
                List<PupilForImportBO> listMarkPupil = new List<PupilForImportBO>();
                List<PupilForImportBO> listJudgePupil = new List<PupilForImportBO>();
                foreach (var pupil in distinctPupil)
                {
                    // Lay diem theo HS
                    listMarkPupil = listMark.Where(o => o.PupilID == pupil.Key.PupilID && o.ClassID == pupil.Key.ClassID).ToList();
                    listJudgePupil = listJudge.Where(o => o.PupilID == pupil.Key.PupilID && o.ClassID == pupil.Key.ClassID).ToList();
                    int currentRow = rowIndex + START_ROW_INDEX;
                    if (rowIndex % 5 == 0 || rowIndex == totalPupil)
                    {
                        if (pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF ||
                            pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                            pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                            pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            tempSheet.CopyPaste(NotStudyingRowDiv5, currentRow, 1, true);
                        }
                        else
                        {
                            tempSheet.CopyPaste(RowDiv5, currentRow, 1, true);
                        }
                    }
                    else
                    {
                        tempSheet.CopyPaste(dataRow, currentRow, 1, true);

                        if (pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                            tempSheet.CopyPaste(leavedOffRow, currentRow, 1, true);

                        if (pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                            tempSheet.CopyPaste(movedClassRow, currentRow, 1, true);

                        if (pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                            tempSheet.CopyPaste(movedSchoolRow, currentRow, 1, true);
                        if (pupil.Key.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            tempSheet.CopyPaste(movedSchoolRow, currentRow, 1, true);
                    }
                    tempSheet.SetCellValue(currentRow, 1, rowIndex);
                    tempSheet.SetCellValue(currentRow, 2, pupil.Key.ClassName);
                    tempSheet.SetCellValue(currentRow, 3, pupil.Key.PupilCode);
                    tempSheet.SetCellValue(currentRow, 4, pupil.Key.FullName);

                    int colIndex = START_COLUMN_INDEX;

                    foreach (ClassSubjectBO subject in listSubject)
                    {
                        PupilForImportBO item = null;
                        // Kiem tra xem mon nhan xet hay tinh diem de lay du lieu tuong ung
                        if (subject.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            item = listJudgePupil.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();
                        else
                            item = listMarkPupil.Where(o => o.SubjectID == subject.SubjectID).FirstOrDefault();

                        if (item != null)
                        {
                            if (!listLockedMarkDetailCheck.Any(u => u.ClassID == item.ClassID && u.SubjectID == subject.SubjectID
                                                                    && ((u.MarkType.Title + u.MarkIndex == model.MarkType) || (model.MarkType == "HK" && u.MarkType.Title == "HK")
                                                                    || u.MarkType.Title == "LHK"
                                                                    ))) //Khoa con diem
                            {
                                if (rowIndex % 5 == 0 || rowIndex == totalPupil)
                                {
                                    tempSheet.CopyPaste(unlockRange5, currentRow, colIndex, true);
                                }
                                else
                                {
                                    tempSheet.CopyPaste(unlockRange, currentRow, colIndex, true);
                                }
                            }

                            if (item.IsExempted)
                            {
                                if (rowIndex % 5 == 0 || rowIndex == totalPupil)
                                {
                                    tempSheet.CopyPaste(mgRange5, currentRow, colIndex, true);
                                }
                                else
                                {
                                    tempSheet.CopyPaste(mgRange, currentRow, colIndex, true);
                                }
                                tempSheet.SetCellValue(currentRow, colIndex, Res.Get("Common_Label_MG"));
                            }
                            else
                            {
                                if (item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                {
                                    if (subject.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                        tempSheet.SetCellValue(currentRow, colIndex, item.Judgement);
                                    else
                                        tempSheet.SetCellValue(currentRow, colIndex, item.Mark);
                                }
                                else
                                {
                                    bool showmark = true;
                                    //kiem tra hoc sinh co duoc hien thi khong
                                    if ((item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                                        || item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                        || item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF) && startSemester1 <= item.EndDate && endSemester1 >= item.EndDate)
                                    {
                                        showmark = false;
                                    }

                                    else if (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                                        || item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                        || (item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF) && startSemester2 <= item.EndDate && endSemester2 >= item.EndDate)
                                    {
                                        if (model.Semester == 1)
                                        {
                                            showmark = true;
                                        }
                                        else if (model.Semester == 2)
                                        {
                                            showmark = false;
                                        }

                                    }

                                    if (rowIndex % 5 == 0 || rowIndex == totalPupil)
                                    {
                                        tempSheet.CopyPaste(unlockRange5, currentRow, colIndex, true);
                                    }
                                    else
                                    {
                                        tempSheet.CopyPaste(unlockRange, currentRow, colIndex, true);
                                    }
                                    //neu cho phep hien thi diem
                                    if (showmark)
                                    {
                                        if (subject.IsCommenting == SMAS.Business.Common.SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                            tempSheet.SetCellValue(currentRow, colIndex, item.Judgement);
                                        else
                                            tempSheet.SetCellValue(currentRow, colIndex, item.Mark);
                                    }
                                }
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                }
                tempSheet.DeleteRow(START_ROW_INDEX);

                #endregion

                #region fill variable
                //${EducationLevel} - ${Semester} - {AcademicYear}				
                //${SupervisingDept} {SchoolName} ${ProvinceDate}
                SchoolProfile school = SchoolProfileBu.Find(glo.SchoolID.Value);
                Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                dicVarable["EducationLevel"] = eduLevel.Resolution;
                dicVarable["Semester"] = semester;
                dicVarable["AcademicYear"] = AcademicYearBu.Find(glo.AcademicYearID).Year + " - " + (AcademicYearBu.Find(glo.AcademicYearID).Year + 1).ToString();
                dicVarable["SchoolName"] = school.SchoolName;
                dicVarable["SupervisingDept"] = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, glo.AppliedLevel.Value);
                dicVarable["ProvinceDate"] = (school.District != null ? school.District.DistrictName : "") + ", " + string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
                dicVarable["MarkType"] = model.MarkType;
                tempSheet.FillVariableValue(dicVarable);

                styleSheet.Delete();
                dataSheet.Delete();
                #endregion

                #region return

                reportName = reportName.Replace("EducationLevel", eduLevel != null ? ReportUtils.StripVNSign(eduLevel.Resolution) : "All").Replace("Semester", semester);
                reportName = ReportUtils.RemoveSpecialCharacters(reportName) + "." + reportDefinition.OutputFormat;
                FileStreamResult result = new FileStreamResult(oBook.ToStream(), "application/octet-stream");
                result.FileDownloadName = reportName;
                return result;
                #endregion
            }
            return null;
        }

        private List<PupilForImportBO> SaveData(SearchViewModel model, List<ImportSemesterTestMarkRow> listPupil)
        {
            GlobalInfo glo = new GlobalInfo();
            //Lay cac hoc sinh ...
            List<ImportSemesterTestMarkRow> listValidPupil = listPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            List<PupilForImportBO> listPupilForImport = new List<PupilForImportBO>();

            AcademicYear objAcademicYear = AcademicYearBu.Find(glo.AcademicYearID.Value);
            List<PupilForImportBO> listPupilForImportDelete = new List<PupilForImportBO>();

            Dictionary<string, object> dicLockMark = new Dictionary<string, object>();
            dicLockMark["AcademicYearID"] = glo.AcademicYearID.Value;
            dicLockMark["EducationLevelID"] = model.EducationLevelID;
            dicLockMark["Semester"] = model.Semester;
            if (model.ClassID.HasValue && model.ClassID.Value > 0) dicLockMark["ClassID"] = model.ClassID.Value;
            List<LockedMarkDetail> listLockedMarkDetailCheck = LockedMarkDetailBu.SearchBySchool(glo.SchoolID.Value, dicLockMark).ToList();
            List<ClassSubject> listClassSubject = ClassSubjectBu.SearchBySchool(glo.SchoolID.Value, dicLockMark).ToList();

            foreach (ImportSemesterTestMarkRow pupil in listValidPupil)
            {
                foreach (var subject in pupil.ListSubject)
                {
                    if (subject.IsSubjectValid //Mon hoc hop le
                        && ((subject.IsCommenting && !string.IsNullOrEmpty(subject.Judgement))  //La mon nhan xet va co diem
                        || (!subject.IsCommenting && subject.MarkValue.HasValue)                //La mon tinh diem va co diem
                        ) && pupil.IsValid && (!string.IsNullOrEmpty(subject.Judgement) || subject.MarkValue.HasValue) && listLockedMarkDetailCheck.Any(o => o.SubjectID == subject.SubjectID && o.ClassID == pupil.ClassID) && listClassSubject.Any(o => o.ClassID == pupil.ClassID)) //Co nhap diem
                    {
                        PupilForImportBO pupilForImport = new PupilForImportBO();
                        pupilForImport.AcademicYearID = glo.AcademicYearID.Value;
                        pupilForImport.ClassID = pupil.ClassID;
                        pupilForImport.ClassName = pupil.ClassName;
                        pupilForImport.FullName = pupil.FullName;
                        pupilForImport.Judgement = subject.IsCommenting ? subject.Judgement : null;
                        pupilForImport.Mark = subject.IsCommenting ? null : RoundMark(subject.MarkValue);
                        pupilForImport.MarkedDate = DateTime.Now;
                        pupilForImport.MarkTypeID = pupil.MarkTypeID;
                        pupilForImport.OrderNumber = pupil.OrderNumber;
                        pupilForImport.PeriodID = null;
                        pupilForImport.PupilCode = pupil.PupilCode;
                        pupilForImport.PupilID = pupil.PupilID;
                        pupilForImport.SchoolID = glo.SchoolID.Value;
                        pupilForImport.Semester = model.Semester;
                        pupilForImport.SubjectID = subject.SubjectID;
                        pupilForImport.Title = pupil.Title;
                        pupilForImport.Year = objAcademicYear.Year;

                        listPupilForImport.Add(pupilForImport);
                    }
                    if (subject.IsSubjectValid //Mon hoc hop le
                        && pupil.IsClassValid && pupil.IsPupilValid && pupil.IsValid && (subject.Mark == "" || subject.Judgement == "") && subject.SubjectID != 0 && listLockedMarkDetailCheck.Any(o => o.SubjectID == subject.SubjectID && o.ClassID == pupil.ClassID) && listClassSubject.Any(o => o.ClassID == pupil.ClassID))
                    {
                        PupilForImportBO pupilDeleteForImport = new PupilForImportBO();
                        pupilDeleteForImport.AcademicYearID = glo.AcademicYearID.Value;
                        pupilDeleteForImport.ClassID = pupil.ClassID;
                        pupilDeleteForImport.ClassName = pupil.ClassName;
                        pupilDeleteForImport.FullName = pupil.FullName;
                        pupilDeleteForImport.Judgement = subject.IsCommenting ? subject.Judgement : null;
                        pupilDeleteForImport.Mark = subject.IsCommenting ? null : RoundMark(subject.MarkValue);
                        pupilDeleteForImport.MarkedDate = DateTime.Now;
                        pupilDeleteForImport.MarkTypeID = pupil.MarkTypeID;
                        pupilDeleteForImport.OrderNumber = pupil.OrderNumber;
                        pupilDeleteForImport.PeriodID = null;
                        pupilDeleteForImport.PupilCode = pupil.PupilCode;
                        pupilDeleteForImport.PupilID = pupil.PupilID;
                        pupilDeleteForImport.SchoolID = glo.SchoolID.Value;
                        pupilDeleteForImport.Semester = model.Semester;
                        pupilDeleteForImport.SubjectID = subject.SubjectID;
                        pupilDeleteForImport.Title = pupil.Title;
                        pupilDeleteForImport.Year = objAcademicYear.Year;

                        listPupilForImportDelete.Add(pupilDeleteForImport);
                    }
                }
            }

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            if (!isMovedHistory)
            {
                JudgeRecordBu.ImportMark(glo.UserAccountID, listPupilForImport, listPupilForImportDelete);
            }
            else
            {
                JudgeRecordBu.ImportMarkHistory(glo.UserAccountID, listPupilForImport, listPupilForImportDelete);
            }
            return listPupilForImport;
        }

        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Save(SearchViewModel model)
        {
            try
            {
                GlobalInfo glo = new GlobalInfo();
                List<ImportSemesterTestMarkRow> listPupil = (List<ImportSemesterTestMarkRow>)Session[ImportSemesterTestMarkContants.LIST_IMPORTED_PUPIL];
                if (listPupil != null)
                {
                    SaveData(model, listPupil);
                    return Json(new JsonMessage(Res.Get("ImportSemesterTestMark_Label_SaveSuccess")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("ImportSemesterTestMark_Label_SaveFailure"), JsonMessage.ERROR));
                }
            }
            finally
            {
                //Xoa session khi import thành cong
                Session.Remove(ImportSemesterTestMarkContants.LIST_IMPORTED_PUPIL);
            }

        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[ImportSemesterTestMarkContants.LIST_EDUCATION_LEVEL] = glo.EducationLevels.Select(u => new SelectListItem { Text = u.Resolution, Value = u.EducationLevelID.ToString(), Selected = false }).ToList();
            AcademicYear acaYear = AcademicYearBu.Find(glo.AcademicYearID.Value);
            int semester = acaYear.FirstSemesterStartDate < DateTime.Now && DateTime.Now < acaYear.FirstSemesterEndDate ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            ViewData[ImportSemesterTestMarkContants.LIST_SEMESTER] = CommonList.Semester().Select(u => new SelectListItem { Text = u.value, Value = u.key, Selected = semester.ToString() == u.key }).ToList();

            List<SelectListItem> listMarkType = new List<SelectListItem>();

            listMarkType.AddRange(MarkRecordBu.GetMarkTitleForSemester(glo.SchoolID.Value, glo.AcademicYearID.Value, 0)
                                                .Split(',')
                                                .Where(u => !string.IsNullOrEmpty(u))
                                                .Select(u => new SelectListItem { Text = u == "HK" ? "KTHK" : u, Value = u, Selected = false }));
            ViewData[ImportSemesterTestMarkContants.LIST_MARK_TITLE] = listMarkType;
        }

        private string GetCellString(IVTWorksheet sheet, int row, int col)
        {
            object o = sheet.GetCellValue(row, col);
            if (o != null)
            {
                return o.ToString().Trim();
            }
            return string.Empty;
        }

        private decimal? GetCellDecimal(IVTWorksheet sheet, int row, int col)
        {
            object o = sheet.GetCellValue(row, col);
            if (o != null && o is double)
            {
                try
                {
                    return Convert.ToDecimal((double)o);
                }
                catch { }
            }
            return null;
        }

        public dynamic GetMarkType(string marktype)
        {
            return new { x = 1, y = 2 };
        }

        public decimal? RoundMark(decimal? mark)
        {
            if (!mark.HasValue)
            {
                return null;
            }
            return Math.Round(mark.Value, 1, MidpointRounding.AwayFromZero);
        }
    }
}
