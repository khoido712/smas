﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportSemesterTestMarkArea
{
    public class ImportSemesterTestMarkContants
    {
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string LIST_SEMESTER = "LIST_SEMESTER";
        public const string LIST_SUBJECT_CAT = "LIST_SUBJECT_CAT";
        public const string SEMESTER_END_DATE = "SEMESTER_END_DATE";
        public const string LIST_IMPORTED_PUPIL = "LIST_IMPORTED_PUPIL";
        public const string ENABLE_BUTTON = "ENABLE_BUTTON";
        public const string LIST_MARK_TITLE = "LIST_MARK_TITLE";
    }
}