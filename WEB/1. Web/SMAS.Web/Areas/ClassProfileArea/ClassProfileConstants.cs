﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ClassProfileArea
{
    public class ClassProfileConstants
    {
        public const string LS_CLASS_PROFILE = "ListClassProfiles";
        public const string LS_EDUCATION_LEVEL = "ListClassProfilesLevel";
        public const string LS_CLASS_PROPERTY_CAT = "ListClassPropertyCat";
        public const string LS_CLASS_PROFILE_SEPARATE = "ListClassProfilesSeparate";
        public const string LS_CLASS_PROFILE_SEPARATE_KEY = "ListClassProfilesSeparateKey";
        public const string LS_CLASS_PROFILE_SEPARATE_GROUP = "ListClassProfilesSeparateGroup";
        //public const string LS_CLASS_PROPERTY_CAT_EDIT = "ListClassPropertyCatEdit";
        //public const string LS_CLASS_PROFILE_SEPARATE_EDIT = "ListClassProfilesSeparateEdit";
        //public const string LS_CLASS_PROFILE_SEPARATE_GROUP_EDIT = "ListClassProfilesSeparateGroupEdit";
        public const string LS_CLASS_PROFILE_VOCATIONAL = "ListClassProfilesVocational";
        public const string LS_CLASS_PROFILE_FOREIGNLANG = "ListClassProfilesLanguage";
        public const string LS_CLASS_PROFILE_SUBSCHOOL = "ListClassProfilesSubschool";
        public const string LS_CLASS_PROFILE_SUBCOMMITEE = "ListClassProfilesSubcommitee";
        public const string LS_CLASS_SCHOOL_FACULTY = "ListClassSchoolFaculty";
        public const string LS_CLASS_HEADTEACHER = "ListClassHeadTeacher";
        public const string NBR_CLASS_PROFILE = "NumberRowClassProfiles";
        public const string ACADEMICYEAR = "ACADEMICYEAR";
        public const string PARENTFROM = "ParentFrom";
        public const int CLASS_APPRENTICESHIP = 7;
        public const int CLASS_APPRENTICESHIP1 = 7;
        public const int CLASS_APPRENTICESHIP2 = 19;
        public const int CLASS_APPRENTICESHIP3 = 31;
        public const string CLASS_PROPERTICAT_APPRENTICESHIP = "ClassProperticatApprenticeship";
        public const string APPRENTICESHIP = "LOPCOHOCSINHHOCNGHE";
        public const int MORNING = 1;
        public const int AFTERNOON = 2;
        public const int NIGHT = 3;
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";

        // QuangNN2 - Bổ sung điều kiện xét loại hình đào tạo của trường là GCTX
        public const int TRAINING_TYPE_GDTX = 3;
        public const string STRTRAINING_TYPE_GDTX = "STRTRAINING_TYPE_GDTX";

        public const string HEAD_TEACHER_NAME = "HeadTecherName";

        //Viethd4: Bien kiem tra truong co ap dung mo hinh truong hoc moi
        public const string IS_NEW_MODEL_SCHOOL = "IsNewModelSchool";

        public const string YEAR_NAME = "YEAR_NAME";
        public const string LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_LEFT = "LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_LEFT";
        public const string LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_RIGHT = "LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_RIGHT";
        public const string LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_LEFT = "LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_LEFT";
        public const string LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_RIGHT = "LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_RIGHT";
        public const string LS_CLASS_PROPERTY_CAT_OTHER_LEFT = "LS_CLASS_PROPERTY_CAT_OTHER_LEFT";
        public const string LS_CLASS_PROPERTY_CAT_OTHER_RIGHT = "LS_CLASS_PROPERTY_CAT_OTHER_RIGHT";
        public const string LS_EDUCATION_CLASS_BY_SCHOOL = "LS_EDUCATION_CLASS_BY_SCHOOL";
        public const string IS_MULTIPLE_LEVEL = "IS_MULTIPLE_LEVEL";

        public const string LS_EDUCATION_CLASS_TYPE = "LS_EDUCATION_CLASS_TYPE";
        public const string LS_EDUCATION_TRAINING_TYPE = "LS_EDUCATION_TRAINING_TYPE";
        public const string LS_EDUCATION_TRAINING_PROGRAM = "LS_EDUCATION_TRAINING_PROGRAM";
        public const string LS_EDUCATION_CLASS_SPECIALTY_CAT = "LS_EDUCATION_CLASS_SPECIALTY_CAT";
        public const string IS_COMBINE_CLASS = "IS_COMBINE_CLASS";
    }
}