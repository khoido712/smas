using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.ClassProfileArea.Models
{
    public class ClassProfileForm
    {
        public int? ClassProfileID { get; set; }
        public int? AcademicYearID { get; set; }
        public string CurAcademicYearName { get; set; }
        public int? SchoolID { get; set; }

        [ResourceDisplayName("ClassProfile_Control_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int EducationLevelID { get; set; }
        public string EduLevelDisplayName { get; set; }

        [ResourceDisplayName("ClassProfile_Control_DisplayName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string DisplayName { get; set; }
        [ResourceDisplayName("ClassProfile_Control_Description")]
        public string Description { get; set; }
        


        [ResourceDisplayName("ClassProfile_Control_Vocational")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? ApprenticeshipGroupID { get; set; }

        [ResourceDisplayName("ClassProfile_Control_HeaderMaster")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? HeadTeacherID { get; set; }
        [ResourceDisplayName("ClassProfile_Control_HeaderMaster")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string HeadTeacherName { get; set; }        
        public int? SubjectForeignLang1ID { get; set; }
        public int? SubjectForeignLang2ID { get; set; }
        public int? SchoolSubsidiaryID { get; set; }
        public int? ClassType { get; set; }
        public int? TrainingType { get; set; }
        public int? TrainingProgramID { get; set; }

         [ResourceDisplayName("String1ClassProfile_Control_Subcommitee")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? SubCommitteeID { get; set; } 
       
        public string SubCommiteeName { get; set; }

        [ResourceDisplayName("ClassProfile_Control_CombileCode")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string CombinedClassCode { get; set; }
        public bool IsCombinedClass { get; set; }
        public bool IsITClass { get; set; }
        
        // Mang luu ID cua checkbox truyen len
        public List<int> ArrClassPropertyCatID { get; set; }
        public List<int> ArrClassPropertyCatGroupIDSectionLeft { get; set; }
        public List<int> ArrClassPropertyCatGroupIDSectionRight { get; set; }
        public List<int> ArrClassPropertyCatGroupIDCombileLeft { get; set; }
        public List<int> ArrClassPropertyCatGroupIDCombileRight { get; set; }
        public List<int> ArrClassPropertyCatGroupID { get; set; }
        public List<int> ArrClassSeperateID { get; set; }
        public List<int> ArrSeperateKeyID { get; set; }

        // So thu tu
        [ResourceDisplayName("ClassProfile_Control_OrderNumber")]
        public int? OrderNumber { get; set; }

        [ResourceDisplayName("ClassProfile_Control_VemisCode")]
        public string VemisCode { get; set; }

        [ResourceDisplayName("ClassProfile_Vnen_Class")]
        public bool IsVnenClass { get; set; }
        [ResourceDisplayName("Is_specialty_classes")]
        public bool IsSpecializedClass { get; set; }
        [ResourceDisplayName("Specialists_classes")]
        public Nullable<int> ClassSpecialtyCatID { get; set; }
    }
}
