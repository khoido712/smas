﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassProfileArea
{
    public class ClassProfileAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassProfileArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassProfileArea_default",
                "ClassProfileArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
