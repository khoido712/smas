﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Web.Areas.ClassProfileArea.Models;
using SMAS.Web.Areas.EmployeeArea.Models;
using System.Transactions;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ImportClassArea;
using System.Text;
using SMAS.Web.Areas.ImportClassArea.Models;
using SMAS.VTUtils.Excel.Export;
using System.Web;
using System.IO;
using SMAS.VTUtils.Pdf;
using Ionic.Zip;
using System.Web.UI.WebControls;

namespace SMAS.Web.Areas.ClassProfileArea.Controllers
{
    public class ClassProfileController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassPropertyCatBusiness ClassPropertyCatBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IPropertyOfClassBusiness PropertyOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IApprenticeshipGroupBusiness ApprenticeshipGroupBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilAbsenceBusiness PupilAbsenceBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly ITrainingProgramBusiness TrainingProgramBusiness;
        private readonly IClassSpecialtyCatBusiness ClassSpecialtyCatBusiness;
        private const int MAX_CLASS_ORDER = 999;
        private int ViewBooklet
        {
            get
            {
                return SessionObject.Get<int>("TranscriptsReportController");
            }
            set
            {
                SessionObject.Set<int>("TranscriptsReportController", value);
            }
        }

        public ClassProfileController(IClassProfileBusiness classprofileBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassPropertyCatBusiness ClassPropertyCatBusiness, ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness,
            IAcademicYearBusiness AcademicYearBusiness, ISubCommitteeBusiness SubCommitteeBusiness,
            IPropertyOfClassBusiness PropertyOfClassBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness,
            ISubjectCatBusiness SubjectCatBusiness
            , IApprenticeshipGroupBusiness ApprenticeshipGroupBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilAbsenceBusiness PupilAbsenceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IClassAssigmentBusiness ClassAssigmentBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            ITrainingProgramBusiness TrainingProgramBusiness,
            IClassSpecialtyCatBusiness ClassSpecialtyCatBusiness)
        {
            this.ApprenticeshipGroupBusiness = ApprenticeshipGroupBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassPropertyCatBusiness = ClassPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.PropertyOfClassBusiness = PropertyOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.TrainingProgramBusiness = TrainingProgramBusiness;
            this.ClassSpecialtyCatBusiness = ClassSpecialtyCatBusiness;
        }

        #region Action in page
        /// <summary>
        /// Get Class profile by education level
        /// </summary>
        /// <param name="educationLevelId"></param>
        /// <returns></returns>

        public PartialViewResult GetClassProfileByEduLevel(int? EducationLevelId)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[ClassProfileConstants.IS_CURRENT_YEAR] = global.IsCurrentYear;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE] = this._SearchClassProfile(EducationLevelId);
            // Lay du lieu khoi hoc

            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[ClassProfileConstants.LS_EDUCATION_LEVEL] = lsEducationLevel;
            }
            else
            {
                ViewData[ClassProfileConstants.LS_EDUCATION_LEVEL] = new List<EducationLevel>();
            }

            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            var gradeSchool = school != null ? school.EducationGrade : 0;
            ViewData[ClassProfileConstants.LS_EDUCATION_CLASS_BY_SCHOOL] = CommonList.EducationLevelByGradeSchool(gradeSchool);
            if (gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN)
            {
                ViewData[ClassProfileConstants.IS_MULTIPLE_LEVEL] = true;
            }
            if (school != null && school.TrainingTypeID.HasValue && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                ViewData[ClassProfileConstants.STRTRAINING_TYPE_GDTX] = ClassProfileConstants.TRAINING_TYPE_GDTX;
            return PartialView("_GridClassProfile");
        }

        /// <summary>
        /// Tim kiem giao vien chu nhiem cho lop hoc
        /// </summary>
        /// <param name="EmployeeObject"></param>
        /// <returns></returns>

        public PartialViewResult SearchHeadTeacher(EmployeeObject EmployeeObject, int page = 1, string orderBy = "")
        {

            if (!EmployeeObject.SchoolFacultyID.HasValue)
            {
                GlobalInfo globalInfo = GlobalInfo.getInstance();
                bool isMamnon = globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                    || globalInfo.AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN;
                throw new SMAS.Business.Common.BusinessException(isMamnon ? "ClassProfile_Label_NotChooseFaculty_ForMN" : "ClassProfile_Label_NotChooseFaculty");
            }
            List<EmployeeObject> paging = this._SearchTeacher(EmployeeObject);
            ViewData[ClassProfileConstants.LS_CLASS_HEADTEACHER] = paging;
            return PartialView("_GridHeadTeacher");
        }

        //
        // GET: /ClassProfile/
        public ViewResult Index()
        {
            SetViewDataPermission("ClassProfile", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo glo = new GlobalInfo();
            ViewData[ClassProfileConstants.IS_CURRENT_YEAR] = glo.IsCurrentYear;
            // Thuc hien tim kiem tat ca lop hoc
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE] = this._SearchClassProfile(null);
            // Khoi tao dialog them moi va sua
            InitData(true);

            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("ClassProfile", glo.UserAccountID, glo.IsAdmin);

            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            dicSeachParam["AppliedLevel"] = glo.AppliedLevel.Value;
            List<ClassPropertyCat> ListClassPropertyCatAll = this.ClassPropertyCatBusiness.Search(dicSeachParam).ToList();

            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ViewData[ClassProfileConstants.CLASS_PROPERTICAT_APPRENTICESHIP] = null;
            }
            else
            {
                var objPropApprenticeShip = ListClassPropertyCatAll.Where(p => p.AppliedLevel == _globalInfo.AppliedLevel
               && Utils.Utils.StripVNSignAndSpace(p.Resolution.ToUpper()).Contains(ClassProfileConstants.APPRENTICESHIP)).FirstOrDefault();
                if (objPropApprenticeShip != null)
                    ViewData[ClassProfileConstants.CLASS_PROPERTICAT_APPRENTICESHIP] = objPropApprenticeShip.ClassPropertyCatID;
            }
            int gradeSchool = 0;
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            bool isNewModelSchool = false;
            if (sp != null)
            {
                gradeSchool = sp.EducationGrade;
                if (sp.IsNewSchoolModel == true)
                {
                    isNewModelSchool = true;
                }
            }

            //Lay ten nam hoc

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            ViewData[ClassProfileConstants.YEAR_NAME] = objAcademicYear.DisplayTitle;
            ViewData[ClassProfileConstants.IS_NEW_MODEL_SCHOOL] = isNewModelSchool;
            ViewData[ClassProfileConstants.LS_EDUCATION_CLASS_BY_SCHOOL] = CommonList.EducationLevelByGradeSchool(gradeSchool);
            if (gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN)
            {
                ViewData[ClassProfileConstants.IS_MULTIPLE_LEVEL] = true;
            }
            if (sp != null && sp.TrainingTypeID.HasValue && sp.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                ViewData[ClassProfileConstants.STRTRAINING_TYPE_GDTX] = ClassProfileConstants.TRAINING_TYPE_GDTX;
            return View();
        }

        public PartialViewResult CallPartialClassProfile()
        {
            // Thuc hien tim kiem tat ca lop hoc
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE] = this._SearchClassProfile(null);
            // Khoi tao dialog them moi va sua
            InitData(true);
            return PartialView("_GridClassProfile");
        }

        //
        // GET: /ClassProfile/Details/5
        public ViewResult Details(int id)
        {
            ClassProfile classprofile = this.ClassProfileBusiness.Find(id);
            return View(classprofile);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_ADD)]
        public JsonResult Create(ClassProfileForm frm)
        {
            int tempMaxOrder = 0;
            string DisplayName = string.Empty;
            if (GetMenupermission("ClassProfile", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int academicYearId = _globalInfo.AcademicYearID.Value;
            int schoolId = _globalInfo.SchoolID.Value;
            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.AllNoTracking
                                                    .Where(cp => cp.AcademicYearID == academicYearId 
                                                        && cp.SchoolID == schoolId
                                                        && cp.EducationLevel.Grade == _globalInfo.AppliedLevel.Value
                                                        && cp.IsActive == true).ToList();
            // Kiem tra ten lop da ton tai hay chua
            if (string.IsNullOrWhiteSpace(frm.DisplayName))
            {
                throw new BusinessException("Thầy/cô chưa khai báo tên lớp!");
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            if (sp != null && sp.TrainingTypeID.HasValue && sp.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
            {
                if (frm.ClassType == null || frm.ClassType == 0)
                {
                    throw new BusinessException("Thầy/cô chưa chọn Loại lớp!");
                }
            }
            frm.DisplayName = frm.DisplayName.Trim();
            //Anhnph1_20150701_ Bổ sung kiểm tra luồng insert được nhiều lớp học cách nhau bởi dấu ","
            List<string> lstDisplayName = frm.DisplayName.Split(',').ToList();

            bool flag = true;
            bool checkSameName = true;
            string flagCheckSameName = lstDisplayName[0].Trim();
            ClassProfile classWithSameName = null;
            for (int i = 0; i < lstDisplayName.Count; i++)
            {
                DisplayName = lstDisplayName[i].Trim();
                classWithSameName = lstClassProfile.FirstOrDefault(cp => cp.AcademicYearID == academicYearId
                                            && cp.DisplayName.ToLower() == DisplayName.ToLower());
                if (classWithSameName != null)
                    flag = false;
                if (i != 0 && DisplayName.ToUpper().Equals(flagCheckSameName.ToUpper()))
                    checkSameName = false;
            }
            if (!checkSameName)
            {
                throw new BusinessException("Không thể thêm mới 2 lớp trùng nhau");
            }
            if (!flag)
            {
                List<object> listParam = new List<object>();
                listParam.Add("ClassProfile_Control_DisplayNameDuplicated");
                throw new BusinessException("ClassProfile_Validate_Duplicate", listParam);
            }


            //Nếu chọn ngoại ngữ 2 thì phải chọn ngoại ngữ 1
            if (frm.SubjectForeignLang2ID != null)
            {
                if (frm.SubjectForeignLang1ID == null)
                {
                    throw new BusinessException("Validate_Require_Language1");
                }
                else
                {
                    if (frm.SubjectForeignLang1ID == frm.SubjectForeignLang2ID)
                    {
                        throw new BusinessException("Validate_Duplicate_Language");
                    }
                }
            }
            // Kiểm tra ràng buộc EducationLevelID
            if (frm.EducationLevelID <= 0)
            {
                throw new BusinessException("PupilPraise_Label_EducationLevelRepuired");
            }

            //anhnph1_20150701_Xu ly them nhieu lop         
            ClassProfile obj = null;
            List<string> listDisplayName = frm.DisplayName.Split(',').Select(s => s.Trim()).ToList();
            listDisplayName = listDisplayName.OrderBy(p => p).ToList();
            listDisplayName = listDisplayName.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
            GlobalInfo globalInfo = new GlobalInfo();

            bool check = false;
            for (int i = 0; i < listDisplayName.Count; i++)
            {
                if (listDisplayName[i].Trim() != "")
                {
                    obj = new ClassProfile();
                    obj.DisplayName = listDisplayName[i].Trim();
                    obj.Description = frm.Description;
                    obj.EducationLevelID = frm.EducationLevelID;

                    int? academicYearID = globalInfo.AcademicYearID;
                    int? schoolID = globalInfo.SchoolID;
                    if (schoolID.HasValue)
                    {
                        obj.SchoolID = schoolID.Value;
                    }
                    if (academicYearID.HasValue)
                    {
                        obj.AcademicYearID = academicYearID.Value;
                    }
                    // Lay thong tin mon hoc va thuoc tinh cua lop hoc
                    obj.FirstForeignLanguageID = frm.SubjectForeignLang1ID;
                    obj.SecondForeignLanguageID = frm.SubjectForeignLang2ID;

                    if (frm.ApprenticeshipGroupID != null && frm.ApprenticeshipGroupID != 0)
                    {
                        obj.ApprenticeshipGroupID = frm.ApprenticeshipGroupID;
                    }

                    obj.HeadTeacherID = frm.HeadTeacherID;

                    if (frm.IsCombinedClass)
                    {
                        if (string.IsNullOrEmpty(frm.CombinedClassCode))
                        {
                            throw new BusinessException("Thầy/cô cần phải nhập mã lớp ghép");
                        }
                        obj.CombinedClassCode = frm.CombinedClassCode;

                        if(!check)
                        {
                            var resultCombined = lstClassProfile.Where(x => x.IsCombinedClass == true
                                                           && x.CombinedClassCode.ToUpper().Equals(frm.CombinedClassCode.ToUpper())).ToList();
                            if (resultCombined.Count > 0)
                            {
                                foreach (var item in resultCombined)
                                {
                                    if (item.HeadTeacherID.HasValue && frm.HeadTeacherID.HasValue)
                                    {
                                        if (item.HeadTeacherID.Value != frm.HeadTeacherID.Value)
                                        {
                                            string message = string.Format("Giáo viên chủ nhiệm của lớp {0} khác giáo viên chủ nhiệm của lớp {1}.", item.DisplayName, listDisplayName[i].Trim());
                                            message += " Các lớp ghép phải có cùng giáo viên chủ nhiệm";
                                            throw new BusinessException(message);
                                        }
                                    }
                                    else if (item.HeadTeacherID.HasValue && !frm.HeadTeacherID.HasValue)
                                    {
                                        obj.HeadTeacherID = item.HeadTeacherID;
                                    }
                                    else if (!item.HeadTeacherID.HasValue && frm.HeadTeacherID.HasValue)
                                    {
                                        item.HeadTeacherID = frm.HeadTeacherID;
                                        this.ClassProfileBusiness.Update(item);
                                    }
                                }
                            }
                            check = true;
                        }
                       
                    }
                    if (frm.IsSpecializedClass)
                    {
                        if (frm.ClassSpecialtyCatID > 0)
                        {
                            obj.ClassSpecialtyCatID = frm.ClassSpecialtyCatID;
                            obj.IsSpecializedClass = frm.IsSpecializedClass;
                        }
                        else
                        {
                            throw new BusinessException("Thầy/cô cần phải chọn hệ chuyên.");
                        }
                    }
                    obj.SchoolSubsidiaryID = frm.SchoolSubsidiaryID;
                    obj.SubCommitteeID = frm.SubCommitteeID;
                    if (!string.IsNullOrEmpty(frm.CombinedClassCode))
                    {
                        obj.CombinedClassCode = frm.CombinedClassCode;
                    }
                    obj.IsCombinedClass = frm.IsCombinedClass;
                    obj.IsITClass = frm.IsITClass;
                    obj.VemisCode = frm.VemisCode;
                    obj.ClassType = frm.ClassType;
                    obj.TrainingType = frm.TrainingType;
                    obj.TrainingProgramID = frm.TrainingProgramID;

                    bool isNewModelSchool = false;
                    if (sp != null)
                    {
                        if (sp.IsNewSchoolModel == true)
                        {
                            isNewModelSchool = true;
                        }
                    }

                    if (isNewModelSchool)
                    {
                        obj.IsVnenClass = frm.IsVnenClass;
                    }
                    else
                    {
                        obj.IsVnenClass = false;
                    }

                    // Nhom checkbox
                    List<PropertyOfClass> listPropertyOfClass = new List<PropertyOfClass>();
                    if (frm.ArrClassPropertyCatID != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatID)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass
                                {
                                    IsActive = true,
                                    ClassPropertyCatID = (int)propertyID
                                };
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }

                    if (frm.ArrClassPropertyCatGroupIDSectionLeft != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatGroupIDSectionLeft)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass
                                {
                                    IsActive = true,
                                    ClassPropertyCatID = (int)propertyID
                                };
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }
                    if (frm.ArrClassPropertyCatGroupIDSectionRight != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatGroupIDSectionRight)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass
                                {
                                    IsActive = true,
                                    ClassPropertyCatID = (int)propertyID
                                };
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }
                    if (frm.ArrClassPropertyCatGroupIDCombileLeft != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatGroupIDCombileLeft)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass
                                {
                                    IsActive = true,
                                    ClassPropertyCatID = (int)propertyID
                                };
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }
                    if (frm.ArrClassPropertyCatGroupIDCombileRight != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatGroupIDCombileRight)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass
                                {
                                    IsActive = true,
                                    ClassPropertyCatID = (int)propertyID
                                };
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }
                    // Nhom radio box
                    if (frm.ArrClassPropertyCatGroupID != null)
                    {
                        foreach (int propertyID in frm.ArrClassPropertyCatGroupID)
                        {
                            if (propertyID != 0)
                            {
                                PropertyOfClass propertyOfClass = new PropertyOfClass();
                                propertyOfClass.ClassPropertyCatID = (int)propertyID;
                                propertyOfClass.IsActive = true;
                                listPropertyOfClass.Add(propertyOfClass);
                            }
                        }
                    }
                    if (frm.ArrClassSeperateID != null && frm.ArrClassSeperateID.Count > 0)
                    {
                        obj.Section = this.GetSectionByListID(frm.ArrClassSeperateID);
                    }
                    else
                    {
                        throw new BusinessException("Validate_ArrClassSeperateID");
                    }

                    if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                    {
                        if (frm.ArrSeperateKeyID != null && frm.ArrSeperateKeyID.Count > 0)
                        {
                            obj.SeperateKey = this.GetSectionByListID(frm.ArrSeperateKeyID);
                        }
                        else
                        {
                            throw new BusinessException("Validate_ArrClassSeperateKeyID");
                        }
                    }

                    if (listPropertyOfClass.Count > 0)
                    {
                        obj.PropertyOfClasses = listPropertyOfClass;
                    }

                    // Congnv: Tính số thứ tự cho lớp                 
                    tempMaxOrder = GetMaxOrderNumber(obj.EducationLevelID, obj.AcademicYearID, obj.SchoolID) + 1;
                    if (i == 0)
                    {
                        obj.OrderNumber = tempMaxOrder;
                    }
                    else if (listDisplayName.Count > 1 && i != 0)
                    {
                        obj.OrderNumber = tempMaxOrder + i;
                    }
                    obj.IsActive = true;
                    this.ClassProfileBusiness.Insert(obj);
                }
            }

            this.ClassProfileBusiness.Save();
            ModelState.Clear();

            //PrepareCreate();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage"))); ;
        }

        public int GetMaxOrderNumber(int? educationLevelID, int? academicYearID, int? schoolID)
        {
            IDictionary<string, object> dic4SearchOrder = new Dictionary<string, object>();
            int? educationLevel4Order = educationLevelID;
            int? academicYearID4Order = academicYearID;
            int? schoolID4Order = schoolID;
            if (educationLevel4Order.HasValue)
            {
                dic4SearchOrder.Add("EducationLevelID", educationLevel4Order);
            }
            if (academicYearID4Order.HasValue)
            {
                dic4SearchOrder.Add("AcademicYearID", academicYearID4Order.Value);
            }
            if (schoolID4Order.HasValue)
            {
                dic4SearchOrder.Add("SchoolID", schoolID4Order.Value);
            }
            IQueryable<ClassProfile> lst = ClassProfileBusiness.Search(dic4SearchOrder).Where(o => o.OrderNumber != null);
            int MaxOrder = 0;
            if (lst.Count() > 0)
            {
                MaxOrder = lst.Max(p => p.OrderNumber).Value;
            }
            return MaxOrder;
        }
        //
        // GET: /ClassProfile/Edit/5
        public ActionResult Edit(int? ClassID)
        {
            CheckPermissionForAction(ClassID.Value, "ClassProfile");
            if (!ClassID.HasValue)
            {
                throw new BusinessException("Common_Error_InternalError");
            }
            // Chuan bi du lieu Form
            InitData(false);

            // QuangNN2 - Bổ sung điều kiện loại hình đào tạo của trường là GDTX
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

            ClassProfile classprofile = this.ClassProfileBusiness.Find(ClassID);
            ClassProfileForm frm = new ClassProfileForm();
            Utils.Utils.BindTo(classprofile, frm);
            if (classprofile.Employee != null)
            {
                frm.HeadTeacherName = classprofile.Employee.FullName;
                ViewData[ClassProfileConstants.HEAD_TEACHER_NAME] = classprofile.Employee.FullName;
            }
            // Lay danh sach mon hoc ngoai ngu cua lop nay
            //List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID.Value).ToList();
            //if (listClassSubject != null)
            //{
            //    foreach (ClassSubject ClassSubject in listClassSubject)
            //    {
            //        SubjectCat SubjectCat = ClassSubject.SubjectCat;
            //        if (SubjectCat != null)
            //        {
            //            int SubjectCatID = SubjectCat.SubjectCatID;
            //            if (SubjectCat.IsForeignLanguage)
            //            {
            //                // Neu la mon ngoai ngu 2
            //                if (ClassSubject.IsSecondForeignLanguage.HasValue && ClassSubject.IsSecondForeignLanguage.Value)
            //                {
            //                    frm.SubjectForeignLang2ID = SubjectCatID;
            //                }
            //                else
            //                {
            //                    // Neu la ngoai ngu 1
            //                    frm.SubjectForeignLang1ID = SubjectCatID;
            //                }
            //            }
            //        }
            //    }
            //}

            frm.SubjectForeignLang1ID = classprofile.FirstForeignLanguageID;
            frm.SubjectForeignLang2ID = classprofile.SecondForeignLanguageID;

            // Buoi hoc Sang - Chieu - Toi
            List<ViettelCheckboxList> listPropertyCatChk = new List<ViettelCheckboxList>();
            List<ViettelCheckboxList> listSeperateKey = new List<ViettelCheckboxList>();
            int sectionKey = classprofile.SeperateKey.HasValue ? classprofile.SeperateKey.Value : 0;
            int section = classprofile.Section.HasValue ? classprofile.Section.Value : 0;
            listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Morning"),
                ClassProfileConstants.MORNING, this.CheckSection(GlobalConstants.SECTION_MONING, section), false));
            listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Afternoon"),
                ClassProfileConstants.AFTERNOON, this.CheckSection(GlobalConstants.SECTION_AFTERNOON, section), false));
            listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Night"),
                ClassProfileConstants.NIGHT, this.CheckSection(GlobalConstants.SECTION_EVENING, section), false));

            listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Morning"),
                ClassProfileConstants.MORNING, this.CheckSection(GlobalConstants.SECTION_MONING, sectionKey), false));
            listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Afternoon"),
                ClassProfileConstants.AFTERNOON, this.CheckSection(GlobalConstants.SECTION_AFTERNOON, sectionKey), false));
            listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Night"),
                ClassProfileConstants.NIGHT, this.CheckSection(GlobalConstants.SECTION_EVENING, sectionKey), false));

            // Tinh chat lop hoc
            List<ViettelCheckboxList> listClassPropertyCat = new List<ViettelCheckboxList>();
            var listClassPropertyCatOtherLeft = new List<ViettelCheckboxList>();
            var listClassPropertyCatOtherRight = new List<ViettelCheckboxList>();
            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            dicSeachParam["AppliedLevel"] = new GlobalInfo().AppliedLevel.Value;

            List<PropertyOfClass> ListPropertyOfClass = this.PropertyOfClassBusiness.SearchByClass(ClassID.Value).ToList();
            List<ClassPropertyCat> ListClassPropertyCatAll = this.ClassPropertyCatBusiness.Search(dicSeachParam).ToList();
            Dictionary<int, List<ViettelCheckboxList>> dicPropertyGroup = new Dictionary<int, List<ViettelCheckboxList>>();
            var dicPropertyGroupSectionLeft = new Dictionary<int, List<ViettelCheckboxList>>();
            var dicPropertyGroupSectionRight = new Dictionary<int, List<ViettelCheckboxList>>();
            var dicPropertyGroupCombileLeft = new Dictionary<int, List<ViettelCheckboxList>>();
            var dicPropertyGroupCombileRight = new Dictionary<int, List<ViettelCheckboxList>>();
            ListClassPropertyCatAll = ListClassPropertyCatAll.OrderBy(x => x.Resolution).ThenBy(x => x.ClassPropertyCatID).ToList();
            foreach (ClassPropertyCat cpc in ListClassPropertyCatAll)
            {
                ViettelCheckboxList item = new ViettelCheckboxList(cpc.Resolution, cpc.ClassPropertyCatID, false, false);
                if (ListPropertyOfClass.FirstOrDefault(o => o.ClassPropertyCatID == cpc.ClassPropertyCatID) != null)
                {
                    item.cchecked = true;
                }
                if (cpc.PropertyGroup.HasValue && cpc.PropertyGroup.Value > 0)
                {
                    // Danh sach hien thi danh radiobox   
                    // PropertyGroup == 1 => Div Section left
                    // PropertyGroup == 2 => Div Section right
                    // PropertyGroup == 3 => Div CombileClass left
                    // PropertyGroup == 4 => Div CombileClass right
                    int propertyGroup = cpc.PropertyGroup.Value;
                    if (dicPropertyGroupSectionLeft.ContainsKey(propertyGroup))
                    {
                        //dicPropertyGroup[propertyGroup].Add(item);
                        if (cpc.PropertyGroup.Value == 1)
                        {
                            dicPropertyGroupSectionLeft[propertyGroup].Add(item);
                            continue;
                        }
                    }
                    else
                    {
                        List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                        tempList.Add(item);
                        //dicPropertyGroup[propertyGroup] = tempList; 
                        if (cpc.PropertyGroup.Value == 1)
                        {
                            dicPropertyGroupSectionLeft[propertyGroup] = tempList;
                            continue;
                        }
                    }

                    if (dicPropertyGroupSectionRight.ContainsKey(propertyGroup))
                    {
                        //dicPropertyGroup[propertyGroup].Add(item);
                        if (cpc.PropertyGroup.Value == 2)
                        {
                            dicPropertyGroupSectionRight[propertyGroup].Add(item);
                            continue;
                        }
                    }
                    else
                    {
                        List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                        tempList.Add(item);
                        //dicPropertyGroup[propertyGroup] = tempList;
                        if (cpc.PropertyGroup.Value == 2)
                        {
                            dicPropertyGroupSectionRight[propertyGroup] = tempList;
                            continue;
                        }
                    }

                    if (dicPropertyGroupCombileLeft.ContainsKey(propertyGroup))
                    {
                        //dicPropertyGroup[propertyGroup].Add(item);
                        if (cpc.PropertyGroup.Value == 3)
                        {
                            dicPropertyGroupCombileLeft[propertyGroup].Add(item);
                            continue;
                        }
                    }
                    else
                    {
                        List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                        tempList.Add(item);
                        //dicPropertyGroup[propertyGroup] = tempList;
                        if (cpc.PropertyGroup.Value == 3)
                        {
                            dicPropertyGroupCombileLeft[propertyGroup] = tempList;
                            continue;
                        }
                    }


                    if (dicPropertyGroupCombileRight.ContainsKey(propertyGroup))
                    {
                        //dicPropertyGroup[propertyGroup].Add(item);
                        if (cpc.PropertyGroup.Value == 4)
                        {
                            dicPropertyGroupCombileRight[propertyGroup].Add(item);
                            continue;
                        }
                    }
                    else
                    {
                        List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                        tempList.Add(item);
                        //dicPropertyGroup[propertyGroup] = tempList;
                        if (cpc.PropertyGroup.Value == 4)
                        {
                            dicPropertyGroupCombileRight[propertyGroup] = tempList;
                            continue;
                        }
                    }
                }
                else
                {
                    // Danh sach hien thi danh checkbox
                    // QuangNN2 - Bổ sung điều kiện loại hình đào tạo của trường là GDTX
                    // TrainingTypeID == 1 => Div Other property left
                    // TrainingTypeID == 2 => Div Other property right
                    if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                    {
                        if (school.TrainingTypeID.HasValue && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                        {
                            if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == 2)
                            {
                                listClassPropertyCat.Add(item);
                                listClassPropertyCatOtherRight.Add(item);
                            }
                            else
                            {
                                listClassPropertyCat.Add(item);
                                listClassPropertyCatOtherLeft.Add(item);
                            }
                        }

                    }
                    else if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == 2)
                    {
                        listClassPropertyCat.Add(item);
                        listClassPropertyCatOtherRight.Add(item);
                    }
                    else
                    {
                        listClassPropertyCat.Add(item);
                        listClassPropertyCatOtherLeft.Add(item);
                    }
                }
            }
            ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT] = listClassPropertyCat;
            ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT_OTHER_LEFT] = listClassPropertyCatOtherLeft;
            ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT_OTHER_RIGHT] = listClassPropertyCatOtherRight;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE] = listPropertyCatChk;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_KEY] = listSeperateKey;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP] = dicPropertyGroup;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_LEFT] = dicPropertyGroupSectionLeft;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_RIGHT] = dicPropertyGroupSectionRight;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_LEFT] = dicPropertyGroupCombileLeft;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_RIGHT] = dicPropertyGroupCombileRight;
            ViewData[ClassProfileConstants.IS_COMBINE_CLASS] = classprofile.IsCombinedClass;

            if (GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_CRECHE
                || GlobalInfo.getInstance().AppliedLevel == SMAS.Web.Constants.GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ViewData[ClassProfileConstants.CLASS_PROPERTICAT_APPRENTICESHIP] = null;
            }
            else
            {
                var objPropCatAppreticeShip =ListClassPropertyCatAll.Where(p => p.AppliedLevel == _globalInfo.AppliedLevel && Utils.Utils.StripVNSignAndSpace(p.Resolution.ToUpper()).Contains(ClassProfileConstants.APPRENTICESHIP)).FirstOrDefault();
                if(objPropCatAppreticeShip != null)
                    ViewData[ClassProfileConstants.CLASS_PROPERTICAT_APPRENTICESHIP] = objPropCatAppreticeShip.ClassPropertyCatID;
            }


            /*DungVA - 01/07/2013 - Kiểm tra xem lớp đã khai báo môn học hoặc có học sinh hay chưa
              Nếu có rồi thì khi sửa bắt buộc phải disabled khối học*/
            Dictionary<string, object> dicSubjectClass = new Dictionary<string, object>();
            dicSubjectClass["SchoolID"] = school.SchoolProfileID;
            IQueryable<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchByClass(ClassID.Value, dicSubjectClass);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ClassID", ClassID.Value);
            IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
            if ((lstClassSubject != null && lstClassSubject.Count() != 0) || (lstPupilOfClass != null && lstPupilOfClass.Count() != 0))
            {
                ViewData["displayEducation"] = true;
            }
            else
            {
                ViewData["displayEducation"] = false;
            }

            //if (lstPupilOfClass != null && lstPupilOfClass.Count() != 0)
            //{
            //    ViewData["displayEducation"] = true;
            //}
            //else
            //{
            //    ViewData["displayEducation"] = false;
            //}
            Dictionary<string, object> dicForCheckbox = new Dictionary<string, object>();
            dicForCheckbox.Add("ClassID", ClassID.Value);
            // THêm academicyear
            // Truyền buổi sáng hoặc chiều hoặc tối dựa theo checkbox vào 
            // check cả 3 sau count từng thằng nếu thằng nào tồn thì disable.
            ViewData["displayChkMorning"] = "false";
            ViewData["displayChkAfternoon"] = "false";
            ViewData["displayChkEvening"] = "false";
            
            /*IQueryable<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dicForCheckbox);
            if (lstPupilAbsence != null && lstPupilAbsence.Count() != 0)
            {
                // Nếu danh sách học sinh nghỉ học lớn hơn 0
                // Thì kiểm tra theo sáng chiều tối
                int morning = lstPupilAbsence.Count(o => o.Section == 1);
                int afternoon = lstPupilAbsence.Count(o => o.Section == 2);
                int evening = lstPupilAbsence.Count(o => o.Section == 3);
                if (morning > 0 && this.CheckSection(GlobalConstants.SECTION_MONING, section))
                {
                    ViewData["displayChkMorning"] = "true";

                }
                if (afternoon > 0 && this.CheckSection(GlobalConstants.SECTION_AFTERNOON, section))
                {
                    ViewData["displayChkAfternoon"] = "true";
                }
                if (evening > 0 && this.CheckSection(GlobalConstants.SECTION_EVENING, section))
                {
                    ViewData["displayChkEvening"] = "true";
                }

            }*/

            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            bool isNewModelSchool = false;
            if (sp != null)
            {
                if (sp.IsNewSchoolModel == true)
                {
                    isNewModelSchool = true;
                }
            }
            ViewData[ClassProfileConstants.IS_NEW_MODEL_SCHOOL] = isNewModelSchool;
            /*End of DungVA - 01/07/2013*/

            return View(frm);
        }

        //
        // POST: /ClassProfile/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_UPDATE)]
        public JsonResult Edit(ClassProfileForm frm)
        {
            GlobalInfo globalInfo = GlobalInfo.getInstance();
            // Kiem tra quyen nguoi dung co duoc phep sua hay khong
            if (GetMenupermission("ClassProfile", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(frm.ClassProfileID.Value, "ClassProfile");
            // Find class profile
            ClassProfile classProfileEditing = this.ClassProfileBusiness.Find(frm.ClassProfileID);
            if (classProfileEditing == null)
            {
                throw new BusinessException(Utils.Res.Get("ErrorMsg_ClassNotFound"));
            }
            int ClassID = classProfileEditing.ClassProfileID;
            // Kiem tra ten lop co trung voi mot lop khac trong cung nam hoc khong khong
            var academicYear = this.AcademicYearBusiness.Find(frm.AcademicYearID);
            var temp = academicYear.ClassProfiles.FirstOrDefault(p => p.ClassProfileID != classProfileEditing.ClassProfileID && p.DisplayName.ToUpper() == frm.DisplayName.ToUpper() && p.IsActive == true);
            if (temp != null)
            {
                throw new BusinessException(Utils.Res.Get("ErrorMsg_SameClassName"));
            }
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            if (sp != null && sp.TrainingTypeID.HasValue && sp.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
            {
                if (frm.ClassType == null || frm.ClassType == 0)
                {
                    throw new BusinessException("Thầy/cô chưa chọn Loại lớp!");
                }
            }
            using (TransactionScope scope = new TransactionScope())
            {
                Utils.Utils.TrimObject(frm);
                //Nếu chọn ngoại ngữ 2 thì phải chọn ngoại ngữ 1
                if (frm.SubjectForeignLang2ID != null)
                {
                    if (frm.SubjectForeignLang1ID == null)
                    {
                        throw new BusinessException("Validate_Require_Language1");
                    }
                    else
                    {
                        if (frm.SubjectForeignLang1ID == frm.SubjectForeignLang2ID)
                        {
                            throw new BusinessException("Validate_Duplicate_Language");
                        }
                    }
                }

                // Cap nhat khong thay doi thong tin khoi hoc
                int oldEducationLevelID = classProfileEditing.EducationLevelID;
                var oldNumberOrder = classProfileEditing.OrderNumber;
                Utils.Utils.BindTo(frm, classProfileEditing, true);
                classProfileEditing.AcademicYearID = globalInfo.AcademicYearID.Value;
                if (frm.EducationLevelID != 0)
                {
                    classProfileEditing.EducationLevelID = frm.EducationLevelID;
                }
                else
                {
                    // Lay lai thong tin khoi hoc neu khong duoc phep sua thong tin khoi
                    classProfileEditing.EducationLevelID = oldEducationLevelID;
                }
                classProfileEditing.DisplayName = frm.DisplayName;
                classProfileEditing.Description = frm.Description;
                if (frm.ApprenticeshipGroupID != null && frm.ApprenticeshipGroupID != 0)
                {
                    classProfileEditing.ApprenticeshipGroupID = frm.ApprenticeshipGroupID;
                }
                classProfileEditing.HeadTeacherID = frm.HeadTeacherID;
                classProfileEditing.FirstForeignLanguageID = frm.SubjectForeignLang1ID;
                classProfileEditing.SecondForeignLanguageID = frm.SubjectForeignLang2ID;
                classProfileEditing.SchoolSubsidiaryID = frm.SchoolSubsidiaryID;
                classProfileEditing.SubCommitteeID = frm.SubCommitteeID;
                if (frm.OrderNumber.HasValue) classProfileEditing.OrderNumber = frm.OrderNumber;
                else classProfileEditing.OrderNumber = oldNumberOrder;

                if (frm.IsCombinedClass)
                {
                    if (string.IsNullOrEmpty(frm.CombinedClassCode))
                {
                        throw new BusinessException("Thầy/cô cần phải nhập mã lớp ghép");          
                    }
                    classProfileEditing.CombinedClassCode = frm.CombinedClassCode;
                }
                if (frm.IsSpecializedClass)
                {
                    if (frm.ClassSpecialtyCatID > 0)
                    {
                        classProfileEditing.ClassSpecialtyCatID = frm.ClassSpecialtyCatID;
                        classProfileEditing.IsSpecializedClass = frm.IsSpecializedClass;
                    }
                    else {
                        throw new BusinessException("Thầy/cô cần phải chọn hệ chuyên.");
                    }
                }
                classProfileEditing.IsITClass = frm.IsITClass;

                bool isNewModelSchool = false;
                if (sp != null)
                {
                    if (sp.IsNewSchoolModel == true)
                    {
                        isNewModelSchool = true;
                    }
                }

                if (isNewModelSchool)
                {
                    classProfileEditing.IsVnenClass = frm.IsVnenClass;
                }
                else
                {
                    classProfileEditing.IsVnenClass = false;
                }

                classProfileEditing.VemisCode = frm.VemisCode;

                int Section = this.GetSectionByListID(frm.ArrClassSeperateID);
                int SectionKey = this.GetSectionByListID(frm.ArrSeperateKeyID);
                if (Section > 0)
                {
                    classProfileEditing.Section = Section;
                }
                else
                {
                    throw new BusinessException("Validate_ArrClassSeperateID");
                }
                if (_globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                {
                    if (classProfileEditing.Section == SystemParamsInFile.SECTION_MORNING)
                    {
                        if (classProfileEditing.SeperateKey != SystemParamsInFile.SECTION_MORNING)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }
                    else if (Section == SystemParamsInFile.SECTION_AFTERNOON)
                    {
                        if (SectionKey != SystemParamsInFile.SECTION_AFTERNOON)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }
                    else if (Section == SystemParamsInFile.SECTION_EVENING)
                    {
                        if (SectionKey != SystemParamsInFile.SECTION_EVENING)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }
                    else if (Section == SystemParamsInFile.SECTION_MORNING_AFTERNOON)
                    {
                        if (SectionKey != SystemParamsInFile.SECTION_MORNING && SectionKey != SystemParamsInFile.SECTION_AFTERNOON && SectionKey != SystemParamsInFile.SECTION_MORNING_AFTERNOON)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }
                    else if (Section == SystemParamsInFile.SECTION_MORNING_EVENING)
                    {
                        if (SectionKey != SystemParamsInFile.SECTION_MORNING && SectionKey != SystemParamsInFile.SECTION_EVENING && SectionKey != SystemParamsInFile.SECTION_MORNING_EVENING)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }
                    else if (Section == SystemParamsInFile.SECTION_AFTERNOON_EVENING)
                    {
                        if (SectionKey != SystemParamsInFile.SECTION_AFTERNOON && SectionKey != SystemParamsInFile.SECTION_EVENING && SectionKey != SystemParamsInFile.SECTION_AFTERNOON_EVENING)
                        {
                            throw new BusinessException("Import_Validate_SectionKey_Err");
                        }
                    }


                    if (SectionKey > 0)
                    {
                        classProfileEditing.SeperateKey = SectionKey;
                    }
                    else
                    {
                        throw new BusinessException("Validate_ArrClassSeperateKeyID");
                    }
                }


                // Delete all PropertyOfClass
                var propertiesOfClass = this.PropertyOfClassBusiness.All.Where(poc => poc.ClassID == classProfileEditing.ClassProfileID).ToList();
                this.PropertyOfClassBusiness.DeleteAll(propertiesOfClass);

                // Gan lai cac gia tri moi se insert vao PropertyOfClass

                // Nhom checkbox
                if (frm.ArrClassPropertyCatID != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatID)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }

                if (frm.ArrClassPropertyCatGroupIDSectionLeft != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatGroupIDSectionLeft)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }
                if (frm.ArrClassPropertyCatGroupIDSectionRight != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatGroupIDSectionRight)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }
                if (frm.ArrClassPropertyCatGroupIDCombileLeft != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatGroupIDCombileLeft)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }
                if (frm.ArrClassPropertyCatGroupIDCombileRight != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatGroupIDCombileRight)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }
                // Nhom radio box
                if (frm.ArrClassPropertyCatGroupID != null)
                {
                    foreach (int propertyID in frm.ArrClassPropertyCatGroupID)
                    {
                        if (propertyID != 0)
                        {
                            PropertyOfClass propertyOfClass = new PropertyOfClass();
                            //propertyOfClass.ClassID = ClassID;
                            propertyOfClass.ClassPropertyCatID = (int)propertyID;
                            propertyOfClass.IsActive = true;
                            classProfileEditing.PropertyOfClasses.Add(propertyOfClass);
                        }
                    }
                }

                //Thuc hien xoa cac mon vnen neu bo check lop hoc vnen
                if (!frm.IsVnenClass)
                {
                    List<ClassSubject> lstCs = ClassSubjectBusiness.All.Where(o => o.ClassID == frm.ClassProfileID && o.IsSubjectVNEN == true).ToList();
                    for (int i = 0; i < lstCs.Count; i++)
                    {
                        ClassSubject cs = lstCs[i];
                        cs.IsSubjectVNEN = false;
                        ClassSubjectBusiness.BaseUpdate(cs);
                    }
                }

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("AcademicYearID", frm.AcademicYearID );
                dic.Add("SchoolID",  _globalInfo.SchoolID.Value );
                dic.Add("AppliedLevel", _globalInfo.AppliedLevel.Value);

                // Save change to database
                this.PropertyOfClassBusiness.Save();
                this.ClassProfileBusiness.UpdateCustom(classProfileEditing, dic);
               // this.ClassProfileBusiness.Update(classProfileEditing);
                //this.ClassProfileBusiness.Save();

                scope.Complete();
                ModelState.Clear();
                ViewData[SMAS.Web.Constants.GlobalConstants.JSON_MSG_LIST] = "['" + Res.Get("ClassProfile_Label_Edit_Suc") + "']";
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));

        }

        //
        // GET: /ClassProfile/Delete/
        [HttpPost]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_DELETE)]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ClassProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            CheckPermissionForAction(id, "ClassProfile");
            ClassProfile classprofile = this.ClassProfileBusiness.Find(id);
            GlobalInfo globalInfo = new GlobalInfo();
            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            int AppliedLevel = globalInfo.AppliedLevel.HasValue ? globalInfo.AppliedLevel.Value : 0;
            int schoolId = globalInfo.SchoolID.Value;
            //Luu thong tin phuc vu viec phuc hoi du lieu
            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = AcademicYearID,
                SCHOOL_ID = schoolId,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_CLASS,
                SHORT_DESCRIPTION = string.Format("Lớp học: {0}", classprofile.DisplayName),
            };

            this.ClassProfileBusiness.Delete(id, AcademicYearID, AppliedLevel, objRes, _globalInfo.UserAccountID);
            return Json(Res.Get("ClassProfile_Label_DeleteSuccessMessage"));
        }


        public ActionResult PrepareSearchHeadTeacher(string formID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic["SchoolID"] = new GlobalInfo().SchoolID;
            List<SchoolFaculty> lstsf = this.SchoolFacultyBusiness.Search(dic).OrderBy(o => o.FacultyName).ToList();
            ViewData[ClassProfileConstants.LS_CLASS_SCHOOL_FACULTY] = lstsf;
            EmployeeObject eo = new EmployeeObject();
            //if (lstsf.Count() > 0)
            //{
            //    eo.SchoolFacultyID = lstsf.FirstOrDefault().SchoolFacultyID;
            //}
            List<EmployeeObject> paging = new List<EmployeeObject>();
            ViewData[ClassProfileConstants.LS_CLASS_HEADTEACHER] = paging;
            ViewData[ClassProfileConstants.PARENTFROM] = formID;
            return View("SearchHeadTeacher");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateOrderNumberInClassProfile(string listClassProfileChangedOrderNumber)
        {
            var js = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<ClassProfileForm> result = new List<ClassProfileForm>();
            // Try catch de validate STT hoac ID lop la so nguyen duong
            try
            {
                result = js.Deserialize<List<ClassProfileForm>>(listClassProfileChangedOrderNumber);
            }
            catch
            {
                throw new BusinessException("ClassProfile_Label_NotPositiveSTT");
            }

            for (int i = 0; i < result.Count; i++)
            {
                ClassProfile classProfile = this.ClassProfileBusiness.Find(result[i].ClassProfileID);
                if (classProfile != null)
                {
                    if (result[i].OrderNumber.HasValue)
                    {
                        if (result[i].OrderNumber.Value > 0)
                        {
                            if (result[i].OrderNumber.Value <= MAX_CLASS_ORDER)
                            {
                                classProfile.OrderNumber = result[i].OrderNumber;
                            }
                            else
                            {
                                throw new BusinessException("ClassProfile_Label_NotRangeSTT");
                            }
                        }
                        else
                        {
                            throw new BusinessException("ClassProfile_Label_NotPositiveSTT");
                        }
                    }
                    else
                    {
                        classProfile.OrderNumber = null;
                    }
                }
            }

            this.ClassProfileBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateOrderNumberMessage")));

        }

        /// <summary>
        /// anhnph1_20150702_Xóa tho6ngtin giáo viên của lớp Giai đạon 10
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="teacherID"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DeleteHeadTecher(int? classProfileID, int? teacherID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ClassProfile", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(classProfileID, "ClassProfile");
            ClassProfile classprofile = this.ClassProfileBusiness.All.Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                                                                    && p.SchoolID == _globalInfo.SchoolID
                                                                    && p.ClassProfileID == classProfileID
                                                                    && p.HeadTeacherID == teacherID
                                                                    && p.IsActive.Value).FirstOrDefault();
            ClassProfile DeleteHeadTeacher = null;
            if (classprofile != null)
            {
                if (!string.IsNullOrEmpty(classprofile.CombinedClassCode))
                {
                    List<ClassProfile> lstPC = this.ClassProfileBusiness.All.Where(p => p.AcademicYearID == _globalInfo.AcademicYearID
                                                                    && p.SchoolID == _globalInfo.SchoolID
                                                                    && p.CombinedClassCode.ToUpper().Equals(classprofile.CombinedClassCode.ToUpper())
                                                                    && p.EducationLevel.Grade == _globalInfo.AppliedLevel.Value
                                                                    && p.IsCombinedClass == true
                                                                    && p.IsActive.Value).ToList();

                    for (int i = 0; i < lstPC.Count; i++)
                    {
                        DeleteHeadTeacher = lstPC[i];
                        DeleteHeadTeacher.HeadTeacherID = null;
                        this.ClassProfileBusiness.Update(DeleteHeadTeacher);
                    }                  
                }
                else
                {
                    DeleteHeadTeacher = new ClassProfile();
                DeleteHeadTeacher = classprofile;
                DeleteHeadTeacher.HeadTeacherID = null;
                this.ClassProfileBusiness.Update(DeleteHeadTeacher);
                    
                }
                this.ClassProfileBusiness.Save();
            }
        }
        #endregion

        #region Extent Method
        private List<ClassProfileForm> _SearchClassProfile(int? EducationLevelId)
        {
            SetViewDataPermission("ClassProfile", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            //search 
            Dictionary<string, object> dic = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();
            ViewData[ClassProfileConstants.IS_CURRENT_YEAR] = globalInfo.IsCurrentYear;
            int? AcademicYearID = globalInfo.AcademicYearID;
            int? SchoolID = globalInfo.SchoolID;
            if (EducationLevelId.HasValue)
            {
                dic.Add("EducationLevelID", EducationLevelId);
            }
            if (AcademicYearID.HasValue)
            {
                dic.Add("AcademicYearID", AcademicYearID.Value);
            }
            if (SchoolID.HasValue)
            {
                dic.Add("SchoolID", SchoolID.Value);
            }
            IQueryable<ClassProfile> lst = ClassProfileBusiness.Search(dic);


            // Convert du lieu sang doi tuong SupervisingDeptForm
            IQueryable<ClassProfileForm> res = lst.Select(o => new ClassProfileForm
            {
                ClassProfileID = o.ClassProfileID,
                DisplayName = o.DisplayName,
                Description = o.IsCombinedClass == true ? (!string.IsNullOrEmpty(o.Description) ? ("Mã lớp ghép: " + o.CombinedClassCode + ". " + o.Description) : ("Mã lớp ghép: " + o.CombinedClassCode)) : o.Description,
                HeadTeacherName = o.Employee.FullName,
                EducationLevelID = o.EducationLevelID,
                OrderNumber = o.OrderNumber
            });
            List<ClassProfileForm> listClassProfileForm = res.ToList();

            // Congnv: Tunning
            //if (!EducationLevelId.HasValue)
            //{
            //    ListClassProfileForm = ListClassProfileForm.OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName).ToList();
            //}
            //else
            //{
            //    ListClassProfileForm = ListClassProfileForm.OrderBy(o => o.DisplayName).ToList();
            //}

            if (!EducationLevelId.HasValue)
            {
                List<ClassProfileForm> listClassProfileFormAfterRemoved = new List<ClassProfileForm>();
                List<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels;
                if (lsEducationLevel != null)
                {
                    for (int i = listClassProfileForm.Count - 1; i >= 0; i--)
                    {
                        if (lsEducationLevel.Exists(o => o.EducationLevelID == listClassProfileForm[i].EducationLevelID))
                        {
                            listClassProfileFormAfterRemoved.Add(listClassProfileForm[i]);
                        }
                    }
                }

                listClassProfileFormAfterRemoved = listClassProfileFormAfterRemoved.OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();

                return listClassProfileFormAfterRemoved;
            }

            return listClassProfileForm.OrderBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
        }

        /// <summary>
        /// Tim kiem giao vien va phan trang
        /// </summary>
        /// <param name="EmployeeObject"></param>
        /// <param name="page"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        private List<EmployeeObject> _SearchTeacher(EmployeeObject EmployeeObject)
        {
            // trim
            // Utils.Utils.TrimObject(EmployeeObject);
            // Dieu kien truy van du lieu
            GlobalInfo globalInfo = new GlobalInfo();
            int SchoolFacultyID = 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            int? AcademicYearID = globalInfo.AcademicYearID;
            if (AcademicYearID.HasValue)
            {
                dic.Add("AcademicYearID", AcademicYearID);
            }
            if (EmployeeObject != null)
            {
                string FullName = EmployeeObject.FullName;
                if (FullName != null && !string.Empty.Equals(FullName))
                {
                    dic["FullName"] = FullName;
                }
                if (EmployeeObject.SchoolFacultyID.HasValue)
                {
                    SchoolFacultyID = EmployeeObject.SchoolFacultyID.Value;
                }
                if (SchoolFacultyID != 0)
                {
                    dic["FacultyID"] = SchoolFacultyID;
                }
            }
            int SchoolID = globalInfo.SchoolID.HasValue ? globalInfo.SchoolID.Value : 0;
            //DungVA - Thêm điều kiện giáo viên còn đang giảng dạy
            dic.Add("EmploymentStatus", SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING);
            List<EmployeeObject> ListEmployee = new List<EmployeeObject>();
            IQueryable<Employee> res = EmployeeBusiness.SearchTeacher(SchoolID, dic);
            if (res != null)
            {
                res = res.OrderBy(o => o.Name).ThenBy(o => o.FullName);
                List<Employee> lstres = res.ToList();

                ListEmployee = lstres.Select(o => new EmployeeObject
                {
                    EmployeeID = o.EmployeeID,
                    BirthDate = o.BirthDate,
                    FullName = o.FullName
                }).ToList();
            }


            //List<EmployeeObject> ListTeacher = new List<EmployeeObject>();
            Dictionary<string, object> dicSearchClassProfile = new Dictionary<string, object>();
            dicSearchClassProfile["AcademicYearID"] = globalInfo.AcademicYearID.Value;
            List<ClassProfile> LsClassProfile = this.ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.GetValueOrDefault(), dicSearchClassProfile).ToList();
            if (LsClassProfile != null && LsClassProfile.Count() > 0)
            {
                foreach (EmployeeObject teacher in ListEmployee)
                {
                    string strHeadClass = string.Empty;

                    List<ClassProfile> ListClassProfile = LsClassProfile.Where(o => o.HeadTeacherID == teacher.EmployeeID).ToList();
                    foreach (ClassProfile ClassProfile in ListClassProfile)
                    {
                        strHeadClass += ", " + ClassProfile.DisplayName;
                    }
                    if (!strHeadClass.Equals(string.Empty))
                    {
                        teacher.HeadClass = strHeadClass.Substring(2);
                    }
                }
            }
            return ListEmployee;
        }

        /// <summary>
        /// Ham khoi tao du lieu truoc khi moi man hinh tao moi khai bao lop
        /// </summary>
        private void InitData(bool isCreate)
        {
            // Tu dien luu tru thong tin tim kiem
            GlobalInfo globalInfo = new GlobalInfo();

            // QuangNN2 - Bổ sung điều kiện loại hình đào tạo của trường là GDTX
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

            int AcademicYearID = globalInfo.AcademicYearID.HasValue ? globalInfo.AcademicYearID.Value : 0;
            Dictionary<string, object> dicSeachParam = new Dictionary<string, object>();
            dicSeachParam.Add("IsActive", true);
            // Lay du lieu khoi hoc
            List<EducationLevel> lsEducationLevel = globalInfo.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[ClassProfileConstants.LS_EDUCATION_LEVEL] = lsEducationLevel;
            }
            else
            {
                ViewData[ClassProfileConstants.LS_EDUCATION_LEVEL] = new List<EducationLevel>();
            }

            // Lay du lieu mon hoc nghe
            dicSeachParam.Add("IsApprenticeshipSubject", true);
            dicSeachParam["AppliedLevel"] = globalInfo.AppliedLevel.Value;
            dicSeachParam["SchoolID"] = globalInfo.SchoolID;
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_VOCATIONAL] = ApprenticeshipGroupBusiness.Search(dicSeachParam).OrderBy(o => o.GroupName);
            // Lay du lieu ct dao tao boi duong
            Dictionary<string, object> dicSeachTraningPro = new Dictionary<string, object>();
            ViewData[ClassProfileConstants.LS_EDUCATION_TRAINING_PROGRAM] = TrainingProgramBusiness.Search(dicSeachTraningPro).OrderBy(o => o.TrainingProgramID);
            Dictionary<string, object> dicSeachSpecialtyClass = new Dictionary<string, object>();
            ViewData[ClassProfileConstants.LS_EDUCATION_CLASS_SPECIALTY_CAT] = ClassSpecialtyCatBusiness.Search(dicSeachSpecialtyClass).OrderBy(o => o.Resolution);
            // Lay du lieu mon hoc ngoai ngu
            dicSeachParam.Remove("IsApprenticeshipSubject");
            dicSeachParam.Add("IsForeignLanguage", true);
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_FOREIGNLANG] = this.SubjectCatBusiness.Search(dicSeachParam).OrderBy(o => o.SubjectName);

            // Lay du lieu truong phu
            dicSeachParam.Remove("IsForeignLanguage");
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SUBSCHOOL] = this.SchoolSubsidiaryBusiness.Search(dicSeachParam).OrderBy(o => o.SubsidiaryName);

            if (isCreate)
            {
                // Lay du lieu tinh chat lop hoc
                List<ClassPropertyCat> ListClassPropertyCatAll = this.ClassPropertyCatBusiness.Search(dicSeachParam).ToList();

                // Buoi hoc Sang - Chieu - Toi
                List<ViettelCheckboxList> listPropertyCatChk = new List<ViettelCheckboxList>();
                listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Morning"), ClassProfileConstants.MORNING, false, false));
                listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Afternoon"), ClassProfileConstants.AFTERNOON, false, false));
                listPropertyCatChk.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Night"), ClassProfileConstants.NIGHT, false, false));

                List<ViettelCheckboxList> listSeperateKey = new List<ViettelCheckboxList>();
                listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Morning"), ClassProfileConstants.MORNING, false, true));
                listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Afternoon"), ClassProfileConstants.AFTERNOON, false, true));
                listSeperateKey.Add(new ViettelCheckboxList(Res.Get("ClassProfile_Control_Separate_Night"), ClassProfileConstants.NIGHT, false, true));

                // Tinh chat lop hoc
                List<ViettelCheckboxList> listClassPropertyCat = new List<ViettelCheckboxList>();
                var listClassPropertyCatOtherLeft = new List<ViettelCheckboxList>();
                var listClassPropertyCatOtherRight = new List<ViettelCheckboxList>();
                Dictionary<int, List<ViettelCheckboxList>> dicPropertyGroup = new Dictionary<int, List<ViettelCheckboxList>>();
                var dicPropertyGroupSectionLeft = new Dictionary<int, List<ViettelCheckboxList>>();
                var dicPropertyGroupSectionRight = new Dictionary<int, List<ViettelCheckboxList>>();
                var dicPropertyGroupCombileLeft = new Dictionary<int, List<ViettelCheckboxList>>();
                var dicPropertyGroupCombileRight = new Dictionary<int, List<ViettelCheckboxList>>();
                ListClassPropertyCatAll = ListClassPropertyCatAll.OrderBy(x => x.Resolution).ThenBy(x => x.ClassPropertyCatID).ToList();
                foreach (ClassPropertyCat cpc in ListClassPropertyCatAll)
                {
                    ViettelCheckboxList item = new ViettelCheckboxList(cpc.Resolution, cpc.ClassPropertyCatID, false, false);
                    if (cpc.PropertyGroup.HasValue && cpc.PropertyGroup.Value > 0)
                    {
                        // Danh sach hien thi danh radiobox   
                        // PropertyGroup == 1 => Div Section left
                        // PropertyGroup == 2 => Div Section right
                        // PropertyGroup == 3 => Div CombileClass left
                        // PropertyGroup == 4 => Div CombileClass right
                        int propertyGroup = cpc.PropertyGroup.Value;
                        if (dicPropertyGroupSectionLeft.ContainsKey(propertyGroup))
                        {
                            //dicPropertyGroup[propertyGroup].Add(item);
                            if (cpc.PropertyGroup.Value == 1)
                            {
                                dicPropertyGroupSectionLeft[propertyGroup].Add(item);
                                continue;
                            }
                        }
                        else
                        {
                            List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                            tempList.Add(item);
                            //dicPropertyGroup[propertyGroup] = tempList; 
                            if (cpc.PropertyGroup.Value == 1)
                            {
                                dicPropertyGroupSectionLeft[propertyGroup] = tempList;
                                continue;
                            }
                        }

                        if (dicPropertyGroupSectionRight.ContainsKey(propertyGroup))
                        {
                            //dicPropertyGroup[propertyGroup].Add(item);
                            if (cpc.PropertyGroup.Value == 2)
                            {
                                dicPropertyGroupSectionRight[propertyGroup].Add(item);
                                continue;
                            }
                        }
                        else
                        {
                            List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                            tempList.Add(item);
                            //dicPropertyGroup[propertyGroup] = tempList;
                            if (cpc.PropertyGroup.Value == 2)
                            {
                                dicPropertyGroupSectionRight[propertyGroup] = tempList;
                                continue;
                            }
                        }

                        if (dicPropertyGroupCombileLeft.ContainsKey(propertyGroup))
                        {
                            //dicPropertyGroup[propertyGroup].Add(item);
                            if (cpc.PropertyGroup.Value == 3)
                            {
                                dicPropertyGroupCombileLeft[propertyGroup].Add(item);
                                continue;
                            }
                        }
                        else
                        {
                            List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                            tempList.Add(item);
                            //dicPropertyGroup[propertyGroup] = tempList;
                            if (cpc.PropertyGroup.Value == 3)
                            {
                                dicPropertyGroupCombileLeft[propertyGroup] = tempList;
                                continue;
                            }
                        }


                        if (dicPropertyGroupCombileRight.ContainsKey(propertyGroup))
                        {
                            //dicPropertyGroup[propertyGroup].Add(item);
                            if (cpc.PropertyGroup.Value == 4)
                            {
                                dicPropertyGroupCombileRight[propertyGroup].Add(item);
                                continue;
                            }
                        }
                        else
                        {
                            List<ViettelCheckboxList> tempList = new List<ViettelCheckboxList>();
                            tempList.Add(item);
                            //dicPropertyGroup[propertyGroup] = tempList;
                            if (cpc.PropertyGroup.Value == 4)
                            {
                                dicPropertyGroupCombileRight[propertyGroup] = tempList;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        // Danh sach hien thi danh checkbox
                        // QuangNN2 - Bổ sung điều kiện loại hình đào tạo của trường là GDTX
                        // TrainingTypeID == 1 => Div Other property left
                        // TrainingTypeID == 2 => Div Other property right
                        if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                        {
                            if (school.TrainingTypeID.HasValue && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                            {
                                if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == 2)
                                {
                                        listClassPropertyCat.Add(item);
                                        listClassPropertyCatOtherRight.Add(item);                                    
                                }
                                else
                                {
                                    listClassPropertyCat.Add(item);
                                    listClassPropertyCatOtherLeft.Add(item);
                                }
                            }
                            
                        }
                        else if (cpc.ApplyTrainingType.HasValue && cpc.ApplyTrainingType.Value == 2)
                        {
                            listClassPropertyCat.Add(item);
                            listClassPropertyCatOtherRight.Add(item);
                        }
                        else
                        {
                            listClassPropertyCat.Add(item);
                            listClassPropertyCatOtherLeft.Add(item);
                        }
                    }
                }

                ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT] = listClassPropertyCat;
                ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT_OTHER_LEFT] = listClassPropertyCatOtherLeft;
                ViewData[ClassProfileConstants.LS_CLASS_PROPERTY_CAT_OTHER_RIGHT] = listClassPropertyCatOtherRight;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE] = listPropertyCatChk;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_KEY] = listSeperateKey;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP] = dicPropertyGroup;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_LEFT] = dicPropertyGroupSectionLeft;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_SECTION_RIGHT] = dicPropertyGroupSectionRight;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_LEFT] = dicPropertyGroupCombileLeft;
                ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SEPARATE_GROUP_COMBILE_RIGHT] = dicPropertyGroupCombileRight;
            }
            // Lay nam hoc hien tai
            AcademicYear AcademicYear = this.AcademicYearBusiness.Find(AcademicYearID);
            ViewData[ClassProfileConstants.ACADEMICYEAR] = AcademicYear;

            // Lay du lieu phan ban
            ViewData[ClassProfileConstants.LS_CLASS_PROFILE_SUBCOMMITEE] = this.SubCommitteeBusiness.Search(dicSeachParam).OrderBy(o => o.Resolution).ToList();
            var gradeSchool = school != null ? school.EducationGrade : 0;
            ViewData[ClassProfileConstants.LS_EDUCATION_CLASS_BY_SCHOOL] = CommonList.EducationLevelByGradeSchool(gradeSchool);
            if (gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_TERTIARY && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_CRECHE
                && gradeSchool != SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN)
            {
                ViewData[ClassProfileConstants.IS_MULTIPLE_LEVEL] = true;
            }
            if (school != null && school.TrainingTypeID.HasValue && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                ViewData[ClassProfileConstants.STRTRAINING_TYPE_GDTX] = ClassProfileConstants.TRAINING_TYPE_GDTX;
            ViewData[ClassProfileConstants.LS_EDUCATION_CLASS_TYPE] = GetDataSelectList(1);
            ViewData[ClassProfileConstants.LS_EDUCATION_TRAINING_TYPE] = GetDataSelectList(2);
            ViewData[ImportClassConstants.SHOW_MESSAGE_ERROR] = true;

        }

        /// <summary>
        /// Chuyen thanh dang so nhi phan
        /// Quanglm
        /// </summary>
        /// <param name="listValue"></param>
        /// <returns></returns>
        private int Convert2Bynary(List<int> listValue)
        {
            int res = 0;
            foreach (int value in listValue)
            {
                res += (int)Math.Pow(2, value - 1);
            }
            return res;
        }
        private int GetSectionByListID(List<int> listValue)
        {
            if (listValue == null || listValue.Count == 0)
            {
                return 0;
            }
            int result = 0;
            int tmp = 1;
            if (listValue.Count == 1)
            {
                result = listValue[0];
            }
            else
            {
                for (int i = 0; i < listValue.Count; i++)
                {
                    tmp += listValue[i];
                }
                result = tmp;
            }
            return result;
        }

        /// <summary>
        /// Kiem tra mot ky tu tai vi tri do xem co trung voi 1
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private bool CheckPosVal(string s, int pos)
        {
            if (s == null || s.Equals(string.Empty))
            {
                return false;
            }
            if (s.Length < 3)
            {
                for (int i = s.Length; i < 3; i++)
                {
                    s = "0" + s;
                }
            }
            if (pos >= s.Length)
            {
                return false;
            }
            char c = s[pos];
            if (c.Equals('1'))
            {
                return true;
            }
            return false;
        }

        private bool CheckSection(string sectionName, int sectionID)
        {
            bool result = false;
            if (sectionName.Equals(GlobalConstants.SECTION_MONING))
            {
                if (sectionID == SystemParamsInFile.SECTION_MORNING || sectionID == SystemParamsInFile.SECTION_MORNING_AFTERNOON
                    || sectionID == SystemParamsInFile.SECTION_MORNING_EVENING || sectionID == SystemParamsInFile.SECTION_ALL)
                {
                    result = true;
                }
            }
            else if (sectionName.Equals(GlobalConstants.SECTION_AFTERNOON))
            {
                if (sectionID == SystemParamsInFile.SECTION_AFTERNOON || sectionID == SystemParamsInFile.SECTION_MORNING_AFTERNOON
                    || sectionID == SystemParamsInFile.SECTION_AFTERNOON_EVENING || sectionID == SystemParamsInFile.SECTION_ALL)
                {
                    result = true;
                }
            }
            else if (sectionName.Equals(GlobalConstants.SECTION_EVENING))
            {
                if (sectionID == SystemParamsInFile.SECTION_EVENING || sectionID == SystemParamsInFile.SECTION_MORNING_EVENING || sectionID == SystemParamsInFile.SECTION_AFTERNOON_EVENING
                    || sectionID == SystemParamsInFile.SECTION_ALL)
                {
                    result = true;
                }
            }
            return result;
        }

        public int? ParseToNull(string numberStr)
        {
            int id;
            return int.TryParse(numberStr, out id) ? (int?)id : null;
        }
        #endregion
        #region Import Excel 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit(UserActionID = SMAS.Business.Common.GlobalConstants.ACTION_IMPORT)]
        public JsonResult ImportExcel()
        {
            /*Lưu file lên server - DungVA - 31/05/2013*/
            HttpPostedFileBase file = (HttpPostedFileBase)Session["stream"];
            string physicalPath = string.Empty;
            if (file != null)
            {
                String FileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), FileName);
                file.SaveAs(physicalPath);
                //Session["FilePath"] = physicalPath;
            }
            /*End Lưu file lên server*/
            if (String.IsNullOrEmpty(physicalPath))
            {
                throw new BusinessException(Res.Get("ImportClass_Label_FailedTemplated"));
            }
            //string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            IVTRange range = sheet.GetRow(1);

            #region Kiểm tra template
            string SchoolNameTitle = (string)sheet.GetCellValue(2, 1);
            string ClassTitle = (string)sheet.GetCellValue(3, 1);
            string OrderNumberTitle = sheet.GetCellValue(5, 1) != null ? sheet.GetCellValue(5, 1).ToString() : string.Empty;
            string educationLevelExcel = (string)sheet.GetCellValue(5, 2);
            string classNameExcel = (string)sheet.GetCellValue(5, 3);
            string subCommityNameExcel = (string)sheet.GetCellValue(5, 4);
            string teacherNameExcel = (string)sheet.GetCellValue(5, 5);
            string session = (string)sheet.GetCellValue(5, 6);
            string sessionKey = (string)sheet.GetCellValue(5, 7);

            string schoolName = Res.Get("ImportClass_Label_SchoolNameExcel") + _globalInfo.SchoolName.ToUpper();
            string EducationLevelName = Res.Get("ImportClass_Label_educationLevelExcel");
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                EducationLevelName = Res.Get("PupilProfile_Label_Class_MN");

            }
            if (String.Compare(SchoolNameTitle, schoolName) != 0 ||
                String.Compare(ClassTitle, Res.Get("ImportClass_Label_listClassExcel")) != 0 ||
                String.Compare(OrderNumberTitle, Res.Get("ImportClass_Label_orderNumberExcel")) != 0 ||
                String.Compare(educationLevelExcel, EducationLevelName) != 0 ||
                String.Compare(classNameExcel, Res.Get("ImportClass_Label_classNameExcel")) != 0 ||
                String.Compare(subCommityNameExcel, Res.Get("ImportClass_Label_subCommityNameExcel")) != 0 ||
                !teacherNameExcel.Contains("Giáo viên chủ nhiệm") ||
                String.Compare(session, Res.Get("ImportClass_Label_Section")) != 0 ||
                (String.Compare(sessionKey, Res.Get("Buổi học chính (*)")) != 0))
            {
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY
                        || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    throw new BusinessException(Res.Get("ImportClass_Label_FailedTemplated"));
                }
            }
            #endregion

            // Khởi tạo dữ liệu
            int START = 6;
            int i = START;
            string educationLevel, className, headTeacher, subComittee, section, sectionkey;
            StringBuilder error = new StringBuilder();

            int SchoolID = _globalInfo.SchoolID.Value;

            List<SubCommittee> lstSubCommittee = SubCommitteeBusiness.All.ToList();
            List<string> lstSubCommitteeName = lstSubCommittee.Select(o => o.Resolution).ToList();
            IQueryable<Employee> iqEmployee = EmployeeBusiness.Search(new Dictionary<string, object> { 
                                                            {
                                                                "CurrentSchoolID", SchoolID
                                                            }});
            List<string> lsEmployeeName = iqEmployee.Select(o => o.FullName.ToLower()).ToList();

            //IQueryable<SchoolFaculty> iqFaculty = SchoolFacultyBusiness.Search(new Dictionary<string, object> { 
            //                                                {
            //                                                    "SchoolID", SchoolID
            //                                                }});
            //List<string> lstFacultyName = iqFaculty.Select(o => o.FacultyName).ToList();
            //IQueryable<TeacherOfFaculty> iqTeacherOfFaculty = TeacherOfFacultyBusiness.Search(new Dictionary<string, object> { 
            //                                                {
            //                                                    "LstFacultyID", iqFaculty.Select(o => o.SchoolFacultyID).ToList()
            //                                                }});

            List<ImportClassViewModel> lstImportClass = new List<ImportClassViewModel>();
            ImportClassViewModel importClass;
            int academicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> dicClassName = new Dictionary<string, object>();
            dicClassName["AcademicYearID"] = academicYearID;
            dicClassName["SchoolID"] = SchoolID;
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(SchoolID, dicClassName).ToList();
            IDictionary<int, string> infoClass = new Dictionary<int, string>();
            lstClassProfile.ForEach(o => infoClass.Add(o.ClassProfileID, o.DisplayName));
            IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicClassName).Where(o => infoClass.Keys.Contains(o.ClassID));
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["AcademicYearID"] = academicYearID;
            IQueryable<ClassSubject> iqClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicClassSubject);
            IDictionary<int, string> infoEducation = new Dictionary<int, string>();
            _globalInfo.EducationLevels.ForEach(o => infoEducation.Add(o.EducationLevelID, o.Resolution));
            List<string> lsClassName = new List<string>();
            ClassProfile objClassProfile;
            List<ClassProfile> lstClassIns = new List<ClassProfile>();

            int EducationLevelID = 0;
            int SubComitteeID = 0; // Ban học
            int HeadTeacherID = 0;
            int SchoolFalcutyID = 0;
            int Section = 0;
            int SectionKey = 0;
            string employeeCode = string.Empty;
            do
            {
                educationLevel = (string)sheet.GetCellValue(i, 2);
                className = (string)sheet.GetCellValue(i, 3);
                subComittee = (string)sheet.GetCellValue(i, 4);
                headTeacher = (string)sheet.GetCellValue(i, 5);
                //schoolFaculty = (string)sheet.GetCellValue(i, 6);
                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                section = (string)sheet.GetCellValue(i, 6);
                //End: 2014/11/24 viethd4 Bổ sung buổi học
                sectionkey = (string)sheet.GetCellValue(i, 7);

                // Điều kiện dừng
                if (string.IsNullOrEmpty(educationLevel) && string.IsNullOrEmpty(className) && string.IsNullOrEmpty(subComittee)
                    && string.IsNullOrEmpty(headTeacher) && string.IsNullOrEmpty(section) && string.IsNullOrEmpty(sectionkey))
                    break;

                #region  Kiểm tra Import
                error = new StringBuilder();
                if (string.IsNullOrEmpty(className)) error.Append(Res.Get("Import_Validate_ClassName"));
                else if (infoClass.Values.Contains(className.Trim()))
                {
                    // Kiểm tra đã chọn khối
                    if (!string.IsNullOrEmpty(educationLevel))
                    {
                        objClassProfile = lstClassProfile.FirstOrDefault(o => className.Equals(o.DisplayName));
                        // Kiểm tra cùng tên khác khối
                        if (objClassProfile.EducationLevelID != infoEducation.FirstOrDefault(o => o.Value.Equals(educationLevel)).Key)
                        {
                            // Kiểm tra đã có HS hoặc đã khai báo môn học
                            bool IsExists = iqPupilOfClass.Count(o => o.ClassID == objClassProfile.ClassProfileID) > 0 || iqClassSubject.Count(o => o.ClassID == objClassProfile.ClassProfileID) > 0;
                            if (IsExists)
                            {
                                error.Append(Res.Get("Import_Validate_DuplicateClassName"));
                            }
                        }
                    }
                }
                else if (lsClassName.Contains(className)) error.Append(Res.Get("Import_Validate_ClassNameExist"));
                else lsClassName.Add(className);

                // Reset data
                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                EducationLevelID = SubComitteeID = HeadTeacherID = SchoolFalcutyID = Section = 0;
                //End: 2014/11/24 viethd4 Bổ sung buổi học

                // Ban hoc
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY) // Cấp 3 có ban học
                {
                    if (string.IsNullOrEmpty(subComittee))
                        error.Append(Res.Get("Import_Validate_SubCommittee"));
                    else if (!lstSubCommitteeName.Contains(subComittee))
                        error.Append(Res.Get("Import_Validate_SubCommitteeExist"));
                    else SubComitteeID = lstSubCommittee.FirstOrDefault(o => o.Resolution.Equals(subComittee)).SubCommitteeID;
                }

                // Giáo viên
                ////Edit code lai k validate GVCN đồng bộ với khi validate tạo mới
                //if (string.IsNullOrEmpty(headTeacher))
                //    error.Append(Res.Get("Import_Validate_HeadTeacher"));
                if (!string.IsNullOrEmpty(headTeacher))
                {
                    var arr = headTeacher.Split('\\');
                    employeeCode = arr[arr.Length - 1].Trim();
                    Employee objE = iqEmployee.FirstOrDefault(o => o.EmployeeCode.Equals(employeeCode));
                    if (objE != null)
                    {
                        HeadTeacherID = objE.EmployeeID;
                    }
                    //else
                    //{
                    //    error.Append("Không tồn tại giáo viên " + headTeacher);
                    //}
                }
                // To bo mon
                //if (string.IsNullOrEmpty(schoolFaculty))
                //    error.Append(Res.Get("Import_Validate_SchoolFaculty"));
                //else if (!lstFacultyName.Contains(schoolFaculty))
                //    error.Append(Res.Get("Import_Validate_SchoolFacultyExist"));
                //else SchoolFalcutyID = iqFaculty.FirstOrDefault(o => o.FacultyName.Equals(schoolFaculty)).SchoolFacultyID;

                // Cap hoc
                //Bổ sung thêm cho cấp GDTX tự lưu loại lớp theo cấp học
                if (string.IsNullOrEmpty(educationLevel))
                    error.Append(Res.Get("Import_Validate_EducationLevel"));
                else if (!infoEducation.Values.Contains(educationLevel))
                    error.Append(Res.Get("Import_Validate_EducationLevelExist"));
                else
                {
                    EducationLevelID = infoEducation.FirstOrDefault(o => o.Value.Equals(educationLevel)).Key;
                }

                // Giao vien thuoc To bo mon
                //if (SchoolFalcutyID > 0 && HeadTeacherID > 0 && !iqTeacherOfFaculty.Any(o => o.FacultyID == SchoolFalcutyID && o.TeacherID == HeadTeacherID))
                //    error.Append(Res.Get("Import_Validate_TeacherOfFaculty"));

                //Start: 2014/11/24 viethd4 Bổ sung buổi học
                //Buổi học
                if (string.IsNullOrEmpty(section))
                {
                    error.Append(Res.Get("Import_Validate_Section_Required"));
                }
                else if (string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING) != 0
                    && string.Compare(section, ImportClassConstants.STRING_SECTION_ALL) != 0)
                {
                    error.Append(Res.Get("Import_Validate_Section_NotExist"));
                }
                else
                {
                    switch (section)
                    {
                        case ImportClassConstants.STRING_SECTION_MORNING:
                            Section = ImportClassConstants.SECTION_MORNING;
                            break;
                        case ImportClassConstants.STRING_SECTION_AFTERNOON:
                            Section = ImportClassConstants.SECTION_AFTERNOON;
                            break;
                        case ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON:
                            Section = ImportClassConstants.SECTION_MORNING_AFTERNOON;
                            break;
                        case ImportClassConstants.STRING_SECTION_EVENING:
                            Section = ImportClassConstants.SECTION_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_MORNING_EVENING:
                            Section = ImportClassConstants.SECTION_MORNING_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING:
                            Section = ImportClassConstants.SECTION_AFTERNOON_EVENING;
                            break;
                        case ImportClassConstants.STRING_SECTION_ALL:
                            Section = ImportClassConstants.SECTION_ALL;
                            break;
                    }
                }
                //End: 2014/11/24 viethd4 Bổ sung buổi học

                //check buoi hoc chinh
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                    || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {

                    if (string.IsNullOrEmpty(sectionkey))
                    {
                        error.Append(Res.Get("Import_Validate_SectionKey_Required"));
                    }
                    else if (string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_MORNING_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING) != 0
                        && string.Compare(section, ImportClassConstants.STRING_SECTION_ALL) != 0)
                    {
                        error.Append(Res.Get("Import_Validate_SectionKey_NotExist"));
                    }
                    else
                    {
                        switch (sectionkey)
                        {
                            case ImportClassConstants.STRING_SECTION_MORNING:
                                SectionKey = ImportClassConstants.SECTION_MORNING;
                                break;
                            case ImportClassConstants.STRING_SECTION_AFTERNOON:
                                SectionKey = ImportClassConstants.SECTION_AFTERNOON;
                                break;
                            case ImportClassConstants.STRING_SECTION_MORNING_AFTERNOON:
                                SectionKey = ImportClassConstants.SECTION_MORNING_AFTERNOON;
                                break;
                            case ImportClassConstants.STRING_SECTION_EVENING:
                                SectionKey = ImportClassConstants.SECTION_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_MORNING_EVENING:
                                SectionKey = ImportClassConstants.SECTION_MORNING_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_AFTERNOON_EVENING:
                                SectionKey = ImportClassConstants.SECTION_AFTERNOON_EVENING;
                                break;
                            case ImportClassConstants.STRING_SECTION_ALL:
                                SectionKey = ImportClassConstants.SECTION_ALL;
                                break;
                        }

                        if (Section == ImportClassConstants.SECTION_MORNING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_AFTERNOON)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_AFTERNOON)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_MORNING_AFTERNOON)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING && SectionKey != ImportClassConstants.SECTION_AFTERNOON && SectionKey != ImportClassConstants.SECTION_MORNING_AFTERNOON)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_MORNING_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_MORNING && SectionKey != ImportClassConstants.SECTION_EVENING && SectionKey != ImportClassConstants.SECTION_MORNING_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                        else if (Section == ImportClassConstants.SECTION_AFTERNOON_EVENING)
                        {
                            if (SectionKey != ImportClassConstants.SECTION_AFTERNOON && SectionKey != ImportClassConstants.SECTION_EVENING && SectionKey != ImportClassConstants.SECTION_AFTERNOON_EVENING)
                            {
                                error.Append(Res.Get("Import_Validate_SectionKey_Err"));
                            }
                        }
                    }
                }
                // Danh sách hiển thị lên
                importClass = new ImportClassViewModel();
                importClass.DisplayName = className;
                importClass.EducationLevelName = educationLevel;
                importClass.SubCommitteeName = subComittee;
                importClass.HeadTeacherName = headTeacher;
                //importClass.SchoolFalcutyName = schoolFaculty;
                importClass.Section = section;
                importClass.SectionKey = sectionkey;

                if (error.Length == 0)
                {
                    importClass.Pass = true;
                    objClassProfile = new ClassProfile();
                    objClassProfile.HeadTeacherID = HeadTeacherID;
                    objClassProfile.EducationLevelID = EducationLevelID;
                    objClassProfile.SubCommitteeID = SubComitteeID;
                    objClassProfile.AcademicYearID = academicYearID;
                    objClassProfile.SchoolID = SchoolID;
                    objClassProfile.DisplayName = className;
                    objClassProfile.Section = Section;
                    objClassProfile.SeperateKey = SectionKey;
                    //Gán loại lớp cho cấp GDTX
                    var school = SchoolProfileBusiness.Find(SchoolID);
                    if (school != null && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                    {
                        var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                        var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                        var listEduTerrary = new List<int>() { 10, 11, 12 };
                        if (listEduLvPrimary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 1;
                        }
                        else if (listEduSecondary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 2;
                        }
                        else if (listEduTerrary.Contains(EducationLevelID))
                        {
                            objClassProfile.ClassType = 3;
                        }
                    }
                    lstClassIns.Add(objClassProfile);
                }
                else
                {
                    importClass.Description = error.ToString();
                    importClass.Pass = false;
                }
                

                lstImportClass.Add(importClass);

                // Tăng chỉ số dòng
                i++;
                #endregion

            } while (true);

            if (lstClassIns.Count == lstImportClass.Count)
            {
                ImportClass(lstClassIns);
                //Chiendd: 14/03/2015, sua thong bao import thanh cong n/m lop hoc
                string retMsg = String.Format("{0} {1}/{2} lớp học", Res.Get("Common_Label_ImportSuccessMessage"), lstClassIns.Count, lstImportClass.Count);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            else
            {
                ViewData[ImportClassConstants.LIST_CLASSPROFILE] = lstImportClass;
                // DungVA - Invisible Subcommittee 
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                    ViewData[ImportClassConstants.INVISIBLE_SUBCOMMITTEE] = true;
                else ViewData[ImportClassConstants.INVISIBLE_SUBCOMMITTEE] = false;
                // End of DungVA - Invisible Subcommittee 

                if (_globalInfo.IsCurrentYear) ViewData[ImportClassConstants.DISABLE_BTNSAVE] = false;
                else ViewData[ImportClassConstants.DISABLE_BTNSAVE] = true;

                Session["ListClassImport"] = lstClassIns;
                Session["ListAllClass"] = lstImportClass;
                return Json(new JsonMessage(string.Empty, JsonMessage.ERROR));
                //Có lỗi thì hệ thống hiển thị màn hình Lựa chọn thao tác import
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            Session["stream"] = file;
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }
                JsonResult res1 = Json(new JsonMessage());
                res1.ContentType = "text/plain";
                return res1;

            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }
        private bool ImportClass(List<ClassProfile> lstClassProfile)
        {
            IQueryable<ClassProfile> iqClassInCurrent = ClassProfileBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.AcademicYearID == _globalInfo.AcademicYearID && o.IsActive.Value);
            List<string> lstClassName = iqClassInCurrent.Select(o => o.DisplayName).ToList();
            int Count = lstClassProfile.Count;
            // Danh sach phai co ten lop khac nhau    
            if (Count > 0)
            {
                ClassProfile cpImport;
                ClassProfile cp;
                for (int i = 0; i < Count; i++)
                {
                    cpImport = lstClassProfile[i];
                    cpImport.IsActive = true;
                    //Gán loại lớp cho cấp GDTX
                    var school = SchoolProfileBusiness.Find(_globalInfo.SchoolID != null ? _globalInfo.SchoolID.Value: 0);
                    if (school != null && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                    {
                        var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                        var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                        var listEduTerrary = new List<int>() { 10, 11, 12 };
                        if (listEduLvPrimary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 1;
                        }
                        else if (listEduSecondary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 2;
                        }
                        else if (listEduTerrary.Contains(cpImport.EducationLevelID))
                        {
                            cpImport.ClassType = 3;
                        }
                    }
                    if (lstClassName.Contains(cpImport.DisplayName))
                    {
                        cp = iqClassInCurrent.FirstOrDefault(o => cpImport.DisplayName.Equals(o.DisplayName));
                        cp.EducationLevelID = cpImport.EducationLevelID;
                        cp.SubCommitteeID = cpImport.SubCommitteeID;
                        cp.HeadTeacherID = cpImport.HeadTeacherID;
                        cp.Section = cpImport.Section;
                        cp.SeperateKey = cpImport.SeperateKey;
                        cp.IsActive = true;
                        if (school != null && school.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                        {
                            var listEduLvPrimary = new List<int>() { 1, 2, 3, 4, 5 };
                            var listEduSecondary = new List<int>() { 6, 7, 8, 9 };
                            var listEduTerrary = new List<int>() { 10, 11, 12 };
                            if (listEduLvPrimary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 1;
                            }
                            else if (listEduSecondary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 2;
                            }
                            else if (listEduTerrary.Contains(cpImport.EducationLevelID))
                            {
                                cp.ClassType = 3;
                            }
                        }
                        ClassProfileBusiness.Update(cp);
                    }
                    else ClassProfileBusiness.Insert(cpImport);
                }
                ClassProfileBusiness.Save();
                Session["ListAllClass"] = null;
                Session["ListClassImport"] = null;
                return true;
            }
            else
            {
                return false;
            }
        }
        public PartialViewResult ViewDataImportExcel()
        {
            List<ImportClassViewModel> lstImportClass = (List<ImportClassViewModel>)Session["ListAllClass"];
            List<ClassProfile> lstClass = (List<ClassProfile>)Session["ListClassImport"];
            ViewData[ImportClassConstants.LIST_CLASSPROFILE] = lstImportClass;
            ViewData[ImportClassConstants.SHOW_MESSAGE_ERROR] = (lstClass.Count != lstImportClass.Count);
            if (_globalInfo.IsCurrentYear) ViewData[ImportClassConstants.DISABLE_BTNSAVE] = false;
            else ViewData[ImportClassConstants.DISABLE_BTNSAVE] = true;
            return PartialView("ViewDataImport");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportDataPass()
        {
            List<ClassProfile> ListClassProfile = (List<ClassProfile>)Session["ListClassImport"];
            List<ImportClassViewModel> lstImportClass = (List<ImportClassViewModel>)Session["ListAllClass"];
            bool retval = ImportClass(ListClassProfile);
            if (retval && ListClassProfile.Count > 0 && lstImportClass.Count > 0)
            {
                //return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
                //Chiendd: 14/03/2015, sua thong bao import thanh cong n/m lop hoc
                string retMsg = String.Format("{0} {1}/{2} lớp học", Res.Get("Common_Label_ImportSuccessMessage"), ListClassProfile.Count, lstImportClass.Count);
                return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS));
            }
            else
            {
                return Json(new JsonMessage("Không có dữ liệu hợp lệ."));
            }
        }
        public FileResult DowloadDataErr()
        {
            string ReportName = string.Empty;
            Stream excel = this.SetDataFileTemplate(ref ReportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }
        public FileResult DownloadFileTemplate()
        {
            string ReportName = string.Empty;
            Stream excel = this.SetDataFileTemplate(ref ReportName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            result.FileDownloadName = ReportName;
            return result;
        }
        private Stream SetDataFileTemplate(ref string reportName, bool isDataErr = false)
        {
            string reportCode = SystemParamsInFile.MAU_IMPORT_LOP_HOC;
            string ReportName = string.Empty;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook;
            try
            {
                oBook = VTExport.OpenWorkbook(templatePath);
            }
            catch
            {
                throw new BusinessException(Res.Get("Import_Validate_FileInUsed"));
            }
            //Lấy sheet 
            IVTWorksheet refSheet = null, sheet1 = null;
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                sheet1 = oBook.GetSheet(1);
                refSheet = oBook.GetSheet(2);
            }
            else
            {
                sheet1 = oBook.GetSheet(1);
                refSheet = oBook.GetSheet(2);
                // Ẩn cột ban học với cấp 1&2, Mầm non
                sheet1.HideColumn(4);
            }
            int START = 5;
            int startRow = START;
            IVTRange range = refSheet.GetRange("A5", "F5");
            IVTRange range1 = sheet1.GetRange("A5", "F5");

            string schoolName = Res.Get("ImportClass_Label_SchoolNameExcel") + _globalInfo.SchoolName.ToUpper();
            refSheet.SetCellValue("A2", schoolName);
            sheet1.SetCellValue("A2", schoolName); // chứa thông tin Import

            //// Title
            //sheet1.SetCellValue("A3", Res.Get("ImportClass_Label_listClassExcel"));
            //sheet1.SetCellValue("A4", Res.Get("ImportClass_Label_orderNumberExcel"));
            //sheet1.SetCellValue("B4", Res.Get("ImportClass_Label_educationLevelExcel"));
            //sheet1.SetCellValue("C4", Res.Get("ImportClass_Label_classNameExcel"));
            //sheet1.SetCellValue("D4", Res.Get("ImportClass_Label_subCommityNameExcel"));
            //sheet1.SetCellValue("E4", Res.Get("ImportClass_Label_teacherNameExcel"));
            ////sheet1.SetCellValue("F4", Res.Get("ImportClass_Label_schoolFacultyNameExcel"));
            ////Start: 2014/11/24 viethd4 Bổ sung buổi học
            //sheet1.SetCellValue("F4", Res.Get("ImportClass_Label_Section"));
            ////End: 2014/11/24 viethd4 Bổ sung buổi học
            // Nếu Cấp Mầm non - Set lại thông tin Nhóm lớp/Tên Lớp
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                sheet1.SetCellValue("B4", Res.Get("PupilProfile_Label_Class_MN"));
                sheet1.HideColumn(7);
            }
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;
            // Xuất DS cấp học
            int d = START;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                refSheet.CopyPasteSameRowHeigh(range, startRow);
                refSheet.SetCellValue("K" + d, lstEducationLevel[i].Resolution);
                d++;
            }
            int rowTeacher = START;
            // Danh sách GV hiện có
            IDictionary<string, object> dicTeacher = new Dictionary<string, object>();
            dicTeacher["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            //dicTeacher["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dicTeacher["isFromEduActive"] = true;
            List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, dicTeacher).ToList();
            // Sắp xếp Tiếng việt có dấu
            lstEmployee = lstEmployee.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name))
                 .ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName)).ToList();

            IDictionary<string, object> dicFalcuty = new Dictionary<string, object>();

            List<SchoolFaculty> lstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicFalcuty).OrderBy(o => o.FacultyName).ToList();

            IDictionary<int, string> DicSchoolFaculty = new Dictionary<int, string>();
            lstSchoolFaculty.ForEach(p =>
            {
                DicSchoolFaculty[p.SchoolFacultyID] = p.FacultyName;
            });

            int SchoolFacultyID = 0;

            for (int i = 0; i < lstEmployee.Count; i++)
            {
                SchoolFacultyID = lstEmployee[i].SchoolFacultyID.HasValue ? lstEmployee[i].SchoolFacultyID.Value : 0;
                refSheet.CopyPasteSameRowHeigh(range, startRow);
                refSheet.SetCellValue("M" + rowTeacher, lstEmployee[i].FullName + "\\ " + DicSchoolFaculty[SchoolFacultyID] + "\\ " + lstEmployee[i].EmployeeCode);
                rowTeacher++;
            }

            // Ban học
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                int rowSubcommitee = START;
                IDictionary<string, object> dicSubcommitee = new Dictionary<string, object>();
                dicSubcommitee["IsActive"] = true;
                List<SubCommittee> lstSubCommittee = SubCommitteeBusiness.Search(dicSubcommitee).OrderBy(o => o.Resolution).ToList();
                for (int i = 0; i < lstSubCommittee.Count; i++)
                {
                    refSheet.CopyPasteSameRowHeigh(range, startRow);
                    refSheet.SetCellValue("L" + rowSubcommitee, lstSubCommittee[i].Resolution);
                    rowSubcommitee++;
                }
            }


            //for (int i = 0; i < lstSchoolFaculty.Count; i++)
            //{
            //    refSheet.CopyPasteSameRowHeigh(range, startRow);
            //    refSheet.SetCellValue("N" + rowFalcuty, lstSchoolFaculty[i].FacultyName);
            //    rowFalcuty++;
            //}

            if (isDataErr)//fill dữ liệu TH download dữ liệu lỗi
            {
                List<ImportClassViewModel> lstImportClassEr = new List<ImportClassViewModel>();
                lstImportClassEr = (List<ImportClassViewModel>)Session["ListAllClass"];
                lstImportClassEr = lstImportClassEr.Where(p => p.Pass == false).ToList();
                ImportClassViewModel objClassErr = new ImportClassViewModel();    
                //Gán +1 cho START Do bị đẩy template
                START++;
                int startRowErr = START;
                for (int i = 0; i < lstImportClassEr.Count; i++)
                {
                    objClassErr = lstImportClassEr[i];
                    //STT
                    sheet1.SetCellValue(startRowErr, 1, i + 1);
                    //Khoi lop
                    sheet1.SetCellValue(startRowErr, 2, objClassErr.EducationLevelName);
                    //Ten lop
                    sheet1.SetCellValue(startRowErr, 3, objClassErr.DisplayName);
                    //Ban hoc
                    sheet1.SetCellValue(startRowErr, 4, objClassErr.SubCommitteeName);
                    //GVCN
                    sheet1.SetCellValue(startRowErr, 5, objClassErr.HeadTeacherName);
                    //To bo mon
                    //sheet1.SetCellValue(startRowErr, 6, objClassErr.SchoolFalcutyName);
                    //Buoi hoc
                    sheet1.SetCellValue(startRowErr, 7, objClassErr.Section);
                    //Mo ta
                    sheet1.SetCellValue(startRowErr, 8, objClassErr.Description);
                    startRowErr++;
                }
                sheet1.GetRange(START, 1, START + lstImportClassEr.Count - 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            else
            {
                sheet1.SetColumnWidth('H', 0);
            }

            reportName = reportDef.OutputNamePattern + "." + reportDef.OutputFormat;
            return oBook.ToStream();
        }

        #endregion
        #region ExportExcel ClassProfile

        public JsonResult GetReport(bool IsMultipleLevel, string strEducationLevel)
        {
            var objSchoolByID = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            var educationLevelID = _globalInfo.EducationLevel;
            var ListEducationLevel = new List<int>();
            List<ClassProfile> listClassProfile = new List<ClassProfile>();
            var ListAppliedLevelByGrade = new List<int?>();
            bool isZip = IsMultipleLevel;
            var AppliedTraningType = 0;
            if (objSchoolByID != null)
            {
                if (objSchoolByID.TrainingTypeID.HasValue && objSchoolByID.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                    AppliedTraningType = ClassProfileConstants.TRAINING_TYPE_GDTX;
                if (IsMultipleLevel) // Export all class school
                {
                    ListAppliedLevelByGrade = CommonList.AppliedLevelByGradeSchool(objSchoolByID.EducationGrade);
                    isZip = true;
                    if (!string.IsNullOrEmpty(strEducationLevel))
                    {
                        ListEducationLevel = strEducationLevel.Split(',').Select(x => int.Parse(x)).ToList();
                    }
                    listClassProfile = ClassProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolID == objSchoolByID.SchoolProfileID
                                                                && (_globalInfo.AcademicYearID == null || x.AcademicYearID == _globalInfo.AcademicYearID)
                                                                && ListEducationLevel.Contains(x.EducationLevelID))
                                                                .ToList();
                }
                else // Export selected level
                {
                    //Xử lý riêng cho cấp MN 2 cấp nhưng không nén file
                    if (strEducationLevel != "13,14,15,16,17,18" && AppliedTraningType != 3)
                    {
                        ListAppliedLevelByGrade.Add(_globalInfo.AppliedLevel);
                    }                   
                    else
                    {
                        ListAppliedLevelByGrade.Add(4);
                        ListAppliedLevelByGrade.Add(5);
                    }
                    if (_globalInfo.AppliedLevels != null && AppliedTraningType == 3)
                    {
                        if (_globalInfo.AppliedLevels.Contains(1))
                        {
                            ListEducationLevel.Add(1);
                            ListEducationLevel.Add(2);
                            ListEducationLevel.Add(3);
                            ListEducationLevel.Add(4);
                            ListEducationLevel.Add(5);
                        }
                        if (_globalInfo.AppliedLevels.Contains(2))
                        {
                            ListEducationLevel.Add(6);
                            ListEducationLevel.Add(7);
                            ListEducationLevel.Add(8);
                            ListEducationLevel.Add(9);
                        }
                        if (_globalInfo.AppliedLevels.Contains(3))
                        {
                            ListEducationLevel.Add(10);
                            ListEducationLevel.Add(11);
                            ListEducationLevel.Add(12);
                        }
                    }
                    else
                    {
                        ListEducationLevel = GetClassByAppliedLevel(_globalInfo.AppliedLevel.Value);
                    }
                    
                    listClassProfile = ClassProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolID == objSchoolByID.SchoolProfileID
                                                                && (_globalInfo.AcademicYearID == null || x.AcademicYearID == _globalInfo.AcademicYearID)
                                                                && ListEducationLevel.Contains(x.EducationLevelID))
                                                                .ToList();
                }
            }
            else
            {
                throw new BusinessException("Không có dữ liệu.");
            } 
            
            string type = JsonReportMessage.NEW;
            string reportCode = "";
            ProcessedReport processedReport = null;
            Transcripts transcript = new Transcripts();
            transcript.AppliedLevel = _globalInfo.AppliedLevel.GetValueOrDefault();
            transcript.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
            transcript.AcademicYearID = _globalInfo.AcademicYearID.Value;
            IDictionary<string, object> info = new Dictionary<string, object>();
            info["AppliedLevel"] = _globalInfo.AppliedLevel.GetValueOrDefault();
            info["SchoolID"] = _globalInfo.SchoolID.GetValueOrDefault();
            info["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            if (!isZip)
            {
                reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_DANHSACHLOP_123MN_Zip;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ClassProfileBusiness.GetReportClassProfile(info, isZip);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            string fileNameByLevel = ListAppliedLevelByGrade != null ? "C" + string.Join("-C", ListAppliedLevelByGrade) : string.Empty;
            fileNameByLevel = fileNameByLevel.Replace("C4-C5", "MN").Replace("C4", "NT").Replace("C5", "MG");
            if (AppliedTraningType == ClassProfileConstants.TRAINING_TYPE_GDTX)
            {
                fileNameByLevel = fileNameByLevel.Replace("C1", string.Empty).Replace("MN", string.Empty).Replace("NT", string.Empty).Replace("---", string.Empty).Replace("--", "-");
                fileNameByLevel += ("-GDTX");
            }
            if (type == JsonReportMessage.NEW)
            {
                if (isZip && strEducationLevel != "13,14,15,16,17,18" && AppliedTraningType != ClassProfileConstants.TRAINING_TYPE_GDTX)
                {
                    string FolderSaveFile = string.Empty;
                    
                    ClassProfileBusiness.DownloadZipFileClassProfile(listClassProfile, ListAppliedLevelByGrade, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value,AppliedTraningType, out FolderSaveFile);
                    if (string.IsNullOrEmpty(FolderSaveFile))
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            processedReport = ClassProfileBusiness.InsertZipFileClassProfileReport(transcript, output, fileNameByLevel, isZip);
                        }

                    }
                }
                else
                {
                    var fileNameToExport = ((_globalInfo.AppliedLevel == 4 || _globalInfo.AppliedLevel == 5) && AppliedTraningType != 3) ? "DanhSach-MN-Lop.xls"
                        : (_globalInfo.AppliedLevel == 1 && AppliedTraningType != 3) ? "DanhSach-C1-Lop.xls"
                        : (_globalInfo.AppliedLevel == 2 && AppliedTraningType != 3) ? "DanhSach-C2-Lop.xls"
                        : AppliedTraningType == 3 ? "DanhSach-GDTX-Lop.xls"
                        : "DanhSach-C3-Lop.xls";

                    if (strEducationLevel == "13,14,15,16,17,18")
                    {
                        fileNameToExport = "DanhSach-MN-Lop.xls";
                    }
                    string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/" + fileNameToExport;
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    var listClassProfileByLevel = listClassProfile.Where(x => ListEducationLevel.Contains(x.EducationLevelID)).ToList();
                    Stream excel = ClassProfileBusiness.CreateClassProfileReport(oBook, listClassProfileByLevel, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, AppliedTraningType, ListAppliedLevelByGrade);
                    processedReport = ClassProfileBusiness.InsertZipFileClassProfileReport(transcript, excel, fileNameByLevel, false);
                    excel.Close();
                }
                
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        public JsonResult GetNewReport(bool IsMultipleLevel, string strEducationLevel)
        {
            var objSchoolByID = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            var educationLevelID = _globalInfo.EducationLevel;
            var ListEducationLevel = new List<int>();
            List<ClassProfile> listClassProfile = new List<ClassProfile>();
            var ListAppliedLevelByGrade = new List<int?>();
            bool isZip = IsMultipleLevel;
            int AppliedTraningType = 0;
            if (objSchoolByID != null)
            {
                if (objSchoolByID.TrainingTypeID.HasValue && objSchoolByID.TrainingTypeID.Value == ClassProfileConstants.TRAINING_TYPE_GDTX)
                    AppliedTraningType = ClassProfileConstants.TRAINING_TYPE_GDTX;
                if (IsMultipleLevel) // Export all class school
                {
                    ListAppliedLevelByGrade = CommonList.AppliedLevelByGradeSchool(objSchoolByID.EducationGrade);
                    isZip = true;
                    if (!string.IsNullOrEmpty(strEducationLevel))
                    {
                        ListEducationLevel = strEducationLevel.Split(',').Select(x => int.Parse(x)).ToList();
                    }
                    listClassProfile = ClassProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolID == objSchoolByID.SchoolProfileID
                                                                && (_globalInfo.AcademicYearID == null || x.AcademicYearID == _globalInfo.AcademicYearID)
                                                                && ListEducationLevel.Contains(x.EducationLevelID))
                                                                .ToList();
                }
                else // Export selected level
                {
                    //Xử lý riêng cho cấp MN 2 cấp nhưng không nén file
                    if (strEducationLevel != "13,14,15,16,17,18" && AppliedTraningType != 3)
                    {
                        ListAppliedLevelByGrade.Add(_globalInfo.AppliedLevel);
                    }
                    else 
                    {
                        ListAppliedLevelByGrade.Add(4);
                        ListAppliedLevelByGrade.Add(5);
                    }
                    if (_globalInfo.AppliedLevels != null && AppliedTraningType == 3)
                    {
                        if ( _globalInfo.AppliedLevels.Contains(1))
                        {
                            ListEducationLevel.Add(1);
                            ListEducationLevel.Add(2);
                            ListEducationLevel.Add(3);
                            ListEducationLevel.Add(4);
                            ListEducationLevel.Add(5);
                        }
                        if (_globalInfo.AppliedLevels.Contains(2))
                        {
                            ListEducationLevel.Add(6);
                            ListEducationLevel.Add(7);
                            ListEducationLevel.Add(8);
                            ListEducationLevel.Add(9);
                        }
                        if (_globalInfo.AppliedLevels.Contains(3))
                        {
                            ListEducationLevel.Add(10);
                            ListEducationLevel.Add(11);
                            ListEducationLevel.Add(12);
                        }
                    }
                    else
                    {
                        ListEducationLevel = GetClassByAppliedLevel(_globalInfo.AppliedLevel.Value);
                    }
                    listClassProfile = ClassProfileBusiness.All.Where(x => x.IsActive == true && x.SchoolID == objSchoolByID.SchoolProfileID
                                                                && (_globalInfo.AcademicYearID == null || x.AcademicYearID == _globalInfo.AcademicYearID)
                                                                && ListEducationLevel.Contains(x.EducationLevelID))
                                                                .ToList();
                }
            }
            else
            {
                throw new BusinessException("Không có dữ liệu.");
            }
            ProcessedReport processedReport = null;
            GlobalInfo GlobalInfo = new GlobalInfo();
            Transcripts transcript = new Transcripts();
            string fileNameByLevel = ListAppliedLevelByGrade != null ? "C" + string.Join("-C", ListAppliedLevelByGrade) : string.Empty;
            fileNameByLevel = fileNameByLevel.Replace("C4-C5", "MN").Replace("C4", "NT").Replace("C5", "MG");
            if (AppliedTraningType == ClassProfileConstants.TRAINING_TYPE_GDTX)
            {
                fileNameByLevel = fileNameByLevel.Replace("C1", string.Empty).Replace("MN", string.Empty).Replace("NT", string.Empty).Replace("---", string.Empty).Replace("--", "-");
                fileNameByLevel += ("-GDTX");
            }
            transcript.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            transcript.AppliedLevel = GlobalInfo.AppliedLevel.GetValueOrDefault();
            transcript.TrainingType = _globalInfo.TrainingType.GetValueOrDefault();
            if (isZip && strEducationLevel != "13,14,15,16,17,18" && AppliedTraningType != ClassProfileConstants.TRAINING_TYPE_GDTX)
            {
                string FolderSaveFile = string.Empty;
                ClassProfileBusiness.DownloadZipFileClassProfile(listClassProfile, ListAppliedLevelByGrade, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value,AppliedTraningType, out FolderSaveFile); 
                if (FolderSaveFile == "")
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                }
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                
               
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = ClassProfileBusiness.InsertZipFileClassProfileReport(transcript, output,fileNameByLevel, isZip);
                    }

                }
            }
            else
            {
                var fileNameToExport = ((_globalInfo.AppliedLevel == 4 || _globalInfo.AppliedLevel == 5) && AppliedTraningType != 3) ? "DanhSach-MN-Lop.xls"
                        : (_globalInfo.AppliedLevel == 1 && AppliedTraningType != 3) ? "DanhSach-C1-Lop.xls"
                        : (_globalInfo.AppliedLevel == 2 && AppliedTraningType != 3) ? "DanhSach-C2-Lop.xls"
                        : AppliedTraningType == 3 ? "DanhSach-GDTX-Lop.xls"
                        : "DanhSach-C3-Lop.xls";
                if (strEducationLevel == "13,14,15,16,17,18")
                {
                    fileNameToExport = "DanhSach-MN-Lop.xls";
                }
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/Lop/" + fileNameToExport;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                if (_globalInfo.AppliedLevels != null && AppliedTraningType == 3)
                {
                    if (_globalInfo.AppliedLevels.Contains(1))
                    {
                        ListAppliedLevelByGrade.Add(1);
                        ListAppliedLevelByGrade.Add(2);
                        ListAppliedLevelByGrade.Add(3);
                        ListAppliedLevelByGrade.Add(4);
                        ListAppliedLevelByGrade.Add(5);
                    }
                    if (_globalInfo.AppliedLevels.Contains(2))
                    {
                        ListAppliedLevelByGrade.Add(6);
                        ListAppliedLevelByGrade.Add(7);
                        ListAppliedLevelByGrade.Add(8);
                        ListAppliedLevelByGrade.Add(9);
                    }
                    if (_globalInfo.AppliedLevels.Contains(3))
                    {
                        ListAppliedLevelByGrade.Add(10);
                        ListAppliedLevelByGrade.Add(11);
                        ListAppliedLevelByGrade.Add(12);
                    }
                }
                var listClassProfileByLevel = listClassProfile.Where(x => ListAppliedLevelByGrade.Contains(x.EducationLevelID)).ToList();
                Stream excel = ClassProfileBusiness.CreateClassProfileReport(oBook, listClassProfileByLevel, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, AppliedTraningType, ListAppliedLevelByGrade);
                processedReport = ClassProfileBusiness.InsertZipFileClassProfileReport(transcript, excel, fileNameByLevel, false);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }


        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);

            Stream excel = null;
            FileStreamResult result = null;
            if ((SystemParamsInFile.REPORT_DANHSACHLOP_123MN_Zip.Equals(processedReport.ReportCode)))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }
            result.FileDownloadName = processedReport.ReportName;
            return result;
        }
        private List<int> GetClassByAppliedLevel(int appliedLevel)
        {
            var ListEducationLevel = new List<int>();
            switch (appliedLevel)
            {
                case 1:
                    ListEducationLevel = new List<int>() { 1, 2, 3, 4, 5 };
                    break;
                case 2:
                    ListEducationLevel = new List<int>() { 6, 7, 8, 9 };
                    break;
                case 3:
                    ListEducationLevel = new List<int>() { 10, 11, 12 };
                    break;
                case 4:
                    ListEducationLevel = new List<int>() { 13, 14, 15 };
                    break;
                case 5:
                    ListEducationLevel = new List<int>() { 16, 17, 18 };
                    break;
                default:
                    ListEducationLevel = new List<int>();
                    break;
            }
            return ListEducationLevel;
        }
        #endregion

        #region Các cột enum fix cứng của CT GDTX
        private SelectList GetDataSelectList(int type)
        {
            if (type == 1)
            {
                var dic = ClassProfileBusiness.DicApprenticeshipClassTypeList();
                List<ListItem> listItemClassType = new List<ListItem>(dic.Count);
                foreach (var item in dic)
                {
                    listItemClassType.Add(new ListItem
                    {
                        Text = item.Value,
                        Value = item.Key
                    });
                }
                return new SelectList(listItemClassType, "Value", "Text");
            }
            else
            {
                var dic = ClassProfileBusiness.DicApprenticeshipTrainingTypeList();
                List<ListItem> listItemTrainingType = new List<ListItem>(dic.Count);
                foreach (var item in dic)
                {
                    listItemTrainingType.Add(new ListItem
                    {
                        Text = item.Value,
                        Value = item.Key
                    });
                }
                return new SelectList(listItemTrainingType, "Value", "Text");
            }
        }
        #endregion
    }
}
