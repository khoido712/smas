﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SituationStatisticsPupilArea
{
    public class SituationStatisticsPupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SituationStatisticsPupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SituationStatisticsPupilArea_default",
                "SituationStatisticsPupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
