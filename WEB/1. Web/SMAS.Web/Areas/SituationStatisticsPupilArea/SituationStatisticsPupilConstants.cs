﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SituationStatisticsPupilArea
{
    public class SituationStatisticsPupilConstants
    {
        public const string LIST_ACADEMIC_YEAER = "LIST_ACADEMIC_YEAER";
        public const string LIST_MONTH = "LIST_MONTH";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_APPLIED_LEVEL = "LIST_APPLIED_LEVEL";
        public const string LIST_EDUCATION_LEVEL = "LIST_EDUCATION_LEVEL";
        public const string APPLIED_LEVEL_PRIMARY = "Cấp 1";
        public const string APPLIED_LEVEL_SECONDARY = "Cấp 2";
        public const string APPLIED_LEVEL_TERTIARY = "Cấp 3";
        public const string HS_THCS_TKBiendongHS_112017 = "HS_THCS_TKBiendongHS_112017";
        public const string SGD_TKBienDongHS_112017 = "SGD_TKBienDongHS_112017";
    }
}